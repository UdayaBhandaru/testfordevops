﻿// <copyright file="SupplyChainConstants.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.SupplyChainManagement
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal static class SupplyChainConstants
    {
        public const string GetSupplyChainPackage = "RECEIVING_MGMT";
        public const string ObjTypeScheduler = "ORDER_SCHEDULER_TAB";
        public const string RecordObjScheduler = "ORDER_SCHEDULER_REC";
        public const string GetProcScheduler = "GETSCHEDULER_ORDERLIST";
        public const string DomainHdrLocationType = "LOC_TYPE";

        // Shipment SKU related
        public const string GetProcShipmentSku = "GETSHIPSKU";
        public const string SetProcShipmentSku = "SETSHIPSKU";
        public const string ObjTypeShipmentSku = "SHIPSKU_TAB";
        public const string RecordObjShipmentSku = "SHIPSKU_REC";

        // Shipment Details related
        public const string GetProcShipmentDetails = "GETSHIPMENT";
        public const string ObjTypeShipmentDetails = "SHIPMENT_TAB";
        public const string RecordObjShipmentDetails = "SHIPMENT_REC";
        public const string SetProcShipmentDetails = "SETSHIPMENT";

        // Domain Header Ids
        public const string DomainHdrShipOrigin = "SHPORG";
        public const string DomainHdrStatusCode = "SHPSTAT";
        public const string DomainHdrInvoiceMatching = "INVMSTAT";
        public const string DomainHdrDistroType = "DISTTYP";
        public const string DomainHdrSkuStatusCode = "SHSKUSTAT";
        public const string DomainHdrIndicator = "INDICATOR";

        // Order Shipment related
        public const string GetProcOrderShipment = "GETSHIPMENT_LIST";
        public const string ObjTypeOrderShipment = "SHIPMENT_COMM_TAB";
        public const string RecordObjOrderShipment = "SHIPMENT_COMM_REC";

        // Print objects
        public const string ObjTypeShipmentPrint = "SHIPMENT_PRINT_TAB";
        public const string GetProcShipmentPrint = "GETSHIPMENTPRINT";
        public const string RecordObjShipmentPrint = "SHIPMENT_PRINT_REC";

        // Mobile Receiving - Order Head
        public const string RecordObjOrderHead = "RCVD_MOBILE_REC";
        public const string ObjTypeOrderHead = "RCVD_MOBILE_TAB";
        public const string GetProcOrderHead = "GETRCVDMOBILEDTL";

        // Mobile Receiving - Order List
        public const string GetProcOrderList = "GETMOBILEORDERLIST";
        public const string ObjTypeOrderList = "ORDER_NO_TAB";
        public const string RecordObjOrderList = "ORDER_NO_REC";

        // Mobile Receiving - Order Item Details
        public const string RecordObjOrderItemDetails = "MOBILE_ORDER_REC";
        public const string ObjTypeOrderItemDetails = "MOBILE_ORDER_TAB";
        public const string GetProcOrderItemDetails = "GETMOBILEORDERS";
        public const string SetProcOrderItemDetails = "SETMOBILESHIPMENT";
        public const string OrderItemDetailsTbl = "P_MOBILE_ORDER_TAB";
    }
}
