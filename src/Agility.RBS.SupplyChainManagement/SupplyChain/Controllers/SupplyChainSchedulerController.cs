﻿// <copyright file="SupplyChainSchedulerController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.SupplyChainManagement.SupplyChain
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.SupplyChainManagement.SupplyChain.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [Route("api/[controller]/[action]")]
    public class SupplyChainSchedulerController : Controller
    {
        private readonly ServiceDocument<SupplyChainSchedulerModel> serviceDocument;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly SupplyChainRepository supplyChainRepository;
        private readonly DomainDataRepository domainDataRepository;

        public SupplyChainSchedulerController(ServiceDocument<SupplyChainSchedulerModel> serviceDocument, SupplyChainRepository supplyChainRepository, DomainDataRepository domainDataRepository)
        {
            this.supplyChainRepository = supplyChainRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in the Scheduler List page.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<SupplyChainSchedulerModel>> List()
        {
            this.serviceDocument.DomainData.Add("locationList", this.domainDataRepository.LocationDomainGet().Result);
            this.serviceDocument.DomainData.Add("locationType", await this.domainDataRepository.DomainDetailData(SupplyChainConstants.DomainHdrLocationType));
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for fetching Scheduler records based on search criteria when Search button clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<SupplyChainSchedulerModel>> GetSchedulerDataForGrid()
        {
            await this.serviceDocument.ToListAsync(this.GetSupplyChainScheduleDataForGrid);
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for fetching shipment Id based on request parameters.
        /// </summary>
        /// <param name="ordShipModel">OrderShipmentRequestModel</param>
        /// <returns>Shipment Id</returns>
        [HttpPost]
        [AllowAnonymous]
        public async ValueTask<int> GetShipmentForOrder([FromBody] OrderShipmentRequestModel ordShipModel)
        {
            int shipmentId = 0;
            var orderShipment = await this.supplyChainRepository.GetShipmentForOrder(ordShipModel, this.serviceDocumentResult);

            if (orderShipment != null && orderShipment.Count > 0)
            {
                shipmentId = orderShipment[0].ShipmentId;
            }

            return shipmentId;
        }

        /// <summary>
        /// This API invoked for fetching shipments based on request parameters.
        /// </summary>
        /// <param name="ordShipModel">OrderShipmentRequestModel</param>
        /// <returns>List Of OrderShipmentResponseModel</returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<List<OrderShipmentResponseModel>> GetShipmentListForOrder([FromBody] OrderShipmentRequestModel ordShipModel)
        {
            try
            {
                var orderShipment = await this.supplyChainRepository.GetShipmentForOrder(ordShipModel, this.serviceDocumentResult);

                return orderShipment;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private async Task<List<SupplyChainSchedulerModel>> GetSupplyChainScheduleDataForGrid()
        {
            try
            {
                var supplyChainScheduleData = await this.supplyChainRepository.GetSupplyChainScheduleData(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return supplyChainScheduleData;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }
    }
}