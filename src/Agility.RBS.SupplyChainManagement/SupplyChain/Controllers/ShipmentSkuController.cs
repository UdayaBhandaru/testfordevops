﻿// <copyright file="ShipmentSkuController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.SupplyChainManagement.SupplyChain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Constants;
    using Agility.RBS.Core.Models;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Models;
    using Agility.RBS.SupplyChainManagement.SupplyChain.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [Route("api/[controller]/[action]")]
    public class ShipmentSkuController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly ServiceDocument<ShipmentSkuModel> serviceDocument;
        private readonly SupplyChainRepository supplyChainRepository;
        private int shipmentId;

        public ShipmentSkuController(
            ServiceDocument<ShipmentSkuModel> serviceDocument,
            SupplyChainRepository supplyChainRepository,
            DomainDataRepository domainDataRepository)
        {
            this.serviceDocument = serviceDocument;
            this.supplyChainRepository = supplyChainRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        /// <summary>
        /// This API invoked for loading grid data along with data for autocomplete, radio button fields in Shipment SKU Page.
        /// </summary>
        /// <param name="shipmentId">Shipment Id</param>
        /// <param name="orderId">Order Id</param>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<ShipmentSkuModel>> List(int shipmentId, int orderId)
        {
            this.shipmentId = shipmentId;
            this.serviceDocument.DomainData.Add("orderItemList", this.domainDataRepository.OrderItemListGet(orderId).Result);
            this.serviceDocument.DomainData.Add("distroType", this.domainDataRepository.DomainDetailData(SupplyChainConstants.DomainHdrDistroType).Result);
            this.serviceDocument.DomainData.Add("skuStatusCode", this.domainDataRepository.DomainDetailData(SupplyChainConstants.DomainHdrSkuStatusCode).Result);
            this.serviceDocument.DomainData.Add("locations", this.domainDataRepository.CommonData("LOC").Result);
            this.serviceDocument.DomainData.Add("WeightUoms", this.domainDataRepository.FilteredUomsDomainGet(RbCommonConstants.UomClassMass).Result);
            this.serviceDocument.DomainData.Add("indicator", this.domainDataRepository.DomainDetailData(SupplyChainConstants.DomainHdrIndicator).Result);
            this.serviceDocument.LocalizationData.AddData("ShipmentSku");
            await this.serviceDocument.ToListAsync(this.ShipmentSkuSearch);
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for saving grid data in the Shipment SKU page when Save button clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<ShipmentSkuModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.ShipmentSkuSave);
            return this.serviceDocument;
        }

        private async Task<List<ShipmentSkuModel>> ShipmentSkuSearch()
        {
            try
            {
                var shipmentSku = await this.supplyChainRepository.ShipmentSkuGet(this.shipmentId, this.serviceDocumentResult, UserProfileHelper.GetInMemoryUserId());
                this.serviceDocument.Result = this.serviceDocumentResult;
                return shipmentSku;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> ShipmentSkuSave()
        {
            this.serviceDocument.Result = await this.supplyChainRepository.ShipmentSkuSet(this.serviceDocument.DataProfile.DataModel);
            if (this.serviceDocument.Result.Type != MessageType.Success)
            {
                return false;
            }

            return true;
        }
    }
}
