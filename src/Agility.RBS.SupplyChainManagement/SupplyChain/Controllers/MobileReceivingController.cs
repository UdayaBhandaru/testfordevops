﻿// <copyright file="MobileReceivingController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.SupplyChainManagement.SupplyChain
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.SupplyChainManagement.SupplyChain.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [Route("api/[controller]/[action]")]
    public class MobileReceivingController : Controller
    {
        private readonly ServiceDocument<OrderHeadResponseModel> serviceDocument;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly SupplyChainRepository supplyChainRepository;
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocument<OrderListResponseModel> serviceDocOrderListResp;
        private readonly ServiceDocument<OrderItemDetailsModel> serviceDocOrderItemDtls;
        private OrderListRequestModel orderListRequestModel;
        private OrderItemDetailsRequestModel orderItemDetailsRequestModel;

        public MobileReceivingController(
            ServiceDocument<OrderHeadResponseModel> serviceDocument,
            SupplyChainRepository supplyChainRepository,
            DomainDataRepository domainDataRepository,
            ServiceDocument<OrderListResponseModel> serviceDocOrderListResp,
            ServiceDocument<OrderItemDetailsModel> serviceDocOrderItemDtls)
        {
            this.supplyChainRepository = supplyChainRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocOrderListResp = serviceDocOrderListResp;
            this.serviceDocOrderItemDtls = serviceDocOrderItemDtls;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        /// <summary>
        /// This API invoked for fetching data for autocomplete, radio button fields.
        /// </summary>
        /// <param name="model">CommonListRequestModel</param>
        /// <returns>Service Document</returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<ServiceDocument<OrderHeadResponseModel>> GetLocationDetails([FromBody]CommonListRequestModel model)
        {
            this.serviceDocument.DomainData.Add("locationList", this.domainDataRepository.LocationDomainGet(model.LoggedInUserId).Result);
            this.serviceDocument.DomainData.Add("locationType", await this.domainDataRepository.DomainDetailData(SupplyChainConstants.DomainHdrLocationType));
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for fetching data for Supplier autocomplete field.
        /// </summary>
        /// <returns>Service Document</returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ServiceDocument<OrderHeadResponseModel>> GetSupplierList()
        {
            this.serviceDocument.DomainData.Add("supplier", this.domainDataRepository.SupplierDomainGet().Result);
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for populating fields in Order Head screen of Receiving Mobile App.
        /// </summary>
        /// <param name="model">OrderHeadRequestModel</param>
        /// <returns>OrderHeadResponseModel</returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<OrderHeadResponseModel> GetOrderHead([FromBody]OrderHeadRequestModel model)
        {
            OrderHeadResponseModel shipmentDetails = new OrderHeadResponseModel();
            try
            {
                shipmentDetails = await this.supplyChainRepository.OrderHeadGet(model, this.serviceDocumentResult);
                shipmentDetails.ResultMessage = this.serviceDocumentResult.InnerException;
                return shipmentDetails;
            }
            catch (Exception ex)
            {
                shipmentDetails.ResultMessage = ex.Message;
                shipmentDetails.ExceptionDetails = ex.ToString();
                return shipmentDetails;
            }
        }

        /// <summary>
        /// This API invoked for fetching orders based on request parameters.
        /// </summary>
        /// <param name="model">OrderListRequestModel</param>
        /// <returns>Service Document</returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<ServiceDocument<OrderListResponseModel>> GetOrderList([FromBody]OrderListRequestModel model)
        {
            this.orderListRequestModel = model;
            await this.serviceDocOrderListResp.ToListAsync(this.OrderListGet);
            return this.serviceDocOrderListResp;
        }

        /// <summary>
        /// This API invoked for populating fields in Order Details screen of Receiving Mobile App.
        /// </summary>
        /// <param name="model">OrderItemDetailsRequestModel</param>
        /// <returns>Service Document</returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<ServiceDocument<OrderItemDetailsModel>> GetOrderItemDetails([FromBody]OrderItemDetailsRequestModel model)
        {
            this.orderItemDetailsRequestModel = model;
            await this.serviceDocOrderItemDtls.OpenAsync(this.OrderItemDetailsOpen);
            return this.serviceDocOrderItemDtls;
        }

        /// <summary>
        /// This API invoked for saving field values in Order Details screen of Receiving Mobile App.
        /// </summary>
        /// <param name="detailModel">OrderItemDetailsModel</param>
        /// <returns>Shipment Id</returns>
        [HttpPost]
        [AllowAnonymous]
        public async ValueTask<int> SaveOrderItemDetails([FromBody]OrderItemDetailsModel detailModel)
        {
            this.serviceDocOrderItemDtls.Result = await this.supplyChainRepository.OrderItemDetailsSet(detailModel);
            if (this.serviceDocOrderItemDtls.Result.Type != MessageType.Success)
            {
                return 0;
            }

            return this.supplyChainRepository.ShipmentId;
        }

        private async Task<List<OrderListResponseModel>> OrderListGet()
        {
            try
            {
                var orderList = await this.supplyChainRepository.OrderListGet(this.orderListRequestModel, this.serviceDocumentResult);
                this.serviceDocOrderListResp.Result = this.serviceDocumentResult;
                return orderList;
            }
            catch (Exception ex)
            {
                this.serviceDocOrderListResp.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<OrderItemDetailsModel> OrderItemDetailsOpen()
        {
            try
            {
                var shipmentDetails = await this.supplyChainRepository.OrderItemDetailsGet(this.orderItemDetailsRequestModel, this.serviceDocumentResult);
                this.serviceDocOrderItemDtls.Result = this.serviceDocumentResult;
                return shipmentDetails;
            }
            catch (Exception ex)
            {
                this.serviceDocOrderItemDtls.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }
    }
}