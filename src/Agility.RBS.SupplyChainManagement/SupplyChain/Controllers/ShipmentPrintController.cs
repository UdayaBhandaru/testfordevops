﻿// <copyright file="ShipmentPrintController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.SupplyChainManagement.SupplyChain
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.SupplyChainManagement.SupplyChain.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    [Route("api/[controller]/[action]")]
    public class ShipmentPrintController : Controller
    {
        private readonly ServiceDocument<ShipmentHeadPrintModel> serviceDocument;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly SupplyChainRepository costChangeRepository;
        private readonly ILogger<ShipmentPrintController> logger;
        private int shipmentId;

        public ShipmentPrintController(
            ServiceDocument<ShipmentHeadPrintModel> serviceDocument,
            SupplyChainRepository costChangeRepository,
            ILogger<ShipmentPrintController> logger)
        {
            this.serviceDocument = serviceDocument;
            this.costChangeRepository = costChangeRepository;
            this.logger = logger;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        /// <summary>
        /// This API invoked for fetching Shipment Details and SKU data when Print button clicked.
        /// </summary>
        /// <param name="shipmentId">Shipment Id</param>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<ShipmentHeadPrintModel>> List(int shipmentId)
        {
            this.shipmentId = shipmentId;
            this.logger.LogInformation("Shipment Print Started");
            await this.serviceDocument.ToListAsync(this.GetShipmentPrintData);
            this.serviceDocument.LocalizationData.AddData("RbsPrint");
            this.logger.LogInformation("Shipment Print Completed");
            return this.serviceDocument;
        }

        private async Task<List<ShipmentHeadPrintModel>> GetShipmentPrintData()
        {
            try
            {
                var printData = await this.costChangeRepository.GetShipmentPrintData(this.shipmentId, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return printData;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                this.logger.LogInformation(ex.ToString());
                return null;
            }
        }
    }
}