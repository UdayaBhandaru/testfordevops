﻿// <copyright file="ShipmentDetailsController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.SupplyChainManagement.SupplyChain
{
    using System;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.SupplyChainManagement.SupplyChain.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [Route("api/[controller]/[action]")]
    public class ShipmentDetailsController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly SupplyChainRepository supplyChainRepository;
        private readonly ServiceDocument<ShipmentDetailsModel> serviceDocument;
        private int shipmentId;

        public ShipmentDetailsController(
            ServiceDocument<ShipmentDetailsModel> serviceDocument,
            SupplyChainRepository supplyChainRepository,
        DomainDataRepository domainDataRepository)
        {
            this.serviceDocument = serviceDocument;
            this.supplyChainRepository = supplyChainRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in Shipment Details page when Add button clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<ShipmentDetailsModel>> New()
        {
            await this.BasicDomainData();
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for populating fields in the Shipment Details page when Edit icon clicked.
        /// </summary>
        /// <param name="id">Shipment Id</param>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<ShipmentDetailsModel>> Open(int id)
        {
            this.shipmentId = id;
            await this.serviceDocument.OpenAsync(this.ShipmentDetailsOpen);
            await this.BasicDomainData();
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for saving field values in the Shipment Details page when Save button clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<ShipmentDetailsModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.ShipmentDetailsSave);
            return this.serviceDocument;
        }

        private async Task<bool> ShipmentDetailsSave()
        {
            this.serviceDocument.Result = await this.supplyChainRepository.ShipmentDetailsSet(this.serviceDocument.DataProfile.DataModel);
            if (this.serviceDocument.Result.Type != MessageType.Success)
            {
                return false;
            }

            this.serviceDocument.DataProfile.DataModel.Shipment = this.supplyChainRepository.ShipmentId;
            return true;
        }

        private async Task<ShipmentDetailsModel> ShipmentDetailsOpen()
        {
            try
            {
                var shipmentDetails = await this.supplyChainRepository.ShipmentDetailsGet(this.shipmentId, this.serviceDocumentResult, UserProfileHelper.GetInMemoryUserId());
                this.serviceDocument.Result = this.serviceDocumentResult;
                return shipmentDetails;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task BasicDomainData()
        {
            this.serviceDocument.DomainData.Add("shipOrigin", this.domainDataRepository.DomainDetailData(SupplyChainConstants.DomainHdrShipOrigin).Result);
            this.serviceDocument.DomainData.Add("statusCode", this.domainDataRepository.DomainDetailData(SupplyChainConstants.DomainHdrStatusCode).Result);
            this.serviceDocument.DomainData.Add("invoiceMatching", await this.domainDataRepository.DomainDetailData(SupplyChainConstants.DomainHdrInvoiceMatching));
            this.serviceDocument.DomainData.Add("storeLocations", this.domainDataRepository.CommonData("STORE").Result);
            this.serviceDocument.DomainData.Add("wareHouseLocations", this.domainDataRepository.CommonData("WAREHOUSE").Result);
            this.serviceDocument.DomainData.Add("locationType", this.domainDataRepository.DomainDetailData(SupplyChainConstants.DomainHdrLocationType).Result);
            this.serviceDocument.LocalizationData.AddData("ShipmentDetails");
        }
    }
}
