﻿// <copyright file="SupplyChainModelMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.SupplyChainManagement.SupplyChain.Mapping
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.RBS.Core;
    using Agility.RBS.SupplyChainManagement.SupplyChain.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class SupplyChainModelMapping : Profile
    {
        public SupplyChainModelMapping()
            : base("SupplyChainModelMapping")
        {
            this.CreateMap<OracleObject, SupplyChainSchedulerModel>()
                .ForMember(m => m.OrderNo, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["ORDER_NO"])))
                .ForMember(m => m.SupplierId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["SUPPLIER_ID"])))
                .ForMember(m => m.Eta, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["ETA"])))
                .ForMember(m => m.SupplierName, opt => opt.MapFrom(r => r["SUP_NAME"]))
                .ForMember(m => m.ReceivingLocationId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["LOCATION_ID"])))
                .ForMember(m => m.LocationName, opt => opt.MapFrom(r => r["LOC_NAME"]))
                .ForMember(m => m.LocationType, opt => opt.MapFrom(r => r["LOC_TYPE"]))
                .ForMember(m => m.LocationTypeDescription, opt => opt.MapFrom(r => r["LOC_TYPE_DESC"]))
                .ForMember(m => m.LangId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["LANG_ID"])));

            this.CreateMap<OracleObject, ShipmentSkuModel>()
                .ForMember(m => m.Shipment, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["SHIPMENT"])))
                .ForMember(m => m.SeqNo, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["SEQ_NO"])))
                .ForMember(m => m.Item, opt => opt.MapFrom(r => r["ITEM"]))
                .ForMember(m => m.DistroNo, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DISTRO_NO"])))
                .ForMember(m => m.DistroType, opt => opt.MapFrom(r => r["DISTRO_TYPE"]))
                .ForMember(m => m.DistroTypeDescription, opt => opt.MapFrom(r => r["DISTRO_TYPE_DESC"]))
                .ForMember(m => m.RefItem, opt => opt.MapFrom(r => r["REF_ITEM"]))
                .ForMember(m => m.Carton, opt => opt.MapFrom(r => r["CARTON"]))
                .ForMember(m => m.InvStatus, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["INV_STATUS"])))
                .ForMember(m => m.StatusCode, opt => opt.MapFrom(r => r["STATUS_CODE"]))
                .ForMember(m => m.StatusCodeDescription, opt => opt.MapFrom(r => r["STATUS_CODE_DESC"]))
                .ForMember(m => m.QtyReceived, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["QTY_RECEIVED"])))
                .ForMember(m => m.UnitCost, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["UNIT_COST"])))
                .ForMember(m => m.UnitRetail, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["UNIT_RETAIL"])))
                .ForMember(m => m.QtyExpected, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["QTY_EXPECTED"])))
                .ForMember(m => m.MatchInvcId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["MATCH_INVC_ID"])))
                .ForMember(m => m.AdjustType, opt => opt.MapFrom(r => r["ADJUST_TYPE"]))
                .ForMember(m => m.ActualReceivingStore, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["ACTUAL_RECEIVING_STORE"])))
                .ForMember(m => m.ActualReceivingStoreName, opt => opt.MapFrom(r => r["RECEIVING_STORE_NAME"]))
                .ForMember(m => m.ReconcileUserId, opt => opt.MapFrom(r => r["RECONCILE_USER_ID"]))
                .ForMember(m => m.ReconcileDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["RECONCILE_DATE"])))
                .ForMember(m => m.TamperedInd, opt => opt.MapFrom(r => r["TAMPERED_IND"]))
                .ForMember(m => m.DispositionedInd, opt => opt.MapFrom(r => r["DISPOSITIONED_IND"]))
                .ForMember(m => m.QtyMatched, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["QTY_MATCHED"])))
                .ForMember(m => m.WeightReceived, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["WEIGHT_RECEIVED"])))
                .ForMember(m => m.WeightReceivedUom, opt => opt.MapFrom(r => r["WEIGHT_RECEIVED_UOM"]))
                .ForMember(m => m.WeightExpected, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["WEIGHT_EXPECTED"])))
                .ForMember(m => m.WeightExpectedUom, opt => opt.MapFrom(r => r["WEIGHT_EXPECTED_UOM"]))
                .ForMember(m => m.IsExpiry, opt => opt.MapFrom(r => r["IS_EXPIRY"]))
                .ForMember(m => m.ExpiryDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["EXPIRY_DATE"])))
                .ForMember(m => m.CreatedBy, opt => opt.MapFrom(r => r["CREATED_BY"]))
                .ForMember(m => m.CreatedDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["CREATED_DATE"])))
                .ForMember(m => m.ModifiedBy, opt => opt.MapFrom(r => r["LAST_UPDATED_BY"]))
                .ForMember(m => m.ModifiedDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["LAST_UPDATED_DATE"])))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "SHIPSKU"));

            this.CreateMap<OracleObject, ShipmentDetailsModel>()
                .ForMember(m => m.Shipment, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["SHIPMENT"])))
                .ForMember(m => m.OrderNo, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["ORDER_NO"])))
                .ForMember(m => m.BillOfLadingNo, opt => opt.MapFrom(r => r["BOL_NO"]))
                .ForMember(m => m.AdvanceShipmentNotice, opt => opt.MapFrom(r => r["ASN"]))
                .ForMember(m => m.ShipDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["SHIP_DATE"])))
                .ForMember(m => m.ReceiveDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["RECEIVE_DATE"])))
                .ForMember(m => m.EstArrDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["EST_ARR_DATE"])))
                .ForMember(m => m.ShipOrigin, opt => opt.MapFrom(r => r["SHIP_ORIGIN"]))
                .ForMember(m => m.StatusCode, opt => opt.MapFrom(r => r["STATUS_CODE"]))
                .ForMember(m => m.InvcMatchStatus, opt => opt.MapFrom(r => r["INVC_MATCH_STATUS"]))
                .ForMember(m => m.InvcMatchDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["INVC_MATCH_DATE"])))
                .ForMember(m => m.ToLoc, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["TO_LOC"])))
                .ForMember(m => m.ToLocType, opt => opt.MapFrom(r => r["TO_LOC_TYPE"]))
                .ForMember(m => m.FromLoc, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["FROM_LOC"])))
                .ForMember(m => m.FromLocType, opt => opt.MapFrom(r => r["FROM_LOC_TYPE"]))
                .ForMember(m => m.Courier, opt => opt.MapFrom(r => r["COURIER"]))
                .ForMember(m => m.NoOfBoxes, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["NO_BOXES"])))
                .ForMember(m => m.ExtRefNoIn, opt => opt.MapFrom(r => r["EXT_REF_NO_IN"]))
                .ForMember(m => m.ExtRefNoOut, opt => opt.MapFrom(r => r["EXT_REF_NO_OUT"]))
                .ForMember(m => m.Comments, opt => opt.MapFrom(r => r["COMMENTS"]))
                .ForMember(m => m.ParentShipment, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["PARENT_SHIPMENT"])))
                .ForMember(m => m.MatchLocation, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["MATCH_LOCATION"])))
                .ForMember(m => m.FileCount, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["FILE_CNT"])))
                .ForMember(m => m.CreatedBy, opt => opt.MapFrom(r => r["CREATED_BY"]))
                .ForMember(m => m.CreatedDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["CREATED_DATE"])))
                .ForMember(m => m.ModifiedBy, opt => opt.MapFrom(r => r["LAST_UPDATED_BY"]))
                .ForMember(m => m.ModifiedDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["LAST_UPDATED_DATE"])))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "SHIPMENT"))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, OrderShipmentResponseModel>()
                .ForMember(m => m.ShipmentId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["SHIPMENT"])))
                .ForMember(m => m.EstimatedArrivalDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["ETA"])))
                .ForMember(m => m.LocationId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["LOCATION_ID"])))
                .ForMember(m => m.LocationName, opt => opt.MapFrom(r => r["LOC_NAME"]))
                .ForMember(m => m.OrderNo, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["ORDER_NO"])));

            this.CreateMap<OracleObject, ShipmentHeadPrintModel>()
                .ForMember(m => m.Shipment, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["SHIPMENT"])))
                .ForMember(m => m.OrderNo, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["ORDER_NO"])))
                .ForMember(m => m.BillOfLadingNo, opt => opt.MapFrom(r => r["BOL_NO"]))
                .ForMember(m => m.AdvanceShipmentNotice, opt => opt.MapFrom(r => r["ASN"]))
                .ForMember(m => m.ShipDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["SHIP_DATE"])))
                .ForMember(m => m.ReceiveDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["RECEIVE_DATE"])))
                .ForMember(m => m.EstArrDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["EST_ARR_DATE"])))
                .ForMember(m => m.ShipOriginDescription, opt => opt.MapFrom(r => r["SHIP_ORIGIN_DESC"]))
                .ForMember(m => m.StatusCodeDescription, opt => opt.MapFrom(r => r["STATUS_CODE_DESC"]))
                .ForMember(m => m.CreatedBy, opt => opt.MapFrom(r => r["CREATED_BY"]))
                .ForMember(m => m.CreatedDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["CREATED_DATE"])))
                .ForMember(m => m.ModifiedBy, opt => opt.MapFrom(r => r["LAST_UPDATED_BY"]))
                .ForMember(m => m.ModifiedDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["LAST_UPDATED_DATE"])));

            this.CreateMap<OracleObject, OrderHeadRcvngModel>()
                .ForMember(m => m.OrderNo, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["ORDER_NO"])))
                .ForMember(m => m.Supplier, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["SUPPLIER"])))
                .ForMember(m => m.SupplierName, opt => opt.MapFrom(r => r["SUP_NAME"]))
                .ForMember(m => m.LocationId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["LOCATION"])))
                .ForMember(m => m.ShipmentId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["SHIPMENT"])))
                .ForMember(m => m.TotalItemsOrdered, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["TOT_ITEMS_ORDERED"])))
                .ForMember(m => m.TotalItemsOrderedQuantity, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["TOT_ITEMS_ORD_QUANTITY"])))
                .ForMember(m => m.TotalItemsOrderedValue, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["TOT_ITEMS_ORD_VALUE"])))
                .ForMember(m => m.TotalItemsReceived, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["TOTAL_ITEMS_RCVD"])))
                .ForMember(m => m.TotalItemsReceivedQuantity, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["TOT_ITEMS_RCVD_QUANTITY"])))
                .ForMember(m => m.TotalItemsReceivedValue, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["TOT_ITEMS_RCVD_VALUE"])));

            this.CreateMap<OracleObject, OrderListResponseModel>()
                .ForMember(m => m.OrderNo, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["ORDER_NO"])))
                .ForMember(m => m.SupplierName, opt => opt.MapFrom(r => r["SUP_NAME"]));

            this.CreateMap<OracleObject, OrderItemDetailsModel>()
                .ForMember(m => m.OrderNo, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["ORDER_NO"])))
                .ForMember(m => m.ShipmentId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["SHIPMENT"])))
                .ForMember(m => m.Item, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["ITEM"])))
                .ForMember(m => m.ItemDescription, opt => opt.MapFrom(r => r["ITEM_DESC"]))
                .ForMember(m => m.DepartmentId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DEPT_ID"])))
                .ForMember(m => m.DepartmentDescription, opt => opt.MapFrom(r => r["DEPT_DESC"]))
                .ForMember(m => m.QuantityOrdered, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["QTY_ORDERED"])))
                .ForMember(m => m.QuantityReceived, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["QTY_RECEIVED"])))
                .ForMember(m => m.UnitCost, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["UNIT_COST"])))
                .ForMember(m => m.CostCurrency, opt => opt.MapFrom(r => r["COST_CURRENCY"]))
                .ForMember(m => m.UnitRetail, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["UNIT_RETAIL"])))
                .ForMember(m => m.RetailCurrency, opt => opt.MapFrom(r => r["RETAIL_CURRENCY"]))
                .ForMember(m => m.DefaultUnit, opt => opt.MapFrom(r => r["DEFAULT_UNIT"]))
                .ForMember(m => m.WeightExpected, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["WEIGHT_EXPECTED"])))
                .ForMember(m => m.WeightExpectedUom, opt => opt.MapFrom(r => r["WEIGHT_EXPECTED_UOM"]))
                .ForMember(m => m.IsExpiry, opt => opt.MapFrom(r => r["IS_EXPIRY"]))
                .ForMember(m => m.ExpiryDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["EXPIRY_DATE"])))
                .ForMember(m => m.UnitType, opt => opt.MapFrom(r => r["UNIT_TYPE"]))
                .ForMember(m => m.Uom, opt => opt.MapFrom(r => r["UOM"]))
                .ForMember(m => m.UnitCostInit, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["UNIT_COST_INIT"])))
                .ForMember(m => m.DiscountPercent, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["DISCOUNT_PERCENT"])))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));
        }
    }
}
