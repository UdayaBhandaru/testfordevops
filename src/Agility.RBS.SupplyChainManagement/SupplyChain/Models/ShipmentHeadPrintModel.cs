﻿// <copyright file="ShipmentHeadPrintModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.SupplyChainManagement.SupplyChain.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.Framework.Core.Profile.Entities;

    public class ShipmentHeadPrintModel : ProfileEntity
    {
        public ShipmentHeadPrintModel()
        {
            this.ShipmentSkuDetails = new List<ShipmentSkuModel>();
        }

        public int? Shipment { get; set; }

        public int? OrderNo { get; set; }

        public List<ShipmentSkuModel> ShipmentSkuDetails { get; private set; }

        public string BillOfLadingNo { get; set; }

        public string AdvanceShipmentNotice { get; set; }

        public DateTime? ShipDate { get; set; }

        public DateTime? ReceiveDate { get; set; }

        public DateTime? EstArrDate { get; set; }

        public string ShipOriginDescription { get; set; }

        public string StatusCodeDescription { get; set; }
    }
}
