﻿// <copyright file="OrderItemDetailsRequestModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.SupplyChainManagement.SupplyChain.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class OrderItemDetailsRequestModel
    {
        public string ItemBarcode { get; set; }

        public int? LocationId { get; set; }

        public int? OrderNo { get; set; }

        public int? ShipmentId { get; set; }
    }
}
