﻿// <copyright file="ShipmentDetailsModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.SupplyChainManagement.SupplyChain.Models
{
    using System;
    using Agility.Framework.Core.Profile.Entities;

    public class ShipmentDetailsModel : ProfileEntity
    {
        public int? Shipment { get; set; }

        public int? OrderNo { get; set; }

        public string BillOfLadingNo { get; set; }

        public string AdvanceShipmentNotice { get; set; }

        public DateTime? ShipDate { get; set; }

        public DateTime? ReceiveDate { get; set; }

        public DateTime? EstArrDate { get; set; }

        public string ShipOrigin { get; set; }

        public string StatusCode { get; set; }

        public string InvcMatchStatus { get; set; }

        public DateTime? InvcMatchDate { get; set; }

        public int? ToLoc { get; set; }

        public string ToLocType { get; set; }

        public int? FromLoc { get; set; }

        public string FromLocType { get; set; }

        public string Courier { get; set; }

        public int? NoOfBoxes { get; set; }

        public string ExtRefNoIn { get; set; }

        public string ExtRefNoOut { get; set; }

        public string Comments { get; set; }

        public int? ParentShipment { get; set; }

        public int? MatchLocation { get; set; }

        public int? FileCount { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }
    }
}
