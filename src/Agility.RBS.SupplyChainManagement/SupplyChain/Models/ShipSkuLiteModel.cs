﻿// <copyright file="ShipSkuLiteModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.SupplyChainManagement.SupplyChain.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class ShipSkuLiteModel : ProfileEntity
    {
        public string Item { get; set; }

        public decimal? QtyExpected { get; set; }

        public decimal? QtyReceived { get; set; }
    }
}
