﻿// <copyright file="OrderItemDetailsModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.SupplyChainManagement.SupplyChain.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.Framework.Core.Profile.Entities;

    public class OrderItemDetailsModel : ProfileEntity
    {
        public int? OrderNo { get; set; }

        public int? ShipmentId { get; set; }

        public int? Item { get; set; }

        public string ItemDescription { get; set; }

        public int? DepartmentId { get; set; }

        public string DepartmentDescription { get; set; }

        public decimal? QuantityOrdered { get; set; }

        public decimal? QuantityReceived { get; set; }

        public decimal? UnitCost { get; set; }

        public string CostCurrency { get; set; }

        public decimal? UnitRetail { get; set; }

        public string RetailCurrency { get; set; }

        public string DefaultUnit { get; set; }

        public decimal? WeightExpected { get; set; }

        public string WeightExpectedUom { get; set; }

        public string IsExpiry { get; set; }

        public DateTime? ExpiryDate { get; set; }

        public string UnitType { get; set; }

        public int? LocationId { get; set; }

        public string Operation { get; set; }

        public int? LangId { get; set; }

        public decimal? UnitCostInit { get; set; }

        public decimal? DiscountPercent { get; set; }

        public string Uom { get; set; }
    }
}
