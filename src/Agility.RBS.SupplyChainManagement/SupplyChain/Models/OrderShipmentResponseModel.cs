﻿// <copyright file="OrderShipmentResponseModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.SupplyChainManagement.SupplyChain.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class OrderShipmentResponseModel : OrderShipmentRequestModel
    {
        public int ShipmentId { get; set; }

        public string LocationName { get; set; }
    }
}
