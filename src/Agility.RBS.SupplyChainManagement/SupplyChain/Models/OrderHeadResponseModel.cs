﻿// <copyright file="OrderHeadResponseModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.SupplyChainManagement.SupplyChain.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.Framework.Core.Profile.Entities;

    public class OrderHeadResponseModel : ProfileEntity
    {
        public OrderHeadRcvngModel DataModel { get; set; }

        public string ResultMessage { get; set; }

        public string ExceptionDetails { get; set; }
    }
}
