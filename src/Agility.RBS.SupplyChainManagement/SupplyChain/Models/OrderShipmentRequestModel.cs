﻿// <copyright file="OrderShipmentRequestModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.SupplyChainManagement.SupplyChain.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class OrderShipmentRequestModel
    {
        public int? OrderNo { get; set; }

        public DateTime? EstimatedArrivalDate { get; set; }

        public int? LocationId { get; set; }
    }
}
