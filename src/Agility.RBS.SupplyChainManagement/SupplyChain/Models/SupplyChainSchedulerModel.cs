﻿// <copyright file="SupplyChainSchedulerModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.SupplyChainManagement.SupplyChain.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.Framework.Core.Profile.Entities;

    public class SupplyChainSchedulerModel : ProfileEntity
    {
        public int? ReceivingLocationId { get; set; }

        public string LocationName { get; set; }

        public string LocationType { get; set; }

        public string LocationTypeDescription { get; set; }

        public int? OrderNo { get; set; }

        public int? SupplierId { get; set; }

        public string SupplierName { get; set; }

        public DateTime? Eta { get; set; }

        public int? LangId { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        public string Operation { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Error { get; set; }
    }
}
