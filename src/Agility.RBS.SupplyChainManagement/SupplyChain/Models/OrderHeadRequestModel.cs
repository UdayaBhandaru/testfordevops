﻿// <copyright file="OrderHeadRequestModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.SupplyChainManagement.SupplyChain.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class OrderHeadRequestModel
    {
        // type of OrderNo is string to support barcode/qrcode input having 10+ length and alphanumeric characters
        public string OrderNo { get; set; }

        public int LocationId { get; set; }
    }
}
