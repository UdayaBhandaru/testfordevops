﻿// <copyright file="OrderListResponseModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.SupplyChainManagement.SupplyChain.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.Framework.Core.Profile.Entities;

    public class OrderListResponseModel : ProfileEntity
    {
        public int OrderNo { get; set; }

        public string SupplierName { get; set; }
    }
}
