﻿// <copyright file="OrderItemListRequestModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.SupplyChainManagement.SupplyChain.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class OrderItemListRequestModel
    {
        public int OrderId { get; set; }

        public int ShipmentId { get; set; }

        public string UserEmail { get; set; }
    }
}
