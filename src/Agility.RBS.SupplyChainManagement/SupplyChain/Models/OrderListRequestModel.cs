﻿// <copyright file="OrderListRequestModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.SupplyChainManagement.SupplyChain.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class OrderListRequestModel
    {
        public int LocationId { get; set; }

        public int SupplierId { get; set; }
    }
}
