﻿// <copyright file="ShipSkuRequestModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.SupplyChainManagement.SupplyChain.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class ShipSkuRequestModel
    {
        public int ShipmentId { get; set; }

        public int ItemId { get; set; }

        public string UserEmail { get; set; }
    }
}
