﻿// <copyright file="OrderHeadRcvngModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.SupplyChainManagement.SupplyChain.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class OrderHeadRcvngModel
    {
        public int? OrderNo { get; set; }

        public int? Supplier { get; set; }

        public string SupplierName { get; set; }

        public int? LocationId { get; set; }

        public int? ShipmentId { get; set; }

        public decimal? TotalItemsOrdered { get; set; }

        public decimal? TotalItemsOrderedQuantity { get; set; }

        public decimal? TotalItemsOrderedValue { get; set; }

        public decimal? TotalItemsReceived { get; set; }

        public decimal? TotalItemsReceivedQuantity { get; set; }

        public decimal? TotalItemsReceivedValue { get; set; }
    }
}
