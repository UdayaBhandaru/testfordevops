﻿// <copyright file="ShipmentSkuModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.SupplyChainManagement.SupplyChain.Models
{
    using System;
    using System.Collections.Generic;
    using Agility.Framework.Core.Profile.Entities;

    public class ShipmentSkuModel : ProfileEntity
    {
        public ShipmentSkuModel()
        {
            this.ListOfShipmentSkuModels = new List<ShipmentSkuModel>();
        }

        public int? Shipment { get; set; }

        public int? SeqNo { get; set; }

        public string Item { get; set; }

        public int? DistroNo { get; set; }

        public string DistroType { get; set; }

        public string DistroTypeDescription { get; set; }

        public string RefItem { get; set; }

        public string Carton { get; set; }

        public int? InvStatus { get; set; }

        public string StatusCode { get; set; }

        public string StatusCodeDescription { get; set; }

        public decimal? QtyReceived { get; set; }

        public decimal? UnitCost { get; set; }

        public decimal? UnitRetail { get; set; }

        public decimal? QtyExpected { get; set; }

        public int? MatchInvcId { get; set; }

        public string AdjustType { get; set; }

        public int? ActualReceivingStore { get; set; }

        public string ActualReceivingStoreName { get; set; }

        public string ReconcileUserId { get; set; }

        public DateTime? ReconcileDate { get; set; }

        public string TamperedInd { get; set; }

        public string DispositionedInd { get; set; }

        public decimal? QtyMatched { get; set; }

        public decimal? WeightReceived { get; set; }

        public string WeightReceivedUom { get; set; }

        public decimal? WeightExpected { get; set; }

        public string WeightExpectedUom { get; set; }

        public string IsExpiry { get; set; }

        public DateTime? ExpiryDate { get; set; }

        public string Operation { get; set; }

        public List<ShipmentSkuModel> ListOfShipmentSkuModels { get; private set; }

        public string TableName { get; set; }
    }
}
