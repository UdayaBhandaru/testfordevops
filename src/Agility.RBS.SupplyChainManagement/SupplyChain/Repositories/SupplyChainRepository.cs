﻿// <copyright file="SupplyChainRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.SupplyChainManagement.SupplyChain
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Constants;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.SupplyChainManagement.SupplyChain.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class SupplyChainRepository : BaseOraclePackage
    {
        public SupplyChainRepository()
        {
            this.PackageName = SupplyChainConstants.GetSupplyChainPackage;
        }

        public int ShipmentId { get; private set; }

        public async Task<List<SupplyChainSchedulerModel>> GetSupplyChainScheduleData(SupplyChainSchedulerModel scmSchInputmodel, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(SupplyChainConstants.RecordObjScheduler, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["LOCATION_ID"]] = scmSchInputmodel.ReceivingLocationId;
            PackageParams packageParameter = this.GetPackageParams(SupplyChainConstants.ObjTypeScheduler, SupplyChainConstants.GetProcScheduler);
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            OracleParameter oracleParameter;

            oracleParameter = new OracleParameter("P_USER", OracleDbType.VarChar)
            {
                Direction = ParameterDirection.Input,
                Value = UserProfileHelper.GetInMemoryUser().UserName
            };
            parameters.Add(oracleParameter);
            oracleParameter = new OracleParameter("P_STARTDATE", OracleDbType.Date)
            {
                Direction = ParameterDirection.Input,
                Value = scmSchInputmodel.FromDate
            };
            parameters.Add(oracleParameter);
            oracleParameter = new OracleParameter("P_ENDDATE", OracleDbType.Date)
            {
                Direction = ParameterDirection.Input,
                Value = scmSchInputmodel.ToDate
            };
            parameters.Add(oracleParameter);
            return await this.GetProcedure2<SupplyChainSchedulerModel>(recordObject, packageParameter, serviceDocumentResult, parameters);
        }

        public async Task<List<ShipmentSkuModel>> ShipmentSkuGet(long shipmentId, ServiceDocumentResult serviceDocumentResult, string userEmail)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(SupplyChainConstants.RecordObjShipmentSku, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["SHIPMENT"]] = shipmentId;
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();

            OracleParameter oracleParameter = new OracleParameter("P_USER", OracleDbType.VarChar)
            {
                Direction = ParameterDirection.Input,
                Value = userEmail
            };
            parameters.Add(oracleParameter);
            PackageParams packageParameter = this.GetPackageParams(SupplyChainConstants.ObjTypeShipmentSku, SupplyChainConstants.GetProcShipmentSku);
            return await this.GetProcedure2<ShipmentSkuModel>(recordObject, packageParameter, serviceDocumentResult, parameters);
        }

        public async Task<ShipmentDetailsModel> ShipmentDetailsGet(long shipmentId, ServiceDocumentResult serviceDocumentResult, string userEmail)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(SupplyChainConstants.RecordObjShipmentDetails, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["SHIPMENT"]] = shipmentId;
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();

            OracleParameter parameter = new OracleParameter("P_USER", OracleDbType.VarChar);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = userEmail;
            parameters.Add(parameter);

            PackageParams packageParameter = this.GetPackageParams(SupplyChainConstants.ObjTypeShipmentDetails, SupplyChainConstants.GetProcShipmentDetails);
            List<ShipmentDetailsModel> shipmentDetails = await this.GetProcedure2<ShipmentDetailsModel>(recordObject, packageParameter, serviceDocumentResult, parameters);
            return shipmentDetails != null ? shipmentDetails[0] : null;
        }

        public async Task<ServiceDocumentResult> ShipmentSkuSet(ShipmentSkuModel shipmentSkuModel)
        {
            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();
            PackageParams packageParameter = this.GetPackageParams(SupplyChainConstants.ObjTypeShipmentSku, SupplyChainConstants.SetProcShipmentSku);
            OracleParameter parameter;
            parameter = new OracleParameter("RESULT", OracleDbType.Boolean)
            {
                Direction = System.Data.ParameterDirection.ReturnValue
            };
            parameters.Add(parameter);
            parameter = new OracleParameter(packageParameter.ObjectTypeTable, OracleDbType.Table)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                ObjectTypeName = packageParameter.ObjectTypeName,
                Value = this.SetParamsShipmentSku(shipmentSkuModel)
            };
            parameters.Add(parameter);
            return await this.SetProcedure(null, packageParameter, parameters);
        }

        public async Task<ServiceDocumentResult> ShipmentDetailsSet(ShipmentDetailsModel shipmentDetailsModel)
        {
            this.ShipmentId = 0;
            PackageParams packageParameter = this.GetPackageParams(SupplyChainConstants.ObjTypeShipmentDetails, SupplyChainConstants.SetProcShipmentDetails);
            OracleObject recordObject = this.SetParamsShipmentDetails(shipmentDetailsModel);
            var result = await this.SetProcedure(recordObject, packageParameter);
            if (this.ObjResult != null)
            {
                this.ShipmentId = Convert.ToInt32(this.ObjResult["SHIPMENT"]);
            }

            return result;
        }

        public async Task<List<OrderShipmentResponseModel>> GetShipmentForOrder(OrderShipmentRequestModel ordShipModel, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(SupplyChainConstants.RecordObjOrderShipment, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["LOCATION_ID"]] = ordShipModel.LocationId;
            recordObject[recordType.Attributes["ORDER_NO"]] = ordShipModel.OrderNo;
            recordObject[recordType.Attributes["ETA"]] = ordShipModel.EstimatedArrivalDate;
            PackageParams packageParameter = this.GetPackageParams(SupplyChainConstants.ObjTypeOrderShipment, SupplyChainConstants.GetProcOrderShipment);
            return await this.GetProcedure2<OrderShipmentResponseModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<List<ShipmentHeadPrintModel>> GetShipmentPrintData(int shipmentId, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(SupplyChainConstants.ObjTypeShipmentPrint, SupplyChainConstants.GetProcShipmentPrint);
            OracleObject recordObject = this.SetParamsShipmentPrint(shipmentId);
            OracleTable pShipmentPrintMstTab = await this.GetProcedure(recordObject, packageParameter, serviceDocumentResult);

            List<ShipmentHeadPrintModel> lstHeader = Mapper.Map<List<ShipmentHeadPrintModel>>(pShipmentPrintMstTab);
            if (lstHeader != null)
            {
                for (int i = 0; i < lstHeader.Count; i++)
                {
                    OracleObject obj = (OracleObject)pShipmentPrintMstTab[i];
                    OracleTable shipmentSkuDetailsObj = (Devart.Data.Oracle.OracleTable)obj["SHIP_SKU"];
                    if (shipmentSkuDetailsObj.Count > 0)
                    {
                        lstHeader[i].ShipmentSkuDetails.AddRange(Mapper.Map<List<ShipmentSkuModel>>(shipmentSkuDetailsObj));
                    }
                }
            }

            return lstHeader;
        }

        public async Task<OrderHeadResponseModel> OrderHeadGet(OrderHeadRequestModel orderHeadRequestModel, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            OrderHeadResponseModel orderHeadResponseModel = new OrderHeadResponseModel();
            OracleType recordType = OracleType.GetObjectType(SupplyChainConstants.RecordObjOrderHead, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["ORDER_NO"]] = orderHeadRequestModel.OrderNo;
            recordObject[recordType.Attributes["LOCATION"]] = orderHeadRequestModel.LocationId;
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();

            PackageParams packageParameter = this.GetPackageParams(SupplyChainConstants.ObjTypeOrderHead, SupplyChainConstants.GetProcOrderHead);
            List<OrderHeadRcvngModel> orderHead = await this.GetProcedure2<OrderHeadRcvngModel>(recordObject, packageParameter, serviceDocumentResult, parameters);
            orderHeadResponseModel.DataModel = orderHead != null ? orderHead[0] : null;
            return orderHeadResponseModel;
        }

        public async Task<List<OrderListResponseModel>> OrderListGet(OrderListRequestModel orderListRequestModel, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(SupplyChainConstants.RecordObjOrderList, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["LOCATION"]] = orderListRequestModel.LocationId;
            recordObject[recordType.Attributes["SUPPLIER"]] = orderListRequestModel.SupplierId;
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();

            PackageParams packageParameter = this.GetPackageParams(SupplyChainConstants.ObjTypeOrderList, SupplyChainConstants.GetProcOrderList);
            return await this.GetProcedure2<OrderListResponseModel>(recordObject, packageParameter, serviceDocumentResult, parameters);
        }

        public async Task<OrderItemDetailsModel> OrderItemDetailsGet(OrderItemDetailsRequestModel orderItemDetailsRequestModel, ServiceDocumentResult serviceDocumentResult)
        {
            try
            {
                this.Connection.Open();
                OracleParameterCollection parameters;
                parameters = this.Parameters;
                parameters.Clear();
                OracleParameter parameter;
                parameter = new OracleParameter("RESULT", OracleDbType.Boolean, ParameterDirection.ReturnValue);
                parameters.Add(parameter);
                parameter = new OracleParameter("P_ITEM_BARCODE", OracleDbType.VarChar)
                {
                    Direction = System.Data.ParameterDirection.Input,
                    Value = orderItemDetailsRequestModel.ItemBarcode
                };
                parameters.Add(parameter);
                parameter = new OracleParameter("P_LOCATION", OracleDbType.VarChar)
                {
                    Direction = System.Data.ParameterDirection.Input,
                    Value = orderItemDetailsRequestModel.LocationId
                };
                parameters.Add(parameter);
                parameter = new OracleParameter("P_ORDER_NO", OracleDbType.Number)
                {
                    Direction = System.Data.ParameterDirection.Input,
                    Value = orderItemDetailsRequestModel.OrderNo
                };
                parameters.Add(parameter);
                parameter = new OracleParameter("P_SHIPMENT", OracleDbType.Number)
                {
                    Direction = System.Data.ParameterDirection.Input,
                    Value = orderItemDetailsRequestModel.ShipmentId
                };
                parameters.Add(parameter);
                parameter = new OracleParameter(SupplyChainConstants.OrderItemDetailsTbl, Devart.Data.Oracle.OracleDbType.Table)
                {
                    Direction = System.Data.ParameterDirection.Output,
                    ObjectTypeName = SupplyChainConstants.ObjTypeOrderItemDetails,
                };

                parameters.Add(parameter);
                PackageParams packageParameter = this.GetPackageParams(SupplyChainConstants.ObjTypeOrderItemDetails, SupplyChainConstants.GetProcOrderItemDetails);
                List<OrderItemDetailsModel> orderItemDetails = await this.GetProcedure2<OrderItemDetailsModel>(null, packageParameter, serviceDocumentResult, parameters);
                return orderItemDetails != null ? orderItemDetails[0] : null;
            }
            finally
            {
                this.Connection.Close();
            }
        }

        public async Task<ServiceDocumentResult> OrderItemDetailsSet(OrderItemDetailsModel detailModel)
        {
            PackageParams packageParameter = this.GetPackageParams(SupplyChainConstants.ObjTypeOrderItemDetails, SupplyChainConstants.SetProcOrderItemDetails);
            this.ShipmentId = 0;
            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();
            OracleParameter parameter;
            parameter = new OracleParameter("P_LOCATION", OracleDbType.VarChar)
            {
                Direction = System.Data.ParameterDirection.Input,
                Value = detailModel.LocationId
            };
            parameters.Add(parameter);
            parameter = new OracleParameter("P_SHIPMENT", OracleDbType.Number)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                Value = detailModel.ShipmentId
            };
            parameters.Add(parameter);
            OracleObject recordObject = this.SetParamsOrderItemDetails(detailModel);

            var result = await this.SetProcedure(recordObject, packageParameter, parameters);
            if (parameters["P_SHIPMENT"].Value != DBNull.Value)
            {
                this.ShipmentId = Convert.ToInt32(parameters["P_SHIPMENT"].Value);
            }

            return result;
        }

        private OracleObject SetParamsShipmentDetails(ShipmentDetailsModel shipmentDetailsModel)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(SupplyChainConstants.RecordObjShipmentDetails, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["SHIPMENT"]] = shipmentDetailsModel.Shipment;
            recordObject[recordType.Attributes["ORDER_NO"]] = shipmentDetailsModel.OrderNo;
            recordObject[recordType.Attributes["BOL_NO"]] = shipmentDetailsModel.BillOfLadingNo;
            recordObject[recordType.Attributes["ASN"]] = shipmentDetailsModel.AdvanceShipmentNotice;
            recordObject[recordType.Attributes["SHIP_DATE"]] = shipmentDetailsModel.ShipDate;
            recordObject[recordType.Attributes["RECEIVE_DATE"]] = shipmentDetailsModel.ReceiveDate;
            recordObject[recordType.Attributes["EST_ARR_DATE"]] = shipmentDetailsModel.EstArrDate;
            recordObject[recordType.Attributes["SHIP_ORIGIN"]] = shipmentDetailsModel.ShipOrigin;
            recordObject[recordType.Attributes["STATUS_CODE"]] = shipmentDetailsModel.StatusCode;
            recordObject[recordType.Attributes["INVC_MATCH_STATUS"]] = shipmentDetailsModel.InvcMatchStatus;
            recordObject[recordType.Attributes["INVC_MATCH_DATE"]] = shipmentDetailsModel.InvcMatchDate;
            recordObject[recordType.Attributes["TO_LOC"]] = shipmentDetailsModel.ToLoc;
            recordObject[recordType.Attributes["TO_LOC_TYPE"]] = shipmentDetailsModel.ToLocType;
            recordObject[recordType.Attributes["FROM_LOC"]] = shipmentDetailsModel.FromLoc;
            recordObject[recordType.Attributes["FROM_LOC_TYPE"]] = shipmentDetailsModel.FromLocType;
            recordObject[recordType.Attributes["COURIER"]] = shipmentDetailsModel.Courier;
            recordObject[recordType.Attributes["NO_BOXES"]] = shipmentDetailsModel.NoOfBoxes;
            recordObject[recordType.Attributes["EXT_REF_NO_IN"]] = shipmentDetailsModel.ExtRefNoIn;
            recordObject[recordType.Attributes["EXT_REF_NO_OUT"]] = shipmentDetailsModel.ExtRefNoOut;
            recordObject[recordType.Attributes["COMMENTS"]] = shipmentDetailsModel.Comments;
            recordObject[recordType.Attributes["PARENT_SHIPMENT"]] = shipmentDetailsModel.ParentShipment;
            recordObject[recordType.Attributes["MATCH_LOCATION"]] = shipmentDetailsModel.MatchLocation;
            recordObject[recordType.Attributes["OPERATION"]] = shipmentDetailsModel.Operation;
            recordObject[recordType.Attributes["LAST_UPDATED_BY"]] = shipmentDetailsModel.ModifiedBy;
            recordObject[recordType.Attributes["CREATED_BY"]] = this.GetCreatedBy(shipmentDetailsModel.CreatedBy);
            return recordObject;
        }

        private OracleTable SetParamsShipmentSku(ShipmentSkuModel shipmentSkuModel)
        {
            this.Connection.Open();
            OracleType shipSkuTableType = OracleType.GetObjectType(SupplyChainConstants.ObjTypeShipmentSku, this.Connection);
            OracleType recordType = OracleType.GetObjectType(SupplyChainConstants.RecordObjShipmentSku, this.Connection);
            OracleTable pShipSkuTab = new OracleTable(shipSkuTableType);
            foreach (ShipmentSkuModel shipSku in shipmentSkuModel.ListOfShipmentSkuModels)
            {
                OracleObject recordObject = new OracleObject(recordType);
                recordObject[recordType.Attributes["SHIPMENT"]] = shipSku.Shipment;
                recordObject[recordType.Attributes["SEQ_NO"]] = shipSku.SeqNo;
                recordObject[recordType.Attributes["ITEM"]] = shipSku.Item;
                recordObject[recordType.Attributes["DISTRO_NO"]] = shipSku.DistroNo;
                recordObject[recordType.Attributes["DISTRO_TYPE"]] = shipSku.DistroType;
                recordObject[recordType.Attributes["REF_ITEM"]] = shipSku.RefItem;
                recordObject[recordType.Attributes["CARTON"]] = shipSku.Carton;
                recordObject[recordType.Attributes["INV_STATUS"]] = shipSku.InvStatus;
                recordObject[recordType.Attributes["STATUS_CODE"]] = shipSku.StatusCode;
                recordObject[recordType.Attributes["QTY_RECEIVED"]] = shipSku.QtyReceived;
                recordObject[recordType.Attributes["UNIT_COST"]] = shipSku.UnitCost;
                recordObject[recordType.Attributes["UNIT_RETAIL"]] = shipSku.UnitRetail;
                recordObject[recordType.Attributes["QTY_EXPECTED"]] = shipSku.QtyExpected;
                recordObject[recordType.Attributes["MATCH_INVC_ID"]] = shipSku.MatchInvcId;
                recordObject[recordType.Attributes["ADJUST_TYPE"]] = shipSku.AdjustType;
                recordObject[recordType.Attributes["ACTUAL_RECEIVING_STORE"]] = shipSku.ActualReceivingStore;
                recordObject[recordType.Attributes["RECONCILE_USER_ID"]] = shipSku.ReconcileUserId;
                recordObject[recordType.Attributes["RECONCILE_DATE"]] = shipSku.ReconcileDate;
                recordObject[recordType.Attributes["TAMPERED_IND"]] = shipSku.TamperedInd;
                recordObject[recordType.Attributes["DISPOSITIONED_IND"]] = shipSku.DispositionedInd;
                recordObject[recordType.Attributes["QTY_MATCHED"]] = shipSku.QtyMatched;
                recordObject[recordType.Attributes["WEIGHT_RECEIVED"]] = shipSku.WeightReceived;
                recordObject[recordType.Attributes["WEIGHT_RECEIVED_UOM"]] = shipSku.WeightReceivedUom;
                recordObject[recordType.Attributes["WEIGHT_EXPECTED"]] = shipSku.WeightExpected;
                recordObject[recordType.Attributes["WEIGHT_EXPECTED_UOM"]] = shipSku.WeightExpectedUom;
                recordObject[recordType.Attributes["IS_EXPIRY"]] = shipSku.IsExpiry;
                recordObject[recordType.Attributes["EXPIRY_DATE"]] = shipSku.ExpiryDate;
                recordObject[recordType.Attributes["OPERATION"]] = shipSku.Operation;
                recordObject[recordType.Attributes["LAST_UPDATED_BY"]] = shipSku.ModifiedBy;
                recordObject[recordType.Attributes["CREATED_BY"]] = this.GetCreatedBy(shipSku.CreatedBy);
                pShipSkuTab.Add(recordObject);
            }

            return pShipSkuTab;
        }

        private OracleObject SetParamsShipmentPrint(int shipmentId)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(SupplyChainConstants.RecordObjShipmentPrint, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);

            recordObject[recordType.Attributes["SHIPMENT"]] = shipmentId;
            return recordObject;
        }

        private OracleObject SetParamsOrderItemDetails(OrderItemDetailsModel detailModel)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(SupplyChainConstants.RecordObjOrderItemDetails, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["ORDER_NO"]] = detailModel.OrderNo;
            recordObject[recordType.Attributes["SHIPMENT"]] = detailModel.ShipmentId;
            recordObject[recordType.Attributes["ITEM"]] = detailModel.Item;
            recordObject[recordType.Attributes["ITEM_DESC"]] = detailModel.ItemDescription;
            recordObject[recordType.Attributes["DEPT_ID"]] = detailModel.DepartmentId;
            recordObject[recordType.Attributes["DEPT_DESC"]] = detailModel.DepartmentDescription;
            recordObject[recordType.Attributes["QTY_ORDERED"]] = detailModel.QuantityOrdered;
            recordObject[recordType.Attributes["QTY_RECEIVED"]] = detailModel.QuantityReceived;
            recordObject[recordType.Attributes["UNIT_TYPE"]] = detailModel.UnitType;
            recordObject[recordType.Attributes["UNIT_COST"]] = detailModel.UnitCost;
            recordObject[recordType.Attributes["COST_CURRENCY"]] = detailModel.CostCurrency;
            recordObject[recordType.Attributes["UNIT_RETAIL"]] = detailModel.UnitRetail;
            recordObject[recordType.Attributes["RETAIL_CURRENCY"]] = detailModel.RetailCurrency;
            recordObject[recordType.Attributes["DEFAULT_UNIT"]] = detailModel.DefaultUnit;
            recordObject[recordType.Attributes["WEIGHT_EXPECTED"]] = detailModel.WeightExpected;
            recordObject[recordType.Attributes["WEIGHT_EXPECTED_UOM"]] = detailModel.WeightExpectedUom;
            recordObject[recordType.Attributes["IS_EXPIRY"]] = detailModel.IsExpiry;
            recordObject[recordType.Attributes["EXPIRY_DATE"]] = detailModel.ExpiryDate;
            recordObject[recordType.Attributes["OPERATION"]] = detailModel.Operation;
            return recordObject;
        }
    }
}