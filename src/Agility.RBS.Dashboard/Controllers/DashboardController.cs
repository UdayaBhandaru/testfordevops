﻿// <copyright file="DashboardController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Dashboard
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Dashboard.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    public class DashboardController : Controller
    {
        private readonly DashboardRepository dashboardRepository;
        private readonly ServiceDocument<DashboardModel> serviceDocument;
        private readonly ServiceDocumentResult serviceDocumentResult;

        public DashboardController(
            DashboardRepository dashboardRepository,
            ServiceDocument<DashboardModel> serviceDocument)
        {
            this.dashboardRepository = dashboardRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<DashboardModel>> GetDashBoardData()
        {
            await this.serviceDocument.ToListAsync(this.CallBackDashboardWidgetsGet);
            return this.serviceDocument;
        }

        public async ValueTask<int> PageWidgetSave([FromBody] List<PageWidgetModel> pageWidgetListObj)
        {
            return await this.dashboardRepository.SavePageWidgets(pageWidgetListObj);
        }

        private async Task<List<DashboardModel>> CallBackDashboardWidgetsGet()
        {
            List<DashboardModel> dashboardModel = new List<DashboardModel>();
            DashboardModel model = new DashboardModel
            {
                AllWidgets = await this.dashboardRepository.WidgetsGet(1),
                PageWidgets = await this.dashboardRepository.PageWidgetsGet(1)
            };

            dashboardModel.Add(model);
            this.serviceDocument.Result = this.serviceDocumentResult;
            return dashboardModel;
        }
    }
}
