﻿// <copyright file="DashboardRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Dashboard
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Common;
    using Agility.Framework.Core.Repositories;
    using Agility.RBS.Dashboard.Entities;
    using Agility.RBS.Dashboard.Models;
    using AutoMapper;
    using Microsoft.EntityFrameworkCore;

    public class DashboardRepository : BaseRepository<DashboardDbContext>
    {
        public DashboardRepository(DashboardDbContext dbContext, IDbStartup iDbStartup)
            : base(iDbStartup)
        {
            this.DbContext = dbContext;
        }

        public async Task<List<WidgetModel>> WidgetsGet(int dashboardId)
        {
            var query = from dashwid in this.DbContext.DashboardWidgets
                        join wid in this.DbContext.Widgets
                        .Include(w => w.Chart)
                        .Include(w => w.ChartOptions)
                        .Include(w => w.WidgetProperties)
                        .Include(w => w.SearchPanel)
                        .ThenInclude(w => w.SearchField)
                        .ThenInclude(w => w.FieldProperty)
                        on dashwid.WIDGETID equals wid.WIDGETID
                        where dashwid.DASHBOARDID == dashboardId
                        select Mapper.Map<WidgetModel>(wid);

            return await query.ToListAsync();
        }

        public async Task<List<PageWidgetModel>> PageWidgetsGet(int dashboardId)
        {
            var query = from pagewid in this.DbContext.DashboardPageWidgets
                        where pagewid.DASHBOARDID == dashboardId
                        orderby pagewid.DASHBOARDPAGEWIDGETID, pagewid.ROWNO, pagewid.COLNO
                        select Mapper.Map<DashboardPageWidgetEntity, PageWidgetModel>(pagewid);
            return await query.ToListAsync();
        }

        public async ValueTask<int> SavePageWidgets(List<PageWidgetModel> pageWidgetListObj)
        {
            foreach (PageWidgetModel pwObj in pageWidgetListObj)
            {
                var widgetOptions = Mapper.Map<WidgetOptionsModel, DashboardPageWidgetEntity>(pwObj.WidgetOptions);
                this.DbContext.DashboardPageWidgets.Update(Mapper.Map<PageWidgetModel, DashboardPageWidgetEntity>(pwObj, widgetOptions));
                await this.DbContext.SaveChangesAsync();
            }

            return 1;
        }
    }
}
