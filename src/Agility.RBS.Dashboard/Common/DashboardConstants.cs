﻿// <copyright file="DashboardConstants.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Dashboard.Common
{
    internal static class DashboardConstants
    {
        // Result
        public const string OracleParameterResult = "RESULT";

        // Dashboard Package
        public const string GetDashboardPackage = "DASHBOARD_PKG";

        public const string ObjTypeDashboard = "DASHBOARD_TAB";
        public const string GetProcDashboard = "GETDASHBOARD";
    }
}