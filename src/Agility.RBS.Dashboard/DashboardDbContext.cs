﻿// <copyright file="DashboardDbContext.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Dashboard
{
    using System.Linq;
    using Agility.Framework.Core.Common;
    using Agility.Framework.Core.Repositories;
    using Agility.RBS.Dashboard.Entities;
    using Microsoft.EntityFrameworkCore;

    public class DashboardDbContext : BaseDbContext<DashboardDbContext>
    {
        public DashboardDbContext(DbContextOptions<DashboardDbContext> options, IDbStartup iDbStartup)
              : base(options, iDbStartup)
        {
        }

        public DbSet<WidgetEntity> Widgets { get; set; }

        public DbSet<DashboardWidgetEntity> DashboardWidgets { get; set; }

        public DbSet<DashboardPageWidgetEntity> DashboardPageWidgets { get; set; }

        public DbSet<ChartTypeEntity> ChartTypes { get; set; }

        public DbSet<WidgetPropertyEntity> WidgetProperties { get; set; }

        public DbSet<ColorPaletteEntity> ColorPalette { get; set; }

        public DbSet<SearchPanelEntity> SearchPanels { get; set; }

        public DbSet<SearchFieldEntity> SearchFields { get; set; }

        public DbSet<FieldPropertyEntity> FieldProperties { get; set; }

        public DbSet<ChartOptionsEntity> ChartOptions { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // Method intentionally left empty.
            this.IDbStartup.OnConfiguring(optionsBuilder, CoreSettings.CoreConfiguration.ConnectionStrings.App.ConnectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<WidgetPropertyEntity>(entity =>
            {
                entity.HasKey(e => new { e.WIDGETID, e.PROPERTYNAME });
            });

            modelBuilder.Entity<ColorPaletteEntity>(entity =>
            {
                entity.HasKey(e => new { e.PALETTENAME, e.COLORKEY });
            });

            modelBuilder.Entity<WidgetEntity>()
                .HasMany(a => a.WidgetProperties)
                .WithOne();

            modelBuilder.Entity<SearchFieldEntity>(entity =>
            {
                entity.HasKey(e => new { e.SEARCHPANELID, e.FIELDID });
            });

            modelBuilder.Entity<FieldPropertyEntity>(entity =>
            {
                entity.HasKey(e => new { e.SEARCHPANELID, e.FIELDID, e.FIELDPROPERTY });
            });

            modelBuilder.Entity<SearchPanelEntity>()
                .HasMany(a => a.SearchField)
                .WithOne();

            modelBuilder.Entity<SearchFieldEntity>()
                .HasMany(a => a.FieldProperty)
                .WithOne();
        }
    }
}
