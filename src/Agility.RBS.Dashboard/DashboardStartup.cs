﻿// <copyright file="DashboardStartup.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Dashboard
{
    using Agility.Framework.Core;
    using Agility.RBS.Dashboard.Mapping;
    using AutoMapper;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;

    public class DashboardStartup : ComponentStartup<DashboardDbContext>
    {
        public void Startup(IConfigurationRoot configuration)
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<DashboardModelMapping>();
                this.InitilizeMapping(cfg);
            });
        }

        public override void ConfigureDependencyInjection(IServiceCollection services)
        {
            services.AddTransient<DashboardRepository>();
        }
    }
}
