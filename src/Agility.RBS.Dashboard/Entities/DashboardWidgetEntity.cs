﻿// <copyright file="DashboardWidgetEntity.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Dashboard.Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("DASHBOARDWIDGET")]
    public class DashboardWidgetEntity
    {
        [Key]
        [Column("DASHBOARDWIDGETID")]
        public int DASHBOARDPAGEWIDGETID { get; set; }

        public int DASHBOARDID { get; set; }

        public int WIDGETID { get; set; }
    }
}
