﻿// <copyright file="FieldPropertyEntity.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Dashboard.Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("FIELDPROPERTY")]
    public class FieldPropertyEntity
    {
        public string FIELDID { get; set; }

        public int SEARCHPANELID { get; set; }

        public string FIELDPROPERTY { get; set; }

        public string PROPERTYVALUE { get; set; }
    }
}
