﻿// <copyright file="DashboardEntity.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Dashboard.Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Agility.Framework.Core.Profile.Entities;

    [DataProfile(Name = "DASHBOARD")]
    public class DashboardEntity : ProfileEntity
    {
        [Key]
        public int DASHBOARDID { get; set; }

        public string DESCRIPTION { get; set; }

        public string DASHBOARDNAME { get; set; }

        public string DASHBOARDTYPE { get; set; }
    }
}
