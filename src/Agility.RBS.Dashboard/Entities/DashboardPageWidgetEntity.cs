﻿// <copyright file="DashboardPageWidgetEntity.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Dashboard.Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("DASHBOARDPAGEWIDGET")]
    public class DashboardPageWidgetEntity
    {
        [Key]
        public int DASHBOARDPAGEWIDGETID { get; set; }

        public int DASHBOARDID { get; set; }

        public int WIDGETID { get; set; }

        public int COLNO { get; set; }

        public int ROWNO { get; set; }

        public int SIZEX { get; set; }

        public int SIZEY { get; set; }

        public int COMPANYID { get; set; }

        public bool DELETEDIND { get; set; }
    }
}
