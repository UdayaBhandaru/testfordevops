﻿// <copyright file="ChartOptionsEntity.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Dashboard.Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("CHARTOPTION")]
    public class ChartOptionsEntity
    {
        [Key]
        public int WIDGETID { get; set; }

        public string CHARTOPTIONS { get; set; }
    }
}
