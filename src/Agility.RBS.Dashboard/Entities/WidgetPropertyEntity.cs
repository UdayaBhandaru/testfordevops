﻿// <copyright file="WidgetPropertyEntity.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Dashboard.Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("WIDGETPROPERTY")]
    public class WidgetPropertyEntity
    {
        [Key]
        public int WIDGETID { get; set; }

        public string PROPERTYNAME { get; set; }

        public string PROPERTYVALUE { get; set; }
    }
}
