﻿// <copyright file="ChartTypeEntity.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Dashboard.Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("CHARTTYPE")]
    public class ChartTypeEntity
    {
        [Key]
        public int CHARTTYPEID { get; set; }

        public string TYPE { get; set; }

        [NotMapped]
        public object Model { get; set; }
    }
}
