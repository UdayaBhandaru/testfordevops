﻿// <copyright file="ColorPaletteEntity.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Dashboard.Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("COLORPALETTE")]
    public class ColorPaletteEntity
    {
        [Key]
        public string PALETTENAME { get; set; }

        public string COLORKEY { get; set; }

        public string COLOR { get; set; }
    }
}
