﻿// <copyright file="SearchFieldEntity.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Dashboard.Entities
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("SEARCHFIELD")]
    public class SearchFieldEntity
    {
        [Key]
        public string FIELDID { get; set; }

        public int SEARCHPANELID { get; set; }

        public int ORDERID { get; set; }

        public List<FieldPropertyEntity> FieldProperty { get; private set; }

        [NotMapped]
        public object SearchFieldData { get; set; }

        [NotMapped]
        public object SearchFieldCascadeData { get; set; }
    }
}
