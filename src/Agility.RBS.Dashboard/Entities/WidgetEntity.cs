﻿// <copyright file="WidgetEntity.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Dashboard.Entities
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("WIDGET")]
    public class WidgetEntity
    {
        [Key]
        public int WIDGETID { get; set; }

        public string NAME { get; set; }

        public int SIZEX { get; set; }

        public int SIZEY { get; set; }

        public string XAXISLABEL { get; set; }

        public string YAXISLABEL { get; set; }

        public int CHARTTYPEID { get; set; }

        public int SEARCHPANELID { get; set; }

        public ChartTypeEntity Chart { get; set; }

        [ForeignKey("WIDGETID")]
        public List<WidgetPropertyEntity> WidgetProperties { get; private set; }

        public List<ColorPaletteEntity> ColorPalette { get; private set; }

        [ForeignKey("SEARCHPANELID")]
        public SearchPanelEntity SearchPanel { get; set; }

        [ForeignKey("WIDGETID")]
        public ChartOptionsEntity ChartOptions { get; set; }

        public int COLNO { get; set; }

        public int ROWNO { get; set; }

        public int DASHBOARDMODELID { get; set; }
    }
}
