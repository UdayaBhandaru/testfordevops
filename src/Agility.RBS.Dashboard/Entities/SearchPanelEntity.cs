﻿// <copyright file="SearchPanelEntity.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Dashboard.Entities
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("SEARCHPANEL")]
    public class SearchPanelEntity
    {
        [Key]
        public int SEARCHPANELID { get; set; }

        public bool VISIBLE { get; set; }

        [ForeignKey("SEARCHPANELID")]
        public List<SearchFieldEntity> SearchField { get; private set; }
    }
}
