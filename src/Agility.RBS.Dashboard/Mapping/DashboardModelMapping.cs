﻿// <copyright file="DashboardModelMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Dashboard.Mapping
{
    using Agility.RBS.Dashboard.Entities;
    using Agility.RBS.Dashboard.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class DashboardModelMapping : Profile
    {
        public DashboardModelMapping()
            : base("DashboardModelMapping")
        {
            this.CreateMap<ChartTypeEntity, ChartModel>();
            this.CreateMap<WidgetEntity, WidgetModel>()
                    .ForMember(dest => dest.WidgetId, opts => opts.MapFrom(src => src.WIDGETID))
                    .ForMember(dest => dest.Name, opts => opts.MapFrom(src => src.NAME))
                    .ForMember(dest => dest.SizeX, opts => opts.MapFrom(src => src.SIZEX))
                    .ForMember(dest => dest.SizeY, opts => opts.MapFrom(src => src.SIZEY))
                    .ForMember(dest => dest.Chart, opts => opts.MapFrom(src => Mapper.Map<ChartTypeEntity, ChartModel>(src.Chart))).ReverseMap();
            this.CreateMap<DashboardPageWidgetEntity, PageWidgetModel>(MemberList.Destination)
                    .ForMember(dest => dest.DashboardPageWidgetId, opts => opts.MapFrom(src => src.DASHBOARDPAGEWIDGETID))
                    .ForMember(dest => dest.WidgetId, opts => opts.MapFrom(src => src.WIDGETID))
                    .ForMember(dest => dest.WidgetOptions, opts => opts.MapFrom(src => src)).ReverseMap();
            this.CreateMap<DashboardPageWidgetEntity, WidgetOptionsModel>()
                    .ForMember(dest => dest.Col, opts => opts.MapFrom(src => src.COLNO))
                    .ForMember(dest => dest.Row, opts => opts.MapFrom(src => src.ROWNO))
                    .ForMember(dest => dest.Sizex, opts => opts.MapFrom(src => src.SIZEX))
                    .ForMember(dest => dest.Sizey, opts => opts.MapFrom(src => src.SIZEY)).ReverseMap();
            this.CreateMap<WidgetPropertyEntity, WidgetPropertyModel>().ReverseMap();
            this.CreateMap<ColorPaletteEntity, ColorPaletteModel>().ReverseMap();
            this.CreateMap<SearchPanelEntity, SearchPanelModel>();
            this.CreateMap<SearchFieldEntity, SearchFieldModel>();
            this.CreateMap<FieldPropertyEntity, FieldPropertyModel>();
            this.CreateMap<ChartOptionsEntity, ChartOptionsModel>();
            this.CreateMap<DashboardWidgetEntity, PageWidgetModel>().ReverseMap();
        }
    }
}
