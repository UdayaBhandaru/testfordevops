﻿// <copyright file="DashboardModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Dashboard.Models
{
    using System.Collections.Generic;
    using Agility.Framework.Core.Profile.Entities;

    [DataProfile(Name = "DASHBOARD")]
    public class DashboardModel : ProfileEntity
    {
        public DashboardModel()
        {
            this.AllWidgets = new List<WidgetModel>();
            this.PageWidgets = new List<PageWidgetModel>();
        }

        public List<WidgetModel> AllWidgets { get; internal set; }

        public List<PageWidgetModel> PageWidgets { get; internal set; }
    }
}
