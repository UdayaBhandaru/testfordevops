﻿// <copyright file="SearchFieldModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Dashboard.Models
{
    using System.Collections.Generic;

    public class SearchFieldModel
    {
        public string FieldId { get; set; }

        public int SearchPanelId { get; set; }

        public int OrderId { get; set; }

        public List<FieldPropertyModel> FieldProperty { get; private set; }

        public object SearchFieldData { get; set; }

        public object SearchFieldCascadeData { get; set; }
    }
}
