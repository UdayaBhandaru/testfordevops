﻿// <copyright file="DashboardWidgetModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Dashboard.Models
{
    using System.Collections.Generic;

    public class DashboardWidgetModel
    {
        public int DashboardId { get; set; }

        public List<WidgetModel> Widget { get; private set; }
    }
}
