﻿// <copyright file="WidgetOptionsModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Dashboard.Models
{
    public class WidgetOptionsModel
    {
        public int Col { get; set; }

        public int Row { get; set; }

        public int Sizex { get; set; }

        public int Sizey { get; set; }
    }
}
