﻿// <copyright file="DashboardDetailsModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Dashboard.Models
{
    using System.ComponentModel.DataAnnotations.Schema;

    public class DashboardDetailsModel
    {
        [Column("DASHBOARDID")]
        public int Dashboardid { get; set; }

        [Column("DESCRIPTION")]
        public string Description { get; set; }

        [Column("DASHBOARDNAME")]
        public string Dashboardname { get; set; }

        [Column("DASHBOARDTYPE")]
        public string Dashboardtype { get; set; }
    }
}
