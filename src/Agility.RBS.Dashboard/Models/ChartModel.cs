﻿// <copyright file="ChartModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Dashboard.Models
{
    public class ChartModel
    {
        public int ChartTypeId { get; set; }

        public string Type { get; set; }

        public object Model { get; set; }
    }
}
