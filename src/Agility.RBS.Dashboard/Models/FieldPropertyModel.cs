﻿// <copyright file="FieldPropertyModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Dashboard.Models
{
    public class FieldPropertyModel
    {
        public string FieldId { get; set; }

        public int SearchPanelId { get; set; }

        public string FieldProperty { get; set; }

        public string PropertyValue { get; set; }
    }
}
