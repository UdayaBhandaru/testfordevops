﻿// <copyright file="PageWidgetModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Dashboard.Models
{
    public class PageWidgetModel
    {
        public int DashboardPageWidgetId { get; set; }

        public int WidgetId { get; set; }

        public bool SkipRefresh { get; set; }

        public int DashboardId { get; set; }

        public WidgetOptionsModel WidgetOptions { get; set; }
    }
}
