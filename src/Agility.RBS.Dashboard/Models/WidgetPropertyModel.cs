﻿// <copyright file="WidgetPropertyModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Dashboard.Models
{
    public class WidgetPropertyModel
    {
        public int WidgetId { get; set; }

        public string PropertyName { get; set; }

        public string PropertyValue { get; set; }
    }
}
