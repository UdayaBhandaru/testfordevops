﻿// <copyright file="ColorPaletteModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Dashboard.Models
{
    public class ColorPaletteModel
    {
        public string PaletteName { get; set; }

        public string ColorKey { get; set; }

        public string Color { get; set; }
    }
}
