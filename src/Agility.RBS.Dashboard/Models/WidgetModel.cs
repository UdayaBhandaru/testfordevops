﻿// <copyright file="WidgetModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Dashboard.Models
{
    using System.Collections.Generic;

    public class WidgetModel
    {
        public int WidgetId { get; set; }

        public string Name { get; set; }

        public int SizeX { get; set; }

        public int SizeY { get; set; }

        public string XAxisLabel { get; set; }

        public string YAxisLabel { get; set; }

        public int ChartTypeId { get; set; }

        public ChartModel Chart { get; set; }

        public List<WidgetPropertyModel> WidgetProperties { get; private set; }

        public List<ColorPaletteModel> ColorPalette { get; private set; }

        public SearchPanelModel SearchPanel { get; set; }

        public ChartOptionsModel ChartOptions { get; set; }

        public int Col { get; set; }

        public int Row { get; set; }
    }
}
