﻿// <copyright file="SearchPanelModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Dashboard.Models
{
    using System.Collections.Generic;

    public class SearchPanelModel
    {
        public int SearchPanelId { get; set; }

        public bool Visible { get; set; }

        public List<SearchFieldModel> SearchField { get; private set; }
    }
}
