﻿// <copyright file="ChartOptionsModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Dashboard.Models
{
    public class ChartOptionsModel
    {
        public int WidgetId { get; set; }

        public string ChartOptions { get; set; }
    }
}
