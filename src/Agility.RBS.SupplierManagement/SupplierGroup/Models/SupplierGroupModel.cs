﻿// <copyright file="SupplierGroupModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.SupplierManagement.SupplierGroup.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class SupplierGroupModel : ProfileEntity
    {
        public int? Id { get; set; }

        public string Name { get; set; }

        public string Status { get; set; }

        public string StatusDesc { get; set; }

        public int LangId { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Error { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }
    }
}
