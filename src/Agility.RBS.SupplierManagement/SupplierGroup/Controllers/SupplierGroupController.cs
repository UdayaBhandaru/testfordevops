﻿// <copyright file="SupplierGroupController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.SupplierManagement.SupplierGroup
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.SupplierManagement.Common;
    using Agility.RBS.SupplierManagement.Repository;
    using Agility.RBS.SupplierManagement.SupplierGroup.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class SupplierGroupController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocument<SupplierGroupModel> serviceDocument;
        private readonly SupplierRepository supplierGroupRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;

        public SupplierGroupController(
            ServiceDocument<SupplierGroupModel> serviceDocument,
            SupplierRepository supplierGroupRepository,
            DomainDataRepository domainDataRepository)
        {
            this.supplierGroupRepository = supplierGroupRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<SupplierGroupModel>> List()
        {
            this.serviceDocument.DomainData.Add("status", this.domainDataRepository.DomainDetailData(SupplierConstants.DomainHdrStatus).Result);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<SupplierGroupModel>> New()
        {
            this.serviceDocument.DomainData.Add("status", this.domainDataRepository.DomainDetailData(SupplierConstants.DomainHdrStatus).Result);
            this.serviceDocument.LocalizationData.AddData("SupplierGroup");
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<SupplierGroupModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.SupplierGroupSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<SupplierGroupModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.SupplierGroupSave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingSupplierGroup(int id, string name)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "SupplierGroup";
            nvc["P_COL1"] = id.ToString();
            nvc["P_COL2"] = name;
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        private async Task<List<SupplierGroupModel>> SupplierGroupSearch()
        {
            try
            {
                var lstUomClass = await this.supplierGroupRepository.GetSupplierGroup(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstUomClass;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> SupplierGroupSave()
        {
            this.serviceDocument.Result = await this.supplierGroupRepository.SetSupplierGroup(this.serviceDocument.DataProfile.DataModel);
            return true;
        }
    }
}
