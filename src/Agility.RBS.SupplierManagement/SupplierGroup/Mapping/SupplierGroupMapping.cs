﻿// <copyright file="SupplierGroupMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.SupplierManagement.SupplierGroup.Mapping
{
    using System;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.CostZone.Models;
    using Agility.RBS.SupplierManagement.SupplierGroup.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class SupplierGroupMapping : Profile
    {
        public SupplierGroupMapping()
            : base("SupplierGroupMapping")
        {
            this.CreateMap<OracleObject, SupplierGroupModel>()
                .ForMember(m => m.Id, opt => opt.MapFrom(r => r["ID"]))
                .ForMember(m => m.Name, opt => opt.MapFrom(r => r["NAME"]))
                .ForMember(m => m.Status, opt => opt.MapFrom(r => r["STATUS"]))
                .ForMember(m => m.StatusDesc, opt => opt.MapFrom(r => r["STATUS_DESC"]))
                .ForMember(m => m.CreatedBy, opt => opt.MapFrom(r => r["CREATED_BY"]))
                .ForMember(m => m.CreatedDate, opt => opt.MapFrom(r => r["CREATED_DATE"]))
                .ForMember(m => m.ModifiedBy, opt => opt.MapFrom(r => r["LAST_UPDATED_BY"]))
                .ForMember(m => m.ModifiedDate, opt => opt.MapFrom(r => r["LAST_UPDATED_DATE"]))
                .ForMember(m => m.ProgramPhase, opt => opt.MapFrom(r => r["PROGRAM_PHASE"]))
                .ForMember(m => m.ProgramMessage, opt => opt.MapFrom(r => r["PROGRAM_MESSAGE"]))
                .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "SUPPLIERGROUP"))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));
        }
    }
}
