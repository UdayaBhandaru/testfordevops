﻿// <copyright file="SupplierConstants.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.SupplierManagement.Common
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal static class SupplierConstants
    {
        // Supplier
        public const string GetSupplierPackage = "UTL_SUPPLIER";
        public const string GetProcSupplier = "GETSUPPLIER";
        public const string SetProcSupplier = "SETSUPPLIER";
        public const string ObjTypeSupplier = "SUPPLIER_TAB";
        public const string RecObjSupplier = "SUPPLIER_REC";
        public const string GetProcSupplierSearch = "GETSUPPLIERSEARCH";
        public const string ObjTypeSupplierSearch = "SUPPLIER_SEARCH_TAB";
        public const string RecObjSupplierSearch = "SUPPLIER_SEARCH_REC";

        // Supplier Super
        public const string ObjTypeSupplierSuper = "SUPPLIER_SUPER_TAB";
        public const string RecordObjSupplierSuper = "SUPPLIER_SUPER_REC";
        public const string GetProcSupplierSuper = "GETSUPPLIERSUPER";

        // Supplier Address
        public const string GetProcSupplierAddress = "GETSUPPLIERADDRESS";
        public const string SetProcSupplierAddress = "SETSUPPLIERADDRESS";
        public const string ObjTypeSupplierAddress = "SUPPLIER_ADDRESS_TAB";
        public const string RecObjSupplierAddress = "SUPPLIER_ADDRESS_REC";

        // Progressive Rebates
        public const string ObjTypeRebateHead = "REBATE_HEAD_TAB";
        public const string RecObjRebateHead = "REBATE_HEAD_REC";
        public const string ObjTypeRebateDetail = "REBATE_DETAIL_TAB";
        public const string RecObjRebateDetail = "REBATE_DETAIL_REC";

        // SupplierGroup
        public const string GetProcSupplierGroup = "GETSUPPLIERGROUP";
        public const string SetProcSupplierGroup = "SETSUPPLIERGROUP";
        public const string ObjTypeSupplierGroup = "SUPPLIER_GROUP_TAB";
        public const string RecordTypeSupplierGroup = "SUPPLIER_GROUP_REC";

        // Domain Header Ids
        public const string DomainHdrStatus = "SPST";
        public const string DomainHdrIndicator = "YSNO";
        public const string DomainHdrPaymentMethod = "PAYM";
        public const string DomainHdrDelivery = "DLVY";
        public const string DomainHdrShipMethod = "SHPM";
        public const string DomainHdrAddressType = "ADDR_TYPE";
        public const string DomainHdrSupplierGroup = "SUPPLIERGROUP";
        public const string DomainHdrSupplierType = "SUPT";
        public const string DomainHdrSupplierTier = "SUPPTIER";
        public const string DomainHdrPayTermsType = "PAYTERMTYP";
    }
}
