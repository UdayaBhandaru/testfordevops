﻿// <copyright file="SupplierManagementStartup.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.SupplierManagement
{
    using System;
    using System.IO;
    using System.Reflection;
    using Agility.Framework.Core;
    using Agility.RBS.SupplierManagement.Repository;
    using Agility.RBS.SupplierManagement.Supplier;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Caching.Distributed;
    using Microsoft.Extensions.Caching.Memory;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using Swashbuckle.AspNetCore.Swagger;

    public class SupplierManagementStartup : ComponentStartup<DbContext>
    {
        public void Startup(Microsoft.Extensions.Configuration.IConfigurationRoot configuration)
        {
            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<Supplier.Mapping.SupplierModelMapping>();
                cfg.AddProfile<SupplierAddress.Mapping.SupplierAddressModelMapping>();
                cfg.AddProfile<SupplierGroup.Mapping.SupplierGroupMapping>();
                cfg.AddProfile<Supplier.Mapping.SupplierSearchMapping>();
                cfg.AddProfile<Supplier.Mapping.SupplierRebateMapping>();
                this.InitilizeMapping(cfg);
            });
        }

        public override void ConfigureServices(IServiceCollection services)
        {
            base.ConfigureServices(services);
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("SupplierV1", new Info { Title = "My API", Version = "v1" });
                var xmlFile = $"{this.GetType().GetTypeInfo().Assembly.GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }

        public override void Configure(
            IApplicationBuilder app,
            IHostingEnvironment env,
            ILoggerFactory loggerFactory,
            IDistributedCache distributedCache,
            IMemoryCache memoryCache,
            IServiceProvider serviceProvider)
        {
            base.Configure(app, env, loggerFactory, distributedCache, memoryCache, serviceProvider);
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("//swagger//SupplierV1//swagger.json", "My API v1");
                c.SupportedSubmitMethods();
            });
        }

        public override void ConfigureDependencyInjection(IServiceCollection services)
        {
            services.AddTransient<SupplierRepository>();
            services.AddTransient<SupplierController>();
            services.AddTransient<SupplierSearchController>();
        }
    }
}
