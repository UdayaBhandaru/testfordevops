﻿// <copyright file="SupplierSearchController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.SupplierManagement.Supplier
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.SupplierManagement.Common;
    using Agility.RBS.SupplierManagement.Repository;
    using Agility.RBS.SupplierManagement.Supplier.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [Route("api/[controller]/[action]")]
    public class SupplierSearchController : Controller
    {
        private readonly ServiceDocument<SupplierSearchModel> serviceDocument;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly SupplierRepository supplierRepository;
        private readonly DomainDataRepository domainDataRepository;

        public SupplierSearchController(ServiceDocument<SupplierSearchModel> serviceDocument, SupplierRepository supplierRepository, DomainDataRepository domainDataRepository)
        {
            this.supplierRepository = supplierRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in the Supplier List page.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<SupplierSearchModel>> List()
        {
            this.serviceDocument.DomainData.Add("status", this.domainDataRepository.DomainDetailData(SupplierConstants.DomainHdrStatus).Result);
            this.serviceDocument.DomainData.Add("supplierTypes", this.domainDataRepository.DomainDetailData(SupplierConstants.DomainHdrSupplierType).Result);
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for fetching Supplier records based on search criteria when Search button clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<SupplierSearchModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.GetListOfSuppliers);
            return this.serviceDocument;
        }

        private async Task<List<SupplierSearchModel>> GetListOfSuppliers()
        {
            try
            {
                List<SupplierSearchModel> listOfSupplierData = await this.supplierRepository.SupplierListGetWithSdoc(this.serviceDocument, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return listOfSupplierData;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }
    }
}