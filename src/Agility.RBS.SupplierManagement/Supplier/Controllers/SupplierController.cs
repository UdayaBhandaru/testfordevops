﻿// <copyright file="SupplierController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.SupplierManagement.Supplier
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Inbox.Common;
    using Agility.RBS.Inbox.Models;
    using Agility.RBS.Inbox.Repository;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Models;
    using Agility.RBS.SupplierManagement.Common;
    using Agility.RBS.SupplierManagement.Repository;
    using Agility.RBS.SupplierManagement.Supplier.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [Route("api/[controller]/[action]")]
    public class SupplierController : Controller
    {
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly SupplierRepository supplierRepository;
        private readonly DomainDataRepository domainDataRepository;
        private readonly InboxRepository inboxRepository;
        private readonly InboxModel inboxModel;
        private ServiceDocument<SupplierMasterModel> serviceDocument;
        private int supplierId;

        public SupplierController(
            ServiceDocument<SupplierMasterModel> serviceDocument,
            SupplierRepository supplierRepository,
            DomainDataRepository domainDataRepository,
            InboxRepository inboxRepository)
        {
            this.supplierRepository = supplierRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
            this.inboxRepository = inboxRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
            this.inboxModel = new InboxModel { Operation = "CHGS", ModuleName = "SUPPLIER" };
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in Supplier page when Add button clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<SupplierMasterModel>> New()
        {
            this.serviceDocument.New(false);
            return await this.GetDomainData();
        }

        /// <summary>
        /// This API invoked for populating fields in the Supplier page when Edit icon clicked.
        /// </summary>
        /// <param name="id">Supplier Id</param>
        /// <returns>ServiceDocument</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<SupplierMasterModel>> Open(int id)
        {
            this.supplierId = id;
            await this.serviceDocument.OpenAsync(this.CallBackOpen);
            return await this.GetDomainData();
        }

        /// <summary>
        /// This API invoked for checking whether supplier already exists or not.
        /// </summary>
        /// <param name="supplierId">Supplier Id</param>
        /// <returns>true or false</returns>
        [HttpGet]
        [AllowAnonymous]
        public async ValueTask<bool> IsExistingSupplier(int supplierId)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "SUPPLIER_MST";
            nvc["P_COL1"] = supplierId.ToString();
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        /// <summary>
        /// This API invoked for saving field values in the Supplier page when Save button clicked.
        /// </summary>
        /// <param name="serviceDocument">Service Document</param>
        /// <returns>ServiceDocument</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<SupplierMasterModel>> Save([FromBody]ServiceDocument<SupplierMasterModel> serviceDocument)
        {
            await this.serviceDocument.SaveAsync(this.SaveSupplier);
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for submitting supplier to next level when Submit button clicked.
        /// </summary>
        /// <param name="serviceDocument">Service Document</param>
        /// <returns>ServiceDocument</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<SupplierMasterModel>> Submit([FromBody]ServiceDocument<SupplierMasterModel> serviceDocument)
        {
            this.SetServiceDocumentAndInboxStateId(serviceDocument);
            await this.serviceDocument.TransitAsync(this.SaveSupplier);

            if (this.serviceDocument.Result.Type == MessageType.Success)
            {
                if (this.serviceDocument.DataProfile.DataModel.WorkflowInstance.StateName == "Worksheet")
                {
                    this.inboxModel.Operation = "DFT";
                }

                await this.inboxRepository.SetInbox(this.inboxModel, this.serviceDocument);
            }

            return this.serviceDocument;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<List<SupplierDomainModel>> SuppliersGetName(string name)
        {
            return await this.domainDataRepository.SupplierDomainGet(name);
        }

        /// <summary>
        /// This API invoked for supplier grid in Product Tab of Item Super Screen
        /// </summary>
        /// <param name="itemNo">Item Code</param>
        /// <returns>List oF SupplierSuperModel</returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<List<SupplierSuperModel>> GetSupplierSuperData(string itemNo)
        {
            return await this.supplierRepository.GetSupplierSuperData(itemNo, this.serviceDocumentResult);
        }

        private async Task<bool> SaveSupplier()
        {
            this.serviceDocument.Result = await this.supplierRepository.SetSupplier(this.serviceDocument.DataProfile.DataModel);
            return true;
        }

        private async Task<SupplierMasterModel> CallBackOpen()
        {
            try
            {
                var supplier = await this.supplierRepository.SupplierGet(this.supplierId, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return supplier;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<ServiceDocument<SupplierMasterModel>> GetDomainData()
        {
            this.serviceDocument.DomainData.Add("status", this.domainDataRepository.DomainDetailData(SupplierConstants.DomainHdrStatus).Result);
            this.serviceDocument.DomainData.Add("indicator", this.domainDataRepository.DomainDetailData(SupplierConstants.DomainHdrIndicator).Result);
            this.serviceDocument.DomainData.Add("VatRegion", this.domainDataRepository.VatRegionDomainGet().Result);
            this.serviceDocument.DomainData.Add("currency", this.domainDataRepository.CurrencyDomainGet().Result);
            ////this.serviceDocument.DomainData.Add("language", this.domainDataRepository.LanguageDomainGet().Result);
            this.serviceDocument.DomainData.Add("paymentMethods", this.domainDataRepository.DomainDetailData(SupplierConstants.DomainHdrPaymentMethod).Result);
            this.serviceDocument.DomainData.Add("terms", this.domainDataRepository.TermsDomainGet().Result);
            this.serviceDocument.DomainData.Add("freight", this.domainDataRepository.FreightDomainGet().Result);
            this.serviceDocument.DomainData.Add("deliveryPolicy", this.domainDataRepository.DomainDetailData(SupplierConstants.DomainHdrDelivery).Result);
            this.serviceDocument.DomainData.Add("shipmethod", this.domainDataRepository.DomainDetailData(SupplierConstants.DomainHdrShipMethod).Result);
            this.serviceDocument.DomainData.Add("country", this.domainDataRepository.CountryDomainGet().Result);
            ////this.serviceDocument.DomainData.Add("supplierGroups", this.domainDataRepository.CommonData(SupplierConstants.DomainHdrSupplierGroup).Result);
            this.serviceDocument.DomainData.Add("supplierTypes", this.domainDataRepository.DomainDetailData(SupplierConstants.DomainHdrSupplierType).Result);
            ////this.serviceDocument.DomainData.Add("supplierTiers", this.domainDataRepository.CommonData(SupplierConstants.DomainHdrSupplierTier).Result);
            ////this.serviceDocument.DomainData.Add("payTermsTypes", this.domainDataRepository.CommonData(SupplierConstants.DomainHdrPayTermsType).Result);
            this.serviceDocument.LocalizationData.AddData("SupplierMaster");
            return this.serviceDocument;
        }

        private void SetServiceDocumentAndInboxStateId(ServiceDocument<SupplierMasterModel> serviceDocument)
        {
            this.serviceDocument = serviceDocument;
            this.inboxModel.StateID = RbsServiceDocCustomizationHelper.GetStateIdDotNetToOracle(serviceDocument);
        }

        // commenting below statements as some fields are hidden as per new requirements
        ////this.serviceDocument.DomainData.Add("settlementCode", this.domainDataRepository.DomainDetailData(SupplierConstants.DomainHdrSettlementCode).Result);
        ////this.serviceDocument.DomainData.Add("dbtMemoCode", this.domainDataRepository.DomainDetailData(SupplierConstants.DomainHdrDbtMemoCode).Result);
        ////this.serviceDocument.DomainData.Add("outLocation", this.domainDataRepository.OutLocationDomainGet().Result);
        ////this.serviceDocument.DomainData.Add("ediSalesRptFrequency", this.domainDataRepository.DomainDetailData(SupplierConstants.DomainHdrEDISalesRptFrequency).Result);
        ////this.serviceDocument.DomainData.Add("inventoryMgmtLevel", this.domainDataRepository.DomainDetailData(SupplierConstants.DomainHdrInventoryMgmtLevel).Result);
        ////this.serviceDocument.DomainData.Add("vmiOrderStatus", this.domainDataRepository.DomainDetailData(SupplierConstants.DomainHdrVMIOrderStatus).Result);
        ////this.serviceDocument.DomainData.Add("rebatePeriod", this.domainDataRepository.DomainDetailData(SupplierConstants.DomainHdrRebatePeriod).Result);
        ////this.serviceDocument.DomainData.Add("addressType", this.domainDataRepository.DomainDetailData(SupplierConstants.DomainHdrAddressType).Result);
    }
}