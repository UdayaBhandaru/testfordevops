﻿// <copyright file="SupplierSearchMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.SupplierManagement.Supplier.Mapping
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.RBS.Core;
    using Agility.RBS.SupplierManagement.Supplier.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class SupplierSearchMapping : Profile
    {
        public SupplierSearchMapping()
            : base("SupplierSearchMapping")
        {
            this.CreateMap<OracleObject, SupplierSearchModel>()
                .ForMember(m => m.SupplierId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["SUPPLIER"])))
                .ForMember(m => m.SupplierName, opt => opt.MapFrom(r => r["SUP_NAME"]))
                .ForMember(m => m.ContactName1, opt => opt.MapFrom(r => r["CONTACT_NAME1"]))
                .ForMember(m => m.SupplierStatus, opt => opt.MapFrom(r => r["SUP_STATUS"]))
                .ForMember(m => m.SupplierStatusDesc, opt => opt.MapFrom(r => r["SUP_STATUS_DESC"]))
                .ForMember(m => m.CurrencyCode, opt => opt.MapFrom(r => r["CURRENCY_CODE"]))
                .ForMember(m => m.SupplierType, opt => opt.MapFrom(r => r["SUPPLIER_TYPE"]))
                .ForMember(m => m.SupplierTypeDesc, opt => opt.MapFrom(r => r["SUPPLIER_TYPE_DESC"]))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "SUPPLIER_MST"));
        }
    }
}
