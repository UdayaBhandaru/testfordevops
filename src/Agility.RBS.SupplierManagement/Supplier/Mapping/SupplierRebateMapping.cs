﻿// <copyright file="SupplierRebateMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.SupplierManagement.Supplier.Mapping
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.RBS.Core;
    using Agility.RBS.SupplierManagement.Supplier.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class SupplierRebateMapping : Profile
    {
        public SupplierRebateMapping()
            : base("SupplierRebateMapping")
        {
            this.CreateMap<OracleObject, SupplierRebateHeadModel>()
                .ForMember(m => m.RebateId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["REBATE_ID"])))
                .ForMember(m => m.SupplierId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["SUPPLIER_ID"])))
                .ForMember(m => m.RebateType, opt => opt.MapFrom(r => r["REBATE_TYPE"]))
                .ForMember(m => m.DefaultRebatePercent, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["DEFAULT_REBATE_PCT"])))
                .ForMember(m => m.RebateStatus, opt => opt.MapFrom(r => r["REBATE_STATUS"]))
                .ForMember(m => m.CreatedBy, opt => opt.MapFrom(r => r["CREATED_BY"]))
                .ForMember(m => m.CreatedDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["CREATED_DATE"])))
                .ForMember(m => m.ModifiedBy, opt => opt.MapFrom(r => r["LAST_UPDATED_BY"]))
                .ForMember(m => m.ModifiedDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["LAST_UPDATED_DATE"])))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, SupplierRebateDetailModel>()
                .ForMember(m => m.RebateId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["REBATE_ID"])))
                .ForMember(m => m.RebateDetailId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["REBATE_DETAIL_ID"])))
                .ForMember(m => m.GrowthPercent, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["GROWTH_PCT"])))
                .ForMember(m => m.MinThresholdLimit, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["MIN_THRESOLD_LIMIT"])))
                .ForMember(m => m.MaxThresholdLimit, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["MAX_THRESOLD_LIMIT"])))
                .ForMember(m => m.RebatePercent, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["REBATE_PCT"])))
                .ForMember(m => m.RebateStartDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["REBATE_START_DATE"])))
                .ForMember(m => m.RebateEndDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["REBATE_END_DATE"])))
                .ForMember(m => m.RebateDetailStatus, opt => opt.MapFrom(r => r["REBATE_DTL_STATUS"]))
                .ForMember(m => m.CreatedBy, opt => opt.MapFrom(r => r["CREATED_BY"]))
                .ForMember(m => m.CreatedDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["CREATED_DATE"])))
                .ForMember(m => m.ModifiedBy, opt => opt.MapFrom(r => r["LAST_UPDATED_BY"]))
                .ForMember(m => m.ModifiedDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["LAST_UPDATED_DATE"])))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));
        }
    }
}
