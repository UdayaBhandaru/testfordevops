﻿// <copyright file="SupplierRebateHeadModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.SupplierManagement.Supplier.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class SupplierRebateHeadModel
    {
        public SupplierRebateHeadModel()
        {
            this.RebateDetails = new List<SupplierRebateDetailModel>();
        }

        public int? RebateId { get; set; }

        public int? SupplierId { get; set; }

        public string RebateType { get; set; }

        public decimal? DefaultRebatePercent { get; set; }

        public string RebateStatus { get; set; }

        public string Operation { get; set; }

        public string CreatedBy { get; set; }

        public string ModifiedBy { get; set; }

        public DateTimeOffset? CreatedDate { get; set; }

        public DateTimeOffset? ModifiedDate { get; set; }

        public List<SupplierRebateDetailModel> RebateDetails { get; private set; }
    }
}
