﻿// <copyright file="SupplierSearchModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.SupplierManagement.Supplier.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Core.Models;

    public class SupplierSearchModel : ProfileEntity, Core.IPaginationModel
    {
        public int? SupplierId { get; set; }

        public string SupplierName { get; set; }

        public string ContactName1 { get; set; }

        public string SupplierStatus { get; set; }

        public string SupplierStatusDesc { get; set; }

        public string CurrencyCode { get; set; }

        public int? LanguageId { get; set; }

        public string LanguageDesc { get; set; }

        public string SupplierType { get; set; }

        public string SupplierTypeDesc { get; set; }

        public string RowId { get; set; }

        public int? StartRow { get; set; }

        public int? EndRow { get; set; }

        public long? TotalRows { get; set; }

        public SortModel[] SortModel { get; set; }

        public string TableName { get; set; }
    }
}
