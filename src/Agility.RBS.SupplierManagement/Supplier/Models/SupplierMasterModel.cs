﻿// <copyright file="SupplierMasterModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.SupplierManagement.Supplier.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Core.Models;
    using Agility.RBS.Inbox.Common;
    using Agility.RBS.SupplierManagement.SupplierAddress.Models;

    [DataProfile(Name = "SUPS")]
    public class SupplierMasterModel : ProfileEntity<SupplierWfModel>, IInboxCommonModel
    {
        public SupplierMasterModel()
        {
            this.ListOfAddress = new List<SupplierAddressMasterModel>();
        }

        [Column("SUPPLIER")]
        public int? SUPPLIER { get; set; }

        [NotMapped]
        public string SupplierName { get; set; }

        [NotMapped]
        public string SupplierSecondaryName { get; set; }

        [NotMapped]
        public string SupplierStatus { get; set; }

        [NotMapped]
        public string SupplierStatusDesc { get; set; }

        [NotMapped]
        public int? LanguageId { get; set; }

        [NotMapped]
        public string LanguageDesc { get; set; }

        [NotMapped]
        public string ContactName1 { get; set; }

        [NotMapped]
        public string ContactPhone { get; set; }

        [NotMapped]
        public string ContactFax { get; set; }

        [NotMapped]
        public string ContactPager { get; set; }

        [NotMapped]
        public string ContactEmail1 { get; set; }

        [NotMapped]
        public string QcIndicator { get; set; }

        [NotMapped]
        public decimal? QcPercentage { get; set; }

        [NotMapped]
        public int? QcFrequency { get; set; }

        [NotMapped]
        public string VcIndicator { get; set; }

        [NotMapped]
        public decimal? VcPercentage { get; set; }

        [NotMapped]
        public int? VcFrequency { get; set; }

        [NotMapped]
        public string CurrencyCode { get; set; }

        [NotMapped]
        public string VatExempt { get; set; }

        [NotMapped]
        public string VatRegistrationId { get; set; }

        [NotMapped]
        public string VatRegion { get; set; }

        [NotMapped]
        public string PaymentMode { get; set; }

        [NotMapped]
        public string AutoApproveInvoiceIndicator { get; set; }

        [NotMapped]
        public decimal? CostChgPercentageVar { get; set; }

        [NotMapped]
        public decimal? CostChgAmountVar { get; set; }

        [NotMapped]
        public string AllowRebate { get; set; }

        [NotMapped]
        public string RebatePeriod { get; set; }

        [NotMapped]
        public decimal? RebateDiscount { get; set; }

        [NotMapped]
        public string DisplayDiscount { get; set; }

        [NotMapped]
        public string DbtMemoCode { get; set; }

        [NotMapped]
        public string SettlementCode { get; set; }

        [NotMapped]
        public string BracketCostingIndicator { get; set; }

        [NotMapped]
        public string AutoApproveDbtMemoIndicator { get; set; }

        [NotMapped]
        public string InvoicePayLocation { get; set; }

        [NotMapped]
        public string InvoiceRecieveLocation { get; set; }

        [NotMapped]
        public string AddInvoiceGrossNet { get; set; }

        [NotMapped]
        public string PrePayInvoiceIndicator { get; set; }

        [NotMapped]
        public string BackOrderIndicator { get; set; }

        [NotMapped]
        public string AllowOverDue { get; set; }

        [NotMapped]
        public string EdiPoIndicator { get; set; }

        [NotMapped]
        public string EdiPoChange { get; set; }

        [NotMapped]
        public string EdiPoConfirm { get; set; }

        [NotMapped]
        public string EdiAsn { get; set; }

        [NotMapped]
        public string EdiSalesRepeatFrequency { get; set; }

        [NotMapped]
        public string EdiSupportAvailableIndicator { get; set; }

        [NotMapped]
        public string EdiContractIndicator { get; set; }

        [NotMapped]
        public string EdiInvoiceIndicator { get; set; }

        [NotMapped]
        public string FreightChargeIndicator { get; set; }

        [NotMapped]
        public string PaymentTerms { get; set; }

        [NotMapped]
        public string FreightTerms { get; set; }

        [NotMapped]
        public string OfTransferFlag { get; set; }

        [NotMapped]
        public string OfEnableTransferFlag { get; set; }

        [NotMapped]
        public string ServicePerfReqIndicator { get; set; }

        [NotMapped]
        public string ShipMethod { get; set; }

        [NotMapped]
        public string FinalDstIndicator { get; set; }

        [NotMapped]
        public int? DefaultItemLeadTime { get; set; }

        [NotMapped]
        public string DeliveryPolicy { get; set; }

        [NotMapped]
        public string DunsNumber { get; set; }

        [NotMapped]
        public string DunsLocation { get; set; }

        [NotMapped]
        public string DsdIndicator { get; set; }

        [NotMapped]
        public string InvManagementLevel { get; set; }

        [NotMapped]
        public string RetAllowIndicator { get; set; }

        [NotMapped]
        public string RetAuthorizationRequired { get; set; }

        [NotMapped]
        public decimal? RetMinimumDolAmount { get; set; }

        [NotMapped]
        public string RetailCourier { get; set; }

        [NotMapped]
        public string ReplenApprovalIndicator { get; set; }

        [NotMapped]
        public decimal? HandlingPercentage { get; set; }

        [NotMapped]
        public string PreMarkIndicator { get; set; }

        [NotMapped]
        public string VmiOrderStatus { get; set; }

        [NotMapped]
        public string CommentDescription { get; set; }

        [NotMapped]
        public string Operation { get; set; }

        [NotMapped]
        public string TableName { get; set; }

        [NotMapped]
        public string AddressType { get; set; }

        [NotMapped]
        public string AddressTypeDesc { get; set; }

        [NotMapped]
        public int? SequenceNo { get; set; }

        [NotMapped]
        public string PrimaryAddressIndicator { get; set; }

        [NotMapped]
        public string PrimaryAddressIndicatorDesc { get; set; }

        [NotMapped]
        public string AddressLine1 { get; set; }

        [NotMapped]
        public string AddressLine2 { get; set; }

        [NotMapped]
        public string AddressLine3 { get; set; }

        [NotMapped]
        public string City { get; set; }

        [NotMapped]
        public string State { get; set; }

        [NotMapped]
        public string CountryID { get; set; }

        [NotMapped]
        public string PostalCode { get; set; }

        [NotMapped]
        public string SupplierGroup { get; set; }

        [NotMapped]
        public string SupplierTier { get; set; }

        [NotMapped]
        public string ContactName2 { get; set; }

        [NotMapped]
        public string ContactTitle1 { get; set; }

        [NotMapped]
        public string ContactTitle2 { get; set; }

        [NotMapped]
        public string ContactEmail2 { get; set; }

        [NotMapped]
        public List<SupplierAddressMasterModel> ListOfAddress { get; private set; }

        [NotMapped]
        public string Others { get; set; }

        [NotMapped]
        public string IsExpiry { get; set; }

        [NotMapped]
        public string MinServiceLevel { get; set; }

        [NotMapped]
        public string SupplierRetailFlag { get; set; }

        [NotMapped]
        public string SupplierType { get; set; }

        [NotMapped]
        public string AccountNumberType { get; set; }

        [NotMapped]
        public string BankName { get; set; }

        [NotMapped]
        public string BankAddress { get; set; }

        [NotMapped]
        public string BankDetails { get; set; }

        [NotMapped]
        public decimal? NewStoreOpeningFee { get; set; }

        [NotMapped]
        public decimal? ExpectedAnnualVol { get; set; }

        [NotMapped]
        public decimal? AccountOpeningFee { get; set; }

        [NotMapped]
        public decimal? ListingFee { get; set; }

        [NotMapped]
        public string PaymentTermType { get; set; }

        [NotMapped]
        public decimal? BarCodePrintCharge { get; set; }

        [NotMapped]
        public string BarCodePrintingFlag { get; set; }

        [NotMapped]
        public string CostHideInvoice { get; set; }

        [NotMapped]
        public string DiscountHideInvoice { get; set; }

        [NotMapped]
        public string DcIndicator { get; set; }

        // below fields are required for formControls
        [NotMapped]
        public string Address1AddressType { get; set; }

        [NotMapped]
        public string Address1AddressLine1 { get; set; }

        [NotMapped]
        public string Address1City { get; set; }

        [NotMapped]
        public string Address1State { get; set; }

        [NotMapped]
        public string Address1CountryID { get; set; }

        [NotMapped]
        public string Address1PostalCode { get; set; }

        [NotMapped]
        public string Address2AddressType { get; set; }

        [NotMapped]
        public string Address2AddressLine1 { get; set; }

        [NotMapped]
        public string Address2City { get; set; }

        [NotMapped]
        public string Address2State { get; set; }

        [NotMapped]
        public string Address2CountryID { get; set; }

        [NotMapped]
        public string Address2PostalCode { get; set; }

        [NotMapped]
        public SupplierRebateHeadModel RebateHead { get; set; }

        // below fields are required for formControls
        [NotMapped]
        public decimal? Slab1GrowthMinThreshold { get; set; }

        [NotMapped]
        public decimal? Slab1GrowthMaxThreshold { get; set; }

        [NotMapped]
        public decimal? Slab1Rebate { get; set; }

        [NotMapped]
        public decimal? Slab2GrowthMinThreshold { get; set; }

        [NotMapped]
        public decimal? Slab2GrowthMaxThreshold { get; set; }

        [NotMapped]
        public decimal? Slab2Rebate { get; set; }

        [NotMapped]
        public decimal? Slab3GrowthMinThreshold { get; set; }

        [NotMapped]
        public decimal? Slab3GrowthMaxThreshold { get; set; }

        [NotMapped]
        public decimal? Slab3Rebate { get; set; }

        [NotMapped]
        public decimal? Slab4GrowthMinThreshold { get; set; }

        [NotMapped]
        public decimal? Slab4GrowthMaxThreshold { get; set; }

        [NotMapped]
        public decimal? Slab4Rebate { get; set; }

        [NotMapped]
        public decimal? Slab5GrowthMinThreshold { get; set; }

        [NotMapped]
        public decimal? Slab5GrowthMaxThreshold { get; set; }

        [NotMapped]
        public decimal? Slab5Rebate { get; set; }

        [NotMapped]
        [Column("ORGANISATION_ID")]
        public int? OrganizationId { get; set; }

        [NotMapped]
        [Column("CREATED_BY")]
        public string CreatedBy { get; set; }

        [NotMapped]
        [Column("LAST_UPDATED_BY")]
        public string ModifiedBy { get; set; }

        [NotMapped]
        [Column("DeletedInd")]
        public bool DeletedInd { get; set; }

        [NotMapped]
        [Column("CREATED_DATE")]
        public DateTimeOffset? CreatedDate { get; set; }

        [NotMapped]
        [Column("LAST_UPDATED_DATE")]
        public DateTimeOffset? ModifiedDate { get; set; }

        [NotMapped]
        public Core.Models.RbWfHeaderModel WorkflowForm { get; set; }

        [NotMapped]
        public string SupplierWfStatus { get; set; }
    }
}
