﻿// <copyright file="SupplierRebateDetailModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.SupplierManagement.Supplier.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class SupplierRebateDetailModel
    {
        public int? RebateId { get; set; }

        public int? RebateDetailId { get; set; }

        public decimal? GrowthPercent { get; set; }

        public decimal? MinThresholdLimit { get; set; }

        public decimal? MaxThresholdLimit { get; set; }

        public decimal? RebatePercent { get; set; }

        public DateTime? RebateStartDate { get; set; }

        public DateTime? RebateEndDate { get; set; }

        public string RebateDetailStatus { get; set; }

        public string Operation { get; set; }

        public string CreatedBy { get; set; }

        public string ModifiedBy { get; set; }

        public DateTimeOffset? CreatedDate { get; set; }

        public DateTimeOffset? ModifiedDate { get; set; }
    }
}
