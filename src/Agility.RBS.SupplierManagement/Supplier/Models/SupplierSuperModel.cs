﻿// <copyright file="SupplierSuperModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.SupplierManagement.Supplier.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.RBS.SupplierManagement.SupplierAddress.Models;

    public class SupplierSuperModel
    {
        public int? Supplier { get; set; }

        public string SupplierName { get; set; }

        public string SupplierType { get; set; }

        public string SupplierTypeName { get; set; }

        public decimal? FixedRebatePercent { get; set; }

        public string AutoReplenishment { get; set; }

        public string PrimarySupplier { get; set; }

        public decimal? OrderMultiple { get; set; }

        public string FullAddress { get; set; }

        public string SupplierDescription
        {
            get
            {
                return this.SupplierName + "(" + this.Supplier + ")";
            }
        }
    }
}
