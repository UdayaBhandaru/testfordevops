﻿// <copyright file="SupplierRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.SupplierManagement.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Constants;
    using Agility.RBS.SupplierManagement.Common;
    using Agility.RBS.SupplierManagement.Supplier.Models;
    using Agility.RBS.SupplierManagement.SupplierAddress.Models;
    using Agility.RBS.SupplierManagement.SupplierGroup.Models;
    using AutoMapper;
    using Devart.Data.Oracle;
    using Microsoft.Extensions.Caching.Distributed;

    public class SupplierRepository : BaseOraclePackage
    {
        public SupplierRepository()
        {
            this.PackageName = SupplierConstants.GetSupplierPackage;
        }

        public async Task<ServiceDocumentResult> SetSupplier(SupplierMasterModel supplierModel)
        {
            this.PackageName = SupplierConstants.GetSupplierPackage;
            PackageParams packageParameter = this.GetPackageParams(SupplierConstants.ObjTypeSupplier, SupplierConstants.SetProcSupplier);
            OracleObject recordObject = this.SetParamsSupplier(supplierModel);
            ServiceDocumentResult sRes = await this.SetProcedure(recordObject, packageParameter);
            return sRes;
        }

        public async Task<List<SupplierAddressMasterModel>> GetListOfSupplierAddress(SupplierAddressMasterModel supplierAddressModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(SupplierConstants.ObjTypeSupplierAddress, SupplierConstants.GetProcSupplierAddress);
            OracleObject recordObject = this.SetParamsSupplierAddress(supplierAddressModel, true);
            return await this.GetProcedure2<SupplierAddressMasterModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ServiceDocumentResult> SetSupplierAddress(SupplierAddressMasterModel supplierAddressModel)
        {
            this.PackageName = SupplierConstants.GetSupplierPackage;
            PackageParams packageParameter = this.GetPackageParams(SupplierConstants.ObjTypeSupplierAddress, SupplierConstants.SetProcSupplierAddress);
            OracleObject recordObject = this.SetParamsSupplierAddress(supplierAddressModel, false);
            ServiceDocumentResult sRes = await this.SetProcedure(recordObject, packageParameter);
            return sRes;
        }

        public async Task<List<SupplierSearchModel>> SupplierListGetWithSdoc(ServiceDocument<SupplierSearchModel> serviceDoc, ServiceDocumentResult serviceDocumentResult)
        {
            SupplierSearchModel supplierModel = serviceDoc.DataProfile.DataModel;
            PackageParams packageParameter = this.GetPackageParams(SupplierConstants.ObjTypeSupplierSearch, SupplierConstants.GetProcSupplierSearch);
            OracleObject recordObject = this.SetParamsSupplierList(supplierModel);
            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();
            OracleParameter parameter;
            parameter = new OracleParameter("P_START", Devart.Data.Oracle.OracleDbType.Number) { Direction = System.Data.ParameterDirection.Input, Value = supplierModel.StartRow };
            parameters.Add(parameter);
            parameter = new OracleParameter("P_END", OracleDbType.Number) { Direction = System.Data.ParameterDirection.Input, Value = supplierModel.EndRow };
            parameters.Add(parameter);
            parameter = new OracleParameter("P_TOTAL", OracleDbType.Number) { Direction = System.Data.ParameterDirection.Output };
            parameters.Add(parameter);
            if (supplierModel.SortModel == null)
            {
                supplierModel.SortModel = new[] { new Core.Models.SortModel { ColId = null, Sort = null } };
            }

            parameter = new OracleParameter("P_SORTCOLID", OracleDbType.VarChar) { Direction = ParameterDirection.Input, Value = supplierModel.SortModel[0].ColId };
            parameters.Add(parameter);
            parameter = new OracleParameter("P_SORTORDER", OracleDbType.VarChar) { Direction = ParameterDirection.Input, Value = supplierModel.SortModel[0].Sort };
            parameters.Add(parameter);
            var suppliers = await this.GetProcedure2<SupplierSearchModel>(recordObject, packageParameter, serviceDocumentResult, parameters);
            supplierModel.TotalRows = (this.Parameters["p_total"].Value != null && this.Parameters["p_total"].Value != DBNull.Value) ? Convert.ToInt64(this.Parameters["p_total"].Value) : 1;
            serviceDoc.DataProfile.DataModel = supplierModel;
            return suppliers;
        }

        public async Task<SupplierMasterModel> SupplierGet(long supplierId, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(SupplierConstants.RecObjSupplier, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["SUPPLIER"]] = supplierId;
            PackageParams packageParameter = this.GetPackageParams(SupplierConstants.ObjTypeSupplier, SupplierConstants.GetProcSupplier);
            OracleTable pSupplierTab = await this.GetProcedure(recordObject, packageParameter, serviceDocumentResult);
            List<SupplierMasterModel> lstHeader = Mapper.Map<List<SupplierMasterModel>>(pSupplierTab);
            if (lstHeader != null)
            {
                OracleObject obj = (OracleObject)pSupplierTab[0];
                OracleTable supplierAddressObj = (Devart.Data.Oracle.OracleTable)obj["SUPPLIER_ADDR_TAB"];
                if (supplierAddressObj.Count > 0)
                {
                    lstHeader[0].ListOfAddress.AddRange(Mapper.Map<List<SupplierAddressMasterModel>>(supplierAddressObj));
                }

                lstHeader[0].RebateHead = FillRebateModel(obj);

                return lstHeader[0];
            }

            return null;
        }

        public async Task<List<SupplierSuperModel>> GetSupplierSuperData(string itemId, ServiceDocumentResult serviceDocumentResult)
        {
            try
            {
                this.Connection.Open();
                OracleParameterCollection parameters;
                parameters = this.Parameters;
                parameters.Clear();
                OracleParameter parameter;
                parameter = new OracleParameter("P_ITEM", Devart.Data.Oracle.OracleDbType.Number)
                {
                    Direction = System.Data.ParameterDirection.Input,
                    Value = itemId
                };
                parameters.Add(parameter);
                parameter = new OracleParameter(SupplierConstants.ObjTypeSupplierSuper, OracleDbType.Table, ParameterDirection.ReturnValue);
                parameter.ObjectTypeName = SupplierConstants.ObjTypeSupplierSuper;
                parameters.Add(parameter);
                OracleTable pSupplierSuperTab = await this.ExecuteSupplierSuper(parameters);

                List<SupplierSuperModel> lstHeader = Mapper.Map<List<SupplierSuperModel>>(pSupplierSuperTab);
                if (lstHeader != null)
                {
                    for (int i = 0; i < lstHeader.Count; i++)
                    {
                        OracleObject obj = (OracleObject)pSupplierSuperTab[i];
                        OracleTable supplAddrDetails = (Devart.Data.Oracle.OracleTable)obj["SUPPLIER_ADDR_TAB"];
                        lstHeader[i].FullAddress = FillSupplierSuperAddress(Mapper.Map<List<SupplierAddressMasterModel>>(supplAddrDetails));
                    }
                }

                return lstHeader;
            }
            finally
            {
                this.Connection.Close();
            }
        }

        public async Task<List<SupplierGroupModel>> GetSupplierGroup(SupplierGroupModel supplierGroupModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(SupplierConstants.ObjTypeSupplierGroup, SupplierConstants.GetProcSupplierGroup);
            OracleObject recordObject = this.SetParamsSupplierGroup(supplierGroupModel, true);
            return await this.GetProcedure2<SupplierGroupModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ServiceDocumentResult> SetSupplierGroup(SupplierGroupModel supplierGroupModel)
        {
            PackageParams packageParameter = this.GetPackageParams(SupplierConstants.ObjTypeSupplierGroup, SupplierConstants.SetProcSupplierGroup);
            OracleObject recordObject = this.SetParamsSupplierGroup(supplierGroupModel, false);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        private static SupplierRebateHeadModel FillRebateModel(OracleObject obj)
        {
            OracleTable pRebateHeadTab = (Devart.Data.Oracle.OracleTable)obj["REBATE_HDR_TAB"];
            List<SupplierRebateHeadModel> srhm = Mapper.Map<List<SupplierRebateHeadModel>>(pRebateHeadTab);
            if (pRebateHeadTab != null && pRebateHeadTab.Count > 0)
            {
                OracleObject rebateHdObj = (OracleObject)pRebateHeadTab[0];
                OracleTable rebateDetailObj = (Devart.Data.Oracle.OracleTable)rebateHdObj["REBATE_DTL_TAB"];
                if (rebateDetailObj.Count > 0)
                {
                    srhm[0].RebateDetails.AddRange(Mapper.Map<List<SupplierRebateDetailModel>>(rebateDetailObj));
                }

                return srhm[0];
            }

            return null;
        }

        private static string FillSupplierSuperAddress(List<SupplierAddressMasterModel> addressList)
        {
            string fullAddress = string.Empty;
            SupplierAddressMasterModel addressObj = addressList.Find(x => x.PrimaryAddressIndicator == "Y");
            if (addressObj != null)
            {
                fullAddress = string.Format("{0},{1},{2},{3}", addressObj.AddressLine1, addressObj.City, addressObj.State, addressObj.CountryID);
            }

            return fullAddress;
        }

        private async Task<OracleTable> ExecuteSupplierSuper(OracleParameterCollection parameters)
        {
            await Task.Run(() => this.ExecuteProcedure(SupplierConstants.GetProcSupplierSuper, parameters));
            return this.Parameters[SupplierConstants.ObjTypeSupplierSuper].Value != DBNull.Value
                ? (Devart.Data.Oracle.OracleTable)this.Parameters[SupplierConstants.ObjTypeSupplierSuper].Value
                : null;
        }

        private OracleObject SetParamsSupplier(SupplierMasterModel supplierModel)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType("SUPPLIER_REC", this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["SUPPLIER"]] = supplierModel.SUPPLIER;
            recordObject[recordType.Attributes["SUP_NAME_SECONDARY"]] = supplierModel.SupplierSecondaryName;
            recordObject[recordType.Attributes["LANG_ID"]] = supplierModel.LanguageId;
            recordObject[recordType.Attributes["CONTACT_EMAIL1"]] = supplierModel.ContactEmail1;
            recordObject[recordType.Attributes["CONTACT_FAX"]] = supplierModel.ContactFax;
            recordObject[recordType.Attributes["CONTACT_NAME1"]] = supplierModel.ContactName1;
            recordObject[recordType.Attributes["CONTACT_PAGER"]] = supplierModel.ContactPager;
            recordObject[recordType.Attributes["CONTACT_PHONE"]] = supplierModel.ContactPhone;
            recordObject[recordType.Attributes["QC_FREQ"]] = supplierModel.QcFrequency;
            recordObject[recordType.Attributes["QC_IND"]] = supplierModel.QcIndicator;
            recordObject[recordType.Attributes["QC_PCT"]] = supplierModel.QcPercentage;
            recordObject[recordType.Attributes["VC_FREQ"]] = supplierModel.VcFrequency;
            recordObject[recordType.Attributes["VC_IND"]] = supplierModel.VcIndicator;
            recordObject[recordType.Attributes["VC_PCT"]] = supplierModel.VcPercentage;
            recordObject[recordType.Attributes["CURRENCY_CODE"]] = supplierModel.CurrencyCode;
            recordObject[recordType.Attributes["VAT_EXEMPT"]] = supplierModel.VatExempt;
            recordObject[recordType.Attributes["VAT_REGION"]] = supplierModel.VatRegion;
            recordObject[recordType.Attributes["VAT_REG_ID"]] = supplierModel.VatRegistrationId;
            recordObject[recordType.Attributes["PAYMENT_METHOD"]] = supplierModel.PaymentMode;
            recordObject[recordType.Attributes["AUTO_APPR_INVC_IND"]] = supplierModel.AutoApproveInvoiceIndicator;
            recordObject[recordType.Attributes["COST_CHG_AMT_VAR"]] = supplierModel.CostChgAmountVar;
            recordObject[recordType.Attributes["COST_CHG_PCT_VAR"]] = supplierModel.CostChgPercentageVar;
            recordObject[recordType.Attributes["ALLOW_REBATE"]] = supplierModel.AllowRebate;
            recordObject[recordType.Attributes["REBATE_PERIOD"]] = supplierModel.RebatePeriod;
            recordObject[recordType.Attributes["REBATE_DISCOUNT"]] = supplierModel.RebateDiscount;
            recordObject[recordType.Attributes["DISPLAY_DISCOUNT"]] = supplierModel.DisplayDiscount;
            recordObject[recordType.Attributes["DBT_MEMO_CODE"]] = supplierModel.DbtMemoCode;
            recordObject[recordType.Attributes["SETTLEMENT_CODE"]] = supplierModel.SettlementCode = "E";
            recordObject[recordType.Attributes["BRACKET_COSTING_IND"]] = supplierModel.BracketCostingIndicator;
            recordObject[recordType.Attributes["AUTO_APPR_DBT_MEMO_IND"]] = supplierModel.AutoApproveDbtMemoIndicator;
            recordObject[recordType.Attributes["INVC_PAY_LOC"]] = supplierModel.InvoicePayLocation;
            recordObject[recordType.Attributes["INVC_RECEIVE_LOC"]] = supplierModel.InvoiceRecieveLocation;
            recordObject[recordType.Attributes["ADDINVC_GROSS_NET"]] = supplierModel.AddInvoiceGrossNet;
            recordObject[recordType.Attributes["PREPAY_INVC_IND"]] = supplierModel.PrePayInvoiceIndicator;
            recordObject[recordType.Attributes["BACKORDER_IND"]] = supplierModel.BackOrderIndicator;
            recordObject[recordType.Attributes["ALLOW_OVERDUE"]] = supplierModel.AllowOverDue;
            recordObject[recordType.Attributes["EDI_ASN"]] = supplierModel.EdiAsn;
            recordObject[recordType.Attributes["EDI_CONTRACT_IND"]] = supplierModel.EdiContractIndicator;
            recordObject[recordType.Attributes["EDI_INVC_IND"]] = supplierModel.EdiInvoiceIndicator;
            recordObject[recordType.Attributes["EDI_PO_CHG"]] = supplierModel.EdiPoChange;
            recordObject[recordType.Attributes["EDI_PO_CONFIRM"]] = supplierModel.EdiPoConfirm;
            recordObject[recordType.Attributes["EDI_PO_IND"]] = supplierModel.EdiPoIndicator;
            recordObject[recordType.Attributes["EDI_SALES_RPT_FREQ"]] = supplierModel.EdiSalesRepeatFrequency;
            recordObject[recordType.Attributes["EDI_SUPP_AVAILABLE_IND"]] = supplierModel.EdiSupportAvailableIndicator;
            recordObject[recordType.Attributes["FREIGHT_CHARGE_IND"]] = supplierModel.FreightChargeIndicator;
            recordObject[recordType.Attributes["TERMS_ID"]] = supplierModel.PaymentTerms;
            recordObject[recordType.Attributes["FREIGHT_TERMS"]] = supplierModel.FreightTerms;
            recordObject[recordType.Attributes["OF_ENABLE_TRANSFER_FLAG"]] = supplierModel.OfEnableTransferFlag;
            recordObject[recordType.Attributes["OF_TRANSFER_FLAG"]] = supplierModel.OfTransferFlag;
            recordObject[recordType.Attributes["SERVICE_PERF_REQ_IND"]] = supplierModel.ServicePerfReqIndicator;
            recordObject[recordType.Attributes["SHIP_METHOD"]] = supplierModel.ShipMethod;
            recordObject[recordType.Attributes["FINAL_DEST_IND"]] = supplierModel.FinalDstIndicator;
            recordObject[recordType.Attributes["DEFAULT_ITEM_LEAD_TIME"]] = supplierModel.DefaultItemLeadTime;
            recordObject[recordType.Attributes["DELIVERY_POLICY"]] = supplierModel.DeliveryPolicy;
            recordObject[recordType.Attributes["DUNS_LOC"]] = supplierModel.DunsLocation;
            recordObject[recordType.Attributes["DUNS_NUMBER"]] = supplierModel.DunsNumber;
            recordObject[recordType.Attributes["DSD_IND"]] = supplierModel.DsdIndicator;
            recordObject[recordType.Attributes["INV_MGMT_LVL"]] = supplierModel.InvManagementLevel;
            recordObject[recordType.Attributes["RET_COURIER"]] = supplierModel.RetailCourier;
            recordObject[recordType.Attributes["RET_ALLOW_IND"]] = supplierModel.RetAllowIndicator;
            recordObject[recordType.Attributes["RET_AUTH_REQ"]] = supplierModel.RetAuthorizationRequired;
            recordObject[recordType.Attributes["RET_MIN_DOL_AMT"]] = supplierModel.RetMinimumDolAmount;
            recordObject[recordType.Attributes["REPLEN_APPROVAL_IND"]] = supplierModel.ReplenApprovalIndicator;
            recordObject[recordType.Attributes["HANDLING_PCT"]] = supplierModel.HandlingPercentage;
            recordObject[recordType.Attributes["PRE_MARK_IND"]] = supplierModel.PreMarkIndicator;
            recordObject[recordType.Attributes["VMI_ORDER_STATUS"]] = supplierModel.VmiOrderStatus;
            recordObject[recordType.Attributes["COMMENT_DESC"]] = supplierModel.CommentDescription;
            recordObject[recordType.Attributes["SUPPLIER_TIER"]] = supplierModel.SupplierTier;
            recordObject[recordType.Attributes["SUPPLIER_GROUP"]] = supplierModel.SupplierGroup;
            recordObject[recordType.Attributes["CONTACT_NAME2"]] = supplierModel.ContactName2;
            recordObject[recordType.Attributes["CONTACT_TITLE1"]] = supplierModel.ContactTitle1;
            recordObject[recordType.Attributes["CONTACT_TITLE2"]] = supplierModel.ContactTitle2;
            recordObject[recordType.Attributes["CONTACT_EMAIL2"]] = supplierModel.ContactEmail2;
            recordObject[recordType.Attributes["OTHERS"]] = supplierModel.Others;
            recordObject[recordType.Attributes["IS_EXPIRY"]] = supplierModel.IsExpiry;
            recordObject[recordType.Attributes["SUPPLIER_RETAIL_FLAG"]] = supplierModel.SupplierRetailFlag;
            recordObject[recordType.Attributes["SUPPLIER_TYPE"]] = supplierModel.SupplierType;
            recordObject[recordType.Attributes["LISTING_DEALS_AMOUNT"]] = supplierModel.ListingFee;
            recordObject[recordType.Attributes["ACCOUNT_OPENING_FEE"]] = supplierModel.AccountOpeningFee;
            recordObject[recordType.Attributes["NEW_STORE_OPENING_FEE"]] = supplierModel.NewStoreOpeningFee;
            recordObject[recordType.Attributes["PAYTERM_TYPE"]] = supplierModel.PaymentTermType;
            recordObject[recordType.Attributes["BARCODE_PRINT_CHARGE"]] = supplierModel.BarCodePrintCharge;
            recordObject[recordType.Attributes["BARCODE_PRINTING"]] = supplierModel.BarCodePrintingFlag;
            recordObject[recordType.Attributes["COST_HIDE_INVOICE"]] = supplierModel.CostHideInvoice;
            recordObject[recordType.Attributes["DISC_HIDE_INVOICE"]] = supplierModel.DiscountHideInvoice;
            recordObject[recordType.Attributes["DC_FLAG"]] = supplierModel.DcIndicator;
            recordObject[recordType.Attributes["BANK_NAME"]] = supplierModel.BankName;
            recordObject[recordType.Attributes["BANK_ADDRESS"]] = supplierModel.BankAddress;
            recordObject[recordType.Attributes["BANK_DETAILS"]] = supplierModel.BankDetails;
            recordObject[recordType.Attributes["ACCOUNTNUMBERTYPE"]] = supplierModel.AccountNumberType;
            recordObject[recordType.Attributes["EXPECTEDANNUALVOL"]] = supplierModel.ExpectedAnnualVol;
            recordObject[recordType.Attributes["MIN_SERVICE_LEVEL"]] = supplierModel.MinServiceLevel;
            recordObject[recordType.Attributes["OPERATION"]] = supplierModel.Operation;
            recordObject[recordType.Attributes["CREATED_BY"]] = this.GetCreatedBy(supplierModel.CreatedBy);
            recordObject[recordType.Attributes["LAST_UPDATED_BY"]] = FxContext.Context.Name;

            // Address Object
            OracleType dtlTableType = OracleType.GetObjectType(SupplierConstants.ObjTypeSupplierAddress, this.Connection);
            OracleTable pUtlDmnDtlTab = new OracleTable(dtlTableType);
            foreach (SupplierAddressMasterModel detail in supplierModel.ListOfAddress)
            {
                OracleObject dtlRecordObject = this.SetParamsSupplierAddress(detail, false);
                pUtlDmnDtlTab.Add(dtlRecordObject);
            }

            recordObject[recordType.Attributes["SUPPLIER_ADDR_TAB"]] = pUtlDmnDtlTab;

            // Progressive Rebates Object
            OracleType rebateHeadTableType = OracleType.GetObjectType(SupplierConstants.ObjTypeRebateHead, this.Connection);
            OracleTable pRebateHeadTab = new OracleTable(rebateHeadTableType);
            OracleObject recordObjectRebateHead = this.SetParamsProgressiveRebate(supplierModel.RebateHead);
            pRebateHeadTab.Add(recordObjectRebateHead);
            recordObject[recordType.Attributes["REBATE_HDR_TAB"]] = pRebateHeadTab;
            recordObject[recordType.Attributes["SUP_NAME"]] = supplierModel.SupplierName;
            recordObject[recordType.Attributes["SUP_STATUS"]] = supplierModel.SupplierStatus;
            return recordObject;
        }

        private OracleObject SetParamsSupplierAddress(SupplierAddressMasterModel supplierAddressModel, bool isSearch)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(SupplierConstants.RecObjSupplierAddress, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["SUPPLIER"]] = supplierAddressModel.SupplierId;
            if (!isSearch)
            {
                recordObject[recordType.Attributes["SEQ_NO"]] = supplierAddressModel.SequenceNo;
                recordObject[recordType.Attributes["CONTACT_EMAIL"]] = supplierAddressModel.ContactEmail;
                recordObject[recordType.Attributes["CONTACT_FAX"]] = supplierAddressModel.ContactFax;
                recordObject[recordType.Attributes["CONTACT_NAME"]] = supplierAddressModel.ContactName;
                recordObject[recordType.Attributes["CONTACT_TELEX"]] = supplierAddressModel.ContactTelex;
                recordObject[recordType.Attributes["CONTACT_PHONE"]] = supplierAddressModel.ContactPhone;
                recordObject[recordType.Attributes["ADD_1"]] = supplierAddressModel.AddressLine1;
                recordObject[recordType.Attributes["ADD_2"]] = supplierAddressModel.AddressLine2;
                recordObject[recordType.Attributes["ADD_3"]] = supplierAddressModel.AddressLine3;
                recordObject[recordType.Attributes["PRIMARY_ADDR_IND"]] = supplierAddressModel.PrimaryAddressIndicator;
                recordObject[recordType.Attributes["COUNTRY_ID"]] = supplierAddressModel.CountryID;
                recordObject[recordType.Attributes["POST"]] = supplierAddressModel.PostalCode;
                recordObject[recordType.Attributes["EDI_ADDR_CHG"]] = supplierAddressModel.EdiAddressChg;
                recordObject[recordType.Attributes["PUBLISH_IND"]] = supplierAddressModel.PublishIndicator;
                recordObject[recordType.Attributes["OPERATION"]] = supplierAddressModel.Operation;
                if (supplierAddressModel.Operation.Equals(RbCommonConstants.InsertOperation))
                {
                    recordObject[recordType.Attributes["CREATED_BY"]] = FxContext.Context.Name;
                }

                recordObject[recordType.Attributes["LAST_UPDATED_BY"]] = FxContext.Context.Name;
            }

            recordObject[recordType.Attributes["ORACLE_VENDOR_SITE_ID"]] = supplierAddressModel.OracleVendorSiteID;
            recordObject[recordType.Attributes["CITY"]] = supplierAddressModel.City;
            recordObject[recordType.Attributes["STATE"]] = supplierAddressModel.State;
            recordObject[recordType.Attributes["COUNTRY"]] = supplierAddressModel.Country;
            recordObject[recordType.Attributes["ADDR_TYPE"]] = supplierAddressModel.AddressType;
            return recordObject;
        }

        private OracleObject SetParamsProgressiveRebate(SupplierRebateHeadModel rebateHead)
        {
            OracleType rebateHeadRecordType = OracleType.GetObjectType(SupplierConstants.RecObjRebateHead, this.Connection);
            OracleObject recordObjectRebateHead = new OracleObject(rebateHeadRecordType);
            recordObjectRebateHead[rebateHeadRecordType.Attributes["REBATE_ID"]] = rebateHead.RebateId;
            recordObjectRebateHead[rebateHeadRecordType.Attributes["SUPPLIER_ID"]] = rebateHead.SupplierId;
            recordObjectRebateHead[rebateHeadRecordType.Attributes["REBATE_TYPE"]] = rebateHead.RebateType;
            recordObjectRebateHead[rebateHeadRecordType.Attributes["DEFAULT_REBATE_PCT"]] = rebateHead.DefaultRebatePercent;
            recordObjectRebateHead[rebateHeadRecordType.Attributes["REBATE_STATUS"]] = rebateHead.RebateStatus;
            recordObjectRebateHead[rebateHeadRecordType.Attributes["OPERATION"]] = rebateHead.Operation;
            if (rebateHead.Operation.Equals(RbCommonConstants.InsertOperation))
            {
                recordObjectRebateHead[rebateHeadRecordType.Attributes["CREATED_BY"]] = FxContext.Context.Name;
            }

            recordObjectRebateHead[rebateHeadRecordType.Attributes["LAST_UPDATED_BY"]] = FxContext.Context.Name;

            // Rebate Detail Object
            OracleType rebateDetailTableType = OracleType.GetObjectType(SupplierConstants.ObjTypeRebateDetail, this.Connection);
            OracleType rebateDetailRecordType = OracleType.GetObjectType(SupplierConstants.RecObjRebateDetail, this.Connection);
            OracleTable pRebateDtlTab = new OracleTable(rebateDetailTableType);
            foreach (SupplierRebateDetailModel detail in rebateHead.RebateDetails)
            {
                var dtlRecordObject = new OracleObject(rebateDetailRecordType);
                dtlRecordObject[rebateDetailRecordType.Attributes["REBATE_ID"]] = detail.RebateId;
                dtlRecordObject[rebateDetailRecordType.Attributes["REBATE_DETAIL_ID"]] = detail.RebateDetailId;
                dtlRecordObject[rebateDetailRecordType.Attributes["GROWTH_PCT"]] = detail.GrowthPercent;
                dtlRecordObject[rebateDetailRecordType.Attributes["MIN_THRESOLD_LIMIT"]] = detail.MinThresholdLimit;
                dtlRecordObject[rebateDetailRecordType.Attributes["MAX_THRESOLD_LIMIT"]] = detail.MaxThresholdLimit;
                dtlRecordObject[rebateDetailRecordType.Attributes["REBATE_PCT"]] = detail.RebatePercent;
                dtlRecordObject[rebateDetailRecordType.Attributes["REBATE_START_DATE"]] = detail.RebateStartDate;
                dtlRecordObject[rebateDetailRecordType.Attributes["REBATE_END_DATE"]] = detail.RebateEndDate;
                dtlRecordObject[rebateDetailRecordType.Attributes["REBATE_DTL_STATUS"]] = detail.RebateDetailStatus;
                dtlRecordObject[rebateDetailRecordType.Attributes["OPERATION"]] = detail.Operation;
                if (detail.Operation.Equals(RbCommonConstants.InsertOperation))
                {
                    dtlRecordObject[rebateDetailRecordType.Attributes["CREATED_BY"]] = FxContext.Context.Name;
                }

                dtlRecordObject[rebateDetailRecordType.Attributes["LAST_UPDATED_BY"]] = FxContext.Context.Name;
                pRebateDtlTab.Add(dtlRecordObject);
            }

            recordObjectRebateHead[rebateHeadRecordType.Attributes["REBATE_DTL_TAB"]] = pRebateDtlTab;

            return recordObjectRebateHead;
        }

        private OracleObject SetParamsSupplierList(SupplierSearchModel supplierModel)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(SupplierConstants.RecObjSupplierSearch, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["SUPPLIER"]] = supplierModel.SupplierId;
            recordObject[recordType.Attributes["SUP_NAME"]] = supplierModel.SupplierName;
            recordObject[recordType.Attributes["SUP_STATUS"]] = supplierModel.SupplierStatus;
            recordObject[recordType.Attributes["SUPPLIER_TYPE"]] = supplierModel.SupplierType;
            return recordObject;
        }

        private OracleObject SetParamsSupplierGroup(SupplierGroupModel supplierGroupModel, bool isSearch)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(SupplierConstants.RecordTypeSupplierGroup, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            if (!isSearch)
            {
                recordObject[recordType.Attributes["OPERATION"]] = supplierGroupModel.Operation;
                recordObject[recordType.Attributes["CREATED_BY"]] = this.GetCreatedBy(supplierGroupModel.CreatedBy);
                recordObject[recordType.Attributes["LAST_UPDATED_BY"]] = FxContext.Context.Name;
            }

            recordObject[recordType.Attributes["ID"]] = supplierGroupModel.Id;
            recordObject[recordType.Attributes["NAME"]] = supplierGroupModel.Name;
            recordObject[recordType.Attributes["STATUS"]] = supplierGroupModel.Status;

            return recordObject;
        }
    }
}