﻿// <copyright file="SupplierAddressController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.SupplierManagement.SupplierAddress
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.SupplierManagement.Common;
    using Agility.RBS.SupplierManagement.Repository;
    using Agility.RBS.SupplierManagement.SupplierAddress.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class SupplierAddressController : Controller
    {
        private readonly ServiceDocument<SupplierAddressMasterModel> serviceDocument;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly SupplierRepository supplierRepository;
        private readonly DomainDataRepository domainDataRepository;

        public SupplierAddressController(ServiceDocument<SupplierAddressMasterModel> serviceDocument, SupplierRepository supplierRepository, DomainDataRepository domainDataRepository)
        {
            this.supplierRepository = supplierRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<SupplierAddressMasterModel>> List()
        {
            this.serviceDocument.DomainData.Add("supplier", this.domainDataRepository.SupplierDomainGet().Result);
            this.serviceDocument.DomainData.Add("status", this.domainDataRepository.DomainDetailData(SupplierConstants.DomainHdrStatus).Result);
            this.serviceDocument.DomainData.Add("addressType", this.domainDataRepository.DomainDetailData(SupplierConstants.DomainHdrAddressType).Result);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<SupplierAddressMasterModel>> New()
        {
            this.serviceDocument.DomainData.Add("supplier", this.domainDataRepository.SupplierDomainGet().Result);
            this.serviceDocument.DomainData.Add("status", this.domainDataRepository.DomainDetailData(SupplierConstants.DomainHdrStatus).Result);
            this.serviceDocument.DomainData.Add("indicator", this.domainDataRepository.DomainDetailData(SupplierConstants.DomainHdrIndicator).Result);
            this.serviceDocument.DomainData.Add("country", this.domainDataRepository.CountryDomainGet().Result);
            this.serviceDocument.DomainData.Add("addressType", this.domainDataRepository.DomainDetailData(SupplierConstants.DomainHdrAddressType).Result);
            this.serviceDocument.LocalizationData.AddData("SupplierAddressMaster");
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<SupplierAddressMasterModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.GetListOfSupplierAddress);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<SupplierAddressMasterModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.SaveSupplierAddress);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingSupplierAddress(int supplierId, string addressType)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "SUPPLIER_ADDRESS";
            nvc["P_COL1"] = supplierId.ToString();
            nvc["P_COL2"] = addressType;
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        private async Task<List<SupplierAddressMasterModel>> GetListOfSupplierAddress()
        {
            try
            {
                var listOfSupplierAddressData = await this.supplierRepository.GetListOfSupplierAddress(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return listOfSupplierAddressData;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> SaveSupplierAddress()
        {
            this.serviceDocument.Result = await this.supplierRepository.SetSupplierAddress(this.serviceDocument.DataProfile.DataModel);
            return true;
        }
    }
}