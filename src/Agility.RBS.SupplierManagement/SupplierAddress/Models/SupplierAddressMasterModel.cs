﻿// <copyright file="SupplierAddressMasterModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.SupplierManagement.SupplierAddress.Models
{
    using System;
    using Agility.Framework.Core.Profile.Entities;

    public class SupplierAddressMasterModel : ProfileEntity
    {
        public string SupplierId { get; set; }

        public string SupplierName { get; set; }

        public string AddressType { get; set; }

        public string AddressTypeDesc { get; set; }

        public int? SequenceNo { get; set; }

        public int? OracleVendorSiteID { get; set; }

        public string ContactName { get; set; }

        public string ContactPhone { get; set; }

        public string ContactFax { get; set; }

        public string ContactEmail { get; set; }

        public string ContactTelex { get; set; }

        public string PrimaryAddressIndicator { get; set; }

        public string PrimaryAddressIndicatorDesc { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string AddressLine3 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string CountryID { get; set; }

        public string PostalCode { get; set; }

        public string EdiAddressChg { get; set; }

        public string Country { get; set; }

        public string PublishIndicator { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }
    }
}
