﻿// <copyright file="SupplierAddressModelMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.SupplierManagement.SupplierAddress.Mapping
{
    using System;
    using Agility.RBS.Core;
    using Agility.RBS.SupplierManagement.SupplierAddress.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class SupplierAddressModelMapping : Profile
    {
        public SupplierAddressModelMapping()
            : base("SupplierAddressModelMapping")
        {
            this.CreateMap<OracleObject, SupplierAddressMasterModel>()
                .ForMember(m => m.SupplierId, opt => opt.MapFrom(r => r["SUPPLIER"]))
                .ForMember(m => m.SupplierName, opt => opt.MapFrom(r => r["SUP_NAME"]))
                .ForMember(m => m.AddressType, opt => opt.MapFrom(r => r["ADDR_TYPE"]))
                .ForMember(m => m.AddressTypeDesc, opt => opt.MapFrom(r => r["ADDR_TYPE_DESC"]))
                .ForMember(m => m.SequenceNo, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["SEQ_NO"])))
                .ForMember(m => m.OracleVendorSiteID, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["ORACLE_VENDOR_SITE_ID"])))
                .ForMember(m => m.ContactName, opt => opt.MapFrom(r => r["CONTACT_NAME"]))
                .ForMember(m => m.ContactEmail, opt => opt.MapFrom(r => r["CONTACT_EMAIL"]))
                .ForMember(m => m.ContactFax, opt => opt.MapFrom(r => r["CONTACT_FAX"]))
                .ForMember(m => m.ContactPhone, opt => opt.MapFrom(r => r["CONTACT_PHONE"]))
                .ForMember(m => m.PrimaryAddressIndicator, opt => opt.MapFrom(r => r["PRIMARY_ADDR_IND"]))
                .ForMember(m => m.PrimaryAddressIndicatorDesc, opt => opt.MapFrom(r => r["PRIMARY_ADDR_IND_DESC"]))
                .ForMember(m => m.AddressLine1, opt => opt.MapFrom(r => r["ADD_1"]))
                .ForMember(m => m.AddressLine2, opt => opt.MapFrom(r => r["ADD_2"]))
                .ForMember(m => m.AddressLine3, opt => opt.MapFrom(r => r["ADD_3"]))
                .ForMember(m => m.City, opt => opt.MapFrom(r => r["CITY"]))
                .ForMember(m => m.State, opt => opt.MapFrom(r => r["STATE"]))
                .ForMember(m => m.CountryID, opt => opt.MapFrom(r => r["COUNTRY_ID"]))
                .ForMember(m => m.PostalCode, opt => opt.MapFrom(r => r["POST"]))
                .ForMember(m => m.EdiAddressChg, opt => opt.MapFrom(r => r["EDI_ADDR_CHG"]))
                .ForMember(m => m.PublishIndicator, opt => opt.MapFrom(r => r["PUBLISH_IND"]))
                .ForMember(m => m.Country, opt => opt.MapFrom(r => r["COUNTRY"]))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "SUPPLIER_ADDRESS"))
                .ForMember(m => m.CreatedDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["CREATED_DATE"])))
                .ForMember(m => m.ModifiedDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["LAST_UPDATED_DATE"])))
                .ForMember(m => m.CreatedBy, opt => opt.MapFrom(r => r["CREATED_BY"]))
                .ForMember(m => m.ModifiedBy, opt => opt.MapFrom(r => r["LAST_UPDATED_BY"]))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));
        }
    }
}
