﻿// <copyright file="OrderMgmtConstants.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.OrderManagement
{
    internal static class OrderMgmtConstants
    {
        public const string GetWrapperPackage = "ORDERWRAPPER_SQL";
        public const string GetOrderPackage = "ORDER_SQL";
        public const string ObjTypeOrderSearch = "ORDER_SEARCH_TAB";
        public const string RecordObjOrderSearch = "ORDER_SEARCH_REC";
        public const string GetProcOrderList = "GETORDERLIST";
        public const string ObjTypeOrder = "ORDER_TAB";
        public const string RecordObjOrder = "ORDER_REC";
        public const string RecordObjOrderHead = "ORDHEAD_REC";
        public const string ObjTypeOrderHead = "ORDHEAD_TAB";

        public const string RecordObjOrdLoc = "ORDLOC_REC";
        public const string ObjTypeOrdLoc = "ORDLOC_TAB";

        public const string RecordObjOrdSku = "ORDSKU_REC";
        public const string ObjTypeOrdSku = "ORDSKU_TAB";

        public const string ObjTypeOrderHeader = "ORDER_HDR_TAB";
        public const string RecordObjOrderHeader = "ORDER_HDR_REC";
        public const string GetProcOrderHead = "GETORDERHEAD";
        public const string SetProcOrder = "SETORDERS";
        public const string DeleteOrders = "DELETEORDERS";
        public const string UpdateOrders = "UPDATEORDERS";
        public const string InsertOrders = "INSERTORDERS";

        public const string ObjTypeOrderDetail = "ORDER_DTL_TAB";
        public const string RecordObjOrderDetail = "ORDER_DTL_REC";
        public const string GetProcOrderDetail = "GETORDERDETAIL";
        public const string SetProcOrderDetail = "SETORDERDETAIL";
        public const string GetImportOrderDetails = "validate_orderimport";

        public const string ObjTypeOrderPrint = "ORDER_HDR_PRINT_TAB";
        public const string GetProcOrderPrint = "GETORDERPRINT";
        public const string RecordObjOrderPrint = "ORDER_HDR_PRINT_REC";

        public const string ObjTypeDsdAr = "DSD_AR_ORDERS_TAB";
        public const string RecordObjDsdAr = "DSD_AR_ORDERS_REC";
        public const string GetProcDsdAr = "GETDSDARORDERS";

        // Domain Header Ids
        public const string DomainHdrOrderType = "ORDO";
        public const string DomainHdrOrderOrigin = "OROR";
        public const string DomainHdrLocType = "LOTP";
        public const string DomainHdrDateType = "DATETYPE";
        public const string DomainHdrOrderStatus = "ODRSTAT";
        public const string DomainHdrIndicator = "INDICATOR";
        public const string DomainHdrShipMethod = "SHIPM";
        public const string DomainHdrPurchaseTyp = "PURT";
        public const string DomainHdrPartnerTyp = "PRTNT";
        public const string DomainHdrPoTyp = "POTYP";
        public const string DomainHdrReject = "REJCT";
        public const string DomainHdrCostSource = "OICS";
        public const string DomainHdrYsno = "YSNO";
        public const string DomainHdrOrst = "ORST";

        public const string DeleteOperation = "D";
        public const string UpdateOperation = "U";
        public const string InsertOperation = "I";
    }
}