﻿// <copyright file="WrapperRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.OrderManagement.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.OrderManagement.Order.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class WrapperRepository : BaseOraclePackage
    {
        public WrapperRepository()
        {
            this.PackageName = OrderMgmtConstants.GetWrapperPackage;
        }

        public int? OrderNo { get; private set; }

        public async Task<ServiceDocumentResult> OrderHeadSet(OrderHdrModel orderHead)
        {
            this.OrderNo = 0;
            PackageParams packageParameter = this.GetPackageParams(OrderMgmtConstants.ObjTypeOrder, OrderMgmtConstants.SetProcOrder);
            OracleObject recordObject = this.SetParamsOrderHead(orderHead, orderHead.OrderDetails);
            var result = await this.SetProcedure(recordObject, packageParameter);
            if (this.ObjResult != null && orderHead.ORDER_NO == null)
            {
                OracleTable orderDtls = (Devart.Data.Oracle.OracleTable)this.ObjResult["ORDHEAD_ROW"];
                if (orderDtls.Count > 0)
                {
                    OracleObject obj = (OracleObject)orderDtls[0];
                    if (obj != null && !string.IsNullOrEmpty(Convert.ToString(obj["ORDER_NO"])))
                    {
                        this.OrderNo = Convert.ToInt32(obj["ORDER_NO"]);
                    }
                }
            }
            else
            {
                this.OrderNo = orderHead.ORDER_NO;
            }

            return result;
        }

        public async Task<ServiceDocumentResult> OrderHeadSetUpdate(OrderHdrModel orderHead)
        {
            ServiceDocumentResult result = null;
            this.OrderNo = 0;
            var deleteDetails = orderHead.OrderDetails.Where(ids => ids.Operation == OrderMgmtConstants.DeleteOperation).ToList();
            var updateDetails = orderHead.OrderDetails.Where(ids => ids.Operation == OrderMgmtConstants.UpdateOperation).ToList();
            var insertDetails = orderHead.OrderDetails.Where(ids => ids.Operation == OrderMgmtConstants.InsertOperation).ToList();
            if (deleteDetails.Count > 0)
            {
                PackageParams packageParameter = this.GetPackageParams(OrderMgmtConstants.ObjTypeOrder, OrderMgmtConstants.DeleteOrders);
                OracleObject recordObject = this.SetParamsOrderHead(orderHead, deleteDetails);
                result = await this.SetProcedure(recordObject, packageParameter);
            }

            if (updateDetails.Count > 0)
            {
                PackageParams packageParameter = this.GetPackageParams(OrderMgmtConstants.ObjTypeOrder, OrderMgmtConstants.UpdateOrders);
                OracleObject recordObject = this.SetParamsOrderHead(orderHead, updateDetails);
                result = await this.SetProcedure(recordObject, packageParameter);
            }

            if (insertDetails.Count > 0)
            {
                PackageParams packageParameter = this.GetPackageParams(OrderMgmtConstants.ObjTypeOrder, OrderMgmtConstants.InsertOrders);
                OracleObject recordObject = this.SetParamsOrderHead(orderHead, insertDetails);
                result = await this.SetProcedure(recordObject, packageParameter);
            }

            this.OrderNo = orderHead.ORDER_NO;
            return result;
        }

        private static void OrdDetailBuilder(OrderHdrModel orderHead, List<OrderDetailModel> orderDetails, OrderDtlObjectModel orderDtlObjectModel)
        {
            foreach (OrderDetailModel detail in orderDetails)
            {
                orderDtlObjectModel.TblItems.Add(detail.Item);
                orderDtlObjectModel.TblLocs.Add(orderHead.Location);
                orderDtlObjectModel.TblLocTypes.Add(orderHead.LocType);
                orderDtlObjectModel.TblUnitRetails.Add(detail.UnitRetail);
                orderDtlObjectModel.TblOrderedQtys.Add(detail.QtyOrdered);
                orderDtlObjectModel.TblPrescaledQtys.Add(detail.QtyOrdered);
                orderDtlObjectModel.TblCancelledQtys.Add(detail.CancelledQty);
                orderDtlObjectModel.TblCancelCodes.Add(detail.CancelCode);
                orderDtlObjectModel.TblCancelDates.Add(detail.CancelDate);
                orderDtlObjectModel.TblCancelIds.Add(detail.CancelId);
                orderDtlObjectModel.TblUnitCosts.Add(detail.UnitCost.Value);
                orderDtlObjectModel.TblCostSources.Add(detail.CostSource);
                orderDtlObjectModel.TblNonscaleInds.Add(detail.NonScaleInd);
                orderDtlObjectModel.TblEstimatedInstockDates.Add(detail.EstimatedInstockDate);
                orderDtlObjectModel.TblEarliestShipDates.Add(orderHead.EarliestShipDate);
                orderDtlObjectModel.TblLatestShipDates.Add(orderHead.LatestShipDate);
                orderDtlObjectModel.TblOriginCountryIds.Add(detail.OriginCountryId);
                orderDtlObjectModel.TblSuppPackSizes.Add(detail.DefaultUnitSize);
                ////tblOperation.Add(detail.Operation);
            }
        }

        private OracleObject SetParamsOrderHead(OrderHdrModel orderHead, List<OrderDetailModel> details)
        {
            this.Connection.Open();
            OracleObject recordObjectOrder = this.GetOracleObject(this.GetObjectType(OrderMgmtConstants.RecordObjOrder));
            recordObjectOrder["ORDHEAD_ROW"] = this.OrdHeadBuilder(orderHead);

            OrderDtlObjectModel orderDtlObjectModel = new OrderDtlObjectModel();
            orderDtlObjectModel.TblItems = this.GetOracleTable(this.GetObjectType("ITEM_TBL"));
            orderDtlObjectModel.TblLocs = this.GetOracleTable(this.GetObjectType("LOC_TBL"));
            orderDtlObjectModel.TblLocTypes = this.GetOracleTable(this.GetObjectType("LOC_TYPE_TBL"));
            orderDtlObjectModel.TblUnitRetails = this.GetOracleTable(this.GetObjectType("UNIT_RETAIL_TBL"));
            orderDtlObjectModel.TblOrderedQtys = this.GetOracleTable(this.GetObjectType("QTY_TBL"));
            orderDtlObjectModel.TblPrescaledQtys = this.GetOracleTable(this.GetObjectType("QTY_TBL"));
            orderDtlObjectModel.TblCancelledQtys = this.GetOracleTable(this.GetObjectType("QTY_TBL"));
            orderDtlObjectModel.TblCancelCodes = this.GetOracleTable(this.GetObjectType("INDICATOR_TBL"));
            orderDtlObjectModel.TblCancelDates = this.GetOracleTable(this.GetObjectType("DATE_TBL"));
            orderDtlObjectModel.TblCancelIds = this.GetOracleTable(this.GetObjectType("USERID_TBL"));
            orderDtlObjectModel.TblUnitCosts = this.GetOracleTable(this.GetObjectType("UNIT_COST_TBL"));
            orderDtlObjectModel.TblCostSources = this.GetOracleTable(this.GetObjectType("CODE_TBL"));
            orderDtlObjectModel.TblNonscaleInds = this.GetOracleTable(this.GetObjectType("INDICATOR_TBL"));
            orderDtlObjectModel.TblEstimatedInstockDates = this.GetOracleTable(this.GetObjectType("DATE_TBL"));
            orderDtlObjectModel.TblOriginCountryIds = this.GetOracleTable(this.GetObjectType("COUNTRY_TBL"));
            orderDtlObjectModel.TblEarliestShipDates = this.GetOracleTable(this.GetObjectType("DATE_TBL"));
            orderDtlObjectModel.TblLatestShipDates = this.GetOracleTable(this.GetObjectType("DATE_TBL"));
            orderDtlObjectModel.TblSuppPackSizes = this.GetOracleTable(this.GetObjectType("QTY_TBL"));
            //// OracleTable tblOperation = new OracleTable(OracleType.GetObjectType("OPERATION_TBL", this.Connection));

            OrdDetailBuilder(orderHead, details, orderDtlObjectModel);
            recordObjectOrder["ORDLOCS"] = this.OrdLocBuilder(orderDtlObjectModel, orderHead.ORDER_NO);
            recordObjectOrder["ORDSKUS"] = this.OrdSkuBuilder(orderDtlObjectModel, orderHead.ORDER_NO);
            return recordObjectOrder;
        }

        private OracleTable OrdHeadBuilder(OrderHdrModel orderHead)
        {
            // Order Head Object
            OracleTable oracleTableOrdHead = this.GetOracleTable(this.GetObjectType(OrderMgmtConstants.ObjTypeOrderHead));
            OracleObject recordObjOrdHead = this.GetOracleObject(this.GetObjectType(OrderMgmtConstants.RecordObjOrderHead));

            recordObjOrdHead["ORDER_NO"] = orderHead.ORDER_NO;
            recordObjOrdHead["ORDER_TYPE"] = orderHead.OrderType;
            recordObjOrdHead["DEPT"] = orderHead.Dept;
            ////recordObjOrdHead["BUYER"] = orderHead.Buyer;
            recordObjOrdHead["SUPPLIER"] = orderHead.Supplier;
            ////recordObjOrdHead["SUPP_ADD_SEQ_NO"] = orderHead.SuppAddSeqNo;
            recordObjOrdHead["LOC_TYPE"] = orderHead.LocType;
            recordObjOrdHead["LOCATION"] = orderHead.Location;
            ////recordObjOrdHead["PROMOTION"] = orderHead.Promotion;
            recordObjOrdHead["QC_IND"] = orderHead.QcInd;
            recordObjOrdHead["NOT_BEFORE_DATE"] = orderHead.NotBeforeDate;
            recordObjOrdHead["NOT_AFTER_DATE"] = orderHead.NotAfterDate;
            ////recordObjOrdHead["OTB_EOW_DATE"] = orderHead.OtbEowDate;
            recordObjOrdHead["EARLIEST_SHIP_DATE"] = orderHead.EarliestShipDate;
            recordObjOrdHead["LATEST_SHIP_DATE"] = orderHead.LatestShipDate;
            ////recordObjOrdHead["CLOSE_DATE"] = orderHead.CloseDate;
            recordObjOrdHead["TERMS"] = orderHead.Terms;
            recordObjOrdHead["FREIGHT_TERMS"] = orderHead.FreightTerms;
            recordObjOrdHead["CUST_ORDER"] = orderHead.CustOrder;
            recordObjOrdHead["PAYMENT_METHOD"] = orderHead.PaymentMethod;
            ////recordObjOrdHead["BACKHAUL_TYPE"] = orderHead.BackhaulType;
            ////recordObjOrdHead["BACKHAUL_ALLOWANCE"] = orderHead.BackhaulAllowance;
            recordObjOrdHead["SHIP_METHOD"] = orderHead.ShipMethod;
            ////recordObjOrdHead["PURCHASE_TYPE"] = orderHead.PurchaseType;
            //// recordObjOrdHead["STATUS"] = orderHead.Status;
            ////recordObjOrdHead["ORIG_APPROVAL_DATE"] = orderHead.OrigApprovalDate;
            ////recordObjOrdHead["ORIG_APPROVAL_ID"] = orderHead.OrigApprovalId;
            recordObjOrdHead["SHIP_PAY_METHOD"] = orderHead.ShipPayMethod;
            ////recordObjOrdHead["FOB_TRANS_RES"] = orderHead.FobTransRes;
            ////recordObjOrdHead["FOB_TRANS_RES_DESC"] = orderHead.FobTransResDesc;
            ////recordObjOrdHead["FOB_TITLE_PASS"] = orderHead.FobTitlePass;
            ////recordObjOrdHead["FOB_TITLE_PASS_DESC"] = orderHead.FobTitlePassDesc;
            ////recordObjOrdHead["EDI_SENT_IND"] = orderHead.EdiSentInd;
            ////recordObjOrdHead["EDI_PO_IND"] = orderHead.EdiPoInd;
            recordObjOrdHead["IMPORT_ORDER_IND"] = orderHead.ImportOrderInd;
            recordObjOrdHead["IMPORT_COUNTRY_ID"] = orderHead.ImportCountryId;
            ////recordObjOrdHead["PO_ACK_RECVD_IND"] = orderHead.PoAckRecvdInd;
            ////recordObjOrdHead["INCLUDE_ON_ORDER_IND"] = orderHead.IncludeOnOrderInd;
            recordObjOrdHead["VENDOR_ORDER_NO"] = orderHead.VendorOrderNo;
            recordObjOrdHead["EXCHANGE_RATE"] = orderHead.ExchangeRate;
            ////recordObjOrdHead["FACTORY"] = orderHead.Factory;
            ////recordObjOrdHead["AGENT"] = orderHead.Agent;
            ////recordObjOrdHead["DISCHARGE_PORT"] = orderHead.DischargePort;
            ////recordObjOrdHead["LADING_PORT"] = orderHead.LadingPort;
            recordObjOrdHead["BILL_TO_ID"] = orderHead.BillToId;
            ////recordObjOrdHead["FREIGHT_CONTRACT_NO"] = orderHead.FreightContractNo;
            recordObjOrdHead["PO_TYPE"] = orderHead.PoType;
            ////recordObjOrdHead["PRE_MARK_IND"] = orderHead.PreMarkInd;
            recordObjOrdHead["CURRENCY_CODE"] = orderHead.CurrencyCode;
            ////recordObjOrdHead["REJECT_CODE"] = orderHead.RejectCode;
            ////recordObjOrdHead["CONTRACT_NO"] = orderHead.ContractNo;
            ////recordObjOrdHead["LAST_SENT_REV_NO"] = orderHead.LastSentRevNo;
            ////recordObjOrdHead["SPLIT_REF_ORDNO"] = orderHead.SplitRefOrdno;
            ////recordObjOrdHead["PICKUP_LOC"] = orderHead.PickupLoc;
            ////recordObjOrdHead["PICKUP_NO"] = orderHead.PickupNo;
            recordObjOrdHead["PICKUP_DATE"] = orderHead.PickupDate;
            ////recordObjOrdHead["APP_DATETIME"] = orderHead.AppDatetime;
            recordObjOrdHead["COMMENT_DESC"] = orderHead.CommentDesc;
            ////recordObjOrdHead["PARTNER_TYPE_1"] = orderHead.PartnerType1;
            ////recordObjOrdHead["PARTNER1"] = orderHead.Partner1;
            ////recordObjOrdHead["PARTNER_TYPE_2"] = orderHead.PartnerType2;
            ////recordObjOrdHead["PARTNER2"] = orderHead.Partner2;
            ////recordObjOrdHead["PARTNER_TYPE_3"] = orderHead.PartnerType3;
            ////recordObjOrdHead["PARTNER3"] = orderHead.Partner3;
            ////recordObjOrdHead["ITEM"] = orderHead.Item;
            ////recordObjOrdHead["SUPP_RMT_ADD_SEQ_NO"] = orderHead.SuppRmtAddSeqNo;
            //// recordObjOrdHead["OPERATION"] = orderHead.Operation;
            oracleTableOrdHead.Add(recordObjOrdHead);
            return oracleTableOrdHead;
        }

        private OracleTable OrdLocBuilder(OrderDtlObjectModel orderDtlObjectModel, int? orderNo)
        {
            // Ord Loc Object
            OracleTable oracleTableOrdLoc = this.GetOracleTable(this.GetObjectType(OrderMgmtConstants.ObjTypeOrdLoc));
            var recordObjOrdLoc = this.GetOracleObject(this.GetObjectType(OrderMgmtConstants.RecordObjOrdLoc));
            recordObjOrdLoc["ITEMS"] = orderDtlObjectModel.TblItems;
            recordObjOrdLoc["LOCATIONS"] = orderDtlObjectModel.TblLocs;
            recordObjOrdLoc["LOC_TYPES"] = orderDtlObjectModel.TblLocTypes;
            recordObjOrdLoc["UNIT_RETAILS"] = orderDtlObjectModel.TblUnitRetails;
            recordObjOrdLoc["ORDERED_QTYS"] = orderDtlObjectModel.TblOrderedQtys;
            recordObjOrdLoc["PRESCALED_QTYS"] = orderDtlObjectModel.TblPrescaledQtys;
            ////recordObjOrdLoc["CANCELLED_QTYS"] = orderDtlObjectModel.TblCancelledQtys;
            ////recordObjOrdLoc["CANCEL_CODES"] = orderDtlObjectModel.TblCancelCodes;
            ////recordObjOrdLoc["CANCEL_DATES"] = orderDtlObjectModel.TblCancelDates;
            ////recordObjOrdLoc["CANCEL_IDS"] = orderDtlObjectModel.TblCancelIds;
            recordObjOrdLoc["UNIT_COSTS"] = orderDtlObjectModel.TblUnitCosts;
            recordObjOrdLoc["COST_SOURCES"] = orderDtlObjectModel.TblCostSources;
            recordObjOrdLoc["NON_SCALE_INDS"] = orderDtlObjectModel.TblNonscaleInds;
            recordObjOrdLoc["ESTIMATED_INSTOCK_DATE"] = orderDtlObjectModel.TblEstimatedInstockDates;
            recordObjOrdLoc["ORDER_NO"] = orderNo;
            oracleTableOrdLoc.Add(recordObjOrdLoc);
            return oracleTableOrdLoc;
        }

        private OracleTable OrdSkuBuilder(OrderDtlObjectModel orderDtlObjectModel, int? orderNo)
        {
            // Ord Sku Object
            OracleTable oracleTableOrdSku = this.GetOracleTable(this.GetObjectType(OrderMgmtConstants.ObjTypeOrdSku));
            var recordObjOrdSku = this.GetOracleObject(this.GetObjectType(OrderMgmtConstants.RecordObjOrdSku));
            recordObjOrdSku["ITEMS"] = orderDtlObjectModel.TblItems;
            recordObjOrdSku["REF_ITEMS"] = orderDtlObjectModel.TblItems;
            recordObjOrdSku["ORIGIN_COUNTRY_IDS"] = orderDtlObjectModel.TblOriginCountryIds;
            recordObjOrdSku["EARLIEST_SHIP_DATES"] = orderDtlObjectModel.TblEarliestShipDates;
            recordObjOrdSku["LATEST_SHIP_DATES"] = orderDtlObjectModel.TblLatestShipDates;
            recordObjOrdSku["NON_SCALE_INDS"] = orderDtlObjectModel.TblNonscaleInds;
            recordObjOrdSku["SUPP_PACK_SIZES"] = orderDtlObjectModel.TblSuppPackSizes;
            recordObjOrdSku["ORDER_NO"] = orderNo;
            oracleTableOrdSku.Add(recordObjOrdSku);
            return oracleTableOrdSku;
        }
    }
}
