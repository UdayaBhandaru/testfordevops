﻿// <copyright file="OrderDetailModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.OrderManagement.Order.Models
{
    using System;
    using System.Collections.Generic;
    using Agility.Framework.Core.Profile.Entities;

    public class OrderDetailModel : ProfileEntity
    {
        public string Item { get; set; }

        public decimal? UnitRetail { get; set; }

        public decimal? QtyOrdered { get; set; }

        public decimal? PrescaledQty { get; set; }

        public decimal? CancelledQty { get; set; }

        public string CancelCode { get; set; }

        public DateTime? CancelDate { get; set; }

        public string CancelId { get; set; }

        public decimal? UnitCost { get; set; }

        public string CostSource { get; set; }

        public string NonScaleInd { get; set; }

        public DateTime? EstimatedInstockDate { get; set; }

        public string ProgramMessage { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }

        public decimal? QtyReceived { get; set; }

        public decimal? LastReceived { get; set; }

        public decimal? LastRoundedQty { get; set; }

        public decimal? LastGrpRoundedQty { get; set; }

        public decimal? QtyCancelled { get; set; }

        public decimal? OriginalReplQty { get; set; }

        public decimal? UnitCostInit { get; set; }

        public int? TsfPoLinkNo { get; set; }

        public string OriginCountryId { get; set; }

        public string ItemDesc { get; set; }

        public decimal? DefaultUnitSize { get; set; }

        public decimal? TotalCost
        {
            get
            {
                return this.QtyOrdered * this.UnitCostInit;
            }
        }
    }
}