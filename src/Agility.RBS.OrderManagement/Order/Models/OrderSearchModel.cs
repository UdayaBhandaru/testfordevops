﻿// <copyright file="OrderSearchModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.OrderManagement.Order.Models
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Core.Models;

    public class OrderSearchModel : ProfileEntity, Core.IPaginationModel
    {
        [NotMapped]
        public int? Supplier { get; set; }

        [NotMapped]
        public string SupName { get; set; }

        [NotMapped]
        public int? DeptId { get; set; }

        [NotMapped]
        public string DeptDesc { get; set; }

        [NotMapped]
        public string LocType { get; set; }

        [NotMapped]
        public string LocTypeDesc { get; set; }

        [NotMapped]
        public int? Location { get; set; }

        [NotMapped]
        public string LocationName { get; set; }

        [NotMapped]
        public string ItemBarcode { get; set; }

        [NotMapped]
        public string ItemDesc { get; set; }

        [Column("ORDER_NO")]
        public int? ORDER_NO { get; set; }

        [NotMapped]
        public string OrderType { get; set; }

        [NotMapped]
        public string OrderTypeDesc { get; set; }

        [NotMapped]
        public string OrderDesc { get; set; }

        [NotMapped]
        public string Status { get; set; }

        [NotMapped]
        public string StatusDesc { get; set; }

        [NotMapped]
        public string DateType { get; set; }

        [NotMapped]
        public DateTime? StartDate { get; set; }

        [NotMapped]
        public DateTime? EndDate { get; set; }

        [NotMapped]
        public string Operation { get; set; }

        [NotMapped]
        public string TableName { get; set; }

        [NotMapped]
        public int? StartRow { get; set; }

        [NotMapped]
        public int? EndRow { get; set; }

        [NotMapped]
        public long? TotalRows { get; set; }

        [NotMapped]
        public SortModel[] SortModel { get; set; }
    }
}
