﻿// <copyright file="OrdHeadWfModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.OrderManagement.Order.Models
{
    using System.ComponentModel.DataAnnotations;
    using Agility.Framework.Core.Workflow.Entities;

    public class OrdHeadWfModel : WorkFlowBaseInstance, IWorkFlowInstance
    {
        [Key]
        public int WorkflowInstanceId { get; set; }
    }
}
