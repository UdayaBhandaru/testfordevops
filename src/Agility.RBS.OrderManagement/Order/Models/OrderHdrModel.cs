﻿// <copyright file="OrderHdrModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.OrderManagement.Order.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Inbox.Common;

    [DataProfile(Name = "ORDHEAD")]
    public class OrderHdrModel : ProfileEntity<OrdHeadWfModel>, IInboxCommonModel
    {
        public OrderHdrModel()
        {
            this.OrderDetails = new List<OrderDetailModel>();
        }

        [Column("ORDER_NO")]
        public int? ORDER_NO { get; set; }

        [NotMapped]
        public string OrderType { get; set; }

        [NotMapped]
        public int? Dept { get; set; }

        [NotMapped]
        public string Buyer { get; set; }

        [NotMapped]
        public string BuyerName { get; set; }

        [NotMapped]
        public int? Supplier { get; set; }

        [NotMapped]
        public string SupplierName { get; set; }

        [NotMapped]
        public int? SuppAddSeqNo { get; set; }

        [NotMapped]
        public string LocType { get; set; }

        [NotMapped]
        public int? Location { get; set; }

        [NotMapped]
        public int? Promotion { get; set; }

        [NotMapped]
        public string QcInd { get; set; }

        [NotMapped]
        public DateTime? WrittenDate { get; set; }

        [NotMapped]
        public DateTime? NotBeforeDate { get; set; }

        [NotMapped]
        public DateTime? NotAfterDate { get; set; }

        [NotMapped]
        public DateTime? OtbEowDate { get; set; }

        [NotMapped]
        public DateTime? EarliestShipDate { get; set; }

        [NotMapped]
        public DateTime? LatestShipDate { get; set; }

        [NotMapped]
        public DateTime? CloseDate { get; set; }

        [NotMapped]
        public string Terms { get; set; }

        [NotMapped]
        public string TermsDesc { get; set; }

        [NotMapped]
        public string FreightTerms { get; set; }

        [NotMapped]
        public string FreightDesc { get; set; }

        [NotMapped]
        public string OrigInd { get; set; }

        [NotMapped]
        public string CustOrder { get; set; }

        [NotMapped]
        public string PaymentMethod { get; set; }

        [NotMapped]
        public string PayMethodName { get; set; }

        [NotMapped]
        public string BackhaulType { get; set; }

        [NotMapped]
        public decimal? BackhaulAllowance { get; set; }

        [NotMapped]
        public string ShipMethod { get; set; }

        [NotMapped]
        public string ShipmentDesc { get; set; }

        [NotMapped]
        public string PurchaseType { get; set; }

        [NotMapped]
        public string Status { get; set; }

        [NotMapped]
        public DateTime? OrigApprovalDate { get; set; }

        [NotMapped]
        public string OrigApprovalId { get; set; }

        [NotMapped]
        public string ShipPayMethod { get; set; }

        [NotMapped]
        public string FobTransRes { get; set; }

        [NotMapped]
        public string FobTransResDesc { get; set; }

        [NotMapped]
        public string FobTitlePass { get; set; }

        [NotMapped]
        public string FobTitlePassDesc { get; set; }

        [NotMapped]
        public string EdiSentInd { get; set; }

        [NotMapped]
        public string EdiPoInd { get; set; }

        [NotMapped]
        public string ImportOrderInd { get; set; }

        [NotMapped]
        public string ImportCountryId { get; set; }

        [NotMapped]
        public string PoAckRecvdInd { get; set; }

        [NotMapped]
        public string IncludeOnOrderInd { get; set; }

        [NotMapped]
        public string VendorOrderNo { get; set; }

        [NotMapped]
        public decimal? ExchangeRate { get; set; }

        [NotMapped]
        public string Factory { get; set; }

        [NotMapped]
        public string Agent { get; set; }

        [NotMapped]
        public string DischargePort { get; set; }

        [NotMapped]
        public string LadingPort { get; set; }

        [NotMapped]
        public string BillToId { get; set; }

        [NotMapped]
        public string FreightContractNo { get; set; }

        [NotMapped]
        public string PoType { get; set; }

        [NotMapped]
        public string PreMarkInd { get; set; }

        [NotMapped]
        public string CurrencyCode { get; set; }

        [NotMapped]
        public string RejectCode { get; set; }

        [NotMapped]
        public int? ContractNo { get; set; }

        [NotMapped]
        public int? LastSentRevNo { get; set; }

        [NotMapped]
        public int? SplitRefOrdno { get; set; }

        [NotMapped]
        public string PickupLoc { get; set; }

        [NotMapped]
        public string PickupNo { get; set; }

        [NotMapped]
        public DateTime? PickupDate { get; set; }

        [NotMapped]
        public DateTime? AppDatetime { get; set; }

        [NotMapped]
        public string CommentDesc { get; set; }

        [NotMapped]
        public string PartnerType1 { get; set; }

        [NotMapped]
        public string Partner1 { get; set; }

        [NotMapped]
        public string PartnerType2 { get; set; }

        [NotMapped]
        public string Partner2 { get; set; }

        [NotMapped]
        public string PartnerType3 { get; set; }

        [NotMapped]
        public string Partner3 { get; set; }

        [NotMapped]
        public string Item { get; set; }

        [NotMapped]
        public int? SuppRmtAddSeqNo { get; set; }

        [NotMapped]
        public string Operation { get; set; }

        [NotMapped]
        [Column("ORGANISATION_ID")]
        public new int? OrganizationId { get; set; }

        [NotMapped]
        [Column("CREATED_BY")]
        public new string CreatedBy { get; set; }

        [NotMapped]
        [Column("LAST_UPDATED_BY")]
        public new string ModifiedBy { get; set; }

        [NotMapped]
        [Column("DeletedInd")]
        public new bool? DeletedInd { get; set; }

        [NotMapped]
        [Column("CREATED_DATE")]
        public new DateTimeOffset? CreatedDate { get; set; }

        [NotMapped]
        [Column("LAST_UPDATED_DATE")]
        public new DateTimeOffset? ModifiedDate { get; set; }

        [NotMapped]
        public int? WfNxtStateInd { get; set; }

        [NotMapped]
        public int? LineItemCnt { get; set; }

        [NotMapped]
        public Core.Models.RbWfHeaderModel WorkflowForm { get; set; }

        [NotMapped]
        public int? FileCount { get; set; }

        [NotMapped]
        public List<OrderDetailModel> OrderDetails { get; private set; }
    }
}
