﻿// <copyright file="OrderDtlObjectModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.OrderManagement.Order.Models
{
    using System;
    using System.Collections.Generic;
    using Agility.Framework.Core.Profile.Entities;
    using Devart.Data.Oracle;

    public class OrderDtlObjectModel
    {
        public OracleTable TblItems { get; internal set; }

        public OracleTable TblLocs { get; internal set; }

        public OracleTable TblLocTypes { get; internal set; }

        public OracleTable TblUnitRetails { get; internal set; }

        public OracleTable TblOrderedQtys { get; internal set; }

        public OracleTable TblPrescaledQtys { get; internal set; }

        public OracleTable TblCancelledQtys { get; internal set; }

        public OracleTable TblCancelCodes { get; internal set; }

        public OracleTable TblCancelDates { get; internal set; }

        public OracleTable TblCancelIds { get; internal set; }

        public OracleTable TblUnitCosts { get; internal set; }

        public OracleTable TblCostSources { get; internal set; }

        public OracleTable TblNonscaleInds { get; internal set; }

        public OracleTable TblEstimatedInstockDates { get; internal set; }

        public OracleTable TblOriginCountryIds { get; internal set; }

        public OracleTable TblEarliestShipDates { get; internal set; }

        public OracleTable TblLatestShipDates { get; internal set; }

        public OracleTable TblSuppPackSizes { get; internal set; }
        //// OracleTable TblOperation { get; internal set; }
    }
}