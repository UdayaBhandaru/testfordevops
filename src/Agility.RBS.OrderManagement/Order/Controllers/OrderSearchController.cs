﻿// <copyright file="OrderSearchController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.OrderManagement.Order.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core.Constants;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.OrderManagement.Order.Models;
    using Agility.RBS.OrderManagement.Order.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    [Route("api/[controller]/[action]")]
    public class OrderSearchController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly ServiceDocument<OrderSearchModel> serviceDocument;
        private readonly OrderRepository orderRepository;

        public OrderSearchController(
            ServiceDocument<OrderSearchModel> serviceDocument,
            OrderRepository orderRepository,
        DomainDataRepository domainDataRepository)
        {
            this.serviceDocument = serviceDocument;
            this.orderRepository = orderRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in the Order List page.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<OrderSearchModel>> List()
        {
            this.serviceDocument.DomainData.Add("department", this.domainDataRepository.DepartmentDomainGet().Result);
            this.serviceDocument.DomainData.Add("location", this.domainDataRepository.LocationDomainGet().Result);
            this.serviceDocument.DomainData.Add("orderType", this.domainDataRepository.DomainDetailData(OrderMgmtConstants.DomainHdrOrderType).Result);
            this.serviceDocument.DomainData.Add(
                "workflowStatusList", (await this.domainDataRepository.WorkflowStatusGet(RbCommonConstants.OrderModule)).OrderBy(x => x.Name));
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for fetching Order records based on search criteria when Search button clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<OrderSearchModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.OrderSearch);
            return this.serviceDocument;
        }

        private async Task<List<OrderSearchModel>> OrderSearch()
        {
            try
            {
                var orders = await this.orderRepository.OrderListGet(this.serviceDocument, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return orders;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }
    }
}
