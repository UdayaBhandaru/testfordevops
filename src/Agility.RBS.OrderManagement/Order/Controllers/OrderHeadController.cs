﻿// <copyright file="OrderHeadController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.OrderManagement.Order.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Models;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Constants;
    using Agility.RBS.Inbox.Common;
    using Agility.RBS.Inbox.Models;
    using Agility.RBS.Inbox.Repository;
    using Agility.RBS.ItemManagement.Item.Models;
    using Agility.RBS.ItemManagement.Item.Repositories;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Models;
    using Agility.RBS.OrderManagement.Common;
    using Agility.RBS.OrderManagement.Order.Models;
    using Agility.RBS.OrderManagement.Order.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    [Route("api/[controller]/[action]")]
    public class OrderHeadController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly OrderRepository orderRepository;
        private readonly InboxRepository inboxRepository;
        private readonly WrapperRepository wrapperRepository;
        private readonly ItemSelectRepository itemSelectRepository;
        private readonly InboxModel inboxModel;
        private ServiceDocument<OrderHdrModel> serviceDocument;
        private int orderNo;

        public OrderHeadController(
            ServiceDocument<OrderHdrModel> serviceDocument,
            OrderRepository orderRepository,
        DomainDataRepository domainDataRepository,
        WrapperRepository wrapperRepository,
        ItemSelectRepository itemSelectRepository,
        InboxRepository inboxRepository)
        {
            this.serviceDocument = serviceDocument;
            this.orderRepository = orderRepository;
            this.domainDataRepository = domainDataRepository;
            this.wrapperRepository = wrapperRepository;
            this.inboxRepository = inboxRepository;
            this.itemSelectRepository = itemSelectRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
            this.inboxModel = new InboxModel { Operation = "CHGS", ModuleName = "ORDER" };
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in Order Head page when Add button clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<OrderHdrModel>> New()
        {
            this.serviceDocument.New(false);
            return await this.OrderDomainData();
        }

        /// <summary>
        /// This API invoked for populating fields in the Order Head page when Edit icon clicked.
        /// </summary>
        /// <param name="id">Order Id</param>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<OrderHdrModel>> Open(int id)
        {
            this.orderNo = id;
            await this.serviceDocument.OpenAsync(this.OrderHeadOpen);
            this.serviceDocument.DomainData.Add("supplier", this.domainDataRepository.SupplierDomainGet(this.serviceDocument.DataProfile.DataModel.Supplier.Value).Result);
            return await this.OrderDomainData();
        }

        /// <summary>
        /// This API invoked for saving field values in the Order Head page when Save button clicked.
        /// </summary>
        /// <param name="serviceDocument">Service Document</param>
        /// <returns>ServiceDocument</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<OrderHdrModel>> Save([FromBody]ServiceDocument<OrderHdrModel> serviceDocument)
        {
            await this.serviceDocument.SaveAsync(this.OrderHeadSave);
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for submitting Order to next level when Submit button clicked.
        /// </summary>
        /// <param name="serviceDocument">Service Document</param>
        /// <returns>ServiceDocument</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<OrderHdrModel>> Submit([FromBody]ServiceDocument<OrderHdrModel> serviceDocument)
        {
            this.serviceDocument = serviceDocument;
            if (this.serviceDocument.DataProfile.DataModel.ORDER_NO == 0 || this.serviceDocument.DataProfile.DataModel.ORDER_NO == null)
            {
                this.inboxModel.Operation = "DFT";
            }

            this.inboxModel.StateID = RbsServiceDocCustomizationHelper.GetStateIdDotNetToOracle(this.serviceDocument);
            await this.serviceDocument.TransitAsync(this.OrderHeadSave);
            if (this.serviceDocument.Result.Type == MessageType.Success)
            {
                await this.inboxRepository.SetInbox(this.inboxModel, this.serviceDocument);
            }

            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for rejecting Order to previous level when Reject button clicked.
        /// </summary>
        /// <param name="serviceDocument">Service Document</param>
        /// <returns>ServiceDocument</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<OrderHdrModel>> Reject([FromBody]ServiceDocument<OrderHdrModel> serviceDocument)
        {
            this.SetServiceDocumentAndInboxStateId(serviceDocument);
            await this.serviceDocument.TransitAsync(GuidConverter.DotNetToOracle(serviceDocument.DataProfile.DataModel.WorkflowForm.StateId), true);
            if (this.serviceDocument.Result.Type == MessageType.Success)
            {
                this.inboxModel.Operation = "REJ";
                this.inboxModel.ActionType = "R";
                this.inboxModel.ToStateID = GuidConverter.DotNetToOracle(this.serviceDocument.DataProfile.DataModel.WorkflowStateId.ToString());
                this.inboxRepository.SetInbox(this.inboxModel, this.serviceDocument);
            }

            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for rejecting Order to previous level when Send Back For Review button clicked.
        /// </summary>
        /// <param name="serviceDocument">Service Document</param>
        /// <returns>ServiceDocument</returns>
        /// <response code="401">Un Authorized</response>
        [AllowAnonymous]
        [HttpPost]
        public async Task<ServiceDocument<OrderHdrModel>> SendBackForReview([FromBody]ServiceDocument<OrderHdrModel> serviceDocument)
        {
            var workFlowInstance = serviceDocument.DataProfile.DataModel.WorkflowInstance;
            string toStateId = this.inboxRepository.GetStateId("SBR", workFlowInstance);
            toStateId = GuidConverter.OracleToDotNet(toStateId);
            await this.serviceDocument.TransitAsync(toStateId, true);

            if (this.serviceDocument.Result.Type == MessageType.Success)
            {
                await this.inboxRepository.SetInbox(
                    new InboxModel
                    {
                        Operation = "SBR",
                        ActionType = "B",
                        ToStateID = GuidConverter.DotNetToOracle(workFlowInstance.WorkflowStateId.ToString()),
                        StateID = GuidConverter.DotNetToOracle(toStateId)
                    },
                    this.serviceDocument);
            }

            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for approving order when Approve button clicked.
        /// </summary>
        /// <param name="serviceDocument">Service Document</param>
        /// <returns>ServiceDocument</returns>
        /// <response code="401">Un Authorized</response>
        [AllowAnonymous]
        [HttpPost]
        public async Task<ServiceDocument<OrderHdrModel>> Approve([FromBody]ServiceDocument<OrderHdrModel> serviceDocument)
        {
            this.orderNo = Convert.ToInt32(serviceDocument.DataProfile.DataModel.ORDER_NO);
            var openedSdoc = await this.serviceDocument.OpenAsync(this.OrderHeadOpen);
            try
            {
                WorkflowAction wfApproveAction = openedSdoc.DataProfile.ActionService.AllowedActions.FirstOrDefault(a => a.ActionName == "Approve");
                if (wfApproveAction != null)
                {
                    openedSdoc.DataProfile.ActionService.CurrentActionId = wfApproveAction.TransitionClaim;
                    await openedSdoc.TransitAsync(this.OrderHeadSave);
                    await this.inboxRepository.SetInbox(this.inboxModel, openedSdoc);
                }
            }
            catch (Exception e)
            {
                openedSdoc.Result = new ServiceDocumentResult
                {
                    InnerException = string.Empty,
                    StackTrace = e.StackTrace,
                    Type = MessageType.Exception
                };
                return this.serviceDocument;
            }

            return openedSdoc;
        }

        /// <summary>
        /// This API invoked for populating dependent fields based on selected Supplier.
        /// </summary>
        /// <param name="supplier">Supplier Id</param>
        /// <returns>List Of SupplierDetailDomainModel</returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<List<SupplierDetailDomainModel>> SupplerDetailsGet(int supplier)
        {
            return await this.domainDataRepository.SupplierDetailsGet(supplier);
        }

        /// <summary>
        /// This API invoked for populating dependent fields Exchange Rate based on Item.
        /// </summary>
        /// <param name="supplier">Supplier</param>
        /// <param name="itemBarcode">Item Barcode</param>
        /// <param name="dept">Department Id</param>
        /// <param name="location">Location</param>
        /// <returns>Exchange Rate</returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<AjaxModel<List<OrderItemSelectModel>>> OrderItemGet(int supplier, string itemBarcode, int dept, int location)
        {
            try
            {
                return await this.itemSelectRepository.SelectOrderItem(new OrderItemSelectModel { Supplier = supplier, ItemBarcode = itemBarcode, DeptId = dept, LocationId = location });
            }
            catch (Exception ex)
            {
                return new AjaxModel<List<OrderItemSelectModel>> { Result = AjaxResult.Exception, Message = ex.Message, Model = null };
            }
        }

        private async Task<OrderHdrModel> OrderHeadOpen()
        {
            try
            {
                var orderHead = await this.orderRepository.OrderHeadGet(this.orderNo, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return orderHead;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> OrderHeadSave()
        {
            this.serviceDocument.Result = this.serviceDocument.DataProfile.DataModel.ORDER_NO == null
                ? await this.wrapperRepository.OrderHeadSet(this.serviceDocument.DataProfile.DataModel)
                : await this.wrapperRepository.OrderHeadSetUpdate(this.serviceDocument.DataProfile.DataModel);

            if (this.serviceDocument.Result.Type != MessageType.Success)
            {
                return false;
            }

            this.serviceDocument.DataProfile.DataModel.ORDER_NO = this.wrapperRepository.OrderNo;
            return true;
        }

        private async Task<ServiceDocument<OrderHdrModel>> OrderDomainData()
        {
            this.serviceDocument.DomainData.Add("orderType", this.domainDataRepository.DomainDetailData(OrderMgmtConstants.DomainHdrOrderType).Result);
            this.serviceDocument.DomainData.Add("locType", this.domainDataRepository.DomainDetailData(OrderMgmtConstants.DomainHdrLocType).Result);
            this.serviceDocument.DomainData.Add("location", this.domainDataRepository.LocationDomainGet().Result);
            this.serviceDocument.DomainData.Add("department", this.domainDataRepository.DepartmentDomainGet().Result);
            this.serviceDocument.DomainData.Add("indicator", this.domainDataRepository.DomainDetailData(OrderMgmtConstants.DomainHdrYsno).Result);
            this.serviceDocument.DomainData.Add("purchaseType", this.domainDataRepository.DomainDetailData(OrderMgmtConstants.DomainHdrPurchaseTyp).Result);
            this.serviceDocument.DomainData.Add("country", this.domainDataRepository.CountryDomainGet().Result);
            this.serviceDocument.DomainData.Add("partnerType", this.domainDataRepository.DomainDetailData(OrderMgmtConstants.DomainHdrPartnerTyp).Result);
            this.serviceDocument.DomainData.Add("poType", this.domainDataRepository.PoTypeGet().Result);
            this.serviceDocument.DomainData.Add("orderOrigin", this.domainDataRepository.DomainDetailData(OrderMgmtConstants.DomainHdrOrderOrigin).Result);
            this.serviceDocument.DomainData.Add("rejectCode", this.domainDataRepository.DomainDetailData(OrderMgmtConstants.DomainHdrReject).Result);
            this.serviceDocument.DomainData.Add("billtoid", this.domainDataRepository.OutLocationDomainGet().Result);
            this.serviceDocument.DomainData.Add("costsrc", this.domainDataRepository.DomainDetailData(OrderMgmtConstants.DomainHdrCostSource).Result);
            this.serviceDocument.LocalizationData.AddData("OrderHead");
            return this.serviceDocument;
        }

        private void SetServiceDocumentAndInboxStateId(ServiceDocument<OrderHdrModel> serviceDocument)
        {
            this.serviceDocument = serviceDocument;
            this.inboxModel.StateID = RbsServiceDocCustomizationHelper.GetStateIdDotNetToOracle(serviceDocument);
        }
    }
}
