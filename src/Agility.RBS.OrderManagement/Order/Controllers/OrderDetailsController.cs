﻿// <copyright file="OrderDetailsController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.OrderManagement.Order.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Documents.Models;
    using Agility.RBS.ItemManagement.Item;
    using Agility.RBS.ItemManagement.Item.Models;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.OrderManagement;
    using Agility.RBS.OrderManagement.Order.Models;
    using Agility.RBS.OrderManagement.Order.Repositories;
    using ExcelDataReader;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [Route("api/[controller]/[action]")]
    public class OrderDetailsController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly ServiceDocument<OrderDetailModel> serviceDocument;
        private readonly OrderRepository orderRepository;
        private int orderNo;

        public OrderDetailsController(
            ServiceDocument<OrderDetailModel> serviceDocument,
            OrderRepository orderRepository,
        DomainDataRepository domainDataRepository)
        {
            this.serviceDocument = serviceDocument;
            this.orderRepository = orderRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        /// <summary>
        /// This API invoked for loading grid data along with data for autocomplete, radio button fields in Order Details Page.
        /// </summary>
        /// <param name="id">Order Id</param>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<OrderDetailModel>> List(int id)
        {
            this.orderNo = id;
            this.serviceDocument.DomainData.Add("locType", this.domainDataRepository.DomainDetailData(OrderMgmtConstants.DomainHdrLocType).Result);
            this.serviceDocument.DomainData.Add("costSrc", this.domainDataRepository.DomainDetailData(OrderMgmtConstants.DomainHdrCostSource).Result);
            this.serviceDocument.DomainData.Add("location", this.domainDataRepository.LocationDomainGet().Result);
            this.serviceDocument.DomainData.Add("indicator", this.domainDataRepository.DomainDetailData(OrderMgmtConstants.DomainHdrIndicator).Result);
            this.serviceDocument.LocalizationData.AddData("OrderDetails");
            await this.serviceDocument.ToListAsync(this.OrderDetailsList);
            return this.serviceDocument;
        }

        private async Task<List<OrderDetailModel>> OrderDetailsList()
        {
            try
            {
                var costChangeDetails = await this.orderRepository.OrderDetailsGet(this.orderNo, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return costChangeDetails;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }
    }
}
