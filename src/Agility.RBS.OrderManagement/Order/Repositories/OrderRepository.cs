﻿// <copyright file="OrderRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.OrderManagement.Order.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.OrderManagement.Order.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class OrderRepository : BaseOraclePackage
    {
        public OrderRepository()
        {
            this.PackageName = OrderMgmtConstants.GetWrapperPackage;
        }

        public int? OrderNo { get; private set; }

        public async Task<List<OrderSearchModel>> OrderListGet(ServiceDocument<OrderSearchModel> serviceDoc, ServiceDocumentResult serviceDocumentResult)
        {
            OrderSearchModel orderSearch = serviceDoc.DataProfile.DataModel;
            PackageParams packageParameter = this.GetPackageParams(OrderMgmtConstants.ObjTypeOrderSearch, OrderMgmtConstants.GetProcOrderList);
            OracleObject recordObject = this.SetParamsOrderList(orderSearch);

            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();

            BaseOracleParameter baseOracleParameter;
            baseOracleParameter = new BaseOracleParameter { Name = "P_START", DataType = OracleDbType.Number, Direction = ParameterDirection.Input, Value = Convert.ToString(orderSearch.StartRow) };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            baseOracleParameter = new BaseOracleParameter { Name = "P_END", DataType = OracleDbType.Number, Direction = ParameterDirection.Input, Value = Convert.ToString(orderSearch.EndRow) };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            if (orderSearch.SortModel == null)
            {
                orderSearch.SortModel = new[] { new Core.Models.SortModel { ColId = null, Sort = null } };
            }

            baseOracleParameter = new BaseOracleParameter { Name = "P_SortColId", DataType = OracleDbType.VarChar, Direction = ParameterDirection.Input, Value = orderSearch.SortModel[0].ColId };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            baseOracleParameter = new BaseOracleParameter { Name = "P_SortOrder", DataType = OracleDbType.VarChar, Direction = ParameterDirection.Input, Value = orderSearch.SortModel[0].Sort };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            baseOracleParameter = new BaseOracleParameter { Name = "P_total", DataType = OracleDbType.Number, Direction = ParameterDirection.Output };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            parameters.Add(this.ParameterBuilder(UserProfileHelper.GetUserParameter()));
            var orders = await this.GetProcedure2<OrderSearchModel>(recordObject, packageParameter, serviceDocumentResult, parameters);
            orderSearch.TotalRows = (this.Parameters["p_total"].Value != null && this.Parameters["p_total"].Value != DBNull.Value) ? Convert.ToInt64(this.Parameters["p_total"].Value) : 1;
            serviceDoc.DataProfile.DataModel = orderSearch;
            return orders;
        }

        public async Task<OrderHdrModel> OrderHeadGet(long orderNo, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(OrderMgmtConstants.RecordObjOrderHeader, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["ORDER_NO"]] = orderNo;
            PackageParams packageParameter = this.GetPackageParams(OrderMgmtConstants.ObjTypeOrderHeader, OrderMgmtConstants.GetProcOrderHead);
            OracleTable pDomainMstTab = await this.GetProcedure(recordObject, packageParameter, serviceDocumentResult);
            List<OrderHdrModel> orderHeads = Mapper.Map<List<OrderHdrModel>>(pDomainMstTab);
            if (orderHeads != null)
            {
                OracleObject obj = (OracleObject)pDomainMstTab[0];
                OracleTable domainDetails = (Devart.Data.Oracle.OracleTable)obj["ORDERLOC"];
                if (domainDetails.Count > 0)
                {
                    orderHeads[0].OrderDetails.AddRange(Mapper.Map<List<OrderDetailModel>>(domainDetails));
                }
            }

            return orderHeads != null ? orderHeads[0] : null;
        }

        public async Task<List<OrderDetailModel>> OrderDetailsGet(int orderNo, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(OrderMgmtConstants.RecordObjOrderDetail, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["ORDER_NO"]] = orderNo;
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            parameters.Add(this.ParameterBuilder(UserProfileHelper.GetUserParameter()));
            PackageParams packageParameter = this.GetPackageParams(OrderMgmtConstants.ObjTypeOrderDetail, OrderMgmtConstants.GetProcOrderDetail);
            return await this.GetProcedure2<OrderDetailModel>(recordObject, packageParameter, serviceDocumentResult, parameters);
        }

        private OracleObject SetParamsOrderList(OrderSearchModel orderSearch)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(OrderMgmtConstants.RecordObjOrderSearch, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);

            recordObject[recordType.Attributes["SUPPLIER"]] = orderSearch.Supplier;
            recordObject[recordType.Attributes["DEPT_ID"]] = orderSearch.DeptId;
            recordObject[recordType.Attributes["LOCATION"]] = orderSearch.Location;
            recordObject[recordType.Attributes["ITEM_BARCODE"]] = orderSearch.ItemBarcode;
            recordObject[recordType.Attributes["ITEM_DESC"]] = orderSearch.ItemDesc;
            recordObject[recordType.Attributes["ORDER_NO"]] = orderSearch.ORDER_NO;
            recordObject[recordType.Attributes["ORDER_TYPE"]] = orderSearch.OrderType;
            recordObject[recordType.Attributes["ORDER_DESC"]] = orderSearch.OrderDesc;
            recordObject[recordType.Attributes["STATUS"]] = orderSearch.Status;
            recordObject[recordType.Attributes["DATE_TYPE"]] = orderSearch.DateType;
            recordObject[recordType.Attributes["START_DATE"]] = orderSearch.StartDate;
            recordObject[recordType.Attributes["END_DATE"]] = orderSearch.EndDate;
            return recordObject;
        }
    }
}