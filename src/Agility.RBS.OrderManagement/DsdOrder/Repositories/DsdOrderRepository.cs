﻿// <copyright file="DsdOrderRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.OrderManagement.DsdOrder
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.OrderManagement.DsdOrder.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class DsdOrderRepository : BaseOraclePackage
    {
        public DsdOrderRepository()
        {
            this.PackageName = "dsd_orders";
        }

        public async Task<List<DsdOrderHeadModel>> DsdArOrdersGet(DsdOrderHeadModel dsdOrderHeadModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(OrderMgmtConstants.ObjTypeDsdAr, OrderMgmtConstants.GetProcDsdAr);
            OracleObject recordObject = this.SetParamsDsdAr(dsdOrderHeadModel);
            return await this.GetProcedure2<DsdOrderHeadModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public bool DsdArOrdersApprove(string orderNo)
        {
            try
            {
                this.Connection.Open();
                OracleParameterCollection parameters = this.Parameters;
                parameters.Clear();
                BaseOracleParameter baseOracleParameter;
                baseOracleParameter = new BaseOracleParameter { Name = "RESULT", DataType = OracleDbType.Boolean, Direction = ParameterDirection.ReturnValue };
                parameters.Add(this.ParameterBuilder(baseOracleParameter));
                baseOracleParameter = new BaseOracleParameter { Name = "P_ORDER_NO", DataType = OracleDbType.Number, Direction = ParameterDirection.Input, Value = orderNo };
                parameters.Add(this.ParameterBuilder(baseOracleParameter));
                this.ExecuteProcedure("SETDSDARORDERS", parameters);
                return (this.Parameters["Result"].Value == DBNull.Value) ? default(bool) : (bool)this.Parameters["Result"].Value;
            }
            catch (Exception ex)
            {
                return true;
            }
            finally
            {
                this.Connection.Close();
            }
        }

        public OracleObject SetParamsDsdAr(DsdOrderHeadModel dsdOrderHeadModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType(OrderMgmtConstants.RecordObjDsdAr);
            OracleObject recordObject = this.GetOracleObject(recordType);
            recordObject["RUN_DATE"] = dsdOrderHeadModel.RunDate;
            recordObject["SUPPLIER"] = dsdOrderHeadModel.Supplier;
            recordObject["STATUS"] = dsdOrderHeadModel.Status;
            return recordObject;
        }
    }
}