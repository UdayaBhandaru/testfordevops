﻿// <copyright file="DsdOrderHeadModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.OrderManagement.DsdOrder.Models
{
    using System;
    using Agility.Framework.Core.Profile.Entities;

    public class DsdOrderHeadModel : ProfileEntity
    {
        public int? Location { get; set; }

        public string Item { get; set; }

        public DateTime? RunDate { get; set; }

        public string ItemDesc { get; set; }

        public int? GroupNo { get; set; }

        public string GroupName { get; set; }

        public int? CatNo { get; set; }

        public string CatName { get; set; }

        public string LocStatus { get; set; }

        public int? Supplier { get; set; }

        public string SupName { get; set; }

        public decimal? PackSize { get; set; }

        public decimal? MinQty { get; set; }

        public decimal? MaxQty { get; set; }

        public decimal? Soh { get; set; }

        public decimal? OnOrder { get; set; }

        public decimal? OrderQty { get; set; }

        public decimal? AdjustQty { get; set; }

        public string AdjustBy { get; set; }

        public string Status { get; set; }

        public int? SosOrdNo { get; set; }

        public int? RmsOrderNo { get; set; }

        public int? PoGroup { get; set; }

        public string Remarks { get; set; }

        public string Operation { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string PError { get; set; }
    }
}