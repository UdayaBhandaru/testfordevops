﻿// <copyright file="DsdOrderModelMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.OrderManagement.DsdOrder.Mapping
{
    using Agility.RBS.Core;
    using Agility.RBS.OrderManagement.DsdOrder.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class DsdOrderModelMapping : Profile
    {
        public DsdOrderModelMapping()
            : base("DsdOrderModelMapping")
        {
            this.CreateMap<OracleObject, DsdOrderHeadModel>()
               .ForMember(m => m.RunDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["run_date"])))
               .ForMember(m => m.Location, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["location"])))
               .ForMember(m => m.Item, opt => opt.MapFrom(r => r["item"]))
               .ForMember(m => m.ItemDesc, opt => opt.MapFrom(r => r["item_desc"]))
               .ForMember(m => m.GroupNo, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["group_no"])))
               .ForMember(m => m.GroupName, opt => opt.MapFrom(r => r["group_name"]))
               .ForMember(m => m.CatNo, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["cat_no"])))
               .ForMember(m => m.CatName, opt => opt.MapFrom(r => r["cat_name"]))
               .ForMember(m => m.LocStatus, opt => opt.MapFrom(r => r["loc_status"]))
               .ForMember(m => m.Supplier, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["supplier"])))
               .ForMember(m => m.SupName, opt => opt.MapFrom(r => r["sup_name"]))
               .ForMember(m => m.PackSize, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["pack_size"])))
               .ForMember(m => m.MinQty, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["min_qty"])))
               .ForMember(m => m.MaxQty, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["max_qty"])))
               .ForMember(m => m.Soh, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["soh"])))
               .ForMember(m => m.OnOrder, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["on_order"])))
               .ForMember(m => m.OrderQty, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["order_qty"])))
               .ForMember(m => m.AdjustQty, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["adjust_qty"])))
               .ForMember(m => m.AdjustBy, opt => opt.MapFrom(r => r["adjust_by"]))
               .ForMember(m => m.Status, opt => opt.MapFrom(r => r["status"]))
               .ForMember(m => m.SosOrdNo, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["sos_ord_no"])))
               .ForMember(m => m.RmsOrderNo, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["rms_order_no"])))
               .ForMember(m => m.PoGroup, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["po_group"])))
               .ForMember(m => m.Remarks, opt => opt.MapFrom(r => r["remarks"]));
        }
    }
}