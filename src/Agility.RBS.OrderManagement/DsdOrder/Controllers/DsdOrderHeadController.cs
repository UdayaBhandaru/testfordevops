﻿// <copyright file="DsdOrderHeadController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.OrderManagement.DsdOrder
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core.Constants;
    using Agility.RBS.Inbox.Common;
    using Agility.RBS.Inbox.Models;
    using Agility.RBS.Inbox.Repository;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Models;
    using Agility.RBS.OrderManagement;
    using Agility.RBS.OrderManagement.DsdOrder.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    [Route("api/[controller]/[action]")]
    public class DsdOrderHeadController : Controller
    {
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly ServiceDocument<DsdOrderHeadModel> serviceDocument;
        private readonly DsdOrderRepository dsdOrderRepository;
        private readonly DomainDataRepository domainDataRepository;

        public DsdOrderHeadController(
            ServiceDocument<DsdOrderHeadModel> serviceDocument, DsdOrderRepository dsdOrderRepository, DomainDataRepository domainDataRepository)
        {
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
            this.dsdOrderRepository = dsdOrderRepository;
            this.domainDataRepository = domainDataRepository;
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in the Bulk Item List page.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        public async Task<ServiceDocument<DsdOrderHeadModel>> List()
        {
            this.serviceDocument.DomainData.Add("status", this.domainDataRepository.DomainDetailData(OrderMgmtConstants.DomainHdrOrst).Result);
            this.serviceDocument.DomainData.Add("location", this.domainDataRepository.LocationDomainGet().Result);
            this.serviceDocument.LocalizationData.AddData("PriceChangeHead");
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<DsdOrderHeadModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.DsdOrderSearch);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public bool? DsdOrderApprove(string orderNo)
        {
            try
            {
                return this.dsdOrderRepository.DsdArOrdersApprove(orderNo);
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return false;
            }
        }

        private async Task<List<DsdOrderHeadModel>> DsdOrderSearch()
        {
            try
            {
                var lstDsdArOrders = await this.dsdOrderRepository.DsdArOrdersGet(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstDsdArOrders;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }
    }
}