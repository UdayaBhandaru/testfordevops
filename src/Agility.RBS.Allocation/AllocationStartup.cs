﻿// <copyright file="AllocationStartup.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Allocation
{
    using Agility.Framework.Core;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;

    public class AllocationStartup : ComponentStartup<DbContext>
    {
        /// <inheritdoc/>
        public override void ConfigureDependencyInjection(IServiceCollection services)
        {
            services.AddTransient<AllocationRepository>();
            services.AddTransient<AllocationController>();
        }

        public void Startup(Microsoft.Extensions.Configuration.IConfigurationRoot configuration)
        {
            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<Mapping.AllocationMapping>();
                this.InitilizeMapping(cfg);
            });
        }
    }
}
