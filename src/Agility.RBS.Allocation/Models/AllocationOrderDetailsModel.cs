﻿// <copyright file="AllocationDetailsModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Allocation.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Agility.Framework.Core.Profile.Entities;

    public class AllocationOrderDetailsModel : ProfileEntity
    {
        public AllocationOrderDetailsModel()
        {
            this.Allocationdata = new List<AllocationModel>();
        }

        [Key]
        public int? OrderNo { get; set; }

        public string Item { get; set; }

        public string ItemDesc { get; set; }

        public int? Location { get; set; }

        public string LocName { get; set; }

        public int? QtyOrdered { get; set; }

        public DateTime? EstDate { get; set; }

        public List<AllocationModel> Allocationdata { get; private set; }

        public string Operation { get; set; }
    }
}
