﻿// <copyright file="AllocationModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Allocation.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Allocation.Models;

    public class AllocationModel : ProfileEntity
    {
        public AllocationModel()
        {
            this.AllocationDetails = new List<AllocationDetailsModel>();
        }

        [Key]
        public int? AllocNo { get; set; }

        public string AllocDesc { get; set; }

        public int? OrderNO { get; set; }

        public string Item { get; set; }

        public string ItemDesc { get; set; }

        public int? FromLoc { get; set; }

        public string FromLocName { get; set; }

        public DateTime? ReleaseDate { get; set; }

        public int? OrdQty { get; set; }

        public List<AllocationDetailsModel> AllocationDetails { get; set; }

        public string Operation { get; set; }
    }
}
