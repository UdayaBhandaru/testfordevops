﻿// <copyright file="AllocationDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Allocation.Models
{
    using System.Collections.Generic;

    public class AllocationDomainModel
    {
        public AllocationDomainModel()
        {
            this.LocationData = new List<ItemLocationModel>();
        }

        public List<ItemLocationModel> LocationData { get; private set; }
    }
}
