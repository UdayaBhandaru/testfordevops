﻿// <copyright file="AllocationDetailsModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Allocation.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using Agility.Framework.Core.Profile.Entities;

    public class AllocationDetailsModel : ProfileEntity
    {
        public int? ToLoc { get; set; }

        public string ToLocName { get; set; }

        public string ToLocType { get; set; }

        public int? QtyAllocated { get; set; }

        public string Operation { get; set; }

        public int? PreviousQty { get; set; }
    }
}
