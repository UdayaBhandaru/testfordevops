﻿// <copyright file="ItemLocationModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Allocation.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Allocation.Models;

    public class ItemLocationModel : ProfileEntity
    {
        public string Item { get; set; }

        public string LocType { get; set; }

        public int? Location { get; set; }

        public string LocName { get; set; }
    }
}
