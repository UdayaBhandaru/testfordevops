﻿// <copyright file="AllocationOrderHdrDetailsModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Allocation.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class AllocationOrderHdrDetailsModel : ProfileEntity
    {
        public int? OrderNo { get; set; }

        public int? Supplier { get; set; }

        public string SupName { get; set; }

        public string LocType { get; set; }

        public int? Location { get; set; }

        public string LocName { get; set; }
    }
}
