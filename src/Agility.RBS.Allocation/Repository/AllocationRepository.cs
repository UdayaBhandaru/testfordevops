﻿// <copyright file="AllocationRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Allocation
{
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Allocation.Common;
    using Agility.RBS.Allocation.Models;
    using Agility.RBS.Core;
    using AutoMapper;
    using Devart.Data.Oracle;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class AllocationRepository : BaseOraclePackage
    {
        private readonly ServiceDocumentResult serviceDocumentResult;

        public AllocationRepository()
        {
            this.PackageName = AllocationConstants.GetAllocationPackage;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public string AllocId { get; private set; }

        public async Task<List<AllocationOrderDetailsModel>> GetAllocationOrders(AllocationOrderDetailsModel allocationSearchModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(AllocationConstants.ObjTypeAllocationSearchTab, AllocationConstants.GetProcAllocationList);
            OracleObject recordObject = this.SetParamsGetAllocationOrders(allocationSearchModel);
            OracleTable pDomainMstTab = await this.GetProcedure(recordObject, packageParameter, this.serviceDocumentResult);
            List<AllocationOrderDetailsModel> allocationOrderDetailsList = Mapper.Map<List<AllocationOrderDetailsModel>>(pDomainMstTab);

            if (allocationOrderDetailsList != null)
            {
                for (int i = 0; i < allocationOrderDetailsList.Count; i++)
                {
                    OracleObject obj = (OracleObject)pDomainMstTab[i];
                    var domainDetails = obj["p_alloc_rec"];
                    if (domainDetails != DBNull.Value)
                    {
                        allocationOrderDetailsList[i].Allocationdata.Add(Mapper.Map<AllocationModel>(domainDetails));
                        for (int j = 0; j < allocationOrderDetailsList[i].Allocationdata.Count; j++)
                        {
                            if (domainDetails != DBNull.Value)
                            {
                                OracleObject obj1 = (OracleObject)domainDetails;
                                OracleTable domainDetails1 = (Devart.Data.Oracle.OracleTable)obj1["XALLOCDTL_TBL"];
                                if (domainDetails1.Count > 0)
                                {
                                    allocationOrderDetailsList[i].Allocationdata[j].AllocationDetails.AddRange(Mapper.Map<List<AllocationDetailsModel>>(domainDetails1));
                                }
                            }
                        }
                    }
                }
            }

            return allocationOrderDetailsList;
        }

        public async Task<AllocationOrderHdrDetailsModel> AllocationOrderDetailsGet(int orderNo)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType("orddtl_rec", this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["ORDER_NO"]] = orderNo;
            PackageParams packageParameter = this.GetPackageParams(AllocationConstants.ObjTypeAllocationOrderTab, AllocationConstants.GetProcOrder);
            var orderdetails = await this.GetProcedure2<AllocationOrderHdrDetailsModel>(recordObject, packageParameter, null);
            return orderdetails != null ? orderdetails[0] : null;
        }

        public async Task<ServiceDocumentResult> saveAllocation(AllocationModel hModel)
        {
            this.AllocId =string.Empty;
            PackageParams packageParameter = this.GetPackageParams(AllocationConstants.ObjTypeAllocationSaveTab, AllocationConstants.GetProcAllocationSet);
            OracleObject recordObject = this.SetParamsReceiving(hModel);
            var result = await this.SetProcedure(recordObject, packageParameter);
            if (this.ObjResult != null && result.Type == MessageType.Success)
            {
                this.AllocId = Convert.ToString(this.ObjResult["ALLOC_NO"]);
            }

            return result;
        }

        public async Task<List<ItemLocationModel>> ItemLocationDomainGet(long? item)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("item_loc_rec");
            OracleObject recordObject = this.GetOracleObject(recordType);
            recordObject["ITEM"] = item;
            PackageParams packageParameter = this.GetPackageParams(AllocationConstants.ObjTypeDomainItemLocTAB, AllocationConstants.GetProcItemLocDomain);
            return await this.GetProcedure2<ItemLocationModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        private OracleObject SetParamsReceiving(AllocationModel hModel)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType("alloc_hdrdtl_rec", this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["ALLOC_NO"]] = hModel.AllocNo;
            recordObject[recordType.Attributes["ALLOC_DESC"]] = hModel.AllocDesc;
            recordObject[recordType.Attributes["ORDER_NO"]] = hModel.OrderNO;
            recordObject[recordType.Attributes["ITEM"]] = hModel.Item;
            recordObject[recordType.Attributes["ITEM_DESC"]] = hModel.ItemDesc;
            recordObject[recordType.Attributes["FROM_LOC"]] = hModel.FromLoc;
            recordObject[recordType.Attributes["FROM_LOC_NAME"]] = hModel.FromLocName;
            recordObject[recordType.Attributes["RELEASE_DATE"]] = hModel.ReleaseDate;
            recordObject[recordType.Attributes["ORD_QTY"]] = hModel.OrdQty;
            recordObject[recordType.Attributes["operation"]] = hModel.Operation;

            // Detail Object
            OracleType dtlTableType = this.GetObjectType("alloc_dtl_tab");
            OracleType dtlRecordType = this.GetObjectType("alloc_dtl_rec");
            OracleTable pUtlDmnDtlTab = new OracleTable(dtlTableType);
            foreach (var item in hModel.AllocationDetails)
            {
                OracleObject dtlRecordObject = this.GetOracleObject(dtlRecordType);
                try
                {
                    if (item.QtyAllocated == item.PreviousQty)
                    {
                        item.QtyAllocated = 0;
                    }

                    if (item.PreviousQty != null && item.QtyAllocated == null)
                    {
                        item.QtyAllocated = item.PreviousQty;
                    }

                    dtlRecordObject[dtlRecordType.Attributes["TO_LOC"]] = item.ToLoc;
                    dtlRecordObject[dtlRecordType.Attributes["TO_LOC_NAME"]] = item.ToLocName;
                    dtlRecordObject[dtlRecordType.Attributes["TO_LOC_TYPE"]] = item.ToLocType;
                    dtlRecordObject[dtlRecordType.Attributes["QTY_ALLOCATED"]] = item.QtyAllocated;
                    dtlRecordObject[dtlRecordType.Attributes["operation"]] = item.Operation;
                    pUtlDmnDtlTab.Add(dtlRecordObject);
                }
                catch (Exception)
                {
                    pUtlDmnDtlTab.Add(dtlRecordObject);
                }
            }

            recordObject["XALLOCDTL_TBL"] = pUtlDmnDtlTab;
            return recordObject;
        }

        private OracleObject SetParamsGetAllocationOrders(AllocationOrderDetailsModel allocationSearchModel)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(AllocationConstants.RecordObjAllocationSearchRec, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["ORDER_NO"]] = allocationSearchModel.OrderNo;
            return recordObject;
        }
    }
}