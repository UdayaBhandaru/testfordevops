﻿// <copyright file="ReceivingMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Allocation.Mapping
{
    using Agility.RBS.Allocation.Models;
    using Agility.RBS.Core;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class AllocationMapping : Profile
    {
        public AllocationMapping()
            : base("AllocationMapping")
        {
            this.CreateMap<OracleObject, AllocationModel>()
            .ForMember(m => m.AllocNo, opt => opt.MapFrom(r => OracleNullHandler.DbNullInt64Handler(r["ALLOC_NO"])))
            .ForMember(m => m.AllocDesc, opt => opt.MapFrom(r => r["ALLOC_DESC"]))
            .ForMember(m => m.OrderNO, opt => opt.MapFrom(r => OracleNullHandler.DbNullInt64Handler(r["ORDER_NO"])))
            .ForMember(m => m.Item, opt => opt.MapFrom(r => r["ITEM"]))
            .ForMember(m => m.ItemDesc, opt => opt.MapFrom(r => r["ITEM_DESC"]))
             .ForMember(m => m.FromLoc, opt => opt.MapFrom(r => OracleNullHandler.DbNullInt64Handler(r["FROM_LOC"])))
            .ForMember(m => m.FromLocName, opt => opt.MapFrom(r => r["FROM_LOC_NAME"]))
            .ForMember(m => m.ReleaseDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["RELEASE_DATE"])))
             .ForMember(m => m.OrdQty, opt => opt.MapFrom(r => OracleNullHandler.DbNullInt64Handler(r["ORD_QTY"])))
            .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, AllocationDetailsModel>()
            .ForMember(m => m.ToLoc, opt => opt.MapFrom(r => OracleNullHandler.DbNullInt64Handler(r["TO_LOC"])))
            .ForMember(m => m.ToLocName, opt => opt.MapFrom(r => r["TO_LOC_NAME"]))
            .ForMember(m => m.ToLocType, opt => opt.MapFrom(r => r["TO_LOC_TYPE"]))
            .ForMember(m => m.QtyAllocated, opt => opt.MapFrom(r => OracleNullHandler.DbNullInt64Handler(r["QTY_ALLOCATED"])))
            .ForMember(m => m.PreviousQty, opt => opt.MapFrom(r => OracleNullHandler.DbNullInt64Handler(r["QTY_ALLOCATED"])))
            .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, AllocationOrderDetailsModel>()
            .ForMember(m => m.OrderNo, opt => opt.MapFrom(r => OracleNullHandler.DbNullInt64Handler(r["ORDER_NO"])))
            .ForMember(m => m.Item, opt => opt.MapFrom(r => r["ITEM"]))
            .ForMember(m => m.ItemDesc, opt => opt.MapFrom(r => r["ITEM_DESC"]))
            .ForMember(m => m.Location, opt => opt.MapFrom(r => OracleNullHandler.DbNullInt64Handler(r["LOCATION"])))
            .ForMember(m => m.LocName, opt => opt.MapFrom(r => r["LOC_NAME"]))
            .ForMember(m => m.QtyOrdered, opt => opt.MapFrom(r => OracleNullHandler.DbNullInt64Handler(r["QTY_ORDERED"])))
            .ForMember(m => m.EstDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["EST_DATE"])))
            .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, AllocationOrderHdrDetailsModel>()
            .ForMember(m => m.OrderNo, opt => opt.MapFrom(r => OracleNullHandler.DbNullInt64Handler(r["ORDER_NO"])))
            .ForMember(m => m.Supplier, opt => opt.MapFrom(r => OracleNullHandler.DbNullInt64Handler(r["SUPPLIER"])))
            .ForMember(m => m.SupName, opt => opt.MapFrom(r => r["SUP_NAME"]))
            .ForMember(m => m.LocType, opt => opt.MapFrom(r => r["LOC_TYPE"]))
            .ForMember(m => m.Location, opt => opt.MapFrom(r => OracleNullHandler.DbNullInt64Handler(r["LOCATION"])))
            .ForMember(m => m.LocName, opt => opt.MapFrom(r => r["LOC_NAME"]));

            this.CreateMap<OracleObject, ItemLocationModel>()
           .ForMember(m => m.Item, opt => opt.MapFrom(r => r["ITEM"]))
           .ForMember(m => m.LocType, opt => opt.MapFrom(r => r["LOC_TYPE"]))
           .ForMember(m => m.Location, opt => opt.MapFrom(r => OracleNullHandler.DbNullInt64Handler(r["LOCATION"])))
           .ForMember(m => m.LocName, opt => opt.MapFrom(r => r["LOC_NAME"]));
        }
    }
}
