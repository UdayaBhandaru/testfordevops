﻿// <copyright file="AllocationController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Allocation
{
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Allocation.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    [Route("api/[controller]/[action]")]
    public class AllocationController : Controller
    {
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly AllocationRepository allocationRepository;
        private readonly ServiceDocument<AllocationModel> serviceDocument;

        public AllocationController(ServiceDocument<AllocationModel> serviceDocument, AllocationRepository receivingRepository)
        {
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
            this.allocationRepository = receivingRepository;
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in the Promotion List page.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        public async Task<ServiceDocument<AllocationModel>> List()
        {
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in Promotion Head page when Add button clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<AllocationModel>> New()
        {
          return serviceDocument;
        }

        [AllowAnonymous]
        public async Task<List<AllocationOrderDetailsModel>> GetAllocationOrders(int orderNo)
        {
            return await this.allocationRepository.GetAllocationOrders(new AllocationOrderDetailsModel { OrderNo = orderNo }, this.serviceDocumentResult);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<AllocationOrderHdrDetailsModel> AllocationOrderDetailsGet(int orderNo)
        {
            try
            {
                return await this.allocationRepository.AllocationOrderDetailsGet(orderNo);
            }
            catch (System.Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }
    }
}
