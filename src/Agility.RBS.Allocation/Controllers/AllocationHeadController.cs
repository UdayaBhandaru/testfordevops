﻿// <copyright file="AllocationHeadController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Allocation
{
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Allocation.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Threading.Tasks;

    [Route("api/[controller]/[action]")]
    public class AllocationHeadController : Controller
    {
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly AllocationRepository allocationRepository;
        private readonly ServiceDocument<AllocationModel> serviceDocument;
        private long priceChangeRequestId;

        public AllocationHeadController(ServiceDocument<AllocationModel> serviceDocument, AllocationRepository allocationRepository)
        {
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
            this.allocationRepository = allocationRepository;
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in the Promotion List page.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        public async Task<ServiceDocument<AllocationModel>> List()
        {
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in Promotion Head page when Add button clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<AllocationModel>> New()
        {
          return serviceDocument;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<AllocationDomainModel> AllocationDomainData(long? item)
        {
            try
            {
                AllocationDomainModel allocationDomainModel = new AllocationDomainModel();
                allocationDomainModel.LocationData.AddRange(this.allocationRepository.ItemLocationDomainGet(item).Result);
                return allocationDomainModel;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public async ValueTask<bool> saveAllocation([FromBody]AllocationModel allocationModel)
        {
            try
            {
                await this.allocationRepository.saveAllocation(allocationModel);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
