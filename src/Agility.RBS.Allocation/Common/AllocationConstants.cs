﻿// <copyright file="AllocationConstants.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Allocation.Common
{
   internal static class AllocationConstants
   {
        // Allocation Package
        public const string GetAllocationPackage = "UTL_ALLOCATION";
        public const string ObjTypeAllocationSearchTab = "alloc_ord_dtl_tab";
        public const string GetProcAllocationList = "getorddtl";
        public const string RecordObjAllocationSearchRec = "alloc_ord_dtl_rec";
        public const string ObjTypeAllocationOrderTab = "orddtl_tab";
        public const string GetProcOrder = "getorddtl";
        public const string ObjTypeAllocationSaveTab = "alloc_hdrdtl_tab";
        public const string GetProcAllocationSet = "setallocation";
        public const string ObjTypeDomainItemLocTAB = "item_loc_tab";
        public const string GetProcItemLocDomain = "getitemloc";
    }
}
