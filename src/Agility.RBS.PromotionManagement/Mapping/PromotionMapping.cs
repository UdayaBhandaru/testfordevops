﻿// <copyright file="PromotionMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Promotion.Mapping
{
    using Agility.RBS.Core;
    using Agility.RBS.MDM.Repositories.Models;
    using Agility.RBS.Promotion.Models;
    using Agility.RBS.PromotionManagement.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class PromotionMapping : Profile
    {
        public PromotionMapping()
            : base("PromotionMapping")
        {
            this.CreateMap<OracleObject, PromotionEventModel>()
            .ForMember(m => m.PROMO_EVENT_ID, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["PROMO_EVENT_ID"])))
            .ForMember(m => m.PromoEventDisplayId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["PROMO_EVENT_DISPLAY_ID"])))
            .ForMember(m => m.Description, opt => opt.MapFrom(r => r["DESCRIPTION"]))
            .ForMember(m => m.Theme, opt => opt.MapFrom(r => r["THEME"]))
            .ForMember(m => m.PromoType, opt => opt.MapFrom(r => r["promo_clearance_ind"]))
            .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"))
            .ForMember(m => m.Description, opt => opt.MapFrom(r => r["DESCRIPTION"]))
            .ForMember(m => m.StartDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["START_DATE"])))
            .ForMember(m => m.EndDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["END_DATE"])))
            .ForMember(m => m.LockVersion, opt => opt.MapFrom(r => r["LOCK_VERSION"]))
            .ForMember(m => m.ProgramMessage, opt => opt.MapFrom(r => r["PROGRAM_MESSAGE"]))
            .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_ERROR"]));

            this.CreateMap<OracleObject, MarchentHierarchieModel>()
           .ForMember(m => m.Division, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["division"])))
           .ForMember(m => m.DivName, opt => opt.MapFrom(r => r["div_name"]))
            .ForMember(m => m.GroupNo, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["group_no"])))
           .ForMember(m => m.GroupName, opt => opt.MapFrom(r => r["group_name"]))
           .ForMember(m => m.Dept, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["dept"])))
           .ForMember(m => m.DeptName, opt => opt.MapFrom(r => r["dept_name"]))
           .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"))
           .ForMember(m => m.Class, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["class"])))
           .ForMember(m => m.SubClass, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["subclass"])))
           .ForMember(m => m.ClassName, opt => opt.MapFrom(r => r["class_name"]))
           .ForMember(m => m.SubClassName, opt => opt.MapFrom(r => r["sub_name"]))
           .ForMember(m => m.ProgramMessage, opt => opt.MapFrom(r => r["PROGRAM_MESSAGE"]))
           .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_ERROR"]));

            this.CreateMap<OracleObject, OrgHierarchieModel>()
         .ForMember(m => m.Format, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["Format"])))
         .ForMember(m => m.FormatName, opt => opt.MapFrom(r => r["Format_Name"]))
         .ForMember(m => m.Name, opt => opt.MapFrom(r => r["Format_Name"]))
         .ForMember(m => m.ProgramMessage, opt => opt.MapFrom(r => r["PROGRAM_MESSAGE"]))
         .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_ERROR"]));

            this.CreateMap<OracleObject, OrgHierLocationModel>()
           .ForMember(m => m.LocationId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["location_id"])))
           .ForMember(m => m.LocationName, opt => opt.MapFrom(r => r["loc_name"]))
           .ForMember(m => m.Name, opt => opt.MapFrom(r => r["loc_name"]))
           .ForMember(m => m.ProgramMessage, opt => opt.MapFrom(r => r["PROGRAM_MESSAGE"]))
           .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_ERROR"]));

            this.CreateMap<OracleObject, FormatLocationModel>()
            .ForMember(m => m.Format, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["FORMAT"])))
            .ForMember(m => m.Location, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["LOCATION"])))
            .ForMember(m => m.FormatName, opt => opt.MapFrom(r => r["FORMAT_NAME"]))
            .ForMember(m => m.LocationName, opt => opt.MapFrom(r => r["LOC_NAME"]));

            this.CreateMap<OracleObject, FuturePromotionModel>()
            .ForMember(m => m.DeptDesc, opt => opt.MapFrom(r => r["DEPT_DESC"]))
            .ForMember(m => m.ClassDesc, opt => opt.MapFrom(r => r["CLASS_DESC"]))
            .ForMember(m => m.SubclassDesc, opt => opt.MapFrom(r => r["SUBCLASS_DESC"]))
            .ForMember(m => m.LocName, opt => opt.MapFrom(r => r["LOC_NAME"]))
            .ForMember(m => m.SellingUomDesc, opt => opt.MapFrom(r => r["SELLING_UOM__DESC"]))
            .ForMember(m => m.ActionDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["ACTION_DATE"])))
             .ForMember(m => m.PcChangeTypeDesc, opt => opt.MapFrom(r => r["PC_CHANGE_TYPE_DESC"]))
             .ForMember(m => m.Promotion1IdDesc, opt => opt.MapFrom(r => r["PROMOTION1_ID_DESC"]))
            .ForMember(m => m.PcChangeAmount, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["PC_CHANGE_AMOUNT"])))
            .ForMember(m => m.Promotion1Id, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["PROMOTION1_ID"])));
        }
    }
}
