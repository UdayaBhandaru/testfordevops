﻿// <copyright file="PromotionListController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.PromotionManagement
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Constants;
    using Agility.RBS.Inbox.Common;
    using Agility.RBS.Inbox.Models;
    using Agility.RBS.Inbox.Repository;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Models;
    using Agility.RBS.Promotion;
    using Agility.RBS.Promotion.Models;
    using Agility.RBS.PromotionManagement.Common;
    using Agility.RBS.PromotionManagement.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    [Route("api/[controller]/[action]")]
    public class PromotionListController : Controller
    {
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly PromotionRepository promotionRepository;
        private readonly InboxModel inboxModel;
        private readonly InboxRepository inboxRepository;
        private readonly HierarchyRepository hierarchyRepository;
        private readonly DomainDataRepository domainDataRepository;
        private ServiceDocument<PromotionEventModel> serviceDocument;
        private long priceChangeRequestId;

        public PromotionListController(
            ServiceDocument<PromotionEventModel> serviceDocument,
            PromotionRepository promotionRepository,
            InboxRepository inboxRepository,
            HierarchyRepository hierarchyRepository,
            DomainDataRepository domainDataRepository)
        {
            this.serviceDocument = serviceDocument;
            this.promotionRepository = promotionRepository;
            this.hierarchyRepository = hierarchyRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
            this.inboxModel = new InboxModel { Operation = "CHGS", ModuleName = "PROMO" };
            this.inboxRepository = inboxRepository;
            this.domainDataRepository = domainDataRepository;
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in the Promotion List page.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<PromotionEventModel>> List()
        {
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for fetching Promotion records based on search criteria when Search button clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<PromotionEventModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.PromotionHeadSearch);
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in Promotion Head page when Add button clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<PromotionEventModel>> New()
        {
            this.serviceDocument.New(false);
            return await this.GetDomainData();
        }

        /// <summary>
        /// This API invoked for saving field values in the Promotion Head page when Save button clicked.
        /// </summary>
        /// <param name="serviceDocument">Service Document</param>
        /// <returns>ServiceDocument</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<PromotionEventModel>> Save([FromBody]ServiceDocument<PromotionEventModel> serviceDocument)
        {
            await this.serviceDocument.SaveAsync(this.PromotionHeadSave);
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for populating fields in the Promotion Head page when Edit icon clicked.
        /// </summary>
        /// <param name="id">Promotion Id</param>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<PromotionEventModel>> Open(int id)
        {
            this.priceChangeRequestId = id;
            await this.serviceDocument.OpenAsync(this.CallBackOpen);
            return await this.GetDomainData();
        }

        /// <summary>
        /// This API invoked for submitting Promotion to next level when Submit button clicked.
        /// </summary>
        /// <param name="serviceDocument">Service Document</param>
        /// <returns>ServiceDocument</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<PromotionEventModel>> Submit([FromBody]ServiceDocument<PromotionEventModel> serviceDocument)
        {
            this.serviceDocument = serviceDocument;
            this.inboxModel.StateID = GuidConverter.DotNetToOracle(Convert.ToString(serviceDocument.DataProfile.DataModel.WorkflowInstance?.WorkflowStateId));
            if (this.serviceDocument.DataProfile.DataModel.PROMO_EVENT_ID == 0 || this.serviceDocument.DataProfile.DataModel.PROMO_EVENT_ID == null)
            {
                this.inboxModel.Operation = "DFT";
            }

            await this.serviceDocument.TransitAsync(this.PromotionHeadSave);
            var stateName = Inbox.Common.RbsServiceDocCustomizationHelper.GetStateNameByProfileIdAndCurActionId<PromotionEventModel>(this.serviceDocument);
            await this.inboxRepository.SetInbox(this.inboxModel, this.serviceDocument);
            if (stateName == "RequestCompleted" || stateName == "Request Completed")
            {
                this.serviceDocument.Result =
                    await this.promotionRepository.SavePromoInDbFromExcel(
                    Path.Combine(RbSettings.RbsExcelFileDownloadPath, "Promotion-" + this.serviceDocument.DataProfile.DataModel.PROMO_EVENT_ID + ".xlsm"),
                    Convert.ToString(this.serviceDocument.DataProfile.DataModel.PROMO_EVENT_ID));
            }

            return this.serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<PromotionEventModel>> Reject([FromBody]ServiceDocument<PromotionEventModel> serviceDocument)
        {
            this.SetServiceDocumentAndInboxStateId(serviceDocument);
            await this.serviceDocument.TransitAsync(GuidConverter.DotNetToOracle(serviceDocument.DataProfile.DataModel.WorkflowForm.StateId), true);
            if (this.serviceDocument.Result.Type == MessageType.Success)
            {
                this.inboxModel.Operation = "REJ";
                this.inboxModel.ActionType = "R";
                this.inboxModel.ToStateID = GuidConverter.DotNetToOracle(this.serviceDocument.DataProfile.DataModel.WorkflowStateId.ToString());
                this.inboxRepository.SetInbox(this.inboxModel, this.serviceDocument);
            }

            return this.serviceDocument;
        }

        public async Task<List<OrgHierarchieModel>> GetMerchTreeview()
        {
            return await this.hierarchyRepository.GetOrgTreeview();
        }

        /// <summary>
        /// This API invoked for Promotion in Product Tab of Item Super Screen
        /// </summary>
        /// <param name="itemNo">Item Code</param>
        /// <returns>List oF Promotions</returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<List<FuturePromotionModel>> GetFuturePromotionData(string itemNo)
        {
            return await this.promotionRepository.GetFuturePromotionData(itemNo, this.serviceDocumentResult);
        }

        private void SetServiceDocumentAndInboxStateId(ServiceDocument<PromotionEventModel> serviceDocument)
        {
            this.serviceDocument = serviceDocument;
            this.inboxModel.StateID = RbsServiceDocCustomizationHelper.GetStateIdDotNetToOracle(serviceDocument);
        }

        private async Task<bool> PromotionHeadSave()
        {
            this.serviceDocument.Result = await this.promotionRepository.PromotionHeadSet(this.serviceDocument.DataProfile.DataModel);
            if (this.serviceDocument.Result.Type != MessageType.Success)
            {
                return false;
            }

            this.serviceDocument.DataProfile.DataModel.PROMO_EVENT_ID = this.promotionRepository.PromotionRequestId;
            return true;
        }

        private async Task<PromotionEventModel> CallBackOpen()
        {
            try
            {
                FilePathModel file = this.domainDataRepository.FilePathGet(this.inboxModel.ModuleName).Result;
                var pHead = await this.promotionRepository.OpenPromoEventWithHierarchy(this.priceChangeRequestId, this.serviceDocumentResult);
                var res = await this.promotionRepository.ReadExcelToGrid(pHead, file, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return pHead?[0] ?? res;
                return res;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<List<PromotionEventModel>> PromotionHeadSearch()
        {
            try
            {
                var priceManagements = await this.promotionRepository.PromotionHeadSearch(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return priceManagements;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<ServiceDocument<PromotionEventModel>> GetDomainData()
        {
            this.serviceDocument.LocalizationData.AddData("PromotionHead");
            return this.serviceDocument;
        }
    }
}
