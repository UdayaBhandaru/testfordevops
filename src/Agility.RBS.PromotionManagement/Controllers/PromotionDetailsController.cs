﻿// <copyright file="PromotionDetailsController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.PromotionManagement
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.CustomExceptions;
    using Agility.RBS.Core.Models;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.Documents;
    using Agility.RBS.Documents.Models;
    using Agility.RBS.ItemManagement.Item;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.Promotion;
    using Agility.RBS.Promotion.Models;
    using Agility.RBS.PromotionManagement.Common;
    using DocumentFormat.OpenXml.Packaging;
    using DocumentFormat.OpenXml.Spreadsheet;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [Route("api/[controller]/[action]")]
    public class PromotionDetailsController : Controller
    {
        private const string PromotionFileName = "Promotion";
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly ServiceDocument<PromotionDetailsModel> serviceDocument;
        private readonly PromotionRepository promotionRepository;
        private readonly DocumentsRepository documentsRepository;
        //// private int promotionRequestId;

        public PromotionDetailsController(
            ServiceDocument<PromotionDetailsModel> serviceDocument,
            PromotionRepository promotionRepository,
            DomainDataRepository domainDataRepository,
            DocumentsRepository documentRepository)
        {
            this.serviceDocument = serviceDocument;
            this.promotionRepository = promotionRepository;
            this.domainDataRepository = domainDataRepository;
            this.documentsRepository = documentRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        /// <summary>
        /// This API invoked for saving grid data in the Promotion Details page when Save button clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<PromotionDetailsModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.PromotionDetailSave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async Task<FileResult> OpenPromoDetailExcel(int id)
        {
            return await this.DownloadLocalItemExcel(id);
        }

        public async System.Threading.Tasks.Task<FileResult> DownloadLocalItemExcel(long id)
        {
            var file = this.domainDataRepository.FilePathGet("PROMO").Result;
            ////string templateFile = RbSettings.PromoFile;
            string xlsmNew = RbSettings.RbsExcelFileDownloadPath + "\\" + Guid.NewGuid().ToString() + ".xlsm";
            string oldFile = $"{RbSettings.RbsExcelFileDownloadPath}\\{PromotionFileName}-{id}.xlsm";
            if (System.IO.File.Exists(oldFile))
            {
                System.IO.File.Copy(oldFile, xlsmNew);
            }
            else
            {
                System.IO.File.Copy(file.TemplatePath, xlsmNew);
                List<PromotionEventModel> listHead = await this.promotionRepository.OpenPromoEventWithHierarchy(id, this.serviceDocumentResult);
                if (listHead.Count > 0)
                {
                    using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(xlsmNew, true))
                    {
                        WorkbookPart workbookPart1 = spreadSheet.WorkbookPart;
                        Sheet sheet1 = workbookPart1.Workbook.Descendants<Sheet>().First(s => s.Name == "FF");
                        this.CreateSheets(listHead, workbookPart1, sheet1);

                        WorkbookPart workbookPart2 = spreadSheet.WorkbookPart;
                        Sheet sheet2 = workbookPart2.Workbook.Descendants<Sheet>().First(s => s.Name == "SM");
                        this.CreateSheets(listHead, workbookPart2, sheet2);

                        WorkbookPart workbookPart3 = spreadSheet.WorkbookPart;
                        Sheet sheet3 = workbookPart3.Workbook.Descendants<Sheet>().First(s => s.Name == "HC");
                        this.CreateSheets(listHead, workbookPart3, sheet3);
                    }
                }
            }

            var memory = new MemoryStream();
            using (var stream = new FileStream(xlsmNew, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }

            memory.Position = 0;

            var contentDispositionHeader = new System.Net.Mime.ContentDisposition
            {
                Inline = true,
                FileName = xlsmNew
            };

            this.Response.Headers.Add("Content-Disposition", contentDispositionHeader.ToString());
            var fileResultant = this.File(memory, DocumentsHelper.GetContentType(xlsmNew), Path.GetFileName(xlsmNew));
            System.IO.File.Delete(xlsmNew);
            return fileResultant;
        }

        [AllowAnonymous]
        public async Task<bool> FileUpload([FromBody]FileDataUpload file)
        {
            var tempFile = $"{Guid.NewGuid()}.xlsm";
            DocumentsHelper.FileUpload(string.Empty, tempFile, file.FileData);
            tempFile = $"{RbSettings.DocumentsServerPhysicalPath}\\{tempFile}";
            var physicalFile = $"{RbSettings.RbsExcelFileDownloadPath}\\{file.FileName}-{file.RequestId}.xlsm";
            var documentFilePath = $"{RbSettings.DocumentsServerVirtualPath}/{file.FileName}-{file.RequestId}.xlsm";
            DocumentDetailModel documentDetailModel = new DocumentDetailModel();
            documentDetailModel.FileName = file.FileName + "-" + file.RequestId + ".xlsm";
            documentDetailModel.FilePath = documentFilePath;
            documentDetailModel.DocumentStatus = "A";
            DocumentHeaderModel documentHeaderModelObj = new DocumentHeaderModel
            {
                ObjectName = "PROMO",
                ObjectValue = file.RequestId,
                Operation = "I"
            };
            if (System.IO.File.Exists(physicalFile))
            {
                System.IO.File.Delete(physicalFile);
                documentDetailModel.Operation = "U";
                documentHeaderModelObj.Operation = "U";
            }
            else
            {
                documentDetailModel.Operation = "I";
            }

            System.IO.File.Copy(tempFile, physicalFile);
            System.IO.File.Delete(tempFile);
            documentHeaderModelObj.DocumentDetails.Add(documentDetailModel);
            ServiceDocumentResult serviceDocumentResultObj = await this.documentsRepository.InsertDocuments(documentHeaderModelObj);
            if (!string.IsNullOrEmpty(serviceDocumentResultObj.InnerException))
            {
                throw new FileUploadException(serviceDocumentResultObj.InnerException);
            }

            return true;
        }

        private void CreateSheets(List<PromotionEventModel> listHead, WorkbookPart workbookPart1, Sheet sheet1)
        {
            WorksheetPart worksheetPart1 = workbookPart1.GetPartById(sheet1.Id.Value) as WorksheetPart;
            Worksheet promoWorksheet = worksheetPart1.Worksheet;
            try
            {
                if (listHead.Count > 0)
                {
                    StringBuilder strFL = new StringBuilder();
                    foreach (var fl in listHead[0].FormatLocList)
                    {
                        strFL = strFL.Append(fl.LocationName).Append(",");
                    }

                    //// Header Row
                    ExcelHelper.WriteToCell<string>("F", 5, promoWorksheet, strFL.ToString());
                    ExcelHelper.WriteToCell<string>("F", 6, promoWorksheet, listHead[0].Theme);
                    ExcelHelper.WriteToCell<string>("F", 8, promoWorksheet, listHead[0].StartDate?.ToString("dd/MM/yyyy"));
                    ExcelHelper.WriteToCell<string>("H", 6, promoWorksheet, Convert.ToString(listHead[0].PROMO_EVENT_ID));
                    //// Header Row
                }

                promoWorksheet.Save();
                Sheet sheetTempData = workbookPart1.Workbook.Descendants<Sheet>().FirstOrDefault(s => s.Name == "TempData");
                WorksheetPart worksheetPartTempData = workbookPart1.GetPartById(sheetTempData.Id.Value) as WorksheetPart;
                Worksheet promotionworksheetTempData = worksheetPartTempData.Worksheet;
                ExcelHelper.WriteToCell<string>("A", 1, promotionworksheetTempData, RbSettings.Web);
                promotionworksheetTempData.Save();
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
            }
        }

        private async Task<bool> PromotionDetailSave()
        {
            this.serviceDocument.Result = await this.promotionRepository.SetPromotionDetails(this.serviceDocument.DataProfile.DataModel);
            if (this.serviceDocument.Result.Type != MessageType.Success)
            {
                return false;
            }

            return true;
        }
    }
}
