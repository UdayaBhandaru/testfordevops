﻿// <copyright file="HierarchyRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Promotion
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Constants;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.Promotion.Models;
    using Agility.RBS.PromotionManagement.Common;
    using Agility.RBS.PromotionManagement.Models;
    using AutoMapper;
    using Devart.Data.Oracle;
    using DocumentFormat.OpenXml.Packaging;
    using DocumentFormat.OpenXml.Spreadsheet;

    public class HierarchyRepository : BaseOraclePackage
    {
        private readonly ServiceDocumentResult serviceDocumentResult;

        public HierarchyRepository()
        {
            this.PackageName = "merch_hierarchy";
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<List<MarchentHierarchieModel>> GetMerchTreeview()
        {
            this.Connection.Open();
            PackageParams packageParameter = this.GetPackageParams("merch_treeview_tab", "getmerchtreeview");
            OracleType recordType = OracleType.GetObjectType("merch_treeview_rec", this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            return await this.GetProcedure2<MarchentHierarchieModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<OrgHierarchieModel>> GetOrgTreeview()
        {
            this.Connection.Open();
            PackageParams packageParameter = this.GetPackageParams("orgh_treeview_tab", "getorgtreeview");
            OracleType recordType = OracleType.GetObjectType("orgh_treeview_rec", this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            OracleTable pPromotionMstTab = await this.GetProcedure(recordObject, packageParameter, this.serviceDocumentResult);
            List<OrgHierarchieModel> lstHeader = Mapper.Map<List<OrgHierarchieModel>>(pPromotionMstTab);
            if (lstHeader != null)
            {
                for (int i = 0; i < lstHeader.Count; i++)
                {
                    OracleObject obj = (OracleObject)pPromotionMstTab[i];
                    OracleTable promotionDetailsObj = (Devart.Data.Oracle.OracleTable)obj["P_LOCATION"];
                    if (promotionDetailsObj.Count > 0)
                    {
                        var childs = Mapper.Map<List<OrgHierLocationModel>>(promotionDetailsObj);
                        childs.ForEach(a => a.ParentId = lstHeader[i].Format);
                        lstHeader[i].ChildRoles.AddRange(childs);
                    }
                }
            }

            return lstHeader;
        }
    }
}