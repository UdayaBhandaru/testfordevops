﻿// <copyright file="PromotionRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Promotion
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Constants;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.MDM.Repositories.Models;
    using Agility.RBS.Promotion.Models;
    using Agility.RBS.PromotionManagement.Common;
    using Agility.RBS.PromotionManagement.Models;
    using AutoMapper;
    using Devart.Data.Oracle;
    using DocumentFormat.OpenXml.Packaging;
    using DocumentFormat.OpenXml.Spreadsheet;
    using ExcelDataReader;

    public class PromotionRepository : BaseOraclePackage
    {
        private readonly ServiceDocumentResult serviceDocumentResult;

        public PromotionRepository()
        {
            this.PackageName = PromotionConstants.GetPromotionPackage;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public long? PromotionRequestId { get; private set; }

        public int? PromotionComponentId { get; private set; }

        public async Task<List<PromotionEventModel>> PromotionHeadSearch(PromotionEventModel promotionSearchModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(PromotionConstants.ObjTypePromotionSearch, PromotionConstants.GetProcPromotionSearch);
            OracleObject recordObject = this.SetParamsPromotionHeadList(promotionSearchModel);
            return await this.GetProcedure2<PromotionEventModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<PromotionEventModel>> OpenPromoEventWithHierarchy(long promotionId, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(PromotionConstants.ObjTypePromotionSearch, PromotionConstants.GetProcPromotionSearch);
            PromotionEventModel promotionSearchModel = new PromotionEventModel { PROMO_EVENT_ID = promotionId };
            OracleObject recordObject = this.SetParamsPromotionHeadList(promotionSearchModel);
            OracleTable pPromotionMstTab = await this.GetProcedure(recordObject, packageParameter, serviceDocumentResult);
            List<PromotionEventModel> lstHeader = Mapper.Map<List<PromotionEventModel>>(pPromotionMstTab);
            if (lstHeader != null)
            {
                for (int i = 0; i < lstHeader.Count; i++)
                {
                    OracleObject obj = (OracleObject)pPromotionMstTab[i];
                    OracleTable promotionDetailsObj = (Devart.Data.Oracle.OracleTable)obj["FORMAT_LOC"];
                    if (promotionDetailsObj.Count > 0)
                    {
                        lstHeader[i].FormatLocList.AddRange(Mapper.Map<List<FormatLocationModel>>(promotionDetailsObj));
                    }
                }
            }

            return lstHeader;
        }

        public async Task<ServiceDocumentResult> PromotionHeadSet(PromotionEventModel promotionEventModel)
        {
            this.PromotionRequestId = 0;
            PackageParams packageParameter = this.GetPackageParams(PromotionConstants.ObjTypePromotionSearch, "setpromotionevent");
            OracleObject recordObject = this.SetParamsPromotionHeadList(promotionEventModel);
            var result = await this.SetProcedure(recordObject, packageParameter);
            if (this.ObjResult != null && promotionEventModel.PROMO_EVENT_ID == null)
            {
                this.PromotionRequestId = Convert.ToInt64(this.ObjResult["PROMO_EVENT_ID"]);
            }
            else
            {
                this.PromotionRequestId = promotionEventModel.PROMO_EVENT_ID;
            }

            return result;
        }

        public async Task<ServiceDocumentResult> SetPromotionDetails(PromotionDetailsModel promotionDtlModel)
        {
            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();
            PackageParams packageParameter = this.GetPackageParams(PromotionConstants.ObjTypePromotionDetails, PromotionConstants.SetProcPromotionDetails);
            OracleParameter parameter;
            parameter = new OracleParameter("RESULT", OracleDbType.Boolean)
            {
                Direction = System.Data.ParameterDirection.ReturnValue
            };
            parameters.Add(parameter);
            parameter = new OracleParameter(packageParameter.ObjectTypeTable, OracleDbType.Table)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                ObjectTypeName = packageParameter.ObjectTypeName,
                Value = this.SetParamsPromotionDetails(promotionDtlModel)
            };
            parameters.Add(parameter);
            var result = await this.SetProcedure(null, packageParameter, parameters);
            if (this.ObjResult != null)
            {
                this.PromotionComponentId = Convert.ToInt32(this.ObjResult["PROMO_COMP_ID"]);
            }

            return result;
        }

        public async Task<PromotionEventModel> ReadExcelToGrid(IList<PromotionEventModel> header, FilePathModel file, ServiceDocumentResult serviceDocumentResult)
        {
            if (header != null && header.Count > 0)
            {
                string bulkItemFile = $"{RbSettings.RbsExcelFileDownloadPath}\\Promotion-{header[0].PROMO_EVENT_ID}.xlsm";
                if (System.IO.File.Exists(bulkItemFile))
                {
                    var dt = await this.GetDataTableByReadingExcel(bulkItemFile);
                    return await this.ConvertExcelDataToType(dt, header[0]);
                }
            }

            return null;
        }

        public async Task<PromotionEventModel> ConvertExcelDataToType(DataTable eSheet, PromotionEventModel iHeadModel)
        {
            if (eSheet != null)
            {
                List<PromotionEventDetailsModel> detailsList = (from DataRow dr in eSheet.Rows
                                                                select new PromotionEventDetailsModel
                                                                {
                                                                    Item = Convert.ToString(dr[1]),
                                                                    Description = Convert.ToString(dr[6]),
                                                                }).ToList();
                detailsList = detailsList?.Where(e => e.Item != string.Empty && e.Description != string.Empty).ToList();
                iHeadModel.PromoDetails.AddRange(detailsList);
            }

            return iHeadModel;
        }

        public async Task<DataTable> GetDataTableByReadingExcel(string fileName)
        {
            if (System.IO.File.Exists(fileName))
            {
                using (var stream = System.IO.File.Open(fileName, FileMode.Open, FileAccess.Read))
                {
                    using (var reader = ExcelReaderFactory.CreateReader(stream))
                    {
                        var eSheet = reader.AsDataSet(new ExcelDataSetConfiguration
                        {
                            ConfigureDataTable = (_) => new ExcelDataTableConfiguration
                            {
                                UseHeaderRow = true,
                                FilterRow = rowReader => rowReader.Depth > 10
                            }
                        }).Tables;
                        var eSheetT = eSheet[1];
                        return eSheetT;
                    }
                }
            }

            return null;
        }

        public async Task<ServiceDocumentResult> SavePromoInDbFromExcel(string fileName, string promoId)
        {
            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();
            PackageParams packageParameter = this.GetPackageParams("promotion_simple_tab", "setpromotionsimple");
            OracleParameter parameter;
            parameter = new OracleParameter("RESULT", OracleDbType.Boolean)
            {
                Direction = System.Data.ParameterDirection.ReturnValue
            };
            parameters.Add(parameter);
            parameter = new OracleParameter(packageParameter.ObjectTypeTable, OracleDbType.Table)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                ObjectTypeName = packageParameter.ObjectTypeName,
                Value = this.SavePromotionExcel(fileName, promoId)
            };
            parameters.Add(parameter);
            return await this.SetProcedure(null, packageParameter, parameters);
        }

        public async Task<List<FuturePromotionModel>> GetFuturePromotionData(string itemId, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            BaseOracleParameter baseOracleParameter;
            baseOracleParameter = new BaseOracleParameter { Name = "P_ITEM", DataType = OracleDbType.VarChar, Direction = ParameterDirection.Input, Value = itemId };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));
            OracleType recordType = this.GetObjectType("FUTURE_RSP_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams("FUTURE_RSP_TAB", "GETFUTUREPROMO");
            return await this.GetProcedure2<FuturePromotionModel>(recordObject, packageParameter, serviceDocumentResult, parameters);
        }

        private OracleTable SavePromotionExcel(string fileName, string promoId)
        {
            using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(fileName, false))
            {
                this.Connection.Open();
                OracleType dtlTableType = this.GetObjectType("promotion_simple_tab");
                OracleType dtlRecordType = this.GetObjectType("promotion_simple_rec");
                WorkbookPart workbookPart = spreadSheet.WorkbookPart;
                var sheets = workbookPart.Workbook.Descendants<Sheet>()
                    .Where(sheet => sheet.Name != "Summary" && sheet.Name != "Sheet1" && sheet.Name != "TempData");
                OracleTable pUtlDmnDtlTab = new OracleTable(dtlTableType);
                long? item = 0;
                string changeAmount = string.Empty;
                foreach (var sheet in sheets)
                {
                    WorksheetPart worksheetPart = workbookPart.GetPartById(sheet.Id.Value) as WorksheetPart;
                    var rows = worksheetPart.Worksheet.Descendants<Row>().ToList();
                    for (int i = 12; i < rows.Count; i++)
                    {
                        OracleObject dtlRecordObject = this.GetOracleObject(dtlRecordType);
                        try
                        {
                            item = Convert.ToInt64(ExcelHelper.GetCellValue("B", i, worksheetPart.Worksheet, workbookPart));
                            dtlRecordObject[dtlRecordType.Attributes["item"]] = item;
                            dtlRecordObject[dtlRecordType.Attributes["promo_event_id"]] = promoId;
                            changeAmount = ExcelHelper.GetCellValue("U", i, worksheetPart.Worksheet, workbookPart);
                            dtlRecordObject[dtlRecordType.Attributes["change_amount"]] = string.IsNullOrWhiteSpace(changeAmount) ? null : changeAmount;
                            dtlRecordObject[dtlRecordType.Attributes["operation"]] = "I";
                            pUtlDmnDtlTab.Add(dtlRecordObject);
                        }
                        catch (Exception)
                        {
                            pUtlDmnDtlTab.Add(dtlRecordObject);
                        }
                    }
                }

                return pUtlDmnDtlTab;
            }
        }

        private OracleTable SetParamsPromotionDetails(PromotionDetailsModel promotionDtlModel)
        {
            this.Connection.Open();
            OracleType promotionDetailsTabType = OracleType.GetObjectType(PromotionConstants.ObjTypePromotionDetails, this.Connection);
            OracleType recordType = OracleType.GetObjectType(PromotionConstants.RecordObjPromotionDetails, this.Connection);
            OracleTable pPromotionDetailsTab = new OracleTable(promotionDetailsTabType);
            foreach (PromotionDetailsModel promotionComponentModel in promotionDtlModel.ListOfPromotionDetails)
            {
                OracleObject recordObject = new OracleObject(recordType);
                recordObject[recordType.Attributes["PROMO_ID"]] = promotionComponentModel.PromotionRequestId;
                recordObject[recordType.Attributes["PROMO_COMP_ID"]] = promotionComponentModel.PromoCompId;
                recordObject[recordType.Attributes["STATE"]] = "WST";
                recordObject[recordType.Attributes["NAME"]] = promotionComponentModel.Name;
                recordObject[recordType.Attributes["FUNDING_PERCENT"]] = promotionComponentModel.FundingPercent;
                recordObject[recordType.Attributes["CONSIGNMENT_RATE"]] = promotionComponentModel.ConsignmentRate;
                recordObject[recordType.Attributes["SECONDARY_IND"]] = promotionComponentModel.SecondaryInd;
                recordObject[recordType.Attributes["BUY_GET_UPTAKE_PERCENT"]] = promotionComponentModel.BuyGetUptakePercent;
                recordObject[recordType.Attributes["DEAL_ID_ITEM"]] = promotionComponentModel.DealIdItem;
                recordObject[recordType.Attributes["DEAL_DETAIL_ID_ITEM"]] = promotionComponentModel.DealDetailIdItem;
                recordObject[recordType.Attributes["DEAL_ID_LOCATION"]] = promotionComponentModel.DealIdLocation;
                recordObject[recordType.Attributes["DEAL_DETAIL_ID_LOCATION"]] = promotionComponentModel.DealDetailIdLocation;
                recordObject[recordType.Attributes["LOCK_VERSION"]] = promotionComponentModel.LockVersion;
                recordObject[recordType.Attributes["CREATED_BY"]] = promotionComponentModel.CreatedBy;
                recordObject[recordType.Attributes["LAST_UPDATED_BY"]] = FxContext.Context.Name;
                recordObject[recordType.Attributes["LAST_UPDATED_DATE"]] = promotionComponentModel.LastUpdatedDate;
                recordObject[recordType.Attributes["OPERATION"]] = promotionComponentModel.Operation;
                recordObject[recordType.Attributes["PROGRAM_PHASE"]] = promotionComponentModel.ProgramPhase;
                recordObject[recordType.Attributes["PROGRAM_MESSAGE"]] = promotionComponentModel.ProgramMessage;
                recordObject[recordType.Attributes["EFFECTIVE_START_DATE"]] = promotionComponentModel.EffectiveStartDate;
                recordObject[recordType.Attributes["EFFECTIVE_END_DATE"]] = promotionComponentModel.EffectiveEndDate;
                recordObject[recordType.Attributes["P_ERROR"]] = promotionComponentModel.Error;

                if (promotionComponentModel.Operation.Equals(RbCommonConstants.InsertOperation))
                {
                    recordObject[recordType.Attributes["CREATED_BY"]] = FxContext.Context.Name;
                }

                pPromotionDetailsTab.Add(recordObject);
            }

            return pPromotionDetailsTab;
        }

        private OracleObject SetParamsPromotionHeadList(PromotionEventModel promotionSearchModel)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(PromotionConstants.RecordObjPromotionSearch, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);

            recordObject[recordType.Attributes["PROMO_EVENT_ID"]] = promotionSearchModel.PROMO_EVENT_ID;
            recordObject[recordType.Attributes["PROMO_EVENT_DISPLAY_ID"]] = promotionSearchModel.PromoEventDisplayId;
            recordObject[recordType.Attributes["DESCRIPTION"]] = promotionSearchModel.Description;
            recordObject[recordType.Attributes["THEME"]] = promotionSearchModel.Theme;
            recordObject[recordType.Attributes["START_DATE"]] = promotionSearchModel.StartDate;
            recordObject[recordType.Attributes["END_DATE"]] = promotionSearchModel.EndDate;
            recordObject[recordType.Attributes["LOCK_VERSION"]] = null;
            recordObject[recordType.Attributes["OPERATION"]] = promotionSearchModel.Operation;
            recordObject[recordType.Attributes["promo_clearance_ind"]] = promotionSearchModel.PromoType;

            // Detail Object
            OracleType dtlTableType = this.GetObjectType("FORMAT_LOC_TAB");
            OracleType dtlRecordType = this.GetObjectType("format_loc_rec");
            OracleTable pUtlDmnDtlTab = new OracleTable(dtlTableType);
            foreach (var item in promotionSearchModel.OrgHierarchy)
            {
                OracleObject dtlRecordObject = this.GetOracleObject(dtlRecordType);
                try
                {
                    dtlRecordObject[dtlRecordType.Attributes["FORMAT"]] = item.ParentId ?? item.Format;
                    dtlRecordObject[dtlRecordType.Attributes["LOCATION"]] = item.LocationId;
                    dtlRecordObject[dtlRecordType.Attributes["PROMO_EVENT_ID"]] = promotionSearchModel.PROMO_EVENT_ID;
                    dtlRecordObject[dtlRecordType.Attributes["OPERATION"]] = promotionSearchModel.Operation;
                    pUtlDmnDtlTab.Add(dtlRecordObject);
                }
                catch (Exception)
                {
                    pUtlDmnDtlTab.Add(dtlRecordObject);
                }
            }

            recordObject["FORMAT_LOC"] = pUtlDmnDtlTab;
            return recordObject;
        }
    }
}