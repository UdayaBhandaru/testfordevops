﻿// <copyright file="PromotionConstants.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.PromotionManagement.Common
{
    internal static class PromotionConstants
    {
        public const string OracleParameterResult = "RESULT";

        // Documents Package
        public const string GetPromotionPackage = "UTL_PROMOTION";

        public const string GetProcPromotionHead = "GETPROMOTIONHEAD";
        public const string SetProcPromotionHead = "SETPROMOTIONHEAD";
        public const string ObjTypePromotionHead = "PROMOTION_HEAD_TAB";
        public const string RecordObjPromotionHead = "PROMOTION_HEAD_REC";

        // promo event
        public const string GetProcPromotionSearch = "getpromotionevent";
        public const string ObjTypePromotionSearch = "promo_event_tab";
        public const string RecordObjPromotionSearch = "promo_event_rec";

        // Details objects
        public const string ObjTypePromotionDetails = "PROMOTION_COMPONENT_TAB";
        public const string RecordObjPromotionDetails = "PROMOTION_COMPONENT_REC";
        public const string GetProcPromotionDetails = "GETPROMOTIONCOMPONENT";
        public const string SetProcPromotionDetails = "SETPROMOTIONCOMPONENT";

        public const string ObjTypePromoCompId = "PROMO_COMP_ID_TAB";
        public const string GetPromoCompPId = "GETPROMOCOMPID";

        // Promo Location objects
        public const string ObjTypePromotionComponentLocation = "PROMO_COMP_LOC_TAB";
        public const string RecordObjPromotionComponentLocation = "PROMO_COMP_LOC_REC";
        public const string GetProcPromotionComponentLocation = "GETPROMOTIONCOMPONENTLOC";
        public const string SetProcPromotionComponentLocation = "SETPROMOTIONCOMPONENTLOC";

        // Print objects
        public const string ObjTypePromotionPrint = "PROMOTION_PRINT_TAB";
        public const string GetProcPromotionPrint = "GETPROMOTIONRPRINT";
        public const string RecordObjPromotionPrint = "PROMOTION_PRINT_REC";

        // New Promo
        public const string ObjTypePromotion = "PROMOTION_TAB";
        public const string RecordObjPromotion = "PROMOTION_REC";
        public const string GetProcPromotion = "GETPROMOTION";
        public const string SetProcPromotion = "SETPROMOTION";
        public const string ObjTypePromoConcept = "PROMO_FORMAT_TAB";
        public const string RecordObjPromoConcept = "PROMO_FORMAT_REC";

        // Domain Header IDs
        public const string DomainHdrDateType = "DATETYPE";
        public const string DomainHdrIndicator = "INDICATOR";
        public const string DomainHdrOrgLevel = "PRCHGLVL";
        public const string DomainHdrMerchLevel = "MERCH_LVL";
        public const string DomainHdrChangeType = "PRCHGTYPE";
        public const string DomainHdrPromoStatus = "PROMOSTAT";
        public const string DomainHdrStatus = "STATUS";
        public const string DomainHdrPromotionType = "PROMOTYPE";
    }
}
