﻿// <copyright file="OrgHierLocationModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.PromotionManagement.Models
{
    using System;
    using System.Collections.Generic;

    public class OrgHierLocationModel
    {
        public int? LocationId { get; set; }

        public string LocationName { get; set; }

        public string LocType { get; set; }

        public string ProgramMessage { get; internal set; }

        public string Error { get; internal set; }

        public string Name { get; set; }

        public int? ParentId { get; set; }

        public int? Format { get; set; }

        public string FormatName { get; set; }
    }
}
