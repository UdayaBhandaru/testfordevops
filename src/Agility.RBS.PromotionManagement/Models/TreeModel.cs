﻿// <copyright file="TreeModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.PromotionManagement.Models
{
    using System;
    using System.Collections.Generic;

    public class TreeModel
    {
        public TreeModel()
        {
            this.Children = new List<TreeModel>();
        }

        public int? Id { get; set; }

        public string Name { get; set; }

        public int? ParentId { get; set; }

        public List<TreeModel> Children { get; internal set; }
    }
}
