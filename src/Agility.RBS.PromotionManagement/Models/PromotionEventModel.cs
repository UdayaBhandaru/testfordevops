﻿// <copyright file="PromotionEventModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Promotion.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Inbox.Common;
    using Agility.RBS.PromotionManagement.Models;

    [DataProfile(Name = "PROMOEVENT")]
    public class PromotionEventModel : ProfileEntity<PromotionHeadWfModel>, IInboxCommonModel
    {
        public PromotionEventModel()
        {
            this.OrgHierarchy = new List<OrgHierLocationModel>();
            this.FormatLocList = new List<FormatLocationModel>();
            this.PromoDetails = new List<PromotionEventDetailsModel>();
        }

        [Column("PROMO_EVENT_ID")]
        public long? PROMO_EVENT_ID { get; set; }

        [NotMapped]
        public long? PromoEventDisplayId { get; set; }

        [NotMapped]
        public string Description { get; set; }

        [NotMapped]
        public string Theme { get; set; }

        [NotMapped]
        public string LockVersion { get; set; }

        [NotMapped]
        public string ProgramPhase { get; set; }

        [NotMapped]
        public string ProgramMessage { get; set; }

        [NotMapped]
        public DateTime? StartDate { get; set; }

        [NotMapped]
        public DateTime? EndDate { get; set; }

        [NotMapped]
        public string Operation { get; set; }

        [NotMapped]
        public string Error { get; set; }

        [NotMapped]
        [Column("ORGANISATION_ID")]
        public new int? OrganizationId { get; set; }

        [NotMapped]
        [Column("CREATED_BY")]
        public new string CreatedBy { get; set; }

        [NotMapped]
        [Column("LAST_UPDATED_BY")]
        public new string ModifiedBy { get; set; }

        [NotMapped]
        [Column("DeletedInd")]
        public new bool DeletedInd { get; set; }

        [NotMapped]
        [Column("CREATED_DATE")]
        public new DateTimeOffset? CreatedDate { get; set; }

        [NotMapped]
        [Column("LAST_UPDATED_DATE")]
        public new DateTimeOffset? ModifiedDate { get; set; }

        [NotMapped]
        public Core.Models.RbWfHeaderModel WorkflowForm { get; set; }

        [NotMapped]
        public List<OrgHierLocationModel> OrgHierarchy { get; internal set; }

        [NotMapped]
        public List<FormatLocationModel> FormatLocList { get; internal set; }

        [NotMapped]
        public List<PromotionEventDetailsModel> PromoDetails { get; internal set; }

        [NotMapped]
        public string PromoType { get; set; }
    }
}
