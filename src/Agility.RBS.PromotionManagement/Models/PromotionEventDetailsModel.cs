﻿// <copyright file="PromotionEventDetailsModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Promotion.Models
{
    public class PromotionEventDetailsModel
    {
        public string Item { get; set; }

        public string Description { get; set; }
    }
}
