﻿// <copyright file="MarchentHierarchieModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.PromotionManagement.Models
{
    public class MarchentHierarchieModel
    {
        public int? Division { get; set; }

        public string DivName { get; set; }

        public int? GroupNo { get; set; }

        public string GroupName { get; set; }

        public int? Dept { get; set; }

        public string DeptName { get; set; }

        public int? Class { get; set; }

        public string ClassName { get; set; }

        public int? SubClass { get; set; }

        public string SubClassName { get; set; }

        public object ProgramMessage { get; internal set; }

        public object Error { get; internal set; }

        public object Operation { get; internal set; }
    }
}
