﻿// <copyright file="FormatLocationModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.PromotionManagement.Models
{
    using System.Collections.Generic;

    public class FormatLocationModel
    {
        public int? Format { get; set; }

        public int? Location { get; set; }

        public string FormatName { get; set; }

        public string LocationName { get; set; }
    }
}
