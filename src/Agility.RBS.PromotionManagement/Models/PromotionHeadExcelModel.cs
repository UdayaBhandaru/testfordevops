﻿// <copyright file="PromotionHeadExcelModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Promotion.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// //delete it after testing
    /// </summary>
    public class PromotionHeadExcelModel
    {
        public PromotionHeadExcelModel()
        {
            this.PromotionDetails = new List<PromotionDetailExcelModel>();
        }

        [NotMapped]
        public string Concept { get; set; }

        public int PromoId { get; set; }

        public string PromoName { get; set; }

        public DateTime? PromoStartDate { get; set; }

        public DateTime? PromoEndDate { get; set; }

        public List<PromotionDetailExcelModel> PromotionDetails { get; private set; }
    }
}
