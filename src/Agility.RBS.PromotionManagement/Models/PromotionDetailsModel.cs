﻿// <copyright file="PromotionDetailsModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Promotion.Models
{
    using System;
    using System.Collections.Generic;
    using Agility.Framework.Core.Profile.Entities;

    public class PromotionDetailsModel : ProfileEntity
    {
        public PromotionDetailsModel()
        {
            this.ListOfPromotionDetails = new List<PromotionDetailsModel>();
        }

        public int? PromotionRequestId { get; set; }

        public int? PromoCompId { get; set; }

        public string State { get; set; }

        public string Name { get; set; }

        public decimal? FundingPercent { get; set; }

        public decimal? ConsignmentRate { get; set; }

        public string SecondaryInd { get; set; }

        public decimal? BuyGetUptakePercent { get; set; }

        public int? DealIdItem { get; set; }

        public int? DealDetailIdItem { get; set; }

        public int? DealIdLocation { get; set; }

        public int? DealDetailIdLocation { get; set; }

        public int? LockVersion { get; set; }

        public string LastUpdatedBy { get; set; }

        public DateTime? LastUpdatedDate { get; set; }

        public DateTime? EffectiveStartDate { get; set; }

        public DateTime? EffectiveEndDate { get; set; }

        public string Operation { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Error { get; set; }

        public List<PromotionDetailsModel> ListOfPromotionDetails { get; private set; }
    }
}
