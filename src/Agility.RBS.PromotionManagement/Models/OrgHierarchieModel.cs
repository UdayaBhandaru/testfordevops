﻿// <copyright file="OrgHierarchieModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.PromotionManagement.Models
{
    using System.Collections.Generic;

    public class OrgHierarchieModel
    {
        public OrgHierarchieModel()
        {
            this.ChildRoles = new List<OrgHierLocationModel>();
        }

        public int? Format { get; set; }

        public string FormatName { get; set; }

        public List<OrgHierLocationModel> ChildRoles { get; internal set; }

        public object ProgramMessage { get; internal set; }

        public object Error { get; internal set; }

        public string Name { get; internal set; }
    }
}
