﻿// <copyright file="TransactionDataRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Finance.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.Finance.Constants;
    using Agility.RBS.Finance.Models;
    using Devart.Data.Oracle;

    /// <summary>
    /// Transaction Data Repositry
    /// </summary>
    public class TransactionDataRepository : BaseOraclePackage
    {
        private readonly ServiceDocumentResult serviceDocumentResult;

        /// <summary>
        /// Initializes a new instance of the <see cref="TransactionDataRepository"/> class.
        /// Transaction Data Repository
        /// </summary>
        public TransactionDataRepository()
        {
            this.PackageName = Constants.FinanceConstants.GetTransactionDataPackage;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        /// <summary>
        /// Transaction Data List
        /// </summary>
        /// <param name="transactionDataModel">transaction search data</param>
        /// <param name="serviceDocumentResult">transaction result</param>
        /// <returns>List of Transaction Data</returns>
        public async Task<List<TransactionDataModel>> GetTransactionDataResult(TransactionDataModel transactionDataModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(FinanceConstants.ObjTypeForTransactionSearch, FinanceConstants.ProcForTransactionSearch);
            OracleObject recordObject = this.SetParamsTrnsactionSearch(transactionDataModel);

            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();

            BaseOracleParameter baseOracleParameter;
            baseOracleParameter = new BaseOracleParameter
            {
                Name = "P_START",
                DataType = OracleDbType.Number,
                Direction = ParameterDirection.Input,
                Value = Convert.ToString(transactionDataModel.StartRow)
            };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            baseOracleParameter = new BaseOracleParameter { Name = "P_END", DataType = OracleDbType.Number, Direction = ParameterDirection.Input, Value = Convert.ToString(transactionDataModel.EndRow) };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            baseOracleParameter = new BaseOracleParameter { Name = "P_total", DataType = OracleDbType.Number, Direction = ParameterDirection.Output };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            if (transactionDataModel.SortModel == null)
            {
                transactionDataModel.SortModel = new[] { new Core.Models.SortModel { ColId = null, Sort = null } };
            }

            baseOracleParameter = new BaseOracleParameter { Name = "P_SortColId", DataType = OracleDbType.VarChar, Direction = ParameterDirection.Input, Value = transactionDataModel.SortModel[0].ColId };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            baseOracleParameter = new BaseOracleParameter { Name = "P_SortOrder", DataType = OracleDbType.VarChar, Direction = ParameterDirection.Input, Value = transactionDataModel.SortModel[0].Sort };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));
            parameters.Add(this.ParameterBuilder(UserProfileHelper.GetUserParameter()));
            var costChanges = await this.GetProcedure2<TransactionDataModel>(recordObject, packageParameter, serviceDocumentResult, parameters);
            transactionDataModel.TotalRows = (this.Parameters["p_total"].Value != null && this.Parameters["p_total"].Value != DBNull.Value) ? Convert.ToInt64(this.Parameters["p_total"].Value) : 1;
            return costChanges;
        }

        /// <summary>Transactions the domain data get.</summary>
        /// <returns>Transaction Type Data</returns>
        public async Task<List<TransactionTypeDomainModel>> TransactionDomainDataGet()
        {
            List<TransactionTypeDomainModel> transactionType = null;
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("TRAN_CODE_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(FinanceConstants.ObjTypeForTransactionType, FinanceConstants.ProcForTransactionType);
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            parameters.Add(this.ParameterBuilder(UserProfileHelper.GetUserParameter()));
            transactionType = await this.GetProcedure2<TransactionTypeDomainModel>(recordObject, packageParameter, this.serviceDocumentResult, parameters);

            return transactionType;
        }

        /// <summary>Sets the parameters trnsaction search.</summary>
        /// <param name="transactionModel">The transaction model.</param>
        /// <returns>Oracle Object</returns>
        public virtual OracleObject SetParamsTrnsactionSearch(TransactionDataModel transactionModel)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(FinanceConstants.RecObjTypeForTransactionSearch, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            if (transactionModel.FromDate != null)
            {
                recordObject[recordType.Attributes["FROM_DATE"]] = transactionModel.FromDate;
            }
            else
            {
                recordObject[recordType.Attributes["FROM_DATE"]] = DBNull.Value;
            }

            if (transactionModel.ToDate != null)
            {
                recordObject[recordType.Attributes["TO_DATE"]] = transactionModel.ToDate;
            }
            else
            {
                recordObject[recordType.Attributes["TO_DATE"]] = DBNull.Value;
            }

            if (transactionModel.Category != null)
            {
                recordObject[recordType.Attributes["Category"]] = transactionModel.Category;
            }
            else
            {
                recordObject[recordType.Attributes["Category"]] = DBNull.Value;
            }

            if (transactionModel.Location != null)
            {
                recordObject[recordType.Attributes["Location"]] = transactionModel.Location;
            }
            else
            {
                recordObject[recordType.Attributes["Location"]] = DBNull.Value;
            }

            if (transactionModel.Segment != null)
            {
                recordObject[recordType.Attributes["SEGMENT"]] = transactionModel.Segment;
            }
            else
            {
                recordObject[recordType.Attributes["SEGMENT"]] = DBNull.Value;
            }

            if (transactionModel.FineLine != null)
            {
                recordObject[recordType.Attributes["FINE_LINE"]] = transactionModel.FineLine;
            }
            else
            {
                recordObject[recordType.Attributes["FINE_LINE"]] = DBNull.Value;
            }

            recordObject[recordType.Attributes["TRAN_TYPE"]] = transactionModel.TransactionTypeId;
            recordObject[recordType.Attributes["ITEM"]] = transactionModel.ItemId;
            recordObject[recordType.Attributes["ITEM_TYPE"]] = transactionModel.ItemType;
            return recordObject;
        }
    }
}