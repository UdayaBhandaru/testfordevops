﻿// <copyright file="TransactionDataModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Finance.Models
{
    using System;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Core.Models;

    /// <summary>Transaction Data Model</summary>
    public class TransactionDataModel : ProfileEntity
    {
        /// <summary>Gets or sets from date.</summary>
        /// <value>From date.</value>
        public DateTime? FromDate { get; set; }

        /// <summary>Gets or sets the date.</summary>
        /// <value>The date.</value>
        public DateTime? Date { get; set; }

        /// <summary>Gets or sets the post date.</summary>
        /// <value>The post date.</value>
        public DateTime? PostDate { get; set; }

        /// <summary>Gets or sets to date.</summary>
        /// <value>To date.</value>
        public DateTime? ToDate { get; set; }

        /// <summary>Gets or sets the category.</summary>
        /// <value>The category.</value>
        public int? Category { get; set; }

        /// <summary>Gets or sets the name of the category.</summary>
        /// <value>The name of the category.</value>
        public string CategoryName { get; set; }

        /// <summary>Gets or sets the fine line.</summary>
        /// <value>The fine line.</value>
        public int? FineLine { get; set; }

        /// <summary>Gets or sets the fine line desc.</summary>
        /// <value>The fine line.</value>
        public string FineLineDescription { get; set; }

        /// <summary>Gets or sets the name of the location.</summary>
        /// <value>The name of the location.</value>
        public string LocationName { get; set; }

        /// <summary>Gets or sets the type of the location.</summary>
        /// <value>The type of the location.</value>
        public string LocationType { get; set; }

        /// <summary>Gets or sets the quantity.</summary>
        /// <value>The quantity.</value>
        public double? Quantity { get; set; }

        /// <summary>Gets or sets the total retail.</summary>
        /// <value>The total retail.</value>
        public double? TotalRetail { get; set; }

        /// <summary>Gets or sets the total cost.</summary>
        /// <value>The total cost.</value>
        public decimal? TotalCost { get; set; }

        /// <summary>Gets or sets the currency.</summary>
        /// <value>The currency.</value>
        public string Currency { get; set; }

        /// <summary>Gets or sets the location.</summary>
        /// <value>The location.</value>
        public int? Location { get; set; }

        /// <summary>Gets or sets the segment.</summary>
        /// <value>The segment.</value>
        public int? Segment { get; set; }

        /// <summary>Gets or sets the item identifier.</summary>
        /// <value>The item identifier.</value>
        public string ItemId { get; set; }

        /// <summary>Gets or sets the item.</summary>
        /// <value>The item.</value>
        public string Item { get; set; }

        /// <summary>Gets or sets the type of the item.</summary>
        /// <value>The type of the item.</value>
        public string ItemType { get; set; }

        /// <summary>Gets or sets the transaction type identifier.</summary>
        /// <value>The transaction type identifier.</value>
        public string TransactionTypeId { get; set; }

        /// <summary>Gets or sets the type of the transaction.</summary>
        /// <value>The type of the transaction.</value>
        public string TransactionType { get; set; }

        /// <summary>Gets or sets gets the sort model.</summary>
        /// <value>The sort model.</value>
        public SortModel[] SortModel { get; set; }

        /// <summary>Gets or sets the start row.</summary>
        /// <value>The start row.</value>
        public int StartRow { get; set; } = 1;

        /// <summary>Gets or sets the end row.</summary>
        /// <value>The end row.</value>
        public int EndRow { get; set; } = 100;

        /// <summary>Gets or sets the total rows.</summary>
        /// <value>The total rows.</value>
        /// <summary>Total Rows</summary>
        public long? TotalRows { get; set; }

        /// <summary>Gets or sets the segment.</summary>
        /// <value>The segment.</value>
        public string SegmentDescription { get; set; }
    }
}
