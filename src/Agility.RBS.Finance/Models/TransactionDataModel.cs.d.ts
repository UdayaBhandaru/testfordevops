declare module server {
	/** Transaction Data Model */
	interface transactionDataModel extends profileEntity {
		/** Gets or sets from date. */
		fromDate?: Date;
		/** Gets or sets the date. */
		date?: Date;
		postDate?: Date;
		/** Gets or sets to date. */
		toDate?: Date;
		/** Gets or sets the category. */
		category?: number;
		/** Gets or sets the name of the category. */
		categoryName: string;
		/** Gets or sets the fine line. */
		fineLine?: number;
		fineLineDescription: string;
		/** Gets or sets the name of the location. */
		locationName: string;
		/** Gets or sets the type of the location. */
		locationType: string;
		/** Gets or sets the quantity. */
		quantity?: number;
		/** Gets or sets the total retail. */
		totalRetail?: number;
		/** Gets or sets the total cost. */
		totalCost?: number;
		/** Gets or sets the currency. */
		currency: string;
		/** Gets or sets the location. */
		location?: number;
		/** Gets or sets the segment. */
		segment?: number;
		/** Gets or sets the item identifier. */
		itemId: string;
		/** Gets or sets the item. */
		item: string;
		/** Gets or sets the type of the item. */
		itemType: string;
		/** Gets or sets the transaction type identifier. */
		transactionTypeId: string;
		/** Gets or sets the type of the transaction. */
		transactionType: string;
		/** Gets or sets gets the sort model. */
		sortModel: {
			colId: string;
			sort: string;
		}[];
		/** Gets or sets the start row. */
		startRow: number;
		/** Gets or sets the end row. */
		endRow: number;
		/** Gets or sets the total rows. */
		totalRows?: number;
		segmentDescription: string;
	}
}
