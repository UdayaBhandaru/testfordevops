﻿// <copyright file="TransactionTypeDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Finance.Models
{
    /// <summary>
    ///   <para>Transaction Type Data Model</para>
    /// </summary>
    public class TransactionTypeDomainModel
    {
        /// <summary>Gets or sets the code.</summary>
        /// <value>The code.</value>
        public int? Code { get; set; }

        /// <summary>Gets or sets the code desciption.</summary>
        /// <value>The code desciption.</value>
        public string CodeDesciption { get; set; }
    }
}
