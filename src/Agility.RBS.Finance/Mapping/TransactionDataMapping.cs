﻿// <copyright file="TransactionDataMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Finance.Mapping
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Agility.RBS.Core;
    using Agility.RBS.Finance.Models;
    using Agility.RBS.MDM.MerchantHierarchy.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    /// <summary>
    ///   <para>Transaction Data Mapping</para>
    /// </summary>
    public class TransactionDataMapping : Profile
    {
        /// <summary>Initializes a new instance of the <see cref="TransactionDataMapping"/> class.</summary>
        public TransactionDataMapping()
            : base("TransactionDataMapping")
        {
            this.CreateMap<OracleObject, TransactionTypeDomainModel>()
                .ForMember(m => m.Code, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["CODE"])))
                .ForMember(m => m.CodeDesciption, opt => opt.MapFrom(r => r["CODE_DESC"]));

            this.CreateMap<OracleObject, TransactionDataModel>()
                .ForMember(m => m.TotalCost, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["TOTAL_COST"])))
                .ForMember(m => m.TotalRetail, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["TOTAL_RETAIL"])))
                .ForMember(m => m.Quantity, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["QUANTITY"])))
                .ForMember(m => m.PostDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["POST_DATE"])))
                .ForMember(m => m.TransactionType, opt => opt.MapFrom(r => r["TRAN_TYPE_DESC"]))
                .ForMember(m => m.Item, opt => opt.MapFrom(r => r["ITEM"]))
                .ForMember(m => m.Date, opt => opt.MapFrom(r => r["FROM_DATE"]))
                .ForMember(m => m.LocationName, opt => opt.MapFrom(r => r["LOCATION_NAME"]))
                .ForMember(m => m.LocationType, opt => opt.MapFrom(r => r["LOC_TYPE_DESC"]))
                .ForMember(m => m.Currency, opt => opt.MapFrom(r => r["CURRENCY_CODE"]))
                ;
        }
    }
}
