﻿// <copyright file="FinanceStartup.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Finance
{
    using System;
    using System.IO;
    using System.Reflection;
    using Agility.Framework.Core;
    using Agility.RBS.Finance.Repositories;
    using AutoMapper;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Caching.Distributed;
    using Microsoft.Extensions.Caching.Memory;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using Swashbuckle.AspNetCore.Swagger;

    /// <summary>
    ///   <para>Finance Start up</para>
    /// </summary>
    public class FinanceStartup : ComponentStartup<DbContext>
    {
        /// <summary>Startups the specified configuration.</summary>
        /// <param name="configuration">The configuration.</param>
        public void Startup(IConfigurationRoot configuration)
        {
            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<Mapping.TransactionDataMapping>();
                this.InitilizeMapping(cfg);
            });
        }

        /// <summary>Configures the services.</summary>
        /// <param name="services">The services.</param>
        public override void ConfigureServices(IServiceCollection services)
        {
            base.ConfigureServices(services);
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("finance", new Info { Title = "My API", Version = "v1" });
                var xmlFile = $"{this.GetType().GetTypeInfo().Assembly.GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }

        /// <summary>Configures the specified application.</summary>
        /// <param name="app">The application.</param>
        /// <param name="env">The env.</param>
        /// <param name="loggerFactory">The logger factory.</param>
        /// <param name="distributedCache">The distributed cache.</param>
        /// <param name="memoryCache">The memory cache.</param>
        /// <param name="serviceProvider">The service provider.</param>
        public override void Configure(
           IApplicationBuilder app,
           IHostingEnvironment env,
           ILoggerFactory loggerFactory,
           IDistributedCache distributedCache,
           IMemoryCache memoryCache,
           IServiceProvider serviceProvider)
        {
            base.Configure(app, env, loggerFactory, distributedCache, memoryCache, serviceProvider);
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("//swagger//finance//swagger.json", "My API v1");
                c.SupportedSubmitMethods();
            });
        }

        /// <summary>Configures the dependency injection.</summary>
        /// <param name="services">The services.</param>
        public override void ConfigureDependencyInjection(IServiceCollection services)
        {
            services.AddTransient<TransactionDataRepository>();
        }
    }
}