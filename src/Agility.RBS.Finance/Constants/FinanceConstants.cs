﻿// <copyright file="FinanceConstants.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Finance.Constants
{
    /// <summary>
    /// Finance Module Constants
    /// </summary>
    internal static class FinanceConstants
    {
        public const string GetTransactionDataPackage = "TRANDATA_SEARCH";
        public const string ProcForTransactionType = "GETTRANCODES";
        public const string ObjTypeForTransactionType = "TRAN_CODE_TAB";
        public const string ProcForTransactionSearch = "GETTRANDATA_LIST";
        public const string ObjTypeForTransactionSearch = "TRAN_DATA_SEARCH_TAB";
        public const string RecObjTypeForTransactionSearch = "TRAN_DATA_SEARCH_REC";
        public const string DomainHdrIndicator = "RCST";
    }
}
