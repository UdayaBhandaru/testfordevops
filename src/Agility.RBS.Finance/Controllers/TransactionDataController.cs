﻿// <copyright file="TransactionDataController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.SupplyChainManagement.SupplyChain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core.Models;
    using Agility.RBS.Finance.Constants;
    using Agility.RBS.Finance.Models;
    using Agility.RBS.Finance.Repositories;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Models;
    using Devart.Data.Oracle;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Transaction Data Controller
    /// </summary>
    [Route("api/[controller]/[action]")]
    public class TransactionDataController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly ServiceDocument<TransactionDataModel> serviceDocument;
        private readonly TransactionDataRepository transactionDataRepository;

        /// <summary>
       /// Initializes a new instance of the <see cref="TransactionDataController"/> class.
        /// </summary>
        /// <param name="serviceDocument">service document</param>
        /// <param name="transactionDataRepository">transaction data repository</param>
        /// <param name="domainDataRepository">domain data repository</param>
        public TransactionDataController(
            ServiceDocument<TransactionDataModel> serviceDocument,
            TransactionDataRepository transactionDataRepository,
            DomainDataRepository domainDataRepository)
        {
            this.serviceDocument = serviceDocument;
            this.transactionDataRepository = transactionDataRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        /// <summary>
        /// This API invoked for loading grid data along with data for autocomplete, radio button fields in Shipment SKU Page.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<TransactionDataModel>> List()
        {
            return await this.TransactionDataDomainData();
        }

        /// <summary>
        /// This API invoked for saving grid data in the Shipment SKU page when Save button clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<TransactionDataModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.TransactionDataSearch);

            return this.serviceDocument;
        }

        /// <summary>
        /// Transaction Domain Data
        /// </summary>
        /// <returns>Transaction Domain Data Service document</returns>
        private async Task<ServiceDocument<TransactionDataModel>> TransactionDataDomainData()
        {
            var itemType = await this.domainDataRepository.DomainDetailData(FinanceConstants.DomainHdrIndicator);
            this.serviceDocument.DomainData.Add("location", await this.domainDataRepository.LocationDomainGet());
            this.serviceDocument.DomainData.Add("category", await this.domainDataRepository.DepartmentDomainGet());
            this.serviceDocument.DomainData.Add("transactionType", await this.transactionDataRepository.TransactionDomainDataGet());
            this.serviceDocument.DomainData.Add("fineline", await this.domainDataRepository.ClassDomainGet());
            this.serviceDocument.DomainData.Add("itemType", itemType.Where(x => x.CodeDesc != "Child Item"));
            return this.serviceDocument;
        }

        /// <summary>
        /// Gives Transaction Search Data
        /// </summary>
        /// <returns>Transaction Search Data</returns>
        private async Task<List<TransactionDataModel>> TransactionDataSearch()
        {
            try
            {
                var subClasses = await this.transactionDataRepository.GetTransactionDataResult(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return subClasses;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }
    }
}
