﻿//-------------------------------------------------------------------------------------------------
// <copyright file="DomainRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// DomainRepository
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.Api.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.RBS.Api.Api.Domain.Models;
    using Devart.Data.Oracle;

    public class DomainRepository : BaseRepository
    {
        public DomainRepository()
        {
            this.PackageName = "RETAIL.UTL_DOMAIN";
        }

        public List<DomainHeaderModel> Getutldomain(DomainHeaderModel domainHeaderModel)
        {
            this.Connection.Open();
            OracleType tableType = OracleType.GetObjectType("UTL_DMN_HDR_TAB", this.Connection);
            OracleType recordType = OracleType.GetObjectType("UTL_DMN_HDR_REC", this.Connection);
            var recordObject = new OracleObject(recordType);

            OracleType dtlTableType = OracleType.GetObjectType("UTL_DMN_DTL_TAB", this.Connection);
            OracleType dtlRecordType = OracleType.GetObjectType("UTL_DMN_DTL_REC", this.Connection);
            var dtlRecordObject = new OracleObject(dtlRecordType);
            recordObject[recordType.Attributes["DOMAIN_ID"]] = domainHeaderModel.DomainId;
            recordObject[recordType.Attributes["DOMAIN_DESC"]] = domainHeaderModel.DomainDesc;
            recordObject[recordType.Attributes["DOMAIN_STATUS"]] = domainHeaderModel.DomainStatus;

            OracleTable pUtlDmnHdrTab = new OracleTable(tableType);
            pUtlDmnHdrTab.Add(recordObject);

            OracleTable pUtlDmnDtlTab = new OracleTable(dtlTableType);
            pUtlDmnDtlTab.Add(dtlRecordObject);

            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();
            OracleParameter parameter;
            parameter = new OracleParameter("RESULT", Devart.Data.Oracle.OracleDbType.Boolean);
            parameter.Direction = System.Data.ParameterDirection.ReturnValue;
            parameters.Add(parameter);
            parameter = new OracleParameter("P_UTL_DMN_HDR_TAB", Devart.Data.Oracle.OracleDbType.Table);
            parameter.Direction = System.Data.ParameterDirection.InputOutput;
            parameter.ObjectTypeName = "RETAIL.UTL_DMN_HDR_TAB";
            parameter.Value = pUtlDmnHdrTab;
            parameters.Add(parameter);
            parameter = new OracleParameter("P_UTL_DMN_DTL_TAB", Devart.Data.Oracle.OracleDbType.Table);
            parameter.Direction = System.Data.ParameterDirection.Output;
            parameter.ObjectTypeName = "RETAIL.UTL_DMN_DTL_TAB";
            parameters.Add(parameter);
            this.ExecuteProcedure("GETUTLDOMAIN", parameters);

            List<DomainHeaderModel> listOfDomainHeader = new List<DomainHeaderModel>();
            List<DomainDetailModel> listOfDomainDetail = new List<DomainDetailModel>();
            if (this.Parameters["P_UTL_DMN_HDR_TAB"].Value != System.DBNull.Value)
            {
                pUtlDmnHdrTab = (Devart.Data.Oracle.OracleTable)this.Parameters["P_UTL_DMN_HDR_TAB"].Value;

                DomainHeaderModel domainHeader;
                if (pUtlDmnHdrTab != null)
                {
                    OracleObject[] domainHeaders = new OracleObject[pUtlDmnHdrTab.Count];
                    for (int i = 0; i < pUtlDmnHdrTab.Count; i++)
                    {
                        domainHeaders[i] = (OracleObject)pUtlDmnHdrTab[i];
                        OracleObject obj = domainHeaders[i];
                        if (obj != null)
                        {
                            domainHeader = new DomainHeaderModel()
                            {
                                DomainId = Convert.ToString(obj["DOMAIN_ID"]),
                                DomainDesc = Convert.ToString(obj["DOMAIN_DESC"]),
                                DomainStatus = Convert.ToString(obj["DOMAIN_STATUS"]),
                                DomainStatusDesc = Convert.ToString(obj["DOMAIN_DETAIL_DESC"]),
                                CreatedBy = Convert.ToString(obj["CREATED_UPDATED_BY"]),
                                ModifiedBy = Convert.ToString(obj["LAST_UPDATED_BY"]),
                                CreatedDate = Convert.ToDateTime(obj["CREATED_UPDATED_DATE"]),
                                ModifiedDate = Convert.ToDateTime(obj["LAST_UPDATED_DATE"]),
                                TableName = "UTL_DOMAIN_HDR"
                            };

                            listOfDomainHeader.Add(domainHeader);
                        }
                    }
                }
            }

            if (this.Parameters["P_UTL_DMN_DTL_TAB"].Value != System.DBNull.Value)
            {
                pUtlDmnDtlTab = (Devart.Data.Oracle.OracleTable)this.Parameters["P_UTL_DMN_DTL_TAB"].Value;

                DomainDetailModel domainDetail;
                if (pUtlDmnDtlTab != null)
                {
                    OracleObject[] domainDetails = new OracleObject[pUtlDmnDtlTab.Count];
                    for (int i = 0; i < pUtlDmnDtlTab.Count; i++)
                    {
                        domainDetails[i] = (OracleObject)pUtlDmnDtlTab[i];
                        OracleObject obj = domainDetails[i];
                        if (obj != null)
                        {
                            domainDetail = new DomainDetailModel();
                            domainDetail.DomainId = obj["DOMAIN_ID"].ToString();
                            domainDetail.DomainDetailId = obj["DOMAIN_DETAIL_ID"].ToString();
                            domainDetail.DomainDetailDesc = obj["DOMAIN_DETAIL_DESC"].ToString();
                            domainDetail.DomainDetailDefault = obj["DOMAIN_DETAIL_DEFAULT"].ToString();
                            domainDetail.DomainStatus = obj["DOMAIN_STATUS"].ToString();
                            domainDetail.DomainDetailSeq = Convert.ToInt32(obj["DOMAIN_DETAIL_SEQ"]);
                            domainDetail.CreatedBy = obj["CREATED_UPDATED_BY"].ToString();
                            domainDetail.CreatedDate = (DateTime)obj["CREATED_UPDATED_DATE"];
                            domainDetail.ModifiedBy = obj["LAST_UPDATED_BY"].ToString();
                            domainDetail.ModifiedDate = (DateTime)obj["LAST_UPDATED_DATE"];
                            domainDetail.Operation = "U";
                            listOfDomainDetail.Add(domainDetail);
                        }
                    }
                }
            }

            foreach (DomainHeaderModel header in listOfDomainHeader)
            {
                header.DomainDetails.AddRange(listOfDomainDetail.FindAll(x => x.DomainId == header.DomainId));
            }

            if (this.Parameters["Result"].Value == System.DBNull.Value)
            {
                return null;
            }
            else
            {
                return listOfDomainHeader;
            }
        }

        public List<DomainDetailModel> GetUtlDmnStatDtls(string pHeaderId)
        {
            this.Connection.Open();
            OracleType tableType = OracleType.GetObjectType("UTL_DMN_DTL_TAB", this.Connection);
            OracleType recordType = OracleType.GetObjectType("UTL_DMN_DTL_REC", this.Connection);
            var recordObject = new OracleObject(recordType);
            OracleTable pUtlDmnDtlTab = new OracleTable(tableType);
            pUtlDmnDtlTab.Add(recordObject);

            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();
            OracleParameter parameter;
            parameter = new OracleParameter("RESULT", Devart.Data.Oracle.OracleDbType.Boolean);
            parameter.Direction = System.Data.ParameterDirection.ReturnValue;
            parameters.Add(parameter);
            parameter = new OracleParameter("P_HEADER_ID", Devart.Data.Oracle.OracleDbType.VarChar);
            parameter.Direction = System.Data.ParameterDirection.Input;
            parameter.Value = pHeaderId;
            parameters.Add(parameter);
            parameter = new OracleParameter("P_UTL_DMN_DTL_TAB", Devart.Data.Oracle.OracleDbType.Table);
            parameter.Direction = System.Data.ParameterDirection.Output;
            parameter.ObjectTypeName = "RETAIL.UTL_DMN_DTL_TAB";
            parameters.Add(parameter);
            this.ExecuteProcedure("GETUTLDMNSTATDTLS", parameters);
            this.Connection.Close();

            List<DomainDetailModel> listOfDomainDetail = new List<DomainDetailModel>();
            if (this.Parameters["P_UTL_DMN_DTL_TAB"].Value != System.DBNull.Value)
            {
                pUtlDmnDtlTab = (Devart.Data.Oracle.OracleTable)this.Parameters["P_UTL_DMN_DTL_TAB"].Value;
                DomainDetailModel domainDetail;
                if (pUtlDmnDtlTab != null)
                {
                    OracleObject[] domainDetails = new OracleObject[pUtlDmnDtlTab.Count];
                    for (int i = 0; i < pUtlDmnDtlTab.Count; i++)
                    {
                        domainDetails[i] = (OracleObject)pUtlDmnDtlTab[i];
                        OracleObject obj = domainDetails[i];
                        if (obj != null)
                        {
                            domainDetail = new DomainDetailModel();
                            domainDetail.DomainId = obj["DOMAIN_ID"].ToString();
                            domainDetail.DomainDetailId = obj["DOMAIN_DETAIL_ID"].ToString();
                            domainDetail.DomainDetailDesc = obj["DOMAIN_DETAIL_DESC"].ToString();
                            domainDetail.DomainDetailDefault = obj["DOMAIN_DETAIL_DEFAULT"].ToString();
                            domainDetail.DomainStatus = obj["DOMAIN_STATUS"].ToString();
                            domainDetail.CreatedBy = obj["CREATED_UPDATED_BY"].ToString();
                            domainDetail.CreatedDate = (DateTime)obj["CREATED_UPDATED_DATE"];
                            domainDetail.ModifiedBy = obj["LAST_UPDATED_BY"].ToString();
                            domainDetail.ModifiedDate = (DateTime)obj["LAST_UPDATED_DATE"];
                            listOfDomainDetail.Add(domainDetail);
                        }
                    }
                }
            }

            if (this.Parameters["Result"].Value == System.DBNull.Value)
            {
                return new List<DomainDetailModel>();
            }
            else
            {
                return listOfDomainDetail;
            }
        }

        public bool Setutldomain(DomainHeaderModel domainHeaderModel, List<DomainDetailModel> domainHeaderDetails)
        {
            this.Connection.Open();
            OracleType tableType = OracleType.GetObjectType("UTL_DMN_HDR_TAB", this.Connection);
            OracleType recordType = OracleType.GetObjectType("UTL_DMN_HDR_REC", this.Connection);
            var recordObject = new OracleObject(recordType);

            recordObject[recordType.Attributes["DOMAIN_ID"]] = domainHeaderModel.DomainId;
            recordObject[recordType.Attributes["DOMAIN_DESC"]] = domainHeaderModel.DomainDesc;
            if (!string.IsNullOrEmpty(domainHeaderModel.DomainStatus))
            {
                recordObject[recordType.Attributes["DOMAIN_STATUS"]] = domainHeaderModel.DomainStatus;
            }
            else
            {
                recordObject[recordType.Attributes["DOMAIN_STATUS"]] = "A";
            }

            recordObject[recordType.Attributes["CREATED_UPDATED_BY"]] = "spulla";
            recordObject[recordType.Attributes["LAST_UPDATED_BY"]] = "spulla";
            recordObject[recordType.Attributes["Operation"]] = domainHeaderModel.Operation;

            OracleTable pUtlDmnHdrTab = new OracleTable(tableType);
            pUtlDmnHdrTab.Add(recordObject);

            OracleType dtlTableType = OracleType.GetObjectType("UTL_DMN_DTL_TAB", this.Connection);
            OracleType dtlRecordType = OracleType.GetObjectType("UTL_DMN_DTL_REC", this.Connection);
            OracleTable pUtlDmnDtlTab = new OracleTable(dtlTableType);

            for (int i = 0; i < domainHeaderDetails.Count; i++)
            {
                var dtlRecordObject = new OracleObject(dtlRecordType);
                dtlRecordObject[dtlRecordType.Attributes["DOMAIN_ID"]] = domainHeaderModel.DomainId;
                if (!string.IsNullOrEmpty(domainHeaderDetails[i].DomainDetailId))
                {
                    dtlRecordObject[dtlRecordType.Attributes["DOMAIN_DETAIL_ID"]] = domainHeaderDetails[i].DomainDetailId;
                }

                if (!string.IsNullOrEmpty(domainHeaderDetails[i].DomainDetailDesc))
                {
                    dtlRecordObject[dtlRecordType.Attributes["DOMAIN_DETAIL_DESC"]] = domainHeaderDetails[i].DomainDetailDesc;
                }

                if (!string.IsNullOrEmpty(domainHeaderDetails[i].DomainDetailSeq.ToString()))
                {
                    dtlRecordObject[dtlRecordType.Attributes["DOMAIN_DETAIL_SEQ"]] = domainHeaderDetails[i].DomainDetailSeq;
                }

                if (!string.IsNullOrEmpty(domainHeaderDetails[i].DomainDetailDefault))
                {
                    dtlRecordObject[dtlRecordType.Attributes["DOMAIN_DETAIL_DEFAULT"]] = domainHeaderDetails[i].DomainDetailDefault;
                }

                if (!string.IsNullOrEmpty(domainHeaderDetails[i].DomainStatus))
                {
                    dtlRecordObject[dtlRecordType.Attributes["DOMAIN_STATUS"]] = domainHeaderDetails[i].DomainStatus;
                }

                dtlRecordObject[dtlRecordType.Attributes["CREATED_UPDATED_BY"]] = "spulla";
                dtlRecordObject[dtlRecordType.Attributes["LAST_UPDATED_BY"]] = "spulla";
                dtlRecordObject[dtlRecordType.Attributes["Operation"]] = domainHeaderDetails[i].Operation;
                pUtlDmnDtlTab.Add(dtlRecordObject);
            }

            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();
            OracleParameter parameter;
            parameter = new OracleParameter("P_UTL_DMN_HDR_TAB", Devart.Data.Oracle.OracleDbType.Table);
            parameter.Direction = System.Data.ParameterDirection.InputOutput;
            parameter.ObjectTypeName = "RETAIL.UTL_DMN_HDR_TAB";
            parameter.Value = pUtlDmnHdrTab;
            parameters.Add(parameter);
            parameter = new OracleParameter("P_UTL_DMN_DTL_TAB", Devart.Data.Oracle.OracleDbType.Table);
            parameter.Direction = System.Data.ParameterDirection.InputOutput;
            parameter.ObjectTypeName = "RETAIL.UTL_DMN_DTL_TAB";
            parameter.Value = pUtlDmnDtlTab;
            parameters.Add(parameter);
            this.ExecuteProcedure("SETUTLDOMAIN", parameters);

            if (this.Parameters["P_UTL_DMN_HDR_TAB"].Value != System.DBNull.Value && this.Parameters["P_UTL_DMN_DTL_TAB"].Value != System.DBNull.Value)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public async ValueTask<bool> IsExistingDomain(string name)
        {
            int result;
            OracleCommand myCommand = new OracleCommand("Select count(1) from UTL_DOMAIN_HDR where DOMAIN_ID='" + name + "'");
            myCommand.Connection = this.Connection;
            this.Connection.Open();
            result = Convert.ToInt32(await myCommand.ExecuteScalarAsync());
            this.Connection.Close();
            return result > 0;
        }
    }
}
