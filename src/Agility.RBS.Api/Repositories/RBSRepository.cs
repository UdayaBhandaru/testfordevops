﻿//-------------------------------------------------------------------------------------------------
// <copyright file="RbsRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// All Cargo related application specific DB Operations
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.Api.Repositories
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Repositories;
    using Agility.Framework.Core.Security.Models;
    using Agility.RBS.Api.Account.Models;
    using Microsoft.EntityFrameworkCore;

    public class RbsRepository : BaseRepository<RbsDbContext>
    {
        public RbsRepository(RbsDbContext dbContext)
        {
            this.DbContext = dbContext;
        }

        public async Task<List<UserProfileModel>> UserProfilesGet(int companyId = 0)
        {
            var query = from users in this.DbContext.UserProfiles
                        where users.DeletedInd.Equals(false)
                        select new UserProfileModel
                        {
                            UserId = users.UserProfileId,
                            Name = users.FirstName,
                            Email = users.EmailAddress,
                            OrganizationId = users.OrganizationId != null ? users.OrganizationId.Value : 0
                        };

            return await query.ToListAsync();
        }
    }
}
