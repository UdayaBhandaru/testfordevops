﻿//-------------------------------------------------------------------------------------------------
// <copyright file="LocationEntity.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// Location DB Model
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.Api.Repositories.Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Agility.Framework.Core.Profile.Entities;

    [Table("Location", Schema = "cargo")]
    public class LocationEntity : BaseEntity
    {
        [Key]
        public int LocationId { get; set; }

        public string Name { get; set; }

        public string LocationCode { get; set; }
    }
}
