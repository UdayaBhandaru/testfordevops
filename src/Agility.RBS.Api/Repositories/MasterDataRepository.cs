﻿// <copyright file="MasterDataRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Agility.RBS.Api.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Agility.RBS.Api.Api.Domain.Models;
    using Agility.RBS.Api.Models;

    public class MasterDataRepository
    {
        private readonly DomainRepository domainRepository;
        private readonly MerchantHierarchyRepository merchantHierarchyRepository;
        private readonly RbsRepository repository;
        private readonly UomRepository uomRepository;

        public MasterDataRepository(MerchantHierarchyRepository merchantHierarchyRepository, DomainRepository domainRepository, RbsRepository repository, UomRepository uomRepository)
        {
            this.domainRepository = domainRepository;
            this.merchantHierarchyRepository = merchantHierarchyRepository;
            this.repository = repository;
            this.uomRepository = uomRepository;
        }

        // Loading Active UOM Class Master Data
        public async Task<List<UomClassModel>> UomClassList()
        {
            return await Task.Run(() => this.uomRepository.GetUomClass(new UomClassModel { UomClassStatus = "A" }));
        }

        // Loading Active UOM Master Data
        public async Task<List<UomModel>> UomList()
        {
            return await Task.Run(() => this.uomRepository.GetUom(new UomModel { Status = "A" }));
        }

        // Loading Active Doamin Master Data of Status
        public async Task<List<DomainDetailModel>> DomainDetailsStatus()
        {
            return await Task.Run(() => this.domainRepository.GetUtlDmnStatDtls("Status"));
        }

        // Loading Active Doamin Master Data of Indicator
        public async Task<List<DomainDetailModel>> DomainDetailsIndicator()
        {
            return await Task.Run(() => this.domainRepository.GetUtlDmnStatDtls("Indicator"));
        }

        // Loading Active Doamin Master Data of Calculation Type
        public async Task<List<DomainDetailModel>> DomainDetailsCalcType()
        {
            return await Task.Run(() => this.domainRepository.GetUtlDmnStatDtls("CalcType"));
        }

        // Loading Active Doamin Master Data of Strategy Type
        public async Task<List<DomainDetailModel>> DomainDetailsStrgyType()
        {
            return await Task.Run(() => this.domainRepository.GetUtlDmnStatDtls("StrgyType"));
        }

        // Loading Active Doamin Master Data of Space Level
        public async Task<List<DomainDetailModel>> DomainDetailsSpaceLevel()
        {
            return await Task.Run(() => this.domainRepository.GetUtlDmnStatDtls("SpaceLevel"));
        }

        // Loading Active Doamin Master Data of Operator
        public async Task<List<DomainDetailModel>> DomainDetailsOperator()
        {
            return await Task.Run(() => this.domainRepository.GetUtlDmnStatDtls("Operator"));
        }

        // Loading Active Division Master Data
        public async Task<List<DivisionModel>> DivisionList()
        {
            return await Task.Run(() => this.merchantHierarchyRepository.Getdivision(new DivisionModel() { DivisionStatus = "A" }));
        }

        // Loading Active Group Master Data
        public async Task<List<GroupModel>> GroupList()
        {
            return await Task.Run(() => this.merchantHierarchyRepository.Getgroup(new GroupModel() { GroupStatus = "A" }));
        }

        // Loading Active Department Master Data
        public async Task<List<DepartmentModel>> DepartmentList()
        {
            return await Task.Run(() => this.merchantHierarchyRepository.Getdepartment(new DepartmentModel() { DeptStatus = "A" }));
        }

        // Loading Active Class Master Data
        public async Task<List<ClassModel>> ClassList()
        {
            return await Task.Run(() => this.merchantHierarchyRepository.Getclass(new ClassModel() { ClassStatus = "A" }));
        }

        public async Task<List<Agility.RBS.Api.Account.Models.UserProfileModel>> UserList()
        {
            return await this.repository.UserProfilesGet();
        }
    }
}
