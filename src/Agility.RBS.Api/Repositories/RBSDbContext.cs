﻿//-------------------------------------------------------------------------------------------------
// <copyright file="RbsDbContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// Cargo related DBContext
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.Api.Repositories
{
    using Agility.Framework.Core.Repositories;
    using Agility.Framework.Web.Security.Entities;
    using Microsoft.EntityFrameworkCore;

    public class RbsDbContext : BaseDbContext<RbsDbContext>
    {
        public RbsDbContext(DbContextOptions<RbsDbContext> options)
            : base(options)
        {
        }

        public DbSet<UserProfileEntity> UserProfiles { get; set; }
    }
}
