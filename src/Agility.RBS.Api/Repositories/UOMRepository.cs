﻿// <copyright file="UOMRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Agility.RBS.Api.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.RBS.Api.Models;
    using Devart.Data.Oracle;

    public class UomRepository : BaseRepository
    {
        public UomRepository()
        {
            this.PackageName = "RETAIL.UTL_UOM";
        }

        public List<UomClassModel> GetUomClass(UomClassModel uomClassModel)
        {
            this.Connection.Open();
            OracleType tableType = OracleType.GetObjectType("UOM_CLASS_MST_TAB", this.Connection);
            OracleType recordType = OracleType.GetObjectType("UOM_CLASS_MST_REC", this.Connection);
            var recordObject = new OracleObject(recordType);
            if (string.IsNullOrEmpty(uomClassModel.UomClass) && string.IsNullOrEmpty(uomClassModel.UomClassDesc) && string.IsNullOrEmpty(uomClassModel.UomClassStatus))
            {
                recordObject[recordType.Attributes["UOM_CLASS"]] = "-1";
            }
            else
            {
                recordObject[recordType.Attributes["UOM_CLASS"]] = !string.IsNullOrEmpty(uomClassModel.UomClass) ? uomClassModel.UomClass.ToUpper() : uomClassModel.UomClass;
                recordObject[recordType.Attributes["UOM_CLASS_DESC"]] = uomClassModel.UomClassDesc;
                recordObject[recordType.Attributes["UOM_CLASS_STATUS"]] = uomClassModel.UomClassStatus;
            }

            OracleTable pUomClassMstTab = new OracleTable(tableType);
            pUomClassMstTab.Add(recordObject);
            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();
            OracleParameter parameter;
            parameter = new OracleParameter("RESULT", Devart.Data.Oracle.OracleDbType.Boolean);
            parameter.Direction = System.Data.ParameterDirection.ReturnValue;
            parameters.Add(parameter);
            parameter = new OracleParameter("P_UOM_CLASS_MST_TAB", Devart.Data.Oracle.OracleDbType.Table);
            parameter.Direction = System.Data.ParameterDirection.InputOutput;
            parameter.ObjectTypeName = "RETAIL.UOM_CLASS_MST_TAB";
            parameter.Value = pUomClassMstTab;
            parameters.Add(parameter);
            this.ExecuteProcedure("GETUOMCLASS", parameters);
            this.Connection.Close();

            var listOfUom = new List<UomClassModel>();
            if (this.Parameters["P_UOM_CLASS_MST_TAB"].Value != System.DBNull.Value)
            {
                pUomClassMstTab = (Devart.Data.Oracle.OracleTable)this.Parameters["P_UOM_CLASS_MST_TAB"].Value;

                UomClassModel uom;
                if (pUomClassMstTab != null)
                {
                    OracleObject[] uoms = new OracleObject[pUomClassMstTab.Count];
                    for (int i = 0; i < pUomClassMstTab.Count; i++)
                    {
                        uoms[i] = (OracleObject)pUomClassMstTab[i];
                        OracleObject obj = uoms[i];
                        if (obj != null)
                        {
                            uom = new UomClassModel()
                            {
                                UomClass = Convert.ToString(obj["UOM_CLASS"]),
                                UomClassDesc = Convert.ToString(obj["UOM_CLASS_DESC"]),
                                UomClassStatus = Convert.ToString(obj["UOM_CLASS_STATUS"]),
                                CreatedBy = Convert.ToString(obj["CREATED_UPDATED_BY"]),
                                ModifiedBy = Convert.ToString(obj["LAST_UPDATED_BY"]),
                                P_Error = Convert.ToString(obj["P_Error"]),
                                CreatedDate = Convert.ToDateTime(obj["CREATED_UPDATED_DATE"]),
                                ModifiedDate = Convert.ToDateTime(obj["LAST_UPDATED_DATE"]),
                                TableName = "UOM_CLASS_MST",
                                DomainDetailDesc = Convert.ToString(obj["DOMAIN_DETAIL_DESC"])
                            };
                            listOfUom.Add(uom);
                        }
                    }
                }
            }

            if (this.Parameters["Result"].Value == System.DBNull.Value)
            {
                return null;
            }
            else
            {
                return listOfUom;
            }
        }

        public bool SetUomClass(UomClassModel uomClassModel)
        {
            this.Connection.Open();
            OracleType tableType = OracleType.GetObjectType("UOM_CLASS_MST_TAB", this.Connection);
            OracleType recordType = OracleType.GetObjectType("UOM_CLASS_MST_REC", this.Connection);
            var recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["UOM_CLASS"]] = uomClassModel.UomClass.ToUpper();
            recordObject[recordType.Attributes["UOM_CLASS_DESC"]] = uomClassModel.UomClassDesc;
            recordObject[recordType.Attributes["UOM_CLASS_STATUS"]] = !string.IsNullOrEmpty(uomClassModel.UomClassStatus) ? uomClassModel.UomClassStatus : "A";
            recordObject[recordType.Attributes["Operation"]] = uomClassModel.Operation;
            recordObject[recordType.Attributes["CREATED_UPDATED_BY"]] = "spulla";
            recordObject[recordType.Attributes["LAST_UPDATED_BY"]] = "spulla";

            OracleTable pUomClassMstTab = new OracleTable(tableType);
            pUomClassMstTab.Add(recordObject);

            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();
            OracleParameter parameter;
            parameter = new OracleParameter("P_UOM_CLASS_MST_TAB", Devart.Data.Oracle.OracleDbType.Table);
            parameter.Direction = System.Data.ParameterDirection.InputOutput;
            parameter.ObjectTypeName = "RETAIL.UOM_CLASS_MST_TAB";
            parameter.Value = pUomClassMstTab;
            parameters.Add(parameter);
            this.ExecuteProcedure("SETUOMCLASS", parameters);
            return this.Parameters["P_UOM_CLASS_MST_TAB"].Value != System.DBNull.Value;
        }

        public List<UomModel> GetUom(UomModel uomModel)
        {
            this.Connection.Open();

            OracleType tableType = OracleType.GetObjectType("UOM_MST_TAB", this.Connection);
            OracleType recordType = OracleType.GetObjectType("UOM_MST_REC", this.Connection);
            var recordObject = new OracleObject(recordType);
            if (string.IsNullOrEmpty(uomModel.UomCode) && string.IsNullOrEmpty(uomModel.UomClass) && string.IsNullOrEmpty(uomModel.Status) && string.IsNullOrEmpty(uomModel.Description) && string.IsNullOrEmpty(uomModel.ShortDescription))
            {
                recordObject[recordType.Attributes["UOM"]] = "-1";
            }
            else
            {
                recordObject[recordType.Attributes["UOM"]] = !string.IsNullOrEmpty(uomModel.UomCode) ? uomModel.UomCode.ToUpper() : uomModel.UomCode;
                recordObject[recordType.Attributes["UOM_CLASS"]] = uomModel.UomClass;
                recordObject[recordType.Attributes["UOM_DESC"]] = uomModel.Description;
                recordObject[recordType.Attributes["UOM_SHORT_DESC"]] = uomModel.ShortDescription;
                recordObject[recordType.Attributes["UOM_STATUS"]] = uomModel.Status;
            }

            OracleTable pUomMstTab = new OracleTable(tableType);
            pUomMstTab.Add(recordObject);

            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();
            OracleParameter parameter;
            parameter = new OracleParameter("RESULT", Devart.Data.Oracle.OracleDbType.Boolean);
            parameter.Direction = System.Data.ParameterDirection.ReturnValue;
            parameters.Add(parameter);
            parameter = new OracleParameter("P_UOM_MST_TAB", Devart.Data.Oracle.OracleDbType.Table);
            parameter.Direction = System.Data.ParameterDirection.InputOutput;
            parameter.ObjectTypeName = "RETAIL.UOM_MST_TAB";
            parameter.Value = pUomMstTab;
            parameters.Add(parameter);
            this.ExecuteProcedure("GETUOM", parameters);
            this.Connection.Close();

            List<UomModel> listOfUom = new List<UomModel>();

            if (this.Parameters["P_UOM_MST_TAB"].Value != System.DBNull.Value)
            {
                pUomMstTab = (Devart.Data.Oracle.OracleTable)this.Parameters["P_UOM_MST_TAB"].Value;

                UomModel uom;
                if (pUomMstTab != null)
                {
                    OracleObject[] uoms = new OracleObject[pUomMstTab.Count];
                    for (int i = 0; i < pUomMstTab.Count; i++)
                    {
                        uoms[i] = (OracleObject)pUomMstTab[i];
                        OracleObject obj = uoms[i];
                        if (obj != null)
                        {
                            uom = new UomModel();
                            uom.UomCode = Convert.ToString(obj["UOM"]);
                            uom.UomClass = Convert.ToString(obj["UOM_CLASS"]);
                            uom.Status = Convert.ToString(obj["UOM_STATUS"]);
                            uom.DomainDetailDesc = Convert.ToString(obj["DOMAIN_DETAIL_DESC"]);

                            uom.Description = Convert.ToString(obj["UOM_DESC"]);
                            uom.ShortDescription = Convert.ToString(obj["UOM_SHORT_DESC"]);
                            if (!string.IsNullOrEmpty(Convert.ToString(obj["DECIMAL_PRECISION"])))
                            {
                                uom.DecimalPrecision = Convert.ToInt32(obj["DECIMAL_PRECISION"]);
                            }
                            else
                            {
                                uom.DecimalPrecision = 0;
                            }

                            uom.P_Error = Convert.ToString(obj["P_Error"]);
                            uom.CreatedBy = Convert.ToString(obj["CREATED_UPDATED_BY"]);
                            uom.ModifiedBy = Convert.ToString(obj["LAST_UPDATED_BY"]);
                            uom.CreatedDate = (DateTime)obj["CREATED_UPDATED_DATE"];
                            uom.ModifiedDate = (DateTime)obj["LAST_UPDATED_DATE"];
                            uom.TableName = "UOM_MST";
                            listOfUom.Add(uom);
                        }
                    }
                }
            }

            if (this.Parameters["Result"].Value == System.DBNull.Value)
            {
                return null;
            }
            else
            {
                return listOfUom;
            }
        }

        public bool SetUom(UomModel uomModel)
        {
            this.Connection.Open();
            OracleType tableType = OracleType.GetObjectType("UOM_MST_TAB", this.Connection);
            OracleType recordType = OracleType.GetObjectType("UOM_MST_REC", this.Connection);
            var recordObject = new OracleObject(recordType);
            var decimalPrecision = uomModel.DecimalPrecision ?? 0;

            recordObject[recordType.Attributes["UOM"]] = uomModel.UomCode.ToUpper();
            recordObject[recordType.Attributes["UOM_CLASS"]] = uomModel.UomClassModel.UomClass;
            recordObject[recordType.Attributes["UOM_DESC"]] = uomModel.Description;
            recordObject[recordType.Attributes["UOM_SHORT_DESC"]] = uomModel.ShortDescription;
            recordObject[recordType.Attributes["DECIMAL_PRECISION"]] = decimalPrecision;
            recordObject[recordType.Attributes["UOM_STATUS"]] = !string.IsNullOrEmpty(uomModel.Status) ? uomModel.Status : "A";

            recordObject[recordType.Attributes["Operation"]] = uomModel.Operation;
            recordObject[recordType.Attributes["CREATED_UPDATED_BY"]] = "spulla";
            recordObject[recordType.Attributes["LAST_UPDATED_BY"]] = "spulla";

            OracleTable pUomMstTab = new OracleTable(tableType);
            pUomMstTab.Add(recordObject);

            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();
            OracleParameter parameter;
            parameter = new OracleParameter("P_UOM_MST_TAB", Devart.Data.Oracle.OracleDbType.Table);
            parameter.Direction = System.Data.ParameterDirection.InputOutput;
            parameter.ObjectTypeName = "RETAIL.UOM_MST_TAB";
            parameter.Value = pUomMstTab;
            parameters.Add(parameter);
            this.ExecuteProcedure("SETUOM", parameters);
            return this.Parameters["P_UOM_MST_TAB"].Value != System.DBNull.Value;
        }

        public List<UomConversionModel> GetUomConversion(UomConversionModel uomConversionModel)
        {
            this.Connection.Open();

            OracleType tableType = OracleType.GetObjectType("UOM_CONV_MST_TAB", this.Connection);
            OracleType recordType = OracleType.GetObjectType("UOM_CONV_MST_REC", this.Connection);
            var recordObject = new OracleObject(recordType);

            if (string.IsNullOrEmpty(uomConversionModel.FromUom) && string.IsNullOrEmpty(uomConversionModel.ToUom))
            {
                recordObject[recordType.Attributes["FROM_UOM"]] = "-1";
            }
            else
            {
                recordObject[recordType.Attributes["FROM_UOM"]] = uomConversionModel.FromUom;
                recordObject[recordType.Attributes["TO_UOM"]] = uomConversionModel.ToUom;
            }

            OracleTable pUomConvMstTab = new OracleTable(tableType);
            pUomConvMstTab.Add(recordObject);

            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();
            OracleParameter parameter;
            parameter = new OracleParameter("RESULT", Devart.Data.Oracle.OracleDbType.Boolean);
            parameter.Direction = System.Data.ParameterDirection.ReturnValue;
            parameters.Add(parameter);
            parameter = new OracleParameter("P_UOM_CONV_MST_TAB", Devart.Data.Oracle.OracleDbType.Table);
            parameter.Direction = System.Data.ParameterDirection.InputOutput;
            parameter.ObjectTypeName = "RETAIL.UOM_CONV_MST_TAB";
            parameter.Value = pUomConvMstTab;
            parameters.Add(parameter);
            this.ExecuteProcedure("GETUOMCONVERSION", parameters);
            this.Connection.Close();

            List<UomConversionModel> listOfUom = new List<UomConversionModel>();

            if (this.Parameters["P_UOM_CONV_MST_TAB"].Value != System.DBNull.Value)
            {
                pUomConvMstTab = (Devart.Data.Oracle.OracleTable)this.Parameters["P_UOM_CONV_MST_TAB"].Value;

                UomConversionModel uom;
                if (pUomConvMstTab != null)
                {
                    OracleObject[] uoms = new OracleObject[pUomConvMstTab.Count];
                    for (int i = 0; i < pUomConvMstTab.Count; i++)
                    {
                        uoms[i] = (OracleObject)pUomConvMstTab[i];
                        OracleObject obj = uoms[i];
                        if (obj != null)
                        {
                            uom = new UomConversionModel();
                            uom.FromUom = Convert.ToString(obj["FROM_UOM"]);
                            uom.ToUom = Convert.ToString(obj["TO_UOM"]);
                            uom.Operator = Convert.ToString(obj["OPERATOR"]);
                            if (!string.IsNullOrEmpty(Convert.ToString(obj["FACTOR"])))
                            {
                                uom.Factor = Convert.ToDecimal(obj["FACTOR"]);
                            }
                            else
                            {
                                uom.Factor = 0;
                            }

                            uom.CreatedBy = Convert.ToString(obj["CREATED_UPDATED_BY"]);
                            uom.ModifiedBy = Convert.ToString(obj["LAST_UPDATED_BY"]);
                            uom.CreatedDate = (DateTime)obj["CREATED_UPDATED_DATE"];
                            uom.ModifiedDate = (DateTime)obj["LAST_UPDATED_DATE"];
                            uom.TableName = "UOM_CONV_MST";
                            listOfUom.Add(uom);
                        }
                    }
                }
            }

            return this.Parameters["Result"].Value == System.DBNull.Value ? null : listOfUom;
        }

        public bool SetUomConversion(UomConversionModel uomConversionModel)
        {
            this.Connection.Open();
            OracleType tableType = OracleType.GetObjectType("UOM_CONV_MST_TAB", this.Connection);
            OracleType recordType = OracleType.GetObjectType("UOM_CONV_MST_REC", this.Connection);
            var recordObject = new OracleObject(recordType);
            var decimalPrecision = uomConversionModel.Factor ?? 0;

            if (!string.IsNullOrEmpty(uomConversionModel.FromUom))
            {
                recordObject[recordType.Attributes["FROM_UOM"]] = uomConversionModel.FromUom;
            }

            if (!string.IsNullOrEmpty(uomConversionModel.ToUom))
            {
                recordObject[recordType.Attributes["TO_UOM"]] = uomConversionModel.ToUom;
            }

            if (decimalPrecision != 0)
            {
                recordObject[recordType.Attributes["FACTOR"]] = decimalPrecision;
            }

            if (!string.IsNullOrEmpty(uomConversionModel.Operator))
            {
                recordObject[recordType.Attributes["OPERATOR"]] = uomConversionModel.Operator;
            }

            recordObject[recordType.Attributes["Operation"]] = uomConversionModel.Operation;
            recordObject[recordType.Attributes["CREATED_UPDATED_BY"]] = "spulla";
            recordObject[recordType.Attributes["LAST_UPDATED_BY"]] = "spulla";

            OracleTable pUomConvMstTab = new OracleTable(tableType);
            pUomConvMstTab.Add(recordObject);

            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();
            OracleParameter parameter;
            parameter = new OracleParameter("P_UOM_CONV_MST_TAB", Devart.Data.Oracle.OracleDbType.Table);
            parameter.Direction = System.Data.ParameterDirection.InputOutput;
            parameter.ObjectTypeName = "RETAIL.UOM_CONV_MST_TAB";
            parameter.Value = pUomConvMstTab;
            parameters.Add(parameter);
            this.ExecuteProcedure("SETUOMCONVERSION", parameters);
            return this.Parameters["P_UOM_CONV_MST_TAB"].Value != System.DBNull.Value;
        }

        public async ValueTask<bool> IsExistingClass(string name)
        {
            int result;
            OracleCommand myCommand = new OracleCommand("Select count(1) from UOM_CLASS_MST where UOM_CLASS='" + name + "'");
            myCommand.Connection = this.Connection;
            this.Connection.Open();
            result = Convert.ToInt32(await myCommand.ExecuteScalarAsync());
            this.Connection.Close();
            return result > 0;
        }

        public async ValueTask<bool> IsExistingUOM(string name)
        {
            int result;
            OracleCommand myCommand = new OracleCommand("Select count(1) from UOM_MST where UOM ='" + name + "'");
            myCommand.Connection = this.Connection;
            this.Connection.Open();
            result = Convert.ToInt32(await myCommand.ExecuteScalarAsync());
            this.Connection.Close();
            return result > 0;
        }

        public async ValueTask<bool> IsExistingConversion(string fromUOM, string toUOM)
        {
            int result;
            OracleCommand myCommand = new OracleCommand("Select count(1) from UOM_CONV_MST where FROM_UOM='" + fromUOM + "' and TO_UOM='" + toUOM + "'");
            myCommand.Connection = this.Connection;
            this.Connection.Open();
            result = Convert.ToInt32(await myCommand.ExecuteScalarAsync());
            this.Connection.Close();
            return result > 0;
        }
    }
}
