﻿// <copyright file="BaseRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Agility.RBS.Api.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.Context;
    using Agility.RBS.Api.Common;
    using Devart.Data.Oracle;

    public class BaseRepository : OraclePackage
    {
        public BaseRepository()
        {
            this.Connection = new OracleConnection(Constants.ConnectionString);
        }
    }
}
