﻿// <copyright file="MerchantHierarchyRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Agility.RBS.Api.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.RBS.Api.Models;
    using Devart.Data.Oracle;

    public class MerchantHierarchyRepository : BaseRepository
    {
        public MerchantHierarchyRepository()
        {
            this.PackageName = "RETAIL.MERCH_HIERARCHY";
        }

        public List<CategoryRoleModel> Getcatrolemst(CategoryRoleModel categoryRoleModel)
        {
            this.Connection.Open();

            OracleType tableType = OracleType.GetObjectType("MERCH_CATROLE_TAB", this.Connection);
            OracleType recordType = OracleType.GetObjectType("MERCH_CATROLE_REC", this.Connection);
            var recordObject = new OracleObject(recordType);
            var groupId = categoryRoleModel.GroupId ?? 0;
            var deptId = categoryRoleModel.DeptId ?? 0;

            if (groupId == 0 && deptId == 0 && string.IsNullOrEmpty(categoryRoleModel.RoleLevel) && string.IsNullOrEmpty(categoryRoleModel.RoleId) && string.IsNullOrEmpty(categoryRoleModel.StrategyType) && string.IsNullOrEmpty(categoryRoleModel.CategoryRole) && string.IsNullOrEmpty(categoryRoleModel.CategoryStrategy))
            {
                recordObject[recordType.Attributes["GROUP_ID"]] = "-1";
            }
            else
            {
                if (groupId != 0)
                {
                    recordObject[recordType.Attributes["GROUP_ID"]] = categoryRoleModel.GroupId;
                }

                if (deptId != 0)
                {
                    recordObject[recordType.Attributes["DEPT_ID"]] = categoryRoleModel.DeptId;
                }

                if (!string.IsNullOrEmpty(categoryRoleModel.RoleLevel))
                {
                    recordObject[recordType.Attributes["ROLE_LEVEL"]] = categoryRoleModel.RoleLevel;
                }

                if (!string.IsNullOrEmpty(categoryRoleModel.RoleId))
                {
                    recordObject[recordType.Attributes["ROLE_ID"]] = categoryRoleModel.RoleId;
                }

                if (!string.IsNullOrEmpty(categoryRoleModel.StrategyType))
                {
                    recordObject[recordType.Attributes["STRATEGY_TYPE"]] = categoryRoleModel.StrategyType;
                }

                if (!string.IsNullOrEmpty(categoryRoleModel.CategoryRole))
                {
                    recordObject[recordType.Attributes["CATEGORY_ROLE"]] = categoryRoleModel.CategoryRole;
                }

                if (!string.IsNullOrEmpty(categoryRoleModel.CategoryStrategy))
                {
                    recordObject[recordType.Attributes["CATEGORY_STRATEGY"]] = categoryRoleModel.CategoryStrategy;
                }
            }

            OracleTable pMerchCatroleTab = new OracleTable(tableType);
            pMerchCatroleTab.Add(recordObject);

            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();
            OracleParameter parameter;
            parameter = new OracleParameter("RESULT", Devart.Data.Oracle.OracleDbType.Boolean);
            parameter.Direction = System.Data.ParameterDirection.ReturnValue;
            parameters.Add(parameter);
            parameter = new OracleParameter("P_MERCH_CATROLE_TAB", Devart.Data.Oracle.OracleDbType.Table);
            parameter.Direction = System.Data.ParameterDirection.InputOutput;
            parameter.ObjectTypeName = "RETAIL.MERCH_CATROLE_TAB";
            parameter.Value = pMerchCatroleTab;
            parameters.Add(parameter);
            this.ExecuteProcedure("GETCATROLEMST", parameters);
            this.Connection.Close();
            List<CategoryRoleModel> categoryRoles = new List<CategoryRoleModel>();
            if (this.Parameters["P_MERCH_CATROLE_TAB"].Value != System.DBNull.Value)
            {
                pMerchCatroleTab = (Devart.Data.Oracle.OracleTable)this.Parameters["P_MERCH_CATROLE_TAB"].Value;

                CategoryRoleModel categoryRole;
                if (pMerchCatroleTab != null)
                {
                    OracleObject[] catRoles = new OracleObject[pMerchCatroleTab.Count];
                    for (int i = 0; i < pMerchCatroleTab.Count; i++)
                    {
                        catRoles[i] = (OracleObject)pMerchCatroleTab[i];
                        OracleObject obj = catRoles[i];
                        if (obj != null)
                        {
                            categoryRole = new CategoryRoleModel();
                            categoryRole.GroupId = Convert.ToInt32(obj["GROUP_ID"]);
                            categoryRole.DeptId = Convert.ToInt32(obj["DEPT_ID"]);

                            categoryRole.RoleLevel = Convert.ToString(obj["ROLE_LEVEL"]);
                            categoryRole.RoleId = Convert.ToString(obj["ROLE_ID"]);
                            categoryRole.StrategyType = Convert.ToString(obj["STRATEGY_TYPE"]);
                            categoryRole.CategoryRole = Convert.ToString(obj["CATEGORY_ROLE"]);
                            categoryRole.CategoryStrategy = Convert.ToString(obj["CATEGORY_STRATEGY"]);

                            categoryRole.CreatedBy = Convert.ToString(obj["CREATED_UPDATED_BY"]);
                            categoryRole.ModifiedBy = Convert.ToString(obj["LAST_UPDATED_BY"]);

                            categoryRole.CreatedDate = Convert.ToDateTime(obj["CREATED_UPDATED_DATE"]);
                            categoryRole.ModifiedDate = Convert.ToDateTime(obj["LAST_UPDATED_DATE"]);
                            categoryRoles.Add(categoryRole);
                        }
                    }
                }
            }

            if (this.Parameters["Result"].Value == System.DBNull.Value)
            {
                return null;
            }
            else
            {
                return categoryRoles;
            }
        }

        public List<ClassModel> Getclass(ClassModel classModel)
        {
            this.Connection.Open();

            OracleType tableType = OracleType.GetObjectType("MERCH_CLASS_TAB", this.Connection);
            OracleType recordType = OracleType.GetObjectType("MERCH_CLASS_REC", this.Connection);
            var recordObject = new OracleObject(recordType);
            var companyId = classModel.CompanyId ?? 0;
            var deptId = classModel.DeptId ?? 0;
            var groupId = classModel.GroupId ?? 0;
            var classId = classModel.ClassId ?? 0;
            if (companyId == 0 && classId == 0 && groupId == 0 && deptId == 0 && string.IsNullOrEmpty(classModel.ClassDesc) && string.IsNullOrEmpty(classModel.ClassStatus))
            {
                recordObject[recordType.Attributes["COMPANY_ID"]] = "-1";
            }
            else
            {
                if (companyId != 0)
                {
                    recordObject[recordType.Attributes["COMPANY_ID"]] = classModel.CompanyId;
                }

                if (deptId != 0)
                {
                    recordObject[recordType.Attributes["DEPT_ID"]] = classModel.DeptId;
                }

                if (groupId != 0)
                {
                    recordObject[recordType.Attributes["GROUP_ID"]] = classModel.GroupId;
                }

                if (classId != 0)
                {
                    recordObject[recordType.Attributes["CLASS_ID"]] = classModel.ClassId;
                }

                recordObject[recordType.Attributes["CLASS_DESC"]] = classModel.ClassDesc;
                recordObject[recordType.Attributes["CLASS_STATUS"]] = classModel.ClassStatus;
            }

            OracleTable pMerchClassTab = new OracleTable(tableType);
            pMerchClassTab.Add(recordObject);

            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();
            OracleParameter parameter;
            parameter = new OracleParameter("RESULT", Devart.Data.Oracle.OracleDbType.Boolean);
            parameter.Direction = System.Data.ParameterDirection.ReturnValue;
            parameters.Add(parameter);
            parameter = new OracleParameter("P_MERCH_CLASS_TAB", Devart.Data.Oracle.OracleDbType.Table);
            parameter.Direction = System.Data.ParameterDirection.InputOutput;
            parameter.ObjectTypeName = "RETAIL.MERCH_CLASS_TAB";
            parameter.Value = pMerchClassTab;
            parameters.Add(parameter);
            this.ExecuteProcedure("GETCLASS", parameters);

            this.Connection.Close();
            List<ClassModel> classModels = new List<ClassModel>();

            if (this.Parameters["P_MERCH_CLASS_TAB"].Value != System.DBNull.Value)
            {
                pMerchClassTab = (Devart.Data.Oracle.OracleTable)this.Parameters["P_MERCH_CLASS_TAB"].Value;

                ClassModel classModel1;
                if (pMerchClassTab != null)
                {
                    OracleObject[] classes = new OracleObject[pMerchClassTab.Count];
                    for (int i = 0; i < pMerchClassTab.Count; i++)
                    {
                        classes[i] = (OracleObject)pMerchClassTab[i];
                        OracleObject obj = classes[i];
                        if (obj != null && obj["CREATED_UPDATED_BY"] != null)
                        {
                            classModel1 = new ClassModel()
                            {
                                GroupId = Convert.ToInt32(obj["GROUP_ID"]),
                                DeptId = Convert.ToInt32(obj["DEPT_ID"]),
                                CompanyId = Convert.ToInt32(obj["COMPANY_ID"]),
                                ClassId = Convert.ToInt32(obj["CLASS_ID"]),
                                ClassDesc = Convert.ToString(obj["CLASS_DESC"]),
                                BuyerId = Convert.ToString(obj["BUYER_ID"]),
                                MerchandiserId = Convert.ToString(obj["MERCHANDISER_ID"]),
                                ClassStatus = Convert.ToString(obj["CLASS_STATUS"]),
                                CreatedBy = Convert.ToString(obj["CREATED_UPDATED_BY"]),
                                ModifiedBy = Convert.ToString(obj["LAST_UPDATED_BY"]),
                                CreatedDate = Convert.ToDateTime(obj["CREATED_UPDATED_DATE"]),
                                ModifiedDate = Convert.ToDateTime(obj["LAST_UPDATED_DATE"]),
                                ClassStatusDesc = Convert.ToString(obj["CLASS_STATUS_DESC"]),
                                CompanyName = Convert.ToString(obj["COMPANY_NAME"]),
                                GroupDesc = Convert.ToString(obj["GROUP_DESC"]),
                                DeptDesc = Convert.ToString(obj["DEPT_DESC"]),
                                TableName = "CLASS_MST"
                            };
                            classModels.Add(classModel1);
                        }
                    }
                }
            }

            if (this.Parameters["Result"].Value == System.DBNull.Value)
            {
                return null;
            }
            else
            {
                return classModels;
            }
        }

        public List<DepartmentModel> Getdepartment(DepartmentModel departmentModel)
        {
            this.Connection.Open();

            OracleType tableType = OracleType.GetObjectType("MERCH_DEPT_TAB", this.Connection);
            OracleType recordType = OracleType.GetObjectType("MERCH_DEPT_REC", this.Connection);
            var recordObject = new OracleObject(recordType);
            var companyId = departmentModel.CompanyId ?? 0;
            var deptId = departmentModel.DeptId ?? 0;
            var groupId = departmentModel.GroupId ?? 0;

            if (companyId == 0 && groupId == 0 && deptId == 0 && string.IsNullOrEmpty(departmentModel.DeptDesc) && string.IsNullOrEmpty(departmentModel.DeptStatus))
            {
                recordObject[recordType.Attributes["COMPANY_ID"]] = "-1";
            }
            else
            {
                if (companyId != 0)
                {
                    recordObject[recordType.Attributes["COMPANY_ID"]] = departmentModel.CompanyId;
                }

                if (deptId != 0)
                {
                    recordObject[recordType.Attributes["DEPT_ID"]] = departmentModel.DeptId;
                }

                if (groupId != 0)
                {
                    recordObject[recordType.Attributes["GROUP_ID"]] = departmentModel.GroupId;
                }

                recordObject[recordType.Attributes["DEPT_DESC"]] = departmentModel.DeptDesc;
                recordObject[recordType.Attributes["DEPT_STATUS"]] = departmentModel.DeptStatus;
            }

            OracleTable pMerchDeptTab = new OracleTable(tableType);
            pMerchDeptTab.Add(recordObject);

            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();
            OracleParameter parameter;
            parameter = new OracleParameter("RESULT", Devart.Data.Oracle.OracleDbType.Boolean);
            parameter.Direction = System.Data.ParameterDirection.ReturnValue;
            parameters.Add(parameter);
            parameter = new OracleParameter("P_MERCH_DEPT_TAB", Devart.Data.Oracle.OracleDbType.Table);
            parameter.Direction = System.Data.ParameterDirection.InputOutput;
            parameter.ObjectTypeName = "RETAIL.MERCH_DEPT_TAB";
            parameter.Value = pMerchDeptTab;
            parameters.Add(parameter);
            this.ExecuteProcedure("GETDEPARTMENT", parameters);

            this.Connection.Close();
            List<DepartmentModel> departments = new List<DepartmentModel>();

            if (this.Parameters["P_MERCH_DEPT_TAB"].Value != System.DBNull.Value)
            {
                pMerchDeptTab = (Devart.Data.Oracle.OracleTable)this.Parameters["P_MERCH_DEPT_TAB"].Value;

                DepartmentModel department;
                if (pMerchDeptTab != null)
                {
                    OracleObject[] depts = new OracleObject[pMerchDeptTab.Count];
                    for (int i = 0; i < pMerchDeptTab.Count; i++)
                    {
                        depts[i] = (OracleObject)pMerchDeptTab[i];
                        OracleObject obj = depts[i];
                        if (obj != null && obj["CREATED_UPDATED_BY"] != null)
                        {
                            var inboundDays = 0;
                            var outboundDays = 0;
                            var productLimit = 0;
                            if (!string.IsNullOrEmpty(Convert.ToString(obj["INBOUND_DAYS"])))
                            {
                                inboundDays = Convert.ToInt32(Convert.ToString(obj["INBOUND_DAYS"]));
                            }

                            if (!string.IsNullOrEmpty(Convert.ToString(obj["OUTBOUND_DAYS"])))
                            {
                                outboundDays = Convert.ToInt32(Convert.ToString(obj["OUTBOUND_DAYS"]));
                            }

                            if (!string.IsNullOrEmpty(Convert.ToString(obj["PRODUCT_LIMIT"])))
                            {
                                productLimit = Convert.ToInt32(Convert.ToString(obj["PRODUCT_LIMIT"]));
                            }

                            department = new DepartmentModel()
                            {
                                GroupId = Convert.ToInt32(obj["GROUP_ID"]),
                                DeptId = Convert.ToInt32(obj["DEPT_ID"]),
                                CompanyId = Convert.ToInt32(obj["COMPANY_ID"]),
                                AssortmentPlanning = Convert.ToString(obj["ASSORTMENT_PLANNING"]),
                                AutoArAllowed = Convert.ToString(obj["AUTO_AR_ALLOWED"]),
                                BudgetInd = Convert.ToString(obj["BUDGET_IND"]),
                                BuyerId = Convert.ToString(obj["BUYER_ID"]),
                                DeptDesc = Convert.ToString(obj["DEPT_DESC"]),
                                DeptStatus = Convert.ToString(obj["DEPT_STATUS"]),
                                ForecastAllowed = Convert.ToString(obj["FORECAST_ALLOWED"]),
                                InboundDays = inboundDays,
                                LocalBarcode = Convert.ToString(obj["LOCAL_BARCODE"]),
                                MerchandiserId = Convert.ToString(obj["MERCHANDISER_ID"]),
                                OutboundDays = outboundDays,
                                ProductLimit = productLimit,
                                RetailVatInc = Convert.ToString(obj["RETAIL_VAT_INC"]),
                                TotalMarketAmount = Convert.ToDecimal(obj["TOTAL_MARKET_AMOUNT"]),
                                VatCode = Convert.ToString(obj["VAT_CODE"]),
                                WeighScaleAllowed = Convert.ToString(obj["WEIGH_SCALE_ALLOWED"]),
                                CreatedBy = Convert.ToString(obj["CREATED_UPDATED_BY"]),
                                ModifiedBy = Convert.ToString(obj["LAST_UPDATED_BY"]),
                                CreatedDate = Convert.ToDateTime(obj["CREATED_UPDATED_DATE"]),
                                ModifiedDate = Convert.ToDateTime(obj["LAST_UPDATED_DATE"]),
                                CompanyName = Convert.ToString(obj["COMPANY_NAME"]),
                                GroupDesc = Convert.ToString(obj["GROUP_DESC"]),
                                DeptStatusDesc = Convert.ToString(obj["DEPT_STATUS_DESC"]),
                                TableName = "DEPT_MST"
                            };
                            departments.Add(department);
                        }
                    }
                }
            }

            if (this.Parameters["Result"].Value == System.DBNull.Value)
            {
                return null;
            }
            else
            {
                return departments;
            }
        }

        public List<DivisionModel> Getdivision(DivisionModel divisionModel)
        {
            this.Connection.Open();

            OracleType tableType = OracleType.GetObjectType("MERCH_DIVISION_TAB", this.Connection);
            OracleType recordType = OracleType.GetObjectType("MERCH_DIVISION_REC", this.Connection);
            var recordObject = new OracleObject(recordType);
            var companyId = divisionModel.CompanyId ?? 0;
            var divisionId = divisionModel.DivisionId ?? 0;

            if (companyId == 0 && divisionId == 0 && string.IsNullOrEmpty(divisionModel.Description) && string.IsNullOrEmpty(divisionModel.DivisionStatus))
            {
                recordObject[recordType.Attributes["COMPANY_ID"]] = "-1";
            }
            else
            {
                if (companyId != 0)
                {
                    recordObject[recordType.Attributes["COMPANY_ID"]] = divisionModel.CompanyId;
                }

                if (divisionId != 0)
                {
                    recordObject[recordType.Attributes["DIVISION_ID"]] = divisionModel.DivisionId;
                }

                recordObject[recordType.Attributes["DESCRIPTION"]] = divisionModel.Description;
                recordObject[recordType.Attributes["DIVISION_STATUS"]] = divisionModel.DivisionStatus;
            }

            OracleTable pMerchDivisionTab = new OracleTable(tableType);
            pMerchDivisionTab.Add(recordObject);

            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();
            OracleParameter parameter;
            parameter = new OracleParameter("RESULT", Devart.Data.Oracle.OracleDbType.Boolean);
            parameter.Direction = System.Data.ParameterDirection.ReturnValue;
            parameters.Add(parameter);
            parameter = new OracleParameter("P_MERCH_DIVISION_TAB", Devart.Data.Oracle.OracleDbType.Table);
            parameter.Direction = System.Data.ParameterDirection.InputOutput;
            parameter.ObjectTypeName = "RETAIL.MERCH_DIVISION_TAB";
            parameter.Value = pMerchDivisionTab;
            parameters.Add(parameter);
            this.ExecuteProcedure("GETDIVISION", parameters);

            this.Connection.Close();
            List<DivisionModel> divisions = new List<DivisionModel>();

            if (this.Parameters["P_MERCH_DIVISION_TAB"].Value != System.DBNull.Value)
            {
                pMerchDivisionTab = (Devart.Data.Oracle.OracleTable)this.Parameters["P_MERCH_DIVISION_TAB"].Value;

                DivisionModel division;
                if (pMerchDivisionTab != null)
                {
                    OracleObject[] divs = new OracleObject[pMerchDivisionTab.Count];
                    for (int i = 0; i < pMerchDivisionTab.Count; i++)
                    {
                        divs[i] = (OracleObject)pMerchDivisionTab[i];
                        OracleObject obj = divs[i];
                        if (obj != null)
                        {
                            division = new DivisionModel()
                            {
                                CompanyId = Convert.ToInt32(obj["COMPANY_ID"]),
                                CompanyName = Convert.ToString(obj["COMPANY_NAME"]),
                                Description = Convert.ToString(obj["DESCRIPTION"]),
                                DivisionId = Convert.ToInt32(obj["DIVISION_ID"]),
                                DivisionMgr = Convert.ToString(obj["DIVISION_MGR"]),
                                DivisionMgrEmail = Convert.ToString(obj["DIVISION_MGR_EMAIL"]),
                                DivisionMgrProxy = Convert.ToString(obj["DIVISION_MGR_PROXY"]),
                                DivisionProxyEmail = Convert.ToString(obj["DIVISION_PROXY_EMAIL"]),
                                DivisionProxyStatus = Convert.ToString(obj["DIVISION_PROXY_STATUS"]),
                                DivisionStatus = Convert.ToString(obj["DIVISION_STATUS"]),
                                DivisionStatusDesc = Convert.ToString(obj["DIVISION_STATUS_DESC"]),
                                CreatedBy = Convert.ToString(obj["CREATED_UPDATED_BY"]),
                                ModifiedBy = Convert.ToString(obj["LAST_UPDATED_BY"]),
                                CreatedDate = Convert.ToDateTime(obj["CREATED_UPDATED_DATE"]),
                                ModifiedDate = Convert.ToDateTime(obj["LAST_UPDATED_DATE"]),
                                DivisionProxyStatusDesc = Convert.ToString(obj["DIVISION_PROXY_STATUS_DESC"]),
                                TableName = "DIVISION_MST"
                            };
                            divisions.Add(division);
                        }
                    }
                }
            }

            if (this.Parameters["Result"].Value == System.DBNull.Value)
            {
                return null;
            }
            else
            {
                return divisions;
            }
        }

        public List<GroupModel> Getgroup(GroupModel groupModel)
        {
            this.Connection.Open();

            OracleType tableType = OracleType.GetObjectType("MERCH_GROUP_TAB", this.Connection);
            OracleType recordType = OracleType.GetObjectType("MERCH_GROUP_REC", this.Connection);
            var recordObject = new OracleObject(recordType);
            var companyId = groupModel.CompanyId ?? 0;
            var divisionId = groupModel.DivisionId ?? 0;
            var groupId = groupModel.GroupId ?? 0;

            if (companyId == 0 && groupId == 0 && divisionId == 0 && string.IsNullOrEmpty(groupModel.GroupDesc) && string.IsNullOrEmpty(groupModel.GroupStatus))
            {
                recordObject[recordType.Attributes["COMPANY_ID"]] = "-1";
            }
            else
            {
                if (companyId != 0)
                {
                    recordObject[recordType.Attributes["COMPANY_ID"]] = groupModel.CompanyId;
                }

                if (divisionId != 0)
                {
                    recordObject[recordType.Attributes["DIVISION_ID"]] = groupModel.DivisionId;
                }

                if (groupId != 0)
                {
                    recordObject[recordType.Attributes["GROUP_ID"]] = groupModel.GroupId;
                }

                recordObject[recordType.Attributes["GROUP_DESC"]] = groupModel.GroupDesc;
                recordObject[recordType.Attributes["GROUP_STATUS"]] = groupModel.GroupStatus;
            }

            OracleTable pMerchGroupTab = new OracleTable(tableType);
            pMerchGroupTab.Add(recordObject);

            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();
            OracleParameter parameter;
            parameter = new OracleParameter("RESULT", Devart.Data.Oracle.OracleDbType.Boolean);
            parameter.Direction = System.Data.ParameterDirection.ReturnValue;
            parameters.Add(parameter);
            parameter = new OracleParameter("P_MERCH_GROUP_TAB", Devart.Data.Oracle.OracleDbType.Table);
            parameter.Direction = System.Data.ParameterDirection.InputOutput;
            parameter.ObjectTypeName = "RETAIL.MERCH_GROUP_TAB";
            parameter.Value = pMerchGroupTab;
            parameters.Add(parameter);
            this.ExecuteProcedure("GETGROUP", parameters);

            this.Connection.Close();
            List<GroupModel> groups = new List<GroupModel>();

            if (this.Parameters["P_MERCH_GROUP_TAB"].Value != System.DBNull.Value)
            {
                pMerchGroupTab = (Devart.Data.Oracle.OracleTable)this.Parameters["P_MERCH_GROUP_TAB"].Value;

                GroupModel group;
                if (pMerchGroupTab != null)
                {
                    OracleObject[] depts = new OracleObject[pMerchGroupTab.Count];
                    for (int i = 0; i < pMerchGroupTab.Count; i++)
                    {
                        depts[i] = (OracleObject)pMerchGroupTab[i];
                        OracleObject obj = depts[i];
                        if (obj != null && obj["CREATED_UPDATED_BY"] != null)
                        {
                            var inboundDays = 0;
                            var outboundDays = 0;
                            var productLimit = 0;
                            if (!string.IsNullOrEmpty(Convert.ToString(obj["INBOUND_DAYS"])))
                            {
                                inboundDays = Convert.ToInt32(Convert.ToString(obj["INBOUND_DAYS"]));
                            }

                            if (!string.IsNullOrEmpty(Convert.ToString(obj["OUTBOUND_DAYS"])))
                            {
                                outboundDays = Convert.ToInt32(Convert.ToString(obj["OUTBOUND_DAYS"]));
                            }

                            if (!string.IsNullOrEmpty(Convert.ToString(obj["PRODUCT_LIMIT"])))
                            {
                                productLimit = Convert.ToInt32(Convert.ToString(obj["PRODUCT_LIMIT"]));
                            }

                            group = new GroupModel()
                            {
                                GroupId = Convert.ToInt32(obj["GROUP_ID"]),
                                CompanyId = Convert.ToInt32(obj["COMPANY_ID"]),
                                BuyerId = Convert.ToString(obj["BUYER_ID"]),
                                MerchandiserId = Convert.ToString(obj["MERCHANDISER_ID"]),
                                BudgetInd = Convert.ToString(obj["BUDGET_IND"]),
                                GroupDesc = Convert.ToString(obj["GROUP_DESC"]),
                                GroupStatus = Convert.ToString(obj["GROUP_STATUS"]),
                                InboundDays = inboundDays,
                                LocalBarcode = Convert.ToString(obj["LOCAL_BARCODE"]),
                                OutboundDays = outboundDays,
                                ProductLimit = productLimit,
                                RetailVatInc = Convert.ToString(obj["RETAIL_VAT_INC"]),
                                TotalMarketAmount = Convert.ToDecimal(obj["TOTAL_MARKET_AMOUNT"] ?? 0),
                                VatCode = Convert.ToString(obj["VAT_CODE"]),
                                WeighScaleAllowed = Convert.ToString(obj["WEIGH_SCALE_ALLOWED"]),
                                DivisionId = Convert.ToInt32(obj["DIVISION_ID"]),
                                EnableKiloComversion = Convert.ToString(obj["ENABLE_KILO_CONVERSION"]),
                                EnableStockControl = Convert.ToString(obj["ENABLE_STOCK_CONTROL"]),
                                OtbCalcType = Convert.ToString(obj["OTB_CALC_TYPE"]),
                                PickConsolidate = Convert.ToString(obj["PICK_CONSOLIDATE"]),
                                PosDeptKeyAllowed = Convert.ToString(obj["POS_DEPT_KEY_ALLOWED"]),
                                CreatedBy = Convert.ToString(obj["CREATED_UPDATED_BY"]),
                                ModifiedBy = Convert.ToString(obj["LAST_UPDATED_BY"]),
                                CreatedDate = Convert.ToDateTime(obj["CREATED_UPDATED_DATE"]),
                                ModifiedDate = Convert.ToDateTime(obj["LAST_UPDATED_DATE"]),
                                CompanyName = Convert.ToString(obj["COMPANY_NAME"]),
                                DivisionDesc = Convert.ToString(obj["DIVISION_DESC"]),
                                GroupStatusDesc = Convert.ToString(obj["GROUP_STATUS_DESC"]),
                                TableName = "GROUP_MST"
                            };
                            groups.Add(group);
                        }
                    }
                }
            }

            if (this.Parameters["Result"].Value == System.DBNull.Value)
            {
                return null;
            }
            else
            {
                return groups;
            }
        }

        public List<MerchantSpaceModel> Getspacemst(MerchantSpaceModel merchantSpaceModel)
        {
            this.Connection.Open();

            OracleType tableType = OracleType.GetObjectType("MERCH_SPACE_TAB", this.Connection);
            OracleType recordType = OracleType.GetObjectType("MERCH_SPACE_REC", this.Connection);
            var recordObject = new OracleObject(recordType);
            var groupId = merchantSpaceModel.GroupId ?? 0;
            var deptId = merchantSpaceModel.DeptId ?? 0;
            if (groupId == 0 && deptId == 0 && string.IsNullOrEmpty(merchantSpaceModel.SpaceLevel) && string.IsNullOrEmpty(merchantSpaceModel.SpaceOrgId))
            {
                recordObject[recordType.Attributes["GROUP_ID"]] = "-1";
            }
            else
            {
                if (groupId != 0)
                {
                    recordObject[recordType.Attributes["GROUP_ID"]] = merchantSpaceModel.GroupId;
                }

                if (deptId != 0)
                {
                    recordObject[recordType.Attributes["DEPT_ID"]] = merchantSpaceModel.DeptId;
                }

                if (!string.IsNullOrEmpty(merchantSpaceModel.SpaceLevel))
                {
                    recordObject[recordType.Attributes["SPACE_LEVEL"]] = merchantSpaceModel.SpaceLevel;
                }

                if (!string.IsNullOrEmpty(merchantSpaceModel.SpaceOrgId))
                {
                    recordObject[recordType.Attributes["SPACE_ORG_ID"]] = merchantSpaceModel.SpaceOrgId;
                }
            }

            OracleTable pMerchSpaceTab = new OracleTable(tableType);
            pMerchSpaceTab.Add(recordObject);

            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();
            OracleParameter parameter;
            parameter = new OracleParameter("RESULT", Devart.Data.Oracle.OracleDbType.Boolean);
            parameter.Direction = System.Data.ParameterDirection.ReturnValue;
            parameters.Add(parameter);
            parameter = new OracleParameter("P_MERCH_SPACE_TAB", Devart.Data.Oracle.OracleDbType.Table);
            parameter.Direction = System.Data.ParameterDirection.InputOutput;
            parameter.ObjectTypeName = "RETAIL.MERCH_SPACE_TAB";
            parameter.Value = pMerchSpaceTab;
            parameters.Add(parameter);
            this.ExecuteProcedure("GETSPACEMST", parameters);

            this.Connection.Close();
            List<MerchantSpaceModel> merchantSpaces = new List<MerchantSpaceModel>();

            if (this.Parameters["P_MERCH_SPACE_TAB"].Value != System.DBNull.Value)
            {
                pMerchSpaceTab = (Devart.Data.Oracle.OracleTable)this.Parameters["P_MERCH_SPACE_TAB"].Value;

                MerchantSpaceModel merchantSpace;
                if (pMerchSpaceTab != null)
                {
                    OracleObject[] divs = new OracleObject[pMerchSpaceTab.Count];
                    for (int i = 0; i < pMerchSpaceTab.Count; i++)
                    {
                        divs[i] = (OracleObject)pMerchSpaceTab[i];
                        OracleObject obj = divs[i];
                        if (obj != null)
                        {
                            merchantSpace = new MerchantSpaceModel()
                            {
                                DeptId = Convert.ToInt32(obj["DEPT_ID"] ?? 0),
                                GroupId = Convert.ToInt32(obj["GROUP_ID"] ?? 0),
                                CatCaptain = Convert.ToInt32(obj["CAT_CAPTAIN"] ?? 0),
                                MultiLocated = Convert.ToString(obj["MULTI_LOCATED"]),
                                PrimaryBrand = Convert.ToString(obj["PRIMARY_BRAND"]),
                                SpaceHeight = Convert.ToDecimal(obj["SPACE_HEIGHT"] ?? 0),
                                SpaceLength = Convert.ToDecimal(obj["SPACE_LENGTH"] ?? 0),
                                SpaceWidth = Convert.ToDecimal(obj["SPACE_WIDTH"] ?? 0),
                                SpaceLevel = Convert.ToString(obj["SPACE_LEVEL"]),
                                SpaceOrgId = Convert.ToString(obj["SPACE_ORG_ID"]),
                                CreatedBy = Convert.ToString(obj["CREATED_UPDATED_BY"]),
                                ModifiedBy = Convert.ToString(obj["LAST_UPDATED_BY"])
                            };
                            merchantSpaces.Add(merchantSpace);
                        }
                    }
                }
            }

            if (this.Parameters["Result"].Value == System.DBNull.Value)
            {
                return null;
            }
            else
            {
                return merchantSpaces;
            }
        }

        public List<SubClassModel> Getsubclass(SubClassModel subClassModel)
        {
            this.Connection.Open();

            OracleType tableType = OracleType.GetObjectType("MERCH_SUB_CLASS_TAB", this.Connection);
            OracleType recordType = OracleType.GetObjectType("MERCH_SUB_CLASS_REC", this.Connection);
            var recordObject = new OracleObject(recordType);
            var groupId = subClassModel.GroupId ?? 0;
            var deptId = subClassModel.DeptId ?? 0;
            var companyId = subClassModel.CompanyId ?? 0;
            var subClassId = subClassModel.SubClassId ?? 0;
            var classId = subClassModel.ClassId ?? 0;

            if (groupId == 0 && deptId == 0 && subClassId == 0 && classId == 0 && companyId == 0 && string.IsNullOrEmpty(subClassModel.SubClassDesc) && string.IsNullOrEmpty(subClassModel.SubClassStatus))
            {
                recordObject[recordType.Attributes["COMPANY_ID"]] = "-1";
            }
            else
            {
                if (groupId != 0)
                {
                    recordObject[recordType.Attributes["GROUP_ID"]] = subClassModel.GroupId;
                }

                if (deptId != 0)
                {
                    recordObject[recordType.Attributes["DEPT_ID"]] = subClassModel.DeptId;
                }

                if (subClassId != 0)
                {
                    recordObject[recordType.Attributes["SUB_CLASS_ID"]] = subClassModel.SubClassId;
                }

                if (classId != 0)
                {
                    recordObject[recordType.Attributes["CLASS_ID"]] = subClassModel.ClassId;
                }

                if (companyId != 0)
                {
                    recordObject[recordType.Attributes["COMPANY_ID"]] = subClassModel.CompanyId;
                }

                recordObject[recordType.Attributes["SUB_CLASS_DESC"]] = subClassModel.SubClassDesc;
                recordObject[recordType.Attributes["SUB_CLASS_STATUS"]] = subClassModel.SubClassStatus;
            }

            OracleTable pMerchSubClassTab = new OracleTable(tableType);
            pMerchSubClassTab.Add(recordObject);

            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();
            OracleParameter parameter;
            parameter = new OracleParameter("RESULT", Devart.Data.Oracle.OracleDbType.Boolean);
            parameter.Direction = System.Data.ParameterDirection.ReturnValue;
            parameters.Add(parameter);
            parameter = new OracleParameter("P_MERCH_SUB_CLASS_TAB", Devart.Data.Oracle.OracleDbType.Table);
            parameter.Direction = System.Data.ParameterDirection.InputOutput;
            parameter.ObjectTypeName = "RETAIL.MERCH_SUB_CLASS_TAB";
            parameter.Value = pMerchSubClassTab;
            parameters.Add(parameter);
            this.ExecuteProcedure("GETSUBCLASS", parameters);

            this.Connection.Close();
            List<SubClassModel> subClasses = new List<SubClassModel>();

            if (this.Parameters["P_MERCH_SUB_CLASS_TAB"].Value != System.DBNull.Value)
            {
                pMerchSubClassTab = (Devart.Data.Oracle.OracleTable)this.Parameters["P_MERCH_SUB_CLASS_TAB"].Value;

                SubClassModel subClass;
                if (pMerchSubClassTab != null)
                {
                    OracleObject[] divs = new OracleObject[pMerchSubClassTab.Count];
                    for (int i = 0; i < pMerchSubClassTab.Count; i++)
                    {
                        divs[i] = (OracleObject)pMerchSubClassTab[i];
                        OracleObject obj = divs[i];
                        if (obj != null && obj["CREATED_UPDATED_BY"] != null)
                        {
                            subClass = new SubClassModel
                            {
                                DeptId = Convert.ToInt32(obj["DEPT_ID"]),
                                GroupId = Convert.ToInt32(obj["GROUP_ID"]),
                                ClassId = Convert.ToInt32(obj["CLASS_ID"]),
                                CompanyId = Convert.ToInt32(obj["COMPANY_ID"]),
                                BuyerId = Convert.ToString(obj["BUYER_ID"]),
                                MerchandiserId = Convert.ToString(obj["MERCHANDISER_ID"]),
                                SubClassDesc = Convert.ToString(obj["SUB_CLASS_DESC"]),
                                SubClassId = Convert.ToInt32(obj["SUB_CLASS_ID"]),
                                SubClassStatus = Convert.ToString(obj["SUB_CLASS_STATUS"]),
                                CreatedBy = Convert.ToString(obj["CREATED_UPDATED_BY"]),
                                ModifiedBy = Convert.ToString(obj["LAST_UPDATED_BY"]),
                                CreatedDate = Convert.ToDateTime(obj["CREATED_UPDATED_DATE"]),
                                ModifiedDate = Convert.ToDateTime(obj["LAST_UPDATED_DATE"]),
                                ClassDesc = Convert.ToString(obj["CLASS_DESC"]),
                                CompanyName = Convert.ToString(obj["COMPANY_NAME"]),
                                GroupDesc = Convert.ToString(obj["GROUP_DESC"]),
                                DeptDesc = Convert.ToString(obj["DEPT_DESC"]),
                                SubClassStatusDesc = Convert.ToString(obj["SUB_CLASS_STATUS_DESC"]),
                                TableName = "SUB_CLASS_MST"
                            };
                            subClasses.Add(subClass);
                        }
                    }
                }
            }

            if (this.Parameters["Result"].Value == System.DBNull.Value)
            {
                return null;
            }
            else
            {
                return subClasses;
            }
        }

        public bool Setcatrolemst(CategoryRoleModel categoryRoleModel)
        {
            this.Connection.Open();

            OracleType tableType = OracleType.GetObjectType("MERCH_CATROLE_TAB", this.Connection);
            OracleType recordType = OracleType.GetObjectType("MERCH_CATROLE_REC", this.Connection);
            var recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["GROUP_ID"]] = categoryRoleModel.GroupId;
            recordObject[recordType.Attributes["DEPT_ID"]] = categoryRoleModel.DeptId;
            recordObject[recordType.Attributes["ROLE_LEVEL"]] = categoryRoleModel.RoleLevel;
            recordObject[recordType.Attributes["ROLE_ID"]] = categoryRoleModel.RoleId;
            recordObject[recordType.Attributes["STRATEGY_TYPE"]] = categoryRoleModel.StrategyType;
            recordObject[recordType.Attributes["CATEGORY_ROLE"]] = categoryRoleModel.CategoryRole;
            recordObject[recordType.Attributes["CATEGORY_STRATEGY"]] = categoryRoleModel.CategoryStrategy;
            recordObject[recordType.Attributes["CREATED_UPDATED_BY"]] = "spulla";
            recordObject[recordType.Attributes["LAST_UPDATED_BY"]] = "spulla";
            recordObject[recordType.Attributes["Operation"]] = categoryRoleModel.Operation;

            OracleTable pMerchCatroleTab = new OracleTable(tableType);
            pMerchCatroleTab.Add(recordObject);

            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();
            OracleParameter parameter;
            parameter = new OracleParameter("P_MERCH_CATROLE_TAB", Devart.Data.Oracle.OracleDbType.Table);
            parameter.Direction = System.Data.ParameterDirection.InputOutput;
            parameter.ObjectTypeName = "RETAIL.MERCH_CATROLE_TAB";
            parameter.Value = pMerchCatroleTab;
            parameters.Add(parameter);
            this.ExecuteProcedure("SETCATROLEMST", parameters);
            return this.Parameters["P_MERCH_CATROLE_TAB"].Value != System.DBNull.Value;
        }

        public bool Setclass(ClassModel classModel)
        {
            this.Connection.Open();

            OracleType tableType = OracleType.GetObjectType("MERCH_CLASS_TAB", this.Connection);
            OracleType recordType = OracleType.GetObjectType("MERCH_CLASS_REC", this.Connection);
            var recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["COMPANY_ID"]] = classModel.CompanyId;
            recordObject[recordType.Attributes["GROUP_ID"]] = classModel.GroupId;
            recordObject[recordType.Attributes["DEPT_ID"]] = classModel.DeptId;
            recordObject[recordType.Attributes["CLASS_ID"]] = classModel.ClassId;
            recordObject[recordType.Attributes["CLASS_DESC"]] = classModel.ClassDesc;
            recordObject[recordType.Attributes["BUYER_ID"]] = classModel.BuyerId;
            recordObject[recordType.Attributes["MERCHANDISER_ID"]] = classModel.MerchandiserId;
            recordObject[recordType.Attributes["Operation"]] = classModel.Operation;

            recordObject[recordType.Attributes["CLASS_STATUS"]] = !string.IsNullOrEmpty(classModel.ClassStatus) ? classModel.ClassStatus : "A";

            recordObject[recordType.Attributes["CREATED_UPDATED_BY"]] = "spulla";
            recordObject[recordType.Attributes["LAST_UPDATED_BY"]] = "spulla";

            OracleTable pMerchClassTab = new OracleTable(tableType);
            pMerchClassTab.Add(recordObject);

            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();
            OracleParameter parameter;
            parameter = new OracleParameter("P_MERCH_CLASS_TAB", Devart.Data.Oracle.OracleDbType.Table);
            parameter.Direction = System.Data.ParameterDirection.InputOutput;
            parameter.ObjectTypeName = "RETAIL.MERCH_CLASS_TAB";
            parameter.Value = pMerchClassTab;
            parameters.Add(parameter);
            this.ExecuteProcedure("SETCLASS", parameters);

            return this.Parameters["P_MERCH_CLASS_TAB"].Value != System.DBNull.Value;
        }

        public bool Setdepartment(DepartmentModel departmentModel)
        {
            this.Connection.Open();

            OracleType tableType = OracleType.GetObjectType("MERCH_DEPT_TAB", this.Connection);
            OracleType recordType = OracleType.GetObjectType("MERCH_DEPT_REC", this.Connection);
            var recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["COMPANY_ID"]] = departmentModel.CompanyId;
            recordObject[recordType.Attributes["GROUP_ID"]] = departmentModel.GroupId;
            recordObject[recordType.Attributes["DEPT_ID"]] = departmentModel.DeptId;
            recordObject[recordType.Attributes["DEPT_DESC"]] = departmentModel.DeptDesc;
            recordObject[recordType.Attributes["PRODUCT_LIMIT"]] = departmentModel.ProductLimit;
            recordObject[recordType.Attributes["INBOUND_DAYS"]] = departmentModel.InboundDays;
            recordObject[recordType.Attributes["OUTBOUND_DAYS"]] = departmentModel.OutboundDays;
            recordObject[recordType.Attributes["LOCAL_BARCODE"]] = departmentModel.LocalBarcode;
            recordObject[recordType.Attributes["WEIGH_SCALE_ALLOWED"]] = departmentModel.WeighScaleAllowed;
            recordObject[recordType.Attributes["VAT_CODE"]] = departmentModel.VatCode;
            recordObject[recordType.Attributes["TOTAL_MARKET_AMOUNT"]] = departmentModel.TotalMarketAmount;
            recordObject[recordType.Attributes["BUYER_ID"]] = departmentModel.BuyerId;
            recordObject[recordType.Attributes["MERCHANDISER_ID"]] = departmentModel.MerchandiserId;
            recordObject[recordType.Attributes["BUDGET_IND"]] = departmentModel.BudgetInd;
            recordObject[recordType.Attributes["RETAIL_VAT_INC"]] = departmentModel.RetailVatInc;
            recordObject[recordType.Attributes["ASSORTMENT_PLANNING"]] = departmentModel.AssortmentPlanning;
            recordObject[recordType.Attributes["FORECAST_ALLOWED"]] = departmentModel.ForecastAllowed;
            recordObject[recordType.Attributes["AUTO_AR_ALLOWED"]] = departmentModel.AutoArAllowed;
            recordObject[recordType.Attributes["Operation"]] = departmentModel.Operation;

            recordObject[recordType.Attributes["DEPT_STATUS"]] = !string.IsNullOrEmpty(departmentModel.DeptStatus) ? departmentModel.DeptStatus : "A";

            recordObject[recordType.Attributes["CREATED_UPDATED_BY"]] = "spulla";
            recordObject[recordType.Attributes["LAST_UPDATED_BY"]] = "spulla";
            OracleTable pMerchDeptTab = new OracleTable(tableType);
            pMerchDeptTab.Add(recordObject);

            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();
            OracleParameter parameter;
            parameter = new OracleParameter("P_MERCH_DEPT_TAB", Devart.Data.Oracle.OracleDbType.Table);
            parameter.Direction = System.Data.ParameterDirection.InputOutput;
            parameter.ObjectTypeName = "RETAIL.MERCH_DEPT_TAB";
            parameter.Value = pMerchDeptTab;
            parameters.Add(parameter);
            this.ExecuteProcedure("SETDEPARTMENT", parameters);

            return this.Parameters["P_MERCH_DEPT_TAB"].Value != System.DBNull.Value;
        }

        public bool Setdivision(DivisionModel divisionModel)
        {
            this.Connection.Open();

            OracleType tableType = OracleType.GetObjectType("MERCH_DIVISION_TAB", this.Connection);
            OracleType recordType = OracleType.GetObjectType("MERCH_DIVISION_REC", this.Connection);
            var recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["COMPANY_ID"]] = divisionModel.CompanyId;
            recordObject[recordType.Attributes["DIVISION_ID"]] = divisionModel.DivisionId;
            recordObject[recordType.Attributes["DESCRIPTION"]] = divisionModel.Description;
            recordObject[recordType.Attributes["DIVISION_MGR"]] = divisionModel.DivisionMgr;
            recordObject[recordType.Attributes["DIVISION_MGR_EMAIL"]] = divisionModel.DivisionMgrEmail;
            recordObject[recordType.Attributes["DIVISION_MGR_PROXY"]] = divisionModel.DivisionMgrProxy;
            recordObject[recordType.Attributes["DIVISION_PROXY_EMAIL"]] = divisionModel.DivisionProxyEmail;
            recordObject[recordType.Attributes["DIVISION_PROXY_STATUS"]] = divisionModel.DivisionProxyStatus;
            recordObject[recordType.Attributes["Operation"]] = divisionModel.Operation;

            recordObject[recordType.Attributes["DIVISION_STATUS"]] = !string.IsNullOrEmpty(divisionModel.DivisionStatus) ? divisionModel.DivisionStatus : "A";

            recordObject[recordType.Attributes["CREATED_UPDATED_BY"]] = "spulla";
            recordObject[recordType.Attributes["LAST_UPDATED_BY"]] = "spulla";
            OracleTable pMerchDivisionTab = new OracleTable(tableType);
            pMerchDivisionTab.Add(recordObject);

            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();
            OracleParameter parameter;
            parameter = new OracleParameter("P_MERCH_DIVISION_TAB", Devart.Data.Oracle.OracleDbType.Table);
            parameter.Direction = System.Data.ParameterDirection.InputOutput;
            parameter.ObjectTypeName = "RETAIL.MERCH_DIVISION_TAB";
            parameter.Value = pMerchDivisionTab;
            parameters.Add(parameter);
            this.ExecuteProcedure("SETDIVISION", parameters);
            return this.Parameters["P_MERCH_DIVISION_TAB"].Value != System.DBNull.Value;
        }

        public bool Setgroup(GroupModel groupModel)
        {
            this.Connection.Open();

            OracleType tableType = OracleType.GetObjectType("MERCH_GROUP_TAB", this.Connection);
            OracleType recordType = OracleType.GetObjectType("MERCH_GROUP_REC", this.Connection);
            var recordObject = new OracleObject(recordType);

            recordObject[recordType.Attributes["COMPANY_ID"]] = groupModel.CompanyId;
            recordObject[recordType.Attributes["DIVISION_ID"]] = groupModel.DivisionId;
            recordObject[recordType.Attributes["GROUP_ID"]] = groupModel.GroupId;
            recordObject[recordType.Attributes["GROUP_DESC"]] = groupModel.GroupDesc;
            recordObject[recordType.Attributes["PRODUCT_LIMIT"]] = groupModel.ProductLimit;
            recordObject[recordType.Attributes["INBOUND_DAYS"]] = groupModel.InboundDays;
            recordObject[recordType.Attributes["OUTBOUND_DAYS"]] = groupModel.OutboundDays;
            recordObject[recordType.Attributes["LOCAL_BARCODE"]] = groupModel.LocalBarcode;
            recordObject[recordType.Attributes["WEIGH_SCALE_ALLOWED"]] = groupModel.WeighScaleAllowed;
            recordObject[recordType.Attributes["POS_DEPT_KEY_ALLOWED"]] = groupModel.PosDeptKeyAllowed;
            recordObject[recordType.Attributes["ENABLE_STOCK_CONTROL"]] = groupModel.EnableStockControl;
            recordObject[recordType.Attributes["VAT_CODE"]] = groupModel.VatCode;
            recordObject[recordType.Attributes["ENABLE_KILO_CONVERSION"]] = groupModel.EnableKiloComversion;
            recordObject[recordType.Attributes["PICK_CONSOLIDATE"]] = groupModel.PickConsolidate;
            recordObject[recordType.Attributes["OTB_CALC_TYPE"]] = groupModel.OtbCalcType;
            recordObject[recordType.Attributes["TOTAL_MARKET_AMOUNT"]] = groupModel.TotalMarketAmount;
            recordObject[recordType.Attributes["BUYER_ID"]] = groupModel.BuyerId;
            recordObject[recordType.Attributes["MERCHANDISER_ID"]] = groupModel.MerchandiserId;
            recordObject[recordType.Attributes["BUDGET_IND"]] = groupModel.BudgetInd;
            recordObject[recordType.Attributes["RETAIL_VAT_INC"]] = groupModel.RetailVatInc;
            recordObject[recordType.Attributes["Operation"]] = groupModel.Operation;

            recordObject[recordType.Attributes["GROUP_STATUS"]] = !string.IsNullOrEmpty(groupModel.GroupStatus) ? groupModel.GroupStatus : "A";

            recordObject[recordType.Attributes["CREATED_UPDATED_BY"]] = "spulla";
            recordObject[recordType.Attributes["LAST_UPDATED_BY"]] = "spulla";
            OracleTable pMerchGroupTab = new OracleTable(tableType);
            pMerchGroupTab.Add(recordObject);

            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();
            OracleParameter parameter;
            parameter = new OracleParameter("P_MERCH_GROUP_TAB", Devart.Data.Oracle.OracleDbType.Table);
            parameter.Direction = System.Data.ParameterDirection.InputOutput;
            parameter.ObjectTypeName = "RETAIL.MERCH_GROUP_TAB";
            parameter.Value = pMerchGroupTab;
            parameters.Add(parameter);
            this.ExecuteProcedure("SETGROUP", parameters);
            return this.Parameters["P_MERCH_GROUP_TAB"].Value != System.DBNull.Value;
        }

        public bool Setspacemst(MerchantSpaceModel merchantSpaceModel)
        {
            this.Connection.Open();

            OracleType tableType = OracleType.GetObjectType("MERCH_SPACE_TAB", this.Connection);
            OracleType recordType = OracleType.GetObjectType("MERCH_SPACE_REC", this.Connection);
            var recordObject = new OracleObject(recordType);

            recordObject[recordType.Attributes["GROUP_ID"]] = merchantSpaceModel.GroupId;
            recordObject[recordType.Attributes["DEPT_ID"]] = merchantSpaceModel.DeptId;
            recordObject[recordType.Attributes["SPACE_LEVEL"]] = merchantSpaceModel.SpaceLevel;
            recordObject[recordType.Attributes["SPACE_ORG_ID"]] = merchantSpaceModel.SpaceOrgId;
            recordObject[recordType.Attributes["MULTI_LOCATED"]] = merchantSpaceModel.MultiLocated;
            recordObject[recordType.Attributes["SPACE_HEIGHT"]] = merchantSpaceModel.SpaceHeight;
            recordObject[recordType.Attributes["SPACE_LENGTH"]] = merchantSpaceModel.SpaceLength;
            recordObject[recordType.Attributes["SPACE_WIDTH"]] = merchantSpaceModel.SpaceWidth;
            recordObject[recordType.Attributes["CAT_CAPTAIN"]] = merchantSpaceModel.CatCaptain;
            recordObject[recordType.Attributes["PRIMARY_BRAND"]] = merchantSpaceModel.PrimaryBrand;
            recordObject[recordType.Attributes["CREATED_UPDATED_BY"]] = "spulla";
            recordObject[recordType.Attributes["LAST_UPDATED_BY"]] = "spulla";
            recordObject[recordType.Attributes["Operation"]] = merchantSpaceModel.Operation;

            OracleTable pMerchSpaceTab = new OracleTable(tableType);
            pMerchSpaceTab.Add(recordObject);

            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();
            OracleParameter parameter;
            parameter = new OracleParameter("P_MERCH_SPACE_TAB", Devart.Data.Oracle.OracleDbType.Table);
            parameter.Direction = System.Data.ParameterDirection.InputOutput;
            parameter.ObjectTypeName = "RETAIL.MERCH_SPACE_TAB";
            parameter.Value = pMerchSpaceTab;
            parameters.Add(parameter);
            this.ExecuteProcedure("SETSPACEMST", parameters);
            return this.Parameters["P_MERCH_SPACE_TAB"].Value != System.DBNull.Value;
        }

        public bool Setsubclass(SubClassModel subClassModel)
        {
            this.Connection.Open();

            OracleType tableType = OracleType.GetObjectType("MERCH_SUB_CLASS_TAB", this.Connection);
            OracleType recordType = OracleType.GetObjectType("MERCH_SUB_CLASS_REC", this.Connection);
            var recordObject = new OracleObject(recordType);

            recordObject[recordType.Attributes["GROUP_ID"]] = subClassModel.GroupId;
            recordObject[recordType.Attributes["DEPT_ID"]] = subClassModel.DeptId;
            recordObject[recordType.Attributes["SUB_CLASS_ID"]] = subClassModel.SubClassId;
            recordObject[recordType.Attributes["CLASS_ID"]] = subClassModel.ClassId;
            recordObject[recordType.Attributes["COMPANY_ID"]] = subClassModel.CompanyId;
            recordObject[recordType.Attributes["SUB_CLASS_DESC"]] = subClassModel.SubClassDesc;
            recordObject[recordType.Attributes["SUB_CLASS_STATUS"]] = subClassModel.SubClassStatus;
            recordObject[recordType.Attributes["BUYER_ID"]] = subClassModel.BuyerId;
            recordObject[recordType.Attributes["MERCHANDISER_ID"]] = subClassModel.MerchandiserId;
            recordObject[recordType.Attributes["Operation"]] = subClassModel.Operation;

            if (!string.IsNullOrEmpty(subClassModel.SubClassStatus))
            {
                recordObject[recordType.Attributes["SUB_CLASS_STATUS"]] = subClassModel.SubClassStatus;
            }
            else
            {
                recordObject[recordType.Attributes["SUB_CLASS_STATUS"]] = "A";
            }

            recordObject[recordType.Attributes["CREATED_UPDATED_BY"]] = "spulla";
            recordObject[recordType.Attributes["LAST_UPDATED_BY"]] = "spulla";

            OracleTable pMerchSubClassTab = new OracleTable(tableType);
            pMerchSubClassTab.Add(recordObject);

            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();
            OracleParameter parameter;
            parameter = new OracleParameter("P_MERCH_SUB_CLASS_TAB", Devart.Data.Oracle.OracleDbType.Table);
            parameter.Direction = System.Data.ParameterDirection.InputOutput;
            parameter.ObjectTypeName = "RETAIL.MERCH_SUB_CLASS_TAB";
            parameter.Value = pMerchSubClassTab;
            parameters.Add(parameter);
            this.ExecuteProcedure("SETSUBCLASS", parameters);
            return this.Parameters["P_MERCH_SUB_CLASS_TAB"].Value != System.DBNull.Value;
        }

        public async ValueTask<bool> IsExistingDivision(int companyId, int id)
        {
            int result;
            OracleCommand myCommand = new OracleCommand("Select count(1) from DIVISION_MST where COMPANY_ID =" + companyId + " and DIVISION_ID = " + id);
            myCommand.Connection = this.Connection;
            this.Connection.Open();
            result = Convert.ToInt32(await myCommand.ExecuteScalarAsync());
            this.Connection.Close();
            return result > 0;
        }

        public async ValueTask<bool> IsExistingGroup(int companyId, int divisionId, int id)
        {
            int result;
            OracleCommand myCommand = new OracleCommand("Select count(1) from GROUP_MST where COMPANY_ID =" + companyId + " and GROUP_ID = " + id + " and DIVISION_ID = " + divisionId);
            myCommand.Connection = this.Connection;
            this.Connection.Open();
            result = Convert.ToInt32(await myCommand.ExecuteScalarAsync());
            this.Connection.Close();
            return result > 0;
        }

        public async ValueTask<bool> IsExistingDept(int companyId, int groupId, int id)
        {
            int result;
            OracleCommand myCommand = new OracleCommand("Select count(1) from DEPT_MST where COMPANY_ID =" + companyId + " and GROUP_ID = " + groupId + " and DEPT_ID = " + id);
            myCommand.Connection = this.Connection;
            this.Connection.Open();
            result = Convert.ToInt32(await myCommand.ExecuteScalarAsync());
            this.Connection.Close();
            return result > 0;
        }

        public async ValueTask<bool> IsExistingClass(int companyId, int groupId, int deptId, int id)
        {
            int result;
            OracleCommand myCommand = new OracleCommand("Select count(1) from CLASS_MST where COMPANY_ID =" + companyId + " and GROUP_ID = " + groupId + " and DEPT_ID = " + deptId + " and CLASS_ID = " + id);
            myCommand.Connection = this.Connection;
            this.Connection.Open();
            result = Convert.ToInt32(await myCommand.ExecuteScalarAsync());
            this.Connection.Close();
            return result > 0;
        }

        public async ValueTask<bool> IsExistingSubClass(int companyId, int groupId, int deptId, int classId, int id)
        {
            int result;
            OracleCommand myCommand = new OracleCommand("Select count(1) from SUB_CLASS_MST where COMPANY_ID =" + companyId + " and GROUP_ID = " + groupId + " and DEPT_ID = " + deptId + " and CLASS_ID = " + classId + " and SUB_CLASS_ID = " + id);
            myCommand.Connection = this.Connection;
            this.Connection.Open();
            result = Convert.ToInt32(await myCommand.ExecuteScalarAsync());
            this.Connection.Close();
            return result > 0;
        }
    }
}
