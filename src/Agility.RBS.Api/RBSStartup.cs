﻿//-------------------------------------------------------------------------------------------------
// <copyright file="RBSStartup.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// Startup project for Api
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.Api
{
    using Agility.Framework.Core;
    using Agility.RBS.Api.Repositories;
    using Microsoft.Extensions.DependencyInjection;

    public class RbsStartup : ComponentStartup<RbsDbContext>
    {
        public override void ConfigureDependencyInjection(IServiceCollection services)
        {
            services.AddTransient<RbsRepository>();
        }
    }
}
