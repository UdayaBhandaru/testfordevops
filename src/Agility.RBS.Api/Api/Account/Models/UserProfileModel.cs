﻿// <copyright file="UserProfileModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Agility.RBS.Api.Account.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class UserProfileModel : ProfileEntity
    {
        public string UserId { get; set; }

        public string Name { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }
    }
}
