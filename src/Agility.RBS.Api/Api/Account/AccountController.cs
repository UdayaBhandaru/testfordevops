﻿// <copyright file="AccountController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Agility.RBS.Api.Account
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Exceptions;
    using Agility.Framework.Core.Models;
    using Agility.Framework.Core.Security.Models;
    using Agility.Framework.Web.Core.Models;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.Framework.Web.Security.Domain;
    using Agility.Framework.Web.Security.Models;
    using Agility.RBS.Api.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Agility.Framework.Core.Models;

    public class AccountController : Controller
    {
        private readonly AccountDomain accountDomain;
        private readonly ServiceDocument<UserProfile> serviceDocument;
        private readonly RbsRepository repository;

        public AccountController(AccountDomain accountDomain, ServiceDocument<UserProfile> serviceDocument, RbsRepository repository)
        {
            this.accountDomain = accountDomain;
            this.serviceDocument = serviceDocument;
            this.repository = repository;
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ServiceDocument<UserProfile>> Login([FromBody]LoginViewModel model)
        {
            await this.accountDomain.Validate(model.Email, model.Password);

            UserProfile profileModel = await this.accountDomain.ProfileGet(model.Email);

            if (profileModel != null)
            {
                ServiceDocumentHelper.SetInMemoryUser(profileModel);

                CookieOptions options = new CookieOptions()
                {
                    HttpOnly = true
                };
                profileModel.UserToken = Guid.NewGuid().ToString();
                this.Response.Cookies.Append("UserToken", profileModel.UserToken, options);

                this.serviceDocument.UserProfile = profileModel;
            }

            return this.serviceDocument;
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<AjaxModel<AjaxResult>> Register([FromBody]UserModel userModel)
        {
            try
            {
                if (userModel != null && userModel.Organization != null)
                {
                    userModel.OrganizationId = userModel.Organization.OrganizationId;
                }

                await this.accountDomain.CreateUser(userModel);
                return new AjaxModel<AjaxResult> { Model = AjaxResult.Success };
            }
            catch (FxException exception)
            {
                return new AjaxModel<AjaxResult> { Model = AjaxResult.Exception, Message = exception.Message };
            }
        }

        public List<OrganizationModel> GetOrganizations()
        {
            return new List<OrganizationModel>() { new OrganizationModel() { OrganizationId = 1,  OrganizationName = "aes" } };
        }

        [AllowAnonymous]
        public Task<List<Agility.RBS.Api.Account.Models.UserProfileModel>> List()
        {
            return this.repository.UserProfilesGet();
        }
    }
}
