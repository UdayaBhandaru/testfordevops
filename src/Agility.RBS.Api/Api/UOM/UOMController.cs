﻿//-------------------------------------------------------------------------------------------------
// <copyright file="UomController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// UOMController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.Api.UOM
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Api.Models;
    using Agility.RBS.Api.Repositories;
    using Devart.Data.Oracle;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    public class UomController : Controller
    {
        private readonly MasterDataRepository masterRepository;
        private readonly ServiceDocument<UomModel> serviceDocument;
        private readonly UomRepository uomRepository;

        public UomController(ServiceDocument<UomModel> serviceDocument, UomRepository uomRepository, MasterDataRepository masterRepository)
        {
            this.uomRepository = uomRepository;
            this.masterRepository = masterRepository;
            this.serviceDocument = serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<UomModel>> List()
        {
            await this.serviceDocument.AddDomainDataAsync(this.masterRepository.DomainDetailsStatus, "status");
            await this.serviceDocument.AddDomainDataAsync(this.masterRepository.UomClassList, "uomclass");
            return this.serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<UomModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.CallBackSearch);
            return this.serviceDocument;
        }

        public async ValueTask<bool> Save()
        {
            return await this.serviceDocument.SaveAsync(this.CallBackSave);
        }

        public async ValueTask<bool> IsExistingUom(string name)
        {
            return await this.uomRepository.IsExistingUOM(name);
        }

        private async Task<List<UomModel>> CallBackSearch()
        {
            return await Task.Run(() =>
            {
                return this.uomRepository.GetUom(this.serviceDocument.DataProfile.DataModel);
            });
        }

        private async Task<bool> CallBackSave()
        {
            return await Task.Run(() =>
            {
                return this.uomRepository.SetUom(this.serviceDocument.DataProfile.DataModel);
            });
        }
    }
}
