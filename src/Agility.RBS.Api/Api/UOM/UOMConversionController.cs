﻿//-------------------------------------------------------------------------------------------------
// <copyright file="UomConversionController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// UOMConversionController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.Api.UOM
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Api.Models;
    using Agility.RBS.Api.Repositories;
    using Devart.Data.Oracle;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    public class UomConversionController : Controller
    {
        private readonly ServiceDocument<UomConversionModel> serviceDocument;
        private readonly MasterDataRepository masterRepository;
        private readonly UomRepository uomRepository;

        public UomConversionController(ServiceDocument<UomConversionModel> serviceDocument, UomRepository uomRepository, MasterDataRepository masterRepository)
        {
            this.uomRepository = uomRepository;
            this.masterRepository = masterRepository;
            this.serviceDocument = serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<UomConversionModel>> List()
        {
            await this.serviceDocument.AddDomainDataAsync(this.masterRepository.DomainDetailsOperator, "operator");
            await this.serviceDocument.AddDomainDataAsync(this.masterRepository.UomList, "uom");
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<UomConversionModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.CallBackSearch);
            return this.serviceDocument;
        }

        public async ValueTask<bool> Save()
        {
            return await this.serviceDocument.SaveAsync(this.CallBackSave);
        }

        public async ValueTask<bool> IsExistingConversion(string fromUOM, string toUOM)
        {
            return await this.uomRepository.IsExistingConversion(fromUOM, toUOM);
        }

        private async Task<List<UomConversionModel>> CallBackSearch()
        {
            return await Task.Run(() =>
            {
                return this.uomRepository.GetUomConversion(this.serviceDocument.DataProfile.DataModel);
            });
        }

        private async Task<bool> CallBackSave()
        {
            return await Task.Run(() =>
            {
                return this.uomRepository.SetUomConversion(this.serviceDocument.DataProfile.DataModel);
            });
        }
    }
}
