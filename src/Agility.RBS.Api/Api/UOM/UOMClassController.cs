﻿//-------------------------------------------------------------------------------------------------
// <copyright file="UomClassController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// UOMClassController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.Api.UOM
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Api.Models;
    using Agility.RBS.Api.Repositories;
    using Devart.Data.Oracle;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    public class UomClassController : Controller
    {
        private readonly MasterDataRepository masterRepository;
        private readonly ServiceDocument<UomClassModel> serviceDocument;
        private readonly UomRepository uomRepository;

        public UomClassController(ServiceDocument<UomClassModel> serviceDocument, UomRepository uomRepository, MasterDataRepository masterRepository)
        {
            this.uomRepository = uomRepository;
            this.masterRepository = masterRepository;
            this.serviceDocument = serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<UomClassModel>> List()
        {
            await this.serviceDocument.AddDomainDataAsync(this.masterRepository.DomainDetailsStatus, "status");
            this.serviceDocument.LocalizationData.AddData("UomClass");
            return this.serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<UomClassModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.CallBackSearch);
            return this.serviceDocument;
        }

        public async ValueTask<bool> Save()
        {
            return await this.serviceDocument.SaveAsync(this.CallBackSave);
        }

        public async Task<ServiceDocument<UomClassModel>> New()
        {
            await this.serviceDocument.NewAsync();
            return this.serviceDocument;
        }

        public async ValueTask<bool> IsExistingClass(string name)
        {
            return await this.uomRepository.IsExistingClass(name);
        }

        private async Task<List<UomClassModel>> CallBackSearch()
        {
            return await Task.Run(() =>
            {
                return this.uomRepository.GetUomClass(this.serviceDocument.DataProfile.DataModel);
            });
        }

        private async Task<bool> CallBackSave()
        {
            return await Task.Run(() =>
            {
                return this.uomRepository.SetUomClass(this.serviceDocument.DataProfile.DataModel);
            });
        }
    }
}
