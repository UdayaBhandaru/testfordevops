﻿//-------------------------------------------------------------------------------------------------
// <copyright file="UOMModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.Api.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class UomModel : ProfileEntity
    {
        public string UomCode { get; set; }

        public string UomClass { get; set; }

        public int? DecimalPrecision { get; set; }

        public string Description { get; set; }

        public string ShortDescription { get; set; }

        public string Status { get; set; }

        public string DomainDetailDesc { get; set; }

        public int Lang_Id { get; set; }

        public string Program_Phase { get; set; }

        public string Program_Message { get; set; }

        public string P_Error { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }

        public UomClassModel UomClassModel { get; set; }
    }
}
