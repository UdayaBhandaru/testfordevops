﻿// <copyright file="UOMClassModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.Api.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class UomClassModel : ProfileEntity
    {
        public string UomClass { get; set; }

        public string UomClassDesc { get; set; }

        public string UomClassStatus { get; set; }

        public string DomainDetailDesc { get; set; }

        public int Lang_Id { get; set; }

        public string Program_Phase { get; set; }

        public string Program_Message { get; set; }

        public string P_Error { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }
    }
}
