﻿//-------------------------------------------------------------------------------------------------
// <copyright file="UOMConversionModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.Api.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class UomConversionModel : ProfileEntity
    {
        public string FromUom { get; set; }

        public string ToUom { get; set; }

        public decimal? Factor { get; set; }

        public string Operator { get; set; }

        public int Lang_Id { get; set; }

        public string Program_Phase { get; set; }

        public string Program_Message { get; set; }

        public string P_Error { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }
    }
}
