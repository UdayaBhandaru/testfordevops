﻿//-------------------------------------------------------------------------------------------------
// <copyright file="DomainHeaderModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// DomainHeaderModel
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.Api.Api.Domain.Models
{
    using System.Collections.Generic;
    using Agility.Framework.Core.Profile.Entities;

    public class DomainHeaderModel : ProfileEntity
    {
        public DomainHeaderModel()
        {
            this.DomainDetails = new List<DomainDetailModel>();
        }

        public string DomainId { get; set; }

        public string DomainDesc { get; set; }

        public string DomainStatus { get; set; }

        public string DomainStatusDesc { get; set; }

        public string TableName { get; set; }

        public List<DomainDetailModel> DomainDetails { get; set; }

        public string Operation { get; set; }
    }
}
