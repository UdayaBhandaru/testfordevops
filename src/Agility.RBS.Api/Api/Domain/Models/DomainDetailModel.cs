﻿//-------------------------------------------------------------------------------------------------
// <copyright file="DomainDetailModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// DomainDetailModel
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.Api.Api.Domain.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class DomainDetailModel : ProfileEntity
    {
        public string DomainId { get; set; }

        public string DomainDetailId { get; set; }

        public string DomainDetailDesc { get; set; }

        public string DomainDetailDefault { get; set; }

        public string DomainStatus { get; set; }

        public int DomainDetailSeq { get; set; }

        public string Operation { get; set; }
    }
}
