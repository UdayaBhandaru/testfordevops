﻿//-------------------------------------------------------------------------------------------------
// <copyright file="DomainController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// DomainRepository
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.Api.Api.Domain
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Api.Api.Domain.Models;
    using Agility.RBS.Api.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    [AllowAnonymous]
    public class DomainController : Controller
    {
        private readonly ServiceDocument<DomainHeaderModel> sdDomainHeader;
        private readonly DomainRepository domainRepository;
        private readonly ILogger<DomainController> _logger;

        public DomainController(
            ServiceDocument<DomainHeaderModel> sdDomainHeader,
            ServiceDocument<DomainDetailModel> sdDomainDetail,
            DomainRepository domainRepository,
            ILogger<DomainController> logger)
        {
            this.sdDomainHeader = sdDomainHeader;
            this.domainRepository = domainRepository;
            this._logger = logger;
        }

        public async Task<ServiceDocument<DomainHeaderModel>> List()
        {
            await this.sdDomainHeader.ToListAsync(this.CallBackList);
            return this.sdDomainHeader;
        }

        public async Task<ServiceDocument<DomainHeaderModel>> Search(string header)
        {
            this._logger.LogInformation("Search Started");
            await this.sdDomainHeader.ToListAsync(this.CallBackList);
            this._logger.LogInformation("Search Completed");
            return this.sdDomainHeader;
        }

        public async Task<List<DomainDetailModel>> DomainDetails(string headerId)
        {
            return await Task.Run(() => this.domainRepository.GetUtlDmnStatDtls(headerId));
        }

        public async ValueTask<bool> Save()
        {
            return await this.sdDomainHeader.SaveAsync(this.CallBackSave);
        }

        public async ValueTask<bool> IsExistingDomain(string name)
        {
            return await this.domainRepository.IsExistingDomain(name);
        }

        private async Task<List<DomainHeaderModel>> CallBackList()
        {
            return await Task.Run(() =>
            {
                return this.domainRepository.Getutldomain(this.sdDomainHeader.DataProfile.DataModel);
            });
        }

        private async Task<bool> CallBackSave()
        {
            return await Task.Run(() =>
            {
                var domainHeaderModel = this.sdDomainHeader.DataProfile.DataModel;
                return this.domainRepository.Setutldomain(domainHeaderModel, domainHeaderModel.DomainDetails);
            });
        }
    }
}
