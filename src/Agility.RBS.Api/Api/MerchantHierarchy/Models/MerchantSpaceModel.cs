﻿// <copyright file="MerchantSpaceModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.Api.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class MerchantSpaceModel : ProfileEntity
    {
        public int? GroupId { get; set; }

        public int? DeptId { get; set; }

        public string SpaceLevel { get; set; }

        public string SpaceOrgId { get; set; }

        public string MultiLocated { get; set; }

        public decimal SpaceHeight { get; set; }

        public decimal SpaceWidth { get; set; }

        public decimal SpaceLength { get; set; }

        public int CatCaptain { get; set; }

        public string PrimaryBrand { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }
    }
}
