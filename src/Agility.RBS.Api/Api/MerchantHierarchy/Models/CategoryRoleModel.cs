﻿// <copyright file="CategoryRoleModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.Api.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class CategoryRoleModel : ProfileEntity
    {
        public int? GroupId { get; set; }

        public string GroupDesc { get; set; }

        public int? DeptId { get; set; }

        public string DeptDesc { get; set; }

        public string RoleLevel { get; set; }

        public string RoleId { get; set; }

        public string StrategyType { get; set; }

        public string CategoryRole { get; set; }

        public string CategoryStrategy { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }
    }
}
