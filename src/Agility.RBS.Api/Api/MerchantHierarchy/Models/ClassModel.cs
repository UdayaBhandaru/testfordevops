﻿// <copyright file="ClassModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.Api.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class ClassModel : ProfileEntity
    {
        public int? CompanyId { get; set; }

        public string CompanyName { get; set; }

        public int? GroupId { get; set; }

        public string GroupDesc { get; set; }

        public int? DeptId { get; set; }

        public string DeptDesc { get; set; }

        public int? ClassId { get; set; }

        public string ClassDesc { get; set; }

        public string BuyerId { get; set; }

        public string MerchandiserId { get; set; }

        public string ClassStatus { get; set; }

        public string ClassStatusDesc { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }
    }
}
