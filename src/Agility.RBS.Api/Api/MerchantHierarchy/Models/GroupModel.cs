﻿// <copyright file="GroupModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.Api.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class GroupModel : ProfileEntity
    {
        public int? CompanyId { get; set; }

        public string CompanyName { get; set; }

        public int? DivisionId { get; set; }

        public string DivisionDesc { get; set; }

        public int? GroupId { get; set; }

        public string GroupDesc { get; set; }

        public int? ProductLimit { get; set; }

        public int? InboundDays { get; set; }

        public int? OutboundDays { get; set; }

        public string LocalBarcode { get; set; }

        public string WeighScaleAllowed { get; set; }

        public string PosDeptKeyAllowed { get; set; }

        public string EnableStockControl { get; set; }

        public string VatCode { get; set; }

        public string EnableKiloComversion { get; set; }

        public string PickConsolidate { get; set; }

        public string OtbCalcType { get; set; }

        public decimal? TotalMarketAmount { get; set; }

        public string BuyerId { get; set; }

        public string MerchandiserId { get; set; }

        public string BudgetInd { get; set; }

        public string RetailVatInc { get; set; }

        public string GroupStatus { get; set; }

        public string GroupStatusDesc { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }
    }
}
