﻿// <copyright file="DivisionModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// Division
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.Api.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class DivisionModel : ProfileEntity
    {
        public int? CompanyId { get; set; }

        public string CompanyName { get; set; }

        public int? DivisionId { get; set; }

        public string Description { get; set; }

        public string DivisionMgr { get; set; }

        public string DivisionMgrEmail { get; set; }

        public string DivisionStatus { get; set; }

        public string DivisionStatusDesc { get; set; }

        public string DivisionMgrProxy { get; set; }

        public string DivisionProxyEmail { get; set; }

        public string DivisionProxyStatus { get; set; }

        public string DivisionProxyStatusDesc { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }
    }
}