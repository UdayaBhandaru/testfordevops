﻿//-------------------------------------------------------------------------------------------------
// <copyright file="CategoryRoleController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// CategoryRoleController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.Api.MerchantHierarchy
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Api.Models;
    using Agility.RBS.Api.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    public class CategoryRoleController : Controller
    {
        private readonly ServiceDocument<CategoryRoleModel> serviceDocument;
        private readonly MerchantHierarchyRepository merchantHierarchyRepository;
        private readonly MasterDataRepository masterRepository;

        public CategoryRoleController(ServiceDocument<CategoryRoleModel> serviceDocument, MerchantHierarchyRepository merchantHierarchyRepository, MasterDataRepository masterRepository)
        {
            this.masterRepository = masterRepository;
            this.merchantHierarchyRepository = merchantHierarchyRepository;
            this.serviceDocument = serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<CategoryRoleModel>> List()
        {
            return await this.CategoryRoleDomainData();
        }

        [HttpPost]
        public async Task<ServiceDocument<CategoryRoleModel>> New()
        {
            await this.CategoryRoleDomainData();
            await this.serviceDocument.AddDomainDataAsync(this.masterRepository.DomainDetailsStrgyType, "strategytype");
            await this.serviceDocument.AddDomainDataAsync(this.masterRepository.DomainDetailsIndicator, "indicator"); // Category Role and Category Strategy
            return this.serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<CategoryRoleModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.CallBackSearch);
            return this.serviceDocument;
        }

        public async ValueTask<bool> Save()
        {
            return await this.serviceDocument.SaveAsync(this.CallBackSave);
        }

        private async Task<List<CategoryRoleModel>> CallBackSearch()
        {
            return await Task.Run(() =>
            {
                List<CategoryRoleModel> list;
                var categoryRoleModel = this.serviceDocument.DataProfile.DataModel;
                list = this.merchantHierarchyRepository.Getcatrolemst(categoryRoleModel);
                return list;
            });
        }

        private async Task<bool> CallBackSave()
        {
            return await Task.Run(() =>
            {
                var categoryRoleModel = this.serviceDocument.DataProfile.DataModel;
                return this.merchantHierarchyRepository.Setcatrolemst(categoryRoleModel);
            });
        }

        private async Task<ServiceDocument<CategoryRoleModel>> CategoryRoleDomainData()
        {
            await this.serviceDocument.AddDomainDataAsync(this.masterRepository.GroupList, "group");
            await this.serviceDocument.AddDomainDataAsync(this.masterRepository.DepartmentList, "department");
            await this.serviceDocument.AddDomainDataAsync(this.masterRepository.DomainDetailsSpaceLevel, "rolelevel");
            return this.serviceDocument;
        }
    }
}
