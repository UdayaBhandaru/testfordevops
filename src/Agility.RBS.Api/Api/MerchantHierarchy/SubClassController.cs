﻿//-------------------------------------------------------------------------------------------------
// <copyright file="SubClassController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// SubClassController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.Api.MerchantHierarchy
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Api.Api.Domain.Models;
    using Agility.RBS.Api.Models;
    using Agility.RBS.Api.Repositories;
    using Devart.Data.Oracle;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    public class SubClassController : Controller
    {
        private readonly ServiceDocument<SubClassModel> serviceDocument;
        private readonly MerchantHierarchyRepository merchantHierarchyRepository;
        private readonly MasterDataRepository masterRepository;

        public SubClassController(ServiceDocument<SubClassModel> serviceDocument, MerchantHierarchyRepository merchantHierarchyRepository, MasterDataRepository masterRepository)
        {
            this.masterRepository = masterRepository;
            this.merchantHierarchyRepository = merchantHierarchyRepository;
            this.serviceDocument = serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<SubClassModel>> List()
        {
            return await this.SubClassDomainData();
        }

        [HttpPost]
        public async Task<ServiceDocument<SubClassModel>> New()
        {
            await this.SubClassDomainData();
            await this.serviceDocument.AddDomainDataAsync(this.masterRepository.UserList, "user");
            return this.serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<SubClassModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.CallBackSearch);
            return this.serviceDocument;
        }

        public async ValueTask<bool> Save()
        {
            return await this.serviceDocument.SaveAsync(this.CallBackSave);
        }

        public async ValueTask<bool> IsExistingSubClass(int companyId, int groupId, int deptId, int classId, int id)
        {
            return await this.merchantHierarchyRepository.IsExistingSubClass(companyId, groupId, deptId, classId, id);
        }

        private async Task<List<SubClassModel>> CallBackSearch()
        {
            return await Task.Run(() =>
            {
                return this.merchantHierarchyRepository.Getsubclass(this.serviceDocument.DataProfile.DataModel);
            });
        }

        private async Task<bool> CallBackSave()
        {
            return await Task.Run(() =>
            {
                return this.merchantHierarchyRepository.Setsubclass(this.serviceDocument.DataProfile.DataModel);
            });
        }

        private async Task<ServiceDocument<SubClassModel>> SubClassDomainData()
        {
            await this.serviceDocument.AddDomainDataAsync(this.masterRepository.DomainDetailsStatus, "status");
            await this.serviceDocument.AddDomainDataAsync(this.masterRepository.GroupList, "group");
            await this.serviceDocument.AddDomainDataAsync(this.masterRepository.DepartmentList, "department");
            await this.serviceDocument.AddDomainDataAsync(this.masterRepository.ClassList, "class");
            return this.serviceDocument;
        }
    }
}
