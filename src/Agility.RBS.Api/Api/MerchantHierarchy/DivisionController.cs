﻿//-------------------------------------------------------------------------------------------------
// <copyright file="DivisionController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// DivisionController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.Api.MerchantHierarchy
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Api.Models;
    using Agility.RBS.Api.Repositories;
    using Devart.Data.Oracle;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    public class DivisionController : Controller
    {
        private readonly ServiceDocument<DivisionModel> serviceDocument;
        private readonly MerchantHierarchyRepository merchantHierarchyRepository;
        private readonly MasterDataRepository masterRepository;

        public DivisionController(ServiceDocument<DivisionModel> serviceDocument, MerchantHierarchyRepository merchantHierarchyRepository, MasterDataRepository masterRepository)
        {
            this.masterRepository = masterRepository;
            this.merchantHierarchyRepository = merchantHierarchyRepository;
            this.serviceDocument = serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<DivisionModel>> List()
        {
            return await this.DivisionDomainData();
        }

        [HttpPost]
        public async Task<ServiceDocument<DivisionModel>> New()
        {
            await this.DivisionDomainData();
            await this.serviceDocument.AddDomainDataAsync(this.masterRepository.UserList, "user");
            return this.serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<DivisionModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.CallBackSearch);
            return this.serviceDocument;
        }

        public async ValueTask<bool> Save()
        {
            return await this.serviceDocument.SaveAsync(this.CallBackSave);
        }

        public async ValueTask<bool> IsExistingDivision(int companyId, int id)
        {
            return await this.merchantHierarchyRepository.IsExistingDivision(companyId, id);
        }

        private async Task<List<DivisionModel>> CallBackSearch()
        {
            return await Task.Run(() =>
            {
                return this.merchantHierarchyRepository.Getdivision(this.serviceDocument.DataProfile.DataModel);
            });
        }

        private async Task<bool> CallBackSave()
        {
            return await Task.Run(() =>
            {
                return this.merchantHierarchyRepository.Setdivision(this.serviceDocument.DataProfile.DataModel);
            });
        }

        private async Task<ServiceDocument<DivisionModel>> DivisionDomainData()
        {
            await this.serviceDocument.AddDomainDataAsync(this.masterRepository.DomainDetailsStatus, "status");
            return this.serviceDocument;
        }
    }
}
