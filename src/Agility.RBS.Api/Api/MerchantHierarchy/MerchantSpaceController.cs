﻿//-------------------------------------------------------------------------------------------------
// <copyright file="MerchantSpaceController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// MerchantSpaceController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.Api.MerchantHierarchy
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Api.Models;
    using Agility.RBS.Api.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    public class MerchantSpaceController : Controller
    {
        private readonly ServiceDocument<MerchantSpaceModel> serviceDocument;
        private readonly MerchantHierarchyRepository merchantHierarchyRepository;
        private readonly MasterDataRepository masterRepository;

        public MerchantSpaceController(ServiceDocument<MerchantSpaceModel> serviceDocument, MerchantHierarchyRepository merchantHierarchyRepository, MasterDataRepository masterRepository)
        {
            this.masterRepository = masterRepository;
            this.merchantHierarchyRepository = merchantHierarchyRepository;
            this.serviceDocument = serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<MerchantSpaceModel>> List()
        {
            return await this.MerchantSpaceDomainData();
        }

        [HttpPost]
        public async Task<ServiceDocument<MerchantSpaceModel>> New()
        {
            await this.MerchantSpaceDomainData();
            await this.serviceDocument.AddDomainDataAsync(this.masterRepository.DomainDetailsIndicator, "multilocated");
            return this.serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<MerchantSpaceModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.CallBackSearch);
            return this.serviceDocument;
        }

        public async ValueTask<bool> Save()
        {
            return await this.serviceDocument.SaveAsync(this.CallBackSave);
        }

        private async Task<List<MerchantSpaceModel>> CallBackSearch()
        {
            return await Task.Run(() =>
            {
                return this.merchantHierarchyRepository.Getspacemst(this.serviceDocument.DataProfile.DataModel);
            });
        }

        private async Task<bool> CallBackSave()
        {
            return await Task.Run(() =>
            {
                return this.merchantHierarchyRepository.Setspacemst(this.serviceDocument.DataProfile.DataModel);
            });
        }

        private async Task<ServiceDocument<MerchantSpaceModel>> MerchantSpaceDomainData()
        {
            await this.serviceDocument.AddDomainDataAsync(this.masterRepository.GroupList, "group");
            await this.serviceDocument.AddDomainDataAsync(this.masterRepository.DepartmentList, "department");
            await this.serviceDocument.AddDomainDataAsync(this.masterRepository.DomainDetailsSpaceLevel, "spacelevel");
            return this.serviceDocument;
        }
    }
}
