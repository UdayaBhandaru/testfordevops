﻿//-------------------------------------------------------------------------------------------------
// <copyright file="DepartmentController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// DepartmentController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.Api.MerchantHierarchy
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Api.Models;
    using Agility.RBS.Api.Repositories;
    using Devart.Data.Oracle;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    public class DepartmentController : Controller
    {
        private readonly ServiceDocument<DepartmentModel> serviceDocument;
        private readonly MerchantHierarchyRepository merchantHierarchyRepository;
        private readonly MasterDataRepository masterRepository;

        public DepartmentController(ServiceDocument<DepartmentModel> serviceDocument, MerchantHierarchyRepository merchantHierarchyRepository, MasterDataRepository masterRepository)
        {
            this.masterRepository = masterRepository;
            this.merchantHierarchyRepository = merchantHierarchyRepository;
            this.serviceDocument = serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<DepartmentModel>> List()
        {
            return await this.DepartmentDomainData();
        }

        [HttpPost]
        public async Task<ServiceDocument<DepartmentModel>> New()
        {
            await this.DepartmentDomainData();
            await this.serviceDocument.AddDomainDataAsync(this.masterRepository.DomainDetailsIndicator, "indicator");
            await this.serviceDocument.AddDomainDataAsync(this.masterRepository.UserList, "user");
            return this.serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<DepartmentModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.CallBackSearch);
            return this.serviceDocument;
        }

        public async ValueTask<bool> Save()
        {
            return await this.serviceDocument.SaveAsync(this.CallBackSave);
        }

        public async ValueTask<bool> IsExistingDept(int companyId, int groupId, int id)
        {
            return await this.merchantHierarchyRepository.IsExistingDept(companyId, groupId, id);
        }

        private async Task<List<DepartmentModel>> CallBackSearch()
        {
            return await Task.Run(() =>
            {
                return this.merchantHierarchyRepository.Getdepartment(this.serviceDocument.DataProfile.DataModel);
            });
        }

        private async Task<bool> CallBackSave()
        {
            return await Task.Run(() =>
            {
                return this.merchantHierarchyRepository.Setdepartment(this.serviceDocument.DataProfile.DataModel);
            });
        }

        private async Task<ServiceDocument<DepartmentModel>> DepartmentDomainData()
        {
            await this.serviceDocument.AddDomainDataAsync(this.masterRepository.DomainDetailsStatus, "status");
            await this.serviceDocument.AddDomainDataAsync(this.masterRepository.GroupList, "group");
            return this.serviceDocument;
        }
    }
}
