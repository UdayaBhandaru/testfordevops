﻿//-------------------------------------------------------------------------------------------------
// <copyright file="ClassController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// UOMClassController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.Api.MerchantHierarchy
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Api.Models;
    using Agility.RBS.Api.Repositories;
    using Devart.Data.Oracle;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    public class ClassController : Controller
    {
        private readonly ServiceDocument<ClassModel> serviceDocument;
        private readonly MerchantHierarchyRepository merchantHierarchyRepository;
        private readonly MasterDataRepository masterRepository;

        public ClassController(ServiceDocument<ClassModel> serviceDocument, MerchantHierarchyRepository merchantHierarchyRepository, MasterDataRepository masterRepository)
        {
            this.masterRepository = masterRepository;
            this.merchantHierarchyRepository = merchantHierarchyRepository;
            this.serviceDocument = serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<ClassModel>> List()
        {
            return await this.ClassDomainData();
        }

        [HttpPost]
        public async Task<ServiceDocument<ClassModel>> New()
        {
            await this.ClassDomainData();
            await this.serviceDocument.AddDomainDataAsync(this.masterRepository.UserList, "user");
            return this.serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<ClassModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.CallBackSearch);
            return this.serviceDocument;
        }

        public async ValueTask<bool> Save()
        {
            return await this.serviceDocument.SaveAsync(this.CallBackSave);
        }

        public async ValueTask<bool> IsExistingClass(int companyId, int groupId, int deptId, int id)
        {
            return await this.merchantHierarchyRepository.IsExistingClass(companyId, groupId, deptId, id);
        }

        private async Task<List<ClassModel>> CallBackSearch()
        {
            return await Task.Run(() =>
            {
                return this.merchantHierarchyRepository.Getclass(this.serviceDocument.DataProfile.DataModel);
            });
        }

        private async Task<bool> CallBackSave()
        {
            return await Task.Run(() =>
            {
                return this.merchantHierarchyRepository.Setclass(this.serviceDocument.DataProfile.DataModel);
            });
        }

        private async Task<ServiceDocument<ClassModel>> ClassDomainData()
        {
            await this.serviceDocument.AddDomainDataAsync(this.masterRepository.DomainDetailsStatus, "status");
            await this.serviceDocument.AddDomainDataAsync(this.masterRepository.GroupList, "group");
            await this.serviceDocument.AddDomainDataAsync(this.masterRepository.DepartmentList, "department");
            return this.serviceDocument;
        }
    }
}
