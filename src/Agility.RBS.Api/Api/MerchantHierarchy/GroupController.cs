﻿//-------------------------------------------------------------------------------------------------
// <copyright file="GroupController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// UOMClassController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.Api.MerchantHierarchy
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Api.Models;
    using Agility.RBS.Api.Repositories;
    using Devart.Data.Oracle;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    public class GroupController : Controller
    {
        private readonly ServiceDocument<GroupModel> serviceDocument;
        private readonly MerchantHierarchyRepository merchantHierarchyRepository;
        private readonly MasterDataRepository masterRepository;

        public GroupController(ServiceDocument<GroupModel> serviceDocument, MerchantHierarchyRepository merchantHierarchyRepository, MasterDataRepository masterRepository)
        {
            this.masterRepository = masterRepository;
            this.merchantHierarchyRepository = merchantHierarchyRepository;
            this.serviceDocument = serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<GroupModel>> List()
        {
            return await this.GroupDomainData();
        }

        [HttpPost]
        public async Task<ServiceDocument<GroupModel>> New()
        {
            await this.GroupDomainData();
            await this.serviceDocument.AddDomainDataAsync(this.masterRepository.DomainDetailsCalcType, "calctype");
            await this.serviceDocument.AddDomainDataAsync(this.masterRepository.DomainDetailsIndicator, "indicator");
            await this.serviceDocument.AddDomainDataAsync(this.masterRepository.UserList, "user");
            return this.serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<GroupModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.CallBackSearch);
            return this.serviceDocument;
        }

        public async ValueTask<bool> Save()
        {
            return await this.serviceDocument.SaveAsync(this.CallBackSave);
        }

        public async ValueTask<bool> IsExistingGroup(int companyId, int divisionId, int id)
        {
            return await this.merchantHierarchyRepository.IsExistingGroup(companyId, divisionId, id);
        }

        private async Task<List<GroupModel>> CallBackSearch()
        {
            return await Task.Run(() =>
            {
                return this.merchantHierarchyRepository.Getgroup(this.serviceDocument.DataProfile.DataModel);
            });
        }

        private async Task<bool> CallBackSave()
        {
            return await Task.Run(() =>
            {
                return this.merchantHierarchyRepository.Setgroup(this.serviceDocument.DataProfile.DataModel);
            });
        }

        private async Task<ServiceDocument<GroupModel>> GroupDomainData()
        {
            await this.serviceDocument.AddDomainDataAsync(this.masterRepository.DomainDetailsStatus, "status");
            await this.serviceDocument.AddDomainDataAsync(this.masterRepository.DivisionList, "division");
            return this.serviceDocument;
        }
    }
}
