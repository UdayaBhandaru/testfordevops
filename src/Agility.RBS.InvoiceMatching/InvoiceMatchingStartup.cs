﻿// <copyright file="InvoiceMatchingStartup.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InvoiceMatching
{
    using Agility.Framework.Core;
    using Agility.RBS.InvoiceMatching.InvoiceMatchingDoc.Controllers;
    using Agility.RBS.InvoiceMatching.InvoiceMatchingDoc.Mapping;
    using Agility.RBS.InvoiceMatching.InvoiceMatchingDoc.Repositories;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;

    public class InvoiceMatchingStartup : ComponentStartup<DbContext>
    {
        public override void ConfigureDependencyInjection(IServiceCollection services)
        {
            services.AddTransient<InvoiceMatchingRepository>();
            services.AddTransient<InvoiceMatchingDocSearchController>();
            services.AddTransient<NonMerchandiseCostController>();
            services.AddTransient<InvoiceMatchingVatController>();
        }

        public void Startup(Microsoft.Extensions.Configuration.IConfigurationRoot configuration)
        {
            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<InvoiceMatchingDocModelMapping>();
                this.InitilizeMapping(cfg);
            });
        }
    }
}
