﻿// <copyright file="InvoiceMatchingConstants.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InvoiceMatching.Common
{
    internal static class InvoiceMatchingConstants
    {
        // InvoiceMatching Package
        public const string GetInvoiceMatchingPackage = "UTL_INVOICE_MATCHING";
        public const string ObjTypeInvoiceMatchingDocSearch = "IM_DOC_GROUP_TAB";
        public const string GetProcInvoiceMatchingDocList = "GETDOCGROUPLIST";
        public const string RecordObjInvoiceMatchingDocSearch = "IM_DOC_GROUP_REC";

        // Domain Data
        public const string DomainHdrDocumentType = "IMTP";
        public const string DomainHdrVendorType = "PTAL";

        // Order Data
        public const string ObjTypeOrderDetailsTAB = "INV_ORD_DTL_TAB";
        public const string GetProcOrderDetails = "GETORDDTL";

        public const string ObjTypeInvoiceMatchingDocTAB = "im_doc_group_hd_tab";
        public const string SetProcInvoiceMatchingDoc = "setdocgrouptab";
        public const string ObjTypeInvoiceMatchingDocREC = "im_doc_group_hd_rec";
        public const string ObjTypeInvoiceMatchingDocHeadDetails = "im_doc_head_tab";
        public const string ObjTypeInvoiceMatchingDocHeadDetailsREC = "im_doc_head_rec";
        public const string GetProcInvoiceMatchingGroupDocHead = "getdocgroup";

        // Non Merch
        public const string ObjTypeNonMerchandiseCostTAB = "im_doc_non_merch_tab";
        public const string SetProcNonMerchandiseCost = "setdocnonmerch";
        public const string ObjTypeNonMerchandiseCostREC = "im_doc_non_merch_rec";
        public const string SetProcNonMerchandiseCostList = "getdocnonmerch";

        // VAT
        public const string ObjTypeVatTAB = "im_doc_vat_tab";
        public const string SetProcVat = "setdocvat";
        public const string ObjTypeVatREC = "im_doc_vat_rec";
        public const string SetProcVatList = "getdocvat";

        // IM_COST_DISCREPANCY
        public const string ObjTypeImCostDiscrepancyTab = "im_cost_disc_tab";
        public const string RecordObjImCostDiscrepancyRec = "im_cost_disc_rec";
        public const string GetProcImCostDiscrepancyList = "getcostdisclist";
        public const string ObjTypeIMDocCostDiscTAB = "im_cost_disc_dtl_tab";
        public const string SetProcIMDocCostDisc = "setcostdisc";
        public const string ObjTypeIMDocCostDiscREC = "im_cost_disc_dtl_rec";

        // ImQtyDiscrepancy
        public const string ObjTypeImQtyDiscrepancyTab = "im_qty_disc_tab";
        public const string GetProcImQtyDiscrepancyList = "getqtydisclist";
        public const string RecordObjImQtyDiscrepancyRec = "im_qty_disc_rec";
        public const string ObjTypeImQtyDiscTAB = "im_qty_disc_dtl_tab";
        public const string SetProcImQtyDisc = "setqtydisc";
        public const string ObjTypeImQtyDiscREC = "im_qty_disc_dtl_rec";

        // ImDocMaintenance
        public const string ObjTypeImDocMaintenanceSearchTAB = "im_doc_search_tab";
        public const string GetProcImDocMaintenanceList = "getdoclist";
        public const string RecordObjImDocMaintenanceREC = "im_doc_search_rec";
        public const string ObjTypeImDocHeadTab = "im_doc_head_tab";
        public const string SetProcImDocHead = "setdochead";
    }
}
