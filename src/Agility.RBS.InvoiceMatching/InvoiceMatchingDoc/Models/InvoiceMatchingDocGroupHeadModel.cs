﻿// <copyright file="InvoiceMatchingDocGroupHeadModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InvoiceMatching.InvoiceMatchingDoc.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Inbox.Common;

    [DataProfile(Name = "INVOICEMATCHINGDOC")]
    public class InvoiceMatchingDocGroupHeadModel : ProfileEntity<InvoiceMatchingWfModel>, IInboxCommonModel
    {
        public InvoiceMatchingDocGroupHeadModel()
        {
            this.InvoiceMatchingDocHead = new List<InvoiceMatchingDocHeadModel>();
        }

        [Column("GROUP_ID")]
        public int? GROUP_ID { get; set; }

        [NotMapped]
        public DateTime? CreateDate { get; set; }

        [NotMapped]
        public string CreateId { get; set; }

        [NotMapped]
        public string Status { get; set; }

        [NotMapped]
        public string StatusDesc { get; set; }

        [NotMapped]
        public int? ControlRecCount { get; set; }

        [NotMapped]
        public int? CalcCount { get; set; }

        [NotMapped]
        public int? VarianceCount { get; set; }

        [NotMapped]
        public int? ControlTotalCost { get; set; }

        [NotMapped]
        public int? CalcCost { get; set; }

        [NotMapped]
        public int? VarianceCost { get; set; }

        [NotMapped]
        public string CurrencyCode { get; set; }

        [NotMapped]
        public string LastUpdateId { get; set; }

        [NotMapped]
        public DateTime? LastUpdateDateTime { get; set; }

        [NotMapped]
        public List<InvoiceMatchingDocHeadModel> InvoiceMatchingDocHead { get; private set; }

        [NotMapped]
        public string Operation { get; set; }

        [NotMapped]
        [Column("ORGANISATION_ID")]
        public new int? OrganizationId { get; set; }

        [NotMapped]
        [Column("CREATED_BY")]
        public new string CreatedBy { get; set; }

        [NotMapped]
        [Column("LAST_UPDATED_BY")]
        public new string ModifiedBy { get; set; }

        [NotMapped]
        [Column("DeletedInd")]
        public new bool DeletedInd { get; set; }

        [NotMapped]
        [Column("CREATED_DATE")]
        public new DateTimeOffset? CreatedDate { get; set; }

        [NotMapped]
        [Column("LAST_UPDATED_DATE")]
        public new DateTimeOffset? ModifiedDate { get; set; }

        [NotMapped]
        public Core.Models.RbWfHeaderModel WorkflowForm { get; set; }

        [NotMapped]
        public string Type { get; set; }

        [NotMapped]
        public string VendorType { get; set; }

        [NotMapped]
        public string VendorTypeDesc { get; set; }

        [NotMapped]
        public DateTime? DocDate { get; set; }

        [NotMapped]
        public int? Supplier { get; set; }

        [NotMapped]
        public string SupNameDesc { get; set; }

        [NotMapped]
        public int? Partner { get; set; }

        [NotMapped]
        public string Terms { get; set; }

        [NotMapped]
        public string PartnerDesc { get; set; }

        [NotMapped]
        public int? RefDoc { get; set; }

        [NotMapped]
        public int? Vendor { get; set; }

        [NotMapped]
        public string VendorName { get; set; }

        [NotMapped]
        public int? ExchangeRate { get; set; }

        [NotMapped]
        public string CurrencyValue { get; set; }
    }
}
