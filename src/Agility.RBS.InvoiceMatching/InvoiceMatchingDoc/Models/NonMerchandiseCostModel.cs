﻿// <copyright file="NonMerchandiseCostModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InvoiceMatching.InvoiceMatchingDoc.Models
{
    using System.ComponentModel.DataAnnotations.Schema;
    using Agility.Framework.Core.Profile.Entities;

    public class NonMerchandiseCostModel : ProfileEntity
    {
        [NotMapped]
        public int? DocId { get; set; }

        [NotMapped]
        public string NonMerchCode { get; set; }

        [NotMapped]
        public string NonMerchCodeDesc { get; set; }

        [NotMapped]
        public int? NonMerchAmt { get; set; }

        [NotMapped]
        public string VatCode { get; set; }

        [NotMapped]
        public int? VatRate { get; set; }

        [NotMapped]
        public string Operation { get; set; }

        [NotMapped]
        public string TotalValue { get; set; }
    }
}
