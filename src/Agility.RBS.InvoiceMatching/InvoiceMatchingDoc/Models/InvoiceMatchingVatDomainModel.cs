﻿// <copyright file="InvoiceMatchingVatDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InvoiceMatching.InvoiceMatchingDoc.Models
{
    using System.Collections.Generic;
    using Agility.RBS.MDM.ValueAddedTax.Models;

    public class InvoiceMatchingVatDomainModel
    {
        public InvoiceMatchingVatDomainModel()
        {
            this.VatCodeType = new List<VatCodeModel>();
            this.VatRateType = new List<VatRateModel>();
            this.InvoiceMatchingVatList = new List<InvoiceMatchingVatModel>();
        }

        public List<VatCodeModel> VatCodeType { get; private set; }

        public List<VatRateModel> VatRateType { get; private set; }

        public List<InvoiceMatchingVatModel> InvoiceMatchingVatList { get; private set; }
    }
}
