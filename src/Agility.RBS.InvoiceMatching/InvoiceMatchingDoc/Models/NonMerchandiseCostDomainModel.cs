﻿// <copyright file="NonMerchandiseCostDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InvoiceMatching.InvoiceMatchingDoc.Models
{
    using System.Collections.Generic;
    using Agility.RBS.MDM.Trait.Models;
    using Agility.RBS.MDM.Traits.Models;
    using Agility.RBS.MDM.ValueAddedTax.Models;

    public class NonMerchandiseCostDomainModel
    {
        public NonMerchandiseCostDomainModel()
        {
            this.NonmerchdiseType = new List<NonMerchandiseDomainModel>();
            this.VatCodeType = new List<VatCodeModel>();
            this.NonMerchandiseCostlist = new List<NonMerchandiseCostModel>();
        }

        public List<NonMerchandiseDomainModel> NonmerchdiseType { get; private set; }

        public List<VatCodeModel> VatCodeType { get; private set; }

        public List<NonMerchandiseCostModel> NonMerchandiseCostlist { get; private set; }
    }
}
