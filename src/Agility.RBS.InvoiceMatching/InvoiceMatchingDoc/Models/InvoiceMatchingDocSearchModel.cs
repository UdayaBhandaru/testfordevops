﻿// <copyright file="InvoiceMatchingDocSearchModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InvoiceMatching.InvoiceMatchingDoc.Models
{
    using System;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Core.Models;

    public class InvoiceMatchingDocSearchModel : ProfileEntity
    {
        public int? GroupId { get; set; }

        public DateTime? CreateDate { get; set; }

        public string CreateId { get; set; }

        public string Status { get; set; }

        public string StatusDesc { get; set; }

        public int? ControlRecCount { get; set; }

        public int? CalcCount { get; set; }

        public int? VarrianceCount { get; set; }

        public int? ControlTotalCost { get; set; }

        public int? CalcCost { get; set; }

        public int? VarrianceCost { get; set; }

        public string CurrencyCode { get; set; }

        public string LastUpdateId { get; set; }

        public DateTime? LastUpdateDateTime { get; set; }

        public string Operation { get; set; }

        public int? StartRow { get; set; }

        public int? EndRow { get; set; }

        public long? TotalRows { get; set; }

        public SortModel[] SortModel { get; set; }
    }
}
