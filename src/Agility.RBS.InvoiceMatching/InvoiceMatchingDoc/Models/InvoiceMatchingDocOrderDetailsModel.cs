﻿// <copyright file="InvoiceMatchingDocOrderDetailsModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InvoiceMatching.InvoiceMatchingDoc.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class InvoiceMatchingDocOrderDetailsModel : ProfileEntity
    {
        public int? OrderNo { get; set; }

        public int? Supllier { get; set; }

        public string SupName { get; set; }

        public string VendorTypeDesc { get; set; }

        public string Terms { get; set; }

        public string LocationType { get; set; }

        public string LocactionTypeDesc { get; set; }

        public int? Location { get; set; }

        public string LocationName { get; set; }

        public string CurrencyCode { get; set; }

        public int? ExchangeRate { get; set; }

        public string Operation { get; set; }
    }
}
