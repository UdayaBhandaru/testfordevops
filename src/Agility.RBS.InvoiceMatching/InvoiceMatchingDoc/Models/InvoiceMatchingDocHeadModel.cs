﻿// <copyright file="InvoiceMatchingDocHeadModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InvoiceMatching.InvoiceMatchingDoc.Models
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;
    using Agility.Framework.Core.Profile.Entities;

    public class InvoiceMatchingDocHeadModel : ProfileEntity
    {
        [NotMapped]
        public int? DocId { get; set; }

        [NotMapped]
        public string Type { get; set; }

        [NotMapped]
        public string TypeDesc { get; set; }

        [NotMapped]
        public string Status { get; set; }

        [NotMapped]
        public string StatusDesc { get; set; }

        [NotMapped]
        public int? OrderNO { get; set; }

        [NotMapped]
        public int? Location { get; set; }

        [NotMapped]
        public string LocName { get; set; }

        [NotMapped]
        public string LocType { get; set; }

        [NotMapped]
        public string LocTypeDesc { get; set; }

        [NotMapped]
        public int? TotalDiscount { get; set; }

        [NotMapped]
        public int? GroupId { get; set; }

        [NotMapped]
        public int? ParentId { get; set; }

        [NotMapped]
        public DateTime? DocDate { get; set; }

        [NotMapped]
        public DateTime? CreateDate { get; set; }

        [NotMapped]
        public string CreateId { get; set; }

        [NotMapped]
        public string VendorType { get; set; }

        [NotMapped]
        public string VendorTypeDesc { get; set; }

        [NotMapped]
        public string Vendor { get; set; }

        [NotMapped]
        public string VendorName { get; set; }

        [NotMapped]
        public string ExtDocId { get; set; }

        [NotMapped]
        public string EdiUploadInd { get; set; }

        [NotMapped]
        public string EdiDownloadInd { get; set; }

        [NotMapped]
        public string Terms { get; set; }

        [NotMapped]
        public int? TermsDscntPct { get; set; }

        [NotMapped]
        public DateTime? DueDate { get; set; }

        [NotMapped]
        public string PaymentMethod { get; set; }

        [NotMapped]
        public string PaymentMethodDesc { get; set; }

        [NotMapped]
        public string MatchId { get; set; }

        [NotMapped]
        public DateTime? MatchDate { get; set; }

        [NotMapped]
        public string ApprovalId { get; set; }

        [NotMapped]
        public DateTime? ApprovalDate { get; set; }

        [NotMapped]
        public string PrePaidInd { get; set; }

        [NotMapped]
        public string PrePaidId { get; set; }

        [NotMapped]
        public DateTime? PostDate { get; set; }

        [NotMapped]
        public string CurrencyCode { get; set; }

        [NotMapped]
        public int? ExchangeRate { get; set; }

        [NotMapped]
        public int? TotalCost { get; set; }

        [NotMapped]
        public int? TotalQty { get; set; }

        [NotMapped]
        public string ManuallyPaidInd { get; set; }

        [NotMapped]
        public string CustomDocRef1 { get; set; }

        [NotMapped]
        public string CustomDocRef2 { get; set; }

        [NotMapped]
        public string CustomDocRef3 { get; set; }

        [NotMapped]
        public string CustomDocRef4 { get; set; }

        [NotMapped]
        public string LastUpdateId { get; set; }

        [NotMapped]
        public DateTime? LastDateTime { get; set; }

        [NotMapped]
        public string FreightType { get; set; }

        [NotMapped]
        public string FreightTypeDesc { get; set; }

        [NotMapped]
        public int? RefDoc { get; set; }

        [NotMapped]
        public int? RefAuthNo { get; set; }

        [NotMapped]
        public string CostFreMatch { get; set; }

        [NotMapped]
        public string DetailMatched { get; set; }

        [NotMapped]
        public string BestTerms { get; set; }

        [NotMapped]
        public string BestTermsSource { get; set; }

        [NotMapped]
        public DateTime? BestTermsDate { get; set; }

        [NotMapped]
        public string BestTermsDateSource { get; set; }

        [NotMapped]
        public int? VarianceWithinTolerance { get; set; }

        [NotMapped]
        public int? ResolutionAdjustedTotalCost { get; set; }

        [NotMapped]
        public int? ResolutionAdjustedTotalQty { get; set; }

        [NotMapped]
        public string ConsignmentInd { get; set; }

        [NotMapped]
        public int? DealId { get; set; }

        [NotMapped]
        public string RtvInd { get; set; }

        [NotMapped]
        public DateTime? DiscountDate { get; set; }

        [NotMapped]
        public string DealType { get; set; }

        [NotMapped]
        public int? TotalCostIncVat { get; set; }

        [NotMapped]
        public DateTime? VatDiscCreateDate { get; set; }

        [NotMapped]
        public string Operation { get; set; }

        [NotMapped]
        public int? MerchCost { get; set; }

        [NotMapped]
        public int? NonMerchCost { get; set; }

        [NotMapped]
        public int? TotalVat { get; set; }
    }
}
