﻿// <copyright file="ImCostDiscrepancySearchModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InvoiceMatching.InvoiceMatchingDoc.Models
{
    using System;
    using System.Collections.Generic;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Core.Models;

    public class ImCostDiscrepancySearchModel : ProfileEntity
    {
        public ImCostDiscrepancySearchModel()
        {
            this.ImCostReviewDetails = new List<ImCostReviewDetailModel>();
        }

        public int? DocId { get; set; }

        public int? Location { get; set; }

        public string LocName { get; set; }

        public string LocType { get; set; }

        public int? OrderNo { get; set; }

        public int? Supplier { get; set; }

        public string SuppName { get; set; }

        public string CurrencyCode { get; set; }

        public DateTime? RoutingDate { get; set; }

        public DateTime? ResolveByDate { get; set; }

        public int? TotalCost { get; set; }

        public int? NoLineDisc { get; set; }

        public int? Dept { get; set; }

        public string DeptDesc { get; set; }

        public int? Class { get; set; }

        public string ClassDesc { get; set; }

        public int? BusinessRoleId { get; set; }

        public string CashDscntInd { get; set; }

        public string ApReviewer { get; set; }

        public string DocType { get; set; }

        public string DocTypeDesc { get; set; }

        public List<ImCostReviewDetailModel> ImCostReviewDetails { get; private set; }

        public string Operation { get; set; }

        public int? StartRow { get; set; }

        public int? EndRow { get; set; }

        public long? TotalRows { get; set; }

        public SortModel[] SortModel { get; set; }
    }
}
