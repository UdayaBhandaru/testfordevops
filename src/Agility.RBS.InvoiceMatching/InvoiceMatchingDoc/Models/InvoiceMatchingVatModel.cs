﻿// <copyright file="InvoiceMatchingVatModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InvoiceMatching.InvoiceMatchingDoc.Models
{
    using System.ComponentModel.DataAnnotations.Schema;
    using Agility.Framework.Core.Profile.Entities;

    public class InvoiceMatchingVatModel : ProfileEntity
    {
        [NotMapped]
        public int? DocId { get; set; }

        [NotMapped]
        public int? VatRate { get; set; }

        [NotMapped]
        public int? VatCode { get; set; }

        [NotMapped]
        public int? VatBasis { get; set; }

        [NotMapped]
        public int? VatAmount { get; set; }

        [NotMapped]
        public string Operation { get; set; }

        [NotMapped]
        public string TotalValue { get; set; }

        [NotMapped]
        public string TotalCount { get; set; }
    }
}
