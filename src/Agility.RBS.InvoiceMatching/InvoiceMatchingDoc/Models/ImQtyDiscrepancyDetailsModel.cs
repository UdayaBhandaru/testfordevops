﻿// <copyright file="ImQtyDiscrepancyDetailsModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InvoiceMatching.InvoiceMatchingDoc.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class ImQtyDiscrepancyDetailsModel
    {
        public int? QtyDiscrepancyId { get; set; }

        public int? DocId { get; set; }

        public int? OrderNo { get; set; }

        public string Item { get; set; }

        public string ItemDesc { get; set; }

        public int? QtyInvoiced { get; set; }

        public int? QtyOrdered { get; set; }

        public int? QtyRecived { get; set; }

        public int? QtyVariance { get; set; }

        public int? QtyVatPerc { get; set; }

        public int? OutstandingVar { get; set; }

        public string ResQtyTyp { get; set; }

        public string ResQtyTypDesc { get; set; }

        public int? ResQty { get; set; }

        public string ReasonCode { get; set; }

        public string ReasonDesc { get; set; }

        public string ActionCode { get; set; }

        public string ActionComments { get; set; }

        public string ReRoutegrp { get; set; }

        public int? ReceiptId { get; set; }

        public string OrdUpc { get; set; }

        public string Vpn { get; set; }

        public string Comments { get; set; }

        public string Operation { get; set; }
    }
}
