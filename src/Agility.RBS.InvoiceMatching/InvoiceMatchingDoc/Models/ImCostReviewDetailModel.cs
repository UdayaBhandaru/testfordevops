﻿// <copyright file="ImCostReviewDetailModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InvoiceMatching.InvoiceMatchingDoc.Models
{
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Core.Models;

    public class ImCostReviewDetailModel
    {
        public int? CostDiscrepancyId { get; set; }

        public int? DocId { get; set; }

        public int? OrderNo { get; set; }

        public string Item { get; set; }

        public string ItemDesc { get; set; }

        public int? DocUnitCost { get; set; }

        public int? DocTotalCost { get; set; }

        public int? OrderUnitCost { get; set; }

        public int? OrderTotalCost { get; set; }

        public int? OrigOrderCost { get; set; }

        public int? CostVariance { get; set; }

        public int? CostVarPerc { get; set; }

        public int? OutstandingVar { get; set; }

        public string CurrencyCode { get; set; }

        public string OrderCostsrc { get; set; }

        public string OrderCostsrcDesc { get; set; }

        public string OrdUpc { get; set; }

        public string Vpn { get; set; }

        public string Comments { get; set; }

        public string ResCostType { get; set; }

        public string ResCostTypeDesc { get; set; }

        public int? ResCost { get; set; }

        public string ReasonCode { get; set; }

        public string ReasonDesc { get; set; }

        public string Action { get; set; }

        public string ActionComments { get; set; }

        public string ReRouteGrp { get; set; }

        public string Operation { get; set; }
    }
}
