﻿// <copyright file="ImDocMaintenanceSearchModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InvoiceMatching.InvoiceMatchingDoc.Models
{
    using System;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Core.Models;

    public class ImDocMaintenanceSearchModel : ProfileEntity
    {
        public int? DocId { get; set; }

        public string Type { get; set; }

        public string TypeDesc { get; set; }

        public string Status { get; set; }

        public string StatusDesc { get; set; }

        public int? OrderNo { get; set; }

        public int? Location { get; set; }

        public string LocName { get; set; }

        public string LocType { get; set; }

        public int? GroupId { get; set; }

        public DateTime? DocDate { get; set; }

        public string VendorType { get; set; }

        public string VendorTypeDesc { get; set; }

        public string Vendor { get; set; }

        public string VendorDesc { get; set; }

        public string ExtDocId { get; set; }

        public DateTime? DueDate { get; set; }

        public int? RefDoc { get; set; }

        public int? StartRow { get; set; }

        public int? EndRow { get; set; }

        public long? TotalRows { get; set; }

        public SortModel[] SortModel { get; set; }
    }
}
