﻿// <copyright file="ImQtyDiscrepancySearchModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InvoiceMatching.InvoiceMatchingDoc.Models
{
    using System;
    using System.Collections.Generic;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Core.Models;

    public class ImQtyDiscrepancySearchModel : ProfileEntity
    {
        public ImQtyDiscrepancySearchModel()
        {
            this.ImQtyDiscrepancyDetails = new List<ImQtyDiscrepancyDetailsModel>();
        }

        public int? DocId { get; set; }

        public int? Location { get; set; }

        public string LocName { get; set; }

        public string LocType { get; set; }

        public int? OrderNo { get; set; }

        public int? Supplier { get; set; }

        public string SuppName { get; set; }

        public DateTime? RoutingDate { get; set; }

        public DateTime? ResolveByDate { get; set; }

        public string ApReviewer { get; set; }

        public string DocType { get; set; }

        public string DocTypeDesc { get; set; }

        public string PastDueInd { get; set; }

        public string FrightPaymentType { get; set; }

        public DateTime? InvoiceDate { get; set; }

        public int? NoLineDisc { get; set; }

        public List<ImQtyDiscrepancyDetailsModel> ImQtyDiscrepancyDetails { get; private set; }

        public string Operation { get; set; }

        public int? StartRow { get; set; }

        public int? EndRow { get; set; }

        public long? TotalRows { get; set; }

        public SortModel[] SortModel { get; set; }
    }
}
