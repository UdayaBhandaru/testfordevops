﻿// <copyright file="InvoiceMatchingWfModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InvoiceMatching.InvoiceMatchingDoc.Models
{
    using System.ComponentModel.DataAnnotations;
    using Agility.Framework.Core.Workflow.Entities;

    public class InvoiceMatchingWfModel : WorkFlowBaseInstance, IWorkFlowInstance
    {
        [Key]
        public int WorkflowInstanceId { get; set; }
    }
}
