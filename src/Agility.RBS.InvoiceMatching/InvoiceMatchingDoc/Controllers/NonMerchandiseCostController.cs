﻿// <copyright file="NonMerchandiseCostController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InvoiceMatching.InvoiceMatchingDoc.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.InvoiceMatching.InvoiceMatchingDoc.Models;
    using Agility.RBS.InvoiceMatching.InvoiceMatchingDoc.Repositories;
    using Agility.RBS.MDM.Trait;
    using Agility.RBS.MDM.ValueAddedTax;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class NonMerchandiseCostController : Controller
    {
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly InvoiceMatchingRepository invoiceMatchingRepository;
        private readonly ServiceDocument<NonMerchandiseCostModel> serviceDocument;
        private readonly TraitRepository traitRepository;
        private readonly ValueAddedTaxRepository valueAddedTaxRepository;

        public NonMerchandiseCostController(
            ServiceDocument<NonMerchandiseCostModel> serviceDocument,
            InvoiceMatchingRepository invoiceMatchingRepository,
            TraitRepository traitRepository,
            ValueAddedTaxRepository valueAddedTaxRepository)
        {
            this.invoiceMatchingRepository = invoiceMatchingRepository;
            this.serviceDocument = serviceDocument;
            this.traitRepository = traitRepository;
            this.valueAddedTaxRepository = valueAddedTaxRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<ServiceDocument<NonMerchandiseCostModel>> New()
        {
            return this.serviceDocument;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<ServiceDocument<NonMerchandiseCostModel>> Open(int id)
        {
            return this.serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<NonMerchandiseCostModel>> Save([FromBody]ServiceDocument<NonMerchandiseCostModel> serviceDocument)
        {
            return this.serviceDocument;
        }

        [AllowAnonymous]
        [HttpPost]
        public async ValueTask<bool> NonMerchandiseCostSave([FromBody]List<NonMerchandiseCostModel> nonMerchandiseCostModel)
        {
            try
            {
                await this.invoiceMatchingRepository.NonMerchandiseCostSave(nonMerchandiseCostModel);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<NonMerchandiseCostDomainModel> NonMerchandiseCostDomainData(int id)
        {
            try
            {
                NonMerchandiseCostDomainModel nonMerchandiseCostDomainModel = new NonMerchandiseCostDomainModel();
                nonMerchandiseCostDomainModel.NonmerchdiseType.AddRange(this.traitRepository.NonmerchdiseGet().Result);
                nonMerchandiseCostDomainModel.VatCodeType.AddRange(this.valueAddedTaxRepository.VatCodeGet().Result);
                nonMerchandiseCostDomainModel.NonMerchandiseCostlist.AddRange(this.invoiceMatchingRepository.NonMerchandiseCostGet(id, this.serviceDocumentResult).Result);
                return nonMerchandiseCostDomainModel;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<List<NonMerchandiseCostModel>> GetNonMerchandiseCostList(int docId)
        {
            try
            {
                var nonMerchandiseCost = await this.invoiceMatchingRepository.NonMerchandiseCostGet(docId, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return nonMerchandiseCost;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }
    }
}
