﻿// <copyright file="InvoiceMatchingVatController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InvoiceMatching.InvoiceMatchingDoc.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.InvoiceMatching.InvoiceMatchingDoc.Models;
    using Agility.RBS.InvoiceMatching.InvoiceMatchingDoc.Repositories;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Trait;
    using Agility.RBS.MDM.ValueAddedTax;
    using Agility.RBS.MDM.ValueAddedTax.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class InvoiceMatchingVatController : Controller
    {
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly InvoiceMatchingRepository invoiceMatchingRepository;
        private readonly ServiceDocument<InvoiceMatchingVatModel> serviceDocument;
        private readonly ValueAddedTaxRepository valueAddedTaxRepository;
        private readonly DomainDataRepository domainDataRepository;

        public InvoiceMatchingVatController(
            ServiceDocument<InvoiceMatchingVatModel> serviceDocument,
            InvoiceMatchingRepository invoiceMatchingRepository,
             ValueAddedTaxRepository valueAddedTaxRepository,
             DomainDataRepository domainDataRepository)
        {
            this.invoiceMatchingRepository = invoiceMatchingRepository;
            this.serviceDocument = serviceDocument;
            this.valueAddedTaxRepository = valueAddedTaxRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<ServiceDocument<InvoiceMatchingVatModel>> New()
        {
            return this.serviceDocument;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<ServiceDocument<InvoiceMatchingVatModel>> Open(int id)
        {
            return this.serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<InvoiceMatchingVatModel>> Save([FromBody]ServiceDocument<InvoiceMatchingVatModel> serviceDocument)
        {
            return this.serviceDocument;
        }

        [AllowAnonymous]
        [HttpPost]
        public async ValueTask<bool> SaveVat([FromBody]List<InvoiceMatchingVatModel> invoiceMatchingVatModel)
        {
            try
            {
                await this.invoiceMatchingRepository.SaveVat(invoiceMatchingVatModel);
                return true;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return false;
            }
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<InvoiceMatchingVatDomainModel> InvoiceMatchingVatDomainData(int docId, string docDate)
        {
            try
            {
                InvoiceMatchingVatDomainModel invoiceMatchingVatDomainModel = new InvoiceMatchingVatDomainModel();
                invoiceMatchingVatDomainModel.VatCodeType.AddRange(this.valueAddedTaxRepository.VatCodeGet().Result);
                invoiceMatchingVatDomainModel.VatRateType.AddRange(this.domainDataRepository.GetVatRateList(docDate).Result);
                invoiceMatchingVatDomainModel.InvoiceMatchingVatList.AddRange(this.invoiceMatchingRepository.VatGet(docId, this.serviceDocumentResult).Result);
                return invoiceMatchingVatDomainModel;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }
    }
}
