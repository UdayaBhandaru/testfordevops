﻿// <copyright file="ImQtyDiscrepancyController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InvoiceMatching.InvoiceMatchingDoc.Controllers
{
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.InvoiceMatching.InvoiceMatchingDoc.Models;
    using Agility.RBS.InvoiceMatching.InvoiceMatchingDoc.Repositories;
    using Agility.RBS.MDM.Administration;
    using Agility.RBS.MDM.Administration.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class ImQtyDiscrepancyController : Controller
    {
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly ServiceDocument<ImQtyDiscrepancySearchModel> serviceDocument;
        private readonly InvoiceMatchingRepository invoiceMatchingRepository;
        private readonly AdministrationRepository administrationRepository;

        public ImQtyDiscrepancyController(
            ServiceDocument<ImQtyDiscrepancySearchModel> serviceDocument,
            InvoiceMatchingRepository invoiceMatchingRepository,
            AdministrationRepository administrationRepository)
        {
            this.serviceDocument = serviceDocument;
            this.invoiceMatchingRepository = invoiceMatchingRepository;
            this.administrationRepository = administrationRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<ImQtyDiscrepancySearchModel>> List()
        {
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<ImQtyDiscrepancySearchModel>> New()
        {
            this.serviceDocument.DomainData.Add("reasonCode", this.administrationRepository.GetReasonCode(new ReasonCodeModel()).Result);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<ImQtyDiscrepancySearchModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.ImQtyDiscrepancySearch);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<ServiceDocument<ImQtyDiscrepancySearchModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.ImQtyDiscrepancySave);
            return this.serviceDocument;
        }

        private async Task<bool> ImQtyDiscrepancySave()
        {
            this.serviceDocument.Result = await this.invoiceMatchingRepository.SetImQtyDiscrepancy(this.serviceDocument.DataProfile.DataModel);
            if (this.serviceDocument.Result.Type != MessageType.Success)
            {
                return false;
            }

            return true;
        }

        private async Task<List<ImQtyDiscrepancySearchModel>> ImQtyDiscrepancySearch()
        {
            try
            {
                var imQtyDiscrepancyList = await this.invoiceMatchingRepository.ImQtyDiscrepancyListGet(this.serviceDocument, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return imQtyDiscrepancyList;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }
    }
}
