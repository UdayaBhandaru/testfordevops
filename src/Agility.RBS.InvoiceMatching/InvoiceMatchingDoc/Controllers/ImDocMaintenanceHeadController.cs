﻿// <copyright file="ImDocMaintenanceHeadController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InvoiceMatching.InvoiceMatchingDoc.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.InvoiceMatching.Common;
    using Agility.RBS.InvoiceMatching.InvoiceMatchingDoc.Models;
    using Agility.RBS.InvoiceMatching.InvoiceMatchingDoc.Repositories;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class ImDocMaintenanceHeadController : Controller
    {
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly ServiceDocument<ImDocMaintenanceHeadModel> serviceDocument;
        private readonly InvoiceMatchingRepository invoiceMatchingRepository;
        private readonly DomainDataRepository domainDataRepository;

        public ImDocMaintenanceHeadController(
            ServiceDocument<ImDocMaintenanceHeadModel> serviceDocument,
            InvoiceMatchingRepository invoiceMatchingRepository,
            DomainDataRepository domainDataRepository)
        {
            this.serviceDocument = serviceDocument;
            this.invoiceMatchingRepository = invoiceMatchingRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<ImDocMaintenanceHeadModel>> New()
        {
            this.serviceDocument.New(false);
            return await this.ImDocMaintenanceDomainData();
        }

        public async Task<ServiceDocument<ImDocMaintenanceHeadModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.ImDocMaintenanceHeadSave);
            return this.serviceDocument;
        }

        private async Task<bool> ImDocMaintenanceHeadSave()
        {
            try
            {
                this.serviceDocument.Result = await this.invoiceMatchingRepository.SetImDocMaintenanceHead(this.serviceDocument.DataProfile.DataModel);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return true;
            }
            catch (System.Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return false;
            }
        }

        private async Task<ServiceDocument<ImDocMaintenanceHeadModel>> ImDocMaintenanceDomainData()
        {
            string partnerType = null;
            this.serviceDocument.DomainData.Add("documentType", this.domainDataRepository.DomainDetailData(InvoiceMatchingConstants.DomainHdrDocumentType).Result);
            this.serviceDocument.DomainData.Add("vendorType", this.domainDataRepository.DomainDetailData(InvoiceMatchingConstants.DomainHdrVendorType).Result);
            this.serviceDocument.DomainData.Add("partner", await this.domainDataRepository.PartnerGet(partnerType));
            this.serviceDocument.DomainData.Add("location", this.domainDataRepository.LocationDomainGet().Result);
            this.serviceDocument.DomainData.Add("domainRadioData", this.domainDataRepository.DomainDetailData("OBSP").Result);
            return this.serviceDocument;
        }
    }
}
