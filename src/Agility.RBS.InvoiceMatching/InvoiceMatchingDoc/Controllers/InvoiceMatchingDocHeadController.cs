﻿// <copyright file="InvoiceMatchingDocHeadController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InvoiceMatching.InvoiceMatchingDoc.Controllers
{
    using System;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Inbox.Common;
    using Agility.RBS.Inbox.Models;
    using Agility.RBS.Inbox.Repository;
    using Agility.RBS.InvoiceMatching.Common;
    using Agility.RBS.InvoiceMatching.InvoiceMatchingDoc.Models;
    using Agility.RBS.InvoiceMatching.InvoiceMatchingDoc.Repositories;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class InvoiceMatchingDocHeadController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly InvoiceMatchingRepository invoiceMatchingRepository;
        private readonly InboxRepository inboxRepository;
        private readonly InboxModel inboxModel;
        private ServiceDocument<InvoiceMatchingDocGroupHeadModel> serviceDocument;
        private int groupId;

        public InvoiceMatchingDocHeadController(
            ServiceDocument<InvoiceMatchingDocGroupHeadModel> serviceDocument,
            DomainDataRepository domainDataRepository,
            InvoiceMatchingRepository invoiceMatchingRepository,
            InboxRepository inboxRepository)
        {
            this.serviceDocument = serviceDocument;
            this.domainDataRepository = domainDataRepository;
            this.inboxRepository = inboxRepository;
            this.invoiceMatchingRepository = invoiceMatchingRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
            this.inboxModel = new InboxModel { Operation = "CHGS", ModuleName = "IM" };
        }

        [HttpGet]
        public async Task<ServiceDocument<InvoiceMatchingDocGroupHeadModel>> New()
        {
            this.serviceDocument.New(false);
            return await this.InvoiceMatchingDocDomainData();
        }

        public async Task<ServiceDocument<InvoiceMatchingDocGroupHeadModel>> Open(int id)
        {
            this.groupId = id;
            await this.serviceDocument.OpenAsync(this.InvoiceMatchingDocOpen);
            return await this.InvoiceMatchingDocDomainData();
        }

        [HttpPost]
        public async Task<ServiceDocument<InvoiceMatchingDocGroupHeadModel>> Save([FromBody]ServiceDocument<InvoiceMatchingDocGroupHeadModel> serviceDocument)
        {
            await this.serviceDocument.SaveAsync(this.InvoiceMatchingDocSave);
            return this.serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<InvoiceMatchingDocGroupHeadModel>> Submit([FromBody]ServiceDocument<InvoiceMatchingDocGroupHeadModel> serviceDocument)
        {
            this.serviceDocument = serviceDocument;
            if (this.serviceDocument.DataProfile.DataModel.GROUP_ID == 0 || this.serviceDocument.DataProfile.DataModel.GROUP_ID == null)
            {
                this.inboxModel.Operation = "DFT";
            }

            this.inboxModel.StateID = RbsServiceDocCustomizationHelper.GetStateIdDotNetToOracle(this.serviceDocument);
            await this.serviceDocument.TransitAsync(this.InvoiceMatchingDocSave);
            if (this.serviceDocument.Result.Type == MessageType.Success)
            {
                await this.inboxRepository.SetInbox(this.inboxModel, this.serviceDocument);
            }

            return this.serviceDocument;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<InvoiceMatchingDocOrderDetailsModel> OrderDetailsGet(int orderno)
        {
            try
            {
                return await this.invoiceMatchingRepository.OrderDetailsGet(orderno);
            }
            catch (System.Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<ServiceDocument<InvoiceMatchingDocGroupHeadModel>> InvoiceMatchingDocDomainData()
        {
            string partnerType = null;
            this.serviceDocument.DomainData.Add("documentType", this.domainDataRepository.DomainDetailData(InvoiceMatchingConstants.DomainHdrDocumentType).Result);
            this.serviceDocument.DomainData.Add("vendorType", this.domainDataRepository.DomainDetailData(InvoiceMatchingConstants.DomainHdrVendorType).Result);
            this.serviceDocument.DomainData.Add("partner", await this.domainDataRepository.PartnerGet(partnerType));
            this.serviceDocument.DomainData.Add("location", this.domainDataRepository.LocationDomainGet().Result);
            return this.serviceDocument;
        }

        private async Task<bool> InvoiceMatchingDocSave()
        {
            this.serviceDocument.Result = await this.invoiceMatchingRepository.SetInvoiceMatchingDoc(this.serviceDocument.DataProfile.DataModel);
            if (this.serviceDocument.Result.Type != MessageType.Success)
            {
                return false;
            }

            this.serviceDocument.DataProfile.DataModel.GROUP_ID = this.invoiceMatchingRepository.GroupId;
            return true;
        }

        private async Task<InvoiceMatchingDocGroupHeadModel> InvoiceMatchingDocOpen()
        {
            try
            {
                var invoicematchingdoc = await this.invoiceMatchingRepository.InvoiceMatchingDocGet(this.groupId, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return invoicematchingdoc;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }
    }
}
