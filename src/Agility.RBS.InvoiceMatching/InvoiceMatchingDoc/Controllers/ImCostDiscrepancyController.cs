﻿// <copyright file="ImCostDiscrepancyController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InvoiceMatching.InvoiceMatchingDoc.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.InvoiceMatching.InvoiceMatchingDoc.Models;
    using Agility.RBS.InvoiceMatching.InvoiceMatchingDoc.Repositories;
    using Agility.RBS.MDM.Administration;
    using Agility.RBS.MDM.Administration.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class ImCostDiscrepancyController : Controller
    {
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly ServiceDocument<ImCostDiscrepancySearchModel> serviceDocument;
        private readonly InvoiceMatchingRepository invoiceMatchingRepository;
        private readonly AdministrationRepository administrationRepository;

        public ImCostDiscrepancyController(
            ServiceDocument<ImCostDiscrepancySearchModel> serviceDocument,
            InvoiceMatchingRepository invoiceMatchingRepository,
            AdministrationRepository administrationRepository)
        {
            this.serviceDocument = serviceDocument;
            this.invoiceMatchingRepository = invoiceMatchingRepository;
            this.administrationRepository = administrationRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<ImCostDiscrepancySearchModel>> List()
        {
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<ImCostDiscrepancySearchModel>> New()
        {
            this.serviceDocument.DomainData.Add("reasonCode", this.administrationRepository.GetReasonCode(new ReasonCodeModel()).Result);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<ImCostDiscrepancySearchModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.InvoiceMatchingDocSearch);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<ServiceDocument<ImCostDiscrepancySearchModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.ImDocCostSave);
            return this.serviceDocument;
        }

        private async Task<bool> ImDocCostSave()
        {
            this.serviceDocument.Result = await this.invoiceMatchingRepository.SetImDocCost(this.serviceDocument.DataProfile.DataModel);
            if (this.serviceDocument.Result.Type != MessageType.Success)
            {
                return false;
            }

            return true;
        }

        private async Task<List<ImCostDiscrepancySearchModel>> InvoiceMatchingDocSearch()
        {
            try
            {
                var imCostDiscrepancyList = await this.invoiceMatchingRepository.ImCostDiscrepancyListGet(this.serviceDocument, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return imCostDiscrepancyList;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }
    }
}
