﻿// <copyright file="InvoiceMatchingDocSearchController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InvoiceMatching.InvoiceMatchingDoc.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.InvoiceMatching.InvoiceMatchingDoc.Models;
    using Agility.RBS.InvoiceMatching.InvoiceMatchingDoc.Repositories;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class InvoiceMatchingDocSearchController : Controller
    {
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly ServiceDocument<InvoiceMatchingDocSearchModel> serviceDocument;
        private readonly InvoiceMatchingRepository invoiceMatchingRepository;

        public InvoiceMatchingDocSearchController(
            ServiceDocument<InvoiceMatchingDocSearchModel> serviceDocument,
            InvoiceMatchingRepository invoiceMatchingRepository)
        {
            this.serviceDocument = serviceDocument;
            this.invoiceMatchingRepository = invoiceMatchingRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<InvoiceMatchingDocSearchModel>> List()
        {
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<InvoiceMatchingDocSearchModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.InvoiceMatchingDocSearch);
            return this.serviceDocument;
        }

        private async Task<List<InvoiceMatchingDocSearchModel>> InvoiceMatchingDocSearch()
        {
            try
            {
                var invoiceMatchingDocList = await this.invoiceMatchingRepository.InvoiceMatchingDocListGet(this.serviceDocument, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return invoiceMatchingDocList;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }
    }
}
