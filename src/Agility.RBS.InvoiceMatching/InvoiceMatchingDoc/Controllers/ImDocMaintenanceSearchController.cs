﻿// <copyright file="ImDocMaintenanceSearchController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InvoiceMatching.InvoiceMatchingDoc.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.InvoiceMatching.Common;
    using Agility.RBS.InvoiceMatching.InvoiceMatchingDoc.Models;
    using Agility.RBS.InvoiceMatching.InvoiceMatchingDoc.Repositories;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class ImDocMaintenanceSearchController : Controller
    {
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly ServiceDocument<ImDocMaintenanceSearchModel> serviceDocument;
        private readonly InvoiceMatchingRepository invoiceMatchingRepository;
        private readonly DomainDataRepository domainDataRepository;

        public ImDocMaintenanceSearchController(
            ServiceDocument<ImDocMaintenanceSearchModel> serviceDocument,
            InvoiceMatchingRepository invoiceMatchingRepository,
            DomainDataRepository domainDataRepository)
        {
            this.serviceDocument = serviceDocument;
            this.invoiceMatchingRepository = invoiceMatchingRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<ImDocMaintenanceSearchModel>> List()
        {
            this.serviceDocument.DomainData.Add("documentType", this.domainDataRepository.DomainDetailData(InvoiceMatchingConstants.DomainHdrDocumentType).Result);
            this.serviceDocument.DomainData.Add("vendorType", this.domainDataRepository.DomainDetailData(InvoiceMatchingConstants.DomainHdrVendorType).Result);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<ImDocMaintenanceSearchModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.ImDocMaintenanceSearch);
            return this.serviceDocument;
        }

        private async Task<List<ImDocMaintenanceSearchModel>> ImDocMaintenanceSearch()
        {
            try
            {
                var imDocMaintenanceList = await this.invoiceMatchingRepository.ImDocMaintenanceListGet(this.serviceDocument, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return imDocMaintenanceList;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }
    }
}
