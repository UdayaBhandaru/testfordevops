﻿// <copyright file="InvoiceMatchingRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InvoiceMatching.InvoiceMatchingDoc.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.InvoiceMatching.Common;
    using Agility.RBS.InvoiceMatching.InvoiceMatchingDoc.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class InvoiceMatchingRepository : BaseOraclePackage
    {
        public InvoiceMatchingRepository()
        {
            this.PackageName = InvoiceMatchingConstants.GetInvoiceMatchingPackage;
        }

        public int? GroupId { get; private set; }

        public async Task<List<InvoiceMatchingDocSearchModel>> InvoiceMatchingDocListGet(ServiceDocument<InvoiceMatchingDocSearchModel> serviceDoc, ServiceDocumentResult serviceDocumentResult)
        {
            InvoiceMatchingDocSearchModel invoiceMatchingDocSearch = serviceDoc.DataProfile.DataModel;
            PackageParams packageParameter = this.GetPackageParams(InvoiceMatchingConstants.ObjTypeInvoiceMatchingDocSearch, InvoiceMatchingConstants.GetProcInvoiceMatchingDocList);
            OracleObject recordObject = this.SetParamsInvoiceMatchingDocList(invoiceMatchingDocSearch);

            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();

            BaseOracleParameter baseOracleParameter;
            baseOracleParameter = new BaseOracleParameter
            {
                Name = "P_START",
                DataType = OracleDbType.Number,
                Direction = ParameterDirection.Input,
                Value = Convert.ToString(invoiceMatchingDocSearch.StartRow)
            };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            baseOracleParameter = new BaseOracleParameter
            {
                Name = "P_END",
                DataType = OracleDbType.Number,
                Direction = ParameterDirection.Input,
                Value = Convert.ToString(invoiceMatchingDocSearch.EndRow)
            };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            baseOracleParameter = new BaseOracleParameter { Name = "P_total", DataType = OracleDbType.Number, Direction = ParameterDirection.Output };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            if (invoiceMatchingDocSearch.SortModel == null)
            {
                invoiceMatchingDocSearch.SortModel = new[] { new Core.Models.SortModel { ColId = null, Sort = null } };
            }

            baseOracleParameter = new BaseOracleParameter
            {
                Name = "P_SortColId",
                DataType = OracleDbType.VarChar,
                Direction = ParameterDirection.Input,
                Value = invoiceMatchingDocSearch.SortModel[0].ColId
            };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            baseOracleParameter = new BaseOracleParameter
            {
                Name = "P_SortOrder",
                DataType = OracleDbType.VarChar,
                Direction = ParameterDirection.Input,
                Value = invoiceMatchingDocSearch.SortModel[0].Sort
            };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));
            parameters.Add(this.ParameterBuilder(UserProfileHelper.GetUserParameter()));
            var invoiceMatchingDocList = await this.GetProcedure2<InvoiceMatchingDocSearchModel>(recordObject, packageParameter, serviceDocumentResult, parameters);
            invoiceMatchingDocSearch.TotalRows = (this.Parameters["p_total"].Value != null && this.Parameters["p_total"].Value != DBNull.Value) ? Convert.ToInt64(this.Parameters["p_total"].Value) : 1;
            serviceDoc.DataProfile.DataModel = invoiceMatchingDocSearch;
            return invoiceMatchingDocList;
        }

        public async Task<List<ImDocMaintenanceSearchModel>> ImDocMaintenanceListGet(ServiceDocument<ImDocMaintenanceSearchModel> serviceDoc, ServiceDocumentResult serviceDocumentResult)
        {
            ImDocMaintenanceSearchModel imDocMaintenanceSearch = serviceDoc.DataProfile.DataModel;
            PackageParams packageParameter = this.GetPackageParams(InvoiceMatchingConstants.ObjTypeImDocMaintenanceSearchTAB, InvoiceMatchingConstants.GetProcImDocMaintenanceList);
            OracleObject recordObject = this.SetParamsimDocMaintenanceList(imDocMaintenanceSearch);

            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();

            BaseOracleParameter baseOracleParameter;
            baseOracleParameter = new BaseOracleParameter
            {
                Name = "P_START",
                DataType = OracleDbType.Number,
                Direction = ParameterDirection.Input,
                Value = Convert.ToString(imDocMaintenanceSearch.StartRow)
            };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            baseOracleParameter = new BaseOracleParameter
            {
                Name = "P_END",
                DataType = OracleDbType.Number,
                Direction = ParameterDirection.Input,
                Value = Convert.ToString(imDocMaintenanceSearch.EndRow)
            };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            baseOracleParameter = new BaseOracleParameter { Name = "P_total", DataType = OracleDbType.Number, Direction = ParameterDirection.Output };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            if (imDocMaintenanceSearch.SortModel == null)
            {
                imDocMaintenanceSearch.SortModel = new[] { new Core.Models.SortModel { ColId = null, Sort = null } };
            }

            baseOracleParameter = new BaseOracleParameter
            {
                Name = "P_SortColId",
                DataType = OracleDbType.VarChar,
                Direction = ParameterDirection.Input,
                Value = imDocMaintenanceSearch.SortModel[0].ColId
            };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            baseOracleParameter = new BaseOracleParameter
            {
                Name = "P_SortOrder",
                DataType = OracleDbType.VarChar,
                Direction = ParameterDirection.Input,
                Value = imDocMaintenanceSearch.SortModel[0].Sort
            };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));
            parameters.Add(this.ParameterBuilder(UserProfileHelper.GetUserParameter()));
            var imDocMaintenanceDocList = await this.GetProcedure2<ImDocMaintenanceSearchModel>(recordObject, packageParameter, serviceDocumentResult, parameters);
            imDocMaintenanceSearch.TotalRows = (this.Parameters["p_total"].Value != null && this.Parameters["p_total"].Value != DBNull.Value) ? Convert.ToInt64(this.Parameters["p_total"].Value) : 1;
            serviceDoc.DataProfile.DataModel = imDocMaintenanceSearch;
            return imDocMaintenanceDocList;
        }

        public async Task<List<ImQtyDiscrepancySearchModel>> ImQtyDiscrepancyListGet(ServiceDocument<ImQtyDiscrepancySearchModel> serviceDocument, ServiceDocumentResult serviceDocumentResult)
        {
            ImQtyDiscrepancySearchModel imQtyDiscrepancySearch = serviceDocument.DataProfile.DataModel;
            PackageParams packageParameter = this.GetPackageParams(InvoiceMatchingConstants.ObjTypeImQtyDiscrepancyTab, InvoiceMatchingConstants.GetProcImQtyDiscrepancyList);
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(InvoiceMatchingConstants.RecordObjImQtyDiscrepancyRec, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);

            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();

            BaseOracleParameter baseOracleParameter;
            baseOracleParameter = new BaseOracleParameter
            {
                Name = "P_START",
                DataType = OracleDbType.Number,
                Direction = ParameterDirection.Input,
                Value = Convert.ToString(imQtyDiscrepancySearch.StartRow)
            };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            baseOracleParameter = new BaseOracleParameter
            {
                Name = "P_END",
                DataType = OracleDbType.Number,
                Direction = ParameterDirection.Input,
                Value = Convert.ToString(imQtyDiscrepancySearch.EndRow)
            };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            baseOracleParameter = new BaseOracleParameter { Name = "P_total", DataType = OracleDbType.Number, Direction = ParameterDirection.Output };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            if (imQtyDiscrepancySearch.SortModel == null)
            {
                imQtyDiscrepancySearch.SortModel = new[] { new Core.Models.SortModel { ColId = null, Sort = null } };
            }

            baseOracleParameter = new BaseOracleParameter
            {
                Name = "P_SortColId",
                DataType = OracleDbType.VarChar,
                Direction = ParameterDirection.Input,
                Value = imQtyDiscrepancySearch.SortModel[0].ColId
            };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            baseOracleParameter = new BaseOracleParameter
            {
                Name = "P_SortOrder",
                DataType = OracleDbType.VarChar,
                Direction = ParameterDirection.Input,
                Value = imQtyDiscrepancySearch.SortModel[0].Sort
            };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));
            parameters.Add(this.ParameterBuilder(UserProfileHelper.GetUserParameter()));
            OracleTable pDomainMstTab = await this.GetProcedure(recordObject, packageParameter, serviceDocumentResult, parameters);
            List<ImQtyDiscrepancySearchModel> imQtyDiscrepancyList = Mapper.Map<List<ImQtyDiscrepancySearchModel>>(pDomainMstTab);
            if (imQtyDiscrepancyList != null)
            {
                for (int i = 0; i < imQtyDiscrepancyList.Count; i++)
                {
                    OracleObject obj = (OracleObject)pDomainMstTab[i];
                    OracleTable domainDetails = (Devart.Data.Oracle.OracleTable)obj["DISC_DTL_TAB"];
                    if (domainDetails.Count > 0)
                    {
                        imQtyDiscrepancyList[i].ImQtyDiscrepancyDetails.AddRange(Mapper.Map<List<ImQtyDiscrepancyDetailsModel>>(domainDetails));
                    }
                }
            }

            imQtyDiscrepancySearch.TotalRows = (this.Parameters["p_total"].Value != null && this.Parameters["p_total"].Value != DBNull.Value) ? Convert.ToInt64(this.Parameters["p_total"].Value) : 1;
            serviceDocument.DataProfile.DataModel = imQtyDiscrepancySearch;
            return imQtyDiscrepancyList;
        }

        public async Task<List<ImCostDiscrepancySearchModel>> ImCostDiscrepancyListGet(ServiceDocument<ImCostDiscrepancySearchModel> serviceDocument, ServiceDocumentResult serviceDocumentResult)
        {
            ImCostDiscrepancySearchModel imCostDiscrepancySearch = serviceDocument.DataProfile.DataModel;
            PackageParams packageParameter = this.GetPackageParams(InvoiceMatchingConstants.ObjTypeImCostDiscrepancyTab, InvoiceMatchingConstants.GetProcImCostDiscrepancyList);
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(InvoiceMatchingConstants.RecordObjImCostDiscrepancyRec, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);

            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();

            BaseOracleParameter baseOracleParameter;
            baseOracleParameter = new BaseOracleParameter
            {
                Name = "P_START",
                DataType = OracleDbType.Number,
                Direction = ParameterDirection.Input,
                Value = Convert.ToString(imCostDiscrepancySearch.StartRow)
            };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            baseOracleParameter = new BaseOracleParameter
            {
                Name = "P_END",
                DataType = OracleDbType.Number,
                Direction = ParameterDirection.Input,
                Value = Convert.ToString(imCostDiscrepancySearch.EndRow)
            };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            baseOracleParameter = new BaseOracleParameter { Name = "P_total", DataType = OracleDbType.Number, Direction = ParameterDirection.Output };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            if (imCostDiscrepancySearch.SortModel == null)
            {
                imCostDiscrepancySearch.SortModel = new[] { new Core.Models.SortModel { ColId = null, Sort = null } };
            }

            baseOracleParameter = new BaseOracleParameter
            {
                Name = "P_SortColId",
                DataType = OracleDbType.VarChar,
                Direction = ParameterDirection.Input,
                Value = imCostDiscrepancySearch.SortModel[0].ColId
            };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            baseOracleParameter = new BaseOracleParameter
            {
                Name = "P_SortOrder",
                DataType = OracleDbType.VarChar,
                Direction = ParameterDirection.Input,
                Value = imCostDiscrepancySearch.SortModel[0].Sort
            };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            parameters.Add(this.ParameterBuilder(UserProfileHelper.GetUserParameter()));

            OracleTable pDomainMstTab = await this.GetProcedure(recordObject, packageParameter, serviceDocumentResult, parameters);
            List<ImCostDiscrepancySearchModel> imCostDiscrepancyList = Mapper.Map<List<ImCostDiscrepancySearchModel>>(pDomainMstTab);
            if (imCostDiscrepancyList != null)
            {
                for (int i = 0; i < imCostDiscrepancyList.Count; i++)
            {
                OracleObject obj = (OracleObject)pDomainMstTab[i];
                OracleTable domainDetails = (Devart.Data.Oracle.OracleTable)obj["DISC_DTL_TAB"];
                if (domainDetails.Count > 0)
                {
                        imCostDiscrepancyList[i].ImCostReviewDetails.AddRange(Mapper.Map<List<ImCostReviewDetailModel>>(domainDetails));
                }
            }
            }

            imCostDiscrepancySearch.TotalRows = (this.Parameters["p_total"].Value != null && this.Parameters["p_total"].Value != DBNull.Value) ? Convert.ToInt64(this.Parameters["p_total"].Value) : 1;
            serviceDocument.DataProfile.DataModel = imCostDiscrepancySearch;
            return imCostDiscrepancyList;
        }

        public async Task<ServiceDocumentResult> SaveVat(List<InvoiceMatchingVatModel> invoiceMatchingVatModel)
        {
            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();
            PackageParams packageParameter = this.GetPackageParams(InvoiceMatchingConstants.ObjTypeVatTAB, InvoiceMatchingConstants.SetProcVat);
            OracleParameter parameter;
            parameter = new OracleParameter("RESULT", Devart.Data.Oracle.OracleDbType.Boolean)
            {
                Direction = System.Data.ParameterDirection.ReturnValue
            };
            parameters.Add(parameter);
            parameter = new OracleParameter(packageParameter.ObjectTypeTable, Devart.Data.Oracle.OracleDbType.Table)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                ObjectTypeName = packageParameter.ObjectTypeName,
                Value = this.SetParamsvat(invoiceMatchingVatModel)
            };
            parameters.Add(parameter);
            var result = await this.SetProcedure(null, packageParameter, parameters);
            return result;
        }

        public async Task<ServiceDocumentResult> NonMerchandiseCostSave(List<NonMerchandiseCostModel> nonMerchandiseCostModel)
        {
            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();
            PackageParams packageParameter = this.GetPackageParams(InvoiceMatchingConstants.ObjTypeNonMerchandiseCostTAB, InvoiceMatchingConstants.SetProcNonMerchandiseCost);
            OracleParameter parameter;
            parameter = new OracleParameter("RESULT", Devart.Data.Oracle.OracleDbType.Boolean)
            {
                Direction = System.Data.ParameterDirection.ReturnValue
            };
            parameters.Add(parameter);
            parameter = new OracleParameter(packageParameter.ObjectTypeTable, Devart.Data.Oracle.OracleDbType.Table)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                ObjectTypeName = packageParameter.ObjectTypeName,
                Value = this.SetParamsNonMerchandiseCost(nonMerchandiseCostModel)
            };
            parameters.Add(parameter);
            var result = await this.SetProcedure(null, packageParameter, parameters);
            return result;
        }

        public async Task<ServiceDocumentResult> SetInvoiceMatchingDoc(InvoiceMatchingDocGroupHeadModel dataModel)
        {
            this.GroupId = 0;

            PackageParams packageParameter = this.GetPackageParams(InvoiceMatchingConstants.ObjTypeInvoiceMatchingDocTAB, InvoiceMatchingConstants.SetProcInvoiceMatchingDoc);
            OracleObject recordObject = this.SetParamsInvoiceMatchingDoc(dataModel);
            var result = await this.SetProcedure(recordObject, packageParameter);

            if (this.ObjResult != null && dataModel.GROUP_ID == null)
            {
                this.GroupId = Convert.ToInt32(this.ObjResult["GROUP_ID"]);
            }
            else
            {
                this.GroupId = dataModel.GROUP_ID;
            }

            return result;
        }

        public async Task<InvoiceMatchingDocGroupHeadModel> InvoiceMatchingDocGet(int groupId, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(InvoiceMatchingConstants.ObjTypeInvoiceMatchingDocREC, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["GROUP_ID"]] = groupId;
            PackageParams packageParameter = this.GetPackageParams(InvoiceMatchingConstants.ObjTypeInvoiceMatchingDocTAB, InvoiceMatchingConstants.GetProcInvoiceMatchingGroupDocHead);
            OracleTable pDomainMstTab = await this.GetProcedure(recordObject, packageParameter, serviceDocumentResult);
            List<InvoiceMatchingDocGroupHeadModel> invoicematchgrouphead = Mapper.Map<List<InvoiceMatchingDocGroupHeadModel>>(pDomainMstTab);

            if (invoicematchgrouphead != null)
            {
                OracleObject obj = (OracleObject)pDomainMstTab[0];
                OracleTable domainDetails = (Devart.Data.Oracle.OracleTable)obj["doc_head_tab"];
                if (domainDetails.Count > 0)
                {
                    invoicematchgrouphead[0].InvoiceMatchingDocHead.AddRange(Mapper.Map<List<InvoiceMatchingDocHeadModel>>(domainDetails));
                }
            }

            return invoicematchgrouphead != null ? invoicematchgrouphead[0] : null;
        }

        public async Task<InvoiceMatchingDocOrderDetailsModel> OrderDetailsGet(int orderno)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("INV_ORD_DTL_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            recordObject["order_no"] = orderno;
            PackageParams packageParameter = this.GetPackageParams(InvoiceMatchingConstants.ObjTypeOrderDetailsTAB, InvoiceMatchingConstants.GetProcOrderDetails);
            var orderdetails = await this.GetProcedure2<InvoiceMatchingDocOrderDetailsModel>(recordObject, packageParameter, null);
            return orderdetails != null ? orderdetails[0] : null;
        }

        public async Task<List<NonMerchandiseCostModel>> NonMerchandiseCostGet(int docId, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("im_doc_non_merch_rec");
            OracleObject recordObject = this.GetOracleObject(recordType);
            recordObject["DOC_ID"] = docId;
            PackageParams packageParameter = this.GetPackageParams(InvoiceMatchingConstants.ObjTypeNonMerchandiseCostTAB, InvoiceMatchingConstants.SetProcNonMerchandiseCostList);
            var nonMerchandiseCostdetails = await this.GetProcedure2<NonMerchandiseCostModel>(recordObject, packageParameter, serviceDocumentResult);
            return nonMerchandiseCostdetails ?? new List<NonMerchandiseCostModel>();
        }

        public async Task<List<InvoiceMatchingVatModel>> VatGet(int docId, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("im_doc_vat_rec");
            OracleObject recordObject = this.GetOracleObject(recordType);
            recordObject["DOC_ID"] = docId;
            PackageParams packageParameter = this.GetPackageParams(InvoiceMatchingConstants.ObjTypeVatTAB, InvoiceMatchingConstants.SetProcVatList);
            var vatdetails = await this.GetProcedure2<InvoiceMatchingVatModel>(recordObject, packageParameter, serviceDocumentResult);
            return vatdetails ?? new List<InvoiceMatchingVatModel>();
        }

        public async Task<ServiceDocumentResult> SetImDocCost(ImCostDiscrepancySearchModel dataModel)
        {
            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();
            PackageParams packageParameter = this.GetPackageParams(InvoiceMatchingConstants.ObjTypeIMDocCostDiscTAB, InvoiceMatchingConstants.SetProcIMDocCostDisc);
            OracleParameter parameter;
            parameter = new OracleParameter("RESULT", Devart.Data.Oracle.OracleDbType.Boolean)
            {
                Direction = System.Data.ParameterDirection.ReturnValue
            };
            parameters.Add(parameter);
            parameter = new OracleParameter(packageParameter.ObjectTypeTable, Devart.Data.Oracle.OracleDbType.Table)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                ObjectTypeName = packageParameter.ObjectTypeName,
                Value = this.SetParamsImDocCostDisc(dataModel)
            };
            parameters.Add(parameter);
            var result = await this.SetProcedure(null, packageParameter, parameters);
            return result;
        }

        public async Task<ServiceDocumentResult> SetImQtyDiscrepancy(ImQtyDiscrepancySearchModel dataModel)
        {
            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();
            PackageParams packageParameter = this.GetPackageParams(InvoiceMatchingConstants.ObjTypeImQtyDiscTAB, InvoiceMatchingConstants.SetProcImQtyDisc);
            OracleParameter parameter;
            parameter = new OracleParameter("RESULT", Devart.Data.Oracle.OracleDbType.Boolean)
            {
                Direction = System.Data.ParameterDirection.ReturnValue
            };
            parameters.Add(parameter);
            parameter = new OracleParameter(packageParameter.ObjectTypeTable, Devart.Data.Oracle.OracleDbType.Table)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                ObjectTypeName = packageParameter.ObjectTypeName,
                Value = this.SetParamsImQtyDiscrepancy(dataModel)
            };
            parameters.Add(parameter);
            var result = await this.SetProcedure(null, packageParameter, parameters);
            return result;
        }

        public async Task<ServiceDocumentResult> SetImDocMaintenanceHead(ImDocMaintenanceHeadModel imDocmodel)
        {
            PackageParams packageParameter = this.GetPackageParams(InvoiceMatchingConstants.ObjTypeImDocHeadTab, InvoiceMatchingConstants.SetProcImDocHead);
            OracleObject recordObject = this.SetParamsmDocMaintenanceHead(imDocmodel);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        private OracleObject SetParamsmDocMaintenanceHead(ImDocMaintenanceHeadModel imDocmodel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("im_doc_head_rec");
            OracleObject recordObject = this.GetOracleObject(recordType);
            recordObject[recordType.Attributes["DOC_ID"]] = imDocmodel.DocId;
            recordObject[recordType.Attributes["TYPE"]] = imDocmodel.Type;
            recordObject[recordType.Attributes["TYPE_DESC"]] = imDocmodel.TypeDesc;
            recordObject[recordType.Attributes["STATUS"]] = imDocmodel.Status;
            recordObject[recordType.Attributes["STATUS_DESC"]] = imDocmodel.StatusDesc;
            recordObject[recordType.Attributes["ORDER_NO"]] = imDocmodel.OrderNO;
            recordObject[recordType.Attributes["LOCATION"]] = imDocmodel.Location;
            recordObject[recordType.Attributes["LOC_NAME"]] = imDocmodel.LocName;
            recordObject[recordType.Attributes["LOC_TYPE"]] = imDocmodel.LocType;
            recordObject[recordType.Attributes["LOC_TYPE_DESC"]] = imDocmodel.LocTypeDesc;
            recordObject[recordType.Attributes["TOTAL_DISCOUNT"]] = imDocmodel.TotalDiscount;
            recordObject[recordType.Attributes["GROUP_ID"]] = imDocmodel.GroupId;
            recordObject[recordType.Attributes["PARENT_ID"]] = imDocmodel.ParentId;
            recordObject[recordType.Attributes["DOC_DATE"]] = imDocmodel.DocDate;
            recordObject[recordType.Attributes["CREATE_DATE"]] = DateTime.UtcNow.Date;
            recordObject[recordType.Attributes["CREATE_ID"]] = imDocmodel.CreateId;
            recordObject[recordType.Attributes["VENDOR_TYPE"]] = imDocmodel.VendorType;
            recordObject[recordType.Attributes["VENDOR_TYPE_DESC"]] = imDocmodel.VendorTypeDesc;
            recordObject[recordType.Attributes["VENDOR"]] = imDocmodel.Vendor;
            recordObject[recordType.Attributes["VENDOR_NAME"]] = imDocmodel.VendorName;
            recordObject[recordType.Attributes["EXT_DOC_ID"]] = "1";
            recordObject[recordType.Attributes["EDI_UPLOAD_IND"]] = imDocmodel.EdiUploadInd;
            recordObject[recordType.Attributes["EDI_DOWNLOAD_IND"]] = imDocmodel.EdiDownloadInd;
            recordObject[recordType.Attributes["TERMS"]] = imDocmodel.Terms;
            recordObject[recordType.Attributes["TERMS_DSCNT_PCT"]] = imDocmodel.TermsDscntPct;
            recordObject[recordType.Attributes["DUE_DATE"]] = imDocmodel.DocDate;
            recordObject[recordType.Attributes["PAYMENT_METHOD"]] = imDocmodel.PaymentMethod;
            recordObject[recordType.Attributes["PAYMENT_METHOD_DESC"]] = imDocmodel.PaymentMethodDesc;
            recordObject[recordType.Attributes["MATCH_ID"]] = imDocmodel.MatchId;
            recordObject[recordType.Attributes["MATCH_DATE"]] = imDocmodel.MatchDate;
            recordObject[recordType.Attributes["APPROVAL_ID"]] = imDocmodel.ApprovalId;
            recordObject[recordType.Attributes["APPROVAL_DATE"]] = imDocmodel.ApprovalDate;
            recordObject[recordType.Attributes["PRE_PAID_IND"]] = imDocmodel.PrePaidInd;
            recordObject[recordType.Attributes["PRE_PAID_ID"]] = imDocmodel.PrePaidId;
            recordObject[recordType.Attributes["POST_DATE"]] = imDocmodel.PostDate;
            recordObject[recordType.Attributes["CURRENCY_CODE"]] = imDocmodel.CurrencyCode;
            recordObject[recordType.Attributes["EXCHANGE_RATE"]] = imDocmodel.ExchangeRate;
            recordObject[recordType.Attributes["TOTAL_COST"]] = imDocmodel.TotalCost;
            recordObject[recordType.Attributes["TOTAL_QTY"]] = imDocmodel.TotalQty;
            recordObject[recordType.Attributes["MANUALLY_PAID_IND"]] = imDocmodel.ManuallyPaidInd;
            recordObject[recordType.Attributes["CUSTOM_DOC_REF_1"]] = imDocmodel.CustomDocRef1;
            recordObject[recordType.Attributes["CUSTOM_DOC_REF_2"]] = imDocmodel.CustomDocRef2;
            recordObject[recordType.Attributes["CUSTOM_DOC_REF_3"]] = imDocmodel.CustomDocRef3;
            recordObject[recordType.Attributes["CUSTOM_DOC_REF_4"]] = imDocmodel.CustomDocRef4;
            recordObject[recordType.Attributes["LAST_UPDATE_ID"]] = FxContext.Context.EmailId;
            recordObject[recordType.Attributes["LAST_DATETIME"]] = DateTime.UtcNow.Date;
            recordObject[recordType.Attributes["FREIGHT_TYPE"]] = imDocmodel.FreightType;
            recordObject[recordType.Attributes["FREIGHT_TYPE_DESC"]] = imDocmodel.FreightType;
            recordObject[recordType.Attributes["REF_DOC"]] = imDocmodel.RefDoc;
            recordObject[recordType.Attributes["REF_AUTH_NO"]] = imDocmodel.RefAuthNo;
            recordObject[recordType.Attributes["COST_PRE_MATCH"]] = imDocmodel.CostFreMatch;
            recordObject[recordType.Attributes["DETAIL_MATCHED"]] = imDocmodel.DetailMatched;
            recordObject[recordType.Attributes["BEST_TERMS"]] = imDocmodel.BestTerms;
            recordObject[recordType.Attributes["BEST_TERMS_SOURCE"]] = imDocmodel.BestTermsSource;
            recordObject[recordType.Attributes["BEST_TERMS_DATE"]] = imDocmodel.BestTermsDate;
            recordObject[recordType.Attributes["BEST_TERMS_DATE_SOURCE"]] = imDocmodel.BestTermsDateSource;
            recordObject[recordType.Attributes["VARIANCE_WITHIN_TOLERANCE"]] = imDocmodel.VarianceWithinTolerance;
            recordObject[recordType.Attributes["RESOLUTION_ADJUSTED_TOTAL_COST"]] = imDocmodel.ResolutionAdjustedTotalCost;
            recordObject[recordType.Attributes["RESOLUTION_ADJUSTED_TOTAL_QTY"]] = imDocmodel.ResolutionAdjustedTotalQty;
            recordObject[recordType.Attributes["CONSIGNMENT_IND"]] = imDocmodel.ConsignmentInd;
            recordObject[recordType.Attributes["DEAL_ID"]] = imDocmodel.DealId;
            recordObject[recordType.Attributes["RTV_IND"]] = imDocmodel.RtvInd;
            recordObject[recordType.Attributes["DISCOUNT_DATE"]] = imDocmodel.DiscountDate;
            recordObject[recordType.Attributes["DEAL_TYPE"]] = imDocmodel.DealType;
            recordObject[recordType.Attributes["TOTAL_COST_INC_VAT"]] = imDocmodel.TotalCostIncVat;
            recordObject[recordType.Attributes["VAT_DISC_CREATE_DATE"]] = imDocmodel.VatDiscCreateDate;
            recordObject[recordType.Attributes["OPERATION"]] = imDocmodel.Operation;
            return recordObject;
        }

        private OracleTable SetParamsvat(List<InvoiceMatchingVatModel> vatModel)
        {
            this.Connection.Open();
            OracleType indocheadVatTableType = OracleType.GetObjectType(InvoiceMatchingConstants.ObjTypeVatTAB, this.Connection);
            OracleType indocheadrecordType = OracleType.GetObjectType(InvoiceMatchingConstants.ObjTypeVatREC, this.Connection);
            OracleTable pInvoiceMatchingDocVatTab = new OracleTable(indocheadVatTableType);

            foreach (InvoiceMatchingVatModel model in vatModel)
            {
                OracleObject recordObjectdtls = new OracleObject(indocheadrecordType);

                recordObjectdtls[indocheadrecordType.Attributes["DOC_ID"]] = model.DocId;
                recordObjectdtls[indocheadrecordType.Attributes["VAT_CODE"]] = model.VatCode;
                recordObjectdtls[indocheadrecordType.Attributes["VAT_RATE"]] = model.VatRate;
                recordObjectdtls[indocheadrecordType.Attributes["VAT_BASIS"]] = model.VatBasis;
                recordObjectdtls[indocheadrecordType.Attributes["OPERATION"]] = model.Operation;
                pInvoiceMatchingDocVatTab.Add(recordObjectdtls);
            }

            return pInvoiceMatchingDocVatTab;
        }

        private OracleTable SetParamsNonMerchandiseCost(List<NonMerchandiseCostModel> nonMerchandiseCostModel)
        {
            this.Connection.Open();
            OracleType indocheadTableType = OracleType.GetObjectType(InvoiceMatchingConstants.ObjTypeNonMerchandiseCostTAB, this.Connection);
            OracleType indocheadrecordType = OracleType.GetObjectType(InvoiceMatchingConstants.ObjTypeNonMerchandiseCostREC, this.Connection);
            OracleTable pInvoiceMatchingDocTab = new OracleTable(indocheadTableType);

            foreach (NonMerchandiseCostModel model in nonMerchandiseCostModel)
            {
                OracleObject recordObjectdtls = new OracleObject(indocheadrecordType);

                recordObjectdtls[indocheadrecordType.Attributes["DOC_ID"]] = model.DocId;
                recordObjectdtls[indocheadrecordType.Attributes["NON_MERCH_CODE"]] = model.NonMerchCode;
                recordObjectdtls[indocheadrecordType.Attributes["NON_MERCH_CODE_DESC"]] = model.NonMerchCodeDesc;
                recordObjectdtls[indocheadrecordType.Attributes["NON_MERCH_AMT"]] = model.NonMerchAmt;
                recordObjectdtls[indocheadrecordType.Attributes["VAT_CODE"]] = model.VatCode;
                recordObjectdtls[indocheadrecordType.Attributes["VAT_RATE"]] = model.VatRate;
                recordObjectdtls[indocheadrecordType.Attributes["OPERATION"]] = model.Operation;
                pInvoiceMatchingDocTab.Add(recordObjectdtls);
            }

            return pInvoiceMatchingDocTab;
        }

        private OracleObject SetParamsInvoiceMatchingDocList(InvoiceMatchingDocSearchModel invoiceMatchingDocSearch)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(InvoiceMatchingConstants.RecordObjInvoiceMatchingDocSearch, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);

            recordObject[recordType.Attributes["GROUP_ID"]] = invoiceMatchingDocSearch.GroupId;
            recordObject[recordType.Attributes["CREATE_DATE"]] = invoiceMatchingDocSearch.CreateDate;
            recordObject[recordType.Attributes["CREATE_ID"]] = invoiceMatchingDocSearch.CreateId;
            recordObject[recordType.Attributes["STATUS"]] = invoiceMatchingDocSearch.Status;
            recordObject[recordType.Attributes["STATUS_DESC"]] = invoiceMatchingDocSearch.StatusDesc;
            recordObject[recordType.Attributes["CONTROL_REC_COUNT"]] = invoiceMatchingDocSearch.ControlRecCount;
            recordObject[recordType.Attributes["CALC_COUNT"]] = invoiceMatchingDocSearch.CalcCount;
            recordObject[recordType.Attributes["VARRIANCE_COUNT"]] = invoiceMatchingDocSearch.VarrianceCount;
            recordObject[recordType.Attributes["CONTROL_TOTAL_COST"]] = invoiceMatchingDocSearch.ControlTotalCost;
            recordObject[recordType.Attributes["CALC_COST"]] = invoiceMatchingDocSearch.CalcCost;
            recordObject[recordType.Attributes["VARRIANCE_COST"]] = invoiceMatchingDocSearch.VarrianceCost;
            recordObject[recordType.Attributes["CURRENCY_CODE"]] = invoiceMatchingDocSearch.CurrencyCode;
            recordObject[recordType.Attributes["LAST_UPDATE_ID"]] = FxContext.Context.EmailId;
            recordObject[recordType.Attributes["LAST_UPDATE_DATETIME"]] = invoiceMatchingDocSearch.LastUpdateDateTime;
            return recordObject;
        }

        private OracleObject SetParamsimDocMaintenanceList(ImDocMaintenanceSearchModel imDocMaintenanceSearchModel)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(InvoiceMatchingConstants.RecordObjImDocMaintenanceREC, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);

            recordObject[recordType.Attributes["DOC_ID"]] = imDocMaintenanceSearchModel.DocId;
            recordObject[recordType.Attributes["TYPE"]] = imDocMaintenanceSearchModel.Type;
            recordObject[recordType.Attributes["TYPE_DESC"]] = imDocMaintenanceSearchModel.TypeDesc;
            recordObject[recordType.Attributes["STATUS"]] = imDocMaintenanceSearchModel.Status;
            recordObject[recordType.Attributes["STATUS_DESC"]] = imDocMaintenanceSearchModel.StatusDesc;
            recordObject[recordType.Attributes["ORDER_NO"]] = imDocMaintenanceSearchModel.OrderNo;
            recordObject[recordType.Attributes["LOCATION"]] = imDocMaintenanceSearchModel.Location;
            recordObject[recordType.Attributes["LOC_NAME"]] = imDocMaintenanceSearchModel.LocName;
            recordObject[recordType.Attributes["LOC_TYPE"]] = imDocMaintenanceSearchModel.LocType;
            recordObject[recordType.Attributes["GROUP_ID"]] = imDocMaintenanceSearchModel.GroupId;
            recordObject[recordType.Attributes["DOC_DATE"]] = imDocMaintenanceSearchModel.DocDate;
            recordObject[recordType.Attributes["VENDOR_TYPE"]] = imDocMaintenanceSearchModel.VendorType;
            recordObject[recordType.Attributes["VENDOR_TYPE_DESC"]] = imDocMaintenanceSearchModel.VendorTypeDesc;
            recordObject[recordType.Attributes["VENDOR"]] = imDocMaintenanceSearchModel.Vendor;
            recordObject[recordType.Attributes["VENDOR_DESC"]] = imDocMaintenanceSearchModel.VendorDesc;
            recordObject[recordType.Attributes["EXT_DOC_ID"]] = imDocMaintenanceSearchModel.ExtDocId;
            recordObject[recordType.Attributes["DUE_DATE"]] = imDocMaintenanceSearchModel.DueDate;
            recordObject[recordType.Attributes["REF_DOC"]] = imDocMaintenanceSearchModel.RefDoc;

            return recordObject;
        }

        private OracleObject SetParamsInvoiceMatchingDoc(InvoiceMatchingDocGroupHeadModel dataModel)
        {
            this.Connection.Open();
            OracleType headerrecordType = this.GetObjectType(InvoiceMatchingConstants.ObjTypeInvoiceMatchingDocREC);
            OracleObject headRecordObject = new OracleObject(headerrecordType);

            headRecordObject["GROUP_ID"] = dataModel.GROUP_ID;
            headRecordObject["CREATE_DATE"] = dataModel.CreateDate;
            headRecordObject["CREATE_ID"] = dataModel.CreateId;
            headRecordObject["STATUS"] = "WKSHT";
            headRecordObject["CONTROL_REC_COUNT"] = dataModel.ControlRecCount;
            headRecordObject["CALC_COUNT"] = dataModel.CalcCount;
            headRecordObject["CONTROL_TOTAL_COST"] = dataModel.ControlTotalCost;
            headRecordObject["CALC_COST"] = dataModel.CalcCost;
            headRecordObject["CURRENCY_CODE"] = dataModel.CurrencyCode;
            headRecordObject["LAST_UPDATE_ID"] = FxContext.Context.EmailId;
            headRecordObject["LAST_UPDATE_DATETIME"] = DateTime.UtcNow.Date;
            headRecordObject["OPERATION"] = dataModel.Operation;

            OracleType indocheadTableType = OracleType.GetObjectType(InvoiceMatchingConstants.ObjTypeInvoiceMatchingDocHeadDetails, this.Connection);
            OracleType indocheadrecordType = OracleType.GetObjectType(InvoiceMatchingConstants.ObjTypeInvoiceMatchingDocHeadDetailsREC, this.Connection);
            OracleTable pInvoiceMatchingDocTab = new OracleTable(indocheadTableType);

            foreach (InvoiceMatchingDocHeadModel detail in dataModel.InvoiceMatchingDocHead)
            {
                OracleObject recordObjectdtls = new OracleObject(indocheadrecordType);
                recordObjectdtls[indocheadrecordType.Attributes["DOC_ID"]] = detail.DocId;
                recordObjectdtls[indocheadrecordType.Attributes["TYPE"]] = detail.Type;
                recordObjectdtls[indocheadrecordType.Attributes["TYPE_DESC"]] = detail.TypeDesc;
                recordObjectdtls[indocheadrecordType.Attributes["STATUS"]] = detail.Status;
                recordObjectdtls[indocheadrecordType.Attributes["STATUS_DESC"]] = detail.StatusDesc;
                recordObjectdtls[indocheadrecordType.Attributes["ORDER_NO"]] = detail.OrderNO;
                recordObjectdtls[indocheadrecordType.Attributes["LOCATION"]] = detail.Location;
                recordObjectdtls[indocheadrecordType.Attributes["LOC_NAME"]] = detail.LocName;
                recordObjectdtls[indocheadrecordType.Attributes["LOC_TYPE"]] = detail.LocType;
                recordObjectdtls[indocheadrecordType.Attributes["LOC_TYPE_DESC"]] = detail.LocTypeDesc;
                recordObjectdtls[indocheadrecordType.Attributes["TOTAL_DISCOUNT"]] = detail.TotalDiscount;
                recordObjectdtls[indocheadrecordType.Attributes["GROUP_ID"]] = detail.GroupId;
                recordObjectdtls[indocheadrecordType.Attributes["PARENT_ID"]] = detail.ParentId;
                recordObjectdtls[indocheadrecordType.Attributes["DOC_DATE"]] = detail.DocDate;
                recordObjectdtls[indocheadrecordType.Attributes["CREATE_DATE"]] = DateTime.UtcNow.Date;
                recordObjectdtls[indocheadrecordType.Attributes["CREATE_ID"]] = detail.CreateId;
                recordObjectdtls[indocheadrecordType.Attributes["VENDOR_TYPE"]] = detail.VendorType;
                recordObjectdtls[indocheadrecordType.Attributes["VENDOR_TYPE_DESC"]] = detail.VendorTypeDesc;
                recordObjectdtls[indocheadrecordType.Attributes["VENDOR"]] = detail.Vendor;
                recordObjectdtls[indocheadrecordType.Attributes["VENDOR_NAME"]] = detail.VendorName;
                recordObjectdtls[indocheadrecordType.Attributes["EXT_DOC_ID"]] = "1";
                recordObjectdtls[indocheadrecordType.Attributes["EDI_UPLOAD_IND"]] = detail.EdiUploadInd;
                recordObjectdtls[indocheadrecordType.Attributes["EDI_DOWNLOAD_IND"]] = detail.EdiDownloadInd;
                recordObjectdtls[indocheadrecordType.Attributes["TERMS"]] = detail.Terms;
                recordObjectdtls[indocheadrecordType.Attributes["TERMS_DSCNT_PCT"]] = detail.TermsDscntPct;
                recordObjectdtls[indocheadrecordType.Attributes["DUE_DATE"]] = detail.DocDate;
                recordObjectdtls[indocheadrecordType.Attributes["PAYMENT_METHOD"]] = detail.PaymentMethod;
                recordObjectdtls[indocheadrecordType.Attributes["PAYMENT_METHOD_DESC"]] = detail.PaymentMethodDesc;
                recordObjectdtls[indocheadrecordType.Attributes["MATCH_ID"]] = detail.MatchId;
                recordObjectdtls[indocheadrecordType.Attributes["MATCH_DATE"]] = detail.MatchDate;
                recordObjectdtls[indocheadrecordType.Attributes["APPROVAL_ID"]] = detail.ApprovalId;
                recordObjectdtls[indocheadrecordType.Attributes["APPROVAL_DATE"]] = detail.ApprovalDate;
                recordObjectdtls[indocheadrecordType.Attributes["PRE_PAID_IND"]] = detail.PrePaidInd;
                recordObjectdtls[indocheadrecordType.Attributes["PRE_PAID_ID"]] = detail.PrePaidId;
                recordObjectdtls[indocheadrecordType.Attributes["POST_DATE"]] = detail.PostDate;
                recordObjectdtls[indocheadrecordType.Attributes["CURRENCY_CODE"]] = detail.CurrencyCode;
                recordObjectdtls[indocheadrecordType.Attributes["EXCHANGE_RATE"]] = detail.ExchangeRate;
                recordObjectdtls[indocheadrecordType.Attributes["TOTAL_COST"]] = detail.TotalCost;
                recordObjectdtls[indocheadrecordType.Attributes["TOTAL_QTY"]] = detail.TotalQty;
                recordObjectdtls[indocheadrecordType.Attributes["MANUALLY_PAID_IND"]] = detail.ManuallyPaidInd;
                recordObjectdtls[indocheadrecordType.Attributes["CUSTOM_DOC_REF_1"]] = detail.CustomDocRef1;
                recordObjectdtls[indocheadrecordType.Attributes["CUSTOM_DOC_REF_2"]] = detail.CustomDocRef2;
                recordObjectdtls[indocheadrecordType.Attributes["CUSTOM_DOC_REF_3"]] = detail.CustomDocRef3;
                recordObjectdtls[indocheadrecordType.Attributes["CUSTOM_DOC_REF_4"]] = detail.CustomDocRef4;
                recordObjectdtls[indocheadrecordType.Attributes["LAST_UPDATE_ID"]] = FxContext.Context.EmailId;
                recordObjectdtls[indocheadrecordType.Attributes["LAST_DATETIME"]] = DateTime.UtcNow.Date;
                recordObjectdtls[indocheadrecordType.Attributes["FREIGHT_TYPE"]] = detail.FreightType;
                recordObjectdtls[indocheadrecordType.Attributes["FREIGHT_TYPE_DESC"]] = detail.FreightType;
                recordObjectdtls[indocheadrecordType.Attributes["REF_DOC"]] = detail.RefDoc;
                recordObjectdtls[indocheadrecordType.Attributes["REF_AUTH_NO"]] = detail.RefAuthNo;
                recordObjectdtls[indocheadrecordType.Attributes["COST_PRE_MATCH"]] = detail.CostFreMatch;
                recordObjectdtls[indocheadrecordType.Attributes["DETAIL_MATCHED"]] = detail.DetailMatched;
                recordObjectdtls[indocheadrecordType.Attributes["BEST_TERMS"]] = detail.BestTerms;
                recordObjectdtls[indocheadrecordType.Attributes["BEST_TERMS_SOURCE"]] = detail.BestTermsSource;
                recordObjectdtls[indocheadrecordType.Attributes["BEST_TERMS_DATE"]] = detail.BestTermsDate;
                recordObjectdtls[indocheadrecordType.Attributes["BEST_TERMS_DATE_SOURCE"]] = detail.BestTermsDateSource;
                recordObjectdtls[indocheadrecordType.Attributes["VARIANCE_WITHIN_TOLERANCE"]] = detail.VarianceWithinTolerance;
                recordObjectdtls[indocheadrecordType.Attributes["RESOLUTION_ADJUSTED_TOTAL_COST"]] = detail.ResolutionAdjustedTotalCost;
                recordObjectdtls[indocheadrecordType.Attributes["RESOLUTION_ADJUSTED_TOTAL_QTY"]] = detail.ResolutionAdjustedTotalQty;
                recordObjectdtls[indocheadrecordType.Attributes["CONSIGNMENT_IND"]] = detail.ConsignmentInd;
                recordObjectdtls[indocheadrecordType.Attributes["DEAL_ID"]] = detail.DealId;
                recordObjectdtls[indocheadrecordType.Attributes["RTV_IND"]] = detail.RtvInd;
                recordObjectdtls[indocheadrecordType.Attributes["DISCOUNT_DATE"]] = detail.DiscountDate;
                recordObjectdtls[indocheadrecordType.Attributes["DEAL_TYPE"]] = detail.DealType;
                recordObjectdtls[indocheadrecordType.Attributes["TOTAL_COST_INC_VAT"]] = detail.TotalCostIncVat;
                recordObjectdtls[indocheadrecordType.Attributes["VAT_DISC_CREATE_DATE"]] = detail.VatDiscCreateDate;
                recordObjectdtls[indocheadrecordType.Attributes["OPERATION"]] = detail.Operation;
                pInvoiceMatchingDocTab.Add(recordObjectdtls);
            }

            headRecordObject["doc_head_tab"] = pInvoiceMatchingDocTab;
            return headRecordObject;
        }

        private OracleTable SetParamsImDocCostDisc(ImCostDiscrepancySearchModel dataModel)
        {
            this.Connection.Open();
            OracleType indocheadTableType = OracleType.GetObjectType(InvoiceMatchingConstants.ObjTypeIMDocCostDiscTAB, this.Connection);
            OracleType indocheadrecordType = OracleType.GetObjectType(InvoiceMatchingConstants.ObjTypeIMDocCostDiscREC, this.Connection);
            OracleTable pInvoiceMatchingDocTab = new OracleTable(indocheadTableType);

            foreach (ImCostReviewDetailModel model in dataModel.ImCostReviewDetails)
            {
                OracleObject recordObjectdtls = new OracleObject(indocheadrecordType);
                recordObjectdtls[indocheadrecordType.Attributes["DOC_ID"]] = model.DocId;
                recordObjectdtls[indocheadrecordType.Attributes["ITEM"]] = model.Item;
                recordObjectdtls[indocheadrecordType.Attributes["RES_COST"]] = model.ResCost;
                recordObjectdtls[indocheadrecordType.Attributes["REASON_CODE"]] = model.ReasonCode;
                recordObjectdtls[indocheadrecordType.Attributes["ACTION"]] = model.Action;
                recordObjectdtls[indocheadrecordType.Attributes["OPERATION"]] = model.Operation;
                pInvoiceMatchingDocTab.Add(recordObjectdtls);
            }

            return pInvoiceMatchingDocTab;
        }

        private OracleTable SetParamsImQtyDiscrepancy(ImQtyDiscrepancySearchModel dataModel)
        {
            this.Connection.Open();
            OracleType indocheadTableType = OracleType.GetObjectType(InvoiceMatchingConstants.ObjTypeImQtyDiscTAB, this.Connection);
            OracleType indocheadrecordType = OracleType.GetObjectType(InvoiceMatchingConstants.ObjTypeImQtyDiscREC, this.Connection);
            OracleTable pInvoiceMatchingDocTab = new OracleTable(indocheadTableType);

            foreach (ImQtyDiscrepancyDetailsModel model in dataModel.ImQtyDiscrepancyDetails)
            {
                OracleObject recordObjectdtls = new OracleObject(indocheadrecordType);
                recordObjectdtls[indocheadrecordType.Attributes["DOC_ID"]] = model.DocId;
                recordObjectdtls[indocheadrecordType.Attributes["ITEM"]] = model.Item;
                recordObjectdtls[indocheadrecordType.Attributes["REASON_CODE"]] = model.ReasonCode;
                recordObjectdtls[indocheadrecordType.Attributes["ACTION_CODE"]] = model.ActionCode;
                recordObjectdtls[indocheadrecordType.Attributes["RES_QTY"]] = model.ResQty;
                recordObjectdtls[indocheadrecordType.Attributes["OPERATION"]] = model.Operation;
                pInvoiceMatchingDocTab.Add(recordObjectdtls);
            }

            return pInvoiceMatchingDocTab;
        }
    }
}
