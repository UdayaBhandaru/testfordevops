﻿// <copyright file="DocumentModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// CostChangeReason
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Document.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class DocumentModel : ProfileEntity
    {
        public int? DocId { get; set; }

        public string DocDesc { get; set; }

        public string DocType { get; set; }

        public string DocTypeDesc { get; set; }

        public string LcInd { get; set; }

        public string SwiftTag { get; set; }

        public int? SeqNo { get; set; }

        public string Text { get; set; }

        public string ProgramMessage { get; set; }

        public string Error { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }
    }
}
