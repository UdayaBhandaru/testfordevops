﻿// <copyright file="DocumentRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// CostZoneRepository
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Document.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Document.Models;
    using AutoMapper;
    using Devart.Data.Oracle;
    using Microsoft.AspNetCore.Authorization;

    public class DocumentRepository : BaseOraclePackage
    {
        public DocumentRepository()
        {
            this.PackageName = MdmConstants.GetDocumentPackage;
        }

        public async Task<List<DocumentModel>> GetDocument(DocumentModel documentModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDocument, MdmConstants.GetProcDocument);
            OracleObject recordObject = this.SetParamsDocument(documentModel, true);
            return await this.GetProcedure2<DocumentModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ServiceDocumentResult> SetDocument(DocumentModel documentModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDocument, MdmConstants.SetProcDocument);
            OracleObject recordObject = this.SetParamsDocument(documentModel, false);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        private OracleObject SetParamsDocument(DocumentModel documentModel, bool isSearch)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(MdmConstants.RecordTypeDocument, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            if (!isSearch)
            {
                recordObject[recordType.Attributes["Operation"]] = documentModel.Operation;
            }

            recordObject[recordType.Attributes["DOC_ID"]] = documentModel.DocId;
            recordObject[recordType.Attributes["DOC_DESC"]] = documentModel.DocDesc;
            recordObject[recordType.Attributes["DOC_TYPE"]] = documentModel.DocType;
            recordObject[recordType.Attributes["DOC_TYPE_DESC"]] = documentModel.DocTypeDesc;
            recordObject[recordType.Attributes["LC_IND"]] = documentModel.LcInd;
            recordObject[recordType.Attributes["SWIFT_TAG"]] = documentModel.SwiftTag;
            recordObject[recordType.Attributes["SEQ_NO"]] = documentModel.SeqNo;
            recordObject[recordType.Attributes["TEXT"]] = documentModel.Text;
            return recordObject;
        }
    }
}