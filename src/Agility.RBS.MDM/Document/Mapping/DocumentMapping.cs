﻿// <copyright file="DocumentMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Document.Mapping
{
    using System;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.Document.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class DocumentMapping : Profile
    {
        public DocumentMapping()
            : base("DocumentMapping")
        {
            this.CreateMap<OracleObject, DocumentModel>()
              .ForMember(m => m.DocId, opt => opt.MapFrom(r => r["DOC_ID"]))
              .ForMember(m => m.DocDesc, opt => opt.MapFrom(r => r["DOC_DESC"]))
              .ForMember(m => m.DocType, opt => opt.MapFrom(r => r["DOC_TYPE"]))
              .ForMember(m => m.DocTypeDesc, opt => opt.MapFrom(r => r["DOC_TYPE_DESC"]))
              .ForMember(m => m.LcInd, opt => opt.MapFrom(r => r["LC_IND"]))
              .ForMember(m => m.SwiftTag, opt => opt.MapFrom(r => r["SWIFT_TAG"]))
              .ForMember(m => m.SeqNo, opt => opt.MapFrom(r => r["SEQ_NO"]))
              .ForMember(m => m.Text, opt => opt.MapFrom(r => r["TEXT"]))
              .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
              .ForMember(m => m.TableName, opt => opt.MapFrom(r => "DOC"))
              .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));
        }
    }
}
