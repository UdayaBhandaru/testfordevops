﻿//-------------------------------------------------------------------------------------------------
// <copyright file="DocumentController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// DocumentController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Document
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Document.Models;
    using Agility.RBS.MDM.Document.Repositories;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    [Route("api/[controller]/[action]")]

    public class DocumentController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocument<DocumentModel> serviceDocument;
        private readonly DocumentRepository documentRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;

        public DocumentController(
            ServiceDocument<DocumentModel> serviceDocument,
           DocumentRepository documentRepository,
            DomainDataRepository domainDataRepository)
        {
            this.documentRepository = documentRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<DocumentModel>> List()
        {
            this.serviceDocument.DomainData.Add("DocType", this.domainDataRepository.DomainDetailData("DOCT").Result);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<DocumentModel>> New()
        {
            this.serviceDocument.DomainData.Add("DocType", this.domainDataRepository.DomainDetailData("DOCT").Result);
            this.serviceDocument.DomainData.Add("indicatorStatus", this.domainDataRepository.DomainDetailData(MdmConstants.VatRegionTypeDomainData).Result);
            this.serviceDocument.LocalizationData.AddData("Document");
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<DocumentModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.DocumentSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<DocumentModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.DocumentSave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingDocument(int docId)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "DOC";
            nvc["P_COL1"] = docId.ToString();
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        private async Task<List<DocumentModel>> DocumentSearch()
        {
            try
            {
                var lstUomClass = await this.documentRepository.GetDocument(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstUomClass;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> DocumentSave()
        {
            this.serviceDocument.Result = await this.documentRepository.SetDocument(this.serviceDocument.DataProfile.DataModel);
            return true;
        }
    }
}
