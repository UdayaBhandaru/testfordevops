﻿// <copyright file="StockCountMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.StockCount.Mapping
{
    using System;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.Brand.Models;
    using Agility.RBS.MDM.CostZone.Models;
    using Agility.RBS.MDM.StockCount.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class StockCountMapping : Profile
    {
        public StockCountMapping()
            : base("StockCountMapping")
        {
        }
    }
}
