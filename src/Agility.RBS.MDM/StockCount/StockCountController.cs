﻿//-------------------------------------------------------------------------------------------------
// <copyright file="StockCountController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// BrandController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.StockCount
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.StockCount.Models;
    using Agility.RBS.MDM.StockCount.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    public class StockCountController : Controller
    {
        private readonly ServiceDocument<StockCountModel> serviceDocument;
        private readonly StockCountRepository partnerRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly DomainDataRepository domainDataRepository;

        public StockCountController(
            ServiceDocument<StockCountModel> serviceDocument,
           StockCountRepository partnerRepository,
             DomainDataRepository domainDataRepository)
        {
            this.partnerRepository = partnerRepository;
            this.serviceDocument = serviceDocument;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<StockCountModel>> List()
        {
            this.serviceDocument.DomainData.Add("actionTypeData", this.domainDataRepository.DomainDetailData("ACTY").Result);
            this.serviceDocument.DomainData.Add("stockCountTypeData", this.domainDataRepository.DomainDetailData("SCTY").Result);
            this.serviceDocument.DomainData.Add("reportGroupData", this.domainDataRepository.DomainDetailData("ACTY").Result);
            this.serviceDocument.DomainData.Add("dptData", this.domainDataRepository.DomainDetailData("ACTY").Result);
            this.serviceDocument.DomainData.Add("categoryData", this.domainDataRepository.DomainDetailData("ACTY").Result);
            this.serviceDocument.DomainData.Add("fineLineData", this.domainDataRepository.DomainDetailData("ACTY").Result);
            this.serviceDocument.DomainData.Add("segmentData", this.domainDataRepository.DomainDetailData("ACTY").Result);
            this.serviceDocument.DomainData.Add("itemData", this.domainDataRepository.DomainDetailData("ACTY").Result);
            this.serviceDocument.DomainData.Add("chainData", this.domainDataRepository.DomainDetailData("ACTY").Result);
            this.serviceDocument.DomainData.Add("countryData", this.domainDataRepository.DomainDetailData("ACTY").Result);
            this.serviceDocument.DomainData.Add("formatData", this.domainDataRepository.DomainDetailData("ACTY").Result);
            this.serviceDocument.DomainData.Add("zoneData", this.domainDataRepository.DomainDetailData("ACTY").Result);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<StockCountModel>> New()
        {
            this.serviceDocument.DomainData.Add("indicator", this.domainDataRepository.DomainDetailData("YSNO").Result);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<StockCountModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.PartnerSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<StockCountModel>> Open(string id)
        {
            this.serviceDocument.DomainData.Add("indicator", this.domainDataRepository.DomainDetailData("YSNO").Result);
            return await this.serviceDocument.OpenAsync(this.EmpOpen);
        }

        public async Task<ServiceDocument<StockCountModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.PartnerSave);
            return this.serviceDocument;
        }

        private async Task<List<StockCountModel>> PartnerSearch()
        {
            try
            {
                var lstUomClass = await this.partnerRepository.GetPartner(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstUomClass;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<StockCountModel> EmpOpen()
        {
            try
            {
                var lstUomClass = await this.partnerRepository.GetPartner(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstUomClass?[0];
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> PartnerSave()
        {
            this.serviceDocument.Result = await this.partnerRepository.SetPartner(this.serviceDocument.DataProfile.DataModel);
            return true;
        }
    }
}
