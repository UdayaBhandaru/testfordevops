﻿// <copyright file="StockCountRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// BrandRepository
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.StockCount.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Employee.Models;
    using Agility.RBS.MDM.StockCount.Models;
    using AutoMapper;
    using Devart.Data.Oracle;
    using Microsoft.AspNetCore.Authorization;

    public class StockCountRepository : BaseOraclePackage
    {
        public StockCountRepository()
        {
            this.PackageName = "UTL_SALES_AUDIT";
        }

        public async Task<List<StockCountModel>> GetPartner(StockCountModel partnerModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams("sa_emp_tab", "getemployee");
            OracleObject recordObject = this.SetParamsPartner(partnerModel);
            return await this.GetProcedure2<StockCountModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ServiceDocumentResult> SetPartner(StockCountModel partnerModel)
        {
            PackageParams packageParameter = this.GetPackageParams("sa_emp_tab", "setemployee");
            OracleObject recordObject = this.SetParamsPartner(partnerModel);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public virtual void SetParamsPartnerSearch(StockCountModel partnerModel, OracleObject recordObject)
        {
           //// recordObject["emp_id"] = partnerModel.EmpId;
           //// recordObject["emp_type"] = partnerModel.EmpType;
           //////// recordObject["emp_type_des"] = partnerModel.EmpTypeDesc;
           //// recordObject["cashier_ind"] = partnerModel.CashierInd;
           //// recordObject["NAME"] = partnerModel.Name;
           //// recordObject["user_id"] = partnerModel.UserId;
           //// recordObject["email"] = partnerModel.Email;
           //// recordObject["salesperson_ind"] = partnerModel.SalespersonInd;
           //// recordObject["phone"] = partnerModel.Phone;
        }

        private OracleObject SetParamsPartner(StockCountModel partnerModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("sa_emp_rec");
            OracleObject recordObject = this.GetOracleObject(recordType);

            this.SetParamsPartnerSearch(partnerModel, recordObject);
            return recordObject;
        }
    }
}