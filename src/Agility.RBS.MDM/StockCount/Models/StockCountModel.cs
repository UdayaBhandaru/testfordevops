﻿// <copyright file="StockCountModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// BrandModel
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.StockCount.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class StockCountModel : ProfileEntity
    {
        public string Action { get; set; }

        public string Type { get; set; }

        public string Stake { get; set; }

        public string CycleCountDesc { get; set; }

        public string MinDate { get; set; }

        public string TableName { get; set; }
    }
}
