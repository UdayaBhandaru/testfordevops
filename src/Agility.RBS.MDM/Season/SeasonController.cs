﻿// <copyright file="SeasonController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Season
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Season;
    using Agility.RBS.MDM.Season.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class SeasonController : Controller
    {
        private readonly ServiceDocument<SeasonModel> serviceDocument;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly SeasonRepository seasonRepository;
        private readonly DomainDataRepository domainDataRepository;

        public SeasonController(ServiceDocument<SeasonModel> serviceDocument, SeasonRepository seasonRepository, DomainDataRepository domainDataRepository)
        {
            this.seasonRepository = seasonRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<SeasonModel>> List()
        {
            this.serviceDocument.DomainData.Add("company", this.domainDataRepository.CompanyHeadDomainGet().Result);
            this.serviceDocument.LocalizationData.AddData("Season");
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<SeasonModel>> New()
        {
            this.serviceDocument.DomainData.Add("company", this.domainDataRepository.CompanyHeadDomainGet().Result);
            this.serviceDocument.LocalizationData.AddData("Season");
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<SeasonModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.SeasonSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<SeasonModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.SeasonSave);
            return this.serviceDocument;
        }

        [AllowAnonymous]

        public async ValueTask<bool> IsExistingSeason(string seasonName)
        {
            return await this.seasonRepository.IsExistingSeason(seasonName);
        }

        private async Task<List<SeasonModel>> SeasonSearch()
        {
            try
            {
                var seasons = await this.seasonRepository.GetSeason(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return seasons;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> SeasonSave()
        {
            this.serviceDocument.Result = await this.seasonRepository.SetSeason(this.serviceDocument.DataProfile.DataModel);
            return true;
        }
    }
}