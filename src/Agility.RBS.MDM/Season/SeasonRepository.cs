﻿// <copyright file="SeasonRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Season
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Season.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class SeasonRepository : BaseOraclePackage
    {
        public SeasonRepository()
        {
            this.PackageName = MdmConstants.GetSeasonPackage;
        }

        public async Task<List<SeasonModel>> GetSeason(SeasonModel seasonModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeSeason, MdmConstants.GetProcSeason);
            OracleObject recordObject = this.SetParamsSeason(seasonModel, true);
            OracleTable pGetUsersMstTab = await this.GetProcedure(recordObject, packageParameter, serviceDocumentResult);
            var lstHeader = Mapper.Map<List<SeasonModel>>(pGetUsersMstTab);
            if (lstHeader != null)
            {
                for (int i = 0; i < lstHeader.Count; i++)
                {
                    OracleObject obj = (OracleObject)pGetUsersMstTab[i];
                    OracleTable seasonDetails = (Devart.Data.Oracle.OracleTable)obj["phase"];
                    lstHeader[i].PhaseDetailList.AddRange(Mapper.Map<List<PhaseDetailModel>>(seasonDetails));
                }
            }

            return lstHeader;
        }

        public async Task<ServiceDocumentResult> SetSeason(SeasonModel seasonModel)
        {
            this.PackageName = MdmConstants.GetSeasonPackage;
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeSeason, MdmConstants.SetProcSeason);
            OracleObject recordObject = this.SetParamsSeason(seasonModel, false);
            ServiceDocumentResult sRes = await this.SetProcedure(recordObject, packageParameter);
            return sRes;
        }

        public async Task<List<PhaseModel>> GetPhase(PhaseModel phaseModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypePhase, MdmConstants.GetProcPhase);
            OracleObject recordObject = this.SetParamsPhase(phaseModel, true);
            return await this.GetProcedure2<PhaseModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ServiceDocumentResult> SetPhase(PhaseModel phaseModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypePhase, MdmConstants.SetProcPhase);
            OracleObject recordObject = this.SetParamsPhase(phaseModel, false);
            ServiceDocumentResult sRes = await this.SetProcedure(recordObject, packageParameter);
            return sRes;
        }

        public async ValueTask<bool> IsExistingPhase(int seasonId, string phaseName)
        {
            int result;
            string strCmd = "Select count(1) from PHASES where SEASON_ID = '" + seasonId + "'" + " and PHASE_DESC = '" + phaseName + "'";
            OracleCommand myCommand = new OracleCommand(strCmd);
            myCommand.Connection = this.Connection;
            this.Connection.Open();
            result = Convert.ToInt32(await myCommand.ExecuteScalarAsync());
            this.Connection.Close();
            return result > 0;
        }

        public async ValueTask<bool> IsExistingSeason(string seasonName)
        {
            int result;
            string strCmd = "Select count(1) from SEASONS where SEASON_DESC = '" + seasonName + "'";
            OracleCommand myCommand = new OracleCommand(strCmd);
            myCommand.Connection = this.Connection;
            this.Connection.Open();
            result = Convert.ToInt32(await myCommand.ExecuteScalarAsync());
            this.Connection.Close();
            return result > 0;
        }

        private OracleObject SetParamsSeason(SeasonModel seasonModel, bool isSerach)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(MdmConstants.RecTypeSeason, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            return this.SetGetSeasonParamas(seasonModel, recordObject, isSerach);
        }

        private OracleObject SetParamsPhase(PhaseModel phaseModel, bool isSearch)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType("PHASE_REC", this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            if (!isSearch)
            {
                recordObject[recordType.Attributes["START_DATE"]] = phaseModel.StartDate;
                recordObject[recordType.Attributes["END_DATE"]] = phaseModel.EndDate;
                recordObject[recordType.Attributes["Operation"]] = phaseModel.Operation;
            }

            recordObject[recordType.Attributes["PHASE_ID"]] = phaseModel.PhaseId;
            recordObject[recordType.Attributes["SEASON_ID"]] = phaseModel.SeasonId;
            recordObject[recordType.Attributes["PHASE_DESC"]] = phaseModel.PhaseName;
            return recordObject;
        }

        private OracleObject SetGetSeasonParamas(SeasonModel seasonModel, OracleObject recordObject, bool isSerach)
        {
            if (!isSerach)
            {
                OracleTable pPhaseDetailsTab = this.PhaseDetailsParamsObject(seasonModel);
                recordObject["phase"] = pPhaseDetailsTab;
                recordObject["Operation"] = seasonModel.Operation;
                recordObject["filter_org_id"] = seasonModel.CompanyId;
                recordObject["filter_org_name"] = seasonModel.CompanyName;
            }

            recordObject["season_id"] = seasonModel.SeasonId;
            recordObject["season_desc"] = seasonModel.SeasonName;
            recordObject["start_date"] = seasonModel.StartDate;
            recordObject["end_date"] = seasonModel.EndDate;
            return recordObject;
        }

        private OracleTable PhaseDetailsParamsObject(SeasonModel seasonModel)
        {
            OracleType dtlTableType = OracleType.GetObjectType(MdmConstants.ObjTypePhase, this.Connection);
            OracleType dtlRecordType = OracleType.GetObjectType(MdmConstants.RecTypePhase, this.Connection);
            OracleTable pLocationTab = new OracleTable(dtlTableType);
            foreach (PhaseDetailModel phaseDetailModel in seasonModel.PhaseDetailList)
            {
                var dtlRecordObject = new OracleObject(dtlRecordType);
                dtlRecordObject[dtlRecordType.Attributes["season_id"]] = seasonModel.SeasonId;
                dtlRecordObject[dtlRecordType.Attributes["season_desc"]] = seasonModel.SeasonName;
                dtlRecordObject[dtlRecordType.Attributes["phase_id"]] = phaseDetailModel?.PhaseId;
                dtlRecordObject[dtlRecordType.Attributes["phase_desc"]] = phaseDetailModel?.PhaseName;
                dtlRecordObject[dtlRecordType.Attributes["start_date"]] = phaseDetailModel?.StartDate;
                dtlRecordObject[dtlRecordType.Attributes["end_date"]] = phaseDetailModel?.EndDate;
                dtlRecordObject[dtlRecordType.Attributes["Operation"]] = phaseDetailModel?.Operation;
                pLocationTab.Add(dtlRecordObject);
            }

            return pLocationTab;
        }
    }
}