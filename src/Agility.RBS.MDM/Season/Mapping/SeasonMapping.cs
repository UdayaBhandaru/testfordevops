﻿// <copyright file="SeasonMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Season.Mapping
{
    using System;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.Season.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class SeasonMapping : Profile
    {
        public SeasonMapping()
            : base("SeasonMapping")
        {
            this.CreateMap<OracleObject, SeasonModel>()
                 .ForMember(m => m.SeasonId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["SEASON_ID"])))
                .ForMember(m => m.SeasonName, opt => opt.MapFrom(r => r["season_desc"]))
                .ForMember(m => m.StartDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["START_DATE"])))
                .ForMember(m => m.EndDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["END_DATE"])))
                .ForMember(m => m.CompanyId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["filter_org_id"])))
                .ForMember(m => m.CompanyName, opt => opt.MapFrom(r => r["filter_org_name"]))
                .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "SEASONS"))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, PhaseModel>()
                .ForMember(m => m.SeasonId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["SEASON_ID"])))
                .ForMember(m => m.SeasonName, opt => opt.MapFrom(r => r["season_desc"]))
                .ForMember(m => m.PhaseId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["PHASE_ID"])))
                .ForMember(m => m.PhaseName, opt => opt.MapFrom(r => r["phase_desc"]))
                .ForMember(m => m.StartDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["START_DATE"])))
                .ForMember(m => m.EndDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["END_DATE"])))
                .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "PHASES"))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, PhaseDetailModel>()
               .ForMember(m => m.SeasonId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["SEASON_ID"])))
               .ForMember(m => m.SeasonName, opt => opt.MapFrom(r => r["season_desc"]))
               .ForMember(m => m.PhaseId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["PHASE_ID"])))
               .ForMember(m => m.PhaseName, opt => opt.MapFrom(r => r["phase_desc"]))
               .ForMember(m => m.StartDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["START_DATE"])))
               .ForMember(m => m.EndDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["END_DATE"])))
               .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
               .ForMember(m => m.TableName, opt => opt.MapFrom(r => "PHASES"))
               .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));
        }
    }
}
