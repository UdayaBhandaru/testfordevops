﻿// <copyright file="PhaseController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Season
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Season.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class PhaseController : Controller
    {
        private readonly ServiceDocument<PhaseModel> serviceDocument;
        private readonly SeasonRepository seasonRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly DomainDataRepository domainDataRepository;

        public PhaseController(
            ServiceDocument<PhaseModel> serviceDocument,
            SeasonRepository seasonRepository,
            DomainDataRepository domainDataRepository)
        {
            this.seasonRepository = seasonRepository;
            this.serviceDocument = serviceDocument;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        [HttpPost]
        public async Task<ServiceDocument<PhaseModel>> List()
        {
            return await this.PhaseDomainData();
        }

        [HttpPost]
        public async Task<ServiceDocument<PhaseModel>> New()
        {
            return await this.PhaseDomainData();
        }

        [HttpPost]
        public async Task<ServiceDocument<PhaseModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.PhaseSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<PhaseModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.PhaseSave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingPhase(int seasonId, string phaseName)
        {
            return await this.seasonRepository.IsExistingPhase(seasonId, phaseName);
        }

        private async Task<List<PhaseModel>> PhaseSearch()
        {
            try
            {
                var phases = await this.seasonRepository.GetPhase(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return phases;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> PhaseSave()
        {
            this.serviceDocument.Result = await this.seasonRepository.SetPhase(this.serviceDocument.DataProfile.DataModel);
            return true;
        }

        private async Task<ServiceDocument<PhaseModel>> PhaseDomainData()
        {
            this.serviceDocument.DomainData.Add("company", this.domainDataRepository.CompanyHeadDomainGet().Result);
            this.serviceDocument.DomainData.Add("season", this.domainDataRepository.SeasonsGet().Result);
            this.serviceDocument.LocalizationData.AddData("Phase");
            return this.serviceDocument;
        }
    }
}
