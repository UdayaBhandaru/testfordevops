﻿//-------------------------------------------------------------------------------------------------
// <copyright file="SeasonModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Season.Models
{
    using System;
    using System.Collections.Generic;
    using Agility.Framework.Core.Profile.Entities;

    public class SeasonModel : ProfileEntity
    {
        public SeasonModel()
        {
            this.PhaseDetailList = new List<PhaseDetailModel>();
        }

        public int? CompanyId { get; set; }

        public string CompanyName { get; set; }

        public int? SeasonId { get; set; }

        public string SeasonName { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int? LangId { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Error { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }

        public List<PhaseDetailModel> PhaseDetailList { get; private set; }
    }
}
