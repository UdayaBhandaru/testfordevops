﻿// <copyright file="PhaseModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Season.Models
{
    using System;
    using Agility.Framework.Core.Profile.Entities;

    public class PhaseModel : ProfileEntity
    {
        public int? CompanyId { get; set; }

        public string CompanyName { get; set; }

        public int? SeasonId { get; set; }

        public string SeasonName { get; set; }

        public int? PhaseId { get; set; }

        public string PhaseName { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }

        public string Error { get; set; }
    }
}
