// <copyright file="SecurityGroupOrgHierarchyModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
namespace Agility.RBS.MDM.Administration.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class SecurityGroupOrgHierarchyModel : ProfileEntity
    {
        public long? GroupId { get; set; }

        public string GroupName { get; set; }

        public string OrgLevel { get; set; }

        public string OrgLevelValue { get; set; }

        public string OrgLevelDesc { get; set; }

        public string OrgLevelValueDesc { get; set; }

        public double? ProgramPhase { get; set; }

        public double? ProgramMessage { get; set; }

        public string PError { get; set; }

        public string Operation { get; set; }
    }
}