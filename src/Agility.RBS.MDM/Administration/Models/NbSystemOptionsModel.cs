// <copyright file="NbSystemOptionsModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
namespace Agility.RBS.MDM.Administration.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class NbSystemOptionsModel : ProfileEntity
    {
        public string TransformableCustInd { get; set; }

        public string CheckPaytermEnabledIndn { get; set; }

        public string DsdLinesPerPo { get; set; }

        public string Operation { get; set; }
    }
}