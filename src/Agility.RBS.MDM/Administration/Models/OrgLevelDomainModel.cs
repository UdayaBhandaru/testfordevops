﻿// <copyright file="OrgLevelDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Administration.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using Agility.RBS.Core.Models;
    using Agility.RBS.MDM.Repositories.Models;
    using Agility.RBS.MDM.UtilityDomain.Models;

    public class OrgLevelDomainModel
    {
        public OrgLevelDomainModel()
        {
            this.OrgLevels = new List<DomainDetailModel>();
            this.Chains = new List<CommonModel>();
            this.Areas = new List<CommonModel>();
            this.Regions = new List<CommonModel>();
            this.Districts = new List<CommonModel>();
            this.SecurityGroups = new List<SecurityGroupDomainModel>();
        }

        public List<DomainDetailModel> OrgLevels { get; private set; }

        public List<CommonModel> Chains { get; private set; }

        public List<CommonModel> Areas { get; private set; }

        public List<CommonModel> Regions { get; private set; }

        public List<CommonModel> Districts { get; private set; }

        public List<SecurityGroupDomainModel> SecurityGroups { get; private set; }
    }
}
