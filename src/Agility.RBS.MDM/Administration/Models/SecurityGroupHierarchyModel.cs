// <copyright file="SecurityGroupHierarchyModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
namespace Agility.RBS.MDM.Administration.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class SecurityGroupHierarchyModel : ProfileEntity
    {
        public long? GroupId { get; set; }

        public string GroupName { get; set; }

        public string MerchLevel { get; set; }

        public string MerchLevelValue { get; set; }

        public string MerchLevelDesc { get; set; }

        public string MerchLevelValueDesc { get; set; }

        public double? ProgramPhase { get; set; }

        public double? ProgramMessage { get; set; }

        public string PError { get; set; }

        public string Operation { get; set; }
    }
}