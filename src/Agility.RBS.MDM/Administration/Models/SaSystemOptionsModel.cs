// <copyright file="SaSystemOptionsModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
namespace Agility.RBS.MDM.Administration.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class SaSystemOptionsModel : ProfileEntity
    {
        public int? DaysBeforePurge { get; set; }

        public int? DayPostSale { get; set; }

        public string BalanceLevelInd { get; set; }

        public int? MaxDaysCompareDups { get; set; }

        public string CompBaseDate { get; set; }

        public int? CompNoDays { get; set; }

        public string CheckDupMissTran { get; set; }

        public string UnitOfWork { get; set; }

        public string AuditAfterImpInd { get; set; }

        public int? FuelDept { get; set; }

        public int? DefaultChain { get; set; }

        public string CloseInOrder { get; set; }

        public string EscheatInd { get; set; }

        public string PartnerType { get; set; }

        public string PartnerId { get; set; }

        public string AutoValidateTranEmployeeId { get; set; }

        public string ViewSysCalcTotal { get; set; }

        public string CcValReqd { get; set; }

        public string WkstationTranAppendInd { get; set; }

        public string CcSecLvlInd { get; set; }

        public string UpdateId { get; set; }

        public string Operation { get; set; }
    }
}