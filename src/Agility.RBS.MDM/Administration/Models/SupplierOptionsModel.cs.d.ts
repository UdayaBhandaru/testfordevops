declare module server {
	interface supplierOptionsModel extends profileEntity {
		supplier?: number;
		supplierName: string;
		sendDebitMemo: string;
		manuallyPaidIndicator: string;
		useInvoceTermsIndicator: string;
		rogDateAllowedIndicator: string;
		apReviewer: string;
		closeOpenShipmentDays?: number;
		matchRecieptsOthersuppLiersIndicator: string;
		qtyDiscDayBeforeRte?: number;
		operation: string;
	}
}
