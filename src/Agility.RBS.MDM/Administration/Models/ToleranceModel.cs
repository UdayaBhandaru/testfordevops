// <copyright file="ToleranceModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
namespace Agility.RBS.MDM.Administration.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class ToleranceModel : ProfileEntity
    {
        public long? ToleranceKey { get; set; }

        public string CostQuantityIndicator { get; set; }

        public string SummaryLineIndicator { get; set; }

        public string ToleranceDocumentType { get; set; }

        public double? LowerLimitInclusive { get; set; }

        public double? UpperLimitExclusive { get; set; }

        public string FavorOf { get; set; }

        public string ToleranceValueType { get; set; }

        public double? ToleranceValue { get; set; }

        public string UpdateId { get; set; }

        public string Operation { get; set; }
    }
}