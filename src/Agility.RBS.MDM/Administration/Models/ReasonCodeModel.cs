// <copyright file="ReasonCodeModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
namespace Agility.RBS.MDM.Administration.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class ReasonCodeModel : ProfileEntity
    {
        public string ReasonCodeType { get; set; }

        public string ReasonCodeTypeDescription { get; set; }

        public string ReasonCodeId { get; set; }

        public string ReasonCodeDescription { get; set; }

        public string Action { get; set; }

        public string ActionDescription { get; set; }

        public string CommentRequiredIndicator { get; set; }

        public string HintComment { get; set; }

        public string DeleteIndicator { get; set; }

        public string Operation { get; set; }
    }
}
