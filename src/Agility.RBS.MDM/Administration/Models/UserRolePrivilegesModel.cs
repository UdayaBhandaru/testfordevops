// <copyright file="UserRolePrivilegesModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
namespace Agility.RBS.MDM.Administration.Models
{
    using System.ComponentModel.DataAnnotations;
    using Agility.Framework.Core.Profile.Entities;

    public class UserRolePrivilegesModel : ProfileEntity
    {
        [Key]
        public string Role { get; set; }

        public long? OrdApprAmt { get; set; }

        public string TsfApprInd { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string PError { get; set; }

        public string Operation { get; set; }
    }
}