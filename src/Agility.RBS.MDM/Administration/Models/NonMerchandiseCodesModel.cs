// <copyright file="NonMerchandiseCodesModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
namespace Agility.RBS.MDM.Administration.Models
{
    using System.Collections.Generic;
    using Agility.Framework.Core.Profile.Entities;

    public class NonMerchandiseCodesModel : ProfileEntity
    {
        public NonMerchandiseCodesModel()
        {
            this.Details = new List<NonMerchandiseCodeDetailModel>();
        }

        public string NonMerchandiseCode { get; set; }

        public string NonMerchandiseCodeDesc { get; set; }

        public List<NonMerchandiseCodeDetailModel> Details { get; internal set; }

        public double? ProgramPhase { get; set; }

        public double? ProgramMessage { get; set; }

        public string PError { get; set; }

        public string Operation { get; set; }
    }
}