﻿// <copyright file="MerchLevelDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Administration.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using Agility.RBS.Core.Models;
    using Agility.RBS.MDM.Repositories.Models;
    using Agility.RBS.MDM.UtilityDomain.Models;

    public class MerchLevelDomainModel
    {
        public MerchLevelDomainModel()
        {
            this.MerchLevels = new List<DomainDetailModel>();
            this.Divisions = new List<CommonModel>();
            this.Groups = new List<CommonModel>();
            this.Departments = new List<CommonModel>();
            this.SecurityGroups = new List<SecurityGroupDomainModel>();
        }

        public List<DomainDetailModel> MerchLevels { get; private set; }

        public List<CommonModel> Divisions { get; private set; }

        public List<CommonModel> Groups { get; private set; }

        public List<CommonModel> Departments { get; private set; }

        public List<SecurityGroupDomainModel> SecurityGroups { get; private set; }
    }
}
