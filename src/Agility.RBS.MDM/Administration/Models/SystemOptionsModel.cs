// <copyright file="SystemOptionsModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
namespace Agility.RBS.MDM.Administration.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class SystemOptionsModel : ProfileEntity
    {
        public int? DebitMemoSendDays { get; set; }

        public int? CloseOpenReceiptDays { get; set; }

        public int? CostResolutionDueDays { get; set; }

        public int? QtyResolutionDueDays { get; set; }

        public int? DocHistDays { get; set; }

        public string DebitMemoPrefixCost { get; set; }

        public string DebitMemoPrefixQty { get; set; }

        public string DebitMemoPrefixVat { get; set; }

        public string CreditMemoPrefixCost { get; set; }

        public string CreditMemoPrefixQty { get; set; }

        public string CreditNoteReqPrefixCost { get; set; }

        public string CreditNoteReqPrefixQty { get; set; }

        public string CreditNoteReqPrefixVat { get; set; }

        public int? PostDatedDocDays { get; set; }

        public decimal? MaxTolerancePct { get; set; }

        public int? DaysBeforeDueDate { get; set; }

        public string DefaultPayNowTerms { get; set; }

        public string VatInd { get; set; }

        public decimal? CalcTolerance { get; set; }

        public string VatValidationType { get; set; }

        public string VatDocumentCreationLvl { get; set; }

        public string DefaultVatHeader { get; set; }

        public string VatResolutionDueDays { get; set; }

        public string CalcToleranceIndicator { get; set; }

        public string PostBasedOnDocHeader { get; set; }

        public string Operation { get; set; }
    }
}
