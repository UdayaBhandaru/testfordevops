// <copyright file="SystemVariablesModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
namespace Agility.RBS.MDM.Administration.Models
{
    using System;
    using System.Collections.Generic;
    using Agility.Framework.Core.Profile.Entities;

    public class SystemVariablesModel : ProfileEntity
    {
        public string AddrCatalog { get; set; }

        public string AipInd { get; set; }

        public string AllocMethod { get; set; }

        public string ApplyProfPresStock { get; set; }

        public string AriInd { get; set; }

        public string AutoApproveChildInd { get; set; }

        public int? AutoEan13Prefix { get; set; }
        
        public string BaseCountryId { get; set; }

        public int? BillOfLadingHistoryMths { get; set; }

        public string BillOfLadingInd { get; set; }

        public string BillToLoc { get; set; }

        public string BracketCostingInd { get; set; }

        public string BudShrinkInd { get; set; }

        public string Calendar454Ind { get; set; }

        public int? CdModulus { get; set; }

        public int? CdWeight1 { get; set; }

        public int? CdWeight2 { get; set; }

        public int? CdWeight3 { get; set; }

        public int? CdWeight4 { get; set; }

        public int? CdWeight5 { get; set; }

        public int? CdWeight6 { get; set; }

        public int? CdWeight7 { get; set; }

        public int? CdWeight8 { get; set; }

        public string CheckDigitInd { get; set; }

        public string ClassLevelVatInd { get; set; }

        public string CloseMthWithOpnCntInd { get; set; }

        public int? CloseOpenShipDays { get; set; }

        public string ConsolidationInd { get; set; }

        public string ContractInd { get; set; }

        public string ContractReplenishInd { get; set; }

        public string CostLevel { get; set; }

        public decimal? CostMoney { get; set; }

        public decimal? CostOutStorage { get; set; }

        public string CostOutStorageMeas { get; set; }

        public string CostOutStorageUom { get; set; }

        public decimal? CostWhStorage { get; set; }

        public string CostWhStorageMeas { get; set; }

        public string CostWhStorageUom { get; set; }

        public string CurrencyCode { get; set; }

        public int? CycleCountLagDays { get; set; }

        public int? DailySalesDiscMths { get; set; }

        public string DataLevelSecurityInd { get; set; }

        public string DateEntry { get; set; }

        public int? DbtMemoSendDays { get; set; }

        public string DealAgePriority { get; set; }

        public int? DealHistoryMonths { get; set; }

        public int? DealLeadDays { get; set; }

        public string DealTypePriority { get; set; }

        public string DefaultAllocChrgInd { get; set; }

        public string DefaultCaseName { get; set; }

        public string DefaultDimensionUom { get; set; }

        public string DefaultInnerName { get; set; }

        public string DefaultOrderType { get; set; }

        public string DefaultPackingMethod { get; set; }

        public string DefaultPalletName { get; set; }

        public string DefaultSizeProfile { get; set; }

        public string DefaultStandardUom { get; set; }

        public string DefaultUop { get; set; }

        public int? DefaultVatRegion { get; set; }

        public string DefaultWeightUom { get; set; }

        public string DeptLevelTransfers { get; set; }

        public string DiffGroupMerchLevelCode { get; set; }

        public string DiffGroupOrgLevelCode { get; set; }

        public string DistributionRule { get; set; }

        public string DomainLevel { get; set; }

        public string DummyCartonInd { get; set; }

        public string DuplicateReceivingInd { get; set; }

        public int? EdiCostChgDays { get; set; }

        public string EdiCostOverrideInd { get; set; }

        public int? EdiDailyRptLag { get; set; }

        public int? EdiNewItemDays { get; set; }

        public int? EdiRevDays { get; set; }

        public string ElcInd { get; set; }

        public string ExtInvcMatchInd { get; set; }

        public string FinancialAp { get; set; }

        public string FobTitlePass { get; set; }

        public string FobTitlePassDesc { get; set; }

        public string ForecastInd { get; set; }

        public int? FutureCostHistoryDays { get; set; }

        public string GenConsignmentInvcFreq { get; set; }

        public string GenConInvcItmSupLocInd { get; set; }

        public string GlRollup { get; set; }

        public string GroceryItemsInd { get; set; }

        public int? IbResultsPurgeDays { get; set; }

        public string ImagePath { get; set; }

        public string ImportHtsDate { get; set; }

        public string ImportInd { get; set; }

        public string IncreaseTsfQtyInd { get; set; }

        public string IntercompanyTransferInd { get; set; }

        public int? InterfacePurgeDays { get; set; }

        public string InvHistLevel { get; set; }

        public decimal? InvcDbtMaxPct { get; set; }

        public int? InvcMatchExtrDays { get; set; }

        public string InvcMatchInd { get; set; }

        public string InvcMatchMultSupInd { get; set; }

        public string InvcMatchQtyInd { get; set; }

        public int? LatestShipDays { get; set; }

        public string LcApplicant { get; set; }

        public int? LcExpDays { get; set; }

        public string LcFormType { get; set; }

        public string LcType { get; set; }

        public string Level1Name { get; set; }

        public string Level2Name { get; set; }

        public string Level3Name { get; set; }

        public string LocActivityInd { get; set; }

        public int? LocCloseHistMonths { get; set; }

        public string LocDlvryInd { get; set; }

        public string LocListOrgLevelCode { get; set; }

        public string LocTraitOrgLevelCode { get; set; }

        public int? LookAheadDays { get; set; }

        public decimal? MaxCumMarkonPct { get; set; }

        public int? MaxScalingIterations { get; set; }

        public int? MaxWeeksSupply { get; set; }

        public string MeasurementType { get; set; }

        public string MerchHierAutoGenInd { get; set; }

        public decimal? MinCumMarkonPct { get; set; }

        public string MultiCurrencyInd { get; set; }

        public string MultichannelInd { get; set; }

        public string NomFlag1Label { get; set; }

        public string NomFlag2Label { get; set; }

        public string NomFlag3Label { get; set; }

        public string NomFlag4Label { get; set; }

        public string NomFlag5Label { get; set; }

        public string NwpInd { get; set; }

        public int? NwpRetentionPeriod { get; set; }

        public string OracleFinancialsVers { get; set; }

        public string OrdApprAmtCode { get; set; }

        public int? OrdApprCloseDelay { get; set; }

        public string OrdAutoClosePartRcvdInd { get; set; }

        public string OrdPackCompHeadInd { get; set; }

        public string OrdPackCompInd { get; set; }

        public int? OrdPartRcvdCloseDelay { get; set; }

        public int? OrdWorksheetCleanUpDelay { get; set; }

        public string OtbProdLevelCode { get; set; }

        public string OtbSystemInd { get; set; }

        public string OtbTimeLevelCode { get; set; }

        public string PartnerIdUniqueInd { get; set; }

        public string PlanInd { get; set; }

        public int? PriceHistRetentionDays { get; set; }

        public int? PrimaryLang { get; set; }

        public string RacRtvTsfInd { get; set; }

        public string RcvCostAdjType { get; set; }

        public string RdwInd { get; set; }

        public string ReclassApprOrderInd { get; set; }

        public DateTime? ReclassDate { get; set; }

        public string ReclassSysMaintDateInd { get; set; }

        public int? RedistFactor { get; set; }

        public string RejectStoreOrdInd { get; set; }

        public long? ReplAttrHistRetentionWeeks { get; set; }

        public int? ReplOrderDays { get; set; }

        public int? ReplOrderHistoryDays { get; set; }

        public int? ReplPackHistWks { get; set; }

        public string ReplResultsAllInd { get; set; }

        public int? ReplResultsPurgeDays { get; set; }

        public int? RetnSchedUpdDays { get; set; }

        public string RoundLvl { get; set; }

        public decimal? RoundToCasePct { get; set; }

        public decimal? RoundToInnerPct { get; set; }

        public decimal? RoundToLayerPct { get; set; }

        public decimal? RoundToPalletPct { get; set; }

        public string RpmInd { get; set; }

        public string RpmRibInd { get; set; }

        public string RtmSimplifiedInd { get; set; }

        public int? RtvNadLeadTime { get; set; }

        public string SalesAuditInd { get; set; }

        public string SeasonMerchLevelCode { get; set; }

        public string SeasonOrgLevelCode { get; set; }

        public string SecondaryDescInd { get; set; }

        public string SelfBillInd { get; set; }

        public int? ShipSchedHistoryMths { get; set; }

        public string SingleStylePoInd { get; set; }

        public string SkulistOrgLevelCode { get; set; }

        public string SoftContractInd { get; set; }

        public string SorItemInd { get; set; }

        public string SorMerchHierInd { get; set; }

        public string SorOrgHierInd { get; set; }

        public string SorPurchaseOrderInd { get; set; }

        public string StakeAutoProcessing { get; set; }

        public decimal? StakeCostVariance { get; set; }

        public int? StakeLockoutDays { get; set; }

        public decimal? StakeRetailVariance { get; set; }

        public int? StakeReviewDays { get; set; }

        public decimal? StakeUnitVariance { get; set; }

        public int? StartOfHalfMonth { get; set; }

        public string StdAvInd { get; set; }

        public string StkldgrVatInclRetlInd { get; set; }

        public string StockLedgerLocLevelCode { get; set; }

        public string StockLedgerProdLevelCode { get; set; }

        public string StockLedgerTimeLevelCode { get; set; }

        public string StorageType { get; set; }

        public int? StoreOrdersPurgeDays { get; set; }

        public string StorePackCompRcvInd { get; set; }

        public string SuppPartAutoGen_ind { get; set; }

        public string TableOwner { get; set; }

        public decimal? TargetRoi { get; set; }

        public decimal? TicketOverPct { get; set; }

        public string TicketTypeMerchLevelCode { get; set; }

        public string TicketTypeOrgLevelCode { get; set; }

        public string TimeDisplay { get; set; }

        public string TimeEntry { get; set; }

        public int? TranDataRetainedDaysNo { get; set; }

        public string TsfAutoCloseStore { get; set; }

        public string TsfAutoCloseWh { get; set; }

        public string TsfForceCloseInd { get; set; }

        public string TsfMdStoreToStoreSndRcv { get; set; }

        public string TsfMdStoreToWhSndRcv { get; set; }

        public string TsfMdWhToStoreSndRcv { get; set; }

        public string TsfMdWhToWhSndRcv { get; set; }

        public int? TsfMrtRetentionDays { get; set; }

        public int? TsfHistoryMths { get; set; }

        public string TsfPriceExceedWacInd { get; set; }

        public string UdaMerchLevelCode { get; set; }

        public string UdaOrgLevelCode { get; set; }

        public string UnavailStkordInvAdjInd { get; set; }

        public string UpdateItemHtsInd { get; set; }

        public string UpdateOrdeHtsInd { get; set; }

        public string VatInd { get; set; }

        public string WhCrossLinkInd { get; set; }

        public int? WhStoreAssignHistDays { get; set; }

        public string WhStoreAssignType { get; set; }

        public string WmsCompatibleInd { get; set; }

        public string WrongStReceiptInd { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string PError { get; set; }

        public string Operation { get; set; }
    }
}