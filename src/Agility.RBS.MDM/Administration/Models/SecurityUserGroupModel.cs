// <copyright file="SecurityUserGroupModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
namespace Agility.RBS.MDM.Administration.Models
{
    using System.ComponentModel.DataAnnotations;
    using Agility.Framework.Core.Profile.Entities;

    public class SecurityUserGroupModel : ProfileEntity
    {
        [Key]
        public long? GroupId { get; set; }

        public string GroupName { get; set; }

        public string UserId { get; set; }

        public string UserName { get; set; }

        public double? ProgramPhase { get; set; }

        public double? ProgramMessage { get; set; }

        public string PError { get; set; }

        public string Operation { get; set; }
    }
}