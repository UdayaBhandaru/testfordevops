// <copyright file="SupplierOptionsModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
namespace Agility.RBS.MDM.Administration.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class SupplierOptionsModel : ProfileEntity
    {
        public long? Supplier { get; set; }

        public string SupplierName { get; set; }

        public string SendDebitMemo { get; set; }

        public string ManuallyPaidIndicator { get; set; }

        public string UseInvoceTermsIndicator { get; set; }

        public string RogDateAllowedIndicator { get; set; }

        public string ApReviewer { get; set; }

        public int? CloseOpenShipmentDays { get; set; }

        public string MatchRecieptsOthersuppLiersIndicator { get; set; }

        public int? QtyDiscDayBeforeRte { get; set; }

        public string Operation { get; set; }
    }
}