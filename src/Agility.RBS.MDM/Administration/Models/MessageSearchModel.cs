// <copyright file="MessageSearchModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
namespace Agility.RBS.MDM.Administration.Models
{
    using System.Collections.Generic;
    using Agility.Framework.Core.Profile.Entities;

    public class MessageSearchModel : ProfileEntity
    {
        public string Key { get; set; }

        public string Text { get; set; }

        public string ErrorType { get; set; }

        public string ErrorTypeDesc { get; set; }

        public int? Language { get; set; }

        public string IsApproved { get; set; }

        public string User { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string PError { get; set; }

        public string Operation { get; set; }
    }
}