// <copyright file="RpmSystemOptionsModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
namespace Agility.RBS.MDM.Administration.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class RpmSystemOptionsModel : ProfileEntity
    {
        public long? SystemOptionsId { get; set; }

        public int? ClearanceHistMonths { get; set; }

        public int? ClearancePromoOverlapInd { get; set; }

        public int? ComplexPromoAllowedInd { get; set; }

        public int? CostCalculationMethod { get; set; }

        public int? DefaultOutOfStockDays { get; set; }

        public int? DefaultResetDate { get; set; }

        public int? EventIdRequired { get; set; }

        public int? MultiItemLocPromoInd { get; set; }

        public int? OpenZoneUse { get; set; }

        public int? PriceChangeProcessingDays { get; set; }

        public int? PriceChangePromoOverlapInd { get; set; }

        public int? PromoApplyOrder { get; set; }

        public int? PromotionHistMonths { get; set; }

        public int? RejectHoldDaysPcClear { get; set; }

        public int? RejectHoldDaysPromo { get; set; }

        public int? RecognizeWhAsLocations { get; set; }

        public int? SalesCalculationMethod { get; set; }

        public int? UpdateItemAttributes { get; set; }

        public int? ZeroCurrEndsInDecs { get; set; }

        public int? SimInd { get; set; }

        public int? ZoneRanging { get; set; }

        public int? ExactDealDates { get; set; }

        public int? LocationMoveProcessingDays { get; set; }

        public int? LocationMovePurgeDays { get; set; }

        public int? DynamicAreaDiffInd { get; set; }

        public int? PriceClrBckgrndCnflctInd { get; set; }

        public int? PromoBckgrndCnflctInd { get; set; }

        public int? WrkshtBckgrndCnflctInd { get; set; }

        public int? AllowBuyGetCyclesInd { get; set; }

        public int? LockVersion { get; set; }

        public int? DispPromHeadMkup { get; set; }

        public int? DisplayConflictsOnly { get; set; }

        public int? ConflictHistoryDaysBefore { get; set; }

        public int? ConflictHistoryDaysAfter { get; set; }

        public int? FilterPriceChangeResults { get; set; }

        public int? FullPromoColDetail { get; set; }

        public int? FullPcColDetail { get; set; }

        public int? PcClrCalcMarkupInd { get; set; }

        public int? PriceChangeSearchMax { get; set; }

        public int? ClearanceSearchMax { get; set; }

        public int? PromotionSearchMax { get; set; }

        public int? FutRetSeedDaysBefore { get; set; }

        public int? DefaultEffectiveDate { get; set; }

        public int? PriceinquirySearchMax { get; set; }

        public int? PromoEndDateRequired { get; set; }

        public int? ClearanceResetInquiryMax { get; set; }

        public string Operation { get; set; }
    }
}