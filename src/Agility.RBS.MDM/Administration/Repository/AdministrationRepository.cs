// <copyright file="AdministrationRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Administration
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.Administration.Models;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.UtilityDomain.Models;
    using AutoMapper;
    using Devart.Data.Oracle;
    using Microsoft.Extensions.Caching.Distributed;

    public class AdministrationRepository : BaseOraclePackage
    {
        private readonly ServiceDocumentResult serviceDocumentResult;

        public AdministrationRepository()
        {
            this.PackageName = MdmConstants.ControlMasterDataPackage;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<List<SystemOptionsModel>> GetSystemoptionss()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("IM_SYS_OPTIONS_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.SystemOptionsTypeTab, MdmConstants.GetProcTypeSystemOptions);
            return await this.GetProcedure2<SystemOptionsModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<ReasonCodeModel>> GetReasonCode(ReasonCodeModel reasonCodeModel)
        {
            this.Connection.Open();
            OracleObject recordObject = this.SetParamsReasonCode(reasonCodeModel);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ReasonCodeTypeTab, MdmConstants.GetProcTypeReasonCode);
            return await this.GetProcedure2<ReasonCodeModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<ToleranceModel>> GetTolerance(ToleranceModel toleranceModel)
        {
            this.Connection.Open();
            var inputModel = toleranceModel ?? new ToleranceModel();
            OracleObject recordObject = this.SetToleranceParams(inputModel);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ToleranceTypeTab, MdmConstants.GetProcTypeTolerance);
            return await this.GetProcedure2<ToleranceModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<SecurityGroupModel>> GetSecurityGroup(SecurityGroupModel securityGroupModel)
        {
            this.Connection.Open();
            var inputModel = securityGroupModel ?? new SecurityGroupModel();
            OracleObject recordObject = this.SetSecurityGroupParams(inputModel);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.SecurityGroupTypeTab, MdmConstants.SecurityGroupGetProc);
            return await this.GetProcedure2<SecurityGroupModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<SecurityUserGroupModel>> GetSecurityUserGroup(SecurityUserGroupModel securityGroupModel)
        {
            this.Connection.Open();
            var inputModel = securityGroupModel ?? new SecurityUserGroupModel();
            OracleObject recordObject = this.SetSecurityUserGroupParams(inputModel);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.SecurityUserGroupTypeTab, MdmConstants.SecurityUserGroupGetProc);
            return await this.GetProcedure2<SecurityUserGroupModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<UserRolePrivilegesModel>> GetUserRolePrivileges(UserRolePrivilegesModel model)
        {
            this.Connection.Open();
            var inputModel = model ?? new UserRolePrivilegesModel();
            OracleObject recordObject = this.SetUserRolePrivilegesParams(inputModel);
            PackageParams packageParameter = this.GetPackageParams("rtk_role_privs_tab", "getroleprivs");
            return await this.GetProcedure2<UserRolePrivilegesModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<NbSystemOptionsModel>> GetNbSystemOptions(NbSystemOptionsModel toleranceModel)
        {
            this.Connection.Open();
            var inputModel = toleranceModel ?? new NbSystemOptionsModel();
            OracleObject recordObject = this.SetNbSystemOptionsParams(inputModel);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.NbSystemOptionsTypeTab, MdmConstants.GetProcTypeNbSystemOptions);
            return await this.GetProcedure2<NbSystemOptionsModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<RpmSystemOptionsModel>> GetRpmSystemOptionsOptions(RpmSystemOptionsModel toleranceModel)
        {
            this.Connection.Open();
            var inputModel = toleranceModel ?? new RpmSystemOptionsModel();
            OracleObject recordObject = this.SetRpmSystemOptionsModelParams(inputModel);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.RpmSystemOptionsTypeTab, MdmConstants.GetProcTypeRpmSystemOptions);
            return await this.GetProcedure2<RpmSystemOptionsModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<SaSystemOptionsModel>> GetSaSystemOptionsOptions(SaSystemOptionsModel toleranceModel)
        {
            this.Connection.Open();
            var inputModel = toleranceModel ?? new SaSystemOptionsModel();
            OracleObject recordObject = this.SetSaSystemOptionsModelParams(inputModel);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.SaSystemOptionsTypeTab, MdmConstants.GetProcTypeSaSystemOptions);
            return await this.GetProcedure2<SaSystemOptionsModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<SupplierOptionsModel>> GetSupplierOptionss(SupplierOptionsModel supplierOptionsModel)
        {
            this.Connection.Open();
            OracleObject recordObject = this.SetParamsSupplierOptions(supplierOptionsModel);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.SupplierOptionsTypeTab, MdmConstants.GetProcTypeSupplierOptions);
            return await this.GetProcedure2<SupplierOptionsModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<ServiceDocumentResult> SetSystemoptions(SystemOptionsModel locationTraitModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.SystemOptionsTypeTab, MdmConstants.SetProcTypeSystemOptions);
            OracleObject recordObject = this.SetParamsSystemoptions(locationTraitModel);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async Task<ServiceDocumentResult> SetSupplierOptions(SupplierOptionsModel locationTraitModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.SupplierOptionsTypeTab, MdmConstants.SetProcTypeSupplierOptions);
            OracleObject recordObject = this.SetParamsSupplierOptions(locationTraitModel);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async Task<ServiceDocumentResult> SetReasonCode(ReasonCodeModel locationTraitModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ReasonCodeTypeTab, MdmConstants.SetProcTypeReasonCode);
            OracleObject recordObject = this.SetParamsReasonCode(locationTraitModel);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async Task<ServiceDocumentResult> SetNonMerchandiseCode(NonMerchandiseCodesModel model)
        {
            PackageParams packageParameter = this.GetPackageParams("non_merch_head_tab", "setnonmerchandise");
            OracleObject recordObject = this.SetNonMerchandiseCodesParams(model);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async Task<ServiceDocumentResult> SetMessageSearch(MessageSearchModel model)
        {
            PackageParams packageParameter = this.GetPackageParams("message_tab", "setmessage");
            OracleObject recordObject = this.SetMessageSearchParams(model);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async Task<ServiceDocumentResult> SetTolerance(ToleranceModel locationTraitModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ToleranceTypeTab, MdmConstants.SetProcTypeTolerance);
            OracleObject recordObject = this.SetToleranceParams(locationTraitModel);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async Task<ServiceDocumentResult> SetSecurityGroup(SecurityGroupModel locationTraitModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.SecurityGroupTypeTab, MdmConstants.SecurityGroupSetProc);
            OracleObject recordObject = this.SetSecurityGroupParams(locationTraitModel);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async Task<ServiceDocumentResult> SetSecurityGroupHierarchy(SecurityGroupHierarchyModel locationTraitModel)
        {
            PackageParams packageParameter = this.GetPackageParams("filter_group_merch_tab", "setgroupmerch");
            OracleObject recordObject = this.SetSecurityGroupHierarchyParams(locationTraitModel);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async Task<ServiceDocumentResult> SetSecurityGroupOrgHierarchy(SecurityGroupOrgHierarchyModel model)
        {
            PackageParams packageParameter = this.GetPackageParams("filter_group_org_tab", "setgrouporg");
            OracleObject recordObject = this.SetSecurityGroupOrgHierarchyParams(model);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async Task<List<SecurityGroupHierarchyModel>> GetSecurityGroupHierarchy(SecurityGroupHierarchyModel securityGroupModel)
        {
            this.Connection.Open();
            var inputModel = securityGroupModel ?? new SecurityGroupHierarchyModel();
            OracleObject recordObject = this.SetSecurityGroupHierarchyParams(inputModel);
            PackageParams packageParameter = this.GetPackageParams("filter_group_merch_tab", "getgroupmerch");
            return await this.GetProcedure2<SecurityGroupHierarchyModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<NonMerchandiseCodesModel>> GetNonMerchandiseCodes(NonMerchandiseCodesModel model, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            var inputModel = model ?? new NonMerchandiseCodesModel();
            OracleObject recordObject = this.SetNonMerchandiseCodesParams(inputModel);
            PackageParams packageParameter = this.GetPackageParams("non_merch_head_tab", "getnonmerchandise");
            OracleTable pPromotionMstTab = await this.GetProcedure(recordObject, packageParameter, serviceDocumentResult);
            List<NonMerchandiseCodesModel> lstHeader = Mapper.Map<List<NonMerchandiseCodesModel>>(pPromotionMstTab);
            if (lstHeader != null)
            {
                for (int i = 0; i < lstHeader.Count; i++)
                {
                    OracleObject obj = (OracleObject)pPromotionMstTab[i];
                    OracleTable promotionDetailsObj = (Devart.Data.Oracle.OracleTable)obj["non_merch_comp"];
                    if (promotionDetailsObj.Count > 0)
                    {
                        lstHeader[i].Details.AddRange(Mapper.Map<List<NonMerchandiseCodeDetailModel>>(promotionDetailsObj));
                    }
                }
            }

            return lstHeader;

            ////return await this.GetProcedure2<NonMerchandiseCodesModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<SecurityGroupOrgHierarchyModel>> GetSecurityGroupOrgHierarchy(SecurityGroupOrgHierarchyModel securityGroupModel)
        {
            this.Connection.Open();
            var inputModel = securityGroupModel ?? new SecurityGroupOrgHierarchyModel();
            OracleObject recordObject = this.SetSecurityGroupOrgHierarchyParams(inputModel);
            PackageParams packageParameter = this.GetPackageParams("filter_group_org_tab", "getgrouporg");
            return await this.GetProcedure2<SecurityGroupOrgHierarchyModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<MessageSearchModel>> GetMessageSearch(MessageSearchModel model, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            var inputModel = model ?? new MessageSearchModel();
            OracleObject recordObject = this.SetMessageSearchParams(inputModel);
            PackageParams packageParameter = this.GetPackageParams("message_tab", "getmessage");
            return await this.GetProcedure2<MessageSearchModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<List<SystemVariablesModel>> GetSystemVariables(SystemVariablesModel model, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            var inputModel = model ?? new SystemVariablesModel();
            OracleObject recordObject = this.SetSysVariablesParams(inputModel);
            PackageParams packageParameter = this.GetPackageParams("system_variable_tab", "getsystemvariable");
            return await this.GetProcedure2<SystemVariablesModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ServiceDocumentResult> SetSecurityUserGroup(SecurityUserGroupModel locationTraitModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.SecurityUserGroupTypeTab, MdmConstants.SecurityUserGroupSetProc);
            OracleObject recordObject = this.SetSecurityUserGroupParams(locationTraitModel);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async Task<ServiceDocumentResult> SetUserRolePrivileges(UserRolePrivilegesModel model)
        {
            PackageParams packageParameter = this.GetPackageParams("rtk_role_privs_tab", "setroleprivs");
            OracleObject recordObject = this.SetUserRolePrivilegesParams(model);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async Task<ServiceDocumentResult> SetNbSystemOptions(NbSystemOptionsModel nbSystemOptionsModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.NbSystemOptionsTypeTab, MdmConstants.SetProcTypeNbSystemOptions);
            OracleObject recordObject = this.SetNbSystemOptionsParams(nbSystemOptionsModel);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async Task<ServiceDocumentResult> SetRpmSystemOptions(RpmSystemOptionsModel rpmSystemOptionsModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.RpmSystemOptionsTypeTab, MdmConstants.SetProcTypeRpmSystemOptions);
            OracleObject recordObject = this.SetRpmSystemOptionsModelParams(rpmSystemOptionsModel);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async Task<ServiceDocumentResult> SetSaSystemOptionsOptions(SaSystemOptionsModel saSystemOptionsModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.SaSystemOptionsTypeTab, MdmConstants.SetProcTypeSaSystemOptions);
            OracleObject recordObject = this.SetSaSystemOptionsModelParams(saSystemOptionsModel);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        private OracleObject SetParamsSystemoptions(SystemOptionsModel systemoptionsModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("IM_SYS_OPTIONS_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            recordObject["DEBIT_MEMO_SEND_DAYS"] = systemoptionsModel.DebitMemoSendDays;
            recordObject["CLOSE_OPEN_RECEIPT_DAYS"] = systemoptionsModel.CloseOpenReceiptDays;
            recordObject["COST_RESOLUTION_DUE_DAYS"] = systemoptionsModel.CostResolutionDueDays;
            recordObject["QTY_RESOLUTION_DUE_DAYS"] = systemoptionsModel.QtyResolutionDueDays;
            recordObject["DOC_HIST_DAYS"] = systemoptionsModel.DocHistDays;
            recordObject["DEBIT_MEMO_PREFIX_COST"] = systemoptionsModel.DebitMemoPrefixCost;
            recordObject["DEBIT_MEMO_PREFIX_QTY"] = systemoptionsModel.DebitMemoPrefixQty;
            recordObject["DEBIT_MEMO_PREFIX_VAT"] = systemoptionsModel.DebitMemoPrefixVat;
            recordObject["CREDIT_MEMO_PREFIX_COST"] = systemoptionsModel.CreditMemoPrefixCost;
            recordObject["CREDIT_MEMO_PREFIX_QTY"] = systemoptionsModel.CreditMemoPrefixQty;
            recordObject["CREDIT_NOTE_REQ_PREFIX_COST"] = systemoptionsModel.CreditNoteReqPrefixCost;
            recordObject["CREDIT_NOTE_REQ_PREFIX_QTY"] = systemoptionsModel.CreditNoteReqPrefixQty;
            recordObject["CREDIT_NOTE_REQ_PREFIX_VAT"] = systemoptionsModel.CreditNoteReqPrefixVat;
            recordObject["POST_DATED_DOC_DAYS"] = systemoptionsModel.PostDatedDocDays;
            recordObject["MAX_TOLERANCE_PCT"] = systemoptionsModel.MaxTolerancePct;
            recordObject["DAYS_BEFORE_DUE_DATE"] = systemoptionsModel.DaysBeforeDueDate;
            recordObject["DEFAULT_PAY_NOW_TERMS"] = systemoptionsModel.DefaultPayNowTerms;
            recordObject["VAT_IND"] = systemoptionsModel.VatInd;
            recordObject["CALC_TOLERANCE"] = systemoptionsModel.CalcTolerance;
            recordObject["VAT_VALIDATION_TYPE"] = systemoptionsModel.VatValidationType;
            recordObject["VAT_DOCUMENT_CREATION_LVL"] = systemoptionsModel.VatDocumentCreationLvl;
            recordObject["DEFAULT_VAT_HEADER"] = systemoptionsModel.DefaultVatHeader;
            recordObject["VAT_RESOLUTION_DUE_DAYS"] = systemoptionsModel.VatResolutionDueDays;
            recordObject["CALC_TOLERANCE_IND"] = systemoptionsModel.CalcToleranceIndicator;
            recordObject["POST_BASED_ON_DOC_HEADER"] = systemoptionsModel.PostBasedOnDocHeader;
            recordObject["OPERATION"] = systemoptionsModel.Operation;
            return recordObject;
        }

        private OracleObject SetParamsSupplierOptions(SupplierOptionsModel supplierOptionsModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("SUPP_OPTIONS_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            recordObject["SUPPLIER"] = !supplierOptionsModel.Supplier.HasValue ? -1 : supplierOptionsModel.Supplier;
            recordObject["SUPPLIER_NAME"] = supplierOptionsModel.SupplierName;
            recordObject["SEND_DEBIT_MEMO"] = supplierOptionsModel.SendDebitMemo;
            recordObject["MANUALLY_PAID_IND"] = supplierOptionsModel.ManuallyPaidIndicator;
            recordObject["USE_INVOICE_TERMS_IND"] = supplierOptionsModel.UseInvoceTermsIndicator;
            recordObject["ROG_DATE_ALLOWED_IND"] = supplierOptionsModel.RogDateAllowedIndicator;
            recordObject["AP_REVIEWER"] = supplierOptionsModel.ApReviewer;
            recordObject["CLOSE_OPEN_SHIPMENT_DAYS"] = supplierOptionsModel.CloseOpenShipmentDays;
            recordObject["MATCH_RCPTS_OTHER_SUPPS_IND"] = supplierOptionsModel.MatchRecieptsOthersuppLiersIndicator;
            recordObject["QTY_DISC_DAY_BEFORE_RTE"] = supplierOptionsModel.QtyDiscDayBeforeRte;
            recordObject["OPERATION"] = supplierOptionsModel.Operation;
            return recordObject;
        }

        private OracleObject SetParamsReasonCode(ReasonCodeModel reasonCodeModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("IM_REASON_CODE_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            recordObject["REASON_CODE_TYPE"] = string.IsNullOrEmpty(reasonCodeModel.ReasonCodeType) ? "-1" : reasonCodeModel.ReasonCodeType;
            recordObject["REASON_CODE_TYPE_DESC"] = reasonCodeModel.ReasonCodeTypeDescription;
            recordObject["REASON_CODE_ID"] = reasonCodeModel.ReasonCodeId;
            recordObject["REASON_CODE_DESC"] = reasonCodeModel.ReasonCodeDescription;
            recordObject["ACTION"] = reasonCodeModel.Action;
            recordObject["ACTION_DESC"] = reasonCodeModel.ActionDescription;
            recordObject["COMMENT_REQUIRED_IND"] = reasonCodeModel.CommentRequiredIndicator;
            recordObject["HINT_COMMENT"] = reasonCodeModel.HintComment;
            recordObject["DELETE_IND"] = reasonCodeModel.DeleteIndicator;
            recordObject["OPERATION"] = reasonCodeModel.Operation;
            return recordObject;
        }

        private OracleObject SetToleranceParams(ToleranceModel toleranceModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("IM_TLRC_SYS_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            recordObject["TOL_KEY"] = !toleranceModel.ToleranceKey.HasValue ? -1 : toleranceModel.ToleranceKey;
            recordObject["COST_QUANTITY_IND"] = toleranceModel.CostQuantityIndicator;
            recordObject["SUMMARY_LINE_IND"] = toleranceModel.SummaryLineIndicator;
            recordObject["TOLERANCE_DOCUMENT_TYPE"] = toleranceModel.ToleranceDocumentType;
            recordObject["LOWER_LIMIT_INCLUSIVE"] = toleranceModel.LowerLimitInclusive;
            recordObject["UPPER_LIMIT_EXCLUSIVE"] = toleranceModel.UpperLimitExclusive;
            recordObject["FAVOR_OF"] = toleranceModel.FavorOf;
            recordObject["TOL_VALUE_TYPE"] = toleranceModel.ToleranceValueType;
            recordObject["TOL_VALUE"] = toleranceModel.ToleranceValue;
            recordObject["UPDATE_ID"] = toleranceModel.UpdateId;
            recordObject["OPERATION"] = toleranceModel.Operation;
            return recordObject;
        }

        private OracleObject SetSecurityGroupParams(SecurityGroupModel toleranceModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("sec_group_rec");
            OracleObject recordObject = this.GetOracleObject(recordType);
            recordObject["group_id"] = toleranceModel.GroupId;
            recordObject["group_name"] = toleranceModel.GroupName;
            recordObject["role"] = toleranceModel.Role;
            recordObject["comments"] = toleranceModel.Comments;
            recordObject["OPERATION"] = toleranceModel.Operation;
            return recordObject;
        }

        private OracleObject SetSecurityGroupHierarchyParams(SecurityGroupHierarchyModel toleranceModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("filter_group_merch_rec");
            OracleObject recordObject = this.GetOracleObject(recordType);
            recordObject["sec_group_id"] = toleranceModel.GroupId;
            recordObject["filter_merch_level"] = toleranceModel.MerchLevel;
            recordObject["filter_merch_id"] = toleranceModel.MerchLevelValue;
            recordObject["OPERATION"] = toleranceModel.Operation;
            return recordObject;
        }

        private OracleObject SetNonMerchandiseCodesParams(NonMerchandiseCodesModel nonMerchandiseCodesModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("non_merch_head_rec");
            OracleObject recordObject = this.GetOracleObject(recordType);
            recordObject["non_merch_code"] = nonMerchandiseCodesModel.NonMerchandiseCode;
            recordObject["non_merch_code_desc"] = nonMerchandiseCodesModel.NonMerchandiseCodeDesc;
            recordObject["OPERATION"] = nonMerchandiseCodesModel.Operation;

            // Detail Object
            OracleType dtlTableType = this.GetObjectType("non_merch_comp_tab");
            OracleType dtlRecordType = this.GetObjectType("non_merch_comp_rec");
            OracleTable pUtlDmnDtlTab = new OracleTable(dtlTableType);
            foreach (var item in nonMerchandiseCodesModel.Details)
            {
                OracleObject dtlRecordObject = this.GetOracleObject(dtlRecordType);
                try
                {
                    dtlRecordObject["non_merch_code"] = nonMerchandiseCodesModel.NonMerchandiseCode;
                    dtlRecordObject[dtlRecordType.Attributes["comp_id"]] = item.ComponentId;
                    dtlRecordObject[dtlRecordType.Attributes["comp_desc"]] = item.Description;
                    dtlRecordObject[dtlRecordType.Attributes["OPERATION"]] = item.Operation;
                    pUtlDmnDtlTab.Add(dtlRecordObject);
                }
                catch (Exception)
                {
                    pUtlDmnDtlTab.Add(dtlRecordObject);
                }
            }

            recordObject["non_merch_comp"] = pUtlDmnDtlTab;
            return recordObject;
        }

        private OracleObject SetSecurityGroupOrgHierarchyParams(SecurityGroupOrgHierarchyModel toleranceModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("filter_group_org_rec");
            OracleObject recordObject = this.GetOracleObject(recordType);
            recordObject["sec_group_id"] = toleranceModel.GroupId;
            recordObject["filter_org_level"] = toleranceModel.OrgLevel;
            recordObject["filter_org_id"] = toleranceModel.OrgLevelValue;
            recordObject["OPERATION"] = toleranceModel.Operation;
            return recordObject;
        }

        private OracleObject SetMessageSearchParams(MessageSearchModel model)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("message_rec");
            OracleObject recordObject = this.GetOracleObject(recordType);
            recordObject["rtk_type"] = model.ErrorType;
            recordObject["rtk_key"] = model.Key;
            recordObject["rtk_text"] = model.Text;
            recordObject["rtk_user"] = model.User;
            recordObject["rtk_approved"] = model.IsApproved;
            recordObject["rtk_lang"] = model.Language;
            recordObject["OPERATION"] = model.Operation;
            return recordObject;
        }

        private OracleObject SetSysVariablesParams(SystemVariablesModel model)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("system_variable_rec");
            OracleObject recordObject = this.GetOracleObject(recordType);
            recordObject["OPERATION"] = model.Operation;
            return recordObject;
        }

        private OracleObject SetSecurityUserGroupParams(SecurityUserGroupModel toleranceModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType(MdmConstants.SecurityUserGroupTypeRec);
            OracleObject recordObject = this.GetOracleObject(recordType);
            recordObject["group_id"] = toleranceModel.GroupId;
            recordObject["group_name"] = toleranceModel.GroupName;
            recordObject["user_id"] = toleranceModel.UserId;
            recordObject["username"] = toleranceModel.UserName;
            recordObject["OPERATION"] = toleranceModel.Operation;
            return recordObject;
        }

        private OracleObject SetUserRolePrivilegesParams(UserRolePrivilegesModel model)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("rtk_role_privs_rec");
            OracleObject recordObject = this.GetOracleObject(recordType);
            recordObject["ROLE"] = model.Role;
            recordObject["ord_appr_amt"] = model.OrdApprAmt;
            recordObject["tsf_appr_ind"] = model.TsfApprInd;
            recordObject["OPERATION"] = model.Operation;
            return recordObject;
        }

        private OracleObject SetNbSystemOptionsParams(NbSystemOptionsModel nbSystemOptionsModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("nb_sys_options_rec");
            OracleObject recordObject = this.GetOracleObject(recordType);
            recordObject["transformable_cust_ind"] = nbSystemOptionsModel.TransformableCustInd;
            return recordObject;
        }

        private OracleObject SetRpmSystemOptionsModelParams(RpmSystemOptionsModel rpmSystemOptionsModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("rpm_sys_options_rec");
            OracleObject recordObject = this.GetOracleObject(recordType);
            recordObject["system_options_id"] = rpmSystemOptionsModel.SystemOptionsId;
            return recordObject;
        }

        private OracleObject SetSaSystemOptionsModelParams(SaSystemOptionsModel saSystemOptionsModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("sa_sys_options_rec");
            OracleObject recordObject = this.GetOracleObject(recordType);
            recordObject["days_before_purge"] = saSystemOptionsModel.DaysBeforePurge;
            return recordObject;
        }
    }
}