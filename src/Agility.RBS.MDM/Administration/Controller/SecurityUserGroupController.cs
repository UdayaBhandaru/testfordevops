//-------------------------------------------------------------------------------------------------
// <copyright file="SecurityUserGroupController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// ToleranceController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Administration
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Administration.Models;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Freight.Models;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    public class SecurityUserGroupController : Controller
    {
        private readonly ServiceDocument<SecurityUserGroupModel> serviceDocument;
        private readonly AdministrationRepository administrationRepository;
        private readonly DomainDataRepository domainDataRepository;

        public SecurityUserGroupController(
            ServiceDocument<SecurityUserGroupModel> serviceDocument,
            AdministrationRepository administrationRepository,
            DomainDataRepository domainDataRepository)
        {
            this.administrationRepository = administrationRepository;
            this.serviceDocument = serviceDocument;
            this.domainDataRepository = domainDataRepository;
        }

        [HttpPost]
        public async Task<ServiceDocument<SecurityUserGroupModel>> Save([FromBody]ServiceDocument<SecurityUserGroupModel> serviceDocument)
        {
            await this.serviceDocument.SaveAsync(this.SecurityGroupSetDataCallback);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<SecurityUserGroupModel>> List()
        {
            this.serviceDocument.DomainData.Add("groupList", this.domainDataRepository.SecurityGroupDomainGet().Result);
            this.serviceDocument.DomainData.Add("userList", this.domainDataRepository.SecurityUserDomainGet().Result);
            return await this.SecurityGroupGetData();
        }

        [HttpPost]
        public async Task<ServiceDocument<SecurityUserGroupModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.SecurityGroupGetDataCallback);
            return this.serviceDocument;
        }

        private async Task<ServiceDocument<SecurityUserGroupModel>> SecurityGroupGetData()
        {
            this.serviceDocument.DataProfile.DataList = await this.administrationRepository.GetSecurityUserGroup(this.serviceDocument.DataProfile.DataModel);
            return this.serviceDocument;
        }

        private async Task<List<SecurityUserGroupModel>> SecurityGroupGetDataCallback()
        {
            try
            {
                return await this.administrationRepository.GetSecurityUserGroup(this.serviceDocument.DataProfile.DataModel);
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> SecurityGroupSetDataCallback()
        {
            this.serviceDocument.Result = await this.administrationRepository.SetSecurityUserGroup(this.serviceDocument.DataProfile.DataModel);
            if (this.serviceDocument.Result.Type != MessageType.Success)
            {
                return false;
            }

            return true;
        }
    }
}
