//-------------------------------------------------------------------------------------------------
// <copyright file="UserRolePrivilegesController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// ToleranceController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Administration
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Administration.Models;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Freight.Models;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    public class UserRolePrivilegesController : Controller
    {
        private readonly ServiceDocument<UserRolePrivilegesModel> serviceDocument;
        private readonly DomainDataRepository domainDataRepository;
        private readonly AdministrationRepository administrationRepository;

        public UserRolePrivilegesController(
            ServiceDocument<UserRolePrivilegesModel> serviceDocument,
            AdministrationRepository administrationRepository,
            DomainDataRepository domainDataRepository)
        {
            this.serviceDocument = serviceDocument;
            this.domainDataRepository = domainDataRepository;
            this.administrationRepository = administrationRepository;
        }

        public async Task<ServiceDocument<UserRolePrivilegesModel>> List()
        {
            this.serviceDocument.DomainData.Add("indicator", this.domainDataRepository.DomainDetailData(MdmConstants.VatRegionTypeDomainData).Result);
            this.serviceDocument.DomainData.Add("roleList", await this.domainDataRepository.UserRolePrivilegesDomainGet());
            return this.serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<UserRolePrivilegesModel>> Save([FromBody]ServiceDocument<UserRolePrivilegesModel> serviceDocument)
        {
            await this.serviceDocument.SaveAsync(this.SecurityGroupSetDataCallback);
            return this.serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<UserRolePrivilegesModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.SecurityGroupGetDataCallback);
            return this.serviceDocument;
        }

        private async Task<List<UserRolePrivilegesModel>> SecurityGroupGetDataCallback()
        {
            try
            {
                return await this.administrationRepository.GetUserRolePrivileges(this.serviceDocument.DataProfile.DataModel);
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> SecurityGroupSetDataCallback()
        {
            this.serviceDocument.Result = await this.administrationRepository.SetUserRolePrivileges(this.serviceDocument.DataProfile.DataModel);
            if (this.serviceDocument.Result.Type != MessageType.Success)
            {
                return false;
            }

            return true;
        }
    }
}
