//-------------------------------------------------------------------------------------------------
// <copyright file="MessageSearchController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// SystemoptionsController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Administration
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Administration.Models;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Freight.Models;
    using Agility.RBS.MDM.MerchantHierarchy.Models;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    public class MessageSearchController : Controller
    {
        private readonly ServiceDocument<MessageSearchModel> serviceDocument;
        private readonly AdministrationRepository administrationRepository;
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;

        public MessageSearchController(
            ServiceDocument<MessageSearchModel> serviceDocument,
            AdministrationRepository administrationRepository,
            DomainDataRepository domainDataRepository)
        {
            this.administrationRepository = administrationRepository;
            this.serviceDocument = serviceDocument;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<MessageSearchModel>> List()
        {
            this.serviceDocument.DomainData.Add("errorTypeList", this.domainDataRepository.DomainDetailData("RETY").Result);
            this.serviceDocument.DomainData.Add("yesNoList", this.domainDataRepository.DomainDetailData("YSNO").Result);
            this.serviceDocument.DomainData.Add("languagesList", await this.domainDataRepository.LanguageDomainGet());
            return this.serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<MessageSearchModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.MessageSearchGetCallback);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<MessageSearchModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.MessageSearchSave);
            return this.serviceDocument;
        }

        private async Task<List<MessageSearchModel>> MessageSearchGetCallback()
        {
            try
            {
                return await this.administrationRepository.GetMessageSearch(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> MessageSearchSave()
        {
            this.serviceDocument.Result = await this.administrationRepository.SetMessageSearch(this.serviceDocument.DataProfile.DataModel);
            return true;
        }
    }
}
