//-------------------------------------------------------------------------------------------------
// <copyright file="NbSystemOptionsController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// NbSystemOptionsController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Administration
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Administration.Models;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Freight.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class NbSystemOptionsController : Controller
    {
        private readonly ServiceDocument<NbSystemOptionsModel> serviceDocument;
        private readonly AdministrationRepository administrationRepository;

        public NbSystemOptionsController(
            ServiceDocument<NbSystemOptionsModel> serviceDocument,
            AdministrationRepository administrationRepository)
        {
            this.administrationRepository = administrationRepository;
            this.serviceDocument = serviceDocument;
        }

        public async Task<ServiceDocument<NbSystemOptionsModel>> List()
        {
            return await this.NbSystemOptionsData();
        }

        public async Task<ServiceDocument<NbSystemOptionsModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.LocalTraitSave);
            return this.serviceDocument;
        }

        private async Task<ServiceDocument<NbSystemOptionsModel>> NbSystemOptionsData()
        {
            this.serviceDocument.DataProfile.DataList = await this.administrationRepository.GetNbSystemOptions(this.serviceDocument.DataProfile.DataModel);
            return this.serviceDocument;
        }

        private async Task<bool> LocalTraitSave()
        {
            this.serviceDocument.DataProfile.DataModel.Operation = "U";
            this.serviceDocument.Result = await this.administrationRepository.SetNbSystemOptions(this.serviceDocument.DataProfile.DataModel);
            return true;
        }
    }
}
