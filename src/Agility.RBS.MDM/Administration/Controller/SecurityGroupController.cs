//-------------------------------------------------------------------------------------------------
// <copyright file="SecurityGroupController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// ToleranceController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Administration
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Administration.Models;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Freight.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    public class SecurityGroupController : Controller
    {
        private readonly ServiceDocument<SecurityGroupModel> serviceDocument;
        private readonly AdministrationRepository administrationRepository;

        public SecurityGroupController(
            ServiceDocument<SecurityGroupModel> serviceDocument,
            AdministrationRepository administrationRepository)
        {
            this.administrationRepository = administrationRepository;
            this.serviceDocument = serviceDocument;
        }

        public async Task<ServiceDocument<SecurityGroupModel>> List()
        {
            return await this.SecurityGroupGetData();
        }

        [HttpPost]
        public async Task<ServiceDocument<SecurityGroupModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.SecurityGroupGetDataCallback);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<SecurityGroupModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.SetSecurityGroup);
            return this.serviceDocument;
        }

        private async Task<ServiceDocument<SecurityGroupModel>> SecurityGroupGetData()
        {
            this.serviceDocument.DataProfile.DataList = await this.administrationRepository.GetSecurityGroup(this.serviceDocument.DataProfile.DataModel);
            return this.serviceDocument;
        }

        private async Task<bool> SetSecurityGroup()
        {
            this.serviceDocument.Result = await this.administrationRepository.SetSecurityGroup(this.serviceDocument.DataProfile.DataModel);
            return true;
        }

        private async Task<List<SecurityGroupModel>> SecurityGroupGetDataCallback()
        {
            try
            {
                return await this.administrationRepository.GetSecurityGroup(this.serviceDocument.DataProfile.DataModel);
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }
    }
}
