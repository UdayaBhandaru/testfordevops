//-------------------------------------------------------------------------------------------------
// <copyright file="SystemVariablesController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// SystemoptionsController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Administration
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Administration.Models;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Freight.Models;
    using Agility.RBS.MDM.MerchantHierarchy.Models;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    public class SystemVariablesController : Controller
    {
        private readonly ServiceDocument<SystemVariablesModel> serviceDocument;
     private readonly AdministrationRepository administrationRepository;
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;

        public SystemVariablesController(
            ServiceDocument<SystemVariablesModel> serviceDocument,
            AdministrationRepository administrationRepository,
            DomainDataRepository domainDataRepository)
        {
            this.administrationRepository = administrationRepository;
            this.serviceDocument = serviceDocument;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<SystemVariablesModel>> List()
        {
            this.serviceDocument.DomainData.Add("countryList", this.domainDataRepository.CountryDomainGet().Result);
            this.serviceDocument.DomainData.Add("languagesList", this.domainDataRepository.LanguageDomainGet().Result);
            this.serviceDocument.DomainData.Add("invHistLvlList", this.domainDataRepository.DomainDetailData("INVH").Result);
            this.serviceDocument.DomainData.Add("distributionRuleList", this.domainDataRepository.DomainDetailData("DRUL").Result);
            this.serviceDocument.DomainData.Add("currencyList", await this.domainDataRepository.CurrencyDomainGet());
            this.serviceDocument.DomainData.Add("allocMethodList", this.domainDataRepository.DomainDetailData("ALMT").Result);
            this.serviceDocument.DomainData.Add("racRtvTsfIndList", this.domainDataRepository.DomainDetailData("RTSF").Result);
            this.serviceDocument.DomainData.Add("storeToStoreList", this.domainDataRepository.DomainDetailData("TFML").Result);
            this.serviceDocument.DomainData.Add("stockLedgerProdLevelCodeList", this.domainDataRepository.DomainDetailData("MER1").Result);
            this.serviceDocument.DomainData.Add("stockLedgerTimeLevelCodeList", this.domainDataRepository.DomainDetailData("SKTL").Result);
            this.serviceDocument.DomainData.Add("stockLedgerLocLevelCodeList", this.domainDataRepository.DomainDetailData("SKLL").Result);
            this.serviceDocument.DomainData.Add("rcvCostAdjTypeList", this.domainDataRepository.DomainDetailData("LACO").Result);
            this.serviceDocument.DomainData.Add("otbProdLevelCodeList", this.domainDataRepository.DomainDetailData("MER1").Result);
            this.serviceDocument.DomainData.Add("otbTimeLevelCodeList", this.domainDataRepository.DomainDetailData("LABL").Result);
            await this.serviceDocument.ToListAsync(this.SystemVariablesGetCallback);
            return this.serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<SystemVariablesModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.SystemVariablesGetCallback);
            return this.serviceDocument;
        }

        private async Task<List<SystemVariablesModel>> SystemVariablesGetCallback()
        {
            try
            {
                return await this.administrationRepository.GetSystemVariables(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }
    }
}
