//-------------------------------------------------------------------------------------------------
// <copyright file="SystemOptionsController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// SystemoptionsController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Administration
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Administration.Models;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Freight.Models;
    using Agility.RBS.MDM.MerchantHierarchy.Models;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class SystemOptionsController : Controller
    {
        private readonly ServiceDocument<SystemOptionsModel> serviceDocument;
        private readonly AdministrationRepository traitRepository;

        public SystemOptionsController(
            ServiceDocument<SystemOptionsModel> serviceDocument,
            AdministrationRepository traitRepository)
        {
            this.traitRepository = traitRepository;
            this.serviceDocument = serviceDocument;
        }

        public async Task<ServiceDocument<SystemOptionsModel>> List()
        {
            return await this.SystemoptionsData();
        }

        public async Task<ServiceDocument<SystemOptionsModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.LocalTraitSave);
            return this.serviceDocument;
        }

        private async Task<ServiceDocument<SystemOptionsModel>> SystemoptionsData()
        {
            this.serviceDocument.DataProfile.DataList = await this.traitRepository.GetSystemoptionss();
            return this.serviceDocument;
        }

        private async Task<bool> LocalTraitSave()
        {
            this.serviceDocument.DataProfile.DataModel.Operation = "U";
            this.serviceDocument.Result = await this.traitRepository.SetSystemoptions(this.serviceDocument.DataProfile.DataModel);
            return true;
        }
    }
}
