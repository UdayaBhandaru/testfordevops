//-------------------------------------------------------------------------------------------------
// <copyright file="SaSystemOptionsController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// SaSystemOptionsController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Administration
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Administration.Models;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Freight.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class SaSystemOptionsController : Controller
    {
        private readonly ServiceDocument<SaSystemOptionsModel> serviceDocument;
        private readonly AdministrationRepository administrationRepository;

        public SaSystemOptionsController(
            ServiceDocument<SaSystemOptionsModel> serviceDocument,
            AdministrationRepository administrationRepository)
        {
            this.administrationRepository = administrationRepository;
            this.serviceDocument = serviceDocument;
        }

        public async Task<ServiceDocument<SaSystemOptionsModel>> List()
        {
            return await this.SaSystemOptionsData();
        }

        public async Task<ServiceDocument<SaSystemOptionsModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.LocalTraitSave);
            return this.serviceDocument;
        }

        private async Task<ServiceDocument<SaSystemOptionsModel>> SaSystemOptionsData()
        {
            this.serviceDocument.DataProfile.DataList = await this.administrationRepository.GetSaSystemOptionsOptions(this.serviceDocument.DataProfile.DataModel);
            return this.serviceDocument;
        }

        private async Task<bool> LocalTraitSave()
        {
            this.serviceDocument.DataProfile.DataModel.Operation = "U";
            this.serviceDocument.Result = await this.administrationRepository.SetSaSystemOptionsOptions(this.serviceDocument.DataProfile.DataModel);
            return true;
        }
    }
}
