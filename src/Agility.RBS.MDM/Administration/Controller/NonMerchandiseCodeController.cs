//-------------------------------------------------------------------------------------------------
// <copyright file="NonMerchandiseCodeController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// SystemoptionsController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Administration
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Administration.Models;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Freight.Models;
    using Agility.RBS.MDM.MerchantHierarchy.Models;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    public class NonMerchandiseCodeController : Controller
    {
        private readonly ServiceDocument<NonMerchandiseCodesModel> serviceDocument;
        private readonly AdministrationRepository administrationRepository;
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;

        public NonMerchandiseCodeController(
            ServiceDocument<NonMerchandiseCodesModel> serviceDocument,
            AdministrationRepository administrationRepository,
            DomainDataRepository domainDataRepository)
        {
            this.administrationRepository = administrationRepository;
            this.serviceDocument = serviceDocument;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<NonMerchandiseCodesModel>> List()
        {
            this.serviceDocument.DomainData.Add("domain", this.domainDataRepository.ComponentDetailsDomainGet().Result);
            return this.serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<NonMerchandiseCodesModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.NonMerchandiseCodesGetCallback);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<NonMerchandiseCodesModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.NonMerchandiseCodesSave);
            return this.serviceDocument;
        }

        private async Task<List<NonMerchandiseCodesModel>> NonMerchandiseCodesGetCallback()
        {
            try
            {
                return await this.administrationRepository.GetNonMerchandiseCodes(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> NonMerchandiseCodesSave()
        {
            this.serviceDocument.Result = await this.administrationRepository.SetNonMerchandiseCode(this.serviceDocument.DataProfile.DataModel);
            return true;
        }
    }
}
