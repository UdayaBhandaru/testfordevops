//-------------------------------------------------------------------------------------------------
// <copyright file="ReasonCodeController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// SystemoptionsController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Administration
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Administration.Models;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Freight.Models;
    using Agility.RBS.MDM.MerchantHierarchy.Models;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class ReasonCodeController : Controller
    {
        private readonly ServiceDocument<ReasonCodeModel> serviceDocument;
        private readonly AdministrationRepository adminsitratorRepository;

        public ReasonCodeController(
            ServiceDocument<ReasonCodeModel> serviceDocument,
            AdministrationRepository traitRepository)
        {
            this.adminsitratorRepository = traitRepository;
            this.serviceDocument = serviceDocument;
        }

        public async Task<ServiceDocument<ReasonCodeModel>> List()
        {
            return await this.SystemoptionsData();
        }

        public async Task<ServiceDocument<ReasonCodeModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.LocalTraitSave);
            return this.serviceDocument;
        }

        private async Task<ServiceDocument<ReasonCodeModel>> SystemoptionsData()
        {
            this.serviceDocument.DataProfile.DataList = await this.adminsitratorRepository.GetReasonCode(this.serviceDocument.DataProfile.DataModel);
            return this.serviceDocument;
        }

        private async Task<bool> LocalTraitSave()
        {
            this.serviceDocument.DataProfile.DataModel.Operation = "U";
            this.serviceDocument.Result = await this.adminsitratorRepository.SetReasonCode(this.serviceDocument.DataProfile.DataModel);
            return true;
        }
    }
}
