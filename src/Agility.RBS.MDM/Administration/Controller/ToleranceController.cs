//-------------------------------------------------------------------------------------------------
// <copyright file="ToleranceController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// ToleranceController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Administration
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Administration.Models;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Freight.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class ToleranceController : Controller
    {
        private readonly ServiceDocument<ToleranceModel> serviceDocument;
        private readonly AdministrationRepository administrationRepository;

        public ToleranceController(
            ServiceDocument<ToleranceModel> serviceDocument,
            AdministrationRepository administrationRepository)
        {
            this.administrationRepository = administrationRepository;
            this.serviceDocument = serviceDocument;
        }

        public async Task<ServiceDocument<ToleranceModel>> List()
        {
            return await this.ToleranceData();
        }

        public async Task<ServiceDocument<ToleranceModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.LocalTraitSave);
            return this.serviceDocument;
        }

        private async Task<ServiceDocument<ToleranceModel>> ToleranceData()
        {
            this.serviceDocument.DataProfile.DataList = await this.administrationRepository.GetTolerance(this.serviceDocument.DataProfile.DataModel);
            return this.serviceDocument;
        }

        private async Task<bool> LocalTraitSave()
        {
            this.serviceDocument.DataProfile.DataModel.Operation = "U";
            this.serviceDocument.Result = await this.administrationRepository.SetTolerance(this.serviceDocument.DataProfile.DataModel);
            return true;
        }
    }
}
