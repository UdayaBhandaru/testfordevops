//-------------------------------------------------------------------------------------------------
// <copyright file="SecurityGroupOrgHierarchyController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// ToleranceController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Administration
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Linq;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core.Models;
    using Agility.RBS.MDM.Administration.Models;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Freight.Models;
    using Agility.RBS.MDM.OrganizationHierarchy;
    using Agility.RBS.MDM.OrganizationHierarchy.Models;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    public class SecurityGroupOrgHierarchyController : Controller
    {
        private readonly ServiceDocument<SecurityGroupOrgHierarchyModel> serviceDocument;
        private readonly DomainDataRepository domainDataRepository;
        private readonly OrganizationHierarchyRepository organizationHierarchyRepository;
        private readonly AdministrationRepository administrationRepository;

        public SecurityGroupOrgHierarchyController(
            ServiceDocument<SecurityGroupOrgHierarchyModel> serviceDocument,
            OrganizationHierarchyRepository organizationHierarchyRepository,
            DomainDataRepository domainDataRepository,
            AdministrationRepository administrationRepository)
        {
            this.serviceDocument = serviceDocument;
            this.domainDataRepository = domainDataRepository;
            this.organizationHierarchyRepository = organizationHierarchyRepository;
            this.administrationRepository = administrationRepository;
        }

        public async Task<ServiceDocument<SecurityGroupOrgHierarchyModel>> List()
        {
            OrgLevelDomainModel orgDomainDetailModel = new OrgLevelDomainModel();
            var securityGroups = this.domainDataRepository.SecGroupDomainGet().Result;
            orgDomainDetailModel.SecurityGroups.AddRange(securityGroups);
            var orgLevels = this.domainDataRepository.DomainDetailData("FLTO").Result;
            foreach (var orgLevel in orgLevels)
            {
                string somValue = string.Empty;
                switch (orgLevel.CodeDesc)
                {
                    case "@OH2":
                        somValue = "Chain";
                        break;
                    case "@OH3":
                        somValue = "Area";
                        break;
                    case "@OH4":
                        somValue = "Region";
                        break;
                    case "@OH5":
                        somValue = "District";
                        break;
                    default:
                        somValue = string.Empty;
                        break;
                }

                if (!string.IsNullOrEmpty(somValue))
                {
                    orgLevel.CodeDesc = somValue;
                }
            }

            orgDomainDetailModel.OrgLevels.AddRange(orgLevels);

            var chainDomain = this.domainDataRepository.ChainDomainGet().Result;
            List<CommonModel> lstChainDomain = new List<CommonModel>();
            foreach (var c in chainDomain)
            {
                lstChainDomain.Add(new CommonModel { Id = Convert.ToString(c.Chain), Name = c.ChainName });
            }

            orgDomainDetailModel.Chains.AddRange(lstChainDomain);
            var areaDomain = this.domainDataRepository.AreaDomainGet().Result;
            List<CommonModel> lstAreaDomain = new List<CommonModel>();
            foreach (var c in areaDomain)
            {
                lstAreaDomain.Add(new CommonModel { Id = Convert.ToString(c.Area), Name = c.AreaName });
            }

            orgDomainDetailModel.Areas.AddRange(lstAreaDomain);
            ServiceDocumentResult serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
            var regionDomain = await this.organizationHierarchyRepository.GetOrgRegion(new RegionModel(), serviceDocumentResult);
            var query = from r in regionDomain
                        select new CommonModel
                        {
                            Id = Convert.ToString(r.Region),
                            Name = r.RegionName
                        };
            List<CommonModel> lstRegion = query.ToList();
            orgDomainDetailModel.Regions.AddRange(lstRegion);
            var districtsDomain = this.domainDataRepository.DistrictsDomainGet().Result;
            List<CommonModel> lstDist = new List<CommonModel>();
            foreach (var d in districtsDomain)
            {
                lstDist.Add(new CommonModel { Id = Convert.ToString(d.District), Name = d.DistrictName });
            }

            orgDomainDetailModel.Districts.AddRange(lstDist);
            this.serviceDocument.DomainData.Add("domainModel", orgDomainDetailModel);
            return this.serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<SecurityGroupOrgHierarchyModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.SecurityGroupGetDataCallback);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<SecurityGroupOrgHierarchyModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.SetSecurityGroupHierarchy);
            return this.serviceDocument;
        }

        private async Task<bool> SetSecurityGroupHierarchy()
        {
            this.serviceDocument.Result = await this.administrationRepository.SetSecurityGroupOrgHierarchy(this.serviceDocument.DataProfile.DataModel);
            return true;
        }

        private async Task<List<SecurityGroupOrgHierarchyModel>> SecurityGroupGetDataCallback()
        {
            try
            {
                return await this.administrationRepository.GetSecurityGroupOrgHierarchy(this.serviceDocument.DataProfile.DataModel);
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }
    }
}
