//-------------------------------------------------------------------------------------------------
// <copyright file="SecurityGroupHierarchyController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// ToleranceController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Administration
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core.Models;
    using Agility.RBS.MDM.Administration.Models;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Freight.Models;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    public class SecurityGroupHierarchyController : Controller
    {
        private readonly ServiceDocument<SecurityGroupHierarchyModel> serviceDocument;
        private readonly AdministrationRepository administrationRepository;
        private readonly DomainDataRepository domainDataRepository;

        public SecurityGroupHierarchyController(
            ServiceDocument<SecurityGroupHierarchyModel> serviceDocument,
            AdministrationRepository administrationRepository,
             DomainDataRepository domainDataRepository)
        {
            this.administrationRepository = administrationRepository;
            this.serviceDocument = serviceDocument;
            this.domainDataRepository = domainDataRepository;
        }

        public async Task<ServiceDocument<SecurityGroupHierarchyModel>> List()
        {
            MerchLevelDomainModel domainModel = new MerchLevelDomainModel();
            domainModel.SecurityGroups.AddRange(this.domainDataRepository.SecGroupDomainGet().Result);
            var merchLevels = this.domainDataRepository.DomainDetailData("FLTM").Result;
            foreach (var merchLevel in merchLevels)
            {
                string somValue = string.Empty;
                switch (merchLevel.CodeDesc)
                {
                    case "@MH2":
                        somValue = "Division";
                        break;
                    case "@MH3":
                        somValue = "Department";
                        break;
                    case "@MH4":
                        somValue = "Category";
                        break;
                    case "@MH5":
                        somValue = "Fineline";
                        break;
                    case "@MH6":
                        somValue = "Segment";
                        break;
                    default:
                        somValue = string.Empty;
                        break;
                }

                if (!string.IsNullOrEmpty(somValue))
                {
                    merchLevel.CodeDesc = somValue;
                }
            }

            domainModel.MerchLevels.AddRange(merchLevels);

            var divisions = this.domainDataRepository.DivisionDomainGet().Result;
            List<CommonModel> lstDivision = new List<CommonModel>();
            foreach (var division in divisions)
            {
                lstDivision.Add(new CommonModel { Id = Convert.ToString(division.Division), Name = division.DivName });
            }

            domainModel.Divisions.AddRange(lstDivision);

            var groups = this.domainDataRepository.GroupDomainGet().Result;
            List<CommonModel> lstGroup = new List<CommonModel>();
            foreach (var group in groups)
            {
                lstGroup.Add(new CommonModel { Id = Convert.ToString(group.GroupNo), Name = group.GroupName });
            }

            domainModel.Groups.AddRange(lstGroup);
            var depts = this.domainDataRepository.DepartmentDomainGet().Result;
            List<CommonModel> lstDept = new List<CommonModel>();
            foreach (var dept in depts)
            {
                lstDept.Add(new CommonModel { Id = Convert.ToString(dept.Dept), Name = dept.DeptName });
            }

            domainModel.Departments.AddRange(lstDept);
            this.serviceDocument.DomainData.Add("domainModel", domainModel);
            return this.serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<SecurityGroupHierarchyModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.SecurityGroupGetDataCallback);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<SecurityGroupHierarchyModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.SetSecurityGroupHierarchy);
            return this.serviceDocument;
        }

        private async Task<bool> SetSecurityGroupHierarchy()
        {
            this.serviceDocument.Result = await this.administrationRepository.SetSecurityGroupHierarchy(this.serviceDocument.DataProfile.DataModel);
            return true;
        }

        private async Task<List<SecurityGroupHierarchyModel>> SecurityGroupGetDataCallback()
        {
            try
            {
                return await this.administrationRepository.GetSecurityGroupHierarchy(this.serviceDocument.DataProfile.DataModel);
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }
    }
}
