//-------------------------------------------------------------------------------------------------
// <copyright file="SupplierOptionsController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// SupplierOptionsController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Administration
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Administration.Models;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Freight.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class SupplierOptionsController : Controller
    {
        private readonly ServiceDocument<SupplierOptionsModel> serviceDocument;
        private readonly AdministrationRepository administrationRepository;

        public SupplierOptionsController(
            ServiceDocument<SupplierOptionsModel> serviceDocument,
            AdministrationRepository administrationRepository)
        {
            this.administrationRepository = administrationRepository;
            this.serviceDocument = serviceDocument;
        }

        public async Task<ServiceDocument<SupplierOptionsModel>> List()
        {
            return await this.SupplierOptionsData();
        }

        public async Task<ServiceDocument<SupplierOptionsModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.LocalTraitSave);
            return this.serviceDocument;
        }

        private async Task<ServiceDocument<SupplierOptionsModel>> SupplierOptionsData()
        {
            this.serviceDocument.DataProfile.DataList = await this.administrationRepository.GetSupplierOptionss(this.serviceDocument.DataProfile.DataModel);
            return this.serviceDocument;
        }

        private async Task<bool> LocalTraitSave()
        {
            this.serviceDocument.DataProfile.DataModel.Operation = "U";
            this.serviceDocument.Result = await this.administrationRepository.SetSupplierOptions(this.serviceDocument.DataProfile.DataModel);
            return true;
        }
    }
}
