﻿// <copyright file="InvStatusTypeController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.InvAdjustmentManagement
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Text;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.InvAdjustmentManagement.Models;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class InvStatusTypeController : Controller
    {
        private readonly ServiceDocument<InvStatusTypeModel> serviceDocument;
        private readonly InvAdjustmentRepository invAdjustmentRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly DomainDataRepository domainDataRepository;

        public InvStatusTypeController(
            ServiceDocument<InvStatusTypeModel> serviceDocument,
            InvAdjustmentRepository invAdjustmentRepository,
            DomainDataRepository domainDataRepository)
        {
            this.invAdjustmentRepository = invAdjustmentRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<InvStatusTypeModel>> List()
        {
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<InvStatusTypeModel>> New()
        {
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<InvStatusTypeModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.InvAdjustmentReasonSearch);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingInvStatusType(string reason, string reasonDesc)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "INV_ADJ_REASON";
            nvc["P_COL1"] = reason;
            nvc["P_COL2"] = reasonDesc;
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        private async Task<List<InvStatusTypeModel>> InvAdjustmentReasonSearch()
        {
            try
            {
                var lstInvStatusType = await this.invAdjustmentRepository.GetInvStatusTypeAsync(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstInvStatusType;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }
    }
}
