﻿// <copyright file="InvAdjustmentRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.InvAdjustmentManagement
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.GlCrossReference.Models;
    using Agility.RBS.MDM.InvAdjustmentManagement.Models;
    using Devart.Data.Oracle;

    public class InvAdjustmentRepository : BaseOraclePackage
    {
        public InvAdjustmentRepository()
        {
            this.PackageName = MdmConstants.GetInvAdjustmentReasonPackage;
        }

        public async Task<List<InvAdjustmentReasonModel>> GetInvAdjustmentReasonAsync(InvAdjustmentReasonModel invAdjustmentReasonModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeInvAdjReason, MdmConstants.GetProcInvAdjReason);
            OracleObject recordObject = this.SetParamsInvAdjReason(invAdjustmentReasonModel);
            return await this.GetProcedure2<InvAdjustmentReasonModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ServiceDocumentResult> SetInvAdjReason(InvAdjustmentReasonModel invAdjustmentReasonModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeInvAdjReason, MdmConstants.SetProcInvAdjReason);
            OracleObject recordObject = this.SetParamsInvAdjReason(invAdjustmentReasonModel);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async Task<List<InvStatusTypeModel>> GetInvStatusTypeAsync(InvStatusTypeModel invstatusModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeInvAdjReason, MdmConstants.GetProcInvAdjReason);
            OracleObject recordObject = this.SetParamsInvstatustype(invstatusModel);
            return await this.GetProcedure2<InvStatusTypeModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<List<InvAdjustmentReasonModel>> InvAdjustmentReasonGet()
        {
            this.Connection.Open();
            OracleObject recordObject = this.GetOracleObject(this.GetObjectType("INV_ADJ_REASON_REC"));
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeInvAdjReason, MdmConstants.GetProcInvAdjReason);
            return await this.GetProcedure2<InvAdjustmentReasonModel>(recordObject, packageParameter, null);
        }

        private OracleObject SetParamsInvstatustype(InvStatusTypeModel invstatusModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("INV_ADJ_REASON_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            recordObject["INV_STATUS"] = invstatusModel?.InvStatus;
            recordObject["INV_STATUS_DESC"] = invstatusModel?.InvStatusDesc;
            recordObject["Operation"] = invstatusModel?.Operation;
            return recordObject;
        }

        private OracleObject SetParamsInvAdjReason(InvAdjustmentReasonModel invAdjustmentReasonModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("INV_ADJ_REASON_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            recordObject["REASON"] = invAdjustmentReasonModel?.Reason;
            recordObject["REASON_DESC"] = invAdjustmentReasonModel?.ReasonDesc;
            recordObject["Operation"] = invAdjustmentReasonModel?.Operation;
            return recordObject;
        }
    }
}
