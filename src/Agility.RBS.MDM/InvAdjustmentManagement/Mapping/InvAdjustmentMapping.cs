﻿// <copyright file="InvAdjustmentMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.InvAdjustmentManagement.Mapping
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.RBS.MDM.InvAdjustmentManagement.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class InvAdjustmentMapping : Profile
    {
        public InvAdjustmentMapping()
            : base("InvAdjustmentMapping")
        {
            this.CreateMap<OracleObject, InvAdjustmentReasonModel>()
                .ForMember(m => m.Reason, opt => opt.MapFrom(r => r["REASON"]))
                .ForMember(m => m.ReasonDesc, opt => opt.MapFrom(r => r["REASON_DESC"]))
                .ForMember(m => m.CogsInd, opt => opt.MapFrom(r => r["COGS_IND"]))
                .ForMember(m => m.ProgramPhase, opt => opt.MapFrom(r => r["PROGRAM_PHASE"]))
                .ForMember(m => m.ProgramMessage, opt => opt.MapFrom(r => r["PROGRAM_MESSAGE"]))
                .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_ERROR"]))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "INV_ADJ_REASON"))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));
        }
    }
}
