﻿// <copyright file="InvStatusTypesRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.InvAdjustmentManagement
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.InvAdjustmentManagement.Models;
    using Devart.Data.Oracle;

    public class InvStatusTypesRepository : BaseOraclePackage
    {
        //////public async Task<List<InvStatusTypeModel>> GetInvStatusTypeAsync(InvStatusTypeModel invStatusTypesModel, ServiceDocumentResult serviceDocumentResult)
        //////{
        //////    PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeInvAdjReason, MdmConstants.GetProcInvAdjReason);
        //////    OracleObject recordObject = this.SetParamsInvStatusTypes(invStatusTypesModel);
        //////    return await this.GetProcedure2<InvStatusTypeModel>(recordObject, packageParameter, serviceDocumentResult);
        //////}

        //////private OracleObject SetParamsInvStatusTypes(object invAdjustmentReasonModel)
        //////{
        //////    this.Connection.Open();
        //////    OracleType recordType = this.GetObjectType("INV_ADJ_REASON_REC");
        //////    OracleObject recordObject = this.GetOracleObject(recordType);
        //////    recordObject["REASON"] = invAdjustmentReasonModel?.Reason;
        //////    recordObject["REASON_DESC"] = invAdjustmentReasonModel?.ReasonDesc;
        //////    recordObject["Operation"] = invAdjustmentReasonModel?.Operation;
        //////    return recordObject;
        //////}
    }
}
