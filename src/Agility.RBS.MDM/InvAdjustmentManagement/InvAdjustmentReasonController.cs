﻿// <copyright file="InvAdjustmentReasonController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.InvAdjustmentManagement
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Text;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.InvAdjustmentManagement.Models;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class InvAdjustmentReasonController : Controller
    {
        private readonly ServiceDocument<InvAdjustmentReasonModel> serviceDocument;
        private readonly InvAdjustmentRepository invAdjustmentRepositoryRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly DomainDataRepository domainDataRepository;

        public InvAdjustmentReasonController(
            ServiceDocument<InvAdjustmentReasonModel> serviceDocument,
            InvAdjustmentRepository invAdjustmentRepositoryRepository,
            DomainDataRepository domainDataRepository)
        {
            this.invAdjustmentRepositoryRepository = invAdjustmentRepositoryRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<InvAdjustmentReasonModel>> List()
        {
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<InvAdjustmentReasonModel>> New()
        {
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<InvAdjustmentReasonModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.InvAdjustmentReasonSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<InvAdjustmentReasonModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.InvAdjReasonSave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingInvAdjustmentReason(string reason, string reasonDesc)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "INV_ADJ_REASON";
            nvc["P_COL1"] = reason;
            nvc["P_COL2"] = reasonDesc;
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        private async Task<List<InvAdjustmentReasonModel>> InvAdjustmentReasonSearch()
        {
            try
            {
                var lstInvAdjReason = await this.invAdjustmentRepositoryRepository.GetInvAdjustmentReasonAsync(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstInvAdjReason;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> InvAdjReasonSave()
        {
            this.serviceDocument.Result = await this.invAdjustmentRepositoryRepository.SetInvAdjReason(this.serviceDocument.DataProfile.DataModel);
            return true;
        }
    }
}
