﻿// <copyright file="InvAdjustmentReasonModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.InvAdjustmentManagement.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.Framework.Core.Profile.Entities;

    public class InvAdjustmentReasonModel : ProfileEntity
    {
        public int? Reason { get; set; }

        public string ReasonDesc { get; set; }

        public string CogsInd { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Error { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }
    }
}
