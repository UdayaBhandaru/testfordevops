﻿//-------------------------------------------------------------------------------------------------
// <copyright file="AverageCostAdjustmentController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// AverageCostAdjustmentController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.AverageCostAdjustment
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.InventoryManagement.AverageCostAdjustment.Models;
    using Agility.RBS.MDM.AverageCostAdjustment.Models;
    using Agility.RBS.MDM.AverageCostAdjustment.Repositories;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.DsdMinMax.Models;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    [Route("api/[controller]/[action]")]

    public class AverageCostAdjustmentController : Controller
    {
        private readonly ServiceDocument<AverageCostAdjustmentModel> serviceDocument;
        private readonly AverageCostAdjustmentRepository averageCostAdjustmentRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;

        public AverageCostAdjustmentController(
            ServiceDocument<AverageCostAdjustmentModel> serviceDocument,
              AverageCostAdjustmentRepository averageCostAdjustmentRepository,
              DomainDataRepository domainDataRepository)
        {
            this.averageCostAdjustmentRepository = averageCostAdjustmentRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<AverageCostAdjustmentModel>> List()
        {
            ////await this.LoadDomainData();
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<AverageCostAdjustmentModel>> New()
        {
           //// await this.LoadDomainData();
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<AverageCostAdjustmentModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.AverageCostAdjustmentSearch);
            return this.serviceDocument;
        }

        public async Task<List<AverageCostAdjustmentModel>> GetItemLocations(string item)
        {
            return await this.averageCostAdjustmentRepository.GetAverageCostAdjustment(new AverageCostAdjustmentModel { Item = item }, this.serviceDocumentResult);
        }

        public async Task<ServiceDocument<AverageCostAdjustmentModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.AverageCostAdjustmentSave);
            return this.serviceDocument;
        }

        [HttpGet]
        public ItemCategeoryDetailsDomainModel GetItemAtteibutes(string item)
        {
            ItemCategeoryDetailsDomainModel itemDescModel;
            itemDescModel= this.averageCostAdjustmentRepository.GetItemAttributes(item);
            return itemDescModel;
        }

        private async Task<List<AverageCostAdjustmentModel>> AverageCostAdjustmentSearch()
        {
            try
            {
                var lstUom = await this.averageCostAdjustmentRepository.GetAverageCostAdjustment(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstUom;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> AverageCostAdjustmentSave()
        {
            this.serviceDocument.Result = await this.averageCostAdjustmentRepository.SetAverageCostAdjustment(this.serviceDocument.DataProfile.DataModel);
            return true;
        }

        ////private async Task LoadDomainData()
        ////{
        ////     this.serviceDocument.DomainData.Add("location", this.GetItemLocations("100053104").Result);
        ////}
    }
}