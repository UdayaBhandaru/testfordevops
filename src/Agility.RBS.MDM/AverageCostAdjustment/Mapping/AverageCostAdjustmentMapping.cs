﻿// <copyright file="AverageCostAdjustmentMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.AverageCostAdjustment.Mapping
{
    using System;
    using Agility.RBS.Core;
    using Agility.RBS.InventoryManagement.AverageCostAdjustment.Models;
    using Agility.RBS.MDM.AverageCostAdjustment.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class AverageCostAdjustmentMapping : Profile
    {
        public AverageCostAdjustmentMapping()
            : base("AverageCostAdjustmentMapping")
        {
            this.CreateMap<OracleObject, AverageCostAdjustmentModel>()
               .ForMember(m => m.Item, opt => opt.MapFrom(r => r["ITEM"]))
               .ForMember(m => m.ItemDesc, opt => opt.MapFrom(r => r["ITEM_DESC"]))
               .ForMember(m => m.Category, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["CATEGORY"])))
               .ForMember(m => m.CategoryDesc, opt => opt.MapFrom(r => r["CATEGORY_DESC"]))
               .ForMember(m => m.Loc, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["LOC"])))
               .ForMember(m => m.LocName, opt => opt.MapFrom(r => r["LOC_NAME"]))
               .ForMember(m => m.LocType, opt => opt.MapFrom(r => r["LOC_TYPE"]))
               .ForMember(m => m.LocTypeDesc, opt => opt.MapFrom(r => r["LOC_TYPE_DESC"]))
               .ForMember(m => m.AvCost, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["AV_COST"])))
               .ForMember(m => m.UnitCost, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["UNIT_COST"])))
               .ForMember(m => m.NewAvCost, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["NEW_AV_COST"])))
               .ForMember(m => m.CurrencyCode, opt => opt.MapFrom(r => r["CURRENCY_CODE"]))
               .ForMember(m => m.StockOnHand, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["STOCK_ON_HAND"])))
               .ForMember(m => m.InTransitQty, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["IN_TRANSIT_QTY"])))
               .ForMember(m => m.PackCompIntran, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["PACK_COMP_INTRAN"])))
               .ForMember(m => m.PackCompSoh, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["PACK_COMP_SOH"])))
               .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
               .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, AverageCostAdjustmentLocModel>()
               .ForMember(m => m.Item, opt => opt.MapFrom(r => r["ITEM"]))
               .ForMember(m => m.ItemDesc, opt => opt.MapFrom(r => r["ITEM_DESC "]))
               .ForMember(m => m.Category, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["CATEGORY"])))
               .ForMember(m => m.CategoryDesc, opt => opt.MapFrom(r => r["CATEGORY_DESC"]))
               .ForMember(m => m.Loc, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["LOC"])))
               .ForMember(m => m.LocName, opt => opt.MapFrom(r => r["LOC_NAME"]))
               .ForMember(m => m.LocType, opt => opt.MapFrom(r => r["LOC_TYPE"]))
               .ForMember(m => m.LocTypeDesc, opt => opt.MapFrom(r => r["LOC_TYPE_DESC"]))
               .ForMember(m => m.AvCost, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["AV_COST"])))
               .ForMember(m => m.UnitCost, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["UNIT_COST"])))
               .ForMember(m => m.NewAvCost, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["NEW_AV_COST"])))
               .ForMember(m => m.CurrencyCode, opt => opt.MapFrom(r => r["CURRENCY_CODE"]))
               .ForMember(m => m.StockOnHand, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["STOCK_ON_HAND"])))
               .ForMember(m => m.InTransitQty, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["IN_TRANSIT_QTY"])))
               .ForMember(m => m.PackCompIntran, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["PACK_COMP_INTRAN"])))
               .ForMember(m => m.PackCompSoh, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["PACK_COMP_SOH"])))
               .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
               .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, AvgCostAdjmntItemDetailsModel>()
             .ForMember(m => m.TsfNo, opt => opt.MapFrom(r => r["TSF_NO"]))
             .ForMember(m => m.Item, opt => opt.MapFrom(r => r["ITEM"]))
             .ForMember(m => m.ItemNumberType, opt => opt.MapFrom(r => r["ITEM_NUMBER_TYPE"]))
             .ForMember(m => m.ItemDesc, opt => opt.MapFrom(r => r["ITEM_DESC"]))
             .ForMember(m => m.UnitCost, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["UNIT_COST"])))
             .ForMember(m => m.StockOnHand, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["STOCK_ON_HAND"])))
             .ForMember(m => m.UnitOfTransfer, opt => opt.MapFrom(r => r["UNIT_OF_TRANSFER"]))
             .ForMember(m => m.UnitRetail, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["UNIT_RETAIL"])))
             .ForMember(m => m.TranQty, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["TRAN_QTY"])))
             .ForMember(m => m.ShipQty, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["SHIP_QTY"])))
             .ForMember(m => m.RecvQty, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["RECV_QTY"])))
             .ForMember(m => m.Program_Message, opt => opt.MapFrom(r => r["PROGRAM_MESSAGE"]))
             .ForMember(m => m.Program_Phase, opt => opt.MapFrom(r => r["PROGRAM_PHASE"]))
             .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_ERROR"]));
        }
    }
}
