﻿// <copyright file="AverageCostAdjustmentRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// AverageCostAdjustmentRepository
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.AverageCostAdjustment.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.InventoryManagement.AverageCostAdjustment.Models;
    using Agility.RBS.MDM.AverageCostAdjustment.Models;
    using Agility.RBS.MDM.Common;
    using AutoMapper;
    using Devart.Data.Oracle;
    using Microsoft.AspNetCore.Authorization;

    public class ItemAttributesRepository : BaseOraclePackage
    {
        public ItemAttributesRepository()
        {
            this.PackageName = MdmConstants.GetPackageItemAttributes;
        }

        public string GetItemAttributes(string item)
        {
            try
            {
                if (string.IsNullOrEmpty(item))
                {
                    throw new ArgumentNullException("item", "Item can not be empty");
                }

                this.Connection.Open();
                OracleParameterCollection parameters = this.Parameters;
                parameters.Clear();
                OracleParameter oracleParameter;
                oracleParameter = new OracleParameter { ParameterName = "p_item", OracleDbType = OracleDbType.VarChar, Direction = ParameterDirection.Input, Value = item };
                parameters.Add(oracleParameter);
                oracleParameter = new OracleParameter("p_catid", Devart.Data.Oracle.OracleDbType.Number)
                {
                    Direction = ParameterDirection.Output
                };
                parameters.Add(oracleParameter);

                oracleParameter = new OracleParameter("p_catdesc", Devart.Data.Oracle.OracleDbType.VarChar)
                {
                    Direction = ParameterDirection.Output
                };
                parameters.Add(oracleParameter);

                this.ExecuteProcedure("getitemcat", parameters);
                if (this.Parameters["p_catid"].Value == System.DBNull.Value)
                {
                    return null;
                }

                return this.Parameters["p_catid"].Value.ToString();
            }
            catch (Exception e)
            {
                return null;
            }
            finally
            {
                this.Connection.Close();
            }
        }
    }
}