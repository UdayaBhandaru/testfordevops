﻿// <copyright file="AverageCostAdjustmentRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// AverageCostAdjustmentRepository
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.AverageCostAdjustment.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.InventoryManagement.AverageCostAdjustment.Models;
    using Agility.RBS.MDM.AverageCostAdjustment.Models;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.DsdMinMax.Models;
    using AutoMapper;
    using Devart.Data.Oracle;
    using Microsoft.AspNetCore.Authorization;

    public class AverageCostAdjustmentRepository : BaseOraclePackage
    {
        public AverageCostAdjustmentRepository()
        {
            this.PackageName = MdmConstants.GetFinancePackage;
        }

        public async Task<List<AverageCostAdjustmentModel>> GetAverageCostAdjustment(AverageCostAdjustmentModel averageCostAdjustmentModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeAvgCostAdjmnt, MdmConstants.GetProcAvgCostAdjmnt);
            OracleObject recordObject = this.SetParamsAverageCostAdjustmentSearch(averageCostAdjustmentModel);
            return await this.GetProcedure2<AverageCostAdjustmentModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public virtual void SetSearchParamsAverageCostAdjustment(AverageCostAdjustmentModel averageCostAdjustmentModel, OracleObject recordObject)
        {
            recordObject["ITEM"] = averageCostAdjustmentModel.Item;
        }

        public async Task<ServiceDocumentResult> SetAverageCostAdjustment(AverageCostAdjustmentModel averageCostAdjustmentModel)
        {
            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();

            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeAvgCostAdjmnt, MdmConstants.SetProcAvgCostAdjmnt);
            OracleParameter parameter;
            parameter = new OracleParameter("P_USER", OracleDbType.VarChar)
            {
                Direction = ParameterDirection.Input,
                Value = UserProfileHelper.GetInMemoryUser().UserName
            };
            parameters.Add(parameter);
            parameter = new OracleParameter("RESULT", Devart.Data.Oracle.OracleDbType.Boolean)
            {
                Direction = System.Data.ParameterDirection.ReturnValue
            };
            parameters.Add(parameter);
            parameter = new OracleParameter(packageParameter.ObjectTypeTable, Devart.Data.Oracle.OracleDbType.Table)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                ObjectTypeName = packageParameter.ObjectTypeName,
                Value = this.SetParamsNewCost(averageCostAdjustmentModel)
            };
            parameters.Add(parameter);
            return await this.SetProcedure(null, packageParameter, parameters);
        }

        public virtual OracleTable SetParamsNewCost(AverageCostAdjustmentModel averageCostAdjustmentModel)
        {
            this.Connection.Open();
            OracleType itemComTableType = this.GetObjectType(MdmConstants.ObjTypeAvgCostAdjmnt);
            OracleType recordType = this.GetObjectType(MdmConstants.RecTypeAvgCostAdjmnt);
            OracleTable pItemCompanyTab = new OracleTable(itemComTableType);

            foreach (AverageCostAdjustmentLocModel avgCostLocRecord in averageCostAdjustmentModel.AverageCostAdjustmentLocList)
            {
                OracleObject recordObject = new OracleObject(recordType);
                recordObject[recordType.Attributes["ITEM"]] = averageCostAdjustmentModel.Item;
                recordObject[recordType.Attributes["ITEM_DESC"]] = string.Empty;
                recordObject[recordType.Attributes["CATEGORY"]] = averageCostAdjustmentModel.Category;
                recordObject[recordType.Attributes["CATEGORY_DESC"]] = string.Empty;
                recordObject[recordType.Attributes["LOC"]] = avgCostLocRecord.Loc;
                recordObject[recordType.Attributes["LOC_NAME"]] = string.Empty;
                recordObject[recordType.Attributes["LOC_TYPE"]] = avgCostLocRecord.LocType;
                recordObject[recordType.Attributes["LOC_TYPE_DESC"]] = string.Empty;
                recordObject[recordType.Attributes["AV_COST"]] = avgCostLocRecord.AvCost;
                recordObject[recordType.Attributes["UNIT_COST"]] = avgCostLocRecord.UnitCost;
                recordObject[recordType.Attributes["NEW_AV_COST"]] = avgCostLocRecord.NewAvCost;
                recordObject[recordType.Attributes["CURRENCY_CODE"]] = avgCostLocRecord.CurrencyCode;
                recordObject[recordType.Attributes["STOCK_ON_HAND"]] = avgCostLocRecord.StockOnHand;
                recordObject[recordType.Attributes["IN_TRANSIT_QTY"]] = avgCostLocRecord.InTransitQty;
                recordObject[recordType.Attributes["PACK_COMP_INTRAN"]] = avgCostLocRecord.PackCompIntran;
                recordObject[recordType.Attributes["PACK_COMP_SOH"]] = avgCostLocRecord.PackCompSoh;
                recordObject[recordType.Attributes["Operation"]] = averageCostAdjustmentModel.Operation;
                pItemCompanyTab.Add(recordObject);
            }

            return pItemCompanyTab;
        }

        public ItemCategeoryDetailsDomainModel GetItemAttributes(string item)
        {
            ItemCategeoryDetailsDomainModel itemDescModel;
            itemDescModel = new ItemCategeoryDetailsDomainModel();

            try
            {
                if (string.IsNullOrEmpty(item))
                {
                    throw new ArgumentNullException("item", "Item can not be empty");
                }

                this.Connection.Open();
                OracleParameterCollection parameters = this.Parameters;
                parameters.Clear();
                OracleParameter oracleParameter;
                oracleParameter = new OracleParameter { ParameterName = "p_item", OracleDbType = OracleDbType.VarChar, Direction = ParameterDirection.Input, Value = item };
                parameters.Add(oracleParameter);
                oracleParameter = new OracleParameter("p_catid", Devart.Data.Oracle.OracleDbType.Number)
                {
                    Direction = ParameterDirection.Output
                };
                parameters.Add(oracleParameter);

                oracleParameter = new OracleParameter("p_catdesc", Devart.Data.Oracle.OracleDbType.VarChar)
                {
                    Direction = ParameterDirection.Output
                };
                parameters.Add(oracleParameter);

                oracleParameter = new OracleParameter("RESULT", Devart.Data.Oracle.OracleDbType.Boolean)
                {
                    Direction = ParameterDirection.ReturnValue
                };
                parameters.Add(oracleParameter);

                this.ExecuteProcedure("GETITEMCAT", parameters);
                if (this.Parameters["p_catdesc"].Value == System.DBNull.Value)
                {
                    return null;
                }

                itemDescModel.CategoryDesc = this.Parameters["p_catdesc"].Value.ToString();
                itemDescModel.Category = Convert.ToInt32( this.Parameters["p_catid"].Value.ToString());

                return itemDescModel;
            }
            catch (Exception e)
            {
                return null;
            }
            finally
            {
                this.Connection.Close();
            }
        }

        private OracleObject SetParamsAverageCostAdjustmentSearch(AverageCostAdjustmentModel averageCostAdjustmentModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("avg_cost_adj_rec");
            OracleObject recordObject = this.GetOracleObject(recordType);
            this.SetSearchParamsAverageCostAdjustment(averageCostAdjustmentModel, recordObject);
            return recordObject;
        }
    }
}