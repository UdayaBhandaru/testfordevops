﻿//-------------------------------------------------------------------------------------------------
// <copyright file="AverageCostAdjustmentLocModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.AverageCostAdjustment.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class AverageCostAdjustmentLocModel : ProfileEntity
    {
        public string Item { get; set; }

        public string ItemDesc { get; set; }

        public int? Category { get; set; }

        public string CategoryDesc { get; set; }

        public int? Loc { get; set; }

        public string LocName { get; set; }

        public string LocType { get; set; }

        public string LocTypeDesc { get; set; }

        public int? AvCost { get; set; }

        public int? UnitCost { get; set; }

        public int? NewAvCost { get; set; }

        public string CurrencyCode { get; set; }

        public int? StockOnHand { get; set; }

        public int? InTransitQty { get; set; }

        public int? PackCompIntran { get; set; }

        public int? PackCompSoh { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Error { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }
    }
}
