﻿// <copyright file="AvgCostAdjmntItemDetailsModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// Item List Domain
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.InventoryManagement.AverageCostAdjustment.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class AvgCostAdjmntItemDetailsModel
    {
        public long? TsfNo { get; set; }

        public string Item { get; set; }

        public string ItemNumberType { get; set; }

        public string ItemDesc { get; set; }

        public decimal? UnitCost { get; set; }

        public decimal? StockOnHand { get; set; }

        public string UnitOfTransfer { get; set; }

        public decimal? UnitRetail { get; set; }

        public decimal? TranQty { get; set; }

        public decimal? ShipQty { get; set; }

        public decimal? RecvQty { get; set; }

        public string Program_Phase { get; set; }

        public string Program_Message { get; set; }

        public string Error { get; set; }
    }
}
