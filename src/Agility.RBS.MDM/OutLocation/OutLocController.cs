﻿//-------------------------------------------------------------------------------------------------
// <copyright file="OutLocController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// OutLocationController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.OutLocation
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.OrganizationHierarchy;
    using Agility.RBS.MDM.OrganizationHierarchy.Models;
    using Agility.RBS.MDM.OutLocation.Models;
    using Agility.RBS.MDM.OutLocation.Repositories;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    [Route("api/[controller]/[action]")]

    public class OutlocController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocument<OutlocModel> serviceDocument;
        private readonly OutlocRepository outLocationRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly OrganizationHierarchyRepository organizationHierarchyRepository;

        public OutlocController(
            ServiceDocument<OutlocModel> serviceDocument,
           OutlocRepository outLocationRepository,
            OrganizationHierarchyRepository organizationHierarchyRepository,
            DomainDataRepository domainDataRepository)
        {
            this.outLocationRepository = outLocationRepository;
            this.domainDataRepository = domainDataRepository;
            this.organizationHierarchyRepository = organizationHierarchyRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<OutlocModel>> List()
        {
            await this.OutLocationDomainData();
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<OutlocModel>> New()
        {
            await this.OutLocationDomainData();
            this.serviceDocument.LocalizationData.AddData("OutLocation");
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<OutlocModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.OutLocationSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<OutlocModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.OutLocationSave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingOutLocation(string outLocationId)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "OUTLOC";
            nvc["P_COL1"] = outLocationId;
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        private async Task<List<OutlocModel>> OutLocationSearch()
        {
            try
            {
                var lstUomClass = await this.outLocationRepository.GetOutLocation(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstUomClass;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> OutLocationSave()
        {
            this.serviceDocument.Result = await this.outLocationRepository.SetOutLocation(this.serviceDocument.DataProfile.DataModel);
            return true;
        }

        private async Task OutLocationDomainData()
        {
            RegionModel regionModel = new RegionModel();
            var regions = await this.organizationHierarchyRepository.GetOrgRegion(regionModel, this.serviceDocumentResult);
            this.serviceDocument.DomainData.Add("Regions", regions);
            this.serviceDocument.DomainData.Add("state", this.domainDataRepository.StateDomainGet().Result);
            this.serviceDocument.DomainData.Add("country", this.domainDataRepository.CountryDomainGet().Result);
            this.serviceDocument.DomainData.Add("currency", this.domainDataRepository.CurrencyDomainGet().Result);
            this.serviceDocument.DomainData.Add("status", this.domainDataRepository.DomainDetailData(MdmConstants.DomainHdrStatus).Result);
            this.serviceDocument.DomainData.Add("location", this.domainDataRepository.LocationDomainGet().Result);
            this.serviceDocument.DomainData.Add("locType", this.domainDataRepository.LocationTypeDomainGet().Result);
            this.serviceDocument.DomainData.Add("locationsType", this.domainDataRepository.DomainDetailData(MdmConstants.DomainHdrLocType).Result);
        }
    }
}
