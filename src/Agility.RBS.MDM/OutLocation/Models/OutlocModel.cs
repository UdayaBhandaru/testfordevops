﻿// <copyright file="OutlocModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// CostChangeReason
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.OutLocation.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class OutlocModel : ProfileEntity
    {
        public string OutLocType { get; set; }

        public string OutLocTypeDesc { get; set; }

        public string OutLocId { get; set; }

        public string OutLocDesc { get; set; }

        public string OutLocCurrency { get; set; }

        public string OutLocAdd1 { get; set; }

        public string OutLocAdd2 { get; set; }

        public string OutLocCity { get; set; }

        public string OutLocState { get; set; }

        public string OutLocCountryId { get; set; }

        public string OutLocCountryName { get; set; }

        public string OutLocPost { get; set; }

        public int? OutLocVatRegion { get; set; }

        public string ContactName { get; set; }

        public string ContactPhone { get; set; }

        public string ContactFax { get; set; }

        public string ContactTelex { get; set; }

        public string ContactEmail { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Error { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }
    }
}
