﻿// <copyright file="OutlocMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.StoreFormat.Mapping
{
    using System;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.OutLocation.Models;
    using Agility.RBS.MDM.StoreFormat.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class OutlocMapping : Profile
    {
        public OutlocMapping()
            : base("OutlocMapping")
        {
            this.CreateMap<OracleObject, OutlocModel>()
              .ForMember(m => m.OutLocType, opt => opt.MapFrom(r => r["outloc_type"]))
              .ForMember(m => m.OutLocTypeDesc, opt => opt.MapFrom(r => r["OUT_LOC_TYPE_DESC"]))
              .ForMember(m => m.OutLocId, opt => opt.MapFrom(r => r["OUTLOC_ID"]))
              .ForMember(m => m.OutLocDesc, opt => opt.MapFrom(r => r["OUTLOC_DESC"]))
              .ForMember(m => m.OutLocCurrency, opt => opt.MapFrom(r => r["OUTLOC_CURRENCY"]))
              .ForMember(m => m.OutLocAdd1, opt => opt.MapFrom(r => r["OUTLOC_ADD1"]))
              .ForMember(m => m.OutLocAdd2, opt => opt.MapFrom(r => r["OUTLOC_ADD2"]))
              .ForMember(m => m.OutLocCity, opt => opt.MapFrom(r => r["OUTLOC_CITY"]))
              .ForMember(m => m.OutLocState, opt => opt.MapFrom(r => r["OUTLOC_STATE"]))
              .ForMember(m => m.OutLocCountryId, opt => opt.MapFrom(r => r["OUTLOC_COUNTRY_ID"]))
              .ForMember(m => m.OutLocCountryName, opt => opt.MapFrom(r => r["OUTLOC_COUNTRY_NAME"]))
              .ForMember(m => m.OutLocPost, opt => opt.MapFrom(r => r["OUTLOC_POST"]))
                .ForMember(m => m.OutLocVatRegion, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["OUTLOC_VAT_REGION"])))
              .ForMember(m => m.ContactName, opt => opt.MapFrom(r => r["CONTACT_NAME"]))
              .ForMember(m => m.ContactPhone, opt => opt.MapFrom(r => r["CONTACT_PHONE"]))
              .ForMember(m => m.ContactFax, opt => opt.MapFrom(r => r["CONTACT_FAX"]))
              .ForMember(m => m.ContactTelex, opt => opt.MapFrom(r => r["CONTACT_TELEX"]))
              .ForMember(m => m.ContactEmail, opt => opt.MapFrom(r => r["CONTACT_EMAIL"]))
               .ForMember(m => m.TableName, opt => opt.MapFrom(r => "OUTLOC"))
               .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));
        }
    }
}
