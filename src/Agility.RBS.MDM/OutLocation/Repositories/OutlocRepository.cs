﻿// <copyright file="OutlocRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// CostZoneRepository
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.OutLocation.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.OutLocation.Models;
    using AutoMapper;
    using Devart.Data.Oracle;
    using Microsoft.AspNetCore.Authorization;

    public class OutlocRepository : BaseOraclePackage
    {
        public OutlocRepository()
        {
            this.PackageName = MdmConstants.GetOutLocationPackage;
        }

        public async Task<List<OutlocModel>> GetOutLocation(OutlocModel outLocationModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeOutLocation, MdmConstants.GetProcOutLocation);
            OracleObject recordObject = this.SetParamsOutLocation(outLocationModel, true);
            return await this.GetProcedure2<OutlocModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ServiceDocumentResult> SetOutLocation(OutlocModel outLocationModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeOutLocation, MdmConstants.SetProcOutLocation);
            OracleObject recordObject = this.SetParamsOutLocation(outLocationModel, false);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        private OracleObject SetParamsOutLocation(OutlocModel outLocationModel, bool isSearch)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(MdmConstants.RecordTypeOutLocation, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);

            if (!isSearch)
            {
                recordObject[recordType.Attributes["Operation"]] = outLocationModel.Operation;
            }

            recordObject[recordType.Attributes["outloc_type"]] = outLocationModel.OutLocType;
            recordObject[recordType.Attributes["out_loc_type_desc"]] = outLocationModel.OutLocTypeDesc;
            recordObject[recordType.Attributes["outloc_id"]] = outLocationModel.OutLocId;
            recordObject[recordType.Attributes["outloc_desc"]] = outLocationModel.OutLocDesc;
            recordObject[recordType.Attributes["outloc_currency"]] = outLocationModel.OutLocCurrency;
            recordObject[recordType.Attributes["outloc_add1"]] = outLocationModel.OutLocAdd1;
            recordObject[recordType.Attributes["outloc_add2"]] = outLocationModel.OutLocAdd2;
            recordObject[recordType.Attributes["outloc_city"]] = outLocationModel.OutLocCity;
            recordObject[recordType.Attributes["outloc_state"]] = outLocationModel.OutLocState;
            recordObject[recordType.Attributes["outloc_country_id"]] = outLocationModel.OutLocCountryId;
            recordObject[recordType.Attributes["outloc_country_name"]] = outLocationModel.OutLocCountryName;
            recordObject[recordType.Attributes["outloc_post"]] = outLocationModel.OutLocPost;
            recordObject[recordType.Attributes["outloc_vat_region"]] = outLocationModel.OutLocVatRegion;
            recordObject[recordType.Attributes["contact_name"]] = outLocationModel.ContactName;
            recordObject[recordType.Attributes["contact_phone"]] = outLocationModel.ContactPhone;
            recordObject[recordType.Attributes["contact_fax"]] = outLocationModel.ContactFax;
            recordObject[recordType.Attributes["contact_telex"]] = outLocationModel.ContactTelex;
            recordObject[recordType.Attributes["contact_email"]] = outLocationModel.ContactEmail;

            return recordObject;
        }
    }
}