﻿//-------------------------------------------------------------------------------------------------
// <copyright file="FavouriteController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// ClassController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Favourite
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Favourite.Models;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class FavouriteController : Controller
    {
        private readonly ServiceDocument<FavouriteModel> serviceDocument;
        private readonly FavouriteRepository favouriteRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;

        public FavouriteController(ServiceDocument<FavouriteModel> serviceDocument, FavouriteRepository favouriteRepository, DomainDataRepository domainDataRepository)
        {
            this.favouriteRepository = favouriteRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        [HttpPost]
        public async Task<ServiceDocument<FavouriteModel>> List()
        {
           await this.serviceDocument.ToListAsync(this.FavouriteSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<FavouriteModel>> New()
        {
            return this.serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<FavouriteModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.FavouriteSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<FavouriteModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.FavouriteSave);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<FavouriteModel>> Remove()
        {
            await this.serviceDocument.SaveAsync(this.FavouriteSave);
            return this.serviceDocument;
        }

        private async Task<List<FavouriteModel>> FavouriteSearch()
        {
            try
            {
                var favourites = await this.favouriteRepository.GetFavourite(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return favourites;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> FavouriteSave()
        {
            this.serviceDocument.Result = await this.favouriteRepository.SetFavourite(this.serviceDocument.DataProfile.DataModel);
            return true;
        }
    }
}
