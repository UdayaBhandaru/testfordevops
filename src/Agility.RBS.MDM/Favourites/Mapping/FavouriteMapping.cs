﻿// <copyright file="FavouriteMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Favourite.Mapping
{
    using System;
    using Agility.RBS.MDM.Favourite.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class FavouriteMapping : Profile
    {
        public FavouriteMapping()
            : base("FavouriteMapping")
        {
            this.CreateMap<OracleObject, FavouriteModel>()
                .ForMember(m => m.UserId, opt => opt.MapFrom(r => r["USER_ID"]))
                .ForMember(m => m.FavouritePagePath, opt => opt.MapFrom(r => r["FAVOURITE_PAGE_URL"]))
                .ForMember(m => m.FavouritePage, opt => opt.MapFrom(r => r["FAVOURITE_PAGE"]))
                .ForMember(m => m.CreatedBy, opt => opt.MapFrom(r => r["CREATED_BY"]));
        }
    }
}
