﻿// <copyright file="FavouriteModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Favourite.Models
{
    using System;
    using Agility.Framework.Core.Profile.Entities;

    public class FavouriteModel : ProfileEntity
    {
        public string UserId { get; set; }

        public string FavouritePagePath { get; set; }

        public string FavouritePage { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }

        public string Error { get; set; }
    }
}