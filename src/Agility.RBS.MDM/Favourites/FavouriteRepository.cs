﻿// <copyright file="FavouriteRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Favourite
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.Framework.Web.Security.Middleware;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Favourite.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class FavouriteRepository : BaseOraclePackage
    {
        public FavouriteRepository()
        {
            this.PackageName = MdmConstants.GetFavouritePackage;
        }

        public async Task<List<FavouriteModel>> GetFavourite(FavouriteModel favouriteModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeFavourite, MdmConstants.GetProcFavourite);
            OracleObject recordObject = this.SetParamsFavourite(favouriteModel, true);
            return await this.GetProcedure2<FavouriteModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ServiceDocumentResult> SetFavourite(FavouriteModel favouriteModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeFavourite, MdmConstants.SetProcFavourite);
            OracleObject recordObject = this.SetParamsFavourite(favouriteModel, false);
            ServiceDocumentResult sRes = await this.SetProcedure(recordObject, packageParameter);
            return sRes;
        }

        private static bool IsGenericSearch(FavouriteModel favouriteModel)
        {
            string userId = favouriteModel.UserId;
            if (!string.IsNullOrEmpty(userId))
            {
                return true;
            }

            return false;
        }

        private OracleObject SetParamsFavourite(FavouriteModel favouriteModel, bool isSearch)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType("USER_FAV_MENU_REC", this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["USER_ID"]] = UserProfileHelper.GetInMemoryUserId();
            if (isSearch)
            {
                if (IsGenericSearch(favouriteModel))
                {
                    recordObject[recordType.Attributes["USER_ID"]] = "-1";
                }
            }
            else
            {
                recordObject[recordType.Attributes["Operation"]] = favouriteModel.Operation;
                recordObject[recordType.Attributes["CREATED_BY"]] = UserProfileHelper.GetInMemoryUserId();
                recordObject[recordType.Attributes["LAST_UPDATED_BY"]] = UserProfileHelper.GetInMemoryUserId();
            }

            recordObject[recordType.Attributes["USER_ID"]] = UserProfileHelper.GetInMemoryUserId();
            recordObject[recordType.Attributes["FAVOURITE_PAGE_URL"]] = favouriteModel.FavouritePagePath;
            recordObject[recordType.Attributes["FAVOURITE_PAGE"]] = favouriteModel.FavouritePage;

            return recordObject;
        }
    }
}