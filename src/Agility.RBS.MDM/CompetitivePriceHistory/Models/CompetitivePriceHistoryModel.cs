﻿// <copyright file="CompetitivePriceHistoryModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.CompetitivePriceHistory.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.Framework.Core.Profile.Entities;

    public class CompetitivePriceHistoryModel : ProfileEntity
    {
        public string Item { get; set; }

        public string RefItem { get; set; }

        public int? CompStore { get; set; }

        public string CompStoreDesc { get; set; }

        public DateTime? RecDate { get; set; }

        public int? CompRetail { get; set; }

        public string CompRetailType { get; set; }

        public int? MultiUnits { get; set; }

        public int? MultiUnitRetail { get; set; }

        public DateTime? PromStartDate { get; set; }

        public DateTime? PromEndDate { get; set; }

        public string OfferType { get; set; }

        public DateTime? PostDate { get; set; }

        public string RpmFull { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Error { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }
    }
}
