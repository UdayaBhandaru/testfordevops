﻿// <copyright file="CompetitivePriceHistoryMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.CompetitivePriceHistory.Mapping
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.CompetitivePriceHistory.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class CompetitivePriceHistoryMapping : Profile
    {
        public CompetitivePriceHistoryMapping()
            : base("CompetitivePriceHistoryMapping")
        {
            this.CreateMap<OracleObject, CompetitivePriceHistoryModel>()
                .ForMember(m => m.Item, opt => opt.MapFrom(r => r["ITEM"]))
                .ForMember(m => m.RefItem, opt => opt.MapFrom(r => r["REF_ITEM"]))
                .ForMember(m => m.CompStore, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["COMP_STORE"])))
                .ForMember(m => m.CompStoreDesc, opt => opt.MapFrom(r => r["COMP_STORE_DESC"]))
                .ForMember(m => m.RecDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["REC_DATE"])))
                .ForMember(m => m.CompRetail, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["COMP_RETAIL"])))
                .ForMember(m => m.CompRetailType, opt => opt.MapFrom(r => r["COMP_RETAIL_TYPE"]))
                .ForMember(m => m.MultiUnits, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["MULTI_UNITS"])))
                .ForMember(m => m.MultiUnitRetail, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["MULTI_UNIT_RETAIL"])))
                .ForMember(m => m.PromStartDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["PROM_START_DATE"])))
                .ForMember(m => m.PromEndDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["PROM_END_DATE"])))
                .ForMember(m => m.OfferType, opt => opt.MapFrom(r => r["OFFER_TYPE"]))
                .ForMember(m => m.PostDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["POST_DATE"])))
                .ForMember(m => m.RpmFull, opt => opt.MapFrom(r => r["RPM_PULL"]))
                .ForMember(m => m.ProgramPhase, opt => opt.MapFrom(r => r["PROGRAM_PHASE"]))
                .ForMember(m => m.ProgramMessage, opt => opt.MapFrom(r => r["PROGRAM_MESSAGE"]))
                .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_ERROR"]))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "CompetitivePriceHistory"))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));
        }
    }
}
