﻿// <copyright file="CompetitivePriceHistoryRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.CompetitivePriceHistory
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.CompetitivePriceHistory.Models;
    using Devart.Data.Oracle;

    public class CompetitivePriceHistoryRepository : BaseOraclePackage
    {
        public CompetitivePriceHistoryRepository()
        {
            this.PackageName = MdmConstants.GetCompetitorsPackage;
        }

        public async Task<List<CompetitivePriceHistoryModel>> GetCompetitivePriceHistoryAsync(CompetitivePriceHistoryModel dataModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeCompetitivePriceHistory, MdmConstants.GetProcCompetitivePriceHistory);
            OracleObject recordObject = this.SetParamsCompetitivePriceHistory(dataModel);
            return await this.GetProcedure2<CompetitivePriceHistoryModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ServiceDocumentResult> SetCompetitivePriceHistory(CompetitivePriceHistoryModel dataModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeCompetitivePriceHistory, MdmConstants.SetProcCompetitivePriceHistory);
            OracleObject recordObject = this.SetParamsCompetitivePriceHistory(dataModel);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        private OracleObject SetParamsCompetitivePriceHistory(CompetitivePriceHistoryModel dataModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("COMP_PRICE_HIST_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            recordObject["ITEM"] = dataModel.Item;
            recordObject["REF_ITEM"] = dataModel.RefItem;
            recordObject["COMP_STORE"] = dataModel.CompStore;
            recordObject["COMP_STORE_DESC"] = dataModel.CompStore;
            recordObject["REC_DATE"] = dataModel.RecDate;
            recordObject["COMP_RETAIL"] = dataModel.CompRetail;
            recordObject["COMP_RETAIL_TYPE"] = dataModel.CompRetailType;
            recordObject["MULTI_UNITS"] = dataModel.MultiUnits;
            recordObject["MULTI_UNIT_RETAIL"] = dataModel.MultiUnitRetail;
            recordObject["PROM_START_DATE"] = dataModel.PromStartDate;
            recordObject["PROM_END_DATE"] = dataModel.PromEndDate;
            recordObject["OFFER_TYPE"] = dataModel.OfferType;
            recordObject["POST_DATE"] = dataModel.PostDate;
            recordObject["RPM_PULL"] = dataModel.RpmFull;
            recordObject["Operation"] = dataModel.Operation;
            return recordObject;
        }
    }
}
