﻿// <copyright file="CompetitivePriceHistoryController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.CompetitivePriceHistory
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Text;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.CompetitivePriceHistory.Models;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

   public class CompetitivePriceHistoryController : Controller
    {
        private readonly ServiceDocument<CompetitivePriceHistoryModel> serviceDocument;
        private readonly CompetitivePriceHistoryRepository competitivePriceHistoryRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly DomainDataRepository domainDataRepository;

        public CompetitivePriceHistoryController(
            ServiceDocument<CompetitivePriceHistoryModel> serviceDocument,
            CompetitivePriceHistoryRepository competitivePriceHistoryRepository,
            DomainDataRepository domainDataRepository)
        {
            this.competitivePriceHistoryRepository = competitivePriceHistoryRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<CompetitivePriceHistoryModel>> List()
        {
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<CompetitivePriceHistoryModel>> New()
        {
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<CompetitivePriceHistoryModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.CompetitivePriceHistorySearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<CompetitivePriceHistoryModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.CompetitivePriceHistorySave);
            return this.serviceDocument;
        }

        public async ValueTask<bool> IsExistingCompetitors(string competitor, string compName)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "COMPETITOR_MST";
            nvc["P_COL1"] = competitor;
            nvc["P_COL2"] = compName;
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        private async Task<List<CompetitivePriceHistoryModel>> CompetitivePriceHistorySearch()
        {
            try
            {
                var lstCompetitivePriceHistory = await this.competitivePriceHistoryRepository.GetCompetitivePriceHistoryAsync(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstCompetitivePriceHistory;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> CompetitivePriceHistorySave()
        {
            this.serviceDocument.Result = await this.competitivePriceHistoryRepository.SetCompetitivePriceHistory(this.serviceDocument.DataProfile.DataModel);
            return true;
        }
    }
}
