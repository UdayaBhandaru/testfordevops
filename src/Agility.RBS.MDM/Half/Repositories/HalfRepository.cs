﻿// <copyright file="HalfRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// CostZoneRepository
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Half.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Half.Models;
    using AutoMapper;
    using Devart.Data.Oracle;
    using Microsoft.AspNetCore.Authorization;

    public class HalfRepository : BaseOraclePackage
    {
        public HalfRepository()
        {
            this.PackageName = MdmConstants.GetHalfPackage;
        }

        public async Task<List<HalfModel>> GetHalf(HalfModel halfModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeHalf, MdmConstants.GetProcHalf);
            OracleObject recordObject = this.SetParamsHalf(halfModel, true);
            return await this.GetProcedure2<HalfModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ServiceDocumentResult> SetHalf(HalfModel halfModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeHalf, MdmConstants.SetProcHalf);
            OracleObject recordObject = this.SetParamsHalf(halfModel, false);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        private OracleObject SetParamsHalf(HalfModel halfModel, bool isSearch)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(MdmConstants.RecordTypeHalf, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            if (!isSearch)
            {
                recordObject[recordType.Attributes["Operation"]] = halfModel.Operation;
            }

            recordObject[recordType.Attributes["HALF_NO"]] = halfModel.HalfNo;
            recordObject[recordType.Attributes["HALF_NAME"]] = halfModel.HalfName;
            recordObject[recordType.Attributes["HALF_DATE"]] = halfModel.HalfDate;
            return recordObject;
        }
    }
}