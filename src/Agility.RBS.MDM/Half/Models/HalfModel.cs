﻿// <copyright file="HalfModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// CostChangeReason
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Half.Models
{
    using System;
    using Agility.Framework.Core.Profile.Entities;

    public class HalfModel : ProfileEntity
    {
        public int? HalfNo { get; set; }

        public string HalfName { get; set; }

        public string HalfDate { get; set; }

        public string ProgramMessage { get; set; }

        public string Error { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }
    }
}
