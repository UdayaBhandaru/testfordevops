﻿//-------------------------------------------------------------------------------------------------
// <copyright file="HalfController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// HalfController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Half
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Half.Models;
    using Agility.RBS.MDM.Half.Repositories;
    using Agility.RBS.MDM.Location.Models;
    using Agility.RBS.MDM.Location.Repositories;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    [Route("api/[controller]/[action]")]

    public class HalfController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocument<HalfModel> serviceDocument;
        private readonly HalfRepository halfRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;

        public HalfController(ServiceDocument<HalfModel> serviceDocument, HalfRepository halfRepository, DomainDataRepository domainDataRepository)
        {
            this.halfRepository = halfRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<HalfModel>> List()
        {
            this.serviceDocument.DomainData.Add("status", this.domainDataRepository.DomainDetailData(MdmConstants.DomainHdrStatus).Result);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<HalfModel>> New()
        {
            this.serviceDocument.DomainData.Add("status", this.domainDataRepository.DomainDetailData(MdmConstants.DomainHdrStatus).Result);
            this.serviceDocument.LocalizationData.AddData("Half");
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<HalfModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.HalfSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<HalfModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.HalfSave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingHalf(int halfNo)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "Half";
            nvc["P_COL1"] = halfNo.ToString();
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        private async Task<List<HalfModel>> HalfSearch()
        {
            try
            {
                var lstUomClass = await this.halfRepository.GetHalf(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstUomClass;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> HalfSave()
        {
            this.serviceDocument.Result = await this.halfRepository.SetHalf(this.serviceDocument.DataProfile.DataModel);
            return true;
        }
    }
}
