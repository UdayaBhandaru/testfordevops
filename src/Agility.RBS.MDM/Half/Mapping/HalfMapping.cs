﻿// <copyright file="HalfMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Half.Mapping
{
    using System;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.Half.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class HalfMapping : Profile
    {
        public HalfMapping()
            : base("HalfMapping")
        {
            this.CreateMap<OracleObject, HalfModel>()
              .ForMember(m => m.HalfNo, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["HALF_NO"])))
              .ForMember(m => m.HalfName, opt => opt.MapFrom(r => r["HALF_NAME"]))
              .ForMember(m => m.HalfDate, opt => opt.MapFrom(r => r["HALF_DATE"]))
              .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
              .ForMember(m => m.TableName, opt => opt.MapFrom(r => "HALF"))
              .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));
        }
    }
}
