// <copyright file="WrapperRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Common
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.MerchantHierarchy.Models;
    using Agility.RBS.MDM.OrganizationHierarchy.Models;
    using Agility.RBS.MDM.UtilityDomain.Models;
    using AutoMapper;
    using Devart.Data.Oracle;
    using Microsoft.Extensions.Caching.Distributed;

    public class WrapperRepository : BaseOraclePackage
    {
        public WrapperRepository()
        {
            this.PackageName = MdmConstants.GetWrapperPackage;
        }

        public async Task<ServiceDocumentResult> SetCompany(CompanyModel companyModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeCompany, MdmConstants.SetProcCompany);
            OracleObject recordObject = this.SetParamsCompany(companyModel);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async Task<ServiceDocumentResult> SetDivision(DivisionModel divisionModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDivision, MdmConstants.SetProcDivision);
            OracleObject recordObject = this.SetParamsDivision(divisionModel);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async Task<ServiceDocumentResult> SetGroup(GroupModel departmentModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDepartment, MdmConstants.SetProcDepartment);
            OracleObject recordObject = this.SetParamsGroup(departmentModel);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async Task<ServiceDocumentResult> SetDepartment(DepartmentModel categoryModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeCategory, MdmConstants.SetProcCategory);
            OracleObject recordObject = this.SetParamsDepartment(categoryModel);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async Task<ServiceDocumentResult> SetClass(ClassModel classModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeClass, MdmConstants.SetProcClass);
            OracleObject recordObject = this.SetParamsClass(classModel);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async Task<ServiceDocumentResult> SetSubClass(SubClassModel subClassModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeSubClass, MdmConstants.SetProcSubClass);
            OracleObject recordObject = this.SetParamsSubClass(subClassModel);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public virtual void SetParamsCompanySave(CompanyModel companyModel, OracleObject recordObject)
        {
            recordObject["COMPANY"] = companyModel.Company;
            recordObject["CO_NAME"] = companyModel.CoName;
            recordObject["CO_ADD1"] = companyModel.CoAdd1;
            recordObject["CO_ADD2"] = companyModel.CoAdd2;
            recordObject["CO_ADD3"] = companyModel.CoAdd3;
            recordObject["CO_CITY"] = companyModel.CoCity;
            recordObject["CO_STATE"] = companyModel.CoState;
            recordObject["CO_COUNTRY"] = companyModel.CoCountry;
            recordObject["CO_POST"] = companyModel.CoPost;
            recordObject["Operation"] = companyModel.Operation;
        }

        public virtual void SetParamsDivisionSave(DivisionModel divisionModel, OracleObject recordObject)
        {
            recordObject["DIVISION"] = divisionModel.Division;
            recordObject["DIV_NAME"] = divisionModel.DivName;
            recordObject["BUYER"] = divisionModel.Buyer;
            recordObject["MERCH"] = divisionModel.Merch;
            recordObject["TOTAL_MARKET_AMT"] = divisionModel.TotalMarketAmt;
            recordObject["Operation"] = divisionModel.Operation;
        }

        public virtual void SetParamsGroupSave(GroupModel departmentModel, OracleObject recordObject)
        {
            recordObject["GROUP_NO"] = departmentModel.GroupNo;
            recordObject["GROUP_NAME"] = departmentModel.GroupName;
            recordObject["DIVISION"] = departmentModel.Division;
            recordObject["BUYER"] = departmentModel.Buyer;
            recordObject["MERCH"] = departmentModel.Merch;
            recordObject["Operation"] = departmentModel.Operation;
        }

        public virtual void SetParamsDepartmentSave(DepartmentModel categoryModel, OracleObject recordObject)
        {
            recordObject["DEPT"] = categoryModel.Dept;
            recordObject["Dept_Name"] = categoryModel.DeptName;
            recordObject["BUYER"] = categoryModel.Buyer;
            recordObject["MERCH"] = categoryModel.Merch;
            recordObject["PROFIT_CALC_TYPE"] = categoryModel.ProfitCalcType;
            recordObject["PURCHASE_TYPE"] = categoryModel.PurchaseType;
            recordObject["GROUP_NO"] = categoryModel.GroupNo;
            recordObject["BUD_INT"] = categoryModel.BudInt;
            recordObject["BUD_MKUP"] = categoryModel.BudMkup;
            recordObject["TOTAL_MARKET_AMT"] = categoryModel.TotalMarketAmt;
            recordObject["MARKUP_CALC_TYPE"] = categoryModel.MarkupCalcType;
            recordObject["OTB_CALC_TYPE"] = categoryModel.OtbCalcType;
            recordObject["MAX_AVG_COUNTER"] = categoryModel.MaxAvgCounter;
            recordObject["AVG_TOLERANCE_PCT"] = categoryModel.AvgTolerancePct;
            recordObject["DEPT_VAT_INCL_IND"] = categoryModel.DeptVatInclInd;
            recordObject["Operation"] = categoryModel.Operation;
        }

        public virtual void SetParamsClass(ClassModel classModel, OracleObject recordObject)
        {
            recordObject["DEPT"] = classModel.Dept;
            recordObject["CLASS"] = classModel.Class;
            recordObject["CLASS_NAME"] = classModel.ClassName;
            recordObject["CLASS_VAT_IND"] = classModel.ClassVatInd;
            recordObject["Operation"] = classModel.Operation;
        }

        public virtual void SetParamsSubClass(SubClassModel subClassModel, OracleObject recordObject)
        {
            recordObject["DEPT"] = subClassModel.Dept;
            recordObject["SUBCLASS"] = subClassModel.Subclass;
            recordObject["CLASS"] = subClassModel.Class;
            recordObject["SUB_NAME"] = subClassModel.SubName;
            recordObject["Operation"] = subClassModel.Operation;
        }

        private OracleObject SetParamsCompany(CompanyModel companyModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("COMPANY_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            this.SetParamsCompanySave(companyModel, recordObject);
            return recordObject;
        }

        private OracleObject SetParamsDivision(DivisionModel divisionModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("MERCH_DIVISION_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            this.SetParamsDivisionSave(divisionModel, recordObject);
            return recordObject;
        }

        private OracleObject SetParamsGroup(GroupModel departmentModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("MERCH_GROUP_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            this.SetParamsGroupSave(departmentModel, recordObject);
            return recordObject;
        }

        private OracleObject SetParamsDepartment(DepartmentModel categoryModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("MERCH_DEPT_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
                this.SetParamsDepartmentSave(categoryModel, recordObject);
            return recordObject;
        }

        private OracleObject SetParamsClass(ClassModel classModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("MERCH_CLASS_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            this.SetParamsClass(classModel, recordObject);
            return recordObject;
        }

        private OracleObject SetParamsSubClass(SubClassModel subClassModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("MERCH_SUB_CLASS_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            this.SetParamsSubClass(subClassModel, recordObject);
            return recordObject;
        }
    }
}
