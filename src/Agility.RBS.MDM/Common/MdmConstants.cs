﻿// <copyright file="MdmConstants.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Common
{
    internal static class MdmConstants
    {
        // Domain Package
        public const string GetDomainPackage = "UTL_DOMAIN";

        // Domain Data Package
        public const string GetDomainDataPackage = "UTL_COMMON";

        // Domain UOM Class
        public const string ObjTypeDomainUomClass = "UOM_CLASS_TAB";
        public const string ObjTypeDomainClass = "CLASS_TAB";
        public const string ObjTypeCurrencyClass = "CURRENCY_COMMON_TAB";
        public const string ObjTypeDomainCompany = "COMPANY_MST_TAB";
        public const string ObjTypeDomainGroup = "GROUP_TAB";
        public const string ObjTypeDomainDepartment = "DEPARTMENT_TAB";
        public const string FreightTypeTab = "FREIGHT_TYPE_TAB";
        public const string FreightSizeTab = "FREIGHT_Size_TAB";
        public const string ObjTypeDomainCategory = "CATEGORY_TAB";
        public const string ObjTypeDomainDivision = "DIVISION_TAB";
        public const string ObjTypeDomainFormat = "FORMAT_TAB";
        public const string ObjTypeDomainOrgCountry = "ORGCOUNTRY_TAB";
        public const string ObjTypeDomainRegion = "REGION_TAB";
        public const string ObjTypeDomainSeason = "SEASON_MST_TAB";
        public const string ObjTypeDomainSubClass = "SUB_CLASS_TAB";
        public const string ObjTypeDomainUom = "UOM_MST_TAB";
        public const string ObjTypeDomainVatCode = "VAT_CODE_TAB";
        public const string ObjTypeDomainVatRegion = "VAT_REGION_TAB";
        public const string ObjTypeDomainItem = "ITEM_MST_TAB";
        public const string GetProcVatCodeDomain = "GETVATCODE";
        public const string GetProcVatCodeByLocIdDomain = "getloc_vatcodes";
        public const string GetProcRegionDomain = "GETREGION";
        public const string GetProcFormatDomain = "GETFORMAT";
        public const string GetProcItemDomain = "GETITEMMST";
        public const string GetProcGroupDomain = "GETGROUP";
        public const string GetProcDeptDomain = "GETDEPARTMENT";
        public const string ObjTypeDomainCountry = "CNT_TAB";
        public const string GetProcCountryDomain = "GETCOUNTRY";
        public const string ObjTypeDomainState = "STATE_TAB";
        public const string GetProcStateDomain = "GETSTATE";
        public const string ObjTypeDomainBuyer = "BUYER_TAB";
        public const string GetProcBuyerDomain = "GETBUYER";
        public const string ObjTypeDomainMerch = "MERCHANT_TAB";
        public const string GetProcMerchDomain = "GETMERCHANT";
        public const string ObjTypeDomainBrand = "BRAND_COMMON_TAB";
        public const string GetProcBrandDomain = "GETBRAND";
        public const string ObjTypeDomainLocation = "LOCATION_TAB";
        public const string GetProcLocationDomain = "GETLOCATION";
        public const string ObjTypeDomainSupplier = "SUPPLIER_COMMON_TAB";
        public const string GetProcSupplierDomain = "GETSUPPLIER";
        public const string GetProcItemSupplierCompany = "GETITEMSUPPCOMPANY";
        public const string ObjTypeItemSupplierCompany = "ITEM_SUPP_COMP_TAB";
        public const string ObjTypeDomainUsers = "USER_LIST_COMMON_TAB";
        public const string GetProcUsersDomain = "GETUSERLISTCOMMON";
        public const string ObjTypeLanguageDomain = "LANGUAGE_COMMON_TAB";
        public const string GetProcLanguage = "GETLANGUAGE";
        public const string ObjTypePaymentDomain = "PAYMENT_METHOD_COMMON_TAB";
        public const string GetProcPayment = "GETPAYMENTMETHOD";
        public const string ObjTypeShipmentDomain = "SHIPMENT_COMMON_TAB";
        public const string GetProcShipment = "GETSHIPMENT";
        public const string GetProcFreightType = "GETFREIGHTTYPE";
        public const string GetProcFreightSize = "GETFREIGHTSIZE";
        public const string ObjTypeTermsDomain = "TERMS_COMMON_TAB";
        public const string GetProcTerms = "GETTERMS";
        public const string ObjTypeFreightDomain = "FRIEGHT_COMMON_TAB";
        public const string GetProcFreight = "GETFRIEGHT";
        public const string ObjTypeDeliveryPolicyDomain = "DELIVERY_POLICY_COMMON_TAB";
        public const string GetProcDeliveryPolicy = "GETDELIVERY_POLICY";
        public const string ObjTypeSecGroupDomain = "sec_group_list_tab";
        public const string GetProcSecGroupDomain = "getsecgrouplist";
        public const string ObjTypePartnerDomain = "PARTNER_TAB";
        public const string GetProcPartnerDomain = "GETPARTNER";
        public const string ObjTypeSecUserDomain = "user_attrib_list_tab";
        public const string GetProcSecUserDomain = "getuserattriblist";
        public const string ObjTypeDomainUserRoles = "ROLE_COMMON_TAB";
        public const string GetProcUserRolesDomain = "GETROLE";

        public const string ObjTypeDomainOutLocation = "OUTLOC_TAB";
        public const string GetProcOutLocationDomain = "GETOUTLOC";

        public const string ObjTypeDomainWhBasedLocations = "LOCATION_COMMON_TAB";
        public const string GetProcWhBasedLocationDomain = "GETLOCATIONLIST";

        public const string ObjTypeDomainCostZone = "COST_ZONE_COMMON_TAB";
        public const string GetProcCostZoneDomain = "GETCOSTZONECOMMON";

        public const string ObjTypeDomainPriceZone = "COST_ZONE_COMMON_TAB";
        public const string GetProcPriceZoneDomain = "GETPRICEZONECOMMON";

        public const string ObjTypeDomainCostChangeReason = "COST_CHG_REASON_COMMON_TAB";
        public const string GetProcCostChangeReasonDomain = "GETCOSTREASON";

        public const string ObjTypeDomainPriceChangeReason = "PRICE_CHG_REASON_COMMON_TAB";
        public const string GetProcPriceChangeReasonDomain = "GETPRICECHANGEREASON";

        public const string ObjTypeDomainCostChangeOrigin = "COST_CHG_ORIGIN_COMMON_TAB";
        public const string GetProcCostChangeOriginDomain = "GETCOST_CHG_ORIGIN";

        public const string ObjTypeDomainPromotion = "PROMOTION_COMMON_TAB";
        public const string GetProcPromotionDomain = "GETPROMOTION";

        public const string ObjTypeDomainDeptBuyer = "DEPT_BUYER_TAB";
        public const string GetProcDeptBuyerDomain = "GETDEPT_BUYER";

        public const string ObjTypeDomainSupplierDtls = "SUPP_COMMON_DTL_TAB";
        public const string GetProcSupplierDtlsDomain = "GETSUPPDTL";

        public const string ObjTypeDomainPriceChangeOrigin = "PRICE_CHG_ORIGIN_COMMON_TAB";
        public const string GetProcPriceChangeOriginDomain = "GETPRICE_CHG_ORIGIN";

        public const string ObjTypeItemListDomain = "ITEM_LIST_HDR_COMM_TAB";
        public const string GetProcItemListDomain = "GETCOMMONITEMLIST";

        public const string ObjTypeCommonOrderItemList = "ORDER_ITEM_TAB";
        public const string GetProcOrderItemList = "GETORDERITEMLIST";

        public const string GetProcGlobalCount = "GETGLOBALINBOXCNT";
        public const string ObjTypeGlobalInboxCount = "GLOBAL_INBOX_CNT_TAB";

        public const string GetProcInboxCommon = "GETGLOBALINBOXCOMMON";
        public const string ObjTypeInboxCommon = "GLOBAL_INBOX_COMMON_TAB";
        public const string RecordObjInboxCommon = "GLOBAL_INBOX_COMMON_REC";

        public const string ObjTypePoTypeDomain = "POTYPE_TAB";
        public const string GetProcPoType = "GETPOTYPE";

        public const string ObjTypeDomainRpmCodes = "RPM_CODES_TAB";
        public const string GetProcRpmCodesDomain = "GETRPMCODES";

        // Result
        public const string OracleParameterResult = "RESULT";

        // Domain
        public const string GetProcDomainHeader = "GETUTLDOMAIN";
        public const string SetProcDomainHeader = "SETUTLDOMAIN";
        public const string VGetProcDomainHeader = "VGETUTLDOMAIN";
        public const string ObjTypeDomainHeader = "UTL_DMN_HDR_TAB";

        // Domain Details
        public const string GetProcDomainDetail = "GETUTLDMNSTATDTLS";
        public const string ObjTypeDomainDetail = "UTL_DMN_DTL_TAB";
        public const string HeaderId = "P_HEADER_ID";

        // Common
        public const string CommonId = "P_ID";
        public const string GetProcCommonObject = "GETCOMMONOBJECT";
        public const string ObjTypeCommon = "COMMON_OBJECT_TAB";

        // UOM Package
        public const string GetUomPackage = "UTL_UOM";

        // UOM Class
        public const string GetProcUomClass = "GETUOMCLASS";
        public const string SetProcUomClass = "SETUOMCLASS";
        public const string VGetProcUomClass = "VGETUOMCLASS";
        public const string ObjTypeUomClass = "UOM_CLASS_MST_TAB";

        // UOM
        public const string GetProcUom = "GETUOM";
        public const string SetProcUom = "SETUOM";
        public const string ObjTypeUom = "UOM_MST_TAB";

        // InvAdjustmentReason Pakage
        public const string GetInvAdjustmentReasonPackage = "INVADJWRAPPER_SQL";

        // InvAdjustmentReason
        public const string GetProcInvAdjReason = "GETINVADJREASON";
        public const string SetProcInvAdjReason = "SETINVADJREASON";
        public const string ObjTypeInvAdjReason = "INV_ADJ_REASON_TAB";

        // UOM Conv
        public const string GetProcUomConv = "GETUOMCONVERSION";
        public const string SetProcUomConv = "SETUOMCONVERSION";
        public const string ObjTypeUomConv = "UOM_CONV_MST_TAB";

        // Merchant Hierarchy Package
        public const string GetMerchantHierarchyPackage = "MERCH_HIERARCHY";
        public const string GetWrapperPackage = "WRAPPER_SQL";

        // Division
        public const string GetProcDivision = "GETDIVISION";
        public const string SetProcDivision = "SETDIVISION";
        public const string VGetProcDivision = "VGETDIVISION";
        public const string ObjTypeDivision = "MERCH_DIVISION_TAB";

        // Group
        public const string GetProcDepartment = "GETGROUP";
        public const string SetProcDepartment = "SETGROUP";
        public const string ObjTypeDepartment = "MERCH_GROUP_TAB";

        // Category
        public const string GetProcCategory = "GETDEPT";
        public const string SetProcCategory = "SETDEPT";
        public const string ObjTypeCategory = "MERCH_DEPT_TAB";

        // Class
        public const string GetProcClass = "GETCLASS1";
        public const string SetProcClass = "SETCLASS";
        public const string ObjTypeClass = "MERCH_CLASS_TAB";

        // Sub Class
        public const string GetProcSubClass = "GETSUBCLASS";
        public const string SetProcSubClass = "SETSUBCLASS";
        public const string ObjTypeSubClass = "MERCH_SUB_CLASS_TAB";

        // Category Role
        public const string GetProcCategoryRole = "GETCATROLEMST";
        public const string SetProcCategoryRole = "SETCATROLEMST";
        public const string ObjTypeCategoryRole = "MERCH_CATROLE_TAB";

        // Merchant Space
        public const string GetProcMerchantSpace = "GETSPACEMST";
        public const string SetProcMerchantSpace = "SETSPACEMST";
        public const string ObjTypeMerchantSpace = "MERCH_SPACE_TAB";

        // Organization Hierarchy Package
        public const string GetOrganizationHierarchyPackage = "UTL_ORGANISATION";

        // Company
        public const string GetProcCompany = "GETCOMPHEAD";
        public const string SetProcCompany = "SETCOMPHEAD";
        public const string ObjTypeCompany = "COMPHEAD_TAB";

        // Org Country
        public const string GetProcOrgCountry = "GETORGCOUNTRY";
        public const string SetProcOrgCountry = "SETORGCOUNTRY";
        public const string ObjTypeOrgCountry = "ORG_COUNTRY_TAB";

        // Org Region
        public const string GetProcOrgRegion = "GETORGREGION";
        public const string SetProcOrgRegion = "SETORGREGION";
        public const string ObjTypeOrgRegion = "ORG_REGION_TAB";

        // Org Format
        public const string GetProcOrgFormat = "GETORGFORMAT";
        public const string SetProcOrgFormat = "SETORGFORMAT";
        public const string ObjTypeOrgFormat = "ORG_FORMAT_TAB";

        // Favourites
        public const string GetProcFavourite = "GET_USERFAVOURITE";
        public const string SetProcFavourite = "SET_USERFAVOURITE";
        public const string ObjTypeFavourite = "USER_FAV_MENU_TAB";
        public const string GetFavouritePackage = "UTL_USER_FAV_MENU";

        // Country
        public const string GetProcCountry = "GETCOUNTRY";
        public const string SetProcCountry = "SETCOUNTRY";
        public const string ObjTypeCountry = "COUNTRY_TAB";
        public const string GetCountryPackage = "UTL_COUNTRY";

        // Location
        public const string GetProcLocation = "GETLOCATION";
        public const string SetProcLocation = "SETLOCATION";
        public const string ObjTypeLocation = "LOCATION_MST_TAB";
        public const string GetLocationPackage = "UTL_ORGANISATION";

        // VAT Package
        public const string GetVATPackage = "UTL_VAT";

        // VAT Region
        public const string GetProcVatRegion = "GETVATREGION";
        public const string SetProcVatRegion = "SETVATREGION";
        public const string VGetProcVatRegion = "VGETVATREGION";
        public const string ObjTypeVatRegion = "VAT_REGION_MST_TAB";

        // VAT Rate
        public const string GetProcVatRate = "GETVATRATES";
        public const string SetProcVatRate = "SETVATRATES";
        public const string VGetProcVatRate = "VGETVATRATES";
        public const string ObjTypeVatRate = "VAT_RATES_MST_TAB";

        // Vat Code
        public const string GetProcVatCode = "GETVATCODES";
        public const string SetProcVatCode = "SETVATCODES";
        public const string VGetProcVatCode = "VGETVATCODES";
        public const string ObjTypeVatCode = "VAT_CODE_MST_TAB";

        // VatRegion VatCode
        public const string GetProcVatRegionVatCode = "GETVATREGIONVATCODE";
        public const string SetProcVatRegionVatCode = "SETVATREGIONVATCODE";
        public const string VGetProcVatRegionVatCode = "VVATREGIONVATCODE";
        public const string ObjTypeVatRegionVatCode = "VAT_REGION_VAT_CODE_MST_TAB";

        public const string VatCode = "P_VAT_CODE";
        public const string GetProcMaxEffDate = "GETMAXEFFECTDATE";

        public const string OrdHdrSuppCountry = "P_SUPP_COUNTRY";
        public const string OrdHdrOrdLocation = "P_ORD_LOCATION";
        public const string OrdHdrExchangeRate = "P_EXCHANGE_RATE";
        public const string GetProcOrdExchangeRate = "GETODR_EXCHANGERATE";

        // Domain Header Ids
        public const string DomainHdrStatus = "STST";
        public const string DomainHdrIndicator = "INDICATOR";
        public const string DomainHdrCalctype = "CALCTYPE";
        public const string DomainHdrStrgytype = "STRGYTYPE";
        public const string DomainHdrSpacelevel = "SPACELEVEL";
        public const string DomainHdrOperator = "OPERATOR";
        public const string DomainHdrLocationType = "LOC_TYPE";
        public const string DomainHdrLocationClass = "LOC_CLASS";
        public const string DomainHdrLocationStoreFormat = "STORE_FMT";

        public const string DomainHdrEDISalesRptFrequency = "EDI_S_R_FR";
        public const string DomainHdrSettlementCode = "STLM_CODE";
        public const string DomainHdrDbtMemoCode = "DBTM_CODE";
        public const string DomainHdrInventoryMgmtLevel = "INV_MG_LVL";
        public const string DomainHdrVMIOrderStatus = "VMI_ORD_ST";
        public const string DomainHdrRebatePeriod = "REBATE_PD";
        public const string DomainHdrAddressType = "ADDR_TYPE";
        public const string DomainHdrUomC = "UOMC";

        // Duplicate check function
        public const string DuplicateCheck = "GETDUPLICATECHECK";

        // ItemList Package
        public const string GetItemListPackage = "ITEM_SEARCH";

        // ItemList
        public const string GetProcItemListHead = "GETITEMLIST";
        public const string SetProcItemListHead = "SETITEMLIST";
        public const string ObjTypeItemListHead = "ITEM_LIST_HDR_TAB";

        // CostZoneGroup
        public const string GetProcCostZoneGroup = "GETCOSTZONEGROUP";
        public const string SetProcCostZoneGroup = "SETCOSTZONEGROUP";
        public const string ObjTypeCostZoneGroup = "COST_ZONE_GROUP_TAB";
        public const string RecordTypeCostZoneGroup = "COST_ZONE_GROUP_REC";

        // CostZone
        public const string GetProcCostZone = "GETCOSTZONE";
        public const string SetProcCostZone = "SETCOSTZONE";
        public const string ObjTypeCostZone = "COST_ZONE_TAB";
        public const string GetCostZonePackage = "UTL_COST";

        // CostChangeReason
        public const string GetProcCostChangeReason = "GETCOSTCHGREASON";
        public const string SetProcCostChangeReason = "SETCOSTCHGREASON";
        public const string ObjTypeCostChangeReason = "COST_CHG_REASON_TAB";
        public const string RecordTypeCostChangeReason = "COST_CHG_REASON_REC";

        // Price Zone Group
        public const string GetProcPriceZoneGroup = "GETPRICEZONEGROUP";
        public const string SetProcPriceZoneGroup = "SETPRICEZONEGROUP";
        public const string ObjTypePriceZoneGroup = "PRICE_ZONE_GROUP_TAB";
        public const string GetPriceZonePackage = "UTL_PRICE";
        public const string RecordTypePriceZoneGroup = "PRICE_ZONE_GROUP_REC";

        // Price Zone
        public const string GetProcPriceZone = "GETPRICEZONE";
        public const string SetProcPriceZone = "SETPRICEZONE";
        public const string ObjTypePriceZone = "PRICE_ZONE_TAB";
        public const string RecordTypePriceZone = "PRICE_ZONE_REC";
        public const string RecordTypePriceZoneLocation = "PRICE_ZONE_LOC_REC";
        public const string PriceZoneLocationTab = "PRICE_ZONE_LOC_TAB";

        // PriceChangeReason
        public const string GetProcPriceChangeReason = "GETPRICECHGREASON";
        public const string SetProcPriceChangeReason = "SETPRICECHGREASON";
        public const string ObjTypePriceChangeReason = "PRICE_CHG_REASON_TAB";
        public const string RecordTypePriceChangeReason = "PRICE_CHG_REASON_REC";

        public const string DomainHdrOrgLevel = "ORG_LVL";

        public const string RecordTypeCostZone = "COST_ZONE_REC";
        public const string RecordTypeCostZoneLocation = "COST_ZONE_LOC_REC";
        public const string CostZoneLocationTab = "COST_ZONE_LOC_TAB";

        // Brand
        public const string GetBrandPackage = "UTL_BRAND";
        public const string GetProcBrand = "GETBRAND";
        public const string SetProcBrand = "SETBRAND";
        public const string ObjTypeBrand = "BRAND_MST_TAB";
        public const string RecordTypeBrand = "BRAND_MST_REC";

        // BrandCategory
        public const string GetProcBrandCategory = "GETBRANDCAT";
        public const string SetProcBrandCategory = "SETBRANDCAT";
        public const string ObjTypeBrandCategory = "BRAND_CAT_MST_TAB";
        public const string RecordTypeBrandCategory = "BRAND_CAT_MST_REC";

        public const string GetItemList = "GETFINDITEMLIST";
        public const string FindItemListTab = "FIND_ITEM_LIST_TAB";

        // WF Status
        public const string GetWfStatusTab = "COMMON_OBJECT_TAB";
        public const string GetWfStatus = "GETITEM_WF_STATUS";

        // Partners
        public const string GetPartnerPackage = "UTL_PARTNER";
        public const string GetProcPartner = "GETPARTNER";
        public const string SetProcPartner = "SETPARTNER";
        public const string ObjTypePartner = "PARTNER_MST_TAB";
        public const string RecordTypePartner = "PARTNER_MST_REC";
        public const string DomainHdrPartnerTyp = "PRTNT";

        // UOM Class Mass
        public const string GetProcUomMass = "GETUOMMASS";

        public const string ObjTypeFilePath = "FILE_PATH_TAB";
        public const string GetProcFilePath = "GETFILEPATH";

        public const string ObjTypeDomainLocationType = "LOCATION_TAB";
        public const string GetProcLocationTypeDomain = "GETLOCATION";

        public const string ObjTypeDomainTransferEnity = "TSF_ENTITY_TAB";
        public const string GetProcTransferEnityDomain = "GETTSFENTITY";

        // Trait
        public const string ControlMasterDataPackage = "UTL_CTRL_MST_DATA";
        public const string LocationTraitTypeTab = "LOC_TRAITS_TAB";
        public const string SupplierTraitTypeTab = "SUP_TRAITS_TAB";
        public const string GetProcTypeLocationTrait = "GETLOCTRAITS";
        public const string GetProcTypeSupplierTrait = "GETSUPPTRAITS";
        public const string SetProcTypeLocationTrait = "SETLOCTRAITS";
        public const string SetProcTypeSupplierTrait = "SETSUPPTRAITS";

        // Administration
        public const string SystemOptionsTypeTab = "IM_SYS_OPTIONS_TAB";
        public const string SupplierOptionsTypeTab = "SUPP_OPTIONS_TAB";
        public const string GetProcTypeSystemOptions = "GETIMSYSOPTIONS";
        public const string GetProcTypeSupplierOptions = "GETSUPPOPTIONS";
        public const string SetProcTypeSystemOptions = "SETIMSYSOPTIONS";
        public const string SetProcTypeSupplierOptions = "SETSUPPOPTIONS";
        public const string ReasonCodeTypeTab = "IM_REASON_CODE_TAB";
        public const string ToleranceTypeTab = "IM_TLRC_SYS_TAB";
        public const string GetProcTypeReasonCode = "GETIMREASONCODES";
        public const string GetProcTypeTolerance = "GETIMTOLERANCESYS";
        public const string SetProcTypeReasonCode = "SETIMREASONCODES";
        public const string SetProcTypeTolerance = "SETIMTOLERANCESYS";
        public const string SecurityGroupTypeTab = "sec_group_tab";
        public const string SecurityGroupGetProc = "getsecgroup";
        public const string SecurityGroupSetProc = "setsecgroup";

        public const string SecurityUserGroupGetProc = "getsecusergroup";
        public const string SecurityUserGroupSetProc = "setsecusergroup";
        public const string SecurityUserGroupTypeTab = "sec_user_group_tab";
        public const string SecurityUserGroupTypeRec = "sec_user_group_rec";

        // Competitors Pakage
        public const string GetCompetitorsPackage = "COMPETITORWRAPPER_SQL";

        // Competitors
        public const string ObjTypeCompetitors = "COMPETITOR_MST_TAB";
        public const string GetProcCompetitors = "GETCOMPETITOR";
        public const string SetProcCompetitors = "SETCOMPETITOR";

        // CompetitivePriceHistory
        public const string ObjTypeCompetitivePriceHistory = "COMP_PRICE_HIST_TAB";
        public const string GetProcCompetitivePriceHistory = "GETCOMPPRICEHIST";
        public const string SetProcCompetitivePriceHistory = "SETCOMPPRICEHIST";

        // Chain
        public const string GetProcChain = "GETCHAIN";
        public const string SetProcChain = "SETCHAIN";
        public const string ObjTypeChain = "ORG_TAB";

        // Chain Domain Data
        public const string ObjTypeDomainChain = "CHAIN_LIST_TAB";
        public const string GetProcChainDomain = "GETCHAINLIST";

        // Area
        public const string GetProcArea = "GETAREA";
        public const string SetProcArea = "SETAREA";
        public const string ObjTypeArea = "AREA_TAB";

        // Area Domain Data
        public const string ObjTypeDomainArea = "AREA_LIST_TAB";
        public const string GetProcAreaDomain = "GETAREALIST";

        // Region Domain Data
        public const string ObjTypeDomainRegionNew = "REGION_LIST_TAB";
        public const string GetProcRegionNewDomain = "GETREGIONLIST";

        public const string GetProcOrgRegionNew = "GETREGION";
        public const string SetProcOrgRegionNew = "SETREGION";
        public const string ObjTypeOrgRegionNew = "REGION_TAB";

        // DISTRICT
        public const string GetProcDistrict = "GETDISTRICT";
        public const string SetProcDistrict = "SETDISTRICT";
        public const string ObjTypeDistrict = "DISTRICT_TAB";

        public const string DomainHdrLocationsType = "LOTP";

        // StoreFormat
        public const string GetStoreFormatPackage = "UTL_STORE_FORMAT";
        public const string GetProcStoreFormat = "GETSTOREFORMAT";
        public const string SetProcStoreFormat = "SETSTOREFORMAT";
        public const string ObjTypeStoreFormat = "STORE_FORMAT_TAB";
        public const string RecordTypeStoreFormat = "STORE_FORMAT_REC";
        public const string VatRegionTypeDomainData = "YSNO";
        public const string DomainHdrDelivery = "DLVY";

        // ValueRegionType
        public const string ObjTypeDomainValueRegionType = "VAT_REGION_TYPE_TAB";
        public const string GetProcValueRegionTypeDomain = "GETVATREGIONTYPE";

        // DealCompType
        public const string ObjTypeDomainValueDealCompType = "DEAL_COMP_TYPE_TAB";
        public const string GetProcValueDealCompTypeDomain = "GETDEALCOMPTYPE";

        // CostLevel
        public const string ObjTypeDomainCostLevel = "COST_LEVEL_TAB";
        public const string GetProcCostLevelDomain = "GETCOSTLEVEL";

        // DSD MinMax
        public const string GetProcDsdMinMax = "GETDSDAR";
        public const string SetProcDsdMinMax = "SETDSDAR";
        public const string ObjTypeDsdMinMax = "DSD_AR_TAB";
        public const string GetDsdMinMaxPackage = "DSD_ORDERS";
        public const string RecordTypeDsdMinMax = "DSD_AR_REC";

        // ItemQty Get
        public const string RecordObjItemQtyDetails = "TSF_ITEM_QTY_REC";
        public const string ObjTypeItemQtyDetails = "TSF_ITEM_QTY_TAB";
        public const string GetProcItemQtyDetail = "GETTSF_ITEM_QTY";
        public const string DomainUnitofTransfer = "DIMO";

        // Partner
        public const string ObjTypePartnerTrans = "PARTNER_DD_TAB";
        public const string GetProcPartnerTrans = "getpartner";

        // Component
        public const string ObjTypeComponentTrans = "elc_comp_dd_tab";
        public const string GetProcComponentTrans = "getelccomplist";

        // Suppier Obigation
        public const string ObjTypeSuppierObligation = "SUPPLIER_DD_TAB";
        public const string GetProcSuppierObligation = "GETSUPPDD";

        // Buyer
        public const string GetBuyerPackage = "UTL_BUYER";
        public const string GetBuyer = "GETBUYER";
        public const string SetProcBuyer = "SETBUYER";
        public const string ObjTypeBuyer = "BUYER_TAB";
        public const string RecordTypeBuyer = "BUYER_REC";

        // PriceLevel
        public const string ObjTypeDomainPriceLevel = "PRICE_LEVEL_TAB";
        public const string GetProcPriceLevelDomain = "GETPRICELEVEL";

        public const string DomainHdrLocType = "LOCT";

        // Season Domain Data
        public const string ObjTypeDomainSeasons = "SEASON_LIST_TAB";
        public const string GetProcSeasonsDomain = "GETSEASONLIST";

        // Company Domain Data
        public const string ObjTypeDomainCompanies = "COMPANY_MST_TAB";
        public const string GetProcCompaniesDomain = "GETCOMPANY";

        // Partner Type Domain Data
        public const string ObjTypeDomainPartnerType = "PARTNER_TYPE_TAB";
        public const string GetProcPartnerTypeDomain = "GETPARTNERTYPE";
        public const string InvPayLocDomainData = "IMPL";
        public const string PartnerTypeDomainData = "PART";

        // Cost Zone Group
        public const string ObjTypeDomainCostZoneGroup = "COST_ZONE_GROUP_COMMON_TAB";
        public const string GetProcCostZoneGroupDomain = "GETCOSTZONEGROUPLIST ";

        // Out Location
        public const string GetProcOutLocation = "GETOUTLOC";
        public const string SetProcOutLocation = "SETOUTLOC";
        public const string ObjTypeOutLocation = "RBS_OUTLOC_TAB";
        public const string RecordTypeOutLocation = "RBS_OUTLOC_REC";

        // Store
        public const string GetProcStore = "GETSTORE";
        public const string SetProcStore = "SETSTORE";
        public const string ObjTypeStore = "STORE_TAB";
        public const string GetStorePackage = "UTL_STORE";

        // Store
        public const string GetProcWareHouse = "getwh";
        public const string SetProcWareHouse = "setwh";
        public const string ObjTypeWareHouse = "WH_TAB";
        public const string GetWareHousePackage = "UTL_STORE";

        // Half
        public const string GetProcHalf = "gethalf";
        public const string SetProcHalf = "sethalf";
        public const string ObjTypeHalf = "half_tab";
        public const string GetHalfPackage = "UTL_CTRL_MST_DATA";
        public const string RecordTypeHalf = "half_rec";

        // Store
        public const string GetProcCurrencyExchangeType = "getcurrency_xref";
        public const string SetProcCurrencyExchangeType = "setcurrency_xref";
        public const string ObjTypeCurrencyExchangeType = "fif_curr_xref_tab";
        public const string GetCurrencyExchangeTypePackage = "UTL_CTRL_MST_DATA";
        public const string RecordTypeCurrencyExchangeType = "fif_curr_xref_rec";

        // Doc
        public const string GetProcDocument = "getdoc";
        public const string SetProcDocument = "setdoc";
        public const string ObjTypeDocument = "doc_tab";
        public const string GetDocumentPackage = "UTL_CTRL_MST_DATA";
        public const string RecordTypeDocument = "doc_rec";

        // Administration
        public const string GetProcTypeSaSystemOptions = "getsasysoptions";
        public const string SetProcTypeSaSystemOptions = "SETIMTOLERANCESYS";
        public const string SaSystemOptionsTypeTab = "sa_sys_options_tab";

        public const string GetProcTypeRpmSystemOptions = "getrpmsysoptions";
        public const string SetProcTypeRpmSystemOptions = "SETIMTOLERANCESYS";
        public const string RpmSystemOptionsTypeTab = "rpm_sys_options_tab";

        public const string GetProcTypeNbSystemOptions = "getnbsysoptions";
        public const string SetProcTypeNbSystemOptions = "SETIMTOLERANCESYS";
        public const string NbSystemOptionsTypeTab = "nb_sys_options_tab";
        public const string ClassStoreDomainData = "CSTR";

        // Nonmerchdise
        public const string ObjTypeNonmerchdiseTAB = "NONMERCH_DD_TAB";
        public const string GetProcNonmerchdise = "GETNONMERCHDD";

        // VatRate
        public const string ObjTypeImDocVatRateTAB = "vat_rates_mst_tab";
        public const string GetProcImDocVatRate = "getvatrate";
        public const string ProfitCalcTypeDomainData = "PFTP";
        public const string OtbCalcTypeDomainData = "OTBC";
        public const string PurchTypesDomainData = "PRTP";
        public const string MarkupCalTypeDomainData = "MUTP";

        // TransferEntity Package
        public const string GetTransferEntityPackage = "TRANSFERWRAPPER_SQL";

        // TransferEntity
        public const string GetProcTransferEntity = "GETTSFENTITY";
        public const string SetProcTransferEntity = "SETTSFENTITY";
        public const string ObjTypeTransferEntity = "TSF_ENTITY_TAB";
        public const string RecTypeTransferEntity = "TSF_ENTITY_REC";

        public const string ObjTypeTransferEntityLoc = "TSF_ENTITY_LOC_TAB";
        public const string RecTypeTransferEntityLoc = "TSF_ENTITY_LOC_REC";

        // TransferZone
        public const string GetProcTsfZone = "GETTSFZONE";
        public const string SetProcTsfZone = "SETTSFZONE";
        public const string ObjTypeTsfZone = "TSFZONE_TAB";

        public const string GetProcTransferZone = "GETTSFZONE";
        public const string SetProcTransferZone = "SETTSFZONE";
        public const string ObjTypeTransferZone = "TSFZONE_TAB";
        public const string RecTypeTransferZone = "TSFZONE_REC";

        public const string ObjTypeTransferZoneLoc = "TSF_ENTITY_LOC_TAB";
        public const string RecTypeTransferZoneLoc = "TSF_ENTITY_LOC_REC";

        // Class
        public const string RecTypeClass = "merch_class_rec1";

        public const string ObjTypeSubClassDetail = "merch_sub_class_tab1";
        public const string RecTypeSubClassDetail = "merch_sub_class_rec1";

        // Currency
        public const string GetProcCurrency = "GETCURRENCY";
        public const string GetCurrencyPackage = "UTL_CURRENCY";
        public const string SetProcCurrency = "SETCURRENCY";

        public const string ObjTypeCurrency = "CURRENCY_TAB";
        public const string ObjRecCurrency = "CURRENCY_REC";

        public const string ObjTypeCurrencyRate = "CURRENCY_RATE_TAB";
        public const string ObjRecCurrencyRate = "CURRENCY_RATE_REC";
        public const string CurrencyExchTypeDomainData = "CXTP";

        public const string GetProcCurrencyRate = "getcurrencyrate";
        public const string SetProcCurrencyRate = "setcurrencyrate";
        public const string RecordTypeCurrencyRate = "currency_rate_rec";

        // Season
        public const string GetSeasonPackage = "UTL_SEASON";
        public const string GetProcSeason = "GETSEASON";
        public const string SetProcSeason = "SETSEASON";

        public const string ObjTypeSeason = "SEASON_TAB";
        public const string RecTypeSeason = "SEASON_REC";

        // Phase
        public const string GetProcPhase = "GETPHASE";
        public const string SetProcPhase = "SETPHASE";
        public const string ObjTypePhase = "PHASE_TAB";
        public const string RecTypePhase = "PHASE_REC";

        // General Ledger
        public const string GetProcGLRef = "GETFIF_GL_REF";
        public const string SetProcGLRef = "SETFIF_GL_REF";
        public const string ObjTypeGLRef = "FIF_GL_REF_SEARCH_TAB";
        public const string RecordTypeGLRef = "FIF_GL_REF_SEARCH_REC";
        public const string ObjTypeSetRef = "fif_gl_ref_TAB";
        public const string RecordTypeSetGLRef = "fif_gl_ref_REC";
        public const string GetGLRefPackage = "UTL_FINANCE";

        // General Ledger
        public const string GetProcSearchGLRef = "GETFIF_GL_REF_LIST";
        public const string ObjTypeGLSearchRef = "FIF_GL_REF_SEARCH_TAB";

        // AvgCostAdjmnt
        public const string GetProcAvgCostAdjmnt = "GETITEMAVGCOST";
        public const string SetProcAvgCostAdjmnt = "SETAVGCOSTADJ";
        public const string ObjTypeAvgCostAdjmnt = "AVG_COST_ADJ_TAB";
        public const string RecTypeAvgCostAdjmnt = "AVG_COST_ADJ_REC";

        // AvgCostAdjmnt Item Details
        public const string RecTypeAvgCostAdjmntItemDetails = "TSF_ITEM_QTY_REC";
        public const string ObjTypeAvgCostAdjmntItemDetails = "TSF_ITEM_QTY_TAB";
        public const string GetProcAvgCostAdjmntItemDetails = "getitemavgcost";
        public const string SetAvgCostAdjmntItemDetails = "setavgcostadj";
        public const string GetPackageItemAttributes = "ITEM_ATTRIB_SQL";
        public const string GetFinancePackage = "UTL_FINANCE";
        public const string GetOutLocationPackage = "UTL_CTRL_MST_DATA";

        // RecCostAdjmnt
        public const string GetProcRecvCostAdjmnt = "GETRCVCOSTORDDTL";
        public const string SetProcRecvCostAdjmnt = "SETRCVCOSTORDDTL";
        public const string ObjTypeRecvCostAdjmnt = "RCV_COST_ORDITEM_TAB";
        public const string RecTypeRecvCostAdjmnt = "RCV_COST_ORDITEM_REC";

        // RecCostAdjmnt Item Info
        public const string ObjTypeRecvSuppDetails = "RCV_COST_ORD_TAB";
        public const string RecTypeRecvSuppDetails = "RCV_COST_ORD_REC";
        public const string GetProcRecvSuppliers = "GETORDDTL";

    }
}