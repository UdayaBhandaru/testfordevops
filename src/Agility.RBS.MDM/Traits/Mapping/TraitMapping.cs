﻿// <copyright file="TraitMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Trait.Mapping
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.Trait.Models;
    using Agility.RBS.MDM.Traits.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class TraitMapping : Profile
    {
        public TraitMapping()
            : base("SupplierTraitMapping")
        {
            this.CreateMap<OracleObject, LocationTraitModel>()
                .ForMember(m => m.LoacationTraitId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["LOC_TRAIT"])))
                .ForMember(m => m.Description, opt => opt.MapFrom(r => r["DESCRIPTION"]))
                .ForMember(m => m.FilterOrganizationId, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["FILTER_ORG_ID"])))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => r["OPERATION"]));

            this.CreateMap<OracleObject, SupplierTraitModel>()
                .ForMember(m => m.SupplierTraitId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["SUP_TRAIT"])))
                .ForMember(m => m.Description, opt => opt.MapFrom(r => r["DESCRIPTION"]))
                .ForMember(m => m.MasterSupplierIndicator, opt => opt.MapFrom(r => r["MASTER_SUP_IND"]))
                .ForMember(m => m.MasterSupplier, opt => opt.MapFrom(r => r["MASTER_SUP"]))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => r["OPERATION"]));

            this.CreateMap<OracleObject, ComponentTraitModel>()
                .ForMember(m => m.CompId, opt => opt.MapFrom(r => r["COMP_ID"]))
                .ForMember(m => m.CompDesc, opt => opt.MapFrom(r => r["COMP_DESC"]))
                .ForMember(m => m.ProgramPhase, opt => opt.MapFrom(r => r["PROGRAM_PHASE"]))
                .ForMember(m => m.ProgramMessage, opt => opt.MapFrom(r => r["PROGRAM_MESSAGE"]))
                .ForMember(m => m.Perror, opt => opt.MapFrom(r => r["P_ERROR"]));

            this.CreateMap<OracleObject, NonMerchandiseDomainModel>()
                .ForMember(m => m.NonMerchCode, opt => opt.MapFrom(r => r["NON_MERCH_CODE"]))
                .ForMember(m => m.NonMerchCodeDesc, opt => opt.MapFrom(r => r["NON_MERCH_CODE_DESC"]))
                .ForMember(m => m.ServiceInd, opt => opt.MapFrom(r => r["SERVICE_IND"]))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));
        }
    }
}
