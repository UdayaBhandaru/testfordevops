﻿// <copyright file="ComponentTraitModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Traits.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class ComponentTraitModel : ProfileEntity
    {
        public string CompId { get; set; }

        public string CompDesc { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Perror { get; set; }
    }
}
