﻿// <copyright file="NonMerchandiseDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Trait.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class NonMerchandiseDomainModel
    {
        public string NonMerchCode { get; set; }

        public string NonMerchCodeDesc { get; set; }

        public string ServiceInd { get; set; }

        public string Operation { get; set; }
    }
}
