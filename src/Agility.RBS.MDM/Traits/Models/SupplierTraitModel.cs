﻿// <copyright file="SupplierTraitModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
namespace Agility.RBS.MDM.Trait.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class SupplierTraitModel : ProfileEntity
    {
        public int SupplierTraitId { get; set; }

        public string Description { get; set; }

        public string MasterSupplierIndicator { get; set; }

        public string MasterSupplier { get; set; }

        public string Operation { get; set; }
    }
}
