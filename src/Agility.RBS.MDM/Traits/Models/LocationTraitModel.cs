﻿// <copyright file="LocationTraitModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
namespace Agility.RBS.MDM.Trait.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class LocationTraitModel : ProfileEntity
    {
        public int? LoacationTraitId { get; set; }

        public string Description { get; set; }

        public int? FilterOrganizationId { get; set; }

        public string Operation { get; set; }
    }
}
