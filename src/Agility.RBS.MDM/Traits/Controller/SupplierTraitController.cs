﻿//-------------------------------------------------------------------------------------------------
// <copyright file="SupplierTraitController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// SupplierTraitController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Trait
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Freight.Models;
    using Agility.RBS.MDM.MerchantHierarchy.Models;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Trait.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class SupplierTraitController : Controller
    {
        private readonly ServiceDocument<SupplierTraitModel> serviceDocument;
        private readonly TraitRepository traitRepository;

        public SupplierTraitController(
            ServiceDocument<SupplierTraitModel> serviceDocument,
            TraitRepository traitRepository)
        {
            this.traitRepository = traitRepository;
            this.serviceDocument = serviceDocument;
        }

        public async Task<ServiceDocument<SupplierTraitModel>> List()
        {
            return await this.SupplierTraitData();
        }

        public async Task<ServiceDocument<SupplierTraitModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.LocalTraitSave);
            return this.serviceDocument;
        }

        private async Task<bool> LocalTraitSave()
        {
            this.serviceDocument.DataProfile.DataModel.Operation = "U";
            this.serviceDocument.Result = await this.traitRepository.SetSupplierTrait(this.serviceDocument.DataProfile.DataModel);
            return true;
        }

        private async Task<ServiceDocument<SupplierTraitModel>> SupplierTraitData()
        {
            this.serviceDocument.DataProfile.DataList = await this.traitRepository.GetSupplierTraits();
            return this.serviceDocument;
        }
    }
}
