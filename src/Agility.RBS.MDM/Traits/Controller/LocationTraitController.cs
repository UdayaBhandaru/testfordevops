﻿//-------------------------------------------------------------------------------------------------
// <copyright file="LocationTraitController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// LocationTraitController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Trait
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Freight.Models;
    using Agility.RBS.MDM.MerchantHierarchy.Models;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Trait.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class LocationTraitController : Controller
    {
        private readonly ServiceDocument<LocationTraitModel> serviceDocument;
        private readonly TraitRepository traitRepository;

        public LocationTraitController(
            ServiceDocument<LocationTraitModel> serviceDocument,
            TraitRepository traitRepository)
        {
            this.traitRepository = traitRepository;
            this.serviceDocument = serviceDocument;
        }

        public async Task<ServiceDocument<LocationTraitModel>> List()
        {
            return await this.LocationTraitData();
        }

        public async Task<ServiceDocument<LocationTraitModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.LocalTraitSave);
            return this.serviceDocument;
        }

        private async Task<bool> LocalTraitSave()
        {
            this.serviceDocument.DataProfile.DataModel.Operation = "U";
            this.serviceDocument.Result = await this.traitRepository.SetLocationTrait(this.serviceDocument.DataProfile.DataModel);
            return true;
        }

        private async Task<ServiceDocument<LocationTraitModel>> LocationTraitData()
        {
            this.serviceDocument.DataProfile.DataList = await this.traitRepository.GetLocationTraits();
            return this.serviceDocument;
        }
    }
}
