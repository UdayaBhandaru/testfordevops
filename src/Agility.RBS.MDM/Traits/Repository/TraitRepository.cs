﻿// <copyright file="TraitRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Trait
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Freight.Models;
    using Agility.RBS.MDM.MerchantHierarchy.Models;
    using Agility.RBS.MDM.Trait.Models;
    using Agility.RBS.MDM.Traits.Models;
    using Agility.RBS.MDM.UtilityDomain.Models;
    using AutoMapper;
    using Devart.Data.Oracle;
    using Microsoft.Extensions.Caching.Distributed;

    public class TraitRepository : BaseOraclePackage
    {
        private readonly ServiceDocumentResult serviceDocumentResult;

        public TraitRepository()
        {
            this.PackageName = MdmConstants.ControlMasterDataPackage;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<List<LocationTraitModel>> GetLocationTraits()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("LOC_TRAITS_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.LocationTraitTypeTab, MdmConstants.GetProcTypeLocationTrait);
            return await this.GetProcedure2<LocationTraitModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<SupplierTraitModel>> GetSupplierTraits()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("SUP_TRAITS_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.SupplierTraitTypeTab, MdmConstants.GetProcTypeSupplierTrait);
            return await this.GetProcedure2<SupplierTraitModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<ServiceDocumentResult> SetLocationTrait(LocationTraitModel locationTraitModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.LocationTraitTypeTab, MdmConstants.SetProcTypeLocationTrait);
            OracleObject recordObject = this.SetParamsLocationTrait(locationTraitModel);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async Task<ServiceDocumentResult> SetSupplierTrait(SupplierTraitModel locationTraitModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.SupplierTraitTypeTab, MdmConstants.SetProcTypeSupplierTrait);
            OracleObject recordObject = this.SetParamsSupplierTrait(locationTraitModel);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async Task<List<ComponentTraitModel>> GetComponentTraits(string type)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("ELC_COMP_DD_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeComponentTrans, MdmConstants.GetProcComponentTrans);
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            OracleParameter parameter = new OracleParameter("COMP_ID", OracleDbType.VarChar);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = type;
            parameters.Add(parameter);
            return await this.GetProcedure2<ComponentTraitModel>(recordObject, packageParameter, this.serviceDocumentResult, parameters);
        }

        public async Task<List<NonMerchandiseDomainModel>> NonmerchdiseGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("NONMERCH_DD_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeNonmerchdiseTAB, MdmConstants.GetProcNonmerchdise);
            return await this.GetProcedure2<NonMerchandiseDomainModel>(recordObject, packageParameter, null);
        }

        private OracleObject SetParamsLocationTrait(LocationTraitModel locationTraitModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("LOC_TRAITS_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            recordObject["loc_trait"] = locationTraitModel.LoacationTraitId;
            recordObject["Description"] = locationTraitModel.Description;
            recordObject["operation"] = locationTraitModel.Operation;
            recordObject["filter_org_id"] = locationTraitModel.FilterOrganizationId;
            return recordObject;
        }

        private OracleObject SetParamsSupplierTrait(SupplierTraitModel supplierTraitModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("SUP_TRAITS_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            recordObject["OPERATION"] = supplierTraitModel.Operation;
            recordObject["SUP_TRAIT"] = supplierTraitModel.SupplierTraitId;
            recordObject["DESCRIPTION"] = supplierTraitModel.Description;
            recordObject["MASTER_SUP_IND"] = supplierTraitModel.MasterSupplierIndicator;
            recordObject["MASTER_SUP"] = supplierTraitModel.MasterSupplier;
            return recordObject;
        }
    }
}
