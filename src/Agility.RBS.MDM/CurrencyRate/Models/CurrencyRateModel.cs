﻿// <copyright file="CurrencyRateModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// CostChangeReason
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.CurrencyRate.Models
{
    using System;
    using Agility.Framework.Core.Profile.Entities;

    public class CurrencyRateModel : ProfileEntity
    {
        public string CurrencyCode { get; set; }

        public string CurrencyDesc { get; set; }

        public DateTime? EffectiveDate { get; set; }

        public string ExchangeType { get; set; }

        public int? ExchangeRate { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Error { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }
    }
}
