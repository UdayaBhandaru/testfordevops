﻿// <copyright file="CurrencyRateRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// CostZoneRepository
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.CurrencyRate.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.CurrencyRate.Models;
    using AutoMapper;
    using Devart.Data.Oracle;
    using Microsoft.AspNetCore.Authorization;

    public class CurrencyRateRepository : BaseOraclePackage
    {
        public CurrencyRateRepository()
        {
            this.PackageName = MdmConstants.GetCurrencyPackage;
        }

        public async Task<List<CurrencyRateModel>> GetCurrencyRate(CurrencyRateModel currencyRateModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeCurrencyRate, MdmConstants.GetProcCurrencyRate);
            OracleObject recordObject = this.SetParamsCurrencyRate(currencyRateModel, true);
            return await this.GetProcedure2<CurrencyRateModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ServiceDocumentResult> SetCurrencyRate(CurrencyRateModel currencyRateModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeCurrencyRate, MdmConstants.SetProcCurrencyRate);
            OracleObject recordObject = this.SetParamsCurrencyRate(currencyRateModel, false);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        private OracleObject SetParamsCurrencyRate(CurrencyRateModel currencyRateModel, bool isSearch)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(MdmConstants.RecordTypeCurrencyRate, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            if (!isSearch)
            {
                recordObject[recordType.Attributes["Operation"]] = currencyRateModel.Operation;
            }

            recordObject[recordType.Attributes["currency_code"]] = currencyRateModel.CurrencyCode;
            recordObject[recordType.Attributes["effective_date"]] = currencyRateModel.EffectiveDate;
            recordObject[recordType.Attributes["exchange_type"]] = currencyRateModel.ExchangeType;
            recordObject[recordType.Attributes["exchange_rate"]] = currencyRateModel.ExchangeRate;

            return recordObject;
        }
    }
}