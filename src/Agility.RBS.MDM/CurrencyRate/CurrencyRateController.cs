﻿//-------------------------------------------------------------------------------------------------
// <copyright file="CurrencyRateController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// CurrencyRateController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.CurrencyRate
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.CurrencyRate.Models;
    using Agility.RBS.MDM.CurrencyRate.Repositories;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    [Route("api/[controller]/[action]")]

    public class CurrencyRateController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocument<CurrencyRateModel> serviceDocument;
        private readonly CurrencyRateRepository currencyRateRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;

        public CurrencyRateController(
            ServiceDocument<CurrencyRateModel> serviceDocument,
           CurrencyRateRepository currencyRateRepository,
            DomainDataRepository domainDataRepository)
        {
            this.currencyRateRepository = currencyRateRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<CurrencyRateModel>> List()
        {
            this.serviceDocument.DomainData.Add("currency", this.domainDataRepository.CurrencyDomainGet().Result);
            this.serviceDocument.DomainData.Add("currencyExchType", this.domainDataRepository.DomainDetailData(MdmConstants.CurrencyExchTypeDomainData).Result);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<CurrencyRateModel>> New()
        {
            this.serviceDocument.DomainData.Add("currency", this.domainDataRepository.CurrencyDomainGet().Result);
            this.serviceDocument.DomainData.Add("currencyExchType", this.domainDataRepository.DomainDetailData(MdmConstants.CurrencyExchTypeDomainData).Result);
            this.serviceDocument.LocalizationData.AddData("CurrencyRate");
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<CurrencyRateModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.CurrencyRateSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<CurrencyRateModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.CurrencyRateSave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingCurrencyRate(string currencyCode)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "CURRENCY_RATES";
            nvc["P_COL1"] = currencyCode;
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        private async Task<List<CurrencyRateModel>> CurrencyRateSearch()
        {
            try
            {
                var lstUomClass = await this.currencyRateRepository.GetCurrencyRate(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstUomClass;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> CurrencyRateSave()
        {
            this.serviceDocument.Result = await this.currencyRateRepository.SetCurrencyRate(this.serviceDocument.DataProfile.DataModel);
            return true;
        }
    }
}
