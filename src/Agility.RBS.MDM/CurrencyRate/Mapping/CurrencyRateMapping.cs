﻿// <copyright file="CurrencyRateMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.StoreFormat.Mapping
{
    using System;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.CurrencyRate.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class CurrencyRateMapping : Profile
    {
        public CurrencyRateMapping()
            : base("CurrencyRateMapping")
        {
            this.CreateMap<OracleObject, CurrencyRateModel>()
              .ForMember(m => m.CurrencyCode, opt => opt.MapFrom(r => r["currency_code"]))
              .ForMember(m => m.CurrencyDesc, opt => opt.MapFrom(r => r["currency_desc"]))
              .ForMember(m => m.EffectiveDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["effective_date"])))
              .ForMember(m => m.ExchangeType, opt => opt.MapFrom(r => r["exchange_type"]))
              .ForMember(m => m.ExchangeRate, opt => opt.MapFrom(r => r["exchange_rate"]))
              .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
              .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));
        }
    }
}
