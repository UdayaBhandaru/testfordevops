﻿//-------------------------------------------------------------------------------------------------
// <copyright file="GlCrossReferenceListController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// GlCrossReferenceController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.GlCrossReference
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Buyer.Models;
    using Agility.RBS.MDM.Buyer.Repositories;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.GlCrossReference.Models;
    using Agility.RBS.MDM.GlCrossReference.Repositories;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Models;
    using Agility.RBS.MDM.ValueAddedTax;
    using Agility.RBS.MDM.ValueAddedTax.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    [Route("api/[controller]/[action]")]

    public class GlCrossReferenceListController : Controller
    {
        private readonly ServiceDocument<GlCrossReferenceSearchModel> serviceDocument;
        private readonly GlCrossReferenceRepository glCrossReferenceRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly DomainDataRepository domainDataRepository;
        private readonly ValueAddedTaxRepository vatRepository;

        public GlCrossReferenceListController(
            ServiceDocument<GlCrossReferenceSearchModel> serviceDocument,
            GlCrossReferenceRepository glCrossReferenceRepository,
            ValueAddedTaxRepository vatRepository,
            DomainDataRepository domainDataRepository)
        {
            this.glCrossReferenceRepository = glCrossReferenceRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
            this.vatRepository = vatRepository;

            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<GlCrossReferenceSearchModel>> List()
        {
            await this.GlCrossRefDomainData();
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<GlCrossReferenceSearchModel>> New()
        {
            await this.GlCrossRefDomainData();
            this.serviceDocument.LocalizationData.AddData("GLCrossRef");
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<GlCrossReferenceSearchModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.GlCrossReferenceSearch);
            return this.serviceDocument;
        }

        private async Task<List<GlCrossReferenceSearchModel>> GlCrossReferenceSearch()
        {
            try
            {
                var lstInvAdjReason = await this.glCrossReferenceRepository.GetGlCrossRefeSearchAsync(this.serviceDocument, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstInvAdjReason;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task GlCrossRefDomainData()
        {
            VatCodeModel vatCodeModel = new VatCodeModel();
            this.serviceDocument.DomainData.Add("vatCode", await this.vatRepository.GetVatCodes(vatCodeModel, this.serviceDocumentResult));
            this.serviceDocument.DomainData.Add("location", await this.domainDataRepository.LocationDomainGet());
            this.serviceDocument.DomainData.Add("category", await this.domainDataRepository.DepartmentDomainGet());
            this.serviceDocument.DomainData.Add("fineline", await this.domainDataRepository.ClassDomainGet());
            this.serviceDocument.DomainData.Add("segment", new List<SubClassDomainModel>());
        }
    }
}
