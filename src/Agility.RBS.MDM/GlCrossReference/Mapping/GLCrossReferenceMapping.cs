﻿// <copyright file="GlCrossReferenceMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.GlCrossReference.Mapping
{
    using System;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.Buyer.Models;
    using Agility.RBS.MDM.GlCrossReference.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class GlCrossReferenceMapping : Profile
    {
        public GlCrossReferenceMapping()
            : base("GlCrossReferenceMapping")
        {
            this.CreateMap<OracleObject, GlCrossReferenceSearchModel>()
            .ForMember(m => m.FifGlCrossRefId, opt => opt.MapFrom(r => r["FIF_GL_CROSS_REF_ID"]))
            .ForMember(m => m.Dept, opt => opt.MapFrom(r => r["DEPT"]))
            .ForMember(m => m.DeptName, opt => opt.MapFrom(r => r["DEPT_NAME"]))
            .ForMember(m => m.Class, opt => opt.MapFrom(r => r["CLASS"]))
            .ForMember(m => m.ClassName, opt => opt.MapFrom(r => r["CLASS_NAME"]))
            .ForMember(m => m.SubClass, opt => opt.MapFrom(r => r["SUBCLASS"]))
            .ForMember(m => m.SubClassName, opt => opt.MapFrom(r => r["SUBCLASS_NAME"]))
            .ForMember(m => m.Location, opt => opt.MapFrom(r => r["LOCATION"]))
            .ForMember(m => m.LocName, opt => opt.MapFrom(r => r["LOC_NAME"]))
            .ForMember(m => m.TranCode, opt => opt.MapFrom(r => r["TRAN_CODE"]))
            .ForMember(m => m.TranCodeDesc, opt => opt.MapFrom(r => r["TRAN_CODE_DESC"]))
            .ForMember(m => m.CostRetailFlag, opt => opt.MapFrom(r => r["COST_RETAIL_FLAG"]))
            .ForMember(m => m.LineType, opt => opt.MapFrom(r => r["LINE_TYPE"]))
            .ForMember(m => m.LineTypeDesc, opt => opt.MapFrom(r => r["LINE_TYPE_DESC"]))
            .ForMember(m => m.TranRefNo, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["TRAN_REF_NO"])))
            .ForMember(m => m.NbVatCode, opt => opt.MapFrom(r => r["NB_VAT_CODE"]))
            .ForMember(m => m.ProgramPhase, opt => opt.MapFrom(r => r["PROGRAM_PHASE"]))
            .ForMember(m => m.ProgramMessage, opt => opt.MapFrom(r => r["PROGRAM_MESSAGE"]))
            .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_ERROR"]))
            .ForMember(m => m.TableName, opt => opt.MapFrom(r => "FIF_GL_CROSS_REF"))
            .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, GlCrossReferenceHeadModel>()
             .ForMember(m => m.FifGlCrossRefId, opt => opt.MapFrom(r => r["FIF_GL_CROSS_REF_ID"]))
             .ForMember(m => m.Dept, opt => opt.MapFrom(r => r["DEPT"]))
             .ForMember(m => m.DeptName, opt => opt.MapFrom(r => r["DEPT_NAME"]))
             .ForMember(m => m.Class, opt => opt.MapFrom(r => r["CLASS"]))
             .ForMember(m => m.ClassName, opt => opt.MapFrom(r => r["CLASS_NAME"]))
             .ForMember(m => m.SubClass, opt => opt.MapFrom(r => r["SUBCLASS"]))
             .ForMember(m => m.SubClassName, opt => opt.MapFrom(r => r["SUBCLASS_NAME"]))
             .ForMember(m => m.Location, opt => opt.MapFrom(r => r["LOCATION"]))
             .ForMember(m => m.LocName, opt => opt.MapFrom(r => r["LOC_NAME"]))
             .ForMember(m => m.TranCode, opt => opt.MapFrom(r => r["TRAN_CODE"]))
             .ForMember(m => m.TranCodeDesc, opt => opt.MapFrom(r => r["TRAN_CODE_DESC"]))
             .ForMember(m => m.CostRetailFlag, opt => opt.MapFrom(r => r["COST_RETAIL_FLAG"]))
             .ForMember(m => m.LineType, opt => opt.MapFrom(r => r["LINE_TYPE"]))
             .ForMember(m => m.LineTypeDesc, opt => opt.MapFrom(r => r["LINE_TYPE_DESC"]))
             .ForMember(m => m.TranRefNo, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["TRAN_REF_NO"])))
             .ForMember(m => m.NbVatCode, opt => opt.MapFrom(r => r["NB_VAT_CODE"]))
             .ForMember(m => m.DrAccount, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DR_ACCOUNT"])))
             .ForMember(m => m.DrEbsCompany, opt => opt.MapFrom(r => r["DR_EBS_COMPANY"]))
             .ForMember(m => m.DrEbsSbu, opt => opt.MapFrom(r => r["DR_EBS_SBU"]))
             .ForMember(m => m.DrEbsLocation, opt => opt.MapFrom(r => r["DR_EBS_LOCATION"]))
             .ForMember(m => m.DrEbsDivision, opt => opt.MapFrom(r => r["DR_EBS_DIVISION"]))
             .ForMember(m => m.DrEbsDept, opt => opt.MapFrom(r => r["DR_EBS_DEPT"]))
             .ForMember(m => m.DrEbsSupCat, opt => opt.MapFrom(r => r["DR_EBS_SUP_CAT"]))
             .ForMember(m => m.DrEbsAccount, opt => opt.MapFrom(r => r["DR_EBS_ACCOUNT"]))
             .ForMember(m => m.DrEbsSuffix, opt => opt.MapFrom(r => r["DR_EBS_SUFFIX"]))
             .ForMember(m => m.DrSequence9, opt => opt.MapFrom(r => r["DR_SEQUENCE9"]))
             .ForMember(m => m.DrSequence10, opt => opt.MapFrom(r => r["DR_SEQUENCE10"]))
             .ForMember(m => m.CrAccount, opt => opt.MapFrom(r => r["CR_ACCOUNT"]))
             .ForMember(m => m.CrEbsCompany, opt => opt.MapFrom(r => r["CR_EBS_COMPANY"]))
             .ForMember(m => m.CrEbsSbu, opt => opt.MapFrom(r => r["CR_EBS_SBU"]))
             .ForMember(m => m.CrEbsLocation, opt => opt.MapFrom(r => r["CR_EBS_LOCATION"]))
             .ForMember(m => m.CrEbsDivision, opt => opt.MapFrom(r => r["CR_EBS_DIVISION"]))
             .ForMember(m => m.CrEbsDept, opt => opt.MapFrom(r => r["CR_EBS_DEPT"]))
             .ForMember(m => m.CrEbsSupCat, opt => opt.MapFrom(r => r["CR_EBS_SUP_CAT"]))
             .ForMember(m => m.CrEbsAccount, opt => opt.MapFrom(r => r["CR_EBS_ACCOUNT"]))
             .ForMember(m => m.CrEbsSuffix, opt => opt.MapFrom(r => r["CR_EBS_SUFFIX"]))
             .ForMember(m => m.CrSequence9, opt => opt.MapFrom(r => r["CR_SEQUENCE9"]))
             .ForMember(m => m.CrSequence10, opt => opt.MapFrom(r => r["CR_SEQUENCE10"]))
             .ForMember(m => m.ProgramPhase, opt => opt.MapFrom(r => r["PROGRAM_PHASE"]))
             .ForMember(m => m.ProgramMessage, opt => opt.MapFrom(r => r["PROGRAM_MESSAGE"]))
             .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_ERROR"]))
             .ForMember(m => m.TableName, opt => opt.MapFrom(r => "FIF_GL_CROSS_REF"))
             .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));
        }
    }
}
