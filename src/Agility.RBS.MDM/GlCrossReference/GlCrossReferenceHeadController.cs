﻿//-------------------------------------------------------------------------------------------------
// <copyright file="GlCrossReferenceController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// GlCrossReferenceController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.GlCrossReference
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Buyer.Models;
    using Agility.RBS.MDM.Buyer.Repositories;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.GlCrossReference.Models;
    using Agility.RBS.MDM.GlCrossReference.Repositories;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Models;
    using Agility.RBS.MDM.ValueAddedTax;
    using Agility.RBS.MDM.ValueAddedTax.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    [Route("api/[controller]/[action]")]

    public class GlCrossReferenceHeadController : Controller
    {
        private readonly ServiceDocument<GlCrossReferenceHeadModel> serviceDocument;
        private readonly GlCrossReferenceRepository glCrossReferenceRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private int fifGlCrossRefId;

        public GlCrossReferenceHeadController(
            ServiceDocument<GlCrossReferenceHeadModel> serviceDocument,
            GlCrossReferenceRepository glCrossReferenceRepository,
            DomainDataRepository domainDataRepository)
        {
            this.glCrossReferenceRepository = glCrossReferenceRepository;
            this.serviceDocument = serviceDocument;

            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<GlCrossReferenceHeadModel>> Open(int id)
        {
            this.fifGlCrossRefId = id;
            return await this.serviceDocument.OpenAsync(this.GlCrossRefOpen);
        }

        private async Task<GlCrossReferenceHeadModel> GlCrossRefOpen()
        {
            try
            {
                GlCrossReferenceHeadModel glCrossReference = await this.glCrossReferenceRepository.GetGlCrossRef(this.fifGlCrossRefId, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return glCrossReference;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }
    }
}
