﻿// <copyright file="GlCrossReferenceSearchModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// CostChangeReason
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.GlCrossReference.Models
{
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Core.Models;

    public class GlCrossReferenceSearchModel : ProfileEntity
    {
        public int? FifGlCrossRefId { get; set; }

        public int? Dept { get; set; }

        public string DeptName { get; set; }

        public int? Class { get; set; }

        public string ClassName { get; set; }

        public int? SubClass { get; set; }

        public string SubClassName { get; set; }

        public int? Location { get; set; }

        public string LocName { get; set; }

        public int? TranCode { get; set; }

        public string TranCodeDesc { get; set; }

        public string CostRetailFlag { get; set; }

        public string LineType { get; set; }

        public string LineTypeDesc { get; set; }

        public int? TranRefNo { get; set; }

        public string NbVatCode { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Error { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }

        public int? StartRow { get; set; }

        public int? EndRow { get; set; }

        public long? TotalRows { get; set; }

        public SortModel[] SortModel { get; set; }
    }
}
