﻿// <copyright file="GlCrossReferenceRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// CostZoneRepository
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.GlCrossReference.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.MDM.Buyer.Models;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.GlCrossReference.Models;
    using AutoMapper;
    using Devart.Data.Oracle;
    using Microsoft.AspNetCore.Authorization;

    public class GlCrossReferenceRepository : BaseOraclePackage
    {
        public GlCrossReferenceRepository()
        {
            this.PackageName = MdmConstants.GetFinancePackage;
        }

        public int FifGlCrossRefId { get; private set; }

        public async Task<List<GlCrossReferenceSearchModel>> GetGlCrossRefeSearchAsync(ServiceDocument<GlCrossReferenceSearchModel> serviceDoc, ServiceDocumentResult serviceDocumentResult)
        {
            GlCrossReferenceSearchModel glSearch = serviceDoc.DataProfile.DataModel;
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeGLSearchRef, MdmConstants.GetProcSearchGLRef);
            OracleObject recordObject = this.SetParamsGlCrossRefSearch(glSearch);

            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();

            BaseOracleParameter baseOracleParameter;
            baseOracleParameter = new BaseOracleParameter { Name = "P_START", DataType = OracleDbType.Number, Direction = ParameterDirection.Input, Value = Convert.ToString(glSearch.StartRow) };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            baseOracleParameter = new BaseOracleParameter { Name = "P_END", DataType = OracleDbType.Number, Direction = ParameterDirection.Input, Value = Convert.ToString(glSearch.EndRow) };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            baseOracleParameter = new BaseOracleParameter { Name = "P_total", DataType = OracleDbType.Number, Direction = ParameterDirection.Output };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            if (glSearch.SortModel == null)
            {
                glSearch.SortModel = new[] { new Core.Models.SortModel { ColId = null, Sort = null } };
            }

            baseOracleParameter = new BaseOracleParameter { Name = "P_SortColId", DataType = OracleDbType.VarChar, Direction = ParameterDirection.Input, Value = glSearch.SortModel[0].ColId };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            baseOracleParameter = new BaseOracleParameter { Name = "P_SortOrder", DataType = OracleDbType.VarChar, Direction = ParameterDirection.Input, Value = glSearch.SortModel[0].Sort };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            parameters.Add(this.ParameterBuilder(UserProfileHelper.GetUserParameter()));
            var glefList = await this.GetProcedure2<GlCrossReferenceSearchModel>(recordObject, packageParameter, serviceDocumentResult, parameters);
            glSearch.TotalRows = (this.Parameters["p_total"].Value != null && this.Parameters["p_total"].Value != DBNull.Value) ? Convert.ToInt64(this.Parameters["p_total"].Value) : 1;
            serviceDoc.DataProfile.DataModel = glSearch;
            return glefList;
        }

        public async Task<GlCrossReferenceHeadModel> GetGlCrossRef(int fifGlCrossRefId, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType(MdmConstants.RecordTypeSetGLRef);
            OracleObject recordObject = this.GetOracleObject(recordType);
            this.SetFifGlCrossRefId(fifGlCrossRefId, recordObject);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeSetRef, MdmConstants.GetProcGLRef);
            var glCrossRefs = await this.GetProcedure2<GlCrossReferenceHeadModel>(recordObject, packageParameter, serviceDocumentResult);
            return glCrossRefs != null ? glCrossRefs[0] : null;
        }

        public virtual void SetFifGlCrossRefId(int fifGlCrossRefId, OracleObject recordObject)
        {
            recordObject["FIF_GL_CROSS_REF_ID"] = fifGlCrossRefId;
        }

        private OracleObject SetParamsGlCrossRefSearch(GlCrossReferenceSearchModel glCrossRefSearchModel)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(MdmConstants.RecordTypeGLRef, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["Operation"]] = null;
            recordObject[recordType.Attributes["FIF_GL_CROSS_REF_ID"]] = glCrossRefSearchModel.FifGlCrossRefId;
            recordObject[recordType.Attributes["DEPT"]] = glCrossRefSearchModel.Dept;
            recordObject[recordType.Attributes["CLASS"]] = glCrossRefSearchModel.Class;
            recordObject[recordType.Attributes["SUBCLASS"]] = glCrossRefSearchModel.SubClassName;
            recordObject[recordType.Attributes["LOCATION"]] = glCrossRefSearchModel.Location;
            recordObject[recordType.Attributes["TRAN_CODE"]] = glCrossRefSearchModel.TranCode;
            return recordObject;
        }
    }
}