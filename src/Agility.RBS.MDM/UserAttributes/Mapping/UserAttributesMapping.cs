﻿// <copyright file="UserAttributesMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.UserAttributes.Mapping
{
    using System;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.Brand.Models;
    using Agility.RBS.MDM.CostZone.Models;
    using Agility.RBS.MDM.Employee.Models;
    using Agility.RBS.MDM.UserAttributes.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class UserAttributesMapping : Profile
    {
        public UserAttributesMapping()
            : base("UserAttributesMapping")
        {
            this.CreateMap<OracleObject, UserAttributesModel>()
                .ForMember(m => m.UserId, opt => opt.MapFrom(r => r["user_id"]))
                .ForMember(m => m.UserName, opt => opt.MapFrom(r => r["user_name"]))
                .ForMember(m => m.Lang, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["lang"])))
                .ForMember(m => m.LanguageDesc, opt => opt.MapFrom(r => r["lang_name"]))
                .ForMember(m => m.StoreDefault, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["store_default"])))
                .ForMember(m => m.StoreName, opt => opt.MapFrom(r => r["store_name"]))
                .ForMember(m => m.Phone, opt => opt.MapFrom(r => r["user_phone"]))
                .ForMember(m => m.Fax, opt => opt.MapFrom(r => r["user_fax"]))
                .ForMember(m => m.Pager, opt => opt.MapFrom(r => r["user_pager"]))
                .ForMember(m => m.Email, opt => opt.MapFrom(r => r["user_email"]))
                .ForMember(m => m.Printer, opt => opt.MapFrom(r => r["default_printer"]))
                .ForMember(m => m.PrinterName, opt => opt.MapFrom(r => r["printer_name"]))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "User_Attrib"))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));
        }
    }
}
