﻿// <copyright file="UserAttributesRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// BrandRepository
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.UserAttributes.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Employee.Models;
    using Agility.RBS.MDM.UserAttributes.Models;
    using AutoMapper;
    using Devart.Data.Oracle;
    using Microsoft.AspNetCore.Authorization;

    public class UserAttributesRepository : BaseOraclePackage
    {
        public UserAttributesRepository()
        {
            this.PackageName = "UTL_CTRL_MST_DATA";
        }

        public async Task<List<UserAttributesModel>> GetUserAttributes(UserAttributesModel userAttributesModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams("user_attrib_tab", "getuserattrib");
            OracleObject recordObject = this.SetParamsUserAttributes(userAttributesModel, true);
            return await this.GetProcedure2<UserAttributesModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ServiceDocumentResult> SetUserAttributes(UserAttributesModel userAttributesModel)
        {
            PackageParams packageParameter = this.GetPackageParams("user_attrib_tab", "setuserattrib");
            OracleObject recordObject = this.SetParamsUserAttributes(userAttributesModel, false);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public virtual void SetParamsUserAttributesSearch(UserAttributesModel userAttributesModel, OracleObject recordObject)
        {
            recordObject["user_id"] = userAttributesModel.UserId;
            recordObject["user_name"] = userAttributesModel.UserName;
            recordObject["lang"] = userAttributesModel.Lang;
            recordObject["lang_name"] = userAttributesModel.LanguageDesc;
            recordObject["store_default"] = userAttributesModel.StoreDefault;
            recordObject["store_name"] = userAttributesModel.StoreName;
            recordObject["user_phone"] = userAttributesModel.Phone;
            recordObject["user_fax"] = userAttributesModel.Fax;
            recordObject["user_pager"] = userAttributesModel.Pager;
            recordObject["user_email"] = userAttributesModel.Email;
            recordObject["default_printer"] = userAttributesModel.Printer;
            recordObject["printer_name"] = userAttributesModel.PrinterName;
        }

        private OracleObject SetParamsUserAttributes(UserAttributesModel userAttributesModel, bool isSearch)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("user_attrib_rec");
            OracleObject recordObject = this.GetOracleObject(recordType);
            if (!isSearch)
            {
                recordObject["Operation"] = userAttributesModel.Operation;
            }

            this.SetParamsUserAttributesSearch(userAttributesModel, recordObject);
            return recordObject;
        }
    }
}