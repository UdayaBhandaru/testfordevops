﻿// <copyright file="UserAttributesModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// BrandModel
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.UserAttributes.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class UserAttributesModel : ProfileEntity
    {
        public string UserId { get; set; }

        public string UserName { get; set; }

        public int? Lang { get; set; }

        public string LanguageDesc { get; set; }

        public int? StoreDefault { get; set; }

        public string StoreName { get; set; }

        public string Phone { get; set; }

        public string Fax { get; set; }

        public string Pager { get; set; }

        public string Email { get; set; }

        public string Printer { get; set; }

        public string PrinterName { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Error { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }
    }
}
