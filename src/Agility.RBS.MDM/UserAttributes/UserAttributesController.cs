﻿//-------------------------------------------------------------------------------------------------
// <copyright file="UserAttributesController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// BrandController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.UserAttributes
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Employee.Models;
    using Agility.RBS.MDM.Employee.Repositories;
    using Agility.RBS.MDM.Location.Models;
    using Agility.RBS.MDM.Location.Repositories;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.UserAttributes.Models;
    using Agility.RBS.MDM.UserAttributes.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    public class UserAttributesController : Controller
    {
        private readonly ServiceDocument<UserAttributesModel> serviceDocument;
        private readonly UserAttributesRepository userAttributesRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly DomainDataRepository domainDataRepository;
        private readonly LocationRepository locationRepository;

        public UserAttributesController(
            ServiceDocument<UserAttributesModel> serviceDocument,
            LocationRepository locationRepository,
            UserAttributesRepository userAttributesRepository,
             DomainDataRepository domainDataRepository)
        {
            this.userAttributesRepository = userAttributesRepository;
            this.serviceDocument = serviceDocument;
            this.domainDataRepository = domainDataRepository;
            this.locationRepository = locationRepository;

            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<UserAttributesModel>> List()
        {
            this.serviceDocument.DomainData.Add("language", this.domainDataRepository.LanguageDomainGet().Result);
            this.serviceDocument.DomainData.Add("indicator", this.domainDataRepository.DomainDetailData(MdmConstants.VatRegionTypeDomainData).Result);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<UserAttributesModel>> New()
        {
            this.serviceDocument.DomainData.Add("language", this.domainDataRepository.LanguageDomainGet().Result);
            this.serviceDocument.DomainData.Add("indicator", this.domainDataRepository.DomainDetailData(MdmConstants.VatRegionTypeDomainData).Result);
            this.serviceDocument.DomainData.Add("storeData", this.locationRepository.GetLocation(new StoreModel(), this.serviceDocumentResult).Result);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<UserAttributesModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.UserAttributesSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<UserAttributesModel>> Open(string id)
        {
            this.serviceDocument.DataProfile.DataModel = new UserAttributesModel { UserId = id };
            this.serviceDocument.DomainData.Add("language", this.domainDataRepository.LanguageDomainGet().Result);
            this.serviceDocument.DomainData.Add("indicator", this.domainDataRepository.DomainDetailData(MdmConstants.VatRegionTypeDomainData).Result);
            this.serviceDocument.DomainData.Add("storeData", this.locationRepository.GetLocation(new StoreModel(), this.serviceDocumentResult).Result);
            return await this.serviceDocument.OpenAsync(this.EmpOpen);
        }

        public async Task<ServiceDocument<UserAttributesModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.UserAttributesSave);
            return this.serviceDocument;
        }

        private async Task<List<UserAttributesModel>> UserAttributesSearch()
        {
            try
            {
                var lstUomClass = await this.userAttributesRepository.GetUserAttributes(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstUomClass;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<UserAttributesModel> EmpOpen()
        {
            try
            {
                var lstUomClass = await this.userAttributesRepository.GetUserAttributes(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstUomClass?[0];
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> UserAttributesSave()
        {
            this.serviceDocument.Result = await this.userAttributesRepository.SetUserAttributes(this.serviceDocument.DataProfile.DataModel);
            return true;
        }
    }
}
