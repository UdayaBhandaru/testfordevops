﻿// <copyright file="WareHouseModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// CostChangeReason
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.WareHouse.Models
{
    using System;
    using Agility.Framework.Core.Profile.Entities;

    public class WareHouseModel : ProfileEntity
    {
        public WareHouseModel()
        {
            this.Operation = "I";
        }

        public int? Wh { get; set; }

        public string WhName { get; set; }

        public string WhNameSecondary { get; set; }

        public string Email { get; set; }

        public int? VatRegion { get; set; }

        public string RegionName { get; set; }

        public int? OrgHierType { get; set; }

        public int? OrgHierValue { get; set; }

        public string CurrencyCode { get; set; }

        public int? PhysicalWh { get; set; }

        public int? PrimaryVwh { get; set; }

        public int? ChannelId { get; set; }

        public string StockHoldingInd { get; set; }

        public string BreakPackInd { get; set; }

        public string RedistWhInd { get; set; }

        public string DeliveryPolicy { get; set; }

        public string RestrictedInd { get; set; }

        public string ProtectedInd { get; set; }

        public string ForecastWhInd { get; set; }

        public int? RoundingSeq { get; set; }

        public string ReplInd { get; set; }

        public int? ReplWhLink { get; set; }

        public int? ReplSrcOrd { get; set; }

        public string IbInd { get; set; }

        public int? IbWhLink { get; set; }

        public string AutoIbClear { get; set; }

        public string DunsNumber { get; set; }

        public string DunsLoc { get; set; }

        public int? TsfEntityId { get; set; }

        public string FinisherInd { get; set; }

        public int? InboundHandlingDays { get; set; }

        public int? OrgUnitId { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }

        public string Error { get; set; }

        public string ProgmPhase { get; set; }

        public string PerrorMessage { get; set; }
    }
}
