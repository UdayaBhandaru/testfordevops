﻿// <copyright file="WareHouseMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.WareHouse.Mapping
{
    using System;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.WareHouse.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class WareHouseMapping : Profile
    {
        public WareHouseMapping()
            : base("WareHouseMapping")
        {
            this.CreateMap<OracleObject, WareHouseModel>()
                  .ForMember(m => m.Wh, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["WH"])))
                  .ForMember(m => m.WhName, opt => opt.MapFrom(r => r["WH_NAME"]))
                  .ForMember(m => m.WhNameSecondary, opt => opt.MapFrom(r => r["WH_NAME_SECONDARY"]))
                  .ForMember(m => m.Email, opt => opt.MapFrom(r => r["EMAIL"]))
                  .ForMember(m => m.VatRegion, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["VAT_REGION"])))
                  .ForMember(m => m.OrgHierType, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["ORG_HIER_TYPE"])))
                  .ForMember(m => m.OrgHierValue, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["ORG_HIER_VALUE"])))
                  .ForMember(m => m.CurrencyCode, opt => opt.MapFrom(r => r["CURRENCY_CODE"]))
                  .ForMember(m => m.PhysicalWh, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["PHYSICAL_WH"])))
                  .ForMember(m => m.PrimaryVwh, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["PRIMARY_VWH"])))
                  .ForMember(m => m.ChannelId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["CHANNEL_ID"])))
                  .ForMember(m => m.StockHoldingInd, opt => opt.MapFrom(r => r["STOCKHOLDING_IND"]))
                  .ForMember(m => m.BreakPackInd, opt => opt.MapFrom(r => r["BREAK_PACK_IND"]))
                  .ForMember(m => m.RedistWhInd, opt => opt.MapFrom(r => r["REDIST_WH_IND"]))
                  .ForMember(m => m.DeliveryPolicy, opt => opt.MapFrom(r => r["DELIVERY_POLICY"]))
                  .ForMember(m => m.RestrictedInd, opt => opt.MapFrom(r => r["RESTRICTED_IND"]))
                  .ForMember(m => m.ProtectedInd, opt => opt.MapFrom(r => r["PROTECTED_IND"]))
                  .ForMember(m => m.ForecastWhInd, opt => opt.MapFrom(r => r["FORECAST_WH_IND"]))
                  .ForMember(m => m.RoundingSeq, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["ROUNDING_SEQ"])))
                  .ForMember(m => m.ReplInd, opt => opt.MapFrom(r => r["REPL_IND"]))
                  .ForMember(m => m.ReplWhLink, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["REPL_WH_LINK"])))
                  .ForMember(m => m.ReplSrcOrd, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["REPL_SRC_ORD"])))
                  .ForMember(m => m.IbInd, opt => opt.MapFrom(r => r["IB_IND"]))
                  .ForMember(m => m.IbWhLink, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["IB_WH_LINK"])))
                  .ForMember(m => m.AutoIbClear, opt => opt.MapFrom(r => r["AUTO_IB_CLEAR"]))
                  .ForMember(m => m.DunsNumber, opt => opt.MapFrom(r => r["DUNS_NUMBER"]))
                  .ForMember(m => m.DunsLoc, opt => opt.MapFrom(r => r["DUNS_LOC"]))
                  .ForMember(m => m.TsfEntityId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["TSF_ENTITY_ID"])))
                  .ForMember(m => m.FinisherInd, opt => opt.MapFrom(r => r["FINISHER_IND"]))
                  .ForMember(m => m.InboundHandlingDays, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["INBOUND_HANDLING_DAYS"])))
                  .ForMember(m => m.OrgUnitId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["ORG_UNIT_ID"])))
                  .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
                  .ForMember(m => m.ProgmPhase, opt => opt.MapFrom(r => r["PROGRAM_PHASE"]))
                  .ForMember(m => m.PerrorMessage, opt => opt.MapFrom(r => r["PROGRAM_MESSAGE"]))
                  .ForMember(m => m.TableName, opt => opt.MapFrom(r => "STORE"))
                  .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));
        }
    }
}
