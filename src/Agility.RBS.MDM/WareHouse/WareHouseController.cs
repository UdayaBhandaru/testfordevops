﻿//-------------------------------------------------------------------------------------------------
// <copyright file="WareHouseController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// WareHouseFormatController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.WareHouse
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.OrganizationHierarchy;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Transfer;
    using Agility.RBS.MDM.Transfer.Models;
    using Agility.RBS.MDM.WareHouse.Models;
    using Agility.RBS.MDM.WareHouse.Models;
    using Agility.RBS.MDM.WareHouse.Repositories;
    using Agility.RBS.MDM.WareHouse.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    [Route("api/[controller]/[action]")]

    public class WareHouseController : Controller
    {
        private readonly ServiceDocument<WareHouseModel> serviceDocument;
        private readonly WareHouseRepository wareHouseRepository;
        private readonly TransferRepository transferRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly DomainDataRepository domainDataRepository;

        public WareHouseController(
            ServiceDocument<WareHouseModel> serviceDocument,
            WareHouseRepository wareHouseRepository,
            DomainDataRepository domainDataRepository,
            TransferRepository transferRepository)
        {
            this.wareHouseRepository = wareHouseRepository;
            this.serviceDocument = serviceDocument;
            this.domainDataRepository = domainDataRepository;
            this.transferRepository = transferRepository;

            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        [HttpPost]
        public async Task<ServiceDocument<WareHouseModel>> List()
        {
            return await this.WareHouseDomainData();
        }

        public async Task<ServiceDocument<WareHouseModel>> New()
        {
            return await this.WareHouseDomainData();
        }

        [HttpPost]
        public async Task<ServiceDocument<WareHouseModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.WareHouseSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<WareHouseModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.WareHouseSave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingWareHouse(int wh)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "WH";
            nvc["P_COL1"] = wh.ToString();
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        private async Task<List<WareHouseModel>> WareHouseSearch()
        {
            try
            {
                var wareHouses = await this.wareHouseRepository.GetWareHouse(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return wareHouses;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> WareHouseSave()
        {
            this.serviceDocument.Result = await this.wareHouseRepository.SetWareHouse(this.serviceDocument.DataProfile.DataModel);
            return true;
        }

        private async Task<ServiceDocument<WareHouseModel>> WareHouseDomainData()
        {
            TransferZoneModel transferZoneModel = new TransferZoneModel();
            TransferEntityModel transferEntityModel = new TransferEntityModel();
            this.serviceDocument.DomainData.Add("deliveryPolicy", this.domainDataRepository.DomainDetailData(MdmConstants.DomainHdrDelivery).Result);
            var transferZones = await this.transferRepository.GetTransferZoneAsync(transferZoneModel, this.serviceDocumentResult);
            this.serviceDocument.DomainData.Add("TransferZones", transferZones);
            var transferEntities = await this.transferRepository.GetTransferEntityAsync(transferEntityModel, this.serviceDocumentResult);
            this.serviceDocument.DomainData.Add("TransferEntities", transferEntities);
            this.serviceDocument.DomainData.Add("indicatorStatus", this.domainDataRepository.DomainDetailData(MdmConstants.VatRegionTypeDomainData).Result);
            this.serviceDocument.DomainData.Add("vatRegionCode", this.domainDataRepository.VatRegionDomainGet().Result);
            this.serviceDocument.DomainData.Add("currency", this.domainDataRepository.CurrencyDomainGet().Result);
            this.serviceDocument.DomainData.Add("language", this.domainDataRepository.LanguageDomainGet().Result);
            this.serviceDocument.DomainData.Add("locType", this.domainDataRepository.LocationTypeDomainGet().Result);
            this.serviceDocument.LocalizationData.AddData("WareHouse");
            return this.serviceDocument;
        }
    }
}
