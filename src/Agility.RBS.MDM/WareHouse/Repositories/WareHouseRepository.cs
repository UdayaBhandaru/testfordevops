﻿// <copyright file="WareHouseRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// CostZoneRepository
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.WareHouse.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.StoreFormat.Models;
    using Agility.RBS.MDM.WareHouse.Models;
    using AutoMapper;
    using Devart.Data.Oracle;
    using Microsoft.AspNetCore.Authorization;

    public class WareHouseRepository : BaseOraclePackage
    {
        public WareHouseRepository()
        {
            this.PackageName = MdmConstants.GetWareHousePackage;
        }

        public async Task<List<WareHouseModel>> GetWareHouse(WareHouseModel wareHouseModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeWareHouse, MdmConstants.GetProcWareHouse);
            OracleObject recordObject = this.SetParamsWareHouse(wareHouseModel, true);
            return await this.GetProcedure2<WareHouseModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ServiceDocumentResult> SetWareHouse(WareHouseModel wareHouseModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeWareHouse, MdmConstants.SetProcWareHouse);
            OracleObject recordObject = this.SetParamsWareHouse(wareHouseModel, false);
            ServiceDocumentResult sRes = await this.SetProcedure(recordObject, packageParameter);
            return sRes;
        }

        public virtual void SetParamsWareHouseSave(WareHouseModel wareHouseModel, OracleObject recordObject)
        {
            recordObject["Operation"] = wareHouseModel.Operation;
            recordObject["WH"] = wareHouseModel.Wh;
            recordObject["WH_NAME"] = wareHouseModel.WhName;
            recordObject["WH_NAME_SECONDARY"] = wareHouseModel.WhNameSecondary;
            recordObject["EMAIL"] = wareHouseModel.Email;
            recordObject["VAT_REGION"] = wareHouseModel.VatRegion;
            recordObject["ORG_HIER_TYPE"] = wareHouseModel.OrgHierType;
            recordObject["ORG_HIER_VALUE"] = wareHouseModel.OrgHierValue;
            recordObject["CURRENCY_CODE"] = wareHouseModel.CurrencyCode;
            recordObject["PHYSICAL_WH"] = wareHouseModel.PhysicalWh;
            recordObject["PRIMARY_VWH"] = wareHouseModel.PrimaryVwh;
            recordObject["CHANNEL_ID"] = wareHouseModel.ChannelId;
            recordObject["STOCKHOLDING_IND"] = wareHouseModel.StockHoldingInd;
            recordObject["BREAK_PACK_IND"] = wareHouseModel.BreakPackInd;
            recordObject["REDIST_WH_IND"] = wareHouseModel.RedistWhInd;
            recordObject["DELIVERY_POLICY"] = wareHouseModel.DeliveryPolicy;
            recordObject["RESTRICTED_IND"] = wareHouseModel.RestrictedInd;
            recordObject["PROTECTED_IND"] = wareHouseModel.ProtectedInd;
            recordObject["FORECAST_WH_IND"] = wareHouseModel.ForecastWhInd;
            recordObject["ROUNDING_SEQ"] = wareHouseModel.RoundingSeq;
            recordObject["REPL_IND"] = wareHouseModel.ReplInd;
            recordObject["REPL_WH_LINK"] = wareHouseModel.ReplWhLink;
            recordObject["REPL_SRC_ORD"] = wareHouseModel.ReplSrcOrd;
            recordObject["IB_IND"] = wareHouseModel.IbInd;
            recordObject["IB_WH_LINK"] = wareHouseModel.IbWhLink;
            recordObject["AUTO_IB_CLEAR"] = wareHouseModel.AutoIbClear;
            recordObject["DUNS_NUMBER"] = wareHouseModel.DunsNumber;
            recordObject["DUNS_LOC"] = wareHouseModel.DunsLoc;
            recordObject["TSF_ENTITY_ID"] = wareHouseModel.TsfEntityId;
            recordObject["FINISHER_IND"] = wareHouseModel.FinisherInd;
            recordObject["INBOUND_HANDLING_DAYS"] = wareHouseModel.InboundHandlingDays;
            recordObject["ORG_UNIT_ID"] = wareHouseModel.OrgUnitId;
        }

        public virtual void SetParamsWareHouseSearch(WareHouseModel wareHouseModel, OracleObject recordObject)
        {
            recordObject["WH"] = wareHouseModel.Wh;
            recordObject["WH_NAME"] = wareHouseModel.WhName;
            recordObject["WH_NAME_SECONDARY"] = wareHouseModel.WhNameSecondary;
        }

        private OracleObject SetParamsWareHouse(WareHouseModel wareHouseModel, bool isSearch)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("WH_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            if (!isSearch)
            {
                this.SetParamsWareHouseSave(wareHouseModel, recordObject);
            }

            this.SetParamsWareHouseSearch(wareHouseModel, recordObject);

            return recordObject;
        }
    }
}