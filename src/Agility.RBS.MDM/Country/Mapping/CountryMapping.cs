﻿// <copyright file="CountryMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Country.Mapping
{
    using System;
    using Agility.RBS.MDM.Country.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class CountryMapping : Profile
    {
        public CountryMapping()
            : base("CountryMapping")
        {
            this.CreateMap<OracleObject, CountryModel>()
                .ForMember(m => m.CountryId, opt => opt.MapFrom(r => r["COUNTRY_ID"]))
                .ForMember(m => m.CountryDesc, opt => opt.MapFrom(r => r["COUNTRY_DESC"]))
                .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "COUNTRY_MST"))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));
        }
    }
}
