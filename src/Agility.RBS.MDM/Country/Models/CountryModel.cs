﻿// <copyright file="CountryModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Country.Models
{
    using System;
    using Agility.Framework.Core.Profile.Entities;

    public class CountryModel : ProfileEntity
    {
        public string CountryId { get; set; }

        public string CountryDesc { get; set; }

        public string CountryName { get; set; }

        public string CountryCode { get; set; }

        public string CountryCode1 { get; set; }

        public string BaseCurrencyCode { get; set; }

        public string BaseCurrencyName { get; set; }

        public string AlternateCurrencyCode { get; set; }

        public string AlternateCurrencyName { get; set; }

        public string CountryFlag { get; set; }

        public string AddressFormat { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }

        public string CountryStatus { get; set; }

        public string CountryStatusDesc { get; set; }

        public string Error { get; set; }
    }
}
