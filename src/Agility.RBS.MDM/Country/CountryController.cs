﻿// <copyright file="CountryController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Country
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Country.Models;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class CountryController : Controller
    {
        private readonly ServiceDocument<CountryModel> serviceDocument;
        private readonly CountryRepository countryRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly DomainDataRepository domainDataRepository;

        public CountryController(
            ServiceDocument<CountryModel> serviceDocument,
            CountryRepository countryRepository,
            DomainDataRepository domainDataRepository)
        {
            this.countryRepository = countryRepository;
            this.serviceDocument = serviceDocument;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        [HttpPost]
        public async Task<ServiceDocument<CountryModel>> List()
        {
            return await this.CountryDomainData();
        }

        [HttpPost]
        public async Task<ServiceDocument<CountryModel>> New()
        {
            return await this.CountryDomainData();
        }

        [HttpPost]
        public async Task<ServiceDocument<CountryModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.CountrySearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<CountryModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.CountrySave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingCountry(string countryId)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "COUNTRY";
            nvc["P_COL1"] = countryId;
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        private async Task<List<CountryModel>> CountrySearch()
        {
            try
            {
                var phases = await this.countryRepository.GetCountry(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return phases;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> CountrySave()
        {
            this.serviceDocument.Result = await this.countryRepository.SetCountry(this.serviceDocument.DataProfile.DataModel);
            return true;
        }

        private async Task<ServiceDocument<CountryModel>> CountryDomainData()
        {
            this.serviceDocument.DomainData.Add("status", this.domainDataRepository.DomainDetailData(MdmConstants.DomainHdrStatus).Result);
            this.serviceDocument.DomainData.Add("currency", this.domainDataRepository.CurrencyDomainGet().Result);
            this.serviceDocument.LocalizationData.AddData("Country");
            return this.serviceDocument;
        }
    }
}
