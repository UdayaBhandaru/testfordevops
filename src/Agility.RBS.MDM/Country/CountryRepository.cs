﻿// <copyright file="CountryRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Country
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Country.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class CountryRepository : BaseOraclePackage
    {
        public CountryRepository()
        {
            this.PackageName = MdmConstants.GetCountryPackage;
        }

        public async Task<List<CountryModel>> GetCountry(CountryModel countryModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeCountry, MdmConstants.GetProcCountry);
            OracleObject recordObject = this.SetParamsCountry(countryModel, true);
            return await this.GetProcedure2<CountryModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ServiceDocumentResult> SetCountry(CountryModel seasonModel)
        {
            this.PackageName = MdmConstants.GetCountryPackage;
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeCountry, MdmConstants.SetProcCountry);
            OracleObject recordObject = this.SetParamsCountry(seasonModel, false);
            ServiceDocumentResult sRes = await this.SetProcedure(recordObject, packageParameter);
            return sRes;
        }

        private OracleObject SetParamsCountry(CountryModel countryModel, bool isSearch)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType("COUNTRY_REC", this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            if (!isSearch)
            {
                recordObject[recordType.Attributes["Operation"]] = countryModel.Operation;
            }

            recordObject[recordType.Attributes["COUNTRY_ID"]] = countryModel.CountryId;
            recordObject[recordType.Attributes["COUNTRY_DESC"]] = countryModel.CountryDesc;
            return recordObject;
        }
    }
}