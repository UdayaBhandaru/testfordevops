﻿//-------------------------------------------------------------------------------------------------
// <copyright file="CategoryRoleController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// CategoryRoleController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.MerchantHierarchy
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.MerchantHierarchy.Models;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class CategoryRoleController : Controller
    {
        private readonly ServiceDocument<CategoryRoleModel> serviceDocument;
        private readonly MerchantHierarchyRepository merchantHierarchyRepository;
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;

        public CategoryRoleController(
            ServiceDocument<CategoryRoleModel> serviceDocument,
            MerchantHierarchyRepository merchantHierarchyRepository,
            DomainDataRepository domainDataRepository)
        {
            this.merchantHierarchyRepository = merchantHierarchyRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<CategoryRoleModel>> List()
        {
            return await this.CategoryRoleDomainData();
        }

        public async Task<ServiceDocument<CategoryRoleModel>> New()
        {
            this.serviceDocument.DomainData.Add("strategytype", this.domainDataRepository.DomainDetailData(MdmConstants.DomainHdrStrgytype).Result);
            this.serviceDocument.DomainData.Add("indicator", this.domainDataRepository.DomainDetailData(MdmConstants.DomainHdrIndicator).Result);
            return await this.CategoryRoleDomainData();
        }

        public async Task<ServiceDocument<CategoryRoleModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.CategoryRoleSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<CategoryRoleModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.CategoryRoleSave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingCategoryRole(int companyId, int divisionId, int deptId, int categoryId, string roleId, string roleLevel)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "CAT_ROLE_MASTER";
            nvc["P_COL1"] = companyId.ToString();
            nvc["P_COL2"] = divisionId.ToString();
            nvc["P_COL3"] = deptId.ToString();
            nvc["P_COL4"] = categoryId.ToString();
            nvc["P_COL5"] = roleLevel;
            nvc["P_COL6"] = roleId;
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        private async Task<List<CategoryRoleModel>> CategoryRoleSearch()
        {
            try
            {
                var catRoles = await this.merchantHierarchyRepository.GetCategoryRole(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return catRoles;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> CategoryRoleSave()
        {
            this.serviceDocument.Result = await this.merchantHierarchyRepository.SetCategoryRole(this.serviceDocument.DataProfile.DataModel);
            return true;
        }

        private async Task<ServiceDocument<CategoryRoleModel>> CategoryRoleDomainData()
        {
            this.serviceDocument.DomainData.Add("company", this.domainDataRepository.CompanyDomainGet().Result);
            this.serviceDocument.DomainData.Add("division", this.domainDataRepository.DivisionDomainGet().Result);
            this.serviceDocument.DomainData.Add("group", this.domainDataRepository.GroupDomainGet().Result);
            this.serviceDocument.DomainData.Add("department", this.domainDataRepository.DepartmentDomainGet().Result);
            this.serviceDocument.DomainData.Add("rolelevel", this.domainDataRepository.DomainDetailData(MdmConstants.DomainHdrSpacelevel).Result);
            this.serviceDocument.DomainData.Add("companylevel", this.domainDataRepository.CommonData("COMPANY").Result);
            this.serviceDocument.DomainData.Add("orgcountrylevel", this.domainDataRepository.CommonData("COUNTRY").Result);
            this.serviceDocument.DomainData.Add("formatlevel", this.domainDataRepository.CommonData("FORMAT").Result);
            this.serviceDocument.DomainData.Add("regionlevel", this.domainDataRepository.CommonData("REGION").Result);
            this.serviceDocument.LocalizationData.AddData("CategoryRole");
            return this.serviceDocument;
        }
    }
}
