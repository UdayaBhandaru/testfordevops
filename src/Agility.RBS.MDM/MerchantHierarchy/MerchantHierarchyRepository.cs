﻿// <copyright file="MerchantHierarchyRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.MerchantHierarchy
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.MerchantHierarchy.Models;
    using Agility.RBS.MDM.UtilityDomain.Models;
    using AutoMapper;
    using Devart.Data.Oracle;
    using Microsoft.Extensions.Caching.Distributed;

    public class MerchantHierarchyRepository : BaseOraclePackage
    {
        public MerchantHierarchyRepository()
        {
            this.PackageName = MdmConstants.GetMerchantHierarchyPackage;
        }

        public async Task<List<ClassModel>> GetClass(ClassModel classModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeClass, MdmConstants.GetProcClass);
            OracleObject recordObject = this.SetParamsClass(classModel, true);
            OracleTable pGetUsersMstTab = await this.GetProcedure(recordObject, packageParameter, serviceDocumentResult);
            var lstHeader = Mapper.Map<List<ClassModel>>(pGetUsersMstTab);
            if (lstHeader != null)
            {
                for (int i = 0; i < lstHeader.Count; i++)
                {
                    OracleObject obj = (OracleObject)pGetUsersMstTab[i];
                    OracleTable classlocationDetails = (Devart.Data.Oracle.OracleTable)obj["sub_class"];
                    lstHeader[i].SubClassDetailList.AddRange(Mapper.Map<List<SubClassDetailModel>>(classlocationDetails));
                }
            }

            return lstHeader;
        }

        public async Task<ServiceDocumentResult> SetClass(ClassModel classModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeClass, MdmConstants.SetProcClass);
            OracleObject recordObject = this.SetParamsClass(classModel, false);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async Task<List<DivisionModel>> GetDivision(DivisionModel divisionModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDivision, MdmConstants.GetProcDivision);
            OracleObject recordObject = this.SetParamsDivision(divisionModel);
            return await this.GetProcedure2<DivisionModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<List<GroupModel>> GetGroup(GroupModel departmentModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDepartment, MdmConstants.GetProcDepartment);
            OracleObject recordObject = this.SetParamsGroup(departmentModel);
            return await this.GetProcedure2<GroupModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<List<DepartmentModel>> GetDepartment(DepartmentModel categoryModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeCategory, MdmConstants.GetProcCategory);
            OracleObject recordObject = this.SetParamsDepartment(categoryModel);
            return await this.GetProcedure2<DepartmentModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<List<SubClassModel>> GetSubClass(SubClassModel subClassModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeSubClass, MdmConstants.GetProcSubClass);
            OracleObject recordObject = this.SetParamsSubClass(subClassModel);
            return await this.GetProcedure2<SubClassModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<List<CategoryRoleModel>> GetCategoryRole(CategoryRoleModel categoryRoleModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeCategoryRole, MdmConstants.GetProcCategoryRole);
            OracleObject recordObject = this.SetParamsCategoryRole(categoryRoleModel, true);
            return await this.GetProcedure2<CategoryRoleModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ServiceDocumentResult> SetCategoryRole(CategoryRoleModel categoryRoleModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeCategoryRole, MdmConstants.SetProcCategoryRole);
            OracleObject recordObject = this.SetParamsCategoryRole(categoryRoleModel, false);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async Task<List<MerchantSpaceModel>> GetMerchantSpace(MerchantSpaceModel merchantSpaceModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeMerchantSpace, MdmConstants.GetProcMerchantSpace);
            OracleObject recordObject = this.SetParamsMerchantSpace(merchantSpaceModel, true);
            return await this.GetProcedure2<MerchantSpaceModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ServiceDocumentResult> SetMerchantSpace(MerchantSpaceModel merchantSpaceModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeMerchantSpace, MdmConstants.SetProcMerchantSpace);
            OracleObject recordObject = this.SetParamsMerchantSpace(merchantSpaceModel, false);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async ValueTask<bool> IsExistingDivision(int id)
        {
            int result;
            OracleCommand myCommand = new OracleCommand("Select count(1) from DIVISION where DIVISION = " + id);
            myCommand.Connection = this.Connection;
            this.Connection.Open();
            result = Convert.ToInt32(await myCommand.ExecuteScalarAsync());
            this.Connection.Close();
            return result > 0;
        }

        public virtual void SetParamsDivisionSearch(DivisionModel divisionModel, OracleObject recordObject)
        {
            recordObject["DIVISION"] = divisionModel.Division;
            recordObject["DIV_NAME"] = divisionModel.DivName;
            recordObject["BUYER"] = divisionModel.Buyer;
            recordObject["MERCH"] = divisionModel.Merch;
        }

        public virtual void SetParamsGroupSearch(GroupModel departmentModel, OracleObject recordObject)
        {
            recordObject["GROUP_NO"] = departmentModel.GroupNo;
            recordObject["GROUP_NAME"] = departmentModel.GroupName;
            recordObject["DIVISION"] = departmentModel.Division;
        }

        public virtual void SetParamsDepartmentSearch(DepartmentModel categoryModel, OracleObject recordObject)
        {
            recordObject["GROUP_NO"] = categoryModel.GroupNo;
            recordObject["Dept_Name"] = categoryModel.DeptName;
            recordObject["DEPT"] = categoryModel.Dept;
        }

        public virtual void SetParamsSubClassSearch(SubClassModel subClassModel, OracleObject recordObject)
        {
            recordObject["DEPT"] = subClassModel.Dept;
            recordObject["SUBCLASS"] = subClassModel.Subclass;
            recordObject["CLASS"] = subClassModel.Class;
            recordObject["SUB_NAME"] = subClassModel.SubName;
        }

        public virtual void SetParamsCategoryRoleSearch(CategoryRoleModel categoryRoleModel, OracleObject recordObject)
        {
            recordObject["COMPANY_ID"] = categoryRoleModel.CompanyId;
            recordObject["DIVISION_ID"] = categoryRoleModel.DivisionId;
            recordObject["DEPT_ID"] = categoryRoleModel.DeptId;
            recordObject["CATEGORY_ID"] = categoryRoleModel.CategoryId;
            recordObject["ROLE_LEVEL"] = categoryRoleModel.RoleLevel;
            recordObject["ROLE_ID"] = categoryRoleModel.RoleId;
        }

        public virtual void SetParamsCategoryRoleSave(CategoryRoleModel categoryRoleModel, OracleObject recordObject)
        {
            recordObject["STRATEGY_TYPE"] = categoryRoleModel.StrategyType;
            recordObject["CATEGORY_ROLE"] = categoryRoleModel.CategoryRole;
            recordObject["CATEGORY_STRATEGY"] = categoryRoleModel.CategoryStrategy;
            recordObject["CREATED_BY"] = this.GetCreatedBy(categoryRoleModel.CreatedBy);
            recordObject["LAST_UPDATED_BY"] = FxContext.Context.Name;
            recordObject["Operation"] = categoryRoleModel.Operation;
        }

        public virtual void SetParamsMerchantSpaceSearch(MerchantSpaceModel merchantSpaceModel, OracleObject recordObject)
        {
            recordObject["COMPANY_ID"] = merchantSpaceModel.CompanyId;
            recordObject["DIVISION_ID"] = merchantSpaceModel.DivisionId;
            recordObject["DEPT_ID"] = merchantSpaceModel.DeptId;
            recordObject["CATEGORY_ID"] = merchantSpaceModel.CategoryId;
            recordObject["SPACE_LEVEL"] = merchantSpaceModel.SpaceLevel;
            recordObject["SPACE_ORG_ID"] = merchantSpaceModel.SpaceOrgId;
        }

        public virtual void SetParamsMerchantSpaceSave(MerchantSpaceModel merchantSpaceModel, OracleObject recordObject)
        {
            recordObject["MULTI_LOCATED"] = merchantSpaceModel.MultiLocated;
            recordObject["SPACE_HEIGHT"] = merchantSpaceModel.SpaceHeight;
            recordObject["SPACE_LENGTH"] = merchantSpaceModel.SpaceLength;
            recordObject["SPACE_WIDTH"] = merchantSpaceModel.SpaceWidth;
            recordObject["CAT_CAPTAIN"] = merchantSpaceModel.CatCaptain;
            recordObject["PRIMARY_BRAND"] = merchantSpaceModel.PrimaryBrand;
            recordObject["CREATED_BY"] = this.GetCreatedBy(merchantSpaceModel.CreatedBy);
            recordObject["LAST_UPDATED_BY"] = FxContext.Context.Name;
            recordObject["Operation"] = merchantSpaceModel.Operation;
        }

        private OracleObject SetParamsDivision(DivisionModel divisionModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("MERCH_DIVISION_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            this.SetParamsDivisionSearch(divisionModel, recordObject);
            return recordObject;
        }

        private OracleObject SetParamsGroup(GroupModel departmentModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("MERCH_GROUP_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            this.SetParamsGroupSearch(departmentModel, recordObject);
            return recordObject;
        }

        private OracleObject SetParamsDepartment(DepartmentModel categoryModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("MERCH_DEPT_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            this.SetParamsDepartmentSearch(categoryModel, recordObject);
            return recordObject;
        }

        private OracleObject SetParamsSubClass(SubClassModel subClassModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("MERCH_SUB_CLASS_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            this.SetParamsSubClassSearch(subClassModel, recordObject);
            return recordObject;
        }

        private OracleObject SetParamsCategoryRole(CategoryRoleModel categoryRoleModel, bool isSearch)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("MERCH_CATROLE_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            if (!isSearch)
            {
                this.SetParamsCategoryRoleSave(categoryRoleModel, recordObject);
            }

            this.SetParamsCategoryRoleSearch(categoryRoleModel, recordObject);

            return recordObject;
        }

        private OracleObject SetParamsMerchantSpace(MerchantSpaceModel merchantSpaceModel, bool isSearch)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("MERCH_SPACE_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            if (!isSearch)
            {
                this.SetParamsMerchantSpaceSave(merchantSpaceModel, recordObject);
            }

            this.SetParamsMerchantSpaceSearch(merchantSpaceModel, recordObject);
            return recordObject;
        }

        private OracleObject SetParamsClass(ClassModel classModel, bool isSerach)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(MdmConstants.RecTypeClass, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            return this.SetGetClassParamas(classModel, recordObject, isSerach);
        }

        private OracleObject SetGetClassParamas(ClassModel classModel, OracleObject recordObject, bool isSerach)
        {
            if (!isSerach)
            {
                OracleTable pSubClassTab = this.SubClassDetailationParamsObject(classModel);
                recordObject["sub_class"] = pSubClassTab;
                recordObject["Operation"] = classModel.Operation;
                recordObject["class_vat_ind"] = classModel.ClassVatInd;
            }

            recordObject["dept"] = classModel.Dept;
            recordObject["class"] = classModel.Class;
            recordObject["class_name"] = classModel.ClassName;
            return recordObject;
        }

        private OracleTable SubClassDetailationParamsObject(ClassModel classModel)
        {
            OracleType dtlTableType = OracleType.GetObjectType(MdmConstants.ObjTypeSubClassDetail, this.Connection);
            OracleType dtlRecordType = OracleType.GetObjectType(MdmConstants.RecTypeSubClassDetail, this.Connection);
            OracleTable pLocationTab = new OracleTable(dtlTableType);

            foreach (SubClassDetailModel subClassDetailModel in classModel.SubClassDetailList)
            {
                var dtlRecordObject = new OracleObject(dtlRecordType);
                dtlRecordObject["dept"] = classModel.Dept;
                dtlRecordObject["class"] = classModel.Class;
                dtlRecordObject["subclass"] = subClassDetailModel.Subclass;
                dtlRecordObject["sub_name"] = subClassDetailModel.SubName;
                dtlRecordObject["Operation"] = subClassDetailModel.Operation;
                pLocationTab.Add(dtlRecordObject);
            }

            return pLocationTab;
        }
    }
}
