﻿//-------------------------------------------------------------------------------------------------
// <copyright file="ClassController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// ClassController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.MerchantHierarchy
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.Framework.Web.Security.Repositories;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.MerchantHierarchy.Models;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class ClassController : Controller
    {
        private readonly ServiceDocument<ClassModel> serviceDocument;
        private readonly MerchantHierarchyRepository classRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly DomainDataRepository domainDataRepository;

        public ClassController(
            ServiceDocument<ClassModel> serviceDocument,
              MerchantHierarchyRepository classRepository,
            DomainDataRepository domainDataRepository)
        {
            this.classRepository = classRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<ClassModel>> List()
        {
            return await this.LoadDomainData();
        }

        public async Task<ServiceDocument<ClassModel>> New()
        {
            return await this.LoadDomainData();
        }

        public async Task<ServiceDocument<ClassModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.ClassSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<ClassModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.ClassSave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingClass(int departmentId, int id)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "CLASS";
            nvc["P_COL1"] = id.ToString();
            nvc["P_COL2"] = departmentId.ToString();
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        private async Task<List<ClassModel>> ClassSearch()
        {
            try
            {
                var lstUom = await this.classRepository.GetClass(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstUom;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> ClassSave()
        {
            this.serviceDocument.Result = await this.classRepository.SetClass(this.serviceDocument.DataProfile.DataModel);
            return true;
        }

        private async Task<ServiceDocument<ClassModel>> LoadDomainData()
        {
            this.serviceDocument.DomainData.Add("indicatorStatus", this.domainDataRepository.DomainDetailData(MdmConstants.VatRegionTypeDomainData).Result);
            this.serviceDocument.DomainData.Add("department", this.domainDataRepository.DepartmentDomainGet().Result);
            this.serviceDocument.LocalizationData.AddData("Class");
            return this.serviceDocument;
        }
    }
}
