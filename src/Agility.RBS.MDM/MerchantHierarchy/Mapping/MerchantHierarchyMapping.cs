﻿// <copyright file="MerchantHierarchyMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.MerchantHierarchy.Mapping
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.MerchantHierarchy.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class MerchantHierarchyMapping : Profile
    {
        public MerchantHierarchyMapping()
            : base("MerchantHierarchyMapping")
        {
            this.CreateMap<OracleObject, DivisionModel>()
                .ForMember(m => m.DivName, opt => opt.MapFrom(r => r["DIV_NAME"]))
                .ForMember(m => m.Division, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DIVISION"])))
                .ForMember(m => m.Buyer, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["BUYER"])))
                .ForMember(m => m.Merch, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["MERCH"])))
                .ForMember(m => m.TotalMarketAmt, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["TOTAL_MARKET_AMT"])))
                .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "DIVISION"))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, GroupModel>()
                .ForMember(m => m.Division, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DIVISION"])))
                .ForMember(m => m.DivName, opt => opt.MapFrom(r => r["DIV_NAME"]))
                .ForMember(m => m.GroupNo, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["GROUP_NO"])))
                .ForMember(m => m.GroupName, opt => opt.MapFrom(r => r["GROUP_NAME"]))
                .ForMember(m => m.Buyer, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["BUYER"])))
                .ForMember(m => m.Merch, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["MERCH"])))
                .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "GROUPS"))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, DepartmentModel>()
                .ForMember(m => m.Dept, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DEPT"])))
                .ForMember(m => m.DeptName, opt => opt.MapFrom(r => r["DEPT_NAME"]))
                .ForMember(m => m.Buyer, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["BUYER"])))
                .ForMember(m => m.Merch, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["MERCH"])))
                .ForMember(m => m.ProfitCalcType, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["PROFIT_CALC_TYPE"])))
                .ForMember(m => m.PurchaseType, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["PURCHASE_TYPE"])))
                .ForMember(m => m.GroupNo, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["GROUP_NO"])))
                .ForMember(m => m.GroupName, opt => opt.MapFrom(r => r["GROUP_NAME"]))
                .ForMember(m => m.BudInt, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["BUD_INT"])))
                .ForMember(m => m.BudMkup, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["BUD_MKUP"])))
                .ForMember(m => m.TotalMarketAmt, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["TOTAL_MARKET_AMT"])))
                .ForMember(m => m.MarkupCalcType, opt => opt.MapFrom(r => r["MARKUP_CALC_TYPE"]))
                .ForMember(m => m.OtbCalcType, opt => opt.MapFrom(r => r["OTB_CALC_TYPE"]))
                .ForMember(m => m.MaxAvgCounter, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["MAX_AVG_COUNTER"])))
                .ForMember(m => m.AvgTolerancePct, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["AVG_TOLERANCE_PCT"])))
                .ForMember(m => m.DeptVatInclInd, opt => opt.MapFrom(r => r["DEPT_VAT_INCL_IND"]))
                .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "DEPS"))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, ClassModel>()
                .ForMember(m => m.Dept, opt => opt.MapFrom(r => r["DEPT"]))
                .ForMember(m => m.DeptDesc, opt => opt.MapFrom(r => r["DEPT_NAME"]))
                .ForMember(m => m.Class, opt => opt.MapFrom(r => r["CLASS"]))
                .ForMember(m => m.ClassName, opt => opt.MapFrom(r => r["CLASS_NAME"]))
                .ForMember(m => m.ClassVatInd, opt => opt.MapFrom(r => r["Class_Vat_Ind"]))
                .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "CLASS"))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, SubClassModel>()
                .ForMember(m => m.Dept, opt => opt.MapFrom(r => r["DEPT"]))
                .ForMember(m => m.DeptName, opt => opt.MapFrom(r => r["DEPT_NAME"]))
                .ForMember(m => m.Class, opt => opt.MapFrom(r => r["CLASS"]))
                .ForMember(m => m.ClassName, opt => opt.MapFrom(r => r["CLASS_NAME"]))
                .ForMember(m => m.Subclass, opt => opt.MapFrom(r => r["SUBCLASS"]))
                .ForMember(m => m.SubName, opt => opt.MapFrom(r => r["SUB_NAME"]))
                .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "SUBCLASS"))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, CategoryRoleModel>()
             .ForMember(m => m.CompanyId, opt => opt.MapFrom(r => r["COMPANY_ID"]))
             .ForMember(m => m.CompanyName, opt => opt.MapFrom(r => r["COMPANY_NAME"]))
              .ForMember(m => m.DivisionId, opt => opt.MapFrom(r => r["DIVISION_ID"]))
                .ForMember(m => m.DivisionDesc, opt => opt.MapFrom(r => r["DIVISION_DESC"]))
             .ForMember(m => m.DeptId, opt => opt.MapFrom(r => r["DEPT_ID"]))
             .ForMember(m => m.DeptDesc, opt => opt.MapFrom(r => r["DEPT_DESC"]))
             .ForMember(m => m.CategoryId, opt => opt.MapFrom(r => r["CATEGORY_ID"]))
             .ForMember(m => m.CategoryDesc, opt => opt.MapFrom(r => r["CATEGORY_DESC"]))
             .ForMember(m => m.RoleLevel, opt => opt.MapFrom(r => r["ROLE_LEVEL"]))
             .ForMember(m => m.RoleId, opt => opt.MapFrom(r => r["ROLE_ID"]))
             .ForMember(m => m.StrategyType, opt => opt.MapFrom(r => r["STRATEGY_TYPE"]))
             .ForMember(m => m.StrategyTypeDesc, opt => opt.MapFrom(r => r["STRATEGY_TYPE_Desc"]))
             .ForMember(m => m.CategoryRole, opt => opt.MapFrom(r => r["CATEGORY_ROLE"]))
             .ForMember(m => m.CategoryRoleDesc, opt => opt.MapFrom(r => r["CATEGORY_ROLE_DESC"]))
             .ForMember(m => m.CategoryStrategy, opt => opt.MapFrom(r => r["CATEGORY_STRATEGY"]))
             .ForMember(m => m.CategoryStrategyDesc, opt => opt.MapFrom(r => r["CATEGORY_STRATEGY_DESC"]))
             .ForMember(m => m.CreatedBy, opt => opt.MapFrom(r => r["CREATED_BY"]))
             .ForMember(m => m.ModifiedBy, opt => opt.MapFrom(r => r["LAST_UPDATED_BY"]))
             .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
             .ForMember(m => m.CreatedDate, opt => opt.MapFrom(r => r["CREATED_DATE"]))
             .ForMember(m => m.ModifiedDate, opt => opt.MapFrom(r => r["LAST_UPDATED_DATE"]))
             .ForMember(m => m.TableName, opt => opt.MapFrom(r => "CAT_ROLE_MASTER"))
             .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, MerchantSpaceModel>()
                .ForMember(m => m.CompanyId, opt => opt.MapFrom(r => r["COMPANY_ID"]))
                .ForMember(m => m.CompanyName, opt => opt.MapFrom(r => r["COMPANY_NAME"]))
                 .ForMember(m => m.DivisionId, opt => opt.MapFrom(r => r["DIVISION_ID"]))
                .ForMember(m => m.DivisionDesc, opt => opt.MapFrom(r => r["DIVISION_DESC"]))
                .ForMember(m => m.DeptId, opt => opt.MapFrom(r => r["DEPT_ID"]))
                .ForMember(m => m.DeptDesc, opt => opt.MapFrom(r => r["DEPT_DESC"]))
                .ForMember(m => m.CategoryId, opt => opt.MapFrom(r => r["CATEGORY_ID"]))
                .ForMember(m => m.CategoryDesc, opt => opt.MapFrom(r => r["CATEGORY_DESC"]))
                .ForMember(m => m.CatCaptain, opt => opt.MapFrom(r => r["CAT_CAPTAIN"]))
                .ForMember(m => m.MultiLocated, opt => opt.MapFrom(r => r["MULTI_LOCATED"]))
                .ForMember(m => m.PrimaryBrand, opt => opt.MapFrom(r => r["PRIMARY_BRAND"]))
                .ForMember(m => m.SpaceHeight, opt => opt.MapFrom(r => r["SPACE_HEIGHT"]))
                .ForMember(m => m.SpaceLength, opt => opt.MapFrom(r => r["SPACE_LENGTH"]))
                .ForMember(m => m.SpaceWidth, opt => opt.MapFrom(r => r["SPACE_WIDTH"]))
                .ForMember(m => m.SpaceLevel, opt => opt.MapFrom(r => r["SPACE_LEVEL"]))
                .ForMember(m => m.SpaceOrgId, opt => opt.MapFrom(r => r["SPACE_ORG_ID"]))
                .ForMember(m => m.CreatedBy, opt => opt.MapFrom(r => r["CREATED_BY"]))
                .ForMember(m => m.ModifiedBy, opt => opt.MapFrom(r => r["LAST_UPDATED_BY"]))
                .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
                .ForMember(m => m.CreatedDate, opt => opt.MapFrom(r => r["CREATED_DATE"]))
                .ForMember(m => m.ModifiedDate, opt => opt.MapFrom(r => r["LAST_UPDATED_DATE"]))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "MERCH_SPACE_MST"))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, SubClassDetailModel>()
               .ForMember(m => m.Dept, opt => opt.MapFrom(r => r["DEPT"]))
               .ForMember(m => m.DeptName, opt => opt.MapFrom(r => r["DEPT_NAME"]))
               .ForMember(m => m.Class, opt => opt.MapFrom(r => r["CLASS"]))
               .ForMember(m => m.ClassName, opt => opt.MapFrom(r => r["CLASS_NAME"]))
               .ForMember(m => m.Subclass, opt => opt.MapFrom(r => r["SUBCLASS"]))
               .ForMember(m => m.SubName, opt => opt.MapFrom(r => r["SUB_NAME"]))
               .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
               .ForMember(m => m.TableName, opt => opt.MapFrom(r => "SUBCLASS"))
               .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));
        }
    }
}
