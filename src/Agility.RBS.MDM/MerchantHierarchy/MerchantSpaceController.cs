﻿//-------------------------------------------------------------------------------------------------
// <copyright file="MerchantSpaceController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// MerchantSpaceController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.MerchantHierarchy
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.MerchantHierarchy.Models;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class MerchantSpaceController : Controller
    {
        private readonly ServiceDocument<MerchantSpaceModel> serviceDocument;
        private readonly MerchantHierarchyRepository merchantHierarchyRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly DomainDataRepository domainDataRepository;

        public MerchantSpaceController(
            ServiceDocument<MerchantSpaceModel> serviceDocument,
            MerchantHierarchyRepository merchantHierarchyRepository,
            DomainDataRepository domainDataRepository)
        {
            this.merchantHierarchyRepository = merchantHierarchyRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<MerchantSpaceModel>> List()
        {
            return await this.MerchantSpaceDomainData();
        }

        public async Task<ServiceDocument<MerchantSpaceModel>> New()
        {
            this.serviceDocument.DomainData.Add("brand", this.domainDataRepository.CommonData("BRAND").Result);
            this.serviceDocument.DomainData.Add("multilocated", this.domainDataRepository.DomainDetailData(MdmConstants.DomainHdrIndicator).Result);
            this.serviceDocument.LocalizationData.AddData("MerchantSpace");
            return await this.MerchantSpaceDomainData();
        }

        public async Task<ServiceDocument<MerchantSpaceModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.MerchantSpaceSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<MerchantSpaceModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.MerchantSpaceSave);
            return this.serviceDocument;
        }

        public async ValueTask<bool> IsExistingMerchantSpaceMaster(int companyId, int divisionId, int deptId, int categoryId, string spaceOrgId, string spaceLevel)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "MERCH_SPACE_MST";
            nvc["P_COL1"] = companyId.ToString();
            nvc["P_COL2"] = divisionId.ToString();
            nvc["P_COL3"] = deptId.ToString();
            nvc["P_COL4"] = categoryId.ToString();
            nvc["P_COL5"] = spaceLevel;
            nvc["P_COL6"] = spaceOrgId;
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        private async Task<List<MerchantSpaceModel>> MerchantSpaceSearch()
        {
            try
            {
                var merchantSpaces = await this.merchantHierarchyRepository.GetMerchantSpace(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return merchantSpaces;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> MerchantSpaceSave()
        {
            this.serviceDocument.Result = await this.merchantHierarchyRepository.SetMerchantSpace(this.serviceDocument.DataProfile.DataModel);
            return true;
        }

        private async Task<ServiceDocument<MerchantSpaceModel>> MerchantSpaceDomainData()
        {
            this.serviceDocument.DomainData.Add("company", this.domainDataRepository.CompanyDomainGet().Result);
            this.serviceDocument.DomainData.Add("division", this.domainDataRepository.DivisionDomainGet().Result);
            this.serviceDocument.DomainData.Add("group", this.domainDataRepository.GroupDomainGet().Result);
            this.serviceDocument.DomainData.Add("department", this.domainDataRepository.DepartmentDomainGet().Result);
            this.serviceDocument.DomainData.Add("spacelevel", this.domainDataRepository.DomainDetailData(MdmConstants.DomainHdrSpacelevel).Result);
            this.serviceDocument.DomainData.Add("companylevel", this.domainDataRepository.CommonData("COMPANY").Result);
            this.serviceDocument.DomainData.Add("orgcountrylevel", this.domainDataRepository.CommonData("COUNTRY").Result);
            this.serviceDocument.DomainData.Add("formatlevel", this.domainDataRepository.CommonData("FORMAT").Result);
            this.serviceDocument.DomainData.Add("regionlevel", this.domainDataRepository.CommonData("REGION").Result);
            return this.serviceDocument;
        }
    }
}
