﻿//-------------------------------------------------------------------------------------------------
// <copyright file="GroupController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// GroupController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.MerchantHierarchy
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.Framework.Web.Security.Repositories;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.MerchantHierarchy.Models;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class GroupController : Controller
    {
        private readonly ServiceDocument<GroupModel> serviceDocument;
        private readonly MerchantHierarchyRepository merchantHierarchyRepository;
        private readonly WrapperRepository wrapperRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly DomainDataRepository domainDataRepository;

        public GroupController(
            ServiceDocument<GroupModel> serviceDocument,
            MerchantHierarchyRepository merchantHierarchyRepository,
            WrapperRepository wrapperRepository,
            DomainDataRepository domainDataRepository)
        {
            this.merchantHierarchyRepository = merchantHierarchyRepository;
            this.wrapperRepository = wrapperRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<GroupModel>> List()
        {
            return await this.GroupDomainData();
        }

        public async Task<ServiceDocument<GroupModel>> New()
        {
            this.serviceDocument.LocalizationData.AddData("Group");
            this.serviceDocument.DomainData.Add("buyer", this.domainDataRepository.BuyerDomainGet().Result);
            this.serviceDocument.DomainData.Add("merch", this.domainDataRepository.MerchDomainGet().Result);
            return await this.GroupDomainData();
        }

        public async Task<ServiceDocument<GroupModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.GroupSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<GroupModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.GroupSave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingGroup(int divisionId, int id)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "GROUPS";
            nvc["P_COL1"] = divisionId.ToString();
            nvc["P_COL2"] = id.ToString();
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        private async Task<List<GroupModel>> GroupSearch()
        {
            try
            {
                List<GroupModel> groups = await this.merchantHierarchyRepository.GetGroup(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return groups;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> GroupSave()
        {
            this.serviceDocument.Result = await this.wrapperRepository.SetGroup(this.serviceDocument.DataProfile.DataModel);
            return true;
        }

        private async Task<ServiceDocument<GroupModel>> GroupDomainData()
        {
            this.serviceDocument.DomainData.Add("division", this.domainDataRepository.DivisionDomainGet().Result);
            return this.serviceDocument;
        }
    }
}
