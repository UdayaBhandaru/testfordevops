﻿//-------------------------------------------------------------------------------------------------
// <copyright file="SubClassController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// SubClassController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.MerchantHierarchy
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.Framework.Web.Security.Repositories;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.MerchantHierarchy.Models;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Caching.Distributed;

    public class SubClassController : Controller
    {
        private readonly ServiceDocument<SubClassModel> serviceDocument;
        private readonly MerchantHierarchyRepository merchantHierarchyRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly DomainDataRepository domainDataRepository;
        private readonly WrapperRepository wrapperRepository;

        public SubClassController(
            ServiceDocument<SubClassModel> serviceDocument,
            MerchantHierarchyRepository merchantHierarchyRepository,
        WrapperRepository wrapperRepository,
            DomainDataRepository domainDataRepository)
        {
            this.merchantHierarchyRepository = merchantHierarchyRepository;
            this.domainDataRepository = domainDataRepository;
            this.wrapperRepository = wrapperRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        [HttpPost]
        public async Task<ServiceDocument<SubClassModel>> List()
        {
            return await this.SubClassDomainData();
        }

        [HttpPost]
        public async Task<ServiceDocument<SubClassModel>> New()
        {
            this.serviceDocument.LocalizationData.AddData("SubClass");
            return await this.SubClassDomainData();
        }

        [HttpPost]
        public async Task<ServiceDocument<SubClassModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.SubClassSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<SubClassModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.SubClassSave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingSubClass(int deptId, int classId, int id)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "SUBCLASS";
            nvc["P_COL1"] = deptId.ToString();
            nvc["P_COL2"] = classId.ToString();
            nvc["P_COL3"] = id.ToString();
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        private async Task<List<SubClassModel>> SubClassSearch()
        {
            try
            {
                var subClasses = await this.merchantHierarchyRepository.GetSubClass(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return subClasses;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> SubClassSave()
        {
            this.serviceDocument.Result = await this.wrapperRepository.SetSubClass(this.serviceDocument.DataProfile.DataModel);
            return true;
        }

        private async Task<ServiceDocument<SubClassModel>> SubClassDomainData()
        {
            this.serviceDocument.DomainData.Add("department", this.domainDataRepository.DepartmentDomainGet().Result);
            this.serviceDocument.DomainData.Add("class", this.domainDataRepository.ClassDomainGet().Result);
            return this.serviceDocument;
        }
    }
}
