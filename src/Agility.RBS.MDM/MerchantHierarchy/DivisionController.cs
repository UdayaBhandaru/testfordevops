﻿//-------------------------------------------------------------------------------------------------
// <copyright file="DivisionController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// DivisionController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.MerchantHierarchy
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.Framework.Web.Security.Repositories;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.MerchantHierarchy.Models;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class DivisionController : Controller
    {
        private readonly ServiceDocument<DivisionModel> serviceDocument;
        private readonly WrapperRepository wrapperRepository;
        private readonly MerchantHierarchyRepository merchantHierarchyRepository;
        private readonly DomainDataRepository domainDataRepository;

        public DivisionController(
            ServiceDocument<DivisionModel> serviceDocument,
            WrapperRepository wrapperRepository,
            MerchantHierarchyRepository merchantHierarchyRepository,
            DomainDataRepository domainDataRepository)
        {
            this.domainDataRepository = domainDataRepository;
            this.wrapperRepository = wrapperRepository;
            this.merchantHierarchyRepository = merchantHierarchyRepository;
            this.serviceDocument = serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<DivisionModel>> List()
        {
            return await this.DivisionDomainData();
        }

        [HttpPost]
        public async Task<ServiceDocument<DivisionModel>> New()
        {
            this.serviceDocument.LocalizationData.AddData("Division");
            return await this.DivisionDomainData();
        }

        [HttpPost]
        public async Task<ServiceDocument<DivisionModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.DivisionSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<DivisionModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.DivisionSave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingDivision(int id)
        {
            return await this.merchantHierarchyRepository.IsExistingDivision(id);
        }

        private async Task<List<DivisionModel>> DivisionSearch()
        {
            try
            {
                ServiceDocumentResult serviceDocumentResult = new ServiceDocumentResult
                {
                    InnerException = string.Empty,
                    StackTrace = string.Empty,
                    Type = MessageType.Success
                };
                var lstDivision = await this.merchantHierarchyRepository.GetDivision(this.serviceDocument.DataProfile.DataModel, serviceDocumentResult);
                this.serviceDocument.Result = serviceDocumentResult;
                return lstDivision;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> DivisionSave()
        {
            this.serviceDocument.Result = await this.wrapperRepository.SetDivision(this.serviceDocument.DataProfile.DataModel);
            return true;
        }

        private async Task<ServiceDocument<DivisionModel>> DivisionDomainData()
        {
            this.serviceDocument.DomainData.Add("buyer", this.domainDataRepository.BuyerDomainGet().Result);
            this.serviceDocument.DomainData.Add("merch", this.domainDataRepository.MerchDomainGet().Result);
            return this.serviceDocument;
        }
    }
}
