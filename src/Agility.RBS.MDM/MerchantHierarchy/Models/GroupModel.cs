﻿// <copyright file="GroupModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.MerchantHierarchy.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class GroupModel : ProfileEntity
    {
        public int? Division { get; set; }

        public string DivName { get; set; }

        public int? GroupNo { get; set; }

        public string GroupName { get; set; }

        public int? Buyer { get; set; }

        public int? Merch { get; set; }

        public string MerchName { get; set; }

        public string BuyerName { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }

        public string Error { get; set; }
    }
}
