﻿// <copyright file="ClassModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.MerchantHierarchy.Models
{
    using System.Collections.Generic;
    using Agility.Framework.Core.Profile.Entities;

    public class ClassModel : ProfileEntity
    {
        public ClassModel()
        {
            this.SubClassDetailList = new List<SubClassDetailModel>();
        }

        public int? Class { get; set; }

        public string ClassName { get; set; }

        public int? Dept { get; set; }

        public string DeptDesc { get; set; }

        public string ClassVatInd { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }

        public string Error { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public List<SubClassDetailModel> SubClassDetailList { get; private set; }
    }
}
