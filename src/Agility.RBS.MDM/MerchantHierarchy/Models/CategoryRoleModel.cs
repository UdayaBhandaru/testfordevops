﻿// <copyright file="CategoryRoleModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.MerchantHierarchy.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class CategoryRoleModel : ProfileEntity
    {
        public int? CompanyId { get; set; }

        public string CompanyName { get; set; }

        public int? DivisionId { get; set; }

        public string DivisionDesc { get; set; }

        public int? DeptId { get; set; }

        public string DeptDesc { get; set; }

        public int? CategoryId { get; set; }

        public string CategoryDesc { get; set; }

        public string RoleLevel { get; set; }

        public string RoleId { get; set; }

        public string StrategyType { get; set; }

        public string StrategyTypeDesc { get; set; }

        public string CategoryRole { get; set; }

        public string CategoryStrategy { get; set; }

        public string CategoryRoleDesc { get; set; }

        public string CategoryStrategyDesc { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }

        public string Error { get; set; }
    }
}
