﻿// <copyright file="DepartmentModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.MerchantHierarchy.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class DepartmentModel : ProfileEntity
    {
        public int? GroupNo { get; set; }

        public string GroupName { get; set; }

        public int? Dept { get; set; }

        public string DeptName { get; set; }

        public int? Buyer { get; set; }

        public int? Merch { get; set; }

        public string MerchName { get; set; }

        public string BuyerName { get; set; }

        public string ProfitCalcType { get; set; }

        public string PurchaseType { get; set; }

        public decimal? BudInt { get; set; }

        public decimal? BudMkup { get; set; }

        public decimal? TotalMarketAmt { get; set; }

        public string MarkupCalcType { get; set; }

        public string OtbCalcType { get; set; }

        public int? MaxAvgCounter { get; set; }

        public decimal? AvgTolerancePct { get; set; }

        public string DeptVatInclInd { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }

        public string Error { get; set; }
    }
}
