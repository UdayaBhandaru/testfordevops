﻿// <copyright file="MerchantSpaceModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.MerchantHierarchy.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class MerchantSpaceModel : ProfileEntity
    {
        public int? CompanyId { get; set; }

        public string CompanyName { get; set; }

        public int? DivisionId { get; set; }

        public string DivisionDesc { get; set; }

        public int? DeptId { get; set; }

        public string DeptDesc { get; set; }

        public int? CategoryId { get; set; }

        public string CategoryDesc { get; set; }

        public string SpaceLevel { get; set; }

        public string SpaceOrgId { get; set; }

        public string MultiLocated { get; set; }

        public decimal SpaceHeight { get; set; }

        public decimal SpaceWidth { get; set; }

        public decimal SpaceLength { get; set; }

        public int CatCaptain { get; set; }

        public string PrimaryBrand { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }

        public string Error { get; set; }
    }
}
