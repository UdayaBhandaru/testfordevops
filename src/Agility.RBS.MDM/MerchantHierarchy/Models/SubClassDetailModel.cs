﻿//-------------------------------------------------------------------------------------------------
// <copyright file="SubClassDetailModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.MerchantHierarchy.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class SubClassDetailModel : ProfileEntity
    {
        public int? Dept { get; set; }

        public string DeptName { get; set; }

        public int? Class { get; set; }

        public string ClassName { get; set; }

        public int? Subclass { get; set; }

        public string SubName { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }

        public string Error { get; set; }
    }
}
