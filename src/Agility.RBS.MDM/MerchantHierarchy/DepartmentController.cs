﻿//-------------------------------------------------------------------------------------------------
// <copyright file="DepartmentController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// DepartmentController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.MerchantHierarchy
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.Framework.Web.Security.Repositories;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.MerchantHierarchy.Models;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.UtilityDomain.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class DepartmentController : Controller
    {
        private readonly ServiceDocument<DepartmentModel> serviceDocument;
        private readonly MerchantHierarchyRepository merchantHierarchyRepository;
        private readonly WrapperRepository wrapperRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly DomainDataRepository domainDataRepository;

        public DepartmentController(
            ServiceDocument<DepartmentModel> serviceDocument,
            MerchantHierarchyRepository merchantHierarchyRepository,
        WrapperRepository wrapperRepository,
        DomainDataRepository domainDataRepository)
        {
            this.merchantHierarchyRepository = merchantHierarchyRepository;
            this.wrapperRepository = wrapperRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<DepartmentModel>> List()
        {
            return await this.DepartmentDomainData();
        }

        public async Task<ServiceDocument<DepartmentModel>> New()
        {
            this.serviceDocument.DomainData.Add("buyer", this.domainDataRepository.BuyerDomainGet().Result);
            this.serviceDocument.DomainData.Add("merch", this.domainDataRepository.MerchDomainGet().Result);
            this.serviceDocument.LocalizationData.AddData("Department");
            return await this.DepartmentDomainData();
        }

        public async Task<ServiceDocument<DepartmentModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.DepartmentSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<DepartmentModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.DepartmentSave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingDepartment(int groupId, int id)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "DEPS";
            nvc["P_COL1"] = id.ToString();
            nvc["P_COL2"] = groupId.ToString();
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        private async Task<List<DepartmentModel>> DepartmentSearch()
        {
            try
            {
                var categories = await this.merchantHierarchyRepository.GetDepartment(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return categories;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> DepartmentSave()
        {
            this.serviceDocument.Result = await this.wrapperRepository.SetDepartment(this.serviceDocument.DataProfile.DataModel);
            return true;
        }

        private async Task<ServiceDocument<DepartmentModel>> DepartmentDomainData()
        {
            this.serviceDocument.DomainData.Add("indicatorStatus", this.domainDataRepository.DomainDetailData(MdmConstants.VatRegionTypeDomainData).Result);
            this.serviceDocument.DomainData.Add("group", this.domainDataRepository.GroupDomainGet().Result);
            this.serviceDocument.DomainData.Add("profitCalcType", this.domainDataRepository.DomainDetailData(MdmConstants.ProfitCalcTypeDomainData).Result);
            this.serviceDocument.DomainData.Add("otbCalcType", this.domainDataRepository.DomainDetailData(MdmConstants.OtbCalcTypeDomainData).Result);

            var purchTypesList = this.domainDataRepository.DomainDetailData(MdmConstants.PurchTypesDomainData).Result;
            foreach (var item in purchTypesList)
            {
                if (item.CodeDesc.Equals("Consignment Merchandise"))
                {
                    item.CodeDesc = " Consignment Stock";
                }
            }

            this.serviceDocument.DomainData.Add("purchTypes", purchTypesList);
            //// this.serviceDocument.DomainData.Add("purchTypes", this.domainDataRepository.DomainDetailData(MdmConstants.PurchTypesDomainData).Result);
            ////  this.serviceDocument.DomainData.Add("markupCalType", this.domainDataRepository.DomainDetailData(MdmConstants.MarkupCalTypeDomainData).Result);

            var markupCalTypeList = this.domainDataRepository.DomainDetailData(MdmConstants.MarkupCalTypeDomainData).Result;
            foreach (var item in markupCalTypeList)
            {
                item.CodeDesc = " % of " + item.CodeDesc;
            }

            this.serviceDocument.DomainData.Add("markupCalType", markupCalTypeList);

            return this.serviceDocument;
        }
    }
}
