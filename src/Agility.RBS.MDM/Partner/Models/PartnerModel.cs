﻿// <copyright file="PartnerModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// BrandModel
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Partner.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class PartnerModel : ProfileEntity
    {
        public string PartnerType { get; set; }

        public string PartnerTypeDesc { get; set; }

        public string PartnerId { get; set; }

        public string PartnerDesc { get; set; }

        public string CurrencyCode { get; set; }

        public string CurrencyDesc { get; set; }

        public int? Lang { get; set; }

        public string Status { get; set; }

        public string StatusDesc { get; set; }

        public string ContactName { get; set; }

        public string ContactPhone { get; set; }

        public string ContactFax { get; set; }

        public string ContactTelex { get; set; }

        public string ContactEmail { get; set; }

        public string MfgId { get; set; }

        public string PrincipleCountryId { get; set; }

        public string PrincipleCountryName { get; set; }

        public int? LineOfCredit { get; set; }

        public int? OutstandCredit { get; set; }

        public int? OpenCredit { get; set; }

        public int? YtdCredit { get; set; }

        public int? YtdDrawdowns { get; set; }

        public string TaxId { get; set; }

        public string Terms { get; set; }

        public string TermsDesc { get; set; }

        public string ServicePerfReqInd { get; set; }

        public string InvcPayLoc { get; set; }

        public string InvcPayLocName { get; set; }

        public string InvcReceiveLoc { get; set; }

        public string InvcReceiveLocName { get; set; }

        public string ImportCountryId { get; set; }

        public string ImportCountryName { get; set; }

        public string PrimaryIaInd { get; set; }

        public string CommentDesc { get; set; }

        public int? TsfEntityId { get; set; }

        public string VatRegion { get; set; }

        public string VatRegionName { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Error { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }
    }
}
