﻿// <copyright file="PartnerMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Partner.Mapping
{
    using System;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.Brand.Models;
    using Agility.RBS.MDM.CostZone.Models;
    using Agility.RBS.MDM.Partner.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class PartnerMapping : Profile
    {
        public PartnerMapping()
            : base("PartnerMapping")
        {
            this.CreateMap<OracleObject, PartnerModel>()
                .ForMember(m => m.PartnerType, opt => opt.MapFrom(r => r["PARTNER_TYPE"]))
                .ForMember(m => m.PartnerTypeDesc, opt => opt.MapFrom(r => r["PARTNER_TYPE_DESC"]))
                .ForMember(m => m.PartnerId, opt => opt.MapFrom(r => r["PARTNER_ID"]))
                .ForMember(m => m.PartnerDesc, opt => opt.MapFrom(r => r["PARTNER_DESC"]))
                .ForMember(m => m.CurrencyCode, opt => opt.MapFrom(r => r["CURRENCY_CODE"]))
                .ForMember(m => m.CurrencyDesc, opt => opt.MapFrom(r => r["CURRENCY_DESC"]))
                .ForMember(m => m.Lang, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["LANG"])))
                .ForMember(m => m.Status, opt => opt.MapFrom(r => r["STATUS"]))
                .ForMember(m => m.StatusDesc, opt => opt.MapFrom(r => r["STATUS_DESC"]))
                .ForMember(m => m.ContactName, opt => opt.MapFrom(r => r["CONTACT_NAME"]))
                .ForMember(m => m.ContactPhone, opt => opt.MapFrom(r => r["CONTACT_PHONE"]))
                .ForMember(m => m.ContactFax, opt => opt.MapFrom(r => r["CONTACT_FAX"]))
                .ForMember(m => m.ContactTelex, opt => opt.MapFrom(r => r["CONTACT_TELEX"]))
                .ForMember(m => m.ContactEmail, opt => opt.MapFrom(r => r["CONTACT_EMAIL"]))
                .ForMember(m => m.MfgId, opt => opt.MapFrom(r => r["MFG_ID"]))
                .ForMember(m => m.PrincipleCountryId, opt => opt.MapFrom(r => r["PRINCIPLE_COUNTRY_ID"]))
                .ForMember(m => m.PrincipleCountryName, opt => opt.MapFrom(r => r["PRINCIPLE_COUNTRY_NAME"]))
                .ForMember(m => m.LineOfCredit, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["LINE_OF_CREDIT"])))
                .ForMember(m => m.OutstandCredit, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["OUTSTAND_CREDIT"])))
                .ForMember(m => m.OpenCredit, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["OPEN_CREDIT"])))
                .ForMember(m => m.YtdCredit, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["YTD_CREDIT"])))
                .ForMember(m => m.YtdDrawdowns, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["YTD_DRAWDOWNS"])))
                .ForMember(m => m.TaxId, opt => opt.MapFrom(r => r["TAX_ID"]))
                .ForMember(m => m.Terms, opt => opt.MapFrom(r => r["TERMS"]))
                .ForMember(m => m.TermsDesc, opt => opt.MapFrom(r => r["TERMS_DESC"]))
                .ForMember(m => m.ServicePerfReqInd, opt => opt.MapFrom(r => r["SERVICE_PERF_REQ_IND"]))
                .ForMember(m => m.InvcPayLoc, opt => opt.MapFrom(r => r["INVC_PAY_LOC"]))
                .ForMember(m => m.InvcPayLocName, opt => opt.MapFrom(r => r["INVC_PAY_LOC_NAME"]))
                .ForMember(m => m.InvcReceiveLoc, opt => opt.MapFrom(r => r["INVC_RECEIVE_LOC"]))
                .ForMember(m => m.InvcReceiveLocName, opt => opt.MapFrom(r => r["INVC_RECEIVE_LOC_NAME"]))
                .ForMember(m => m.ImportCountryId, opt => opt.MapFrom(r => r["IMPORT_COUNTRY_ID"]))
                .ForMember(m => m.ImportCountryName, opt => opt.MapFrom(r => r["IMPORT_COUNTRY_NAME"]))
                .ForMember(m => m.PrimaryIaInd, opt => opt.MapFrom(r => r["PRIMARY_IA_IND"]))
                .ForMember(m => m.CommentDesc, opt => opt.MapFrom(r => r["COMMENT_DESC"]))
                .ForMember(m => m.TsfEntityId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["TSF_ENTITY_ID"])))
                .ForMember(m => m.VatRegion, opt => opt.MapFrom(r => r["VAT_REGION"]))
                .ForMember(m => m.VatRegionName, opt => opt.MapFrom(r => r["VAT_REGION_NAME"]))
                 .ForMember(m => m.CreatedDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["CREATED_DATE"])))
                .ForMember(m => m.ModifiedDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["LAST_UPDATED_DATE"])))
                .ForMember(m => m.CreatedBy, opt => opt.MapFrom(r => r["CREATED_BY"]))
                .ForMember(m => m.ModifiedBy, opt => opt.MapFrom(r => r["LAST_UPDATED_BY"]))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "UDA_VALUES"))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));
        }
    }
}
