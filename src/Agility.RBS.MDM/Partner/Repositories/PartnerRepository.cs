﻿// <copyright file="PartnerRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// BrandRepository
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Partner.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Partner.Models;
    using AutoMapper;
    using Devart.Data.Oracle;
    using Microsoft.AspNetCore.Authorization;

    public class PartnerRepository : BaseOraclePackage
    {
        public PartnerRepository()
        {
            this.PackageName = MdmConstants.GetPartnerPackage;
        }

        public async Task<List<PartnerModel>> GetPartner(PartnerModel partnerModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypePartner, MdmConstants.GetProcPartner);
            OracleObject recordObject = this.SetParamsPartner(partnerModel, true);
            return await this.GetProcedure2<PartnerModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ServiceDocumentResult> SetPartner(PartnerModel partnerModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypePartner, MdmConstants.SetProcPartner);
            OracleObject recordObject = this.SetParamsPartner(partnerModel, false);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public virtual void SetParamsPartnerSearch(PartnerModel partnerModel, OracleObject recordObject)
        {
            recordObject["Partner_ID"] = partnerModel.PartnerId;
            recordObject["PARTNER_TYPE"] = partnerModel.PartnerType;
            recordObject["PARTNER_DESC"] = partnerModel.PartnerDesc;
            recordObject["CURRENCY_CODE"] = partnerModel.CurrencyCode;
            recordObject["LANG"] = partnerModel.Lang;
            recordObject["STATUS"] = "A";
            recordObject["CONTACT_NAME"] = partnerModel.ContactName;
            recordObject["CONTACT_PHONE"] = partnerModel.ContactPhone;
            recordObject["CONTACT_FAX"] = partnerModel.ContactFax;
            recordObject["CONTACT_TELEX"] = partnerModel.ContactTelex;
            recordObject["CONTACT_EMAIL"] = partnerModel.ContactEmail;
            recordObject["MFG_ID"] = partnerModel.MfgId;
            recordObject["PRINCIPLE_COUNTRY_ID"] = partnerModel.PrincipleCountryId;
            recordObject["LINE_OF_CREDIT"] = partnerModel.LineOfCredit;
            recordObject["OUTSTAND_CREDIT"] = partnerModel.OutstandCredit;
            recordObject["OPEN_CREDIT"] = partnerModel.OpenCredit;
            recordObject["YTD_CREDIT"] = partnerModel.YtdCredit;
            recordObject["YTD_DRAWDOWNS"] = partnerModel.YtdDrawdowns;
            recordObject["TAX_ID"] = partnerModel.TaxId;
            recordObject["TERMS"] = partnerModel.Terms;
            recordObject["SERVICE_PERF_REQ_IND"] = partnerModel.ServicePerfReqInd;
            recordObject["INVC_PAY_LOC"] = partnerModel.InvcPayLoc;
            recordObject["INVC_RECEIVE_LOC"] = partnerModel.InvcReceiveLoc;
            recordObject["IMPORT_COUNTRY_ID"] = partnerModel.ImportCountryId;
            recordObject["PRIMARY_IA_IND"] = partnerModel.PrimaryIaInd;
            recordObject["COMMENT_DESC"] = partnerModel.CommentDesc;
            recordObject["TSF_ENTITY_ID"] = partnerModel.TsfEntityId;
            recordObject["VAT_REGION"] = partnerModel.VatRegion;
        }

        public virtual void SetParamsPartnerSave(PartnerModel partnerModel, OracleObject recordObject)
        {
            recordObject["Operation"] = partnerModel.Operation;
            recordObject["CREATED_BY"] = this.GetCreatedBy(partnerModel.CreatedBy);
            recordObject["LAST_UPDATED_BY"] = FxContext.Context.Name;
        }

        private OracleObject SetParamsPartner(PartnerModel partnerModel, bool isSearch)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType(MdmConstants.RecordTypePartner);
            OracleObject recordObject = this.GetOracleObject(recordType);
            if (!isSearch)
            {
                this.SetParamsPartnerSave(partnerModel, recordObject);
            }

            this.SetParamsPartnerSearch(partnerModel, recordObject);

            return recordObject;
        }
    }
}