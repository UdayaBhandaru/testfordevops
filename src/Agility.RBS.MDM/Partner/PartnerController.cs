﻿//-------------------------------------------------------------------------------------------------
// <copyright file="PartnerController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// BrandController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Partner
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Partner.Models;
    using Agility.RBS.MDM.Partner.Repositories;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class PartnerController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocument<PartnerModel> serviceDocument;
        private readonly PartnerRepository partnerRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;

        public PartnerController(
            ServiceDocument<PartnerModel> serviceDocument,
            PartnerRepository partnerRepository,
            DomainDataRepository domainDataRepository)
        {
            this.partnerRepository = partnerRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<PartnerModel>> List()
        {
            this.PartnerDomainData();
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<PartnerModel>> New()
        {
           this.PartnerDomainData();
            this.serviceDocument.LocalizationData.AddData("Partner");
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<PartnerModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.PartnerSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<PartnerModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.PartnerSave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingPartner(string partnerId)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "PARTNER_MST";
            nvc["P_COL1"] = partnerId;
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        private async Task<List<PartnerModel>> PartnerSearch()
        {
            try
            {
                var lstUomClass = await this.partnerRepository.GetPartner(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstUomClass;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> PartnerSave()
        {
            this.serviceDocument.Result = await this.partnerRepository.SetPartner(this.serviceDocument.DataProfile.DataModel);
            return true;
        }

        private void PartnerDomainData()
        {
            this.serviceDocument.DomainData.Add("indicatorStatus", this.domainDataRepository.DomainDetailData(MdmConstants.VatRegionTypeDomainData).Result);
            this.serviceDocument.DomainData.Add("partnerType", this.domainDataRepository.DomainDetailData(MdmConstants.PartnerTypeDomainData).Result);
            this.serviceDocument.DomainData.Add("country", this.domainDataRepository.CountryDomainGet().Result);
            this.serviceDocument.DomainData.Add("location", this.domainDataRepository.LocationDomainGet().Result);
            this.serviceDocument.DomainData.Add("currency", this.domainDataRepository.CurrencyDomainGet().Result);
            this.serviceDocument.DomainData.Add("language", this.domainDataRepository.LanguageDomainGet().Result);
            this.serviceDocument.DomainData.Add("vatRegion", this.domainDataRepository.VatRegionDomainGet().Result);
            this.serviceDocument.DomainData.Add("terms", this.domainDataRepository.TermsDomainGet().Result);
            this.serviceDocument.DomainData.Add("invcPayLoc", this.domainDataRepository.DomainDetailData(MdmConstants.InvPayLocDomainData).Result);
        }
    }
}
