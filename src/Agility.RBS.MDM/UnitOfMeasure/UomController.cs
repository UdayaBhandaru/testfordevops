﻿//-------------------------------------------------------------------------------------------------
// <copyright file="UomController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.UnitOfMeasure
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.UnitOfMeasure.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class UomController : Controller
    {
        private readonly ServiceDocument<UomModel> serviceDocument;
        private readonly UnitOfMeasureRepository uomRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly DomainDataRepository domainDataRepository;

        public UomController(
            ServiceDocument<UomModel> serviceDocument,
            UnitOfMeasureRepository uomRepository,
            DomainDataRepository domainDataRepository)
        {
            this.uomRepository = uomRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<UomModel>> List()
        {
            this.serviceDocument.DomainData.Add("uomclass", this.domainDataRepository.DomainDetailData(MdmConstants.DomainHdrUomC).Result);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<UomModel>> New()
        {
            this.serviceDocument.DomainData.Add("uomclass", this.domainDataRepository.DomainDetailData(MdmConstants.DomainHdrUomC).Result);
            this.serviceDocument.LocalizationData.AddData("Uom");
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<UomModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.UomSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<UomModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.UomSave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingUom(string uomClass, string name)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "UOM_CLASS";
            nvc["P_COL1"] = name;
            nvc["P_COL2"] = uomClass;
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        private async Task<List<UomModel>> UomSearch()
        {
            try
            {
                var lstUom = await this.uomRepository.GetUom(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstUom;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> UomSave()
        {
            this.serviceDocument.Result = await this.uomRepository.SetUom(this.serviceDocument.DataProfile.DataModel);
            return true;
        }
    }
}