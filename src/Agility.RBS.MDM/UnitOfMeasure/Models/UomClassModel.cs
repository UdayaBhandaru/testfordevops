﻿// <copyright file="UomClassModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.UnitOfMeasure.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class UomClassModel : ProfileEntity
    {
        public string UomClass { get; set; }

        public string UomClassDesc { get; set; }

        public string UomClassStatus { get; set; }

        public string UomClassStatusDesc { get; set; }

        public int LangId { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Error { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }
    }
}
