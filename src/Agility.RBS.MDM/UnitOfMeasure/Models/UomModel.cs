﻿//-------------------------------------------------------------------------------------------------
// <copyright file="UomModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.UnitOfMeasure.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class UomModel : ProfileEntity
    {
        public string Uom { get; set; }

        public string UomClass { get; set; }

        public string UomDesc { get; set; }

        public string UomDescription
        {
            get
            {
                return this.UomDesc + "(" + this.Uom + ")";
            }
        }

        public string Operation { get; set; }

        public string TableName { get; set; }
    }
}
