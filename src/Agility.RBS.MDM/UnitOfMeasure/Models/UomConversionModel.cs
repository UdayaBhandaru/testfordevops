﻿//-------------------------------------------------------------------------------------------------
// <copyright file="UomConversionModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UomConversion
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.UnitOfMeasure.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class UomConversionModel : ProfileEntity
    {
        public string FromUom { get; set; }

        public string FromUomDesc { get; set; }

        public string ToUom { get; set; }

        public string ToUomDesc { get; set; }

        public decimal? Factor { get; set; }

        public string Operator { get; set; }

        public string OperatorDesc { get; set; }

        public int LangId { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Error { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }
    }
}
