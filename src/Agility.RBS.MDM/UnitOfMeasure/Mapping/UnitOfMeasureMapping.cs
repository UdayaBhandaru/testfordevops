﻿// <copyright file="UnitOfMeasureMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.UnitOfMeasure.Mapping
{
    using System;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.UnitOfMeasure.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class UnitOfMeasureMapping : Profile
    {
        public UnitOfMeasureMapping()
            : base("UnitOfMeasureMapping")
        {
            this.CreateMap<OracleObject, UomClassModel>()
                .ForMember(m => m.UomClass, opt => opt.MapFrom(r => r["UOM_CLASS"]))
                .ForMember(m => m.UomClassDesc, opt => opt.MapFrom(r => r["UOM_CLASS_DESC"]))
                .ForMember(m => m.UomClassStatus, opt => opt.MapFrom(r => r["UOM_CLASS_STATUS"]))
                .ForMember(m => m.UomClassStatusDesc, opt => opt.MapFrom(r => r["UOM_CLASS_STATUS_DESC"]))
                .ForMember(m => m.CreatedBy, opt => opt.MapFrom(r => r["CREATED_BY"]))
                .ForMember(m => m.ModifiedBy, opt => opt.MapFrom(r => r["LAST_UPDATED_BY"]))
                .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
                .ForMember(m => m.CreatedDate, opt => opt.MapFrom(r => r["CREATED_DATE"]))
                .ForMember(m => m.ModifiedDate, opt => opt.MapFrom(r => r["LAST_UPDATED_DATE"]))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "UOM_CLASS_MST"))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, UomModel>()
               .ForMember(m => m.Uom, opt => opt.MapFrom(r => r["UOM"]))
               .ForMember(m => m.UomClass, opt => opt.MapFrom(r => r["UOM_CLASS"]))
               .ForMember(m => m.UomDesc, opt => opt.MapFrom(r => r["UOM_DESC"]))
               .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"))
               .ForMember(m => m.TableName, opt => opt.MapFrom(r => "UOM_CLASS"));

            this.CreateMap<OracleObject, UomConversionModel>()
               .ForMember(m => m.FromUom, opt => opt.MapFrom(r => r["FROM_UOM"]))
               .ForMember(m => m.FromUomDesc, opt => opt.MapFrom(r => r["FROM_UOM_DESC"]))
               .ForMember(m => m.ToUom, opt => opt.MapFrom(r => r["TO_UOM"]))
               .ForMember(m => m.ToUomDesc, opt => opt.MapFrom(r => r["TO_UOM_DESC"]))
               .ForMember(m => m.Operator, opt => opt.MapFrom(r => r["OPERATOR"]))
               .ForMember(m => m.OperatorDesc, opt => opt.MapFrom(r => r["OPERATOR_DESC"]))
               .ForMember(m => m.Factor, opt => opt.MapFrom(r => r["FACTOR"]))
               .ForMember(m => m.TableName, opt => opt.MapFrom(r => "UOM_CONVERSION"))
               .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));
        }
    }
}
