﻿// <copyright file="UnitOfMeasureRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.UnitOfMeasure
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.UnitOfMeasure.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class UnitOfMeasureRepository : BaseOraclePackage
    {
        public UnitOfMeasureRepository()
        {
            this.PackageName = MdmConstants.GetUomPackage;
        }

        public async Task<List<UomClassModel>> GetUomClass(UomClassModel uomClassModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeUomClass, MdmConstants.GetProcUomClass);
            OracleObject recordObject = this.SetParamsUomClass(uomClassModel, true);
            return await this.GetProcedure2<UomClassModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ServiceDocumentResult> SetUomClass(UomClassModel uomClassModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeUomClass, MdmConstants.SetProcUomClass);
            OracleObject recordObject = this.SetParamsUomClass(uomClassModel, false);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async Task<List<UomModel>> GetUom(UomModel uomModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeUom, MdmConstants.GetProcUom);
            OracleObject recordObject = this.SetParamsUom(uomModel);
            return await this.GetProcedure2<UomModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ServiceDocumentResult> SetUom(UomModel uomModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeUom, MdmConstants.SetProcUom);
            OracleObject recordObject = this.SetParamsUom(uomModel);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async Task<List<UomConversionModel>> GetUomConversion(UomConversionModel uomConversionModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeUomConv, MdmConstants.GetProcUomConv);
            OracleObject recordObject = this.SetSearchParamsUomConv(uomConversionModel);
            return await this.GetProcedure2<UomConversionModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ServiceDocumentResult> SetUomConversion(UomConversionModel uomConversionModel)
        {
            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeUomConv, MdmConstants.SetProcUomConv);
            OracleParameter parameter;
            parameter = new OracleParameter("RESULT", Devart.Data.Oracle.OracleDbType.Boolean)
            {
                Direction = System.Data.ParameterDirection.ReturnValue
            };
            parameters.Add(parameter);
            parameter = new OracleParameter(packageParameter.ObjectTypeTable, Devart.Data.Oracle.OracleDbType.Table)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                ObjectTypeName = packageParameter.ObjectTypeName,
                Value = this.SetParamsUomConversion(uomConversionModel)
            };
            parameters.Add(parameter);
            return await this.SetProcedure(null, packageParameter, parameters);
        }

        public virtual void SetParamsUomClassSearch(UomClassModel uomClassModel, OracleObject recordObject)
        {
            recordObject["UOM_CLASS"] = uomClassModel.UomClass;
            recordObject["UOM_CLASS_DESC"] = uomClassModel.UomClassDesc;
            recordObject["UOM_CLASS_STATUS"] = uomClassModel.UomClassStatus;
        }

        public virtual void SetParamsUomClassSave(UomClassModel uomClassModel, OracleObject recordObject)
        {
            recordObject["Operation"] = uomClassModel.Operation;
            recordObject["CREATED_BY"] = this.GetCreatedBy(uomClassModel.CreatedBy);
            recordObject["LAST_UPDATED_BY"] = FxContext.Context.Name;
        }

        public virtual void SetParamsUomSearch(UomModel uomModel, OracleObject recordObject)
        {
            recordObject["UOM"] = uomModel.Uom;
            recordObject["UOM_CLASS"] = uomModel.UomClass;
            recordObject["UOM_DESC"] = uomModel.UomDesc;
            recordObject["Operation"] = uomModel.Operation;
        }

        public virtual void SetParamsUomConvSearch(UomConversionModel uomConversionModel, OracleObject recordObject)
        {
            recordObject["FROM_UOM"] = uomConversionModel.FromUom;
            recordObject["TO_UOM"] = uomConversionModel.ToUom;
        }

        public virtual OracleTable SetParamsUomConvSave(UomConversionModel uomConversionModel, List<UomConversionModel> uomConversions)
        {
            this.Connection.Open();
            OracleType tableType = OracleType.GetObjectType("UOM_CONV_MST_TAB", this.Connection);
            OracleType recordType = OracleType.GetObjectType("UOM_CONV_MST_REC", this.Connection);
            OracleTable pUomConvTab = new OracleTable(tableType);
            foreach (UomConversionModel uomConversion in uomConversions)
            {
                OracleObject recordObject = new OracleObject(recordType);
                recordObject["FACTOR"] = uomConversionModel.Factor;
                recordObject["OPERATOR"] = uomConversion.Operator;
                recordObject["Operation"] = uomConversionModel.Operation;
                recordObject["FROM_UOM"] = uomConversion.FromUom;
                recordObject["TO_UOM"] = uomConversion.ToUom;

                pUomConvTab.Add(recordObject);
            }

            return pUomConvTab;
        }

        private OracleObject SetParamsUomClass(UomClassModel uomClassModel, bool isSearch)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("UOM_CLASS_MST_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            if (!isSearch)
            {
                this.SetParamsUomClassSave(uomClassModel, recordObject);
            }

            this.SetParamsUomClassSearch(uomClassModel, recordObject);
            return recordObject;
        }

        private OracleObject SetParamsUom(UomModel uomModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("UOM_MST_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            this.SetParamsUomSearch(uomModel, recordObject);
            return recordObject;
        }

        private OracleObject SetSearchParamsUomConv(UomConversionModel uomConversionModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("UOM_CONV_MST_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            this.SetParamsUomConvSearch(uomConversionModel, recordObject);
            return recordObject;
        }

        private OracleTable SetParamsUomConversion(UomConversionModel uomConversionModel)
        {
            List<UomConversionModel> uomConversions = new List<UomConversionModel>();
            var uomConv = new UomConversionModel
            {
                FromUom = uomConversionModel.ToUom,
                ToUom = uomConversionModel.FromUom,
                Operator = uomConversionModel.Operator == "D" ? "M" : "D",
            };

            uomConversions.Add(uomConversionModel);
            uomConversions.Add(uomConv);
            return this.SetParamsUomConvSave(uomConversionModel, uomConversions);
        }
    }
}