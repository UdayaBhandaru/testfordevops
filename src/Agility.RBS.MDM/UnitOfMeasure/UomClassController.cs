﻿//-------------------------------------------------------------------------------------------------
// <copyright file="UomClassController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClassController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.UnitOfMeasure
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.UnitOfMeasure.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class UomClassController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocument<UomClassModel> serviceDocument;
        private readonly UnitOfMeasureRepository uomRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;

        public UomClassController(
            ServiceDocument<UomClassModel> serviceDocument,
            UnitOfMeasureRepository uomRepository,
            DomainDataRepository domainDataRepository)
        {
            this.uomRepository = uomRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<UomClassModel>> List()
        {
            this.serviceDocument.DomainData.Add("status", this.domainDataRepository.DomainDetailData(MdmConstants.DomainHdrStatus).Result);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<UomClassModel>> New()
        {
            this.serviceDocument.DomainData.Add("status", this.domainDataRepository.DomainDetailData(MdmConstants.DomainHdrStatus).Result);
            this.serviceDocument.LocalizationData.AddData("UomClass");
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<UomClassModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.UomClassSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<UomClassModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.UomClassSave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingClass(string name)
        {
            try
            {
                NameValueCollection nvc = new NameValueCollection();
                nvc["P_TAB"] = "UOM_CLASS_MST";
                nvc["P_COL1"] = name;
                return await Task.Run(() =>
                {
                    return this.domainDataRepository.GetDuplicateCheck(nvc);
                });
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return false;
            }
        }

        private async Task<List<UomClassModel>> UomClassSearch()
        {
            try
            {
                var lstUomClass = await this.uomRepository.GetUomClass(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstUomClass;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> UomClassSave()
        {
            this.serviceDocument.Result = await this.uomRepository.SetUomClass(this.serviceDocument.DataProfile.DataModel);
            return true;
        }
    }
}
