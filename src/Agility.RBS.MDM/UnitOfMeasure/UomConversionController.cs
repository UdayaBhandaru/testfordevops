﻿//-------------------------------------------------------------------------------------------------
// <copyright file="UomConversionController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMConversionController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.UnitOfMeasure
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.UnitOfMeasure;
    using Agility.RBS.MDM.UnitOfMeasure.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class UomConversionController : Controller
    {
        private readonly ServiceDocument<UomConversionModel> serviceDocument;
        private readonly UnitOfMeasureRepository uomRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly DomainDataRepository domainDataRepository;

        public UomConversionController(
            ServiceDocument<UomConversionModel> serviceDocument,
            UnitOfMeasureRepository uomRepository,
            DomainDataRepository domainDataRepository)
        {
            this.uomRepository = uomRepository;
            this.serviceDocument = serviceDocument;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<UomConversionModel>> List()
        {
            this.serviceDocument.DomainData.Add("uom", await this.uomRepository.GetUom(new UomModel(), this.serviceDocumentResult));
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<UomConversionModel>> New()
        {
            this.serviceDocument.DomainData.Add("operator", this.domainDataRepository.DomainDetailData(MdmConstants.DomainHdrOperator).Result);
            this.serviceDocument.DomainData.Add("uom", await this.uomRepository.GetUom(new UomModel(), this.serviceDocumentResult));
            this.serviceDocument.LocalizationData.AddData("UomConversion");
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<UomConversionModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.UomConversionSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<UomConversionModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.UomConversionSave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingConversion(string fromUOM, string toUOM)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "UOM_CONVERSION";
            nvc["P_COL1"] = fromUOM;
            nvc["P_COL2"] = toUOM;
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        private async Task<List<UomConversionModel>> UomConversionSearch()
        {
            try
            {
                var lstUomConv = await this.uomRepository.GetUomConversion(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstUomConv;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> UomConversionSave()
        {
            this.serviceDocument.Result = await this.uomRepository.SetUomConversion(this.serviceDocument.DataProfile.DataModel);
            return true;
        }
    }
}