﻿// <copyright file="ChainController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.OrganizationHierarchy
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.MerchantHierarchy;
    using Agility.RBS.MDM.OrganizationHierarchy.Models;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class ChainController : Controller
    {
        private readonly ServiceDocument<ChainModel> serviceDocument;
        private readonly OrganizationHierarchyRepository organizationHierarchyRepository;
        private readonly DomainDataRepository domainDataRepository;

        public ChainController(
            ServiceDocument<ChainModel> serviceDocument,
            OrganizationHierarchyRepository organizationHierarchyRepository,
        DomainDataRepository domainDataRepository)
        {
            this.organizationHierarchyRepository = organizationHierarchyRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<ChainModel>> List()
        {
            return this.serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<ChainModel>> New()
        {
            this.serviceDocument.DomainData.Add("state", this.domainDataRepository.StateDomainGet().Result);
            this.serviceDocument.DomainData.Add("currency", this.domainDataRepository.CurrencyDomainGet().Result);
            this.serviceDocument.LocalizationData.AddData("Chain");
            return this.serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<ChainModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.ChainSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<ChainModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.ChainSave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingChain(int hierValue)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "CHAIN";
            nvc["P_COL1"] = hierValue.ToString();
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        private async Task<List<ChainModel>> ChainSearch()
        {
            try
            {
                ServiceDocumentResult serviceDocumentResult = new ServiceDocumentResult
                {
                    InnerException = string.Empty,
                    StackTrace = string.Empty,
                    Type = MessageType.Success
                };
                var companies = await this.organizationHierarchyRepository.GetChain(this.serviceDocument.DataProfile.DataModel, serviceDocumentResult);
                this.serviceDocument.Result = serviceDocumentResult;
                return companies;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> ChainSave()
        {
            this.serviceDocument.Result = await this.organizationHierarchyRepository.SetChain(this.serviceDocument.DataProfile.DataModel);
            return true;
        }
    }
}
