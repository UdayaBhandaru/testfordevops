﻿//-------------------------------------------------------------------------------------------------
// <copyright file="RegionController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// DivisionController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.OrganizationHierarchy
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.OrganizationHierarchy.Models;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class RegionController : Controller
    {
        private readonly ServiceDocument<RegionModel> serviceDocument;
        private readonly OrganizationHierarchyRepository organizationHierarchyRepository;
        private readonly DomainDataRepository domainDataRepository;

        public RegionController(
            ServiceDocument<RegionModel> serviceDocument,
            OrganizationHierarchyRepository organizationHierarchyRepository,
            DomainDataRepository domainDataRepository)
        {
            this.domainDataRepository = domainDataRepository;
            this.organizationHierarchyRepository = organizationHierarchyRepository;
            this.serviceDocument = serviceDocument;
        }

        public async Task<ServiceDocument<RegionModel>> List()
        {
            return await this.RegionDomainData();
        }

        public async Task<ServiceDocument<RegionModel>> New()
        {
            return await this.RegionDomainData();
        }

        public async Task<ServiceDocument<RegionModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.RegionSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<RegionModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.RegionSave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingRegion(int region, int area)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "REGION";
            nvc["P_COL1"] = region.ToString();
            nvc["P_COL2"] = area.ToString();
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        private async Task<List<RegionModel>> RegionSearch()
        {
            try
            {
                ServiceDocumentResult serviceDocumentResult = new ServiceDocumentResult
                {
                    InnerException = string.Empty,
                    StackTrace = string.Empty,
                    Type = MessageType.Success
                };
                var regions = await this.organizationHierarchyRepository.GetOrgRegion(this.serviceDocument.DataProfile.DataModel, serviceDocumentResult);
                this.serviceDocument.Result = serviceDocumentResult;
                return regions;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> RegionSave()
        {
            this.serviceDocument.Result = await this.organizationHierarchyRepository.SetOrgRegion(this.serviceDocument.DataProfile.DataModel);
            return true;
        }

        private async Task<ServiceDocument<RegionModel>> RegionDomainData()
        {
            this.serviceDocument.DomainData.Add("area", this.domainDataRepository.AreaDomainGet().Result);
            this.serviceDocument.DomainData.Add("currency", this.domainDataRepository.CurrencyDomainGet().Result);
            this.serviceDocument.LocalizationData.AddData("Region");
            return this.serviceDocument;
        }
    }
}
