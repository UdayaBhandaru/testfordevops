﻿// <copyright file="OrganizationHierarchyMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.OrganizationHierarchy.Mapping
{
    using System;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.OrganizationHierarchy.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class OrganizationHierarchyMapping : Profile
    {
        public OrganizationHierarchyMapping()
            : base("OrganizationHierarchyMapping")
        {
            this.CreateMap<OracleObject, CompanyModel>()
                .ForMember(m => m.Company, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["COMPANY"])))
                .ForMember(m => m.CoName, opt => opt.MapFrom(r => r["CO_NAME"]))
                .ForMember(m => m.CoAdd1, opt => opt.MapFrom(r => r["CO_ADD1"]))
                .ForMember(m => m.CoAdd2, opt => opt.MapFrom(r => r["CO_ADD2"]))
                .ForMember(m => m.CoAdd3, opt => opt.MapFrom(r => r["CO_ADD3"]))
                .ForMember(m => m.CoCity, opt => opt.MapFrom(r => r["CO_CITY"]))
                .ForMember(m => m.CoState, opt => opt.MapFrom(r => r["CO_STATE"]))
                .ForMember(m => m.CoCountry, opt => opt.MapFrom(r => r["CO_COUNTRY"]))
                .ForMember(m => m.CoPost, opt => opt.MapFrom(r => r["CO_POST"]))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "COMPANY_MST"))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, OrgCountryModel>()
                .ForMember(m => m.CompanyId, opt => opt.MapFrom(r => r["COMPANY_ID"]))
                .ForMember(m => m.CompanyName, opt => opt.MapFrom(r => r["COMPANY_NAME"]))
                .ForMember(m => m.CountryCode, opt => opt.MapFrom(r => r["COUNTRY_CODE"]))
                .ForMember(m => m.CountryName, opt => opt.MapFrom(r => r["COUNTRY_NAME"]))
                .ForMember(m => m.LegalName, opt => opt.MapFrom(r => r["LEGAL_NAME"]))
                .ForMember(m => m.TaxRegNo, opt => opt.MapFrom(r => r["TAX_REG_NO"]))
                .ForMember(m => m.VatExempt, opt => opt.MapFrom(r => r["VAT_EXEMPT"]))
                .ForMember(m => m.InterCompany, opt => opt.MapFrom(r => r["INTER_COMPANY"]))
                .ForMember(m => m.VatCode, opt => opt.MapFrom(r => r["VAT_CODE"]))
                .ForMember(m => m.PluChargeValue, opt => opt.MapFrom(r => r["PLU_CHARGE_VALUE"]))
                .ForMember(m => m.DefaultWhs, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DEFAULT_WHS"])))
                .ForMember(m => m.CountryStatus, opt => opt.MapFrom(r => r["COUNTRY_STATUS"]))
                .ForMember(m => m.CountryStatusDesc, opt => opt.MapFrom(r => r["COUNTRY_STATUS_DESC"]))
                .ForMember(m => m.CreatedBy, opt => opt.MapFrom(r => r["CREATED_BY"]))
                .ForMember(m => m.ModifiedBy, opt => opt.MapFrom(r => r["LAST_UPDATED_BY"]))
                .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
                .ForMember(m => m.CreatedDate, opt => opt.MapFrom(r => r["CREATED_DATE"]))
                .ForMember(m => m.ModifiedDate, opt => opt.MapFrom(r => r["LAST_UPDATED_DATE"]))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "ORG_COUNTRY_MST"))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, RegionModel>()

               .ForMember(m => m.Region, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["REGION"])))
               .ForMember(m => m.RegionName, opt => opt.MapFrom(r => r["REGION_NAME"]))
               .ForMember(m => m.MgrName, opt => opt.MapFrom(r => r["MGR_NAME"]))
               .ForMember(m => m.Area, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["AREA"])))
               .ForMember(m => m.AreaName, opt => opt.MapFrom(r => r["AREA_NAME"]))
               .ForMember(m => m.CurrencyCode, opt => opt.MapFrom(r => r["CURRENCY_CODE"]))
               .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_ERROR"]))
               .ForMember(m => m.ProgramMessage, opt => opt.MapFrom(r => r["PROGRAM_MESSAGE"]))
               .ForMember(m => m.ProgramPhase, opt => opt.MapFrom(r => r["PROGRAM_PHASE"]))
               .ForMember(m => m.TableName, opt => opt.MapFrom(r => "REGION"))
               .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, FormatModel>()
             .ForMember(m => m.CompanyId, opt => opt.MapFrom(r => r["COMPANY_ID"]))
             .ForMember(m => m.CompanyName, opt => opt.MapFrom(r => r["COMPANY_NAME"]))
             .ForMember(m => m.CountryCode, opt => opt.MapFrom(r => r["COUNTRY_CODE"]))
             .ForMember(m => m.CountryName, opt => opt.MapFrom(r => r["COUNTRY_NAME"]))
             .ForMember(m => m.RegionCode, opt => opt.MapFrom(r => r["REGION_CODE"]))
             .ForMember(m => m.RegionName, opt => opt.MapFrom(r => r["REGION_NAME"]))
             .ForMember(m => m.FormatCode, opt => opt.MapFrom(r => r["FORMAT_CODE"]))
             .ForMember(m => m.FormatName, opt => opt.MapFrom(r => r["FORMAT_NAME"]))
             .ForMember(m => m.LegalName, opt => opt.MapFrom(r => r["LEGAL_NAME"]))
             .ForMember(m => m.TaxRegNo, opt => opt.MapFrom(r => r["TAX_REG_NO"]))
             .ForMember(m => m.VatExempt, opt => opt.MapFrom(r => r["VAT_EXEMPT"]))
             .ForMember(m => m.InterCompany, opt => opt.MapFrom(r => r["INTER_COMPANY"]))
             .ForMember(m => m.VatCode, opt => opt.MapFrom(r => r["VAT_CODE"]))
             .ForMember(m => m.PluChargeValue, opt => opt.MapFrom(r => r["PLU_CHARGE_VALUE"]))
             .ForMember(m => m.DefaultWhs, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DEFAULT_WHS"])))
             .ForMember(m => m.FormatStatus, opt => opt.MapFrom(r => r["FORMAT_STATUS"]))
             .ForMember(m => m.FormatStatusDesc, opt => opt.MapFrom(r => r["FORMAT_STATUS_DESC"]))
             .ForMember(m => m.CreatedBy, opt => opt.MapFrom(r => r["CREATED_BY"]))
             .ForMember(m => m.ModifiedBy, opt => opt.MapFrom(r => r["LAST_UPDATED_BY"]))
             .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
             .ForMember(m => m.CreatedDate, opt => opt.MapFrom(r => r["CREATED_DATE"]))
             .ForMember(m => m.ModifiedDate, opt => opt.MapFrom(r => r["LAST_UPDATED_DATE"]))
             .ForMember(m => m.TableName, opt => opt.MapFrom(r => "FORMAT_MST"))
             .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, ChainModel>()
                .ForMember(m => m.HierValue, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["HIER_VALUE"])))
                .ForMember(m => m.HierLevel, opt => opt.MapFrom(r => r["HIER_LEVEL"]))
                .ForMember(m => m.HierDesc, opt => opt.MapFrom(r => r["HIER_DESC"]))
                .ForMember(m => m.MgrName, opt => opt.MapFrom(r => r["MGR_NAME"]))
                .ForMember(m => m.CurrencyCode, opt => opt.MapFrom(r => r["CURRENCY_CODE"]))
                .ForMember(m => m.ParentId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["PARENT_ID"])))
                .ForMember(m => m.OldParentId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["OLD_PARENT_ID"])))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "CHAIN"))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, AreaModel>()
               .ForMember(m => m.Area, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["AREA"])))
               .ForMember(m => m.AreaName, opt => opt.MapFrom(r => r["AREA_NAME"]))
               .ForMember(m => m.MgrName, opt => opt.MapFrom(r => r["MGR_NAME"]))
               .ForMember(m => m.Chain, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["CHAIN"])))
               .ForMember(m => m.ChainName, opt => opt.MapFrom(r => r["CHAIN_NAME"]))
               .ForMember(m => m.CurrencyCode, opt => opt.MapFrom(r => r["CURRENCY_CODE"]))
               .ForMember(m => m.Perror, opt => opt.MapFrom(r => r["P_ERROR"]))
               .ForMember(m => m.ProgramMessage, opt => opt.MapFrom(r => r["PROGRAM_MESSAGE"]))
               .ForMember(m => m.ProgramPhase, opt => opt.MapFrom(r => r["PROGRAM_PHASE"]))
               .ForMember(m => m.TableName, opt => opt.MapFrom(r => "AREA"))
               .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, DistrictModel>()
             .ForMember(m => m.District, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DISTRICT"])))
             .ForMember(m => m.DistrictName, opt => opt.MapFrom(r => r["DISTRICT_NAME"]))
             .ForMember(m => m.MgrName, opt => opt.MapFrom(r => r["MGR_NAME"]))
             .ForMember(m => m.Region, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["REGION"])))
             .ForMember(m => m.RegionName, opt => opt.MapFrom(r => r["REGION_NAME"]))
             .ForMember(m => m.CurrencyCode, opt => opt.MapFrom(r => r["CURRENCY_CODE"]))
             .ForMember(m => m.Perror, opt => opt.MapFrom(r => r["P_ERROR"]))
             .ForMember(m => m.ProgramMessage, opt => opt.MapFrom(r => r["PROGRAM_MESSAGE"]))
             .ForMember(m => m.ProgramPhase, opt => opt.MapFrom(r => r["PROGRAM_PHASE"]))
             .ForMember(m => m.TableName, opt => opt.MapFrom(r => "DISTRICT"))
             .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));
        }
    }
}
