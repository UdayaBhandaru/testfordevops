﻿// <copyright file="OrganizationHierarchyRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.OrganizationHierarchy
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.OrganizationHierarchy.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class OrganizationHierarchyRepository : BaseOraclePackage
    {
        public OrganizationHierarchyRepository()
        {
            this.PackageName = MdmConstants.GetOrganizationHierarchyPackage;
        }

        public async Task<List<CompanyModel>> GetCompany(CompanyModel companyModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeCompany, MdmConstants.GetProcCompany);
            OracleObject recordObject = this.SetParamsCompany(companyModel);
            return await this.GetProcedure2<CompanyModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<List<ChainModel>> GetChain(ChainModel chainModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeChain, MdmConstants.GetProcChain);
            OracleObject recordObject = this.SetParamsChain(chainModel);
            return await this.GetProcedure2<ChainModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<List<AreaModel>> GetArea(AreaModel chainModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeArea, MdmConstants.GetProcArea);
            OracleObject recordObject = this.SetParamsArea(chainModel);
            return await this.GetProcedure2<AreaModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<List<DistrictModel>> GetDistrict(DistrictModel districtModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDistrict, MdmConstants.GetProcDistrict);
            OracleObject recordObject = this.SetParamsDistrict(districtModel);
            return await this.GetProcedure2<DistrictModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<List<OrgCountryModel>> GetOrgCountry(OrgCountryModel orgCountryModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeOrgCountry, MdmConstants.GetProcOrgCountry);
            OracleObject recordObject = this.SetParamsOrgCountry(orgCountryModel, true);
            return await this.GetProcedure2<OrgCountryModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ServiceDocumentResult> SetOrgCountry(OrgCountryModel orgCountryModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeOrgCountry, MdmConstants.SetProcOrgCountry);
            OracleObject recordObject = this.SetParamsOrgCountry(orgCountryModel, false);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async Task<List<RegionModel>> GetOrgRegion(RegionModel regionModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeOrgRegionNew, MdmConstants.GetProcOrgRegionNew);
            OracleObject recordObject = this.SetParamsOrgRegion(regionModel, true);
            return await this.GetProcedure2<RegionModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ServiceDocumentResult> SetOrgRegion(RegionModel regionModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeOrgRegionNew, MdmConstants.SetProcOrgRegionNew);
            OracleObject recordObject = this.SetParamsOrgRegion(regionModel, false);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async Task<List<FormatModel>> GetOrgFormat(FormatModel formatModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeOrgFormat, MdmConstants.GetProcOrgFormat);
            OracleObject recordObject = this.SetParamsOrgFormat(formatModel, true);
            return await this.GetProcedure2<FormatModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ServiceDocumentResult> SetOrgFormat(FormatModel formatModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeOrgFormat, MdmConstants.SetProcOrgFormat);
            OracleObject recordObject = this.SetParamsOrgFormat(formatModel, false);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async Task<ServiceDocumentResult> SetCompany(CompanyModel companyModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeCompany, MdmConstants.SetProcCompany);
            OracleObject recordObject = this.SetParamsCompanySave(companyModel);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async Task<ServiceDocumentResult> SetChain(ChainModel chainModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeChain, MdmConstants.SetProcChain);
            OracleObject recordObject = this.SetParamsChainSave(chainModel);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async Task<ServiceDocumentResult> SetArea(AreaModel areaModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeArea, MdmConstants.SetProcArea);
            OracleObject recordObject = this.SetParamsAreaSave(areaModel);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async Task<ServiceDocumentResult> SetDistrict(DistrictModel districtModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDistrict, MdmConstants.SetProcDistrict);
            OracleObject recordObject = this.SetParamsDistrictSave(districtModel);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public virtual void SetParamsCompanySearch(CompanyModel companyModel, OracleObject recordObject)
        {
            recordObject["COMPANY"] = companyModel.Company;
            recordObject["CO_NAME"] = companyModel.CoName;
        }

        public virtual void SetParamsChainSearch(ChainModel chainModel, OracleObject recordObject)
        {
            recordObject["HIER_LEVEL"] = chainModel.HierLevel;
            recordObject["HIER_VALUE"] = chainModel.HierValue;
        }

        public virtual void SetParamsAreaSearch(AreaModel areaModel, OracleObject recordObject)
        {
            recordObject["AREA"] = areaModel.Area;
            recordObject["AREA_NAME"] = areaModel.AreaName;
        }

        public virtual void SetParamsDistrictSearch(DistrictModel districtModel, OracleObject recordObject)
        {
            recordObject["District"] = districtModel.District;
            recordObject["DISTRICT_NAME"] = districtModel.DistrictName;
        }

        public virtual void SetParamsOrgCountrySave(OrgCountryModel orgCountryModel, OracleObject recordObject)
        {
            recordObject["TAX_REG_NO"] = orgCountryModel.TaxRegNo;
            recordObject["VAT_EXEMPT"] = orgCountryModel.VatExempt;
            recordObject["INTER_COMPANY"] = orgCountryModel.InterCompany;
            recordObject["VAT_CODE"] = orgCountryModel.VatCode;
            recordObject["PLU_CHARGE_VALUE"] = orgCountryModel.PluChargeValue;
            recordObject["DEFAULT_WHS"] = orgCountryModel.DefaultWhs;
            recordObject["Operation"] = orgCountryModel.Operation;
            recordObject["CREATED_BY"] = this.GetCreatedBy(orgCountryModel.CreatedBy);
            recordObject["LAST_UPDATED_BY"] = FxContext.Context.Name;
        }

        public virtual void SetParamsOrgCountrySearch(OrgCountryModel orgCountryModel, OracleObject recordObject)
        {
            recordObject["COMPANY_ID"] = orgCountryModel.CompanyId;
            recordObject["COUNTRY_CODE"] = orgCountryModel.CountryCode;
            recordObject["COUNTRY_NAME"] = orgCountryModel.CountryName;
            recordObject["LEGAL_NAME"] = orgCountryModel.LegalName;
            recordObject["COUNTRY_STATUS"] = orgCountryModel.CountryStatus;
        }

        public virtual void SetParamsOrgRegionSave(RegionModel regionModel, OracleObject recordObject)
        {
            recordObject["REGION"] = regionModel.Region;
            recordObject["REGION_NAME"] = regionModel.RegionName;
            recordObject["MGR_NAME"] = regionModel.MgrName;
            recordObject["AREA"] = regionModel.Area;
            recordObject["CURRENCY_CODE"] = regionModel.CurrencyCode;
            recordObject["Operation"] = regionModel.Operation;
            ////recordObject["CREATED_BY"] = this.GetCreatedBy(regionModel.CreatedBy);
            ////recordObject["LAST_UPDATED_BY"] = FxContext.Context.Name;
        }

        public virtual void SetParamsOrgRegionSearch(RegionModel regionModel, OracleObject recordObject)
        {
            recordObject["REGION"] = regionModel.Region;
            recordObject["REGION_NAME"] = regionModel.RegionName;
            recordObject["AREA"] = regionModel.Area;
        }

        public virtual void SetParamsOrgFormatSave(FormatModel formatModel, OracleObject recordObject)
        {
            recordObject["COMPANY_ID"] = formatModel.CompanyId;
            recordObject["COUNTRY_CODE"] = formatModel.CountryCode;
            recordObject["REGION_CODE"] = formatModel.RegionCode;
            recordObject["FORMAT_CODE"] = formatModel.FormatCode;
            recordObject["FORMAT_NAME"] = formatModel.FormatName;
            recordObject["LEGAL_NAME"] = formatModel.LegalName;
            recordObject["TAX_REG_NO"] = formatModel.TaxRegNo;
            recordObject["VAT_EXEMPT"] = formatModel.VatExempt;
            recordObject["INTER_COMPANY"] = formatModel.InterCompany;
            recordObject["VAT_CODE"] = formatModel.VatCode;
            recordObject["PLU_CHARGE_VALUE"] = formatModel.PluChargeValue;
            recordObject["DEFAULT_WHS"] = formatModel.DefaultWhs;
            recordObject["Operation"] = formatModel.Operation;
            recordObject["CREATED_BY"] = this.GetCreatedBy(formatModel.CreatedBy);
            recordObject["LAST_UPDATED_BY"] = FxContext.Context.Name;
        }

        public virtual void SetParamsOrgFormatSearch(FormatModel formatModel, OracleObject recordObject)
        {
            recordObject["COMPANY_ID"] = formatModel.CompanyId;
            recordObject["COUNTRY_CODE"] = formatModel.CountryCode;
            recordObject["REGION_CODE"] = formatModel.RegionCode;
            recordObject["FORMAT_CODE"] = formatModel.FormatCode;
            recordObject["FORMAT_NAME"] = formatModel.FormatName;
            recordObject["LEGAL_NAME"] = formatModel.LegalName;
            recordObject["FORMAT_STATUS"] = formatModel.FormatStatus;
        }

        public virtual void SetParamsCompanySave(CompanyModel companyModel, OracleObject recordObject)
        {
            recordObject["COMPANY"] = companyModel.Company;
            recordObject["CO_NAME"] = companyModel.CoName;
            recordObject["CO_ADD1"] = companyModel.CoAdd1;
            recordObject["CO_ADD2"] = companyModel.CoAdd2;
            recordObject["CO_ADD3"] = companyModel.CoAdd3;
            recordObject["CO_CITY"] = companyModel.CoCity;
            recordObject["CO_STATE"] = companyModel.CoState;
            recordObject["CO_COUNTRY"] = companyModel.CoCountry;
            recordObject["CO_POST"] = companyModel.CoPost;
            recordObject["Operation"] = companyModel.Operation;
        }

        public virtual void SetParamsChainSave(ChainModel chainModel, OracleObject recordObject)
        {
            recordObject["HIER_LEVEL"] = chainModel.HierLevel;
            recordObject["HIER_VALUE"] = chainModel.HierValue;
            recordObject["HIER_DESC"] = chainModel.HierDesc;
            recordObject["MGR_NAME"] = chainModel.MgrName;
            recordObject["CURRENCY_CODE"] = chainModel.CurrencyCode;
            recordObject["PARENT_ID"] = chainModel.ParentId;
            recordObject["OLD_PARENT_ID"] = chainModel.OldParentId;
            recordObject["Operation"] = chainModel.Operation;
        }

        public virtual void SetParamsAreaSave(AreaModel chainModel, OracleObject recordObject)
        {
            recordObject["AREA"] = chainModel.Area;
            recordObject["AREA_NAME"] = chainModel.AreaName;
            recordObject["MGR_NAME"] = chainModel.MgrName;
            recordObject["CHAIN"] = chainModel.Chain;
            recordObject["CURRENCY_CODE"] = chainModel.CurrencyCode;
            recordObject["Operation"] = chainModel.Operation;
        }

        public virtual void SetParamsDistrictSave(DistrictModel districtModel, OracleObject recordObject)
        {
            recordObject["DISTRICT"] = districtModel.District;
            recordObject["DISTRICT_NAME"] = districtModel.DistrictName;
            recordObject["REGION"] = districtModel.Region;
            recordObject["REGION_NAME"] = districtModel.RegionName;
            recordObject["MGR_NAME"] = districtModel.MgrName;
            recordObject["CURRENCY_CODE"] = districtModel.CurrencyCode;
            recordObject["Operation"] = districtModel.Operation;
        }

        private OracleObject SetParamsCompany(CompanyModel companyModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("COMPHEAD_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            this.SetParamsCompanySearch(companyModel, recordObject);
            return recordObject;
        }

        private OracleObject SetParamsChain(ChainModel chainModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("ORG_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            this.SetParamsChainSearch(chainModel, recordObject);
            return recordObject;
        }

        private OracleObject SetParamsArea(AreaModel areaModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("AREA_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            this.SetParamsAreaSearch(areaModel, recordObject);
            return recordObject;
        }

        private OracleObject SetParamsDistrict(DistrictModel districtModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("DISTRICT_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            this.SetParamsDistrictSearch(districtModel, recordObject);
            return recordObject;
        }

        private OracleObject SetParamsOrgCountry(OrgCountryModel orgCountryModel, bool isSearch)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("ORG_COUNTRY_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            if (!isSearch)
            {
                this.SetParamsOrgCountrySave(orgCountryModel, recordObject);
            }

            this.SetParamsOrgCountrySearch(orgCountryModel, recordObject);
            return recordObject;
        }

        private OracleObject SetParamsOrgRegion(RegionModel regionModel, bool isSearch)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("REGION_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            if (!isSearch)
            {
                this.SetParamsOrgRegionSave(regionModel, recordObject);
            }
            else
            {
            this.SetParamsOrgRegionSearch(regionModel, recordObject);
            }

            return recordObject;
        }

        private OracleObject SetParamsOrgFormat(FormatModel formatModel, bool isSearch)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("ORG_FORMAT_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            if (!isSearch)
            {
                this.SetParamsOrgFormatSave(formatModel, recordObject);
            }

            this.SetParamsOrgFormatSearch(formatModel, recordObject);
            return recordObject;
        }

        private OracleObject SetParamsCompanySave(CompanyModel companyModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("COMPHEAD_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            this.SetParamsCompanySave(companyModel, recordObject);
            return recordObject;
        }

        private OracleObject SetParamsChainSave(ChainModel chainModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("ORG_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            this.SetParamsChainSave(chainModel, recordObject);
            return recordObject;
        }

        private OracleObject SetParamsAreaSave(AreaModel areaModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("AREA_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            this.SetParamsAreaSave(areaModel, recordObject);
            return recordObject;
        }

        private OracleObject SetParamsDistrictSave(DistrictModel districtModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("DISTRICT_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            this.SetParamsDistrictSave(districtModel, recordObject);
            return recordObject;
        }
    }
}