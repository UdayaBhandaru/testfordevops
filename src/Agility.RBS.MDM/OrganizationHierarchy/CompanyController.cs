﻿//-------------------------------------------------------------------------------------------------
// <copyright file="CompanyController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// ClassController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.OrganizationHierarchy
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.MerchantHierarchy;
    using Agility.RBS.MDM.OrganizationHierarchy.Models;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class CompanyController : Controller
    {
        private readonly ServiceDocument<CompanyModel> serviceDocument;
        private readonly OrganizationHierarchyRepository organizationHierarchyRepository;
        private readonly DomainDataRepository domainDataRepository;

        public CompanyController(
            ServiceDocument<CompanyModel> serviceDocument,
            OrganizationHierarchyRepository organizationHierarchyRepository,
        DomainDataRepository domainDataRepository)
        {
            this.organizationHierarchyRepository = organizationHierarchyRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<CompanyModel>> List()
        {
            return this.serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<CompanyModel>> New()
        {
            this.serviceDocument.DomainData.Add("state", this.domainDataRepository.StateDomainGet().Result);
            this.serviceDocument.DomainData.Add("country", this.domainDataRepository.CountryDomainGet().Result);
            this.serviceDocument.LocalizationData.AddData("Company");
            return this.serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<CompanyModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.CompanySearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<CompanyModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.CompanySave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingCompany(int companyId)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "COMPHEAD";
            nvc["P_COL1"] = companyId.ToString();
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        private async Task<List<CompanyModel>> CompanySearch()
        {
            try
            {
                ServiceDocumentResult serviceDocumentResult = new ServiceDocumentResult
                {
                    InnerException = string.Empty,
                    StackTrace = string.Empty,
                    Type = MessageType.Success
                };
                var companies = await this.organizationHierarchyRepository.GetCompany(this.serviceDocument.DataProfile.DataModel, serviceDocumentResult);
                this.serviceDocument.Result = serviceDocumentResult;
                return companies;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> CompanySave()
        {
            this.serviceDocument.Result = await this.organizationHierarchyRepository.SetCompany(this.serviceDocument.DataProfile.DataModel);
            return true;
        }
    }
}
