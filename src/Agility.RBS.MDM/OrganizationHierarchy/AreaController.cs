﻿// <copyright file="AreaController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.OrganizationHierarchy
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.MerchantHierarchy;
    using Agility.RBS.MDM.OrganizationHierarchy.Models;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class AreaController : Controller
    {
        private readonly ServiceDocument<AreaModel> serviceDocument;
        private readonly OrganizationHierarchyRepository organizationHierarchyRepository;
        private readonly DomainDataRepository domainDataRepository;

        public AreaController(
            ServiceDocument<AreaModel> serviceDocument,
            OrganizationHierarchyRepository organizationHierarchyRepository,
        DomainDataRepository domainDataRepository)
        {
            this.organizationHierarchyRepository = organizationHierarchyRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<AreaModel>> List()
        {
            return this.serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<AreaModel>> New()
        {
            this.serviceDocument.DomainData.Add("Chain", this.domainDataRepository.ChainDomainGet().Result);
            this.serviceDocument.DomainData.Add("currency", this.domainDataRepository.CurrencyDomainGet().Result);
            this.serviceDocument.LocalizationData.AddData("Area");
            return this.serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<AreaModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.AreaSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<AreaModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.AreaSave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingArea(int area)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "AREA";
            nvc["P_COL1"] = area.ToString();
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        private async Task<List<AreaModel>> AreaSearch()
        {
            try
            {
                ServiceDocumentResult serviceDocumentResult = new ServiceDocumentResult
                {
                    InnerException = string.Empty,
                    StackTrace = string.Empty,
                    Type = MessageType.Success
                };
                var companies = await this.organizationHierarchyRepository.GetArea(this.serviceDocument.DataProfile.DataModel, serviceDocumentResult);
                this.serviceDocument.Result = serviceDocumentResult;
                return companies;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> AreaSave()
        {
            this.serviceDocument.Result = await this.organizationHierarchyRepository.SetArea(this.serviceDocument.DataProfile.DataModel);
            return true;
        }
    }
}
