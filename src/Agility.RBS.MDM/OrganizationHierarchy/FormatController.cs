﻿//-------------------------------------------------------------------------------------------------
// <copyright file="FormatController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// DepartmentController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.OrganizationHierarchy
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.Framework.Web.Security.Repositories;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.OrganizationHierarchy.Models;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class FormatController : Controller
    {
        private readonly ServiceDocument<FormatModel> serviceDocument;
        private readonly OrganizationHierarchyRepository organizationHierarchyRepository;
        private readonly DomainDataRepository domainDataRepository;

        public FormatController(
            ServiceDocument<FormatModel> serviceDocument,
            OrganizationHierarchyRepository organizationHierarchyRepository,
            DomainDataRepository domainDataRepository)
        {
            this.organizationHierarchyRepository = organizationHierarchyRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<FormatModel>> List()
        {
            return await this.FormatDomainData();
        }

        [HttpPost]
        public async Task<ServiceDocument<FormatModel>> New()
        {
            await this.GetDomainData();
            return await this.FormatDomainData();
        }

        [HttpPost]
        public async Task<ServiceDocument<FormatModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.FormatSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<FormatModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.FormatSave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingFormat(int companyId, string countryCode, string regionCode, string formatCode)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "FORMAT_MST";
            nvc["P_COL1"] = companyId.ToString();
            nvc["P_COL2"] = countryCode;
            nvc["P_COL3"] = regionCode;
            nvc["P_COL4"] = formatCode;
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        private async Task<List<FormatModel>> FormatSearch()
        {
            try
            {
                ServiceDocumentResult serviceDocumentResult = new ServiceDocumentResult
                {
                    InnerException = string.Empty,
                    StackTrace = string.Empty,
                    Type = MessageType.Success
                };
                var formats = await this.organizationHierarchyRepository.GetOrgFormat(this.serviceDocument.DataProfile.DataModel, serviceDocumentResult);
                this.serviceDocument.Result = serviceDocumentResult;
                return formats;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> FormatSave()
        {
            this.serviceDocument.Result = await this.organizationHierarchyRepository.SetOrgFormat(this.serviceDocument.DataProfile.DataModel);
            return true;
        }

        private async Task<ServiceDocument<FormatModel>> FormatDomainData()
        {
            ServiceDocumentResult serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
            RegionModel regionModel = new RegionModel();
            this.serviceDocument.DomainData.Add("company", this.domainDataRepository.CompanyDomainGet().Result);
            this.serviceDocument.DomainData.Add("orgcountry", this.domainDataRepository.OrgCountryDomainGet().Result);
            this.serviceDocument.DomainData.Add("region", await this.organizationHierarchyRepository.GetOrgRegion(regionModel, serviceDocumentResult));
            this.serviceDocument.DomainData.Add("status", this.domainDataRepository.DomainDetailData(MdmConstants.DomainHdrStatus).Result);
            return this.serviceDocument;
        }

        private async Task GetDomainData()
        {
            this.serviceDocument.DomainData.Add("indicator", this.domainDataRepository.DomainDetailData(MdmConstants.DomainHdrIndicator).Result);
            this.serviceDocument.DomainData.Add("vatcode", this.domainDataRepository.VatCodeDomainGet().Result);
            this.serviceDocument.DomainData.Add("location", this.domainDataRepository.GetLocationByWherehouseBasedDomainGet().Result);
            this.serviceDocument.LocalizationData.AddData("Format");
        }
    }
}
