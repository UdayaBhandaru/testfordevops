﻿// <copyright file="DistrictController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.OrganizationHierarchy
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.MerchantHierarchy;
    using Agility.RBS.MDM.OrganizationHierarchy.Models;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class DistrictController : Controller
    {
        private readonly ServiceDocument<DistrictModel> serviceDocument;
        private readonly OrganizationHierarchyRepository organizationHierarchyRepository;
        private readonly DomainDataRepository domainDataRepository;

        public DistrictController(
            ServiceDocument<DistrictModel> serviceDocument,
            OrganizationHierarchyRepository organizationHierarchyRepository,
        DomainDataRepository domainDataRepository)
        {
            this.organizationHierarchyRepository = organizationHierarchyRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<DistrictModel>> List()
        {
            return this.serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<DistrictModel>> New()
        {
            ServiceDocumentResult serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
            RegionModel regionModel = new RegionModel();
            var regions = await this.organizationHierarchyRepository.GetOrgRegion(regionModel, serviceDocumentResult);

            this.serviceDocument.DomainData.Add("Regions", regions);
            this.serviceDocument.DomainData.Add("currency", this.domainDataRepository.CurrencyDomainGet().Result);
            this.serviceDocument.LocalizationData.AddData("District");
            return this.serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<DistrictModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.DistrictSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<DistrictModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.DistrictSave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingDistrict(int district)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "DISTRICT";
            nvc["P_COL1"] = district.ToString();
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        private async Task<List<DistrictModel>> DistrictSearch()
        {
            try
            {
                ServiceDocumentResult serviceDocumentResult = new ServiceDocumentResult
                {
                    InnerException = string.Empty,
                    StackTrace = string.Empty,
                    Type = MessageType.Success
                };
                var companies = await this.organizationHierarchyRepository.GetDistrict(this.serviceDocument.DataProfile.DataModel, serviceDocumentResult);
                this.serviceDocument.Result = serviceDocumentResult;
                return companies;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> DistrictSave()
        {
            this.serviceDocument.Result = await this.organizationHierarchyRepository.SetDistrict(this.serviceDocument.DataProfile.DataModel);
            return true;
        }
    }
}
