﻿// <copyright file="FormatModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.OrganizationHierarchy.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class FormatModel : ProfileEntity
    {
        public FormatModel()
        {
            this.Operation = "I";
        }

        public int? CompanyId { get; set; }

        public string CompanyName { get; set; }

        public string CountryCode { get; set; }

        public string CountryName { get; set; }

        public string RegionCode { get; set; }

        public string RegionName { get; set; }

        public string FormatCode { get; set; }

        public string FormatName { get; set; }

        public string LegalName { get; set; }

        public string TaxRegNo { get; set; }

        public string VatExempt { get; set; }

        public string InterCompany { get; set; }

        public string VatCode { get; set; }

        public string PluChargeValue { get; set; }

        public int? DefaultWhs { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }

        public string FormatStatus { get; set; }

        public string FormatStatusDesc { get; set; }

        public string Error { get; set; }
    }
}
