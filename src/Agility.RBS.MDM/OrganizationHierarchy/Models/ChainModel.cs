﻿// <copyright file="ChainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.OrganizationHierarchy.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class ChainModel : ProfileEntity
    {
        public int? HierValue { get; set; }

        public string HierLevel { get; set; }

        public string HierDesc { get; set; }

        public string MgrName { get; set; }

        public string CurrencyCode { get; set; }

        public int? ParentId { get; set; }

        public int? OldParentId { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }
    }
}
