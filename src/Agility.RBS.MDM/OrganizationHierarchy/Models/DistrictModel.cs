﻿// <copyright file="DistrictModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.OrganizationHierarchy.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class DistrictModel : ProfileEntity
    {
        public int? District { get; set; }

        public string DistrictName { get; set; }

        public string MgrName { get; set; }

        public int? Region { get; set; }

        public string RegionName { get; set; }

        public string CurrencyCode { get; set; }

        public string Operation { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Perror { get; set; }

        public string TableName { get; set; }
    }
}
