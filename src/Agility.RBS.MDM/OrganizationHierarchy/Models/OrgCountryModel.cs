﻿// <copyright file="OrgCountryModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.OrganizationHierarchy.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class OrgCountryModel : ProfileEntity
    {
        public OrgCountryModel()
        {
            this.Operation = "I";
        }

        public int? CompanyId { get; set; }

        public string CompanyName { get; set; }

        public string CountryCode { get; set; }

        public string CountryName { get; set; }

        public string LegalName { get; set; }

        public string TaxRegNo { get; set; }

        public string VatExempt { get; set; }

        public string InterCompany { get; set; }

        public string VatCode { get; set; }

        public string PluChargeValue { get; set; }

        public int? DefaultWhs { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }

        public string CountryStatus { get; set; }

        public string CountryStatusDesc { get; set; }

        public string Error { get; set; }
    }
}