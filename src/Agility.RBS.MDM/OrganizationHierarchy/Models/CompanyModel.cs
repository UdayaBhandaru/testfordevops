﻿// <copyright file="CompanyModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.OrganizationHierarchy.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class CompanyModel : ProfileEntity
    {
        public int? Company { get; set; }

        public string CoName { get; set; }

        public string CoAdd1 { get; set; }

        public string CoAdd2 { get; set; }

        public string CoAdd3 { get; set; }

        public string CoCity { get; set; }

        public string CoState { get; set; }

        public string CoCountry { get; set; }

        public string CoPost { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }
    }
}
