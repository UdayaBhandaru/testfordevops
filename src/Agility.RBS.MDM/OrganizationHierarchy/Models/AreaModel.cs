﻿// <copyright file="AreaModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.OrganizationHierarchy.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class AreaModel : ProfileEntity
    {
        public int? Area { get; set; }

        public string AreaName { get; set; }

        public string MgrName { get; set; }

        public int? Chain { get; set; }

        public string ChainName { get; set; }

        public string CurrencyCode { get; set; }

        public string Operation { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Perror { get; set; }

        public string TableName { get; set; }
    }
}
