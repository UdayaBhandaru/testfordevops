﻿// <copyright file="RegionModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.OrganizationHierarchy.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class RegionModel : ProfileEntity
    {
        public RegionModel()
        {
            this.Operation = "I";
        }

        public int? Region { get; set; }

        public string RegionName { get; set; }

        public string MgrName { get; set; }

        public int? Area { get; set; }

        public string AreaName { get; set; }

        public string CurrencyCode { get; set; }

        public string Operation { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Error { get; set; }

        public string TableName { get; set; }

        public int? CompanyId { get; set; }

        public string CompanyName { get; set; }

        public string CountryCode { get; set; }

        public string CountryName { get; set; }

        public string LegalName { get; set; }

        public string TaxRegNo { get; set; }

        public string VatExempt { get; set; }

        public string InterCompany { get; set; }

        public string VatCode { get; set; }

        public string PluChargeValue { get; set; }

        public int? DefaultWhs { get; set; }

        public string RegionCode { get; set; }

        public string RegionStatus { get; set; }

        public string RegionStatusDesc { get; set; }
    }
}
