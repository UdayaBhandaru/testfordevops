﻿// <copyright file="LocationModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.OrganizationHierarchy.Models
{
    using System;
    using Agility.Framework.Core.Profile.Entities;

    public class LocationModel : ProfileEntity
    {
        public LocationModel()
        {
            this.Operation = "I";
        }

        public int? CompanyId { get; set; }

        public int? LocationId { get; set; }

        public int? DefaultWareHouse { get; set; }

        public int? SisterStore { get; set; }

        public int? TsfEntityId { get; set; }

        public int? StopOrderDays { get; set; }

        public int? StartOrderDays { get; set; }

        public int? TotalSqaureFeet { get; set; }

        public int? SellingSquareFeet { get; set; }

        public int? LinearDistance { get; set; }

        public int? OrganizationUnitId { get; set; }

        public string MgrId { get; set; }

        public DateTime? OpenDate { get; set; }

        public DateTime? CloseDate { get; set; }

        public DateTime? AcquiredDate { get; set; }

        public DateTime? ReModelDate { get; set; }

        public string Code { get; set; }

        public string CompanyName { get; set; }

        public string CountryCode { get; set; }

        public string CountryName { get; set; }

        public string RegionCode { get; set; }

        public string RegionName { get; set; }

        public string FormatCode { get; set; }

        public string FormatName { get; set; }

        public string LocationName { get; set; }

        public string Type { get; set; }

        public string TypeName { get; set; }

        public string Name10 { get; set; }

        public string Name3 { get; set; }

        public string NameArabic { get; set; }

        public string AnchorStore { get; set; }

        public string AnchorStoreName { get; set; }

        public string MallName { get; set; }

        public string StoreFormat { get; set; }

        public string StoreFormatName { get; set; }

        public string VatRegion { get; set; }

        public string VatRegionName { get; set; }

        public string TransferZone { get; set; }

        public string Language { get; set; }

        public string LanguageName { get; set; }

        public string CurrencyCode { get; set; }

        public string CurrencyName { get; set; }

        public string LocationClass { get; set; }

        public string ClassName { get; set; }

        public string MgrName { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string Address3 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Phone1 { get; set; }

        public string Phone2 { get; set; }

        public string Phone3 { get; set; }

        public string Fax1 { get; set; }

        public string Fax2 { get; set; }

        public string GeoCode { get; set; }

        public string Email { get; set; }

        public string VatIncludeInd { get; set; }

        public string VatIncludeIndDesc { get; set; }

        public string StockHoldingInd { get; set; }

        public string StockHoldingIndDesc { get; set; }

        public string PriceZone { get; set; }

        public string PriceZoneName { get; set; }

        public string CostZone { get; set; }

        public string CostZoneName { get; set; }

        public string TranNumGenerated { get; set; }

        public string IntegratedPosInd { get; set; }

        public string OriginCurrencyCode { get; set; }

        public string OriginCurrencyName { get; set; }

        public string Status { get; set; }

        public string StatusDesc { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }

        public string Error { get; set; }
    }
}