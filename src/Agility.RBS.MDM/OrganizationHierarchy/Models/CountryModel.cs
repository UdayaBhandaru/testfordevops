﻿// <copyright file="CountryModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.OrganizationHierarchy.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class CountryModel : ProfileEntity
    {
        public string CountryCode { get; set; }

        public string CountryName { get; set; }
    }
}
