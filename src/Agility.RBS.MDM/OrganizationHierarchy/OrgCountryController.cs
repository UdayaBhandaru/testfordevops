﻿// <copyright file="OrgCountryController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.OrganizationHierarchy
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.OrganizationHierarchy.Models;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class OrgCountryController : Controller
    {
        private readonly ServiceDocument<OrgCountryModel> serviceDocument;
        private readonly OrganizationHierarchyRepository organizationHierarchyRepository;
        private readonly DomainDataRepository domainDataRepository;

        public OrgCountryController(
            ServiceDocument<OrgCountryModel> serviceDocument,
            OrganizationHierarchyRepository organizationHierarchyRepository,
            DomainDataRepository domainDataRepository)
        {
            this.domainDataRepository = domainDataRepository;
            this.organizationHierarchyRepository = organizationHierarchyRepository;
            this.serviceDocument = serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<OrgCountryModel>> List()
        {
            return await this.OrgCountryDomainData();
        }

        [HttpPost]
        public async Task<ServiceDocument<OrgCountryModel>> New()
        {
            await this.GetDomainData();
            return await this.OrgCountryDomainData();
        }

        [HttpPost]
        public async Task<ServiceDocument<OrgCountryModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.OrgCountrySearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<OrgCountryModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.OrgCountrySave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingOrgCountry(int companyId, string countryCode)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "ORG_COUNTRY_MST";
            nvc["P_COL1"] = companyId.ToString();
            nvc["P_COL2"] = countryCode;
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        private async Task<List<OrgCountryModel>> OrgCountrySearch()
        {
            try
            {
                ServiceDocumentResult serviceDocumentResult = new ServiceDocumentResult
                {
                    InnerException = string.Empty,
                    StackTrace = string.Empty,
                    Type = MessageType.Success
                };
                var orgCountries = await this.organizationHierarchyRepository.GetOrgCountry(this.serviceDocument.DataProfile.DataModel, serviceDocumentResult);
                this.serviceDocument.Result = serviceDocumentResult;
                return orgCountries;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> OrgCountrySave()
        {
            this.serviceDocument.Result = await this.organizationHierarchyRepository.SetOrgCountry(this.serviceDocument.DataProfile.DataModel);
            return true;
        }

        private async Task<ServiceDocument<OrgCountryModel>> OrgCountryDomainData()
        {
            this.serviceDocument.DomainData.Add("status", this.domainDataRepository.DomainDetailData(MdmConstants.DomainHdrStatus).Result);
            ////this.serviceDocument.DomainData.Add("company", await this.organizationHierarchyRepository.GetCompany(companyModel, serviceDocumentResult));
            this.serviceDocument.DomainData.Add("country", this.domainDataRepository.CountryDomainGet().Result);
            return this.serviceDocument;
        }

        private async Task GetDomainData()
        {
            this.serviceDocument.DomainData.Add("indicator", this.domainDataRepository.DomainDetailData(MdmConstants.DomainHdrIndicator).Result);
            this.serviceDocument.DomainData.Add("vatcode", this.domainDataRepository.VatCodeDomainGet().Result);
            this.serviceDocument.DomainData.Add("location", this.domainDataRepository.GetLocationByWherehouseBasedDomainGet().Result);
            this.serviceDocument.LocalizationData.AddData("OrgCountry");
        }
    }
}