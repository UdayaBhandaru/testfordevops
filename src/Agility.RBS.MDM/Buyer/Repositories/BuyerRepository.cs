﻿// <copyright file="BuyerRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// CostZoneRepository
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Buyer.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.MDM.Buyer.Models;
    using Agility.RBS.MDM.Common;
    using AutoMapper;
    using Devart.Data.Oracle;
    using Microsoft.AspNetCore.Authorization;

    public class BuyerRepository : BaseOraclePackage
    {
        public BuyerRepository()
        {
            this.PackageName = MdmConstants.GetBuyerPackage;
        }

        public async Task<List<BuyerModel>> GetBuyer(BuyerModel buyerModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeBuyer, MdmConstants.GetBuyer);
            OracleObject recordObject = this.SetParamsBuyer(buyerModel, true);
            return await this.GetProcedure2<BuyerModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ServiceDocumentResult> SetBuyer(BuyerModel buyerModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeBuyer, MdmConstants.SetProcBuyer);
            OracleObject recordObject = this.SetParamsBuyer(buyerModel, false);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        private OracleObject SetParamsBuyer(BuyerModel buyerModel, bool isSearch)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(MdmConstants.RecordTypeBuyer, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            if (!isSearch)
            {
                recordObject[recordType.Attributes["Operation"]] = buyerModel.Operation;
            }

            recordObject[recordType.Attributes["buyer"]] = buyerModel.Buyer;
            recordObject[recordType.Attributes["buyer_name"]] = buyerModel.BuyerName;
            recordObject[recordType.Attributes["buyer_phone"]] = buyerModel.BuyerPhone;
            recordObject[recordType.Attributes["buyer_fax"]] = buyerModel.BuyerFax;

            return recordObject;
        }
    }
}