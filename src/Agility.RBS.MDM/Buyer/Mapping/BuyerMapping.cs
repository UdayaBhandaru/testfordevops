﻿// <copyright file="BuyerMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Buyer.Mapping
{
    using System;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.Buyer.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class BuyerMapping : Profile
    {
        public BuyerMapping()
            : base("BuyerMapping")
        {
            this.CreateMap<OracleObject, BuyerModel>()
              .ForMember(m => m.Buyer, opt => opt.MapFrom(r => r["buyer"]))
              .ForMember(m => m.BuyerName, opt => opt.MapFrom(r => r["buyer_name"]))
              .ForMember(m => m.BuyerPhone, opt => opt.MapFrom(r => r["buyer_phone"]))
              .ForMember(m => m.BuyerFax, opt => opt.MapFrom(r => r["buyer_fax"]))
              .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
              .ForMember(m => m.TableName, opt => opt.MapFrom(r => "BUYER"))
              .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));
        }
    }
}
