﻿//-------------------------------------------------------------------------------------------------
// <copyright file="BuyerController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// BuyerController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Buyer
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Buyer.Models;
    using Agility.RBS.MDM.Buyer.Repositories;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    [Route("api/[controller]/[action]")]

    public class BuyerController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocument<BuyerModel> serviceDocument;
        private readonly BuyerRepository buyerRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;

        public BuyerController(
            ServiceDocument<BuyerModel> serviceDocument,
           BuyerRepository buyerRepository,
            DomainDataRepository domainDataRepository)
        {
            this.buyerRepository = buyerRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<BuyerModel>> List()
        {
            this.serviceDocument.DomainData.Add("status", this.domainDataRepository.DomainDetailData(MdmConstants.DomainHdrStatus).Result);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<BuyerModel>> New()
        {
            this.serviceDocument.DomainData.Add("status", this.domainDataRepository.DomainDetailData(MdmConstants.DomainHdrStatus).Result);
            this.serviceDocument.LocalizationData.AddData("Buyer");
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<BuyerModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.BuyerSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<BuyerModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.BuyerSave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingBuyer(int buyer)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "BUYER";
            nvc["P_COL1"] = buyer.ToString();
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        private async Task<List<BuyerModel>> BuyerSearch()
        {
            try
            {
                var lstUomClass = await this.buyerRepository.GetBuyer(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstUomClass;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> BuyerSave()
        {
            this.serviceDocument.Result = await this.buyerRepository.SetBuyer(this.serviceDocument.DataProfile.DataModel);
            return true;
        }
    }
}
