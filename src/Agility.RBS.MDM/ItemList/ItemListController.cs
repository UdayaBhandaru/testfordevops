﻿// <copyright file="ItemListController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.ItemList
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.Framework.Web.Security.Repositories;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.ItemList.Models;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class ItemListController : Controller
    {
        private readonly ServiceDocument<ItemListHeadModel> serviceDocument;
        private readonly ItemListRepository itemListRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly DomainDataRepository domainDataRepository;

        public ItemListController(
            ServiceDocument<ItemListHeadModel> serviceDocument,
            ItemListRepository itemListRepository,
            DomainDataRepository domainDataRepository)
        {
            this.itemListRepository = itemListRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<ItemListHeadModel>> List()
        {
            this.serviceDocument.DomainData.Add("indicator", this.domainDataRepository.DomainDetailData(MdmConstants.VatRegionTypeDomainData).Result);
            this.serviceDocument.LocalizationData.AddData("ItemList");
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<ItemListHeadModel>> New()
        {
            this.serviceDocument.DomainData.Add("indicator", this.domainDataRepository.DomainDetailData(MdmConstants.VatRegionTypeDomainData).Result);
            this.serviceDocument.LocalizationData.AddData("ItemList");
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<ItemListHeadModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.ItemListHeadSearch);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async Task<ItemListDetailModel> SearchItemGet(string item)
        {
            try
            {
                return await this.itemListRepository.SelectItems(item, this.serviceDocumentResult);
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        public async Task<ServiceDocument<ItemListHeadModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.ItemListHeadSave);
            return this.serviceDocument;
        }

        private async Task<List<ItemListHeadModel>> ItemListHeadSearch()
        {
            try
            {
                var categories = await this.itemListRepository.GetItemListHead(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return categories;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> ItemListHeadSave()
        {
            this.serviceDocument.Result = await this.itemListRepository.SetItemListHead(this.serviceDocument.DataProfile.DataModel);
            return true;
        }
    }
}
