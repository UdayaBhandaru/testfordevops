﻿// <copyright file="ItemListMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.ItemList.Mapping
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.ItemList.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class ItemListMapping : Profile
    {
        public ItemListMapping()
            : base("ItemListMapping")
        {
            this.CreateMap<OracleObject, ItemListHeadModel>()
                .ForMember(m => m.ListId, opt => opt.MapFrom(r => r["LIST_ID"]))
                .ForMember(m => m.ListDesc, opt => opt.MapFrom(r => r["LIST_DESC"]))
                .ForMember(m => m.StaticInd, opt => opt.MapFrom(r => r["STATIC_IND"]))
                .ForMember(m => m.StaticIndDesc, opt => opt.MapFrom(r => r["STATIC_IND_DESC"]))
                .ForMember(m => m.CommentDesc, opt => opt.MapFrom(r => r["COMMENT_DESC"]))
                .ForMember(m => m.VisibilityInd, opt => opt.MapFrom(r => r["VISIBILITY_IND"]))
                .ForMember(m => m.VisibilityIndDesc, opt => opt.MapFrom(r => r["VISIBILITY_IND_DESC"]))
                .ForMember(m => m.FilterOrgId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["FILTER_ORG_ID"])))
                .ForMember(m => m.LastRebuildDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["LAST_REBUILD_DATE"])))
                .ForMember(m => m.ListStatus, opt => opt.MapFrom(r => r["LIST_STATUS"]))
                .ForMember(m => m.ListStatusDesc, opt => opt.MapFrom(r => r["LIST_STATUS_DESC"]))
                .ForMember(m => m.CreatedBy, opt => opt.MapFrom(r => r["CREATED_BY"]))
                .ForMember(m => m.ModifiedBy, opt => opt.MapFrom(r => r["LAST_UPDATED_BY"]))
                .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
                .ForMember(m => m.CreatedDate, opt => opt.MapFrom(r => r["CREATED_DATE"]))
                .ForMember(m => m.ModifiedDate, opt => opt.MapFrom(r => r["LAST_UPDATED_DATE"]))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "ITEMLIST_HEAD"))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, ItemListDetailModel>()
                .ForMember(m => m.ListId, opt => opt.MapFrom(r => r["LIST_ID"]))
                .ForMember(m => m.Item, opt => opt.MapFrom(r => r["ITEM"]))
                .ForMember(m => m.ItemDesc, opt => opt.MapFrom(r => r["ITEM_DESC"]))
                .ForMember(m => m.DivisionId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DIVISION_ID"])))
                .ForMember(m => m.DivisionDesc, opt => opt.MapFrom(r => r["DIVISION_DESC"]))
                .ForMember(m => m.DeptId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DEPT_ID"])))
                .ForMember(m => m.DeptDesc, opt => opt.MapFrom(r => r["DEPT_DESC"]))
                .ForMember(m => m.CategoryId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["CATEGORY_ID"])))
                .ForMember(m => m.CategoryDesc, opt => opt.MapFrom(r => r["CATEGORY_DESC"]))
                .ForMember(m => m.LineStatus, opt => opt.MapFrom(r => r["LINE_STATUS"]))
                .ForMember(m => m.LineStatusDesc, opt => opt.MapFrom(r => r["LINE_STATUS_DESC"]))
                .ForMember(m => m.CreatedBy, opt => opt.MapFrom(r => r["CREATED_BY"]))
                .ForMember(m => m.ModifiedBy, opt => opt.MapFrom(r => r["LAST_UPDATED_BY"]))
                .ForMember(m => m.CreatedDate, opt => opt.MapFrom(r => r["CREATED_DATE"]))
                .ForMember(m => m.ModifiedDate, opt => opt.MapFrom(r => r["LAST_UPDATED_DATE"]))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));
        }
    }
}
