﻿// <copyright file="ItemListRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.ItemList
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.ItemList.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class ItemListRepository : BaseOraclePackage
    {
        public ItemListRepository()
        {
            this.PackageName = MdmConstants.GetItemListPackage;
        }

        public async Task<List<ItemListHeadModel>> GetItemListHead(ItemListHeadModel itemListHeadModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeItemListHead, MdmConstants.GetProcItemListHead);
            OracleObject recordObject = this.SetParamsItemList(itemListHeadModel, true);

            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            parameters.Add(this.ParameterBuilder(UserProfileHelper.GetUserParameter()));
            OracleTable pItemListTab = await this.GetProcedure(recordObject, packageParameter, serviceDocumentResult, parameters);

            var lstHeader = Mapper.Map<List<ItemListHeadModel>>(pItemListTab);
            if (lstHeader != null)
            {
                for (int i = 0; i < lstHeader.Count; i++)
                {
                    OracleObject obj = (OracleObject)pItemListTab[i];
                    OracleTable itemListDetails = (Devart.Data.Oracle.OracleTable)obj["ITEM_LIST_DTL"];
                    if (itemListDetails.Count > 0)
                    {
                        lstHeader[i].ItemListDetails.AddRange(Mapper.Map<List<ItemListDetailModel>>(itemListDetails));
                    }
                }
            }

            return lstHeader;
        }

        public async Task<ServiceDocumentResult> SetItemListHead(ItemListHeadModel itemListHeadModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeItemListHead, MdmConstants.SetProcItemListHead);
            OracleObject recordObject = this.SetParamsItemList(itemListHeadModel, false);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async Task<ItemListDetailModel> SelectItems(string item, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            ItemListDetailModel itemListDetailModel = null;
            OracleType recordType = OracleType.GetObjectType("find_item_list_rec", this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["ITEM_BARCODE"]] = item;
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.FindItemListTab, MdmConstants.GetItemList);
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            parameters.Add(this.ParameterBuilder(UserProfileHelper.GetUserParameter()));
            OracleTable pItemTab = await this.GetProcedure(recordObject, packageParameter, serviceDocumentResult, parameters);
            OracleObject obj = (OracleObject)pItemTab[0];
            if (obj != null)
            {
                itemListDetailModel = new ItemListDetailModel
                {
                    Item = Convert.ToString(obj["item_barcode"]),
                    ItemDesc = Convert.ToString(obj["item_desc"]),
                    SupplierId = Convert.ToInt32(OracleNullHandler.DbNullIntHandler(obj["SUPPLIER_ID"])),
                    SuppName = Convert.ToString(obj["supp_name"]),
                    DivisionId = Convert.ToInt32(OracleNullHandler.DbNullIntHandler(obj["division_id"])),
                    DivisionDesc = Convert.ToString(obj["division_desc"]),
                    DeptId = Convert.ToInt32(OracleNullHandler.DbNullIntHandler(obj["dept_id"])),
                    DeptDesc = Convert.ToString(obj["dept_desc"]),
                    CategoryId = Convert.ToInt32(OracleNullHandler.DbNullIntHandler(obj["category_id"])),
                    CategoryDesc = Convert.ToString(obj["category_desc"]),
                    ClassDesc = Convert.ToString(obj["class_desc"]),
                    SubclassDesc = Convert.ToString(obj["sub_class_desc"])
                };
            }

            return itemListDetailModel;
        }

        private OracleObject SetParamsItemList(ItemListHeadModel itemListHeadModel, bool isSearch)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType("ITEM_LIST_HDR_REC", this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            if (!isSearch)
            {
                recordObject[recordType.Attributes["CREATED_BY"]] = FxContext.Context.Name;
                recordObject[recordType.Attributes["LAST_UPDATED_BY"]] = FxContext.Context.Name;
                recordObject[recordType.Attributes["Operation"]] = itemListHeadModel.Operation;

                // Detail Object
                OracleType dtlTableType = OracleType.GetObjectType("ITEM_LIST_DTL_TAB", this.Connection);
                OracleType dtlRecordType = OracleType.GetObjectType("ITEM_LIST_DTL_REC", this.Connection);
                OracleTable pUtlDmnDtlTab = new OracleTable(dtlTableType);
                foreach (ItemListDetailModel detail in itemListHeadModel.ItemListDetails)
                {
                    var dtlRecordObject = new OracleObject(dtlRecordType);
                    dtlRecordObject[dtlRecordType.Attributes["LIST_ID"]] = itemListHeadModel.ListId;
                    dtlRecordObject[dtlRecordType.Attributes["ITEM"]] = detail.Item;
                    dtlRecordObject[dtlRecordType.Attributes["LINE_STATUS"]] = detail.LineStatus;
                    dtlRecordObject[dtlRecordType.Attributes["CREATED_BY"]] = this.GetCreatedBy(detail.CreatedBy);
                    dtlRecordObject[dtlRecordType.Attributes["LAST_UPDATED_BY"]] = FxContext.Context.Name;
                    dtlRecordObject[dtlRecordType.Attributes["Operation"]] = detail.Operation;
                    pUtlDmnDtlTab.Add(dtlRecordObject);
                }

                recordObject[recordType.Attributes["ITEM_LIST_DTL"]] = pUtlDmnDtlTab;
            }

            recordObject[recordType.Attributes["LIST_ID"]] = itemListHeadModel.ListId;
            recordObject[recordType.Attributes["LIST_DESC"]] = itemListHeadModel.ListDesc;
            recordObject[recordType.Attributes["STATIC_IND"]] = itemListHeadModel.StaticInd;
            recordObject[recordType.Attributes["COMMENT_DESC"]] = itemListHeadModel.CommentDesc;
            recordObject[recordType.Attributes["VISIBILITY_IND"]] = itemListHeadModel.VisibilityInd;
            recordObject[recordType.Attributes["FILTER_ORG_ID"]] = itemListHeadModel.FilterOrgId;
            recordObject[recordType.Attributes["LAST_REBUILD_DATE"]] = itemListHeadModel.LastRebuildDate;
            recordObject[recordType.Attributes["LIST_STATUS"]] = itemListHeadModel.ListStatus;
            return recordObject;
        }
    }
}
