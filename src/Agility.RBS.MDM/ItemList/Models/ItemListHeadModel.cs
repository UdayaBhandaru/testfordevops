﻿// <copyright file="ItemListHeadModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.ItemList.Models
{
    using System;
    using System.Collections.Generic;
    using Agility.Framework.Core.Profile.Entities;

    public class ItemListHeadModel : ProfileEntity
    {
        public ItemListHeadModel()
        {
            this.ItemListDetails = new List<ItemListDetailModel>();
        }

        public int? ListId { get; set; }

        public string ListDesc { get; set; }

        public string StaticInd { get; set; }

        public string StaticIndDesc { get; set; }

        public string CommentDesc { get; set; }

        public int? FilterOrgId { get; set; }

        public string VisibilityInd { get; set; }

        public string VisibilityIndDesc { get; set; }

        public DateTime? LastRebuildDate { get; set; }

        public string ListStatus { get; set; }

        public string ListStatusDesc { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }

        public string Error { get; set; }

        public List<ItemListDetailModel> ItemListDetails { get; private set; }
    }
}
