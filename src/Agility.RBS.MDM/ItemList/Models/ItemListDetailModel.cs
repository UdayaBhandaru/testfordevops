﻿// <copyright file="ItemListDetailModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.ItemList.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class ItemListDetailModel : ProfileEntity
    {
        public int? ListId { get; set; }

        public string Item { get; set; }

        public string LineStatus { get; set; }

        public string LineStatusDesc { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }

        public string Error { get; set; }

        public int? SupplierId { get; set; }

        public string SuppName { get; set; }

        public string ItemBarcode { get; set; }

        public string ItemDesc { get; set; }

        public int? DeptId { get; set; }

        public string DeptDesc { get; set; }

        public int? CategoryId { get; set; }

        public string CategoryDesc { get; set; }

        public int? BrandId { get; set; }

        public string BrandName { get; set; }

        public int? DivisionId { get; set; }

        public string DivisionDesc { get; set; }

        public string ClassDesc { get; set; }

        public string SubclassDesc { get; set; }
    }
}
