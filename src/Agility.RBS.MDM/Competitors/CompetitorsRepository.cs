﻿// <copyright file="CompetitorsRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Competitors
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Competitors.Models;
    using Devart.Data.Oracle;

    public class CompetitorsRepository : BaseOraclePackage
    {
        public CompetitorsRepository()
        {
            this.PackageName = MdmConstants.GetCompetitorsPackage;
        }

        public async Task<List<CompetitorsModel>> GetCompetitorsAsync(CompetitorsModel competitorModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeCompetitors, MdmConstants.GetProcCompetitors);
            OracleObject recordObject = this.SetParamscompetitor(competitorModel);
            return await this.GetProcedure2<CompetitorsModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ServiceDocumentResult> SetCompetitors(CompetitorsModel competitorModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeCompetitors, MdmConstants.SetProcCompetitors);
            OracleObject recordObject = this.SetParamscompetitor(competitorModel);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        private OracleObject SetParamscompetitor(CompetitorsModel competitorModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("COMPETITOR_MST_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            recordObject["COMPETITOR"] = competitorModel?.Competitor;
            recordObject["COMP_NAME"] = competitorModel?.CompName;
            recordObject["ADDRESS_1"] = competitorModel?.Address1;
            recordObject["COUNTRY_ID"] = competitorModel?.CountryId;
            recordObject["Operation"] = competitorModel?.Operation;
            return recordObject;
        }
    }
}
