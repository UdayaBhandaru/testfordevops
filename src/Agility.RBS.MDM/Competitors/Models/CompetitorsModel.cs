﻿// <copyright file="CompetitorsModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Competitors.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.Framework.Core.Profile.Entities;

  public class CompetitorsModel : ProfileEntity
    {
        public int? Competitor { get; set; }

        public string CompName { get; set; }

        public string Address1 { get; set; }

        public string CountryId { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Error { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }
    }
}
