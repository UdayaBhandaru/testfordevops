﻿// <copyright file="CompetitorsMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Competitors.Mapping
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.Competitors.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class CompetitorsMapping : Profile
    {
        public CompetitorsMapping()
            : base("CompetitorsMapping")
        {
            this.CreateMap<OracleObject, CompetitorsModel>()
                .ForMember(m => m.Competitor, opt => opt.MapFrom(r => r["COMPETITOR"]))
                .ForMember(m => m.CompName, opt => opt.MapFrom(r => r["COMP_NAME"]))
                .ForMember(m => m.Address1, opt => opt.MapFrom(r => r["ADDRESS_1"]))
                .ForMember(m => m.CountryId, opt => opt.MapFrom(r => r["COUNTRY_ID"]))
                .ForMember(m => m.ProgramPhase, opt => opt.MapFrom(r => r["PROGRAM_PHASE"]))
                .ForMember(m => m.ProgramMessage, opt => opt.MapFrom(r => r["PROGRAM_MESSAGE"]))
                .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_ERROR"]))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "Competitors"))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));
        }
    }
}
