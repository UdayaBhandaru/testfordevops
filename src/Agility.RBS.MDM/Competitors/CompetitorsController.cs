﻿// <copyright file="CompetitorsController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Competitors
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Text;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Competitors.Models;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class CompetitorsController : Controller
    {
        private readonly ServiceDocument<CompetitorsModel> serviceDocument;
        private readonly CompetitorsRepository competitorsRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly DomainDataRepository domainDataRepository;

        public CompetitorsController(
            ServiceDocument<CompetitorsModel> serviceDocument,
            CompetitorsRepository competitorsRepository,
            DomainDataRepository domainDataRepository)
        {
            this.competitorsRepository = competitorsRepository;
            this.serviceDocument = serviceDocument;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<CompetitorsModel>> List()
        {
            return await this.GetDomainData();
        }

        public async Task<ServiceDocument<CompetitorsModel>> New()
        {
            return await this.GetDomainData();
        }

        public async Task<ServiceDocument<CompetitorsModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.CompetitorsSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<CompetitorsModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.CompetitorsSave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingCompetitors(string competitor, string compName)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "COMPETITOR_MST";
            nvc["P_COL1"] = competitor;
            nvc["P_COL2"] = compName;
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        private async Task<List<CompetitorsModel>> CompetitorsSearch()
        {
            try
            {
                var lstCompetiors = await this.competitorsRepository.GetCompetitorsAsync(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstCompetiors;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> CompetitorsSave()
        {
            this.serviceDocument.Result = await this.competitorsRepository.SetCompetitors(this.serviceDocument.DataProfile.DataModel);
            return true;
        }

        private async Task<ServiceDocument<CompetitorsModel>> GetDomainData()
        {
            this.serviceDocument.DomainData.Add("country", this.domainDataRepository.CountryDomainGet().Result);
            return this.serviceDocument;
        }
    }
}
