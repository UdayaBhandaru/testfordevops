﻿// <copyright file="BrandRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// BrandRepository
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Brand.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.MDM.Brand.Models;
    using Agility.RBS.MDM.Common;
    using AutoMapper;
    using Devart.Data.Oracle;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.Extensions.Caching.Distributed;

    public class BrandRepository : BaseOraclePackage
    {
        public BrandRepository()
        {
            this.PackageName = MdmConstants.GetBrandPackage;
        }

        public async Task<List<BrandModel>> GetBrand(BrandModel brandModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeBrand, MdmConstants.GetProcBrand);
            OracleObject recordObject = this.SetParamsBrand(brandModel, true);
            return await this.GetProcedure2<BrandModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ServiceDocumentResult> SetBrand(BrandModel brandModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeBrand, MdmConstants.SetProcBrand);
            OracleObject recordObject = this.SetParamsBrand(brandModel, false);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async Task<List<BrandCategoryModel>> GetBrandCategory(BrandCategoryModel brandCatModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeBrandCategory, MdmConstants.GetProcBrandCategory);
            OracleObject recordObject = this.SetParamsBrandCategory(brandCatModel, true);
            return await this.GetProcedure2<BrandCategoryModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public virtual void SetParamsBrandSearch(BrandModel brandModel, OracleObject recordObject)
        {
            recordObject["BRAND_ID"] = brandModel.BrandId;
            recordObject["BRAND_NAME"] = brandModel.BrandName;
            recordObject["brand_value"] = brandModel.BrandValue;
        }

        public virtual void SetParamsBrandSave(BrandModel brandModel, OracleObject recordObject)
        {
            recordObject["Operation"] = brandModel.Operation;
        }

        public async Task<ServiceDocumentResult> SetBrandCategory(BrandCategoryModel brandCatModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeBrandCategory, MdmConstants.SetProcBrandCategory);
            OracleObject recordObject = this.SetParamsBrandCategory(brandCatModel, false);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public virtual void SetParamsBrandCategorySearch(BrandCategoryModel brandCatModel, OracleObject recordObject)
        {
            recordObject["BRAND_ID"] = brandCatModel.BrandId;
            recordObject["BRAND_CAT_ID"] = brandCatModel.BrandName;
            recordObject["BRAND_KEY_CAT"] = brandCatModel.BrandKeyCat;
            recordObject["BRAND_CAT_STATUS"] = brandCatModel.BrandCatStatus;
        }

        public virtual void SetParamsBrandCategorySave(BrandCategoryModel brandCategoryModel, OracleObject recordObject)
        {
            recordObject["Operation"] = brandCategoryModel.Operation;
            recordObject["CREATED_BY"] = this.GetCreatedBy(brandCategoryModel.CreatedBy);
            recordObject["LAST_UPDATED_BY"] = FxContext.Context.Name;
        }

        private OracleObject SetParamsBrand(BrandModel brandModel, bool isSearch)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType(MdmConstants.RecordTypeBrand);
            OracleObject recordObject = this.GetOracleObject(recordType);
            if (!isSearch)
            {
                this.SetParamsBrandSave(brandModel, recordObject);
            }

            this.SetParamsBrandSearch(brandModel, recordObject);
            return recordObject;
        }

        private OracleObject SetParamsBrandCategory(BrandCategoryModel brandCatModel, bool isSearch)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType(MdmConstants.RecordTypeBrandCategory);
            OracleObject recordObject = this.GetOracleObject(recordType);
            if (!isSearch)
            {
                this.SetParamsBrandCategorySave(brandCatModel, recordObject);
            }

            this.SetParamsBrandCategorySearch(brandCatModel, recordObject);
            return recordObject;
        }
    }
}