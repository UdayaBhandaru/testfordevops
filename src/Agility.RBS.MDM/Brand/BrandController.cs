﻿//-------------------------------------------------------------------------------------------------
// <copyright file="BrandController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// BrandController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Brand
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Brand.Models;
    using Agility.RBS.MDM.Brand.Repositories;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class BrandController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocument<BrandModel> serviceDocument;
        private readonly BrandRepository brandRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;

        public BrandController(
            ServiceDocument<BrandModel> serviceDocument,
            BrandRepository brandZoneRepository,
            DomainDataRepository domainDataRepository)
        {
            this.brandRepository = brandZoneRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<BrandModel>> List()
        {
            this.serviceDocument.DomainData.Add("status", this.domainDataRepository.DomainDetailData(MdmConstants.DomainHdrStatus).Result);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<BrandModel>> New()
        {
            this.serviceDocument.DomainData.Add("status", this.domainDataRepository.DomainDetailData(MdmConstants.DomainHdrStatus).Result);
            this.serviceDocument.LocalizationData.AddData("Brand");
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<BrandModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.BrandSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<BrandModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.BrandSave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingBrand(int brandValue, string brandName)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "UDA_VALUES";
            nvc["P_COL1"] = brandValue.ToString();
            nvc["P_COL2"] = brandName;
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        private async Task<List<BrandModel>> BrandSearch()
        {
            try
            {
                var lstUomClass = await this.brandRepository.GetBrand(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstUomClass;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> BrandSave()
        {
            this.serviceDocument.Result = await this.brandRepository.SetBrand(this.serviceDocument.DataProfile.DataModel);
            return true;
        }
    }
}
