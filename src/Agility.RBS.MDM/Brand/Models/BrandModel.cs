﻿// <copyright file="BrandModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// BrandModel
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Brand.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class BrandModel : ProfileEntity
    {
        public int? BrandId { get; set; }

        public string BrandName { get; set; }

        public int? BrandValue { get; set; }

        public int LangId { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Error { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }
    }
}
