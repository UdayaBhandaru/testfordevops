﻿// <copyright file="BrandCategoryModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// BrandCategoryModel
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Brand.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class BrandCategoryModel : ProfileEntity
    {
        public int? BrandId { get; set; }

        public string BrandName { get; set; }

        public int? BrandCatId { get; set; }

        public string BrandKeyCat { get; set; }

        public string BrandCatStatus { get; set; }

        public string BrandCatStatusDesc { get; set; }

        public int LangId { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Error { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }
    }
}
