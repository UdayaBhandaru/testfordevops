﻿//-------------------------------------------------------------------------------------------------
// <copyright file="BrandCategoryController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// BrandCategoryController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.BrandCategory
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Brand.Models;
    using Agility.RBS.MDM.Brand.Repositories;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class BrandCategoryController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocument<BrandCategoryModel> serviceDocument;
        private readonly BrandRepository brandRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;

        public BrandCategoryController(
            ServiceDocument<BrandCategoryModel> serviceDocument,
            BrandRepository brandZoneRepository,
            DomainDataRepository domainDataRepository)
        {
            this.brandRepository = brandZoneRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<BrandCategoryModel>> List()
        {
            this.serviceDocument.DomainData.Add("status", this.domainDataRepository.DomainDetailData(MdmConstants.DomainHdrStatus).Result);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<BrandCategoryModel>> New()
        {
            this.serviceDocument.DomainData.Add("brand", this.domainDataRepository.CommonData("BRAND").Result);
            this.serviceDocument.DomainData.Add("status", this.domainDataRepository.DomainDetailData(MdmConstants.DomainHdrStatus).Result);
            this.serviceDocument.LocalizationData.AddData("BrandCategory");
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<BrandCategoryModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.BrandCategorySearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<BrandCategoryModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.BrandCategorySave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingBrandCategory(int brandId, int brandCatId)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "BRAND_CAT_MST";
            nvc["P_COL1"] = brandId.ToString();
            nvc["P_COL2"] = brandCatId.ToString();
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        private async Task<List<BrandCategoryModel>> BrandCategorySearch()
        {
            try
            {
                var lstUomClass = await this.brandRepository.GetBrandCategory(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstUomClass;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> BrandCategorySave()
        {
            this.serviceDocument.Result = await this.brandRepository.SetBrandCategory(this.serviceDocument.DataProfile.DataModel);
            return true;
        }
    }
}
