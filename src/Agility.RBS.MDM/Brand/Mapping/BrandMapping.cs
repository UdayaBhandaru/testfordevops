﻿// <copyright file="BrandMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Brand.Mapping
{
    using System;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.Brand.Models;
    using Agility.RBS.MDM.CostZone.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class BrandMapping : Profile
    {
        public BrandMapping()
            : base("BrandMapping")
        {
            this.CreateMap<OracleObject, BrandModel>()
                .ForMember(m => m.BrandId, opt => opt.MapFrom(r => r["BRAND_ID"]))
                .ForMember(m => m.BrandName, opt => opt.MapFrom(r => r["BRAND_NAME"]))
                .ForMember(m => m.BrandValue, opt => opt.MapFrom(r => r["brand_value"]))
                .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "BRAND_MST"))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, BrandCategoryModel>()
                .ForMember(m => m.BrandId, opt => opt.MapFrom(r => r["BRAND_ID"]))
                .ForMember(m => m.BrandName, opt => opt.MapFrom(r => r["BRAND_NAME"]))
                .ForMember(m => m.BrandCatId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["BRAND_CAT_ID"])))
                .ForMember(m => m.BrandKeyCat, opt => opt.MapFrom(r => r["BRAND_KEY_CAT"]))
                .ForMember(m => m.BrandCatStatus, opt => opt.MapFrom(r => r["BRAND_CAT_STATUS"]))
                .ForMember(m => m.BrandCatStatusDesc, opt => opt.MapFrom(r => r["BRAND_CAT_STATUS_DESC"]))
                .ForMember(m => m.CreatedBy, opt => opt.MapFrom(r => r["CREATED_BY"]))
                .ForMember(m => m.ModifiedBy, opt => opt.MapFrom(r => r["LAST_UPDATED_BY"]))
                .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
                .ForMember(m => m.CreatedDate, opt => opt.MapFrom(r => r["CREATED_DATE"]))
                .ForMember(m => m.ModifiedDate, opt => opt.MapFrom(r => r["LAST_UPDATED_DATE"]))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "BRAND_CAT_MST"))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));
        }
    }
}
