﻿// <copyright file="EmployeeModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// BrandModel
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Employee.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class EmployeeModel : ProfileEntity
    {
        public string EmpType { get; set; }

        public string EmpTypeDesc { get; set; }

        public string EmpId { get; set; }

        public string PartnerDesc { get; set; }

        public string CashierInd { get; set; }

        public string SalespersonInd { get; set; }

        public string Phone { get; set; }

        public string Name { get; set; }

        public string UserId { get; set; }

        public string Email { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Error { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }
    }
}
