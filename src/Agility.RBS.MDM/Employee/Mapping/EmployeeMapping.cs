﻿// <copyright file="EmployeeMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Employee.Mapping
{
    using System;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.Brand.Models;
    using Agility.RBS.MDM.CostZone.Models;
    using Agility.RBS.MDM.Employee.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class EmployeeMapping : Profile
    {
        public EmployeeMapping()
            : base("EmployeeMapping")
        {
            this.CreateMap<OracleObject, EmployeeModel>()
                .ForMember(m => m.EmpId, opt => opt.MapFrom(r => r["emp_id"]))
                .ForMember(m => m.EmpType, opt => opt.MapFrom(r => r["emp_type"]))
                .ForMember(m => m.EmpTypeDesc, opt => opt.MapFrom(r => r["emp_type_desc"]))
                .ForMember(m => m.CashierInd, opt => opt.MapFrom(r => r["cashier_ind"]))
                .ForMember(m => m.SalespersonInd, opt => opt.MapFrom(r => r["salesperson_ind"]))
                .ForMember(m => m.Name, opt => opt.MapFrom(r => r["name"]))
                .ForMember(m => m.Phone, opt => opt.MapFrom(r => r["phone"]))
                .ForMember(m => m.UserId, opt => opt.MapFrom(r => r["user_id"]))
                .ForMember(m => m.Email, opt => opt.MapFrom(r => r["email"]))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "Employee"))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));
        }
    }
}
