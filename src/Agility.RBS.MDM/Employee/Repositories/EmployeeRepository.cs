﻿// <copyright file="EmployeeRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// BrandRepository
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Employee.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Employee.Models;
    using AutoMapper;
    using Devart.Data.Oracle;
    using Microsoft.AspNetCore.Authorization;

    public class EmployeeRepository : BaseOraclePackage
    {
        public EmployeeRepository()
        {
            this.PackageName = "UTL_SALES_AUDIT";
        }

        public async Task<List<EmployeeModel>> GetPartner(EmployeeModel partnerModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams("sa_emp_tab", "getemployee");
            OracleObject recordObject = this.SetParamsPartner(partnerModel, true);
            return await this.GetProcedure2<EmployeeModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ServiceDocumentResult> SetPartner(EmployeeModel partnerModel)
        {
            PackageParams packageParameter = this.GetPackageParams("sa_emp_tab", "setemployee");
            OracleObject recordObject = this.SetParamsPartner(partnerModel, false);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public virtual void SetParamsPartnerSearch(EmployeeModel partnerModel, OracleObject recordObject)
        {
            recordObject["emp_id"] = partnerModel.EmpId;
            recordObject["emp_type"] = partnerModel.EmpType;
           //// recordObject["emp_type_des"] = partnerModel.EmpTypeDesc;
            recordObject["cashier_ind"] = partnerModel.CashierInd;
            recordObject["NAME"] = partnerModel.Name;
            recordObject["user_id"] = partnerModel.UserId;
            recordObject["email"] = partnerModel.Email;
            recordObject["salesperson_ind"] = partnerModel.SalespersonInd;
            recordObject["phone"] = partnerModel.Phone;
        }

        private OracleObject SetParamsPartner(EmployeeModel partnerModel, bool isSearch)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("sa_emp_rec");
            OracleObject recordObject = this.GetOracleObject(recordType);
            if (!isSearch)
            {
                recordObject["Operation"] = partnerModel.Operation;
            }

            this.SetParamsPartnerSearch(partnerModel, recordObject);
            return recordObject;
        }
    }
}