﻿//-------------------------------------------------------------------------------------------------
// <copyright file="EmployeeController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// BrandController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Employee
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Employee.Models;
    using Agility.RBS.MDM.Employee.Repositories;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    public class EmployeeController : Controller
    {
        private readonly ServiceDocument<EmployeeModel> serviceDocument;
        private readonly EmployeeRepository partnerRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly DomainDataRepository domainDataRepository;

        public EmployeeController(
            ServiceDocument<EmployeeModel> serviceDocument,
            EmployeeRepository partnerRepository,
             DomainDataRepository domainDataRepository)
        {
            this.partnerRepository = partnerRepository;
            this.serviceDocument = serviceDocument;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<EmployeeModel>> List()
        {
            this.serviceDocument.DomainData.Add("indicator", this.domainDataRepository.DomainDetailData("YSNO").Result);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<EmployeeModel>> New()
        {
            this.serviceDocument.DomainData.Add("indicator", this.domainDataRepository.DomainDetailData("YSNO").Result);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<EmployeeModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.PartnerSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<EmployeeModel>> Open(string id)
        {
            this.serviceDocument.DataProfile.DataModel = new EmployeeModel { EmpId = id };
            this.serviceDocument.DomainData.Add("indicator", this.domainDataRepository.DomainDetailData("YSNO").Result);
            return await this.serviceDocument.OpenAsync(this.EmpOpen);
        }

        public async Task<ServiceDocument<EmployeeModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.PartnerSave);
            return this.serviceDocument;
        }

        private async Task<List<EmployeeModel>> PartnerSearch()
        {
            try
            {
                var lstUomClass = await this.partnerRepository.GetPartner(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstUomClass;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<EmployeeModel> EmpOpen()
        {
            try
            {
                var lstUomClass = await this.partnerRepository.GetPartner(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstUomClass?[0];
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> PartnerSave()
        {
            this.serviceDocument.Result = await this.partnerRepository.SetPartner(this.serviceDocument.DataProfile.DataModel);
            return true;
        }
    }
}
