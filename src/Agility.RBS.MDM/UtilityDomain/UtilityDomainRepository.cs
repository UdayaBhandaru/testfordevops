﻿//-------------------------------------------------------------------------------------------------
// <copyright file="UtilityDomainRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UtilityDomainRepository
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.UtilityDomain
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.UtilityDomain.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class UtilityDomainRepository : BaseOraclePackage
    {
        public UtilityDomainRepository()
        {
            this.PackageName = MdmConstants.GetDomainPackage;
        }

        public async Task<List<DomainHeaderModel>> GetUtilityDomain(DomainHeaderModel domainHeaderModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainHeader, MdmConstants.GetProcDomainHeader);
            OracleObject recordObject = this.SetParamsDomain(domainHeaderModel, true);
            OracleTable pDomainMstTab = await this.GetProcedure(recordObject, packageParameter, serviceDocumentResult);

            var lstHeader = Mapper.Map<List<DomainHeaderModel>>(pDomainMstTab);
            if (lstHeader != null)
            {
                for (int i = 0; i < lstHeader.Count; i++)
                {
                    OracleObject obj = (OracleObject)pDomainMstTab[i];
                    OracleTable domainDetails = (Devart.Data.Oracle.OracleTable)obj["UTL_DOMAIN_DTL"];
                    if (domainDetails.Count > 0)
                    {
                        lstHeader[i].DomainDetails.AddRange(Mapper.Map<List<DomainDetailModel>>(domainDetails));
                    }
                }
            }

            return lstHeader;
        }

        public async Task<ServiceDocumentResult> SetUtilityDomain(DomainHeaderModel domainHeaderModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainHeader, MdmConstants.SetProcDomainHeader);
            OracleObject recordObject = this.SetParamsDomain(domainHeaderModel, false);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async Task<List<DomainDetailModel>> GetUtilityDomainDetail(string domainId, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainDetail, MdmConstants.GetProcDomainDetail);
            BaseOracleParameter baseOracleParameter;
            baseOracleParameter = new BaseOracleParameter { Name = "RESULT", DataType = OracleDbType.Boolean, Direction = ParameterDirection.ReturnValue };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));
            baseOracleParameter = new BaseOracleParameter { Name = MdmConstants.HeaderId, DataType = OracleDbType.VarChar, Direction = ParameterDirection.Input, Value = domainId };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));
            baseOracleParameter = new BaseOracleParameter
            {
                Name = packageParameter.ObjectTypeTable,
                DataType = OracleDbType.Table,
                Direction = ParameterDirection.Output,
                ObjectTypeName = packageParameter.ObjectTypeName
            };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));
            return await this.GetProcedure2<DomainDetailModel>(null, packageParameter, serviceDocumentResult, parameters);
        }

        public virtual void SetParamsDomainSearch(DomainHeaderModel domainHeaderModel, OracleObject recordObject)
        {
            recordObject["code_type"] = domainHeaderModel.DomainId;
            recordObject["code_type_desc"] = domainHeaderModel.DomainDesc;
            recordObject["Operation"] = domainHeaderModel.Operation;
        }

        public virtual void SetParamsDomainSave(DomainHeaderModel domainHeaderModel, OracleObject recordObject)
        {
            // Detail Object
            OracleType dtlTableType = this.GetObjectType("UTL_DMN_DTL_TAB");
            OracleType dtlRecordType = this.GetObjectType("UTL_DMN_DTL_REC");
            OracleTable pUtlDmnDtlTab = new OracleTable(dtlTableType);
            foreach (DomainDetailModel detail in domainHeaderModel.DomainDetails)
            {
                var dtlRecordObject = this.GetOracleObject(dtlRecordType);
                dtlRecordObject[dtlRecordType.Attributes["code_type"]] = domainHeaderModel.DomainId;
                dtlRecordObject[dtlRecordType.Attributes["CODE"]] = detail.Code;
                dtlRecordObject[dtlRecordType.Attributes["CODE_DESC"]] = detail.CodeDesc;
                dtlRecordObject[dtlRecordType.Attributes["REQUIRED_IND"]] = detail.RequiredInd.Equals("1") ? "Y" : "N";
                dtlRecordObject[dtlRecordType.Attributes["code_seq"]] = detail.CodeSeq;
                dtlRecordObject[dtlRecordType.Attributes["Operation"]] = domainHeaderModel.Operation;
                pUtlDmnDtlTab.Add(dtlRecordObject);
            }

            recordObject["UTL_DOMAIN_DTL"] = pUtlDmnDtlTab;
        }

        private OracleObject SetParamsDomain(DomainHeaderModel domainHeaderModel, bool isSearch)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("UTL_DMN_HDR_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            if (!isSearch)
            {
                this.SetParamsDomainSave(domainHeaderModel, recordObject);
            }

            this.SetParamsDomainSearch(domainHeaderModel, recordObject);

            return recordObject;
        }
    }
}