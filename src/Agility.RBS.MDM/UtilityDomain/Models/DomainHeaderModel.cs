﻿//-------------------------------------------------------------------------------------------------
// <copyright file="DomainHeaderModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// DomainHeaderModel
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.UtilityDomain.Models
{
    using System.Collections.Generic;
    using Agility.Framework.Core.Profile.Entities;

    public class DomainHeaderModel : ProfileEntity
    {
        public DomainHeaderModel()
        {
            this.DomainDetails = new List<DomainDetailModel>();
        }

        public string DomainId { get; set; }

        public string DomainDesc { get; set; }

        public string DomainStatus { get; set; }

        public string DomainStatusDesc
        {
            get { return this.DomainStatus == "A" ? "ACTIVE" : "INACTIVE"; }
        }

        public string TableName { get; set; }

        public List<DomainDetailModel> DomainDetails { get; private set; }

        public string Operation { get; set; }

        public string Error { get; set; }

        public string CodeType { get; set; }
    }
}
