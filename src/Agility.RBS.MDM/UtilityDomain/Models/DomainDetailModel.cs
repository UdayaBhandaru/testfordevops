﻿//-------------------------------------------------------------------------------------------------
// <copyright file="DomainDetailModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// DomainDetailModel
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.UtilityDomain.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class DomainDetailModel : ProfileEntity
    {
        public string CodeType { get; set; }

        public string Code { get; set; }

        public string CodeDesc { get; set; }

        public string RequiredInd { get; set; }

        public int CodeSeq { get; set; }
    }
}
