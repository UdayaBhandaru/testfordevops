﻿//-------------------------------------------------------------------------------------------------
// <copyright file="UtilityDomainController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UtilityDomainController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.UtilityDomain
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.UtilityDomain.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    public class UtilityDomainController : Controller
    {
        private readonly ServiceDocument<DomainHeaderModel> serviceDocument;
        private readonly UtilityDomainRepository domainRepository;
        private readonly ILogger<UtilityDomainController> logger;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly DomainDataRepository domainDataRepository;

        public UtilityDomainController(
            ServiceDocument<DomainHeaderModel> serviceDocument,
            UtilityDomainRepository domainRepository,
            ILogger<UtilityDomainController> logger,
            DomainDataRepository domainDataRepository)
        {
            this.serviceDocument = serviceDocument;
            this.domainRepository = domainRepository;
            this.logger = logger;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<DomainHeaderModel>> List()
        {
            this.serviceDocument.DomainData.Add("status", this.domainDataRepository.DomainDetailData(MdmConstants.DomainHdrStatus).Result);
            this.serviceDocument.LocalizationData.AddData("Domain");
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<DomainHeaderModel>> Search()
        {
            this.logger.LogInformation("Search Started");
            await this.serviceDocument.ToListAsync(this.UtilityDomainSearch);
            this.logger.LogInformation("Search Completed");
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<DomainHeaderModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.UtilityDomainSave);
            this.serviceDocument.DomainData.Add("status", this.domainDataRepository.DomainDetailData(MdmConstants.DomainHdrStatus).Result);
            this.serviceDocument.LocalizationData.AddData("Domain");
            return this.serviceDocument;
        }

        public async Task<List<DomainDetailModel>> DomainDetails(string headerId)
        {
            try
            {
                return await this.domainRepository.GetUtilityDomainDetail(headerId, this.serviceDocumentResult);
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingDomain(string name)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "UTL_DOMAIN_HDR";
            nvc["P_COL1"] = name;
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        private async Task<List<DomainHeaderModel>> UtilityDomainSearch()
        {
            try
            {
                var domains = await this.domainRepository.GetUtilityDomain(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return domains;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> UtilityDomainSave()
        {
            this.serviceDocument.Result = await this.domainRepository.SetUtilityDomain(this.serviceDocument.DataProfile.DataModel);
            return true;
        }
    }
}
