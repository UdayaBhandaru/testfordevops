﻿// <copyright file="UtilityDomainMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.UtilityDomain.Mapping
{
    using Agility.RBS.MDM.UtilityDomain.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class UtilityDomainMapping : Profile
    {
        public UtilityDomainMapping()
            : base("UtilityDomainMapping")
        {
            this.CreateMap<OracleObject, DomainHeaderModel>()
                .ForMember(m => m.DomainId, opt => opt.MapFrom(r => r["code_type"]))
                .ForMember(m => m.DomainDesc, opt => opt.MapFrom(r => r["code_type_desc"]))
                .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "CODE_HEAD"))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, DomainDetailModel>()
                .ForMember(m => m.CodeType, opt => opt.MapFrom(r => r["CODE_TYPE"]))
                .ForMember(m => m.Code, opt => opt.MapFrom(r => r["CODE"]))
                .ForMember(m => m.CodeDesc, opt => opt.MapFrom(r => r["CODE_DESC"]))
                .ForMember(m => m.RequiredInd, opt => opt.MapFrom(r => r["REQUIRED_IND"]))
                .ForMember(m => m.CodeSeq, opt => opt.MapFrom(r => r["CODE_SEQ"]));
        }
    }
}
