﻿// <copyright file="StoreModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// CostChangeReason
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Location.Models
{
    using System;
    using Agility.Framework.Core.Profile.Entities;

    public class StoreModel : ProfileEntity
    {
        public StoreModel()
        {
            this.Operation = "I";
        }

        public int? Store { get; set; }

        public string StoreName { get; set; }

        public string StoreName10 { get; set; }

        public string StoreName3 { get; set; }

        public string StoreNameSecondary { get; set; }

        public string StoreClass { get; set; }

        public string StoreMgrName { get; set; }

        public DateTime? StoreOpenDate { get; set; }

        public DateTime? StoreCloseDate { get; set; }

        public DateTime? AcquiredDate { get; set; }

        public DateTime? RemodelDate { get; set; }

        public string FaxNumber { get; set; }

        public string PhoneNumber { get; set; }

        public string Email { get; set; }

        public int? TotalSquareFt { get; set; }

        public int? SellingSquareFt { get; set; }

        public int? LinearDistance { get; set; }

        public int? VatRegion { get; set; }

        public string RegionName { get; set; }

        public string VatIncludeInd { get; set; }

        public string StockHoldingInd { get; set; }

        public int? ChannelId { get; set; }

        public int? StoreFormat { get; set; }

        public string MallName { get; set; }

        public int? District { get; set; }

        public int? TransferZone { get; set; }

        public string TransferZoneDesc { get; set; }

        public int? DefaultWh { get; set; }

        public int? StopOrderDays { get; set; }

        public int? StartOrderDays { get; set; }

        public string CurrencyCode { get; set; }

        public int? Lang { get; set; }

        public string LangDesc { get; set; }

        public string TranNoGenerated { get; set; }

        public string IntegratedPosInd { get; set; }

        public string OrigCurrencyCode { get; set; }

        public string DunsNumber { get; set; }

        public string DunsLoc { get; set; }

        public int? SisterStore { get; set; }

        public int? TsfEntityId { get; set; }

        public int? OrgUnitId { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }

        public string Error { get; set; }

        public string ProgmPhase { get; set; }

        public string PerrorMessage { get; set; }
    }
}
