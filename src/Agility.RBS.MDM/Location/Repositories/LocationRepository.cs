﻿// <copyright file="LocationRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// CostZoneRepository
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Location.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Location.Models;
    using AutoMapper;
    using Devart.Data.Oracle;
    using Microsoft.AspNetCore.Authorization;

    public class LocationRepository : BaseOraclePackage
    {
        public LocationRepository()
        {
            this.PackageName = MdmConstants.GetStorePackage;
        }

        public async Task<List<StoreModel>> GetLocation(StoreModel storeModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeStore, MdmConstants.GetProcStore);
            OracleObject recordObject = this.SetParamsLocation(storeModel, true);
            return await this.GetProcedure2<StoreModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ServiceDocumentResult> SetLocation(StoreModel storeModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeStore, MdmConstants.SetProcStore);
            OracleObject recordObject = this.SetParamsLocation(storeModel, false);
            ServiceDocumentResult sRes = await this.SetProcedure(recordObject, packageParameter);
            return sRes;
        }

        public virtual void SetParamsLocationSave(StoreModel storeModel, OracleObject recordObject)
        {
            recordObject["Operation"] = storeModel.Operation;
            recordObject["STORE"] = storeModel.Store;
            recordObject["STORE_NAME"] = storeModel.StoreName;
            recordObject["STORE_NAME10"] = storeModel.StoreName10;
            recordObject["STORE_NAME3"] = storeModel.StoreName3;
            recordObject["STORE_NAME_SECONDARY"] = storeModel.StoreNameSecondary;
            recordObject["STORE_CLASS"] = storeModel.StoreClass;
            recordObject["STORE_MGR_NAME"] = storeModel.StoreMgrName;
            recordObject["STORE_OPEN_DATE"] = storeModel.StoreOpenDate;
            recordObject["STORE_CLOSE_DATE"] = storeModel.StoreCloseDate;
            recordObject["ACQUIRED_DATE"] = storeModel.AcquiredDate;
            recordObject["REMODEL_DATE"] = storeModel.RemodelDate;
            recordObject["FAX_NUMBER"] = storeModel.FaxNumber;
            recordObject["PHONE_NUMBER"] = storeModel.PhoneNumber;
            recordObject["EMAIL"] = storeModel.Email;
            recordObject["TOTAL_SQUARE_FT"] = storeModel.TotalSquareFt;
            recordObject["SELLING_SQUARE_FT"] = storeModel.SellingSquareFt;
            recordObject["LINEAR_DISTANCE"] = storeModel.LinearDistance;
            recordObject["VAT_REGION"] = storeModel.VatRegion;
            recordObject["VAT_INCLUDE_IND"] = storeModel.VatIncludeInd;
            recordObject["STOCKHOLDING_IND"] = storeModel.StockHoldingInd;
            recordObject["CHANNEL_ID"] = storeModel.ChannelId;
            recordObject["STORE_FORMAT"] = storeModel.StoreFormat;
            recordObject["MALL_NAME"] = storeModel.MallName;
            recordObject["District"] = storeModel.District;
            recordObject["TRANSFER_ZONE"] = storeModel.TransferZone;
            recordObject["DEFAULT_WH"] = storeModel.DefaultWh;
            recordObject["STOP_ORDER_DAYS"] = storeModel.StopOrderDays;
            recordObject["START_ORDER_DAYS"] = storeModel.StartOrderDays;
            recordObject["CURRENCY_CODE"] = storeModel.CurrencyCode;
            recordObject["LANG"] = storeModel.Lang;
            recordObject["TRAN_NO_GENERATED"] = "R";
            recordObject["INTEGRATED_POS_IND"] = storeModel.IntegratedPosInd;
            recordObject["ORIG_CURRENCY_CODE"] = storeModel.OrigCurrencyCode;
            recordObject["DUNS_NUMBER"] = storeModel.DunsNumber;
            recordObject["DUNS_LOC"] = storeModel.DunsLoc;
            recordObject["SISTER_STORE"] = storeModel.SisterStore;
            recordObject["TSF_ENTITY_ID"] = storeModel.TsfEntityId;
            recordObject["ORG_UNIT_ID"] = storeModel.OrgUnitId;
        }

        public virtual void SetParamsLocationSearch(StoreModel storeModel, OracleObject recordObject)
        {
            recordObject["STORE"] = storeModel.Store;
            recordObject["STORE_NAME"] = storeModel.StoreName;
            recordObject["STORE_MGR_NAME"] = storeModel.StoreMgrName;
            recordObject["MALL_NAME"] = storeModel.MallName;
        }

        private OracleObject SetParamsLocation(StoreModel storeModel, bool isSearch)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("STORE_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            if (!isSearch)
            {
                this.SetParamsLocationSave(storeModel, recordObject);
            }

            this.SetParamsLocationSearch(storeModel, recordObject);

            return recordObject;
        }
    }
}