﻿//-------------------------------------------------------------------------------------------------
// <copyright file="LocationController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// StoreFormatController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Location
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Location.Models;
    using Agility.RBS.MDM.Location.Repositories;
    using Agility.RBS.MDM.OrganizationHierarchy;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.StoreFormat.Models;
    using Agility.RBS.MDM.StoreFormat.Repositories;
    using Agility.RBS.MDM.Transfer;
    using Agility.RBS.MDM.Transfer.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    [Route("api/[controller]/[action]")]

    public class LocationController : Controller
    {
        private readonly ServiceDocument<StoreModel> serviceDocument;
        private readonly LocationRepository locationRepository;
        private readonly TransferRepository transferRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly DomainDataRepository domainDataRepository;
        private readonly OrganizationHierarchyRepository organizationHierarchyRepository;
        private readonly StoreFormatRepository storeFormatRepository;

        public LocationController(
            ServiceDocument<StoreModel> serviceDocument,
            LocationRepository locationRepository,
            DomainDataRepository domainDataRepository,
            TransferRepository transferRepository,
            OrganizationHierarchyRepository organizationHierarchyRepository,
            StoreFormatRepository storeFormatRepository)
        {
            this.locationRepository = locationRepository;
            this.serviceDocument = serviceDocument;
            this.domainDataRepository = domainDataRepository;
            this.transferRepository = transferRepository;
            this.organizationHierarchyRepository = organizationHierarchyRepository;
            this.storeFormatRepository = storeFormatRepository;

            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        [HttpPost]
        public async Task<ServiceDocument<StoreModel>> List()
        {
            return await this.LocationDomainData();
        }

        public async Task<ServiceDocument<StoreModel>> New()
        {
            return await this.LocationDomainData();
        }

        [HttpPost]
        public async Task<ServiceDocument<StoreModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.LocationSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<StoreModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.LocationSave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingLocation(int store, string storeName)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "STORE";
            nvc["P_COL1"] = store.ToString();
            nvc["P_COL2"] = storeName;
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        private async Task<List<StoreModel>> LocationSearch()
        {
            try
            {
                var locations = await this.locationRepository.GetLocation(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return locations;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> LocationSave()
        {
            this.serviceDocument.Result = await this.locationRepository.SetLocation(this.serviceDocument.DataProfile.DataModel);
            return true;
        }

        private async Task<ServiceDocument<StoreModel>> LocationDomainData()
        {
            TransferZoneModel transferZoneModel = new TransferZoneModel();
            TransferEntityModel transferEntityModel = new TransferEntityModel();
            StoreFormatModel storeFormatModel = new StoreFormatModel();
            OrganizationHierarchy.Models.DistrictModel districtModel = new OrganizationHierarchy.Models.DistrictModel();
            var transferZones = await this.transferRepository.GetTransferZoneAsync(transferZoneModel, this.serviceDocumentResult);
            var storeForamt = await this.storeFormatRepository.GetStoreFormat(storeFormatModel, this.serviceDocumentResult);
            this.serviceDocument.DomainData.Add("TransferZones", transferZones);
            this.serviceDocument.DomainData.Add("StoreForamt", storeForamt);
            var transferEntities = await this.transferRepository.GetTransferEntityAsync(transferEntityModel, this.serviceDocumentResult);
            this.serviceDocument.DomainData.Add("TransferEntities", transferEntities);
            var districts = await this.organizationHierarchyRepository.GetDistrict(districtModel, this.serviceDocumentResult);
            this.serviceDocument.DomainData.Add("Districts", districts);
            this.serviceDocument.DomainData.Add("indicatorStatus", this.domainDataRepository.DomainDetailData(MdmConstants.VatRegionTypeDomainData).Result);
            this.serviceDocument.DomainData.Add("vatRegionCode", this.domainDataRepository.VatRegionDomainGet().Result);
            this.serviceDocument.DomainData.Add("currency", this.domainDataRepository.CurrencyDomainGet().Result);
            this.serviceDocument.DomainData.Add("language", this.domainDataRepository.LanguageDomainGet().Result);
            this.serviceDocument.DomainData.Add("locType", this.domainDataRepository.LocationTypeDomainGet().Result);
            this.serviceDocument.DomainData.Add("ClassStore", this.domainDataRepository.DomainDetailData(MdmConstants.ClassStoreDomainData).Result);
            this.serviceDocument.LocalizationData.AddData("Location");

            return this.serviceDocument;
        }
    }
}
