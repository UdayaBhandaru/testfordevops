﻿// <copyright file="LocationMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.StoreFormat.Mapping
{
    using System;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.Location.Models;
    using Agility.RBS.MDM.StoreFormat.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class LocationMapping : Profile
    {
        public LocationMapping()
            : base("LocationMapping")
        {
            this.CreateMap<OracleObject, StoreModel>()
                  .ForMember(m => m.Store, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["STORE"])))
                  .ForMember(m => m.StoreName, opt => opt.MapFrom(r => r["STORE_NAME"]))
                  .ForMember(m => m.StoreName10, opt => opt.MapFrom(r => r["STORE_NAME10"]))
                  .ForMember(m => m.StoreName3, opt => opt.MapFrom(r => r["STORE_NAME3"]))
                  .ForMember(m => m.StoreNameSecondary, opt => opt.MapFrom(r => r["STORE_NAME_SECONDARY"]))
                  .ForMember(m => m.StoreClass, opt => opt.MapFrom(r => r["STORE_CLASS"]))
                  .ForMember(m => m.StoreMgrName, opt => opt.MapFrom(r => r["STORE_MGR_NAME"]))
                  .ForMember(m => m.StoreOpenDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["STORE_OPEN_DATE"])))
                  .ForMember(m => m.StoreCloseDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["STORE_CLOSE_DATE"])))
                  .ForMember(m => m.AcquiredDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["ACQUIRED_DATE"])))
                  .ForMember(m => m.FaxNumber, opt => opt.MapFrom(r => r["FAX_NUMBER"]))
                  .ForMember(m => m.PhoneNumber, opt => opt.MapFrom(r => r["PHONE_NUMBER"]))
                  .ForMember(m => m.Email, opt => opt.MapFrom(r => r["EMAIL"]))
                  .ForMember(m => m.TotalSquareFt, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["TOTAL_SQUARE_FT"])))
                  .ForMember(m => m.SellingSquareFt, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["SELLING_SQUARE_FT"])))
                  .ForMember(m => m.LinearDistance, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["LINEAR_DISTANCE"])))
                  .ForMember(m => m.VatRegion, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["VAT_REGION"])))
                  .ForMember(m => m.VatIncludeInd, opt => opt.MapFrom(r => r["VAT_INCLUDE_IND"]))
                  .ForMember(m => m.StockHoldingInd, opt => opt.MapFrom(r => r["STOCKHOLDING_IND"]))
                  .ForMember(m => m.ChannelId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["CHANNEL_ID"])))
                  .ForMember(m => m.StoreFormat, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["STORE_FORMAT"])))
                  .ForMember(m => m.MallName, opt => opt.MapFrom(r => r["MALL_NAME"]))
                  .ForMember(m => m.District, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DISTRICT"])))
                  .ForMember(m => m.TransferZone, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["TRANSFER_ZONE"])))
                  .ForMember(m => m.DefaultWh, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DEFAULT_WH"])))
                  .ForMember(m => m.StopOrderDays, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["STOP_ORDER_DAYS"])))
                  .ForMember(m => m.StartOrderDays, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["START_ORDER_DAYS"])))
                  .ForMember(m => m.CurrencyCode, opt => opt.MapFrom(r => r["CURRENCY_CODE"]))
                  .ForMember(m => m.Lang, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["LANG"])))
                  .ForMember(m => m.TranNoGenerated, opt => opt.MapFrom(r => r["TRAN_NO_GENERATED"]))
                  .ForMember(m => m.IntegratedPosInd, opt => opt.MapFrom(r => r["INTEGRATED_POS_IND"]))
                  .ForMember(m => m.OrigCurrencyCode, opt => opt.MapFrom(r => r["ORIG_CURRENCY_CODE"]))
                  .ForMember(m => m.DunsNumber, opt => opt.MapFrom(r => r["DUNS_NUMBER"]))
                  .ForMember(m => m.DunsLoc, opt => opt.MapFrom(r => r["DUNS_LOC"]))
                  .ForMember(m => m.SisterStore, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["SISTER_STORE"])))
                  .ForMember(m => m.TsfEntityId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["TSF_ENTITY_ID"])))
                  .ForMember(m => m.OrgUnitId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["ORG_UNIT_ID"])))
                  .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
                  .ForMember(m => m.ProgmPhase, opt => opt.MapFrom(r => r["PROGRAM_PHASE"]))
                  .ForMember(m => m.PerrorMessage, opt => opt.MapFrom(r => r["PROGRAM_MESSAGE"]))
                  .ForMember(m => m.TableName, opt => opt.MapFrom(r => "STORE"))
                  .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));
        }
    }
}
