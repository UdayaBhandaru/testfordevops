﻿// <copyright file="DsdMinMaxModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// CostChangeReason
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.DsdMinMax.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class DsdMinMaxModel : ProfileEntity
    {
        public int? Supplier { get; set; }

        public string SupName { get; set; }

        public string Item { get; set; }

        public string ItemDesc { get; set; }

        public int? Location { get; set; }

        public string LocName { get; set; }

        public decimal? MinQty { get; set; }

        public decimal? MaxQty { get; set; }

        public int? ReplDay { get; set; }

        public string Day1 { get; set; }

        public string Day2 { get; set; }

        public string Day3 { get; set; }

        public string Day4 { get; set; }

        public string Day5 { get; set; }

        public string Day6 { get; set; }

        public string Day7 { get; set; }

        public int? PoGroup { get; set; }

        public string Error { get; set; }

        public string Operation { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string TableName { get; set; }
    }
}
