﻿// <copyright file="ItemDescpDetailsDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// Item List Domain
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.DsdMinMax.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class ItemDescpDetailsDomainModel
    {
        public string Item { get; set; }

        public string ItemDesc { get; set; }

        public string Program_Phase { get; set; }

        public string Program_Message { get; set; }

        public string Error { get; set; }
    }
}
