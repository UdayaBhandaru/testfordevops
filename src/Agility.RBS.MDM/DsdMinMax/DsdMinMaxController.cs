﻿//-------------------------------------------------------------------------------------------------
// <copyright file="DsdMinMaxController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// DsdMinMaxController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.DsdMinMax
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.DsdMinMax.Models;
    using Agility.RBS.MDM.DsdMinMax.Repositories;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Transfer;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    [Route("api/[controller]/[action]")]

    public class DsdMinMaxController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocument<DsdMinMaxModel> serviceDocument;
        private readonly DsdMinMaxRepository dsdMinMaxRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;

        public DsdMinMaxController(
            ServiceDocument<DsdMinMaxModel> serviceDocument,
           DsdMinMaxRepository dsdMinMaxRepository,
            DomainDataRepository domainDataRepository)
        {
            this.dsdMinMaxRepository = dsdMinMaxRepository;
            this.domainDataRepository = domainDataRepository;

            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<DsdMinMaxModel>> List()
        {
           this.DomaintData();
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<DsdMinMaxModel>> New()
        {
           this.DomaintData();
            this.serviceDocument.LocalizationData.AddData("DsdMinMax");
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<DsdMinMaxModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.DsdMinMaxSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<DsdMinMaxModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.DsdMinMaxSave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingDsdMinMax(int supplier, string item, int location, string poGroup)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "NB_DSD_AR";
            nvc["P_COL1"] = supplier.ToString();
            nvc["P_COL1"] = item;
            nvc["P_COL1"] = location.ToString();
            nvc["P_COL1"] = poGroup;

            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        [HttpGet]
        public ItemDescpDetailsDomainModel GetItemDescription(string item)
        {
            ItemDescpDetailsDomainModel itemDescpDetailsDomainModel;
            itemDescpDetailsDomainModel = new ItemDescpDetailsDomainModel();
            itemDescpDetailsDomainModel.ItemDesc = this.domainDataRepository.GetItemDescription(item);
            return itemDescpDetailsDomainModel;
        }

        private async Task<List<DsdMinMaxModel>> DsdMinMaxSearch()
        {
            try
            {
                var lstUomClass = await this.dsdMinMaxRepository.GetDsdMinMax(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstUomClass;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> DsdMinMaxSave()
        {
            this.serviceDocument.Result = await this.dsdMinMaxRepository.SetDsdMinMax(this.serviceDocument.DataProfile.DataModel);
            return true;
        }

        private void DomaintData()
        {
            this.serviceDocument.DomainData.Add("supplier", this.domainDataRepository.SupplierDomainGet().Result);
            this.serviceDocument.DomainData.Add("locType", this.domainDataRepository.LocationTypeDomainGet().Result);
            this.serviceDocument.DomainData.Add("status", this.domainDataRepository.DomainDetailData(MdmConstants.DomainHdrStatus).Result);
            this.serviceDocument.DomainData.Add("indicatorStatus", this.domainDataRepository.DomainDetailData(MdmConstants.VatRegionTypeDomainData).Result);
        }
    }
}
