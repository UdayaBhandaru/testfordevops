﻿// <copyright file="DsdMinMaxRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// CostZoneRepository
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.DsdMinMax.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.DsdMinMax.Models;
    using AutoMapper;
    using Devart.Data.Oracle;
    using Microsoft.AspNetCore.Authorization;

    public class DsdMinMaxRepository : BaseOraclePackage
    {
        public DsdMinMaxRepository()
        {
            this.PackageName = MdmConstants.GetDsdMinMaxPackage;
        }

        public async Task<List<DsdMinMaxModel>> GetDsdMinMax(DsdMinMaxModel dsdMinMaxModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDsdMinMax, MdmConstants.GetProcDsdMinMax);
            OracleObject recordObject = this.SetParamsDsdMinMax(dsdMinMaxModel, true);
            return await this.GetProcedure2<DsdMinMaxModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ServiceDocumentResult> SetDsdMinMax(DsdMinMaxModel dsdMinMaxModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDsdMinMax, MdmConstants.SetProcDsdMinMax);
            OracleObject recordObject = this.SetParamsDsdMinMax(dsdMinMaxModel, false);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        private OracleObject SetParamsDsdMinMax(DsdMinMaxModel dsdMinMaxModel, bool isSearch)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(MdmConstants.RecordTypeDsdMinMax, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            if (!isSearch)
            {
                recordObject[recordType.Attributes["Operation"]] = dsdMinMaxModel.Operation;
            }

            recordObject[recordType.Attributes["supplier"]] = dsdMinMaxModel.Supplier;
            recordObject[recordType.Attributes["item"]] = dsdMinMaxModel.Item;
            recordObject[recordType.Attributes["location"]] = dsdMinMaxModel.Location;
            recordObject[recordType.Attributes["min_qty"]] = dsdMinMaxModel.MinQty;
            recordObject[recordType.Attributes["max_qty"]] = dsdMinMaxModel.MaxQty;
            recordObject[recordType.Attributes["repl_day"]] = dsdMinMaxModel.ReplDay;
            recordObject[recordType.Attributes["day1"]] = dsdMinMaxModel.Day1;
            recordObject[recordType.Attributes["day2"]] = dsdMinMaxModel.Day2;
            recordObject[recordType.Attributes["day3"]] = dsdMinMaxModel.Day3;
            recordObject[recordType.Attributes["day4"]] = dsdMinMaxModel.Day4;
            recordObject[recordType.Attributes["day5"]] = dsdMinMaxModel.Day5;
            recordObject[recordType.Attributes["day6"]] = dsdMinMaxModel.Day6;
            recordObject[recordType.Attributes["day7"]] = dsdMinMaxModel.Day7;
            recordObject[recordType.Attributes["po_group"]] = dsdMinMaxModel.PoGroup;
            return recordObject;
        }
    }
}