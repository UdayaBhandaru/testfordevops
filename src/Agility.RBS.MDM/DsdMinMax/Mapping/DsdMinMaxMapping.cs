﻿// <copyright file="DsdMinMaxMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.DsdMinMax.Mapping
{
    using System;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.DsdMinMax.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class DsdMinMaxMapping : Profile
    {
        public DsdMinMaxMapping()
            : base("DsdMinMaxMapping")
        {
            this.CreateMap<OracleObject, DsdMinMaxModel>()
              .ForMember(m => m.Supplier, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["supplier"])))
              .ForMember(m => m.SupName, opt => opt.MapFrom(r => r["sup_name"]))
              .ForMember(m => m.Item, opt => opt.MapFrom(r => r["item"]))
              .ForMember(m => m.ItemDesc, opt => opt.MapFrom(r => r["item_desc"]))
              .ForMember(m => m.Location, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["location"])))
              .ForMember(m => m.LocName, opt => opt.MapFrom(r => r["loc_name"]))
              .ForMember(m => m.MinQty, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["min_qty"])))
              .ForMember(m => m.MaxQty, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["max_qty"])))
              .ForMember(m => m.ReplDay, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["repl_day"])))
              .ForMember(m => m.Day1, opt => opt.MapFrom(r => r["day1"]))
              .ForMember(m => m.Day2, opt => opt.MapFrom(r => r["day2"]))
              .ForMember(m => m.Day3, opt => opt.MapFrom(r => r["day3"]))
              .ForMember(m => m.Day4, opt => opt.MapFrom(r => r["day4"]))
              .ForMember(m => m.Day5, opt => opt.MapFrom(r => r["day5"]))
              .ForMember(m => m.Day6, opt => opt.MapFrom(r => r["day6"]))
              .ForMember(m => m.Day7, opt => opt.MapFrom(r => r["day7"]))
              .ForMember(m => m.PoGroup, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["po_group"])))
              .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
              .ForMember(m => m.ProgramPhase, opt => opt.MapFrom(r => r["program_phase"]))
              .ForMember(m => m.ProgramMessage, opt => opt.MapFrom(r => r["program_message"]))
              .ForMember(m => m.TableName, opt => opt.MapFrom(r => "NB_DSD_AR"))
              .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));
        }
    }
}
