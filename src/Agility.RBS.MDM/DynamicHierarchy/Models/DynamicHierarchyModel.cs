﻿// <copyright file="DynamicHierarchyModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// BrandModel
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.DynamicHierarchy.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class DynamicHierarchyModel : ProfileEntity
    {
        public string RmsName { get; set; }

        public string ClientName { get; set; }

        public string AbbrName { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Error { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }
    }
}
