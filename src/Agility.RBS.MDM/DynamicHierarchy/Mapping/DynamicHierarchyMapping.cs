﻿// <copyright file="DynamicHierarchyMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.DynamicHierarchy.Mapping
{
    using System;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.Brand.Models;
    using Agility.RBS.MDM.CostZone.Models;
    using Agility.RBS.MDM.DynamicHierarchy.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class DynamicHierarchyMapping : Profile
    {
        public DynamicHierarchyMapping()
            : base("DynamicHierarchyMapping")
        {
            this.CreateMap<OracleObject, DynamicHierarchyModel>()
                .ForMember(m => m.RmsName, opt => opt.MapFrom(r => r["rms_name"]))
                .ForMember(m => m.ClientName, opt => opt.MapFrom(r => r["client_name"]))
                .ForMember(m => m.AbbrName, opt => opt.MapFrom(r => r["abbr_name"]))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "Employee"))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));
        }
    }
}
