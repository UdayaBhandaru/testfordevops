﻿//-------------------------------------------------------------------------------------------------
// <copyright file="DynamicHierarchyController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// BrandController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.DynamicHierarchy
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.DynamicHierarchy.Models;
    using Agility.RBS.MDM.DynamicHierarchy.Repositories;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    public class DynamicHierarchyController : Controller
    {
        private readonly ServiceDocument<DynamicHierarchyModel> serviceDocument;
        private readonly DynamicHierarchyRepository dynamicHierarchyRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly DomainDataRepository domainDataRepository;

        public DynamicHierarchyController(
            ServiceDocument<DynamicHierarchyModel> serviceDocument,
            DynamicHierarchyRepository dynamicHierarchyRepository,
             DomainDataRepository domainDataRepository)
        {
            this.dynamicHierarchyRepository = dynamicHierarchyRepository;
            this.serviceDocument = serviceDocument;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<DynamicHierarchyModel>> List()
        {
            this.serviceDocument.DomainData.Add("indicator", this.domainDataRepository.DomainDetailData("YSNO").Result);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<DynamicHierarchyModel>> New()
        {
            this.serviceDocument.DomainData.Add("indicator", this.domainDataRepository.DomainDetailData("YSNO").Result);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<DynamicHierarchyModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.DynamicHierarchySearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<DynamicHierarchyModel>> Open(string id)
        {
            this.serviceDocument.DataProfile.DataModel = new DynamicHierarchyModel { RmsName = id };
            this.serviceDocument.DomainData.Add("indicator", this.domainDataRepository.DomainDetailData("YSNO").Result);
            return await this.serviceDocument.OpenAsync(this.EmpOpen);
        }

        public async Task<ServiceDocument<DynamicHierarchyModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.DynamicHierarchySave);
            return this.serviceDocument;
        }

        private async Task<List<DynamicHierarchyModel>> DynamicHierarchySearch()
        {
            try
            {
                var lstUomClass = await this.dynamicHierarchyRepository.GetDynamicHierarchy(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstUomClass;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<DynamicHierarchyModel> EmpOpen()
        {
            try
            {
                var lstUomClass = await this.dynamicHierarchyRepository.GetDynamicHierarchy(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstUomClass?[0];
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> DynamicHierarchySave()
        {
            this.serviceDocument.Result = await this.dynamicHierarchyRepository.SetDynamicHierarchy(this.serviceDocument.DataProfile.DataModel);
            return true;
        }
    }
}
