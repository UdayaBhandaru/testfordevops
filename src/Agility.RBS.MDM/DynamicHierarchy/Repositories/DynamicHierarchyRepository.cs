﻿// <copyright file="DynamicHierarchyRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// BrandRepository
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.DynamicHierarchy.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.DynamicHierarchy.Models;
    using AutoMapper;
    using Devart.Data.Oracle;
    using Microsoft.AspNetCore.Authorization;

    public class DynamicHierarchyRepository : BaseOraclePackage
    {
        public DynamicHierarchyRepository()
        {
            this.PackageName = "UTL_CTRL_MST_DATA";
        }

        public async Task<List<DynamicHierarchyModel>> GetDynamicHierarchy(DynamicHierarchyModel dynamicHierarchyModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams("dyn_hier_tab", "getdynhier");
            OracleObject recordObject = this.SetParamsDynamicHierarchy(dynamicHierarchyModel, true);
            return await this.GetProcedure2<DynamicHierarchyModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ServiceDocumentResult> SetDynamicHierarchy(DynamicHierarchyModel dynamicHierarchyModel)
        {
            PackageParams packageParameter = this.GetPackageParams("dyn_hier_tab", "setdynhier");
            OracleObject recordObject = this.SetParamsDynamicHierarchy(dynamicHierarchyModel, false);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public virtual void SetParamsDynamicHierarchySearch(DynamicHierarchyModel dynamicHierarchyModel, OracleObject recordObject)
        {
            recordObject["rms_name"] = dynamicHierarchyModel.RmsName;
            recordObject["client_name"] = dynamicHierarchyModel.ClientName;
            recordObject["abbr_name"] = dynamicHierarchyModel.AbbrName;
        }

        private OracleObject SetParamsDynamicHierarchy(DynamicHierarchyModel dynamicHierarchyModel, bool isSearch)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("dyn_hier_rec");
            OracleObject recordObject = this.GetOracleObject(recordType);
            if (!isSearch)
            {
                recordObject["operation"] = dynamicHierarchyModel.Operation;
            }

            this.SetParamsDynamicHierarchySearch(dynamicHierarchyModel, recordObject);
            return recordObject;
        }
    }
}