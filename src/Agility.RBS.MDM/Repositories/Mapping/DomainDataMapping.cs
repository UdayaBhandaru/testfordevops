﻿// <copyright file="DomainDataMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Repositories.Mapping
{
    using System;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Models;
    using Agility.RBS.MDM.Repositories.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class DomainDataMapping : Profile
    {
        public DomainDataMapping()
            : base("DomainDataMapping")
        {
            // UOM Domain Data Mapping
            this.CreateMap<OracleObject, UomDomainModel>()
               .ForMember(m => m.UomCode, opt => opt.MapFrom(r => r["UOM"]))
               .ForMember(m => m.UomClass, opt => opt.MapFrom(r => r["UOM_CLASS"]))
               .ForMember(m => m.Description, opt => opt.MapFrom(r => r["UOM_DESC"]));

            // Division Domain Data Mapping
            this.CreateMap<OracleObject, DivisionDomainModel>()
                .ForMember(m => m.DivName, opt => opt.MapFrom(r => r["DIV_NAME"]))
                .ForMember(m => m.Division, opt => opt.MapFrom(r => r["DIVISION"]));

            // Group Domain Data Mapping
            this.CreateMap<OracleObject, GroupDomainModel>()
               .ForMember(m => m.GroupNo, opt => opt.MapFrom(r => r["GROUP_NO"]))
               .ForMember(m => m.Division, opt => opt.MapFrom(r => r["DIVISION"]))
               .ForMember(m => m.GroupName, opt => opt.MapFrom(r => r["GROUP_NAME"]));

            // security Group Domain Data Mapping
            this.CreateMap<OracleObject, SecurityGroupDomainModel>()
               .ForMember(m => m.GroupId, opt => opt.MapFrom(r => r["GROUP_id"]))
               .ForMember(m => m.GroupName, opt => opt.MapFrom(r => r["GROUP_NAME"]));

            // security User Domain Data Mapping
            this.CreateMap<OracleObject, SecurityUserDomainModel>()
               .ForMember(m => m.UserId, opt => opt.MapFrom(r => r["USER_ID"]))
               .ForMember(m => m.UserName, opt => opt.MapFrom(r => r["USER_NAME"]));

            // Department Domain Data Mapping
            this.CreateMap<OracleObject, DepartmentDomainModel>()
                .ForMember(m => m.GroupNo, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["GROUP_NO"])))
                .ForMember(m => m.Dept, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DEPT"])))
                .ForMember(m => m.DeptName, opt => opt.MapFrom(r => r["DEPT_NAME"]));

            // Class Domain Data Mapping
            this.CreateMap<OracleObject, ClassDomainModel>()
                .ForMember(m => m.Dept, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DEPT"])))
                .ForMember(m => m.Class, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["CLASS_ID"])))
                .ForMember(m => m.ClassName, opt => opt.MapFrom(r => r["CLASS_NAME"]));

            // Sub Class Domain Data Mapping
            this.CreateMap<OracleObject, SubClassDomainModel>()
                .ForMember(m => m.Dept, opt => opt.MapFrom(r => r["DEPT"]))
                .ForMember(m => m.Class, opt => opt.MapFrom(r => r["CLASS_ID"]))
                .ForMember(m => m.Subclass, opt => opt.MapFrom(r => r["SUBCLASS"]))
                .ForMember(m => m.SubName, opt => opt.MapFrom(r => r["SUB_NAME"]));

            // Company Domain Data Mapping
            this.CreateMap<OracleObject, CompanyDomainModel>()
                .ForMember(m => m.CompanyId, opt => opt.MapFrom(r => r["COMPANY"]))
                .ForMember(m => m.CompanyName, opt => opt.MapFrom(r => r["CO_NAME"]));

            // Org Country Domain Data Mapping
            this.CreateMap<OracleObject, OrgCountryDomainModel>()
                .ForMember(m => m.CompanyId, opt => opt.MapFrom(r => r["COMPANY_ID"]))
                .ForMember(m => m.CountryCode, opt => opt.MapFrom(r => r["COUNTRY_CODE"]))
                .ForMember(m => m.CountryName, opt => opt.MapFrom(r => r["COUNTRY_NAME"]))
                .ForMember(m => m.CountryStatus, opt => opt.MapFrom(r => r["COUNTRY_STATUS"]));

            // Country Domain Data Mapping
            this.CreateMap<OracleObject, CountryDomainModel>()
                .ForMember(m => m.CountryCode, opt => opt.MapFrom(r => r["COUNTRY_ID"]))
                .ForMember(m => m.CountryName, opt => opt.MapFrom(r => r["COUNTRY_DESC"]));

            // Country Currency Domain Data Mapping
            this.CreateMap<OracleObject, CountryCurrDomainModel>()
                .ForMember(m => m.CountryCode, opt => opt.MapFrom(r => r["COUNTRY_ID"]))
                .ForMember(m => m.CurrencyCode, opt => opt.MapFrom(r => r["CURRENCY_CODE"]))
                .ForMember(m => m.ExchangeRate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["EXCHANGE_RATE"])))
                .ForMember(m => m.CountryName, opt => opt.MapFrom(r => r["COUNTRY_DESC"]));

            // State Domain Data Mapping
            this.CreateMap<OracleObject, StateDomainModel>()
                .ForMember(m => m.State, opt => opt.MapFrom(r => r["STATE"]))
                .ForMember(m => m.Description, opt => opt.MapFrom(r => r["DESCRIPTION"]));

            // Buyer Domain Data Mapping
            this.CreateMap<OracleObject, BuyerDomainModel>()
                .ForMember(m => m.Buyer, opt => opt.MapFrom(r => r["BUYER"]))
                .ForMember(m => m.BuyerName, opt => opt.MapFrom(r => r["BUYER_NAME"]));

            // Merch Domain Data Mapping
            this.CreateMap<OracleObject, MerchDomainModel>()
                .ForMember(m => m.Merch, opt => opt.MapFrom(r => r["MERCH"]))
                .ForMember(m => m.MerchName, opt => opt.MapFrom(r => r["MERCH_NAME"]));

            // Region Domain Data Mapping
            this.CreateMap<OracleObject, RegionDomainModel>()
                .ForMember(m => m.CompanyId, opt => opt.MapFrom(r => r["COMPANY_ID"]))
                .ForMember(m => m.CountryCode, opt => opt.MapFrom(r => r["COUNTRY_CODE"]))
                .ForMember(m => m.RegionCode, opt => opt.MapFrom(r => r["REGION_CODE"]))
                .ForMember(m => m.RegionName, opt => opt.MapFrom(r => r["REGION_NAME"]))
                .ForMember(m => m.RegionStatus, opt => opt.MapFrom(r => r["REGION_STATUS"]));

            // Format Domain Data Mapping
            this.CreateMap<OracleObject, FormatDomainModel>()
             .ForMember(m => m.CompanyId, opt => opt.MapFrom(r => r["COMPANY_ID"]))
             .ForMember(m => m.CountryCode, opt => opt.MapFrom(r => r["COUNTRY_CODE"]))
             .ForMember(m => m.RegionCode, opt => opt.MapFrom(r => r["REGION_CODE"]))
             .ForMember(m => m.FormatCode, opt => opt.MapFrom(r => r["FORMAT_CODE"]))
             .ForMember(m => m.FormatName, opt => opt.MapFrom(r => r["FORMAT_NAME"]))
             .ForMember(m => m.FormatStatus, opt => opt.MapFrom(r => r["FORMAT_STATUS"]));

            // Location Domain Data Mapping
            this.CreateMap<OracleObject, LocationDomainModel>()
             .ForMember(m => m.LocationId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["LOCATION_ID"])))
             .ForMember(m => m.LocationName, opt => opt.MapFrom(r => r["LOC_NAME"]))
             .ForMember(m => m.LocType, opt => opt.MapFrom(r => r["LOC_TYPE"]));

            this.CreateMap<OracleObject, LocationTypeDomainModel>()
            .ForMember(m => m.LocationId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["LOCATION_ID"])))
            .ForMember(m => m.LocationName, opt => opt.MapFrom(r => r["LOC_NAME"]))
            .ForMember(m => m.LocType, opt => opt.MapFrom(r => r["LOC_TYPE"]));

            // Season Domain Data Mapping
            this.CreateMap<OracleObject, SeasonDomainModel>()
                .ForMember(m => m.SeasonId, opt => opt.MapFrom(r => r["SEASON_ID"]))
                .ForMember(m => m.SeasonName, opt => opt.MapFrom(r => r["SEASON_DESC"]));

            // Currency Domain Data Mapping
            this.CreateMap<OracleObject, CurrencyDomainModel>()
                .ForMember(m => m.CurrencyCode, opt => opt.MapFrom(r => r["CURRENCY_CODE"]))
                .ForMember(m => m.CurrencyDesc, opt => opt.MapFrom(r => r["CURRENCY_DESC"]));

            // VAT Region Domain Data Mapping
            this.CreateMap<OracleObject, VatRegionDomainModel>()
                .ForMember(m => m.VatRegionCode, opt => opt.MapFrom(r => r["VAT_REGION_CODE"]))
                .ForMember(m => m.VatRegionName, opt => opt.MapFrom(r => r["VAT_REGION_NAME"]))
                .ForMember(m => m.VatRegionStatus, opt => opt.MapFrom(r => r["VAT_REGION_STATUS"]));

            // VAT Code Domain Data Mapping
            this.CreateMap<OracleObject, VatCodeDomainModel>()
               .ForMember(m => m.VatCode, opt => opt.MapFrom(r => r["VAT_CODE"]))
               .ForMember(m => m.VatStatus, opt => opt.MapFrom(r => r["VAT_STATUS"]));

            // Brand Domain Data Mapping
            this.CreateMap<OracleObject, BrandDomainModel>()
               .ForMember(m => m.BrandId, opt => opt.MapFrom(r => r["BRAND_ID"]))
               .ForMember(m => m.BrandName, opt => opt.MapFrom(r => r["BRAND_NAME"]));

            // Supplier Domain Data Mapping
            this.CreateMap<OracleObject, SupplierDomainModel>()
               .ForMember(m => m.Supplier, opt => opt.MapFrom(r => r["SUPPLIER"]))
               .ForMember(m => m.SupName, opt => opt.MapFrom(r => r["SUP_NAME"]))
               .ForMember(m => m.ContactName, opt => opt.MapFrom(r => r["CONTACT_NAME"]))
               .ForMember(m => m.ContactPhone, opt => opt.MapFrom(r => r["CONTACT_PHONE"]))
               .ForMember(m => m.ContactFax, opt => opt.MapFrom(r => r["CONTACT_FAX"]))
               .ForMember(m => m.ContactEmail, opt => opt.MapFrom(r => r["CONTACT_EMAIL"]))
               .ForMember(m => m.SupStatus, opt => opt.MapFrom(r => r["SUP_STATUS"]))
               .ForMember(m => m.OriginCountryId, opt => opt.MapFrom(r => r["ORIGIN_COUNTRY_ID"]))
               .ForMember(m => m.OriginCountryName, opt => opt.MapFrom(r => r["ORIGIN_COUNTRY_NAME"]))
               .ForMember(m => m.CurrencyCode, opt => opt.MapFrom(r => r["CURRENCY_CODE"]))
               .ForMember(m => m.IncotermsCode, opt => opt.MapFrom(r => r["INCOTERMS_CODE"]))
               .ForMember(m => m.IncotermsDesc, opt => opt.MapFrom(r => r["INCOTERMS_DESC"]))
                .ForMember(m => m.SupplierType, opt => opt.MapFrom(r => r["SUPPLIER_TYPE"]))
               .ForMember(m => m.ProgramPhase, opt => opt.MapFrom(r => r["PROGRAM_PHASE"]))
               .ForMember(m => m.ProgramMessage, opt => opt.MapFrom(r => r["PROGRAM_MESSAGE"]))
               .ForMember(m => m.RebateDiscount, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["REBATE_DISCOUNT"])))
               .ForMember(m => m.PError, opt => opt.MapFrom(r => r["P_ERROR"]));

            this.CreateMap<OracleObject, ItemSupplierCompanyModel>()
            .ForMember(m => m.Item, opt => opt.MapFrom(r => r["ITEM"]))
            .ForMember(m => m.CompanyId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["COMPANY_ID"])))
            .ForMember(m => m.CompanyName, opt => opt.MapFrom(r => r["COMPANY_NAME"]))
            .ForMember(m => m.Supplier, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["SUPPLIER"])));

            this.CreateMap<OracleObject, UsersDomainModel>()
               .ForMember(m => m.UserId, opt => opt.MapFrom(r => r["USER_ID"]))
               .ForMember(m => m.UserName, opt => opt.MapFrom(r => r["USER_NAME"]))
               .ForMember(m => m.Email, opt => opt.MapFrom(r => r["USER_EMAIL_ADDR"]))
               .ForMember(m => m.ReportingMgrId, opt => opt.MapFrom(r => r["REPORTING_MGR_ID"]))
               .ForMember(m => m.ReportingMgrName, opt => opt.MapFrom(r => r["REPORTING_MGR_NAME"]))
               .ForMember(m => m.Status, opt => opt.MapFrom(r => r["STATUS"]));

            this.CreateMap<OracleObject, RolesDomainModel>()
               .ForMember(m => m.Id, opt => opt.MapFrom(r => r["ROLE_ID"]))
               .ForMember(m => m.Name, opt => opt.MapFrom(r => r["ROLE_NAME"]));

            this.CreateMap<OracleObject, UserRolePrivilegesDomainModel>()
              .ForMember(m => m.RoleId, opt => opt.MapFrom(r => r["ROLE"]))
              .ForMember(m => m.Role, opt => opt.MapFrom(r => r["ROLE"]));

            // Language Domain Data Mapping
            this.CreateMap<OracleObject, LanguageDomainModel>()
             .ForMember(m => m.LanguageID, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["lang"])))
                .ForMember(m => m.LanguageDesc, opt => opt.MapFrom(r => r["description"]));

            // Payment Methods Domain Data Mapping
            this.CreateMap<OracleObject, PaymentDomainModel>()
                .ForMember(m => m.PaymentMethodCode, opt => opt.MapFrom(r => r["PAY_METHOD_CODE"]))
                .ForMember(m => m.PaymentMethodName, opt => opt.MapFrom(r => r["PAY_METHOD_NAME"]))
                .ForMember(m => m.PaymentMethodDesc, opt => opt.MapFrom(r => r["PAY_METHOD_DESC"]))
                .ForMember(m => m.PaymentMethodStatus, opt => opt.MapFrom(r => r["PAY_METHOD_STATUS"]));

            // Terms Domain Data Mapping
            this.CreateMap<OracleObject, TermsDomainModel>()
                .ForMember(m => m.TermsID, opt => opt.MapFrom(r => r["TERMS_ID"]))
                .ForMember(m => m.TermsCode, opt => opt.MapFrom(r => r["TERMS_CODE"]))
                .ForMember(m => m.TermsDesc, opt => opt.MapFrom(r => r["TERMS_DESC"]))
                .ForMember(m => m.TermsStatus, opt => opt.MapFrom(r => r["TERMS_STATUS"]));

            // Freight Domain Data Mapping
            this.CreateMap<OracleObject, FreightDomainModel>()
                .ForMember(m => m.FreightTermsId, opt => opt.MapFrom(r => r["FREIGHT_TERMS"]))
                .ForMember(m => m.FreightTermsName, opt => opt.MapFrom(r => r["FREIGHT_DESC"]))
                .ForMember(m => m.FreightTermsStatus, opt => opt.MapFrom(r => r["FREIGHT_STATUS"]));

            // Delivery Policy Domain Data Mapping
            this.CreateMap<OracleObject, DeliveryPolicyDomainModel>()
                .ForMember(m => m.DeliveryPolicyID, opt => opt.MapFrom(r => r["DELIVERY_POLICY_ID"]))
                .ForMember(m => m.DeliveryPolicyName, opt => opt.MapFrom(r => r["DELIVERY_POLICY_DESC"]))
                .ForMember(m => m.DeliveryPolicyStatus, opt => opt.MapFrom(r => r["DELIVERY_POLICY_STATUS"]));

            // Out Location Domain Data Mapping
            this.CreateMap<OracleObject, OutLocationDomainModel>()
                .ForMember(m => m.OutlocId, opt => opt.MapFrom(r => r["OUTLOC_ID"]))
                .ForMember(m => m.OutlocDesc, opt => opt.MapFrom(r => r["OUTLOC_DESC"]));

            // Cost Zone Domain Data Mapping
            this.CreateMap<OracleObject, CostZoneDomainModel>()
                .ForMember(m => m.ZoneId, opt => opt.MapFrom(r => r["ZONE_ID"]))
                .ForMember(m => m.ZoneDescription, opt => opt.MapFrom(r => r["ZONE_DESCRIPTION"]));

            // Cost Change Reason Domain Data Mapping
            this.CreateMap<OracleObject, CostChangeReasonDomainModel>()
                .ForMember(m => m.Reason, opt => opt.MapFrom(r => r["REASON"]))
                .ForMember(m => m.ReasonDescription, opt => opt.MapFrom(r => r["REASON_DESC"]))
                .ForMember(m => m.ReasonStatus, opt => opt.MapFrom(r => r["REASON_STATUS"]));

            // Cost Change Origin Domain Data Mapping
            this.CreateMap<OracleObject, CostChangeOriginDomainModel>()
                .ForMember(m => m.OriginDetailId, opt => opt.MapFrom(r => r["DOMAIN_DETAIL_ID"]))
                .ForMember(m => m.OriginDescription, opt => opt.MapFrom(r => r["DOMAIN_DETAIL_DESC"]))
                .ForMember(m => m.OriginStatus, opt => opt.MapFrom(r => r["DOMAIN_STATUS"]));

            // Common Data Mapping
            this.CreateMap<OracleObject, CommonModel>()
                .ForMember(m => m.Id, opt => opt.MapFrom(r => r["ID"]))
                .ForMember(m => m.Name, opt => opt.MapFrom(r => r["NAME"]))
                .ForMember(m => m.Status, opt => opt.MapFrom(r => r["STATUS"]));

            this.CreateMap<OracleObject, PriceChangeReasonDomainModel>()
                .ForMember(m => m.Reason, opt => opt.MapFrom(r => r["REASON"]))
                .ForMember(m => m.ReasonDesc, opt => opt.MapFrom(r => r["REASON_DESC"]));

            this.CreateMap<OracleObject, PromotionDomainModel>()
             .ForMember(m => m.PromoId, opt => opt.MapFrom(r => r["PROMO_ID"]))
             .ForMember(m => m.Name, opt => opt.MapFrom(r => r["NAME"]))
             .ForMember(m => m.State, opt => opt.MapFrom(r => r["STATE"]))
             .ForMember(m => m.PromotionType, opt => opt.MapFrom(r => r["PROMOTION_TYPE"]));

            this.CreateMap<OracleObject, SupplierDetailDomainModel>()
            .ForMember(m => m.Supplier, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["SUPPLIER"])))
            .ForMember(m => m.SupName, opt => opt.MapFrom(r => r["SUP_NAME"]))
            .ForMember(m => m.SupStatus, opt => opt.MapFrom(r => r["SUP_STATUS"]))
            .ForMember(m => m.QcInd, opt => opt.MapFrom(r => r["QC_IND"]))
            .ForMember(m => m.FreightTerms, opt => opt.MapFrom(r => r["FREIGHT_TERMS"]))
            .ForMember(m => m.FreightTermsDesc, opt => opt.MapFrom(r => r["FREIGHT_TERMS_DESC"]))
            .ForMember(m => m.PaymentMethod, opt => opt.MapFrom(r => r["PAYMENT_METHOD"]))
            .ForMember(m => m.PaymentMethodDesc, opt => opt.MapFrom(r => r["PAYMENT_METHOD_DESC"]))
            .ForMember(m => m.ShipMethod, opt => opt.MapFrom(r => r["SHIP_METHOD"]))
            .ForMember(m => m.ShipMethodDesc, opt => opt.MapFrom(r => r["SHIP_METHOD_DESC"]))
            .ForMember(m => m.TermsDesc, opt => opt.MapFrom(r => r["TERMS_DESC"]))
            .ForMember(m => m.CurrencyCode, opt => opt.MapFrom(r => r["CURRENCY_CODE"]))
            .ForMember(m => m.ExchangeRate, opt => opt.MapFrom(r => r["EXCHANGE_RATE"]))
            .ForMember(m => m.TermsId, opt => opt.MapFrom(r => r["TERMS_ID"]));

            this.CreateMap<OracleObject, SupplierAddressDomainModel>()
            .ForMember(m => m.Supplier, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["SUPPLIER"])))
            .ForMember(m => m.SupName, opt => opt.MapFrom(r => r["SUP_NAME"]))
            .ForMember(m => m.SupStatus, opt => opt.MapFrom(r => r["SUP_STATUS"]))
            .ForMember(m => m.QcInd, opt => opt.MapFrom(r => r["QC_IND"]))
            .ForMember(m => m.TermsId, opt => opt.MapFrom(r => r["TERMS_ID"]))
            .ForMember(m => m.TermsDesc, opt => opt.MapFrom(r => r["TERMS_DESC"]))
            .ForMember(m => m.FreightTerms, opt => opt.MapFrom(r => r["FREIGHT_TERMS"]))
            .ForMember(m => m.FreightTermsDesc, opt => opt.MapFrom(r => r["FREIGHT_TERMS_DESC"]))
            .ForMember(m => m.PaymentMethod, opt => opt.MapFrom(r => r["PAYMENT_METHOD"]))
            .ForMember(m => m.PaymentMethodDesc, opt => opt.MapFrom(r => r["PAYMENT_METHOD_DESC"]))
            .ForMember(m => m.ShipMethod, opt => opt.MapFrom(r => r["SHIP_METHOD"]))
            .ForMember(m => m.ShipMethodDesc, opt => opt.MapFrom(r => r["SHIP_METHOD_DESC"]))
            .ForMember(m => m.CurrencyCode, opt => opt.MapFrom(r => r["CURRENCY_CODE"]))
            .ForMember(m => m.ExchangeRate, opt => opt.MapFrom(r => r["EXCHANGE_RATE"]))
             .ForMember(m => m.Add1, opt => opt.MapFrom(r => r["Add_1"]))
             .ForMember(m => m.Add2, opt => opt.MapFrom(r => r["Add_2"]))
             .ForMember(m => m.Add3, opt => opt.MapFrom(r => r["Add_3"]))
             .ForMember(m => m.City, opt => opt.MapFrom(r => r["City"]))
             .ForMember(m => m.State, opt => opt.MapFrom(r => r["State"]))
             .ForMember(m => m.StateDesc, opt => opt.MapFrom(r => r["state_desc"]))
             .ForMember(m => m.Post, opt => opt.MapFrom(r => r["Post"]))
             .ForMember(m => m.CountryId, opt => opt.MapFrom(r => r["Country_Id"]))
             .ForMember(m => m.CountryDesc, opt => opt.MapFrom(r => r["country_desc"]))
             .ForMember(m => m.ContactName, opt => opt.MapFrom(r => r["Contact_Name"]))
             .ForMember(m => m.ContactPhone, opt => opt.MapFrom(r => r["Contact_Phone"]))
             .ForMember(m => m.ContactFax, opt => opt.MapFrom(r => r["Contact_Fax"]));

            // Department Buyer Domain Data Mapping
            this.CreateMap<OracleObject, DeptBuyerDomainModel>()
               .ForMember(m => m.DeptId, opt => opt.MapFrom(r => r["DEPT_ID"]))
               .ForMember(m => m.DeptDesc, opt => opt.MapFrom(r => r["DEPT_DESC"]))
               .ForMember(m => m.BuyerId, opt => opt.MapFrom(r => r["BUYER_ID"]))
               .ForMember(m => m.BuyerName, opt => opt.MapFrom(r => r["BUYER_NAME"]))
               .ForMember(m => m.DeptStatus, opt => opt.MapFrom(r => r["DEPT_STATUS"]));

            // Partner Domain Data Mapping
            this.CreateMap<OracleObject, PartnerDomainModel>()
               .ForMember(m => m.PartnerType, opt => opt.MapFrom(r => r["PARTNER_TYPE"]))
               .ForMember(m => m.PartnerId, opt => opt.MapFrom(r => r["PARTNER_ID"]))
               .ForMember(m => m.PartnerDesc, opt => opt.MapFrom(r => r["PARTNER_DESC"]))
               .ForMember(m => m.Status, opt => opt.MapFrom(r => r["STATUS"]));

            this.CreateMap<OracleObject, ItemListDomainModel>()
                .ForMember(m => m.ListId, opt => opt.MapFrom(r => r["LIST_ID"]))
                .ForMember(m => m.ListDesc, opt => opt.MapFrom(r => r["LIST_DESC"]))
                .ForMember(m => m.ListStatus, opt => opt.MapFrom(r => r["LIST_STATUS"]));

            // Order Item List Common Data Mapping
            this.CreateMap<OracleObject, OrderItemListCommonModel>()
               .ForMember(m => m.Item, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["ITEM"])))
               .ForMember(m => m.UnitCost, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["UNIT_COST"])))
               .ForMember(m => m.CostCurrency, opt => opt.MapFrom(r => r["COST_CURRENCY"]))
               .ForMember(m => m.UnitRetail, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["UNIT_RETAIL"])))
               .ForMember(m => m.RetailCurrency, opt => opt.MapFrom(r => r["RETAIL_CURRENCY"]))
               .ForMember(m => m.QuantityExpected, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["QTY_EXPECTED"])))
               .ForMember(m => m.WeightExpected, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["WEIGHT_EXPECTED"])))
               .ForMember(m => m.WeightExpUom, opt => opt.MapFrom(r => r["WEIGHT_EXP_UOM"]))
               .ForMember(m => m.IsExpiry, opt => opt.MapFrom(r => r["IS_EXPIRY"]));

            this.CreateMap<OracleObject, WorkflowStatusDomainModel>()
             .ForMember(m => m.Id, opt => opt.MapFrom(r => r["ID"]))
             .ForMember(m => m.Name, opt => opt.MapFrom(r => r["NAME"]))
             .ForMember(m => m.Status, opt => opt.MapFrom(r => r["STATUS"]));

            this.CreateMap<OracleObject, InboxCommonModel>()
            .ForMember(m => m.ActionType, opt => opt.MapFrom(r => r["ACTION_TYPE"]))
            .ForMember(m => m.Priority, opt => opt.MapFrom(r => r["PRIORITY"]));

            // File Path Data Mapping
            this.CreateMap<OracleObject, FilePathModel>()
                .ForMember(m => m.Code, opt => opt.MapFrom(r => r["CODE"]))
                .ForMember(m => m.TemplatePath, opt => opt.MapFrom(r => r["TEMPLATE_PATH"]))
                .ForMember(m => m.ArchivePath, opt => opt.MapFrom(r => r["ARCHIVE_PATH"]))
                .ForMember(m => m.ProcessPath, opt => opt.MapFrom(r => r["PROCESS_PATH"]));

            // Location Domain Data Mapping
            this.CreateMap<OracleObject, ReturntoVendorDomainModel>()
             .ForMember(m => m.ItemDesc, opt => opt.MapFrom(r => r["item_desc"]))
             .ForMember(m => m.UnitCostExts, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["unit_cost"])));

            // PO Type Data Mapping
            this.CreateMap<OracleObject, PoTypeDomainModel>()
               .ForMember(m => m.PoType, opt => opt.MapFrom(r => r["PO_TYPE"]))
               .ForMember(m => m.PoTypeDesc, opt => opt.MapFrom(r => r["PO_TYPE_DESC"]));

            this.CreateMap<OracleObject, TransferEntityDomainModel>()
             .ForMember(m => m.Desc, opt => opt.MapFrom(r => r["TSF_ENTITY_DESC"]))
             .ForMember(m => m.TsfEntityId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["TSF_ENTITY_ID"])));

            this.CreateMap<OracleObject, ChainDomainModel>()
            .ForMember(m => m.Chain, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["CHAIN"])))
            .ForMember(m => m.ChainName, opt => opt.MapFrom(r => r["CHAIN_NAME"]));

            this.CreateMap<OracleObject, AreaDomainModel>()
          .ForMember(m => m.Area, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["AREA"])))
          .ForMember(m => m.AreaName, opt => opt.MapFrom(r => r["AREA_NAME"]));

            // RPM Codes Domain Data Mapping
            this.CreateMap<OracleObject, RpmCodeDomainModel>()
               .ForMember(m => m.CodeId, opt => opt.MapFrom(r => r["CODE_ID"]))
               .ForMember(m => m.CodeType, opt => opt.MapFrom(r => r["CODE_TYPE"]))
               .ForMember(m => m.Code, opt => opt.MapFrom(r => r["CODE"]))
               .ForMember(m => m.Description, opt => opt.MapFrom(r => r["DESCRIPTION"]));

            // Partner Trans Domain Data Mapping
            this.CreateMap<OracleObject, PartnerTransDomainModel>()
               .ForMember(m => m.PartnerId, opt => opt.MapFrom(r => r["PARTNER_ID"]))
               .ForMember(m => m.PartnerDesc, opt => opt.MapFrom(r => r["PARTNER_DESC"]))
               .ForMember(m => m.PartnerStatus, opt => opt.MapFrom(r => r["PARTNER_STATUS"]))
               .ForMember(m => m.PartnerType, opt => opt.MapFrom(r => r["PARTNER_TYPE"]))
               .ForMember(m => m.Currency, opt => opt.MapFrom(r => r["CURRENCY"]))
               .ForMember(m => m.ExchangeRate, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["EXCHANGE_RATE"])))
               .ForMember(m => m.Terms, opt => opt.MapFrom(r => r["TERMS"]))
               .ForMember(m => m.ProgramPhase, opt => opt.MapFrom(r => r["PROGRAM_PHASE"]))
               .ForMember(m => m.ProgramMessage, opt => opt.MapFrom(r => r["PROGRAM_MESSAGE"]))
               .ForMember(m => m.PError, opt => opt.MapFrom(r => r["P_ERROR"]));

            // RPM Codes Domain Data Mapping
            this.CreateMap<OracleObject, VatRegionTypeModel>()
               .ForMember(m => m.VatRegionType, opt => opt.MapFrom(r => r["vat_region_type"]))
               .ForMember(m => m.VatRegionTypeDesc, opt => opt.MapFrom(r => r["vat_region_type_desc"]));

            this.CreateMap<OracleObject, CostLevelModel>()
              .ForMember(m => m.CostLevel, opt => opt.MapFrom(r => r["cost_level"]))
              .ForMember(m => m.CostLevelDesc, opt => opt.MapFrom(r => r["cost_level_desc"]));

            // Supplier Obligaton Data Mapping
            this.CreateMap<OracleObject, SupplierObligationModel>()
               .ForMember(m => m.Supplier, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["SUPPLIER"])))
               .ForMember(m => m.SupName, opt => opt.MapFrom(r => r["SUP_NAME"]))
               .ForMember(m => m.SupStatus, opt => opt.MapFrom(r => r["SUP_STATUS"]))
               .ForMember(m => m.CurrencyCode, opt => opt.MapFrom(r => r["CURRENCY_CODE"]))
               .ForMember(m => m.ExchangeRate, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["EXCHANGE_RATE"])))
               .ForMember(m => m.Terms, opt => opt.MapFrom(r => r["TERMS"]))
               .ForMember(m => m.ProgramPhase, opt => opt.MapFrom(r => r["PROGRAM_PHASE"]))
               .ForMember(m => m.ProgramMessage, opt => opt.MapFrom(r => r["PROGRAM_MESSAGE"]))
               .ForMember(m => m.PError, opt => opt.MapFrom(r => r["P_ERROR"]));

            this.CreateMap<OracleObject, PriceLevelModel>()
              .ForMember(m => m.PriceLevel, opt => opt.MapFrom(r => r["price_level"]))
              .ForMember(m => m.PriceLevelDesc, opt => opt.MapFrom(r => r["price_level_desc"]));

            this.CreateMap<OracleObject, CompanyHeadDomainModel>()
            .ForMember(m => m.CompanyId, opt => opt.MapFrom(r => r["Company"]))
            .ForMember(m => m.CompanyName, opt => opt.MapFrom(r => r["Co_Name"]));

            // Partner Type Domain Data Mapping
            this.CreateMap<OracleObject, PartnerTypeDomainModel>()
               .ForMember(m => m.PartnerType, opt => opt.MapFrom(r => r["PARTNER_TYPE"]))
               .ForMember(m => m.PartnerDesc, opt => opt.MapFrom(r => r["partner_type_desc"]));

            // Deal Comp Type Domain Data Mapping
            this.CreateMap<OracleObject, DealCompTypeDomainModel>()
               .ForMember(m => m.DealCompType, opt => opt.MapFrom(r => r["DEAL_COMP_TYPE"]))
               .ForMember(m => m.DealCompTypeDesc, opt => opt.MapFrom(r => r["DEAL_COMP_TYPE_DESC"]));

            this.CreateMap<OracleObject, CostZoneGroupDomainModel>()
             .ForMember(m => m.ZoneGroupId, opt => opt.MapFrom(r => r["ZONE_GROUP_ID"]))
             .ForMember(m => m.ZoneGroupDescription, opt => opt.MapFrom(r => r["DESCRIPTION"]));

            this.CreateMap<OracleObject, DistrictDomainModel>()
           .ForMember(m => m.District, opt => opt.MapFrom(r => r["district"]))
           .ForMember(m => m.DistrictName, opt => opt.MapFrom(r => r["district_name"]));

            this.CreateMap<OracleObject, NonMerchandiseCodeDetailDomainModel>()
           .ForMember(m => m.ComponentId, opt => opt.MapFrom(r => r["comp_id"]))
           .ForMember(m => m.Description, opt => opt.MapFrom(r => r["comp_desc"]));
        }
    }
}