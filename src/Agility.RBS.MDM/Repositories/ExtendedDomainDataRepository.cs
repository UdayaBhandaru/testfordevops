﻿// <copyright file="ExtendedDomainDataRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Data;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Core.Security.Models;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.Framework.Web.Security.Middleware;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Models;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Repositories.Models;
    using Agility.RBS.MDM.UtilityDomain;
    using Agility.RBS.MDM.UtilityDomain.Models;
    using Agility.RBS.MDM.ValueAddedTax.Models;
    using AutoMapper;
    using Devart.Data.Oracle;
    using Microsoft.Extensions.Caching.Distributed;

    public partial class DomainDataRepository : BaseOraclePackage
    {
        public string GetItemDescription(string item)
        {
            try
            {
                if (string.IsNullOrEmpty(item))
                {
                    throw new ArgumentNullException("item", "Item can not be empty");
                }

                this.Connection.Open();
                OracleParameterCollection parameters = this.Parameters;
                parameters.Clear();
                OracleParameter oracleParameter;
                oracleParameter = new OracleParameter { ParameterName = "p_item", OracleDbType = OracleDbType.VarChar, Direction = ParameterDirection.Input, Value = item };
                parameters.Add(oracleParameter);
                oracleParameter = new OracleParameter("RESULT", Devart.Data.Oracle.OracleDbType.VarChar)
                {
                    Direction = ParameterDirection.ReturnValue
                };
                parameters.Add(oracleParameter);
                this.ExecuteProcedure("GETITEMDESC", parameters);
                if (this.Parameters["Result"].Value == System.DBNull.Value)
                {
                    return null;
                }

                return this.Parameters["Result"].Value.ToString();
            }
            catch (Exception e)
            {
                return null;
            }
            finally
            {
                this.Connection.Close();
            }
        }

        public async Task<List<PriceLevelModel>> PriceLevelGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("PRICE_LEVEL_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainPriceLevel, MdmConstants.GetProcPriceLevelDomain);
            return await this.GetProcedure2<PriceLevelModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<SeasonDomainModel>> SeasonsGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("SEASON_LIST_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainSeasons, MdmConstants.GetProcSeasonsDomain);
            return await this.GetProcedure2<SeasonDomainModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<CompanyHeadDomainModel>> CompanyHeadDomainGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("COMPANY_OBJ_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainCompanies, MdmConstants.GetProcCompaniesDomain);
            return await this.GetProcedure2<CompanyHeadDomainModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<CostLevelModel>> CostLevelGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("COST_LEVEL_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainCostLevel, MdmConstants.GetProcCostLevelDomain);
            return await this.GetProcedure2<CostLevelModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<SupplierObligationModel>> SupplierObligationDomainGet(string name)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("SUPPLIER_DD_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeSuppierObligation, MdmConstants.GetProcSuppierObligation);
            List<SupplierObligationModel> suppliers = await this.GetProcedure2<SupplierObligationModel>(recordObject, packageParameter, this.serviceDocumentResult);

            if (!string.IsNullOrEmpty(name))
            {
                suppliers = suppliers.Where(i => UpperCase(i.SupName).Contains(UpperCase(name))).ToList();
            }

            return suppliers;
        }

        public async Task<List<SupplierObligationModel>> SupplierObligationDomainGet(int supplier)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("SUPPLIER_DD_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            recordObject["SUPPLIER"] = supplier;
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeSuppierObligation, MdmConstants.GetProcSuppierObligation);
            return await this.GetProcedure2<SupplierObligationModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<CostZoneGroupDomainModel>> CostZoneGroupDomainGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("COST_ZONE_GROUP_COMMON_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainCostZoneGroup, MdmConstants.GetProcCostZoneGroupDomain);
            return await this.GetProcedure2<CostZoneGroupDomainModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<VatRateModel>> GetVatRateList(string docDate)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("VAT_RATES_MST_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            ////recordObject["ACTIVE_DATE"] = docDate;
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            OracleParameter oracleParameter;
            oracleParameter = new OracleParameter { ParameterName = "ACTIVE_DATE", OracleDbType = OracleDbType.Date, Direction = ParameterDirection.Input, Value = docDate };
            parameters.Add(oracleParameter);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeImDocVatRateTAB, MdmConstants.GetProcImDocVatRate);
            var vatreates = await this.GetProcedure2<VatRateModel>(recordObject, packageParameter, this.serviceDocumentResult, parameters);
            return vatreates ?? new List<VatRateModel>();
        }
    }
}
