﻿// <copyright file="DomainDataRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Data;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Core.Models;
    using Agility.Framework.Core.Security.Models;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.Framework.Web.Security.Middleware;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Models;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Repositories.Models;
    using Agility.RBS.MDM.UtilityDomain;
    using Agility.RBS.MDM.UtilityDomain.Models;
    using AutoMapper;
    using Devart.Data.Oracle;
    using Microsoft.Extensions.Caching.Distributed;

    public partial class DomainDataRepository : BaseOraclePackage
    {
        private readonly UtilityDomainRepository domainRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;

        public DomainDataRepository(UtilityDomainRepository domainRepository)
        {
            this.PackageName = MdmConstants.GetDomainDataPackage;
            this.domainRepository = domainRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        // Loading Active Doamin Master Data of Item Source
        public async Task<List<DomainDetailModel>> DomainDetailData(string domainHdr)
        {
            return await this.domainRepository.GetUtilityDomainDetail(domainHdr, this.serviceDocumentResult);
        }

        public async Task<List<UomDomainModel>> UomDomainGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("UOM_MST_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainUom, MdmConstants.GetProcUom);
            return await this.GetProcedure2<UomDomainModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<DivisionDomainModel>> DivisionDomainGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("DIVISION_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainDivision, MdmConstants.GetProcDivision);
            return await this.GetProcedure2<DivisionDomainModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<NonMerchandiseCodeDetailDomainModel>> ComponentDetailsDomainGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("elc_complist_rec");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams("elc_complist_tab", "getelccomplist");
            return await this.GetProcedure2<NonMerchandiseCodeDetailDomainModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<GroupDomainModel>> GroupDomainGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("GROUP_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainGroup, MdmConstants.GetProcGroupDomain);
            return await this.GetProcedure2<GroupDomainModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<SecurityGroupDomainModel>> SecGroupDomainGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("SEC_GROUP_LIST_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams("sec_group_list_tab", "getsecgrouplist");
            return await this.GetProcedure2<SecurityGroupDomainModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<DepartmentDomainModel>> DepartmentDomainGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("DEPARTMENT_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainDepartment, MdmConstants.GetProcDeptDomain);
            return await this.GetProcedure2<DepartmentDomainModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<ClassDomainModel>> ClassDomainGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("CLASS_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainClass, MdmConstants.GetProcClass);
            return await this.GetProcedure2<ClassDomainModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<CurrencyDomainModel>> CurrencyDomainGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("CURRENCY_COMMON_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeCurrencyClass, MdmConstants.GetProcCurrency);
            return await this.GetProcedure2<CurrencyDomainModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<AjaxModel<List<SubClassDomainModel>>> SubClassDomainGet(int deptId, int classId)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("SUB_CLASS_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainSubClass, MdmConstants.GetProcSubClass);
            var subClass = await this.GetProcedureAjaxModel<SubClassDomainModel>(recordObject, packageParameter);
            subClass.Model = subClass.Model.Where(i => i.Dept == deptId && i.Class == classId).ToList();
            return subClass;
        }

        public async Task<List<CompanyDomainModel>> CompanyDomainGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("COMPANY_OBJ_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainCompany, MdmConstants.GetProcCompaniesDomain);
            return await this.GetProcedure2<CompanyDomainModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<CountryDomainModel>> CountryDomainGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("CNT_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainCountry, MdmConstants.GetProcCountryDomain);
            return await this.GetProcedure2<CountryDomainModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<CountryCurrDomainModel>> CountryCurrDomainGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("COUNTRY_CURRENCY_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams("COUNTRY_CURRENCY_TAB", "GETCOUNTRYCURRENCY");
            return await this.GetProcedure2<CountryCurrDomainModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<StateDomainModel>> StateDomainGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("STATE_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainState, MdmConstants.GetProcStateDomain);
            return await this.GetProcedure2<StateDomainModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<BuyerDomainModel>> BuyerDomainGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("BUYER_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainBuyer, MdmConstants.GetProcBuyerDomain);
            return await this.GetProcedure2<BuyerDomainModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<SecurityGroupDomainModel>> SecurityGroupDomainGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("SEC_GROUP_LIST_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeSecGroupDomain, MdmConstants.GetProcSecGroupDomain);
            return await this.GetProcedure2<SecurityGroupDomainModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<DistrictDomainModel>> DistrictsDomainGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("district_list_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams("district_list_tab", "getdistrictlist");
            return await this.GetProcedure2<DistrictDomainModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<SecurityUserDomainModel>> SecurityUserDomainGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("USER_ATTRIB_LIST_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeSecUserDomain, MdmConstants.GetProcSecUserDomain);
            return await this.GetProcedure2<SecurityUserDomainModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<MerchDomainModel>> MerchDomainGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("MERCHANT_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainMerch, MdmConstants.GetProcMerchDomain);
            return await this.GetProcedure2<MerchDomainModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<OrgCountryDomainModel>> OrgCountryDomainGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("ORGCOUNTRY_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainOrgCountry, MdmConstants.GetProcOrgCountry);
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            parameters.Add(this.ParameterBuilder(UserProfileHelper.GetUserParameter()));
            return await this.GetProcedure2<OrgCountryDomainModel>(recordObject, packageParameter, this.serviceDocumentResult, parameters);
        }

        public async Task<List<RegionDomainModel>> RegionDomainGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("REGION_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainRegion, MdmConstants.GetProcRegionDomain);
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            parameters.Add(this.ParameterBuilder(UserProfileHelper.GetUserParameter()));
            return await this.GetProcedure2<RegionDomainModel>(recordObject, packageParameter, this.serviceDocumentResult, parameters);
        }

        public async Task<List<FormatDomainModel>> FormatDomainGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("FORMAT_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainFormat, MdmConstants.GetProcFormatDomain);
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            parameters.Add(this.ParameterBuilder(UserProfileHelper.GetUserParameter()));
            return await this.GetProcedure2<FormatDomainModel>(recordObject, packageParameter, this.serviceDocumentResult, parameters);
        }

        public async Task<List<VatCodeDomainModel>> VatCodeDomainGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("VAT_CODE_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainVatCode, MdmConstants.GetProcVatCodeDomain);
            return await this.GetProcedure2<VatCodeDomainModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<VatCodeDomainModel>> VatCodeDomainGetByLocId(int locId)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("VAT_CODE_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainVatCode, MdmConstants.GetProcVatCodeByLocIdDomain);
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            OracleParameter oracleParameter;
            oracleParameter = new OracleParameter("p_location", OracleDbType.Number)
            {
                Direction = ParameterDirection.Input,
                Value = locId
            };
            parameters.Add(oracleParameter);
            return await this.GetProcedure2<VatCodeDomainModel>(recordObject, packageParameter, this.serviceDocumentResult, parameters);
        }

        public async Task<List<VatRegionDomainModel>> VatRegionDomainGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("VAT_REGION_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainVatRegion, MdmConstants.GetProcVatRegion);
            return await this.GetProcedure2<VatRegionDomainModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<BrandDomainModel>> BrandDomainGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("BRAND_COMMON_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainBrand, MdmConstants.GetProcBrandDomain);
            return await this.GetProcedure2<BrandDomainModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<BrandDomainModel>> BrandDomainGet(string name)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("BRAND_COMMON_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainBrand, MdmConstants.GetProcBrandDomain);
            List<BrandDomainModel> brands = await this.GetProcedure2<BrandDomainModel>(recordObject, packageParameter, this.serviceDocumentResult);
            if (!string.IsNullOrEmpty(name))
            {
                brands = brands.Where(i => UpperCase(i.BrandName).StartsWith(UpperCase(name))).ToList();
            }

            return brands;
        }

        public async Task<List<LocationDomainModel>> LocationDomainGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("LOCATION_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainLocation, MdmConstants.GetProcLocationDomain);
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            parameters.Add(this.ParameterBuilder(UserProfileHelper.GetUserParameter()));
            return await this.GetProcedure2<LocationDomainModel>(recordObject, packageParameter, this.serviceDocumentResult, parameters);
        }

        public async Task<List<LocationTypeDomainModel>> LocationTypeDomainGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("LOCATION_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainLocationType, MdmConstants.GetProcLocationTypeDomain);
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            parameters.Add(this.ParameterBuilder(UserProfileHelper.GetUserParameter()));
            return await this.GetProcedure2<LocationTypeDomainModel>(recordObject, packageParameter, this.serviceDocumentResult, parameters);
        }

        public async Task<List<SupplierDomainModel>> SupplierDomainGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("SUPPLIER_COMMON_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainSupplier, MdmConstants.GetProcSupplierDomain);
            return await this.GetProcedure2<SupplierDomainModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<SupplierDomainModel>> SupplierDomainGet(int supplier)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("SUPPLIER_COMMON_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            recordObject["SUPPLIER"] = supplier;
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainSupplier, MdmConstants.GetProcSupplierDomain);
            return await this.GetProcedure2<SupplierDomainModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<SupplierDomainModel>> SupplierDomainGet(string name)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("SUPPLIER_COMMON_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainSupplier, MdmConstants.GetProcSupplierDomain);
            List<SupplierDomainModel> suppliers = await this.GetProcedure2<SupplierDomainModel>(recordObject, packageParameter, this.serviceDocumentResult);
            if (!string.IsNullOrEmpty(name))
            {
                suppliers = suppliers.Where(i => UpperCase(i.SupNameDesc).Contains(UpperCase(name))).ToList();
            }

            return suppliers;
        }

        public async Task<List<ItemSupplierCompanyModel>> GetItemSuppCompany(int itemId)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("ITEM_SUPP_COMP_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            this.SetItemCode(itemId, recordObject);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeItemSupplierCompany, MdmConstants.GetProcItemSupplierCompany);
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            parameters.Add(this.ParameterBuilder(UserProfileHelper.GetUserParameter()));
            return await this.GetProcedure2<ItemSupplierCompanyModel>(recordObject, packageParameter, this.serviceDocumentResult, parameters);
        }

        public virtual void SetItemCode(int itemId, OracleObject recordObject)
        {
            recordObject["ITEM"] = itemId;
        }

        public async Task<ItemDomainModel> ItemDomainGet()
        {
            ItemDomainModel itemDomainModel = new ItemDomainModel();
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("ITEM_MST_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainItem, MdmConstants.GetProcItemDomain);
            OracleTable pItemMstTab = await this.GetProcedure(recordObject, packageParameter, this.serviceDocumentResult);
            OracleObject obj = (OracleObject)pItemMstTab[0];

            OracleTable uomTable = (Devart.Data.Oracle.OracleTable)obj["UOM_DTL"];
            if (uomTable.Count > 0)
            {
                itemDomainModel.Uoms.AddRange(Mapper.Map<List<UomDomainModel>>(uomTable));
            }

            OracleTable divisionTable = (Devart.Data.Oracle.OracleTable)obj["DIVISION_DTL"];
            if (divisionTable.Count > 0)
            {
                itemDomainModel.Divisions.AddRange(Mapper.Map<List<DivisionDomainModel>>(divisionTable));
            }

            OracleTable groupTable = (Devart.Data.Oracle.OracleTable)obj["GROUP_DTL"];
            if (groupTable.Count > 0)
            {
                itemDomainModel.Groups.AddRange(Mapper.Map<List<GroupDomainModel>>(groupTable));
            }

            OracleTable deptTable = (Devart.Data.Oracle.OracleTable)obj["DEPT_DTL"];
            if (deptTable.Count > 0)
            {
                itemDomainModel.Departments.AddRange(Mapper.Map<List<DepartmentDomainModel>>(deptTable));
            }

            OracleTable classTable = (Devart.Data.Oracle.OracleTable)obj["CLASS_DTL"];
            if (classTable.Count > 0)
            {
                itemDomainModel.Classes.AddRange(Mapper.Map<List<ClassDomainModel>>(classTable));
            }

            return itemDomainModel;
        }

        public async Task<List<UsersDomainModel>> UsersDomainGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("USER_LIST_COMMON_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainUsers, MdmConstants.GetProcUsersDomain);
            return await this.GetProcedure2<UsersDomainModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<RolesDomainModel>> UsersRolesDomainGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("ROLE_COMMON_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainUserRoles, MdmConstants.GetProcUserRolesDomain);
            return await this.GetProcedure2<RolesDomainModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<UserRolePrivilegesDomainModel>> UserRolePrivilegesDomainGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("dba_roles_rec");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams("dba_roles_tab", "getrolelist");
            return await this.GetProcedure2<UserRolePrivilegesDomainModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<LanguageDomainModel>> LanguageDomainGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("LANGUAGE_COMMON_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeLanguageDomain, MdmConstants.GetProcLanguage);
            return await this.GetProcedure2<LanguageDomainModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<TermsDomainModel>> TermsDomainGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("TERMS_COMMON_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeTermsDomain, MdmConstants.GetProcTerms);
            return await this.GetProcedure2<TermsDomainModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<FreightDomainModel>> FreightDomainGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("FRIEGHT_COMMON_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeFreightDomain, MdmConstants.GetProcFreight);
            return await this.GetProcedure2<FreightDomainModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<OutLocationDomainModel>> OutLocationDomainGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("OUTLOC_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainOutLocation, MdmConstants.GetProcOutLocationDomain);
            return await this.GetProcedure2<OutLocationDomainModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<LocationDomainModel>> GetLocationByWherehouseBasedDomainGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("LOCATION_COMMON_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainWhBasedLocations, MdmConstants.GetProcWhBasedLocationDomain);
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            parameters.Add(this.ParameterBuilder(UserProfileHelper.GetUserParameter()));
            return await this.GetProcedure2<LocationDomainModel>(recordObject, packageParameter, this.serviceDocumentResult, parameters);
        }

        public bool GetDuplicateCheck(NameValueCollection nvc)
        {
            this.Connection.Open();

            OracleParameter parameter;
            OracleParameterCollection parameters = new OracleParameterCollection();
            parameters = this.Parameters;
            parameters.Clear();
            nvc.AllKeys.ToList().ForEach(x =>
            {
                parameter = new OracleParameter(x, Devart.Data.Oracle.OracleDbType.VarChar);
                parameter.Direction = ParameterDirection.Input;
                parameter.Value = nvc[x];
                parameters.Add(parameter);
            });
            parameter = new OracleParameter("RESULT", Devart.Data.Oracle.OracleDbType.Boolean)
            {
                Direction = ParameterDirection.ReturnValue
            };
            parameters.Add(parameter);
            this.ExecuteProcedure(MdmConstants.DuplicateCheck, parameters);
            return (this.Parameters["Result"].Value != System.DBNull.Value) && (bool)this.Parameters["Result"].Value;
        }

        public async Task<List<CostChangeReasonDomainModel>> CostChangeReasonsDomainGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("COST_CHG_REASON_COMMON_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainCostChangeReason, MdmConstants.GetProcCostChangeReasonDomain);
            return await this.GetProcedure2<CostChangeReasonDomainModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<PriceChangeReasonDomainModel>> PriceChangeReasonsDomainGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("PRICE_CHG_REASON_COMMON_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainPriceChangeReason, MdmConstants.GetProcPriceChangeReasonDomain);
            return await this.GetProcedure2<PriceChangeReasonDomainModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<CommonModel>> CommonData(string commonId)
        {
            return await this.GetUtilityCommonData(commonId, this.serviceDocumentResult);
        }

        public async Task<List<CommonModel>> GetUtilityCommonData(string commonId, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeCommon, MdmConstants.GetProcCommonObject);
            BaseOracleParameter baseOracleParameter;
            baseOracleParameter = new BaseOracleParameter { Name = "RESULT", DataType = OracleDbType.Boolean, Direction = ParameterDirection.ReturnValue };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            parameters.Add(this.ParameterBuilder(UserProfileHelper.GetUserParameter()));
            baseOracleParameter = new BaseOracleParameter { Name = MdmConstants.CommonId, DataType = OracleDbType.VarChar, Direction = ParameterDirection.Input, Value = commonId };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));
            baseOracleParameter = new BaseOracleParameter
            {
                Name = packageParameter.ObjectTypeTable,
                DataType = OracleDbType.Table,
                Direction = ParameterDirection.InputOutput,
                ObjectTypeName = packageParameter.ObjectTypeName
            };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));
            return await this.GetProcedure2<CommonModel>(null, packageParameter, serviceDocumentResult, parameters);
        }

        public async Task<List<CommonModel>> GetUtilityCommonData(string commonId, string loggedInUserId, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeCommon, MdmConstants.GetProcCommonObject);
            BaseOracleParameter baseOracleParameter;
            baseOracleParameter = new BaseOracleParameter { Name = "RESULT", DataType = OracleDbType.Boolean, Direction = ParameterDirection.ReturnValue };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            OracleParameter parameter = new OracleParameter("P_USER", OracleDbType.VarChar);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = loggedInUserId;
            parameters.Add(parameter);
            baseOracleParameter = new BaseOracleParameter { Name = MdmConstants.CommonId, DataType = OracleDbType.VarChar, Direction = ParameterDirection.Input, Value = commonId };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));
            baseOracleParameter = new BaseOracleParameter
            {
                Name = packageParameter.ObjectTypeTable,
                DataType = OracleDbType.Table,
                Direction = ParameterDirection.InputOutput,
                ObjectTypeName = packageParameter.ObjectTypeName
            };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));
            return await this.GetProcedure2<CommonModel>(null, packageParameter, serviceDocumentResult, parameters);
        }

        public async Task<List<SupplierDetailDomainModel>> SupplierDetailsGet(int supplier)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("SUPP_COMMON_DTL_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            recordObject["SUPPLIER"] = supplier;
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainSupplierDtls, MdmConstants.GetProcSupplierDtlsDomain);
            return await this.GetProcedure2<SupplierDetailDomainModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<SupplierAddressDomainModel>> SupplierDetailAddrGet(int supplier)
        {
            this.Connection.Open();
            OracleObject recordObject = this.GetOracleObject(this.GetObjectType("SHIP_INFO_REC"));
            recordObject["SUPPLIER"] = supplier;
            PackageParams packageParameter = this.GetPackageParams("SHIP_INFO_TAB", "GETSHIPINFO");
            return await this.GetProcedure2<SupplierAddressDomainModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public decimal? ExchangeRateGet(string pSuppCountry, string pOrdLocation)
        {
            try
            {
                this.Connection.Open();
                OracleParameterCollection parameters = this.Parameters;
                parameters.Clear();
                BaseOracleParameter baseOracleParameter;
                baseOracleParameter = new BaseOracleParameter { Name = "RESULT", DataType = OracleDbType.Boolean, Direction = ParameterDirection.ReturnValue };
                parameters.Add(this.ParameterBuilder(baseOracleParameter));
                baseOracleParameter = new BaseOracleParameter { Name = MdmConstants.OrdHdrSuppCountry, DataType = OracleDbType.VarChar, Direction = ParameterDirection.Input, Value = pSuppCountry };
                parameters.Add(this.ParameterBuilder(baseOracleParameter));
                baseOracleParameter = new BaseOracleParameter { Name = MdmConstants.OrdHdrOrdLocation, DataType = OracleDbType.Number, Direction = ParameterDirection.Input, Value = pOrdLocation };
                parameters.Add(this.ParameterBuilder(baseOracleParameter));
                baseOracleParameter = new BaseOracleParameter { Name = MdmConstants.OrdHdrExchangeRate, DataType = OracleDbType.Number, Direction = ParameterDirection.Output };
                parameters.Add(this.ParameterBuilder(baseOracleParameter));
                this.ExecuteProcedure(MdmConstants.GetProcOrdExchangeRate, parameters);
                return (this.Parameters[MdmConstants.OrdHdrExchangeRate].Value == DBNull.Value) ? default(decimal?) : (decimal)this.Parameters[MdmConstants.OrdHdrExchangeRate].Value;
            }
            finally
            {
                this.Connection.Close();
            }
        }

        public async Task<List<ItemListDomainModel>> ItemListDomainGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("ITEM_LIST_HDR_COMM_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeItemListDomain, MdmConstants.GetProcItemListDomain);

            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            parameters.Add(this.ParameterBuilder(UserProfileHelper.GetUserParameter()));
            return await this.GetProcedure2<ItemListDomainModel>(recordObject, packageParameter, this.serviceDocumentResult, parameters);
        }

        public async Task<List<OrderItemListCommonModel>> OrderItemListGet(int orderNo)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("ORDER_ITEM_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeCommonOrderItemList, MdmConstants.GetProcOrderItemList);
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            OracleParameter oracleParameter;
            oracleParameter = new OracleParameter("P_ORDER_NO", OracleDbType.Number)
            {
                Direction = ParameterDirection.Input,
                Value = orderNo
            };
            parameters.Add(oracleParameter);
            return await this.GetProcedure2<OrderItemListCommonModel>(recordObject, packageParameter, this.serviceDocumentResult, parameters);
        }

        public async Task<InboxCountCommonModel> GetAllInboxTabsCountFromDB(string module)
        {
            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();

            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeGlobalInboxCount, MdmConstants.GetProcGlobalCount);
            OracleParameter parameter;
            parameter = new OracleParameter("RESULT", Devart.Data.Oracle.OracleDbType.Boolean)
            {
                Direction = System.Data.ParameterDirection.ReturnValue
            };
            parameters.Add(parameter);
            parameter = new OracleParameter("P_USERID", OracleDbType.VarChar)
            {
                Direction = System.Data.ParameterDirection.Input,
                Value = UserProfileHelper.GetInMemoryUserId()
            };
            parameters.Add(parameter);
            parameter = new OracleParameter("p_module", OracleDbType.VarChar)
            {
                Direction = System.Data.ParameterDirection.Input,
                Value = module
            };
            parameters.Add(parameter);
            parameter = new OracleParameter(packageParameter.ObjectTypeTable, Devart.Data.Oracle.OracleDbType.Table)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                ObjectTypeName = packageParameter.ObjectTypeName,
                Value = this.SetParamsInboxTabsCount()
            };
            parameters.Add(parameter);
            OracleTable pGlobalInboxTab = await this.GetProcedure(null, packageParameter, this.serviceDocumentResult, parameters);
            OracleObject obj = (OracleObject)pGlobalInboxTab[0];
            if (obj != null)
            {
                InboxCountCommonModel inboxCountModelObj = new InboxCountCommonModel
                {
                    PendingCnt = OracleNullHandler.DbNullIntHandler(obj["Pending_Cnt"]),
                    SnoozedCnt = OracleNullHandler.DbNullIntHandler(obj["Snoozed_Cnt"]),
                    CompletedCnt = OracleNullHandler.DbNullIntHandler(obj["Completed_Cnt"]),
                    SentCnt = OracleNullHandler.DbNullIntHandler(obj["Sent_Cnt"]),
                    PercentCnt = OracleNullHandler.DbNullIntHandler(obj["Percent_Cnt"]),
                    DuedateCnt = OracleNullHandler.DbNullIntHandler(obj["Duedate_Cnt"]),
                    HighPriorityCnt = OracleNullHandler.DbNullIntHandler(obj["HighPriority_Cnt"]),
                    DraftCnt = OracleNullHandler.DbNullIntHandler(obj["draft_cnt"])
                };
                return inboxCountModelObj;
            }

            return null;
        }

        public async Task<List<WorkflowStatusDomainModel>> WorkflowStatusGet(string moduleName)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("COMMON_OBJECT_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            OracleParameter oracleParameter;
            oracleParameter = new OracleParameter("P_MODULENAME", OracleDbType.VarChar)
            {
                Direction = ParameterDirection.Input,
                Value = moduleName
            };
            parameters.Add(oracleParameter);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.GetWfStatusTab, MdmConstants.GetWfStatus);
            return await this.GetProcedure2<WorkflowStatusDomainModel>(recordObject, packageParameter, this.serviceDocumentResult, parameters);
        }

        public async Task<InboxCommonModel> InboxCommonGet(string objectName, long objectValue)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType(MdmConstants.RecordObjInboxCommon);
            OracleObject recordObject = this.GetOracleObject(recordType);
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            OracleParameter oracleParameter;
            oracleParameter = new OracleParameter("P_PROFILEINSTANCEID", OracleDbType.VarChar)
            {
                Direction = ParameterDirection.Input,
                Value = objectValue
            };
            parameters.Add(oracleParameter);
            oracleParameter = new OracleParameter("P_MODULENAME", OracleDbType.VarChar)
            {
                Direction = ParameterDirection.Input,
                Value = objectName
            };
            parameters.Add(oracleParameter);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeInboxCommon, MdmConstants.GetProcInboxCommon);
            var inboxCommonModelListObj = await this.GetProcedure2<InboxCommonModel>(recordObject, packageParameter, this.serviceDocumentResult, parameters);
            return inboxCommonModelListObj != null ? inboxCommonModelListObj[0] : null;
        }

        public async Task<List<UomDomainModel>> FilteredUomsDomainGet(string uomClass)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("UOM_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            this.SetUomClass(uomClass, recordObject);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainUom, MdmConstants.GetProcUomMass);
            return await this.GetProcedure2<UomDomainModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public virtual void SetUomClass(string uomClass, OracleObject recordObject)
        {
            recordObject["UOM_CLASS"] = uomClass;
        }

        public async Task<List<LocationDomainModel>> LocationDomainGet(string userId)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("LOCATION_COMMON_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainLocation, MdmConstants.GetProcLocationDomain);
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            OracleParameter parameter = new OracleParameter("P_USER", OracleDbType.VarChar);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = userId;
            parameters.Add(parameter);
            return await this.GetProcedure2<LocationDomainModel>(recordObject, packageParameter, this.serviceDocumentResult, parameters);
        }

        public OracleTable SetParamsInboxTabsCount()
        {
            this.Connection.Open();
            OracleType itemComTableType = OracleType.GetObjectType(MdmConstants.ObjTypeGlobalInboxCount, this.Connection);
            OracleType recordType = OracleType.GetObjectType("GLOBAL_INBOX_CNT_REC", this.Connection);
            OracleTable pItemCompanyTab = new OracleTable(itemComTableType);
            OracleObject recordObject = new OracleObject(recordType);
            pItemCompanyTab.Add(recordObject);
            return pItemCompanyTab;
        }

        public async Task<FilePathModel> FilePathGet(string module)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("FILE_PATH_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeFilePath, MdmConstants.GetProcFilePath);
            List<FilePathModel> filePaths = await this.GetProcedure2<FilePathModel>(recordObject, packageParameter, this.serviceDocumentResult);
            return filePaths.FirstOrDefault(ids => ids.Code == module);
        }

        public string GetCommonDesc(string id, string descriptionColumn)
        {
            try
            {
                this.Connection.Open();
                OracleParameterCollection parameters;
                parameters = this.Parameters;
                parameters.Clear();
                OracleParameter parameter;
                parameter = new OracleParameter("RESULT", OracleDbType.VarChar, ParameterDirection.ReturnValue);
                parameters.Add(parameter);
                parameter = new OracleParameter("P_ID", OracleDbType.VarChar)
                {
                    Direction = System.Data.ParameterDirection.Input,
                    Value = id
                };
                parameters.Add(parameter);
                parameter = new OracleParameter("P_DOMAIN_ID", OracleDbType.VarChar)
                {
                    Direction = System.Data.ParameterDirection.Input,
                    Value = descriptionColumn
                };
                parameters.Add(parameter);

                this.ExecuteProcedure("GETCOMMONOBJECTDESC", parameters);
                return this.Parameters["RESULT"].Value == System.DBNull.Value ? null : this.Parameters["RESULT"].Value.ToString();
            }
            finally
            {
                this.Connection.Close();
            }
        }

        public async Task<List<PoTypeDomainModel>> PoTypeGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("POTYPE_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypePoTypeDomain, MdmConstants.GetProcPoType);
            return await this.GetProcedure2<PoTypeDomainModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<ChainDomainModel>> ChainDomainGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("CHAIN_LIST_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainChain, MdmConstants.GetProcChainDomain);
            return await this.GetProcedure2<ChainDomainModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<AreaDomainModel>> AreaDomainGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("AREA_LIST_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainArea, MdmConstants.GetProcAreaDomain);
            return await this.GetProcedure2<AreaDomainModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<RpmCodeDomainModel>> RpmCodesGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("RPM_CODES_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainRpmCodes, MdmConstants.GetProcRpmCodesDomain);
            return await this.GetProcedure2<RpmCodeDomainModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<PartnerTransDomainModel>> PartnerGet(string partnerType)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("PARTNER_DD_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypePartnerTrans, MdmConstants.GetProcPartnerTrans);
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            OracleParameter parameter = new OracleParameter("PARTNER_TYPE", OracleDbType.VarChar);
            parameter.Direction = ParameterDirection.Input;
            parameter.Value = partnerType;
            parameters.Add(parameter);
            return await this.GetProcedure2<PartnerTransDomainModel>(recordObject, packageParameter, this.serviceDocumentResult, parameters);
        }

        public async Task<List<VatRegionTypeModel>> VatRegionTypeGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("VAT_REGION_TYPE_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainValueRegionType, MdmConstants.GetProcValueRegionTypeDomain);
            return await this.GetProcedure2<VatRegionTypeModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<DealCompTypeDomainModel>> DealCompTypeGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("DEAL_COMP_TYPE_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeDomainValueDealCompType, MdmConstants.GetProcValueDealCompTypeDomain);
            return await this.GetProcedure2<DealCompTypeDomainModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }
    }
}
