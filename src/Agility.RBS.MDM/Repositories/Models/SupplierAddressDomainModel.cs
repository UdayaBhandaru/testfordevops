﻿// <copyright file="SupplierAddressDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class SupplierAddressDomainModel
    {
        public int Supplier { get; set; }

        public string SupName { get; set; }

        public string SupStatus { get; set; }

        public string QcInd { get; set; }

        public string TermsId { get; set; }

        public string TermsDesc { get; set; }

        public string FreightTerms { get; set; }

        public string FreightTermsDesc { get; set; }

        public string PaymentMethod { get; set; }

        public string PaymentMethodDesc { get; set; }

        public string ShipMethod { get; set; }

        public string ShipMethodDesc { get; set; }

        public string CurrencyCode { get; set; }

        public decimal RetMinDolAmt { get; set; }

        public decimal HandlingPct { get; set; }

        public string RetCourier { get; set; }

        public decimal ExchangeRate { get; set; }

        public string AddrType { get; set; }

        public string Add1 { get; set; }

        public string Add2 { get; set; }

        public string Add3 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string StateDesc { get; set; }

        public string CountryId { get; set; }

        public string CountryDesc { get; set; }

        public string Post { get; set; }

        public string ContactName { get; set; }

        public string ContactPhone { get; set; }

        public string ContactFax { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string PError { get; set; }
    }
}