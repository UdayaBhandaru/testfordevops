﻿// <copyright file="CostChangeReasonDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Repositories.Models
{
    using System;
    using Agility.Framework.Core.Profile.Entities;

    public class CostChangeReasonDomainModel
    {
        public int Reason { get; set; }

        public string ReasonDescription { get; set; }

        public string ReasonStatus { get; set; }
    }
}
