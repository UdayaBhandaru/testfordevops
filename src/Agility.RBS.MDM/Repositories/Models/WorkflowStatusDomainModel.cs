﻿// <copyright file="WorkflowStatusDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class WorkflowStatusDomainModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Status { get; set; }
    }
}
