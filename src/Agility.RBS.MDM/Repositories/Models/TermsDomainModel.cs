﻿// <copyright file="TermsDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class TermsDomainModel
    {
        public string TermsID { get; set; }

        public string TermsCode { get; set; }

        public string TermsDesc { get; set; }

        public string TermsStatus { get; set; }
    }
}
