﻿// <copyright file="ClassDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class ClassDomainModel
    {
        public int? Dept { get; set; }

        public int? Class { get; set; }

        public string ClassName { get; set; }

        public string ClassDescription
        {
            get
            {
                return this.ClassName + "(" + this.Class + ")";
            }
        }
    }
}
