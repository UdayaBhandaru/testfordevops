﻿// <copyright file="SupplierObligationModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class SupplierObligationModel
    {
        public int? Supplier { get; set; }

        public string SupName { get; set; }

        public string SupStatus { get; set; }

        public string CurrencyCode { get; set; }

        public int? ExchangeRate { get; set; }

        public string Terms { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string PError { get; set; }

        public string SupNameDesc
        {
            get
            {
                return this.SupName + "(" + this.Supplier + ")";
            }
        }
    }
}
