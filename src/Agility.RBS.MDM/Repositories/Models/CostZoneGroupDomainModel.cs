﻿// <copyright file="CostZoneGroupDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class CostZoneGroupDomainModel
    {
        public int? ZoneGroupId { get; set; }

        public string ZoneGroupDescription { get; set; }

        public string ZoneGrpDesp
        {
            get
            {
                return this.ZoneGroupDescription + "(" + this.ZoneGroupId + ")";
            }
        }
    }
}
