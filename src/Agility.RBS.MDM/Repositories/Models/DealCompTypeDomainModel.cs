﻿// <copyright file="DealCompTypeDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class DealCompTypeDomainModel
    {
        public string DealCompType { get; set; }

        public string DealCompTypeDesc { get; set; }
    }
}
