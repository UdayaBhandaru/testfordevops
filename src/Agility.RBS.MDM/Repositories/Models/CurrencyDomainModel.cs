﻿// <copyright file="CurrencyDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class CurrencyDomainModel
    {
        public string CurrencyCode { get; set; }

        public string CurrencyDesc { get; set; }
    }
}
