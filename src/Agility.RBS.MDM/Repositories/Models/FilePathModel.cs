﻿// <copyright file="FilePathModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class FilePathModel
    {
        public string Code { get; set; }

        public string ProcessPath { get; set; }

        public string TemplatePath { get; set; }

        public string ArchivePath { get; set; }
    }
}
