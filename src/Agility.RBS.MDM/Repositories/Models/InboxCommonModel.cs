﻿// <copyright file="InboxCommonModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Repositories.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class InboxCommonModel
    {
        public string ActionType { get; set; }

        public string Priority { get; set; }
    }
}
