﻿// <copyright file="InboxCountCommonModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Repositories.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class InboxCountCommonModel
    {
        public int? PendingCnt { get; set; }

        public int? SnoozedCnt { get; set; }

        public int? CompletedCnt { get; set; }

        public int? SentCnt { get; set; }

        public string UserID { get; set; }

        public int? PercentCnt { get; set; }

        public int? DuedateCnt { get; set; }

        public int? HighPriorityCnt { get; set; }

        public int? DraftCnt { get; set; }
    }
}
