﻿// <copyright file="ItemSupplierCompanyModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Repositories.Models
{
    using System;
    using System.Collections.Generic;
    using Agility.Framework.Core.Profile.Entities;

    public class ItemSupplierCompanyModel
    {
        public string Item { get; set; }

        public int? CompanyId { get; set; }

        public string CompanyName { get; set; }

        public int? Supplier { get; set; }

        public string SuppName { get; set; }
    }
}