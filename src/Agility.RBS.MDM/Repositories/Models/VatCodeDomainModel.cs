﻿// <copyright file="VatCodeDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class VatCodeDomainModel
    {
        public string VatCode { get; set; }

        public string VatStatus { get; set; }
    }
}
