﻿// <copyright file="PriceLevelModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class PriceLevelModel
    {
        public string PriceLevel { get; set; }

        public string PriceLevelDesc { get; set; }

        public string PriceLevelDescription
        {
            get
            {
                return this.PriceLevelDesc + "(" + this.PriceLevel + ")";
            }
        }
    }
}
