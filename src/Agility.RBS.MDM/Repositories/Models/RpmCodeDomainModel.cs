﻿// <copyright file="RpmCodeDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class RpmCodeDomainModel
    {
        public int? CodeId { get; set; }

        public int? CodeType { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }
    }
}
