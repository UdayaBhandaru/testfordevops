﻿// <copyright file="CostChangeOriginDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Repositories.Models
{
    using System;
    using Agility.Framework.Core.Profile.Entities;

    public class CostChangeOriginDomainModel
    {
        public string OriginDetailId { get; set; }

        public string OriginDescription { get; set; }

        public string OriginStatus { get; set; }
    }
}
