﻿// <copyright file="BrandDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class BrandDomainModel
    {
        public int BrandId { get; set; }

        public string BrandName { get; set; }

        public string BrandDescription
        {
            get
            {
                return this.BrandName + "(" + this.BrandId + ")";
            }
        }
    }
}
