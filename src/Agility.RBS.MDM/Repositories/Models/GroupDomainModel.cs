﻿// <copyright file="GroupDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class GroupDomainModel
    {
        public int? Division { get; set; }

        public int? GroupNo { get; set; }

        public string GroupName { get; set; }

        public string GroupDescription
        {
            get
            {
                return this.GroupName + "(" + this.GroupNo + ")";
            }
        }
    }
}
