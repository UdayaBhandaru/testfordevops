﻿// <copyright file="DivisionDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class DivisionDomainModel
    {
        public int? Division { get; set; }

        public string DivName { get; set; }

        public string DivisionDescription
        {
            get
            {
                return this.DivName + "(" + this.Division + ")";
            }
        }
    }
}
