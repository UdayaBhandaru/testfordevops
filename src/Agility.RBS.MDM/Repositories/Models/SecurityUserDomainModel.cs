﻿// <copyright file="SecurityUserDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class SecurityUserDomainModel
    {
        public string UserId { get; set; }

        public string UserName { get; set; }
    }
}
