﻿// <copyright file="ChainDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class ChainDomainModel
    {
        public int Chain { get; set; }

        public string ChainName { get; set; }

        public string ChainNameDesc
        {
            get
            {
                return this.ChainName + "(" + this.Chain + ")";
            }
        }
    }
}
