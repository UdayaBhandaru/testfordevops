﻿// <copyright file="OutLocationDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class OutLocationDomainModel
    {
        public string OutlocId { get; set; }

        public string OutlocDesc { get; set; }

        public string OutLocationStatus { get; set; }
    }
}
