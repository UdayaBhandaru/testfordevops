﻿// <copyright file="VatRegionTypeModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class VatRegionTypeModel
    {
        public string VatRegionType { get; set; }

        public string VatRegionTypeDesc { get; set; }

        public string VatRegnTypeDesc
        {
            get
            {
                return this.VatRegionType + "(" + this.VatRegionTypeDesc + ")";
            }
        }
    }
}
