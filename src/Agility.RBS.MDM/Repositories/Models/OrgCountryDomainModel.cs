﻿// <copyright file="OrgCountryDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class OrgCountryDomainModel
    {
        public int? CompanyId { get; set; }

        public string CountryCode { get; set; }

        public string CountryName { get; set; }

        public string CountryStatus { get; set; }

        public string CountryNameDesc
        {
            get
            {
                return this.CountryName + "(" + this.CountryCode + ")";
            }
        }
    }
}
