﻿// <copyright file="PaymentDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class PaymentDomainModel
    {
        public string PaymentMethodCode { get; set; }

        public string PaymentMethodName { get; set; }

        public string PaymentMethodDesc { get; set; }

        public string PaymentMethodStatus { get; set; }
    }
}
