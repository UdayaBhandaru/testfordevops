﻿// <copyright file="CountryCurrDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class CountryCurrDomainModel
    {
        public string CountryCode { get; set; }

        public string CountryName { get; set; }

        public string CurrencyCode { get; set; }

        public decimal? ExchangeRate { get; set; }

    }
}
