﻿// <copyright file="SeasonDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class SeasonDomainModel
    {
        public int? SeasonId { get; set; }

        public string SeasonName { get; set; }

        public string SeasonDescription
        {
            get
            {
                return this.SeasonName + "(" + this.SeasonId + ")";
            }
        }
    }
}
