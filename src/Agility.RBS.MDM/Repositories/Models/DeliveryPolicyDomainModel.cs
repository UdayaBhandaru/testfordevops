﻿// <copyright file="DeliveryPolicyDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class DeliveryPolicyDomainModel
    {
        public string DeliveryPolicyID { get; set; }

        public string DeliveryPolicyName { get; set; }

        public string DeliveryPolicyDesc
        {
            get
            {
                return this.DeliveryPolicyName + "(" + this.DeliveryPolicyID + ")";
            }
        }

        public string DeliveryPolicyStatus { get; set; }
    }
}
