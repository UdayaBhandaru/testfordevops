﻿// <copyright file="DepartmentDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class DepartmentDomainModel
    {
        public int? GroupNo { get; set; }

        public int? Dept { get; set; }

        public string DeptName { get; set; }

        public string DeptDescription
        {
            get
            {
                return this.DeptName + "(" + this.Dept + ")";
            }
        }
    }
}
