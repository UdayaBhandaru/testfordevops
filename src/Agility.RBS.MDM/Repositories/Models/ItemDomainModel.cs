﻿// <copyright file="ItemDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Repositories.Models
{
    using System.Collections.Generic;

    public class ItemDomainModel
    {
        public List<UomDomainModel> Uoms { get; private set; } = new List<UomDomainModel>();

        public List<DivisionDomainModel> Divisions { get; private set; } = new List<DivisionDomainModel>();

        public List<GroupDomainModel> Groups { get; private set; } = new List<GroupDomainModel>();

        public List<DepartmentDomainModel> Departments { get; private set; } = new List<DepartmentDomainModel>();

        public List<ClassDomainModel> Classes { get; private set; } = new List<ClassDomainModel>();
    }
}
