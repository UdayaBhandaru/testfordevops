﻿// <copyright file="UserRolePrivilegesDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Repositories.Models
{
    using System.Collections.Generic;

    public class UserRolePrivilegesDomainModel
    {
        public string RoleId { get; set; }

        public string Role { get; set; }
    }
}
