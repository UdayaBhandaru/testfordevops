﻿// <copyright file="PromotionDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class PromotionDomainModel
    {
        public int PromoId { get; set; }

        public string Name { get; set; }

        public string PromotionType { get; set; }

        public string State { get; set; }
    }
}
