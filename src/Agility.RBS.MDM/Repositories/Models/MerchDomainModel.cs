﻿// <copyright file="MerchDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class MerchDomainModel
    {
        public int? Merch { get; set; }

        public string MerchName { get; set; }
    }
}
