﻿// <copyright file="ShipmentDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class ShipmentDomainModel
    {
        public string ShipmentMethodID { get; set; }

        public string ShipmentMethodDesc { get; set; }

        public string ShipmentMethodStatus { get; set; }
    }
}
