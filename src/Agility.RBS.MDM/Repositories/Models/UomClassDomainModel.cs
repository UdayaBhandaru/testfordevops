﻿// <copyright file="UomClassDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class UomClassDomainModel
    {
        public string UomClass { get; set; }

        public string UomClassDesc { get; set; }

        public string UomClassStatus { get; set; }

        public string UomClassDescription
        {
            get
            {
                return this.UomClassDesc + "(" + this.UomClass + ")";
            }
        }
    }
}
