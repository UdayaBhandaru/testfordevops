﻿// <copyright file="CostZoneDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class CostZoneDomainModel
    {
        public string ZoneId { get; set; }

        public string ZoneDescription { get; set; }
    }
}
