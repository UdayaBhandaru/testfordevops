﻿// <copyright file="LocationTypeDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class LocationTypeDomainModel
    {
        public int? LocationId { get; set; }

        public string LocationName { get; set; }

        public string LocType { get; set; }

        public string LocationDescription
        {
            get
            {
                return this.LocationName + "(" + this.LocationId + ")";
            }
        }
    }
}
