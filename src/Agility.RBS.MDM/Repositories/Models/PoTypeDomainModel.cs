﻿// <copyright file="PoTypeDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Repositories.Models
{
    public class PoTypeDomainModel
    {
        public string PoType { get; set; }

        public string PoTypeDesc { get; set; }
    }
}
