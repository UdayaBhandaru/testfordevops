﻿// <copyright file="ItemListDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// Item List Domain
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class ItemListDomainModel
    {
        public int? ListId { get; set; }

        public string ListDesc { get; set; }

        public string ListStatus { get; set; }
    }
}
