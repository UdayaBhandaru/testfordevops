﻿// <copyright file="BuyerDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class BuyerDomainModel
    {
        public int? Buyer { get; set; }

        public string BuyerName { get; set; }
    }
}
