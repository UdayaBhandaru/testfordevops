﻿// <copyright file="CompanyDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class CompanyDomainModel
    {
        public int? CompanyId { get; set; }

        public string CompanyName { get; set; }

        public string CompanyNameDesc
        {
            get
            {
                return this.CompanyName + "(" + this.CompanyId + ")";
            }
        }

        public string CompanyStatus { get; set; }
    }
}
