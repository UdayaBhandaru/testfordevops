﻿// <copyright file="DistrictDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class DistrictDomainModel
    {
        public int District { get; set; }

        public string DistrictName { get; set; }

        public string DistrictNameDesc
        {
            get
            {
                return this.DistrictName + "(" + this.District + ")";
            }
        }
    }
}
