﻿// <copyright file="CountryDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class CountryDomainModel
    {
        public string CountryCode { get; set; }

        public string CountryName { get; set; }
    }
}
