﻿// <copyright file="TransferEntityDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class TransferEntityDomainModel
    {
        public int? TsfEntityId { get; set; }

        public string Desc { get; set; }

        public string TsfEntityDescription
        {
            get
            {
                return this.Desc + "(" + this.TsfEntityId + ")";
            }
        }
    }
}
