﻿// <copyright file="VatRegionDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class VatRegionDomainModel
    {
        public int VatRegionCode { get; set; }

        public string VatRegionName { get; set; }

        public string VatRegionStatus { get; set; }

        public string VatRegionDescription
        {
            get
            {
                return this.VatRegionName + "(" + this.VatRegionCode + ")";
            }
        }
    }
}
