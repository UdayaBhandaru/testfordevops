﻿// <copyright file="SupplierDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Repositories.Models
{
    using System;
    using Agility.Framework.Core.Profile.Entities;

    public class SupplierDomainModel
    {
        public int Supplier { get; set; }

        public string SupName { get; set; }

        public string ContactName { get; set; }

        public string ContactPhone { get; set; }

        public string ContactFax { get; set; }

        public string ContactEmail { get; set; }

        public string SupNameDesc
        {
            get
            {
                return this.SupName + "(" + this.Supplier + ")";
            }
        }

        public string SupStatus { get; set; }

        public string OriginCountryId { get; set; }

        public string OriginCountryName { get; set; }

        public string CurrencyCode { get; set; }

        public string IncotermsCode { get; set; }

        public string IncotermsDesc { get; set; }

        public string SupplierType { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string PError { get; set; }

        public decimal? RebateDiscount { get; set; }
    }
}
