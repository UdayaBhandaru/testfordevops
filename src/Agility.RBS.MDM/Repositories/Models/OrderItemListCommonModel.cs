﻿// <copyright file="OrderItemListCommonModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Repositories.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class OrderItemListCommonModel
    {
        public int? Item { get; set; }

        public int? UnitCost { get; set; }

        public string CostCurrency { get; set; }

        public decimal? UnitRetail { get; set; }

        public string RetailCurrency { get; set; }

        public decimal? QuantityExpected { get; set; }

        public decimal? WeightExpected { get; set; }

        public string WeightExpUom { get; set; }

        public string IsExpiry { get; set; }
    }
}
