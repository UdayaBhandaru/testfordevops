﻿// <copyright file="SupplierDetailDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Repositories.Models
{
    using System.Collections.Generic;
    using Agility.Framework.Core.Profile.Entities;

    public class SupplierDetailDomainModel
    {
        public int Supplier { get; set; }

        public string SupName { get; set; }

        public string SupStatus { get; set; }

        public string QcInd { get; set; }

        public string TermsId { get; set; }

        public string TermsDesc { get; set; }

        public string FreightTerms { get; set; }

        public string FreightTermsDesc { get; set; }

        public string PaymentMethod { get; set; }

        public string PaymentMethodDesc { get; set; }

        public string ShipMethod { get; set; }

        public string ShipMethodDesc { get; set; }

        public string CurrencyCode { get; set; }

        public decimal RetMinDolAmt { get; set; }

        public decimal HandlingPct { get; set; }

        public string RetCourier { get; set; }

        public decimal ExchangeRate { get; set; }
    }
}
