﻿// <copyright file="UomDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class UomDomainModel
    {
        public string UomClass { get; set; }

        public string UomCode { get; set; }

        public string Description { get; set; }

        public string Status { get; set; }

        public string UomDescription
        {
            get
            {
                return this.Description + "(" + this.UomCode + ")";
            }
        }
    }
}
