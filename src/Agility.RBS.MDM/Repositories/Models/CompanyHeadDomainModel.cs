﻿// <copyright file="CompanyHeadDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class CompanyHeadDomainModel
    {
        public int? CompanyId { get; set; }

        public string CompanyName { get; set; }

        public string CompanyNameDesc
        {
            get
            {
                return this.CompanyName + "(" + this.CompanyId + ")";
            }
        }
    }
}
