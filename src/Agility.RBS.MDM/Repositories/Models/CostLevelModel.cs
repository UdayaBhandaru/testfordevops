﻿// <copyright file="CostLevelModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class CostLevelModel
    {
        public string CostLevel { get; set; }

        public string CostLevelDesc { get; set; }

        public string CostLevelDescription
        {
            get
            {
                return this.CostLevelDesc + "(" + this.CostLevel + ")";
            }
        }
    }
}
