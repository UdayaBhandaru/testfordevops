// <copyright file="NonMerchandiseCodeDetailDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class NonMerchandiseCodeDetailDomainModel
    {
        public string ComponentId { get; set; }

        public string Description { get; set; }

        public string Operation { get; set; }
    }
}