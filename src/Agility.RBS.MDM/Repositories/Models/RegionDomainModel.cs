﻿// <copyright file="RegionDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class RegionDomainModel
    {
        public int? Region { get; set; }

        public string RegionName { get; set; }

        public int? CompanyId { get; set; }

        public string CountryCode { get; set; }

        public string RegionCode { get; set; }

        public string RegionStatus { get; set; }

        public string RegionDescription
        {
            get
            {
                return this.RegionName + "(" + this.Region + ")";
            }
        }
    }
}
