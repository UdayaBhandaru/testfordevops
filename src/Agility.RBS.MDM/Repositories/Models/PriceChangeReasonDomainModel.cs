﻿// <copyright file="PriceChangeReasonDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Repositories.Models
{
    public class PriceChangeReasonDomainModel
    {
        public int Reason { get; set; }

        public string ReasonDesc { get; set; }
    }
}
