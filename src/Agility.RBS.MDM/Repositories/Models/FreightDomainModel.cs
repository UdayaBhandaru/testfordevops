﻿// <copyright file="FreightDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class FreightDomainModel
    {
        public string FreightTermsId { get; set; }

        public string FreightTermsName { get; set; }

        public string FreightTermsDesc
        {
            get
            {
                return this.FreightTermsName + "(" + this.FreightTermsId + ")";
            }
        }

        public string FreightTermsStatus { get; set; }
    }
}
