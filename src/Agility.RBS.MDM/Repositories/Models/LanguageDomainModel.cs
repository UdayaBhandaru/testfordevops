﻿// <copyright file="LanguageDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class LanguageDomainModel
    {
        public int? LanguageID { get; set; }

        public string LanguageDesc { get; set; }

        public string LanguageStatus { get; set; }
    }
}
