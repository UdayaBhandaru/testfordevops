﻿// <copyright file="FormatDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class FormatDomainModel
    {
        public int? CompanyId { get; set; }

        public string CountryCode { get; set; }

        public string RegionCode { get; set; }

        public string FormatCode { get; set; }

        public string FormatName { get; set; }

        public string FormatStatus { get; set; }

        public string FormatDescription
        {
            get
            {
                return this.FormatName + "(" + this.FormatCode + ")";
            }
        }
    }
}
