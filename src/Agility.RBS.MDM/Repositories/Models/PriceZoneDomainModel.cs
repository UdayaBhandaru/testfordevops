﻿// <copyright file="PriceZoneDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class PriceZoneDomainModel
    {
        public string ZoneId { get; set; }

        public string ZoneDescription { get; set; }
    }
}
