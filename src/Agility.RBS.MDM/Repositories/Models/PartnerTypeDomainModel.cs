﻿// <copyright file="PartnerTypeDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class PartnerTypeDomainModel
    {
        public string PartnerType { get; set; }

        public string PartnerDesc { get; set; }
    }
}
