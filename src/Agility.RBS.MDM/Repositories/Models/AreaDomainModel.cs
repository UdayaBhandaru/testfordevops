﻿// <copyright file="AreaDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class AreaDomainModel
    {
        public int Area { get; set; }

        public string AreaName { get; set; }

        public string AreaNameDesc
        {
            get
            {
                return this.AreaName + "(" + this.Area + ")";
            }
        }
    }
}
