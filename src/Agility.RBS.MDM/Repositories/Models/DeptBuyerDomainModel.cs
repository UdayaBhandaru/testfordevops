﻿// <copyright file="DeptBuyerDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class DeptBuyerDomainModel
    {
        public int? DeptId { get; set; }

        public string DeptDesc { get; set; }

        public string DeptStatus { get; set; }

        public string BuyerId { get; set; }

        public string BuyerName { get; set; }

        public string DeptDescription
        {
            get
            {
                return this.DeptDesc + "(" + this.DeptId + ")";
            }
        }
    }
}
