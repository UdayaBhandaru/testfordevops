﻿// <copyright file="PartnerTransDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class PartnerTransDomainModel
    {
        public string PartnerId { get; set; }

        public string PartnerDesc { get; set; }

        public string PartnerStatus { get; set; }

        public string PartnerType { get; set; }

        public string Currency { get; set; }

        public int? ExchangeRate { get; set; }

        public string Terms { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string PError { get; set; }
    }
}
