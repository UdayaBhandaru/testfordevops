﻿// <copyright file="RolesDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Repositories.Models
{
    using System.Collections.Generic;

    public class RolesDomainModel
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }
}
