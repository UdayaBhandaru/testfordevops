﻿// <copyright file="SubClassDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Repositories.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class SubClassDomainModel
    {
        public int? Dept { get; set; }

        public int? Class { get; set; }

        public int? Subclass { get; set; }

        public string SubName { get; set; }

        public string SubClassDescription
        {
            get
            {
                return this.SubName + "(" + this.Subclass + ")";
            }
        }
    }
}
