﻿// <copyright file="UsersDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Repositories.Models
{
    using System.Collections.Generic;

    public class UsersDomainModel
    {
        public string UserId { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public string ReportingMgrId { get; set; }

        public string ReportingMgrName { get; set; }

        public string Status { get; set; }
    }
}
