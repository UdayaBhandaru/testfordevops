﻿// <copyright file="FreightSizeModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Freight.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class FreightSizeModel : ProfileEntity
    {
        public string FreightSize { get; set; }

        public string FreightSizeDescription { get; set; }
    }
}
