﻿// <copyright file="FreightTypeModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Freight.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class FreightTypeModel : ProfileEntity
    {
        public string FreightType { get; set; }

        public string FreightTypeDescription { get; set; }
    }
}