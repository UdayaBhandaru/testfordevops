﻿//-------------------------------------------------------------------------------------------------
// <copyright file="FreightTypeController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// FreightTypeController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Freight
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Freight.Models;
    using Agility.RBS.MDM.MerchantHierarchy.Models;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class FreightTypeController : Controller
    {
        private readonly ServiceDocument<FreightTypeModel> serviceDocument;
        private readonly FreightRepository freightRepository;

        public FreightTypeController(
            ServiceDocument<FreightTypeModel> serviceDocument,
            FreightRepository freightRepository,
            DomainDataRepository domainDataRepository)
        {
            this.freightRepository = freightRepository;
            this.serviceDocument = serviceDocument;
        }

        public async Task<ServiceDocument<FreightTypeModel>> List()
        {
            return await this.FreightTypeData();
        }

        private async Task<ServiceDocument<FreightTypeModel>> FreightTypeData()
        {
            this.serviceDocument.DataProfile.DataList = await this.freightRepository.GetFrieghtType();
            return this.serviceDocument;
        }
    }
}
