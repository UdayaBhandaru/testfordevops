﻿//-------------------------------------------------------------------------------------------------
// <copyright file="FreightSizeController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// FreightSizeController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.Freight
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Freight.Models;
    using Agility.RBS.MDM.MerchantHierarchy.Models;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class FreightSizeController : Controller
    {
        private readonly ServiceDocument<FreightSizeModel> serviceDocument;
        private readonly FreightRepository freightRepository;

        public FreightSizeController(
            ServiceDocument<FreightSizeModel> serviceDocument,
            FreightRepository freightRepository,
            DomainDataRepository domainDataRepository)
        {
            this.freightRepository = freightRepository;
            this.serviceDocument = serviceDocument;
        }

        public async Task<ServiceDocument<FreightSizeModel>> List()
        {
            return await this.FreightSizeData();
        }

        private async Task<ServiceDocument<FreightSizeModel>> FreightSizeData()
        {
            this.serviceDocument.DataProfile.DataList = await this.freightRepository.GetFrieghtSize();
            return this.serviceDocument;
        }
    }
}
