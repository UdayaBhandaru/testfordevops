﻿// <copyright file="FreightMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Freight.Mapping
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.Freight.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class FreightMapping : Profile
    {
        public FreightMapping()
            : base("FreightMapping")
        {
            this.CreateMap<OracleObject, FreightSizeModel>()
                .ForMember(m => m.FreightSize, opt => opt.MapFrom(r => r["freight_size"]))
                .ForMember(m => m.FreightSizeDescription, opt => opt.MapFrom(r => r["Freight_size_desc"]));
            this.CreateMap<OracleObject, FreightTypeModel>()
                .ForMember(m => m.FreightType, opt => opt.MapFrom(r => r["freight_type"]))
                .ForMember(m => m.FreightTypeDescription, opt => opt.MapFrom(r => r["Freight_type_desc"]));
        }
    }
}
