﻿// <copyright file="FreightRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Freight
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Freight.Models;
    using Agility.RBS.MDM.MerchantHierarchy.Models;
    using Agility.RBS.MDM.UtilityDomain.Models;
    using AutoMapper;
    using Devart.Data.Oracle;
    using Microsoft.Extensions.Caching.Distributed;

    public class FreightRepository : BaseOraclePackage
    {
        private readonly ServiceDocumentResult serviceDocumentResult;

        public FreightRepository()
        {
            this.PackageName = MdmConstants.GetDomainDataPackage;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        /// <summary>Gets the frieght type list</summary>
        /// <returns>freight type list</returns>
        public async Task<List<FreightTypeModel>> GetFrieghtType()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("FREIGHT_TYPE_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.FreightTypeTab, MdmConstants.GetProcFreightType);
            return await this.GetProcedure2<FreightTypeModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        /// <summary>Gets the  frieght size list.</summary>
        /// <returns>list of freight size</returns>
        public async Task<List<FreightSizeModel>> GetFrieghtSize()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("FREIGHT_SIZE_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.FreightSizeTab, MdmConstants.GetProcFreightSize);
            return await this.GetProcedure2<FreightSizeModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }
    }
}
