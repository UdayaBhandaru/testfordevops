﻿// <copyright file="CurrencyRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Currency
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Currency.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class CurrencyRepository : BaseOraclePackage
    {
        public CurrencyRepository()
        {
            this.PackageName = MdmConstants.GetCurrencyPackage;
        }

        public async Task<List<CurrencyModel>> GetCurrency(CurrencyModel currencyModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeCurrency, MdmConstants.GetProcCurrency);
            OracleObject recordObject = this.SetParamsCurrency(currencyModel, true);
            OracleTable pGetUsersMstTab = await this.GetProcedure(recordObject, packageParameter, serviceDocumentResult);
            var lstHeader = Mapper.Map<List<CurrencyModel>>(pGetUsersMstTab);
            if (lstHeader != null)
            {
                for (int i = 0; i < lstHeader.Count; i++)
                {
                    OracleObject obj = (OracleObject)pGetUsersMstTab[i];
                    OracleTable currencylocationDetails = (Devart.Data.Oracle.OracleTable)obj["currency_rate"];
                    lstHeader[i].CurrencyDetailList.AddRange(Mapper.Map<List<CurrencyDetailModel>>(currencylocationDetails));
                }
            }

            return lstHeader;
        }

        public async Task<ServiceDocumentResult> SetCurrency(CurrencyModel currencyModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeCurrency, MdmConstants.SetProcCurrency);
            OracleObject recordObject = this.SetParamsCurrency(currencyModel, false);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        private OracleObject SetParamsCurrency(CurrencyModel currencyModel, bool isSerach)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(MdmConstants.ObjRecCurrency, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            return this.SetGetCurrencyParamas(currencyModel, recordObject, isSerach);
        }

        private OracleObject SetGetCurrencyParamas(CurrencyModel currencyModel, OracleObject recordObject, bool isSerach)
        {
            if (!isSerach)
            {
                OracleTable pSubCurrencyTab = this.CurrencyDetailationParamsObject(currencyModel);
                recordObject["currency_rate"] = pSubCurrencyTab;
                recordObject["Operation"] = currencyModel.Operation;
                recordObject["CURRENCY_COST_FMT"] = currencyModel.CurrencyCostFmt;
                recordObject["CURRENCY_RTL_FMT"] = currencyModel.CurrencyRtlFmt;
                recordObject["CURRENCY_COST_DEC"] = currencyModel.CurrencyCostDec;
                recordObject["CURRENCY_RTL_DEC"] = currencyModel.CurrencyRtlDec;
            }

            recordObject["CURRENCY_CODE"] = currencyModel.CurrencyCode;
            recordObject["CURRENCY_DESC"] = currencyModel.CurrencyDesc;
            return recordObject;
        }

        private OracleTable CurrencyDetailationParamsObject(CurrencyModel currencyModel)
        {
            OracleType dtlTableType = this.GetObjectType(MdmConstants.ObjTypeCurrencyRate);
            OracleType dtlRecordType = this.GetObjectType(MdmConstants.ObjRecCurrencyRate);
            OracleTable pLocationTab = new OracleTable(dtlTableType);
            foreach (CurrencyDetailModel currencyDetailModel in currencyModel.CurrencyDetailList)
            {
                var dtlRecordObject = new OracleObject(dtlRecordType);
                dtlRecordObject["CURRENCY_CODE"] = currencyModel.CurrencyCode;
                dtlRecordObject["CURRENCY_DESC"] = currencyModel.CurrencyDesc;
                dtlRecordObject["EFFECTIVE_DATE"] = currencyDetailModel.EffectiveDate;
                dtlRecordObject["EXCHANGE_TYPE"] = currencyDetailModel.ExchangeType;
                dtlRecordObject["EXCHANGE_RATE"] = currencyDetailModel.ExchangeRate;
                dtlRecordObject["Operation"] = currencyDetailModel.Operation;
                pLocationTab.Add(dtlRecordObject);
            }

            return pLocationTab;
        }
    }
}