﻿// <copyright file="CurrencyModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Currency.Models
{
    using System;
    using System.Collections.Generic;
    using Agility.Framework.Core.Profile.Entities;

    public class CurrencyModel : ProfileEntity
    {
        public CurrencyModel()
        {
            this.CurrencyDetailList = new List<CurrencyDetailModel>();
        }

        public string CurrencyCode { get; set; }

        public string CurrencyDesc { get; set; }

        public string CurrencyCostFmt { get; set; }

        public string CurrencyRtlFmt { get; set; }

        public int? CurrencyCostDec { get; set; }

        public int? CurrencyRtlDec { get; set; }

        public string CurrencyIsoCode { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }

        public string Error { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public List<CurrencyDetailModel> CurrencyDetailList { get; private set; }

        public object CurrencyDetialList { get; internal set; }
    }
}
