﻿//-------------------------------------------------------------------------------------------------
// <copyright file="CurrencyController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// CurrencyController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.MerchantHierarchy
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.Framework.Web.Security.Repositories;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Currency;
    using Agility.RBS.MDM.Currency.Models;
    using Agility.RBS.MDM.MerchantHierarchy.Models;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    public class CurrencyController : Controller
    {
        private readonly ServiceDocument<CurrencyModel> serviceDocument;
        private readonly CurrencyRepository currencyRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly DomainDataRepository domainDataRepository;

        public CurrencyController(
            ServiceDocument<CurrencyModel> serviceDocument,
              CurrencyRepository currencyRepository,
            DomainDataRepository domainDataRepository)
        {
            this.currencyRepository = currencyRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<CurrencyModel>> List()
        {
            return await this.LoadDomainData();
        }

        public async Task<ServiceDocument<CurrencyModel>> New()
        {
            return await this.LoadDomainData();
        }

        public async Task<ServiceDocument<CurrencyModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.CurrencySearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<CurrencyModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.CurrencySave);
            return this.serviceDocument;
        }

        private async Task<List<CurrencyModel>> CurrencySearch()
        {
            try
            {
                var lstUom = await this.currencyRepository.GetCurrency(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstUom;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> CurrencySave()
        {
            this.serviceDocument.Result = await this.currencyRepository.SetCurrency(this.serviceDocument.DataProfile.DataModel);
            return true;
        }

        private async Task<ServiceDocument<CurrencyModel>> LoadDomainData()
        {
            this.serviceDocument.DomainData.Add("currencyExchType", this.domainDataRepository.DomainDetailData(MdmConstants.CurrencyExchTypeDomainData).Result);
            this.serviceDocument.DomainData.Add("indicatorStatus", this.domainDataRepository.DomainDetailData(MdmConstants.VatRegionTypeDomainData).Result);
            this.serviceDocument.LocalizationData.AddData("Currency");
            return this.serviceDocument;
        }
    }
}
