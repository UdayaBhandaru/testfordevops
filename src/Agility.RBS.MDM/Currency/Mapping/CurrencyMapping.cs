﻿// <copyright file="CurrencyMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Currency.Mapping
{
    using System;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.Currency.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class CurrencyMapping : Profile
    {
        public CurrencyMapping()
            : base("CurrencyMapping")
        {
            this.CreateMap<OracleObject, CurrencyModel>()
                .ForMember(m => m.CurrencyCode, opt => opt.MapFrom(r => r["CURRENCY_CODE"]))
                .ForMember(m => m.CurrencyDesc, opt => opt.MapFrom(r => r["CURRENCY_DESC"]))
                .ForMember(m => m.CurrencyCostFmt, opt => opt.MapFrom(r => r["CURRENCY_COST_FMT"]))
                .ForMember(m => m.CurrencyRtlFmt, opt => opt.MapFrom(r => r["CURRENCY_RTL_FMT"]))
                .ForMember(m => m.CurrencyCostDec, opt => opt.MapFrom(r => r["CURRENCY_COST_DEC"]))
                .ForMember(m => m.CurrencyRtlDec, opt => opt.MapFrom(r => r["CURRENCY_RTL_DEC"]))
                .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "CURRENCIES"))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, CurrencyDetailModel>()
            .ForMember(m => m.CurrencyCode, opt => opt.MapFrom(r => r["currency_code"]))
            .ForMember(m => m.CurrencyDesc, opt => opt.MapFrom(r => r["currency_desc"]))
            .ForMember(m => m.EffectiveDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["effective_date"])))
            .ForMember(m => m.ExchangeType, opt => opt.MapFrom(r => r["exchange_type"]))
            .ForMember(m => m.ExchangeRate, opt => opt.MapFrom(r => r["exchange_rate"]))
            .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
            .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));
        }
    }
}
