﻿//-------------------------------------------------------------------------------------------------
// <copyright file="CostZoneLocModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.CostZone.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class CostZoneLocModel : ProfileEntity
    {
        public int? ZoneGroupId { get; set; }

        public int? ZoneId { get; set; }

        public int? LocationId { get; set; }

        public string LocationType { get; set; }

        public string LocationName { get; set; }

        public string Description { get; set; }

        public string CostLocStatus { get; set; }

        public string CostLocStatusDesc { get; set; }

        public string IsPrimaryInd { get; set; }

        public string Status { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }

        public string Error { get; set; }
    }
}
