﻿//-------------------------------------------------------------------------------------------------
// <copyright file="CostZoneModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// CostZoneModel
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.CostZone.Models
{
    using System.Collections.Generic;
    using Agility.Framework.Core.Profile.Entities;

    public class CostZoneModel : ProfileEntity
    {
        public CostZoneModel()
        {
            this.CostZoneLocList = new List<CostZoneLocModel>();
        }

        public int? ZoneGroupId { get; set; }

        public int? ZoneId { get; set; }

        public string ZoneGroupDesc { get; set; }

        public int? CostZonelocationId { get; set; }

        public string Description { get; set; }

        public string CurrencyCode { get; set; }

        public string BaseCostInd { get; set; }

        public string CostZoneStatus { get; set; }

        public string CostZoneStatusDesc { get; set; }

        public int LangId { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Error { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }

        public List<CostZoneLocModel> CostZoneLocList { get; private set; }
    }
}
