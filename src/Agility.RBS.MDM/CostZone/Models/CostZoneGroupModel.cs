﻿// <copyright file="CostZoneGroupModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// CostZoneGroup
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.CostZone.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class CostZoneGroupModel : ProfileEntity
    {
        public int? ZoneGroupId { get; set; }

        public string CostLevel { get; set; }

        public string CostLevelDesc { get; set; }

        public string Description { get; set; }

        public int LangId { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Error { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }
    }
}
