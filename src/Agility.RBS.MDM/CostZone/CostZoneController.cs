﻿//-------------------------------------------------------------------------------------------------
// <copyright file="CostZoneController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// CostZoneController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.CostZone
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.CostZone.Models;
    using Agility.RBS.MDM.CostZone.Repositories;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class CostZoneController : Controller
    {
        private readonly ServiceDocument<CostZoneModel> serviceDocument;
        private readonly CostZoneRepository costZoneRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly DomainDataRepository domainDataRepository;

        public CostZoneController(
            ServiceDocument<CostZoneModel> serviceDocument,
              CostZoneRepository costZoneRepository,
            DomainDataRepository domainDataRepository)
        {
            this.costZoneRepository = costZoneRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<CostZoneModel>> List()
        {
            this.LoadDomainData();
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<CostZoneModel>> New()
        {
            this.LoadDomainData();
            this.serviceDocument.LocalizationData.AddData("CostZone");
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<CostZoneModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.CostZoneSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<CostZoneModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.CostZoneSave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingCostZone(int zoneGroupId)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "COST_ZONE";
            nvc["P_COL1"] = zoneGroupId.ToString();
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        private async Task<List<CostZoneModel>> CostZoneSearch()
        {
            try
            {
                var lstUom = await this.costZoneRepository.GetCostZone(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstUom;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> CostZoneSave()
        {
            this.serviceDocument.Result = await this.costZoneRepository.SetCostZone(this.serviceDocument.DataProfile.DataModel);
            return true;
        }

        private void LoadDomainData()
        {
            this.serviceDocument.DomainData.Add("CostZoneGroup", this.domainDataRepository.CostZoneGroupDomainGet().Result);
            this.serviceDocument.DomainData.Add("currency", this.domainDataRepository.CurrencyDomainGet().Result);
            this.serviceDocument.DomainData.Add("indicatorStatus", this.domainDataRepository.DomainDetailData(MdmConstants.VatRegionTypeDomainData).Result);
            this.serviceDocument.DomainData.Add("location", this.domainDataRepository.LocationDomainGet().Result);
            this.serviceDocument.DomainData.Add("locType", this.domainDataRepository.LocationTypeDomainGet().Result);
            this.serviceDocument.DomainData.Add("locationsType", this.domainDataRepository.DomainDetailData(MdmConstants.DomainHdrLocType).Result);
        }
    }
}