﻿//-------------------------------------------------------------------------------------------------
// <copyright file="CostZoneGroupController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// CostZoneGroupController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.CostZone
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.CostZone.Models;
    using Agility.RBS.MDM.CostZone.Repositories;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class CostZoneGroupController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocument<CostZoneGroupModel> serviceDocument;
        private readonly CostZoneRepository costZoneRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;

        public CostZoneGroupController(
            ServiceDocument<CostZoneGroupModel> serviceDocument,
            CostZoneRepository costZoneRepository,
            DomainDataRepository domainDataRepository)
        {
            this.costZoneRepository = costZoneRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<CostZoneGroupModel>> List()
        {
            this.serviceDocument.DomainData.Add("costLevel", this.domainDataRepository.CostLevelGet().Result);

            return this.serviceDocument;
        }

        public async Task<ServiceDocument<CostZoneGroupModel>> New()
        {
            this.serviceDocument.DomainData.Add("costLevel", this.domainDataRepository.CostLevelGet().Result);
            this.serviceDocument.LocalizationData.AddData("CostZoneGroup");
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<CostZoneGroupModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.CostZoneGroupSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<CostZoneGroupModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.CostZoneGroupSave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingCostZoneGroup(string description)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "COST_ZONE_GROUP";
            nvc["P_COL1"] = description;
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        private async Task<List<CostZoneGroupModel>> CostZoneGroupSearch()
        {
            try
            {
                var lstCostZoneGroup = await this.costZoneRepository.GetCostZoneGroup(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstCostZoneGroup;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> CostZoneGroupSave()
        {
            this.serviceDocument.Result = await this.costZoneRepository.SetCostZoneGroup(this.serviceDocument.DataProfile.DataModel);
            return true;
        }
    }
}
