﻿//-------------------------------------------------------------------------------------------------
// <copyright file="CostChangeReasonController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// CostChangeReasonController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.CostZone
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.CostZone.Models;
    using Agility.RBS.MDM.CostZone.Repositories;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class CostChangeReasonController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocument<CostChangeReasonModel> serviceDocument;
        private readonly CostZoneRepository costZoneRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;

        public CostChangeReasonController(
            ServiceDocument<CostChangeReasonModel> serviceDocument,
            CostZoneRepository costZoneRepository,
            DomainDataRepository domainDataRepository)
        {
            this.costZoneRepository = costZoneRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<CostChangeReasonModel>> List()
        {
            this.serviceDocument.DomainData.Add("status", this.domainDataRepository.DomainDetailData(MdmConstants.DomainHdrStatus).Result);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<CostChangeReasonModel>> New()
        {
            this.serviceDocument.DomainData.Add("status", this.domainDataRepository.DomainDetailData(MdmConstants.DomainHdrStatus).Result);
            this.serviceDocument.LocalizationData.AddData("CostChangeReason");
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<CostChangeReasonModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.CostChangeReasonSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<CostChangeReasonModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.CostChangeReasonSave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingCostChangeReason(string reasonDesc)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "COST_CHG_REASON";
            nvc["P_COL1"] = reasonDesc;
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        private async Task<List<CostChangeReasonModel>> CostChangeReasonSearch()
        {
            try
            {
                var lstUomClass = await this.costZoneRepository.GetCostChangeReason(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstUomClass;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> CostChangeReasonSave()
        {
            this.serviceDocument.Result = await this.costZoneRepository.SetCostChangeReason(this.serviceDocument.DataProfile.DataModel);
            return true;
        }
    }
}
