﻿// <copyright file="CostZoneRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// CostZoneRepository
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.CostZone.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.CostZone.Models;
    using AutoMapper;
    using Devart.Data.Oracle;
    using Microsoft.AspNetCore.Authorization;

    public class CostZoneRepository : BaseOraclePackage
    {
        public CostZoneRepository()
        {
            this.PackageName = MdmConstants.GetCostZonePackage;
        }

        public async Task<List<CostZoneGroupModel>> GetCostZoneGroup(CostZoneGroupModel costZoneGroupModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeCostZoneGroup, MdmConstants.GetProcCostZoneGroup);
            OracleObject recordObject = this.SetParamsCostZoneGroup(costZoneGroupModel, true);
            return await this.GetProcedure2<CostZoneGroupModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ServiceDocumentResult> SetCostZoneGroup(CostZoneGroupModel costZoneGroupModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeCostZoneGroup, MdmConstants.SetProcCostZoneGroup);
            OracleObject recordObject = this.SetParamsCostZoneGroup(costZoneGroupModel, false);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async Task<List<CostZoneModel>> GetCostZone(CostZoneModel costZoneModel, ServiceDocumentResult serviceDocumentResult)
        {
            ////OracleParameterCollection parameters;
            ////parameters = this.Parameters;
            ////parameters.Clear();
            ////parameters.Add(this.ParameterBuilder(UserProfileHelper.GetUserParameter()));
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeCostZone, MdmConstants.GetProcCostZone);
            OracleObject recordObject = this.SetParamsCostZone(costZoneModel, true);
            OracleTable pGetUsersMstTab = await this.GetProcedure(recordObject, packageParameter, serviceDocumentResult);
            var lstHeader = Mapper.Map<List<CostZoneModel>>(pGetUsersMstTab);
            if (lstHeader != null)
            {
                for (int i = 0; i < lstHeader.Count; i++)
                {
                    OracleObject obj = (OracleObject)pGetUsersMstTab[i];
                    OracleTable costZonelocationDetails = (Devart.Data.Oracle.OracleTable)obj["COST_ZONE_LOC"];
                    lstHeader[i].CostZoneLocList.AddRange(Mapper.Map<List<CostZoneLocModel>>(costZonelocationDetails));
                }
            }

            return lstHeader;
        }

        public async Task<ServiceDocumentResult> SetCostZone(CostZoneModel costZoneModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeCostZone, MdmConstants.SetProcCostZone);
            OracleObject recordObject = this.SetParamsCostZone(costZoneModel, false);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async Task<List<CostChangeReasonModel>> GetCostChangeReason(CostChangeReasonModel costChangeReasonModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeCostChangeReason, MdmConstants.GetProcCostChangeReason);
            OracleObject recordObject = this.SetParamsCostChangeReason(costChangeReasonModel, true);
            return await this.GetProcedure2<CostChangeReasonModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ServiceDocumentResult> SetCostChangeReason(CostChangeReasonModel costChangeReasonModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeCostChangeReason, MdmConstants.SetProcCostChangeReason);
            OracleObject recordObject = this.SetParamsCostChangeReason(costChangeReasonModel, false);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        private OracleObject SetParamsCostZoneGroup(CostZoneGroupModel costZoneGroupModel, bool isSearch)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(MdmConstants.RecordTypeCostZoneGroup, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            if (!isSearch)
            {
                recordObject[recordType.Attributes["Operation"]] = costZoneGroupModel.Operation;
            }

            recordObject[recordType.Attributes["ZONE_GROUP_ID"]] = costZoneGroupModel.ZoneGroupId;
            recordObject[recordType.Attributes["COST_LEVEL"]] = costZoneGroupModel.CostLevel;
            recordObject[recordType.Attributes["COST_LEVEL_DESC"]] = costZoneGroupModel.CostLevelDesc;
            recordObject[recordType.Attributes["DESCRIPTION"]] = costZoneGroupModel.Description;

            return recordObject;
        }

        private OracleObject SetParamsCostChangeReason(CostChangeReasonModel costChangeReasonModel, bool isSearch)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(MdmConstants.RecordTypeCostChangeReason, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            if (!isSearch)
            {
                recordObject[recordType.Attributes["Operation"]] = costChangeReasonModel.Operation;
            }

            recordObject[recordType.Attributes["REASON"]] = costChangeReasonModel.Reason;
            recordObject[recordType.Attributes["REASON_DESC"]] = costChangeReasonModel.ReasonDesc;
            return recordObject;
        }

        private OracleTable CostZoneLocationParamsObject(CostZoneModel costZoneModel, bool isSearchForLoc)
        {
            // ZoenLocation Object
            OracleType dtlTableType = OracleType.GetObjectType(MdmConstants.CostZoneLocationTab, this.Connection);
            OracleType dtlRecordType = OracleType.GetObjectType(MdmConstants.RecordTypeCostZoneLocation, this.Connection);
            OracleTable pLocationTab = new OracleTable(dtlTableType);

            if (isSearchForLoc)
            {
                var dtlRecordObject = new OracleObject(dtlRecordType);
                dtlRecordObject[dtlRecordType.Attributes["ZONE_GROUP_ID"]] = costZoneModel.ZoneGroupId;
                dtlRecordObject[dtlRecordType.Attributes["ZONE_ID"]] = costZoneModel.ZoneId;
                dtlRecordObject[dtlRecordType.Attributes["LOCATION"]] = costZoneModel.CostZonelocationId;
                dtlRecordObject[dtlRecordType.Attributes["LOCATION_NAME"]] = string.Empty;
                dtlRecordObject[dtlRecordType.Attributes["LOC_TYPE"]] = string.Empty;
                dtlRecordObject[dtlRecordType.Attributes["Operation"]] = string.Empty;

                pLocationTab.Add(dtlRecordObject);
            }
            else
            {
                if (costZoneModel.CostZoneLocList != null && costZoneModel.CostZoneLocList.Count > 0)
                {
                    foreach (CostZoneLocModel costZoneLocModel in costZoneModel.CostZoneLocList)
                    {
                        var dtlRecordObject = new OracleObject(dtlRecordType);
                        dtlRecordObject[dtlRecordType.Attributes["ZONE_GROUP_ID"]] = costZoneModel.ZoneGroupId;
                        dtlRecordObject[dtlRecordType.Attributes["ZONE_ID"]] = costZoneModel.ZoneId;
                        dtlRecordObject[dtlRecordType.Attributes["LOCATION"]] = costZoneLocModel.LocationId;
                        dtlRecordObject[dtlRecordType.Attributes["LOC_TYPE"]] = costZoneLocModel.LocationType;
                        dtlRecordObject[dtlRecordType.Attributes["LOCATION_NAME"]] = costZoneLocModel.LocationName;
                        dtlRecordObject[dtlRecordType.Attributes["Operation"]] = costZoneLocModel.Operation;
                        pLocationTab.Add(dtlRecordObject);
                    }
                }
            }

            return pLocationTab;
        }

        private OracleObject SetParamsCostZone(CostZoneModel costZoneModel, bool isSerach)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(MdmConstants.RecordTypeCostZone, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            return this.SetGetCostZoneParamas(costZoneModel, recordType, recordObject, isSerach);
        }

        private OracleObject SetGetCostZoneParamas(CostZoneModel costZoneModel, OracleType recordType, OracleObject recordObject, bool isSerach)
        {
            if (!isSerach)
            {
                recordObject[recordType.Attributes["Operation"]] = string.Empty;
            }

            OracleTable pLocationTab = this.CostZoneLocationParamsObject(costZoneModel, isSerach);

            recordObject[recordType.Attributes["ZONE_GROUP_ID"]] = costZoneModel.ZoneGroupId;
            recordObject[recordType.Attributes["ZONE_ID"]] = costZoneModel.ZoneId;
            recordObject[recordType.Attributes["DESCRIPTION"]] = costZoneModel.Description;
            recordObject[recordType.Attributes["CURRENCY_CODE"]] = costZoneModel.CurrencyCode;
            recordObject[recordType.Attributes["BASE_COST_IND"]] = costZoneModel.BaseCostInd;
            recordObject[recordType.Attributes["COST_ZONE_LOC"]] = pLocationTab;
            recordObject[recordType.Attributes["Operation"]] = costZoneModel.Operation;
            return recordObject;
        }
    }
}