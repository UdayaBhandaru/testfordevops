﻿// <copyright file="CostZoneMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.CostZone.Mapping
{
    using System;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.CostZone.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class CostZoneMapping : Profile
    {
        public CostZoneMapping()
            : base("CostZoneMapping")
        {
            this.CreateMap<OracleObject, CostZoneGroupModel>()
                .ForMember(m => m.ZoneGroupId, opt => opt.MapFrom(r => r["ZONE_GROUP_ID"]))
                .ForMember(m => m.CostLevel, opt => opt.MapFrom(r => r["COST_LEVEL"]))
                .ForMember(m => m.CostLevelDesc, opt => opt.MapFrom(r => r["COST_LEVEL_DESC"]))
                .ForMember(m => m.Description, opt => opt.MapFrom(r => r["DESCRIPTION"]))
                .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "COST_ZONE_GROUP"))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, CostZoneModel>()
               .ForMember(m => m.ZoneGroupId, opt => opt.MapFrom(r => r["ZONE_GROUP_ID"]))
               .ForMember(m => m.ZoneId, opt => opt.MapFrom(r => r["ZONE_ID"]))
               .ForMember(m => m.Description, opt => opt.MapFrom(r => r["DESCRIPTION"]))
               .ForMember(m => m.CurrencyCode, opt => opt.MapFrom(r => r["CURRENCY_CODE"]))
               .ForMember(m => m.BaseCostInd, opt => opt.MapFrom(r => r["BASE_COST_IND"]))
               .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
               .ForMember(m => m.TableName, opt => opt.MapFrom(r => "COST_ZONE"))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, CostChangeReasonModel>()
              .ForMember(m => m.Reason, opt => opt.MapFrom(r => r["REASON"]))
              .ForMember(m => m.ReasonDesc, opt => opt.MapFrom(r => r["REASON_DESC"]))
              .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
              .ForMember(m => m.TableName, opt => opt.MapFrom(r => "COST_CHG_REASON"))
              .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, CostZoneLocModel>()
             .ForMember(m => m.ZoneGroupId, opt => opt.MapFrom(r => r["ZONE_GROUP_ID"]))
             .ForMember(m => m.ZoneId, opt => opt.MapFrom(r => r["ZONE_ID"]))
             .ForMember(m => m.LocationId, opt => opt.MapFrom(r => r["LOCATION"]))
             .ForMember(m => m.LocationName, opt => opt.MapFrom(r => r["LOCATION_NAME"]))
             .ForMember(m => m.LocationType, opt => opt.MapFrom(r => r["LOC_TYPE"]))
              .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
              .ForMember(m => m.TableName, opt => opt.MapFrom(r => "COST_ZONE_GROUP_LOC"))
              .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));
        }
    }
}
