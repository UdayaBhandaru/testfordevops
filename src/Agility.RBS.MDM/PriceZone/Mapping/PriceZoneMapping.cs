﻿// <copyright file="PriceZoneMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// PriceZoneMapping
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.PriceZone.Mapping
{
    using System;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.PriceZone.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class PriceZoneMapping : Profile
    {
        public PriceZoneMapping()
            : base("PriceZoneMapping")
        {
            this.CreateMap<OracleObject, PriceZoneGroupModel>()
                .ForMember(m => m.ZoneGroupId, opt => opt.MapFrom(r => r["ZONE_GROUP_ID"]))
                .ForMember(m => m.PriceLevel, opt => opt.MapFrom(r => r["PRICING_LEVEL"]))
                .ForMember(m => m.PriceLevelDesc, opt => opt.MapFrom(r => r["PRICING_LEVEL_DESC"]))
                .ForMember(m => m.Description, opt => opt.MapFrom(r => r["DESCRIPTION"]))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "PRICE_ZONE_GROUP"))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, PriceChangeReasonModel>()
               .ForMember(m => m.Reason, opt => opt.MapFrom(r => r["REASON"]))
               .ForMember(m => m.ReasonDesc, opt => opt.MapFrom(r => r["REASON_DESC"]))
               .ForMember(m => m.ReasonStatus, opt => opt.MapFrom(r => r["REASON_STATUS"]))
               .ForMember(m => m.ReasonStatusDesc, opt => opt.MapFrom(r => r["REASON_STATUS_DESC"]))
               .ForMember(m => m.CreatedBy, opt => opt.MapFrom(r => r["CREATED_BY"]))
               .ForMember(m => m.ModifiedBy, opt => opt.MapFrom(r => r["LAST_UPDATED_BY"]))
               .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
               .ForMember(m => m.CreatedDate, opt => opt.MapFrom(r => r["CREATED_DATE"]))
               .ForMember(m => m.ModifiedDate, opt => opt.MapFrom(r => r["LAST_UPDATED_DATE"]))
               .ForMember(m => m.TableName, opt => opt.MapFrom(r => "PRICE_CHG_REASON"))
               .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, PriceZoneLocModel>()
            .ForMember(m => m.ZoneId, opt => opt.MapFrom(r => r["ZONE_ID"]))
            .ForMember(m => m.LocationId, opt => opt.MapFrom(r => r["LOCATION"]))
            .ForMember(m => m.LocationName, opt => opt.MapFrom(r => r["LOCATION_DESC"]))
             .ForMember(m => m.ZoneLocStatus, opt => opt.MapFrom(r => r["ZONE_LOC_STATUS"]))
             .ForMember(m => m.ZoneLocStatusDesc, opt => opt.MapFrom(r => r["ZONE_LOC_STATUS_DESC"]))
             .ForMember(m => m.BaseInd, opt => opt.MapFrom(r => r["BASE_IND"]))
             .ForMember(m => m.IsPrimaryInd, opt => opt.MapFrom(r => r["IS_PRIMARY_IND"]))
             .ForMember(m => m.LocType, opt => opt.MapFrom(r => r["LOC_TYPE"]))
             .ForMember(m => m.CreatedBy, opt => opt.MapFrom(r => r["CREATED_BY"]))
             .ForMember(m => m.ModifiedBy, opt => opt.MapFrom(r => r["LAST_UPDATED_BY"]))
             .ForMember(m => m.CreatedDate, opt => opt.MapFrom(r => r["CREATED_DATE"]))
             .ForMember(m => m.ModifiedDate, opt => opt.MapFrom(r => r["LAST_UPDATED_DATE"]))
             .ForMember(m => m.TableName, opt => opt.MapFrom(r => "PRICE_ZONE_LOCATION"))
             .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, PriceZoneModel>()
              .ForMember(m => m.ZoneId, opt => opt.MapFrom(r => r["ZONE_ID"]))
              .ForMember(m => m.ZoneGroupName, opt => opt.MapFrom(r => r["ZONE_GROUP_NAME"]))
              .ForMember(m => m.ZoneGroupDispId, opt => opt.MapFrom(r => r["ZONE_DISPLAY_ID"]))
              .ForMember(m => m.ZoneGroupId, opt => opt.MapFrom(r => r["ZONE_GROUP_ID"]))
              .ForMember(m => m.Name, opt => opt.MapFrom(r => r["NAME"]))
              .ForMember(m => m.CurrencyCode, opt => opt.MapFrom(r => r["CURRENCY_CODE"]))
              .ForMember(m => m.BaseCostInd, opt => opt.MapFrom(r => r["BASE_IND"]))
              .ForMember(m => m.PriceZoneStatus, opt => opt.MapFrom(r => r["ZONE_STATUS"]))
              .ForMember(m => m.PriceZoneStatusDesc, opt => opt.MapFrom(r => r["ZONE_STATUS_DESC"]))

              .ForMember(m => m.CreatedBy, opt => opt.MapFrom(r => r["CREATED_BY"]))
              .ForMember(m => m.ModifiedBy, opt => opt.MapFrom(r => r["LAST_UPDATED_BY"]))
              .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
              .ForMember(m => m.CreatedDate, opt => opt.MapFrom(r => r["CREATED_DATE"]))
              .ForMember(m => m.ModifiedDate, opt => opt.MapFrom(r => r["LAST_UPDATED_DATE"]))
              .ForMember(m => m.TableName, opt => opt.MapFrom(r => "PRICE_ZONE"))
               .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));
        }
    }
}
