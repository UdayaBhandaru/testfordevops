﻿// <copyright file="PriceZoneRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// PriceZoneRepository
//-------------------------------------------------------------------------------------------------
namespace Agility.RBS.MDM.PriceZone.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.PriceZone.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class PriceZoneRepository : BaseOraclePackage
    {
        public PriceZoneRepository()
        {
            this.PackageName = MdmConstants.GetPriceZonePackage;
        }

        public async Task<List<PriceZoneGroupModel>> GetPriceZoneGroup(PriceZoneGroupModel priceZoneGroupModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypePriceZoneGroup, MdmConstants.GetProcPriceZoneGroup);
            OracleObject recordObject = this.SetParamsPriceZoneGroup(priceZoneGroupModel, true);
            OracleTable pPriceZoneGroupMstTab = await this.GetProcedure(recordObject, packageParameter, serviceDocumentResult);
            return Mapper.Map<List<PriceZoneGroupModel>>(pPriceZoneGroupMstTab);
        }

        public async Task<ServiceDocumentResult> SetPriceZoneGroup(PriceZoneGroupModel priceZoneGroupModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypePriceZoneGroup, MdmConstants.SetProcPriceZoneGroup);
            OracleObject recordObject = this.SetParamsPriceZoneGroup(priceZoneGroupModel, false);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async Task<List<PriceChangeReasonModel>> GetPriceChangeReason(PriceChangeReasonModel priceChangeReasonModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypePriceChangeReason, MdmConstants.GetProcPriceChangeReason);
            OracleObject recordObject = this.SetParamsPriceChangeReason(priceChangeReasonModel, true);
            OracleTable pPriceZoneGroupMstTab = await this.GetProcedure(recordObject, packageParameter, serviceDocumentResult);
            return Mapper.Map<List<PriceChangeReasonModel>>(pPriceZoneGroupMstTab);
        }

        public async Task<ServiceDocumentResult> SetPriceChangeReason(PriceChangeReasonModel priceChangeReasonModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypePriceChangeReason, MdmConstants.SetProcPriceChangeReason);
            OracleObject recordObject = this.SetParamsPriceChangeReason(priceChangeReasonModel, false);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async Task<List<PriceZoneModel>> GetPriceZone(PriceZoneModel priceZoneModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypePriceZone, MdmConstants.GetProcPriceZone);
            OracleObject recordObject = this.SetParamsPriceZone(priceZoneModel, true);
            OracleTable pGetUsersMstTab = await this.GetProcedure(recordObject, packageParameter, serviceDocumentResult);
            var lstHeader = Mapper.Map<List<PriceZoneModel>>(pGetUsersMstTab);
            if (lstHeader != null)
            {
                for (int i = 0; i < lstHeader.Count; i++)
                {
                    OracleObject obj = (OracleObject)pGetUsersMstTab[i];
                    OracleTable priceZonelocationDetails = (Devart.Data.Oracle.OracleTable)obj["PRICE_ZONE_LOC"];
                    lstHeader[i].PriceZoneLocList.AddRange(Mapper.Map<List<PriceZoneLocModel>>(priceZonelocationDetails));
                }
            }

            return lstHeader;
        }

        public async Task<ServiceDocumentResult> SetPriceZone(PriceZoneModel priceZoneModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypePriceZone, MdmConstants.SetProcPriceZone);
            OracleObject recordObject = this.SetParamsPriceZone(priceZoneModel, false);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        private OracleObject SetParamsPriceZoneGroup(PriceZoneGroupModel priceZoneGroupModel, bool isSearch)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(MdmConstants.RecordTypePriceZoneGroup, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            if (!isSearch)
            {
                recordObject[recordType.Attributes["Operation"]] = priceZoneGroupModel.Operation;
            }

            recordObject[recordType.Attributes["ZONE_GROUP_ID"]] = priceZoneGroupModel.ZoneGroupId;
            recordObject[recordType.Attributes["PRICING_LEVEL"]] = priceZoneGroupModel.PriceLevel;
            recordObject[recordType.Attributes["PRICING_LEVEL_DESC"]] = priceZoneGroupModel.PriceLevelDesc;
            recordObject[recordType.Attributes["DESCRIPTION"]] = priceZoneGroupModel.Description;

            return recordObject;
        }

        private OracleObject SetParamsPriceChangeReason(PriceChangeReasonModel priceChangeReasonModel, bool isSearch)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(MdmConstants.RecordTypePriceChangeReason, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            if (isSearch)
            {
                var reason = priceChangeReasonModel.Reason ?? 0;
                bool checkCondition = (reason == 0) && string.IsNullOrEmpty(priceChangeReasonModel.ReasonStatus) &&
                    string.IsNullOrEmpty(priceChangeReasonModel.ReasonDesc);

                if (checkCondition)
                {
                    recordObject[recordType.Attributes["REASON"]] = "-1";
                    return recordObject;
                }
            }
            else
            {
                recordObject[recordType.Attributes["Operation"]] = priceChangeReasonModel.Operation;
                recordObject[recordType.Attributes["CREATED_BY"]] = this.GetCreatedBy(priceChangeReasonModel.CreatedBy);
                recordObject[recordType.Attributes["LAST_UPDATED_BY"]] = FxContext.Context.Name;
            }

            recordObject[recordType.Attributes["REASON"]] = priceChangeReasonModel.Reason;
            recordObject[recordType.Attributes["REASON_DESC"]] = priceChangeReasonModel.ReasonDesc;
            recordObject[recordType.Attributes["REASON_STATUS"]] = priceChangeReasonModel.ReasonStatus;
            return recordObject;
        }

        private OracleTable PriceZoneLocationParamsObject(PriceZoneModel priceZoneModel, bool isSearchForLoc)
        {
            // ZoenLocation Object
            OracleType dtlTableType = OracleType.GetObjectType(MdmConstants.PriceZoneLocationTab, this.Connection);
            OracleType dtlRecordType = OracleType.GetObjectType(MdmConstants.RecordTypePriceZoneLocation, this.Connection);
            OracleTable pLocationTab = new OracleTable(dtlTableType);
            if (isSearchForLoc)
            {
                var dtlRecordObject = new OracleObject(dtlRecordType);
                dtlRecordObject[dtlRecordType.Attributes["ZONE_ID"]] = priceZoneModel.ZoneId;
                dtlRecordObject[dtlRecordType.Attributes["LOCATION"]] = priceZoneModel.PriceZonelocationId;
                dtlRecordObject[dtlRecordType.Attributes["LOC_TYPE"]] = string.Empty;
                dtlRecordObject[dtlRecordType.Attributes["BASE_IND"]] = string.Empty;
                dtlRecordObject[dtlRecordType.Attributes["ZONE_LOC_STATUS"]] = string.Empty;
                dtlRecordObject[dtlRecordType.Attributes["IS_PRIMARY_IND"]] = string.Empty;
                dtlRecordObject[dtlRecordType.Attributes["CREATED_BY"]] = this.GetCreatedBy(priceZoneModel.CreatedBy);
                dtlRecordObject[dtlRecordType.Attributes["LAST_UPDATED_BY"]] = FxContext.Context.Name;
                dtlRecordObject[dtlRecordType.Attributes["Operation"]] = string.Empty;
                pLocationTab.Add(dtlRecordObject);
            }
            else
            {
                if (priceZoneModel.PriceZoneLocList != null && priceZoneModel.PriceZoneLocList.Count > 0)
                {
                    foreach (PriceZoneLocModel priceZoneLocModel in priceZoneModel.PriceZoneLocList)
                    {
                        var dtlRecordObject = new OracleObject(dtlRecordType);
                        dtlRecordObject[dtlRecordType.Attributes["ZONE_ID"]] = priceZoneModel.ZoneId;
                        dtlRecordObject[dtlRecordType.Attributes["LOCATION"]] = priceZoneLocModel.LocationId;
                        dtlRecordObject[dtlRecordType.Attributes["LOC_TYPE"]] = priceZoneLocModel.LocType;
                        dtlRecordObject[dtlRecordType.Attributes["BASE_IND"]] = priceZoneLocModel.BaseInd;
                        dtlRecordObject[dtlRecordType.Attributes["ZONE_LOC_STATUS"]] = priceZoneLocModel.ZoneLocStatus;
                        dtlRecordObject[dtlRecordType.Attributes["IS_PRIMARY_IND"]] = priceZoneLocModel.IsPrimaryInd;
                        dtlRecordObject[dtlRecordType.Attributes["CREATED_BY"]] = this.GetCreatedBy(priceZoneModel.CreatedBy);
                        dtlRecordObject[dtlRecordType.Attributes["LAST_UPDATED_BY"]] = FxContext.Context.Name;
                        dtlRecordObject[dtlRecordType.Attributes["Operation"]] = priceZoneLocModel.Operation;
                        pLocationTab.Add(dtlRecordObject);
                    }
                }
            }

                return pLocationTab;
        }

        private OracleObject SetParamsPriceZone(PriceZoneModel priceZoneModel, bool isSerach)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(MdmConstants.RecordTypePriceZone, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            return this.SetGetPriceZoneParamas(priceZoneModel, recordType, recordObject, isSerach);
        }

        private OracleObject SetGetPriceZoneParamas(PriceZoneModel priceZoneModel, OracleType recordType, OracleObject recordObject, bool isSerach)
        {
            if (isSerach)
            {
                recordObject[recordType.Attributes["CREATED_BY"]] = null;
                recordObject[recordType.Attributes["LAST_UPDATED_BY"]] = null;
                recordObject[recordType.Attributes["Operation"]] = null;
                recordObject[recordType.Attributes["ZONE_STATUS"]] = null;
            }
            else
            {
                recordObject[recordType.Attributes["CREATED_BY"]] = FxContext.Context.Name;
                recordObject[recordType.Attributes["LAST_UPDATED_BY"]] = FxContext.Context.Name;
                recordObject[recordType.Attributes["Operation"]] = priceZoneModel.Operation;
                recordObject[recordType.Attributes["ZONE_STATUS"]] = priceZoneModel.PriceZoneStatus;
            }

            OracleTable pLocationTab = this.PriceZoneLocationParamsObject(priceZoneModel, isSerach);
            recordObject[recordType.Attributes["PRICE_ZONE_LOC"]] = pLocationTab;

            recordObject[recordType.Attributes["ZONE_ID"]] = priceZoneModel.ZoneId;
            recordObject[recordType.Attributes["ZONE_GROUP_ID"]] = priceZoneModel.ZoneGroupId;
            recordObject[recordType.Attributes["ZONE_DISPLAY_ID"]] = priceZoneModel.ZoneGroupDispId;
            recordObject[recordType.Attributes["NAME"]] = priceZoneModel.Name;
            recordObject[recordType.Attributes["CURRENCY_CODE"]] = priceZoneModel.CurrencyCode;
            recordObject[recordType.Attributes["BASE_IND"]] = priceZoneModel.BaseCostInd;
            recordObject[recordType.Attributes["ZONE_STATUS"]] = priceZoneModel.PriceZoneStatus;

            return recordObject;
        }
    }
}