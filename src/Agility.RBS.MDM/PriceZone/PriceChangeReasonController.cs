﻿//-------------------------------------------------------------------------------------------------
// <copyright file="PriceChangeReasonController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// PriceChangeReasonController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.PriceZone
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.PriceZone.Models;
    using Agility.RBS.MDM.PriceZone.Repositories;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class PriceChangeReasonController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocument<PriceChangeReasonModel> serviceDocument;
        private readonly PriceZoneRepository priceZoneRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;

        public PriceChangeReasonController(
            ServiceDocument<PriceChangeReasonModel> serviceDocument,
            PriceZoneRepository priceZoneRepository,
            DomainDataRepository domainDataRepository)
        {
            this.priceZoneRepository = priceZoneRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<PriceChangeReasonModel>> List()
        {
            this.serviceDocument.DomainData.Add("status", this.domainDataRepository.DomainDetailData(MdmConstants.DomainHdrStatus).Result);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<PriceChangeReasonModel>> New()
        {
            this.serviceDocument.DomainData.Add("status", this.domainDataRepository.DomainDetailData(MdmConstants.DomainHdrStatus).Result);
            this.serviceDocument.LocalizationData.AddData("PriceChangeReason");
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<PriceChangeReasonModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.PriceChangeReasonSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<PriceChangeReasonModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.PriceChangeReasonSave);
            return this.serviceDocument;
        }

        private async Task<List<PriceChangeReasonModel>> PriceChangeReasonSearch()
        {
            try
            {
                var lstUomClass = await this.priceZoneRepository.GetPriceChangeReason(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstUomClass;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> PriceChangeReasonSave()
        {
            this.serviceDocument.Result = await this.priceZoneRepository.SetPriceChangeReason(this.serviceDocument.DataProfile.DataModel);
            return true;
        }
    }
}
