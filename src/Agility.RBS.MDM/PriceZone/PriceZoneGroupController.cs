﻿//-------------------------------------------------------------------------------------------------
// <copyright file="PriceZoneGroupController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// PriceZoneGroupController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.PriceZone
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.PriceZone.Models;
    using Agility.RBS.MDM.PriceZone.Repositories;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class PriceZoneGroupController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocument<PriceZoneGroupModel> serviceDocument;
        private readonly PriceZoneRepository priceZoneRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;

        public PriceZoneGroupController(
            ServiceDocument<PriceZoneGroupModel> serviceDocument,
            PriceZoneRepository priceZoneRepository,
            DomainDataRepository domainDataRepository)
        {
            this.priceZoneRepository = priceZoneRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<PriceZoneGroupModel>> List()
        {
            this.serviceDocument.DomainData.Add("priceLevel", this.domainDataRepository.PriceLevelGet().Result);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<PriceZoneGroupModel>> New()
        {
            this.serviceDocument.DomainData.Add("priceLevel", this.domainDataRepository.PriceLevelGet().Result);
            this.serviceDocument.LocalizationData.AddData("PriceZoneGroup");
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<PriceZoneGroupModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.PriceZoneGroupSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<PriceZoneGroupModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.PriceZoneGroupSave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingPriceZoneGroup(int zoneGroupDispId)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "PRICE_ZONE_GROUP";
            nvc["P_COL1"] = zoneGroupDispId.ToString();
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        private async Task<List<PriceZoneGroupModel>> PriceZoneGroupSearch()
        {
            try
            {
                var lstUomClass = await this.priceZoneRepository.GetPriceZoneGroup(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstUomClass;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> PriceZoneGroupSave()
        {
            this.serviceDocument.Result = await this.priceZoneRepository.SetPriceZoneGroup(this.serviceDocument.DataProfile.DataModel);
            return true;
        }
    }
}
