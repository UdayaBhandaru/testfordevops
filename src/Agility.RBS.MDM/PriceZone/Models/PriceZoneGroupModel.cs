﻿// <copyright file="PriceZoneGroupModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.PriceZone.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class PriceZoneGroupModel : ProfileEntity
    {
        public int? ZoneGroupId { get; set; }

        public string Description { get; set; }

        public string PriceLevel { get; set; }

        public string PriceLevelDesc { get; set; }

        public int LangId { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Error { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }
    }
}
