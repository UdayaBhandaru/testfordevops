﻿//-------------------------------------------------------------------------------------------------
// <copyright file="PriceZoneModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// PriceZoneModel
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.PriceZone.Models
{
    using System.Collections.Generic;
    using Agility.Framework.Core.Profile.Entities;

    public class PriceZoneModel : ProfileEntity
    {
        public PriceZoneModel()
        {
            this.PriceZoneLocList = new List<PriceZoneLocModel>();
            this.LocationRemovedData = new List<PriceZoneLocModel>();
        }

        public int? ZoneGroupId { get; set; }

        public string ZoneGroupName { get; set; }

        public int? ZoneId { get; set; }

        public int? ZoneGroupDispId { get; set; }

        public int? PriceZonelocationId { get; set; }

        public string LocType { get; set; }

        public string Name { get; set; }

        public string CurrencyCode { get; set; }

        public string BaseCostInd { get; set; }

        public string PriceZoneStatus { get; set; }

        public string PriceZoneStatusDesc { get; set; }

        public int LangId { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Error { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }

        public List<PriceZoneLocModel> PriceZoneLocList { get; private set; }

        public List<PriceZoneLocModel> LocationRemovedData { get; private set; }
    }
}
