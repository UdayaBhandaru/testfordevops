﻿// <copyright file="PriceChangeReasonModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.PriceZone.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class PriceChangeReasonModel : ProfileEntity
    {
        public int? Reason { get; set; }

        public string ReasonDesc { get; set; }

        public string ReasonStatus { get; set; }

        public string ReasonStatusDesc { get; set; }

        public int LangId { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Error { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }
    }
}
