﻿//-------------------------------------------------------------------------------------------------
// <copyright file="PriceZoneLocModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// PriceZoneLocModel
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.PriceZone.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class PriceZoneLocModel : ProfileEntity
    {
        public PriceZoneLocModel()
        {
            this.Operation = "I";
        }

        public int? ZoneId { get; set; }

        public int? LocationId { get; set; }

        public string LocationName { get; set; }

        public string LocType { get; set; }

        public string BaseInd { get; set; }

        public string ZoneLocStatus { get; set; }

        public string ZoneLocStatusDesc { get; set; }

        public string IsPrimaryInd { get; set; }

        public int LangId { get; set; }

        public string Status { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }

        public string Error { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }
    }
}
