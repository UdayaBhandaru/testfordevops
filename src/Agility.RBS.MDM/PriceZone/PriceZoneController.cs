﻿//-------------------------------------------------------------------------------------------------
// <copyright file="PriceZoneController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// PriceZoneController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.PriceZone
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.PriceZone.Models;
    using Agility.RBS.MDM.PriceZone.Repositories;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class PriceZoneController : Controller
    {
        private readonly ServiceDocument<PriceZoneModel> serviceDocument;
        private readonly PriceZoneRepository priceZoneRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly DomainDataRepository domainDataRepository;

        public PriceZoneController(
            ServiceDocument<PriceZoneModel> serviceDocument,
              PriceZoneRepository priceZoneRepository,
            DomainDataRepository domainDataRepository)
        {
            this.priceZoneRepository = priceZoneRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<PriceZoneModel>> List()
        {
            this.LoadDomainData();
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<PriceZoneModel>> New()
        {
            this.LoadDomainData();
            this.serviceDocument.LocalizationData.AddData("PriceZone");
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<PriceZoneModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.PriceZoneSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<PriceZoneModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.PriceZoneSave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingPriceZone(int zoneGroupId)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "PRICE_ZONE";
            nvc["P_COL1"] = zoneGroupId.ToString();
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        private async Task<List<PriceZoneModel>> PriceZoneSearch()
        {
            try
            {
                var lstUom = await this.priceZoneRepository.GetPriceZone(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstUom;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> PriceZoneSave()
        {
            this.serviceDocument.Result = await this.priceZoneRepository.SetPriceZone(this.serviceDocument.DataProfile.DataModel);
            return true;
        }

        private void LoadDomainData()
        {
            this.serviceDocument.DomainData.Add("locationType", this.domainDataRepository.DomainDetailData(MdmConstants.DomainHdrLocationType).Result);
            this.serviceDocument.DomainData.Add("currency", this.domainDataRepository.CurrencyDomainGet().Result);
            this.serviceDocument.DomainData.Add("indicator", this.domainDataRepository.DomainDetailData(MdmConstants.DomainHdrIndicator).Result);
            this.serviceDocument.DomainData.Add("location", this.domainDataRepository.LocationDomainGet().Result);
            this.serviceDocument.DomainData.Add("status", this.domainDataRepository.DomainDetailData(MdmConstants.DomainHdrStatus).Result);
            this.serviceDocument.DomainData.Add("priceZonegroup", this.domainDataRepository.CommonData("PZGROUP").Result);
        }
    }
}