﻿//-------------------------------------------------------------------------------------------------
// <copyright file="CurrencyExchangeTypeController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// CurrencyExchangeTypeController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.CurrencyExchangeType
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.CurrencyExchangeType.Models;
    using Agility.RBS.MDM.CurrencyExchangeType.Repositories;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    [Route("api/[controller]/[action]")]

    public class CurrencyExchangeTypeController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocument<CurrencyExchangeTypeModel> serviceDocument;
        private readonly CurrencyExchangeTypeRepository currencyExchangeTypeRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;

        public CurrencyExchangeTypeController(
            ServiceDocument<CurrencyExchangeTypeModel> serviceDocument,
           CurrencyExchangeTypeRepository currencyExchangeTypeRepository,
            DomainDataRepository domainDataRepository)
        {
            this.currencyExchangeTypeRepository = currencyExchangeTypeRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<CurrencyExchangeTypeModel>> List()
        {
            this.serviceDocument.DomainData.Add("status", this.domainDataRepository.DomainDetailData(MdmConstants.DomainHdrStatus).Result);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<CurrencyExchangeTypeModel>> New()
        {
            this.serviceDocument.DomainData.Add("status", this.domainDataRepository.DomainDetailData(MdmConstants.DomainHdrStatus).Result);
            this.serviceDocument.LocalizationData.AddData("CurrencyExchangeType");
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<CurrencyExchangeTypeModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.CurrencyExchangeTypeSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<CurrencyExchangeTypeModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.CurrencyExchangeTypeSave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingCurrencyExchangeType(string fifExchangeType)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "FIF_CURRENCY_XREF";
            nvc["P_COL1"] = fifExchangeType;
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        private async Task<List<CurrencyExchangeTypeModel>> CurrencyExchangeTypeSearch()
        {
            try
            {
                var lstUomClass = await this.currencyExchangeTypeRepository.GetCurrencyExchangeType(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstUomClass;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> CurrencyExchangeTypeSave()
        {
            this.serviceDocument.Result = await this.currencyExchangeTypeRepository.SetCurrencyExchangeType(this.serviceDocument.DataProfile.DataModel);
            return true;
        }
    }
}
