﻿// <copyright file="CurrencyExchangeTypeModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// CostChangeReason
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.CurrencyExchangeType.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class CurrencyExchangeTypeModel : ProfileEntity
    {
        public string FifExchangeType { get; set; }

        public string RmsExchangeType { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Error { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }
    }
}
