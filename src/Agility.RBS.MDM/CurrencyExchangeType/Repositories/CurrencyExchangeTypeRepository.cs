﻿// <copyright file="CurrencyExchangeTypeRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// CostZoneRepository
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.CurrencyExchangeType.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.CurrencyExchangeType.Models;
    using AutoMapper;
    using Devart.Data.Oracle;
    using Microsoft.AspNetCore.Authorization;

    public class CurrencyExchangeTypeRepository : BaseOraclePackage
    {
        public CurrencyExchangeTypeRepository()
        {
            this.PackageName = MdmConstants.GetCurrencyExchangeTypePackage;
        }

        public async Task<List<CurrencyExchangeTypeModel>> GetCurrencyExchangeType(CurrencyExchangeTypeModel currencyExchangeTypeModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeCurrencyExchangeType, MdmConstants.GetProcCurrencyExchangeType);
            OracleObject recordObject = this.SetParamsCurrencyExchangeType(currencyExchangeTypeModel, true);
            return await this.GetProcedure2<CurrencyExchangeTypeModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ServiceDocumentResult> SetCurrencyExchangeType(CurrencyExchangeTypeModel currencyExchangeTypeModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeCurrencyExchangeType, MdmConstants.SetProcCurrencyExchangeType);
            OracleObject recordObject = this.SetParamsCurrencyExchangeType(currencyExchangeTypeModel, false);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        private OracleObject SetParamsCurrencyExchangeType(CurrencyExchangeTypeModel currencyExchangeTypeModel, bool isSearch)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(MdmConstants.RecordTypeCurrencyExchangeType, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            if (!isSearch)
            {
                recordObject[recordType.Attributes["Operation"]] = currencyExchangeTypeModel.Operation;
            }

            recordObject[recordType.Attributes["FIF_EXCHANGE_TYPE"]] = currencyExchangeTypeModel.FifExchangeType;
            recordObject[recordType.Attributes["RMS_EXCHANGE_TYPE"]] = currencyExchangeTypeModel.RmsExchangeType;
            return recordObject;
        }
    }
}