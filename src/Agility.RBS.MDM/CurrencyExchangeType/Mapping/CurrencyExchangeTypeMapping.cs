﻿// <copyright file="CurrencyExchangeTypeMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.CurrencyExchangeType.Mapping
{
    using System;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.CurrencyExchangeType.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class CurrencyExchangeTypeMapping : Profile
    {
        public CurrencyExchangeTypeMapping()
            : base("CurrencyExchangeTypeMapping")
        {
            this.CreateMap<OracleObject, CurrencyExchangeTypeModel>()
              .ForMember(m => m.FifExchangeType, opt => opt.MapFrom(r => r["FIF_EXCHANGE_TYPE"]))
              .ForMember(m => m.RmsExchangeType, opt => opt.MapFrom(r => r["RMS_EXCHANGE_TYPE"]))
              .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
              .ForMember(m => m.TableName, opt => opt.MapFrom(r => "FIF_CURRENCY_XREF"))
              .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));
        }
    }
}
