﻿// <copyright file="StoreFormatModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// CostChangeReason
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.StoreFormat.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class StoreFormatModel : ProfileEntity
    {
        public int? StoreFormat { get; set; }

        public string FormatName { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Error { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }
    }
}
