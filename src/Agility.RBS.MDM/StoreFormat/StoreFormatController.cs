﻿//-------------------------------------------------------------------------------------------------
// <copyright file="StoreFormatController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// StoreFormatController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.StoreFormat
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.StoreFormat.Models;
    using Agility.RBS.MDM.StoreFormat.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    [Route("api/[controller]/[action]")]

    public class StoreFormatController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocument<StoreFormatModel> serviceDocument;
        private readonly StoreFormatRepository storeFormatRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;

        public StoreFormatController(
            ServiceDocument<StoreFormatModel> serviceDocument,
           StoreFormatRepository storeFormatRepository,
            DomainDataRepository domainDataRepository)
        {
            this.storeFormatRepository = storeFormatRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<StoreFormatModel>> List()
        {
            this.serviceDocument.DomainData.Add("status", this.domainDataRepository.DomainDetailData(MdmConstants.DomainHdrStatus).Result);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<StoreFormatModel>> New()
        {
            this.serviceDocument.DomainData.Add("status", this.domainDataRepository.DomainDetailData(MdmConstants.DomainHdrStatus).Result);
            this.serviceDocument.LocalizationData.AddData("StoreFormat");
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<StoreFormatModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.StoreFormatSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<StoreFormatModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.StoreFormatSave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingStoreFormat(int storeFormat)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "STORE_FORMAT";
            nvc["P_COL1"] = storeFormat.ToString();
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        private async Task<List<StoreFormatModel>> StoreFormatSearch()
        {
            try
            {
                var lstUomClass = await this.storeFormatRepository.GetStoreFormat(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstUomClass;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> StoreFormatSave()
        {
            this.serviceDocument.Result = await this.storeFormatRepository.SetStoreFormat(this.serviceDocument.DataProfile.DataModel);
            return true;
        }
    }
}
