﻿// <copyright file="StoreFormatRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// CostZoneRepository
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.StoreFormat.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.StoreFormat.Models;
    using AutoMapper;
    using Devart.Data.Oracle;
    using Microsoft.AspNetCore.Authorization;

    public class StoreFormatRepository : BaseOraclePackage
    {
        public StoreFormatRepository()
        {
            this.PackageName = MdmConstants.GetStoreFormatPackage;
        }

        public async Task<List<StoreFormatModel>> GetStoreFormat(StoreFormatModel storeFormatModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeStoreFormat, MdmConstants.GetProcStoreFormat);
            OracleObject recordObject = this.SetParamsStoreFormat(storeFormatModel, true);
            return await this.GetProcedure2<StoreFormatModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ServiceDocumentResult> SetStoreFormat(StoreFormatModel storeFormatModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeStoreFormat, MdmConstants.SetProcStoreFormat);
            OracleObject recordObject = this.SetParamsStoreFormat(storeFormatModel, false);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        private OracleObject SetParamsStoreFormat(StoreFormatModel storeFormatModel, bool isSearch)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(MdmConstants.RecordTypeStoreFormat, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            if (!isSearch)
            {
                recordObject[recordType.Attributes["Operation"]] = storeFormatModel.Operation;
            }

            recordObject[recordType.Attributes["STORE_FORMAT"]] = storeFormatModel.StoreFormat;
            recordObject[recordType.Attributes["FORMAT_NAME"]] = storeFormatModel.FormatName;
            return recordObject;
        }
    }
}