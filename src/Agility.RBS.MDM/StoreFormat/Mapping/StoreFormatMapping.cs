﻿// <copyright file="StoreFormatMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.StoreFormat.Mapping
{
    using System;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.StoreFormat.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class StoreFormatMapping : Profile
    {
        public StoreFormatMapping()
            : base("StoreFormatMapping")
        {
            this.CreateMap<OracleObject, StoreFormatModel>()
              .ForMember(m => m.StoreFormat, opt => opt.MapFrom(r => r["STORE_FORMAT"]))
              .ForMember(m => m.FormatName, opt => opt.MapFrom(r => r["FORMAT_NAME"]))
              .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
              .ForMember(m => m.TableName, opt => opt.MapFrom(r => "STORE_FORMAT"))
              .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));
        }
    }
}
