﻿// <copyright file="TransferZoneController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Transfer
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Text;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Transfer.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    public class TransferZoneController : Controller
    {
        private readonly ServiceDocument<TransferZoneModel> serviceDocument;
        private readonly TransferRepository transferRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly DomainDataRepository domainDataRepository;
        
                public TransferZoneController(
            ServiceDocument<TransferZoneModel> serviceDocument,
            TransferRepository transferRepository,
            DomainDataRepository domainDataRepository)
        {
            this.transferRepository = transferRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<TransferZoneModel>> List()
        {
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<TransferZoneModel>> New()
        {
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<TransferZoneModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.TransferZoneSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<TransferZoneModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.TransferZoneSave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingTransferZone(int tsfZone, string tsfZoneDesc)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "TSFZONE";
            nvc["P_COL1"] = tsfZone.ToString();
            nvc["P_COL2"] = tsfZoneDesc;
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<List<TransferEntityLocationsModel>> GetAvialbleLocationsData(string tsfZoneTypeParam)
        {
            return await this.transferRepository.GetAvialbleLocationsData(tsfZoneTypeParam, this.serviceDocumentResult);
        }

        private async Task<bool> TransferZoneSave()
        {
            this.serviceDocument.Result = await this.transferRepository.SetTransferZone(this.serviceDocument.DataProfile.DataModel);
            return true;
        }

        private async Task<List<TransferZoneModel>> TransferZoneSearch()
        {
            try
            {
                var lstTsfZone = await this.transferRepository.GetTransferZoneAsync(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstTsfZone;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }
    }
}
