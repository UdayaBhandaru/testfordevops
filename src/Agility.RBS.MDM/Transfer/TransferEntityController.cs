﻿// <copyright file="TransferEntityController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Transfer
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Transfer.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    public class TransferEntityController : Controller
    {
        private readonly ServiceDocument<TransferEntityModel> serviceDocument;
        private readonly TransferRepository transferRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly DomainDataRepository domainDataRepository;

        public TransferEntityController(
            ServiceDocument<TransferEntityModel> serviceDocument,
            TransferRepository transferRepository,
            DomainDataRepository domainDataRepository)
        {
            this.transferRepository = transferRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<TransferEntityModel>> List()
        {
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<TransferEntityModel>> New()
        {
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<TransferEntityModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.TransferEntitySearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<TransferEntityModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.TransferEntitySave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingTransferEntity(int tsfEntityId, string tsfEntityDesc)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "TSF_ENTITY";
            nvc["P_COL1"] = tsfEntityId.ToString();
            nvc["P_COL2"] = tsfEntityDesc;
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<List<TransferEntityLocationsModel>> GetAvialbleLocationsData(string tsfEntityTypeParam)
        {
            return await this.transferRepository.GetAvialbleLocationsData(tsfEntityTypeParam, this.serviceDocumentResult);
        }

        private async Task<List<TransferEntityModel>> TransferEntitySearch()
        {
            try
            {
                var lstTsfEntity = await this.transferRepository.GetTransferEntityAsync(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstTsfEntity;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> TransferEntitySave()
        {
            this.serviceDocument.Result = await this.transferRepository.SetTransferEntity(this.serviceDocument.DataProfile.DataModel);
            return true;
        }
    }
}
