﻿// <copyright file="TransferZoneModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Transfer.Models
{
    using System;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.Transfer.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class TransferZoneModel : ProfileEntity
    {
        public int? TsfZone { get; set; }

        public string TsfZoneDesc { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Error { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }
    }
}
