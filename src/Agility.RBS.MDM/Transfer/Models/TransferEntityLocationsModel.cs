﻿// <copyright file="TransferEntityLocationsModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Transfer.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class TransferEntityLocationsModel : ProfileEntity
    {
        public int? LocationId { get; set; }

        public string LocName { get; set; }

        public string LocType { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Error { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }
    }
}
