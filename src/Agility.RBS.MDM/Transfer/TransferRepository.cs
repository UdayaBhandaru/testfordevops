﻿// <copyright file="TransferRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Transfer
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Transfer.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class TransferRepository : BaseOraclePackage
    {
        public TransferRepository()
        {
            this.PackageName = MdmConstants.GetTransferEntityPackage;
        }

        public async Task<List<TransferEntityModel>> GetTransferEntityAsync(TransferEntityModel transferModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeTransferEntity, MdmConstants.GetProcTransferEntity);
            OracleObject recordObject = this.SetParamsTransfer(transferModel);
            return await this.GetProcedure2<TransferEntityModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ServiceDocumentResult> SetTransferEntity(TransferEntityModel transferModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeTransferEntity, MdmConstants.SetProcTransferEntity);
            OracleObject recordObject = this.SetParamsTransfer(transferModel);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async Task<ServiceDocumentResult> SetTransferZone(TransferZoneModel tsfzoneModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeTsfZone, MdmConstants.SetProcTsfZone);
            OracleObject recordObject = this.SetParamsTransfer(tsfzoneModel);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async Task<List<TransferZoneModel>> GetTransferZoneAsync(TransferZoneModel tsfzoneModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeTsfZone, MdmConstants.GetProcTsfZone);
            OracleObject recordObject = this.SetParamsTransfer(tsfzoneModel);
            return await this.GetProcedure2<TransferZoneModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<List<TransferEntityLocationsModel>> GetAvialbleLocationsData(string tsfEntityTypeParam, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            BaseOracleParameter baseOracleParameter;
            baseOracleParameter = new BaseOracleParameter { Name = "p_tsfentity", DataType = OracleDbType.VarChar, Direction = ParameterDirection.Input, Value = tsfEntityTypeParam };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));
            baseOracleParameter = new BaseOracleParameter { Name = "p_tsfzone", DataType = OracleDbType.VarChar, Direction = ParameterDirection.Input, Value = string.Empty };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));
            OracleType recordType = this.GetObjectType("LOCATION_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams("LOCATION_TAB", "GETTSFLOCATION");
            return await this.GetProcedure2<TransferEntityLocationsModel>(recordObject, packageParameter, serviceDocumentResult, parameters);
        }

        private OracleObject SetParamsTransfer(TransferZoneModel tsfzoneModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("TSFZONE_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            recordObject["TRANSFER_ZONE"] = tsfzoneModel?.TsfZone;
            recordObject["DESCRIPTION"] = tsfzoneModel?.TsfZoneDesc;
            recordObject["Operation"] = tsfzoneModel?.Operation;
            return recordObject;
        }

        private OracleObject SetParamsTransfer(TransferEntityModel transferModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("TSF_ENTITY_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            recordObject["TSF_ENTITY_ID"] = transferModel?.TsfEntityId;
            recordObject["TSF_ENTITY_DESC"] = transferModel?.TsfEntityDesc;
            recordObject["Operation"] = transferModel?.Operation;
            return recordObject;
        }
    }
}
