﻿// <copyright file="TransferMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.Transfer.Mapping
{
    using System;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.Transfer.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class TransferMapping : Profile
    {
        public TransferMapping()
            : base("TransferMapping")
        {
            this.CreateMap<OracleObject, TransferEntityModel>()
                .ForMember(m => m.TsfEntityId, opt => opt.MapFrom(r => r["TSF_ENTITY_ID"]))
                .ForMember(m => m.TsfEntityDesc, opt => opt.MapFrom(r => r["TSF_ENTITY_DESC"]))
                .ForMember(m => m.ProgramPhase, opt => opt.MapFrom(r => r["PROGRAM_PHASE"]))
                .ForMember(m => m.ProgramMessage, opt => opt.MapFrom(r => r["PROGRAM_MESSAGE"]))
                .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_ERROR"]))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "TRANSFER_ENTITY"))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, TransferZoneModel>()
                .ForMember(m => m.TsfZone, opt => opt.MapFrom(r => r["TRANSFER_ZONE"]))
                .ForMember(m => m.TsfZoneDesc, opt => opt.MapFrom(r => r["DESCRIPTION"]))
                .ForMember(m => m.ProgramPhase, opt => opt.MapFrom(r => r["PROGRAM_PHASE"]))
                .ForMember(m => m.ProgramMessage, opt => opt.MapFrom(r => r["PROGRAM_MESSAGE"]))
                .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_ERROR"]))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "TRANSFER_ZONE"))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, TransferEntityLocationsModel>()
                .ForMember(m => m.LocationId, opt => opt.MapFrom(r => r["LOCATION_ID"]))
                .ForMember(m => m.LocName, opt => opt.MapFrom(r => r["LOC_NAME"]))
                .ForMember(m => m.LocType, opt => opt.MapFrom(r => r["LOC_TYPE"]))
                 .ForMember(m => m.ProgramPhase, opt => opt.MapFrom(r => r["PROGRAM_PHASE"]))
                .ForMember(m => m.ProgramMessage, opt => opt.MapFrom(r => r["PROGRAM_MESSAGE"]))
                .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_ERROR"]));
        }
    }
}
