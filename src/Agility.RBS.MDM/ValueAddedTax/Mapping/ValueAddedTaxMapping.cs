﻿// <copyright file="ValueAddedTaxMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.ValueAddedTax.Mapping
{
    using System;
    using Agility.RBS.MDM.ValueAddedTax.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class ValueAddedTaxMapping : Profile
    {
        public ValueAddedTaxMapping()
            : base("ValueAddedTaxMapping")
        {
            this.CreateMap<OracleObject, VatRegionModel>()
                .ForMember(m => m.VatRegion, opt => opt.MapFrom(r => r["VAT_REGION"]))
                .ForMember(m => m.VatRegionName, opt => opt.MapFrom(r => r["VAT_REGION_NAME"]))
                .ForMember(m => m.VatRegionType, opt => opt.MapFrom(r => r["VAT_REGION_TYPE"]))
                .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "VAT_REGION_MST"))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, VatRateModel>()
             .ForMember(m => m.VatCode, opt => opt.MapFrom(r => r["VAT_CODE"]))
             .ForMember(m => m.ActiveDate, opt => opt.MapFrom(r => r["ACTIVE_DATE"]))
             .ForMember(m => m.CreateDate, opt => opt.MapFrom(r => r["CREATE_DATE"]))
             .ForMember(m => m.CreateId, opt => opt.MapFrom(r => r["CREATE_ID"]))
             .ForMember(m => m.VatRate, opt => opt.MapFrom(r => r["VAT_RATE"]))
             .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
             .ForMember(m => m.TableName, opt => opt.MapFrom(r => "VAT_CODES_RATES_MST"))
             .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, VatCodeModel>()
               .ForMember(m => m.VatCode, opt => opt.MapFrom(r => r["VAT_CODE"]))
               .ForMember(m => m.VatCodeDesc, opt => opt.MapFrom(r => r["VAT_CODE_DESC"]))
               .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, VatCodeVatRegionModel>()
               .ForMember(m => m.VatCode, opt => opt.MapFrom(r => r["VAT_CODE"]))
               .ForMember(m => m.VatRegionCode, opt => opt.MapFrom(r => r["VAT_REGION_CODE"]))
               .ForMember(m => m.VatStatus, opt => opt.MapFrom(r => r["VAT_STATUS"]))
               .ForMember(m => m.VatStatusDesc, opt => opt.MapFrom(r => r["VAT_STATUS_DESC"]))
               .ForMember(m => m.VatRegion, opt => opt.MapFrom(r => r["VAT_REGION"]))
               .ForMember(m => m.CreatedBy, opt => opt.MapFrom(r => r["CREATED_BY"]))
               .ForMember(m => m.ModifiedBy, opt => opt.MapFrom(r => r["LAST_UPDATED_BY"]))
               .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
               .ForMember(m => m.CreatedDate, opt => opt.MapFrom(r => r["CREATED_DATE"]))
               .ForMember(m => m.ModifiedDate, opt => opt.MapFrom(r => r["LAST_UPDATED_DATE"]))
               .ForMember(m => m.TableName, opt => opt.MapFrom(r => "VAT_REGION_VAT_CODE_MST"))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));
        }
    }
}
