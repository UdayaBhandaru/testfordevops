﻿//-------------------------------------------------------------------------------------------------
// <copyright file="VatRegionController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// VatRegionController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.ValueAddedTax
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.ValueAddedTax.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class VatRegionController : Controller
    {
        private readonly ServiceDocument<VatRegionModel> serviceDocument;
        private readonly ValueAddedTaxRepository vatRepository;
        private readonly DomainDataRepository domainDataRepository;

        public VatRegionController(
            ServiceDocument<VatRegionModel> serviceDocument,
            ValueAddedTaxRepository vatRepository,
            DomainDataRepository domainDataRepository)
        {
            this.vatRepository = vatRepository;
            this.serviceDocument = serviceDocument;
            this.domainDataRepository = domainDataRepository;
        }

        public async Task<ServiceDocument<VatRegionModel>> List()
        {
            return await this.VatRegionDomainData();
        }

        public async Task<ServiceDocument<VatRegionModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.VatRegionSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<VatRegionModel>> New()
        {
            return await this.VatRegionDomainData();
        }

        public async Task<ServiceDocument<VatRegionModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.VatRegionSave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingVatRegionCode(string vatRegionName)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "VAT_REGION_MST";
            nvc["P_COL1"] = vatRegionName;
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        private async Task<List<VatRegionModel>> VatRegionSearch()
        {
            try
            {
                ServiceDocumentResult serviceDocumentResult = new ServiceDocumentResult
                {
                    InnerException = string.Empty,
                    StackTrace = string.Empty,
                    Type = MessageType.Success
                };
                var vatRegion = await this.vatRepository.GetVatRegion(this.serviceDocument.DataProfile.DataModel, serviceDocumentResult);
                this.serviceDocument.Result = serviceDocumentResult;
                return vatRegion;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> VatRegionSave()
        {
            this.serviceDocument.Result = await this.vatRepository.SetVatRegion(this.serviceDocument.DataProfile.DataModel);
            return true;
        }

        private async Task<ServiceDocument<VatRegionModel>> VatRegionDomainData()
        {
            this.serviceDocument.DomainData.Add("vatRegionType", this.domainDataRepository.VatRegionTypeGet().Result);
            this.serviceDocument.DomainData.Add("VatRegion", this.domainDataRepository.VatRegionDomainGet().Result);
            this.serviceDocument.DomainData.Add("status", this.domainDataRepository.DomainDetailData(MdmConstants.DomainHdrStatus).Result);
            this.serviceDocument.LocalizationData.AddData("VatRegion");
            return this.serviceDocument;
        }
    }
}
