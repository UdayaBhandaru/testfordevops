﻿//-------------------------------------------------------------------------------------------------
// <copyright file="VatCodeController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// VatCodeController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.ValueAddedTax
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.ValueAddedTax.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class VatCodeController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocument<VatCodeModel> serviceDocument;
        private readonly ValueAddedTaxRepository vatRepository;

        public VatCodeController(ServiceDocument<VatCodeModel> serviceDocument, ValueAddedTaxRepository vatRepository, DomainDataRepository domainDataRepository)
        {
            this.vatRepository = vatRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
        }

        public async Task<ServiceDocument<VatCodeModel>> List()
        {
            return await this.VatCodeDomainData();
        }

        public async Task<ServiceDocument<VatCodeModel>> New()
        {
            return await this.VatCodeDomainData();
        }

        public async Task<ServiceDocument<VatCodeModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.VatCodeSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<VatCodeModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.VatCodeSave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingVatCode(string vatCode)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "VAT_CODE_MST";
            nvc["P_COL1"] = vatCode;
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        private async Task<List<VatCodeModel>> VatCodeSearch()
        {
            try
            {
                ServiceDocumentResult serviceDocumentResult = new ServiceDocumentResult
                {
                    InnerException = string.Empty,
                    StackTrace = string.Empty,
                    Type = MessageType.Success
                };
                var vatCodes = await this.vatRepository.GetVatCodes(this.serviceDocument.DataProfile.DataModel, serviceDocumentResult);
                this.serviceDocument.Result = serviceDocumentResult;
                return vatCodes;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> VatCodeSave()
        {
            this.serviceDocument.Result = await this.vatRepository.SetVatCodes(this.serviceDocument.DataProfile.DataModel);
            return true;
        }

        private async Task<ServiceDocument<VatCodeModel>> VatCodeDomainData()
        {
            this.serviceDocument.LocalizationData.AddData("VatCode");
            return this.serviceDocument;
        }
    }
}
