﻿//-------------------------------------------------------------------------------------------------
// <copyright file="VatRegionVatCodeController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// VatRegionVatCodeController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.ValueAddedTax
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.ValueAddedTax.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class VatRegionVatCodeController : Controller
    {
        private readonly ServiceDocument<VatCodeVatRegionModel> serviceDocument;
        private readonly ValueAddedTaxRepository vatRepository;
        private readonly DomainDataRepository domainDataRepository;

        public VatRegionVatCodeController(
            ServiceDocument<VatCodeVatRegionModel> serviceDocument,
            ValueAddedTaxRepository vatRepository,
            DomainDataRepository domainDataRepository)
        {
            this.vatRepository = vatRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<VatCodeVatRegionModel>> List()
        {
            return await this.VatCodeVatRegionDomainData();
        }

        [HttpPost]
        public async Task<ServiceDocument<VatCodeVatRegionModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.VatCodeVatRegionSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<VatCodeVatRegionModel>> New()
        {
            return await this.VatCodeVatRegionDomainData();
        }

        public async Task<ServiceDocument<VatCodeVatRegionModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.VatCodeVatRegionSave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingVatRegionVatCode(string vatCode, string vatRegionCode)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "VAT_REGION_VAT_CODE_MST";
            nvc["P_COL1"] = vatCode;
            nvc["P_COL2"] = vatRegionCode;
            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        private async Task<List<VatCodeVatRegionModel>> VatCodeVatRegionSearch()
        {
            try
            {
                ServiceDocumentResult serviceDocumentResult = new ServiceDocumentResult
                {
                    InnerException = string.Empty,
                    StackTrace = string.Empty,
                    Type = MessageType.Success
                };
                var vatRegionVatCodes = await this.vatRepository.GetVatRegionVatCode(this.serviceDocument.DataProfile.DataModel, serviceDocumentResult);
                this.serviceDocument.Result = serviceDocumentResult;
                return vatRegionVatCodes;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> VatCodeVatRegionSave()
        {
            this.serviceDocument.Result = await this.vatRepository.SetVatRegionVatCode(this.serviceDocument.DataProfile.DataModel);
            return true;
        }

        private async Task<ServiceDocument<VatCodeVatRegionModel>> VatCodeVatRegionDomainData()
        {
            this.serviceDocument.DomainData.Add("vatCode", this.domainDataRepository.VatCodeDomainGet().Result);
            this.serviceDocument.DomainData.Add("vatRegionCode", this.domainDataRepository.VatRegionDomainGet().Result);
            this.serviceDocument.DomainData.Add("status", this.domainDataRepository.DomainDetailData(MdmConstants.DomainHdrStatus).Result);
            this.serviceDocument.LocalizationData.AddData("VatRegionVatCode");
            return this.serviceDocument;
        }
    }
}
