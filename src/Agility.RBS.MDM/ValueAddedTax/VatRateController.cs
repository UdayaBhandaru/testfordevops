﻿//-------------------------------------------------------------------------------------------------
// <copyright file="VatRateController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// VatRateController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.ValueAddedTax
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.ValueAddedTax.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class VatRateController : Controller
    {
        private readonly ServiceDocument<VatRateModel> serviceDocument;
        private readonly ValueAddedTaxRepository vatRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;

        public VatRateController(ServiceDocument<VatRateModel> serviceDocument, ValueAddedTaxRepository vatRepository, DomainDataRepository domainDataRepository)
        {
            this.vatRepository = vatRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<VatRateModel>> List()
        {
            return await this.VatRateDomainData();
        }

        public async Task<ServiceDocument<VatRateModel>> New()
        {
            return await this.VatRateDomainData();
        }

        public async Task<ServiceDocument<VatRateModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.VatRateSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<VatRateModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.VatRateSave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public DateTime? MaxEffectDate(string pVatCode)
        {
            try
            {
                return this.vatRepository.MaxEffectDateGet(pVatCode);
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return new DateTime(0);
            }
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingVatRate(string vatCode, string vatRate)
        {
            return await this.vatRepository.IsExistingVatRate(vatCode, vatRate);
        }

        private async Task<List<VatRateModel>> VatRateSearch()
        {
            try
            {
                var vatRates = await this.vatRepository.GetVatRates(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return vatRates;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> VatRateSave()
        {
            this.serviceDocument.Result = await this.vatRepository.SetVatRates(this.serviceDocument.DataProfile.DataModel);
            return true;
        }

        private async Task<ServiceDocument<VatRateModel>> VatRateDomainData()
        {
            VatCodeModel vatCodeModel = new VatCodeModel();
            this.serviceDocument.DomainData.Add("vatCode", await this.vatRepository.GetVatCodes(vatCodeModel, this.serviceDocumentResult));
            this.serviceDocument.LocalizationData.AddData("VatRates");
            return this.serviceDocument;
        }
    }
}
