﻿// <copyright file="ValueAddedTaxRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.ValueAddedTax
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Data;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.ValueAddedTax.Models;
    using AutoMapper;
    using Devart.Data.Oracle;
    using Microsoft.AspNetCore.Authorization;

    public class ValueAddedTaxRepository : BaseOraclePackage
    {
        private readonly DomainDataRepository domainDataRepository;

        public ValueAddedTaxRepository(DomainDataRepository domainDataRepository)
        {
            this.PackageName = MdmConstants.GetVATPackage;
            this.domainDataRepository = domainDataRepository;
        }

        public async Task<List<VatCodeModel>> GetVatCodes(VatCodeModel vatCodeModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeVatCode, MdmConstants.GetProcVatCode);
            OracleObject recordObject = this.SetParamsVatCodes(vatCodeModel, true);
            return await this.GetProcedure2<VatCodeModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ServiceDocumentResult> SetVatCodes(VatCodeModel vatCodeModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeVatCode, MdmConstants.SetProcVatCode);
            OracleObject recordObject = this.SetParamsVatCodes(vatCodeModel, false);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async Task<List<VatRegionModel>> GetVatRegion(VatRegionModel vatRegionModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeVatRegion, MdmConstants.GetProcVatRegion);
            OracleObject recordObject = this.SetParamsVatRegion(vatRegionModel, true);
            return await this.GetProcedure2<VatRegionModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ServiceDocumentResult> SetVatRegion(VatRegionModel vatRegionModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeVatRegion, MdmConstants.SetProcVatRegion);
            OracleObject recordObject = this.SetParamsVatRegion(vatRegionModel, false);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async Task<List<VatRateModel>> GetVatRates(VatRateModel vatRateModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeVatRate, MdmConstants.GetProcVatRate);
            OracleObject recordObject = this.SetParamsVatRate(vatRateModel, true);
            return await this.GetProcedure2<VatRateModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ServiceDocumentResult> SetVatRates(VatRateModel vatRateModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeVatRate, MdmConstants.SetProcVatRate);
            OracleObject recordObject = this.SetParamsVatRate(vatRateModel, false);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async Task<List<VatCodeVatRegionModel>> GetVatRegionVatCode(VatCodeVatRegionModel vatCodeVatRegionModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeVatRegionVatCode, MdmConstants.GetProcVatRegionVatCode);
            OracleObject recordObject = this.SetParamsVatRegionVatCodes(vatCodeVatRegionModel, true);
            return await this.GetProcedure2<VatCodeVatRegionModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ServiceDocumentResult> SetVatRegionVatCode(VatCodeVatRegionModel vatCodeVatRegionModel)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeVatRegionVatCode, MdmConstants.SetProcVatRegionVatCode);
            OracleObject recordObject = this.SetParamsVatRegionVatCodes(vatCodeVatRegionModel, false);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        [AllowAnonymous]
        public async ValueTask<bool> IsExistingVatRate(string vaCode, string varRate)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["P_TAB"] = "VAT_CODE_RATES_MST";
            nvc["P_COL1"] = vaCode;
            nvc["P_COL2"] = varRate;

            return await Task.Run(() =>
            {
                return this.domainDataRepository.GetDuplicateCheck(nvc);
            });
        }

        public DateTime? MaxEffectDateGet(string pVatCode)
        {
            try
            {
                this.Connection.Open();
                OracleParameterCollection parameters = this.Parameters;
                parameters.Clear();
                BaseOracleParameter baseOracleParameter;
                baseOracleParameter = new BaseOracleParameter { Name = "RESULT", DataType = OracleDbType.Date, Direction = ParameterDirection.ReturnValue };
                parameters.Add(this.ParameterBuilder(baseOracleParameter));
                baseOracleParameter = new BaseOracleParameter { Name = MdmConstants.VatCode, DataType = OracleDbType.VarChar, Direction = ParameterDirection.Input, Value = pVatCode };
                parameters.Add(this.ParameterBuilder(baseOracleParameter));
                this.ExecuteProcedure(MdmConstants.GetProcMaxEffDate, parameters);
                return (this.Parameters["Result"].Value == DBNull.Value) ? default(DateTime?) : (DateTime)this.Parameters["Result"].Value;
            }
            finally
            {
                this.Connection.Close();
            }
        }

        public OracleObject SetParamsVatCodes(VatCodeModel vatCodeModel, bool isSearch)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("VAT_CODE_MST_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            if (!isSearch)
            {
                this.SetParamsVatCodeSave(vatCodeModel, recordObject);
            }

            this.SetParamVatCodeSearch(vatCodeModel, recordObject);
            return recordObject;
        }

        public virtual void SetParamsVatCodeSave(VatCodeModel vatCodeModel, OracleObject recordObject)
        {
            recordObject["Operation"] = vatCodeModel.Operation;
        }

        public virtual void SetParamVatCodeSearch(VatCodeModel vatCodeModel, OracleObject recordObject)
        {
            recordObject["VAT_CODE"] = vatCodeModel.VatCode;
            recordObject["VAT_CODE_DESC"] = vatCodeModel.VatCodeDesc;
        }

        public async Task<List<VatCodeModel>> VatCodeGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("VAT_CODE_MST_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeVatCode, MdmConstants.GetProcVatCode);
            return await this.GetProcedure2<VatCodeModel>(recordObject, packageParameter, null);
        }

        private OracleObject SetParamsVatRegion(VatRegionModel vatRegionModel, bool isSearch)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType("VAT_REGION_MST_REC", this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            if (!isSearch)
            {
                recordObject[recordType.Attributes["Operation"]] = vatRegionModel.Operation;
            }

            recordObject[recordType.Attributes["VAT_REGION"]] = vatRegionModel.VatRegion;
            recordObject[recordType.Attributes["VAT_REGION_NAME"]] = vatRegionModel.VatRegionName;
            recordObject[recordType.Attributes["VAT_REGION_TYPE"]] = vatRegionModel.VatRegionType;
            return recordObject;
        }

        private OracleObject SetParamsVatRegionVatCodes(VatCodeVatRegionModel vatCodeVatRegionModel, bool isSearch)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType("VAT_REGION_VAT_CODE_MST_REC", this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            if (!isSearch)
            {
                recordObject[recordType.Attributes["Operation"]] = vatCodeVatRegionModel.Operation;
                recordObject[recordType.Attributes["CREATED_BY"]] = this.GetCreatedBy(vatCodeVatRegionModel.CreatedBy);
                recordObject[recordType.Attributes["LAST_UPDATED_BY"]] = FxContext.Context.Name;
            }

            recordObject[recordType.Attributes["VAT_CODE"]] = vatCodeVatRegionModel.VatCode;
            recordObject[recordType.Attributes["VAT_REGION_CODE"]] = vatCodeVatRegionModel.VatRegionCode;
            recordObject[recordType.Attributes["VAT_STATUS"]] = vatCodeVatRegionModel.VatStatus;
            return recordObject;
        }

        private OracleObject SetParamsVatRate(VatRateModel vatRateModel, bool isSearch)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType("VAT_RATES_MST_REC", this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            if (!isSearch)
            {
                recordObject[recordType.Attributes["Operation"]] = vatRateModel.Operation;
            }

            recordObject[recordType.Attributes["VAT_CODE"]] = vatRateModel.VatCode;
            recordObject[recordType.Attributes["ACTIVE_DATE"]] = vatRateModel.ActiveDate;
            recordObject[recordType.Attributes["CREATE_DATE"]] = vatRateModel.CreateDate;
            recordObject[recordType.Attributes["VAT_RATE"]] = vatRateModel.VatRate;
            recordObject[recordType.Attributes["CREATE_ID"]] = vatRateModel.CreateId;
            return recordObject;
        }
    }
}
