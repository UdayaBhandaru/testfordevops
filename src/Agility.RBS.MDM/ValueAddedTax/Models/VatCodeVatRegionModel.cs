﻿// <copyright file="VatCodeVatRegionModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// VatCodeVatRegionModel
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.ValueAddedTax.Models
{
    using System;
    using Agility.Framework.Core.Profile.Entities;

    public class VatCodeVatRegionModel : ProfileEntity
    {
        public string VatCode { get; set; }

        public string VatRegionCode { get; set; }

        public string VatStatus { get; set; }

        public string VatStatusDesc { get; set; }

        public string VatRegion { get; set; }

        public int LangId { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Error { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }
    }
}
