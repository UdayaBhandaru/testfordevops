﻿// <copyright file="VatCodeModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.ValueAddedTax.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class VatCodeModel : ProfileEntity
    {
        public string VatCode { get; set; }

        public string VatCodeDesc { get; set; }

        public string PriceVatInclusive { get; set; }

        public string PriceVatInclusiveDesc { get; set; }

        public string VatRateOn { get; set; }

        public string VatRateOnDesc { get; set; }

        public string VatStatus { get; set; }

        public string VatStatusDesc { get; set; }

        public int LangId { get; set; }

        public string Operation { get; set; }
    }
}
