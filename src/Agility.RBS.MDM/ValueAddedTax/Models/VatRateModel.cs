﻿// <copyright file="VatRateModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// VatRateModel
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.ValueAddedTax.Models
{
    using System;
    using Agility.Framework.Core.Profile.Entities;

    public class VatRateModel : ProfileEntity
    {
        public string VatCode { get; set; }

        public DateTime? ActiveDate { get; set; }

        public DateTime? CreateDate { get; set; }

        public string CreateId { get; set; }

        public DateTime? EffectiveStartDate { get; set; }

        public DateTime? EffectiveEndDate { get; set; }

        public decimal? VatRate { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }

        public string Error { get; set; }
    }
}
