﻿//-------------------------------------------------------------------------------------------------
// <copyright file="VatRegionModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// VatRegion
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.ValueAddedTax.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class VatRegionModel : ProfileEntity
    {
        public string VatRegionCode { get; set; }

        public int? VatRegion { get; set; }

        public string VatRegionName { get; set; }

        public string VatRegionType { get; set; }

        public string VatRegionStatus { get; set; }

        public string VatRegionStatusDesc { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }

        public string Error { get; set; }
    }
}
