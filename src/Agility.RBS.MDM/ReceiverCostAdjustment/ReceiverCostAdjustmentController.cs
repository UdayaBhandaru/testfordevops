﻿//-------------------------------------------------------------------------------------------------
// <copyright file="ReceiverCostAdjustmentController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// ReceiverCostAdjustmentController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.ReceiverCostAdjustment
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Models;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;

    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.DsdMinMax.Models;
    using Agility.RBS.MDM.ReceiverCostAdjustment.Models;
    using Agility.RBS.MDM.ReceiverCostAdjustment.Repositories;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    [Route("api/[controller]/[action]")]

    public class ReceiverCostAdjustmentController : Controller
    {
        private readonly ServiceDocument<ReceiverCostAdjustmentModel> serviceDocument;
        private readonly ReceiverCostAdjustmentRepository receiverCostAdjustmentRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;

        public ReceiverCostAdjustmentController(
            ServiceDocument<ReceiverCostAdjustmentModel> serviceDocument,
              ReceiverCostAdjustmentRepository receiverCostAdjustmentRepository,
              DomainDataRepository domainDataRepository)
        {
            this.receiverCostAdjustmentRepository = receiverCostAdjustmentRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<ReceiverCostAdjustmentModel>> List()
        {
            ////await this.LoadDomainData();
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<ReceiverCostAdjustmentModel>> New()
        {
           //// await this.LoadDomainData();
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<ReceiverCostAdjustmentModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.ReceiverCostAdjustmentSearch);
            return this.serviceDocument;
        }

        public async Task<List<ReceiverCostAdjustmentModel>> GetItemLocations(int orderNo)
        {
            return await this.receiverCostAdjustmentRepository.GetReceiverCostAdjustment(new ReceiverCostAdjustmentModel { OrderNo = orderNo }, this.serviceDocumentResult);
        }

        public async Task<ServiceDocument<ReceiverCostAdjustmentModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.ReceiverCostAdjustmentSave);
            return this.serviceDocument;
        }

        [HttpGet]
        public async Task<List<SupplerDetailsModel>> GetItemAtteibutes(int orderNo)
        {
            return await this.receiverCostAdjustmentRepository.GetRecSuppDetails(new SupplerDetailsModel { OrderNo = orderNo }, this.serviceDocumentResult);
        }

        private async Task<List<ReceiverCostAdjustmentModel>> ReceiverCostAdjustmentSearch()
        {
            try
            {
                var lstUom = await this.receiverCostAdjustmentRepository.GetReceiverCostAdjustment(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstUom;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> ReceiverCostAdjustmentSave()
        {
            this.serviceDocument.Result = await this.receiverCostAdjustmentRepository.SetReceiverCostAdjustment(this.serviceDocument.DataProfile.DataModel);
            return true;
        }

        ////private async Task LoadDomainData()
        ////{
        ////     this.serviceDocument.DomainData.Add("location", this.GetItemLocations("100053104").Result);
        ////}
    }
}