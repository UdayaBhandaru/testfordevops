﻿// <copyright file="ItemDescpDetailsDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// Item List Domain
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.ReceiverCostAdjustment.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class SupplerDetailsModel
    {
        public int? OrderNo { get; set; }

        public int? Supplier { get; set; }

        public string SupName { get; set; }

        public string SuppDesription
        {
            get
            {
                return this.SupName + "(" + this.Supplier + ")";
            }
        }

        public string Status { get; set; }

        public string StatusDesc { get; set; }

        public int? ExchangeRate { get; set; }

        public string CurrencyCode { get; set; }

        public string Program_Phase { get; set; }

        public string Program_Message { get; set; }

        public string Error { get; set; }
    }
}
