﻿//-------------------------------------------------------------------------------------------------
// <copyright file="ReceiverCostAdjustmentModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// CostZoneModel
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.ReceiverCostAdjustment.Models
{
    using System.Collections.Generic;
    using Agility.Framework.Core.Profile.Entities;

    public class ReceiverCostAdjustmentModel : ProfileEntity
    {
        public ReceiverCostAdjustmentModel()
        {
            this.ReceiverCostAdjustmentLocList = new List<ReceiverCostAdjustmentLocModel>();
        }

        public int? OrderNo { get; set; }

        public string Item { get; set; }

        public string ItemDesc { get; set; }

        public int? Location { get; set; }

        public string LocName { get; set; }

        public int? Category { get; set; }

        public string CategoryDesc { get; set; }

        public int? QtyOrdered { get; set; }

        public int? QtyReceived { get; set; }

        public int? UnitCost { get; set; }

        public int? NewUnitCost { get; set; }

        public int? StockOnHand { get; set; }

        public int? InTransitQty { get; set; }

        public int? PurchaseCost { get; set; }

        public int? PurchaseCostNew { get; set; }

        public int? AvCost { get; set; }

        public int? NewAvCost { get; set; }

        public int? LandedCost { get; set; }

        public int? NewLandedCost { get; set; }

        public string CurrencyCode { get; set; }

        public string SuppCostAdj { get; set; }

        public string ApplyAllLoc { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Error { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }

        public List<ReceiverCostAdjustmentLocModel> ReceiverCostAdjustmentLocList { get; private set; }
    }
}
