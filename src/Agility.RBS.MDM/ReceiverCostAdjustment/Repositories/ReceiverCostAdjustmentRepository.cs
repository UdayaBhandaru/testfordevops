﻿// <copyright file="ReceiverCostAdjustmentRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// ReceiverCostAdjustmentRepository
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.MDM.ReceiverCostAdjustment.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Core.Models;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.DsdMinMax.Models;
    using Agility.RBS.MDM.ReceiverCostAdjustment.Models;
    using AutoMapper;
    using Devart.Data.Oracle;
    using Microsoft.AspNetCore.Authorization;

    public class ReceiverCostAdjustmentRepository : BaseOraclePackage
    {
        private readonly ServiceDocumentResult serviceDocumentResult;

        public ReceiverCostAdjustmentRepository()
        {
            this.PackageName = MdmConstants.GetFinancePackage;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<List<ReceiverCostAdjustmentModel>> GetReceiverCostAdjustment(ReceiverCostAdjustmentModel receiverCostAdjustmentModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeRecvCostAdjmnt, MdmConstants.GetProcRecvCostAdjmnt);
            OracleObject recordObject = this.SetParamsReceiverCostAdjustmentSearch(receiverCostAdjustmentModel);
            return await this.GetProcedure2<ReceiverCostAdjustmentModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<List<SupplerDetailsModel>> GetRecSuppDetails(SupplerDetailsModel supplerDetailsModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeRecvSuppDetails, MdmConstants.GetProcRecvSuppliers);
            OracleObject recordObject = this.SetParamsGetOrdersReceived(supplerDetailsModel);
            var returningModel = await this.GetProcedure2<SupplerDetailsModel>(recordObject, packageParameter, this.serviceDocumentResult);
            ////var ajaxResult = new AjaxModel<List<SupplerDetailsModel>>
            ////{
            ////    Message = this.serviceDocumentResult.InnerException,
            ////    Result = AjaxResult.Success,
            ////    Model = returningModel
            ////};
            ////if (!string.IsNullOrEmpty(this.serviceDocumentResult.InnerException))
            ////{
            ////    ajaxResult.Result = AjaxResult.Exception;
            ////}
            return returningModel;
        }

        private OracleObject SetParamsGetOrdersReceived(SupplerDetailsModel supplerDetailsModel)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(MdmConstants.RecTypeRecvSuppDetails, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["order_no"]] = supplerDetailsModel.OrderNo;
            return recordObject;
        }

        public virtual void SetSearchParamsReceiverCostAdjustment(ReceiverCostAdjustmentModel receiverCostAdjustmentModel, OracleObject recordObject)
        {
            recordObject["ORDER_NO"] = receiverCostAdjustmentModel.OrderNo;
        }

        public virtual void SetSearchParamsRecvSupplier(SupplerDetailsModel supplerDetailsModel, OracleObject recordObject)
        {
            recordObject["ORDER_NO"] = supplerDetailsModel.OrderNo;
        }

        public async Task<ServiceDocumentResult> SetReceiverCostAdjustment(ReceiverCostAdjustmentModel receiverCostAdjustmentModel)
        {
            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();

            PackageParams packageParameter = this.GetPackageParams(MdmConstants.ObjTypeRecvCostAdjmnt, MdmConstants.SetProcRecvCostAdjmnt);
            OracleParameter parameter;
            parameter = new OracleParameter("P_USER", OracleDbType.VarChar)
            {
                Direction = ParameterDirection.Input,
                Value = UserProfileHelper.GetInMemoryUser().UserName
            };
            parameters.Add(parameter);
            parameter = new OracleParameter("RESULT", Devart.Data.Oracle.OracleDbType.Boolean)
            {
                Direction = System.Data.ParameterDirection.ReturnValue
            };
            parameters.Add(parameter);
            parameter = new OracleParameter(packageParameter.ObjectTypeTable, Devart.Data.Oracle.OracleDbType.Table)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                ObjectTypeName = packageParameter.ObjectTypeName,
                Value = this.SetParamsNewCost(receiverCostAdjustmentModel)
            };
            parameters.Add(parameter);
            return await this.SetProcedure(null, packageParameter, parameters);
        }

        public virtual OracleTable SetParamsNewCost(ReceiverCostAdjustmentModel receiverCostAdjustmentModel)
        {
            this.Connection.Open();
            OracleType itemComTableType = this.GetObjectType(MdmConstants.ObjTypeRecvCostAdjmnt);
            OracleType recordType = this.GetObjectType(MdmConstants.RecTypeRecvCostAdjmnt);
            OracleTable pItemCompanyTab = new OracleTable(itemComTableType);

            foreach (ReceiverCostAdjustmentLocModel recvCostLocRecord in receiverCostAdjustmentModel.ReceiverCostAdjustmentLocList)
            {
                OracleObject recordObject = new OracleObject(recordType);
                recordObject[recordType.Attributes["ORDER_NO"]] = receiverCostAdjustmentModel.OrderNo;
                recordObject[recordType.Attributes["ITEM"]] = receiverCostAdjustmentModel.Item;
                recordObject[recordType.Attributes["ITEM_DESC"]] = string.Empty;
                recordObject[recordType.Attributes["LOCATION"]] = recvCostLocRecord.Location;
                recordObject[recordType.Attributes["LOC_NAME"]] = recvCostLocRecord.LocName;
                recordObject[recordType.Attributes["DEPT"]] = receiverCostAdjustmentModel.Category;
                recordObject[recordType.Attributes["DEPT_NAME"]] = string.Empty;
                recordObject[recordType.Attributes["QTY_ORDERED"]] = recvCostLocRecord.QtyOrdered;
                recordObject[recordType.Attributes["QTY_RECEIVED"]] = recvCostLocRecord.QtyReceived;
                recordObject[recordType.Attributes["UNIT_COST"]] = recvCostLocRecord.UnitCost;
                recordObject[recordType.Attributes["NEW_UNIT_COST"]] = recvCostLocRecord.NewUnitCost;
                recordObject[recordType.Attributes["STOCK_ON_HAND"]] = recvCostLocRecord.StockOnHand;
                recordObject[recordType.Attributes["IN_TRANSIT_QTY"]] = recvCostLocRecord.InTransitQty;
                recordObject[recordType.Attributes["PURCHASE_COST"]] = recvCostLocRecord.PurchaseCost;
                recordObject[recordType.Attributes["PURCHASE_COST_NEW"]] = recvCostLocRecord.PurchaseCostNew;
                recordObject[recordType.Attributes["AV_COST"]] = recvCostLocRecord.AvCost;
                recordObject[recordType.Attributes["NEW_AV_COST"]] = recvCostLocRecord.NewAvCost;
                recordObject[recordType.Attributes["LANDED_COST"]] = recvCostLocRecord.LandedCost;
                recordObject[recordType.Attributes["NEW_LANDED_COST"]] = recvCostLocRecord.NewLandedCost;
                recordObject[recordType.Attributes["CURRENCY_CODE"]] = recvCostLocRecord.CurrencyCode;
                recordObject[recordType.Attributes["SUPP_COST_ADJ"]] = recvCostLocRecord.SuppCostAdj;
                recordObject[recordType.Attributes["APPLY_ALL_LOC"]] = recvCostLocRecord.ApplyAllLoc;
                recordObject[recordType.Attributes["Operation"]] = receiverCostAdjustmentModel.Operation;
                pItemCompanyTab.Add(recordObject);
            }

            return pItemCompanyTab;
        }

        public ItemCategeoryDetailsDomainModel GetItemAttributes(int? orderNo)
        {
            ItemCategeoryDetailsDomainModel itemDescModel;
            itemDescModel = new ItemCategeoryDetailsDomainModel();

            try
            {
                if ((orderNo ?? 0) == 0)
                {
                    throw new ArgumentNullException("orderNo", "Item can not be empty");
                }

                this.Connection.Open();
                OracleParameterCollection parameters = this.Parameters;
                parameters.Clear();
                OracleParameter oracleParameter;
                oracleParameter = new OracleParameter { ParameterName = "p_item", OracleDbType = OracleDbType.VarChar, Direction = ParameterDirection.Input, Value = orderNo };
                parameters.Add(oracleParameter);
                oracleParameter = new OracleParameter("p_catid", Devart.Data.Oracle.OracleDbType.Number)
                {
                    Direction = ParameterDirection.Output
                };
                parameters.Add(oracleParameter);

                oracleParameter = new OracleParameter("p_catdesc", Devart.Data.Oracle.OracleDbType.VarChar)
                {
                    Direction = ParameterDirection.Output
                };
                parameters.Add(oracleParameter);

                oracleParameter = new OracleParameter("RESULT", Devart.Data.Oracle.OracleDbType.Boolean)
                {
                    Direction = ParameterDirection.ReturnValue
                };
                parameters.Add(oracleParameter);

                this.ExecuteProcedure("GETITEMCAT", parameters);
                if (this.Parameters["p_catdesc"].Value == System.DBNull.Value)
                {
                    return null;
                }

                itemDescModel.CategoryDesc = this.Parameters["p_catdesc"].Value.ToString();
                itemDescModel.Category = Convert.ToInt32( this.Parameters["p_catid"].Value.ToString());

                return itemDescModel;
            }
            catch (Exception e)
            {
                return null;
            }
            finally
            {
                this.Connection.Close();
            }
        }

        private OracleObject SetParamsReceiverCostAdjustmentSearch(ReceiverCostAdjustmentModel receiverCostAdjustmentModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType(MdmConstants.RecTypeRecvCostAdjmnt);
            OracleObject recordObject = this.GetOracleObject(recordType);
            this.SetSearchParamsReceiverCostAdjustment(receiverCostAdjustmentModel, recordObject);
            return recordObject;
        }
    }
}