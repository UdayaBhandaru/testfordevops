﻿// <copyright file="ReceiverCostAdjustmentMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.ReceiverCostAdjustment.Mapping
{
    using System;
    using Agility.RBS.Core;
    using Agility.RBS.InventoryManagement.AverageCostAdjustment.Models;
    using Agility.RBS.MDM.ReceiverCostAdjustment.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class ReceiverCostAdjustmentMapping : Profile
    {
        public ReceiverCostAdjustmentMapping()
            : base("ReceiverCostAdjustmentMapping")
        {
            this.CreateMap<OracleObject, ReceiverCostAdjustmentModel>()
               .ForMember(m => m.Category, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["ORDER_NO"])))
               .ForMember(m => m.Item, opt => opt.MapFrom(r => r["ITEM"]))
               .ForMember(m => m.ItemDesc, opt => opt.MapFrom(r => r["ITEM_DESC"]))
               .ForMember(m => m.Location, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["LOCATION"])))
               .ForMember(m => m.LocName, opt => opt.MapFrom(r => r["LOC_NAME"]))
               .ForMember(m => m.Category, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DEPT"])))
               .ForMember(m => m.CategoryDesc, opt => opt.MapFrom(r => r["DEPT_NAME"]))
               .ForMember(m => m.QtyOrdered, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["QTY_ORDERED"])))
               .ForMember(m => m.QtyReceived, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["QTY_RECEIVED"])))
               .ForMember(m => m.UnitCost, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["UNIT_COST"])))
               .ForMember(m => m.NewUnitCost, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["NEW_UNIT_COST"])))
               .ForMember(m => m.StockOnHand, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["STOCK_ON_HAND"])))
               .ForMember(m => m.InTransitQty, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["IN_TRANSIT_QTY"])))
               .ForMember(m => m.PurchaseCost, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["PURCHASE_COST"])))
               .ForMember(m => m.PurchaseCostNew, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["PURCHASE_COST_NEW"])))
               .ForMember(m => m.AvCost, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["AV_COST"])))
               .ForMember(m => m.NewAvCost, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["NEW_AV_COST"])))
               .ForMember(m => m.LandedCost, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["LANDED_COST"])))
               .ForMember(m => m.NewLandedCost, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["NEW_LANDED_COST"])))
               .ForMember(m => m.CurrencyCode, opt => opt.MapFrom(r => r["CURRENCY_CODE"]))
               .ForMember(m => m.SuppCostAdj, opt => opt.MapFrom(r => r["SUPP_COST_ADJ"]))
               .ForMember(m => m.ApplyAllLoc, opt => opt.MapFrom(r => r["APPLY_ALL_LOC"]))
               .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"))
               .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]));

            this.CreateMap<OracleObject, ReceiverCostAdjustmentLocModel>()
              .ForMember(m => m.Category, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["ORDER_NO"])))
               .ForMember(m => m.Item, opt => opt.MapFrom(r => r["ITEM"]))
               .ForMember(m => m.ItemDesc, opt => opt.MapFrom(r => r["ITEM_DESC"]))
               .ForMember(m => m.Location, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["LOCATION"])))
               .ForMember(m => m.LocName, opt => opt.MapFrom(r => r["LOC_NAME"]))
               .ForMember(m => m.Category, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DEPT"])))
               .ForMember(m => m.CategoryDesc, opt => opt.MapFrom(r => r["DEPT_NAME"]))
               .ForMember(m => m.QtyOrdered, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["QTY_ORDERED"])))
               .ForMember(m => m.QtyReceived, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["QTY_RECEIVED"])))
               .ForMember(m => m.UnitCost, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["UNIT_COST"])))
               .ForMember(m => m.NewUnitCost, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["NEW_UNIT_COST"])))
               .ForMember(m => m.StockOnHand, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["STOCK_ON_HAND"])))
               .ForMember(m => m.InTransitQty, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["IN_TRANSIT_QTY"])))
               .ForMember(m => m.PurchaseCost, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["PURCHASE_COST"])))
               .ForMember(m => m.PurchaseCostNew, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["PURCHASE_COST_NEW"])))
               .ForMember(m => m.AvCost, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["AV_COST"])))
               .ForMember(m => m.NewAvCost, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["NEW_AV_COST"])))
               .ForMember(m => m.LandedCost, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["LANDED_COST"])))
               .ForMember(m => m.NewLandedCost, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["NEW_LANDED_COST"])))
               .ForMember(m => m.CurrencyCode, opt => opt.MapFrom(r => r["CURRENCY_CODE"]))
               .ForMember(m => m.SuppCostAdj, opt => opt.MapFrom(r => r["SUPP_COST_ADJ"]))
               .ForMember(m => m.ApplyAllLoc, opt => opt.MapFrom(r => r["APPLY_ALL_LOC"]))
               .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"))
               .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]));

            this.CreateMap<OracleObject, SupplerDetailsModel>()
            .ForMember(m => m.OrderNo, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["ORDER_NO"])))
            .ForMember(m => m.Supplier, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["SUPPLIER"])))
            .ForMember(m => m.SupName, opt => opt.MapFrom(r => r["SUP_NAME"]))
            .ForMember(m => m.Status, opt => opt.MapFrom(r => r["STATUS"]))
            .ForMember(m => m.StatusDesc, opt => opt.MapFrom(r => r["STATUS_DESC"]))
            .ForMember(m => m.ExchangeRate, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["EXCHANGE_RATE"])))
            .ForMember(m => m.CurrencyCode, opt => opt.MapFrom(r => r["CURRENCY_CODE"]))
            .ForMember(m => m.Program_Message, opt => opt.MapFrom(r => r["PROGRAM_MESSAGE"]))
            .ForMember(m => m.Program_Phase, opt => opt.MapFrom(r => r["PROGRAM_PHASE"]))
            .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_ERROR"]));
        }
    }
}
