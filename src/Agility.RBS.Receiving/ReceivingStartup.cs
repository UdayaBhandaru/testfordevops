﻿// <copyright file="ReceivingStartup.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Receiving
{
    using Agility.Framework.Core;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;

    public class ReceivingStartup : ComponentStartup<DbContext>
    {
        /// <inheritdoc/>
        public override void ConfigureDependencyInjection(IServiceCollection services)
        {
            services.AddTransient<ReceivingRepository>();
            services.AddTransient<ReceivingController>();
        }

        public void Startup(Microsoft.Extensions.Configuration.IConfigurationRoot configuration)
        {
            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<Mapping.ReceivingMapping>();
                this.InitilizeMapping(cfg);
            });
        }
    }
}
