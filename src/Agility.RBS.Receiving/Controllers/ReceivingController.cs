﻿// <copyright file="ReceivingController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Receiving
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Models;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.Receiving.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    [Route("api/[controller]/[action]")]
    public class ReceivingController : Controller
    {
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly DomainDataRepository domainDataRepository;
        private readonly ReceivingRepository receivingRepository;
        private readonly ServiceDocument<ReceivingModel> serviceDocument;
        private long priceChangeRequestId;

        public ReceivingController(ServiceDocument<ReceivingModel> serviceDocument, DomainDataRepository domainDataRepository, ReceivingRepository receivingRepository)
        {
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
            this.domainDataRepository = domainDataRepository;
            this.receivingRepository = receivingRepository;
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in the Promotion List page.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<ReceivingModel>> List()
        {
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for fetching Promotion records based on search criteria when Search button clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<ReceivingModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.ReceivingSearch);
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in Promotion Head page when Add button clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<ReceivingModel>> New()
        {
            this.serviceDocument.New(false);
            return await this.GetDomainData();
        }

        public async Task<AjaxModel<List<ReceivingDetailsModel>>> GetOrdersReceived(string orderNo, string supplier)
        {
            return await this.receivingRepository.GetOrdersReceived(new ReceivingDetailsModel { OrderNo = orderNo, Supplier = supplier, LocType = null, Location = null }, this.serviceDocumentResult);
        }

        /// <summary>
        /// This API invoked for saving field values in the Promotion Head page when Save button clicked.
        /// </summary>
        /// <param name="serviceDocument">Service Document</param>
        /// <returns>ServiceDocument</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<ReceivingModel>> Save([FromBody]ServiceDocument<ReceivingModel> serviceDocument)
        {
            await this.serviceDocument.SaveAsync(this.ReceiveConfirmSave);
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for populating fields in the Promotion Head page when Edit icon clicked.
        /// </summary>
        /// <param name="id">Promotion Id</param>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<ReceivingModel>> Open(int id)
        {
            this.priceChangeRequestId = id;
            await this.serviceDocument.OpenAsync(this.CallBackOpen);
            return await this.GetDomainData();
        }

        private async Task<bool> ReceiveConfirmSave()
        {
            this.serviceDocument.Result = await this.receivingRepository.SetReceiving(this.serviceDocument.DataProfile.DataModel);
            if (this.serviceDocument.Result.Type != MessageType.Success)
            {
                return false;
            }

            this.serviceDocument.DataProfile.DataModel.ShipmentNo = this.receivingRepository.ShipmentId;
            return true;
        }

        private async Task<ReceivingModel> CallBackOpen()
        {
            try
            {
                this.serviceDocument.Result = this.serviceDocumentResult;
                return null;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<List<ReceivingModel>> ReceivingSearch()
        {
            try
            {
                this.serviceDocument.Result = this.serviceDocumentResult;
                return null;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<ServiceDocument<ReceivingModel>> GetDomainData()
        {
            this.serviceDocument.LocalizationData.AddData("PromotionHead");
            return this.serviceDocument;
        }
    }
}
