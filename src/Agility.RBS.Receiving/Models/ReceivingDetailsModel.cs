﻿// <copyright file="ReceivingDetailsModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Receiving.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using Agility.Framework.Core.Profile.Entities;

    public class ReceivingDetailsModel : ProfileEntity
    {
        public ReceivingDetailsModel()
        {
        }

        [Key]
        public string OrderNo { get; set; }

        public string Supplier { get; set; }

        public string SupName { get; set; }

        public string Location { get; set; }

        public string LocName { get; set; }

        public string DivisionId { get; set; }

        public string LocTypeDesc { get; set; }

        public string LocType { get; set; }

        public string Item { get; set; }

        public string ItemDesc { get; set; }

        public string ItemUom { get; set; }

        public long? OrderedQuantity { get; set; }

        public decimal? UnitCost { get; set; }

        public long? PrescaledQty { get; set; }

        public string Operation { get; set; }

        public string ReceiveDate { get; set; }

        public DateTime? ExpiryDate { get; set; }

        public bool IsExpiry { get; set; }

        public string ReceivedQuantity { get; set; }

        public string CurrencyCode { get; set; }

        public string Weight { get; set; }

        public string WeightUom { get; set; }

        public string UserId { get; set; }

        public string ExpectedDate { get; set; }
    }
}
