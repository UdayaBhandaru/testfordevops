﻿// <copyright file="ReceivingModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Receiving.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Agility.Framework.Core.Profile.Entities;

    public class ReceivingModel : ProfileEntity
    {
        public ReceivingModel()
        {
            this.ReceivingDetails = new List<ReceivingDetailsModel>();
        }

        [Key]
        public string OrderNo { get; set; }

        public string Operation { get; set; }

        public string Supplier { get; set; }

        public string SupName { get; set; }

        public string Location { get; set; }

        public string LocName { get; set; }

        public string DivisionId { get; set; }

        public string LocTypeDesc { get; set; }

        public string LocType { get; set; }

        public new string CreatedBy { get; set; }

        public string ReceiveDate { get; set; }

        public List<ReceivingDetailsModel> ReceivingDetails { get; set; }

        public string ShipmentNo { get; internal set; }
    }
}
