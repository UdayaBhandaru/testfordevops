﻿// <copyright file="ReceivingRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Receiving
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Core.Models;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Receiving.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class ReceivingRepository : BaseOraclePackage
    {
        private readonly ServiceDocumentResult serviceDocumentResult;

        public ReceivingRepository()
        {
            this.PackageName = "UTL_RCV_WRP";
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public string ShipmentId { get; private set; }

        public async Task<AjaxModel<List<ReceivingDetailsModel>>> GetOrdersReceived(ReceivingDetailsModel promotionSearchModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams("po_rcv_ord_dtls_tab", "getorddtl");
            OracleObject recordObject = this.SetParamsGetOrdersReceived(promotionSearchModel);
            var returningModel = await this.GetProcedure2<ReceivingDetailsModel>(recordObject, packageParameter, this.serviceDocumentResult);
            var ajaxResult = new AjaxModel<List<ReceivingDetailsModel>>()
            {
                Message = this.serviceDocumentResult.InnerException,
                Result = AjaxResult.Success,
                Model = returningModel
            };
            if (!string.IsNullOrEmpty(this.serviceDocumentResult.InnerException))
            {
                ajaxResult.Result = AjaxResult.Exception;
            }

            return ajaxResult;
        }

        public async Task<ServiceDocumentResult> SetReceiving(ReceivingModel hModel)
        {
            PackageParams packageParameter = this.GetPackageParams("po_shipment_tab", "set_po_receiving");
            OracleObject recordObject = this.SetParamsReceiving(hModel);
            var result = await this.SetProcedure(recordObject, packageParameter);
            if (this.ObjResult != null && result.Type == MessageType.Success)
            {
                this.ShipmentId = Convert.ToString(this.ObjResult["SHIPMENT"]);
            }

            return result;
        }

        private OracleObject SetParamsReceiving(ReceivingModel hModel)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType("po_shipment_rec", this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["ORDER_NO"]] = hModel.OrderNo;

            // Detail Object
            OracleType dtlTableType = this.GetObjectType("po_shipdtl_tab");
            OracleType dtlRecordType = this.GetObjectType("po_shipdtl_rec");
            OracleTable pUtlDmnDtlTab = new OracleTable(dtlTableType);
            foreach (var item in hModel.ReceivingDetails)
            {
                OracleObject dtlRecordObject = this.GetOracleObject(dtlRecordType);
                try
                {
                    dtlRecordObject[dtlRecordType.Attributes["item_id"]] = item.Item;
                    dtlRecordObject[dtlRecordType.Attributes["unit_qty"]] = item.ReceivedQuantity ?? null;
                    dtlRecordObject[dtlRecordType.Attributes["unit_cost"]] = item.UnitCost;
                    dtlRecordObject[dtlRecordType.Attributes["currency_code"]] = item.CurrencyCode ?? "KWD";
                    dtlRecordObject[dtlRecordType.Attributes["shipped_qty"]] = item.OrderedQuantity;
                    dtlRecordObject[dtlRecordType.Attributes["weight"]] = item.Weight;
                    dtlRecordObject[dtlRecordType.Attributes["weight_uom"]] = item.WeightUom;
                    dtlRecordObject[dtlRecordType.Attributes["is_expiry"]] = item.ExpiryDate == null ? "N" : "Y";
                    dtlRecordObject[dtlRecordType.Attributes["expiry_date"]] = item.ExpiryDate;
                    dtlRecordObject[dtlRecordType.Attributes["user_id"]] = FxContext.Context.EmailId;
                    dtlRecordObject[dtlRecordType.Attributes["operation"]] = "I";
                    pUtlDmnDtlTab.Add(dtlRecordObject);
                }
                catch (Exception)
                {
                    pUtlDmnDtlTab.Add(dtlRecordObject);
                }

                recordObject[recordType.Attributes["SUPPLIER"]] = item.Supplier;
                recordObject[recordType.Attributes["LOCATION"]] = item.Location;
                recordObject[recordType.Attributes["LOC_TYPE"]] = item.LocType;
                recordObject[recordType.Attributes["RECEIVE_DATE"]] = DateTime.Now;
                recordObject[recordType.Attributes["EXT_REF_NO"]] = 123;
                recordObject[recordType.Attributes["SHIPMENT"]] = null;
                recordObject[recordType.Attributes["OPERATION"]] = "I";
            }

            recordObject["SHIPDTL_TAB"] = pUtlDmnDtlTab;
            return recordObject;
        }

        private OracleObject SetParamsGetOrdersReceived(ReceivingDetailsModel promotionSearchModel)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType("po_rcv_ord_dtls_rec", this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["order_no"]] = promotionSearchModel.OrderNo;
            recordObject[recordType.Attributes["supplier"]] = promotionSearchModel.Supplier;
            recordObject[recordType.Attributes["loc_type"]] = promotionSearchModel.LocType;
            recordObject[recordType.Attributes["location"]] = promotionSearchModel.Location;
            recordObject[recordType.Attributes["location"]] = promotionSearchModel.Location;
            recordObject[recordType.Attributes["exp_date"]] = promotionSearchModel.ExpectedDate;
            return recordObject;
        }
    }
}