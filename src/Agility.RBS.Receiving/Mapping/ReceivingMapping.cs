﻿// <copyright file="ReceivingMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Receiving.Mapping
{
    using Agility.RBS.Core;
    using Agility.RBS.Receiving.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class ReceivingMapping : Profile
    {
        public ReceivingMapping()
            : base("ReceivingMapping")
        {
            this.CreateMap<OracleObject, ReceivingDetailsModel>()
            .ForMember(m => m.OrderNo, opt => opt.MapFrom(r => r["order_no"]))
            .ForMember(m => m.Supplier, opt => opt.MapFrom(r => r["supplier"]))
            .ForMember(m => m.SupName, opt => opt.MapFrom(r => r["sup_name"]))
            .ForMember(m => m.LocTypeDesc, opt => opt.MapFrom(r => r["loc_typ_desc"]))
            .ForMember(m => m.LocType, opt => opt.MapFrom(r => r["loc_type"]))
            .ForMember(m => m.Location, opt => opt.MapFrom(r => r["location"]))
            .ForMember(m => m.LocName, opt => opt.MapFrom(r => r["loc_name"]))
            .ForMember(m => m.Item, opt => opt.MapFrom(r => r["item"]))
             .ForMember(m => m.ExpectedDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["exp_date"])))
            .ForMember(m => m.ItemDesc, opt => opt.MapFrom(r => r["item_desc"]))
            .ForMember(m => m.ItemUom, opt => opt.MapFrom(r => r["item_uom"]))
            .ForMember(m => m.OrderedQuantity, opt => opt.MapFrom(r => OracleNullHandler.DbNullInt64Handler(r["ordered_qty"])))
            .ForMember(m => m.UnitCost, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["unit_cost"])))
            .ForMember(m => m.PrescaledQty, opt => opt.MapFrom(r => OracleNullHandler.DbNullInt64Handler(r["prescaled_qty"])))
            .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));
        }
    }
}
