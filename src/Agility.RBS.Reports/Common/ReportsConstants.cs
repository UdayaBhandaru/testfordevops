﻿// <copyright file="ReportsConstants.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Reports.Common
{
    internal static class ReportsConstants
    {
        // Domain Package
        public const string GetDomainPackage = "UTL_DOMAIN";

        // Domain Data Package
        public const string GetDomainDataPackage = "UTL_COMMON";

        public const string GetProcItemList = "GETITEMLIST";
        public const string GetProcItemBasic = "GETITEMBASICS";
        public const string SetProcItemBasic = "SETITEMBASICS";
        public const string GetItemSelectPackage = "ITEM_SEARCH";
        public const string ObjTypeItemSearch = "ITEM_SEARCH_TAB";
        public const string DomainHdrStatus = "STATUS";
        public const string DomainHdrOrderType = "ODRTYP";
    }
}