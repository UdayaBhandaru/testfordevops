﻿// <copyright file="ReportsStartup.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM
{
    using Agility.Framework.Core;
    using Agility.RBS.Reports;
    using Agility.RBS.Reports;
    using Agility.RBS.Reports.Mapping;
    using AutoMapper;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;

    public class ReportsStartup : ComponentStartup<DbContext>
    {
        public void Startup(IConfigurationRoot configuration)
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<ReportsMapping>();
                this.InitilizeMapping(cfg);
            });
        }

        public override void ConfigureDependencyInjection(IServiceCollection services)
        {
            services.AddTransient<ReportsRepository>();
        }
    }
}