﻿// <copyright file="OrderReportController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.UnitOfMeasure
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Linq;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.Reports;
    using Agility.RBS.Reports.Common;
    using Agility.RBS.Reports.ItemReports.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

      public class OrderReportController : Controller
    {
        private readonly ServiceDocument<OrderReportModel> serviceDocument;
        private readonly DomainDataRepository domainDataRepository;

        public OrderReportController(
            ServiceDocument<OrderReportModel> serviceDocument, DomainDataRepository domainDataRepository)
        {
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
        }

        public async Task<ServiceDocument<OrderReportModel>> List()
        {
            this.serviceDocument.DomainData.Add("orderType", this.domainDataRepository.DomainDetailData(ReportsConstants.DomainHdrOrderType).Result);
            this.serviceDocument.DomainData.Add("location", await this.domainDataRepository.CommonData("LOC"));
            this.serviceDocument.DomainData.Add("supplier", this.domainDataRepository.SupplierDomainGet().Result);
            this.serviceDocument.DomainData.Add("group", this.domainDataRepository.GroupDomainGet().Result);
            this.serviceDocument.DomainData.Add("status", this.domainDataRepository.DomainDetailData(ReportsConstants.DomainHdrStatus).Result);
            return this.serviceDocument;
        }
    }
}