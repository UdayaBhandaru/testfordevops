﻿// <copyright file="PriceChangeReportController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.UnitOfMeasure
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Linq;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.Reports;
    using Agility.RBS.Reports.Common;
    using Agility.RBS.Reports.ItemReports.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

      public class PriceChangeReportController : Controller
    {
        private readonly ServiceDocument<PriceChangeReportModel> serviceDocument;
        private readonly DomainDataRepository domainDataRepository;

        public PriceChangeReportController(
            ServiceDocument<PriceChangeReportModel> serviceDocument, DomainDataRepository domainDataRepository)
        {
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
        }

        public async Task<ServiceDocument<PriceChangeReportModel>> List()
        {
            this.serviceDocument.DomainData.Add("priceChangeReasons", await this.domainDataRepository.CommonData("PRICE_CHG_REASON"));
            return this.serviceDocument;
        }
    }
}