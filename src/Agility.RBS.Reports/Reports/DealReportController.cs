﻿// <copyright file="DealReportController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.MDM.UnitOfMeasure
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Linq;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.Reports;
    using Agility.RBS.Reports.Common;
    using Agility.RBS.Reports.ItemReports.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

      public class DealReportController : Controller
    {
        private readonly ServiceDocument<DealReportModel> serviceDocument;
        private readonly DomainDataRepository domainDataRepository;

        public DealReportController(
            ServiceDocument<DealReportModel> serviceDocument, DomainDataRepository domainDataRepository)
        {
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
        }

        public async Task<ServiceDocument<DealReportModel>> List()
        {
            var itemModel = this.domainDataRepository.ItemDomainGet().Result;
            this.serviceDocument.DomainData.Add("status", this.domainDataRepository.DomainDetailData(ReportsConstants.DomainHdrStatus).Result);
            this.serviceDocument.DomainData.Add("division", itemModel.Divisions.OrderBy(x => x.DivisionDescription));
            this.serviceDocument.DomainData.Add("group", itemModel.Groups.OrderBy(x => x.GroupName));
            this.serviceDocument.DomainData.Add("department", itemModel.Departments.OrderBy(x => x.DeptName));
            this.serviceDocument.DomainData.Add("class", itemModel.Classes.OrderBy(x => x.ClassName));
            return this.serviceDocument;
        }
    }
}