﻿//-------------------------------------------------------------------------------------------------
// <copyright file="ItemReportModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.Reports.ItemReports.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class ItemReportModel : ProfileEntity
    {
        public long? Item { get; set; }

        public string ItemType { get; set; }

        public string ItemTypeDesc { get; set; }

        public int? DivisionId { get; set; }

        public string DivisionDesc { get; set; }

        public int? DeptId { get; set; }

        public string DeptDesc { get; set; }

        public int? CategoryId { get; set; }

        public string CategoryDesc { get; set; }

        public int? ClassId { get; set; }

        public string ClassDesc { get; set; }

        public int? SubClassId { get; set; }

        public string SubclassDesc { get; set; }

        public string ItemStatus { get; set; }

        public string ItemStatusDesc { get; set; }

        public int LangId { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Error { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }
    }
}
