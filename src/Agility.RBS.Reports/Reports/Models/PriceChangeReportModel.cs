﻿//-------------------------------------------------------------------------------------------------
// <copyright file="PriceChangeReportModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UOMClass
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.Reports.ItemReports.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class PriceChangeReportModel : ProfileEntity
    {
        public int LangId { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Error { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }
    }
}
