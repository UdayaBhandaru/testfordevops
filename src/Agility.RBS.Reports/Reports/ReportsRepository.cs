﻿// <copyright file="ReportsRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Reports
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Reports.Common;
    using Agility.RBS.Reports.Common;
    using Agility.RBS.Reports.ItemReports.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class ReportsRepository : BaseOraclePackage
    {
        public ReportsRepository()
        {
            this.PackageName = ReportsConstants.GetItemSelectPackage;
        }

        public async Task<List<ItemReportModel>> GetItem(ItemReportModel itemReportModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(ReportsConstants.ObjTypeItemSearch, ReportsConstants.GetProcItemBasic);
            OracleObject recordObject = this.SetParamsItem(itemReportModel, true);
            return await this.GetProcedure2<ItemReportModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        private OracleObject SetParamsItem(ItemReportModel itemReportModel, bool isSearch)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType("ITEM_BASICS_TAB", this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            if (!isSearch)
            {
                recordObject[recordType.Attributes["ITEM"]] = itemReportModel.Item;
                recordObject[recordType.Attributes["Operation"]] = itemReportModel.Operation;
                recordObject[recordType.Attributes["CREATED_BY"]] = this.GetCreatedBy(itemReportModel.CreatedBy);
                recordObject[recordType.Attributes["LAST_UPDATED_BY"]] = FxContext.Context.Name;
            }

            recordObject[recordType.Attributes["ITEM_TYPE"]] = itemReportModel.ItemType;
            recordObject[recordType.Attributes["ITEM_TYPE_DESC"]] = itemReportModel.ItemTypeDesc;
            recordObject[recordType.Attributes["ITEM_STATUS"]] = itemReportModel.ItemStatus;
            return recordObject;
        }
    }
}