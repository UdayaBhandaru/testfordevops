﻿// <copyright file="ReportsMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Reports.Mapping
{
    using System;
    using Agility.RBS.Core;
    using Agility.RBS.Reports.ItemReports.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class ReportsMapping : Profile
    {
        public ReportsMapping()
            : base("ReportsMapping")
        {
            this.CreateMap<OracleObject, ItemReportModel>()
             .ForMember(m => m.Item, opt => opt.MapFrom(r => r["ITEM"]))
                .ForMember(m => m.ItemType, opt => opt.MapFrom(r => r["ITEM_TYPE"]))
                .ForMember(m => m.ItemTypeDesc, opt => opt.MapFrom(r => r["ITEM_TYPE_DESC"]))
                .ForMember(m => m.DivisionId, opt => opt.MapFrom(r => r["DIVISION_ID"]))
                .ForMember(m => m.DivisionDesc, opt => opt.MapFrom(r => r["DIVISION_DESC"]))
                .ForMember(m => m.CategoryId, opt => opt.MapFrom(r => r["CATEGORY_ID"]))
                .ForMember(m => m.CategoryDesc, opt => opt.MapFrom(r => r["CATEGORY_DESC"]))
                .ForMember(m => m.DeptId, opt => opt.MapFrom(r => r["DEPT_ID"]))
                .ForMember(m => m.DeptDesc, opt => opt.MapFrom(r => r["DEPT_DESC"]))
                .ForMember(m => m.ClassId, opt => opt.MapFrom(r => r["CLASS_ID"]))
                .ForMember(m => m.ClassDesc, opt => opt.MapFrom(r => r["CLASS_DESC"]))
                .ForMember(m => m.SubClassId, opt => opt.MapFrom(r => r["SUB_CLASS_ID"]))
                .ForMember(m => m.SubclassDesc, opt => opt.MapFrom(r => r["SUB_CLASS_DESC"]))
                .ForMember(m => m.ItemStatus, opt => opt.MapFrom(r => r["ITEM_STATUS"]))
                .ForMember(m => m.ItemStatusDesc, opt => opt.MapFrom(r => r["ITEM_STATUS_DESC"]))
                .ForMember(m => m.CreatedBy, opt => opt.MapFrom(r => r["CREATED_BY"]))
                .ForMember(m => m.ModifiedBy, opt => opt.MapFrom(r => r["LAST_UPDATED_BY"]))
                .ForMember(m => m.CreatedDate, opt => opt.MapFrom(r => r["CREATED_DATE"]))
                .ForMember(m => m.ModifiedDate, opt => opt.MapFrom(r => r["LAST_UPDATED_DATE"]))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "ITEM_BASICS"));
        }
    }
}
