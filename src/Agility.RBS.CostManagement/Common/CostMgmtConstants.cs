﻿// <copyright file="CostMgmtConstants.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.CostManagement
{
    internal static class CostMgmtConstants
    {
        // Cost Management
        public const string GetCostPackage = "UTL_COST";
        public const string ObjTypeCostChangeSearch = "COST_CHG_SEARCH_TAB";
        public const string RecordObjCostChangeSearch = "COST_CHG_SEARCH_REC";
        public const string GetProcCostChangeSearch = "GETCOSTCHANGELIST";

        public const string GetProcCostChangeHead = "GETCOSTCHANGEHEAD";
        public const string ObjTypeCostChangeHead = "COST_CHANGE_HEAD_TAB";
        public const string RecordObjCostChangeHead = "COST_CHANGE_HEAD_REC";
        public const string SetProcCostChangeHead = "SETCOSTCHANGEHEAD";

        public const string ObjTypeCostChangeDetails = "COST_CHANGE_DETAIL_TAB";
        public const string RecordObjCostChangeDetails = "COST_CHANGE_DETAIL_REC";
        public const string GetProcCostChangeDetails = "GETCOSTCHANGEDETAIL";
        public const string SetProcCostChangeDetails = "SETCOSTCHANGEDETAIL";

        public const string ObjTypeCostChangePrint = "COST_CHANGE_PRINT_TAB";
        public const string GetProcCostChangePrint = "GETCOSTCHANGEPRINT";
        public const string RecordObjCostChangePrint = "COST_CHANGE_PRINT_REC";

        // Domain Header Ids
        public const string DomainHdrCostChgReqStatus = "COSTCRSTAT";
        public const string DomainHdrDateType = "DATETYPE";
        public const string DomainHdrIndicator = "INDICATOR";
        public const string DomainHdrCostChgType = "COSTCGTYPE";
        public const string DomainHdrOrgLevel = "COSTCHGLVL";
        public const string DomainHdrCostChgOrigin = "COST_ORGN";

        public const string CostChangeHeadWfObjType = "Cost_CHANGE_HEAD_TAB";
        public const string SetProcCostChangeHeadWf = "SETCOSTCHANGE_WORKFLOW";

        // Cost Change Detail For Excel
        public const string GetCostChangeDetailExceclList = "Getcostchgdetail";

        // Set Cost Change Info1
        public const string SetCostChangeDetailExceclList = "setcostchgdetail";

        public const string CostChangeHeadExcelListTab = "COST_CHANGE_FORM_TAB";
        public const string RecordObjCostChangeHeadExcelList = "COST_CHANGE_FORM_REC";

        public const string CostChangeDetailExcelListTab = "COST_DTL_FORM_TAB";
        public const string RecordObjCostChangeDetailExcelList = "COST_DTL_FORM_REC";

        // RSP
        public const string CostChangeRspExcelListTab = "COST_DTL_FORM_TAB";
        public const string RecordObjCostChangeRspExcelList = "COST_DTL_FORM_TAB";
    }
}