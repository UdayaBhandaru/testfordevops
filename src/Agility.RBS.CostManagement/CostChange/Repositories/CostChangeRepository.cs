﻿// <copyright file="CostChangeRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.CostManagement.CostChange
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Constants;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.CostManagement.CostChange.Models;
    using Agility.RBS.Inbox.Common;
    using Agility.RBS.MDM.Repositories.Models;
    using AutoMapper;
    using Devart.Data.Oracle;
    using DocumentFormat.OpenXml.Packaging;
    using DocumentFormat.OpenXml.Spreadsheet;

    public class CostChangeRepository : BaseOraclePackage
    {
        public CostChangeRepository()
        {
            this.PackageName = CostMgmtConstants.GetCostPackage;
        }

        public int CostChangeRequestId { get; private set; }

        public async Task<CostChangeHeadModel> ConvertExcelDataToType(DataTable eSheet, CostChangeHeadModel iHeadModel)
        {
            if (eSheet != null)
            {
                List<CostChangeDetailModel> orderDetailsList = (from DataRow dr in eSheet.Rows
                                                                 where Convert.ToString(dr[0]) != "Department" && Convert.ToString(dr[2]) != string.Empty
                                                                 select new CostChangeDetailModel
                                                                 {
                                                                     Item = Convert.ToString(dr[1]),
                                                                     DepartmentDescription = Convert.ToString(dr[0]),
                                                                     ItemDescription = Convert.ToString(dr[2]),
                                                                 }).ToList();

                iHeadModel.CostChangeDetails.AddRange(orderDetailsList);
            }

            return iHeadModel;
        }

        public async Task<List<CostChangeSearchModel>> CostChangeListGet(ServiceDocument<CostChangeSearchModel> serviceDoc, ServiceDocumentResult serviceDocumentResult)
        {
            CostChangeSearchModel costChangeSearch = serviceDoc.DataProfile.DataModel;
            PackageParams packageParameter = this.GetPackageParams(CostMgmtConstants.ObjTypeCostChangeSearch, CostMgmtConstants.GetProcCostChangeSearch);
            OracleObject recordObject = this.SetParamsCostChangeRequest(costChangeSearch);

            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();

            BaseOracleParameter baseOracleParameter;
            baseOracleParameter = new BaseOracleParameter
            {
                Name = "P_START",
                DataType = OracleDbType.Number,
                Direction = ParameterDirection.Input,
                Value = Convert.ToString(costChangeSearch.StartRow)
            };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            baseOracleParameter = new BaseOracleParameter { Name = "P_END", DataType = OracleDbType.Number, Direction = ParameterDirection.Input, Value = Convert.ToString(costChangeSearch.EndRow) };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            baseOracleParameter = new BaseOracleParameter { Name = "P_total", DataType = OracleDbType.Number, Direction = ParameterDirection.Output };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            if (costChangeSearch.SortModel == null)
            {
                costChangeSearch.SortModel = new[] { new Core.Models.SortModel { ColId = null, Sort = null } };
            }

            baseOracleParameter = new BaseOracleParameter { Name = "P_SortColId", DataType = OracleDbType.VarChar, Direction = ParameterDirection.Input, Value = costChangeSearch.SortModel[0].ColId };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            baseOracleParameter = new BaseOracleParameter { Name = "P_SortOrder", DataType = OracleDbType.VarChar, Direction = ParameterDirection.Input, Value = costChangeSearch.SortModel[0].Sort };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));
            parameters.Add(this.ParameterBuilder(UserProfileHelper.GetUserParameter()));
            var costChanges = await this.GetProcedure2<CostChangeSearchModel>(recordObject, packageParameter, serviceDocumentResult, parameters);
            costChangeSearch.TotalRows = (this.Parameters["p_total"].Value != null && this.Parameters["p_total"].Value != DBNull.Value) ? Convert.ToInt64(this.Parameters["p_total"].Value) : 1;
            serviceDoc.DataProfile.DataModel = costChangeSearch;
            return costChanges;
        }

        public async Task<CostChangeHeadModel> CostChangeHeadGet(int costChangeRequestId, FilePathModel file, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType(CostMgmtConstants.RecordObjCostChangeHead);
            OracleObject recordObject = this.GetOracleObject(recordType);
            this.SetCostChangeRequestId(costChangeRequestId, recordObject);
            PackageParams packageParameter = this.GetPackageParams(CostMgmtConstants.ObjTypeCostChangeHead, CostMgmtConstants.GetProcCostChangeHead);
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            parameters.Add(this.ParameterBuilder(UserProfileHelper.GetUserParameter()));
            var costChangeHeads = await this.GetProcedure2<CostChangeHeadModel>(recordObject, packageParameter, serviceDocumentResult, parameters);
            if (costChangeHeads != null && costChangeHeads.Count > 0)
            {
                string pricechangeFile = $"{RbSettings.RbsExcelFileDownloadPath}\\Cost Change Form Creation-{costChangeHeads[0].COST_CHANGE.Value}.xlsm";
                if (costChangeHeads[0].CostChangeStatus == "Request Completed" || costChangeHeads[0].CostChangeStatus == "RequestCompleted")
                {
                    pricechangeFile = $"{file.ProcessPath}\\Cost Change Form Creation-{costChangeRequestId}.xlsm";
                    if (!File.Exists(pricechangeFile))
                    {
                        pricechangeFile = $"{file.ArchivePath}\\Cost Change Form Creation-{costChangeRequestId}.xlsm";
                    }
                }

                if (File.Exists(pricechangeFile))
                {
                    await this.ConvertExcelDataToType(await this.GetDataTableByReadingExcel(pricechangeFile), costChangeHeads[0]);
                }

                return costChangeHeads[0];
            }

            return null;
        }

        public async Task<CostChangeHeadModel> CostChangeHeadGet(int costChangeRequestId, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType(CostMgmtConstants.RecordObjCostChangeHead);
            OracleObject recordObject = this.GetOracleObject(recordType);
            this.SetCostChangeRequestId(costChangeRequestId, recordObject);
            PackageParams packageParameter = this.GetPackageParams(CostMgmtConstants.ObjTypeCostChangeHead, CostMgmtConstants.GetProcCostChangeHead);
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            parameters.Add(this.ParameterBuilder(UserProfileHelper.GetUserParameter()));
            var costChangeHeads = await this.GetProcedure2<CostChangeHeadModel>(recordObject, packageParameter, serviceDocumentResult, parameters);
            return costChangeHeads != null ? costChangeHeads[0] : null;
        }

        public virtual void SetCostChangeRequestId(int costChangeRequestId, OracleObject recordObject)
        {
            recordObject["COST_CHANGE_ID"] = costChangeRequestId;
        }

        public async Task<ServiceDocumentResult> SetCostChangeHead(CostChangeHeadModel costChangeHeadModel)
        {
            this.CostChangeRequestId = 0;
            PackageParams packageParameter = this.GetPackageParams(CostMgmtConstants.ObjTypeCostChangeHead, CostMgmtConstants.SetProcCostChangeHead);
            OracleObject recordObject = this.SetParamsCostChangeHead(costChangeHeadModel);
            var result = await this.SetProcedure(recordObject, packageParameter);
            if (this.ObjResult != null)
            {
                this.CostChangeRequestId = Convert.ToInt32(this.ObjResult["COST_CHANGE_ID"]);
            }

            return result;
        }

        public void SetCostChangeDetailsFromExcel(string fileName)
        {
            PackageParams packageParameter = this.GetPackageParams(CostMgmtConstants.CostChangeHeadExcelListTab, CostMgmtConstants.SetCostChangeDetailExceclList);
            OracleObject recordObject = this.SetParamsCostChangeDetailsFromExcel(fileName);
            this.SetProcedureBulk(recordObject, packageParameter).ConfigureAwait(true);
        }

        public virtual OracleObject SetParamsCostChangeRequest(CostChangeSearchModel costChgHdrModel)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(CostMgmtConstants.RecordObjCostChangeSearch, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject["ITEM_BARCODE"] = costChgHdrModel.ItemBarcode;
            recordObject["ITEM_DESC"] = costChgHdrModel.ItemDesc;
            recordObject["COST_CHANGE_ID"] = costChgHdrModel.COST_CHANGE_ID;
            recordObject["Cost_Change_Desc"] = costChgHdrModel.CostChangeDesc;
            recordObject["COST_STATUS"] = costChgHdrModel.CostChangeStatus;
            recordObject["REASON"] = costChgHdrModel.Reason;
            recordObject["SUPPLIER_ID"] = costChgHdrModel.SupplierId;
            return recordObject;
        }

        public virtual OracleObject SetParamsCostChangeHead(CostChangeHeadModel costChgHdrModel)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(CostMgmtConstants.RecordObjCostChangeHead, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject["COST_CHANGE_ID"] = costChgHdrModel.COST_CHANGE;
            recordObject["REASON"] = costChgHdrModel.Reason;
            recordObject["DIVISION_ID"] = costChgHdrModel.DivisionId;
            recordObject["SUPPLIER_ID"] = costChgHdrModel.SupplierId;
            recordObject["COST_CHANGE_ORIGIN"] = costChgHdrModel.CostChangeOrigin = "SUP";
            recordObject["EFFECTIVE_START_DATE"] = costChgHdrModel.EffectiveStartDate;
            recordObject["EFFECTIVE_END_DATE"] = costChgHdrModel.EffectiveEndDate;
            recordObject["RUN_DATE"] = costChgHdrModel.RunDate;
            recordObject["OPERATION"] = costChgHdrModel.Operation;
            recordObject["CREATED_BY"] = this.GetCreatedBy(costChgHdrModel.CreatedBy);
            recordObject["LAST_UPDATED_BY"] = FxContext.Context.Name;
            recordObject["COST_CHANGE_DESC"] = costChgHdrModel.CostChangeDesc;
            recordObject["COST_STATUS"] = "A";
            recordObject["LAST_UPDATED_BY"] = FxContext.Context.Name;
            recordObject["WF_NXT_STATE_IND"] = null;
            recordObject["LANG_ID"] = "1";
            return recordObject;
        }

        public async Task<List<FutureCostModel>> GetFutureCostData(string itemId, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            BaseOracleParameter baseOracleParameter;
            baseOracleParameter = new BaseOracleParameter { Name = "P_ITEM", DataType = OracleDbType.VarChar, Direction = ParameterDirection.Input, Value = itemId };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));
            OracleType recordType = this.GetObjectType("FUTURE_SUPER_COST_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams("FUTURE_SUPER_COST_TAB", "GETFUTURECOST");
            return await this.GetProcedure2<FutureCostModel>(recordObject, packageParameter, serviceDocumentResult, parameters);
        }

        public OracleObject SetParamsCostChangeDetailsFromExcel(string fileName)
        {
            using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(fileName, false))
            {
                WorkbookPart workbookPart = spreadSheet.WorkbookPart;
                Sheet sheet1 = workbookPart.Workbook.Descendants<Sheet>().First(s => s.Name == "Cost Change Form");
                WorksheetPart worksheetPart = workbookPart.GetPartById(sheet1.Id.Value) as WorksheetPart;
                var rows = worksheetPart.Worksheet.Descendants<Row>().ToList();

                this.Connection.Open();
                OracleType recordType = this.GetObjectType("COST_CHANGE_FORM_REC");
                OracleObject recordObject = this.GetOracleObject(recordType);

                // Detail Object
                OracleType dtlTableType = this.GetObjectType(CostMgmtConstants.CostChangeDetailExcelListTab);
                OracleType dtlRecordType = this.GetObjectType(CostMgmtConstants.RecordObjCostChangeDetailExcelList);
                OracleTable pUtlDmnDtlTab = new OracleTable(dtlTableType);

                for (int i = 13; i < rows.Count; i++)
                {
                    string itemBarcode = ExcelHelper.GetCellValue("B", i + 1, worksheetPart.Worksheet, workbookPart);
                    if (!string.IsNullOrEmpty(itemBarcode))
                    {
                        OracleObject dtlRecordObject = this.GetOracleObject(dtlRecordType);
                        dtlRecordObject["COST_CHANGE_ID"] = ExcelHelper.GetCellValue("F", 6, worksheetPart.Worksheet, workbookPart);
                        dtlRecordObject["SUPPLIER_ID"] = ExcelHelper.GetCellValue("B", 6, worksheetPart.Worksheet, workbookPart);
                        dtlRecordObject["ITEM_BARCODE"] = itemBarcode;
                        dtlRecordObject["CASE_COST"] = NumericCellNullCheck(ExcelHelper.GetCellValue("L", i + 1, worksheetPart.Worksheet, workbookPart));
                        dtlRecordObject["UNIT_COST"] = NumericCellNullCheck(ExcelHelper.GetCellValue("N", i + 1, worksheetPart.Worksheet, workbookPart));
                        dtlRecordObject["NET_UNIT_COST"] = NumericCellNullCheck(ExcelHelper.GetCellValue("R", i + 1, worksheetPart.Worksheet, workbookPart));
                        dtlRecordObject["OPERATION"] = "I";
                        pUtlDmnDtlTab.Add(dtlRecordObject);
                        recordObject["COST_DTL_FORM"] = pUtlDmnDtlTab;
                    }
                    else
                    {
                        return recordObject;
                    }
                }

                return recordObject;
            }
        }
    }
}