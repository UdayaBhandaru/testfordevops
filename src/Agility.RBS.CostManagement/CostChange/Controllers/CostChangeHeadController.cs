﻿// <copyright file="CostChangeHeadController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.CostManagement.CostChange
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Constants;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.CostManagement;
    using Agility.RBS.CostManagement.CostChange.Models;
    using Agility.RBS.Inbox.Common;
    using Agility.RBS.Inbox.Models;
    using Agility.RBS.Inbox.Repository;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Models;
    using DocumentFormat.OpenXml.Packaging;
    using DocumentFormat.OpenXml.Spreadsheet;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    [Route("api/[controller]/[action]")]
    public class CostChangeHeadController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly CostChangeRepository costChangeRepository;
        private readonly InboxModel inboxModel;
        private readonly InboxRepository inboxRepository;
        private ServiceDocument<CostChangeHeadModel> serviceDocument;
        private int costChangeRequestId;

        public CostChangeHeadController(
            ServiceDocument<CostChangeHeadModel> serviceDocument,
            CostChangeRepository costChangeRepository,
            DomainDataRepository domainDataRepository,
            InboxRepository inboxRepository)
        {
            this.serviceDocument = serviceDocument;
            this.costChangeRepository = costChangeRepository;
            this.domainDataRepository = domainDataRepository;
            this.inboxRepository = inboxRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
            this.inboxModel = new InboxModel { Operation = "CHGS", ModuleName = "COST" };
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in Cost Change Head page when Add button clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<CostChangeHeadModel>> New()
        {
            this.serviceDocument.New(false);
            await this.BasicDomainData();
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for populating fields in the Cost Change Head page when Edit icon clicked.
        /// </summary>
        /// <param name="id">Cost Change Id</param>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<CostChangeHeadModel>> Open(int id)
        {
            this.costChangeRequestId = id;
            await this.serviceDocument.OpenAsync(this.CostChangeHeadOpen);
            ////this.serviceDocument.DomainData.Add("inboxCommonData", this.domainDataRepository.InboxCommonGet(RbCommonConstants.CostModule, id).Result);
            await this.BasicDomainData();
            this.serviceDocument.DomainData.Add("supplier", this.domainDataRepository.SupplierDomainGet(this.serviceDocument.DataProfile.DataModel.SupplierId.Value).Result);
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for saving field values in the Cost Change Head page when Save button clicked.
        /// </summary>
        /// <param name="serviceDocument">Service Document</param>
        /// <returns>ServiceDocument</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<CostChangeHeadModel>> Save([FromBody]ServiceDocument<CostChangeHeadModel> serviceDocument)
        {
            await this.serviceDocument.SaveAsync(this.CostChangeHeadSave);
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for submitting cost change to next level when Submit button clicked.
        /// </summary>
        /// <param name="serviceDocument">Service Document</param>
        /// <returns>ServiceDocument</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        [AllowAnonymous]
        public async Task<ServiceDocument<CostChangeHeadModel>> Submit([FromBody]ServiceDocument<CostChangeHeadModel> serviceDocument)
        {
            this.serviceDocument = serviceDocument;
            this.inboxModel.StateID = GuidConverter.DotNetToOracle(Convert.ToString(serviceDocument.DataProfile.DataModel.WorkflowInstance?.WorkflowStateId));
            await this.serviceDocument.TransitAsync(this.CostChangeHeadSave);
            if (this.serviceDocument.Result.Type == MessageType.Success)
            {
                if (this.serviceDocument.DataProfile.DataModel.WorkflowInstance.StateName == "Worksheet")
                {
                    this.inboxModel.Operation = "DFT";
                }

                await this.inboxRepository.SetInbox(this.inboxModel, this.serviceDocument);
            }

            var stateName = RbsServiceDocCustomizationHelper.GetStateNameByProfileIdAndCurActionId<CostChangeHeadModel>(this.serviceDocument);
            if (stateName == "RequestCompleted" || stateName == "Request Completed")
            {
                var filePaths = this.domainDataRepository.FilePathGet(this.inboxModel.ModuleName).Result;
                string rootFolderPath = RbSettings.RbsExcelFileDownloadPath;
                string file = "Cost Change Form Creation-" + this.serviceDocument.DataProfile.DataModel.COST_CHANGE.ToString() + ".xlsm";
                string fileToMove = Path.Combine(rootFolderPath, file);
                string moveTo = Path.Combine(filePaths.ProcessPath, file);

                //// moving file
                System.IO.File.Move(fileToMove, moveTo);
                if (System.IO.File.Exists(moveTo))
                {
                    this.costChangeRepository.SetCostChangeDetailsFromExcel(moveTo);
                    string fileToArchieve = Path.Combine(filePaths.ArchivePath, file);
                    System.IO.File.Move(moveTo, fileToArchieve);
                }
            }

            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for rejecting cost change to previous level when Reject button clicked.
        /// </summary>
        /// <param name="serviceDocument">Service Document</param>
        /// <returns>ServiceDocument</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<CostChangeHeadModel>> Reject([FromBody]ServiceDocument<CostChangeHeadModel> serviceDocument)
        {
            this.inboxModel.StateID = GuidConverter.DotNetToOracle(Convert.ToString(serviceDocument.DataProfile.DataModel.WorkflowInstance?.WorkflowStateId));
            await this.serviceDocument.TransitAsync(GuidConverter.DotNetToOracle(serviceDocument.DataProfile.DataModel.WorkflowForm.StateId), true);
            if (this.serviceDocument.Result.Type == MessageType.Success)
            {
                this.inboxModel.Operation = "REJ";
                this.inboxModel.ActionType = "R";
                this.inboxModel.ToStateID = GuidConverter.DotNetToOracle(this.serviceDocument.DataProfile.DataModel.WorkflowStateId.ToString());
                this.inboxRepository.SetInbox(this.inboxModel, this.serviceDocument);
            }

            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked to discard CostChange
        /// </summary>
        /// <returns>ServiceDocument</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<CostChangeHeadModel>> Delete()
        {
            await this.serviceDocument.SaveAsync(this.CostChangeDelete);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        public async Task<FileResult> OpenCostManagementExcel(int id)
        {
            FilePathModel file = this.domainDataRepository.FilePathGet(this.inboxModel.ModuleName).Result;
            var user = Core.Utility.UserProfileHelper.GetInMemoryUser();
            var companyid = this.HttpContext.Request.Headers["companyid"];
            var jwtTokenTuple = System.Tuple.Create<string, string, string>(this.HttpContext.Session.Id, companyid, Core.JwtTokenHelper.GetToken(user.UserId, user.UserName));

            string xlsm = $"{RbSettings.RbsExcelFileDownloadPath}\\{Guid.NewGuid().ToString()}.xlsm";
            string costChangeFile = $"{RbSettings.RbsExcelFileDownloadPath}\\Cost Change Form Creation-{id}.xlsm";
            string costChangeFileArc = $"{file.ArchivePath}\\Cost Change Form Creation-{id}.xlsm";
            string costChangeFileProcess = $"{file.ProcessPath}\\Cost Change Form Creation-{id}.xlsm";

            if (System.IO.File.Exists(costChangeFile))
            {
                System.IO.File.Copy(costChangeFile, xlsm);
            }
            else if (System.IO.File.Exists(costChangeFileProcess))
            {
                System.IO.File.Copy(costChangeFileProcess, xlsm);
            }
            else if (System.IO.File.Exists(costChangeFileArc))
            {
                System.IO.File.Copy(costChangeFileArc, xlsm);
            }
            else
            {
                var costChangeDetailsdata = await this.costChangeRepository.CostChangeHeadGet(id, this.serviceDocumentResult);
                System.IO.File.Copy(file.TemplatePath, xlsm);
                using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(xlsm, true))
                {
                    WorkbookPart workbookPart1 = spreadSheet.WorkbookPart;
                    Sheet sheet1 = workbookPart1.Workbook.Descendants<Sheet>().First(s => s.Name == "Cost Change Form");
                    WorksheetPart worksheetPart1 = workbookPart1.GetPartById(sheet1.Id.Value) as WorksheetPart;
                    Worksheet costworksheet = worksheetPart1.Worksheet;
                    try
                    {
                        ////Jwt Token
                        ExcelHelper.WriteToCell<string>("BG", 15, costworksheet, "test");
                        ExcelHelper.WriteToCell<string>("BG", 16, costworksheet, jwtTokenTuple.Item1);
                        ExcelHelper.WriteToCell<string>("BG", 17, costworksheet, jwtTokenTuple.Item1);

                        ////Header Row
                        ExcelHelper.WriteToCell<int>("B", 6, costworksheet, Convert.ToInt32(costChangeDetailsdata.SupplierId));
                        ExcelHelper.WriteToCell<string>("B", 7, costworksheet, costChangeDetailsdata.SupplierName);
                        ExcelHelper.WriteToCell<int>("B", 8, costworksheet, Convert.ToInt32(costChangeDetailsdata.Rebates));
                        ExcelHelper.WriteToCell<int>("F", 6, costworksheet, Convert.ToInt32(costChangeDetailsdata.COST_CHANGE));
                        ExcelHelper.WriteToCell<string>("F", 7, costworksheet, costChangeDetailsdata.ReasonDesc);
                        ExcelHelper.WriteToCell<string>("I", 6, costworksheet, costChangeDetailsdata.EffectiveStartDate?.ToString("dd/MM/yyyy"));
                        ExcelHelper.WriteToCell<string>("I", 7, costworksheet, costChangeDetailsdata.EffectiveEndDate?.ToString("dd/MM/yyyy"));
                        costworksheet.Save();
                        Sheet sheetTempData = workbookPart1.Workbook.Descendants<Sheet>().FirstOrDefault(s => s.Name == "TempData");
                        WorksheetPart worksheetPartTempData = workbookPart1.GetPartById(sheetTempData.Id.Value) as WorksheetPart;
                        Worksheet promotionworksheetTempData = worksheetPartTempData.Worksheet;
                        ExcelHelper.WriteToCell<string>("A", 1, promotionworksheetTempData, RbSettings.Web);
                        promotionworksheetTempData.Save();
                    }
                    catch (Exception ex)
                    {
                        this.serviceDocument.Result = new ServiceDocumentResult
                        {
                            InnerException = "Exception",
                            StackTrace = ex.ToString(),
                            Type = MessageType.Exception
                        };
                    }
                }
            }

            return await DocumentsHelper.GetFilestream(xlsm, this);
        }

        private async Task<CostChangeHeadModel> CostChangeHeadOpen()
        {
            try
            {
                FilePathModel file = this.domainDataRepository.FilePathGet(this.inboxModel.ModuleName).Result;
                var costChangeHead = await this.costChangeRepository.CostChangeHeadGet(this.costChangeRequestId, file, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return costChangeHead;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> CostChangeHeadSave()
        {
            this.serviceDocument.DataProfile.DataModel.CostChangeStatus = RbsServiceDocCustomizationHelper.GetStateNameByProfileIdAndCurActionId(this.serviceDocument);
            this.serviceDocument.Result = await this.costChangeRepository.SetCostChangeHead(this.serviceDocument.DataProfile.DataModel);
            if (this.serviceDocument.Result.Type != MessageType.Success)
            {
                return false;
            }

            this.serviceDocument.DataProfile.DataModel.COST_CHANGE = this.costChangeRepository.CostChangeRequestId;
            return true;
        }

        private async Task<bool> CostChangeDelete()
        {
            this.serviceDocument.DataProfile.DataModel.Operation = "D";
            this.serviceDocument.Result = await this.costChangeRepository.SetCostChangeHead(this.serviceDocument.DataProfile.DataModel);
            return true;
        }

        private async Task BasicDomainData()
        {
            this.serviceDocument.DomainData.Add("costChangeReasons", await this.domainDataRepository.CostChangeReasonsDomainGet());
            this.serviceDocument.DomainData.Add("division", this.domainDataRepository.DivisionDomainGet().Result);
            this.serviceDocument.LocalizationData.AddData("CostChangeHead");
        }
    }
}
