﻿// <copyright file="CostManagementController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.CostManagement.CostChange
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Models;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Constants;
    using Agility.RBS.Core.Models;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.CostManagement.CostChange.Models;
    using Agility.RBS.Inbox.Models;
    using Agility.RBS.ItemManagement.Item.Models;
    using Agility.RBS.ItemManagement.Item.Repositories;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Models;
    using DocumentFormat.OpenXml.Packaging;
    using DocumentFormat.OpenXml.Spreadsheet;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [Route("api/[controller]/[action]")]
    [AllowAnonymous]
    public class CostManagementController : Controller
    {
        private readonly ItemSelectRepository itemRepository;
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;

        public CostManagementController(
            ServiceDocument<CostChangeDetailExceclModel> serviceDocument,
            ItemSelectRepository itemRepository,
            ItemRepository itemRepository1,
            CostChangeRepository costChangeRepository,
            DomainDataRepository domainDataRepository)
        {
            this.itemRepository = itemRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<List<SupplierDomainModel>> SuppliersGet()
        {
            return this.domainDataRepository.SupplierDomainGet().Result;
        }

        [HttpGet]
        public async Task<List<CostChangeItemModel>> CostChangeItemGet(int itemNo, int supplier)
        {
            return await this.itemRepository.SelectCostChgItems(itemNo, supplier, this.serviceDocumentResult);
        }

        [HttpGet]
        public async Task<List<SupplierDomainModel>> SuppliersGetName(string name)
        {
            return await this.domainDataRepository.SupplierDomainGet(name);
        }

        [HttpGet]
        public async Task<AjaxModel<List<CostChangeItemGroupsModel>>> CostChangeItemGetForParentChildItems(string itemNo, long supplier)
        {
            try
            {
                return await this.itemRepository.SelectCostChgItemsParentChildItems(itemNo, supplier);
            }
            catch (Exception ex)
            {
                return new AjaxModel<List<CostChangeItemGroupsModel>> { Result = AjaxResult.Exception, Message = ex.Message, Model = null };
            }
        }

        public async Task<bool> FileUpload([FromBody]FileDataUpload file)
        {
            var tempFile = $"{Guid.NewGuid()}.xlsm";
            DocumentsHelper.FileUpload(string.Empty, tempFile, file.FileData);
            tempFile = $"{RbSettings.RbsExcelFileDownloadPath}\\{tempFile}";
            var bulkItemFile = $"{RbSettings.RbsExcelFileDownloadPath}\\{file.FileName}-{file.RequestId}.xlsm";
            if (System.IO.File.Exists(bulkItemFile))
            {
                System.IO.File.Delete(bulkItemFile);
            }

            System.IO.File.Copy(tempFile, bulkItemFile);
            System.IO.File.Delete(tempFile);

            return true;
        }
    }
}
