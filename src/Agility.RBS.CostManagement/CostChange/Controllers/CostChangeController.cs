﻿// <copyright file="CostChangeController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.CostManagement.CostChange
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Constants;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.CostManagement.CostChange.Models;
    using Agility.RBS.MDM.Repositories;
    using DocumentFormat.OpenXml;
    using DocumentFormat.OpenXml.Packaging;
    using DocumentFormat.OpenXml.Spreadsheet;
    using Microsoft.AspNetCore.Authentication.Cookies;
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    [Route("api/[controller]/[action]")]
    ////[Authorize(AuthenticationSchemes = AuthSchemes)]
    public class CostChangeController : Controller
    {
        private readonly ServiceDocument<CostChangeSearchModel> serviceDocument;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly CostChangeRepository costChangeRepository;
        private readonly DomainDataRepository domainDataRepository;

        public CostChangeController(ServiceDocument<CostChangeSearchModel> serviceDocument, CostChangeRepository costChangeRepository, DomainDataRepository domainDataRepository)
        {
            this.costChangeRepository = costChangeRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in the Cost Change List page.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<CostChangeSearchModel>> List()
        {
            this.serviceDocument.DomainData.Add("costChangeReasons", await this.domainDataRepository.CostChangeReasonsDomainGet());
            this.serviceDocument.DomainData.Add("division", this.domainDataRepository.DivisionDomainGet().Result);
            this.serviceDocument.DomainData.Add("workflowStatusList", this.domainDataRepository.WorkflowStatusGet(RbCommonConstants.CostModule).Result.OrderBy(x => x.Name));
            ////this.serviceDocument.DomainData.Add("tokenTuple", "item1");
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for fetching Cost Change records based on search criteria when Search button clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [Authorize]
        [HttpPost]
        public async Task<ServiceDocument<CostChangeSearchModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.GetListOfCostChangeRequests);
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for future prices in Product Tab of Item Super Screen
        /// </summary>
        /// <param name="itemNo">Item Code</param>
        /// <returns>List oF FuturePriceModel</returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<List<FutureCostModel>> GetFutureCostData(string itemNo)
        {
            return await this.costChangeRepository.GetFutureCostData(itemNo, this.serviceDocumentResult);
        }

        private async Task<List<CostChangeSearchModel>> GetListOfCostChangeRequests()
        {
            try
            {
                List<CostChangeSearchModel> listOfCostChangeReqData = await this.costChangeRepository.CostChangeListGet(this.serviceDocument, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return listOfCostChangeReqData;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }
    }
}