﻿// <copyright file="FutureCostModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.CostManagement.CostChange.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class FutureCostModel
    {
        public string SupName { get; set; }

        public string LocName { get; set; }

        public string LocTypeDesc { get; set; }

        public DateTime? ActiveDate { get; set; }

        public decimal? BaseCost { get; set; }

        public decimal? NetCost { get; set; }

        public decimal? NetNetCost { get; set; }

        public decimal? DeadNetNetCost { get; set; }

        public decimal? PricingCost { get; set; }

        public DateTime? CalcDate { get; set; }
    }
}
