﻿// <copyright file="CostChangeMarketPriceExceclModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.CostManagement.CostChange.Models
{
    using System;
    using System.Collections.Generic;
    using Agility.Framework.Core.Profile.Entities;

    public class CostChangeMarketPriceExceclModel : ProfileEntity
    {
        public string Competitor { get; set; }

        public string CompName { get; set; }

        public string Item { get; set; }

        public int? CompRetail { get; set; }
    }
}
