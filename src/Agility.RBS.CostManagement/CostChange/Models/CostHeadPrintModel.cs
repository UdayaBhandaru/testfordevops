﻿// <copyright file="CostHeadPrintModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.CostManagement.CostChange.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.Framework.Core.Profile.Entities;

    public class CostHeadPrintModel : ProfileEntity
    {
        public CostHeadPrintModel()
        {
            this.CostDetails = new List<CostChangeDetailModel>();
        }

        public int? CostChangeId { get; set; }

        public string CostChangeDescription { get; set; }

        public List<CostChangeDetailModel> CostDetails { get; private set; }

        public int? Reason { get; set; }

        public string CostChangeOrigin { get; set; }

        public string ReasonDescription { get; set; }

        public string CostChangeOriginDescription { get; set; }

        public string CostChangeStatus { get; set; }

        public DateTime? EffectiveStartDate { get; set; }

        public DateTime? EffectiveEndDate { get; set; }

        public DateTime? RunDate { get; set; }

        public DateTime? ApprovalDate { get; set; }

        public string ApprovedBy { get; set; }
    }
}
