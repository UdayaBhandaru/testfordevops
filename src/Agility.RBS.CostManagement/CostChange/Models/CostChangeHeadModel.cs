﻿// <copyright file="CostChangeHeadModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.CostManagement.CostChange.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Inbox.Common;

    [DataProfile(Name = "COSTSUSPSUPHEAD")]
    public class CostChangeHeadModel : ProfileEntity<CostChangeHeadWfModel>, IInboxCommonModel
    {
        public CostChangeHeadModel()
        {
            this.CostChangeDetails = new List<CostChangeDetailModel>();
        }

        [Column("COST_CHANGE")]
        public int? COST_CHANGE { get; set; }

        [NotMapped]
        public string CostChangeDesc { get; set; }

        [NotMapped]
        public int? Reason { get; set; }

        [NotMapped]
        public int? DivisionId { get; set; }

        [NotMapped]
        public string DivisionDesc { get; set; }

        [NotMapped]
        public int? SupplierId { get; set; }

        [NotMapped]
        public string SupplierName { get; set; }

        [NotMapped]
        public string ReasonDesc { get; set; }

        [NotMapped]
        public string CostChangeOrigin { get; set; }

        [NotMapped]
        public string CostChangeStatus { get; set; }

        [NotMapped]
        public string CostChangeStatusDesc { get; set; }

        [NotMapped]
        public DateTime? EffectiveStartDate { get; set; }

        [NotMapped]
        public DateTime? EffectiveEndDate { get; set; }

        [NotMapped]
        public DateTime? RunDate { get; set; }

        [NotMapped]
        public DateTime? ApprovalDate { get; set; }

        [NotMapped]
        public string ApprovedBy { get; set; }

        [NotMapped]
        public string Operation { get; set; }

        [NotMapped]
        [Column("ORGANISATION_ID")]
        public new int? OrganizationId { get; set; }

        [NotMapped]
        [Column("CREATED_BY")]
        public new string CreatedBy { get; set; }

        [NotMapped]
        [Column("LAST_UPDATED_BY")]
        public new string ModifiedBy { get; set; }

        [NotMapped]
        [Column("DeletedInd")]
        public new bool DeletedInd { get; set; }

        [NotMapped]
        [Column("CREATED_DATE")]
        public new DateTimeOffset? CreatedDate { get; set; }

        [NotMapped]
        [Column("LAST_UPDATED_DATE")]
        public new DateTimeOffset? ModifiedDate { get; set; }

        [NotMapped]
        public int? WfNxtStateInd { get; set; }

        [NotMapped]
        public int? LineItemCnt { get; set; }

        [NotMapped]
        public int? NoMarginItemCnt { get; set; }

        [NotMapped]
        public Core.Models.RbWfHeaderModel WorkflowForm { get; set; }

        [NotMapped]
        public int? FileCount { get; set; }

        [NotMapped]
        public int? Rebates { get; set; }

        [NotMapped]
        public List<CostChangeDetailModel> CostChangeDetails { get; private set; }
    }
}
