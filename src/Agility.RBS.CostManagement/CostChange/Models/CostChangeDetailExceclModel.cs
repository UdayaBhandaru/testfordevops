﻿// <copyright file="CostChangeDetailExceclModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.CostManagement.CostChange.Models
{
    using System;
    using System.Collections.Generic;
    using Agility.Framework.Core.Profile.Entities;
    using Newtonsoft.Json;

    public class CostChangeDetailExceclModel : ProfileEntity
    {
        public CostChangeDetailExceclModel()
        {
            this.ListOfCostChangeRspExceclModels = new List<CostChangeRspExceclModel>();
            this.ListOfCostChangeMarketPriceExceclModels = new List<CostChangeMarketPriceExceclModel>();
            this.ListOfCostChangeDetailExceclModels = new List<CostChangeDetailExceclModel>();
        }

        public int? CostChangeId { get; set; }

        public int? DeptId { get; set; }

        public int? SupplierId { get; set; }

        public string DeptDesc { get; set; }

        public int? CategoryId { get; set; }

        public string CategoryDesc { get; set; }

        public string ItemBarcode { get; set; }

        public string ItemDesc { get; set; }

        public int? ItemAttrSales { get; set; }

        public decimal? CaseCost { get; set; }

        public string CostCurCode { get; set; }

        public decimal? UnitCost { get; set; }

        public int AnnualDiscount { get; set; }

        public decimal? NetUnitCost { get; set; }

        public int? Variance { get; set; }

        public int? AverageCategoryMargin { get; set; }

        public int? MarketPrice { get; set; }

        public string Operation { get; set; }

        public List<CostChangeRspExceclModel> ListOfCostChangeRspExceclModels { get; private set; }

        [JsonIgnore]
        public List<CostChangeMarketPriceExceclModel> ListOfCostChangeMarketPriceExceclModels { get; private set; }

        [JsonIgnore]
        public List<CostChangeDetailExceclModel> ListOfCostChangeDetailExceclModels { get; private set; }
    }
}
