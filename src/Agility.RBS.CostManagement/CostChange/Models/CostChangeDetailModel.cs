﻿// <copyright file="CostChangeDetailModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.CostManagement.CostChange.Models
{
    using System;
    using System.Collections.Generic;
    using Agility.Framework.Core.Profile.Entities;

    public class CostChangeDetailModel : ProfileEntity
    {
        public CostChangeDetailModel()
        {
            this.ListOfCostChangeDetails = new List<CostChangeDetailModel>();
        }

        public int? CostChangeId { get; set; }

        public string CostChangeDescription { get; set; }

        public int? SupplierId { get; set; }

        public string SupplierName { get; set; }

        public string OriginCountryId { get; set; }

        public string OriginCountryName { get; set; }

        public string Item { get; set; }

        public string ItemDescription { get; set; }

        public string OrgLevel { get; set; }

        public string OrgLevelDesc { get; set; }

        public int? OrgLevelValue { get; set; }

        public string OrgLevelValueDesc { get; set; }

        public int? CostChangeLineId { get; set; }

        public string CostChangeLineDescription { get; set; }

        public decimal? CurrentUnitCost { get; set; }

        public int? ReasonCode { get; set; }

        public string ReasonDesc { get; set; }

        public decimal? CurrentRsp { get; set; }

        public decimal? NewUnitCost { get; set; }

        public string CostChangeType { get; set; }

        public string ReCalculateOrderIndicator { get; set; }

        public int? Department { get; set; }

        public string DepartmentDescription { get; set; }

        public int? Category { get; set; }

        public string CategoryDescription { get; set; }

        public string LineStatus { get; set; }

        public DateTime? EffectiveStartDate { get; set; }

        public DateTime? EffectiveEndDate { get; set; }

        public int? RetryCount { get; set; }

        public DateTime? ResetDate { get; set; }

        public DateTime? ApprovalDate { get; set; }

        public string ApprovedBy { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }

        public List<CostChangeDetailModel> ListOfCostChangeDetails { get; private set; }

        public decimal? CurrentDiscountPercent { get; set; }

        public decimal? FutureDiscountPercent { get; set; }

        public int? FreeGoodsQty { get; set; }

        public int? FreeGoodsItem { get; set; }

        public decimal? Margin { get; set; }

        public decimal? ExchangeRate { get; set; }

        public string CostCurrencyCode { get; set; }

        public string PriceCurrencyCode { get; set; }

        public string WorkflowStatus { get; set; }

        public string WorkflowStatusDesc { get; set; }
    }
}
