﻿// <copyright file="CostChangeSearchModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.CostManagement.CostChange.Models
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Core.Models;

    public class CostChangeSearchModel : ProfileEntity, Core.IPaginationModel
    {
        public int? COST_CHANGE_ID { get; set; }

        public string CostChangeDesc { get; set; }

        public string CostChangeStatus { get; set; }

        public string CostChangeStatusDesc { get; set; }

        public int? SupplierId { get; set; }

        public string SupName { get; set; }

        public int? DivisionId { get; set; }

        public string DivisionName { get; set; }

        public string ItemBarcode { get; set; }

        public string ItemDesc { get; set; }

        public int? Reason { get; set; }

        public string ReasonDesc { get; set; }

        public int? StartRow { get; set; }

        public int? EndRow { get; set; }

        public long? TotalRows { get; set; }

        public SortModel[] SortModel { get; set; }
    }
}
