﻿// <copyright file="CostManagementStartup.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.CostManagement
{
    using System;
    using System.IO;
    using System.Reflection;
    using Agility.Framework.Core;
    using Agility.RBS.CostManagement.CostChange;
    using Agility.RBS.CostManagement.CostChange.Mapping;
    using AutoMapper;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Caching.Distributed;
    using Microsoft.Extensions.Caching.Memory;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using Swashbuckle.AspNetCore.Swagger;

    public class CostManagementStartup : ComponentStartup<DbContext>
    {
        public void Startup(IConfigurationRoot configuration)
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<CostChangeModelMapping>();
                this.InitilizeMapping(cfg);
            });
        }

        public override void ConfigureServices(IServiceCollection services)
        {
            base.ConfigureServices(services);
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("CostChangeV1", new Info { Title = "My API", Version = "v1" });
                var xmlFile = $"{this.GetType().GetTypeInfo().Assembly.GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }

        public override void Configure(
            IApplicationBuilder app,
            IHostingEnvironment env,
            ILoggerFactory loggerFactory,
            IDistributedCache distributedCache,
            IMemoryCache memoryCache,
            IServiceProvider serviceProvider)
        {
            base.Configure(app, env, loggerFactory, distributedCache, memoryCache, serviceProvider);
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("//swagger//CostChangeV1//swagger.json", "My API v1");
                c.SupportedSubmitMethods();
            });
        }

        public override void ConfigureDependencyInjection(IServiceCollection services)
        {
            services.AddTransient<CostChangeRepository>();
            services.AddTransient<CostChangeController>();
            services.AddTransient<CostChangeHeadController>();
        }
    }
}