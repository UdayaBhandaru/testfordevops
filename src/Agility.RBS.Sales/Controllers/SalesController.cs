﻿// <copyright file="SalesController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Sales.Repository
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Sales.Model;
    using Agility.RBS.Sales.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    [Route("api/[controller]/[action]")]
    public class SalesController : Controller
    {
        private readonly SalesRepository salesRepository;
        private readonly SalesReportRepository salesReportRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;

        public SalesController(SalesRepository salesRepository, SalesReportRepository salesReportRepository)
        {
            this.salesRepository = salesRepository;
            this.salesReportRepository = salesReportRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<List<DivisionTreeModel>> GetMerchTreeview()
        {
            return await this.salesRepository.GetMerchTreeview();
        }

        public async Task<ServiceDocument<SalesReportResultModel>> Search(ServiceDocument<SalesReportSearchModel> sDoc)
        {
            ServiceDocument<SalesReportResultModel> outSDoc = new ServiceDocument<SalesReportResultModel>();
            outSDoc.DataProfile.DataList = await this.salesReportRepository.GetSalesList(sDoc.DataProfile.DataModel, this.serviceDocumentResult);
            return outSDoc;
        }
    }
}
