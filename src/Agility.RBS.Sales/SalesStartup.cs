﻿// <copyright file="SalesStartup.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Sales
{
    using Agility.Framework.Core;
    using Agility.RBS.Sales.Repository;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;

    public class SalesStartup : ComponentStartup<DbContext>
    {
        /// <inheritdoc/>
        public override void ConfigureDependencyInjection(IServiceCollection services)
        {
            services.AddTransient<SalesRepository>();
            services.AddTransient<SalesReportRepository>();
            services.AddTransient<SalesController>();
        }

        public void Startup(Microsoft.Extensions.Configuration.IConfigurationRoot configuration)
        {
            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<Mapping.SalesMapping>();
                this.InitilizeMapping(cfg);
            });
        }
    }
}
