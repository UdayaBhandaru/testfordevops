﻿// <copyright file="SalesMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Sales.Mapping
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.RBS.Core;
    using Agility.RBS.Sales.Model;
    using Agility.RBS.Sales.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class SalesMapping : Profile
    {
        public SalesMapping()
            : base("SalesMapping")
        {
            ////    this.CreateMap<MarchentHierarchieModel, DivisionTreeModel>()
            ////.ForMember(m => m.Division, opt => opt.MapFrom(r => r.Division))
            ////.ForMember(m => m.DivName, opt => opt.MapFrom(r => r.DivName))
            ////.ForMember(m => m.Children, opt => opt.MapFrom(r => new { Dept = r.Dept, DeptName = r.DeptName }));

            this.CreateMap<OracleObject, DivisionTreeModel>()
          .ForMember(m => m.Division, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DIVISION"])))
          .ForMember(m => m.Id, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DIVISION"])))
          .ForMember(m => m.DivName, opt => opt.MapFrom(r => r["DIVISION_NAME"]))
            .ForMember(m => m.Name, opt => opt.MapFrom(r => r["DIVISION_NAME"]));

            this.CreateMap<OracleObject, GroupTreeModel>()
         .ForMember(m => m.GroupNo, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["GROUP_ID"])))
         .ForMember(m => m.Id, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["GROUP_ID"])))
          .ForMember(m => m.Name, opt => opt.MapFrom(r => r["GROUP_NAME"]))
         .ForMember(m => m.GroupName, opt => opt.MapFrom(r => r["GROUP_NAME"]));

            this.CreateMap<OracleObject, DptTreeModel>()
           .ForMember(m => m.Dept, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DEPT"])))
           .ForMember(m => m.Id, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DEPT"])))
           .ForMember(m => m.Name, opt => opt.MapFrom(r => r["DEPT_NAME"]))
           .ForMember(m => m.DeptName, opt => opt.MapFrom(r => r["DEPT_NAME"]));

            this.CreateMap<OracleObject, SalesReportResultModel>()
           .ForMember(m => m.Company, opt => opt.MapFrom(r => r["company"]))
           .ForMember(m => m.CompanyName, opt => opt.MapFrom(r => r["company_name"]))
           .ForMember(m => m.Format, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["format"])))
            .ForMember(m => m.FormatName, opt => opt.MapFrom(r => r["format_name"]))
           .ForMember(m => m.Store, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["store"])))
           .ForMember(m => m.StoreName, opt => opt.MapFrom(r => r["store_name"]))
            .ForMember(m => m.TotalRetail, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["total_retail"])))
           .ForMember(m => m.TotalCost, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["total_cost"])))
           .ForMember(m => m.Margin, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["margin"])))
            .ForMember(m => m.LyTotalRetail, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["ly_total_retail"])))
           .ForMember(m => m.LyTotalCost, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["ly_total_cost"])))
           .ForMember(m => m.LyMargin, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["ly_margin"])))
            .ForMember(m => m.TabType, opt => opt.MapFrom(r => r["tab_type"]))
           .ForMember(m => m.PError, opt => opt.MapFrom(r => r["p_error"]))
           .ForMember(m => m.ProgramPhase, opt => opt.MapFrom(r => r["program_phase"]));
        }
    }
}
