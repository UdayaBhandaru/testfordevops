﻿// <copyright file="SalesRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Sales.Repository
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Sales.Model;
    using Agility.RBS.Sales.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class SalesRepository : BaseOraclePackage
    {
        private readonly ServiceDocumentResult serviceDocumentResult;

        public SalesRepository()
        {
            this.PackageName = "MERCH_HIERARCHY";
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<List<DivisionTreeModel>> GetMerchTreeview()
        {
            this.Connection.Open();
            PackageParams packageParameter = this.GetPackageParams("mh_tv_division_tab", "getmerchtreeview");
            OracleType recordType = OracleType.GetObjectType("MH_TV_DIVISION_REC", this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            OracleTable divisionTab = await this.GetProcedure(recordObject, packageParameter, this.serviceDocumentResult);
            List<DivisionTreeModel> lstDivision = Mapper.Map<List<DivisionTreeModel>>(divisionTab);
            if (lstDivision != null)
            {
                for (int i = 0; i < lstDivision.Count; i++)
                {
                    OracleObject obj = (OracleObject)divisionTab[i];
                    OracleTable groupTab = (Devart.Data.Oracle.OracleTable)obj["group_tab"];
                    if (groupTab.Count > 0)
                    {
                        var childs = Mapper.Map<List<GroupTreeModel>>(groupTab);
                        childs.ForEach(a => a.Division = lstDivision[i].Division);
                        lstDivision[i].Children.AddRange(childs);

                        for (int j = 0; j < lstDivision[i].Children.Count; j++)
                        {
                            OracleObject groupObj = (OracleObject)groupTab[j];
                            OracleTable deptTab = (Devart.Data.Oracle.OracleTable)groupObj["DEPT_TAB"];
                            if (deptTab.Count > 0)
                            {
                                var childsDpt = Mapper.Map<List<DptTreeModel>>(deptTab);
                                childsDpt.ForEach(a => a.Group = lstDivision[i].Children[j].GroupNo);
                                childsDpt.ForEach(a => a.Division = lstDivision[i].Division);
                                lstDivision[i].Children[j].Children.AddRange(childsDpt);
                            }
                        }
                    }
                }
            }

            return lstDivision;
        }
    }
}
