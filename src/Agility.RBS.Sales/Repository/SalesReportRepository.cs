﻿// <copyright file="SalesReportRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Sales.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Sales.Model;
    using Agility.RBS.Sales.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class SalesReportRepository : BaseOraclePackage
    {
        private readonly ServiceDocumentResult serviceDocumentResult;

        public SalesReportRepository()
        {
            this.PackageName = "UTL_SALES_WRP";
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<List<SalesReportResultModel>> GetSalesList(SalesReportSearchModel salesModel, ServiceDocumentResult serviceDocumentResult)
        {
            switch (salesModel.MerHierarchy?[0].Type)
            {
                case "DIVISION":
                    salesModel.Division = salesModel.MerHierarchy?[0].Id;
                    break;
                case "GROUP":
                    salesModel.Division = salesModel.MerHierarchy[0].Division;
                    salesModel.Dept = salesModel.MerHierarchy[0].Id;
                    break;
                case "DPT":
                    salesModel.Division = salesModel.MerHierarchy[0].Division;
                    salesModel.Dept = salesModel.MerHierarchy[0].Group;
                    salesModel.Category = salesModel.MerHierarchy[0].Id;
                    break;
            }

            this.Connection.Open();
            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();
            OracleParameter parameter;
            PackageParams packageParameter = this.GetPackageParams("sales_report_tab", "GET_STORE_SALES_REPORT");
            OracleObject recordObject = this.SetSalesSearch(salesModel);

            OracleType tableType = OracleType.GetObjectType("sales_report_search_Tab", this.Connection);
            OracleTable pUomClassMstTab = new OracleTable(tableType);
            pUomClassMstTab.Add(recordObject);

            parameter = new OracleParameter("p_sales_report_search_Tab", Devart.Data.Oracle.OracleDbType.Table)
            {
                Direction = ParameterDirection.Input,
                ObjectTypeName = "sales_report_search_Tab",
                Value = pUomClassMstTab
            };

            OracleType recordType = OracleType.GetObjectType("sales_report_rec", this.Connection);
            OracleObject salesReportRecordObject = new OracleObject(recordType);
            parameters.Add(parameter);
            return await this.GetProcedure2<SalesReportResultModel>(salesReportRecordObject, packageParameter, this.serviceDocumentResult, parameters);
        }

        public OracleObject SetSalesSearch(SalesReportSearchModel salesModel)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType("sales_report_search_rec", this.Connection);
            OracleObject recordObject = new OracleObject(recordType);

            recordObject[recordType.Attributes["from_date"]] = salesModel.FromDate; ////salesModel.FromDate;
            recordObject[recordType.Attributes["to_date"]] = salesModel.ToDate;
            recordObject[recordType.Attributes["division"]] = salesModel.Division;
            recordObject[recordType.Attributes["div_name"]] = salesModel.DivName;
            recordObject[recordType.Attributes["dept"]] = salesModel.Dept;
            recordObject[recordType.Attributes["dept_name"]] = salesModel.DeptName;
            recordObject[recordType.Attributes["category"]] = salesModel.Category;
            recordObject[recordType.Attributes["category_name"]] = salesModel.CategoryName;
            recordObject[recordType.Attributes["class"]] = salesModel.Class;
            recordObject[recordType.Attributes["class_name"]] = salesModel.ClassName;
            recordObject[recordType.Attributes["subclass"]] = salesModel.Subclass;
            recordObject[recordType.Attributes["subclass_name"]] = salesModel.SubclassName;
            recordObject[recordType.Attributes["format"]] = salesModel.Format;
            recordObject[recordType.Attributes["format_name"]] = salesModel.FormatName;
            recordObject[recordType.Attributes["location"]] = salesModel.Location;
            recordObject[recordType.Attributes["loc_name"]] = salesModel.LocName;
            recordObject[recordType.Attributes["tab_type"]] = salesModel.TabType;
            return recordObject;
        }
    }
}
