﻿// <copyright file="SalesReportSearchModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Sales.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Sales.Models;

    public class SalesReportSearchModel : ProfileEntity
    {
        public SalesReportSearchModel()
        {
            this.MerHierarchy = new List<MerchHierarchyNodeModel>();
        }

        [Key]
        public int? Division { get; set; }

        public int? Dept { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        public string DivName { get; set; }

        public string DeptName { get; set; }

        public int? Category { get; set; }

        public string CategoryName { get; set; }

        public int? Class { get; set; }

        public string ClassName { get; set; }

        public int? Subclass { get; set; }

        public string SubclassName { get; set; }

        public int? Format { get; set; }

        public string FormatName { get; set; }

        public int? Location { get; set; }

        public string LocName { get; set; }

        public string TabType { get; set; }

        public List<MerchHierarchyNodeModel> MerHierarchy { get; internal set; }
    }
}
