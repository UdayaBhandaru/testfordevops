﻿// <copyright file="GroupTreeModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Sales.Models
{
    using System;
    using System.Collections.Generic;

    public class GroupTreeModel
    {
        public GroupTreeModel()
        {
            this.Children = new List<DptTreeModel>();
            this.Type = "GROUP";
        }

        public List<DptTreeModel> Children { get; internal set; }

        public int? GroupNo { get; internal set; }

        public string GroupName { get; internal set; }

        public string Name { get; internal set; }

        public int? Division { get; set; }

        public int? Id { get; set; }

        public string Type { get; internal set; }
    }
}
