﻿// <copyright file="DivisionTreeModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Sales.Models
{
    using System;
    using System.Collections.Generic;

    public class DivisionTreeModel
    {
        public DivisionTreeModel()
        {
            this.Children = new List<GroupTreeModel>();
            this.Type = "DIVISION";
        }

        public int? Division { get; set; }

        public string DivName { get; set; }

        public List<GroupTreeModel> Children { get; internal set; }

        public int? ParentId { get; set; }

        public string Name { get; internal set; }

        public int? Id { get; set; }

        public string Type { get; internal set; }
    }
}
