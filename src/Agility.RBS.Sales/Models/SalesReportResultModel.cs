﻿// <copyright file="SalesReportResultModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Sales.Model
{
    using System;
    using Agility.Framework.Core.Profile.Entities;

    public class SalesReportResultModel : ProfileEntity
    {
        public string Company { get; set; }

        public string CompanyName { get; set; }

        public int? Format { get; set; }

        public string FormatName { get; set; }

        public int? Store { get; set; }

        public string StoreName { get; set; }

        public decimal? TotalRetail { get; set; }

        public decimal TotalCost { get; set; }

        public decimal? Margin { get; set; }

        public decimal? LyTotalRetail { get; set; }

        public decimal? LyTotalCost { get; set; }

        public decimal? LyMargin { get; set; }

        public string TabType { get; set; }

        public string ProgramPhase { get; set; }

        public string PrograMessage { get; set; }

        public string PError { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }
    }
}
