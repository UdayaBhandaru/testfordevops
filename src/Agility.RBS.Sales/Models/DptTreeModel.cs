﻿// <copyright file="DptTreeModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Sales.Models
{
    using System;
    using System.Collections.Generic;

    public class DptTreeModel
    {
        public DptTreeModel()
        {
            this.Children = new List<string>();
            this.Type = "DPT";
        }

        public int? Dept { get; set; }

        public string DeptName { get; set; }

        public List<string> Children { get; internal set; }

        public int? Group { get; set; }

        public string Name { get; internal set; }

        public int? Id { get; set; }

        public string Type { get; internal set; }

        public int? Division { get; internal set; }
    }
}
