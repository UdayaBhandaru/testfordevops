﻿// <copyright file="MerchHierarchyNodeModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Sales.Models
{
    public class MerchHierarchyNodeModel
    {
        public int? Division { get; set; }

        public int? Group { get; set; }

        public int? ParentId { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }

        public int? Id { get; set; }
    }
}
