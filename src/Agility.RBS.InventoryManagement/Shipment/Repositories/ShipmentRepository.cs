﻿// <copyright file="ShipmentRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InventoryManagement.Shipment.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Text;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.InventoryManagement.Common;
    using Agility.RBS.InventoryManagement.Shipment.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class ShipmentRepository : BaseOraclePackage
    {
        public ShipmentRepository()
        {
            this.PackageName = InventoryManagementConstants.GetShipmentPackage;
        }

        public async Task<List<ShipmentSearchModel>> GetShipmentDetails(ServiceDocument<ShipmentSearchModel> serviceDoc, ServiceDocumentResult serviceDocumentResult)
        {
            ShipmentSearchModel shipmentSearch = serviceDoc.DataProfile.DataModel;
            PackageParams packageParameter = this.GetPackageParams(InventoryManagementConstants.ObjTypeShipmentSearch, InventoryManagementConstants.GetProcShipmentSearch);
            OracleObject recordObject = this.SetParamsShipment(shipmentSearch);
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();

            BaseOracleParameter baseOracleParameter;
            baseOracleParameter = new BaseOracleParameter { Name = "P_START", DataType = OracleDbType.Number, Direction = ParameterDirection.Input, Value = Convert.ToString(shipmentSearch.StartRow) };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            baseOracleParameter = new BaseOracleParameter { Name = "P_END", DataType = OracleDbType.Number, Direction = ParameterDirection.Input, Value = Convert.ToString(shipmentSearch.EndRow) };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            if (shipmentSearch.SortModel == null)
            {
                shipmentSearch.SortModel = new[] { new Core.Models.SortModel { ColId = null, Sort = null } };
            }

            baseOracleParameter = new BaseOracleParameter { Name = "P_SortColId", DataType = OracleDbType.VarChar, Direction = ParameterDirection.Input, Value = shipmentSearch.SortModel[0].ColId };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            baseOracleParameter = new BaseOracleParameter { Name = "P_SortOrder", DataType = OracleDbType.VarChar, Direction = ParameterDirection.Input, Value = shipmentSearch.SortModel[0].Sort };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            baseOracleParameter = new BaseOracleParameter { Name = "P_total", DataType = OracleDbType.Number, Direction = ParameterDirection.Output };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            parameters.Add(this.ParameterBuilder(UserProfileHelper.GetUserParameter()));
            var shipmentlist = await this.GetProcedure2<ShipmentSearchModel>(recordObject, packageParameter, serviceDocumentResult, parameters);
            shipmentSearch.TotalRows = (this.Parameters["p_total"].Value != null && this.Parameters["p_total"].Value != DBNull.Value) ? Convert.ToInt64(this.Parameters["p_total"].Value) : 1;
            serviceDoc.DataProfile.DataModel = shipmentSearch;
            return shipmentlist;
        }

        public async Task<ShipmentHeaderModel> ShipmentDetailsGet(int shipmentNo, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType(InventoryManagementConstants.RecordObjShipmentDetails);
            OracleObject recordObject = this.GetOracleObject(recordType);
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();

            PackageParams packageParameter = this.GetPackageParams(InventoryManagementConstants.ObjTypeShipmentDetails, InventoryManagementConstants.SetProcShipmentDetails);

            parameters.Add(this.ParameterBuilder(UserProfileHelper.GetUserParameter()));
            BaseOracleParameter baseOracleParameter;
            baseOracleParameter = new BaseOracleParameter { Name = "SHIPMENT", DataType = OracleDbType.Number, Direction = ParameterDirection.Input, Value = Convert.ToString(shipmentNo) };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            var pItemTab = await this.GetProcedure(recordObject, packageParameter, serviceDocumentResult, parameters);

            var lstHeader = Mapper.Map<List<ShipmentHeaderModel>>(pItemTab);
            if (lstHeader != null)
            {
                for (int i = 0; i < lstHeader.Count; i++)
                {
                    OracleObject obj = (OracleObject)pItemTab[i];
                    OracleTable shipmentDetailsObj = (Devart.Data.Oracle.OracleTable)obj["SHP_DTL_TAb"];
                    if (shipmentDetailsObj.Count > 0)
                    {
                        lstHeader[i].ShipmentDetails.AddRange(Mapper.Map<List<ShipmentDetailModel>>(shipmentDetailsObj));
                    }
                }
            }

            return lstHeader?[0];
        }

        private OracleObject SetParamsShipment(ShipmentSearchModel shipmentsearchModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("SHIPMENT_SEARCH_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            recordObject["SHIPMENT"] = shipmentsearchModel.Shipment;
            recordObject["ORDER_NO"] = shipmentsearchModel.OrderNo;
            recordObject["BOL_NO"] = shipmentsearchModel.BolNo;
            recordObject["ASN"] = shipmentsearchModel.Asn;
            recordObject["SUPPLIER"] = shipmentsearchModel.Supplier;
            recordObject["SHIP_DATE_BEFORE"] = shipmentsearchModel.ShipDateBefore;
            recordObject["SHIP_DATE_AFTER"] = shipmentsearchModel.ShipDateAfter;
            recordObject["RECEIVE_DATE_BEFORE"] = shipmentsearchModel.ReceiveDateBefore;
            recordObject["RECEIVE_DATE_AFTER"] = shipmentsearchModel.ReceiveDateAfter;
            recordObject["STATUS_CODE"] = shipmentsearchModel.StatusCode;
            recordObject["TO_LOC"] = shipmentsearchModel.ToLoc;
            recordObject["TO_LOC_NAME"] = shipmentsearchModel.ToLocName;
            recordObject["ITEM"] = shipmentsearchModel.Item;
            recordObject["LANG_ID"] = shipmentsearchModel.LongId;
            return recordObject;
        }
    }
}
