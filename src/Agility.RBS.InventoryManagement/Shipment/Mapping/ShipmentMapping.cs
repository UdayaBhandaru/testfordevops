﻿// <copyright file="ShipmentMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InventoryManagement.Shipment.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.RBS.Core;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class ShipmentMapping : Profile
    {
        public ShipmentMapping()
           : base("ShipmentMapping")
        {
            this.CreateMap<OracleObject, ShipmentSearchModel>()
                .ForMember(m => m.Shipment, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["SHIPMENT"])))
                .ForMember(m => m.OrderNo, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["ORDER_NO"])))
                .ForMember(m => m.BolNo, opt => opt.MapFrom(r => r["BOL_NO"]))
                .ForMember(m => m.Asn, opt => opt.MapFrom(r => r["ASN"]))
                .ForMember(m => m.Supplier, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["SUPPLIER"])))
                .ForMember(m => m.ShipDateBefore, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["SHIP_DATE_BEFORE"])))
                .ForMember(m => m.ShipDateAfter, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["SHIP_DATE_AFTER"])))
                .ForMember(m => m.ReceiveDateBefore, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["RECEIVE_DATE_BEFORE"])))
                .ForMember(m => m.ReceiveDateAfter, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["RECEIVE_DATE_AFTER"])))
                .ForMember(m => m.StatusCode, opt => opt.MapFrom(r => r["STATUS_CODE"]))
                .ForMember(m => m.StatusDesc, opt => opt.MapFrom(r => r["STATUS_DESC"]))
                .ForMember(m => m.ToLoc, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["TO_LOC"])))
                .ForMember(m => m.ToLocName, opt => opt.MapFrom(r => r["TO_LOC_NAME"]))
                .ForMember(m => m.Item, opt => opt.MapFrom(r => r["ITEM"]))
                .ForMember(m => m.LongId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["LANG_ID"])))
                .ForMember(m => m.ProgramPhase, opt => opt.MapFrom(r => r["PROGRAM_PHASE"]))
                .ForMember(m => m.ProgramMessage, opt => opt.MapFrom(r => r["PROGRAM_MESSAGE"]))
                .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_ERROR"]))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "SHIPMENT"))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, ShipmentHeaderModel>()
               .ForMember(m => m.Shipment, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["SHIPMENT"])))
               .ForMember(m => m.OrderNo, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["ORDER_NO"])))
               .ForMember(m => m.ShipDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["SHIP_DATE"])))
               .ForMember(m => m.ReceiveDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["RECEIVE_DATE"])))
               .ForMember(m => m.ShipOrgin, opt => opt.MapFrom(r => r["SHIP_ORIGIN"]))
               .ForMember(m => m.ShipOrginDesc, opt => opt.MapFrom(r => r["SHIP_ORIGIN_DESC"]))
               .ForMember(m => m.ToLoc, opt => opt.MapFrom(r => r["TO_LOC"]))
               .ForMember(m => m.ToLocName, opt => opt.MapFrom(r => r["TO_LOC_NAME"]))
               .ForMember(m => m.NotBeforeDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["NOT_BEFORE_DATE"])))
               .ForMember(m => m.NotAfterDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["NOT_AFTER_DATE"])))
               .ForMember(m => m.StatusCode, opt => opt.MapFrom(r => r["STATUS_CODE"]))
               .ForMember(m => m.StatusDesc, opt => opt.MapFrom(r => r["STATUS_DESC"]))
               .ForMember(m => m.Courier, opt => opt.MapFrom(r => r["COURIER"]))
               .ForMember(m => m.NoOfBoxes, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["NO_OF_BOXES"])))
               .ForMember(m => m.Asn, opt => opt.MapFrom(r => r["ASN"]))
               .ForMember(m => m.Comments, opt => opt.MapFrom(r => r["COMMENTS"]))
               .ForMember(m => m.LongId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["LANG_ID"])))
               .ForMember(m => m.ProgramPhase, opt => opt.MapFrom(r => r["PROGRAM_PHASE"]))
               .ForMember(m => m.ProgramMessage, opt => opt.MapFrom(r => r["PROGRAM_MESSAGE"]))
               .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_ERROR"]))
               .ForMember(m => m.TableName, opt => opt.MapFrom(r => "SHIPMENTHEADER"))
               .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, ShipmentDetailModel>()
               .ForMember(m => m.Shipment, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["SHIPMENT"])))
               .ForMember(m => m.SeqNo, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["SEQ_NO"])))
               .ForMember(m => m.Item, opt => opt.MapFrom(r => r["ITEM"]))
               .ForMember(m => m.ItemDesc, opt => opt.MapFrom(r => r["ITEM_DESC"]))
               .ForMember(m => m.DistroNo, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DISTRO_NO"])))
               .ForMember(m => m.CartonNO, opt => opt.MapFrom(r => r["CARTON_NO"]))
               .ForMember(m => m.QuantityExpected, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["QUANTITY_EXPECTED"])))
               .ForMember(m => m.QuantityReceived, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["QUANTITY_RECEIVED"])))
               .ForMember(m => m.TemperedInd, opt => opt.MapFrom(r => r["TEMPERED_IND"]))
               .ForMember(m => m.ActualReceivingStore, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["ACTUAL_RECEIVING_STORE"])))
               .ForMember(m => m.ReconcileDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["RECONCILE_DATE"])))
               .ForMember(m => m.ReconcileUser, opt => opt.MapFrom(r => r["RECONCILE_USER"]))
               .ForMember(m => m.QuantityMatched, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["QUANTITY_MATCHED"])))
               .ForMember(m => m.MatchInvoiceId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["MATCH_INVOICE_ID"])))
               .ForMember(m => m.LongId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["LANG_ID"])))
               .ForMember(m => m.ProgramPhase, opt => opt.MapFrom(r => r["PROGRAM_PHASE"]))
               .ForMember(m => m.ProgramMessage, opt => opt.MapFrom(r => r["PROGRAM_MESSAGE"]))
               .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_ERROR"]))
               .ForMember(m => m.TableName, opt => opt.MapFrom(r => "SHIPMENTHEADERDETAIL"))
               .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));
        }
    }
}
