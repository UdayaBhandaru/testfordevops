﻿// <copyright file="ShipmentHeaderModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InventoryManagement.Shipment.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Core.Models;

    public class ShipmentHeaderModel : ProfileEntity
    {
        public ShipmentHeaderModel()
        {
            this.ShipmentDetails = new List<ShipmentDetailModel>();
        }

        [Column("SHIPMENT")]
        public int? Shipment { get; set; }

        [NotMapped]
        public int? OrderNo { get; set; }

        [NotMapped]
        public DateTime? ShipDate { get; set; }

        [NotMapped]
        public DateTime? ReceiveDate { get; set; }

        [NotMapped]
        public string ShipOrgin { get; set; }

        [NotMapped]
        public string ShipOrginDesc { get; set; }

        [NotMapped]
        public string ToLoc { get; set; }

        [NotMapped]
        public string ToLocName { get; set; }

        [NotMapped]
        public DateTime? NotBeforeDate { get; set; }

        [NotMapped]
        public DateTime? NotAfterDate { get; set; }

        [NotMapped]
        public string StatusCode { get; set; }

        [NotMapped]
        public string StatusDesc { get; set; }

        [NotMapped]
        public string Courier { get; set; }

        [NotMapped]
        public int? NoOfBoxes { get; set; }

        [NotMapped]
        public string Asn { get; set; }

        [NotMapped]
        public string Comments { get; set; }

        [NotMapped]
        public int? LongId { get; set; }

        [NotMapped]
        public string ProgramPhase { get; set; }

        [NotMapped]
        public string ProgramMessage { get; set; }

        [NotMapped]
        public string Error { get; set; }

        public List<ShipmentDetailModel> ShipmentDetails { get; private set; }

        [NotMapped]
        public string Operation { get; set; }

        [NotMapped]
        public string TableName { get; set; }
    }
}
