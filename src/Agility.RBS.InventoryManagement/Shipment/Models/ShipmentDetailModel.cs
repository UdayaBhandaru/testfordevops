﻿// <copyright file="ShipmentDetailModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InventoryManagement.Shipment.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Core.Models;

    public class ShipmentDetailModel : ProfileEntity
    {
        [NotMapped]
        public int? Shipment { get; set; }

        [NotMapped]
        public int? SeqNo { get; set; }

        [NotMapped]
        public string Item { get; set; }

        [NotMapped]
        public string ItemDesc { get; set; }

        [NotMapped]
        public int? DistroNo { get; set; }

        [NotMapped]
        public string CartonNO { get; set; }

        [NotMapped]
        public int? QuantityExpected { get; set; }

        [NotMapped]
        public int? QuantityReceived { get; set; }

        [NotMapped]
        public string TemperedInd { get; set; }

        [NotMapped]
        public int? ActualReceivingStore { get; set; }

        [NotMapped]
        public DateTime? ReconcileDate { get; set; }

        [NotMapped]
        public string ReconcileUser { get; set; }

        [NotMapped]
        public int? QuantityMatched { get; set; }

        [NotMapped]
        public int? MatchInvoiceId { get; set; }

        [NotMapped]
        public int? LongId { get; set; }

        [NotMapped]
        public string ProgramPhase { get; set; }

        [NotMapped]
        public string ProgramMessage { get; set; }

        [NotMapped]
        public string Error { get; set; }

        [NotMapped]
        public string Operation { get; set; }

        [NotMapped]
        public string TableName { get; set; }
    }
}
