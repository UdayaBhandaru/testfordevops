﻿// <copyright file="ShipmentSearchModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InventoryManagement.Shipment.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Core.Models;

    public class ShipmentSearchModel : ProfileEntity
    {
        [NotMapped]
        public int? Shipment { get; set; }

        [NotMapped]
        public int? OrderNo { get; set; }

        [NotMapped]
        public string BolNo { get; set; }

        [NotMapped]
        public string Asn { get; set; }

        [NotMapped]
        public int? Supplier { get; set; }

        [NotMapped]
        public DateTime? ShipDateBefore { get; set; }

        [NotMapped]
        public DateTime? ShipDateAfter { get; set; }

        [NotMapped]
        public DateTime? ReceiveDateBefore { get; set; }

        [NotMapped]
        public DateTime? ReceiveDateAfter { get; set; }

        [NotMapped]
        public string StatusCode { get; set; }

        [NotMapped]
        public string StatusDesc { get; set; }

        [NotMapped]
        public int? ToLoc { get; set; }

        [NotMapped]
        public string ToLocName { get; set; }

        [NotMapped]
        public string Item { get; set; }

        [NotMapped]
        public int? LongId { get; set; }

        [NotMapped]
        public string ProgramPhase { get; set; }

        [NotMapped]
        public string ProgramMessage { get; set; }

        [NotMapped]
        public string Error { get; set; }

        [NotMapped]
        public string Operation { get; set; }

        [NotMapped]
        public string TableName { get; set; }

        [NotMapped]
        public int? StartRow { get; set; }

        [NotMapped]
        public int? EndRow { get; set; }

        [NotMapped]
        public long? TotalRows { get; set; }

        [NotMapped]
        public SortModel[] SortModel { get; set; }

        [NotMapped]
        public string User { get; set; }

        [NotMapped]
        public string TypeId { get; set; }
    }
}
