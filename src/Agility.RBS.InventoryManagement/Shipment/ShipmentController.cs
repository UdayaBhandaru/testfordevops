﻿// <copyright file="ShipmentController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InventoryManagement.Shipment
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.InventoryManagement.Common;
    using Agility.RBS.InventoryManagement.Shipment.Models;
    using Agility.RBS.InventoryManagement.Shipment.Repositories;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.UtilityDomain;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class ShipmentController : Controller
    {
        private readonly ServiceDocument<ShipmentSearchModel> serviceDocument;
        private readonly ShipmentRepository shipmentRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly DomainDataRepository domainDataRepository;

        public ShipmentController(
            ServiceDocument<ShipmentSearchModel> serviceDocument,
            ShipmentRepository shipmentRepository,
            DomainDataRepository domainDataRepository)
        {
            this.shipmentRepository = shipmentRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<ShipmentSearchModel>> List()
        {
            return await this.GetDomainData();
        }

        public async Task<ServiceDocument<ShipmentSearchModel>> New()
        {
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<ShipmentSearchModel>> Save()
        {
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<ShipmentSearchModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.ShipmentSearch);
            return this.serviceDocument;
        }

        private async Task<List<ShipmentSearchModel>> ShipmentSearch()
        {
            try
            {
                var lstshipment = await this.shipmentRepository.GetShipmentDetails(this.serviceDocument, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstshipment;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<ServiceDocument<ShipmentSearchModel>> GetDomainData()
        {
            this.serviceDocument.DomainData.Add("status", this.domainDataRepository.DomainDetailData("SHPS").Result);
            this.serviceDocument.DomainData.Add("location", await this.domainDataRepository.LocationDomainGet());
            this.serviceDocument.DomainData.Add("supplier", await this.domainDataRepository.SupplierDomainGet());
            this.serviceDocument.DomainData.Add("type", this.domainDataRepository.DomainDetailData("RCST").Result);
            return this.serviceDocument;
        }
    }
}
