﻿// <copyright file="ShipmentDetailController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InventoryManagement.Shipment.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.InventoryManagement.Common;
    using Agility.RBS.InventoryManagement.Shipment.Models;
    using Agility.RBS.InventoryManagement.Shipment.Repositories;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.UtilityDomain;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

   public class ShipmentDetailController : Controller
    {
        private readonly ServiceDocument<ShipmentHeaderModel> serviceDocument;
        private readonly ShipmentRepository shipmentRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private int shipmentNo;

        public ShipmentDetailController(
            ServiceDocument<ShipmentHeaderModel> serviceDocument,
            ShipmentRepository shipmentRepository)
        {
            this.shipmentRepository = shipmentRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        [HttpGet]
        public async Task<ServiceDocument<ShipmentHeaderModel>> Open(int id)
        {
            this.shipmentNo = id;
            await this.serviceDocument.OpenAsync(this.ShipmentOpen);
            this.serviceDocument.Result = this.serviceDocumentResult;
            return this.serviceDocument;
        }

        private async Task<ShipmentHeaderModel> ShipmentOpen()
        {
            try
            {
                var shipment = await this.shipmentRepository.ShipmentDetailsGet(this.shipmentNo, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return shipment;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }
    }
}
