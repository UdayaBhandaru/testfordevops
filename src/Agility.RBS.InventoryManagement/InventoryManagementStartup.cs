﻿// <copyright file="InventoryManagementStartup.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InventoryManagement
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;
    using System.Text;
    using Agility.Framework.Core;
    using Agility.RBS.Core.Services;
    using Agility.RBS.InventoryManagement.InventoryAdjustment;
    using Agility.RBS.InventoryManagement.Repositories;
    using Agility.RBS.InventoryManagement.ReturntoVendor;
    using Agility.RBS.InventoryManagement.ReturntoVendor.Mapping;
    using Agility.RBS.InventoryManagement.ReturntoVendor.Repositories;
    using Agility.RBS.InventoryManagement.Shipment;
    using Agility.RBS.InventoryManagement.Shipment.Repositories;
    using Agility.RBS.InventoryManagement.TransferManagement;
    using Agility.RBS.InventoryManagement.TransferManagement.Mapping;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Caching.Distributed;
    using Microsoft.Extensions.Caching.Memory;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using Swashbuckle.AspNetCore.Swagger;

    public class InventoryManagementStartup : ComponentStartup<DbContext>
    {
        public void Startup(Microsoft.Extensions.Configuration.IConfigurationRoot configuration)
        {
            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<ReturntoVendorMapping>();
                cfg.AddProfile<TransferModelMapping>();
                this.InitilizeMapping(cfg);
            });
        }

        public override void Configure(
           IApplicationBuilder app,
           IHostingEnvironment env,
           ILoggerFactory loggerFactory,
           IDistributedCache distributedCache,
           IMemoryCache memoryCache,
           IServiceProvider serviceProvider)
        {
            base.Configure(app, env, loggerFactory, distributedCache, memoryCache, serviceProvider);
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("//swagger//InventoryV1//swagger.json", "My API v1");
                c.SupportedSubmitMethods();
            });
        }

        public override void ConfigureServices(IServiceCollection services)
        {
            base.ConfigureServices(services);
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("InventoryV1", new Info { Title = "My API", Version = "v1" });
                var xmlFile = $"{this.GetType().GetTypeInfo().Assembly.GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }

        public override void ConfigureDependencyInjection(IServiceCollection services)
        {
            services.AddTransient<InvAdjustmentController>();
            services.AddTransient<InventoryRepository>();
            services.AddTransient<ReturntoVendorController>();
            services.AddTransient<ReturntoVendorRepository>();
            services.AddTransient<TransferHeadController>();
            services.AddTransient<TransferDetailsController>();
            services.AddTransient<TransferSearchController>();
            services.AddTransient<TransferRepository>();
            services.AddTransient<ShipmentController>();
            services.AddTransient<ShipmentRepository>();
        }
    }
}
