﻿// <copyright file="InventoryRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InventoryManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.InventoryManagement.Common;
    using Agility.RBS.InventoryManagement.InventoryAdjustment.Models;
    using Devart.Data.Oracle;

    public class InventoryRepository : BaseOraclePackage
    {
        public InventoryRepository()
        {
            this.PackageName = InventoryManagementConstants.GetInventoryPackage;
        }

        public async Task<ServiceDocumentResult> SetInventoryAdjustment(InvAdjustmentModel invAdjustmentModel)
        {
            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();
            PackageParams packageParameter = this.GetPackageParams(InventoryManagementConstants.ObjTypeInvAdjustmentByItem, InventoryManagementConstants.SetProcInvAdjustmentByItem);
            OracleParameter parameter;
            parameter = new OracleParameter("RESULT", OracleDbType.Boolean)
            {
                Direction = System.Data.ParameterDirection.ReturnValue
            };
            parameters.Add(parameter);
            parameter = new OracleParameter("P_USERID", OracleDbType.VarChar)
            {
                Direction = System.Data.ParameterDirection.Input,
                Value = FxContext.Context.Name
            };
            parameters.Add(parameter);
            parameter = new OracleParameter(packageParameter.ObjectTypeTable, OracleDbType.Table)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                ObjectTypeName = packageParameter.ObjectTypeName,
                Value = this.SetParamsInventoryAdjustment(invAdjustmentModel)
            };
            parameters.Add(parameter);
            return await this.SetProcedure(null, packageParameter, parameters);
        }

        public virtual OracleTable SetParamsInventoryAdjustment(InvAdjustmentModel invAdjustmentModel)
        {
            this.Connection.Open();
            OracleType pInvAdjByItemTableType = OracleType.GetObjectType(InventoryManagementConstants.ObjTypeInvAdjustmentByItem, this.Connection);
            OracleType recordType = OracleType.GetObjectType(InventoryManagementConstants.RecordObjInvAdjustmentByItem, this.Connection);
            OracleTable pInvAdjByItemTab = new OracleTable(pInvAdjByItemTableType);
            foreach (InvAdjustmentModel invAdjModel in invAdjustmentModel.ListOfInvAdjustment)
            {
                OracleObject recordObject = new OracleObject(recordType);
                recordObject[recordType.Attributes["ITEM"]] = invAdjModel.Item;
                recordObject[recordType.Attributes["ITEM_DESC"]] = invAdjModel.ItemDesc;
                recordObject[recordType.Attributes["INV_STATUS"]] = invAdjModel.InvStatus;
                recordObject[recordType.Attributes["LOCATION_TYPE"]] = invAdjModel.LocationType;
                recordObject[recordType.Attributes["LOCATION_ID"]] = invAdjModel.LocationId;
                recordObject[recordType.Attributes["ADJ_QTY"]] = invAdjModel.AdjQty;
                recordObject[recordType.Attributes["REASON"]] = invAdjModel.Reason;
                ////recordObject[recordType.Attributes["TOTAL_STOCK"]] = invAdjModel.TotalStock;
                pInvAdjByItemTab.Add(recordObject);
            }

            return pInvAdjByItemTab;
        }
    }
}
