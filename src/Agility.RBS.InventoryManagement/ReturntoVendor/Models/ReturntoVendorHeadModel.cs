﻿// <copyright file="ReturntoVendorHeadModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// BulkItemHeaderModel
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.InventoryManagement.ReturntoVendor.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Inbox.Common;

    [DataProfile(Name = "RTVHEAD")]
    public class ReturntoVendorHeadModel : ProfileEntity<ReturntoVendorHeadWfModel>, IInboxCommonModel
    {
        public ReturntoVendorHeadModel()
        {
            this.ReturntoVendorDetails = new List<ReturntoVendorDetailModel>();
        }

        [Column("RTV_ORDER_NO")]
        public int? RTV_ORDER_NO { get; set; }

        [NotMapped]
        public int? Supplier { get; set; }

        [NotMapped]
        public string SupplierName { get; set; }

        [NotMapped]
        public int? StatusInd { get; set; }

        [NotMapped]
        public string LocType { get; set; }

        [NotMapped]
        public int? Location { get; set; }

        [NotMapped]
        public string LocationName { get; set; }

        [NotMapped]
        public decimal? TotalOrderAmt { get; set; }

        [NotMapped]
        public string ShipToAdd1 { get; set; }

        [NotMapped]
        public string ShipToAdd2 { get; set; }

        [NotMapped]
        public string ShipToAdd3 { get; set; }

        [NotMapped]
        public string State { get; set; }

        [NotMapped]
        public string ShipToCity { get; set; }

        [NotMapped]
        public string ShipToPcode { get; set; }

        [NotMapped]
        public string ShipToCountryId { get; set; }

        [NotMapped]
        public string RetAuthNum { get; set; }

        [NotMapped]
        public string Courier { get; set; }

        [NotMapped]
        public decimal? Freight { get; set; }

        [NotMapped]
        public DateTime? CompletedDate { get; set; }

        [NotMapped]
        public string ExtRefNo { get; set; }

        [NotMapped]
        public string CommentDesc { get; set; }

        [NotMapped]
        public int? MrtNo { get; set; }

        [NotMapped]
        public DateTime? NotAfterDate { get; set; }

        [NotMapped]
        public decimal? RestockPct { get; set; }

        [NotMapped]
        public decimal? RestockCost { get; set; }

        [NotMapped]
        public string Item { get; set; }

        [NotMapped]
        public List<ReturntoVendorDetailModel> ReturntoVendorDetails { get; private set; }

        [NotMapped]
        public int? LangId { get; set; }

        [NotMapped]
        public string ProgramPhase { get; set; }

        [NotMapped]
        public string ProgramMessage { get; set; }

        [NotMapped]
        public string Error { get; set; }

        [NotMapped]
        public string Operation { get; set; }

        [NotMapped]
        public string TableName { get; set; }

        [NotMapped]
        public Core.Models.RbWfHeaderModel WorkflowForm { get; set; }

        [NotMapped]
        [Column("ORGANISATION_ID")]
        public new int? OrganizationId { get; set; }

        [NotMapped]
        [Column("CREATED_BY")]
        public new string CreatedBy { get; set; }

        [NotMapped]
        [Column("LAST_UPDATED_BY")]
        public new string ModifiedBy { get; set; }

        [NotMapped]
        [Column("DeletedInd")]
        public new bool? DeletedInd { get; set; }

        [NotMapped]
        [Column("CREATED_DATE")]
        public new DateTimeOffset? CreatedDate { get; set; }

        [NotMapped]
        [Column("LAST_UPDATED_DATE")]
        public new DateTimeOffset? ModifiedDate { get; set; }

        [NotMapped]
        public int? WfNxtStateInd { get; set; }

        [NotMapped]
        public string Status { get; set; }

        [NotMapped]
        public string MinRetAmt { get; set; }

        [NotMapped]
        public string CurrencyCode { get; set; }

        [NotMapped]
        public string Items { get; set; }

        [NotMapped]
        public string ItemDesc { get; set; }

        [NotMapped]
        public decimal? ReturnedQtys { get; set; }

        [NotMapped]
        public decimal? UnitCostLocs { get; set; }

        [NotMapped]
        public string Reasons { get; set; }

        [NotMapped]
        public decimal? RestockPcts { get; set; }

        [NotMapped]
        public string ContactName { get; set; }

        [NotMapped]
        public string ContactPhone { get; set; }

        [NotMapped]
        public string ContactFax { get; set; }

        [NotMapped]
        public string ShipToCountry { get; set; }

        [NotMapped]
        public string StateName { get; set; }

        [NotMapped]
        public int? Wh { get; set; }

        [NotMapped]
        public int? Store { get; set; }

        [NotMapped]
        public decimal? TotalRtvCost { get; set; }
    }
}
