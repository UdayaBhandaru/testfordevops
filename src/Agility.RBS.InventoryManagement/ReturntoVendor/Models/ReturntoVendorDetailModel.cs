﻿// <copyright file="ReturntoVendorDetailModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InventoryManagement.ReturntoVendor.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class ReturntoVendorDetailModel
    {
        public long? DetailSeqNo { get; set; }

        public string Items { get; set; }

        public string ItemDesc { get; set; }

        public decimal? ReturnedQtys { get; set; }

        public string FromDisps { get; set; }

        public string ToDisps { get; set; }

        public decimal? UnitCostExts { get; set; }

        public decimal? UnitCostSupps { get; set; }

        public decimal? UnitCostLocs { get; set; }

        public string Reasons { get; set; }

        public decimal? RestockPcts { get; set; }

        public string InvStatuses { get; set; }

        public decimal? McReturnedQtys { get; set; }

        public decimal? Weights { get; set; }

        public string WeightUoms { get; set; }

        public decimal? WeightCuoms { get; set; }

        public decimal? McWeightCuoms { get; set; }

        public string Cuoms { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string PError { get; set; }

        public string ItemXformInd { get; set; }

        public string SellableInd { get; set; }

        public int? Dept { get; set; }

        public int? Class { get; set; }

        public int? Subclass { get; set; }

        public string CostUom { get; set; }

        public string ContainerItem { get; set; }

        public string ReasonName { get; set; }
    }
}
