﻿// <copyright file="ReturntoVendorSearchModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// BulkItemHeaderModel
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.InventoryManagement.ReturntoVendor.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Core.Models;
    using Agility.RBS.Inbox.Common;

    public class ReturntoVendorSearchModel : ProfileEntity, Core.IPaginationModel
    {
        public int? RTV_ORDER_NO { get; set; }

        public int? Supplier { get; set; }

        public string SupplierName { get; set; }

        public string LocType { get; set; }

        public int? Location { get; set; }

        public string LocationName { get; set; }

        public string RetAuthNum { get; set; }

        public DateTime? CompletedDate { get; set; }

        public string CommentDesc { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Error { get; set; }

        public string TableName { get; set; }

        public new DateTimeOffset? CreatedDate { get; set; }

        public string Status { get; set; }

        public int? StartRow { get; set; }

        public int? EndRow { get; set; }

        public long? TotalRows { get; set; }

        public SortModel[] SortModel { get; set; }
    }
}
