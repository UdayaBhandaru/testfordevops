﻿// <copyright file="ReturntoVendorRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// BulkItemRepository
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.InventoryManagement.ReturntoVendor.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.InventoryManagement.Common;
    using Agility.RBS.InventoryManagement.ReturntoVendor.Models;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Repositories.Models;
    using AutoMapper;
    using Devart.Data.Oracle;
    using DocumentFormat.OpenXml;
    using DocumentFormat.OpenXml.Packaging;
    using DocumentFormat.OpenXml.Spreadsheet;
    using Microsoft.AspNetCore.Authorization;

    public class ReturntoVendorRepository : BaseOraclePackage
    {
        public ReturntoVendorRepository()
        {
            this.PackageName = InventoryManagementConstants.GetRtvPackage;
        }

        public int RtvOrderNo { get; private set; }

        public async Task<List<ReturntoVendorSearchModel>> ReturntoVendorListGet(ServiceDocument<ReturntoVendorSearchModel> serviceDoc, ServiceDocumentResult serviceDocumentResult)
        {
            ReturntoVendorSearchModel orderSearch = serviceDoc.DataProfile.DataModel;
            PackageParams packageParameter = this.GetPackageParams(InventoryManagementConstants.ObjTypeRTVSearch, InventoryManagementConstants.GetProcRTVSearch);
            OracleObject recordObject = this.SetParamsRtvList(orderSearch);

            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();

            BaseOracleParameter baseOracleParameter;
            baseOracleParameter = new BaseOracleParameter { Name = "P_START", DataType = OracleDbType.Number, Direction = ParameterDirection.Input, Value = Convert.ToString(orderSearch.StartRow) };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            baseOracleParameter = new BaseOracleParameter { Name = "P_END", DataType = OracleDbType.Number, Direction = ParameterDirection.Input, Value = Convert.ToString(orderSearch.EndRow) };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            if (orderSearch.SortModel == null)
            {
                orderSearch.SortModel = new[] { new Core.Models.SortModel { ColId = null, Sort = null } };
            }

            baseOracleParameter = new BaseOracleParameter { Name = "P_SortColId", DataType = OracleDbType.VarChar, Direction = ParameterDirection.Input, Value = orderSearch.SortModel[0].ColId };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            baseOracleParameter = new BaseOracleParameter { Name = "P_SortOrder", DataType = OracleDbType.VarChar, Direction = ParameterDirection.Input, Value = orderSearch.SortModel[0].Sort };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            baseOracleParameter = new BaseOracleParameter { Name = "P_total", DataType = OracleDbType.Number, Direction = ParameterDirection.Output };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            parameters.Add(this.ParameterBuilder(UserProfileHelper.GetUserParameter()));
            var orders = await this.GetProcedure2<ReturntoVendorSearchModel>(recordObject, packageParameter, serviceDocumentResult, parameters);
            orderSearch.TotalRows = (this.Parameters["p_total"].Value != null && this.Parameters["p_total"].Value != DBNull.Value) ? Convert.ToInt64(this.Parameters["p_total"].Value) : 1;
            serviceDoc.DataProfile.DataModel = orderSearch;
            return orders;
        }

        public async Task<List<ReturntoVendorHeadModel>> GetReturntoVendor(ReturntoVendorHeadModel returntoVendorHeaderModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(InventoryManagementConstants.ObjTypeRTVHead, InventoryManagementConstants.GetProcRTVHead);
            OracleObject recordObject = this.SetParamsReturntoVendor(returntoVendorHeaderModel, true);
            OracleTable pDomainMstTab = await this.GetProcedure(recordObject, packageParameter, serviceDocumentResult);

            var lstHeader = Mapper.Map<List<ReturntoVendorHeadModel>>(pDomainMstTab);
            if (lstHeader != null)
            {
                for (int i = 0; i < lstHeader.Count; i++)
                {
                    OracleObject obj = (OracleObject)pDomainMstTab[i];
                    OracleTable domainDetails = (Devart.Data.Oracle.OracleTable)obj["RTV_DETAIL_REC"];
                    if (domainDetails.Count > 0)
                    {
                        lstHeader[i].ReturntoVendorDetails.AddRange(Mapper.Map<List<ReturntoVendorDetailModel>>(domainDetails));
                    }
                }
            }

            return lstHeader;
        }

        public async Task<ReturntoVendorHeadModel> ReturntoVendorGet(int rtvOrderNo, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType(InventoryManagementConstants.RecordObjRTVGet);
            OracleObject recordObject = this.GetOracleObject(recordType);
            this.SetRtvOrderNo(rtvOrderNo, recordObject);
            PackageParams packageParameter = this.GetPackageParams(InventoryManagementConstants.ObjTypeRTVGet, InventoryManagementConstants.GetProcRTVHead);

            OracleTable pDomainMstTab = await this.GetProcedure(recordObject, packageParameter, serviceDocumentResult);
            List<ReturntoVendorHeadModel> rtvs = Mapper.Map<List<ReturntoVendorHeadModel>>(pDomainMstTab);
            if (rtvs != null)
            {
                if (rtvs[0].Store != null)
                {
                    rtvs[0].Location = rtvs[0].Store;
                    rtvs[0].LocType = "S";
                }
                else
                {
                    rtvs[0].Location = rtvs[0].Wh;
                    rtvs[0].LocType = "W";
                }

                OracleObject obj = (OracleObject)pDomainMstTab[0];
                OracleTable domainDetails = (Devart.Data.Oracle.OracleTable)obj["detail_tbl"];
                if (domainDetails.Count > 0)
                {
                    rtvs[0].ReturntoVendorDetails.AddRange(Mapper.Map<List<ReturntoVendorDetailModel>>(domainDetails));
                }
            }

            return rtvs != null ? rtvs[0] : null;
        }

        public virtual void SetRtvOrderNo(int rtvOrderNo, OracleObject recordObject)
        {
            recordObject["RTV_ORDER_NO"] = rtvOrderNo;
        }

        public async Task<ServiceDocumentResult> ReturntoVendorSet(ReturntoVendorHeadModel returntoVendorHeaderModel)
        {
            this.RtvOrderNo = 0;
            PackageParams packageParameter = this.GetPackageParams(InventoryManagementConstants.ObjTypeRTVHead, InventoryManagementConstants.SetProcRTVHead);
            OracleObject recordObject = this.SetParamsReturntoVendor(returntoVendorHeaderModel);
            var result = await this.SetProcedure(recordObject, packageParameter);
            if (this.ObjResult != null)
            {
                this.RtvOrderNo = Convert.ToInt32(this.ObjResult["RTV_ORDER_NO"]);
            }

            return result;
        }

        public virtual void SetParamsSearch(ReturntoVendorHeadModel returntoVendorHeaderModel, OracleObject recordObject)
        {
            recordObject["RTV_ORDER_NO"] = returntoVendorHeaderModel.RTV_ORDER_NO;
            recordObject["LOC_TYPE"] = returntoVendorHeaderModel.LocType;
            recordObject["LOCATION"] = returntoVendorHeaderModel.Location;
            recordObject["SUPPLIER"] = returntoVendorHeaderModel.Supplier;
            recordObject["STATUS"] = returntoVendorHeaderModel.Status;
        }

        public virtual void SetParamsSave(ReturntoVendorHeadModel returntoVendorHeaderModel, OracleObject recordObject)
        {
            recordObject["Operation"] = returntoVendorHeaderModel.Operation;
            recordObject["CREATED_BY"] = this.GetCreatedBy(returntoVendorHeaderModel.CreatedBy);
            recordObject["LAST_UPDATED_BY"] = FxContext.Context.Name;

            // Detail Object
            OracleType dtlTableType = this.GetObjectType("UTL_DMN_DTL_TAB");
            OracleType dtlRecordType = this.GetObjectType("UTL_DMN_DTL_REC");
            OracleTable pUtlDmnDtlTab = new OracleTable(dtlTableType);
            foreach (ReturntoVendorDetailModel detail in returntoVendorHeaderModel.ReturntoVendorDetails)
            {
                var dtlRecordObject = this.GetOracleObject(dtlRecordType);
                dtlRecordObject[dtlRecordType.Attributes["DOMAIN_ID"]] = returntoVendorHeaderModel.RTV_ORDER_NO;
                dtlRecordObject[dtlRecordType.Attributes["DOMAIN_DETAIL_ID"]] = detail.Items;
                pUtlDmnDtlTab.Add(dtlRecordObject);
            }

            recordObject["UTL_DOMAIN_DTL"] = pUtlDmnDtlTab;
        }

        public OracleObject SetParamsReturntoVendor(ReturntoVendorHeadModel returntoVendorHeaderModel, bool isSearch)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType(InventoryManagementConstants.RecordObjRTVHead);
            OracleObject recordObject = this.GetOracleObject(recordType);
            if (!isSearch)
            {
                this.SetParamsSave(returntoVendorHeaderModel, recordObject);
            }

            this.SetParamsSearch(returntoVendorHeaderModel, recordObject);
            return recordObject;
        }

        private OracleObject SetParamsReturntoVendor(ReturntoVendorHeadModel returntoVendorHeaderModel)
        {
            this.Connection.Open();
            OracleObject recordObjRtv = this.GetOracleObject(this.GetObjectType(InventoryManagementConstants.RecordObjRTVHead));
            recordObjRtv["RTV_ORDER_NO"] = returntoVendorHeaderModel.RTV_ORDER_NO;
            recordObjRtv["LOC"] = returntoVendorHeaderModel.Location;
            ////recordObjRtv["physical_wh"] = returntoVendorHeaderModel.RTV_ORDER_NO;
            recordObjRtv["LOC_TYPE"] = returntoVendorHeaderModel.LocType;
            recordObjRtv["EXT_REF_NO"] = returntoVendorHeaderModel.ExtRefNo;
            recordObjRtv["RET_AUTH_NUM"] = returntoVendorHeaderModel.RetAuthNum;
            recordObjRtv["SUPPLIER"] = returntoVendorHeaderModel.Supplier;
            recordObjRtv["SHIP_ADDR1"] = returntoVendorHeaderModel.ShipToAdd1;
            recordObjRtv["SHIP_ADDR2"] = returntoVendorHeaderModel.ShipToAdd2;
            recordObjRtv["SHIP_ADDR3"] = returntoVendorHeaderModel.ShipToAdd3;
            recordObjRtv["STATE"] = returntoVendorHeaderModel.State;
            recordObjRtv["CITY"] = returntoVendorHeaderModel.ShipToCity;
            recordObjRtv["PCODE"] = returntoVendorHeaderModel.ShipToPcode;
            recordObjRtv["COUNTRY"] = returntoVendorHeaderModel.ShipToCountryId;
            recordObjRtv["tran_date"] = DateTime.Now;
            recordObjRtv["COMMENTS"] = returntoVendorHeaderModel.CommentDesc;
            ////recordObjRtv["total_order_amt_unit_based"] = returntoVendorHeaderModel.RTV_ORDER_NO;
            ////recordObjRtv["total_order_amt_wgt_based"] = returntoVendorHeaderModel.RTV_ORDER_NO;
            recordObjRtv["RET_COURIER"] = returntoVendorHeaderModel.Courier;
            recordObjRtv["RESTOCK_PCT"] = returntoVendorHeaderModel.RestockPct;

            var tblItems = this.GetOracleTable(this.GetObjectType("ITEM_TBL"));
            var tblReturnedQtys = this.GetOracleTable(this.GetObjectType("QTY_TBL"));
            var tblFromDisps = this.GetOracleTable(this.GetObjectType("INV_STATUS_CODES_TBL"));
            var tblToDisps = this.GetOracleTable(this.GetObjectType("INV_STATUS_CODES_TO_DIST_TBL"));
            var tblUnitCostExts = this.GetOracleTable(this.GetObjectType("UNIT_COST_TBL"));
            var tblUnitCostSupps = this.GetOracleTable(this.GetObjectType("UNIT_COST_TBL"));
            var tblUnitCostLocs = this.GetOracleTable(this.GetObjectType("UNIT_COST_TBL"));
            var tblReasons = this.GetOracleTable(this.GetObjectType("REASON_TBL"));
            var tblRestockPcts = this.GetOracleTable(this.GetObjectType("RESTOCK_PCT_TBL"));
            var tblInvStatuses = this.GetOracleTable(this.GetObjectType("INV_STATUS_TBL"));
            var tblMcReturnedQtys = this.GetOracleTable(this.GetObjectType("QTY_TBL"));
            var tblWeights = this.GetOracleTable(this.GetObjectType("WEIGHT_TBL"));
            var tblWeightUoms = this.GetOracleTable(this.GetObjectType("UOM_CLASS_TBL"));
            var tblWeightCuoms = this.GetOracleTable(this.GetObjectType("WEIGHT_TBL"));
            var tblMcWeightCuoms = this.GetOracleTable(this.GetObjectType("WEIGHT_TBL"));
            var tblCuoms = this.GetOracleTable(this.GetObjectType("COST_UOM_TBL"));

            // RTV Details Object
            OracleTable oracleTableRtvItem = this.GetOracleTable(this.GetObjectType(InventoryManagementConstants.ObjTypeRtvItem));

            foreach (ReturntoVendorDetailModel detail in returntoVendorHeaderModel.ReturntoVendorDetails)
            {
                tblItems.Add(detail.Items);
                tblReturnedQtys.Add(detail.ReturnedQtys);
                tblFromDisps.Add(detail.FromDisps);
                tblToDisps.Add(detail.ToDisps);
                tblUnitCostExts.Add(detail.UnitCostExts);
                tblUnitCostSupps.Add(detail.UnitCostSupps);
                tblUnitCostLocs.Add(detail.UnitCostLocs);
                tblReasons.Add(detail.Reasons);
                tblRestockPcts.Add(detail.RestockPcts);
                tblInvStatuses.Add(detail.InvStatuses);
                tblMcReturnedQtys.Add(detail.ReturnedQtys);
                tblWeights.Add(detail.Weights);
                tblWeightUoms.Add(detail.WeightUoms);
                tblWeightCuoms.Add(detail.WeightCuoms);
                tblMcWeightCuoms.Add(detail.McWeightCuoms);
                tblCuoms.Add(detail.Cuoms);

                var recordObjRtvItem = this.GetOracleObject(this.GetObjectType(InventoryManagementConstants.RecordObjRtvItem));
                recordObjRtvItem["ITEM"] = detail.Items;
                oracleTableRtvItem.Add(recordObjRtvItem);
            }

            // RTV Details Object
            OracleTable oracleTableOrdLoc = this.GetOracleTable(this.GetObjectType(InventoryManagementConstants.ObjTypeRtvDetail));
            var recordObjOrdLoc = this.GetOracleObject(this.GetObjectType(InventoryManagementConstants.RecordObjRtvDetail));
            recordObjOrdLoc["ITEMS"] = tblItems;
            recordObjOrdLoc["RETURNED_QTYS"] = tblReturnedQtys;
            recordObjOrdLoc["FROM_DISPS"] = tblFromDisps;
            recordObjOrdLoc["TO_DISPS"] = tblToDisps;
            recordObjOrdLoc["UNIT_COST_EXTS"] = tblUnitCostExts;
            recordObjOrdLoc["UNIT_COST_SUPPS"] = tblUnitCostSupps;
            recordObjOrdLoc["UNIT_COST_LOCS"] = tblUnitCostLocs;
            recordObjOrdLoc["REASONS"] = tblReasons;
            recordObjOrdLoc["RESTOCK_PCTS"] = tblRestockPcts;
            recordObjOrdLoc["INV_STATUSES"] = tblInvStatuses;
            recordObjOrdLoc["MC_RETURNED_QTYS"] = tblMcReturnedQtys;
            recordObjOrdLoc["WEIGHTS"] = tblWeights;
            recordObjOrdLoc["WEIGHT_UOMS"] = tblWeightUoms;
            recordObjOrdLoc["WEIGHT_CUOMS"] = tblWeightCuoms;
            recordObjOrdLoc["MC_WEIGHT_CUOMS"] = tblMcWeightCuoms;
            recordObjOrdLoc["CUOMS"] = tblCuoms;
            oracleTableOrdLoc.Add(recordObjOrdLoc);
            recordObjRtv["DETAIL_TBL"] = oracleTableOrdLoc;
            recordObjRtv["IM_ROW_TBL"] = oracleTableRtvItem;
            return recordObjRtv;
        }

        private OracleObject SetParamsRtvList(ReturntoVendorSearchModel orderSearch)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(InventoryManagementConstants.RecordObjRTVSearch, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);

            recordObject[recordType.Attributes["RTV_ORDER_NO"]] = orderSearch.RTV_ORDER_NO;
            recordObject[recordType.Attributes["SUPPLIER"]] = orderSearch.Supplier;
            recordObject[recordType.Attributes["LOCATION"]] = orderSearch.Location;
            ////recordObject[recordType.Attributes["STATUS"]] = orderSearch.Status;
            return recordObject;
        }
    }
}