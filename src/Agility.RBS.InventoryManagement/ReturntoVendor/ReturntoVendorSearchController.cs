﻿// <copyright file="ReturntoVendorSearchController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InventoryManagement.ReturntoVendor
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core.Constants;
    using Agility.RBS.InventoryManagement.ReturntoVendor.Models;
    using Agility.RBS.InventoryManagement.ReturntoVendor.Repositories;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    [Route("api/[controller]/[action]")]
    public class ReturntoVendorSearchController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly ServiceDocument<ReturntoVendorSearchModel> serviceDocument;
        private readonly ReturntoVendorRepository returntoVendorRepository;

        public ReturntoVendorSearchController(
            ServiceDocument<ReturntoVendorSearchModel> serviceDocument,
            ReturntoVendorRepository returntoVendorRepository,
        DomainDataRepository domainDataRepository)
        {
            this.serviceDocument = serviceDocument;
            this.returntoVendorRepository = returntoVendorRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in the Bulk Item List page.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        public async Task<ServiceDocument<ReturntoVendorSearchModel>> List()
        {
            this.serviceDocument.DomainData.Add("workflowStatusList", this.domainDataRepository.WorkflowStatusGet(RbCommonConstants.ItemModule).Result.OrderBy(x => x.Name));
            this.serviceDocument.DomainData.Add("location", this.domainDataRepository.LocationDomainGet().Result);
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for fetching Order records based on search criteria when Search button clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<ReturntoVendorSearchModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.ReturntoVendorSearch);
            return this.serviceDocument;
        }

        private async Task<List<ReturntoVendorSearchModel>> ReturntoVendorSearch()
        {
            try
            {
                var lstUomClass = await this.returntoVendorRepository.ReturntoVendorListGet(this.serviceDocument, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstUomClass;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }
    }
}
