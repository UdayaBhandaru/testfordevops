﻿// <copyright file="ReturntoVendorMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InventoryManagement.ReturntoVendor.Mapping
{
    using System;
    using Agility.RBS.Core;
    using Agility.RBS.InventoryManagement.ReturntoVendor.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class ReturntoVendorMapping : Profile
    {
        public ReturntoVendorMapping()
            : base("ReturntoVendorMapping")
        {
            this.CreateMap<OracleObject, ReturntoVendorSearchModel>()
                .ForMember(m => m.Supplier, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["SUPPLIER"])))
                .ForMember(m => m.SupplierName, opt => opt.MapFrom(r => r["SUP_NAME"]))
                .ForMember(m => m.Location, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["LOCATION"])))
                .ForMember(m => m.LocationName, opt => opt.MapFrom(r => r["LOC_NAME"]))
                .ForMember(m => m.RTV_ORDER_NO, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["RTV_ORDER_NO"])))
               //// .ForMember(m => m.Status, opt => opt.MapFrom(r => r["STATUS"]))
                .ForMember(m => m.CreatedDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["CREATED_DATE"])))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "RTVHEAD"));

            this.CreateMap<OracleObject, ReturntoVendorHeadModel>()
              .ForMember(m => m.RTV_ORDER_NO, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["RTV_ORDER_NO"])))
              .ForMember(m => m.Location, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["LOC"])))
              .ForMember(m => m.Wh, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["WH"])))
              .ForMember(m => m.Store, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["STORE"])))
              .ForMember(m => m.LocType, opt => opt.MapFrom(r => r["loc_type"]))
              .ForMember(m => m.ExtRefNo, opt => opt.MapFrom(r => r["ext_ref_no"]))
              .ForMember(m => m.RetAuthNum, opt => opt.MapFrom(r => r["ret_auth_num"]))
              .ForMember(m => m.Supplier, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["SUPPLIER"])))
              .ForMember(m => m.ShipToAdd1, opt => opt.MapFrom(r => r["ship_addr1"]))
              .ForMember(m => m.ShipToAdd2, opt => opt.MapFrom(r => r["ship_addr2"]))
              .ForMember(m => m.ShipToAdd3, opt => opt.MapFrom(r => r["ship_addr3"]))
              .ForMember(m => m.State, opt => opt.MapFrom(r => r["state"]))
              .ForMember(m => m.ShipToCity, opt => opt.MapFrom(r => r["city"]))
              .ForMember(m => m.ShipToPcode, opt => opt.MapFrom(r => r["pcode"]))
              .ForMember(m => m.ShipToCountryId, opt => opt.MapFrom(r => r["country"]))
              .ForMember(m => m.CreatedDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["tran_date"])))
              .ForMember(m => m.CommentDesc, opt => opt.MapFrom(r => r["comments"]))
              .ForMember(m => m.Courier, opt => opt.MapFrom(r => r["ret_courier"]))
              .ForMember(m => m.RestockPct, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["restock_pct"])))
              .ForMember(m => m.RestockCost, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["restock_cost"])))
              .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"))
              .ForMember(m => m.TableName, opt => opt.MapFrom(r => "RTVHEAD"));

            this.CreateMap<OracleObject, ReturntoVendorDetailModel>()
                .ForMember(m => m.DetailSeqNo, opt => opt.MapFrom(r => OracleNullHandler.DbNullInt64Handler(r["seq_no"])))
                .ForMember(m => m.Items, opt => opt.MapFrom(r => r["item"]))
                .ForMember(m => m.ItemDesc, opt => opt.MapFrom(r => r["item_desc"]))
                .ForMember(m => m.ReturnedQtys, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["qty_requested"])))
                .ForMember(m => m.UnitCostLocs, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["unit_cost"])))
                .ForMember(m => m.Reasons, opt => opt.MapFrom(r => r["reason"]))
                .ForMember(m => m.ReasonName, opt => opt.MapFrom(r => r["reason_desc"]))
                .ForMember(m => m.RestockPcts, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["restock_pct"])));
        }
    }
}
