﻿// <copyright file="ReturntoVendorController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InventoryManagement.ReturntoVendor
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Constants;
    using Agility.RBS.Documents;
    using Agility.RBS.Inbox.Common;
    using Agility.RBS.Inbox.Models;
    using Agility.RBS.Inbox.Repository;
    using Agility.RBS.InventoryManagement.Common;
    using Agility.RBS.InventoryManagement.ReturntoVendor.Models;
    using Agility.RBS.InventoryManagement.ReturntoVendor.Repositories;
    using Agility.RBS.ItemManagement.Item.Models;
    using Agility.RBS.ItemManagement.Item.Repositories;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    [Route("api/[controller]/[action]")]
    public class ReturntoVendorController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly ReturntoVendorRepository returntoVendorRepository;
        private readonly ItemSelectRepository itemSelectRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly InboxModel inboxModel;
        private readonly InboxRepository inboxRepository;
        private ServiceDocument<ReturntoVendorHeadModel> serviceDocument;
        private int rtvOrderNo;

        public ReturntoVendorController(
            ServiceDocument<ReturntoVendorHeadModel> serviceDocument,
            ReturntoVendorRepository returntoVendorRepository,
            DomainDataRepository domainDataRepository,
            InboxRepository inboxRepository,
            ItemSelectRepository itemSelectRepository)
        {
            this.returntoVendorRepository = returntoVendorRepository;
            this.domainDataRepository = domainDataRepository;
            this.inboxRepository = inboxRepository;
            this.serviceDocument = serviceDocument;
            this.itemSelectRepository = itemSelectRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
            this.inboxModel = new InboxModel { Operation = "CHGS", ModuleName = "RTV" };
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in Bulk Item Head page when Add button clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<ReturntoVendorHeadModel>> New()
        {
            this.serviceDocument.New(false);
            this.serviceDocument.DomainData.Add("locType", this.domainDataRepository.DomainDetailData(InventoryManagementConstants.DomainHdrLocType).Result);
            this.serviceDocument.DomainData.Add("location", this.domainDataRepository.LocationDomainGet().Result);
            this.serviceDocument.DomainData.Add("reason", this.domainDataRepository.DomainDetailData(InventoryManagementConstants.DomainHdrReason).Result);
            this.serviceDocument.LocalizationData.AddData("ReturntoVendor");
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for populating fields in the Bulk Item Head page when Edit icon clicked.
        /// </summary>
        /// <param name="id">Cost Change Id</param>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<ReturntoVendorHeadModel>> Open(int id)
        {
            this.rtvOrderNo = id;
            await this.serviceDocument.OpenAsync(this.ReturntoVendorOpen);
            this.serviceDocument.DomainData.Add("locType", this.domainDataRepository.DomainDetailData(InventoryManagementConstants.DomainHdrLocType).Result);
            this.serviceDocument.DomainData.Add("location", this.domainDataRepository.LocationDomainGet().Result);
            this.serviceDocument.DomainData.Add("reason", this.domainDataRepository.DomainDetailData(InventoryManagementConstants.DomainHdrReason).Result);
            this.serviceDocument.DomainData.Add("supplier", this.domainDataRepository.SupplierDomainGet(this.serviceDocument.DataProfile.DataModel.Supplier.Value).Result);
            this.serviceDocument.LocalizationData.AddData("ReturntoVendor");
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for submitting cost change to next level when Submit button clicked.
        /// </summary>
        /// <param name="serviceDocument">Service Document</param>
        /// <returns>ServiceDocument</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<ReturntoVendorHeadModel>> Submit([FromBody]ServiceDocument<ReturntoVendorHeadModel> serviceDocument)
        {
            this.serviceDocument = serviceDocument;
            this.inboxModel.StateID = GuidConverter.DotNetToOracle(Convert.ToString(serviceDocument.DataProfile.DataModel.WorkflowInstance?.WorkflowStateId));
            await this.serviceDocument.TransitAsync(this.ReturntoVendorSave);

            if (this.serviceDocument.Result.Type == MessageType.Success)
            {
                if (this.serviceDocument.DataProfile.DataModel.WorkflowInstance.StateName == "Worksheet")
                {
                    this.inboxModel.Operation = "DFT";
                }

                await this.inboxRepository.SetInbox(this.inboxModel, this.serviceDocument);
            }

            return this.serviceDocument;
        }

        public async Task<ServiceDocument<ReturntoVendorHeadModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.ReturntoVendorSave);
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for populating dependent fields based on selected Supplier.
        /// </summary>
        /// <param name="supplier">Supplier Id</param>
        /// <returns>List Of SupplierDetailDomainModel</returns>
        [HttpGet]
        public async Task<List<SupplierAddressDomainModel>> SupplierDetailAddrGet(int supplier)
        {
            return await this.domainDataRepository.SupplierDetailAddrGet(supplier);
        }

        /// <summary>
        /// This API invoked for populating dependent fields RTV Items Get.
        /// </summary>
        /// <param name="supplier">Supplier</param>
        /// <param name="itemBarcode">Item Barcode</param>
        /// <param name="location">Location</param>
        /// <returns>RTV Items Get</returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<List<RtvItemSelectModel>> RtvItemGet(int supplier, string itemBarcode, int location)
        {
            try
            {
                return await this.itemSelectRepository.SelectRtvItem(
                    new RtvItemSelectModel { Supplier = supplier, Item = itemBarcode, LocationId = location }, this.serviceDocumentResult);
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<ReturntoVendorHeadModel> ReturntoVendorOpen()
        {
            try
            {
                var returntoVendor = await this.returntoVendorRepository.ReturntoVendorGet(this.rtvOrderNo, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return returntoVendor;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> ReturntoVendorSave()
        {
            this.serviceDocument.DataProfile.DataModel.Status = RbsServiceDocCustomizationHelper.GetStateNameByProfileIdAndCurActionId(this.serviceDocument);
            this.serviceDocument.Result = await this.returntoVendorRepository.ReturntoVendorSet(this.serviceDocument.DataProfile.DataModel);
            if (this.serviceDocument.Result.Type != MessageType.Success)
            {
                return false;
            }

            this.serviceDocument.DataProfile.DataModel.RTV_ORDER_NO = this.returntoVendorRepository.RtvOrderNo;
            return true;
        }
    }
}
