﻿// <copyright file="InventoryManagementConstants.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InventoryManagement.Common
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal static class InventoryManagementConstants
    {
        // Result
        public const string OracleParameterResult = "RESULT";

        // Inventory Package
        public const string GetInventoryPackage = "INVADJWRAPPER_SQL";

        // RTV Package
        public const string GetRtvPackage = "RTV_MGMT";

        // Shipment Pakage
        public const string GetShipmentPackage = "shipmentwrapper_sql";

        // Domain Header Ids
        public const string DomainHdrInventoryStatus = "INST";
        public const string DomainHdrLocType = "LOTP";
        public const string DomainHdrReason = "RTVR";
        public const string DomainHdrInventoryAdjustmentReason = "INVADJ_REASON";

        public const string ObjTypeInvAdjustmentByItem = "INV_ADJ_SOH_ITEM_TAB";
        public const string RecordObjInvAdjustmentByItem = "INV_ADJ_SOH_ITEM_REC";
        public const string SetProcInvAdjustmentByItem = "INVADJ_SOH_BY_ITEM";

        // Return to Vendor
        public const string GetProcRTVHead = "GETRTV";
        public const string SetProcRTVHead = "SETRTV";
        public const string ObjTypeRTVHead = "RTV_RECORD_TAB";
        public const string RecordObjRTVHead = "RTV_RECORD_REC";

        public const string ObjTypeRTVGet = "RTV_RECORDGET_TAB";
        public const string RecordObjRTVGet = "RTV_RECORDGET_REC";

        public const string GetProcRTVSearch = "GETRTVLIST";
        public const string ObjTypeRTVSearch = "RTV_SEARCH_TAB";
        public const string RecordObjRTVSearch = "RTV_SEARCH_REC";

        // RTV Details
        public const string ObjTypeRtvDetail = "rtv_detail_tab";
        public const string RecordObjRtvDetail = "rtv_detail_rec";

        public const string ObjTypeRtvItem = "ITEM_MASTER_TAB";
        public const string RecordObjRtvItem = "ITEM_MASTER_REC";

        public const string GetTransferPackage = "TRANSFERWRAPPER_SQL";
        public const string DomainHdrTransferType = "TR3E";
        public const string DomainHdrContextType = "CNTX";
        public const string DomainHdrLocationType = "LOTP";
        public const string DomainHdrInventoryType = "TSIT";
        public const string DomainStatus = "TRST";

        public const string DomainClrItemList = "CLIL";
        public const string DomainTransferQtyType = "QYTP";
        public const string DomainTransferPriceAdjTypes = "TPA1";

        public const string RecordObjTransferHead = "TSFHEAD_REC";
        public const string ObjTypeTransferHead = "TSFHEAD_TAB";
        public const string SetProcTransferHead = "SETTSFHEAD";
        public const string GetProcTransferHead = "GETTSFHEAD";
        public const string DomainHdrStatus = "STATUS";

        public const string ObjTypeDomainTransferEnity = "TSF_ENTITY_TAB";
        public const string GetProcTransferEnityDomain = "GETTSFENTITY";

        // Details
        public const string RecordObjTransferDetail = "TSFDETAIL_REC";
        public const string ObjTypeTransferDetail = "TSFDETAIL_TAB";
        public const string SetProcTransferDetail = "SETTSFDETAIL";
        public const string GetProcTransferDetail = "GETTSFDETAIL";

        // ItemQty Get
        public const string RecordObjItemQtyDetails = "TSF_ITEM_QTY_REC";
        public const string ObjTypeItemQtyDetails = "TSF_ITEM_QTY_TAB";
        public const string GetProcItemQtyDetail = "GETTSF_ITEM_QTY";
        public const string DomainUnitofTransfer = "DIMO";

        // Shipment
        public const string ObjTypeShipmentSearch = "SHIPMENT_SEARCH_TAB";
        public const string GetProcShipmentSearch = "GETSHIPMENT_LIST";
        public const string RecordObjShipmentDetails = "SHIPMENT_HDRDTL_REC";
        public const string ObjTypeShipmentDetails = "SHIPMENT_HDRDTL_TAB";
        public const string SetProcShipmentDetails = "GETSHIPMENT_DTL";
    }
}
