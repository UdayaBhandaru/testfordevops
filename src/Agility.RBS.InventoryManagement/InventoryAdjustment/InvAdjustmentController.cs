﻿// <copyright file="InvAdjustmentController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InventoryManagement.InventoryAdjustment
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Workflow.Entities;
    using Agility.Framework.Core.Workflow.Models;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Constants;
    using Agility.RBS.Core.Models;
    using Agility.RBS.Core.Services;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.InventoryManagement.Common;
    using Agility.RBS.InventoryManagement.InventoryAdjustment.Models;
    using Agility.RBS.InventoryManagement.Repositories;
    using Agility.RBS.ItemManagement.Item.Models;
    using Agility.RBS.ItemManagement.Item.Repositories;
    using Agility.RBS.MDM.InvAdjustmentManagement;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [Route("api/[controller]/[action]")]
    public class InvAdjustmentController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly InventoryRepository inventoryRepository;
        private readonly ItemSelectRepository itemSelectRepository;
        private readonly ServiceDocument<InvAdjustmentModel> serviceDocument;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly InvAdjustmentRepository invAdjustmentRepository;

        public InvAdjustmentController(
            ServiceDocument<InvAdjustmentModel> serviceDocument,
            DomainDataRepository domainDataRepository,
            InventoryRepository inventoryRepository,
            ItemSelectRepository itemSelectRepository,
            InvAdjustmentRepository invAdjustmentRepository)
        {
            this.serviceDocument = serviceDocument;
            this.domainDataRepository = domainDataRepository;
            this.inventoryRepository = inventoryRepository;
            this.itemSelectRepository = itemSelectRepository;
            this.invAdjustmentRepository = invAdjustmentRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in the Item List page.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<InvAdjustmentModel>> List()
        {
            return await this.GetDomainData();
        }

        /// <summary>
        /// This API invoked for populating dependent fields when Get Item hyperlink clicked.
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="columnName">column Name</param>
        /// <returns>Name/Description</returns>
        [HttpGet]
        [AllowAnonymous]
        public InvAdjustmentModel GetDataBasedOnId(string id, string columnName)
        {
            try
            {
                var model = new InvAdjustmentModel();
                string result = this.domainDataRepository.GetCommonDesc(id, columnName);
                if (!string.IsNullOrEmpty(result))
                {
                    if (columnName == "ITEM_DESC")
                    {
                        model.ItemDesc = result;
                    }
                    else
                    {
                        model.LocName = result;
                    }

                    return model;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        /// <summary>
        /// This API invoked for saving field values in the Inventory Adjustment page when Save button clicked.
        /// </summary>
        /// <returns>ServiceDocument</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        [AllowAnonymous]
        public async Task<ServiceDocument<InvAdjustmentModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.InvAdjustmentSave);
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for populating columns in grid of Inv Adj by Loc Screen
        /// </summary>
        /// <param name="itemCode">item Code</param>
        /// <param name="locType">location Type</param>
        /// <param name="locId">location Id</param>
        /// <returns>ItemInventoryModel</returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ItemInventoryModel> GetItemRelatedData(string itemCode, string locType, int locId)
        {
            try
            {
                var itemInventory = await this.itemSelectRepository.GetItemDataForInventory(itemCode, locType, locId, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return itemInventory;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<ServiceDocument<InvAdjustmentModel>> GetDomainData()
        {
            this.serviceDocument.DomainData.Add("InventoryStatus", this.domainDataRepository.DomainDetailData(InventoryManagementConstants.DomainHdrInventoryStatus).Result);
            this.serviceDocument.DomainData.Add("location", await this.domainDataRepository.LocationDomainGet());
            this.serviceDocument.DomainData.Add("LocType", this.domainDataRepository.DomainDetailData(InventoryManagementConstants.DomainHdrLocType).Result);
            this.serviceDocument.DomainData.Add("InvAdjustmentReason", await this.invAdjustmentRepository.InvAdjustmentReasonGet());
            return this.serviceDocument;
        }

        private async Task<bool> InvAdjustmentSave()
        {
            this.serviceDocument.Result = await this.inventoryRepository.SetInventoryAdjustment(this.serviceDocument.DataProfile.DataModel);
            if (this.serviceDocument.Result.Type != MessageType.Success)
            {
                return false;
            }

            return true;
        }
    }
}
