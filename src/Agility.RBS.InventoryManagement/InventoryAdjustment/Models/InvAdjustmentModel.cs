﻿// <copyright file="InvAdjustmentModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InventoryManagement.InventoryAdjustment.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.Framework.Core.Profile.Entities;

    public class InvAdjustmentModel : ProfileEntity
    {
        public InvAdjustmentModel()
        {
            this.ListOfInvAdjustment = new List<InvAdjustmentModel>();
        }

        public string Item { get; set; }

        public string ItemDesc { get; set; }

        public string InvStatus { get; set; }

        public string LocationType { get; set; }

        public int? LocationId { get; set; }

        public string LocName { get; set; }

        public decimal? AdjQty { get; set; }

        public int? Reason { get; set; }

        public decimal? TotalStock { get; set; }

        public List<InvAdjustmentModel> ListOfInvAdjustment { get; private set; }
    }
}
