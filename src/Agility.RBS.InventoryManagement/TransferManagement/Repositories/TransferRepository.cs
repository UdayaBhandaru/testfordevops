﻿// <copyright file="TransferRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InventoryManagement.TransferManagement
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.InventoryManagement.Common;
    using Agility.RBS.InventoryManagement.TransferManagement.Models;
    using Agility.RBS.MDM.Repositories.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class TransferRepository : BaseOraclePackage
    {
        private readonly ServiceDocumentResult serviceDocumentResult;

        public TransferRepository()
        {
            this.PackageName = InventoryManagementConstants.GetTransferPackage;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public long? TSF_NO { get; private set; }

        public async Task<List<TransferSearchModel>> TransferListGet(TransferSearchModel returntoVendorHeaderModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(InventoryManagementConstants.ObjTypeTransferHead, InventoryManagementConstants.GetProcTransferHead);
            OracleObject recordObject = this.SetParamsTransferListGet(returntoVendorHeaderModel);
            return await this.GetProcedure2<TransferSearchModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<TransferHdrModel> TransferHeadGet(long transferNo, ServiceDocumentResult serviceDocumentResult)
        {
            TransferHdrModel transferHdrModel = new TransferHdrModel();
            transferHdrModel.TSF_NO = transferNo;
            PackageParams packageParameter = this.GetPackageParams(InventoryManagementConstants.ObjTypeTransferHead, InventoryManagementConstants.GetProcTransferHead);
            OracleObject recordObject = this.SetParamsTransferHeadGet(transferHdrModel);
            List<TransferHdrModel> priceChangeHeads = await this.GetProcedure2<TransferHdrModel>(recordObject, packageParameter, serviceDocumentResult);
            return priceChangeHeads != null ? priceChangeHeads[0] : null;
        }

        public virtual void SetPriceChangeRequestId(long priceChangeRequestId, OracleObject recordObject)
        {
            recordObject["TSF_NO"] = priceChangeRequestId;
        }

        public async Task<ServiceDocumentResult> TransferHeadSet(TransferHdrModel transferHead)
        {
            this.TSF_NO = 0;
            PackageParams packageParameter = this.GetPackageParams(InventoryManagementConstants.ObjTypeTransferHead, InventoryManagementConstants.SetProcTransferHead);
            OracleObject recordObject = this.SetParamsTransferHeadSet(transferHead);
            var result = await this.SetProcedure(recordObject, packageParameter);
            if (this.ObjResult != null && transferHead.TSF_NO == null)
            {
                this.TSF_NO = Convert.ToInt64(this.ObjResult["tsf_no"]);
            }
            else
            {
                this.TSF_NO = transferHead.TSF_NO;
            }

            return result;
        }

        public async Task<List<TransferDetailsModel>> TransferDetailsGet(long transferNo, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(InventoryManagementConstants.RecordObjTransferDetail, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["TSF_NO"]] = transferNo;
            PackageParams packageParameter = this.GetPackageParams(InventoryManagementConstants.ObjTypeTransferDetail, InventoryManagementConstants.GetProcTransferDetail);
            return await this.GetProcedure2<TransferDetailsModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<List<ItemQtyDomainModel>> ItemQtyDetailsGet(string item, string tsF_NO)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("TSF_ITEM_QTY_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            recordObject["ITEM"] = item;
            recordObject["TSF_NO"] = tsF_NO;
            PackageParams packageParameter = this.GetPackageParams(InventoryManagementConstants.ObjTypeItemQtyDetails, InventoryManagementConstants.GetProcItemQtyDetail);
            OracleTable pSupplierDtlsTab = await this.GetProcedure(recordObject, packageParameter, this.serviceDocumentResult);
            var lstHeader = Mapper.Map<List<ItemQtyDomainModel>>(pSupplierDtlsTab);
            return lstHeader;
        }

        public async Task<ServiceDocumentResult> SetTransferDetails(TransferDetailsModel transferDetailModel)
        {
            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();
            PackageParams packageParameter = this.GetPackageParams(InventoryManagementConstants.ObjTypeTransferDetail, InventoryManagementConstants.SetProcTransferDetail);
            OracleParameter parameter;
            parameter = new OracleParameter("RESULT", Devart.Data.Oracle.OracleDbType.Boolean)
            {
                Direction = System.Data.ParameterDirection.ReturnValue
            };
            parameters.Add(parameter);
            parameter = new OracleParameter(packageParameter.ObjectTypeTable, Devart.Data.Oracle.OracleDbType.Table)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                ObjectTypeName = packageParameter.ObjectTypeName,
                Value = this.SetParamsTransferDetail(transferDetailModel)
            };
            parameters.Add(parameter);
            return await this.SetProcedure(null, packageParameter, parameters);
        }

        public virtual OracleTable SetParamsTransferDetail(TransferDetailsModel transferDetails)
        {
            this.Connection.Open();
            OracleTable pItemCompanyTab = this.GetOracleTable(this.GetObjectType(InventoryManagementConstants.ObjTypeTransferDetail));
            foreach (TransferDetailsModel transferDetailModel in transferDetails.TransferDetails)
            {
                OracleObject recordObject = this.GetOracleObject(this.GetObjectType(InventoryManagementConstants.RecordObjTransferDetail));
                recordObject["TSF_NO"] = transferDetailModel.TSF_NO;
                recordObject["ITEM_TYPE"] = "I";
                recordObject["ITEM"] = transferDetailModel.Item;
                recordObject["DIFF_ID"] = transferDetailModel.DiffId;
                recordObject["ITEM_LIST"] = transferDetailModel.ItemList;
                recordObject["TSF_TYPE"] = transferDetailModel.TsfType;
                recordObject["INV_STATUS"] = transferDetailModel.InvStatus;
                recordObject["QTY_TYPE"] = transferDetailModel.QtyType;
                recordObject["TSF_QTY"] = transferDetailModel.TsfQty;
                recordObject["ADJUST_TYPE"] = transferDetailModel.AdjustType;
                recordObject["ADJUST_VALUE"] = transferDetailModel.AdjustValue;
                recordObject["FROM_LOC_TYPE"] = transferDetailModel.FromLocType;
                recordObject["FROM_LOC"] = transferDetailModel.FromLoc;
                recordObject["TO_LOC_TYPE"] = transferDetailModel.ToLocType;
                recordObject["TO_LOC"] = transferDetailModel.ToLoc;
                recordObject["DEPT"] = transferDetailModel.Dept;
                recordObject["ENTITY_TYPE"] = transferDetailModel.EntityType;
                recordObject["RESTOCK_PCT"] = transferDetailModel.RestockPct;
                if (transferDetailModel.Operation == "I")
                {
                    recordObject["OPERATION"] = transferDetailModel.Operation;
                }

                pItemCompanyTab.Add(recordObject);
            }

            return pItemCompanyTab;
        }

        public virtual void SetParamsTransferListSearch(TransferSearchModel tfsHdrModel, OracleObject recordObject)
        {
            recordObject["TSF_NO"] = tfsHdrModel.TSF_NO;
            recordObject["TSF_PARENT_NO"] = tfsHdrModel.TSF_PARENT_NO;
            recordObject["FROM_LOC_TYPE"] = tfsHdrModel.FromLocType;
            recordObject["FROM_LOC"] = tfsHdrModel.FromLoc;
            recordObject["TO_LOC_TYPE"] = tfsHdrModel.ToLocType;
            recordObject["TO_LOC"] = tfsHdrModel.ToLoc;
            recordObject["EXP_DC_DATE"] = tfsHdrModel.ExpDcDate;
            recordObject["DEPT"] = tfsHdrModel.Dept;
            recordObject["INVENTORY_TYPE"] = tfsHdrModel.InventoryType;
            recordObject["TSF_TYPE"] = tfsHdrModel.TsfType;
            recordObject["STATUS"] = tfsHdrModel.Status;
            recordObject["FREIGHT_CODE"] = "N";
            recordObject["ROUTING_CODE"] = tfsHdrModel.RoutingCode;
            recordObject["CREATE_DATE"] = tfsHdrModel.CreateDate;
            recordObject["CREATE_ID"] = "Ravi";
            recordObject["APPROVAL_DATE"] = tfsHdrModel.ApprovalDate;
            recordObject["APPROVAL_ID"] = tfsHdrModel.ApprovalId;
            recordObject["DELIVERY_DATE"] = tfsHdrModel.DeliveryDate;
            recordObject["CLOSE_DATE"] = tfsHdrModel.CloseDate;
            recordObject["EXT_REF_NO"] = tfsHdrModel.ExtRefNo;
            recordObject["REPL_TSF_APPROVE_IND"] = "N";
            recordObject["COMMENT_DESC"] = tfsHdrModel.CommentDesc;
            recordObject["EXP_DC_EOW_DATE"] = tfsHdrModel.ExpDcEowDate;
            recordObject["MRT_NO"] = tfsHdrModel.MrtNo;
            recordObject["NOT_AFTER_DATE"] = tfsHdrModel.NotAfterDate;
            recordObject["CONTEXT_TYPE"] = tfsHdrModel.ContextType;
            recordObject["CONTEXT_VALUE"] = tfsHdrModel.ContextValue;
            recordObject["RESTOCK_PCT"] = tfsHdrModel.RestockPct;
            recordObject["OPERATION"] = tfsHdrModel.Operation;
            recordObject["PROGRAM_PHASE"] = null;
            recordObject["PROGRAM_MESSAGE"] = null;
            recordObject["P_ERROR"] = null;
        }

        public virtual void SetParamsTransferHeadSearch(TransferHdrModel tfsHdrModel, OracleObject recordObject)
        {
            recordObject["TSF_NO"] = tfsHdrModel.TSF_NO;
            recordObject["TSF_PARENT_NO"] = tfsHdrModel.TsfParentNo;
            recordObject["FROM_LOC_TYPE"] = tfsHdrModel.FromLocType;
            recordObject["FROM_LOC"] = tfsHdrModel.FromLoc;
            recordObject["TO_LOC_TYPE"] = tfsHdrModel.ToLocType;
            recordObject["TO_LOC"] = tfsHdrModel.ToLoc;
            recordObject["EXP_DC_DATE"] = tfsHdrModel.ExpDcDate;
            recordObject["DEPT"] = tfsHdrModel.Dept;
            recordObject["INVENTORY_TYPE"] = tfsHdrModel.InventoryType;
            recordObject["TSF_TYPE"] = tfsHdrModel.TsfType;
            recordObject["STATUS"] = tfsHdrModel.Status;
            recordObject["FREIGHT_CODE"] = "N";
            recordObject["ROUTING_CODE"] = tfsHdrModel.RoutingCode;
            recordObject["CREATE_DATE"] = tfsHdrModel.CreateDate;
            recordObject["CREATE_ID"] = "Ravi";
            recordObject["APPROVAL_DATE"] = tfsHdrModel.ApprovalDate;
            recordObject["APPROVAL_ID"] = tfsHdrModel.ApprovalId;
            recordObject["DELIVERY_DATE"] = tfsHdrModel.DeliveryDate;
            recordObject["CLOSE_DATE"] = tfsHdrModel.CloseDate;
            recordObject["EXT_REF_NO"] = tfsHdrModel.ExtRefNo;
            recordObject["REPL_TSF_APPROVE_IND"] = "N";
            recordObject["COMMENT_DESC"] = tfsHdrModel.CommentDesc;
            recordObject["EXP_DC_EOW_DATE"] = tfsHdrModel.ExpDcEowDate;
            recordObject["MRT_NO"] = tfsHdrModel.MrtNo;
            recordObject["NOT_AFTER_DATE"] = tfsHdrModel.NotAfterDate;
            recordObject["CONTEXT_TYPE"] = tfsHdrModel.ContextType;
            recordObject["CONTEXT_VALUE"] = tfsHdrModel.ContextValue;
            recordObject["RESTOCK_PCT"] = tfsHdrModel.RestockPct;
            recordObject["OPERATION"] = tfsHdrModel.Operation;
            recordObject["PROGRAM_PHASE"] = null;
            recordObject["PROGRAM_MESSAGE"] = null;
            recordObject["P_ERROR"] = null;
        }

        public virtual void SetParamsTransferSave(TransferHdrModel tfsHdrModel, OracleObject recordObject)
        {
            recordObject["TSF_NO"] = tfsHdrModel.TSF_NO;
            recordObject["TSF_PARENT_NO"] = tfsHdrModel.TsfParentNo;
            recordObject["FROM_LOC_TYPE"] = tfsHdrModel.FromLocType;
            recordObject["FROM_LOC"] = tfsHdrModel.FromLoc;
            recordObject["TO_LOC_TYPE"] = tfsHdrModel.ToLocType;
            recordObject["TO_LOC"] = tfsHdrModel.ToLoc;
            recordObject["EXP_DC_DATE"] = tfsHdrModel.ExpDcDate;
            recordObject["DEPT"] = tfsHdrModel.Dept;
            recordObject["INVENTORY_TYPE"] = tfsHdrModel.InventoryType;
            recordObject["TSF_TYPE"] = tfsHdrModel.TsfType;
            recordObject["STATUS"] = "I";
            recordObject["FREIGHT_CODE"] = "N";
            recordObject["ROUTING_CODE"] = tfsHdrModel.RoutingCode;
            recordObject["CREATE_DATE"] = tfsHdrModel.CreateDate;
            recordObject["CREATE_ID"] = "Ravi";
            recordObject["APPROVAL_DATE"] = tfsHdrModel.ApprovalDate;
            recordObject["APPROVAL_ID"] = tfsHdrModel.ApprovalId;
            recordObject["DELIVERY_DATE"] = tfsHdrModel.DeliveryDate;
            recordObject["CLOSE_DATE"] = tfsHdrModel.CloseDate;
            recordObject["EXT_REF_NO"] = tfsHdrModel.ExtRefNo;
            recordObject["REPL_TSF_APPROVE_IND"] = "N";
            recordObject["COMMENT_DESC"] = tfsHdrModel.CommentDesc;
            recordObject["EXP_DC_EOW_DATE"] = tfsHdrModel.ExpDcEowDate;
            recordObject["MRT_NO"] = tfsHdrModel.MrtNo;
            recordObject["NOT_AFTER_DATE"] = tfsHdrModel.NotAfterDate;
            recordObject["CONTEXT_TYPE"] = tfsHdrModel.ContextType;
            recordObject["CONTEXT_VALUE"] = tfsHdrModel.ContextValue;
            recordObject["RESTOCK_PCT"] = tfsHdrModel.RestockPct;
            ////recordObject["CREATED_BY"] = "Ravi";
            ////recordObject["LAST_UPDATED_BY"] = "Ravi";
            recordObject["OPERATION"] = tfsHdrModel.Operation;
            recordObject["PROGRAM_PHASE"] = null;
            recordObject["PROGRAM_MESSAGE"] = null;
            recordObject["P_ERROR"] = null;
        }

        public async Task<List<TransferEntityDomainModel>> TransferEntityDomainGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("TSF_ENTITY_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(InventoryManagementConstants.ObjTypeDomainTransferEnity, InventoryManagementConstants.GetProcTransferEnityDomain);
            ////OracleParameterCollection parameters = this.Parameters;
            ////parameters.Clear();
            ////parameters.Add(this.ParameterBuilder(UserProfileHelper.GetUserParameter()));
            return await this.GetProcedure2<TransferEntityDomainModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        private OracleObject SetParamsTransferListGet(TransferSearchModel tfsHdrModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType(InventoryManagementConstants.RecordObjTransferHead);
            OracleObject recordObject = this.GetOracleObject(recordType);
            this.SetParamsTransferListSearch(tfsHdrModel, recordObject);
            return recordObject;
        }

        private OracleObject SetParamsTransferHeadGet(TransferHdrModel tfsHdrModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType(InventoryManagementConstants.RecordObjTransferHead);
            OracleObject recordObject = this.GetOracleObject(recordType);
            this.SetParamsTransferHeadSearch(tfsHdrModel, recordObject);
            return recordObject;
        }

        private OracleObject SetParamsTransferHeadSet(TransferHdrModel tfsHdrModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType(InventoryManagementConstants.RecordObjTransferHead);
            OracleObject recordObject = this.GetOracleObject(recordType);
            this.SetParamsTransferSave(tfsHdrModel, recordObject);
            return recordObject;
        }
    }
}