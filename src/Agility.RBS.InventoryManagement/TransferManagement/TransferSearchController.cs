﻿// <copyright file="TransferSearchController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InventoryManagement.TransferManagement
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core.Constants;
    using Agility.RBS.InventoryManagement.Common;
    using Agility.RBS.InventoryManagement.TransferManagement.Models;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    [Route("api/[controller]/[action]")]
    public class TransferSearchController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly ServiceDocument<TransferSearchModel> serviceDocument;
        private readonly TransferRepository transferRepository;

        public TransferSearchController(
            ServiceDocument<TransferSearchModel> serviceDocument,
            TransferRepository transferRepository,
        DomainDataRepository domainDataRepository)
        {
            this.serviceDocument = serviceDocument;
            this.transferRepository = transferRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in the transfer List page.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<TransferSearchModel>> List()
        {
            this.serviceDocument.DomainData.Add("transferType", this.domainDataRepository.DomainDetailData(InventoryManagementConstants.DomainHdrTransferType).Result);
            this.serviceDocument.DomainData.Add(
                "workflowStatusList", (await this.domainDataRepository.WorkflowStatusGet(RbCommonConstants.TransferModule)).OrderBy(x => x.Name));
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for fetching transfer records based on search criteria when Search button clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<TransferSearchModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.TransferSearch);
            return this.serviceDocument;
        }

        private async Task<List<TransferSearchModel>> TransferSearch()
        {
            try
            {
                var transfers = await this.transferRepository.TransferListGet(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return transfers;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }
    }
}
