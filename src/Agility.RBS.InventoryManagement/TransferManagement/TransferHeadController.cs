﻿// <copyright file="TransferHeadController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InventoryManagement.TransferManagement
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Constants;
    using Agility.RBS.Inbox.Common;
    using Agility.RBS.Inbox.Models;
    using Agility.RBS.Inbox.Repository;
    using Agility.RBS.InventoryManagement.Common;
    using Agility.RBS.InventoryManagement.TransferManagement.Models;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    [Route("api/[controller]/[action]")]
    public class TransferHeadController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly TransferRepository transferRepository;
        private readonly InboxRepository inboxRepository;
        private readonly InboxModel inboxModel;
        private ServiceDocument<TransferHdrModel> serviceDocument;
        private long transferNo;

        public TransferHeadController(
            ServiceDocument<TransferHdrModel> serviceDocument,
            TransferRepository transferRepository,
        DomainDataRepository domainDataRepository,
        InboxRepository inboxRepository)
        {
            this.serviceDocument = serviceDocument;
            this.transferRepository = transferRepository;
            this.domainDataRepository = domainDataRepository;
            this.inboxRepository = inboxRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
            this.inboxModel = new InboxModel { Operation = "CHGS", ModuleName = "TRANSFER" };
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in Transfer Head page when Add button clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<TransferHdrModel>> New()
        {
            this.serviceDocument.New(false);
            return await this.TransferDomainData();
        }

        /// <summary>
        /// This API invoked for populating fields in the transfer Head page when Edit icon clicked.
        /// </summary>
        /// <param name="id">transfer Id</param>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<TransferHdrModel>> Open(string id)
        {
            this.transferNo = Convert.ToInt64(id);
            await this.serviceDocument.OpenAsync(this.TransferHeadOpen);
           //// this.serviceDocument.DomainData.Add("inboxCommonData", this.domainDataRepository.InboxCommonGet(RbCommonConstants.OrderModule, this.transferNo).Result);
             await this.TransferDomainData();
            return this.serviceDocument;
            ////this.serviceDocument.DomainData.Add("inboxCommonData", this.domainDataRepository.InboxCommonGet(RbCommonConstants.TransferModule, id).Result);
        }

        /// <summary>
        /// This API invoked for saving field values in the transfer Head page when Save button clicked.
        /// </summary>
        /// <param name="serviceDocument">Service Document</param>
        /// <returns>ServiceDocument</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<TransferHdrModel>> Save([FromBody]ServiceDocument<TransferHdrModel> serviceDocument)
        {
            await this.serviceDocument.SaveAsync(this.TransferHeadSave);
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for submitting transfer to next level when Submit button clicked.
        /// </summary>
        /// <param name="serviceDocument">Service Document</param>
        /// <returns>ServiceDocument</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<TransferHdrModel>> Submit([FromBody]ServiceDocument<TransferHdrModel> serviceDocument)
        {
            this.serviceDocument = serviceDocument;
            if (this.serviceDocument.DataProfile.DataModel.TSF_NO == 0 || this.serviceDocument.DataProfile.DataModel.TSF_NO == null)
            {
                this.inboxModel.Operation = "DFT";
            }

            this.inboxModel.StateID = RbsServiceDocCustomizationHelper.GetStateIdDotNetToOracle(this.serviceDocument);
            await this.serviceDocument.TransitAsync(this.TransferHeadSave);
            if (this.serviceDocument.Result.Type == MessageType.Success)
            {
                await this.inboxRepository.SetInbox(this.inboxModel, this.serviceDocument);
            }

            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for rejecting transfer to previous level when Reject button clicked.
        /// </summary>
        /// <param name="serviceDocument">Service Document</param>
        /// <returns>ServiceDocument</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<TransferHdrModel>> Reject([FromBody]ServiceDocument<TransferHdrModel> serviceDocument)
        {
            this.SetServiceDocumentAndInboxStateId(serviceDocument);
            await this.serviceDocument.TransitAsync(GuidConverter.DotNetToOracle(serviceDocument.DataProfile.DataModel.WorkflowForm.StateId), true);
            if (this.serviceDocument.Result.Type == MessageType.Success)
            {
                this.inboxModel.Operation = "REJ";
                this.inboxModel.ActionType = "R";
                this.inboxModel.ToStateID = GuidConverter.DotNetToOracle(this.serviceDocument.DataProfile.DataModel.WorkflowStateId.ToString());
                this.inboxRepository.SetInbox(this.inboxModel, this.serviceDocument);
            }

            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for rejecting transfer to previous level when Send Back For Review button clicked.
        /// </summary>
        /// <param name="serviceDocument">Service Document</param>
        /// <returns>ServiceDocument</returns>
        /// <response code="401">Un Authorized</response>
        [AllowAnonymous]
        [HttpPost]
        public async Task<ServiceDocument<TransferHdrModel>> SendBackForReview([FromBody]ServiceDocument<TransferHdrModel> serviceDocument)
        {
            var workFlowInstance = serviceDocument.DataProfile.DataModel.WorkflowInstance;
            string toStateId = this.inboxRepository.GetStateId("SBR", workFlowInstance);
            toStateId = GuidConverter.OracleToDotNet(toStateId);
            await this.serviceDocument.TransitAsync(toStateId, true);

            if (this.serviceDocument.Result.Type == MessageType.Success)
            {
                await this.inboxRepository.SetInbox(
                    new InboxModel
                    {
                        Operation = "SBR",
                        ActionType = "B",
                        ToStateID = GuidConverter.DotNetToOracle(workFlowInstance.WorkflowStateId.ToString()),
                        StateID = GuidConverter.DotNetToOracle(toStateId)
                    },
                    this.serviceDocument);
            }

            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for approving transfer when Approve button clicked.
        /// </summary>
        /// <param name="serviceDocument">Service Document</param>
        /// <returns>ServiceDocument</returns>
        /// <response code="401">Un Authorized</response>
        [AllowAnonymous]
        [HttpPost]
        public async Task<ServiceDocument<TransferHdrModel>> Approve([FromBody]ServiceDocument<TransferHdrModel> serviceDocument)
        {
            this.transferNo = Convert.ToInt64(serviceDocument.DataProfile.DataModel.TSF_NO);
            var openedSdoc = await this.serviceDocument.OpenAsync(this.TransferHeadOpen);
            try
            {
                WorkflowAction wfApproveAction = openedSdoc.DataProfile.ActionService.AllowedActions.FirstOrDefault(a => a.ActionName == "Approve");
                if (wfApproveAction != null)
                {
                    openedSdoc.DataProfile.ActionService.CurrentActionId = wfApproveAction.TransitionClaim;
                    await openedSdoc.TransitAsync(this.TransferHeadSave);
                    await this.inboxRepository.SetInbox(this.inboxModel, openedSdoc);
                }
            }
            catch (Exception e)
            {
                openedSdoc.Result = new ServiceDocumentResult
                {
                    InnerException = string.Empty,
                    StackTrace = e.StackTrace,
                    Type = MessageType.Exception
                };
                return this.serviceDocument;
            }

            return openedSdoc;
        }

        /// <summary>
        /// This API invoked for populating dependent fields based on selected Supplier.
        /// </summary>
        /// <param name="supplier">Supplier Id</param>
        /// <returns>List Of SupplierDetailDomainModel</returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<List<SupplierDetailDomainModel>> SupplerDetailsGet(int supplier)
        {
            return await this.domainDataRepository.SupplierDetailsGet(supplier);
        }

        /// <summary>
        /// This API invoked for populating dependent field Exchange Rate based on selected Import Country.
        /// </summary>
        /// <param name="pSuppCountry">Import Country Id</param>
        /// <param name="pOrdLocation">Location Id</param>
        /// <returns>Exchange Rate</returns>
        [AllowAnonymous]
        [HttpGet]
        public decimal? ExchangeRateGet(string pSuppCountry, string pOrdLocation)
        {
            try
            {
                return this.domainDataRepository.ExchangeRateGet(pSuppCountry, pOrdLocation);
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return default(decimal?);
            }
        }

        private async Task<TransferHdrModel> TransferHeadOpen()
        {
            try
            {
                var transferHead = await this.transferRepository.TransferHeadGet(this.transferNo, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return transferHead;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> TransferHeadSave()
        {
            this.serviceDocument.Result = await this.transferRepository.TransferHeadSet(this.serviceDocument.DataProfile.DataModel);
            if (this.serviceDocument.Result.Type != MessageType.Success)
            {
                return false;
            }

            this.serviceDocument.DataProfile.DataModel.TSF_NO = this.transferRepository.TSF_NO;
            return true;
        }

        private async Task<ServiceDocument<TransferHdrModel>> TransferDomainData()
        {
            this.serviceDocument.DomainData.Add("transferType", this.domainDataRepository.DomainDetailData(InventoryManagementConstants.DomainHdrTransferType).Result);
            this.serviceDocument.DomainData.Add("contextType", this.domainDataRepository.DomainDetailData(InventoryManagementConstants.DomainHdrContextType).Result);
            this.serviceDocument.DomainData.Add("locType", this.domainDataRepository.LocationTypeDomainGet().Result);
            this.serviceDocument.DomainData.Add("status", this.domainDataRepository.DomainDetailData(InventoryManagementConstants.DomainHdrStatus).Result);
            this.serviceDocument.DomainData.Add("TsfEntity", this.transferRepository.TransferEntityDomainGet().Result);
            ////this.serviceDocument.DomainData.Add("group", this.domainDataRepository.DeptBuyerDomainGet().Result);
            this.serviceDocument.DomainData.Add("group", this.domainDataRepository.GroupDomainGet().Result);
            this.serviceDocument.DomainData.Add("locationsType", this.domainDataRepository.DomainDetailData(InventoryManagementConstants.DomainHdrLocationType).Result);
            this.serviceDocument.DomainData.Add("inventoryType", this.domainDataRepository.DomainDetailData(InventoryManagementConstants.DomainHdrInventoryType).Result);
            this.serviceDocument.DomainData.Add("statusType", this.domainDataRepository.DomainDetailData(InventoryManagementConstants.DomainStatus).Result);

            this.serviceDocument.LocalizationData.AddData("TransferHead");
            return this.serviceDocument;
        }

        private void SetServiceDocumentAndInboxStateId(ServiceDocument<TransferHdrModel> serviceDocument)
        {
            this.serviceDocument = serviceDocument;
            this.inboxModel.StateID = RbsServiceDocCustomizationHelper.GetStateIdDotNetToOracle(serviceDocument);
        }
    }
}
