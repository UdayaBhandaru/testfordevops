﻿// <copyright file="TransferDetailsController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InventoryManagement.TransferManagement
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Documents.Models;
    using Agility.RBS.InventoryManagement.Common;
    using Agility.RBS.InventoryManagement.TransferManagement.Models;
    using Agility.RBS.ItemManagement.Item.Models;
    using Agility.RBS.ItemManagement.Item.Repositories;
    using Agility.RBS.MDM.Repositories;
    using ExcelDataReader;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]

    [Route("api/[controller]/[action]")]
    public class TransferDetailsController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly ServiceDocument<TransferDetailsModel> serviceDocument;
        private readonly TransferRepository transferRepository;
        private long transferNo;

        public TransferDetailsController(
            ServiceDocument<TransferDetailsModel> serviceDocument,
            TransferRepository transferRepository,
        DomainDataRepository domainDataRepository,
        ItemSelectRepository itemSelectRepository)
        {
            this.serviceDocument = serviceDocument;
            this.transferRepository = transferRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        /// <summary>
        /// This API invoked for loading grid data along with data for autocomplete, radio button fields in transfer Details Page.
        /// </summary>
        /// <param name="tsF_NO">transfer Id</param>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<TransferDetailsModel>> List(string tsF_NO)
        {
            this.transferNo = Convert.ToInt64(tsF_NO);
            this.TransferDetailsDomainData();
            await this.serviceDocument.ToListAsync(this.TransferDetailsList);
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for saving grid data in the transfer Details page when Save button clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<TransferDetailsModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.TransferDetailSave);
            return this.serviceDocument;
        }

        [HttpGet]
        public async Task<List<ItemQtyDomainModel>> ItemQtyDetailsGet(string item, string tsF_NO)
        {
            return await this.transferRepository.ItemQtyDetailsGet(item, tsF_NO);
        }

        /// <summary>
        /// This API invoked for populating dependent fields when Get Item hyperlink clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<TransferDetailsModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.TransferDetailsList);
            return this.serviceDocument;
        }

        [HttpGet]
        private async Task<List<TransferDetailsModel>> TransferDetailsList()
        {
            try
            {
                var costChangeDetails = await this.transferRepository.TransferDetailsGet(this.transferNo, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return costChangeDetails;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> TransferDetailSave()
        {
            this.serviceDocument.Result = await this.transferRepository.SetTransferDetails(this.serviceDocument.DataProfile.DataModel);
            if (this.serviceDocument.Result.Type != MessageType.Success)
            {
                return false;
            }

            return true;
        }

        private void TransferDetailsDomainData()
        {
            this.serviceDocument.DomainData.Add("locType", this.domainDataRepository.DomainDetailData(InventoryManagementConstants.DomainHdrLocType).Result);
            this.serviceDocument.DomainData.Add("locationsType", this.domainDataRepository.DomainDetailData(InventoryManagementConstants.DomainHdrLocationType).Result);
            this.serviceDocument.DomainData.Add("inventoryType", this.domainDataRepository.DomainDetailData(InventoryManagementConstants.DomainHdrInventoryType).Result);
            this.serviceDocument.DomainData.Add("group", this.domainDataRepository.GroupDomainGet().Result);
            this.serviceDocument.DomainData.Add("clrItemList", this.domainDataRepository.DomainDetailData(InventoryManagementConstants.DomainClrItemList).Result);
            this.serviceDocument.DomainData.Add("transferQtyType", this.domainDataRepository.DomainDetailData(InventoryManagementConstants.DomainTransferQtyType).Result);
            this.serviceDocument.DomainData.Add("transferType", this.domainDataRepository.DomainDetailData(InventoryManagementConstants.DomainHdrTransferType).Result);
            this.serviceDocument.DomainData.Add("transferPriceAdjTypes", this.domainDataRepository.DomainDetailData(InventoryManagementConstants.DomainTransferPriceAdjTypes).Result);
            this.serviceDocument.DomainData.Add("uomTransfer", this.domainDataRepository.DomainDetailData(InventoryManagementConstants.DomainUnitofTransfer).Result);
            this.serviceDocument.LocalizationData.AddData("TransferDetails");
        }
    }
}
