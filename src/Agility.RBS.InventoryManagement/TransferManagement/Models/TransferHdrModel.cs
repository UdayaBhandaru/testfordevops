﻿// <copyright file="TransferHdrModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InventoryManagement.TransferManagement.Models
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Inbox.Common;

    [DataProfile(Name = "TSFHEAD")]
    public class TransferHdrModel : ProfileEntity<TransferHdrWfModel>, IInboxCommonModel
    {
        [Column("TSF_NO")]
        public long? TSF_NO { get; set; }

        public string TsfNoResult { get; set; }

        [NotMapped]
        public long? TsfParentNo { get; set; }

        [NotMapped]
        public string FromLocType { get; set; }

        [NotMapped]
        public int? FromLoc { get; set; }

        [NotMapped]
        public string ToLocType { get; set; }

        [NotMapped]
        public int? ToLoc { get; set; }

        [NotMapped]
        public DateTime? ExpDcDate { get; set; }

        [NotMapped]
        public int? Dept { get; set; }

        [NotMapped]
        public string DeptDesc { get; set; }

        [NotMapped]
        public string InventoryType { get; set; }

        [NotMapped]
        public string TsfType { get; set; }

        [NotMapped]
        public string Status { get; set; }

        [NotMapped]
        public string StatusDesc { get; set; }

        [NotMapped]
        public string FreightCode { get; set; }

        [NotMapped]
        public string RoutingCode { get; set; }

        [NotMapped]
        public DateTime? CreateDate { get; set; }

        [NotMapped]
        public string CreateId { get; set; }

        [NotMapped]
        public DateTime? ApprovalDate { get; set; }

        [NotMapped]
        public string ApprovalId { get; set; }

        [NotMapped]
        public DateTime? DeliveryDate { get; set; }

        [NotMapped]
        public DateTime? CloseDate { get; set; }

        [NotMapped]
        public string ExtRefNo { get; set; }

        [NotMapped]
        public string ReplTsfApproveInd { get; set; }

        [NotMapped]
        public string CommentDesc { get; set; }

        [NotMapped]
        public DateTime? ExpDcEowDate { get; set; }

        [NotMapped]
        public int? MrtNo { get; set; }

        [NotMapped]
        public DateTime? NotAfterDate { get; set; }

        [NotMapped]
        public string ContextType { get; set; }

        [NotMapped]
        public string ContextValue { get; set; }

        [NotMapped]
        public decimal? RestockPct { get; set; }

        [NotMapped]
        public string Operation { get; set; }

        [NotMapped]
        public string ProgramPhase { get; set; }

        [NotMapped]
        public string ProgramMessage { get; set; }

        [NotMapped]
        public string Error { get; set; }

        [NotMapped]
        [Column("ORGANISATION_ID")]
        public new int? OrganizationId { get; set; }

        [NotMapped]
        [Column("LAST_UPDATED_BY")]
        public new string ModifiedBy { get; set; }

        [NotMapped]
        [Column("DeletedInd")]
        public new bool? DeletedInd { get; set; }

        [NotMapped]
        [Column("LAST_UPDATED_DATE")]
        public new DateTimeOffset? ModifiedDate { get; set; }

        [NotMapped]
        [Column("CREATED_DATE")]
        public new DateTimeOffset? CreatedDate { get; set; }

        [NotMapped]
        [Column("CREATED_BY")]
        public new string CreatedBy { get; set; }

        [NotMapped]
        public int? WfNxtStateInd { get; set; }

        [NotMapped]
        public Core.Models.RbWfHeaderModel WorkflowForm { get; set; }

        [NotMapped]
        public int? FileCount { get; set; }
    }
}
