﻿// <copyright file="TransferDetailsModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InventoryManagement.TransferManagement.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using Agility.Framework.Core.Profile.Entities;

    public class TransferDetailsModel : ProfileEntity
    {
        public TransferDetailsModel()
        {
            this.TransferDetails = new List<TransferDetailsModel>();
        }

        public long? TSF_NO { get; set; }

        public string ItemType { get; set; }

        public string Item { get; set; }

        public string DiffId { get; set; }

        public int? ItemList { get; set; }

        public string TsfType { get; set; }

        public int? InvStatus { get; set; }

        public string QtyType { get; set; }

        public decimal? TsfQty { get; set; }

        public string AdjustType { get; set; }

        public int? AdjustValue { get; set; }

        public string FromLocType { get; set; }

        public int? FromLoc { get; set; }

        public string ToLocType { get; set; }

        public int? ToLoc { get; set; }

        public int? Dept { get; set; }

        public string EntityType { get; set; }

        public decimal? RestockPct { get; set; }

        public string Operation { get; set; }

        public string Program_Phase { get; set; }

        public string Program_Message { get; set; }

        public string Error { get; set; }

        public List<TransferDetailsModel> TransferDetails { get; private set; }

        public string TableName { get; set; }

        public string WorkflowStatus { get; set; }

        public string WorkflowStatusDesc { get; set; }

        public string UnitOfTransfer { get; set; }

        public decimal? StockOnHand { get; set; }

        public decimal? UnitCost { get; set; }

        public int? TransferPrice { get; set; }

        public string ItemDesc { get; set; }

        public decimal? UnitRetail { get; set; }

        public decimal? TranQty { get; set; }

        public decimal? ShipQty { get; set; }

        public decimal? RecvQty { get; set; }
    }
}