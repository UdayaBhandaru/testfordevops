﻿// <copyright file="TransferHdrWfModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InventoryManagement.TransferManagement.Models
{
    using System.ComponentModel.DataAnnotations;
    using Agility.Framework.Core.Workflow.Entities;

    public class TransferHdrWfModel : WorkFlowBaseInstance, IWorkFlowInstance
    {
        [Key]
        public int WorkflowInstanceId { get; set; }
    }
}
