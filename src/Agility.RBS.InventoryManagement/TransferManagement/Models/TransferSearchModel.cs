﻿// <copyright file="TransferSearchModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.InventoryManagement.TransferManagement.Models
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Core.Models;

    public class TransferSearchModel : ProfileEntity, Core.IPaginationModel
    {
        [NotMapped]
        public int? Supplier { get; set; }

        [NotMapped]
        public string SupName { get; set; }

        [NotMapped]
        public int? DeptId { get; set; }

        [NotMapped]
        public string DeptDesc { get; set; }

        [NotMapped]
        public string LocType { get; set; }

        [NotMapped]
        public string LocTypeDesc { get; set; }

        [NotMapped]
        public int? Location { get; set; }

        [NotMapped]
        public string LocationName { get; set; }

        [NotMapped]
        public string ItemBarcode { get; set; }

        [NotMapped]
        public string ItemDesc { get; set; }

        [Column("ORDER_NO")]
        public int? ORDER_NO { get; set; }

        [NotMapped]
        public string OrderType { get; set; }

        [NotMapped]
        public string OrderTypeDesc { get; set; }

        [NotMapped]
        public string OrderDesc { get; set; }

        [NotMapped]
        public string Status { get; set; }

        [NotMapped]
        public string StatusDesc { get; set; }

        [NotMapped]
        public string DateType { get; set; }

        [NotMapped]
        public DateTime? StartDate { get; set; }

        [NotMapped]
        public DateTime? EndDate { get; set; }

        [NotMapped]
        public string Operation { get; set; }

        [NotMapped]
        public string TableName { get; set; }

        [NotMapped]
        public int? StartRow { get; set; }

        [NotMapped]
        public int? EndRow { get; set; }

        [NotMapped]
        public long? TotalRows { get; set; }

        [NotMapped]
        public SortModel[] SortModel { get; set; }

        [Column("TSF_NO")]
        public long? TSF_NO { get; set; }

        [Column("TSF_PARENT_NO")]
        public long? TSF_PARENT_NO { get; set; }

        [NotMapped]
        public string FromLocType { get; set; }

        [NotMapped]
        public int? FromLoc { get; set; }

        [NotMapped]
        public string ToLocType { get; set; }

        [NotMapped]
        public int? ToLoc { get; set; }

        [NotMapped]
        public DateTime? ExpDcDate { get; set; }

        [NotMapped]
        public int? Dept { get; set; }

        [NotMapped]
        public string InventoryType { get; set; }

        [NotMapped]
        public string TsfType { get; set; }

        [NotMapped]
        public string FreightCode { get; set; }

        [NotMapped]
        public string RoutingCode { get; set; }

        [NotMapped]
        public DateTime? CreateDate { get; set; }

        [NotMapped]
        public string CreateId { get; set; }

        [NotMapped]
        public DateTime? ApprovalDate { get; set; }

        [NotMapped]
        public string ApprovalId { get; set; }

        [NotMapped]
        public DateTime? DeliveryDate { get; set; }

        [NotMapped]
        public DateTime? CloseDate { get; set; }

        [NotMapped]
        public string ExtRefNo { get; set; }

        [NotMapped]
        public string ReplTsfApproveInd { get; set; }

        [NotMapped]
        public string CommentDesc { get; set; }

        [NotMapped]
        public DateTime? ExpDcEowDate { get; set; }

        [NotMapped]
        public int? MrtNo { get; set; }

        [NotMapped]
        public DateTime? NotAfterDate { get; set; }

        [NotMapped]
        public string ContextType { get; set; }

        [NotMapped]
        public string ContextValue { get; set; }

        [NotMapped]
        public int? RestockPct { get; set; }

        [NotMapped]
        [Column("ORGANISATION_ID")]
        public new int? OrganizationId { get; set; }

        [NotMapped]
        [Column("CREATED_BY")]
        public new string CreatedBy { get; set; }

        [NotMapped]
        [Column("LAST_UPDATED_BY")]
        public new string ModifiedBy { get; set; }

        [NotMapped]
        [Column("DeletedInd")]
        public new bool? DeletedInd { get; set; }

        [NotMapped]
        [Column("CREATED_DATE")]
        public new DateTimeOffset? CreatedDate { get; set; }

        [NotMapped]
        [Column("LAST_UPDATED_DATE")]
        public new DateTimeOffset? ModifiedDate { get; set; }

        [NotMapped]
        public int? WfNxtStateInd { get; set; }

        [NotMapped]
        public Core.Models.RbWfHeaderModel WorkflowForm { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Error { get; set; }

        [NotMapped]
        public int? FileCount { get; set; }

        [NotMapped]
        public string FromLocDesc { get; set; }

        [NotMapped]
        public string ToLocDesc { get; set; }

        [NotMapped]
        public string TsfTypeDesc { get; set; }

        [NotMapped]
        public string StatusType { get; set; }
    }
}
