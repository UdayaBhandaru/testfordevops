﻿// <copyright file="TransportationModelMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Transportation.Transportation.Mapping
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.RBS.Core;
    using Agility.RBS.Transportation.Transportation.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class TransportationModelMapping : Profile
    {
        public TransportationModelMapping()
            : base("TransportationModelMapping")
        {
            this.CreateMap<OracleObject, TransportationModel>()
            .ForMember(m => m.TransportationId, opt => opt.MapFrom(r => OracleNullHandler.DbNullInt64Handler(r["TRANSPORTATION_ID"])))
            .ForMember(m => m.VesselId, opt => opt.MapFrom(r => r["VESSEL_ID"]))
            .ForMember(m => m.VoyageFltId, opt => opt.MapFrom(r => r["VOYAGE_FLT_ID"]))
            .ForMember(m => m.EstimatedDepartDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["ESTIMATED_DEPART_DATE"])))
            .ForMember(m => m.ActualDepartDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["ACTUAL_DEPART_DATE"])))
            .ForMember(m => m.OrderNo, opt => opt.MapFrom(r => OracleNullHandler.DbNullInt64Handler(r["ORDER_NO"])))
            .ForMember(m => m.Item, opt => opt.MapFrom(r => r["ITEM"]))
            .ForMember(m => m.ItemDesc, opt => opt.MapFrom(r => r["ITEM_DESC"]))
            .ForMember(m => m.Supplier, opt => opt.MapFrom(r => OracleNullHandler.DbNullInt64Handler(r["SUPPLIER"])))
            .ForMember(m => m.SupName, opt => opt.MapFrom(r => r["SUP_NAME"]))
            .ForMember(m => m.BlAwbId, opt => opt.MapFrom(r => r["BL_AWB_ID"]))
            .ForMember(m => m.ContainerId, opt => opt.MapFrom(r => r["CONTAINER_ID"]))
            .ForMember(m => m.InvoiceId, opt => opt.MapFrom(r => r["INVOICE_ID"]))
            .ForMember(m => m.InvoiceDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["INVOICE_DATE"])))
            .ForMember(m => m.ShipmentNo, opt => opt.MapFrom(r => r["SHIPMENT_NO"]))
            .ForMember(m => m.ActualArrivalDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["ACTUAL_ARRIVAL_DATE"])))
            .ForMember(m => m.DeliveryDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["DELIVERY_DATE"])))
            .ForMember(m => m.Status, opt => opt.MapFrom(r => r["STATUS"]))
            .ForMember(m => m.StatusDesc, opt => opt.MapFrom(r => r["STATUS_DESC"]))
            .ForMember(m => m.TranModeId, opt => opt.MapFrom(r => r["TRAN_MODE_ID"]))
            .ForMember(m => m.TransModeDesc, opt => opt.MapFrom(r => r["TRANS_MODE_DESC"]))
            .ForMember(m => m.VesselScacCode, opt => opt.MapFrom(r => r["VESSEL_SCAC_CODE"]))
            .ForMember(m => m.VesselScacDesc, opt => opt.MapFrom(r => r["VESSEL_SCAC_DESC"]))
            .ForMember(m => m.ContainerScacCode, opt => opt.MapFrom(r => r["CONTAINER_SCAC_CODE"]))
            .ForMember(m => m.ContainerScacDesc, opt => opt.MapFrom(r => r["CONTAINER_SCAC_DESC"]))
            .ForMember(m => m.SealId, opt => opt.MapFrom(r => r["SEAL_ID"]))
            .ForMember(m => m.FreightType, opt => opt.MapFrom(r => r["FREIGHT_TYPE"]))
            .ForMember(m => m.FreightTypeDesc, opt => opt.MapFrom(r => r["FREIGHT_TYPE_DESC"]))
            .ForMember(m => m.FreightSize, opt => opt.MapFrom(r => r["FREIGHT_SIZE"]))
            .ForMember(m => m.FreightSizeDesc, opt => opt.MapFrom(r => r["FREIGHT_SIZE_DESC"]))
            .ForMember(m => m.OriginCountryId, opt => opt.MapFrom(r => r["ORIGIN_COUNTRY_ID"]))
            .ForMember(m => m.OriginCountryDesc, opt => opt.MapFrom(r => r["ORIGIN_COUNTRY_DESC"]))
            .ForMember(m => m.ConsolidationCountryId, opt => opt.MapFrom(r => r["CONSOLIDATION_COUNTRY_ID"]))
            .ForMember(m => m.ConsolidationCountryDesc, opt => opt.MapFrom(r => r["CONSOLIDATION_COUNTRY_DESC"]))
            .ForMember(m => m.ExportCountryId, opt => opt.MapFrom(r => r["EXPORT_COUNTRY_ID"]))
            .ForMember(m => m.ExportCountryDesc, opt => opt.MapFrom(r => r["EXPORT_COUNTRY_DESC"]))
            .ForMember(m => m.EstimatedArrivalDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["ESTIMATED_ARRIVAL_DATE"])))
            .ForMember(m => m.LadingPort, opt => opt.MapFrom(r => r["LADING_PORT"]))
            .ForMember(m => m.LadingPortDesc, opt => opt.MapFrom(r => r["LADING_PORT_DESC"]))
            .ForMember(m => m.DischargePort, opt => opt.MapFrom(r => r["DISCHARGE_PORT"]))
            .ForMember(m => m.DischargePortDesc, opt => opt.MapFrom(r => r["DISCHARGE_PORT_DESC"]))
            .ForMember(m => m.Consolidator, opt => opt.MapFrom(r => r["CONSOLIDATOR"]))
            .ForMember(m => m.ConsolidatorDesc, opt => opt.MapFrom(r => r["CONSOLIDATOR_DESC"]))
            .ForMember(m => m.ReceiptId, opt => opt.MapFrom(r => r["RECEIPT_ID"]))
            .ForMember(m => m.FcrId, opt => opt.MapFrom(r => r["FCR_ID"]))
            .ForMember(m => m.FcrDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["FCR_DATE"])))
            .ForMember(m => m.ServiceContractNo, opt => opt.MapFrom(r => r["SERVICE_CONTRACT_NO"]))
            .ForMember(m => m.InTransitNo, opt => opt.MapFrom(r => r["IN_TRANSIT_NO"]))
            .ForMember(m => m.InTransitDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["IN_TRANSIT_DATE"])))
            .ForMember(m => m.LotNo, opt => opt.MapFrom(r => r["LOT_NO"]))
            .ForMember(m => m.InvoiceAmt, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["INVOICE_AMT"])))
            .ForMember(m => m.CurrencyCode, opt => opt.MapFrom(r => r["CURRENCY_CODE"]))
            .ForMember(m => m.ExchangeRate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["EXCHANGE_RATE"])))
            .ForMember(m => m.CartonQty, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["CARTON_QTY"])))
            .ForMember(m => m.CartonUom, opt => opt.MapFrom(r => r["CARTON_UOM"]))
            .ForMember(m => m.ItemQty, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["ITEM_QTY"])))
            .ForMember(m => m.ItemQtyUom, opt => opt.MapFrom(r => r["ITEM_QTY_UOM"]))
            .ForMember(m => m.GrossWt, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["GROSS_WT"])))
            .ForMember(m => m.GrossWtUom, opt => opt.MapFrom(r => r["GROSS_WT_UOM"]))
            .ForMember(m => m.NetWt, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["NET_WT"])))
            .ForMember(m => m.NetWtUom, opt => opt.MapFrom(r => r["NET_WT_UOM"]))
            .ForMember(m => m.Cubic, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["CUBIC"])))
            .ForMember(m => m.CubicUom, opt => opt.MapFrom(r => r["CUBIC_UOM"]))
            .ForMember(m => m.PackingMethod, opt => opt.MapFrom(r => r["PACKING_METHOD"]))
            .ForMember(m => m.PackingMethodDesc, opt => opt.MapFrom(r => r["PACKING_METHOD_DESC"]))
            .ForMember(m => m.RushInd, opt => opt.MapFrom(r => r["RUSH_IND"]))
            .ForMember(m => m.CandidateInd, opt => opt.MapFrom(r => r["CANDIDATE_IND"]))
            .ForMember(m => m.Comments, opt => opt.MapFrom(r => r["COMMENTS"]))
            .ForMember(m => m.SelectedInd, opt => opt.MapFrom(r => r["SELECTED_IND"]))
            .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, TransportationSearchModel>()
            .ForMember(m => m.TransportationId, opt => opt.MapFrom(r => OracleNullHandler.DbNullInt64Handler(r["TRANSPORTATION_ID"])))
            .ForMember(m => m.VesselId, opt => opt.MapFrom(r => r["VESSEL_ID"]))
            .ForMember(m => m.VoyageFltId, opt => opt.MapFrom(r => r["VOYAGE_FLT_ID"]))
            .ForMember(m => m.EstimatedDepartDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["ESTIMATED_DEPART_DATE"])))
            .ForMember(m => m.OrderNo, opt => opt.MapFrom(r => OracleNullHandler.DbNullInt64Handler(r["ORDER_NO"])))
            .ForMember(m => m.Item, opt => opt.MapFrom(r => r["ITEM"]))
            .ForMember(m => m.ItemDesc, opt => opt.MapFrom(r => r["ITEM_DESC"]))
            .ForMember(m => m.ShipmentNo, opt => opt.MapFrom(r => r["SHIPMENT_NO"]))
            .ForMember(m => m.Status, opt => opt.MapFrom(r => r["STATUS"]))
            .ForMember(m => m.StatusDesc, opt => opt.MapFrom(r => r["STATUS_DESC"]));

            this.CreateMap<OracleObject, TransTotalModel>()
            .ForMember(m => m.TransportationId, opt => opt.MapFrom(r => OracleNullHandler.DbNullInt64Handler(r["TRANSPORTATION_ID"])))
            .ForMember(m => m.TotalsLevel, opt => opt.MapFrom(r => r["TOTALS_LEVEL"]))
            .ForMember(m => m.TotalUnits, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["TOTAL_UNITS"])))
            .ForMember(m => m.UnitsUom, opt => opt.MapFrom(r => r["UNITS_UOM"]))
            .ForMember(m => m.TotalCartons, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["TOTAL_CARTONS"])))
            .ForMember(m => m.CartonsUom, opt => opt.MapFrom(r => r["CARTONS_UOM"]))
            .ForMember(m => m.TotalGrossWeight, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["TOTAL_GROSS_WEIGHT"])))
            .ForMember(m => m.GrossWeightUom, opt => opt.MapFrom(r => r["GROSS_WEIGHT_UOM"]))
            .ForMember(m => m.TotalNetWeight, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["TOTAL_NET_WEIGHT"])))
            .ForMember(m => m.NetWeightUom, opt => opt.MapFrom(r => r["NET_WEIGHT_UOM"]))
            .ForMember(m => m.TotalCube, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["TOTAL_CUBE"])))
            .ForMember(m => m.CubeUom, opt => opt.MapFrom(r => r["CUBE_UOM"]))
            .ForMember(m => m.TotalValue, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["TOTAL_VALUE"])))
            .ForMember(m => m.CurrencyCode, opt => opt.MapFrom(r => r["CURRENCY_CODE"]));
        }
    }
}
