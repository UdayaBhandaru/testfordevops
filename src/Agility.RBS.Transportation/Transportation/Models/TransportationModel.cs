﻿// <copyright file="TransportationModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Transportation.Transportation.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text;
    using Agility.Framework.Core.Profile.Entities;

    public class TransportationModel : ProfileEntity
    {
        [Key]
        public long? TransportationId { get; set; }

        public string VesselId { get; set; }

        public string VoyageFltId { get; set; }

        public DateTime? EstimatedDepartDate { get; set; }

        public DateTime? ActualDepartDate { get; set; }

        public long? OrderNo { get; set; }

        public string Item { get; set; }

        public string ItemDesc { get; set; }

        public long? Supplier { get; set; }

        public string SupName { get; set; }

        public string BlAwbId { get; set; }

        public string ContainerId { get; set; }

        public string InvoiceId { get; set; }

        public DateTime? InvoiceDate { get; set; }

        public string ShipmentNo { get; set; }

        public DateTime? ActualArrivalDate { get; set; }

        public DateTime? DeliveryDate { get; set; }

        public string Status { get; set; }

        public string StatusDesc { get; set; }

        public string TranModeId { get; set; }

        public string TransModeDesc { get; set; }

        public string VesselScacCode { get; set; }

        public string VesselScacDesc { get; set; }

        public string ContainerScacCode { get; set; }

        public string ContainerScacDesc { get; set; }

        public string SealId { get; set; }

        public string FreightType { get; set; }

        public string FreightTypeDesc { get; set; }

        public string FreightSize { get; set; }

        public string FreightSizeDesc { get; set; }

        public string OriginCountryId { get; set; }

        public string OriginCountryDesc { get; set; }

        public string ConsolidationCountryId { get; set; }

        public string ConsolidationCountryDesc { get; set; }

        public string ExportCountryId { get; set; }

        public string ExportCountryDesc { get; set; }

        public DateTime? EstimatedArrivalDate { get; set; }

        public string LadingPort { get; set; }

        public string LadingPortDesc { get; set; }

        public string DischargePort { get; set; }

        public string DischargePortDesc { get; set; }

        public string Consolidator { get; set; }

        public string ConsolidatorDesc { get; set; }

        public string ReceiptId { get; set; }

        public string FcrId { get; set; }

        public DateTime? FcrDate { get; set; }

        public string ServiceContractNo { get; set; }

        public string InTransitNo { get; set; }

        public DateTime? InTransitDate { get; set; }

        public string LotNo { get; set; }

        public decimal? InvoiceAmt { get; set; }

        public string CurrencyCode { get; set; }

        public decimal? ExchangeRate { get; set; }

        public decimal? CartonQty { get; set; }

        public string CartonUom { get; set; }

        public decimal? ItemQty { get; set; }

        public string ItemQtyUom { get; set; }

        public decimal? GrossWt { get; set; }

        public string GrossWtUom { get; set; }

        public decimal? NetWt { get; set; }

        public string NetWtUom { get; set; }

        public decimal? Cubic { get; set; }

        public string CubicUom { get; set; }

        public string PackingMethod { get; set; }

        public string PackingMethodDesc { get; set; }

        public string RushInd { get; set; }

        public string CandidateInd { get; set; }

        public string Comments { get; set; }

        public string SelectedInd { get; set; }

        public string Operation { get; set; }
    }
}
