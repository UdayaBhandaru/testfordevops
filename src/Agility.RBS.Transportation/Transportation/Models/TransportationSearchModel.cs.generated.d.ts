declare module Server.Dtos {
	interface TransportationSearchModel extends ProfileEntity {
		transportationId?: number;
		vesselId: string;
		voyageFltId: string;
		estimatedDepartDate?: Date;
		orderNo?: number;
		item: string;
		itemDesc: string;
		shipmentNo: string;
		status: string;
		statusDesc: string;
		startRow?: number;
		endRow?: number;
		totalRows?: number;
		sortModel: {
			colId: string;
			sort: string;
		}[];
	}
}
