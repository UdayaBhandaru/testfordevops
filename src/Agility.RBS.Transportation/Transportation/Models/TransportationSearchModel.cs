﻿// <copyright file="TransportationSearchModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Transportation.Transportation.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Core.Models;
    using Agility.RBS.Inbox.Common;

    public class TransportationSearchModel : ProfileEntity, Core.IPaginationModel
    {
        public long? TransportationId { get; set; }

        public string VesselId { get; set; }

        public string VoyageFltId { get; set; }

        public DateTime? EstimatedDepartDate { get; set; }

        public long? OrderNo { get; set; }

        public string Item { get; set; }

        public string ItemDesc { get; set; }

        public string ShipmentNo { get; set; }

        public string Status { get; set; }

        public string StatusDesc { get; set; }

        public int? StartRow { get; set; }

        public int? EndRow { get; set; }

        public long? TotalRows { get; set; }

        public SortModel[] SortModel { get; set; }
    }
}