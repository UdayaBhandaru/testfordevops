﻿// <copyright file="TransTotalModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Transportation.Transportation.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.Framework.Core.Profile.Entities;

    public class TransTotalModel
    {
        public long? TransportationId { get; set; }

        public string ShipmentNo { get; set; }

        public string TotalsLevel { get; set; }

        public string TotalLvlDesc { get; set; }

        public decimal? TotalUnits { get; set; }

        public string UnitsUom { get; set; }

        public decimal? TotalCartons { get; set; }

        public string CartonsUom { get; set; }

        public decimal? TotalGrossWeight { get; set; }

        public string GrossWeightUom { get; set; }

        public decimal? TotalNetWeight { get; set; }

        public string NetWeightUom { get; set; }

        public decimal? TotalCube { get; set; }

        public string CubeUom { get; set; }

        public decimal? TotalValue { get; set; }

        public string CurrencyCode { get; set; }
    }
}
