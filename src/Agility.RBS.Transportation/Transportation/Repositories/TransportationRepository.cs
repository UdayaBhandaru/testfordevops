﻿// <copyright file="TransportationRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Transportation.Transportation.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Core.Models;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.Transportation.Common;
    using Agility.RBS.Transportation.Obligation.Models;
    using Agility.RBS.Transportation.ShipmentTracking.Models;
    using Agility.RBS.Transportation.Transportation.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class TransportationRepository : BaseOraclePackage
    {
        public TransportationRepository()
        {
            this.PackageName = TransportationMgmtConstants.TransportationPackage;
        }

        public long? TransportationId { get; private set; }

        public async Task<List<TransportationSearchModel>> TransportationListGet(ServiceDocument<TransportationSearchModel> serviceDoc, ServiceDocumentResult serviceDocumentResult)
        {
            TransportationSearchModel priceChangeSearch = serviceDoc.DataProfile.DataModel;
            PackageParams packageParameter = this.GetPackageParams("TRSP_SEARCH_TAB", "GETTRANSLIST");
            OracleObject recordObject = this.SetParamsTransportationList(priceChangeSearch);

            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();

            BaseOracleParameter baseOracleParameter;
            baseOracleParameter = new BaseOracleParameter
            {
                Name = "P_START",
                DataType = OracleDbType.Number,
                Direction = ParameterDirection.Input,
                Value = Convert.ToString(priceChangeSearch.StartRow)
            };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            baseOracleParameter = new BaseOracleParameter { Name = "P_END", DataType = OracleDbType.Number, Direction = ParameterDirection.Input, Value = Convert.ToString(priceChangeSearch.EndRow) };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            baseOracleParameter = new BaseOracleParameter { Name = "P_total", DataType = OracleDbType.Number, Direction = ParameterDirection.Output };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            if (priceChangeSearch.SortModel == null)
            {
                priceChangeSearch.SortModel = new[] { new Core.Models.SortModel { ColId = null, Sort = null } };
            }

            baseOracleParameter = new BaseOracleParameter { Name = "P_SortColId", DataType = OracleDbType.VarChar, Direction = ParameterDirection.Input, Value = priceChangeSearch.SortModel[0].ColId };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            baseOracleParameter = new BaseOracleParameter { Name = "P_SortOrder", DataType = OracleDbType.VarChar, Direction = ParameterDirection.Input, Value = priceChangeSearch.SortModel[0].Sort };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            parameters.Add(this.ParameterBuilder(UserProfileHelper.GetUserParameter()));
            var orders = await this.GetProcedure2<TransportationSearchModel>(recordObject, packageParameter, serviceDocumentResult, parameters);
            priceChangeSearch.TotalRows = (this.Parameters["p_total"].Value != null && this.Parameters["p_total"].Value != DBNull.Value) ? Convert.ToInt64(this.Parameters["p_total"].Value) : 1;
            serviceDoc.DataProfile.DataModel = priceChangeSearch;
            return orders;
        }

        public async Task<TransportationModel> GetTransportation(long transportationId, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("TRANSPORTATION_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            this.SetTransportationId(transportationId, recordObject);
            PackageParams packageParameter = this.GetPackageParams("TRANSPORTATION_TAB", "GETTRANSPORTATION");
            var priceChangeHeads = await this.GetProcedure2<TransportationModel>(recordObject, packageParameter, serviceDocumentResult);
            if (priceChangeHeads != null && priceChangeHeads.Count > 0)
            {
                return priceChangeHeads[0];
            }

            return null;
        }

        public virtual void SetTransportationId(long transportationId, OracleObject recordObject)
        {
            recordObject["TRANSPORTATION_ID"] = transportationId;
        }

        public async Task<ServiceDocumentResult> TransportationSet(TransportationModel transportationModel)
        {
            this.TransportationId = 0;
            PackageParams packageParameter = this.GetPackageParams("TRANSPORTATION_TAB", "SETTRANSPORTATION");
            OracleObject recordObject = this.SetParamsTransportation(transportationModel);
            var result = await this.SetProcedure(recordObject, packageParameter);
            if (this.ObjResult != null && transportationModel.TransportationId == null)
            {
                this.TransportationId = Convert.ToInt32(this.ObjResult["TRANSPORTATION_ID"]);
            }
            else
            {
                this.TransportationId = transportationModel.TransportationId;
            }

            return result;
        }

        public OracleObject SetParamsTransportation(TransportationModel transportationModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("TRANSPORTATION_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            this.SetParamsSave(transportationModel, recordObject);
            return recordObject;
        }

        public virtual void SetParamsSave(TransportationModel transportationModel, OracleObject recordObject)
        {
            recordObject["OPERATION"] = transportationModel.Operation;
            recordObject["TRANSPORTATION_ID"] = transportationModel.TransportationId;
            recordObject["VESSEL_ID"] = transportationModel.VesselId;
            recordObject["VOYAGE_FLT_ID"] = transportationModel.VoyageFltId;
            recordObject["ESTIMATED_DEPART_DATE"] = transportationModel.EstimatedDepartDate;
            recordObject["ORDER_NO"] = transportationModel.OrderNo;
            recordObject["ITEM"] = transportationModel.Item;
            recordObject["ITEM_DESC"] = transportationModel.ItemDesc;
            recordObject["SHIPMENT_NO"] = transportationModel.ShipmentNo;
            recordObject["STATUS"] = transportationModel.Status;
            recordObject["ACTUAL_DEPART_DATE"] = transportationModel.ActualDepartDate;
            recordObject["SUPPLIER"] = transportationModel.Supplier;
            recordObject["BL_AWB_ID"] = transportationModel.BlAwbId;
            recordObject["CONTAINER_ID"] = transportationModel.ContainerId;
            recordObject["INVOICE_ID"] = transportationModel.InvoiceId;
            recordObject["INVOICE_DATE"] = transportationModel.InvoiceDate;
            recordObject["ACTUAL_ARRIVAL_DATE"] = transportationModel.ActualArrivalDate;
            recordObject["DELIVERY_DATE"] = transportationModel.DeliveryDate;
            recordObject["TRAN_MODE_ID"] = transportationModel.TranModeId;
            recordObject["VESSEL_SCAC_CODE"] = transportationModel.VesselScacCode;
            recordObject["CONTAINER_SCAC_CODE"] = transportationModel.ContainerScacCode;
            recordObject["SEAL_ID"] = transportationModel.SealId;
            recordObject["FREIGHT_TYPE"] = transportationModel.FreightType;
            recordObject["FREIGHT_SIZE"] = transportationModel.FreightSize;
            recordObject["ORIGIN_COUNTRY_ID"] = transportationModel.OriginCountryId;
            recordObject["CONSOLIDATION_COUNTRY_ID"] = transportationModel.ConsolidationCountryId;
            recordObject["EXPORT_COUNTRY_ID"] = transportationModel.ExportCountryId;
            recordObject["ESTIMATED_ARRIVAL_DATE"] = transportationModel.EstimatedArrivalDate;
            recordObject["LADING_PORT"] = transportationModel.LadingPort;
            recordObject["DISCHARGE_PORT"] = transportationModel.DischargePort;
            recordObject["CONSOLIDATOR"] = transportationModel.Consolidator;
            recordObject["RECEIPT_ID"] = transportationModel.ReceiptId;
            recordObject["FCR_ID"] = transportationModel.FcrId;
            recordObject["FCR_DATE"] = transportationModel.FcrDate;
            recordObject["SERVICE_CONTRACT_NO"] = transportationModel.ServiceContractNo;
            recordObject["IN_TRANSIT_NO"] = transportationModel.InTransitNo;
            recordObject["IN_TRANSIT_DATE"] = transportationModel.InTransitDate;
            recordObject["LOT_NO"] = transportationModel.LotNo;
            recordObject["INVOICE_AMT"] = transportationModel.InvoiceAmt;
            recordObject["CURRENCY_CODE"] = transportationModel.CurrencyCode;
            recordObject["EXCHANGE_RATE"] = transportationModel.ExchangeRate;
            recordObject["CARTON_QTY"] = transportationModel.CartonQty;
            recordObject["CARTON_UOM"] = transportationModel.CartonUom;
            recordObject["ITEM_QTY"] = transportationModel.ItemQty;
            recordObject["ITEM_QTY_UOM"] = transportationModel.ItemQtyUom;
            recordObject["GROSS_WT"] = transportationModel.GrossWt;
            recordObject["GROSS_WT_UOM"] = transportationModel.GrossWtUom;
            recordObject["NET_WT"] = transportationModel.NetWt;
            recordObject["NET_WT_UOM"] = transportationModel.NetWtUom;
            recordObject["CUBIC"] = transportationModel.Cubic;
            recordObject["CUBIC_UOM"] = transportationModel.CubicUom;
            recordObject["PACKING_METHOD"] = transportationModel.PackingMethod;
            recordObject["RUSH_IND"] = transportationModel.RushInd;
            recordObject["CANDIDATE_IND"] = transportationModel.CandidateInd;
            recordObject["COMMENTS"] = transportationModel.Comments;
            recordObject["SELECTED_IND"] = transportationModel.SelectedInd;
        }

        public OracleObject SetParamsTransportationList(TransportationSearchModel transportationSearchModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("TRSP_SEARCH_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            recordObject["TRANSPORTATION_ID"] = transportationSearchModel.TransportationId;
            recordObject["VESSEL_ID"] = transportationSearchModel.VesselId;
            recordObject["VOYAGE_FLT_ID"] = transportationSearchModel.VoyageFltId;
            recordObject["ESTIMATED_DEPART_DATE"] = transportationSearchModel.EstimatedDepartDate;
            recordObject["ORDER_NO"] = transportationSearchModel.OrderNo;
            recordObject["ITEM"] = transportationSearchModel.Item;
            recordObject["ITEM_DESC"] = transportationSearchModel.ItemDesc;
            recordObject["SHIPMENT_NO"] = transportationSearchModel.ShipmentNo;
            recordObject["STATUS"] = transportationSearchModel.Status;
            return recordObject;
        }

        // Loading Active SCAC Codes
        public async Task<List<NbDomainDetailModel>> ScacCodesGet()
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType(TransportationMgmtConstants.CodeHeadRec);
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(TransportationMgmtConstants.CodeHeadTab, TransportationMgmtConstants.GetScacProc);
            return await this.GetProcedure2<NbDomainDetailModel>(recordObject, packageParameter, null);
        }

        public async Task<AjaxModel<List<TransTotalModel>>> TransTotalsGet(string transportationId, string totalsLevel)
        {
            PackageParams packageParameter = this.GetPackageParams("TRANS_TOTALS_TAB", "GETTRANSTOTALS");
            OracleObject recordObject = this.SetParamsTransTotals(transportationId, totalsLevel);
            return await this.GetProcedureAjaxModel<TransTotalModel>(recordObject, packageParameter);
        }

        private OracleObject SetParamsTransTotals(string transportationId, string totalsLevel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("TRANS_TOTALS_REC");
            OracleObject recordObject = new OracleObject(recordType);
            recordObject["TRANSPORTATION_ID"] = transportationId;
            recordObject["TOTALS_LEVEL"] = totalsLevel;
            return recordObject;
        }
    }
}
