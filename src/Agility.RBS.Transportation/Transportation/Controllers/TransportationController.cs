﻿// <copyright file="TransportationController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Transportation.Transportation.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Models;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Inbox.Common;
    using Agility.RBS.Inbox.Models;
    using Agility.RBS.Inbox.Repository;
    using Agility.RBS.MDM.Freight;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Models;
    using Agility.RBS.MDM.Trait;
    using Agility.RBS.MDM.UnitOfMeasure;
    using Agility.RBS.MDM.UnitOfMeasure.Models;
    using Agility.RBS.MDM.ValueAddedTax;
    using Agility.RBS.MDM.ValueAddedTax.Models;
    using Agility.RBS.Transportation.Common;
    using Agility.RBS.Transportation.ShipmentTracking.Models;
    using Agility.RBS.Transportation.ShipmentTracking.Repositories;
    using Agility.RBS.Transportation.Transportation.Models;
    using Agility.RBS.Transportation.Transportation.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    public class TransportationController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly UnitOfMeasureRepository uomRepository;
        private readonly TransportationRepository transportationRepository;
        private readonly FreightRepository freightRepository;
        private readonly ServiceDocument<TransportationModel> serviceDocument;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private long transportationId;

        public TransportationController(
            ServiceDocument<TransportationModel> serviceDocument,
            DomainDataRepository domainDataRepository,
            UnitOfMeasureRepository uomRepository,
            FreightRepository freightRepository,
            TransportationRepository transportationRepository)
        {
            this.serviceDocument = serviceDocument;
            this.domainDataRepository = domainDataRepository;
            this.uomRepository = uomRepository;
            this.freightRepository = freightRepository;
            this.transportationRepository = transportationRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in Bulk Item Head page when Add button clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<TransportationModel>> New()
        {
            return await this.TransportationDomainData();
        }

        [HttpPost]
        public async Task<ServiceDocument<TransportationModel>> Save([FromBody]ServiceDocument<TransportationModel> serviceDocument)
        {
            await this.serviceDocument.SaveAsync(this.TransportationSave);
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for populating fields in the Bulk Item Head page when Edit icon clicked.
        /// </summary>
        /// <param name="id">Cost Change Id</param>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<TransportationModel>> Open(long id)
        {
            this.transportationId = id;
            await this.serviceDocument.OpenAsync(this.TransportationOpen);
            return await this.TransportationDomainData();
        }

        [HttpGet]
        public async Task<AjaxModel<List<TransTotalModel>>> TransTotalsGet(string transportationId, string totalsLevel)
        {
            try
            {
                return await this.transportationRepository.TransTotalsGet(transportationId, totalsLevel);
            }
            catch (Exception ex)
            {
                return new AjaxModel<List<TransTotalModel>> { Result = AjaxResult.Exception, Message = ex.Message, Model = null };
            }
        }

        private async Task<TransportationModel> TransportationOpen()
        {
            try
            {
                var transportation = await this.transportationRepository.GetTransportation(this.transportationId, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return transportation;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> TransportationSave()
        {
            this.serviceDocument.Result = await this.transportationRepository.TransportationSet(this.serviceDocument.DataProfile.DataModel);
            if (this.serviceDocument.Result.Type != MessageType.Success)
            {
                return false;
            }

            this.serviceDocument.DataProfile.DataModel.TransportationId = this.transportationRepository.TransportationId;
            return true;
        }

        private async Task<ServiceDocument<TransportationModel>> TransportationDomainData()
        {
            this.serviceDocument.DomainData.Add("status", this.domainDataRepository.DomainDetailData(TransportationMgmtConstants.DomainHdrStatus).Result);
            this.serviceDocument.DomainData.Add("indicator", this.domainDataRepository.DomainDetailData(TransportationMgmtConstants.DomainHdrCheckAuth).Result);
            this.serviceDocument.DomainData.Add("transmode", this.domainDataRepository.DomainDetailData(TransportationMgmtConstants.DomainHdrTransMode).Result);
            this.serviceDocument.DomainData.Add("vesselscac", this.transportationRepository.ScacCodesGet().Result);
            this.serviceDocument.DomainData.Add("consolidator", await this.domainDataRepository.PartnerGet("CO"));
            this.serviceDocument.DomainData.Add("uom", await this.uomRepository.GetUom(new UomModel(), this.serviceDocumentResult));
            this.serviceDocument.DomainData.Add("country", this.domainDataRepository.CountryDomainGet().Result);
            this.serviceDocument.DomainData.Add("outloc", this.domainDataRepository.OutLocationDomainGet().Result);
            this.serviceDocument.DomainData.Add("packing", this.domainDataRepository.DomainDetailData(TransportationMgmtConstants.DomainHdrPackingMethod).Result);
            this.serviceDocument.DomainData.Add("freighttype", this.freightRepository.GetFrieghtType().Result);
            this.serviceDocument.DomainData.Add("totallevel", this.domainDataRepository.DomainDetailData(TransportationMgmtConstants.DomainHdrTotalLevel).Result);
            return this.serviceDocument;
        }
    }
}
