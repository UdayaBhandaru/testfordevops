﻿//-------------------------------------------------------------------------------------------------
// <copyright file="TransportationSearchController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// TransportationSearchController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.Transportation.Transportation.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Constants;
    using Agility.RBS.Core.CustomExceptions;
    using Agility.RBS.Core.Models;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.Documents;
    using Agility.RBS.Documents.Models;
    using Agility.RBS.Inbox.Common;
    using Agility.RBS.Inbox.Models;
    using Agility.RBS.Inbox.Repository;
    using Agility.RBS.ItemManagement.Common;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Models;
    using Agility.RBS.Transportation.Transportation.Models;
    using Agility.RBS.Transportation.Transportation.Repositories;
    using DocumentFormat.OpenXml.Packaging;
    using DocumentFormat.OpenXml.Spreadsheet;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [Route("api/[controller]/[action]")]
    public class TransportationSearchController : Controller
    {
        ////private readonly DomainDataRepository domainDataRepository;
        private readonly TransportationRepository transportationRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly ServiceDocument<TransportationSearchModel> serviceDocument;

        public TransportationSearchController(
            ServiceDocument<TransportationSearchModel> serviceDocument,
            TransportationRepository transportationRepository,
            DomainDataRepository domainDataRepository,
            InboxRepository inboxRepository)
        {
            this.transportationRepository = transportationRepository;
            ////this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete fields in the Transportation List page.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        public async Task<ServiceDocument<TransportationSearchModel>> List()
        {
            this.serviceDocument.LocalizationData.AddData("PriceChangeHead");
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<TransportationSearchModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.TransportationSearch);
            return this.serviceDocument;
        }

        private async Task<List<TransportationSearchModel>> TransportationSearch()
        {
            try
            {
                List<TransportationSearchModel> transportations = await this.transportationRepository.TransportationListGet(this.serviceDocument, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return transportations;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }
    }
}
