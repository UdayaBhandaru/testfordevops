﻿// <copyright file="TransportationMgmtConstants.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Transportation.Common
{
    internal static class TransportationMgmtConstants
    {
        public const string TransportationPackage = "UTL_TRANSPORTATION";

        public const string GetWrapperPackage = "UTL_FINANCE";
        public const string ObjTypeObligationSearch = "OBLG_SEARCH_TAB";
        public const string GetProcbligationList = "GETOBLGLIST";
        public const string RecordObjObligationSearch = "OBLG_SEARCH_REC";
        public const string RecordObjObligationHeader = "OBLIGATION_REC";
        public const string ObjTypeObligationHeader = "OBLIGATION_TAB";
        public const string GetProcObligationHead = "GETOBLIGATION";
        public const string ObjTypeObligation = "OBLIGATION_TAB";
        public const string SetProcObligation = "SETOBLIGATION";
        public const string ObjTypeObligationDetails = "OBLIGATION_COMP_TAB";
        public const string ObjTypeObligationDetailsrec = "OBLIGATION_COMP_REC";

        // Domain Header Ids
        public const string DomainHdrObligationType = "OBLG";
        public const string DomainHdrPartnerType = "PTAL";
        public const string DomainHdrPaymentType = "OPYM";
        public const string DomainHdrObligationStatus = "OBST";
        public const string DomainHdrCheckAuth = "YSNO";
        public const string DomainHdrRaioButtonData = "OBSP";

        public const string DomainHdrShipmentType = "SHPM";
        public const string DomainHdrStatus = "TRCO";
        public const string DomainHdrOverAllStatus = "SHIPFSTAT";
        public const string DomainHdrShipLine = "SHIPLINE";
        public const string DomainHdrContent = "CONTENTS";
        public const string DomainHdrFreightType = "TRFC";
        public const string DomainHdrDelayRsn = "DELAY";
        public const string DomainHdrHeldRsn = "MURE";
        public const string DomainHdrIfrajType = "IFRAR";
        public const string DomainHdrConSize = "CONSIZE";
        public const string DomainHdrTransMode = "TRMO";
        public const string DomainHdrPackingMethod = "PKMT";
        public const string DomainHdrReady48 = "R48";
        public const string DomainHdrTotalLevel = "TRTO";

        // Shipment TRANS
        public const string ObjTypeTRANSShipmentDomain = "TRANS_SHIP_DD_TAB";
        public const string GetProcTRANSShipmentDomain = "GETSHIPMENT";

        // Domain Details
        public const string GetProcDomainDetail = "GETUTLDMNSTATDTLS";
        public const string ObjTypeDomainDetail = "UTL_DMN_DTL_TAB";
        public const string HeaderId = "P_HEADER_ID";

        // Shipment Tarcking
        public const string NbShipmentTab = "NB_SHIPMENT_TAB";
        public const string GetShipmentProc = "GETSHIPMENT";
        public const string NbShipmentRec = "NB_SHIPMENT_REC";
        public const string TransOrdDdRec = "TRANS_ORD_DD_REC";
        public const string TransOrdDdTab = "TRANS_ORD_DD_TAB";
        public const string GetOrdDtlsProc = "GETORDDTLS";
        public const string SetShipmentProc = "SETSHIPMENT";
        public const string NbCodeDtlTab = "NB_CODE_DTL_TAB";
        public const string GetNbCodeDtlsProc = "GETNBCODEDTLS";
        public const string NbCodeDtlRec = "NB_CODE_DTL_REC";

        public const string CodeHeadTab = "CODE_HEAD_TAB";
        public const string GetScacProc = "GETSCAC";
        public const string CodeHeadRec = "CODE_HEAD_REC";
    }
}
