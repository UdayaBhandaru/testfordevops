﻿// <copyright file="TransportationStartup.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Transportation
{
    using System;
    using System.IO;
    using System.Reflection;
    using System.Text;
    using Agility.Framework.Core;
    using Agility.RBS.Transportation.Obligation.Controllers;
    using Agility.RBS.Transportation.Obligation.Mapping;
    using Agility.RBS.Transportation.Obligation.Repositories;
    using Agility.RBS.Transportation.ShipmentTracking.Controllers;
    using Agility.RBS.Transportation.ShipmentTracking.Mapping;
    using Agility.RBS.Transportation.ShipmentTracking.Repositories;
    using Agility.RBS.Transportation.Transportation.Controllers;
    using Agility.RBS.Transportation.Transportation.Mapping;
    using Agility.RBS.Transportation.Transportation.Repositories;
    using AutoMapper;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Caching.Distributed;
    using Microsoft.Extensions.Caching.Memory;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using Swashbuckle.AspNetCore.Swagger;

    public class TransportationStartup : ComponentStartup<DbContext>
    {
        public void Startup(IConfigurationRoot configuration)
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<ObligationModelMapping>();
                cfg.AddProfile<ShipmentTrackingModelMapping>();
                cfg.AddProfile<TransportationModelMapping>();
                this.InitilizeMapping(cfg);
            });
        }

        public override void ConfigureServices(IServiceCollection services)
        {
            base.ConfigureServices(services);
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("TransportationV1", new Info { Title = "My API", Version = "v1" });
                var xmlFile = $"{this.GetType().GetTypeInfo().Assembly.GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }

        public override void Configure(
           IApplicationBuilder app,
           IHostingEnvironment env,
           ILoggerFactory loggerFactory,
           IDistributedCache distributedCache,
           IMemoryCache memoryCache,
           IServiceProvider serviceProvider)
        {
            base.Configure(app, env, loggerFactory, distributedCache, memoryCache, serviceProvider);
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("//swagger//TransportationV1//swagger.json", "My API v1");
                c.SupportedSubmitMethods();
            });
        }

        public override void ConfigureDependencyInjection(IServiceCollection services)
        {
            services.AddTransient<ObligationHeadController>();
            services.AddTransient<ObligationRepository>();
            services.AddTransient<ShipmentTrackingController>();
            services.AddTransient<ShipmentTrackingRepository>();
            services.AddTransient<TransportationSearchController>();
            services.AddTransient<TransportationController>();
            services.AddTransient<TransportationRepository>();
        }
    }
}
