﻿// <copyright file="ShipmentTrackingRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Transportation.ShipmentTracking.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Core.Models;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.Transportation.Common;
    using Agility.RBS.Transportation.Obligation.Models;
    using Agility.RBS.Transportation.ShipmentTracking.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class ShipmentTrackingRepository : BaseOraclePackage
    {
        public ShipmentTrackingRepository()
        {
            this.PackageName = TransportationMgmtConstants.TransportationPackage;
        }

        public long? ShipId { get; private set; }

        public async Task<List<ShipmentModel>> GetShipmentTracking(ShipmentModel shipmentModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(TransportationMgmtConstants.NbShipmentTab, TransportationMgmtConstants.GetShipmentProc);
            OracleObject recordObject = this.SetParamsShipmentTracking(shipmentModel, true);
            OracleTable pDomainMstTab = await this.GetProcedure(recordObject, packageParameter, serviceDocumentResult);

            var lstHeader = Mapper.Map<List<ShipmentModel>>(pDomainMstTab);
            if (lstHeader != null)
            {
                for (int i = 0; i < lstHeader.Count; i++)
                {
                    OracleObject obj = (OracleObject)pDomainMstTab[i];
                    OracleTable containers = (Devart.Data.Oracle.OracleTable)obj["ship_containers"];
                    if (containers.Count > 0)
                    {
                        lstHeader[i].ShipContainers.AddRange(Mapper.Map<List<ShipContainerModel>>(containers));
                    }

                    OracleTable docs = (Devart.Data.Oracle.OracleTable)obj["ship_docs"];
                    if (docs.Count > 0)
                    {
                        lstHeader[i].ShipDocs.AddRange(Mapper.Map<List<ShipDocModel>>(docs));
                    }

                    OracleTable events = (Devart.Data.Oracle.OracleTable)obj["ship_event"];
                    if (events.Count > 0)
                    {
                        lstHeader[i].ShipEvents.AddRange(Mapper.Map<List<ShipEventModel>>(events));
                    }

                    OracleTable orders = (Devart.Data.Oracle.OracleTable)obj["ship_orders"];
                    if (orders.Count > 0)
                    {
                        lstHeader[i].ShipOrders.AddRange(Mapper.Map<List<ShipOrderModel>>(orders));
                    }
                }

                return lstHeader;
            }

            return null;
        }

        public async Task<ShipmentModel> ShipmentTrackingGet(int shipId, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(TransportationMgmtConstants.NbShipmentTab, TransportationMgmtConstants.GetShipmentProc);
            this.Connection.Open();
            OracleType recordType = this.GetObjectType(TransportationMgmtConstants.NbShipmentRec);
            OracleObject recordObject = this.GetOracleObject(recordType);
            recordObject["SHIP_ID"] = shipId;
            OracleTable pDomainMstTab = await this.GetProcedure(recordObject, packageParameter, serviceDocumentResult);

            List<ShipmentModel> lstHeader = Mapper.Map<List<ShipmentModel>>(pDomainMstTab);
            if (lstHeader != null)
            {
                for (int i = 0; i < lstHeader.Count; i++)
                {
                    OracleObject obj = (OracleObject)pDomainMstTab[i];
                    OracleTable containers = (Devart.Data.Oracle.OracleTable)obj["SHIP_CONTAINERS"];
                    if (containers.Count > 0)
                    {
                        lstHeader[i].ShipContainers.AddRange(Mapper.Map<List<ShipContainerModel>>(containers));
                    }

                    OracleTable docs = (Devart.Data.Oracle.OracleTable)obj["SHIP_DOCS"];
                    if (docs.Count > 0)
                    {
                        lstHeader[i].ShipDocs.AddRange(Mapper.Map<List<ShipDocModel>>(docs));
                    }

                    OracleTable events = (Devart.Data.Oracle.OracleTable)obj["SHIP_EVENT"];
                    if (events.Count > 0)
                    {
                        lstHeader[i].ShipEvents.AddRange(Mapper.Map<List<ShipEventModel>>(events));
                    }

                    OracleTable orders = (Devart.Data.Oracle.OracleTable)obj["SHIP_ORDERS"];
                    if (orders.Count > 0)
                    {
                        lstHeader[i].ShipOrders.AddRange(Mapper.Map<List<ShipOrderModel>>(orders));
                    }
                }

                return lstHeader[0];
            }

            return null;
        }

        public async Task<TransOrderModel> OrderDetailsGet(int orderId, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType(TransportationMgmtConstants.TransOrdDdRec);
            OracleObject recordObject = this.GetOracleObject(recordType);
            recordObject["ORDER_NO"] = orderId;
            PackageParams packageParameter = this.GetPackageParams(TransportationMgmtConstants.TransOrdDdTab, TransportationMgmtConstants.GetOrdDtlsProc);
            var orders = await this.GetProcedure2<TransOrderModel>(recordObject, packageParameter, serviceDocumentResult);
            if (orders != null)
            {
                return orders[0];
            }

            return null;
        }

        public OracleObject SetParamsShipmentTracking(ShipmentModel shipmentModel, bool isSearch)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType(TransportationMgmtConstants.NbShipmentRec);
            OracleObject recordObject = this.GetOracleObject(recordType);
            if (!isSearch)
            {
                this.SetParamsSave(shipmentModel, recordObject);
            }

            this.SetParamsSearch(shipmentModel, recordObject);
            return recordObject;
        }

        public virtual void SetParamsSearch(ShipmentModel shipmentModel, OracleObject recordObject)
        {
            recordObject["SHIP_ID"] = shipmentModel.ShipId;
            recordObject["SHIP_VESSEL_ID"] = shipmentModel.ShipVesselId;
            recordObject["SHIP_VOYAGE_FLT_ID"] = shipmentModel.ShipVoyageFltId;
            recordObject["SHIP_EST_DEPT_DATE"] = shipmentModel.ShipEstDeptDate;
        }

        public virtual void SetParamsSave(ShipmentModel shipmentModel, OracleObject recordObject)
        {
            recordObject["Operation"] = shipmentModel.Operation;
            ////recordObject["CREATED_BY"] = this.GetCreatedBy(returntoVendorHeaderModel.CreatedBy);
            ////recordObject["LAST_UPDATED_BY"] = FxContext.Context.Name;

            recordObject["SHIPMENT_TYPE"] = shipmentModel.ShipmentType;
            recordObject["SHIP_STATUS"] = shipmentModel.ShipStatus;
            recordObject["SHIP_COMPANY"] = shipmentModel.ShipCompany;
            recordObject["SHIP_DIVISION"] = shipmentModel.ShipDivision;
            recordObject["SHIP_FINAL_STATUS"] = shipmentModel.ShipFinalStatus;
            recordObject["SHIP_LINE"] = shipmentModel.ShipLine;
            recordObject["SHIP_AWB_NO"] = shipmentModel.ShipAwbNo;
            recordObject["SHIP_FREIGHT_TYPE"] = shipmentModel.ShipFreightType;
            recordObject["SHIP_COUNTRY_ORIGIN"] = shipmentModel.ShipCountryOrigin;
            recordObject["SHIP_PORT_OF_ORIGIN"] = shipmentModel.ShipPortOfOrigin;
            recordObject["SHIP_DESTINATION"] = shipmentModel.ShipDestination;
            recordObject["SHIP_DEST_PORT"] = shipmentModel.ShipDestPort;
            recordObject["SHIP_POLICY_NO"] = shipmentModel.ShipPolicyNo;
            recordObject["SHIP_DOC_RCPT_DATE"] = shipmentModel.ShipDocRcptDate;
            recordObject["SHIP_ORIG_RCPT_DATE"] = shipmentModel.ShipOrigRcptDate;
            recordObject["SHIP_AN_DATE"] = shipmentModel.ShipAnDate;
            recordObject["SHIP_ATA_DATE"] = shipmentModel.ShipAtaDate;
            recordObject["SHIP_CONTENTS"] = shipmentModel.ShipContents;
            recordObject["SHIP_BROKER_DATE"] = shipmentModel.ShipBrokerDate;
            recordObject["SHIP_BROKER"] = shipmentModel.ShipBroker;
            recordObject["SHIP_CLEARANCE_DATE"] = shipmentModel.ShipClearanceDate;
            recordObject["SHIP_DELAY_REASON"] = shipmentModel.ShipDelayReason;
            recordObject["SHIP_BAYAN_DATE"] = shipmentModel.ShipBayanDate;
            recordObject["SHIP_BAYAN_NO"] = shipmentModel.ShipBayanNo;
            recordObject["SHIP_HELD_DATE"] = shipmentModel.ShipHeldDate;
            recordObject["SHIP_HELD_REASON"] = shipmentModel.ShipHeldReason;
            recordObject["IFRAJ_TYPE"] = shipmentModel.IfrajType;
            recordObject["IFRAJ_REQ_DATE"] = shipmentModel.IfrajReqDate;
            recordObject["IFRAJ_RECEIPT_DATE"] = shipmentModel.IfrajReceiptDate;
            recordObject["SHIP_INVOICE_NO"] = shipmentModel.ShipInvoiceNo;
            recordObject["SHIP_INVOICE_DATE"] = shipmentModel.ShipInvoiceDate;
            recordObject["SHIP_INVOICE_CURR"] = shipmentModel.ShipInvoiceCurr;
            recordObject["SHIP_INVOICE_AMT"] = shipmentModel.ShipInvoiceAmt;
            recordObject["SHIP_FREIGHT_CHARGE"] = shipmentModel.ShipFreightCharge;
            recordObject["SHIP_TRANSP_AMT"] = shipmentModel.ShipTranspAmt;
            recordObject["SHIP_INSURANCE_AMT"] = shipmentModel.ShipInsuranceAmt;
            recordObject["SHIP_CLEARANCE_AMT"] = shipmentModel.ShipClearanceAmt;
            recordObject["SHIP_DEMURAGE_AMT"] = shipmentModel.ShipDemurageAmt;
            recordObject["SHIP_DEMURAGE_REASON"] = shipmentModel.ShipDemurageReason;
            recordObject["SHIP_FINISHING_EXP"] = shipmentModel.ShipFinishingExp;
            recordObject["SHIP_CUSTOMS_DUTY"] = shipmentModel.ShipCustomsDuty;
            recordObject["SHIP_VAT_PER"] = shipmentModel.ShipVatPer;
            recordObject["SHIP_VAT_AMT"] = shipmentModel.ShipVatAmt;
            recordObject["TRANSFER_STATUS"] = shipmentModel.TransferStatus;
            recordObject["SHIP_MUN_SHIFT_NO"] = shipmentModel.ShipMunShiftNo;
            recordObject["BLADIYA_FILE_NO"] = shipmentModel.BladiyaFileNo;
            recordObject["SHIP_ETA_DATE"] = shipmentModel.ShipEtaDate;
            recordObject["SHIP_SUPPLIER"] = shipmentModel.ShipSupplier;

            // Container Object
            OracleType containerTableType = this.GetObjectType("NB_SHIP_CONTAINER_TAB");
            OracleType containerRecordType = this.GetObjectType("NB_SHIP_CONTAINER_REC");
            OracleTable pContainerTab = new OracleTable(containerTableType);
            foreach (ShipContainerModel container in shipmentModel.ShipContainers)
            {
                var containerRecordObject = this.GetOracleObject(containerRecordType);
                containerRecordObject["SHIP_ID"] = shipmentModel.ShipId;
                containerRecordObject["CONTAINER_ID"] = container.ContainerId;
                containerRecordObject["LINE_SEQ"] = container.LineSeq;
                containerRecordObject["CONTAINER_SIZE"] = container.ContainerSize;
                containerRecordObject["ITEM_COUNT"] = container.ItemCount;
                containerRecordObject["TOTAL_WEIGHT"] = container.TotalWeight;
                containerRecordObject["Operation"] = container.Operation;
                pContainerTab.Add(containerRecordObject);
            }

            recordObject["SHIP_CONTAINERS"] = pContainerTab;

            // Doc Object
            OracleType docTableType = this.GetObjectType("NB_SHIP_DOC_TAB");
            OracleType docRecordType = this.GetObjectType("NB_SHIP_DOC_REC");
            OracleTable pDocTab = new OracleTable(docTableType);
            foreach (ShipDocModel doc in shipmentModel.ShipDocs)
            {
                var docRecordObject = this.GetOracleObject(docRecordType);
                docRecordObject["SHIP_ID"] = shipmentModel.ShipId;
                docRecordObject["DOC_ID"] = doc.DocId;
                docRecordObject["LINE_SEQ"] = doc.LineSeq;
                docRecordObject["DOC_EXP_DATE"] = doc.DocExpDate;
                docRecordObject["DOC_ACT_DATE"] = doc.DocActDate;
                docRecordObject["COMMENTS"] = doc.DocComments;
                docRecordObject["Operation"] = doc.Operation;
                pDocTab.Add(docRecordObject);
            }

            recordObject["SHIP_DOCS"] = pDocTab;

            // Events Object
            OracleType eventTableType = this.GetObjectType("NB_SHIP_EVENT_TAB");
            OracleType eventRecordType = this.GetObjectType("NB_SHIP_EVENT_REC");
            OracleTable pEventTab = new OracleTable(eventTableType);
            foreach (ShipEventModel shipEvent in shipmentModel.ShipEvents)
            {
                var eventRecordObject = this.GetOracleObject(eventRecordType);
                eventRecordObject["SHIP_ID"] = shipmentModel.ShipId;
                eventRecordObject["EVENT_ID"] = shipEvent.EventId;
                eventRecordObject["LINE_SEQ"] = shipEvent.LineSeq;
                eventRecordObject["EVENT_VALUE"] = shipEvent.EventValue;
                eventRecordObject["EVENT_TEXT"] = shipEvent.EventText;
                eventRecordObject["EVENT_DATE"] = shipEvent.EventDate;
                eventRecordObject["DOC_ENTRY_DATE"] = shipEvent.DocEntryDate;
                eventRecordObject["COMMENTS"] = shipEvent.Comments;
                eventRecordObject["Operation"] = shipEvent.Operation;
                pEventTab.Add(eventRecordObject);
            }

            recordObject["SHIP_EVENT"] = pEventTab;

            // Orders Object
            OracleType orderTableType = this.GetObjectType("NB_SHIP_ORDER_TAB");
            OracleType orderRecordType = this.GetObjectType("NB_SHIP_ORDER_REC");
            OracleTable pOrderTab = new OracleTable(orderTableType);
            foreach (ShipOrderModel shipOrder in shipmentModel.ShipOrders)
            {
                var orderRecordObject = this.GetOracleObject(orderRecordType);
                orderRecordObject["SHIP_ID"] = shipmentModel.ShipId;
                orderRecordObject["ORDER_ID"] = shipOrder.OrderId;
                orderRecordObject["SEQ_NO"] = shipOrder.SeqNo;
                orderRecordObject["CREATE_DATE"] = shipOrder.CreateDate;
                orderRecordObject["Operation"] = shipOrder.Operation;
                pOrderTab.Add(orderRecordObject);
            }

            recordObject["SHIP_ORDERS"] = pOrderTab;
        }

        public async Task<ServiceDocumentResult> SetShipmentTracking(ShipmentModel shipmentModel)
        {
            PackageParams packageParameter = this.GetPackageParams(TransportationMgmtConstants.NbShipmentTab, TransportationMgmtConstants.SetShipmentProc);
            OracleObject recordObject = this.SetParamsShipmentTracking(shipmentModel, false);
            var result = await this.SetProcedure(recordObject, packageParameter);
            if (this.ObjResult != null && shipmentModel.ShipId == null)
            {
                this.ShipId = Convert.ToInt32(this.ObjResult["SHIP_ID"]);
            }
            else
            {
                this.ShipId = shipmentModel.ShipId;
            }

            return result;
        }

        // Loading Active Doamin Master Data of Item Source
        public async Task<List<NbDomainDetailModel>> NbDomainDetailData(string domainHdr)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType(TransportationMgmtConstants.NbCodeDtlRec);
            OracleObject recordObject = this.GetOracleObject(recordType);
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            PackageParams packageParameter = this.GetPackageParams(TransportationMgmtConstants.NbCodeDtlTab, TransportationMgmtConstants.GetNbCodeDtlsProc);
            BaseOracleParameter baseOracleParameter;
            baseOracleParameter = new BaseOracleParameter { Name = TransportationMgmtConstants.HeaderId, DataType = OracleDbType.VarChar, Direction = ParameterDirection.Input, Value = domainHdr };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));
            return await this.GetProcedure2<NbDomainDetailModel>(recordObject, packageParameter, null, parameters);
        }

        public async Task<AjaxModel<bool>> InterfacetoTrans(string shipId, string procName)
        {
            try
            {
                this.Connection.Open();
                OracleParameterCollection parameters = this.Parameters;
                parameters.Clear();
                BaseOracleParameter baseOracleParameter;
                baseOracleParameter = new BaseOracleParameter { Name = "RESULT", DataType = OracleDbType.Boolean, Direction = ParameterDirection.ReturnValue };
                parameters.Add(this.ParameterBuilder(baseOracleParameter));
                baseOracleParameter = new BaseOracleParameter { Name = "P_SHIPMENT_ID", DataType = OracleDbType.Number, Direction = ParameterDirection.Input, Value = shipId };
                parameters.Add(this.ParameterBuilder(baseOracleParameter));
                baseOracleParameter = new BaseOracleParameter { Name = "P_ERROR", DataType = OracleDbType.VarChar, Direction = ParameterDirection.Output };
                parameters.Add(this.ParameterBuilder(baseOracleParameter));
                this.ExecuteProcedure(procName, parameters);
                return (this.Parameters["P_ERROR"].Value != DBNull.Value)
                    ? new AjaxModel<bool> { Result = AjaxResult.ValidationException, Message = Convert.ToString(this.Parameters["P_ERROR"].Value), Model = false }
                    : new AjaxModel<bool> { Result = AjaxResult.Success, Message = "Transportation created successfully", Model = true };
            }
            finally
            {
                this.Connection.Close();
            }
        }
    }
}
