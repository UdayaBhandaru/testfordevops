﻿// <copyright file="ShipmentTrackingController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Transportation.ShipmentTracking.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Models;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Inbox.Common;
    using Agility.RBS.Inbox.Models;
    using Agility.RBS.Inbox.Repository;
    using Agility.RBS.MDM.Freight;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Models;
    using Agility.RBS.MDM.Trait;
    using Agility.RBS.MDM.UnitOfMeasure;
    using Agility.RBS.MDM.UnitOfMeasure.Models;
    using Agility.RBS.MDM.ValueAddedTax;
    using Agility.RBS.MDM.ValueAddedTax.Models;
    using Agility.RBS.Transportation.Common;
    using Agility.RBS.Transportation.ShipmentTracking.Models;
    using Agility.RBS.Transportation.ShipmentTracking.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    public class ShipmentTrackingController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly ShipmentTrackingRepository shipmentTrackingRepository;
        private readonly ValueAddedTaxRepository valueAddedTaxRepository;
        private readonly ServiceDocument<ShipmentModel> serviceDocument;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly FreightRepository freightRepository;

        public ShipmentTrackingController(
            ServiceDocument<ShipmentModel> serviceDocument,
            DomainDataRepository domainDataRepository,
            ValueAddedTaxRepository valueAddedTaxRepository,
            FreightRepository freightRepository,
            ShipmentTrackingRepository shipmentTrackingRepository)
        {
            this.serviceDocument = serviceDocument;
            this.domainDataRepository = domainDataRepository;
            this.valueAddedTaxRepository = valueAddedTaxRepository;
            this.shipmentTrackingRepository = shipmentTrackingRepository;
            this.freightRepository = freightRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in the Item List page.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<ShipmentModel>> List()
        {
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in Bulk Item Head page when Add button clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<ShipmentModel>> New()
        {
            return await this.ShipmentDomainData();
        }

        /// <summary>
        /// This API invoked for fetching Order records based on search criteria when Search button clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<ShipmentModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.ShipmentTrackSearch);
            return this.serviceDocument;
        }

        [HttpPost]
        public async Task<ServiceDocument<ShipmentModel>> Save([FromBody]ServiceDocument<ShipmentModel> serviceDocument)
        {
            await this.serviceDocument.SaveAsync(this.ShipmentTrackSave);
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for populating dependent fields based on selected Supplier.
        /// </summary>
        /// <param name="orderId">Order Id</param>
        /// <returns>ShipOrderModel</returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<TransOrderModel> OrderDetailsGet(int orderId)
        {
            return await this.shipmentTrackingRepository.OrderDetailsGet(orderId, this.serviceDocumentResult);
        }

        /// <summary>
        /// This API invoked for populating dependent fields based on selected Supplier.
        /// </summary>
        /// <param name="shipId">Ship Id</param>
        /// <returns>ShipOrderModel</returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<ShipmentModel> ShipmentTrackOpen(int shipId)
        {
            var shipment = await this.shipmentTrackingRepository.ShipmentTrackingGet(shipId, this.serviceDocumentResult);
            shipment.Suppliers = await this.domainDataRepository.SupplierDomainGet(shipment.ShipSupplier.Value);
            return shipment;
        }

        /// <summary>
        /// This API invoked for populating dependent fields based on selected Supplier.
        /// </summary>
        /// <param name="supplier">supplier</param>
        /// <returns>ShipOrderModel</returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<List<SupplierDomainModel>> SupplierDomainGet(int supplier)
        {
            return await this.domainDataRepository.SupplierDomainGet(supplier);
        }

        /// <summary>
        /// This API invoked for creating the interface to transportation based on Shipment.
        /// </summary>
        /// <param name="shipId">shipId</param>
        /// <returns>Interface to Transportation</returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<AjaxModel<bool>> InterfacetoTrans(string shipId)
        {
            try
            {
                return await this.shipmentTrackingRepository.InterfacetoTrans(shipId, "CREATE_TRANSP");
            }
            catch (Exception ex)
            {
                return new AjaxModel<bool> { Result = AjaxResult.Exception, Message = ex.Message, Model = false };
            }
        }

        /// <summary>
        /// This API invoked for creating the interface to transportation based on Shipment.
        /// </summary>
        /// <param name="shipId">shipId</param>
        /// <returns>Interface to Transportation</returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<AjaxModel<bool>> RollbackTrans(string shipId)
        {
            try
            {
                return await this.shipmentTrackingRepository.InterfacetoTrans(shipId, "DELETE_TRANSP");
            }
            catch (Exception ex)
            {
                return new AjaxModel<bool> { Result = AjaxResult.Exception, Message = ex.Message, Model = false };
            }
        }

        private async Task<List<ShipmentModel>> ShipmentTrackSearch()
        {
            try
            {
                var lstShipment = await this.shipmentTrackingRepository.GetShipmentTracking(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                if (lstShipment.Count > 0)
                {
                    foreach (ShipmentModel shipment in lstShipment)
                    {
                        shipment.Suppliers = await this.domainDataRepository.SupplierDomainGet(shipment.ShipSupplier.Value);
                    }
                }

                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstShipment;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> ShipmentTrackSave()
        {
            this.serviceDocument.Result = await this.shipmentTrackingRepository.SetShipmentTracking(this.serviceDocument.DataProfile.DataModel);
            if (this.serviceDocument.Result.Type != MessageType.Success)
            {
                return false;
            }

            this.serviceDocument.DataProfile.DataModel.ShipId = this.shipmentTrackingRepository.ShipId;
            return true;
        }

        private async Task<ServiceDocument<ShipmentModel>> ShipmentDomainData()
        {
            this.serviceDocument.DomainData.Add("shipmenttype", this.domainDataRepository.DomainDetailData(TransportationMgmtConstants.DomainHdrShipmentType).Result);
            this.serviceDocument.DomainData.Add("status", this.domainDataRepository.DomainDetailData(TransportationMgmtConstants.DomainHdrStatus).Result);
            this.serviceDocument.DomainData.Add("overallstatus", this.shipmentTrackingRepository.NbDomainDetailData(TransportationMgmtConstants.DomainHdrOverAllStatus).Result);
            this.serviceDocument.DomainData.Add("shipline", this.shipmentTrackingRepository.NbDomainDetailData(TransportationMgmtConstants.DomainHdrShipLine).Result);
            this.serviceDocument.DomainData.Add("country", this.domainDataRepository.CountryDomainGet().Result);
            this.serviceDocument.DomainData.Add("outloc", this.domainDataRepository.OutLocationDomainGet().Result);
            this.serviceDocument.DomainData.Add("content", this.shipmentTrackingRepository.NbDomainDetailData(TransportationMgmtConstants.DomainHdrContent).Result);
            this.serviceDocument.DomainData.Add("broker", await this.domainDataRepository.PartnerGet("BR"));
            this.serviceDocument.DomainData.Add("delayrsn", this.shipmentTrackingRepository.NbDomainDetailData(TransportationMgmtConstants.DomainHdrDelayRsn).Result);
            this.serviceDocument.DomainData.Add("heldrsn", this.domainDataRepository.DomainDetailData(TransportationMgmtConstants.DomainHdrHeldRsn).Result);
            this.serviceDocument.DomainData.Add("ifrajtype", this.shipmentTrackingRepository.NbDomainDetailData(TransportationMgmtConstants.DomainHdrIfrajType).Result);
            this.serviceDocument.DomainData.Add("consize", this.shipmentTrackingRepository.NbDomainDetailData(TransportationMgmtConstants.DomainHdrConSize).Result);
            this.serviceDocument.DomainData.Add("currency", this.domainDataRepository.CurrencyDomainGet().Result);
            this.serviceDocument.DomainData.Add("vatcode", this.valueAddedTaxRepository.GetVatCodes(new VatCodeModel(), new ServiceDocumentResult()).Result);
            this.serviceDocument.DomainData.Add("freighttype", this.freightRepository.GetFrieghtType().Result);
            this.serviceDocument.DomainData.Add("munshift", this.shipmentTrackingRepository.NbDomainDetailData(TransportationMgmtConstants.DomainHdrReady48).Result);
            return this.serviceDocument;
        }
    }
}
