﻿// <copyright file="ShipmentTrackingModelMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Transportation.ShipmentTracking.Mapping
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.RBS.Core;
    using Agility.RBS.Transportation.ShipmentTracking.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class ShipmentTrackingModelMapping : Profile
    {
        public ShipmentTrackingModelMapping()
            : base("ShipmentTrackingModelMapping")
        {
            this.CreateMap<OracleObject, ShipmentModel>()
                .ForMember(m => m.ShipId, opt => opt.MapFrom(r => OracleNullHandler.DbNullInt64Handler(r["SHIP_ID"])))
                  .ForMember(m => m.ShipmentType, opt => opt.MapFrom(r => r["SHIPMENT_TYPE"]))
                  .ForMember(m => m.TypeDesc, opt => opt.MapFrom(r => r["TYPE_DESC"]))
                  .ForMember(m => m.ShipStatus, opt => opt.MapFrom(r => r["SHIP_STATUS"]))
                  .ForMember(m => m.StatusDesc, opt => opt.MapFrom(r => r["STATUS_DESC"]))
                  .ForMember(m => m.ShipCompany, opt => opt.MapFrom(r => r["SHIP_COMPANY"]))
                  .ForMember(m => m.CompanyDesc, opt => opt.MapFrom(r => r["COMPANY_DESC"]))
                  .ForMember(m => m.ShipDivision, opt => opt.MapFrom(r => r["SHIP_DIVISION"]))
                  .ForMember(m => m.DivisionDesc, opt => opt.MapFrom(r => r["DIVISION_DESC"]))
                  .ForMember(m => m.ShipFinalStatus, opt => opt.MapFrom(r => r["SHIP_FINAL_STATUS"]))
                  .ForMember(m => m.FinalStatusDesc, opt => opt.MapFrom(r => r["FINAL_STATUS_DESC"]))
                  .ForMember(m => m.ShipLine, opt => opt.MapFrom(r => r["SHIP_LINE"]))
                  .ForMember(m => m.ShipLineDesc, opt => opt.MapFrom(r => r["SHIP_LINE_DESC"]))
                  .ForMember(m => m.ShipAwbNo, opt => opt.MapFrom(r => r["SHIP_AWB_NO"]))
                  .ForMember(m => m.ShipFreightType, opt => opt.MapFrom(r => r["SHIP_FREIGHT_TYPE"]))
                  .ForMember(m => m.FreightTypeDesc, opt => opt.MapFrom(r => r["FREIGHT_TYPE_DESC"]))
                  .ForMember(m => m.ShipCountryOrigin, opt => opt.MapFrom(r => r["SHIP_COUNTRY_ORIGIN"]))
                  .ForMember(m => m.OrgCountryName, opt => opt.MapFrom(r => r["ORG_COUNTRY_NAME"]))
                  .ForMember(m => m.ShipPortOfOrigin, opt => opt.MapFrom(r => r["SHIP_PORT_OF_ORIGIN"]))
                  .ForMember(m => m.OrgPortName, opt => opt.MapFrom(r => r["ORG_PORT_NAME"]))
                  .ForMember(m => m.ShipDestination, opt => opt.MapFrom(r => r["SHIP_DESTINATION"]))
                  .ForMember(m => m.DestinationName, opt => opt.MapFrom(r => r["DESTINATION_NAME"]))
                  .ForMember(m => m.ShipDestPort, opt => opt.MapFrom(r => r["SHIP_DEST_PORT"]))
                  .ForMember(m => m.DestPortName, opt => opt.MapFrom(r => r["DEST_PORT_NAME"]))
                  .ForMember(m => m.ShipPolicyNo, opt => opt.MapFrom(r => r["SHIP_POLICY_NO"]))
                  .ForMember(m => m.ShipDocRcptDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["SHIP_DOC_RCPT_DATE"])))
                  .ForMember(m => m.ShipOrigRcptDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["SHIP_ORIG_RCPT_DATE"])))
                  .ForMember(m => m.ShipAnDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["SHIP_AN_DATE"])))
                  .ForMember(m => m.ShipAtaDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["SHIP_ATA_DATE"])))
                  .ForMember(m => m.ShipContents, opt => opt.MapFrom(r => r["SHIP_CONTENTS"]))
                  .ForMember(m => m.ContentsDesc, opt => opt.MapFrom(r => r["CONTENTS_DESC"]))
                  .ForMember(m => m.ShipBrokerDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["SHIP_BROKER_DATE"])))
                  .ForMember(m => m.ShipBroker, opt => opt.MapFrom(r => r["SHIP_BROKER"]))
                  .ForMember(m => m.BrokerName, opt => opt.MapFrom(r => r["BROKER_NAME"]))
                  .ForMember(m => m.ShipClearanceDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["SHIP_CLEARANCE_DATE"])))
                  .ForMember(m => m.ShipDelayReason, opt => opt.MapFrom(r => r["SHIP_DELAY_REASON"]))
                  .ForMember(m => m.DelayReasonDesc, opt => opt.MapFrom(r => r["DELAY_REASON_DESC"]))
                  .ForMember(m => m.ShipBayanDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["SHIP_BAYAN_DATE"])))
                  .ForMember(m => m.ShipBayanNo, opt => opt.MapFrom(r => r["SHIP_BAYAN_NO"]))
                  .ForMember(m => m.ShipHeldDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["SHIP_HELD_DATE"])))
                  .ForMember(m => m.ShipHeldReason, opt => opt.MapFrom(r => r["SHIP_HELD_REASON"]))
                  .ForMember(m => m.HeldReasonDesc, opt => opt.MapFrom(r => r["HELD_REASON_DESC"]))
                  .ForMember(m => m.IfrajType, opt => opt.MapFrom(r => r["IFRAJ_TYPE"]))
                  .ForMember(m => m.IfrajTypeDesc, opt => opt.MapFrom(r => r["IFRAJ_TYPE_DESC"]))
                  .ForMember(m => m.IfrajReqDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["IFRAJ_REQ_DATE"])))
                  .ForMember(m => m.IfrajReceiptDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["IFRAJ_RECEIPT_DATE"])))
                  .ForMember(m => m.ShipInvoiceNo, opt => opt.MapFrom(r => r["SHIP_INVOICE_NO"]))
                  .ForMember(m => m.ShipInvoiceDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["SHIP_INVOICE_DATE"])))
                  .ForMember(m => m.ShipInvoiceCurr, opt => opt.MapFrom(r => r["SHIP_INVOICE_CURR"]))
                  .ForMember(m => m.ShipInvoiceAmt, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["SHIP_INVOICE_AMT"])))
                  .ForMember(m => m.ShipFreightCharge, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["SHIP_FREIGHT_CHARGE"])))
                  .ForMember(m => m.ShipTranspAmt, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["SHIP_TRANSP_AMT"])))
                  .ForMember(m => m.ShipInsuranceAmt, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["SHIP_INSURANCE_AMT"])))
                  .ForMember(m => m.ShipClearanceAmt, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["SHIP_CLEARANCE_AMT"])))
                  .ForMember(m => m.ShipDemurageAmt, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["SHIP_DEMURAGE_AMT"])))
                  .ForMember(m => m.ShipDemurageReason, opt => opt.MapFrom(r => r["SHIP_DEMURAGE_REASON"]))
                  .ForMember(m => m.ShipFinishingExp, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["SHIP_FINISHING_EXP"])))
                  .ForMember(m => m.ShipCustomsDuty, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["SHIP_CUSTOMS_DUTY"])))
                  .ForMember(m => m.ShipVatPer, opt => opt.MapFrom(r => r["SHIP_VAT_PER"]))
                  .ForMember(m => m.ShipVatAmt, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["SHIP_VAT_AMT"])))
                  .ForMember(m => m.ShipVesselId, opt => opt.MapFrom(r => r["SHIP_VESSEL_ID"]))
                  .ForMember(m => m.ShipVoyageFltId, opt => opt.MapFrom(r => r["SHIP_VOYAGE_FLT_ID"]))
                  .ForMember(m => m.ShipEstDeptDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["SHIP_EST_DEPT_DATE"])))
                  .ForMember(m => m.TransferStatus, opt => opt.MapFrom(r => r["TRANSFER_STATUS"]))
                  .ForMember(m => m.ShipMunShiftNo, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["SHIP_MUN_SHIFT_NO"])))
                  .ForMember(m => m.BladiyaFileNo, opt => opt.MapFrom(r => r["BLADIYA_FILE_NO"]))
                  .ForMember(m => m.ShipEtaDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["SHIP_ETA_DATE"])))
                  .ForMember(m => m.ShipSupplier, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["SHIP_SUPPLIER"])))
                  .ForMember(m => m.SupName, opt => opt.MapFrom(r => r["SUP_NAME"]))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, ShipContainerModel>()
              .ForMember(m => m.ShipId, opt => opt.MapFrom(r => OracleNullHandler.DbNullInt64Handler(r["SHIP_ID"])))
              .ForMember(m => m.ContainerId, opt => opt.MapFrom(r => r["CONTAINER_ID"]))
              .ForMember(m => m.LineSeq, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["LINE_SEQ"])))
              .ForMember(m => m.ContainerSize, opt => opt.MapFrom(r => r["CONTAINER_SIZE"]))
              .ForMember(m => m.ContainerSizeDesc, opt => opt.MapFrom(r => r["CONTAINER_SIZE_DESC"]))
              .ForMember(m => m.ItemCount, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["ITEM_COUNT"])))
              .ForMember(m => m.TotalWeight, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["TOTAL_WEIGHT"])))
            .ForMember(m => m.TableName, opt => opt.MapFrom(r => "ShipContainer"))
              .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, ShipDocModel>()
             .ForMember(m => m.ShipId, opt => opt.MapFrom(r => OracleNullHandler.DbNullInt64Handler(r["SHIP_ID"])))
             .ForMember(m => m.DocId, opt => opt.MapFrom(r => r["DOC_ID"]))
             .ForMember(m => m.LineSeq, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["LINE_SEQ"])))
             .ForMember(m => m.DocExpDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["DOC_EXP_DATE"])))
             .ForMember(m => m.DocActDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["DOC_ACT_DATE"])))
             .ForMember(m => m.DocComments, opt => opt.MapFrom(r => r["COMMENTS"]))
            .ForMember(m => m.TableName, opt => opt.MapFrom(r => "ShipDoc"))
             .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, ShipEventModel>()
            .ForMember(m => m.ShipId, opt => opt.MapFrom(r => OracleNullHandler.DbNullInt64Handler(r["SHIP_ID"])))
            .ForMember(m => m.EventId, opt => opt.MapFrom(r => r["EVENT_ID"]))
            .ForMember(m => m.LineSeq, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["LINE_SEQ"])))
            .ForMember(m => m.EventValue, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["EVENT_VALUE"])))
            .ForMember(m => m.EventText, opt => opt.MapFrom(r => r["EVENT_TEXT"]))
            .ForMember(m => m.EventDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["EVENT_DATE"])))
            .ForMember(m => m.DocEntryDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["DOC_ENTRY_DATE"])))
            .ForMember(m => m.Comments, opt => opt.MapFrom(r => r["COMMENTS"]))
            .ForMember(m => m.TableName, opt => opt.MapFrom(r => "ShipEvent"))
            .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, ShipOrderModel>()
            .ForMember(m => m.ShipId, opt => opt.MapFrom(r => OracleNullHandler.DbNullInt64Handler(r["SHIP_ID"])))
            .ForMember(m => m.OrderId, opt => opt.MapFrom(r => r["ORDER_ID"]))
            .ForMember(m => m.SeqNo, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["SEQ_NO"])))
            .ForMember(m => m.CreateDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["CREATE_DATE"])))
            .ForMember(m => m.SupName, opt => opt.MapFrom(r => r["SUP_NAME"]))
           .ForMember(m => m.CountryId, opt => opt.MapFrom(r => r["COUNTRY_ID"]))
           .ForMember(m => m.CountryDesc, opt => opt.MapFrom(r => r["COUNTRY_DESC"]))
           .ForMember(m => m.WrittenDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["WRITTEN_DATE"])))
           .ForMember(m => m.CurrencyCode, opt => opt.MapFrom(r => r["CURRENCY_CODE"]))
            .ForMember(m => m.TableName, opt => opt.MapFrom(r => "ShipOrder"))
            .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, NbDomainDetailModel>()
               .ForMember(m => m.CodeType, opt => opt.MapFrom(r => r["CODE_TYPE"]))
               .ForMember(m => m.Code, opt => opt.MapFrom(r => r["CODE_ID"]))
               .ForMember(m => m.CodeDesc, opt => opt.MapFrom(r => r["CODE_DETAIL_NAME"]))
               .ForMember(m => m.CodeStatus, opt => opt.MapFrom(r => r["CODE_STATUS"]));

            this.CreateMap<OracleObject, TransOrderModel>()
           .ForMember(m => m.OrderNo, opt => opt.MapFrom(r => OracleNullHandler.DbNullInt64Handler(r["ORDER_NO"])))
           .ForMember(m => m.Supplier, opt => opt.MapFrom(r => OracleNullHandler.DbNullInt64Handler(r["SUPPLIER"])))
           .ForMember(m => m.SupName, opt => opt.MapFrom(r => r["SUP_NAME"]))
           .ForMember(m => m.CountryId, opt => opt.MapFrom(r => r["COUNTRY_ID"]))
           .ForMember(m => m.CountryDesc, opt => opt.MapFrom(r => r["COUNTRY_DESC"]))
           .ForMember(m => m.WrittenDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["WRITTEN_DATE"])))
           .ForMember(m => m.CurrencyCode, opt => opt.MapFrom(r => r["CURRENCY_CODE"]));
        }
    }
}
