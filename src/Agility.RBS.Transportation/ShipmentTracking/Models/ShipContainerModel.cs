﻿// <copyright file="ShipContainerModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Transportation.ShipmentTracking.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.Framework.Core.Profile.Entities;

    public class ShipContainerModel
    {
        public string Operation { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string PError { get; set; }

        public long? ShipId { get; set; }

        public string ContainerId { get; set; }

        public int? LineSeq { get; set; }

        public string ContainerSize { get; set; }

        public string ContainerSizeDesc { get; set; }

        public int? ItemCount { get; set; }

        public decimal? TotalWeight { get; set; }

        public string TableName { get; set; }
    }
}
