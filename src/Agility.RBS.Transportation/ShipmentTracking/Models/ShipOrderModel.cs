﻿// <copyright file="ShipOrderModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Transportation.ShipmentTracking.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.Framework.Core.Profile.Entities;

    public class ShipOrderModel
    {
        public long? ShipId { get; set; }

        public int? SeqNo { get; set; }

        public string OrderId { get; set; }

        public DateTime? CreateDate { get; set; }

        public string Operation { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string PError { get; set; }

        public string TableName { get; set; }

        public string Supplier { get; set; }

        public string SupName { get; set; }

        public string CountryId { get; set; }

        public string CountryDesc { get; set; }

        public DateTime? WrittenDate { get; set; }

        public string CurrencyCode { get; set; }
    }
}
