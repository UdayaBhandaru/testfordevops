﻿// <copyright file="ShipmentModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Transportation.ShipmentTracking.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.MDM.Repositories.Models;

    public class ShipmentModel : ProfileEntity
    {
        public ShipmentModel()
        {
            this.ShipContainers = new List<ShipContainerModel>();
            this.ShipDocs = new List<ShipDocModel>();
            this.ShipEvents = new List<ShipEventModel>();
            this.ShipOrders = new List<ShipOrderModel>();
            this.Suppliers = new List<SupplierDomainModel>();
        }

        [Key]
        public long? ShipId { get; set; }

        public string ShipmentType { get; set; }

        public string TypeDesc { get; set; }

        public string ShipStatus { get; set; }

        public string StatusDesc { get; set; }

        public string ShipCompany { get; set; }

        public string CompanyDesc { get; set; }

        public string ShipDivision { get; set; }

        public string DivisionDesc { get; set; }

        public string ShipFinalStatus { get; set; }

        public string FinalStatusDesc { get; set; }

        public string ShipLine { get; set; }

        public string ShipLineDesc { get; set; }

        public string ShipAwbNo { get; set; }

        public string ShipFreightType { get; set; }

        public string FreightTypeDesc { get; set; }

        public string ShipCountryOrigin { get; set; }

        public string OrgCountryName { get; set; }

        public string ShipPortOfOrigin { get; set; }

        public string OrgPortName { get; set; }

        public string ShipDestination { get; set; }

        public string DestinationName { get; set; }

        public string ShipDestPort { get; set; }

        public string DestPortName { get; set; }

        public string ShipPolicyNo { get; set; }

        public DateTime? ShipDocRcptDate { get; set; }

        public DateTime? ShipOrigRcptDate { get; set; }

        public DateTime? ShipAnDate { get; set; }

        public DateTime? ShipAtaDate { get; set; }

        public string ShipContents { get; set; }

        public string ContentsDesc { get; set; }

        public DateTime? ShipBrokerDate { get; set; }

        public string ShipBroker { get; set; }

        public string BrokerName { get; set; }

        public DateTime? ShipClearanceDate { get; set; }

        public string ShipDelayReason { get; set; }

        public string DelayReasonDesc { get; set; }

        public DateTime? ShipBayanDate { get; set; }

        public string ShipBayanNo { get; set; }

        public DateTime? ShipHeldDate { get; set; }

        public string ShipHeldReason { get; set; }

        public string HeldReasonDesc { get; set; }

        public string IfrajType { get; set; }

        public string IfrajTypeDesc { get; set; }

        public DateTime? IfrajReqDate { get; set; }

        public DateTime? IfrajReceiptDate { get; set; }

        public string ShipInvoiceNo { get; set; }

        public DateTime? ShipInvoiceDate { get; set; }

        public string ShipInvoiceCurr { get; set; }

        public decimal? ShipInvoiceAmt { get; set; }

        public decimal? ShipFreightCharge { get; set; }

        public decimal? ShipTranspAmt { get; set; }

        public decimal? ShipInsuranceAmt { get; set; }

        public decimal? ShipClearanceAmt { get; set; }

        public decimal? ShipDemurageAmt { get; set; }

        public string ShipDemurageReason { get; set; }

        public decimal? ShipFinishingExp { get; set; }

        public decimal? ShipCustomsDuty { get; set; }

        public string ShipVatPer { get; set; }

        public decimal? ShipVatAmt { get; set; }

        public string ShipVesselId { get; set; }

        public string ShipVoyageFltId { get; set; }

        public DateTime? ShipEstDeptDate { get; set; }

        public string TransferStatus { get; set; }

        public int? ShipMunShiftNo { get; set; }

        public string BladiyaFileNo { get; set; }

        public DateTime? ShipEtaDate { get; set; }

        public int? ShipSupplier { get; set; }

        public string SupName { get; set; }

        [NotMapped]
        public List<ShipContainerModel> ShipContainers { get; private set; }

        [NotMapped]
        public List<ShipDocModel> ShipDocs { get; private set; }

        [NotMapped]
        public List<ShipEventModel> ShipEvents { get; private set; }

        [NotMapped]
        public List<ShipOrderModel> ShipOrders { get; private set; }

        public string Operation { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string PError { get; set; }

        [NotMapped]
        public List<SupplierDomainModel> Suppliers { get; internal set; }
    }
}
