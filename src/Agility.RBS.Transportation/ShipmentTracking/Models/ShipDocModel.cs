﻿// <copyright file="ShipDocModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Transportation.ShipmentTracking.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.Framework.Core.Profile.Entities;

    public class ShipDocModel
    {
        public string Operation { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string PError { get; set; }

        public long? ShipId { get; set; }

        public string DocId { get; set; }

        public int? LineSeq { get; set; }

        public DateTime? DocExpDate { get; set; }

        public DateTime? DocActDate { get; set; }

        public string DocComments { get; set; }

        public string TableName { get; set; }
    }
}
