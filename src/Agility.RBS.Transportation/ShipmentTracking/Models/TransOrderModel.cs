﻿// <copyright file="TransOrderModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Transportation.ShipmentTracking.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.Framework.Core.Profile.Entities;

    public class TransOrderModel
    {
        public string OrderNo { get; set; }

        public string Supplier { get; set; }

        public string SupName { get; set; }

        public string CountryId { get; set; }

        public string CountryDesc { get; set; }

        public DateTime? WrittenDate { get; set; }

        public string CurrencyCode { get; set; }
    }
}
