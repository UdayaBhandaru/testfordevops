﻿// <copyright file="ShipEventModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Transportation.ShipmentTracking.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.Framework.Core.Profile.Entities;

    public class ShipEventModel
    {
        public string Operation { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string PError { get; set; }

        public long? ShipId { get; set; }

        public string EventId { get; set; }

        public int? LineSeq { get; set; }

        public int? EventValue { get; set; }

        public string EventText { get; set; }

        public DateTime? EventDate { get; set; }

        public DateTime? DocEntryDate { get; set; }

        public string Comments { get; set; }

        public string TableName { get; set; }
    }
}
