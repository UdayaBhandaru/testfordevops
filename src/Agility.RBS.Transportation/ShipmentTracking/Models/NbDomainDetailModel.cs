﻿//-------------------------------------------------------------------------------------------------
// <copyright file="NbDomainDetailModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// DomainDetailModel
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.Transportation.ShipmentTracking.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class NbDomainDetailModel : ProfileEntity
    {
        public string CodeType { get; set; }

        public string Code { get; set; }

        public string CodeDesc { get; set; }

        public string CodeStatus { get; set; }
    }
}
