﻿// <copyright file="ObligationRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Transportation.Obligation.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.Transportation.Common;
    using Agility.RBS.Transportation.Obligation.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class ObligationRepository : BaseOraclePackage
    {
        public ObligationRepository()
        {
            this.PackageName = TransportationMgmtConstants.GetWrapperPackage;
        }

        public int? ObligationKey { get; private set; }

        public async Task<List<ObligationSearchModel>> ObligationListGet(ServiceDocument<ObligationSearchModel> serviceDoc, ServiceDocumentResult serviceDocumentResult)
        {
            ObligationSearchModel obligationSearch = serviceDoc.DataProfile.DataModel;
            PackageParams packageParameter = this.GetPackageParams(TransportationMgmtConstants.ObjTypeObligationSearch, TransportationMgmtConstants.GetProcbligationList);
            OracleObject recordObject = this.SetParamsObligationList(obligationSearch);

            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();

            BaseOracleParameter baseOracleParameter;
            baseOracleParameter = new BaseOracleParameter
            {
                Name = "P_START",
                DataType = OracleDbType.Number,
                Direction = ParameterDirection.Input,
                Value = Convert.ToString(obligationSearch.StartRow)
            };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            baseOracleParameter = new BaseOracleParameter { Name = "P_END", DataType = OracleDbType.Number, Direction = ParameterDirection.Input, Value = Convert.ToString(obligationSearch.EndRow) };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            baseOracleParameter = new BaseOracleParameter { Name = "P_total", DataType = OracleDbType.Number, Direction = ParameterDirection.Output };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            if (obligationSearch.SortModel == null)
            {
                obligationSearch.SortModel = new[] { new Core.Models.SortModel { ColId = null, Sort = null } };
            }

            baseOracleParameter = new BaseOracleParameter { Name = "P_SortColId", DataType = OracleDbType.VarChar, Direction = ParameterDirection.Input, Value = obligationSearch.SortModel[0].ColId };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            baseOracleParameter = new BaseOracleParameter { Name = "P_SortOrder", DataType = OracleDbType.VarChar, Direction = ParameterDirection.Input, Value = obligationSearch.SortModel[0].Sort };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));
            parameters.Add(this.ParameterBuilder(UserProfileHelper.GetUserParameter()));
            var obligationList = await this.GetProcedure2<ObligationSearchModel>(recordObject, packageParameter, serviceDocumentResult, parameters);
            obligationSearch.TotalRows = (this.Parameters["p_total"].Value != null && this.Parameters["p_total"].Value != DBNull.Value) ? Convert.ToInt64(this.Parameters["p_total"].Value) : 1;
            serviceDoc.DataProfile.DataModel = obligationSearch;
            return obligationList;
        }

        public async Task<TransShipmentModel> ShipmentDomainGet(int shipNo)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType("TRANS_SHIP_DD_REC", this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["SHIP_ID"]] = shipNo;
            PackageParams packageParameter = this.GetPackageParams(TransportationMgmtConstants.ObjTypeTRANSShipmentDomain, TransportationMgmtConstants.GetProcTRANSShipmentDomain);
            var shipdetails = await this.GetProcedure2<TransShipmentModel>(recordObject, packageParameter, null);
            return shipdetails != null ? shipdetails[0] : null;
        }

        public async Task<ObligationHeadModel> ObligationHeadGet(long obligationKey, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(TransportationMgmtConstants.RecordObjObligationHeader, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["OBLIGATION_KEY"]] = obligationKey;
            PackageParams packageParameter = this.GetPackageParams(TransportationMgmtConstants.ObjTypeObligationHeader, TransportationMgmtConstants.GetProcObligationHead);
            OracleTable pDomainMstTab = await this.GetProcedure(recordObject, packageParameter, serviceDocumentResult);
            List<ObligationHeadModel> obligationHeads = Mapper.Map<List<ObligationHeadModel>>(pDomainMstTab);

            if (obligationHeads != null)
            {
                OracleObject obj = (OracleObject)pDomainMstTab[0];
                OracleTable domainDetails = (Devart.Data.Oracle.OracleTable)obj["OBLG_COMP_TAB"];
                if (domainDetails.Count > 0)
                {
                    obligationHeads[0].ObligationDetails.AddRange(Mapper.Map<List<ObligationDetailModel>>(domainDetails));
                }
            }

            return obligationHeads != null ? obligationHeads[0] : null;
        }

        public async Task<ServiceDocumentResult> SetObligation(ObligationHeadModel dataModel)
        {
            this.ObligationKey = 0;

            PackageParams packageParameter = this.GetPackageParams(TransportationMgmtConstants.ObjTypeObligation, TransportationMgmtConstants.SetProcObligation);
            OracleObject recordObject = this.SetParamsObligation(dataModel);
            var result = await this.SetProcedure(recordObject, packageParameter);

            if (this.ObjResult != null && dataModel.OBLIGATION_KEY == null)
            {
                this.ObligationKey = Convert.ToInt32(this.ObjResult["OBLIGATION_KEY"]);
            }
            else
            {
                this.ObligationKey = dataModel.OBLIGATION_KEY;
            }

            return result;
        }

        private OracleObject SetParamsObligationList(ObligationSearchModel obligationSearch)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(TransportationMgmtConstants.RecordObjObligationSearch, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);

            recordObject[recordType.Attributes["OBLIGATION_KEY"]] = obligationSearch.ObligationKey;
            recordObject[recordType.Attributes["OBLIGATION_LEVEL"]] = obligationSearch.ObligationLevel;
            recordObject[recordType.Attributes["OBLIGATION_LEVEL_DESC"]] = obligationSearch.ObligationLevelDesc;
            recordObject[recordType.Attributes["KEY_VALUE_1"]] = obligationSearch.KeyValue1;
            recordObject[recordType.Attributes["KEY_VALUE_2"]] = obligationSearch.KeyValue2;
            recordObject[recordType.Attributes["KEY_VALUE_3"]] = obligationSearch.KeyValue3;
            recordObject[recordType.Attributes["KEY_VALUE_4"]] = obligationSearch.KeyValue4;
            recordObject[recordType.Attributes["KEY_VALUE_5"]] = obligationSearch.KeyValue5;
            recordObject[recordType.Attributes["KEY_VALUE_6"]] = obligationSearch.KeyValue6;
            recordObject[recordType.Attributes["PARTNER_TYPE"]] = obligationSearch.PartnerType;
            recordObject[recordType.Attributes["PARTNER_TYPE_DESC"]] = obligationSearch.PartnerTypeDesc;
            recordObject[recordType.Attributes["PARTNER_ID"]] = obligationSearch.PartnerId;
            recordObject[recordType.Attributes["PARTNER_NAME"]] = obligationSearch.PartnerName;
            recordObject[recordType.Attributes["SUPPLIER"]] = obligationSearch.Supplier;
            recordObject[recordType.Attributes["EXT_INVC_NO"]] = obligationSearch.ExtInvcNo;
            recordObject[recordType.Attributes["FROM_EXT_INVC_DATE"]] = obligationSearch.FromExtInvcDate;
            recordObject[recordType.Attributes["TO_EXT_INVC_DATE"]] = obligationSearch.ToExtInvcDate;
            recordObject[recordType.Attributes["FROM_PAID_DATE"]] = obligationSearch.FromPaidDate;
            recordObject[recordType.Attributes["TO_PAID_DATE"]] = obligationSearch.ToPaidDate;
            recordObject[recordType.Attributes["PAYMENT_METHOD"]] = obligationSearch.PaymentMethod;
            recordObject[recordType.Attributes["CHECK_AUTH_NO"]] = obligationSearch.CheckAuthNo;
            recordObject[recordType.Attributes["CURRENCY_CODE"]] = obligationSearch.CurrencyCode;
            recordObject[recordType.Attributes["EXCHANGE_RATE"]] = obligationSearch.ExchangeRate;
            recordObject[recordType.Attributes["STATUS"]] = obligationSearch.Status;
            return recordObject;
        }

        private OracleObject SetParamsObligation(ObligationHeadModel dataModel)
        {
            this.Connection.Open();
            OracleType headerrecordType = this.GetObjectType(TransportationMgmtConstants.RecordObjObligationHeader);
            OracleObject headRecordObject = new OracleObject(headerrecordType);

            headRecordObject["OBLIGATION_KEY"] = dataModel.OBLIGATION_KEY;
            headRecordObject["OBLIGATION_LEVEL"] = dataModel.ObligationLevel;
            headRecordObject["OBLIGATION_LEVEL_DESC"] = dataModel.ObligationLevelDesc;
            headRecordObject["KEY_VALUE_1"] = dataModel.KeyValue1;
            headRecordObject["KEY_VALUE_2"] = dataModel.KeyValue2;
            headRecordObject["KEY_VALUE_3"] = dataModel.KeyValue3;
            headRecordObject["KEY_VALUE_4"] = dataModel.KeyValue4;
            headRecordObject["KEY_VALUE_5"] = dataModel.KeyValue5;
            headRecordObject["KEY_VALUE_6"] = dataModel.KeyValue6;
            headRecordObject["PARTNER_TYPE"] = dataModel.PartnerType;
            headRecordObject["PARTNER_ID"] = dataModel.PartnerId;
            headRecordObject["SUPPLIER"] = dataModel.Supplier;
            headRecordObject["EXT_INVC_NO"] = dataModel.ExtInvcNo;
            headRecordObject["EXT_INVC_DATE"] = dataModel.ExtInvcDate;
            headRecordObject["PAID_DATE"] = dataModel.PaidDate;
            headRecordObject["PAID_AMT"] = dataModel.PaidAmt;
            headRecordObject["PAYMENT_METHOD"] = dataModel.PaymentMethod;
            headRecordObject["CHECK_AUTH_NO"] = dataModel.CheckAuthNo;
            headRecordObject["CURRENCY_CODE"] = dataModel.CurrencyCode;
            headRecordObject["EXCHANGE_RATE"] = dataModel.ExchangeRate;
            headRecordObject["STATUS"] = dataModel.Status;
            headRecordObject["COMMENT_DESC"] = dataModel.CommentDesc;
            headRecordObject["OPERATION"] = dataModel.Operation;

            OracleType obligationdetailsTableType = OracleType.GetObjectType(TransportationMgmtConstants.ObjTypeObligationDetails, this.Connection);
            OracleType obligationdetailsrecordType = OracleType.GetObjectType(TransportationMgmtConstants.ObjTypeObligationDetailsrec, this.Connection);
            OracleTable pObligationDtlTab = new OracleTable(obligationdetailsTableType);

            foreach (ObligationDetailModel detail in dataModel.ObligationDetails)
            {
                detail.InAlcInd = detail.InAlcInd == "1" ? "Y" : "N";

                OracleObject recordObjectdtls = new OracleObject(obligationdetailsrecordType);
                recordObjectdtls[obligationdetailsrecordType.Attributes["COMP_ID"]] = detail.CompId;
                ////recordObjectdtls[obligationdetailsrecordType.Attributes["ALLOC_TYPE"]] = detail.AllocType;
                recordObjectdtls[obligationdetailsrecordType.Attributes["ALLOC_BASIS_UOM"]] = detail.AllocBasicUom;
                recordObjectdtls[obligationdetailsrecordType.Attributes["AMT"]] = detail.Amt;
                recordObjectdtls[obligationdetailsrecordType.Attributes["QTY"]] = detail.Qty;
                recordObjectdtls[obligationdetailsrecordType.Attributes["RATE"]] = detail.Rate;
                recordObjectdtls[obligationdetailsrecordType.Attributes["PER_COUNT"]] = detail.PerCount;
                recordObjectdtls[obligationdetailsrecordType.Attributes["PER_COUNT_UOM"]] = detail.PerCountUom;
                recordObjectdtls[obligationdetailsrecordType.Attributes["IN_ALC_IND"]] = detail.InAlcInd;
                recordObjectdtls[obligationdetailsrecordType.Attributes["OPERATION"]] = detail.Operation;
                pObligationDtlTab.Add(recordObjectdtls);
            }

            headRecordObject["OBLG_COMP_TAB"] = pObligationDtlTab;
            return headRecordObject;
        }
    }
}
