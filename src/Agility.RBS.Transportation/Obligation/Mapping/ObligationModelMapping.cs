﻿// <copyright file="ObligationModelMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Transportation.Obligation.Mapping
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.RBS.Core;
    using Agility.RBS.Transportation.Obligation.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class ObligationModelMapping : Profile
    {
        public ObligationModelMapping()
            : base("ObligationModelMapping")
        {
            this.CreateMap<OracleObject, ObligationSearchModel>()
                .ForMember(m => m.ObligationKey, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["OBLIGATION_KEY"])))
                .ForMember(m => m.ObligationLevel, opt => opt.MapFrom(r => r["OBLIGATION_LEVEL"]))
                .ForMember(m => m.ObligationLevelDesc, opt => opt.MapFrom(r => r["OBLIGATION_LEVEL_DESC"]))
                .ForMember(m => m.KeyValue1, opt => opt.MapFrom(r => r["KEY_VALUE_1"]))
                .ForMember(m => m.KeyValue2, opt => opt.MapFrom(r => r["KEY_VALUE_2"]))
                .ForMember(m => m.KeyValue3, opt => opt.MapFrom(r => r["KEY_VALUE_3"]))
                .ForMember(m => m.KeyValue4, opt => opt.MapFrom(r => r["KEY_VALUE_4"]))
                .ForMember(m => m.KeyValue5, opt => opt.MapFrom(r => r["KEY_VALUE_5"]))
                .ForMember(m => m.KeyValue6, opt => opt.MapFrom(r => r["KEY_VALUE_6"]))
                .ForMember(m => m.PartnerType, opt => opt.MapFrom(r => r["PARTNER_TYPE"]))
                .ForMember(m => m.PartnerTypeDesc, opt => opt.MapFrom(r => r["PARTNER_TYPE_DESC"]))
                .ForMember(m => m.PartnerId, opt => opt.MapFrom(r => r["PARTNER_ID"]))
                .ForMember(m => m.PartnerName, opt => opt.MapFrom(r => r["PARTNER_NAME"]))
                .ForMember(m => m.Supplier, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["SUPPLIER"])))
                .ForMember(m => m.SupName, opt => opt.MapFrom(r => r["SUP_NAME"]))
                .ForMember(m => m.ExtInvcNo, opt => opt.MapFrom(r => r["EXT_INVC_NO"]))
                .ForMember(m => m.FromExtInvcDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["FROM_EXT_INVC_DATE"])))
                .ForMember(m => m.ToExtInvcDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["TO_EXT_INVC_DATE"])))
                .ForMember(m => m.FromPaidDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["FROM_PAID_DATE"])))
                .ForMember(m => m.ToPaidDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["TO_PAID_DATE"])))
                .ForMember(m => m.PaymentMethod, opt => opt.MapFrom(r => r["PAYMENT_METHOD"]))
                .ForMember(m => m.PaymentMethodDesc, opt => opt.MapFrom(r => r["PAY_METHOD_DESC"]))
                .ForMember(m => m.CheckAuthNo, opt => opt.MapFrom(r => r["CHECK_AUTH_NO"]))
                .ForMember(m => m.CurrencyCode, opt => opt.MapFrom(r => r["CURRENCY_CODE"]))
                .ForMember(m => m.ExchangeRate, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["EXCHANGE_RATE"])))
                .ForMember(m => m.Status, opt => opt.MapFrom(r => r["STATUS"]))
                .ForMember(m => m.ProgramPhase, opt => opt.MapFrom(r => r["PROGRAM_PHASE"]))
                .ForMember(m => m.ProgramMessage, opt => opt.MapFrom(r => r["PROGRAM_MESSAGE"]))
                .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_ERROR"]))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "OBLIGATION"))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, TransShipmentModel>()
               .ForMember(m => m.ShipId, opt => opt.MapFrom(r => r["SHIP_ID"]))
               .ForMember(m => m.ShipVesselId, opt => opt.MapFrom(r => r["SHIP_VESSEL_ID"]))
               .ForMember(m => m.ShipVoyageFltId, opt => opt.MapFrom(r => r["SHIP_VOYAGE_FLT_ID"]))
               .ForMember(m => m.ShipEstDeptDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["SHIP_EST_DEPT_DATE"])))
               .ForMember(m => m.ProgramPhase, opt => opt.MapFrom(r => r["PROGRAM_PHASE"]))
               .ForMember(m => m.ProgramMessage, opt => opt.MapFrom(r => r["PROGRAM_MESSAGE"]))
               .ForMember(m => m.PError, opt => opt.MapFrom(r => r["P_ERROR"]));

            this.CreateMap<OracleObject, ObligationHeadModel>()
                .ForMember(m => m.OBLIGATION_KEY, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["OBLIGATION_KEY"])))
                .ForMember(m => m.ObligationLevel, opt => opt.MapFrom(r => r["OBLIGATION_LEVEL"]))
                .ForMember(m => m.ObligationLevelDesc, opt => opt.MapFrom(r => r["OBLIGATION_LEVEL_DESC"]))
                .ForMember(m => m.KeyValue1, opt => opt.MapFrom(r => r["KEY_VALUE_1"]))
                .ForMember(m => m.KeyValue2, opt => opt.MapFrom(r => r["KEY_VALUE_2"]))
                .ForMember(m => m.KeyValue3, opt => opt.MapFrom(r => r["KEY_VALUE_3"]))
                .ForMember(m => m.KeyValue4, opt => opt.MapFrom(r => r["KEY_VALUE_4"]))
                .ForMember(m => m.KeyValue5, opt => opt.MapFrom(r => r["KEY_VALUE_5"]))
                .ForMember(m => m.KeyValue6, opt => opt.MapFrom(r => r["KEY_VALUE_6"]))
                .ForMember(m => m.PartnerType, opt => opt.MapFrom(r => r["PARTNER_TYPE"]))
                .ForMember(m => m.PartnerTypeDesc, opt => opt.MapFrom(r => r["PARTNER_TYPE_DESC"]))
                .ForMember(m => m.PartnerId, opt => opt.MapFrom(r => r["PARTNER_ID"]))
                .ForMember(m => m.PartnerName, opt => opt.MapFrom(r => r["PARTNER_NAME"]))
                .ForMember(m => m.Supplier, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["SUPPLIER"])))
                .ForMember(m => m.SupName, opt => opt.MapFrom(r => r["SUP_NAME"]))
                .ForMember(m => m.ExtInvcNo, opt => opt.MapFrom(r => r["EXT_INVC_NO"]))
                .ForMember(m => m.ExtInvcDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["EXT_INVC_DATE"])))
                .ForMember(m => m.PaidDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["PAID_DATE"])))
                .ForMember(m => m.PaidAmt, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["PAID_AMT"])))
                .ForMember(m => m.PaymentMethod, opt => opt.MapFrom(r => r["PAYMENT_METHOD"]))
                .ForMember(m => m.PaymentMethodDesc, opt => opt.MapFrom(r => r["PAY_METHOD_DESC"]))
                .ForMember(m => m.CheckAuthNo, opt => opt.MapFrom(r => r["CHECK_AUTH_NO"]))
                .ForMember(m => m.CurrencyCode, opt => opt.MapFrom(r => r["CURRENCY_CODE"]))
                .ForMember(m => m.ExchangeRate, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["EXCHANGE_RATE"])))
                .ForMember(m => m.Status, opt => opt.MapFrom(r => r["STATUS"]))
                .ForMember(m => m.CommentDesc, opt => opt.MapFrom(r => r["COMMENT_DESC"]))
                .ForMember(m => m.ProgramPhase, opt => opt.MapFrom(r => r["PROGRAM_PHASE"]))
                .ForMember(m => m.ProgramMessage, opt => opt.MapFrom(r => r["PROGRAM_MESSAGE"]))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"))
                .ForMember(m => m.PError, opt => opt.MapFrom(r => r["P_ERROR"]));

            this.CreateMap<OracleObject, ObligationDetailModel>()
                .ForMember(m => m.ObligationKey, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["OBLIGATION_KEY"])))
                .ForMember(m => m.CompId, opt => opt.MapFrom(r => r["COMP_ID"]))
                .ForMember(m => m.CompDesc, opt => opt.MapFrom(r => r["COMP_DESC"]))
                .ForMember(m => m.AllocType, opt => opt.MapFrom(r => r["ALLOC_TYPE"]))
                .ForMember(m => m.AllocTypeDesc, opt => opt.MapFrom(r => r["ALLOC_TYPE_DESC"]))
                .ForMember(m => m.AllocBasicUom, opt => opt.MapFrom(r => r["ALLOC_BASIS_UOM"]))
                .ForMember(m => m.Amt, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["AMT"])))
                .ForMember(m => m.Qty, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["QTY"])))
                .ForMember(m => m.Rate, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["RATE"])))
                .ForMember(m => m.PerCount, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["PER_COUNT"])))
                .ForMember(m => m.PerCountUom, opt => opt.MapFrom(r => r["PER_COUNT_UOM"]))
                .ForMember(m => m.InAlcInd, opt => opt.MapFrom(r => r["IN_ALC_IND"]))
                .ForMember(m => m.ProgramPhase, opt => opt.MapFrom(r => r["PROGRAM_PHASE"]))
                .ForMember(m => m.ProgramMessage, opt => opt.MapFrom(r => r["PROGRAM_MESSAGE"]))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"))
                .ForMember(m => m.PError, opt => opt.MapFrom(r => r["P_ERROR"]));
        }
    }
}
