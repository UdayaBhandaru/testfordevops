﻿// <copyright file="ObligationHeadController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Transportation.Obligation.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Inbox.Common;
    using Agility.RBS.Inbox.Models;
    using Agility.RBS.Inbox.Repository;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Models;
    using Agility.RBS.MDM.Trait;
    using Agility.RBS.MDM.UnitOfMeasure;
    using Agility.RBS.MDM.UnitOfMeasure.Models;
    using Agility.RBS.Transportation.Common;
    using Agility.RBS.Transportation.Obligation.Models;
    using Agility.RBS.Transportation.Obligation.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class ObligationHeadController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly ObligationRepository obligationRepository;
        private readonly UnitOfMeasureRepository uomRepository;
        private readonly InboxRepository inboxRepository;
        private readonly TraitRepository traitRepository;
        private readonly InboxModel inboxModel;
        private ServiceDocument<ObligationHeadModel> serviceDocument;
        private int obligationKey;

        public ObligationHeadController(
            ServiceDocument<ObligationHeadModel> serviceDocument,
            DomainDataRepository domainDataRepository,
            UnitOfMeasureRepository uomRepository,
            ObligationRepository obligationRepository,
            InboxRepository inboxRepository,
            TraitRepository traitRepository)
        {
            this.serviceDocument = serviceDocument;
            this.domainDataRepository = domainDataRepository;
            this.uomRepository = uomRepository;
            this.inboxRepository = inboxRepository;
            this.traitRepository = traitRepository;
            this.obligationRepository = obligationRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
            this.inboxModel = new InboxModel { Operation = "CHGS", ModuleName = "OBLIGATION" };
        }

        [HttpGet]
        public async Task<ServiceDocument<ObligationHeadModel>> New()
        {
            this.serviceDocument.New(false);
            return await this.ObligationDomainData();
        }

        [HttpPost]
        public async Task<ServiceDocument<ObligationHeadModel>> Save([FromBody]ServiceDocument<ObligationHeadModel> serviceDocument)
        {
            await this.serviceDocument.SaveAsync(this.ObligationHeadSave);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<ObligationHeadModel>> Open(int id)
        {
            this.obligationKey = id;
            await this.serviceDocument.OpenAsync(this.ObligationHeadOpen);
            if (this.serviceDocument.DataProfile.DataModel.Supplier != null)
            {
                this.serviceDocument.DomainData.Add("supplier", this.domainDataRepository.SupplierObligationDomainGet(this.serviceDocument.DataProfile.DataModel.Supplier.Value).Result);
            }

            return await this.ObligationDomainData();
        }

        [HttpPost]
        public async Task<ServiceDocument<ObligationHeadModel>> Submit([FromBody]ServiceDocument<ObligationHeadModel> serviceDocument)
        {
            this.serviceDocument = serviceDocument;
            if (this.serviceDocument.DataProfile.DataModel.OBLIGATION_KEY == 0 || this.serviceDocument.DataProfile.DataModel.OBLIGATION_KEY == null)
            {
                this.inboxModel.Operation = "DFT";
            }

            this.inboxModel.StateID = RbsServiceDocCustomizationHelper.GetStateIdDotNetToOracle(this.serviceDocument);
            await this.serviceDocument.TransitAsync(this.ObligationHeadSave);
            if (this.serviceDocument.Result.Type == MessageType.Success)
            {
                await this.inboxRepository.SetInbox(this.inboxModel, this.serviceDocument);
            }

            return this.serviceDocument;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<List<PartnerTransDomainModel>> GetPartnerDetailsById(string partnerType)
        {
            return await this.domainDataRepository.PartnerGet(partnerType);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<List<SupplierObligationModel>> SuppliersObligationGetName(string name)
        {
            return await this.domainDataRepository.SupplierObligationDomainGet(name);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<TransShipmentModel> ShipmentDetailsGet(int shipNo)
        {
            try
            {
                return await this.obligationRepository.ShipmentDomainGet(shipNo);
            }
            catch (System.Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> ObligationHeadSave()
        {
            this.serviceDocument.Result = await this.obligationRepository.SetObligation(this.serviceDocument.DataProfile.DataModel);
            if (this.serviceDocument.Result.Type != MessageType.Success)
            {
                return false;
            }

            this.serviceDocument.DataProfile.DataModel.OBLIGATION_KEY = this.obligationRepository.ObligationKey;
            return true;
        }

        private async Task<ServiceDocument<ObligationHeadModel>> ObligationDomainData()
        {
            string partnerType = null;
            this.serviceDocument.DomainData.Add("obligation", this.domainDataRepository.DomainDetailData(TransportationMgmtConstants.DomainHdrObligationType).Result);
            this.serviceDocument.DomainData.Add("partnerType", this.domainDataRepository.DomainDetailData(TransportationMgmtConstants.DomainHdrPartnerType).Result);
            this.serviceDocument.DomainData.Add("paymentType", this.domainDataRepository.DomainDetailData(TransportationMgmtConstants.DomainHdrPaymentType).Result);
            this.serviceDocument.DomainData.Add("checkAuth", this.domainDataRepository.DomainDetailData(TransportationMgmtConstants.DomainHdrCheckAuth).Result);
            this.serviceDocument.DomainData.Add("uom", await this.uomRepository.GetUom(new UomModel(), this.serviceDocumentResult));
            this.serviceDocument.DomainData.Add("component", await this.traitRepository.GetComponentTraits("E"));
            this.serviceDocument.DomainData.Add("domainRadioData", this.domainDataRepository.DomainDetailData(TransportationMgmtConstants.DomainHdrRaioButtonData).Result);
            this.serviceDocument.DomainData.Add("partner", await this.domainDataRepository.PartnerGet(partnerType));
            return this.serviceDocument;
        }

        private async Task<ObligationHeadModel> ObligationHeadOpen()
        {
            try
            {
                var obligationHead = await this.obligationRepository.ObligationHeadGet(this.obligationKey, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return obligationHead;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }
    }
}
