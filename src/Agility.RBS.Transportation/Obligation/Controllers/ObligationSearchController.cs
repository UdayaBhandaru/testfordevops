﻿// <copyright file="ObligationSearchController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Transportation.Obligation.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.Transportation.Common;
    using Agility.RBS.Transportation.Obligation.Models;
    using Agility.RBS.Transportation.Obligation.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    public class ObligationSearchController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly ServiceDocument<ObligationSearchModel> serviceDocument;
        private readonly ObligationRepository obligationRepository;

        public ObligationSearchController(
            ServiceDocument<ObligationSearchModel> serviceDocument,
            ObligationRepository obligationRepository,
        DomainDataRepository domainDataRepository)
        {
            this.serviceDocument = serviceDocument;
            this.obligationRepository = obligationRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<ObligationSearchModel>> List()
        {
            string partnerType = null;
            this.serviceDocument.DomainData.Add("obligation", this.domainDataRepository.DomainDetailData(TransportationMgmtConstants.DomainHdrObligationType).Result);
            this.serviceDocument.DomainData.Add("partnerType", this.domainDataRepository.DomainDetailData(TransportationMgmtConstants.DomainHdrPartnerType).Result);
            this.serviceDocument.DomainData.Add("paymentType", this.domainDataRepository.DomainDetailData(TransportationMgmtConstants.DomainHdrPaymentType).Result);
            this.serviceDocument.DomainData.Add("status", this.domainDataRepository.DomainDetailData(TransportationMgmtConstants.DomainHdrObligationStatus).Result);
            this.serviceDocument.DomainData.Add("checkAuth", this.domainDataRepository.DomainDetailData(TransportationMgmtConstants.DomainHdrCheckAuth).Result);
            this.serviceDocument.DomainData.Add("currency", this.domainDataRepository.CurrencyDomainGet().Result);
            this.serviceDocument.DomainData.Add("partner", await this.domainDataRepository.PartnerGet(partnerType));
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<ObligationSearchModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.ObligationSearch);
            return this.serviceDocument;
        }

        private async Task<List<ObligationSearchModel>> ObligationSearch()
        {
            try
            {
                var obligationList = await this.obligationRepository.ObligationListGet(this.serviceDocument, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return obligationList;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }
    }
}
