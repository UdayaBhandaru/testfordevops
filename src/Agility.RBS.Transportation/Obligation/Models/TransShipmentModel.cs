﻿// <copyright file="TransShipmentModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Transportation.Obligation.Models
{
    using System;
    using Agility.Framework.Core.Profile.Entities;

    public class TransShipmentModel : ProfileEntity
    {
        public int? ShipId { get; set; }

        public string ShipVesselId { get; set; }

        public string ShipVoyageFltId { get; set; }

        public DateTime? ShipEstDeptDate { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string PError { get; set; }
    }
}
