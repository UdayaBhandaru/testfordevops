﻿// <copyright file="ObligationDetailModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Transportation.Obligation.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.Framework.Core.Profile.Entities;

    public class ObligationDetailModel : ProfileEntity
    {
        public int? ObligationKey { get; set; }

        public string CompId { get; set; }

        public string CompDesc { get; set; }

        public string AllocType { get; set; }

        public string AllocTypeDesc { get; set; }

        public string AllocBasicUom { get; set; }

        public int? Amt { get; set; }

        public int? Qty { get; set; }

        public int? Rate { get; set; }

        public int? PerCount { get; set; }

        public string PerCountUom { get; set; }

        public string InAlcInd { get; set; }

        public string Operation { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string PError { get; set; }
    }
}
