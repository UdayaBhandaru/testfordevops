﻿// <copyright file="ObligationSearchModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Transportation.Obligation.Models
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Core.Models;

    public class ObligationSearchModel : ProfileEntity
    {
        [NotMapped]
        public int? ObligationKey { get; set; }

        [NotMapped]
        public string ObligationLevel { get; set; }

        [NotMapped]
        public string ObligationLevelDesc { get; set; }

        [NotMapped]
        public string KeyValue1 { get; set; }

        [NotMapped]
        public string KeyValue2 { get; set; }

        [NotMapped]
        public string KeyValue3 { get; set; }

        [NotMapped]
        public string KeyValue4 { get; set; }

        [NotMapped]
        public string KeyValue5 { get; set; }

        [NotMapped]
        public string KeyValue6 { get; set; }

        [NotMapped]
        public string PartnerType { get; set; }

        [NotMapped]
        public string PartnerTypeDesc { get; set; }

        [NotMapped]
        public string PartnerId { get; set; }

        [NotMapped]
        public string PartnerName { get; set; }

        [NotMapped]
        public int? Supplier { get; set; }

        [NotMapped]
        public string SupName { get; set; }

        [NotMapped]
        public string ExtInvcNo { get; set; }

        [NotMapped]
        public DateTime? FromExtInvcDate { get; set; }

        [NotMapped]
        public DateTime? ToExtInvcDate { get; set; }

        [NotMapped]
        public DateTime? FromPaidDate { get; set; }

        [NotMapped]
        public DateTime? ToPaidDate { get; set; }

        [NotMapped]
        public string PaymentMethod { get; set; }

        [NotMapped]
        public string PaymentMethodDesc { get; set; }

        [NotMapped]
        public string CheckAuthNo { get; set; }

        [NotMapped]
        public string CurrencyCode { get; set; }

        [NotMapped]
        public int? ExchangeRate { get; set; }

        [NotMapped]
        public string Status { get; set; }

        [NotMapped]
        public string ProgramPhase { get; set; }

        [NotMapped]
        public string ProgramMessage { get; set; }

        [NotMapped]
        public string Error { get; set; }

        [NotMapped]
        public string Operation { get; set; }

        [NotMapped]
        public string TableName { get; set; }

        [NotMapped]
        public int? StartRow { get; set; }

        [NotMapped]
        public int? EndRow { get; set; }

        [NotMapped]
        public long? TotalRows { get; set; }

        [NotMapped]
        public SortModel[] SortModel { get; set; }
    }
}
