﻿// <copyright file="ObligationWfModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Transportation.Obligation.Models
{
    using System.ComponentModel.DataAnnotations;
    using Agility.Framework.Core.Workflow.Entities;

    public class ObligationWfModel : WorkFlowBaseInstance, IWorkFlowInstance
    {
        [Key]
        public int WorkflowInstanceId { get; set; }
    }
}
