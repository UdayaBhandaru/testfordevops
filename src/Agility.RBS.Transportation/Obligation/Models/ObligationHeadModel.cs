﻿// <copyright file="ObligationHeadModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Transportation.Obligation.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Inbox.Common;

    [DataProfile(Name = "OBLIGATION")]
    public class ObligationHeadModel : ProfileEntity<ObligationWfModel>, IInboxCommonModel
    {
        public ObligationHeadModel()
        {
            this.ObligationDetails = new List<ObligationDetailModel>();
        }

        [Column("OBLIGATION_KEY")]
        public int? OBLIGATION_KEY { get; set; }

        [NotMapped]
        public string ObligationLevel { get; set; }

        [NotMapped]
        public string ObligationLevelDesc { get; set; }

        [NotMapped]
        public string KeyValue1 { get; set; }

        [NotMapped]
        public string KeyValue2 { get; set; }

        [NotMapped]
        public string KeyValue3 { get; set; }

        [NotMapped]
        public string KeyValue4 { get; set; }

        [NotMapped]
        public string KeyValue5 { get; set; }

        [NotMapped]
        public string KeyValue6 { get; set; }

        [NotMapped]
        public string PartnerType { get; set; }

        [NotMapped]
        public string PartnerTypeDesc { get; set; }

        [NotMapped]
        public string PartnerId { get; set; }

        [NotMapped]
        public string PartnerName { get; set; }

        [NotMapped]
        public int? Supplier { get; set; }

        [NotMapped]
        public string SupName { get; set; }

        [NotMapped]
        public string ExtInvcNo { get; set; }

        [NotMapped]
        public DateTime? ExtInvcDate { get; set; }

        [NotMapped]
        public DateTime? PaidDate { get; set; }

        [NotMapped]
        public int? PaidAmt { get; set; }

        [NotMapped]
        public string PaymentMethod { get; set; }

        [NotMapped]
        public string PaymentMethodDesc { get; set; }

        [NotMapped]
        public string CheckAuthNo { get; set; }

        [NotMapped]
        public string CurrencyCode { get; set; }

        [NotMapped]
        public int? ExchangeRate { get; set; }

        [NotMapped]
        public string Status { get; set; }

        [NotMapped]
        public string CommentDesc { get; set; }

        [NotMapped]
        public string Operation { get; set; }

        [NotMapped]
        public string ProgramPhase { get; set; }

        [NotMapped]
        public string ProgramMessage { get; set; }

        [NotMapped]
        public string PError { get; set; }

        [NotMapped]
        [Column("ORGANISATION_ID")]
        public new int? OrganizationId { get; set; }

        [NotMapped]
        [Column("CREATED_BY")]
        public new string CreatedBy { get; set; }

        [NotMapped]
        [Column("LAST_UPDATED_BY")]
        public new string ModifiedBy { get; set; }

        [NotMapped]
        [Column("DeletedInd")]
        public new bool DeletedInd { get; set; }

        [NotMapped]
        [Column("CREATED_DATE")]
        public new DateTimeOffset? CreatedDate { get; set; }

        [NotMapped]
        [Column("LAST_UPDATED_DATE")]
        public new DateTimeOffset? ModifiedDate { get; set; }

        [NotMapped]
        public Core.Models.RbWfHeaderModel WorkflowForm { get; set; }

        [NotMapped]
        public List<ObligationDetailModel> ObligationDetails { get; private set; }
    }
}
