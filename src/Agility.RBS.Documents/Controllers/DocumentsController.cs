﻿// <copyright file="DocumentsController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Documents
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net.Http.Headers;
    using System.Text;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.CustomExceptions;
    using Agility.RBS.Documents.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    public class DocumentsController : Controller
    {
        private readonly DocumentsRepository documentsRepository;
        private readonly ServiceDocument<DocumentHeaderModel> serviceDocument;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private string objectName;
        private string objectValue;

        public DocumentsController(
            DocumentsRepository documentsRepository,
            ServiceDocument<DocumentHeaderModel> serviceDocument,
            IHostingEnvironment hostingEnvironment)
        {
            this.documentsRepository = documentsRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        [HttpPost]
        public async ValueTask<bool> UploadDocuments([FromBody] DocumentHeaderModel documentHeaderModelObj)
        {
            try
            {
                foreach (DocumentDetailModel documentDetailModObj in documentHeaderModelObj.DocumentDetails)
                {
                    documentDetailModObj.FilePath = DocumentsHelper.FileUpload(
                        documentHeaderModelObj.ObjectName,
                        documentDetailModObj.FileName,
                        documentDetailModObj.FileData,
                        documentHeaderModelObj.ObjectValue);
                }

                ServiceDocumentResult serviceDocumentResultObj = await this.documentsRepository.InsertDocuments(documentHeaderModelObj);
                if (!string.IsNullOrEmpty(serviceDocumentResultObj.InnerException))
                {
                    throw new FileUploadException(serviceDocumentResultObj.InnerException);
                }

                return true;
            }
            catch (Exception ex)
            {
                throw new FileUploadException(ex.Message);
            }
        }

        public async Task<ServiceDocument<DocumentHeaderModel>> GetListOfDocuments(string objectName, string objectValue)
        {
            this.objectName = objectName;
            this.objectValue = objectValue;
            await this.serviceDocument.ToListAsync(this.CallBackGetListOfDocuments);
            return this.serviceDocument;
        }

        public FileResult DownloadDocument(int documentDetailId)
        {
            List<DocumentDetailModel> documentDetailListObj = this.documentsRepository.GetDocumentBasedOnId(documentDetailId, this.serviceDocumentResult);
            this.serviceDocument.Result = this.serviceDocumentResult;
            string savedPathInDB = string.Empty;
            string savedFileNameInDB = string.Empty;
            if (documentDetailListObj != null && documentDetailListObj.Count > 0)
            {
                savedPathInDB = documentDetailListObj[0].FilePath;
                savedFileNameInDB = documentDetailListObj[0].FileName;
            }

            string filePath = DocumentsHelper.GetFullPathForDownload(savedPathInDB);
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            FileContentResult resultObj = this.File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, savedFileNameInDB);
            return resultObj;
        }

        [HttpPost]
        public async ValueTask<bool> DeleteDocuments([FromBody] DocumentHeaderModel documentHeaderModelObj)
        {
            try
            {
                ServiceDocumentResult serviceDocumentResultObj = await this.documentsRepository.InsertDocuments(documentHeaderModelObj);
                if (!string.IsNullOrEmpty(serviceDocumentResultObj.InnerException))
                {
                    throw new FileUploadException(serviceDocumentResultObj.InnerException);
                }

                return true;
            }
            catch (Exception ex)
            {
                throw new FileUploadException(ex.Message);
            }
        }

        private async Task<List<DocumentHeaderModel>> CallBackGetListOfDocuments()
        {
            try
            {
                DocumentHeaderModel documentHeaderModelObj = new DocumentHeaderModel
                {
                    ObjectName = this.objectName,
                    ObjectValue = this.objectValue
                };
                List<DocumentHeaderModel> listDocumentsObj = await this.documentsRepository.GetListOfDocuments(documentHeaderModelObj, this.serviceDocumentResult);
                ////if (listDocumentsObj != null && listDocumentsObj.Count > 0)
                ////{
                ////    foreach (DocumentDetailModel docDetail in listDocumentsObj[0].DocumentDetails)
                ////    {
                ////        docDetail.StateID = GuidConverter.OracleToDotNet(Convert.ToString(docDetail.StateID));
                ////    }
                ////}

                this.serviceDocument.Result = this.serviceDocumentResult;
                return listDocumentsObj;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }
    }
}