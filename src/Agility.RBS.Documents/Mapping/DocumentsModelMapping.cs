﻿// <copyright file="DocumentsModelMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Documents.Mapping
{
    using System;
    using Agility.RBS.Core;
    using Agility.RBS.Documents.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class DocumentsModelMapping : Profile
    {
        public DocumentsModelMapping()
            : base("DocumentsModelMapping")
        {
            this.CreateMap<OracleObject, DocumentHeaderModel>()
                .ForMember(m => m.Id, opt => opt.MapFrom(r => r["ID"]))
                .ForMember(m => m.ObjectName, opt => opt.MapFrom(r => r["OBJECT_NAME"]))
                .ForMember(m => m.ObjectValue, opt => opt.MapFrom(r => r["OBJECT_VALUE"]))
                .ForMember(m => m.CreatedBy, opt => opt.MapFrom(r => r["CREATED_BY"]))
                .ForMember(m => m.ModifiedBy, opt => opt.MapFrom(r => r["LAST_UPDATED_BY"]))
                .ForMember(m => m.CreatedDate, opt => opt.MapFrom(r => r["CREATED_DATE"]))
                .ForMember(m => m.ModifiedDate, opt => opt.MapFrom(r => r["LAST_UPDATED_DATE"]))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "DOCUMENT_HEADER"))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => r["OPERATION"]));

            this.CreateMap<OracleObject, DocumentDetailModel>()
                .ForMember(m => m.Id, opt => opt.MapFrom(r => r["ID"]))
                .ForMember(m => m.DocumentHeaderID, opt => opt.MapFrom(r => r["HEADER_ID"]))
                .ForMember(m => m.FileName, opt => opt.MapFrom(r => r["FILE_NAME"]))
                .ForMember(m => m.FilePath, opt => opt.MapFrom(r => r["FILE_PATH"]))
                .ForMember(m => m.StateID, opt => opt.MapFrom(r => GuidConverter.OracleToDotNet(Convert.ToString(r["WORKFLOWSTATEID"]))))
                .ForMember(m => m.StateName, opt => opt.MapFrom(r => r["STATENAME"]))
                .ForMember(m => m.DocumentStatus, opt => opt.MapFrom(r => r["DOCUMENT_STATUS"]))
                .ForMember(m => m.CreatedBy, opt => opt.MapFrom(r => r["CREATED_BY"]))
                .ForMember(m => m.ModifiedBy, opt => opt.MapFrom(r => r["LAST_UPDATED_BY"]))
                .ForMember(m => m.CreatedDate, opt => opt.MapFrom(r => r["CREATED_DATE"]))
                .ForMember(m => m.ModifiedDate, opt => opt.MapFrom(r => r["LAST_UPDATED_DATE"]))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => r["OPERATION"]));
        }
    }
}
