﻿// <copyright file="DocumentHeaderModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Documents.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using Agility.Framework.Core.Profile.Entities;

    public class DocumentHeaderModel : ProfileEntity
    {
        public DocumentHeaderModel()
        {
            this.DocumentDetails = new List<DocumentDetailModel>();
        }

        [Column("ID")]
        public int? Id { get; set; }

        public string ObjectName { get; set; }

        public string ObjectValue { get; set; }

        public List<DocumentDetailModel> DocumentDetails { get; private set; }

        public string Operation { get; set; }

        public string TableName { get; set; }
    }
}
