﻿// <copyright file="DocumentDetailModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Documents.Models
{
    using System.ComponentModel.DataAnnotations.Schema;
    using Agility.Framework.Core.Profile.Entities;

    public class DocumentDetailModel : ProfileEntity
    {
        [Column("ID")]
        public int? Id { get; set; }

        public string FilePath { get; set; }

        public string FileName { get; set; }

        public string FileData { get; set; }

        public string DocumentStatus { get; set; }

        public string Operation { get; set; }

        public int? DocumentHeaderID { get; set; }

        public Microsoft.AspNetCore.Http.IFormFile File { get; set; }

        public string StateID { get; set; }

        public string StateName { get; set; }
    }
}
