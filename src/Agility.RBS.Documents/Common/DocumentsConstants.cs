﻿// <copyright file="DocumentsConstants.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Documents.Common
{
    internal static class DocumentsConstants
    {
        // Result
        public const string OracleParameterResult = "RESULT";

        // Documents Package
        public const string GetDocumentsPackage = "UTL_DOCUMENT";

        public const string ObjTypeDocumentsHeader = "UTL_DOC_HDR_TAB";
        public const string SetProcDocuments = "SETUTLDOCUMENT";
        public const string GetProcDocuments = "GETUTLDOCUMENT";
        public const string DocumentDetailTbl = "P_UTL_DOC_DTL_TAB";
        public const string ObjTypeNameDocumentDetail = "UTL_DOC_DTL_TAB";
        public const string GetProcDocumentDetail = "GETDETAILDOCUMENT";
    }
}