﻿// <copyright file="DocumentsRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Documents
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.CustomExceptions;
    using Agility.RBS.Core.Models;
    using Agility.RBS.Documents.Common;
    using Agility.RBS.Documents.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class DocumentsRepository : BaseOraclePackage
    {
        public DocumentsRepository()
        {
            this.PackageName = DocumentsConstants.GetDocumentsPackage;
        }

        public static async Task<bool> CopyingFile(FileDataUpload file)
        {
            var tempFile = $"{Guid.NewGuid()}.xlsm";
            DocumentsHelper.FileUpload(string.Empty, tempFile, file.FileData);
            tempFile = $"{RbSettings.RbsExcelFileDownloadPath}\\{tempFile}";
            var bulkItemFile = $"{RbSettings.RbsExcelFileDownloadPath}\\{file.FileName}-{file.RequestId}.xlsm";
            if (System.IO.File.Exists(bulkItemFile))
            {
                System.IO.File.Delete(bulkItemFile);
            }

            System.IO.File.Copy(tempFile, bulkItemFile);
            System.IO.File.Delete(tempFile);
            return true;
        }

        public async Task<List<DocumentHeaderModel>> GetListOfDocuments(DocumentHeaderModel documentHeaderModelObj, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(DocumentsConstants.ObjTypeDocumentsHeader, DocumentsConstants.GetProcDocuments);
            OracleObject recordObject = this.SetParamsDocuments(documentHeaderModelObj, true);
            OracleTable pDocumentMstTab = await this.GetProcedure(recordObject, packageParameter, serviceDocumentResult);

            List<DocumentHeaderModel> lstHeader = Mapper.Map<List<DocumentHeaderModel>>(pDocumentMstTab);
            if (lstHeader != null)
            {
                for (int i = 0; i < lstHeader.Count; i++)
                {
                    OracleObject obj = (OracleObject)pDocumentMstTab[i];
                    OracleTable documentDetailsObj = (Devart.Data.Oracle.OracleTable)obj["UTL_DOC_DTL"];
                    if (documentDetailsObj.Count > 0)
                    {
                        lstHeader[i].DocumentDetails.AddRange(Mapper.Map<List<DocumentDetailModel>>(documentDetailsObj));
                    }
                }
            }

            return lstHeader;
        }

        public async Task<ServiceDocumentResult> InsertDocuments(DocumentHeaderModel documentHeaderModelObj)
        {
            PackageParams packageParameter = this.GetPackageParams(DocumentsConstants.ObjTypeDocumentsHeader, DocumentsConstants.SetProcDocuments);
            OracleObject recordObject = this.SetParamsDocuments(documentHeaderModelObj, false);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public List<DocumentDetailModel> GetDocumentBasedOnId(int documentDetailId, ServiceDocumentResult serviceDocumentResult)
        {
            try
            {
                OracleTable documentDetailTableObj = this.GetDetailResponseFromDB(documentDetailId, serviceDocumentResult);
                return Mapper.Map<List<DocumentDetailModel>>(documentDetailTableObj);
            }
            finally
            {
                this.Connection.Close();
            }
        }

        public OracleTable GetDetailResponseFromDB(int documentDetailId, ServiceDocumentResult serviceDocumentResult)
        {
            try
            {
                this.Connection.Open();
                OracleTable p_detail_tbl = null;
                OracleParameterCollection parameters;
                parameters = this.Parameters;
                parameters.Clear();
                OracleParameter parameter;
                parameter = new OracleParameter("RESULT", OracleDbType.Boolean, ParameterDirection.ReturnValue);
                parameters.Add(parameter);

                parameter = new OracleParameter("P_ID", OracleDbType.Number)
                {
                    Direction = System.Data.ParameterDirection.Input,
                    Value = documentDetailId
                };
                parameters.Add(parameter);

                parameter = new OracleParameter(DocumentsConstants.DocumentDetailTbl, Devart.Data.Oracle.OracleDbType.Table)
                {
                    Direction = System.Data.ParameterDirection.Output,
                    ObjectTypeName = DocumentsConstants.ObjTypeNameDocumentDetail,
                };

                parameters.Add(parameter);

                this.ExecuteProcedure(DocumentsConstants.GetProcDocumentDetail, parameters);

                if (this.Parameters[DocumentsConstants.DocumentDetailTbl].Value != System.DBNull.Value)
                {
                    p_detail_tbl = (OracleTable)this.Parameters[DocumentsConstants.DocumentDetailTbl].Value;
                }

                if (p_detail_tbl != null && p_detail_tbl.Count == 1)
                {
                    OracleObject obj = (OracleObject)p_detail_tbl[0];
                    if (obj == null)
                    {
                        serviceDocumentResult.InnerException = "No Records Found";
                        serviceDocumentResult.Type = MessageType.Exception;
                        return null;
                    }
                    else if (!string.IsNullOrEmpty(Convert.ToString(obj["P_Error"])))
                    {
                        serviceDocumentResult.InnerException = Convert.ToString(obj["Program_Message"]);
                        serviceDocumentResult.StackTrace = Convert.ToString(obj["P_Error"]);
                        serviceDocumentResult.Type = MessageType.Exception;
                        return null;
                    }
                    else
                    {
                        serviceDocumentResult.InnerException = string.Empty;
                        serviceDocumentResult.StackTrace = string.Empty;
                        serviceDocumentResult.Type = MessageType.Success;
                        return p_detail_tbl;
                    }
                }

                serviceDocumentResult.InnerException = string.Empty;
                serviceDocumentResult.StackTrace = string.Empty;
                serviceDocumentResult.Type = MessageType.Success;

                return p_detail_tbl;
            }
            finally
            {
                this.Connection.Close();
            }
        }

        public OracleObject SetParamsDocuments(DocumentHeaderModel documentHeaderModelObj, bool isGetCall)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType("UTL_DOC_HDR_REC", this.Connection);
            OracleObject recordObject = new OracleObject(recordType);

            if (isGetCall)
            {
                recordObject[recordType.Attributes["OBJECT_NAME"]] = documentHeaderModelObj.ObjectName;
                recordObject[recordType.Attributes["OBJECT_VALUE"]] = documentHeaderModelObj.ObjectValue;
                return recordObject;
            }
            else
            {
                if (documentHeaderModelObj.Operation.Equals("I"))
                {
                    recordObject[recordType.Attributes["CREATED_BY"]] = FxContext.Context.Name ?? "SEEDDATA";
                }

                recordObject[recordType.Attributes["LAST_UPDATED_BY"]] = FxContext.Context.Name ?? "SEEDDATA";
                recordObject[recordType.Attributes["OPERATION"]] = documentHeaderModelObj.Operation;

                // Detail Object
                OracleType dtlTableType = OracleType.GetObjectType("UTL_DOC_DTL_TAB", this.Connection);
                OracleType dtlRecordType = OracleType.GetObjectType("UTL_DOC_DTL_REC", this.Connection);
                OracleTable pUtlDocDtlTab = new OracleTable(dtlTableType);
                documentHeaderModelObj.DocumentDetails.ForEach(detail =>
                {
                    var dtlRecordObject = new OracleObject(dtlRecordType);
                    dtlRecordObject[dtlRecordType.Attributes["FILE_PATH"]] = detail.FilePath;
                    dtlRecordObject[dtlRecordType.Attributes["FILE_NAME"]] = detail.FileName;
                    dtlRecordObject[dtlRecordType.Attributes["DOCUMENT_STATUS"]] = detail.DocumentStatus;
                    dtlRecordObject[dtlRecordType.Attributes["CREATED_BY"]] = FxContext.Context.Name ?? "SEEDDATA";
                    dtlRecordObject[dtlRecordType.Attributes["LAST_UPDATED_BY"]] = FxContext.Context.Name ?? "SEEDDATA";
                    dtlRecordObject[dtlRecordType.Attributes["OPERATION"]] = detail.Operation;
                    pUtlDocDtlTab.Add(dtlRecordObject);
                });

                recordObject[recordType.Attributes["UTL_DOC_DTL"]] = pUtlDocDtlTab;
            }

            recordObject[recordType.Attributes["OBJECT_NAME"]] = documentHeaderModelObj.ObjectName;
            recordObject[recordType.Attributes["OBJECT_VALUE"]] = documentHeaderModelObj.ObjectValue;
            return recordObject;
        }
    }
}
