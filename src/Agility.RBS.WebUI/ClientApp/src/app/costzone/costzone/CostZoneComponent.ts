import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ServiceDocument, MessageType, CommonService, MessageResult, FormMode } from "@agility/frameworkcore";
import { FormGroup, FormControl } from "@angular/forms";
import { SharedService } from "../../Common/SharedService";
import { CostZoneModel } from "./CostZoneModel";
import { CostZoneService } from "./CostZoneService";
import { StateDomainModel } from '../../Common/DomainData/StateDomainModel';
import { CountryDomainModel } from '../../Common/DomainData/CountryDomainModel';
import { CurrencyDomainModel } from '../../Common/DomainData/CurrencyDomainModel';
import { CostZoneLocModel } from './CostZoneLocModel';
import { GridOptions } from 'ag-grid-community';
import { CommonModel } from '../../Common/CommonModel';
import { DomainDetailModel } from '../../domain/DomainDetailModel';
import { GridSelectComponent } from '../../Common/grid/GridSelectComponent';
import { GridInputComponent } from '../../Common/grid/GridInputComponent';
import { CostZoneGroupModel } from '../costzonegroup/CostZoneGroupModel';
import { IndicatorComponent } from '../../Common/grid/IndicatorComponent';
import { GridDeleteComponent } from '../../Common/grid/GridDeleteComponent';
import { LocationTypeDomainModel } from '../../Common/DomainData/LocationTypeDomainModel';
import { CostZoneGroupDomainModel } from '../../Common/DomainData/CostZoneGroupDomainModel';
import { GridEditComponent } from '../../Common/grid/GridEditComponent';

@Component({
  selector: "CostZone",
  templateUrl: "./CostZoneComponent.html"
})

export class CostZoneComponent implements OnInit {
  PageTitle = "Cost Zone";
  serviceDocument: ServiceDocument<CostZoneModel>;
  costZoneModel: CostZoneModel;
  localizationData: any;
  domainDtlData: DomainDetailModel[]; defaultStatus: string;
  editMode: boolean = false; editModel: CostZoneModel;
  costZoneGroupmodel: CostZoneGroupModel;
  primaryIndicatorData: CostZoneLocModel[];
  defaultIndicator: string;
  currencyData: CurrencyDomainModel[];
  locationData: CostZoneLocModel[];
  showCustomRequiredMessageforLoc: string;
  public gridOptions: GridOptions;
  columns: any[];
  data: any[] = [];
  sno: number = 0;
  temp: {};
  costLocData: CostZoneLocModel[];
  private messageResult: MessageResult = new MessageResult();
  componentParentName: CostZoneComponent;
  isChecked: boolean = false;
  locationName: string;
  locationSelectedData: CostZoneLocModel[] = [];
  indicatorData: DomainDetailModel[];
  locTypeData: DomainDetailModel[];
  costZoneGroupData: CostZoneGroupDomainModel[];
  gridApi: any;


  constructor(private service: CostZoneService, private sharedService: SharedService, private route: ActivatedRoute
    , private router: Router, private ref: ChangeDetectorRef,private commonService: CommonService) {
  }

  ngOnInit(): void {

    
    this.costZoneGroupData = Object.assign([], this.service.serviceDocument.domainData["costZoneGroup"]);
    this.currencyData = Object.assign([], this.service.serviceDocument.domainData["currency"]);
    this.locationData = Object.assign([], this.service.serviceDocument.domainData["location"]);
    this.indicatorData = Object.assign([], this.service.serviceDocument.domainData["indicatorStatus"]);
    this.locTypeData = Object.assign([], this.service.serviceDocument.domainData["locationsType"]);
    this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);

    this.bind();
    this.componentParentName = this;
  }

  bind(): void {

    this.columns = [

      {
        headerName: "Location", field: "locationId", cellRendererFramework: GridSelectComponent, width: 400,
        cellRendererParams: { references: this.locationData, keyvaluepair: { key: "locationId", value: "locationName" } }
      },
      {
        headerName: "Location Type", field: "locationType", cellRendererFramework: GridSelectComponent, width: 280,
        cellRendererParams: { references: this.locTypeData, keyvaluepair: { key: "code", value: "codeDesc" } }
      },
      { headerName: "Description", field: "description", cellRendererFramework: GridInputComponent, width: 280 },
      //{ headerName: "Action", field: "action", cellRendererFramework: GridDeleteComponent, width: 60 }
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 150
        , cellRendererParams: { restrictEditIcon: true }
      }
    ];

    this.gridOptions = {
      onGridReady: (params: any) => {
        this.gridApi = params.api;
        this.gridApi.startEditingCell({
          rowIndex: 0,
          colKey: "locationId"
        });
      },
      context: {
        componentParent: this
      }
    };
    
    delete this.editModel;
    if (this.sharedService.editObject) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
      this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
      Object.assign(this.service.serviceDocument.dataProfile.dataModel, { operation: "U", zoneGroupId: this.editModel.zoneGroupId.toString() });
      let arr: CostZoneLocModel[] = [];
      this.service.serviceDocument.dataProfile.dataModel.costZoneLocList.forEach((x) => {
        arr.push(Object.assign({}, x));
      });
      this.editModel.costZoneLocList = arr;
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
      this.data = this.service.serviceDocument.dataProfile.dataModel.costZoneLocList;
      delete this.sharedService.editObject;
      this.editMode = true;
    } else {
      this.costZoneModel = {
        zoneGroupId: null, zoneId: null, costZonelocationId: null, description: null, costZoneLocList: [],
        currencyCode: null, baseCostInd: "Y",  operation: "I"
      };
      this.service.serviceDocument.dataProfile.dataModel = this.costZoneModel;
      this.data = this.service.serviceDocument.dataProfile.dataModel.costZoneLocList;
      this.service.newModel(this.costZoneModel);
    }
    this.service.serviceDocument.dataProfile.profileForm = this.commonService
      .getFormGroup(this.service.serviceDocument.dataProfile.dataModel, FormMode.Open);
    this.service.serviceDocument.dataProfile.profileForm.controls["costZoneLocList"] = new FormControl();
    this.serviceDocument = this.service.serviceDocument;
  }

  add($event: MouseEvent): void {
    $event.preventDefault();
    this.sno = this.data.length > 0 ? this.data.slice(0).sort((a: any, b: any) => { return a.codeSeq - b.codeSeq; })
      .reverse()[0].codeSeq : this.sno;
    this.sno = +(this.sno) + 1;
    if (this.data.length === 0) {
      this.service.serviceDocument.dataProfile.dataModel.costZoneLocList = [];
    }
    if (this.temp && Object.keys(this.temp).filter((key: string) => {
      return key !== "organizationId" && key !== "workflowInstance" && key !== "workflowInstanceId" && key !== "workflowStateId" && key !== "tenantId";
    }).map(key => this.temp[key]).some((val: any) => { return val === "" || val === null; })) {
      this.sharedService.errorForm("Please provide all details");
    } else {
      if (this.data.length === 0) {
        this.data.push({
          "locationId": null, "locationType": null,"description":null, "operation": "I"
        });
        this.gridApi.setRowData(this.data);
      } else {
        this.data.push({
          "locationId": null, "locationType": null, "description": null, "operation": "I"
        });
        this.gridApi.setRowData(this.data);
      }
    }
  }
 

  change(cell: any, event: any): void {
    
    this.temp = this.data[cell.rowIndex];
    this.gridOptions.api.setRowData(this.data);
  }

  delete(cell: any): void {    
      this.data.splice(cell.rowIndex, 1);  
    this.gridApi.setRowData(this.data);
  } 

  select(cell: any, event: any): void {

    this.data[cell.rowIndex][cell.column.colId] = event.target.value;
    this.temp = this.data[cell.rowIndex];  

  }
  reset(): void {
    if (!this.editMode) {
      this.sno = 0;
      this.costZoneModel = {
        zoneGroupId: null,  zoneId: null, costZonelocationId: null, description: null, costZoneLocList: [],
        currencyCode: null, baseCostInd: "Y", operation: "I"
      };
      this.temp = {};
      this.data = [];
      this.showCustomRequiredMessageforLoc = null;
      this.service.serviceDocument.dataProfile.dataModel = this.costZoneModel;
      this.data = this.service.serviceDocument.dataProfile.dataModel.costZoneLocList;
      this.service.newModel(this.costZoneModel);
    } else {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);
      Object.assign(this.service.serviceDocument.dataProfile.dataModel, { zoneGroupId: this.editModel.zoneGroupId.toString() });
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
      let arr: CostZoneLocModel[] = [];
      this.editModel.costZoneLocList.forEach((x) => {
        arr.push(Object.assign({}, x));
      });
      this.service.serviceDocument.dataProfile.dataModel.costZoneLocList = arr;
      this.data = this.service.serviceDocument.dataProfile.dataModel.costZoneLocList;
    }
    this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
  }

  save(saveOnly: boolean): void {
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.saveSubscription(saveOnly);
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  saveAndContinue(): void {
    this.save(false);
  }

  saveSubscription(saveOnly: boolean): void {
    if (this.data == null || this.data.length === 0) {
      this.showCustomRequiredMessageforLoc = "Atleast 1 Location detail is required";
    } else {     
      let valid: boolean = true;
      let emptyProperties: string = "<br/><ul>";
      this.data.forEach((item: CostZoneLocModel) => {
        Object.keys(item).forEach((prop: string) => {
          if (prop !== "organizationId" && prop !== "workflowInstance" && prop !== "workflowInstanceId" && prop !== "workflowStateId"
            && prop !== "tenantId" && prop !== "error") {
            if (item[prop] === "" || item[prop] === null ) {
              emptyProperties += emptyProperties.indexOf(prop.toUpperCase()) > -1 ? "" : "<li>" + prop.toUpperCase() + "</li>";
              valid = false;
            }
          }
        });
        emptyProperties += "</ul>";
      });
      if (valid) {
        this.saveCostZone(saveOnly);
        this.showCustomRequiredMessageforLoc = null;
      }
      if (!valid) {
        this.messageResult.message = "Please provide the following details" + emptyProperties.slice(0, -1);
        this.sharedService.errorForm(this.messageResult.message);
      }
    }
  }

  saveCostZone(saveOnly: boolean): void {
    this.serviceDocument.dataProfile.profileForm.controls["costZoneLocList"] = new FormControl();
    this.serviceDocument.dataProfile.profileForm.controls["costZoneLocList"].setValue(this.data);
    this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.sharedService.saveForm(this.localizationData.costZone.costzonesave).subscribe(() => {
          if (saveOnly) {
            this.router.navigate(["/CostZone/List"], { skipLocationChange: true });
          } else {
            if (!this.editMode) {
              this.costZoneModel = {
                zoneGroupId: null, zoneId: null, costZonelocationId: null, description: null, costZoneLocList: [],
                currencyCode: null, baseCostInd: "Y", operation: "I"
              };
              this.service.serviceDocument.dataProfile.dataModel = this.costZoneModel;
              this.data = this.service.serviceDocument.dataProfile.dataModel.costZoneLocList;
              this.service.newModel(this.costZoneModel);
            } else {
              Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
              this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.editModel);
              this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            }
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            this.serviceDocument = this.service.serviceDocument;
          }
        }, (error: string) => { console.log(error); });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
    }, (error: string) => { console.log(error); });
  } 
}
