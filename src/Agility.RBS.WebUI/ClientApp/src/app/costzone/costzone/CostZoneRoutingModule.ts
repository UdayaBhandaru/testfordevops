import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ComanyListComponent } from "./CostZoneListComponent";
import { CostZoneListResolver, CostZoneResolver} from "./CostZoneResolver";
import { CostZoneComponent } from "./CostZoneComponent";
const UtilityRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: ComanyListComponent,
                resolve:
                {
                    serviceDocument: CostZoneListResolver
                }
            },
            {
                path: "New",
                component: CostZoneComponent,
                resolve:
                {
                    serviceDocument: CostZoneResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(UtilityRoutes)
    ]
})
export class CostZoneRoutingModule { }
