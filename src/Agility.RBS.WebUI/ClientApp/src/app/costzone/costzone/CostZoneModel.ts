import { CostZoneLocModel } from './CostZoneLocModel';

export class CostZoneModel {   
  zoneGroupId?: number;
  zoneId?: number;
  description: string;
  currencyCode: string;
  baseCostInd: string;
  costZonelocationId?: number; 
  costZoneLocList?:
  CostZoneLocModel[];
  operation?: string;
  tableName?: string;
}
