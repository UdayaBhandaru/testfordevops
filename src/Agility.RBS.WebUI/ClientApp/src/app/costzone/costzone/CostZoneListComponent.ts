import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { GridOptions } from "ag-grid-community";
import { GridEditComponent } from "../../Common/grid/GridEditComponent";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { InfoComponent } from "../../Common/InfoComponent";
import { CostZoneModel } from "./CostZoneModel";
import { CostZoneService } from "./CostZoneService";
import { SharedService } from "../../Common/SharedService";
import { GridExportModel } from "../../Common/grid/GridExportModel";
import { CostZoneLocModel } from './CostZoneLocModel';
import { CurrencyDomainModel } from '../../Common/DomainData/CurrencyDomainModel';
import { CommonModel } from '../../Common/CommonModel';
import { CostZoneGroupDomainModel } from '../../Common/DomainData/CostZoneGroupDomainModel';

@Component({
  selector: "CostZone-list",
  templateUrl: "./CostZoneListComponent.html"
})
export class ComanyListComponent implements OnInit {
  PageTitle = "Cost Zone";
  redirectUrl = "CostZone";
  componentName: any;
  serviceDocument: ServiceDocument<CostZoneModel>;
  public gridOptions: GridOptions;
  columns: any[];
  model: CostZoneModel;
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  domainDtlData: DomainDetailModel[];
  locationData: CostZoneLocModel[];
  locationSelectedData: CostZoneLocModel[] = []; 
  primaryIndicatorData: DomainDetailModel[];
  currencyData: CurrencyDomainModel[];
  costZoneGroupData: CostZoneGroupDomainModel[];

  constructor(public service: CostZoneService, private sharedService: SharedService, private route: ActivatedRoute,
    private router: Router) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      //{ headerName: "Zone Group", field: "zoneGroupDesc", tooltipField: "zoneGroupDesc", width: 40 },
      { headerName: "Zone Id", field: "zoneId", tooltipField: "zoneId", width: 30 },
      { headerName: "Description", field: "description", tooltipField: "description" },
      { headerName: "Currency Code", field: "currencyCode", tooltipField: "currencyCode", width: 45 },     
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 35
      }
    ];
    this.route.data.subscribe(() => {
      this.loadCostZoneDomainData();
      this.setZopneModel();
      if (this.sharedService.searchData) {
        this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData, this.sharedService.searchData.Zone);
        this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
      } else {
        this.service.newModel(this.model);
      }
      this.serviceDocument = this.service.serviceDocument;
    });
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }

  private loadCostZoneDomainData(): void {
    this.currencyData = Object.assign([], this.service.serviceDocument.domainData["currency"]);
    this.costZoneGroupData = Object.assign([], this.service.serviceDocument.domainData["costZoneGroup"]);
    //this.domainDtlData = this.service.serviceDocument.domainData["status"];
    //this.primaryIndicatorData = this.service.serviceDocument.domainData["indicator"];
    //this.locationData = Object.assign([], this.service.serviceDocument.domainData["location"]);
    //this.costZoneGroupData = Object.assign([], this.service.serviceDocument.domainData["costZonegroup"]);
    this.sharedService.domainData = {
      costZoneStatusDomainData: this.domainDtlData, costZoneGrpDrdDomainData: this.costZoneGroupData
      , costZoneLocDrdDomainData: this.locationData
    };
  }

  private setZopneModel(): void {
    this.model = new CostZoneModel();
    this.model = {
      zoneGroupId: null, zoneId: null, costZonelocationId: null, description: null, costZoneLocList: [],
      currencyCode: null, baseCostInd: null,operation: null
    };
  }

  cleanUp(): void {
    this.locationSelectedData = [];
  }
}
