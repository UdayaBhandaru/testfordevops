export class CostZoneLocModel {
    zoneGroupId?: number;
    zoneId?: number;
    locationId?: number;
    locationName: string;
    locationType: string;
    operation?: string;
    tableName?: string;
    error?: string;
}
