import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { ComanyListComponent } from "./CostZoneListComponent";
import { CostZoneService } from "./CostZoneService";
import { CostZoneRoutingModule } from "./CostZoneRoutingModule";
import { CostZoneComponent } from "./CostZoneComponent";
import { CostZoneListResolver, CostZoneResolver } from "./CostZoneResolver";
import { RbsSharedModule } from "../../RbsSharedModule";

@NgModule({
    imports: [
        FrameworkCoreModule,
        CostZoneRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        ComanyListComponent,
        CostZoneComponent,
    ],
    providers: [
        CostZoneService,
        CostZoneListResolver,
        CostZoneResolver,
    ]
})
export class CostZoneModule {
    constructor() {
        LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
    }
}
