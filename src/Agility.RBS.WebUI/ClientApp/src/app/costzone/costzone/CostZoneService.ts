import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { CostZoneModel } from "./CostZoneModel";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";

@Injectable()
export class CostZoneService {
    serviceDocument: ServiceDocument<CostZoneModel> = new ServiceDocument<CostZoneModel>();
    constructor(private httpHelperService: HttpHelperService) { }

    newModel(model: CostZoneModel): ServiceDocument<CostZoneModel> {
        return this.serviceDocument.newModel(model);
    }

    search(): Observable<ServiceDocument<CostZoneModel>> {
      return this.serviceDocument.search("/api/CostZone/Search");
    }

    save(): Observable<ServiceDocument<CostZoneModel>> {
      return this.serviceDocument.save("/api/CostZone/Save", true);
    }

    list(): Observable<ServiceDocument<CostZoneModel>> {
      return this.serviceDocument.list("/api/CostZone/List");
    }

    addEdit(): Observable<ServiceDocument<CostZoneModel>> {
      return this.serviceDocument.list("/api/CostZone/New");
    }

  isExistingCostZone(zoneGroupId: string): Observable<boolean> {
    return this.httpHelperService.get("/api/CostZone/IsExistingCostZone", new HttpParams().set("zoneGroupId", zoneGroupId));
    }
}
