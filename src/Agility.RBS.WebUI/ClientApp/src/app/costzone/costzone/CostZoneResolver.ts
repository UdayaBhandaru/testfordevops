import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { CostZoneService } from "./CostZoneService";
import { CostZoneModel } from "./CostZoneModel";
@Injectable()
export class CostZoneListResolver implements Resolve<ServiceDocument<CostZoneModel>> {
    constructor(private service: CostZoneService) { }
    resolve(): Observable<ServiceDocument<CostZoneModel>> {
        return this.service.list();
    }
}

@Injectable()
export class CostZoneResolver implements Resolve<ServiceDocument<CostZoneModel>> {
    constructor(private service: CostZoneService) { }
    resolve(): Observable<ServiceDocument<CostZoneModel>> {
        return this.service.addEdit();
    }
}



