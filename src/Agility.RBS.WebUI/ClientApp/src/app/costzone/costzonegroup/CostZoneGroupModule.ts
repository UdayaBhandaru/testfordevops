import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { ComanyListComponent } from "./CostZoneGroupListComponent";
import { CostZoneGroupService } from "./CostZoneGroupService";
import { CostZoneGroupRoutingModule } from "./CostZoneGroupRoutingModule";
import { CostZoneGroupComponent } from "./CostZoneGroupComponent";
import { CostZoneGroupListResolver, CostZoneGroupResolver } from "./CostZoneGroupResolver";
import { RbsSharedModule } from "../../RbsSharedModule";

@NgModule({
    imports: [
        FrameworkCoreModule,
        CostZoneGroupRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        ComanyListComponent,
        CostZoneGroupComponent,
    ],
    providers: [
        CostZoneGroupService,
        CostZoneGroupListResolver,
        CostZoneGroupResolver,
    ]
})
export class CostZoneGroupModule {
    constructor() {
        LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
    }
}
