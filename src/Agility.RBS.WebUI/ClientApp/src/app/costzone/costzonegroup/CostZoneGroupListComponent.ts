import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { GridOptions } from "ag-grid-community";
import { GridEditComponent } from "../../Common/grid/GridEditComponent";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { InfoComponent } from "../../Common/InfoComponent";
import { CostZoneGroupModel } from "./CostZoneGroupModel";
import { CostZoneGroupService } from "./CostZoneGroupService";
import { SharedService } from "../../Common/SharedService";
import { GridExportModel } from "../../Common/grid/GridExportModel";

@Component({
  selector: "costzonegroup-list",
  templateUrl: "./CostZoneGroupListComponent.html"
})
export class ComanyListComponent implements OnInit {
  PageTitle = "Cost Zone Group";
  redirectUrl = "CostZoneGroup";
  componentName: any;
  serviceDocument: ServiceDocument<CostZoneGroupModel>;
  public gridOptions: GridOptions;
  columns: any[];
  model: CostZoneGroupModel;
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;

  constructor(public service: CostZoneGroupService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "Zone Group Id", field: "zoneGroupId", tooltipField: "zoneGroupId", width: +60 },
      { headerName: "Description", field: "description", tooltipField: "description", width: 60 },
      { headerName: "Cost Level Desc", field: "costLevelDesc", tooltipField: "costLevelDesc" },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 60
      }
    ];
    
    this.model = { zoneGroupId: null, description: null };

    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }
}
