import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { CostZoneGroupService } from "./CostZoneGroupService";
import { CostZoneGroupModel } from "./CostZoneGroupModel";
@Injectable()
export class CostZoneGroupListResolver implements Resolve<ServiceDocument<CostZoneGroupModel>> {
    constructor(private service: CostZoneGroupService) { }
    resolve(): Observable<ServiceDocument<CostZoneGroupModel>> {
        return this.service.list();
    }
}

@Injectable()
export class CostZoneGroupResolver implements Resolve<ServiceDocument<CostZoneGroupModel>> {
    constructor(private service: CostZoneGroupService) { }
    resolve(): Observable<ServiceDocument<CostZoneGroupModel>> {
        return this.service.addEdit();
    }
}



