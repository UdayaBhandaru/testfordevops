import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ComanyListComponent } from "./CostZoneGroupListComponent";
import { CostZoneGroupListResolver, CostZoneGroupResolver} from "./CostZoneGroupResolver";
import { CostZoneGroupComponent } from "./CostZoneGroupComponent";
const UtilityRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: ComanyListComponent,
                resolve:
                {
                    serviceDocument: CostZoneGroupListResolver
                }
            },
            {
                path: "New",
                component: CostZoneGroupComponent,
                resolve:
                {
                    serviceDocument: CostZoneGroupResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(UtilityRoutes)
    ]
})
export class CostZoneGroupRoutingModule { }
