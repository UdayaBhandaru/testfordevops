import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { CostZoneGroupModel } from "./CostZoneGroupModel";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";

@Injectable()
export class CostZoneGroupService {
    serviceDocument: ServiceDocument<CostZoneGroupModel> = new ServiceDocument<CostZoneGroupModel>();
    constructor(private httpHelperService: HttpHelperService) { }

    newModel(model: CostZoneGroupModel): ServiceDocument<CostZoneGroupModel> {
        return this.serviceDocument.newModel(model);
    }

    search(): Observable<ServiceDocument<CostZoneGroupModel>> {
      return this.serviceDocument.search("/api/CostZoneGroup/Search");
    }

    save(): Observable<ServiceDocument<CostZoneGroupModel>> {
      return this.serviceDocument.save("/api/CostZoneGroup/Save", true);
    }

    list(): Observable<ServiceDocument<CostZoneGroupModel>> {
      return this.serviceDocument.list("/api/CostZoneGroup/List");
    }

    addEdit(): Observable<ServiceDocument<CostZoneGroupModel>> {
      return this.serviceDocument.list("/api/CostZoneGroup/New");
    }

  isExistingCostZoneGroup(description: string): Observable<boolean> {
    return this.httpHelperService.get("/api/CostZoneGroup/IsExistingCostZoneGroup", new HttpParams().set("description", description));
    }
}
