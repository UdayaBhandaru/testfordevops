import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { SharedService } from "../../Common/SharedService";
import { CostZoneGroupModel } from "./CostZoneGroupModel";
import { CostZoneGroupService } from "./CostZoneGroupService";
import { StateDomainModel } from '../../Common/DomainData/StateDomainModel';
import { CountryDomainModel } from '../../Common/DomainData/CountryDomainModel';
import { CurrencyDomainModel } from '../../Common/DomainData/CurrencyDomainModel';
import { CostLevelModel } from '../../Common/DomainData/CostLevelModel';

@Component({
  selector: "CostZoneGroup",
  templateUrl: "./CostZoneGroupComponent.html"
})

export class CostZoneGroupComponent implements OnInit {
  PageTitle = "Cost Zone Group";
  redirectUrl = "CostZoneGroup";
    serviceDocument: ServiceDocument<CostZoneGroupModel>;
    editMode: boolean = false;
    editModel: any;
    localizationData: any;  
    model: CostZoneGroupModel;  
  countryDomainData: CountryDomainModel[];
  cityDomainData: StateDomainModel[];
  currencyData: CurrencyDomainModel[];
  costLevelModel: CostLevelModel[];

    constructor(private service: CostZoneGroupService, private sharedService: SharedService, private router: Router
        , private ref: ChangeDetectorRef) {
    }

    ngOnInit(): void {
      this.countryDomainData = Object.assign([], this.service.serviceDocument.domainData["country"]);

      this.currencyData = Object.assign([], this.service.serviceDocument.domainData["currency"]);
      this.costLevelModel = Object.assign([], this.service.serviceDocument.domainData["costLevel"]);

        this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
        this.bind();
    }

    bind(): void {
        delete this.editModel;
        if (this.sharedService.editObject) {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);         
          Object.assign(this.service.serviceDocument.dataProfile.dataModel, { operation: "U"});
            this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            delete this.sharedService.editObject;
            this.editMode = true;

        } else {            
          this.model = { zoneGroupId: null, costLevel: null, costLevelDesc: null, description: null, operation: "I"};
           
            this.service.serviceDocument.dataProfile.dataModel = this.model;
            this.service.newModel(this.model);
        }
        this.serviceDocument = this.service.serviceDocument;
    }

    save(saveOnly: boolean): void {
        if (this.serviceDocument.dataProfile.profileForm.valid) {
            this.saveSubscription(saveOnly);
        } else {
            this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
        }
    }

    saveSubscription(saveOnly: boolean): void {
        this.checkCostZoneGroup(saveOnly);
  }

    saveCostZoneGroup(saveOnly: boolean): void {
        this.service.save().subscribe(() => {
            if (this.service.serviceDocument.result.type === MessageType.success) {
                this.sharedService.saveForm("CostZoneGroup saved successfully.").subscribe(() => {
                    if (saveOnly) {
                        this.router.navigate(["/" + this.redirectUrl + "/List"], { skipLocationChange: true });
                    } else {
                        if (!this.editMode) {
                          this.model = { zoneGroupId: null, costLevel: null, costLevelDesc: null, description:null, operation: "I" };                            
                            this.service.newModel(this.model);
                        } else {
                            Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
                            this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.service.serviceDocument.dataProfile.dataModel);
                            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
                        }
                    }
                }, (error: string) => { console.log(error); });
            } else {
                this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
            }
        }, (error: string) => { console.log(error); });
    }

    checkCostZoneGroup(saveOnly: boolean): void {
        if (!this.editMode) {
          this.service.isExistingCostZoneGroup(this.serviceDocument.dataProfile.profileForm.controls["description"].value).subscribe((response: any) => {
                if (response) {
                    this.sharedService.errorForm("CostZoneGroup already exisit.");
                } else {
                    this.saveCostZoneGroup(saveOnly);
                }
            });
        } else {
            this.saveCostZoneGroup(saveOnly);
        }
    }

    reset(): void {
        if (!this.editMode) {
            this.model = {
              zoneGroupId: null, costLevel: null, costLevelDesc: null, description: null, operation: "I"
            };
           
            this.service.serviceDocument.dataProfile.dataModel = this.model;
            this.service.newModel(this.model);

        } else {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);            
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
        }
        this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
    }
}
