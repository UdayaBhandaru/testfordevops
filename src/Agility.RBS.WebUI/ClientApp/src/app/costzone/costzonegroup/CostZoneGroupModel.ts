export class CostZoneGroupModel { 
  zoneGroupId: number;
  costLevel?: string;
  costLevelDesc?: string;
  description: string;
  operation?: string;
}
