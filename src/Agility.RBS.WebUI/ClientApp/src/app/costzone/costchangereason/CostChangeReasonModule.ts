import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { ComanyListComponent } from "./CostChangeReasonListComponent";
import { CostChangeReasonService } from "./CostChangeReasonService";
import { CostChangeReasonRoutingModule } from "./CostChangeReasonRoutingModule";
import { CostChangeReasonComponent } from "./CostChangeReasonComponent";
import { CostChangeReasonListResolver, CostChangeReasonResolver } from "./CostChangeReasonResolver";
import { RbsSharedModule } from "../../RbsSharedModule";

@NgModule({
    imports: [
        FrameworkCoreModule,
        CostChangeReasonRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        ComanyListComponent,
        CostChangeReasonComponent,
    ],
    providers: [
        CostChangeReasonService,
        CostChangeReasonListResolver,
        CostChangeReasonResolver,
    ]
})
export class CostChangeReasonModule {
    constructor() {
        LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
    }
}
