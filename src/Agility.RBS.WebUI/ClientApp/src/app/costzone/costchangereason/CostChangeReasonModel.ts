export class CostChangeReasonModel { 
  reason: number;
  reasonDesc: string;
  operation?: string;
}
