import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { GridOptions } from "ag-grid-community";
import { GridEditComponent } from "../../Common/grid/GridEditComponent";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { InfoComponent } from "../../Common/InfoComponent";
import { CostChangeReasonModel } from "./CostChangeReasonModel";
import { CostChangeReasonService } from "./CostChangeReasonService";
import { SharedService } from "../../Common/SharedService";
import { GridExportModel } from "../../Common/grid/GridExportModel";

@Component({
  selector: "costchangereason-list",
  templateUrl: "./CostChangeReasonListComponent.html"
})
export class ComanyListComponent implements OnInit {
  PageTitle = "CostChangeReason";
  redirectUrl = "CostChangeReason";
  componentName: any;
  serviceDocument: ServiceDocument<CostChangeReasonModel>;
  public gridOptions: GridOptions;
  columns: any[];
  model: CostChangeReasonModel;
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;

  constructor(public service: CostChangeReasonService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "Reason Id", field: "reason", tooltipField: "reason", width: 30 },
      { headerName: "Description", field: "reasonDesc", tooltipField: "reasonDesc" },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 80
      }
    ];
    
    this.model = { reason: null, reasonDesc: null };

    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }
}
