import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { CostChangeReasonService } from "./CostChangeReasonService";
import { CostChangeReasonModel } from "./CostChangeReasonModel";
@Injectable()
export class CostChangeReasonListResolver implements Resolve<ServiceDocument<CostChangeReasonModel>> {
    constructor(private service: CostChangeReasonService) { }
    resolve(): Observable<ServiceDocument<CostChangeReasonModel>> {
        return this.service.list();
    }
}

@Injectable()
export class CostChangeReasonResolver implements Resolve<ServiceDocument<CostChangeReasonModel>> {
    constructor(private service: CostChangeReasonService) { }
    resolve(): Observable<ServiceDocument<CostChangeReasonModel>> {
        return this.service.addEdit();
    }
}



