import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ComanyListComponent } from "./CostChangeReasonListComponent";
import { CostChangeReasonListResolver, CostChangeReasonResolver} from "./CostChangeReasonResolver";
import { CostChangeReasonComponent } from "./CostChangeReasonComponent";
const UtilityRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: ComanyListComponent,
                resolve:
                {
                    serviceDocument: CostChangeReasonListResolver
                }
            },
            {
                path: "New",
                component: CostChangeReasonComponent,
                resolve:
                {
                    serviceDocument: CostChangeReasonResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(UtilityRoutes)
    ]
})
export class CostChangeReasonRoutingModule { }
