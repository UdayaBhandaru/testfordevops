import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { CostChangeReasonModel } from "./CostChangeReasonModel";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";

@Injectable()
export class CostChangeReasonService {
    serviceDocument: ServiceDocument<CostChangeReasonModel> = new ServiceDocument<CostChangeReasonModel>();
    constructor(private httpHelperService: HttpHelperService) { }

    newModel(model: CostChangeReasonModel): ServiceDocument<CostChangeReasonModel> {
        return this.serviceDocument.newModel(model);
    }

    search(): Observable<ServiceDocument<CostChangeReasonModel>> {
      return this.serviceDocument.search("/api/CostChangeReason/Search");
    }

    save(): Observable<ServiceDocument<CostChangeReasonModel>> {
      return this.serviceDocument.save("/api/CostChangeReason/Save", true);
    }

    list(): Observable<ServiceDocument<CostChangeReasonModel>> {
      return this.serviceDocument.list("/api/CostChangeReason/List");
    }

    addEdit(): Observable<ServiceDocument<CostChangeReasonModel>> {
      return this.serviceDocument.list("/api/CostChangeReason/New");
    }

  isExistingCostChangeReason(reasonDesc: string): Observable<boolean> {
    return this.httpHelperService.get("/api/CostChangeReason/IsExistingCostChangeReason", new HttpParams().set("reasonDesc", reasonDesc));
    }
}
