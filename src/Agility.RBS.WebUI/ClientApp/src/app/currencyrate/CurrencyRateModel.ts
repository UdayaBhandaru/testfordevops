export class CurrencyRateModel { 
  currencyCode: string;
  currencyDesc: string;
  effectiveDate: Date;
  exchangeType: string;
  exchangeRate?:number
  operation?: string;
}
