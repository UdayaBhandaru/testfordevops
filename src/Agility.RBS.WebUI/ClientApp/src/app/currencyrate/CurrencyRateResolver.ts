import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { CurrencyRateService } from "./CurrencyRateService";
import { CurrencyRateModel } from "./CurrencyRateModel";
@Injectable()
export class CurrencyRateListResolver implements Resolve<ServiceDocument<CurrencyRateModel>> {
    constructor(private service: CurrencyRateService) { }
    resolve(): Observable<ServiceDocument<CurrencyRateModel>> {
        return this.service.list();
    }
}

@Injectable()
export class CurrencyRateResolver implements Resolve<ServiceDocument<CurrencyRateModel>> {
    constructor(private service: CurrencyRateService) { }
    resolve(): Observable<ServiceDocument<CurrencyRateModel>> {
        return this.service.addEdit();
    }
}



