import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CurrencyRateListComponent } from "./CurrencyRateListComponent";
import { CurrencyRateListResolver, CurrencyRateResolver} from "./CurrencyRateResolver";
import { CurrencyRateComponent } from "./CurrencyRateComponent";
const UtilityRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
              component: CurrencyRateListComponent,
                resolve:
                {
                    serviceDocument: CurrencyRateListResolver
                }
            },
            {
                path: "New",
                component: CurrencyRateComponent,
                resolve:
                {
                    serviceDocument: CurrencyRateResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(UtilityRoutes)
    ]
})
export class CurrencyRateRoutingModule { }
