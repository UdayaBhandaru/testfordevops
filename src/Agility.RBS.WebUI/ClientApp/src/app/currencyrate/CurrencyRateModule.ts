import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { CurrencyRateListComponent } from "./CurrencyRateListComponent";
import { CurrencyRateService } from "./CurrencyRateService";
import { CurrencyRateRoutingModule } from "./CurrencyRateRoutingModule";
import { CurrencyRateComponent } from "./CurrencyRateComponent";
import { CurrencyRateListResolver, CurrencyRateResolver } from "./CurrencyRateResolver";
import { RbsSharedModule } from '../RbsSharedModule';
import { AgGridSharedModule } from '../AgGridSharedModule';
import { TitleSharedModule } from '../TitleSharedModule';

@NgModule({
    imports: [
        FrameworkCoreModule,
        CurrencyRateRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        CurrencyRateListComponent,
        CurrencyRateComponent,
    ],
    providers: [
        CurrencyRateService,
        CurrencyRateListResolver,
        CurrencyRateResolver,
    ]
})
export class CurrencyRateModule {
    constructor() {
        LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
    }
}
