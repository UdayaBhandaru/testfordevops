import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { CurrencyRateModel } from "./CurrencyRateModel";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from '../Common/HttpHelperService';

@Injectable()
export class CurrencyRateService {
    serviceDocument: ServiceDocument<CurrencyRateModel> = new ServiceDocument<CurrencyRateModel>();
    constructor(private httpHelperService: HttpHelperService) { }

    newModel(model: CurrencyRateModel): ServiceDocument<CurrencyRateModel> {
        return this.serviceDocument.newModel(model);
    }

    search(): Observable<ServiceDocument<CurrencyRateModel>> {
      return this.serviceDocument.search("/api/CurrencyRate/Search");
    }

    save(): Observable<ServiceDocument<CurrencyRateModel>> {
      return this.serviceDocument.save("/api/CurrencyRate/Save", true);
    }

    list(): Observable<ServiceDocument<CurrencyRateModel>> {
      return this.serviceDocument.list("/api/CurrencyRate/List");
    }

    addEdit(): Observable<ServiceDocument<CurrencyRateModel>> {
      return this.serviceDocument.list("/api/CurrencyRate/New");
    }
  isExistingCurrencyRate(currencyCode: number): Observable<boolean> {
    return this.httpHelperService.get("/api/CurrencyRate/IsExistingCurrencyRate", new HttpParams().set("currencyCode", currencyCode.toString()));
    }
}
