import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { GridOptions } from "ag-grid-community";
import { CurrencyRateModel } from './CurrencyRateModel';
import { GridExportModel } from '../Common/grid/GridExportModel';
import { CurrencyRateService } from './CurrencyRateService';
import { SharedService } from '../Common/SharedService';
import { GridEditComponent } from '../Common/grid/GridEditComponent';
import { InfoComponent } from '../Common/InfoComponent';
import { CurrencyDomainModel } from '../Common/DomainData/CurrencyDomainModel';
@Component({
  selector: "CurrencyRate-list",
  templateUrl: "./CurrencyRateListComponent.html"
})
export class CurrencyRateListComponent implements OnInit {
  PageTitle = "Currency Rate";
  redirectUrl = "CurrencyRate";
  componentName: any;
  serviceDocument: ServiceDocument<CurrencyRateModel>;
  public gridOptions: GridOptions;
  columns: any[];
  model: CurrencyRateModel;
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  currencyData: CurrencyDomainModel[];

  constructor(public service: CurrencyRateService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.currencyData = Object.assign([], this.service.serviceDocument.domainData["currency"]);
    this.componentName = this;
    this.columns = [
      { headerName: "currency Code", field: "currencyCode", tooltipField: "currencyCode" },
      { headerName: "currencyDesc", field: "currencyDesc", tooltipField: "currencyDesc" },
      { headerName: "effective Date", field: "effectiveDate", tooltipField: "effectiveDate" },
      { headerName: "exchange Type", field: "exchangeType", tooltipField: "exchangeType" },
      { headerName: "exchange Rate", field: "exchangeRate", tooltipField: "exchangeRate" },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent
      }
    ];
    
    this.model = { currencyCode: null, currencyDesc: null, effectiveDate: null, exchangeType: null, exchangeRate: null, operation: "I" };

    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }
}
