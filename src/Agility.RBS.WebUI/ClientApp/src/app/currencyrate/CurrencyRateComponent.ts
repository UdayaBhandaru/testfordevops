import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { CurrencyRateModel } from './CurrencyRateModel';
import { CurrencyRateService } from './CurrencyRateService';
import { SharedService } from '../Common/SharedService';
import { CurrencyDomainModel } from "../Common/DomainData/CurrencyDomainModel";
import { DomainDetailModel } from '../domain/DomainDetailModel';
import { CurrencyModel } from '../currency/CurrencyModel';
@Component({
  selector: "CurrencyRate",
  templateUrl: "./CurrencyRateComponent.html"
})

export class CurrencyRateComponent implements OnInit {
  PageTitle = "Currency Rate";
  redirectUrl = "CurrencyRate";
  serviceDocument: ServiceDocument<CurrencyRateModel>;
  editMode: boolean = false;
  editModel: any;
  localizationData: any;
  model: CurrencyRateModel;
  currencyData: CurrencyDomainModel[];
  currencyExchTypeData: DomainDetailModel[];
  currencyModel: CurrencyModel;
  constructor(private service: CurrencyRateService, private sharedService: SharedService, private router: Router
    , private ref: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    this.currencyData = Object.assign([], this.service.serviceDocument.domainData["currency"]);
    this.currencyExchTypeData = Object.assign([], this.service.serviceDocument.domainData["currencyExchType"]);
    this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
    this.bind();
  }

  bind(): void {
    delete this.editModel;
    if (this.sharedService.editObject) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
      Object.assign(this.service.serviceDocument.dataProfile.dataModel, { operation: "U" });
      this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
      delete this.sharedService.editObject;
      this.editMode = true;

    } else {
      this.currencyRateModel();
      this.service.serviceDocument.dataProfile.dataModel = this.model;
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  save(saveOnly: boolean): void {
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.saveSubscription(saveOnly);
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  saveSubscription(saveOnly: boolean): void {
    //this.checkCurrencyRate(saveOnly);
    this.saveCurrencyRate(saveOnly);
  }

  saveCurrencyRate(saveOnly: boolean): void {
    this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.sharedService.saveForm("CurrencyRate saved successfully.").subscribe(() => {
          if (saveOnly) {
            this.router.navigate(["/" + this.redirectUrl + "/List"], { skipLocationChange: true });
          } else {
            if (!this.editMode) {
              this.currencyRateModel();
              this.service.newModel(this.model);
            } else {
              Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
              this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.service.serviceDocument.dataProfile.dataModel);
              this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            }
          }
        }, (error: string) => { console.log(error); });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
    }, (error: string) => { console.log(error); });
  }

  checkCurrencyRate(saveOnly: boolean): void {
    if (!this.editMode) {
      this.service.isExistingCurrencyRate(this.serviceDocument.dataProfile.profileForm.controls["currencyCode"].value).subscribe((response: any) => {
        if (response) {
          this.sharedService.errorForm("CurrencyRate already exisit.");
        } else {
          this.saveCurrencyRate(saveOnly);
        }
      });
    } else {
      this.saveCurrencyRate(saveOnly);
    }
  }

  reset(): void {
    if (!this.editMode) {
      this.currencyRateModel();
      this.service.serviceDocument.dataProfile.dataModel = this.model;
      this.service.newModel(this.model);

    } else {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    }
    this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
  }

  private currencyRateModel() {
    this.model = { currencyCode: null, currencyDesc: null, effectiveDate: null, exchangeType: null, exchangeRate: null, operation: "I" };
  }

  openCurrency($event: MouseEvent): void {
    $event.preventDefault();
    this.currencyModel = {
      currencyCode: this.service.serviceDocument.dataProfile.dataModel.currencyCode,
      currencyDesc: null,
      currencyCostFmt: null,
      currencyRtlFmt: null,
      currencyCostDec: null,
      currencyRtlDec: null,
      currencyIsoCode: null,
      currencyIsoNumeric: null,
      currencySymbol: null,
      currencyMajor: null,
      currencyMinor: null,
      currencyDecMark: null,
      currencyTSep: null,
      currencyHtmlCode: null,
      currencyStatus: null,
      currencyStatusDesc: null,
      Error: null,
      Operation: null
    };
    this.sharedService.searchData = Object.assign({}, this.currencyRateModel);
    this.router.navigate(["/Currency"], { skipLocationChange: true });
  }

}
