﻿import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { DomainListComponent } from "./DomainListComponent";
import { DomainResolver } from "./DomainResolver";
import { DomainComponent } from "./DomainComponent";
const DomainRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: DomainListComponent,
                resolve:
                {
                    references: DomainResolver
                }
            },
            {
                path: "New",
                component: DomainComponent,
                resolve:
                {
                    references: DomainResolver
                }
            },
            {
                path: "Edit",
                component: DomainComponent,
                resolve:
                {
                    references: DomainResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(DomainRoutes)
    ]
})
export class DomainRoutingModule { }
