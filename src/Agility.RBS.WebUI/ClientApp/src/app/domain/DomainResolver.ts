﻿import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { DomainHeaderModel } from "./DomainHeaderModel";
import { DomainDetailModel } from "./DomainDetailModel";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { DomainService } from "./DomainService";

@Injectable()
export class DomainResolver implements Resolve<ServiceDocument<DomainHeaderModel>> {
    constructor(private service: DomainService) { }
    resolve(): Observable<ServiceDocument<DomainHeaderModel>> {
        return this.service.list();
    }
}