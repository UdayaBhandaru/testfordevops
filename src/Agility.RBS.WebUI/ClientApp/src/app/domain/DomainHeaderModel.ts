﻿import { DomainDetailModel } from "./DomainDetailModel";

export class DomainHeaderModel {
    domainId?: string;
    domainDesc?: string;
    domainStatus?: string = "A";
    domainStatusDesc?: string;
    domainDetails?: DomainDetailModel[];
    operation?: string;
    tableName?: string;
}