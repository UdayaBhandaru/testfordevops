﻿import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { DomainRoutingModule } from "./DomainRoutingModule";
import { DomainListComponent } from "./DomainListComponent";
import { DomainComponent } from "./DomainComponent";
import { DomainService } from "./DomainService";
import { DomainResolver } from "./DomainResolver";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../AgGridSharedModule";
import { TitleSharedModule } from "../TitleSharedModule";
import { RbsSharedModule } from "../RbsSharedModule";

@NgModule({
    imports: [
        FrameworkCoreModule,
        DomainRoutingModule,
        AgGridModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        DomainListComponent,
        DomainComponent
    ],
    providers: [
        DomainService,
        DomainResolver
    ]
})
export class DomainModule {
}
