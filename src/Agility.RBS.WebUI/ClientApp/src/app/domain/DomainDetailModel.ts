export class DomainDetailModel {
    code?: string;
    codeDesc?: string;
    requiredInd?: string = "0";
    codeSeq?: number;
    codeType?: string;
}
