import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ComanyListComponent } from "./AreaListComponent";
import { AreaListResolver, AreaResolver} from "./AreaResolver";
import { AreaComponent } from "./AreaComponent";
const UtilityRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: ComanyListComponent,
                resolve:
                {
                    serviceDocument: AreaListResolver
                }
            },
            {
                path: "New",
                component: AreaComponent,
                resolve:
                {
                    serviceDocument: AreaResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(UtilityRoutes)
    ]
})
export class AreaRoutingModule { }
