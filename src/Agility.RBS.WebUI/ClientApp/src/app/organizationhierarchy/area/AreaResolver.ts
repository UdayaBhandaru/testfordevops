import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { AreaService } from "./AreaService";
import { AreaModel } from "./AreaModel";
@Injectable()
export class AreaListResolver implements Resolve<ServiceDocument<AreaModel>> {
    constructor(private service: AreaService) { }
    resolve(): Observable<ServiceDocument<AreaModel>> {
        return this.service.list();
    }
}

@Injectable()
export class AreaResolver implements Resolve<ServiceDocument<AreaModel>> {
    constructor(private service: AreaService) { }
    resolve(): Observable<ServiceDocument<AreaModel>> {
        return this.service.addEdit();
    }
}



