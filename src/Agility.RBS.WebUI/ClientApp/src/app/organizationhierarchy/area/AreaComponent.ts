import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { SharedService } from "../../Common/SharedService";
import { AreaModel } from "./AreaModel";
import { AreaService } from "./AreaService";
import { StateDomainModel } from '../../Common/DomainData/StateDomainModel';
import { CountryDomainModel } from '../../Common/DomainData/CountryDomainModel';
import { ChainDomainModel } from '../../Common/DomainData/ChainDomainModel';
import { CurrencyDomainModel } from '../../Common/DomainData/CurrencyDomainModel';

@Component({
  selector: "area",
  templateUrl: "./AreaComponent.html"
})

export class AreaComponent implements OnInit {
  PageTitle = "Area";
  redirectUrl = "Area";
    serviceDocument: ServiceDocument<AreaModel>;
    editMode: boolean = false;
    editModel: any;
    localizationData: any;  
    model: AreaModel;  
  chainDomainData: ChainDomainModel[]; 
  cityDomainData: StateDomainModel[];
  currencyData: CurrencyDomainModel[];

    constructor(private service: AreaService, private sharedService: SharedService, private router: Router
        , private ref: ChangeDetectorRef) {
    }

    ngOnInit(): void {
      this.chainDomainData = Object.assign([], this.service.serviceDocument.domainData["chain"]);
      this.currencyData = Object.assign([], this.service.serviceDocument.domainData["currency"]);
        this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
        this.bind();
    }

    bind(): void {
        delete this.editModel;
        if (this.sharedService.editObject) {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);         
            Object.assign(this.service.serviceDocument.dataProfile.dataModel, { operation: "U" });
            this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            delete this.sharedService.editObject;
            this.editMode = true;

        } else {            
            this.model = {
              area: null, areaName: null, mgrName: null, chain: null, chainName: null, currencyCode: null, operation: "I"
            };
           
            this.service.serviceDocument.dataProfile.dataModel = this.model;
            this.service.newModel(this.model);
        }
        this.serviceDocument = this.service.serviceDocument;
    }

    save(saveOnly: boolean): void {
        if (this.serviceDocument.dataProfile.profileForm.valid) {
            this.saveSubscription(saveOnly);
        } else {
            this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
        }
    }

    saveSubscription(saveOnly: boolean): void {
        this.checkArea(saveOnly);
  }

    saveArea(saveOnly: boolean): void {
        this.service.save().subscribe(() => {
            if (this.service.serviceDocument.result.type === MessageType.success) {
                this.sharedService.saveForm("Area saved successfully").subscribe(() => {
                    if (saveOnly) {
                        this.router.navigate(["/" + this.redirectUrl + "/List"], { skipLocationChange: true });
                    } else {
                        if (!this.editMode) {
                            this.model = {
                              area: null, areaName: null, mgrName: null, chain: null, chainName: null, currencyCode: null, operation: "I"
                            };                            
                            this.service.newModel(this.model);
                        } else {
                            Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
                            this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.service.serviceDocument.dataProfile.dataModel);
                            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
                        }
                    }
                }, (error: string) => { console.log(error); });
            } else {
                this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
            }
        }, (error: string) => { console.log(error); });
    }

    checkArea(saveOnly: boolean): void {
        if (!this.editMode) {
          this.service.isExistingArea(this.serviceDocument.dataProfile.profileForm.controls["area"].value).subscribe((response: any) => {
                if (response) {
                  this.sharedService.errorForm("Area already exist.");
                } else {
                    this.saveArea(saveOnly);
                }
            });
        } else {
            this.saveArea(saveOnly);
        }
    }

    reset(): void {
        if (!this.editMode) {
            this.model = {
              area: null, areaName: null, mgrName: null, chain: null, chainName: null, currencyCode: null, operation: "I"
            };
           
            this.service.serviceDocument.dataProfile.dataModel = this.model;
            this.service.newModel(this.model);

        } else {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);            
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
        }
        this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
    }
}
