export class AreaModel { 
  area: number;
  areaName?: string;
  mgrName?: string;
  chain?: number;
  chainName?: string;
  currencyCode?: string;  
  operation?: string;
  createdUpdatedBy?: string;
  createdUpdatedDate?: Date;
  lastUpdatedBy?: string;
  lastUpdatedDate?: Date;
}
