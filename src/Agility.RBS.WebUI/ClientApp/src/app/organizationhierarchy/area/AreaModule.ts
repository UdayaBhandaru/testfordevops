import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { ComanyListComponent } from "./AreaListComponent";
import { AreaService } from "./AreaService";
import { AreaRoutingModule } from "./AreaRoutingModule";
import { AreaComponent } from "./AreaComponent";
import { AreaListResolver, AreaResolver } from "./AreaResolver";
import { RbsSharedModule } from "../../RbsSharedModule";

@NgModule({
    imports: [
        FrameworkCoreModule,
        AreaRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        ComanyListComponent,
        AreaComponent,
    ],
    providers: [
        AreaService,
        AreaListResolver,
        AreaResolver,
    ]
})
export class AreaModule {
    constructor() {
        LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
    }
}
