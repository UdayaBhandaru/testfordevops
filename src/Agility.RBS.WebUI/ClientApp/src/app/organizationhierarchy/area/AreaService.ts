import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { AreaModel } from "./AreaModel";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";

@Injectable()
export class AreaService {
    serviceDocument: ServiceDocument<AreaModel> = new ServiceDocument<AreaModel>();
    constructor(private httpHelperService: HttpHelperService) { }

    newModel(model: AreaModel): ServiceDocument<AreaModel> {
        return this.serviceDocument.newModel(model);
    }

    search(): Observable<ServiceDocument<AreaModel>> {
        return this.serviceDocument.search("/api/Area/Search");
    }

    save(): Observable<ServiceDocument<AreaModel>> {
        return this.serviceDocument.save("/api/Area/Save", true);
    }

    list(): Observable<ServiceDocument<AreaModel>> {
      return this.serviceDocument.list("/api/Area/List");
    }

    addEdit(): Observable<ServiceDocument<AreaModel>> {
      return this.serviceDocument.list("/api/Area/New");
    }

  isExistingArea(area: number): Observable<boolean> {
    return this.httpHelperService.get("/api/Area/IsExistingArea", new HttpParams().set("area", area.toString()));
    }
}
