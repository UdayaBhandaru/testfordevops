import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { GridOptions } from "ag-grid-community";
import { GridEditComponent } from "../../Common/grid/GridEditComponent";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { InfoComponent } from "../../Common/InfoComponent";
import { AreaModel } from "./AreaModel";
import { AreaService } from "./AreaService";
import { SharedService } from "../../Common/SharedService";
import { GridExportModel } from "../../Common/grid/GridExportModel";

@Component({
  selector: "area-list",
  templateUrl: "./AreaListComponent.html"
})
export class ComanyListComponent implements OnInit {
  PageTitle = "Area";
  redirectUrl = "Area";
  componentName: any;
  serviceDocument: ServiceDocument<AreaModel>;
  public gridOptions: GridOptions;
  columns: any[];
  model: AreaModel;
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;

  constructor(public service: AreaService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "area", field: "area", tooltipField: "area"},
      { headerName: "Area Name", field: "areaName", tooltipField: "areaName" },
      { headerName: "Manager Name", field: "mgrName", tooltipField: "mgrName" },
      { headerName: "Chain Name", field: "chainName", tooltipField: "chainName" },
      //{ headerName: "currencyCode", field: "currencyCode", tooltipField: "currencyCode" },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 80
      }
    ];
    
    this.model = { area: null, areaName: null};
    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }
}
