﻿import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";

import { OrgCountryModel } from "./OrgCountryModel";
import { OrgCountryService } from "./OrgCountryService";


@Injectable()
export class OrgCountryListResolver implements Resolve<ServiceDocument<OrgCountryModel>> {
    constructor(private service: OrgCountryService) { }
    resolve(): Observable<ServiceDocument<OrgCountryModel>> {
        return this.service.list();
    }
}

@Injectable()
export class OrgCountryResolver implements Resolve<ServiceDocument<OrgCountryModel>> {
    constructor(private service: OrgCountryService) { }
    resolve(): Observable<ServiceDocument<OrgCountryModel>> {
        return this.service.addEdit();
    }
}


