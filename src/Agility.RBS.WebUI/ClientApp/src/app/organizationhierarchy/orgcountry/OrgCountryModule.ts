﻿import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { OrgCountryRoutingModule } from "./OrgCountryRoutingModule";
import { OrgCountryListResolver, OrgCountryResolver } from "./OrgCountryResolver";
import { OrgCountryListComponent } from "./OrgCountryListComponent";
import { OrgCountryService } from "./OrgCountryService";
import { OrgCountryComponent } from "./OrgCountryComponent";
import { RbsSharedModule } from "../../RbsSharedModule";

@NgModule({
    imports: [
        FrameworkCoreModule,
        OrgCountryRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        OrgCountryListComponent,
        OrgCountryComponent
    ],
    providers: [
        OrgCountryService,
        OrgCountryListResolver,
        OrgCountryResolver

    ]
})
export class OrgCountryModule {
    constructor() {
        LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
    }
}
