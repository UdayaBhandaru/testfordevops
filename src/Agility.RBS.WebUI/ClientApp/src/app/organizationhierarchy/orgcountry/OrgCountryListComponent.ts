import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { GridOptions } from "ag-grid-community";
import { GridEditComponent } from "../../Common/grid/GridEditComponent";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { InfoComponent } from "../../Common/InfoComponent";
import { OrgCountryModel } from "./OrgCountryModel";
import { OrgCountryService } from "./OrgCountryService";
import { SharedService } from "../../Common/SharedService";
import { GridExportModel } from "../../Common/grid/GridExportModel";
import { CountryDomainModel } from "../../Common/DomainData/CountryDomainModel";
import { CompanyDomainModel } from "../../Common/DomainData/CompanyDomainModel";
import { CompanyModel } from "../company/CompanyModel";
@Component({
    selector: "org-country-list",
    templateUrl: "./OrgCountryListComponent.html"
})

export class OrgCountryListComponent implements OnInit {
    PageTitle = "Org Country";
    redirectUrl = "OrgCountry";
    companyData: CompanyModel[];
    countryData: CountryDomainModel[];
    componentName: any;
    serviceDocument: ServiceDocument<OrgCountryModel>;
    domainDtlData: DomainDetailModel[];
    public gridOptions: GridOptions;
    columns: any[];
    model: OrgCountryModel;

    gridExportModel: GridExportModel; gridExportGroup: FormGroup;
    showSearchCriteria: boolean = true;

    constructor(public service: OrgCountryService, private sharedService: SharedService, private router: Router) {
    }

    ngOnInit(): void {
        this.componentName = this;
        this.columns = [
            { headerName: "Company", field: "companyName", tooltipField: "companyName" },
            { headerName: "Country Code", field: "countryCode", tooltipField: "countryCode", width: 100 },
            { headerName: "Country Name", field: "countryName", tooltipField: "countryName" },
            { headerName: "Legal Name", field: "legalName", tooltipField: "legalName", width: 100 },
            { headerName: "Status", field: "countryStatusDesc", tooltipField: "countryStatusDesc", width: 60 },
            {
                headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 80
            }
        ];

        this.companyData = this.service.serviceDocument.domainData["company"];
        this.countryData = this.service.serviceDocument.domainData["country"];
        this.domainDtlData = this.service.serviceDocument.domainData["status"];

        this.sharedService.domainData = { company: this.companyData, status: this.domainDtlData, country: this.countryData };

        this.model = { companyId: null, countryCode: null, countryStatus: null, legalName: null };
        if (this.sharedService.searchData) {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
        } else {
            this.service.newModel(this.model);
        }
        this.serviceDocument = this.service.serviceDocument;
    }

    showSearchCriteriaChild(event: boolean): void {
        this.showSearchCriteria = event;
    }

    open(cell: any, mode: string): void {
        if (mode === "edit") {
            this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
        }
        this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
    }
}
