﻿import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { OrgCountryListResolver, OrgCountryResolver } from "./OrgCountryResolver";
import { OrgCountryListComponent } from "./OrgCountryListComponent";
import { OrgCountryComponent } from "./OrgCountryComponent";


const UtilityRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: OrgCountryListComponent,
                resolve:
                {
                    serviceDocument: OrgCountryListResolver
                }
            },
            {
                path: "New",
                component: OrgCountryComponent,
                resolve:
                {
                    serviceDocument: OrgCountryResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(UtilityRoutes)
    ]
})
export class OrgCountryRoutingModule { }
