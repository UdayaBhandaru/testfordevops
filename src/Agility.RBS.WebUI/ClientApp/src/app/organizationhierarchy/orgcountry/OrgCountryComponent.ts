import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { OrgCountryModel } from "./OrgCountryModel";
import { FormGroup } from "@angular/forms";
import { SharedService } from "../../Common/SharedService";
import { OrgCountryService } from "./OrgCountryService";
import { CompanyModel } from "../company/CompanyModel";
import { CountryDomainModel } from "../../Common/DomainData/CountryDomainModel";
import { CompanyDomainModel } from "../../Common/DomainData/CompanyDomainModel";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { LocationDomainModel } from "../../Common/DomainData/LocationDomainModel";
import { VatCodeDomainModel } from "../../Common/DomainData/VatCodeDomainModel";
import { Observable } from "rxjs";
import { RegionModel } from "../../organizationhierarchy/region/RegionModel";

@Component({
  selector: "org-country",
  templateUrl: "./OrgCountryComponent.html"
})

export class OrgCountryComponent implements OnInit {
  PageTitle = "Org Country";
  redirectUrl = "OrgCountry";
  serviceDocument: ServiceDocument<OrgCountryModel>;
  editMode: boolean = false;
  editModel: any;
  localizationData: any;
  companyData: CompanyModel[];
  countryData: CountryDomainModel[];
  indicatorData: DomainDetailModel[];
  domainDtlData: DomainDetailModel[];
  vatCodeData: VatCodeDomainModel[];
  vatCodeList: VatCodeDomainModel[];
  model: OrgCountryModel;
  companyModel: CompanyModel;
  regionModel: RegionModel;
  locationData: LocationDomainModel[];
  whBasedLocationList: LocationDomainModel[];
  vatExempReadonly: boolean = false;
  constructor(private service: OrgCountryService, private sharedService: SharedService, private router: Router
    , private ref: ChangeDetectorRef) {
  }

  ngAfterViewChecked(): void {
    this.ref.detectChanges();
  }

  ngOnInit(): void {
    this.indicatorData = Object.assign([], this.service.serviceDocument.domainData["indicator"]);
    this.companyData = Object.assign([], this.service.serviceDocument.domainData["company"]);
    this.countryData = Object.assign([], this.service.serviceDocument.domainData["country"]);
    this.domainDtlData = Object.assign([], this.service.serviceDocument.domainData["status"]);
    this.vatCodeList = Object.assign([], this.service.serviceDocument.domainData["vatcode"]);
    this.whBasedLocationList = Object.assign([], this.service.serviceDocument.domainData["location"]);
    this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
    this.bind();
  }

  bind(): void {
    delete this.editModel;
    if (this.sharedService.editObject) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
      this.locationData = this.whBasedLocationList.filter(item => item.companyId === this.service.serviceDocument.dataProfile.dataModel.companyId
        && item.countryCode === this.service.serviceDocument.dataProfile.dataModel.countryCode);
      this.vatCodeData = this.vatCodeList;
      if (this.service.serviceDocument.dataProfile.dataModel.vatExempt === "Y") {
        this.vatExempReadonly = true;
      }
      Object.assign(this.service.serviceDocument.dataProfile.dataModel, { operation: "U" });
      this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
      delete this.sharedService.editObject;
      this.editMode = true;
    } else {
      //this.companyData = this.companyData.filter(item => item.co === "A");
      this.vatCodeData = this.vatCodeList.filter(item => item.vatStatus === "A");
      this.model = {
        companyId: null, companyName: null, countryCode: null, countryName: null, legalName: null, taxRegNo: null,
        vatExempt: null, interCompany: null, vatCode: null, pluChargeValue: null, defaultWhs: null, operation: "I",
        countryStatus: "A"
      };
      this.service.serviceDocument.dataProfile.dataModel = this.model;
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  save(saveOnly: boolean): void {
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.saveSubscription(saveOnly);
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  saveSubscription(saveOnly: boolean): void {

    this.checkOrgCountry(saveOnly);
  }

  saveOrgCountry(saveOnly: boolean): void {
    this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.sharedService.saveForm(this.localizationData.orgCountry.orgcountrysave).subscribe(() => {
          if (saveOnly) {
            this.router.navigate(["/" + this.redirectUrl + "/List"], { skipLocationChange: true });
          } else {
            if (!this.editMode) {
              this.model = {
                companyId: null, companyName: null, countryCode: null, countryName: null, legalName: null
                , taxRegNo: null, vatExempt: null, interCompany: null, vatCode: null, pluChargeValue: null
                , defaultWhs: null, operation: "I", countryStatus: "A"
              };
              this.service.serviceDocument.dataProfile.profileForm.controls["vatCode"].setValue(null);
              this.service.serviceDocument.dataProfile.profileForm.controls["vatCode"].enable();
              this.service.serviceDocument.dataProfile.profileForm.controls["vatExempt"].setValue(null);
              this.service.serviceDocument.dataProfile.profileForm.controls["vatExempt"].enable();
              this.service.newModel(this.model);
            } else {
              Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
              this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.service.serviceDocument.dataProfile.dataModel);
              this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            }
          }
        }, (error: string) => { console.log(error); });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
    }, (error: string) => { console.log(error); });
  }

  checkOrgCountry(saveOnly: boolean): void {
    if (!this.editMode) {
      let form: FormGroup = this.serviceDocument.dataProfile.profileForm;
      if (form.controls["companyId"].value && form.controls["countryCode"].value) {
        this.service.isExistingOrgCountry(form.controls["companyId"].value, form.controls["countryCode"].value).subscribe
          ((response: boolean) => {
            if (response) {
              this.sharedService.errorForm(this.localizationData.orgCountry.orgcountrycheck);
            } else {
              this.saveOrgCountry(saveOnly);
            }
          });
      }
    } else {
      this.saveOrgCountry(saveOnly);
    }
  }

  reset(): void {
    if (!this.editMode) {
      this.model = {
        companyId: null, companyName: null, countryCode: null, countryName: null, legalName: null
        , taxRegNo: null, vatExempt: null, interCompany: null, vatCode: null, pluChargeValue: null
        , defaultWhs: null, operation: "I", countryStatus: "A"
      };
      this.serviceDocument.dataProfile.profileForm.controls["vatCode"].setValue(null);
      this.serviceDocument.dataProfile.profileForm.controls["vatCode"].enable();
      this.serviceDocument.dataProfile.profileForm.controls["vatExempt"].setValue(null);
      this.serviceDocument.dataProfile.profileForm.controls["vatExempt"].enable();
      this.service.serviceDocument.dataProfile.dataModel = this.model;
      this.service.newModel(this.model);
    } else {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);
      if (this.service.serviceDocument.dataProfile.dataModel.vatExempt === "Y") {
        this.vatCodeData = Object.assign([], this.vatCodeList);
        this.serviceDocument.dataProfile.profileForm.controls["vatCode"].setValue("VATEXEMPT");
        this.serviceDocument.dataProfile.profileForm.controls["vatCode"].disable();
        this.ref.detectChanges();
      } else {
        this.vatCodeData = Object.assign([], this.vatCodeList.filter(id => id.vatCode !== "VATEXEMPT"));
        this.serviceDocument.dataProfile.profileForm.controls["vatCode"].setValue(null);
        this.serviceDocument.dataProfile.profileForm.controls["vatCode"].enable();
      }
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    }
    this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
  }

  openCompanyMaster($event: MouseEvent): void {
    $event.preventDefault();
    this.companyModel = {
      company: null, coName: null, coAdd1: null, coAdd2: null, coAdd3: null, coCity: null
      , coState: null, coCountry: null, coPost: null, operation: "I"
    };
    this.sharedService.searchData = Object.assign({}, this.companyModel);
    this.router.navigate(["/Company"], { skipLocationChange: true });
  }

  openRegionMaster($event: MouseEvent): void {
    $event.preventDefault();
    this.regionModel = {
      companyId: this.service.serviceDocument.dataProfile.dataModel.companyId,
      countryCode: this.service.serviceDocument.dataProfile.dataModel.countryCode,
      regionCode: null, regionName: null, legalName: null, taxRegNo: null, vatExempt: null, interCompany: null,
      vatCode: null, pluChargeValue: null, defaultWhs: null, createdUpdatedBy: null, createdUpdatedDate: null,
      lastUpdatedBy: null, lastUpdatedDate: null, operation: null, companyName: null, countryName: null,
      regionStatus: null, regionStatusDesc: null
    };
    this.sharedService.searchData = Object.assign({}, this.regionModel);
    this.router.navigate(["/Region/"], { skipLocationChange: true });
  }
  changeCompany($event: any): void {
    this.service.serviceDocument.dataProfile.profileForm.patchValue({
      legalName: $event.options.legalName,
      vatExempt: $event.options.vatExempt,
      taxRegNo: $event.options.taxRegNo,
      interCompany: $event.options.interCompany,
      vatCode: $event.options.vatCode,
      pluChargeValue: $event.options.pluChargeValue
    });
    if ($event.options.vatExempt === "Y" && $event.options.vatCode === "VATEXEMPT") {
      this.serviceDocument.dataProfile.profileForm.controls["vatCode"].disable();
      this.serviceDocument.dataProfile.profileForm.controls["vatExempt"].disable();
    } else {
      this.serviceDocument.dataProfile.profileForm.controls["vatCode"].enable();
      this.serviceDocument.dataProfile.profileForm.controls["vatCode"].setValue($event.options.vatCode);
      this.serviceDocument.dataProfile.profileForm.controls["vatExempt"].enable();

    }
  }
  setDependentControl($event: any): void {
    if ($event.value === "Y") {
      this.vatCodeData = Object.assign([], this.vatCodeList);
      this.serviceDocument.dataProfile.profileForm.controls["vatCode"].setValue("VATEXEMPT");
      this.serviceDocument.dataProfile.profileForm.controls["vatCode"].disable();
      this.ref.detectChanges();
    } else {
      this.vatCodeData = Object.assign([], this.vatCodeList.filter(id => id.vatCode !== "VATEXEMPT"));
      this.serviceDocument.dataProfile.profileForm.controls["vatCode"].setValue(null);
      this.serviceDocument.dataProfile.profileForm.controls["vatCode"].enable();
    }
  }
}
