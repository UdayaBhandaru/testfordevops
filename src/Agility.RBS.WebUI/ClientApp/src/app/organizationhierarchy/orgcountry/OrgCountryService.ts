﻿import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { OrgCountryModel } from "./OrgCountryModel";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";

@Injectable()
export class OrgCountryService {
    serviceDocument: ServiceDocument<OrgCountryModel> = new ServiceDocument<OrgCountryModel>();
    constructor(private httpHelperService: HttpHelperService) { }

    newModel(model: OrgCountryModel): ServiceDocument<OrgCountryModel> {
        return this.serviceDocument.newModel(model);
    }

    search(): Observable<ServiceDocument<OrgCountryModel>> {
        return this.serviceDocument.search("/api/OrgCountry/Search");
    }

    save(): Observable<ServiceDocument<OrgCountryModel>> {
        return this.serviceDocument.save("/api/OrgCountry/Save",true);
    }

    list(): Observable<ServiceDocument<OrgCountryModel>> {
        return this.serviceDocument.list("/api/OrgCountry/List");
    }

    addEdit(): Observable<ServiceDocument<OrgCountryModel>> {
        return this.serviceDocument.list("/api/OrgCountry/New");
    }

    isExistingOrgCountry(companyId: number, countryCode: string): Observable<boolean> {
        return this.httpHelperService.get("/api/OrgCountry/IsExistingOrgCountry", new HttpParams().set("companyId", companyId.toString())
            .set("countryCode", countryCode));
    }
}