﻿export class OrgCountryModel {
    companyId: number;
    countryCode?: string;
    countryName?: string;
    legalName?: string;
    taxRegNo?: string;
    vatExempt?: string;
    interCompany?: string;
    vatCode?: string;
    pluChargeValue?: string;
    defaultWhs?: number;
    createdUpdatedBy?: string;
    createdUpdatedDate?: Date;
    lastUpdatedBy?: string;
    lastUpdatedDate?: Date;
    operation?: string;
    companyName?: string;
    countryStatus?: string;
    countryStatusDesc?: string;
}