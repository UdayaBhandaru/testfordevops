﻿import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LocationListResolver, LocationResolver } from "./LocationResolver";
import { LocationListComponent } from "./LocationListComponent";
import { LocationComponent } from "./LocationComponent";

const UtilityRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: LocationListComponent,
                resolve:
                {
                    serviceDocument: LocationListResolver
                }
            },
            {
                path: "New",
                component: LocationComponent,
                resolve:
                {
                    serviceDocument: LocationResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(UtilityRoutes)
    ]
})
export class LocationRoutingModule { }
