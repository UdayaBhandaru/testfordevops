﻿import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { LocationRoutingModule } from "./LocationRoutingModule";
import { LocationListResolver, LocationResolver } from "./LocationResolver";
import { LocationService } from "./LocationService";
import { LocationComponent } from "./LocationComponent";
import { LocationListComponent } from "./LocationListComponent";
import { RbsSharedModule } from "../../RbsSharedModule";

@NgModule({
    imports: [
        FrameworkCoreModule,
        LocationRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        LocationComponent,
        LocationListComponent

    ],
    providers: [
        LocationService,
        LocationListResolver,
        LocationResolver
    ]
})
export class LocationModule {
    constructor() {
        LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
    }
}
