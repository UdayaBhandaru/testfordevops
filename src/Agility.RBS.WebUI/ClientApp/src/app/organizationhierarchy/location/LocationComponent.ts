import { Component, OnInit, AfterViewInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { FormGroup, Validators, ValidatorFn, AbstractControl } from "@angular/forms";
import { SharedService } from "../../Common/SharedService";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { LocationService } from "./LocationService";
import { CompanyModel } from "../company/CompanyModel";
import { FormatModel } from "../format/FormatModel";
import { CompanyDomainModel } from "../../Common/DomainData/CompanyDomainModel";
import { OrgCountryDomainModel } from "../../Common/DomainData/OrgCountryDomainModel";
import { RegionDomainModel } from "../../Common/DomainData/RegionDomainModel";
import { FormatDomainModel } from "../../Common/DomainData/FormatDomainModel";
import { VatCodeDomainModel } from "../../Common/DomainData/VatCodeDomainModel";
import { CurrencyDomainModel } from "../../Common/DomainData/CurrencyDomainModel";
import { VatRegionDomainModel } from "../../Common/DomainData/VatRegionDomainModel";
import { LanguageDomainModel } from "../../Common/DomainData/LanguageDomainModel";
import { CommonModel } from "../../Common/CommonModel";
import { UserDomainModel } from "../../Common/DomainData/UserDomainModel";
import { StoreModel } from './StoreModel';
import { TransferEntityModel } from '../../transfer/TransferEntity/TransferEntityModel';
import { TransferZoneModel } from '../../transfer/transferzone/TransferZoneModel';
import { DistrictModel } from '../district/DistrictModel';
import { VatRegionModel } from '../../valueaddedtax/vatregion/VatRegionModel';
import { LocationTypeDomainModel } from '../../Common/DomainData/LocationTypeDomainModel';
import { StoreFormatModel } from '../../storeformat/StoreFormatModel';

@Component({

  selector: "location",
  templateUrl: "./LocationComponent.html"
})
export class LocationComponent implements OnInit, AfterViewInit {
  PageTitle = "Store";
  redirectUrl = "Location";
  serviceDocument: ServiceDocument<StoreModel>;
  editMode: boolean = false;
  editModel: StoreModel;
  localizationData: any;

  indicatorData: DomainDetailModel[];
  domainDtlData: DomainDetailModel[];
  languageData: LanguageDomainModel[];
  users: UserDomainModel[];
  transferZoneData: TransferZoneModel[];
  transferEntityData: TransferEntityModel[];
  vatRegionCode: VatRegionDomainModel[];
  currencyData: CurrencyDomainModel[];
  storeLocationsData: CommonModel[];
  districtData: DistrictModel[]
  model: StoreModel;
  fromLocationData: LocationTypeDomainModel[];
  storeFormatData: StoreFormatModel[];
  storeClassData: DomainDetailModel[];
  defaultWhData: LocationTypeDomainModel[];
  fromLocationsFileredList: LocationTypeDomainModel[];

  showCustomRequiredMessageForCloseDate: string;
  showCustomRequiredMessageForSisterStore: string;

  public i: number = 0;
  mandotryFieldList: string[] = [
    "vatIncludeInd",
    "stockHoldingInd",
    "integratedPosInd",
    "storeClass"
  ];

  constructor(private service: LocationService, private route: ActivatedRoute, private sharedService: SharedService
    , private router: Router) {
  }

  ngOnInit(): void {
    this.route.data.subscribe(() => {
      this.indicatorData = Object.assign([], this.service.serviceDocument.domainData["indicatorStatus"]);
      this.transferZoneData = Object.assign([], this.service.serviceDocument.domainData["transferZones"]);
      this.transferEntityData = Object.assign([], this.service.serviceDocument.domainData["transferEntities"]);
      this.languageData = Object.assign([], this.service.serviceDocument.domainData["language"]);
      this.currencyData = Object.assign([], this.service.serviceDocument.domainData["currency"]);
      this.users = Object.assign([], this.service.serviceDocument.domainData["users"]);
      this.storeLocationsData = Object.assign([], this.service.serviceDocument.domainData["storeLocations"]);
      this.vatRegionCode = Object.assign([], this.service.serviceDocument.domainData["vatRegionCode"]);
      this.districtData = Object.assign([], this.service.serviceDocument.domainData["districts"]);
      this.fromLocationData = Object.assign([], this.service.serviceDocument.domainData["locType"]);
      this.storeFormatData = Object.assign([], this.service.serviceDocument.domainData["storeForamt"]);
      this.storeClassData = Object.assign([], this.service.serviceDocument.domainData["classStore"]);
      this.fromLocationsFileredList = Object.assign([], this.service.serviceDocument.domainData["locType"]);


      this.storeLocationsData.forEach(x => x.status = +x.status);

      this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
    });
    this.bind();
  }

  ngAfterViewInit(): void {
    this.addValidators();
  }

  bind(): void {
    //if (this.locationClassData) {
    //  if (this.i === 0) {
    //    this.i = 1;
    //    this.locationClassData1 = Object.assign([], this.locationClassData);
    //    this.locationClassData = this.locationClassData1.sort(function (a: any, b: any): any { return a.domainDetailSeq - b.domainDetailSeq; });
    //  }
    //}
    delete this.editModel;
    this.defaultWhData = this.fromLocationsFileredList.filter(item => item.locType === "W");

    if (this.sharedService.editObject) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
      let dm: StoreModel = this.service.serviceDocument.dataProfile.dataModel;      
      this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
      delete this.sharedService.editObject;
      this.editMode = true;
    } else {
      this.model = {
        store: null,
        storeName: null,
        storeName10: null,
        storeName3: null,
        storeNameSecondary: null,
        storeClass: null,
        storeMgrName: null,
        storeOpenDate: null,
        storeCloseDate: null,
        acquiredDate: null,
        remodelDate: null,
        faxNumber: null,
        phoneNumber: null,
        email: null,
        totalSquareFt: null,
        sellingSquareFt: null,
        linearDistance: null,
        vatRegion: null,
        regionName:null,
        vatIncludeInd: null,
        stockHoldingInd: null,
        channelId: null,
        storeFormat: null,
        mallName: null,
        district: null,
        transferZone: null,
        transferZoneDesc: null,
        defaultWh: null,
        stopOrderDays: null,
        startOrderDays: null,
        currencyCode: null,
        lang: null,
        langDesc: null,
        tranNoGenerated: null,
        integratedPosInd: null,
        origCurrencyCode: null,
        dunsNumber: null,
        dunsLoc: null,
        sisterStore: null,
        tsfEntityId: null,
        orgUnitId: null,
        tableName: null,
        error: null,
        operation: "I"
      };
      this.service.serviceDocument.dataProfile.dataModel = this.model;
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  save(saveOnly: boolean): void {
    let fm: FormGroup = this.serviceDocument.dataProfile.profileForm;
    this.mandotryFieldList.forEach(x => fm.controls[x].setValidators([Validators.required]));
    this.mandotryFieldList.forEach(x => fm.controls[x].updateValueAndValidity());
    this.sharedService.validateForm(fm);
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.saveSubscription(saveOnly);
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  addValidators(): void {
    let lfg: FormGroup = this.serviceDocument.dataProfile.profileForm;
    this.mandotryFieldList.forEach(x => lfg.controls[x].setValidators([Validators.required, radioButtonValidator("Required*")]));
    lfg.controls["email"].setValidators([Validators.required, this.sharedService.emailValidator("Invalid Email Address")]);
  }

  saveSubscription(saveOnly: boolean): void {
    this.checkLocation(saveOnly);
  }

  saveLocation(saveOnly: boolean): void {
    if (!this.serviceDocument.dataProfile.profileForm.controls["storeCloseDate"].value) {
      this.showCustomRequiredMessageForCloseDate = "Required";
    } else {
      this.service.save().subscribe(() => {
        if (this.service.serviceDocument.result.type === MessageType.success) {
          this.sharedService.saveForm("Store saved successfully").subscribe(() => {
            if (saveOnly) {
              this.router.navigate(["/" + this.redirectUrl + "/List"], { skipLocationChange: true });
            } else {
              if (!this.editMode) {
                this.model = new StoreModel();
                this.model = {
                  store: null,
                  storeName: null,
                  storeName10: null,
                  storeName3: null,
                  storeNameSecondary: null,
                  storeClass: null,
                  storeMgrName: null,
                  storeOpenDate: null,
                  storeCloseDate: null,
                  acquiredDate: null,
                  remodelDate: null,
                  faxNumber: null,
                  phoneNumber: null,
                  email: null,
                  totalSquareFt: null,
                  sellingSquareFt: null,
                  linearDistance: null,
                  vatRegion: null,
                  regionName: null,
                  vatIncludeInd: null,
                  stockHoldingInd: null,
                  channelId: null,
                  storeFormat: null,
                  mallName: null,
                  district: null,
                  transferZone: null,
                  transferZoneDesc: null,
                  defaultWh: null,
                  stopOrderDays: null,
                  startOrderDays: null,
                  currencyCode: null,
                  lang: null,
                  langDesc: null,
                  tranNoGenerated: null,
                  integratedPosInd: null,
                  origCurrencyCode: null,
                  dunsNumber: null,
                  dunsLoc: null,
                  sisterStore: null,
                  tsfEntityId: null,
                  orgUnitId: null,
                  tableName: null,
                  error: null,
                  operation: "I"
                };
                this.service.newModel(this.model);
              } else {
                Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
                this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.service.serviceDocument.dataProfile.dataModel);
                this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
              }
            }
          });
        } else {
          this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
        }
      });
      this.showCustomRequiredMessageForCloseDate = null;

    }
  }

  checkLocation(saveOnly: boolean): void {
    if (!this.editMode) {
      let form: FormGroup = this.serviceDocument.dataProfile.profileForm;
      if (form.controls["store"].value && form.controls["storeName"].value) {
        this.service.isExistingLocation(form.controls["store"].value, form.controls["storeName"].value).subscribe
          ((response: boolean) => {
            if (response) {
              this.sharedService.errorForm(this.localizationData.location.locationcheck);
            } else {

              this.saveLocation(saveOnly);
            }
          });
      }
    } else {

      this.saveLocation(saveOnly);
    }
  }

  reset(): void {
    this.showCustomRequiredMessageForCloseDate = null;
    this.showCustomRequiredMessageForSisterStore = null;

    if (!this.editMode) {    
      this.model = {
        store: null,
        storeName: null,
        storeName10: null,
        storeName3: null,
        storeNameSecondary: null,
        storeClass: null,
        storeMgrName: null,
        storeOpenDate: null,
        storeCloseDate: null,
        acquiredDate: null,
        remodelDate: null,
        faxNumber: null,
        phoneNumber: null,
        email: null,
        totalSquareFt: null,
        sellingSquareFt: null,
        linearDistance: null,
        vatRegion: null,
        regionName: null,
        vatIncludeInd: null,
        stockHoldingInd: null,
        channelId: null,
        storeFormat: null,
        mallName: null,
        district: null,
        transferZone: null,
        transferZoneDesc: null,
        defaultWh: null,
        stopOrderDays: null,
        startOrderDays: null,
        currencyCode: null,
        lang: null,
        langDesc: null,
        tranNoGenerated: null,
        integratedPosInd: null,
        origCurrencyCode: null,
        dunsNumber: null,
        dunsLoc: null,
        sisterStore: null,
        tsfEntityId: null,
        orgUnitId: null,
        tableName: null,
        error: null,
        operation: "I"
      };
      this.service.serviceDocument.dataProfile.profileForm.setValue({
        companyId: null, companyName: null, countryCode: null, countryName: null, legalName: null, taxRegNo: null, vatExempt: null
        , interCompany: null, vatCode: null, pluChargeValue: null, defaultWhs: null, operation: "I", currentActionId: null, countryStatus: "A"
      });
      this.service.serviceDocument.dataProfile.dataModel = this.model;
      this.service.newModel(this.model);
    } else {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
      this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
    }
  }
 
  setSisterStoreControlValidation($event: any): void {
    if (this.serviceDocument.dataProfile.profileForm.controls["stockHoldingInd"].value === "N") {
      this.serviceDocument.dataProfile.profileForm.controls["sisterStore"].setValue(null);
      this.showCustomRequiredMessageForSisterStore = "Required";
    } else {
      this.showCustomRequiredMessageForSisterStore = null;
    }
  }
  checkSisterStore($event: any): void {
    if (this.serviceDocument.dataProfile.profileForm.controls["sisterStore"].value !== null) {
      this.showCustomRequiredMessageForSisterStore = null;
    }
  }
}

export function radioButtonValidator(errorMessage: string): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } => {
    if (control.value != null && control.value.length > 0) {
      return null;
    } else {
      return { errorMessage };
    }
  };
}
