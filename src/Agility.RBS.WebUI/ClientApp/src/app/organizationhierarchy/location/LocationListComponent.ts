import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { GridOptions } from "ag-grid-community";
import { GridEditComponent } from "../../Common/grid/GridEditComponent";
import { InfoComponent } from "../../Common/InfoComponent";
import { LocationService } from "./LocationService";
import { SharedService } from "../../Common/SharedService";
import { GridExportModel } from "../../Common/grid/GridExportModel";
import { StoreModel } from './StoreModel';

@Component({
  selector: "location-list",
  templateUrl: "./LocationListComponent.html"
})

export class LocationListComponent implements OnInit {
  pageTitle = "Store";
  redirectUrl = "Location";
  componentName: any;
  serviceDocument: ServiceDocument<StoreModel>;
  public gridOptions: GridOptions;
  columns: any[];
  model: StoreModel;
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;

  constructor(public service: LocationService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "store Id", field: "store", tooltipField: "store" },
      { headerName: "store Name", field: "storeName", tooltipField: "storeName" },
      { headerName: "store Name Secondary", field: "storeNameSecondary", tooltipField: "storeNameSecondary" },
      { headerName: "store Format", field: "storeFormat", tooltipField: "storeFormat" },     
      //{ headerName: "MallName", field: "mallName", tooltipField: "mallName" },
      { headerName: "LocationEmail", field: "email", tooltipField: "email" },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 110
      }
    ];

    this.model = {
      store: null,
      storeName: null,
      storeName10: null,
      storeName3: null,
      storeNameSecondary: null,
      storeClass: null,
      storeMgrName: null,
      storeOpenDate: null,
      storeCloseDate: null,
      acquiredDate: null,
      remodelDate: null,
      faxNumber: null,
      phoneNumber: null,
      email: null,
      totalSquareFt: null,
      sellingSquareFt: null,
      linearDistance: null,
      vatRegion: null,
      regionName: null,
      vatIncludeInd: null,
      stockHoldingInd: null,
      channelId: null,
      storeFormat: null,
      mallName: null,
      district: null,
      transferZone: null,
      transferZoneDesc: null,
      defaultWh: null,
      stopOrderDays: null,
      startOrderDays: null,
      currencyCode: null,
      lang: null,
      langDesc: null,
      tranNoGenerated: null,
      integratedPosInd: null,
      origCurrencyCode: null,
      dunsNumber: null,
      dunsLoc: null,
      sisterStore: null,
      tsfEntityId: null,
      orgUnitId: null,
      tableName: null,
      error: null,
      operation: null
    };
    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    this.showSearchCriteria = false;
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }   
}
