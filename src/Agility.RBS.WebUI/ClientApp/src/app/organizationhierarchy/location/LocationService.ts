import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { StoreModel } from "./StoreModel";
import { SharedService } from "../../Common/SharedService";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";

@Injectable()
export class LocationService {
  serviceDocument: ServiceDocument<StoreModel> = new ServiceDocument<StoreModel>();
    constructor(private httpHelperService: HttpHelperService, private sharedService: SharedService) { }

  newModel(model: StoreModel): ServiceDocument<StoreModel> {
        return this.serviceDocument.newModel(model);
    }

  search(): Observable<ServiceDocument<StoreModel>> {
        return this.serviceDocument.search("/api/Location/Search");
    }

  save(): Observable<ServiceDocument<StoreModel>> {
        return this.serviceDocument.save("/api/Location/Save", true, () => this.setDate());
    }

  list(): Observable<ServiceDocument<StoreModel>> {
        return this.serviceDocument.list("/api/Location/List");
    }

  addEdit(): Observable<ServiceDocument<StoreModel>> {
        return this.serviceDocument.list("/api/Location/New");
    }

  isExistingLocation(store: number, storeName: string): Observable<boolean>
  {
    return this.httpHelperService.get("/api/Location/IsExistingLocation", new HttpParams().set("store", store.toString()).set("storeName", storeName));
  }
    setDate(): void {
      var sd: StoreModel = this.serviceDocument.dataProfile.dataModel;
        if (sd !== null) {
          if (sd.storeOpenDate != null) {
            sd.storeOpenDate = this.sharedService.setOffSet(sd.storeOpenDate);
            }
          if (sd.storeCloseDate != null) {
            sd.storeCloseDate = this.sharedService.setOffSet(sd.storeCloseDate);
            }
            if (sd.acquiredDate != null) {
                sd.acquiredDate = this.sharedService.setOffSet(sd.acquiredDate);
            }
          if (sd.remodelDate != null) {
            sd.remodelDate = this.sharedService.setOffSet(sd.remodelDate);
            }          
        }
    }
}
