import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { StoreModel } from "./StoreModel";
import { LocationService } from "./LocationService";

@Injectable()
export class LocationListResolver implements Resolve<ServiceDocument<StoreModel>> {
    constructor(private service: LocationService) { }
  resolve(): Observable<ServiceDocument<StoreModel>> {
        return this.service.list();
    }
}

@Injectable()
export class LocationResolver implements Resolve<ServiceDocument<StoreModel>> {
    constructor(private service: LocationService) { }
  resolve(): Observable<ServiceDocument<StoreModel>> {
        return this.service.addEdit();
    }
}

