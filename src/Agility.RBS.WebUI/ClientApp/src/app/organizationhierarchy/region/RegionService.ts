﻿import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { RegionModel } from "./RegionModel";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";

@Injectable()
export class RegionService {
    serviceDocument: ServiceDocument<RegionModel> = new ServiceDocument<RegionModel>();
    constructor(private httpHelperService: HttpHelperService) { }

    newModel(model: RegionModel): ServiceDocument<RegionModel> {
        return this.serviceDocument.newModel(model);
    }

    search(): Observable<ServiceDocument<RegionModel>> {
        return this.serviceDocument.search("/api/region/Search");
    }

    save(): Observable<ServiceDocument<RegionModel>> {
        return this.serviceDocument.save("/api/region/Save",true);
    }

    list(): Observable<ServiceDocument<RegionModel>> {
        return this.serviceDocument.list("/api/region/List");
    }

    addEdit(): Observable<ServiceDocument<RegionModel>> {
        return this.serviceDocument.list("/api/region/New");
    }

    isExistingRegion(companyId: number, countryCode: string, regionCode: string): Observable<boolean> {
        return this.httpHelperService.get("/api/region/IsExistingRegion", new HttpParams().set("companyId", companyId.toString())
            .set("countryCode", countryCode).set("regionCode", regionCode));

    }
}