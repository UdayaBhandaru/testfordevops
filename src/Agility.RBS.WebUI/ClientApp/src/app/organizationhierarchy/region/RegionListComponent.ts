import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { GridOptions } from "ag-grid-community";
import { GridEditComponent } from "../../Common/grid/GridEditComponent";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { InfoComponent } from "../../Common/InfoComponent";
import { SharedService } from "../../Common/SharedService";
import { GridExportModel } from "../../Common/grid/GridExportModel";
import { RegionModel } from "./RegionModel";
import { RegionService } from "./RegionService";
import { CompanyModel } from "../company/CompanyModel";
import { OrgCountryModel } from "../orgcountry/OrgCountryModel";
import { AreaDomainModel } from '../../Common/DomainData/AreaDomainModel';

@Component({
  selector: "region-list",
  templateUrl: "./RegionListComponent.html"
})

export class RegionListComponent implements OnInit {
  PageTitle = "Region";
  redirectUrl = "Region";
  componentName: any;
  areaDomainData: AreaDomainModel[];
  areaDomainList: AreaDomainModel[];
  serviceDocument: ServiceDocument<RegionModel>;
  public gridOptions: GridOptions;
  columns: any[];
  model: RegionModel;
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;

  constructor(public service: RegionService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "Region", field: "region", tooltipField: "region" },
      { headerName: "Region Name", field: "regionName", tooltipField: "regionName" },
      { headerName: "Manager Name", field: "mgrName", tooltipField: "mgrName", width: 120 },
      { headerName: "Currency Code", field: "currencyCode", tooltipField: "currencyCode", width: 100 },     
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 100
      }
    ];

    this.areaDomainData = this.service.serviceDocument.domainData["area"];
    this.sharedService.domainData = {
      country: this.areaDomainData
    };

    this.model = { region: null, regionName: null, mgrName: null, companyId: null, area : null,areaName:null,currencyCode:null};
    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.areaDomainData = this.areaDomainData.filter(item => item.area === this.service.serviceDocument.dataProfile.dataModel.area);
      this.sharedService.domainData["orgcountry"] = this.areaDomainData;
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }


  reset(): void {
    this.areaDomainData = [];
  }
}
