﻿import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { RegionService } from "./RegionService";
import { RegionModel } from "./RegionModel";

@Injectable()
export class RegionListResolver implements Resolve<ServiceDocument<RegionModel>> {
    constructor(private service: RegionService) { }
    resolve(): Observable<ServiceDocument<RegionModel>> {
        return this.service.list();
    }
}

@Injectable()
export class RegionResolver implements Resolve<ServiceDocument<RegionModel>> {
    constructor(private service: RegionService) { }
    resolve(): Observable<ServiceDocument<RegionModel>> {
        return this.service.addEdit();
    }
}


