﻿import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { RegionRoutingModule } from "./RegionRoutingModule";
import { RegionListResolver, RegionResolver } from "./RegionResolver";
import { RegionListComponent } from "./RegionListComponent";
import { RegionComponent } from "./RegionComponent";
import { RegionService } from "./RegionService";
import { RbsSharedModule } from "../../RbsSharedModule";

@NgModule({
    imports: [
        FrameworkCoreModule,
        RegionRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        RegionListComponent,
        RegionComponent
    ],
    providers: [
        RegionService,
        RegionListResolver,
        RegionResolver
    ]
})
export class RegionModule {
    constructor() {
        LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
    }
}
