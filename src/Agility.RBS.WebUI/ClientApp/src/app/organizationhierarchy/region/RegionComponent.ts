import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { SharedService } from "../../Common/SharedService";
import { RegionService } from "./RegionService";
import { RegionModel } from "./RegionModel";
import { OrgCountryModel } from "../orgcountry/OrgCountryModel";
import { FormatModel } from "../format/FormatModel";

import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { LocationDomainModel } from "../../Common/DomainData/LocationDomainModel";
import { Observable } from "rxjs";
import { CompanyDomainModel } from "../../Common/DomainData/CompanyDomainModel";
import { OrgCountryDomainModel } from "../../Common/DomainData/OrgCountryDomainModel";
import { VatCodeDomainModel } from "../../Common/DomainData/VatCodeDomainModel";
import { ChainDomainModel } from '../../Common/DomainData/ChainDomainModel';
import { AreaDomainModel } from '../../Common/DomainData/AreaDomainModel';
import { CurrencyDomainModel } from '../../Common/DomainData/CurrencyDomainModel';

@Component({
  selector: "region",
  templateUrl: "./RegionComponent.html"
})

export class RegionComponent implements OnInit {
  PageTitle = "Region";
  redirectUrl = "Region";
  serviceDocument: ServiceDocument<RegionModel>;
  editMode: boolean = false;
  editModel: any;
  localizationData: any;
  areaDomainData: AreaDomainModel[];
  currencyData: CurrencyDomainModel[];

  model: RegionModel;
  orgCountryModel: OrgCountryModel;
  formatModel: FormatModel;
  vatExempReadonly: boolean = false;
  constructor(private service: RegionService, private sharedService: SharedService, private router: Router
    , private ref: ChangeDetectorRef) {
  }

  ngAfterViewChecked(): void {
    this.ref.detectChanges();
  }

  ngOnInit(): void {


    this.areaDomainData = Object.assign([], this.service.serviceDocument.domainData["area"]);
    this.currencyData = Object.assign([], this.service.serviceDocument.domainData["currency"]);
    this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
    this.bind();
  }

  bind(): void {
    delete this.editModel;
    if (this.sharedService.editObject) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
      //this.orgCountryData = this.orgCountryList.filter(item => item.companyId === this.service.serviceDocument.dataProfile.dataModel.companyId);     
      this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
      delete this.sharedService.editObject;
      this.editMode = true;
    } else {  
      this.model = {
        region: null, regionName: null, mgrName: null, area: null, areaName: null, currencyCode: null, operation: "I",
         companyId: null, companyName: null, countryCode: null, countryName: null, legalName: null, taxRegNo: null
        , vatExempt: null, interCompany: null, vatCode: null, pluChargeValue: null, defaultWhs: null
        , regionCode: null, regionStatus: "A"
      };
      this.service.serviceDocument.dataProfile.dataModel = this.model;
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  save(saveOnly: boolean): void {
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.saveSubscription(saveOnly);
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  saveSubscription(saveOnly: boolean): void {
    this.checkRegion(saveOnly);
  }

  saveRegion(saveOnly: boolean): void {
    this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.sharedService.saveForm(this.localizationData.region.regionsave).subscribe(() => {
          if (saveOnly) {
            this.router.navigate(["/" + this.redirectUrl + "/List"], { skipLocationChange: true });
          } else {
            if (!this.editMode) {
              this.model = {
                region: null, regionName: null, mgrName: null, area: null, areaName: null, currencyCode: null, operation: "I",
                companyId: null, companyName: null, countryCode: null, countryName: null, legalName: null, taxRegNo: null
                , vatExempt: null, interCompany: null, vatCode: null, pluChargeValue: null, defaultWhs: null
                , regionCode: null, regionStatus: "A"     
              };              
              this.service.newModel(this.model);
            } else {
              Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
              this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.service.serviceDocument.dataProfile.dataModel);
              this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            }
          }
        }, (error: string) => { console.log(error); });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
    }, (error: string) => { console.log(error); });
  }

  checkRegion(saveOnly: boolean): void {
    if (!this.editMode) {
      let form: any = this.serviceDocument.dataProfile.profileForm;
      this.service.isExistingRegion(form.controls["region"].value, form.controls["regionName"].value, form.controls["area"].value).subscribe
        ((response: boolean) => {
          if (response) {
            this.sharedService.errorForm(this.localizationData.region.regioncheck);
          } else {
            this.saveRegion(saveOnly);
          }
        });
    } else {
      this.saveRegion(saveOnly);
    }
  }

  reset(): void {
    if (!this.editMode) {
      this.model = {
        region: null, regionName: null, mgrName: null, area: null, areaName: null, currencyCode: null, operation: "I",
        companyId: null, companyName: null, countryCode: null, countryName: null, legalName: null, taxRegNo: null
        , vatExempt: null, interCompany: null, vatCode: null, pluChargeValue: null, defaultWhs: null
        , regionCode: null, regionStatus: "A"    
      };     
      this.service.serviceDocument.dataProfile.dataModel = this.model;
      this.service.newModel(this.model);
    } else {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);      
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    }
    this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
  }

 
  //changeCountry($event: any): void {
  //  this.service.serviceDocument.dataProfile.profileForm.patchValue({
  //    legalName: $event.options.legalName,
  //    taxRegNo: $event.options.taxRegNo,
  //    vatExempt: $event.options.vatExempt,
  //    interCompany: $event.options.interCompany,
  //    vatCode: $event.options.vatCode,
  //    pluChargeValue: $event.options.pluChargeValue
  //  });
  //  if ($event.options.vatExempt === "Y" && $event.options.vatCode === "VATEXEMPT") {
  //    this.serviceDocument.dataProfile.profileForm.controls["vatCode"].disable();
  //    this.serviceDocument.dataProfile.profileForm.controls["vatExempt"].disable();
  //  } else {
  //    this.serviceDocument.dataProfile.profileForm.controls["vatCode"].enable();
  //    this.serviceDocument.dataProfile.profileForm.controls["vatCode"].setValue($event.options.vatCode);
  //    this.serviceDocument.dataProfile.profileForm.controls["vatExempt"].enable();

  //  }
  //}  
}
