export class RegionModel {    
    region?: string;
    regionName?: string;
    mgrName?: string;
    area?: number;
    areaName?: string;    
    currencyCode?: string;
    createdUpdatedBy?: string;
    createdUpdatedDate?: Date;
    lastUpdatedBy?: string;
    lastUpdatedDate?: Date;
    operation?: string;
    companyId: number;
    countryCode?: string;
    regionCode?: string;  
    legalName?: string;
    taxRegNo?: string;
    vatExempt?: string;
    interCompany?: string;
    vatCode?: string;
    pluChargeValue?: string;
    defaultWhs?: number;  
    companyName?: string;
    countryName?: string;
    regionStatus?: string;
    regionStatusDesc?: string;
}
