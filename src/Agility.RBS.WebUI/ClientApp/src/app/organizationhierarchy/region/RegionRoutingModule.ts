﻿import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { RegionListResolver, RegionResolver } from "./RegionResolver";
import { RegionListComponent } from "./RegionListComponent";
import { RegionComponent } from "./RegionComponent";
const UtilityRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: RegionListComponent,
                resolve:
                {
                    serviceDocument: RegionListResolver
                }
            },
            {
                path: "New",
                component: RegionComponent,
                resolve:
                {
                    serviceDocument: RegionResolver
                }
            }
        ]

    }
];

@NgModule({
    imports: [
        RouterModule.forChild(UtilityRoutes)
    ]
})
export class RegionRoutingModule { }
