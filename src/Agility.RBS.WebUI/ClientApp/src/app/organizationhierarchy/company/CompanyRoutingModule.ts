﻿import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ComanyListComponent } from "./CompanyListComponent";
import { CompanyListResolver, CompanyResolver} from "./CompanyResolver";
import { CompanyComponent } from "./CompanyComponent";
const UtilityRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: ComanyListComponent,
                resolve:
                {
                    serviceDocument: CompanyListResolver
                }
            },
            {
                path: "New",
                component: CompanyComponent,
                resolve:
                {
                    serviceDocument: CompanyResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(UtilityRoutes)
    ]
})
export class CompanyRoutingModule { }
