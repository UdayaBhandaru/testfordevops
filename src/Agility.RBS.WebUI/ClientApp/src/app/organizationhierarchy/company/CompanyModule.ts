﻿import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { ComanyListComponent } from "./CompanyListComponent";
import { CompanyService } from "./CompanyService";
import { CompanyRoutingModule } from "./CompanyRoutingModule";
import { CompanyComponent } from "./CompanyComponent";
import { CompanyListResolver, CompanyResolver } from "./CompanyResolver";
import { RbsSharedModule } from "../../RbsSharedModule";

@NgModule({
    imports: [
        FrameworkCoreModule,
        CompanyRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        ComanyListComponent,
        CompanyComponent,
    ],
    providers: [
        CompanyService,
        CompanyListResolver,
        CompanyResolver,
    ]
})
export class CompanyModule {
    constructor() {
        LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
    }
}
