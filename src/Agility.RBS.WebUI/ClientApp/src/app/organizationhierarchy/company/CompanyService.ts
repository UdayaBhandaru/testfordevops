﻿import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { CompanyModel } from "./CompanyModel";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";

@Injectable()
export class CompanyService {
    serviceDocument: ServiceDocument<CompanyModel> = new ServiceDocument<CompanyModel>();
    constructor(private httpHelperService: HttpHelperService) { }

    newModel(model: CompanyModel): ServiceDocument<CompanyModel> {
        return this.serviceDocument.newModel(model);
    }

    search(): Observable<ServiceDocument<CompanyModel>> {
        return this.serviceDocument.search("/api/Company/Search");
    }

    save(): Observable<ServiceDocument<CompanyModel>> {
        return this.serviceDocument.save("/api/Company/Save", true);
    }

    list(): Observable<ServiceDocument<CompanyModel>> {
        return this.serviceDocument.list("/api/Company/List");
    }

    addEdit(): Observable<ServiceDocument<CompanyModel>> {
        return this.serviceDocument.list("/api/Company/New");
    }

    isExistingCompany(companyId: number): Observable<boolean> {
        return this.httpHelperService.get("/api/Company/IsExistingCompany", new HttpParams().set("companyId", companyId.toString()));
    }
}