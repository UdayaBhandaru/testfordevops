﻿import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { CompanyService } from "./CompanyService";
import { CompanyModel } from "./CompanyModel";
@Injectable()
export class CompanyListResolver implements Resolve<ServiceDocument<CompanyModel>> {
    constructor(private service: CompanyService) { }
    resolve(): Observable<ServiceDocument<CompanyModel>> {
        return this.service.list();
    }
}

@Injectable()
export class CompanyResolver implements Resolve<ServiceDocument<CompanyModel>> {
    constructor(private service: CompanyService) { }
    resolve(): Observable<ServiceDocument<CompanyModel>> {
        return this.service.addEdit();
    }
}



