export class CompanyModel { 
  company: number;
  coName?: string;
  coAdd1?: string;
  coAdd2?: string;
  coAdd3?: string;
  coCity?: string;
  coState?: string;
  coCountry?: string;
  coPost?: string;
  operation?: string;
  createdUpdatedBy?: string;
  createdUpdatedDate?: Date;
  lastUpdatedBy?: string;
  lastUpdatedDate?: Date;

}
