import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { GridOptions } from "ag-grid-community";
import { GridEditComponent } from "../../Common/grid/GridEditComponent";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { InfoComponent } from "../../Common/InfoComponent";
import { CompanyModel } from "./CompanyModel";
import { CompanyService } from "./CompanyService";
import { SharedService } from "../../Common/SharedService";
import { GridExportModel } from "../../Common/grid/GridExportModel";

@Component({
  selector: "company-list",
  templateUrl: "./CompanyListComponent.html"
})
export class ComanyListComponent implements OnInit {
  PageTitle = "Company";
  redirectUrl = "Company";
  componentName: any;
  serviceDocument: ServiceDocument<CompanyModel>;
  public gridOptions: GridOptions;
  columns: any[];
  model: CompanyModel;
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;

  constructor(public service: CompanyService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "Id", field: "company", tooltipField: "company"},
      { headerName: "Name", field: "coName", tooltipField: "coName" },
      { headerName: "Address1", field: "coAdd1", tooltipField: "coAdd1" },
      { headerName: "Address2", field: "coAdd2", tooltipField: "coAdd2" },
      { headerName: "Address3", field: "coAdd3", tooltipField: "coAdd3" },
      { headerName: "Country", field: "coCountry", tooltipField: "coCountry" },
      { headerName: "State", field: "coState", tooltipField: "coState" },
      { headerName: "City", field: "coCity", tooltipField: "coCity" },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 80
      }
    ];
    
    this.model = { company: null, coName: null};
    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }
}
