import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { SharedService } from "../../Common/SharedService";
import { CompanyModel } from "./CompanyModel";
import { CompanyService } from "./CompanyService";
import { StateDomainModel } from '../../Common/DomainData/StateDomainModel';
import { CountryDomainModel } from '../../Common/DomainData/CountryDomainModel';

@Component({
    selector: "company",
    templateUrl: "./CompanyComponent.html"
})

export class CompanyComponent implements OnInit {
    PageTitle = "Company";
    redirectUrl = "Company";
    serviceDocument: ServiceDocument<CompanyModel>;
    editMode: boolean = false;
    editModel: any;
    localizationData: any;  
    model: CompanyModel;  
  countryDomainData: CountryDomainModel[];
  stateDomainData: StateDomainModel[];
  cityDomainData: StateDomainModel[];

    constructor(private service: CompanyService, private sharedService: SharedService, private router: Router
        , private ref: ChangeDetectorRef) {
    }

    ngOnInit(): void {
      this.countryDomainData = Object.assign([], this.service.serviceDocument.domainData["country"]);
      this.stateDomainData = Object.assign([], this.service.serviceDocument.domainData["state"]);
        this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
        this.bind();
    }

    bind(): void {
        delete this.editModel;
        if (this.sharedService.editObject) {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);         
            Object.assign(this.service.serviceDocument.dataProfile.dataModel, { operation: "U" });
            this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            delete this.sharedService.editObject;
            this.editMode = true;

        } else {            
            this.model = {
              company: null, coName: null, coAdd1: null, coAdd2: null, coAdd3: null, coCity: null
              , coState: null, coCountry: null, coPost: null,operation: "I"
            };
           
            this.service.serviceDocument.dataProfile.dataModel = this.model;
            this.service.newModel(this.model);
        }
        this.serviceDocument = this.service.serviceDocument;
    }

    save(saveOnly: boolean): void {
        if (this.serviceDocument.dataProfile.profileForm.valid) {
            this.saveSubscription(saveOnly);
        } else {
            this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
        }
    }

    saveSubscription(saveOnly: boolean): void {
        this.checkCompany(saveOnly);
  }

    saveCompany(saveOnly: boolean): void {
        this.service.save().subscribe(() => {
            if (this.service.serviceDocument.result.type === MessageType.success) {
                this.sharedService.saveForm(this.localizationData.company.companysave).subscribe(() => {
                    if (saveOnly) {
                        this.router.navigate(["/" + this.redirectUrl + "/List"], { skipLocationChange: true });
                    } else {
                        if (!this.editMode) {
                            this.model = {
                              company: null, coName: null, coAdd1: null, coAdd2: null, coAdd3: null, coCity: null
                              , coState: null, coCountry: null, coPost: null, operation: "I"
                            };                            
                            this.service.newModel(this.model);
                        } else {
                            Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
                            this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.service.serviceDocument.dataProfile.dataModel);
                            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
                        }
                    }
                }, (error: string) => { console.log(error); });
            } else {
                this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
            }
        }, (error: string) => { console.log(error); });
    }

    checkCompany(saveOnly: boolean): void {
        if (!this.editMode) {
          this.service.isExistingCompany(this.serviceDocument.dataProfile.profileForm.controls["company"].value).subscribe((response: any) => {
                if (response) {
                    this.sharedService.errorForm(this.localizationData.company.companycheck);
                } else {
                    this.saveCompany(saveOnly);
                }
            });
        } else {
            this.saveCompany(saveOnly);
        }
    }

    reset(): void {
        if (!this.editMode) {
            this.model = {
              company: null, coName: null, coAdd1: null, coAdd2: null, coAdd3: null, coCity: null
              , coState: null, coCountry: null, coPost: null, operation: "I"
            };
           
            this.service.serviceDocument.dataProfile.dataModel = this.model;
            this.service.newModel(this.model);

        } else {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);            
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
        }
        this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
    }
}
