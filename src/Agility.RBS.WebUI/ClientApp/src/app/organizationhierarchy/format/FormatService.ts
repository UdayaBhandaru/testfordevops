﻿import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormatModel } from "./FormatModel";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";

@Injectable()
export class FormatService {
    serviceDocument: ServiceDocument<FormatModel> = new ServiceDocument<FormatModel>();
    constructor(private httpHelperService: HttpHelperService) { }

    newModel(model: FormatModel): ServiceDocument<FormatModel> {
        return this.serviceDocument.newModel(model);
    }

    search(): Observable<ServiceDocument<FormatModel>> {
        return this.serviceDocument.search("/api/format/Search");
    }

    save(): Observable<ServiceDocument<FormatModel>> {
        return this.serviceDocument.save("/api/format/Save" ,true);
    }

    list(): Observable<ServiceDocument<FormatModel>> {
        return this.serviceDocument.list("/api/format/List");
    }

    addEdit(): Observable<ServiceDocument<FormatModel>> {
        return this.serviceDocument.list("/api/format/New");
    }

    isExistingFormat(companyId: number, countryCode: string, regionCode: string, formatCode: string): Observable<boolean> {
        return this.httpHelperService.get("/api/format/IsExistingFormat", new HttpParams().set("companyId", companyId.toString())
            .set("countryCode", countryCode).set("regionCode", regionCode).set("formatCode", formatCode));
    }
}