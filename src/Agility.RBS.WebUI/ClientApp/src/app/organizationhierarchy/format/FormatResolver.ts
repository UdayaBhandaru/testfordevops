﻿import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormatService } from "./FormatService";
import { FormatModel } from "./FormatModel";

@Injectable()
export class FormatListResolver implements Resolve<ServiceDocument<FormatModel>> {
    constructor(private service: FormatService) { }
    resolve(): Observable<ServiceDocument<FormatModel>> {
        return this.service.list();
    }
}

@Injectable()
export class FormatResolver implements Resolve<ServiceDocument<FormatModel>> {
    constructor(private service: FormatService) { }
    resolve(): Observable<ServiceDocument<FormatModel>> {
        return this.service.addEdit();
    }
}


