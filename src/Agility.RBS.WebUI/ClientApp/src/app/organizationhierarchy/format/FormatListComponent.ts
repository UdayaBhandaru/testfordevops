import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { GridOptions } from "ag-grid-community";
import { GridEditComponent } from "../../Common/grid/GridEditComponent";
import { InfoComponent } from "../../Common/InfoComponent";
import { SharedService } from "../../Common/SharedService";
import { GridExportModel } from "../../Common/grid/GridExportModel";
import { FormatModel } from "./FormatModel";
import { FormatService } from "./FormatService";
import { CompanyModel } from "../company/CompanyModel";
import { OrgCountryModel } from "../orgcountry/OrgCountryModel";
import { RegionModel } from "../region/RegionModel";
import { DomainDetailModel } from "../../domain/DomainDetailModel";

@Component({
  selector: "format-list",
  templateUrl: "./FormatListComponent.html"
})

export class FormatListComponent implements OnInit {
  PageTitle = "Format";
  redirectUrl = "Format";
  componentName: any;
  serviceDocument: ServiceDocument<FormatModel>;
  companyData: CompanyModel[];
  orgCountryData: OrgCountryModel[]; orgCountryList: OrgCountryModel[];
  regionData: RegionModel[]; regionList: RegionModel[];
  domainDtlData: DomainDetailModel[];
  public gridOptions: GridOptions;
  columns: any[];
  model: FormatModel;
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;

  constructor(public service: FormatService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "Company", field: "companyName", tooltipField: "companyName" },
      { headerName: "Country", field: "countryName", tooltipField: "countryName" },
      { headerName: "Region", field: "regionName", tooltipField: "regionName" },
      { headerName: "Format Code", field: "formatCode", tooltipField: "formatCode" },
      { headerName: "Format Name", field: "formatName", tooltipField: "formatName" },
      { headerName: "Legal Name", field: "legalName", tooltipField: "legalName" },
      { headerName: "Status", field: "formatStatusDesc", tooltipField: "formatStatusDesc", width: 130 },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 130
      }
    ];

    this.companyData = this.service.serviceDocument.domainData["company"];
    this.orgCountryList = this.service.serviceDocument.domainData["orgcountry"];
    this.regionList = this.service.serviceDocument.domainData["region"];
    this.domainDtlData = this.service.serviceDocument.domainData["status"];

    this.sharedService.domainData = {
      company: this.companyData, status: this.domainDtlData
    };

    this.model = { companyId: null, countryCode: null, regionCode: null, formatCode: null, formatName: null, legalName: null, formatStatus: null };
    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.orgCountryData = this.orgCountryList.filter(item => item.companyId === this.service.serviceDocument.dataProfile.dataModel.companyId);
      this.regionData = this.regionList.filter(item => item.countryCode === this.service.serviceDocument.dataProfile.dataModel.countryCode
        && item.companyId === this.service.serviceDocument.dataProfile.dataModel.companyId);
      this.sharedService.domainData["country"] = this.orgCountryData;
      this.sharedService.domainData["region"] = this.regionData;
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }

  changeCompany(): void {
    this.serviceDocument.dataProfile.profileForm.controls["countryCode"].setValue(null);
    this.serviceDocument.dataProfile.profileForm.controls["regionCode"].setValue(null);
    this.orgCountryData = this.orgCountryList.filter(item => item.companyId === +this.serviceDocument.dataProfile.profileForm.controls["companyId"].value);
    this.sharedService.domainData["country"] = this.orgCountryData;
  }

  changeCountry(): void {
    this.serviceDocument.dataProfile.profileForm.controls["regionCode"].setValue(null);
    this.regionData = this.regionList.filter(item => item.countryCode === this.serviceDocument.dataProfile.profileForm.controls["countryCode"].value
      && item.companyId === +this.serviceDocument.dataProfile.profileForm.controls["companyId"].value);
    this.sharedService.domainData["region"] = this.regionData;
  }
  reset(): void {
    this.orgCountryData = [];
    this.regionData = [];
  }
}
