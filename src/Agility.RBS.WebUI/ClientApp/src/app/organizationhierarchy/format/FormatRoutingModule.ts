﻿import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { FormatListResolver, FormatResolver } from "./FormatResolver";
import { FormatListComponent } from "./FormatListComponent";
import { FormatComponent } from "./FormatComponent";
const UtilityRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: FormatListComponent,
                resolve:
                {
                    serviceDocument: FormatListResolver
                }
            },
            {
                path: "New",
                component: FormatComponent,
                resolve:
                {
                    serviceDocument: FormatResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(UtilityRoutes)
    ]
})
export class FormatRoutingModule { }
