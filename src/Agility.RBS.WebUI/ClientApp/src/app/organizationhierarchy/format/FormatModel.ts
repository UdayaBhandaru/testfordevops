﻿export class FormatModel {
    companyId: number;
    countryCode?: string;
    regionCode?: string;
    formatCode?: string;
    formatName?: string;
    legalName?: string;
    taxRegNo?: string;
    vatExempt?: string;
    interCompany?: string;
    vatCode?: string;
    pluChargeValue?: string;
    defaultWhs?: number;
    createdUpdatedBy?: string;
    createdUpdatedDate?: Date;
    lastUpdatedBy?: string;
    lastUpdatedDate?: Date;
    operation?: string;
    companyName?: string;
    countryName?: string;
    regionName?: string;
    formatStatus?: string;
}