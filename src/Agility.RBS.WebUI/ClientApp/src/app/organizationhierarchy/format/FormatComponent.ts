import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { SharedService } from "../../Common/SharedService";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { RegionModel } from "../region/RegionModel";
import { FormatModel } from "./FormatModel";
import { FormatService } from "./FormatService";
import { Observable } from "rxjs";
import { CompanyDomainModel } from "../../Common/DomainData/CompanyDomainModel";
import { OrgCountryDomainModel } from "../../Common/DomainData/OrgCountryDomainModel";
import { RegionDomainModel } from "../../Common/DomainData/RegionDomainModel";
import { LocationDomainModel } from "../../Common/DomainData/LocationDomainModel";
import { VatCodeDomainModel } from "../../Common/DomainData/VatCodeDomainModel";
import { FormGroup } from "@angular/forms";
import { LocationModel } from "../../organizationhierarchy/location/LocationModel";


@Component({
    selector: "format",
    templateUrl: "./FormatComponent.html"
})

export class FormatComponent implements OnInit {
    PageTitle = "Format";
    redirectUrl = "Format";
    serviceDocument: ServiceDocument<FormatModel>;
    editMode: boolean = false;
    editModel: any;
    localizationData: any;
    companyData: CompanyDomainModel[];
    orgCountryData: OrgCountryDomainModel[]; orgCountryList: OrgCountryDomainModel[];
    regionData: RegionModel[]; regionList: RegionModel[];
    vatCodeData: VatCodeDomainModel[];
    domainDtlData: DomainDetailModel[];
    indicatorData: DomainDetailModel[];
    model: FormatModel;
    regionModel: RegionModel;
    vatCodeList: VatCodeDomainModel[];
    locationModel: LocationModel;
    locationData: LocationDomainModel[];
    whBasedLocationList: LocationDomainModel[];
    vatExempReadonly: boolean = false;
    constructor(private service: FormatService, private sharedService: SharedService, private router: Router
        , private ref: ChangeDetectorRef) {
    }

    ngOnInit(): void {
        this.indicatorData = Object.assign([], this.service.serviceDocument.domainData["indicator"]);
        this.companyData = Object.assign([], this.service.serviceDocument.domainData["company"]);
        this.orgCountryList = Object.assign([], this.service.serviceDocument.domainData["orgcountry"]);
        this.regionList = Object.assign([], this.service.serviceDocument.domainData["region"]);
        this.vatCodeList = Object.assign([], this.service.serviceDocument.domainData["vatcode"]);
        this.domainDtlData = Object.assign([], this.service.serviceDocument.domainData["status"]);
        this.whBasedLocationList = Object.assign([], this.service.serviceDocument.domainData["location"]);
        this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
        this.bind();
    }

    ngAfterViewChecked(): void {
        this.ref.detectChanges();
    }

    bind(): void {
        delete this.editModel;
        if (this.sharedService.editObject) {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
            this.orgCountryData = this.orgCountryList.filter(item => item.companyId === this.service.serviceDocument.dataProfile.dataModel.companyId);
            this.regionData = this.regionList.filter(item => item.countryCode === this.service.serviceDocument.dataProfile.dataModel.countryCode
                && item.companyId === this.service.serviceDocument.dataProfile.dataModel.companyId);
            this.locationData = this.whBasedLocationList.filter(item => item.companyId === this.service.serviceDocument.dataProfile.dataModel.companyId
                && item.countryCode === this.service.serviceDocument.dataProfile.dataModel.countryCode
                && item.regionCode === this.service.serviceDocument.dataProfile.dataModel.regionCode
                && item.formatCode === this.service.serviceDocument.dataProfile.dataModel.formatCode);
            this.vatCodeData = this.vatCodeList;
            if (this.service.serviceDocument.dataProfile.dataModel.vatExempt === "Y") {
                this.vatExempReadonly = true;
            }
            this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            delete this.sharedService.editObject;
            this.editMode = true;
        } else {
            this.companyData = this.companyData.filter(item => item.companyStatus === "A");
            this.vatCodeData = this.vatCodeList.filter(item => item.vatStatus === "A");
            this.model = {
                companyId: null, countryCode: null, regionCode: null, formatCode: null, formatName: null, legalName: null,
                taxRegNo: null, vatExempt: null, interCompany: null, vatCode: null, pluChargeValue: null, defaultWhs: null, formatStatus: "A",
                operation: "I"
            };
            this.service.serviceDocument.dataProfile.dataModel = this.model;
            this.service.newModel(this.model);
        }
        this.serviceDocument = this.service.serviceDocument;
    }

    save(saveOnly: boolean): void {
        if (this.serviceDocument.dataProfile.profileForm.valid) {
            this.saveSubscription(saveOnly);
        } else {
            this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
        }
    }

    saveSubscription(saveOnly: boolean): void {
        this.checkFormat(saveOnly);
    }

    private checkFormat(saveOnly: boolean): void {
        if (!this.editMode) {
            let form: FormGroup = this.serviceDocument.dataProfile.profileForm;
            if (form.controls["companyId"].value && form.controls["countryCode"].value && form.controls["regionCode"].value
                && form.controls["formatCode"].value) {
                this.service.isExistingFormat(form.controls["companyId"].value, form.controls["countryCode"].value,
                    form.controls["regionCode"].value, form.controls["formatCode"].value).subscribe
                    ((response: boolean) => {
                        if (response) {
                            this.sharedService.errorForm(this.localizationData.format.formatcheck);
                        } else {
                            this.saveFormat(saveOnly);
                        }
                    });
            }
        } else {
            this.saveFormat(saveOnly);
        }
    }

    private saveFormat(saveOnly: boolean): void {
        this.service.save().subscribe(() => {
            if (this.service.serviceDocument.result.type === MessageType.success) {
                this.sharedService.saveForm(this.localizationData.format.formatsave).subscribe(() => {
                    if (saveOnly) {
                        this.router.navigate(["/" + this.redirectUrl + "/List"], { skipLocationChange: true });
                    } else {
                        if (!this.editMode) {
                            this.model = {
                                companyId: null, countryCode: null, regionCode: null, formatCode: null, formatName: null
                                , legalName: null, taxRegNo: null, vatExempt: null, interCompany: null, vatCode: null
                                , pluChargeValue: null, defaultWhs: null, formatStatus: "A", operation: "I"
                            };
                            this.service.serviceDocument.dataProfile.profileForm.controls["vatCode"].setValue(null);
                            this.service.serviceDocument.dataProfile.profileForm.controls["vatCode"].enable();
                            this.service.serviceDocument.dataProfile.profileForm.controls["vatExempt"].setValue(null);
                            this.service.serviceDocument.dataProfile.profileForm.controls["vatExempt"].enable();
                            this.service.newModel(this.model);
                        } else {
                            Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
                            this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.service.serviceDocument.dataProfile.dataModel);
                            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
                        }
                    }
                }, (error: string) => { console.log(error); });
            } else {
                this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
            }
        }, (error: string) => { console.log(error); });
    }

    changeCompany(): void {
        this.serviceDocument.dataProfile.profileForm.controls["countryCode"].setValue(null);
        this.serviceDocument.dataProfile.profileForm.controls["regionCode"].setValue(null);
        this.serviceDocument.dataProfile.profileForm.controls["vatCode"].setValue(null);
        this.serviceDocument.dataProfile.profileForm.controls["vatCode"].enable();
        this.orgCountryData = this.orgCountryList.filter(item => item.companyId === +this.serviceDocument.dataProfile.profileForm.controls["companyId"].value
            && item.countryStatus === "A");
    }

    changeCountry(): void {
        this.serviceDocument.dataProfile.profileForm.controls["regionCode"].setValue(null);
        this.regionData = this.regionList.filter(item => item.countryCode === this.serviceDocument.dataProfile.profileForm.controls["countryCode"].value
            && item.companyId === +this.serviceDocument.dataProfile.profileForm.controls["companyId"].value
            && item.regionStatus === "A");
    }

    reset(): void {
        if (!this.editMode) {
            this.model = {
                companyId: null, countryCode: null, regionCode: null, formatCode: null, formatName: null, legalName: null
                , taxRegNo: null, vatExempt: null, interCompany: null, vatCode: null, pluChargeValue: null, defaultWhs: null
                , formatStatus: "A", operation: "I"
            };
            this.serviceDocument.dataProfile.profileForm.controls["vatCode"].setValue(null);
            this.serviceDocument.dataProfile.profileForm.controls["vatCode"].enable();
            this.serviceDocument.dataProfile.profileForm.controls["vatExempt"].setValue(null);
            this.serviceDocument.dataProfile.profileForm.controls["vatExempt"].enable();
            this.service.serviceDocument.dataProfile.dataModel = this.model;
            this.service.newModel(this.model);
        } else {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);
            if (this.service.serviceDocument.dataProfile.dataModel.vatExempt === "Y") {
                this.vatCodeData = Object.assign([], this.vatCodeList);
                this.serviceDocument.dataProfile.profileForm.controls["vatCode"].setValue("VATEXEMPT");
                this.serviceDocument.dataProfile.profileForm.controls["vatCode"].disable();
                this.ref.detectChanges();
            } else {
                this.vatCodeData = Object.assign([], this.vatCodeList.filter(id => id.vatCode !== "VATEXEMPT"));
                this.serviceDocument.dataProfile.profileForm.controls["vatCode"].setValue(null);
                this.serviceDocument.dataProfile.profileForm.controls["vatCode"].enable();
            }
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
        }
        this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
    }

    openregion($event: MouseEvent): void {
        $event.preventDefault();
        this.regionModel = {
            companyId: this.service.serviceDocument.dataProfile.dataModel.companyId,
            countryCode: this.service.serviceDocument.dataProfile.dataModel.countryCode,
            regionCode: this.service.serviceDocument.dataProfile.dataModel.regionCode,
            regionName: null, legalName: null, taxRegNo: null, vatExempt: null, interCompany: null,
            vatCode: null, pluChargeValue: null, defaultWhs: null, createdUpdatedBy: null, createdUpdatedDate: null,
            lastUpdatedBy: null, lastUpdatedDate: null, operation: null, companyName: null, countryName: null,
            regionStatus: null, regionStatusDesc: null
        };
        this.sharedService.searchData = Object.assign({}, this.regionModel);
        this.router.navigate(["/Region/"], { skipLocationChange: true });
    }

    openLocation($event: MouseEvent): void {
        $event.preventDefault();
        this.locationModel = {
            code: null, companyId: this.service.serviceDocument.dataProfile.dataModel.companyId,
            companyName: null, countryCode: this.service.serviceDocument.dataProfile.dataModel.countryCode, countryName: null,
            regionCode: this.service.serviceDocument.dataProfile.dataModel.regionCode, regionName: null,
            formatCode: this.service.serviceDocument.dataProfile.dataModel.formatCode, formatName: null,
            locationId: null, locationName: null, type: null, typeName: null, name10: null, name3: null, nameArabic: null,
            openDate: null, closeDate: null, acquiredDate: null, reModelDate: null, anchorStore: null, anchorStoreName: null,
            mallName: null, storeFormat: null, storeFormatName: null, vatRegion: null, vatRegionName: null, transferZone: null,
            language: null, languageName: null, stopOrderDays: null, startOrderDays: null, currencyCode: null, currencyName: null,
            locationClass: null, className: null, mgrId: null, mgrName: null, address1: null, address2: null, address3: null,
            city: null, state: null, phone1: null, phone2: null, phone3: null, fax1: null, fax2: null, totalSqaureFeet: null,
            sellingSquareFeet: null, linearDistance: null, geoCode: null, email: null, vatIncludeInd: null, vatIncludeIndDesc: null,
            stockHoldingInd: null, stockHoldingIndDesc: null, priceZone: null, priceZoneName: null, costZone: null, costZoneName: null,
            defaultWareHouse: null, tranNumGenerated: null, integratedPosInd: null, originCurrencyCode: null,
            originCurrencyName: null, sisterStore: null, tsfEntityId: null, organizationUnitId: null, tableName: null, status: null
        };
        this.sharedService.searchData = Object.assign({}, this.locationModel);
        this.router.navigate(["/Location/"], { skipLocationChange: true });
    }
    changeRegion($event: any): void {
        this.service.serviceDocument.dataProfile.profileForm.patchValue({
            legalName: $event.options.legalName,
            taxRegNo: $event.options.taxRegNo,
            vatExempt: $event.options.vatExempt,
            interCompany: $event.options.interCompany,
            vatCode: $event.options.vatCode,
            pluChargeValue: $event.options.pluChargeValue
        });

        if ($event.options.vatExempt === "Y" && $event.options.vatCode === "VATEXEMPT") {
            this.serviceDocument.dataProfile.profileForm.controls["vatCode"].disable();
            this.serviceDocument.dataProfile.profileForm.controls["vatExempt"].disable();
        } else {
            this.serviceDocument.dataProfile.profileForm.controls["vatCode"].enable();
            this.serviceDocument.dataProfile.profileForm.controls["vatCode"].setValue($event.options.vatCode);
            this.serviceDocument.dataProfile.profileForm.controls["vatExempt"].enable();

        }
    }
    setDependentControl($event: any): void {
        if ($event.value === "Y") {
            this.vatCodeData = Object.assign([], this.vatCodeList);
            this.serviceDocument.dataProfile.profileForm.controls["vatCode"].setValue("VATEXEMPT");
            this.serviceDocument.dataProfile.profileForm.controls["vatCode"].disable();
            this.ref.detectChanges();
        } else {
            this.vatCodeData = Object.assign([], this.vatCodeList.filter(id => id.vatCode !== "VATEXEMPT"));
            this.serviceDocument.dataProfile.profileForm.controls["vatCode"].setValue(null);
            this.serviceDocument.dataProfile.profileForm.controls["vatCode"].enable();
        }
    }
}
