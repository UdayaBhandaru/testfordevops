﻿import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { FormatRoutingModule } from "./FormatRoutingModule";
import { FormatListResolver, FormatResolver } from "./FormatResolver";
import { FormatComponent } from "./FormatComponent";
import { FormatListComponent } from "./FormatListComponent";
import { FormatService } from "./FormatService";
import { RbsSharedModule } from "../../RbsSharedModule";
@NgModule({
    imports: [
        FrameworkCoreModule,
        FormatRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [

        FormatListComponent,
        FormatComponent

    ],
    providers: [
        FormatService,
        FormatListResolver,
        FormatResolver
    ]
})
export class FormatModule {
    constructor() {
        LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
    }
}
