import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { DistrictService } from "./DistrictService";
import { DistrictModel } from "./DistrictModel";
@Injectable()
export class DistrictListResolver implements Resolve<ServiceDocument<DistrictModel>> {
    constructor(private service: DistrictService) { }
    resolve(): Observable<ServiceDocument<DistrictModel>> {
        return this.service.list();
    }
}

@Injectable()
export class DistrictResolver implements Resolve<ServiceDocument<DistrictModel>> {
    constructor(private service: DistrictService) { }
    resolve(): Observable<ServiceDocument<DistrictModel>> {
        return this.service.addEdit();
    }
}



