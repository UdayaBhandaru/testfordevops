import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { DistrictModel } from "./DistrictModel";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";

@Injectable()
export class DistrictService {
    serviceDocument: ServiceDocument<DistrictModel> = new ServiceDocument<DistrictModel>();
    constructor(private httpHelperService: HttpHelperService) { }

    newModel(model: DistrictModel): ServiceDocument<DistrictModel> {
        return this.serviceDocument.newModel(model);
    }

    search(): Observable<ServiceDocument<DistrictModel>> {
      return this.serviceDocument.search("/api/District/Search");
    }

    save(): Observable<ServiceDocument<DistrictModel>> {
      return this.serviceDocument.save("/api/District/Save", true);
    }

    list(): Observable<ServiceDocument<DistrictModel>> {
      return this.serviceDocument.list("/api/District/List");
    }

    addEdit(): Observable<ServiceDocument<DistrictModel>> {
      return this.serviceDocument.list("/api/District/New");
    }

  isExistingDistrict(district: number): Observable<boolean> {
    return this.httpHelperService.get("/api/District/IsExistingDistrict", new HttpParams().set("district", district.toString()));
    }
}
