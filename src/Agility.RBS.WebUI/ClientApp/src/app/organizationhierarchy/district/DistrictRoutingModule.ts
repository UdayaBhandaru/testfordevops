import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ComanyListComponent } from "./DistrictListComponent";
import { DistrictListResolver, DistrictResolver} from "./DistrictResolver";
import { DistrictComponent } from "./DistrictComponent";
const UtilityRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: ComanyListComponent,
                resolve:
                {
                    serviceDocument: DistrictListResolver
                }
            },
            {
                path: "New",
                component: DistrictComponent,
                resolve:
                {
                    serviceDocument: DistrictResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(UtilityRoutes)
    ]
})
export class DistrictRoutingModule { }
