import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { ComanyListComponent } from "./DistrictListComponent";
import { DistrictService } from "./DistrictService";
import { DistrictRoutingModule } from "./DistrictRoutingModule";
import { DistrictComponent } from "./DistrictComponent";
import { DistrictListResolver, DistrictResolver } from "./DistrictResolver";
import { RbsSharedModule } from "../../RbsSharedModule";

@NgModule({
    imports: [
        FrameworkCoreModule,
        DistrictRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        ComanyListComponent,
        DistrictComponent,
    ],
    providers: [
        DistrictService,
        DistrictListResolver,
        DistrictResolver,
    ]
})
export class DistrictModule {
    constructor() {
        LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
    }
}
