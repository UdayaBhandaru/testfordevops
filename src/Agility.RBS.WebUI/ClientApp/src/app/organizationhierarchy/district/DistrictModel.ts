export class DistrictModel { 
  district: number;
  districtName?: string;
  mgrName?: string;
  region?: number;
  regionName?: string;
  currencyCode?: string;  
  operation?: string;
  createdUpdatedBy?: string;
  createdUpdatedDate?: Date;
  lastUpdatedBy?: string;
  lastUpdatedDate?: Date;
}
