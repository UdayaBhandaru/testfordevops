import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { SharedService } from "../../Common/SharedService";
import { DistrictModel } from "./DistrictModel";
import { DistrictService } from "./DistrictService";
import { StateDomainModel } from '../../Common/DomainData/StateDomainModel';
import { CountryDomainModel } from '../../Common/DomainData/CountryDomainModel';
import { ChainDomainModel } from '../../Common/DomainData/ChainDomainModel';
import { CurrencyDomainModel } from '../../Common/DomainData/CurrencyDomainModel';
import { RegionModel } from '../region/RegionModel';

@Component({
  selector: "district",
  templateUrl: "./DistrictComponent.html"
})

export class DistrictComponent implements OnInit {
  PageTitle = "District";
  redirectUrl = "District";
    serviceDocument: ServiceDocument<DistrictModel>;
    editMode: boolean = false;
    editModel: any;
    localizationData: any;  
    model: DistrictModel;  
  chainDomainData: ChainDomainModel[];
  stateDomainData: StateDomainModel[];
  cityDomainData: StateDomainModel[];
  currencyData: CurrencyDomainModel[];
  regionData: RegionModel[]; 
    constructor(private service: DistrictService, private sharedService: SharedService, private router: Router
        , private ref: ChangeDetectorRef) {
    }

    ngOnInit(): void {
      this.currencyData = Object.assign([], this.service.serviceDocument.domainData["currency"]);
      this.regionData = Object.assign([], this.service.serviceDocument.domainData["regions"]);
        this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
        this.bind();
    }

    bind(): void {
        delete this.editModel;
        if (this.sharedService.editObject) {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);         
            Object.assign(this.service.serviceDocument.dataProfile.dataModel, { operation: "U" });
            this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            delete this.sharedService.editObject;
            this.editMode = true;

        } else {            
            this.model = {
              district: null, districtName: null, mgrName: null, region: null, regionName: null, currencyCode: null, operation: "I"
            };
           
            this.service.serviceDocument.dataProfile.dataModel = this.model;
            this.service.newModel(this.model);
        }
        this.serviceDocument = this.service.serviceDocument;
    }

    save(saveOnly: boolean): void {
        if (this.serviceDocument.dataProfile.profileForm.valid) {
            this.saveSubscription(saveOnly);
        } else {
            this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
        }
    }

    saveSubscription(saveOnly: boolean): void {
        this.checkDistrict(saveOnly);
  }

    saveDistrict(saveOnly: boolean): void {
        this.service.save().subscribe(() => {
            if (this.service.serviceDocument.result.type === MessageType.success) {
                this.sharedService.saveForm("District saved successfully").subscribe(() => {
                    if (saveOnly) {
                        this.router.navigate(["/" + this.redirectUrl + "/List"], { skipLocationChange: true });
                    } else {
                        if (!this.editMode) {
                            this.model = {
                              district: null, districtName: null, mgrName: null, region: null, regionName: null, currencyCode: null, operation: "I"
                            };                            
                            this.service.newModel(this.model);
                        } else {
                            Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
                            this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.service.serviceDocument.dataProfile.dataModel);
                            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
                        }
                    }
                }, (error: string) => { console.log(error); });
            } else {
                this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
            }
        }, (error: string) => { console.log(error); });
    }

    checkDistrict(saveOnly: boolean): void {
        if (!this.editMode) {
          this.service.isExistingDistrict(this.serviceDocument.dataProfile.profileForm.controls["district"].value).subscribe((response: any) => {
                if (response) {
                  this.sharedService.errorForm("District already exist.");
                } else {
                    this.saveDistrict(saveOnly);
                }
            });
        } else {
            this.saveDistrict(saveOnly);
        }
    }

    reset(): void {
        if (!this.editMode) {
            this.model = {
              district: null, districtName: null, mgrName: null, region: null, regionName: null, currencyCode: null, operation: "I"
            };
           
            this.service.serviceDocument.dataProfile.dataModel = this.model;
            this.service.newModel(this.model);

        } else {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);            
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
        }
        this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
    }
}
