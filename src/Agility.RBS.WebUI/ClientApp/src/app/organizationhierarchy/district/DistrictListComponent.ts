import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { GridOptions } from "ag-grid-community";
import { GridEditComponent } from "../../Common/grid/GridEditComponent";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { InfoComponent } from "../../Common/InfoComponent";
import { DistrictModel } from "./DistrictModel";
import { DistrictService } from "./DistrictService";
import { SharedService } from "../../Common/SharedService";
import { GridExportModel } from "../../Common/grid/GridExportModel";

@Component({
  selector: "district-list",
  templateUrl: "./DistrictListComponent.html"
})
export class ComanyListComponent implements OnInit {
  PageTitle = "District";
  redirectUrl = "District";
  componentName: any;
  serviceDocument: ServiceDocument<DistrictModel>;
  public gridOptions: GridOptions;
  columns: any[];
  model: DistrictModel;
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;

  constructor(public service: DistrictService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "district", field: "district", tooltipField: "district", width: 80 },
      { headerName: "District Name", field: "districtName", tooltipField: "districtName" },
      { headerName: "Manager Name", field: "mgrName", tooltipField: "mgrName" },
      { headerName: "Region Name", field: "regionName", tooltipField: "regionName" },
      //{ headerName: "currencyCode", field: "currencyCode", tooltipField: "currencyCode" },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 80
      }
    ];
    
    this.model = { district: null, districtName: null };
    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }
}
