import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ComanyListComponent } from "./ChainListComponent";
import { ChainListResolver, ChainResolver} from "./ChainResolver";
import { ChainComponent } from "./ChainComponent";
const UtilityRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: ComanyListComponent,
                resolve:
                {
                    serviceDocument: ChainListResolver
                }
            },
            {
                path: "New",
                component: ChainComponent,
                resolve:
                {
                    serviceDocument: ChainResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(UtilityRoutes)
    ]
})
export class ChainRoutingModule { }
