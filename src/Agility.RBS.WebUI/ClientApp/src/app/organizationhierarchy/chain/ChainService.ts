import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { ChainModel } from "./ChainModel";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";

@Injectable()
export class ChainService {
    serviceDocument: ServiceDocument<ChainModel> = new ServiceDocument<ChainModel>();
    constructor(private httpHelperService: HttpHelperService) { }

    newModel(model: ChainModel): ServiceDocument<ChainModel> {
        return this.serviceDocument.newModel(model);
    }

    search(): Observable<ServiceDocument<ChainModel>> {
      return this.serviceDocument.search("/api/Chain/Search");
    }

    save(): Observable<ServiceDocument<ChainModel>> {
      return this.serviceDocument.save("/api/Chain/Save", true);
    }

    list(): Observable<ServiceDocument<ChainModel>> {
      return this.serviceDocument.list("/api/Chain/List");
    }

    addEdit(): Observable<ServiceDocument<ChainModel>> {
      return this.serviceDocument.list("/api/Chain/New");
    }

  isExistingChain(hierValue: number): Observable<boolean> {
    return this.httpHelperService.get("/api/Chain/IsExistingChain", new HttpParams().set("hierValue", hierValue.toString()));
    }
}
