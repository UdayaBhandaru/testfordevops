import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { ChainService } from "./ChainService";
import { ChainModel } from "./ChainModel";
@Injectable()
export class ChainListResolver implements Resolve<ServiceDocument<ChainModel>> {
    constructor(private service: ChainService) { }
    resolve(): Observable<ServiceDocument<ChainModel>> {
        return this.service.list();
    }
}

@Injectable()
export class ChainResolver implements Resolve<ServiceDocument<ChainModel>> {
    constructor(private service: ChainService) { }
    resolve(): Observable<ServiceDocument<ChainModel>> {
        return this.service.addEdit();
    }
}



