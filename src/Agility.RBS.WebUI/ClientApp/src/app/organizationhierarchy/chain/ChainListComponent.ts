import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { GridOptions } from "ag-grid-community";
import { GridEditComponent } from "../../Common/grid/GridEditComponent";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { InfoComponent } from "../../Common/InfoComponent";
import { ChainModel } from "./ChainModel";
import { ChainService } from "./ChainService";
import { SharedService } from "../../Common/SharedService";
import { GridExportModel } from "../../Common/grid/GridExportModel";

@Component({
  selector: "chain-list",
  templateUrl: "./ChainListComponent.html"
})
export class ComanyListComponent implements OnInit {
  PageTitle = "Chain";
  redirectUrl = "Chain";
  componentName: any;
  serviceDocument: ServiceDocument<ChainModel>;
  public gridOptions: GridOptions;
  columns: any[];
  model: ChainModel;
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;

  constructor(public service: ChainService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "CHAIN ID", field: "hierValue", tooltipField: "hierValue" },
      { headerName: "CHAIN NAME", field: "hierDesc", tooltipField: "hierDesc" },
      { headerName: "MANAGER NAME", field: "mgrName", tooltipField: "mgrName" },    
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 80
      }
    ];
    
    this.model = { hierValue: null, hierLevel: null, hierDesc:null };
    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }
}
