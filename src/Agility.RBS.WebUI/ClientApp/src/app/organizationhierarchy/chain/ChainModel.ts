export class ChainModel { 
  hierValue: number;
  hierLevel?: string;
  hierDesc?: string;
  mgrName?: string;
  currencyCode?: string;
  parentId?: number;
  oldParentId?: number; 
  operation?: string;
  createdUpdatedBy?: string;
  createdUpdatedDate?: Date;
  lastUpdatedBy?: string;
  lastUpdatedDate?: Date;
}
