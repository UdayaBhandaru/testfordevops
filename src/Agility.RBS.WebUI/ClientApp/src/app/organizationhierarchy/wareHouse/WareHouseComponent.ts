import { Component, OnInit, AfterViewInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { FormGroup, Validators, ValidatorFn, AbstractControl } from "@angular/forms";
import { SharedService } from "../../Common/SharedService";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { WareHouseService } from "./WareHouseService";
import { CompanyModel } from "../company/CompanyModel";
import { FormatModel } from "../format/FormatModel";
import { CompanyDomainModel } from "../../Common/DomainData/CompanyDomainModel";
import { OrgCountryDomainModel } from "../../Common/DomainData/OrgCountryDomainModel";
import { RegionDomainModel } from "../../Common/DomainData/RegionDomainModel";
import { FormatDomainModel } from "../../Common/DomainData/FormatDomainModel";
import { VatCodeDomainModel } from "../../Common/DomainData/VatCodeDomainModel";
import { CurrencyDomainModel } from "../../Common/DomainData/CurrencyDomainModel";
import { VatRegionDomainModel } from "../../Common/DomainData/VatRegionDomainModel";
import { LanguageDomainModel } from "../../Common/DomainData/LanguageDomainModel";
import { CommonModel } from "../../Common/CommonModel";
import { UserDomainModel } from "../../Common/DomainData/UserDomainModel";
import { WareHouseModel } from './WareHouseModel';
import { TransferEntityModel } from '../../transfer/TransferEntity/TransferEntityModel';
import { TransferZoneModel } from '../../transfer/transferzone/TransferZoneModel';
import { DistrictModel } from '../district/DistrictModel';
import { VatRegionModel } from '../../valueaddedtax/vatregion/VatRegionModel';
import { LocationTypeDomainModel } from '../../Common/DomainData/LocationTypeDomainModel';

@Component({

  selector: "wareHouse",
  templateUrl: "./WareHouseComponent.html"
})
export class WareHouseComponent implements OnInit, AfterViewInit {
  PageTitle = "WareHouse";
  redirectUrl = "WareHouse";
  serviceDocument: ServiceDocument<WareHouseModel>;
  editMode: boolean = false;
  editModel: WareHouseModel;
  localizationData: any;

  indicatorData: DomainDetailModel[];
  domainDtlData: DomainDetailModel[];
  languageData: LanguageDomainModel[];
  users: UserDomainModel[];
  transferZoneData: TransferZoneModel[];
  transferEntityData: TransferEntityModel[];
  vatRegionCode: VatRegionDomainModel[];
  currencyData: CurrencyDomainModel[];
  storeLocationsData: CommonModel[];
  districtData: DistrictModel[]
  model: WareHouseModel;
  deliveryPolicyData: DomainDetailModel[];
  fromLocationData: LocationTypeDomainModel[];
  fromLocationsFileredList: LocationTypeDomainModel[];
  showCustomRequiredMessageForCloseDate: string;
  showCustomRequiredMessageForSisterStore: string;

  public i: number = 0;
  mandotryFieldList: string[] = [
    "stockHoldingInd",
    "breakPackInd",
    "redistWhInd",
    "redistWhInd",
    "ibInd",
    "protectedInd",
    "forecastWhInd",
    "replInd",
    "autoIbClear",
    "finisherInd"
  ];

  constructor(private service: WareHouseService, private route: ActivatedRoute, private sharedService: SharedService
    , private router: Router) {
  }

  ngOnInit(): void {
    this.route.data.subscribe(() => {
      this.deliveryPolicyData = Object.assign([], this.service.serviceDocument.domainData["deliveryPolicy"]);
      this.indicatorData = Object.assign([], this.service.serviceDocument.domainData["indicatorStatus"]);
      this.transferZoneData = Object.assign([], this.service.serviceDocument.domainData["transferZones"]);
      this.transferEntityData = Object.assign([], this.service.serviceDocument.domainData["transferEntities"]);
      this.languageData = Object.assign([], this.service.serviceDocument.domainData["language"]);
      this.currencyData = Object.assign([], this.service.serviceDocument.domainData["currency"]);
      this.users = Object.assign([], this.service.serviceDocument.domainData["users"]);
      this.storeLocationsData = Object.assign([], this.service.serviceDocument.domainData["storeLocations"]);
      this.vatRegionCode = Object.assign([], this.service.serviceDocument.domainData["vatRegionCode"]);
      this.districtData = Object.assign([], this.service.serviceDocument.domainData["districts"]);
      this.fromLocationsFileredList = Object.assign([], this.service.serviceDocument.domainData["locType"]);

      this.storeLocationsData.forEach(x => x.status = +x.status);

      this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
    });
    this.bind();
  }

  ngAfterViewInit(): void {
    this.addValidators();
  }

  bind(): void {
    //if (this.locationClassData) {
    //  if (this.i === 0) {
    //    this.i = 1;
    //    this.locationClassData1 = Object.assign([], this.locationClassData);
    //    this.locationClassData = this.locationClassData1.sort(function (a: any, b: any): any { return a.domainDetailSeq - b.domainDetailSeq; });
    //  }
    //}
    delete this.editModel;
    this.fromLocationData = this.fromLocationsFileredList.filter(item => item.locType === "W");
    if (this.sharedService.editObject) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
      let dm: WareHouseModel = this.service.serviceDocument.dataProfile.dataModel;      
      this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
      delete this.sharedService.editObject;
      this.editMode = true;
    } else {
      this.model = {
        wh: null,
        whName: null,
        whNameSecondary: null,
        email: null,
        vatRegion: null,
        regionName: null,
        orgHierType: null,
        orgHierValue: null,
        currencyCode: null,
        physicalWh: null,
        primaryVwh: null,
        channelId: null,
        stockHoldingInd: null,
        breakPackInd: null,
        redistWhInd: null,
        deliveryPolicy: null,
        restrictedInd: null,
        protectedInd: null,
        forecastWhInd: null,
        roundingSeq: null,
        replInd: null,
        replWhLink: null,
        replSrcOrd: null,
        ibInd: null,
        ibWhLink: null,
        autoIbClear: null,
        dunsNumber: null,
        dunsLoc: null,
        tsfEntityId: null,
        finisherInd: null,
        inboundHandlingDays: null,
        orgUnitId: null,
        tableName: null,
        error: null,
        operation: "I"
      };
      this.service.serviceDocument.dataProfile.dataModel = this.model;
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  save(saveOnly: boolean): void {
    let fm: FormGroup = this.serviceDocument.dataProfile.profileForm;
    this.mandotryFieldList.forEach(x => fm.controls[x].setValidators([Validators.required]));
    this.mandotryFieldList.forEach(x => fm.controls[x].updateValueAndValidity());
    this.sharedService.validateForm(fm);
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.saveSubscription(saveOnly);
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  addValidators(): void {
    let lfg: FormGroup = this.serviceDocument.dataProfile.profileForm;
    this.mandotryFieldList.forEach(x => lfg.controls[x].setValidators([Validators.required, radioButtonValidator("Required*")]));
    lfg.controls["email"].setValidators([Validators.required, this.sharedService.emailValidator("Invalid Email Address")]);
  }

  saveSubscription(saveOnly: boolean): void {
    this.checkWareHouse(saveOnly);
  }

  saveWareHouse(saveOnly: boolean): void {
   
      this.service.save().subscribe(() => {
        if (this.service.serviceDocument.result.type === MessageType.success) {
          this.sharedService.saveForm("WareHouse saved successfully").subscribe(() => {
            if (saveOnly) {
              this.router.navigate(["/" + this.redirectUrl + "/List"], { skipLocationChange: true });
            } else {
              if (!this.editMode) {
                this.model = new WareHouseModel();
                this.model = {
                  wh: null,
                  whName: null,
                  whNameSecondary: null,
                  email: null,
                  vatRegion: null,
                  regionName: null,
                  orgHierType: null,
                  orgHierValue: null,
                  currencyCode: null,
                  physicalWh: null,
                  primaryVwh: null,
                  channelId: null,
                  stockHoldingInd: null,
                  breakPackInd: null,
                  redistWhInd: null,
                  deliveryPolicy: null,
                  restrictedInd: null,
                  protectedInd: null,
                  forecastWhInd: null,
                  roundingSeq: null,
                  replInd: null,
                  replWhLink: null,
                  replSrcOrd: null,
                  ibInd: null,
                  ibWhLink: null,
                  autoIbClear: null,
                  dunsNumber: null,
                  dunsLoc: null,
                  tsfEntityId: null,
                  finisherInd: null,
                  inboundHandlingDays: null,
                  orgUnitId: null,
                  tableName: null,
                  error: null,
                  operation: "I"
                };
                this.service.newModel(this.model);
              } else {
                Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
                this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.service.serviceDocument.dataProfile.dataModel);
                this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
              }
            }
          });
        } else {
          this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
        }
      });
      this.showCustomRequiredMessageForCloseDate = null;

  
  }

  checkWareHouse(saveOnly: boolean): void {
    if (!this.editMode) {
      let form: FormGroup = this.serviceDocument.dataProfile.profileForm;
      if (form.controls["wh"].value ) {
        this.service.isExistingWareHouse(form.controls["wh"].value).subscribe
          ((response: boolean) => {
            if (response) {
              this.sharedService.errorForm(this.localizationData.location.locationcheck);
            } else {

              this.saveWareHouse(saveOnly);
            }
          });
      }
    } else {

      this.saveWareHouse(saveOnly);
    }
  }

  reset(): void {
    this.showCustomRequiredMessageForCloseDate = null;
    this.showCustomRequiredMessageForSisterStore = null;

    if (!this.editMode) {    
      this.model = {
        wh: null,
        whName: null,
        whNameSecondary: null,
        email: null,
        vatRegion: null,
        regionName: null,
        orgHierType: null,
        orgHierValue: null,
        currencyCode: null,
        physicalWh: null,
        primaryVwh: null,
        channelId: null,
        stockHoldingInd: null,
        breakPackInd: null,
        redistWhInd: null,
        deliveryPolicy: null,
        restrictedInd: null,
        protectedInd: null,
        forecastWhInd: null,
        roundingSeq: null,
        replInd: null,
        replWhLink: null,
        replSrcOrd: null,
        ibInd: null,
        ibWhLink: null,
        autoIbClear: null,
        dunsNumber: null,
        dunsLoc: null,
        tsfEntityId: null,
        finisherInd: null,
        inboundHandlingDays: null,
        orgUnitId: null,
        tableName: null,
        error: null,
        operation: "I"
      };
      this.service.serviceDocument.dataProfile.profileForm.setValue({
        companyId: null, companyName: null, countryCode: null, countryName: null, legalName: null, taxRegNo: null, vatExempt: null
        , interCompany: null, vatCode: null, pluChargeValue: null, defaultWhs: null, operation: "I", currentActionId: null, countryStatus: "A"
      });
      this.service.serviceDocument.dataProfile.dataModel = this.model;
      this.service.newModel(this.model);
    } else {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
      this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
    }
  } 
}

export function radioButtonValidator(errorMessage: string): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } => {
    if (control.value != null && control.value.length > 0) {
      return null;
    } else {
      return { errorMessage };
    }
  };
}
