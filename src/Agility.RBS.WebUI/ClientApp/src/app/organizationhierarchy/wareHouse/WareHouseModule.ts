import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { WareHouseRoutingModule } from "./WareHouseRoutingModule";
import { WareHouseListResolver, WareHouseResolver } from "./WareHouseResolver";
import { WareHouseService } from "./WareHouseService";
import { WareHouseComponent } from "./WareHouseComponent";
import { WareHouseListComponent } from "./WareHouseListComponent";
import { RbsSharedModule } from "../../RbsSharedModule";

@NgModule({
    imports: [
        FrameworkCoreModule,
        WareHouseRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        WareHouseComponent,
        WareHouseListComponent

    ],
    providers: [
        WareHouseService,
        WareHouseListResolver,
        WareHouseResolver
    ]
})
export class WareHouseModule {
    constructor() {
        LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
    }
}
