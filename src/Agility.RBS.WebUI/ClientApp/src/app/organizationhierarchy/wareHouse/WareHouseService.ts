import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { WareHouseModel } from "./WareHouseModel";
import { SharedService } from "../../Common/SharedService";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";

@Injectable()
export class WareHouseService {
  serviceDocument: ServiceDocument<WareHouseModel> = new ServiceDocument<WareHouseModel>();
    constructor(private httpHelperService: HttpHelperService, private sharedService: SharedService) { }

  newModel(model: WareHouseModel): ServiceDocument<WareHouseModel> {
        return this.serviceDocument.newModel(model);
    }

  search(): Observable<ServiceDocument<WareHouseModel>> {
        return this.serviceDocument.search("/api/WareHouse/Search");
    }

  save(): Observable<ServiceDocument<WareHouseModel>> {
        return this.serviceDocument.save("/api/WareHouse/Save", true);
    }

  list(): Observable<ServiceDocument<WareHouseModel>> {
        return this.serviceDocument.list("/api/WareHouse/List");
    }

  addEdit(): Observable<ServiceDocument<WareHouseModel>> {
        return this.serviceDocument.list("/api/WareHouse/New");
    }

  isExistingWareHouse(wh: number): Observable<boolean>
  {
    return this.httpHelperService.get("/api/WareHouse/IsExistingWareHouse", new HttpParams().set("wh", wh.toString()));
  }   
}
