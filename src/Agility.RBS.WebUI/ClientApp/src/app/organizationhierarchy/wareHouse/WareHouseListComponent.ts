import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { GridOptions } from "ag-grid-community";
import { GridEditComponent } from "../../Common/grid/GridEditComponent";
import { InfoComponent } from "../../Common/InfoComponent";
import { WareHouseService } from "./WareHouseService";
import { SharedService } from "../../Common/SharedService";
import { GridExportModel } from "../../Common/grid/GridExportModel";
import { WareHouseModel } from './WareHouseModel';

@Component({
  selector: "ware-house-list",
  templateUrl: "./WareHouseListComponent.html"
})

export class WareHouseListComponent implements OnInit {
  pageTitle = "WareHouse";
  redirectUrl = "WareHouse";
  componentName: any;
  serviceDocument: ServiceDocument<WareHouseModel>;
  public gridOptions: GridOptions;
  columns: any[];
  model: WareHouseModel;
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;

  constructor(public service: WareHouseService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "ware house", field: "wh", tooltipField: "wh" },
      { headerName: "ware house Name", field: "whName", tooltipField: "whName" },
      { headerName: "ware house Name Secondary", field: "whNameSecondary", tooltipField: "whNameSecondary" },
      { headerName: "email", field: "email", tooltipField: "email" },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 110
      }
    ];

    this.model = {
      wh: null,
      whName: null,
      whNameSecondary: null,
      email: null,
      vatRegion: null,
      regionName: null,
      orgHierType: null,
      orgHierValue: null,
      currencyCode: null,
      physicalWh: null,
      primaryVwh: null,
      channelId: null,
      stockHoldingInd: null,
      breakPackInd: null,
      redistWhInd: null,
      deliveryPolicy: null,
      restrictedInd: null,
      protectedInd: null,
      forecastWhInd: null,
      roundingSeq: null,
      replInd: null,
      replWhLink: null,
      replSrcOrd: null,
      ibInd: null,
      ibWhLink: null,
      autoIbClear: null,
      dunsNumber: null,
      dunsLoc: null,
      tsfEntityId: null,
      finisherInd: null,
      inboundHandlingDays: null,
      orgUnitId: null,
      tableName: null,
      error: null,
      operation: null
    };
    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    this.showSearchCriteria = false;
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }
}
