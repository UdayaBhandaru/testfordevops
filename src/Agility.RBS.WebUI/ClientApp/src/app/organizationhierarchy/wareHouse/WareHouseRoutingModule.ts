import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { WareHouseListResolver, WareHouseResolver } from "./WareHouseResolver";
import { WareHouseListComponent } from "./WareHouseListComponent";
import { WareHouseComponent } from "./WareHouseComponent";

const UtilityRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: WareHouseListComponent,
                resolve:
                {
                    serviceDocument: WareHouseListResolver
                }
            },
            {
                path: "New",
                component: WareHouseComponent,
                resolve:
                {
                    serviceDocument: WareHouseResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(UtilityRoutes)
    ]
})
export class WareHouseRoutingModule { }
