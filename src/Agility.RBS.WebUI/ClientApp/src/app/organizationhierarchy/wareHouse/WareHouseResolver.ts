import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { WareHouseModel } from "./WareHouseModel";
import { WareHouseService } from "./WareHouseService";

@Injectable()
export class WareHouseListResolver implements Resolve<ServiceDocument<WareHouseModel>> {
    constructor(private service: WareHouseService) { }
  resolve(): Observable<ServiceDocument<WareHouseModel>> {
        return this.service.list();
    }
}

@Injectable()
export class WareHouseResolver implements Resolve<ServiceDocument<WareHouseModel>> {
    constructor(private service: WareHouseService) { }
  resolve(): Observable<ServiceDocument<WareHouseModel>> {
        return this.service.addEdit();
    }
}

