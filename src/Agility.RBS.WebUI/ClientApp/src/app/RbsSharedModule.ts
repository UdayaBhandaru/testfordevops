import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { DateAdapter, MatAutocompleteModule, MatButtonModule, MatCardModule, MatCheckboxModule, MatChipsModule, MatCommonModule, MatDatepickerModule, MatDialogModule, MatExpansionModule, MatFormFieldModule, MatIconModule, MatInputModule, MatNativeDateModule, MatRadioModule, MatSelectModule, MatSidenavModule, MatSidenav, MatSnackBarModule, MatTabsModule, MatTooltipModule, NativeDateAdapter, } from "@angular/material";
import { FlexLayoutModule } from "@angular/flex-layout";
import { UpperCaseTextDirective } from "./Common/controls/RbUpperCaseInputDirective";

@NgModule({
    imports: [
        MatAutocompleteModule, FlexLayoutModule,
        MatCardModule,
        MatCheckboxModule,
        MatButtonModule,
        MatChipsModule,
        MatCommonModule,
        MatSelectModule,
        MatDatepickerModule,
        MatDialogModule,
        MatInputModule,
        MatSnackBarModule,
        MatTooltipModule,
        MatTabsModule,        
        MatNativeDateModule,
        MatTooltipModule,
        MatIconModule,
        MatTabsModule,
        MatExpansionModule,
        MatFormFieldModule,
        MatRadioModule, ReactiveFormsModule
    ],
    declarations: [
        UpperCaseTextDirective
    ],
    exports: [
        MatAutocompleteModule, ReactiveFormsModule, FlexLayoutModule,
        MatCardModule,
        MatCheckboxModule,
        MatButtonModule,
        MatChipsModule,
        MatCommonModule,
        MatSelectModule,
        MatDatepickerModule,
        MatDialogModule,
        MatInputModule,
        MatSnackBarModule,
        MatTooltipModule,
        MatIconModule,
        MatTabsModule,
        MatNativeDateModule,
        MatTooltipModule,
        MatTabsModule,
        MatExpansionModule,
        MatFormFieldModule,
        MatRadioModule, UpperCaseTextDirective
    ]
})
export class RbsSharedModule {
}
