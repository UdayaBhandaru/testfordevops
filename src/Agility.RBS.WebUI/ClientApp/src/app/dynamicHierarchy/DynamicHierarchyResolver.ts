import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { DynamicHierarchyService } from './DynamicHierarchyService';
import { DynamicHierarchyModel } from './DynamicHierarchyModel';



@Injectable()
export class DynamicHierarchyListResolver implements Resolve<ServiceDocument<DynamicHierarchyModel>> {
  constructor(private service: DynamicHierarchyService) { }
  resolve(): Observable<ServiceDocument<DynamicHierarchyModel>> {
        return this.service.list();
    }
}

@Injectable()
export class DynamicHierarchyResolver implements Resolve<ServiceDocument<DynamicHierarchyModel>> {
  constructor(private service: DynamicHierarchyService) { }
  resolve(route: ActivatedRouteSnapshot): Observable<ServiceDocument<DynamicHierarchyModel>> {
    return this.service.addEdit(route.params["id"]);
  }
}
