import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { HttpParams } from "@angular/common/http";
import { DynamicHierarchyModel } from './DynamicHierarchyModel';

@Injectable()
export class DynamicHierarchyService {
  serviceDocument: ServiceDocument<DynamicHierarchyModel> = new ServiceDocument<DynamicHierarchyModel>();

  constructor() { }

  newModel(model: DynamicHierarchyModel): ServiceDocument<DynamicHierarchyModel> {
    return this.serviceDocument.newModel(model);
  }
  save(): Observable<ServiceDocument<DynamicHierarchyModel>> {
    return this.serviceDocument.save("/api/DynamicHierarchy/Save", true);
  }


  addEdit(id: number): Observable<ServiceDocument<DynamicHierarchyModel>> {
    if (+id === 0) {
      return this.serviceDocument.new("/api/DynamicHierarchy/New");
    } else {
      return this.serviceDocument.open("/api/DynamicHierarchy/Open", new HttpParams().set("id", id.toString()));
    }
  }

  search(): Observable<ServiceDocument<DynamicHierarchyModel>> {
    return this.serviceDocument.search("/api/DynamicHierarchy/Search", true);
  }

  list(): Observable<ServiceDocument<DynamicHierarchyModel>> {
    return this.serviceDocument.list("/api/DynamicHierarchy/List");
  }
}
