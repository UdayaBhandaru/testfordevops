import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { DynamicHierarchyResolver, DynamicHierarchyListResolver } from './DynamicHierarchyResolver';
import { DynamicHierarchyListComponent } from './DynamicHierarchyListComponent';
import { DynamicHierarchyComponent } from './DynamicHierarchyComponent';

const PromotionRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "", redirectTo: "List", pathMatch: "full"
      },
      {
        path: "List",
        component: DynamicHierarchyListComponent,
        resolve:
        {
          serviceDocument: DynamicHierarchyListResolver
        }
      },
      {
        path: "New/:id",
        component: DynamicHierarchyComponent,
        resolve: {
          serviceDocument: DynamicHierarchyResolver
        },
      }
    ]
  }
];

@NgModule({
    imports: [
        RouterModule.forChild(PromotionRoutes)
    ]
})
export class DynamicHierarchyRoutingModule { }

