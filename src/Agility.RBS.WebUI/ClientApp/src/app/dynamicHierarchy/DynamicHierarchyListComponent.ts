import { Component, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { GridOptions, ColDef } from "ag-grid-community";
import { SearchComponent } from '../Common/SearchComponent';
import { SharedService } from '../Common/SharedService';
import { GridEditComponent } from '../Common/grid/GridEditComponent';
import { InfoComponent } from '../Common/InfoComponent';
import { DynamicHierarchyModel } from './DynamicHierarchyModel';
import { DynamicHierarchyService } from './DynamicHierarchyService';


@Component({
  selector: "dynamic-hierarchy-list",
  templateUrl: "./DynamicHierarchyListComponent.html"
})

export class DynamicHierarchyListComponent implements OnInit {
  pageTitle = "Dynamic Hierarchy";
  redirectUrl = "DynamicHierarchy/New/";
  serviceDocument: ServiceDocument<DynamicHierarchyModel>;
  private gridOptions: GridOptions;
  columns: ColDef[];
  model: DynamicHierarchyModel;
  isSearchParamGiven: boolean = false;
  @ViewChild("searchPanelPromotion") searchPanelPromotion: SearchComponent;
  gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  defaultDateType: string;
  componentName: this;
  searchProps = ["promO_EVENT_ID", "theme", "startDate", "endDate", "description"];
  constructor(public service: DynamicHierarchyService, public sharedService: SharedService, public router: Router) {
  }

  ngOnInit(): void {

    this.componentName = this;
    this.columns =
      [
      { headerName: "rms Name", field: "rmsName", tooltipField: "rmsName", width: 50, headerTooltip: "rmsName" },
      { headerName: "client Name", field: "clientName", tooltipField: "clientName", width: 40, headerTooltip: "clientName" },
      { headerName: "abbrName", field: "abbrName", tooltipField: "abbrName", headerTooltip: "abbrName" },     
      {
        headerName: "ACTIONS", field: "Actions", cellRendererFramework: GridEditComponent, width: 60
      }
      ];
    this.model = { rmsName: null, clientName: null, abbrName: null}
    this.service.newModel(this.model);
    this.serviceDocument = this.service.serviceDocument;
  }



  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
      this.sharedService.previousUrl = "../DynamicHierarchy/List";
    }

    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl + cell.data.rmsName
      , this.service.serviceDocument.dataProfile.dataList);
  }

  addValidation(evnt: any): void {
    this.navigateToAdd();
  }

  private navigateToAdd(): void {
    this.router.navigate(["/" + this.redirectUrl + "0"], { skipLocationChange: true });
    this.sharedService.previousUrl = "../DynamicHierarchy/List";
  }
}
