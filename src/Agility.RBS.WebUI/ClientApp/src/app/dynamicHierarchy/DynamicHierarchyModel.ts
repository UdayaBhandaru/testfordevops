export class DynamicHierarchyModel {
  rmsName: string;
  clientName: string;
  abbrName: string; 
  operation?: string;
}
