import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { GridOptions } from "ag-grid-community";
import { GridEditComponent } from "../../Common/grid/GridEditComponent";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { InfoComponent } from "../../Common/InfoComponent";
import { SeasonModel } from "./SeasonModel";
import { SeasonService } from "./SeasonService";
import { SharedService } from "../../Common/SharedService";
import { GridExportModel } from "../../Common/grid/GridExportModel";
import { PhaseDetailModel } from './PhaseDetailModel';
import { CurrencyDomainModel } from '../../Common/DomainData/CurrencyDomainModel';
import { CommonModel } from '../../Common/CommonModel';
import { DepartmentDomainModel } from '../../Common/DomainData/DepartmentDomainModel';
import { GridDateComponent } from '../../Common/grid/GridDateComponent';
import { CompanyModel } from '../../organizationhierarchy/company/CompanyModel';

@Component({
  selector: "Season-list",
  templateUrl: "./SeasonListComponent.html"
})
export class SeasonListComponent implements OnInit {
  PageTitle = "Season";
  redirectUrl = "Season";
  componentName: any;
  serviceDocument: ServiceDocument<SeasonModel>;
  public gridOptions: GridOptions;
  columns: any[];
  model: SeasonModel;
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  domainDtlData: DomainDetailModel[];
  locationData: PhaseDetailModel[];
  locationSelectedData: PhaseDetailModel[] = [];
  companyData: CompanyModel[];

  constructor(public service: SeasonService, private sharedService: SharedService, private route: ActivatedRoute,
    private router: Router) {
  }

  ngOnInit(): void {

    

    this.componentName = this;
    this.columns = [
      { headerName: "ID", field: "seasonId", tooltipField: "seasonId", width: 50 },
      { headerName: "Name", field: "seasonName", tooltipField: "seasonName" },
      { headerName: "Start Date", field: "startDate", tooltipField: "startDate", width: 75 },
      { headerName: "End Date", field: "endDate", tooltipField: "endDate", width: 75 },
      { headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 60 }
    ];
    this.route.data.subscribe(() => {
      this.loadSeasonDomainData();
      this.setZopneModel();
      if (this.sharedService.searchData) {
        this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData, this.sharedService.searchData.Zone);
        this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
      } else {
        this.service.newModel(this.model);
      }
      this.serviceDocument = this.service.serviceDocument;
    });
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }

  private loadSeasonDomainData(): void {
    this.companyData = this.service.serviceDocument.domainData["company"];
    this.sharedService.domainData = {
      seasonStatusDomainData: this.domainDtlData, seasonLocDrdDomainData: this.locationData
    };
  }

  private setZopneModel(): void {
    this.model = new SeasonModel();
    this.seasonHeaderModel();
  }

  cleanUp(): void {
    this.locationSelectedData = [];
  }
  private seasonHeaderModel() {
    this.model = {
      companyId: null, seasonId: null, seasonName: null, startDate: null, endDate: null, companyName: null, phaseDetailList: [], operation: "I" 
    };
  }
}
