import { Component, OnInit } from "@angular/core";
import { ServiceDocument, FormMode, CommonService, MessageResult, MessageType } from "@agility/frameworkcore";
import { Router } from "@angular/router";
import { FormControl } from "@angular/forms";
import { DomainComponent } from '../../domain/DomainComponent';
import { GridOptions } from 'ag-grid-community';
import { SeasonModel } from './SeasonModel';
import { PhaseDetailModel } from './PhaseDetailModel';
import { SeasonService } from './SeasonService';
import { SharedService } from '../../Common/SharedService';
import { GridInputComponent } from '../../Common/grid/GridInputComponent';
import { IndicatorComponent } from '../../Common/grid/IndicatorComponent';
import { GridEditComponent } from '../../Common/grid/GridEditComponent';
import { DepartmentDomainModel } from '../../Common/DomainData/DepartmentDomainModel';
import { DomainDetailModel } from '../../domain/DomainDetailModel';
import { GridSelectComponent } from '../../Common/grid/GridSelectComponent';
import { GridNumericComponent } from '../../Common/grid/GridNumericComponent';
import { GridDateComponent } from '../../Common/grid/GridDateComponent';
import { CompanyModel } from '../../organizationhierarchy/company/CompanyModel';

@Component({
  selector: "season",
  templateUrl: "./SeasonComponent.html"  
})
export class SeasonComponent implements OnInit {
  gridApi: any;
  PageTitle = "Season";
  sheetName: string = "Season";
  redirectUrl = "Season";
  public gridOptions: GridOptions;
  serviceDocument: ServiceDocument<SeasonModel>;
  headermodel: SeasonModel;
  detailModel: PhaseDetailModel;
  columns: any[];
  data: any[] = [];
  sno: number = 0;
  domainDtlData: PhaseDetailModel[];
  temp: {};
  editMode: boolean = false;
  editModel: any;
  private messageResult: MessageResult = new MessageResult();
  componentParentName: SeasonComponent;
  localizationData: any;
  companyData: CompanyModel[];

  constructor(private service: SeasonService, private commonService: CommonService,
    private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.companyData = this.service.serviceDocument.domainData["company"];
    this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
    this.bind();
    this.componentParentName = this;
  }

  bind(): void {
    this.columns = [
      //{
      //  headerName: "company", field: "companyId", cellRendererFramework: GridSelectComponent, width: 150,
      //  cellRendererParams: { references: this.companyData, keyvaluepair: { key: "companyId", value: "companyNameDesc" } }
      //}, 

      { headerName: "phase Id", field: "phaseId", cellRendererFramework: GridNumericComponent },
      { headerName: "phase Name", field: "phaseName", cellRendererFramework: GridInputComponent },
      {
        headerName: "startDate", field: "startDate", tooltipField: "startDate | date:'dd-MMM - yyyy'"
        , cellRendererFramework: GridDateComponent, filter: "date", suppressSizeToFit: true, width: 150
      },
      {
        headerName: "endDate", field: "endDate", tooltipField: "endDate | date:'dd-MMM - yyyy'"
        , cellRendererFramework: GridDateComponent, filter: "date", suppressSizeToFit: true, width: 150
      },

    ];

    this.gridOptions = {
      onGridReady: (params: any) => {
        this.gridApi = params.api;
        this.gridApi.startEditingCell({
          rowIndex: 0,
          colKey: "codeSeq"
        });
      },
      context: {
        componentParent: this
      }
    };

    delete this.editModel;
    if (this.sharedService.editObject) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
      this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
      let arr: PhaseDetailModel[] = [];
      this.service.serviceDocument.dataProfile.dataModel.phaseDetailList.forEach((x) => {
        arr.push(Object.assign({}, x));
      });
      this.editModel.phaseDetailList = arr;
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
      this.data = this.service.serviceDocument.dataProfile.dataModel.phaseDetailList;
      delete this.sharedService.editObject;
      this.editMode = true;

    } else {
      this.headModelProperties();
      this.service.serviceDocument.dataProfile.dataModel = this.headermodel;
      this.data = this.service.serviceDocument.dataProfile.dataModel.phaseDetailList;
      this.service.newModel(this.headermodel);
    }
    this.service.serviceDocument.dataProfile.profileForm = this.commonService
      .getFormGroup(this.service.serviceDocument.dataProfile.dataModel, FormMode.Open);
    this.service.serviceDocument.dataProfile.profileForm.controls["phaseDetailList"] = new FormControl();
    this.serviceDocument = this.service.serviceDocument;
  }

  save(saveOnly: boolean): void {
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.saveSubscription(saveOnly);
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  saveAndContinue(): void {
    this.save(false);
  }

  add($event: MouseEvent): void {
    $event.preventDefault();
    this.sno = this.data.length > 0 ? this.data.slice(0).sort((a: any, b: any) => { return a.codeSeq - b.codeSeq; })
      .reverse()[0].codeSeq : this.sno;
    this.sno = +(this.sno) + 1;
    if (this.data.length === 0) {
      this.service.serviceDocument.dataProfile.dataModel.phaseDetailList = [];
    }
    if (this.temp && Object.keys(this.temp).filter((key: string) => {
      return key !== "organizationId" && key !== "workflowInstance" && key !== "workflowInstanceId" && key !== "workflowStateId" && key !== "tenantId";
    }).map(key => this.temp[key]).some((val: any) => { return val === "" || val === null; })) {
      this.sharedService.errorForm(this.localizationData.domain.domaindetailsall);
    } else {
      if (this.data.length === 0) {
        this.data.push({
          "phaseId": null, "phaseName": null, "startDate": null, "endDate": null, "operation": "I"
        });
        this.gridApi.setRowData(this.data);
      } else {
        this.data.push({
          "phaseId": null, "phaseName": null, "startDate": null, "endDate": null, "operation": "I"
        });
        this.gridApi.setRowData(this.data);
      }
    }
  }

  delete(cell: any): void {
    if (this.data[cell.rowIndex] && this.data[cell.rowIndex].createdBy) {
      this.data[cell.rowIndex]["domainStatus"] = "I";
    } else {
      this.data.splice(cell.rowIndex, 1);
    }
    this.gridApi.setRowData(this.data);
  }

  reset(): void {
    if (!this.editMode) {
      this.sno = 0;
      this.headModelProperties();
       this.service.serviceDocument.dataProfile.dataModel = this.headermodel;
      this.data = this.service.serviceDocument.dataProfile.dataModel.phaseDetailList;
      this.service.newModel(this.headermodel);
    } else {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);

      let arr: PhaseDetailModel[] = [];
      this.editModel.phaseDetailList.forEach((x) => {
        arr.push(Object.assign({}, x));
      });
      this.service.serviceDocument.dataProfile.dataModel.phaseDetailList = arr;
      this.data = this.service.serviceDocument.dataProfile.dataModel.phaseDetailList;
    }
    this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
  }

  checkDomain(): boolean {
    if (!this.editMode) {
      let detailId: any = this.serviceDocument.dataProfile.profileForm.controls["domainId"].value;
      if (detailId) {
        this.service.isExistingSeason(detailId).subscribe((response: boolean) => {
          if (response) {
            this.sharedService.errorForm(this.localizationData.domain.domaindatacheck);
            this.serviceDocument.dataProfile.profileForm.controls["domainId"].setValue(null);
          }
        });
      }
    }
    return false;
  }

  change(cell: any, event: any): void {
    if (event.target) {
      if (event.target.checked) {
        this.data.forEach((detail: PhaseDetailModel) => { detail.seasonName = "N"; });
      }
      this.data[cell.rowIndex]["requiredInd"] = event.target.checked ? "Y" : "N";
    }
    this.temp = this.data[cell.rowIndex];
    this.gridApi.setRowData(this.data);
  }

  update(cell: any, event: any): void {
    if (cell.column.colId === "codeSeq" && this.data.some((data: PhaseDetailModel, index: number) => {
      return +data.seasonId === +event.target.value && index !== cell.rowIndex;
    })) {
      this.sharedService.errorForm(this.localizationData.domain.domainsequencecheck);
      event.target.value = null;
      this.data[cell.rowIndex]["codeSeq"] = null;
      return;
    } else if (cell.column.colId === "code" && this.data.some((data: PhaseDetailModel, index: number) => {
      return data.seasonId === event.target.value && index !== cell.rowIndex;
    })) {
      this.sharedService.errorForm(this.localizationData.domain.domaindetailscheck);
      event.target.value = null;
      this.data[cell.rowIndex]["code"] = null;
      return;
    } else {
      this.data[cell.rowIndex][cell.column.colId] = event.target.value;
    }
    this.temp = this.data[cell.rowIndex];
  }

  select(cell: any, event: any): void {
    this.data[cell.rowIndex][cell.column.colId] = event.target.value;
    this.temp = this.data[cell.rowIndex];
  }

  saveSubscription(saveOnly: boolean): void {
    let valid: boolean = true;
    let emptyProperties: string = "<br/><ul>";
    this.data.forEach((item: PhaseDetailModel) => {
      Object.keys(item).forEach((prop: string) => {
        if (prop !== "organizationId" && prop !== "workflowInstance" && prop !== "workflowInstanceId" && prop !== "workflowStateId"
          && prop !== "tenantId" && prop !== "createdBy" && prop !== "createdDate" && prop !== "modifiedBy" && prop !== "modifiedDate"
          && prop !== "season" && prop !== "seasonName" && prop !== "deptName" ) {
          if (item[prop] === "" || item[prop] === null) {
            emptyProperties += emptyProperties.indexOf(prop.toUpperCase()) > -1 ? "" : "<li>" + prop.toUpperCase() + "</li>";
            valid = false;
          }
        }
      });
      emptyProperties += "</ul>";
    });
    if (!valid) {
      this.messageResult.message = this.localizationData.domain.domaindetails + emptyProperties.slice(0, -1);
      this.sharedService.errorForm(this.messageResult.message);
    } else if (this.data.length == 0  ) {
      this.sharedService.errorForm(this.localizationData.domain.domaindefault);
    } else {
      this.serviceDocument.dataProfile.profileForm.controls["phaseDetailList"] = new FormControl();
      this.serviceDocument.dataProfile.profileForm.controls["phaseDetailList"].setValue(this.data);
      this.service.save().subscribe(() => {
        if (this.service.serviceDocument.result.type === MessageType.success) {
          this.sharedService.saveForm("Fine Line saved successfully").subscribe(() => {
            if (saveOnly) {
              this.router.navigate(["/" + this.redirectUrl + "/List"], { skipLocationChange: true });
            } else {
              if (!this.editMode) {
                this.sno = 0;
                this.headModelProperties();
                 this.service.serviceDocument.dataProfile.dataModel = this.headermodel;
                this.data = this.service.serviceDocument.dataProfile.dataModel.phaseDetailList;
                this.service.newModel(this.headermodel);
              } else {
                Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
                this.data = this.service.serviceDocument.dataProfile.dataModel.phaseDetailList;
                this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.service.serviceDocument.dataProfile.dataModel);
                this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
              }
            }
          }, (error: string) => { console.log(error); });
        } else {
          this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
        }
      }, (error: string) => { console.log(error); });
    }
  }



  private headModelProperties()
  {
    this.headermodel = { companyId: null, seasonId: null, seasonName: null, startDate: null, endDate: null, companyName:null, phaseDetailList: [], operation: "I" };
   
  }
  private detailModelProperties()
  {

    this.detailModel = { companyId: null, seasonId: null, seasonName: null, phaseId: null, phaseName: null, startDate: null, endDate: null, companyName:null, operation: "I" };

  }
}
