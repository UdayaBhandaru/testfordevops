import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { SeasonModel } from "./SeasonModel";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";

@Injectable()
export class SeasonService {
    serviceDocument: ServiceDocument<SeasonModel> = new ServiceDocument<SeasonModel>();
    constructor(private httpHelperService: HttpHelperService) { }

    newModel(model: SeasonModel): ServiceDocument<SeasonModel> {
        return this.serviceDocument.newModel(model);
    }

    search(): Observable<ServiceDocument<SeasonModel>> {
      return this.serviceDocument.search("/api/Season/Search");
    }

    save(): Observable<ServiceDocument<SeasonModel>> {
      return this.serviceDocument.save("/api/Season/Save", true);
    }

    list(): Observable<ServiceDocument<SeasonModel>> {
      return this.serviceDocument.list("/api/Season/List");
    }

    addEdit(): Observable<ServiceDocument<SeasonModel>> {
      return this.serviceDocument.list("/api/Season/New");
    }

  isExistingSeason(zoneGroupId: string): Observable<boolean> {
    return this.httpHelperService.get("/api/Season/IsExistingSeason", new HttpParams().set("zoneGroupId", zoneGroupId));
    }
}
