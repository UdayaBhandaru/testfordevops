import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { SeasonService } from "./SeasonService";
import { SeasonModel } from "./SeasonModel";
@Injectable()
export class SeasonListResolver implements Resolve<ServiceDocument<SeasonModel>> {
    constructor(private service: SeasonService) { }
    resolve(): Observable<ServiceDocument<SeasonModel>> {
        return this.service.list();
    }
}

@Injectable()
export class SeasonResolver implements Resolve<ServiceDocument<SeasonModel>> {
    constructor(private service: SeasonService) { }
    resolve(): Observable<ServiceDocument<SeasonModel>> {
        return this.service.addEdit();
    }
}



