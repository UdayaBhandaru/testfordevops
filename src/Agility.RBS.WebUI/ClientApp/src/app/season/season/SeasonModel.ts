import { PhaseDetailModel } from './PhaseDetailModel';

export class SeasonModel {
  companyId?: number;
  seasonId?: number;
  seasonName: string;
  startDate?: Date;
  endDate?: Date;
  operation?: string;
  companyName?: string;
  phaseDetailList?:
  PhaseDetailModel[];
}
