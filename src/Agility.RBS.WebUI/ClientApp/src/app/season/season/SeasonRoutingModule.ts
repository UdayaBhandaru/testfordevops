import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { SeasonListComponent } from "./SeasonListComponent";
import { SeasonListResolver, SeasonResolver} from "./SeasonResolver";
import { SeasonComponent } from "./SeasonComponent";
const UtilityRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
              component: SeasonListComponent,
                resolve:
                {
                    serviceDocument: SeasonListResolver
                }
            },
            {
                path: "New",
                component: SeasonComponent,
                resolve:
                {
                    serviceDocument: SeasonResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(UtilityRoutes)
    ]
})
export class SeasonRoutingModule { }
