import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { SeasonListComponent } from "./SeasonListComponent";
import { SeasonService } from "./SeasonService";
import { SeasonRoutingModule } from "./SeasonRoutingModule";
import { SeasonComponent } from "./SeasonComponent";
import { SeasonListResolver, SeasonResolver } from "./SeasonResolver";
import { RbsSharedModule } from "../../RbsSharedModule";

@NgModule({
    imports: [
        FrameworkCoreModule,
        SeasonRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        SeasonListComponent,
        SeasonComponent,
    ],
    providers: [
        SeasonService,
        SeasonListResolver,
        SeasonResolver,
    ]
})
export class SeasonModule {
    constructor() {
        LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
    }
}
