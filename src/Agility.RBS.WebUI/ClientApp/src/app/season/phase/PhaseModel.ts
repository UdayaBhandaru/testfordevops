export class PhaseModel {
    companyId: number;
    seasonId: number;
    seasonName?: string;
    phaseId?: number;
    phaseName?: string;
    startDate?: Date;
    endDate?: Date;
    operation?: string;
    companyName?: string;
}
