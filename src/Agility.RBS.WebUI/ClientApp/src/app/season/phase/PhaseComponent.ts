import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { SharedService } from "../../Common/SharedService";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { PhaseService } from "./PhaseService";
import { PhaseModel } from "./PhaseModel";
import { SeasonModel } from "../Season/SeasonModel";
import { CompanyModel } from "../../organizationhierarchy/company/CompanyModel";
import { VatRateModel } from "../../Valueaddedtax/vatrates/VatRateModel";
import { SeasonDomainModel } from '../../Common/DomainData/SeasonDomainModel';

@Component({
    selector: "phase",
    templateUrl: "./PhaseComponent.html"
})
export class PhaseComponent implements OnInit {
    PageTitle = "Phase";
    redirectUrl = "Phase";
    serviceDocument: ServiceDocument<PhaseModel>;
    editMode: boolean = false;
    editModel: any;
    localizationData: any;
    companyData: CompanyModel[];
    seasonData: SeasonModel[];
    model: PhaseModel;
    seasonModel: SeasonModel;
    constructor(private service: PhaseService, private sharedService: SharedService, private router: Router) {
    }

    ngOnInit(): void {
        this.companyData = Object.assign([], this.service.serviceDocument.domainData["company"]);
        this.seasonData = Object.assign([], this.service.serviceDocument.domainData["season"]);
        this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
        this.bind();
    }

    bind(): void {
        delete this.editModel;
        if (this.sharedService.editMode) {
            if (this.sharedService.editObject) {
                this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
                this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
                this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
                delete this.sharedService.editObject;
            }
            this.editMode = true;
        } else {
            if (this.sharedService.searchData) {
                let sd: any = this.sharedService.searchData;
                this.model = {
                    companyId: sd.companyId, companyName: sd.companyName, seasonId: sd.seasonId, phaseName: sd.phaseName
                    , startDate: sd.startDate, endDate: sd.endDate, operation: "I"
                };
            } else {
                this.model = {
                    companyId: null, companyName: null, seasonId: null, phaseName: null, startDate: null, endDate: null
                    , operation: "I"
                };
            }
            this.service.serviceDocument.dataProfile.dataModel = this.model;
            this.service.newModel(this.model);
        }
        this.serviceDocument = this.service.serviceDocument;
    }

    save(saveOnly: boolean): void {
        if (this.serviceDocument.dataProfile.profileForm.valid) {
            this.saveSubscription(saveOnly);
        } else {
            this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
        }
    }

    saveSubscription(saveOnly: boolean): void {
        this.checkPhase(saveOnly);
    }
    savePhase(saveOnly: boolean): void {
        this.service.save().subscribe(() => {
            if (this.service.serviceDocument.result.type === MessageType.success) {
                this.sharedService.saveForm(this.localizationData.phase.phasesave).subscribe(() => {
                    if (saveOnly) {
                        this.router.navigate(["/" + this.redirectUrl + "/List"], { skipLocationChange: true });
                    } else {
                        if (!this.editMode) {
                            this.model = {
                                companyId: null, companyName: null, seasonId: null, phaseName: null, startDate: null, endDate: null
                                , operation: "I"
                            };
                            this.service.newModel(this.model);
                        } else {
                            Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
                            this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.service.serviceDocument.dataProfile.dataModel);
                            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
                        }
                    }
                }, (error: string) => { console.log(error); });
            } else {
                this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
            }
        }, (error: string) => { console.log(error); });
    }

    //changeCompany(): void {
    //    this.serviceDocument.dataProfile.profileForm.controls["seasonId"].setValue(null);
    //    this.seasonData = this.seasonList.filter(item => item.companyId === +this.serviceDocument.dataProfile.profileForm.controls["companyId"].value
    //        && item.seasonStatus === "A");
    //}

    checkPhase(saveOnly: boolean): void {
        if (!this.editMode) {
            let form: any = this.serviceDocument.dataProfile.profileForm;
            if (form.controls["seasonId"].value && form.controls["phaseName"].value) {
                this.service.isExistingPhase(form.controls["seasonId"].value, form.controls["phaseName"].value).subscribe
                    ((response: boolean) => {
                        if (response) {
                            this.sharedService.errorForm(this.localizationData.phase.phasecheck);
                        } else {
                            this.savePhase(saveOnly);
                        }
                    });
            }
        } else {
            this.savePhase(saveOnly);
        }
    }

    reset(): void {
        if (!this.editMode) {
            this.model = {
                companyId: null, companyName: null, seasonId: null, phaseName: null, startDate: null, endDate: null, operation: "I"
             };
            this.service.newModel(this.model);
        } else {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
        }
        this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
  }

  openSeasonMaster($event: MouseEvent): void {
    $event.preventDefault();
    this.seasonModel = {
      companyId: this.service.serviceDocument.dataProfile.dataModel.companyId,
      seasonId: this.service.serviceDocument.dataProfile.dataModel.seasonId,
      seasonName: null,
      startDate: this.service.serviceDocument.dataProfile.dataModel.startDate,
      endDate: this.service.serviceDocument.dataProfile.dataModel.endDate
    };
    this.sharedService.searchData = Object.assign({}, this.seasonModel);
    this.router.navigate(["/Season"], { skipLocationChange: true });
  }
}
