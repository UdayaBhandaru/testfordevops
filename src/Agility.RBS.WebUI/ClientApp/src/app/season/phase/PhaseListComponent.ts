import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { GridOptions } from "ag-grid-community";
import { GridEditComponent } from "../../Common/grid/GridEditComponent";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { InfoComponent } from "../../Common/InfoComponent";
import { SharedService } from "../../Common/SharedService";
import { GridExportModel } from "../../Common/grid/GridExportModel";
import { PhaseModel } from "./PhaseModel";
import { PhaseService } from "./PhaseService";
import { SeasonModel } from "../season/SeasonModel";
import { CompanyModel } from "../../organizationhierarchy/company/CompanyModel";
import { GridDateComponent } from "../../Common/grid/GridDateComponent";

@Component({
  selector: "phase-list",
  templateUrl: "./PhaseListComponent.html"
})

export class PhaseListComponent implements OnInit {
  PageTitle = "Phase";
  redirectUrl = "Phase";
  sheetName: string = "PhaseMaster";
  componentName: any;
  domainDtlData: DomainDetailModel[];
  companyData: CompanyModel[];
  serviceDocument: ServiceDocument<PhaseModel>;
  seasonData: SeasonModel[];
  seasonList: SeasonModel[];
  columns: any[];
  model: PhaseModel;
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  public gridOptions: GridOptions;

  constructor(public service: PhaseService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "SEASON NAME", field: "seasonName", tooltipField: "seasonName", width: 140 },
      { headerName: "PHASE NAME", field: "phaseName", tooltipField: "phaseName", width: 140 },
      { headerName: "START DATE", field: "startDate", tooltipField: "startDate", cellRendererFramework: GridDateComponent, width: 75 },
      { headerName: "END DATE", field: "endDate", tooltipField: "endDate", cellRendererFramework: GridDateComponent, width: 75 },
      {
        headerName: "ACTIONS", field: "Actions", cellRendererFramework: GridEditComponent, width: 80
      }
    ];

    this.companyData = this.service.serviceDocument.domainData["company"];
    this.domainDtlData = this.service.serviceDocument.domainData["status"];
    this.seasonList = this.service.serviceDocument.domainData["season"];

    this.sharedService.domainData["company"] = this.companyData;
    this.sharedService.domainData["season"] = this.seasonList;
    this.model = {
      companyId: null, companyName: null, seasonId: null, phaseName: null, startDate: null, endDate: null, operation: null      
    };
    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }

  changeCompany(): void {
    this.serviceDocument.dataProfile.profileForm.controls["seasonId"].setValue(null);
    this.seasonData = this.seasonList.filter(item => item.companyId === +this.serviceDocument.dataProfile.profileForm.controls["companyId"].value);
  }

  reset(): void {
    this.seasonData = [];
  }
}
