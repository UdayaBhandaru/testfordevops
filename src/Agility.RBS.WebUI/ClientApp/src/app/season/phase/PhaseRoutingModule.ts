﻿import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { PhaseListResolver, PhaseResolver } from "./PhaseResolver";
import { PhaseListComponent } from "./PhaseListComponent";
import { PhaseComponent } from "./PhaseComponent";
const SeasonRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: PhaseListComponent,
                resolve:
                {
                    serviceDocument: PhaseListResolver
                }
            },
            {
                path: "New",
                component: PhaseComponent,
                resolve:
                {
                    serviceDocument: PhaseResolver
                }
            }
        ]
    }
];
@NgModule({
    imports: [
        RouterModule.forChild(SeasonRoutes)
    ]
})
export class PhaseRoutingModule { }
