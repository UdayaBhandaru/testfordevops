import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { PhaseModel } from "./PhaseModel";
import { SharedService } from "../../Common/SharedService";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";

@Injectable()
export class PhaseService {
    serviceDocument: ServiceDocument<PhaseModel> = new ServiceDocument<PhaseModel>();

    constructor(private httpHelperService: HttpHelperService, private sharedService: SharedService) { }

    newModel(model: PhaseModel): ServiceDocument<PhaseModel> {
        return this.serviceDocument.newModel(model);
    }

    search(): Observable<ServiceDocument<PhaseModel>> {
        return this.serviceDocument.search("/api/phase/Search");
    }

    save(): Observable<ServiceDocument<PhaseModel>> {
        return this.serviceDocument.save("/api/phase/Save", true, () => this.setDate());
    }

    list(): Observable<ServiceDocument<PhaseModel>> {
        return this.serviceDocument.list("/api/phase/List");
    }

    addEdit(): Observable<ServiceDocument<PhaseModel>> {
        return this.serviceDocument.list("/api/phase/New");
    }

    isExistingPhase(seasonId: number, phaseName: string): Observable<boolean> {
        return this.httpHelperService.get("/api/phase/IsExistingPhase", new HttpParams()
            .set("seasonId", seasonId.toString()).set("phaseName", phaseName));
    }

    setDate(): void {
        let sd: PhaseModel = this.serviceDocument.dataProfile.dataModel;
        if (sd !== null) {
            if (sd.startDate != null) {
                sd.startDate = this.sharedService.setOffSet(sd.startDate);
            }
            if (sd.endDate != null) {
                sd.endDate = this.sharedService.setOffSet(sd.endDate);
            }
        }
    }
}
