﻿import { Injectable } from "@angular/core";
import { Resolve } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { PhaseModel } from "./PhaseModel";
import { PhaseService } from "./PhaseService";
@Injectable()
export class PhaseResolver implements Resolve<ServiceDocument<PhaseModel>> {
    constructor(private service: PhaseService) { }
    resolve(): Observable<ServiceDocument<PhaseModel>> {
        return this.service.addEdit();
    }
}

@Injectable()
export class PhaseListResolver implements Resolve<ServiceDocument<PhaseModel>> {
    constructor(private service: PhaseService) { }
    resolve(): Observable<ServiceDocument<PhaseModel>> {
        return this.service.list();
    }
}