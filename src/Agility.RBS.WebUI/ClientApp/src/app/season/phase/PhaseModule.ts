﻿import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { PhaseResolver, PhaseListResolver } from "./PhaseResolver";
import { PhaseComponent } from "./PhaseComponent";
import { PhaseListComponent } from "./PhaseListComponent";
import { PhaseService } from "./PhaseService";
import { PhaseRoutingModule } from "./PhaseRoutingModule";
import { DatePickerModule } from "../../Common/DatePickerModule";
import { RbsSharedModule } from "../../RbsSharedModule";

@NgModule({
    imports: [
        FrameworkCoreModule,
        PhaseRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        DatePickerModule,
        RbsSharedModule
    ],
    declarations: [
        PhaseComponent,
        PhaseListComponent
    ],
    providers: [
        PhaseService,
        PhaseResolver,
        PhaseListResolver
    ]
})
export class PhaseModule {
    constructor() {
        LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
    }
}
