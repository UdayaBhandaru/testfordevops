﻿import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { HomeService } from "../home/HomeService";
import { DashboardModel } from "./DashboardModel";


@Injectable()
export class DashboardResolver implements Resolve<ServiceDocument<DashboardModel>> {
    constructor(private service: HomeService) { }
    resolve(route: ActivatedRouteSnapshot): Observable<ServiceDocument<DashboardModel>> {
        return this.service.list();
    }
}