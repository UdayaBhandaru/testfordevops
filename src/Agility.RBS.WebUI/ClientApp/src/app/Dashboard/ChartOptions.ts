﻿export class ChartOptions {
    chart: ChartOptionsChart;
    data?: any;
}

export class ChartOptionsChart {
    enableSmartLabels: boolean;
    showShadow: boolean;
    showBorder: boolean;
    decimals: boolean;
    showLabels: boolean;
    showLegend: boolean;
}

export class ChartOptionsTooltip {
    hideDelay: number;
    valueFormatter?: Function;
    headerFormatter: Function;
}

export class ChartOptionsMargin {
    top: number;
    right: number;
    bottom: number;
    left: number;
}

export class ChartOptionsInteractiveLayer {
    tooltip: ChartOptionsTooltip;
}

export class ChartOptionsAxis {
    axisLabel: string;
    fontSize: number;
    showMaxMin?: boolean;
    axisLabelDistance: number;
    tickFormat: Function;
}

export class ChartOptionsTitle {
    enable: boolean;
    text?: string;
    className: string;
}
