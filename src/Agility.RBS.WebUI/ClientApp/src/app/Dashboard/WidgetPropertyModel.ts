﻿export class WidgetPropertyModel {
    widgetId: number;
    propertyName: string;
    propertyValue: string;
}
