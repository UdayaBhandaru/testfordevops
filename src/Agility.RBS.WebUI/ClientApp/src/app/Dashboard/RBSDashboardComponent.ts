import { Component, Input, OnInit, Output, EventEmitter, ChangeDetectorRef } from "@angular/core";
import { DashboardModel } from "./DashboardModel";
import { PageWidgetModel } from "./PageWidgetModel";
import { NgGrid, NgGridItem, NgGridConfig, NgGridItemConfig, NgGridItemEvent } from "angular2-grid";
import { ChartOptionsService } from "./ChartOptionsService";

@Component({
  selector: "rbs-dashboard",
  template: `<div [ngGrid]="gridConfig">
        <div *ngFor="let row of pageWidgetList">
            <div [(ngGridItem)]="row.widgetOptions" (onItemChange)="updateItem($event)" (onResize)="onResize($event,row)" (onDrag)="onDrag($event)"
                 (onDragStop)="onDragStop($event)" (onResizeStop)="onResizeStop($event)">
                    <rbs-widget [widgetData]="row"></rbs-widget>
            </div>
        </div>
    </div>`
})

export class RBSDashboardComponent implements OnInit {

  @Input() dashboard: DashboardModel[];
  @Output() updatedDashboardData: EventEmitter<DashboardModel[]> = new EventEmitter<DashboardModel[]>();
  pageWidgetList: PageWidgetModel[] = [];

  public gridConfig: NgGridConfig = <NgGridConfig>{
    "margins": [5],
    "draggable": true,
    "resizable": true,
    "max_cols": 0,
    "max_rows": 0,
    "visible_cols": 0,
    "visible_rows": 0,
    "min_cols": 1,
    "min_rows": 1,
    "col_width": 2,
    "row_height": 2,
    "cascade": "up",
    "min_width": 50,
    "min_height": 50,
    "fix_to_grid": false,
    "auto_style": true,
    "auto_resize": false,
    "maintain_ratio": false,
    "prefer_new": false,
    "zoom_on_drag": false,
    "limit_to_screen": true
  };
  private config: any = { "dragHandle": ".handle", "col": 1, "row": 1, "sizex": 50, "sizey": 40 };

  constructor(public chartOptionsService: ChartOptionsService) {
  }

  ngOnInit(): void {
    if (this.dashboard != null && this.dashboard.length > 0) {
      this.chartOptionsService.setOptions(this.dashboard[0].allWidgets);
      this.preprocessWidgets(this.dashboard);
      this.pageWidgetList = this.dashboard[0].pageWidgets;
    }
  }

  updateItem(event: NgGridItemEvent): void {
    let input: NgGridItemEvent = event;
  }

  onDrag(event: NgGridItemEvent): void {
    let input: NgGridItemEvent = event;
  }

  onResize(event: NgGridItemEvent, pageWidget: PageWidgetModel): void {
    if (event.width > 10) {
      pageWidget.width = (event.width - 10).toString();
    }
    if (event.height > 10) {
      pageWidget.height = (event.height - 10).toString();
    }
    pageWidget.widgetOptions.chart.resize();
  }

  onDragStop(eventData: NgGridItemEvent): void {
    this.updatedDashboardData.emit(this.dashboard);
  }

  onResizeStop(eventData: NgGridItemEvent): void {
    this.updatedDashboardData.emit(this.dashboard);
  }

  private preprocessWidgets(dashboard: DashboardModel[]): void {
    let self: RBSDashboardComponent = this;
    dashboard[0].pageWidgets.forEach(function (item: PageWidgetModel): void {
      if (!item.skipRefresh) {
        self.preprocessWidgetItem(item, dashboard);
      }
    });
  }

  private preprocessWidgetItem(widgetItem: PageWidgetModel, dashboard: DashboardModel[]): void {
    let self: RBSDashboardComponent = this;
    for (var index: number = 0; index < dashboard[0].allWidgets.length; index++) {
      if (widgetItem.widgetId === dashboard[0].allWidgets[index].widgetId) {
        widgetItem.widget = dashboard[0].allWidgets[index];
        widgetItem.widget.isLoaded = true;
        widgetItem.widgetOptions.chart = widgetItem.widget.chart.options;
        break;
      }
    }
  }
}
