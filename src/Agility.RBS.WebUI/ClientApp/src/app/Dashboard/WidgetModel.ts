﻿import { WidgetPropertyModel } from "./WidgetPropertyModel";
import { ColorPaletteModel } from "./ColorPaletteModel";
import { SearchPanelModel } from "./SearchPanelModel";
import { ChartModel } from "./ChartModel";
import { ChartOptionsModel } from "./ChartOptionsModel";

export class WidgetModel {
    widgetId: number;
    skipRefresh: boolean;
    isLoaded: boolean;
    name: string;
    subTitle: string;
    sizeX: number;
    sizeY: number;
    xAxisLabel: string;
    yAxisLabel: string;
    chart: ChartModel;
    chartOptions: ChartOptionsModel;
    widgetProperties: Array<WidgetPropertyModel>;
    colorPalette: Array<ColorPaletteModel>;
    searchPanel: SearchPanelModel;
}
