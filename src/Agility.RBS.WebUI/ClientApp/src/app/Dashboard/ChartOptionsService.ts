﻿import { Injectable } from "@angular/core";
import { ChartOptions } from "./ChartOptions";
import { ChartOptionsModel } from "./ChartOptionsModel";
import { WidgetModel } from "./WidgetModel";
import { ColorPaletteModel } from "./ColorPaletteModel";

@Injectable()
export class ChartOptionsService {

    public setOptions(widgets: Array<WidgetModel>): void {
        let self: ChartOptionsService = this;
        widgets.forEach(function (item: WidgetModel): void {
            if (!item.skipRefresh) {
                item.chartOptions.chart = item.chartOptions.chartOptions;
                switch (item.chart.type) {
                    case "pie2D":
                        item.chart.options = self.pieChartOptions(item.chartOptions.chart);
                        break;
                    case "line":
                        item.chart.options = self.lineChartOptions(item.chartOptions.chart);
                        break;
                    case "funnel":
                        item.chart.options = self.funnelChartOptions(item.chartOptions.chart);
                        break;
                    case "Column2D":
                        item.chart.options = self.col2DChartOptions(item.chartOptions.chart);
                        break;
                    case "bar2D":
                        item.chart.options = self.bar2DChartOptions(item.chartOptions.chart);
                        break;
                    default:
                        item.chart.options = <ChartOptions>self.options();
                        break;
                }
            }
        });
    }

    private options(): ChartOptions {
        return {
            chart: {
                enableSmartLabels: false,
                showShadow: false,
                showBorder: false,
                decimals: true,
                showLabels: false,
                showLegend: true
            }
        };
    }

    private pieChartOptions(chartOpt: string): ChartOptions {
        let self: ChartOptionsService = this;
        let chart: any = JSON.parse(chartOpt);
        let fullChartObj: any = {
            chart: Object.assign(chart, self.options().chart),
            data: [
                {
                    "label": "Non-Reward Customers",
                    "value": 430
                },
                {
                    "label": "Reward Customers",
                    "value": 570
                }
            ]
        };
        return fullChartObj;
    }

    private lineChartOptions(chartOpt: string): ChartOptions {
        let self: ChartOptionsService = this;
        let chart: any = JSON.parse(chartOpt);
        let fullChartObj: any = {
            chart: Object.assign(chart, self.options().chart),
            data: [
                {
                    "label": 0,
                    "value": 449
                },
                {
                    "label": 1,
                    "value": 272
                },
                {
                    "label": 2,
                    "value": 188
                },
                {
                    "label": 3,
                    "value": 147
                },
                {
                    "label": 4,
                    "value": 103
                },
                {
                    "label": 5,
                    "value": 108
                },
                {
                    "label": 6,
                    "value": 188
                },
                {
                    "label": 7,
                    "value": 233
                },
                {
                    "label": 8,
                    "value": 371
                },
                {
                    "label": 9,
                    "value": 564
                },
                {
                    "label": 10,
                    "value": 686
                },
                {
                    "label": 11,
                    "value": 877
                },
                {
                    "label": 12,
                    "value": 912
                },
                {
                    "label": 13,
                    "value": 1040
                },
                {
                    "label": 14,
                    "value": 865
                },
                {
                    "label": 15,
                    "value": 727
                },
                {
                    "label": 16,
                    "value": 857
                },
                {
                    "label": 17,
                    "value": 902
                },
                {
                    "label": 18,
                    "value": 972
                },
                {
                    "label": 19,
                    "value": 1109
                },
                {
                    "label": 20,
                    "value": 1142
                },
                {
                    "label": 21,
                    "value": 1033
                },
                {
                    "label": 22,
                    "value": 934
                },
                {
                    "label": 23,
                    "value": 742
                }]
        };
        return fullChartObj;
    }

    private funnelChartOptions(chartOpt: string): ChartOptions {
        let self: ChartOptionsService = this;
        let chart: any = JSON.parse(chartOpt);
        let fullChartObj: any = {
            chart: Object.assign(chart, self.options().chart),
            data: [
                {
                    "label": "SuperMarket",
                    "value": 191186.39
                },
                {
                    "label": "FreshFood",
                    "value": 72594.51
                },
                {
                    "label": "HomeCenter",
                    "value": 18266.03
                },
                {
                    "label": "Investor Sales",
                    "value": 8412.49
                }
            ]

        };
        return fullChartObj;
    }

    private col2DChartOptions(chartOpt: string): ChartOptions {
        let self: ChartOptionsService = this;
        let chart: any = JSON.parse(chartOpt);
        let fullChartObj: any = {
            chart: Object.assign(chart, self.options().chart),
            data: [
                {
                    "label": "13.Salmiya",
                    "value": 35261
                },
                {
                    "label": "29.Sharq",
                    "value": 27809
                },
                {
                    "label": "38.Al-Kout",
                    "value": 13790
                },
                {
                    "label": "14.Salwa",
                    "value": 15428
                },
                {
                    "label": "12.Fahaheel",
                    "value": 8298
                },
                {
                    "label": "23.Jabriya",
                    "value": 10083
                },
                {
                    "label": "15.Ahmadi",
                    "value": 4868
                },
                {
                    "label": "25.Promenade",
                    "value": 2959
                },
                {
                    "label": "Hawally",
                    "value": 25528
                },
                {
                    "label": "Egila",
                    "value": 12679
                },
                {
                    "label": "Boulevard",
                    "value": 16416
                },
                {
                    "label": "20.Shuwaikh",
                    "value": 31766
                },
                {
                    "label": "24.Mangaf",
                    "value": 31468
                },
                {
                    "label": "37.Dhajeej New",
                    "value": 19133
                },
                {
                    "label": "28.Jahra 2",
                    "value": 19026
                },
                {
                    "label": "35.Sulaibiya",
                    "value": 8903
                },
                {
                    "label": "ALL C-Stores",
                    "value": 7046.11
                }
            ]
        };
        return fullChartObj;
    }

    private bar2DChartOptions(chartOpt: string): ChartOptions {
        let self: ChartOptionsService = this;
        let chart: any = JSON.parse(chartOpt);
        let fullChartObj: any = {
            chart: Object.assign(chart, self.options().chart),
            data: [
                {
                    "label": "SuperMarket",
                    "value": 191186.39
                },
                {
                    "label": "FreshFood",
                    "value": 72594.51
                },
                {
                    "label": "HomeCenter",
                    "value": 18266.03
                },
                {
                    "label": "Investor Sales",
                    "value": 8412.49
                }
            ]

        };
        return fullChartObj;
    }
}