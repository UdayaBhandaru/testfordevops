﻿import { SearchFieldDataModel } from "./SearchFieldDataModel";
import { SearchFieldCascadeDataModel } from "./SearchFieldCascadeDataModel";
import { FieldPropertyModel } from "./FieldPropertyModel";

export class SearchFieldModel {
    fieldId: string;
    searchPanelId: number;
    orderId: number;
    fieldProperty: Array<FieldPropertyModel>;
    searchFieldData: Array<SearchFieldDataModel>;
    searchFieldCascadeData: Array<SearchFieldCascadeDataModel>;
    fieldObject: any;
}
