﻿export class FieldPropertyModel {
    fieldId: string;
    searchPanelId: number;
    fieldProperty: string;
    propertyValue: string;
}
