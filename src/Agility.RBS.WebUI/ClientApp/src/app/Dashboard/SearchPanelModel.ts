﻿import { SearchFieldModel } from "./SearchFieldModel";

    export class SearchPanelModel {
        searchPanelId: number;
        visible: boolean;
        searchField: Array<SearchFieldModel>;
    }
