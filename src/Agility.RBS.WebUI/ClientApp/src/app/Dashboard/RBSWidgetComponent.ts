﻿import { Component, Input, OnInit, Output, EventEmitter, ChangeDetectorRef, ElementRef, ViewChild, AfterViewInit } from "@angular/core";
import { PageWidgetModel } from "./PageWidgetModel";
import * as FusionCharts from "fusioncharts";

@Component({
    selector: "rbs-widget",
    template: `<div class="handle" #widgetCtrl{{widgetId}}>
            <div id="chartContainer{{widgetId}}" class="box-header-btns pull-right">
                <fusioncharts [type]="widgetData.widget.chart.type" dataFormat="JSON" [dataSource]="widgetData.widgetOptions.chart"
                class="fusion-charts-content" [width]="widgetData.width" [height]="widgetData.height"></fusioncharts>
            </div>
        </div>`
})

export class RBSWidgetComponent implements OnInit, AfterViewInit {
    @Input() widgetData: PageWidgetModel;
    @ViewChild("widgetCtrl{{widgetId}}") elRef;
    widgetId: number;

    ngOnInit(): void {
        if (this.widgetData) {
            this.widgetId = this.widgetData.widgetId;
            this.widgetData.width = "300";
            this.widgetData.height = "300";
        }
    }

    ngAfterViewInit(): void {
        let self: RBSWidgetComponent = this;
        setTimeout(() => {
            if (self.widgetData) {
                self.widgetData.width = (self.elRef.nativeElement.parentNode.parentElement.clientWidth - 10).toString();
                self.widgetData.height = (self.elRef.nativeElement.parentNode.parentElement.clientHeight - 10).toString();
            }
        }, 1000);
    }
}