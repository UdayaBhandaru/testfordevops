﻿import { ChartOptions } from "./ChartOptions";
export class ChartModel {
    typeId: number;
    type: string;
    model: any;
    data: any;
    options: ChartOptions;
    api: any;
}