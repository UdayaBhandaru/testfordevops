﻿import { WidgetModel } from "./WidgetModel";
import { PageWidgetModel } from "./PageWidgetModel";

export class DashboardModel {
    allWidgets: Array<WidgetModel>;
    pageWidgets: Array<PageWidgetModel>;
}
