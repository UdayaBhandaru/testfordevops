﻿export class ChartOptionsModel {
    widgetId?: number;
    chartOptions?: string;
    chart?: string;
}