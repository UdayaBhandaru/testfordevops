﻿import { WidgetOptionsModel } from "./WidgetOptionsModel";
import { WidgetModel } from "./WidgetModel";

export class PageWidgetModel {
    dashboardPageWidgetId: number;
    widgetId: number;
    skipRefresh: boolean;
    widgetOptions: WidgetOptionsModel;
    widget: WidgetModel;
    width: string;
    height: string;
}
