﻿export class ColorPaletteModel {
    paletteName: string;
    colorKey: string;
    color: string;
}
