﻿import { Injectable } from "@angular/core";
import { Resolve } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { CountryModel } from "./country/CountryModel";
import { CountryService } from "./country/CountryService";



@Injectable()
export class CountryResolver implements Resolve<ServiceDocument<CountryModel>> {
    constructor(private service: CountryService) { }
    resolve(): Observable<ServiceDocument<CountryModel>> {
        return this.service.list();
    }
}


@Injectable()
export class CountryListResolver implements Resolve<ServiceDocument<CountryModel>> {
    constructor(private service: CountryService) { }
    resolve(): Observable<ServiceDocument<CountryModel>> {
        return this.service.list();
    }
}

