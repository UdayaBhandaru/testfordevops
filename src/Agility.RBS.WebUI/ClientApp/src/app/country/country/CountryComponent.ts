import { Component, OnInit, TemplateRef, ViewChild, ChangeDetectionStrategy } from "@angular/core";
import { MatDialogRef } from "@angular/material";
import { Router, ActivatedRoute } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { FormGroup, FormBuilder, FormControl, Validators, ValidatorFn, AbstractControl } from "@angular/forms";
import { SharedService } from "../../Common/SharedService";
import { FormMode } from "@agility/frameworkcore";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { CountryModel } from "./CountryModel";
import { CurrencyDomainModel } from "../../Common/DomainData/CurrencyDomainModel";
import { CountryService } from "./CountryService";

@Component({
  selector: "country",
  templateUrl: "./CountryComponent.html"
})

export class CountryComponent implements OnInit {
  PageTitle = "Country";
  redirectUrl = "Country";
  serviceDocument: ServiceDocument<CountryModel>;
  domainDtlData: DomainDetailModel[];
  currencyData: CurrencyDomainModel[];
  editModel: any;
  editMode: boolean = false;
  localizationData: any;
  model: CountryModel;
  currencyModel: CurrencyDomainModel;

  constructor(private service: CountryService, private sharedService: SharedService, private route: ActivatedRoute
    , private router: Router) {
  }

  ngOnInit(): void {
    this.bind();
  }

  save(saveOnly: boolean): void {
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.saveSubscription(saveOnly);
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  saveSubscription(saveOnly: boolean): void {
    this.checkCountry(saveOnly);
  }

  saveCountry(saveOnly: boolean): void {
    this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.sharedService.saveForm(this.localizationData.country.countrysave).subscribe(() => {
          if (saveOnly) {
            this.router.navigate(["/" + this.redirectUrl + "/List"], { skipLocationChange: true });
          } else {
            if (!this.editMode) {
              this.model = {
                countryId: null,countryDesc:null,countryName: null, baseCurrencyCode: null, alternateCurrencyCode: null
                , baseCurrencyName: null, alternateCurrencyName: null, countryFlag: null, addressFormat: null, operation: "I"
              };
              this.service.newModel(this.model);
            } else {
              Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
              this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.service.serviceDocument.dataProfile.dataModel);
              this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            }
          }
        }, (error: string) => { console.log(error); });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
    }, (error: string) => { console.log(error); });
  }

  checkCountry(saveOnly: boolean): void {
    if (!this.editMode) {
      let form: FormGroup = this.serviceDocument.dataProfile.profileForm;
      if (form.controls["countryId"].value) {
        this.service.isExistingCountry(form.controls["countryId"].value).subscribe
          ((response: boolean) => {
            if (response) {
              this.sharedService.errorForm(this.localizationData.country.countrycheck);
            } else {
              this.saveCountry(saveOnly);
            }
          });
      }
    } else {
      this.saveCountry(saveOnly);
    }
  }

  saveAndContinue(): void {
    this.save(false);
  }

  reset(): void {
    if (!this.editMode) {
      this.service.serviceDocument.dataProfile.profileForm.setValue({
        companyId: 0, endDate: null, countryId: 0, countryName: null
        , countryStatus: "A", operation: "I", startDate: null, currentActionId: null
      });
    } else {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    }
    this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
  }

  bind(): void {
    this.route.data.subscribe(() => {
      this.domainDtlData = Object.assign([], this.service.serviceDocument.domainData["status"]);
      this.currencyData = Object.assign([], this.service.serviceDocument.domainData["currency"]);
      this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);

      delete this.editModel;
      if (this.sharedService.editObject) {
        this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
        Object.assign(this.service.serviceDocument.dataProfile.dataModel, { operation: "U" });
        this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
        this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
        delete this.sharedService.editObject;
        this.editMode = true;
      } else {
        this.model = new CountryModel();
        this.model = {
          countryId: null, countryDesc: null, countryName: null, baseCurrencyCode: null, alternateCurrencyCode: null
          , baseCurrencyName: null, alternateCurrencyName: null, countryFlag: null, addressFormat: null, operation: "I"
        };
        this.service.newModel(this.model);
        delete this.service.countryModel;
      }
      this.serviceDocument = this.service.serviceDocument;
    });
  }
}
