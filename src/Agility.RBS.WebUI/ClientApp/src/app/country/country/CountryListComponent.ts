import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup, FormBuilder, FormControl, Validators, ValidatorFn, AbstractControl } from "@angular/forms";
import { SharedService } from "../../Common/SharedService";
import { GridOptions } from "ag-grid-community";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { GridExportModel } from "../../Common/grid/GridExportModel";
import { InfoComponent } from "../../Common/InfoComponent";
import { GridEditComponent } from "../../Common/grid/GridEditComponent";
import { CountryModel } from "./CountryModel";
import { CountryService } from "./CountryService";
import { CompanyModel } from "../../organizationhierarchy/company/CompanyModel";

@Component({
  selector: "country-list",
  templateUrl: "./CountryListComponent.html"
})
export class CountryListComponent implements OnInit {
  public serviceDocument: ServiceDocument<CountryModel>;
  PageTitle = "Country";
  redirectUrl = "Country";
  componentName: any;
  domainDtlData: DomainDetailModel[];
  companyData: CompanyModel[];
  columns: any[];
  model: CountryModel;
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  sIndex: number;
  gridOptions: GridOptions;

  constructor(public service: CountryService, private route: ActivatedRoute, private sharedService: SharedService
    , private router: Router) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "Country Id", field: "countryId", tooltipField: "countryId", width: 90 },
      { headerName: "Description", field: "countryDesc", tooltipField: "countryDesc" },
      { headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 90 }
    ];

    this.domainDtlData = this.service.serviceDocument.domainData["status"];
    this.companyData = this.service.serviceDocument.domainData["company"];

    this.sharedService.domainData = { company: this.companyData, status: this.domainDtlData };

    this.model = {
      countryId: null, countryDesc: null, countryName: null, baseCurrencyCode: null, alternateCurrencyCode: null
      , baseCurrencyName: null, alternateCurrencyName: null, countryFlag: null, addressFormat: null
    };
    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }
}
