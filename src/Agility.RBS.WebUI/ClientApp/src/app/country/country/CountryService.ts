import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { CountryModel } from "./CountryModel";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";

@Injectable()
export class CountryService {
    serviceDocument: ServiceDocument<CountryModel> = new ServiceDocument<CountryModel>();
    countryModel = new CountryModel();
    searchData: any;
    constructor(private httpHelperService: HttpHelperService) { }

    newModel(model: CountryModel): ServiceDocument<CountryModel> {
        return this.serviceDocument.newModel(model);
    }

    search(): Observable<ServiceDocument<CountryModel>> {
        return this.serviceDocument.search("/api/Country/Search");
    }

    save(): Observable<ServiceDocument<CountryModel>> {
        return this.serviceDocument.save("/api/Country/Save");
    }

    list(): Observable<ServiceDocument<CountryModel>> {
        return this.serviceDocument.list("/api/Country/List");
    }

    addEdit(): Observable<ServiceDocument<CountryModel>> {
        return this.serviceDocument.list("/api/Country/New");
    }

  isExistingCountry(countryId: string): Observable<boolean> {
    return this.httpHelperService.get("/api/Country/IsExistingCountry", new HttpParams().set("countryId", countryId));
    }
}
