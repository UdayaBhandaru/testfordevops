export class CountryModel {
    countryId: string;
    countryDesc: string;
    countryName: string;
    baseCurrencyCode: string;
    baseCurrencyName: string;
    alternateCurrencyCode: string;
    alternateCurrencyName: string;
    countryFlag: string;
    addressFormat: string;
    operation?: string;
    createdUpdatedBy?: string;
    createdUpdatedDate?: Date;
    lastUpdatedBy?: string;
    lastUpdatedDate?: Date;
}
