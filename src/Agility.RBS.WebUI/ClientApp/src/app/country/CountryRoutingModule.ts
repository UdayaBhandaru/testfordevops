﻿import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CountryListResolver, CountryResolver } from "./CountryResolver";
import { CountryListComponent } from "./country/CountryListComponent";
import { CountryComponent } from "./country/CountryComponent";

const CountryRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: CountryListComponent,
                resolve:
                {
                    serviceDocument: CountryListResolver
                }
            },
            {
                path: "New",
                component: CountryComponent,
                resolve:
                {
                    serviceDocument: CountryResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(CountryRoutes)
    ]
})
export class CountryRoutingModule { }
