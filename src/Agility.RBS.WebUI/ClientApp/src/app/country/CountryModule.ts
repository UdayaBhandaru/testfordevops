﻿import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { AgGridSharedModule } from "../AgGridSharedModule";
import { TitleSharedModule } from "../TitleSharedModule";
import { CountryResolver, CountryListResolver } from "./CountryResolver";
import { CountryRoutingModule } from "./CountryRoutingModule";
import { CountryComponent } from "./country/CountryComponent";
import { DatePickerModule } from "../Common/DatePickerModule";
import { CountryListComponent } from "./country/CountryListComponent";
import { CountryService } from "./country/CountryService";
import { RbsSharedModule } from "../RbsSharedModule";

@NgModule({
    imports: [
        FrameworkCoreModule,
        CountryRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        DatePickerModule,
        RbsSharedModule
    ],
    declarations: [
        CountryComponent,
        CountryListComponent
    ],
    providers: [
        CountryService,
        CountryResolver,
        CountryListResolver
    ]
})
export class CountryModule {
    constructor() {
        LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
    }
}
