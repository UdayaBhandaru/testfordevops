import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { UomConversionModel } from "./UomConversionModel";
import { UomConversionService } from "./UomConversionService";
import { SharedService } from "../../Common/SharedService";
import { UomModel } from "../uom/UomModel";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { FormGroup } from "@angular/forms";

@Component({
    selector: "uom-conversion",
    templateUrl: "./UomConversionComponent.html"
})
export class UomConversionComponent implements OnInit {
    PageTitle = "Unit of Measurement Conversion";
    serviceDocument: ServiceDocument<UomConversionModel>;
    convertData: string;
    editModel: any;
    localizationData: any;
    uomFromData: UomModel[];
    uomToData: UomModel[];
    operators: DomainDetailModel[];
    model: UomConversionModel;
    editMode: boolean = false;
    uomModel: UomModel;
    uomClassCode: string;
  pForm: FormGroup;

  uomConvrOperator = [
    {
      id: 'M',
      text: 'MULTIPLY'
    },
    {
      id: 'D',
      text: 'DIVIDE'
    }
  ];

    constructor(private service: UomConversionService, private sharedService: SharedService
        , private router: Router, private ref: ChangeDetectorRef) {
    }

    ngOnInit(): void {
        this.uomFromData = Object.assign([], this.service.serviceDocument.domainData["uom"]);
        //this.operators = Object.assign([], this.service.serviceDocument.domainData["operator"]);
        this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
        this.bind();
    }

    ngAfterViewChecked(): void {
        this.ref.detectChanges();
    }

    bind(): void {
        delete this.editModel;
        if (this.sharedService.editObject) {
          let domainOperator: any = this.uomConvrOperator.filter(id => id.id !== this.sharedService.editObject.operator);
            if (domainOperator) {
                this.sharedService.editObject.operatorDescReverse = domainOperator[0].text;
            }
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
            this.uomToData = Object.assign([], this.uomFromData);
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
            this.editMode = true;
            delete this.sharedService.editObject;
        } else {
            this.model = {
                fromUom: null, toUom: null, factor: null, operator: null, operation: "I", fromUomDesc: null, toUomDesc: null, operatorDescReverse: null
            };
            this.service.newModel(this.model);
        }
        this.serviceDocument = this.service.serviceDocument;
        if (this.editMode) {
            this.convert();
        }
    }

    convert(): void {
        this.pForm = this.serviceDocument.dataProfile.profileForm;
        this.convertData = null;
        if (this.pForm.valid && this.pForm.controls["factor"].value !== 0) {
            if (this.pForm.controls["operator"].value === "M") {
                this.convertData = "1 " + this.pForm.controls["fromUom"].value + " = " + this.pForm.controls["factor"].value + " "
                    + this.pForm.controls["toUom"].value;
            } else if (this.pForm.controls["operator"].value === "D") {
                this.convertData = this.pForm.controls["factor"].value + this.pForm.controls["fromUom"].value + " = 1 "
                    + this.pForm.controls["toUom"].value;
            }
        }
    }


    operatorChange($event: any): void {
        this.convert();
        if ($event.options.id) {
          let domainOperator: any = this.uomConvrOperator.filter(id => id.id !== $event.options.id);
            if (domainOperator) {
                this.serviceDocument.dataProfile.profileForm.controls["operatorDescReverse"].setValue(domainOperator[0].text);
            }
        }

    }

    factorChange($event: any): void {
        this.convert();
        this.serviceDocument.dataProfile.profileForm.controls["factor"].setValue($event.srcElement.value);
    }

    changeFromUOM($event: any, selected: string): void {
        if (selected === "F") {
            this.uomClassCode = this.uomFromData
                .filter(item => item.uom === this.serviceDocument.dataProfile.profileForm.controls["fromUom"].value)[0].uomClass;
            this.serviceDocument.dataProfile.profileForm.controls["toUom"].setValue(null);
            this.uomToData = this.uomFromData.filter(item => item.uomClass === this.uomClassCode
                && item.uom !== this.serviceDocument.dataProfile.profileForm.controls["fromUom"].value);
          this.serviceDocument.dataProfile.profileForm.controls["fromUomDesc"].setValue($event.options.uomDesc);
        } else if (selected === "T") {
          this.serviceDocument.dataProfile.profileForm.controls["toUomDesc"].setValue($event.options.uomDesc);
        }
        this.convertData = null;
    }

    save(saveOnly: boolean): void {
        if (this.serviceDocument.dataProfile.profileForm.valid) {
            if (this.checkUomFromAndToValues()) {
                this.saveSubscription(saveOnly);
            } else {
                this.sharedService.errorForm("From and To values cannot be same.");
            }
        } else {
            this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
        }
    }

    saveSubscription(saveOnly: boolean): void {
        this.checkUomConv(saveOnly);
    }

    saveUpmConverstion(saveOnly: boolean): void {
        this.service.save().subscribe(() => {
            if (this.service.serviceDocument.result.type === MessageType.success) {
                this.sharedService.saveForm(this.localizationData.uomConversion.uomconversionsave).subscribe(() => {
                    if (saveOnly) {
                        this.router.navigate(["/UOMConversion/List"], { skipLocationChange: true });
                    } else {
                        if (!this.editMode) {
                            this.model = {
                                fromUom: null, toUom: null, factor: null, operator: null, operation: "I", fromUomDesc: null, toUomDesc: null
                                , operatorDescReverse: null
                            };
                            this.service.newModel(this.model);
                        } else {
                            Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
                            this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.editModel);
                            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
                        }
                    }
                }, (error: string) => { console.log(error); });
            } else {
                this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
            }
        }, (error: string) => { console.log(error); });
    }

    reset(): void {
        if (!this.editMode) {
            this.model = {
                fromUom: null, toUom: null, factor: null, operator: null, operation: "I", fromUomDesc: null, toUomDesc: null, operatorDescReverse: null
            };
            this.service.newModel(this.model);
        } else {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
        }
        this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
    }

    checkUomConv(saveOnly: boolean): void {
        if (!this.editMode) {
            if (this.checkUomFromAndToValues()) {
                this.service.isExistingConversion(this.serviceDocument.dataProfile.profileForm.controls["fromUom"].value
                    , this.serviceDocument.dataProfile.profileForm.controls["toUom"].value)
                    .subscribe((response: any) => {
                        if (response) {
                            this.sharedService.errorForm(this.localizationData.uomConversion.uomconversioncheck);
                            this.convertData = null;
                        } else {
                            this.convert();
                            this.saveUpmConverstion(saveOnly);
                        }
                    });
            } else {
                this.sharedService.errorForm("From and To values cannot be same.");
            }
        } else {
            this.saveUpmConverstion(saveOnly);
        }
    }

    checkUomFromAndToValues(): boolean {
        if (this.serviceDocument.dataProfile.profileForm.controls["fromUom"].value != null
            && this.serviceDocument.dataProfile.profileForm.controls["toUom"].value != null) {
            if (this.serviceDocument.dataProfile.profileForm.controls["fromUom"].value
                === this.serviceDocument.dataProfile.profileForm.controls["toUom"].value) {
                return false;
            }
        }
        return true;
    }  
}
