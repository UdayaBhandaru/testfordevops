﻿import { Injectable } from "@angular/core";
import { Resolve } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { UomConversionModel } from "./UomConversionModel";
import { UomConversionService } from "./UomConversionService";

@Injectable()
export class UomConversionListResolver implements Resolve<ServiceDocument<UomConversionModel>> {
    constructor(private service: UomConversionService) { }
    resolve(): Observable<ServiceDocument<UomConversionModel>> {
        return this.service.list();
    }
}

@Injectable()
export class UomConversionResolver implements Resolve<ServiceDocument<UomConversionModel>> {
    constructor(private service: UomConversionService) { }
    resolve(): Observable<ServiceDocument<UomConversionModel>> {
        return this.service.addEdit();
    }
}


