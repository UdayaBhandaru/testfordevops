﻿import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { UomConversionListComponent } from "./UomConversionListComponent";
import { UomConversionComponent } from "./UomConversionComponent";
import { UomConversionResolver, UomConversionListResolver } from "./UnitOfMeasureConversionResolver";

const UtilityRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: UomConversionListComponent,
                resolve:
                {
                    serviceDocument: UomConversionListResolver
                }
            },
            {
                path: "New",
                component: UomConversionComponent,
                resolve:
                {
                    serviceDocument: UomConversionResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(UtilityRoutes)
    ]
})
export class UnitOfMeasureConversionRoutingModule { }
