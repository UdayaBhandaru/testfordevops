﻿export class UomConversionModel {
    fromUom: string;
    fromUomDesc?: string;
    toUom: string;
    toUomDesc?: string;
    toUomWithDescription?: string;
    factor: number;
    operator?: string;
    operatorDesc?: string;
    operation?: string;
    operatorDescReverse?: string;
}