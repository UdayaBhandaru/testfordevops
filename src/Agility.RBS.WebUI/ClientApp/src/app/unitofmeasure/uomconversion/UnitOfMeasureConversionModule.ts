﻿import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { UnitOfMeasureConversionRoutingModule } from "./UnitOfMeasureConversionRoutingModule";
import { UomConversionListComponent } from "./UomConversionListComponent";
import { UomConversionComponent } from "./UomConversionComponent";
import { UomConversionService } from "./UomConversionService";
import { UomConversionResolver, UomConversionListResolver } from "./UnitOfMeasureConversionResolver";
import { RbsSharedModule } from "../../RbsSharedModule";
@NgModule({
    imports: [
        FrameworkCoreModule,
        AgGridSharedModule,
        TitleSharedModule,
        UnitOfMeasureConversionRoutingModule,
        RbsSharedModule
    ],
    declarations: [
        UomConversionListComponent,
        UomConversionComponent
    ],
    providers: [
        UomConversionService,
        UomConversionResolver,
        UomConversionListResolver
    ]
})
export class UnitOfMeasureConversionModule {
    constructor() {
        LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
    }
}
