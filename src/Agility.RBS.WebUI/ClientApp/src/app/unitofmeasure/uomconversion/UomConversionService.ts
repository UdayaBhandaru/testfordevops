﻿import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { UomConversionModel } from "./UomConversionModel";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";

@Injectable()
export class UomConversionService {
    serviceDocument: ServiceDocument<UomConversionModel> = new ServiceDocument<UomConversionModel>();

    constructor(private httpHelperService: HttpHelperService) { }

    newModel(model: UomConversionModel): ServiceDocument<UomConversionModel> {
        return this.serviceDocument.newModel(model);
    }

    search(): Observable<ServiceDocument<UomConversionModel>> {
        return this.serviceDocument.search("/api/UOMConversion/Search");
    }

    save(): Observable<ServiceDocument<UomConversionModel>> {
        return this.serviceDocument.save("/api/UOMConversion/Save", true);
    }

    list(): Observable<ServiceDocument<UomConversionModel>> {
        return this.serviceDocument.list("/api/UOMConversion/List");
    }

    addEdit(): Observable<ServiceDocument<UomConversionModel>> {
        return this.serviceDocument.list("/api/UOMConversion/New");
    }

    isExistingConversion(fromUOM: string, toUOM: string): Observable<boolean> {
        return this.httpHelperService.get("/api/UOMConversion/IsExistingConversion", new HttpParams().set("fromUOM", fromUOM).set("toUOM", toUOM));
    }
}