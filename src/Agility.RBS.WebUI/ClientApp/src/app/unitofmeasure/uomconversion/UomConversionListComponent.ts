import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { SharedService } from "../../Common/SharedService";
import { GridOptions } from "ag-grid-community";
import { GridEditComponent } from "../../Common/grid/GridEditComponent";
import { GridExportModel } from "../../Common/grid/GridExportModel";
import { InfoComponent } from "../../Common/InfoComponent";
import { UomConversionModel } from "./UomConversionModel";
import { UomConversionService } from "./UomConversionService";
import { UomModel } from "../uom/UomModel";

@Component({
  selector: "uom-conversion-list",
  templateUrl: "./UomConversionListComponent.html"
})
export class UomConversionListComponent implements OnInit {
  public serviceDocument: ServiceDocument<UomConversionModel>;
  public gridOptions: GridOptions;
  PageTitle = "Unit of Measurement Conversion";
  redirectUrl = "UOMConversion";
  componentName: any;
  columns: any[];
  model: UomConversionModel = { fromUom: null, toUom: null, factor: null, operator: null };
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  uomFromData: UomModel[];
  uomToData: UomModel[];

  constructor(public service: UomConversionService, private sharedService: SharedService, private router: Router, private ref: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      //{ headerName: "FROM UOM", field: "fromUom", tooltipField: "fromUom", width: 90 },
      { headerName: "FROM UOM DESCRIPTION", field: "fromUomDesc", tooltipField: "fromUomDesc" },
      //{ headerName: "TO UOM", field: "toUom", tooltipField: "toUom", width: 70 },
      { headerName: "TO UOM DESCRIPTION", field: "toUomDesc", tooltipField: "toUomDesc" },
      { headerName: "OPERATOR", field: "operatorDesc", tooltipField: "operatorDesc" },
      { headerName: "FACTOR", field: "factor", tooltipField: "factor" },
      {
        headerName: "ACTIONS", field: "Actions", cellRendererFramework: GridEditComponent, width: 90
      }
    ];

    this.uomFromData = this.service.serviceDocument.domainData["uom"];
    delete this.sharedService.domainData;
    this.sharedService.domainData = { uomFrom: this.uomFromData };

    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }
  ngAfterViewChecked(): void {
    this.ref.detectChanges();
  }
  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }

  changeFromUOM(): void {
    let uomClassCode: string = this.uomFromData
      .filter(item => item.uom === this.serviceDocument.dataProfile.profileForm.controls["fromUom"].value)[0].uomClass;
    this.serviceDocument.dataProfile.profileForm.controls["toUom"].setValue(null);
    this.uomToData = this.uomFromData.filter(item => item.uomClass === uomClassCode
      && item.uom !== this.serviceDocument.dataProfile.profileForm.controls["fromUom"].value);
  }

  checkUomConv(): void {
    if (!this.checkUomFromAndToValues()) {
      this.sharedService.errorForm("From and To values cannot be same.");
    }
  }

  checkUomFromAndToValues(): boolean {
    if (this.serviceDocument.dataProfile.profileForm.controls["fromUom"].value !== null
      && this.serviceDocument.dataProfile.profileForm.controls["toUom"].value !== null) {
      if (this.serviceDocument.dataProfile.profileForm.controls["fromUom"].value
        === this.serviceDocument.dataProfile.profileForm.controls["toUom"].value) {
        return false;
      }
    }
    return true;
  }
}
