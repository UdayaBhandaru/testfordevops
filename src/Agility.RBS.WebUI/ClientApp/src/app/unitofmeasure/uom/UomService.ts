﻿import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { UomModel } from "./UomModel";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";

@Injectable()
export class UomService {
    serviceDocument: ServiceDocument<UomModel> = new ServiceDocument<UomModel>();
    constructor(private httpHelperService: HttpHelperService) { }

    newModel(model: UomModel): ServiceDocument<UomModel> {
        return this.serviceDocument.newModel(model);
    }

    search(): Observable<ServiceDocument<UomModel>> {
        return this.serviceDocument.search("/api/UOM/Search");
    }

    save(): Observable<ServiceDocument<UomModel>> {
        return this.serviceDocument.save("/api/UOM/Save", true);
    }

    list(): Observable<ServiceDocument<UomModel>> {
        return this.serviceDocument.list("/api/UOM/List");
    }

    addEdit(): Observable<ServiceDocument<UomModel>> {
        return this.serviceDocument.list("/api/UOM/New");
    }

    isExistingUOM(uomClass: string, name: string): Observable<boolean> {
        return this.httpHelperService.get("/api/UOM/IsExistingUOM", new HttpParams().set("uomClass", uomClass).set("name", name));
    }
}