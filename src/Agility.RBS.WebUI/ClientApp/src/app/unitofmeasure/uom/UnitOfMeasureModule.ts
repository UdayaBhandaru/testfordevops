﻿import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { UnitOfMeasureRoutingModule } from "./UnitOfMeasureRoutingModule";
import { UomListComponent } from "./UomListComponent";
import { UomComponent } from "./UomComponent";
import { UomService } from "./UomService";
import { UomResolver, UomListResolver } from "./UnitOfMeasureResolver";
import { RbsSharedModule } from "../../RbsSharedModule";

@NgModule({
    imports: [
        FrameworkCoreModule,
        UnitOfMeasureRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        UomListComponent,
        UomComponent
    ],
    providers: [
        UomService,
        UomResolver,
        UomListResolver
    ]
})
export class UnitOfMeasureModule {
    constructor() {
        LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
    }
}
