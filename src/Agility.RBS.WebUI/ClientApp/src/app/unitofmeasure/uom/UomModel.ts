export class UomModel {
    uom: string;
    uomClass: string;
    uomDesc: string;
    uomDescription?: string;
    operation?: string;

}
