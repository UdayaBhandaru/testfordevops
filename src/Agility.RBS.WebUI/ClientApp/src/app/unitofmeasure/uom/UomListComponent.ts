import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { SharedService } from "../../Common/SharedService";
import { GridOptions } from "ag-grid-community";
import { UomService } from "./UomService";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { InfoComponent } from "../../Common/InfoComponent";
import { GridExportModel } from "../../Common/grid/GridExportModel";
import { GridEditComponent } from "../../Common/grid/GridEditComponent";
import { UomModel } from "./UomModel";

@Component({
  selector: "uom-list",
  templateUrl: "./UomListComponent.html"
})
export class UomListComponent implements OnInit {
  public serviceDocument: ServiceDocument<UomModel>;
  public gridOptions: GridOptions;
  PageTitle = "Unit of Measurement";
  redirectUrl = "UOM";
  componentName: any;
  uomClassData: DomainDetailModel[];
  columns: any[];
  model: UomModel = {
    uom: null, uomClass: null, uomDesc: null
  };
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;

  constructor(public service: UomService, private sharedService: SharedService, private router: Router, private ref: ChangeDetectorRef) {
  }

  ngAfterViewChecked(): void {
    this.ref.detectChanges();
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "UOM", field: "uom", tooltipField: "uom", width: 60 },
      { headerName: "UOM CLASS", field: "uomClass", tooltipField: "uomClass", width: 90 },
      { headerName: "DESCRIPTION", field: "uomDesc", tooltipField: "uomDesc" },
      {
        headerName: "ACTIONS", field: "Actions", cellRendererFramework: GridEditComponent, width: 60
      }
    ];

    this.uomClassData = this.service.serviceDocument.domainData["uomclass"];

    delete this.sharedService.domainData;
    this.sharedService.domainData = { uomClass: this.uomClassData };
    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }
}
