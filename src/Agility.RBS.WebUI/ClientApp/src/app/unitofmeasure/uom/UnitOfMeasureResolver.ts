﻿import { Injectable } from "@angular/core";
import { Resolve } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { UomModel } from "./UomModel";
import { UomService } from "./UomService";
@Injectable()
export class UomListResolver implements Resolve<ServiceDocument<UomModel>> {
    constructor(private service: UomService) { }
    resolve(): Observable<ServiceDocument<UomModel>> {
        return this.service.list();
    }
}

@Injectable()
export class UomResolver implements Resolve<ServiceDocument<UomModel>> {
    constructor(private service: UomService) { }
    resolve(): Observable<ServiceDocument<UomModel>> {
        return this.service.addEdit();
    }
}


