﻿import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { UomListComponent } from "./UomListComponent";
import { UomComponent } from "./UomComponent";
import { UomResolver } from "./UnitOfMeasureResolver";

const UtilityRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: UomListComponent,
                resolve:
                {
                    serviceDocument: UomResolver
                }
            },
            {
                path: "New",
                component: UomComponent,
                resolve:
                {
                    serviceDocument: UomResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(UtilityRoutes)
    ]
})
export class UnitOfMeasureRoutingModule { }
