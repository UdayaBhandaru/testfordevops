import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { SharedService } from "../../Common/SharedService";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { UomModel } from "./UomModel";
import { UomConversionModel } from "../../unitofmeasure/uomconversion/UomConversionModel";
import { UomService } from "./UomService";
import { FormControl, FormGroup } from "@angular/forms";

@Component({
    selector: "uom",
    templateUrl: "./UomComponent.html"
})
export class UomComponent implements OnInit {
    PageTitle = "Unit of Measurement";
    serviceDocument: ServiceDocument<UomModel>;
    uomClassData: DomainDetailModel[];
    localizationData: any;
    editModel: UomModel;
    editMode: boolean = false;
    defaultStatus: string;
    model: UomModel;
    uomConverstionmodel: UomConversionModel;

    constructor(private service: UomService, private sharedService: SharedService, private router: Router) {
    }

    ngOnInit(): void {
        this.uomClassData = Object.assign([], this.service.serviceDocument.domainData["uomclass"]);
        this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
        this.bind();
    }

    bind(): void {
        delete this.editModel;
        if (this.sharedService.editObject) {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
            this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            delete this.sharedService.editObject;
            this.editMode = true;
        } else {
            this.model = {
                uom: null, uomClass: null, uomDesc: null, operation: "I"
            };
            this.service.newModel(this.model);
        }
        this.serviceDocument = this.service.serviceDocument;
    }

    save(saveOnly: boolean): void {
        if (this.serviceDocument.dataProfile.profileForm.valid) {
            this.saveSubscription(saveOnly);
        } else {
            this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
        }
    }

    saveAndContinue(): void {
        this.save(false);
    }

    saveSubscription(saveOnly: boolean): void {
        this.checkUom(saveOnly);
    }
    saveUom(saveOnly: boolean): void {
        this.service.save().subscribe(() => {
            if (this.service.serviceDocument.result.type === MessageType.success) {
                this.sharedService.saveForm(this.localizationData.uom.uomsave).subscribe(() => {
                    if (saveOnly) {
                        this.router.navigate(["/UOM/List"], { skipLocationChange: true });
                    } else {
                        if (!this.editMode) {
                            this.model = {
                                uom: null, uomClass: null, uomDesc: null, operation: "I"
                            };
                            this.service.newModel(this.model);
                        } else {
                            Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
                            this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.editModel);
                            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
                        }
                    }
                }, (error: string) => { console.log(error); });
            } else {
                this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
            }
        }, (error: string) => { console.log(error); });
    }

    checkUom(saveOnly: boolean): void {
        if (!this.editMode) {
            var form: FormGroup = this.serviceDocument.dataProfile.profileForm;
            if (form.controls["uomClass"].value && form.controls["uom"].value) {
                this.service.isExistingUOM(form.controls["uomClass"].value, form.controls["uom"].value)
                    .subscribe((response: any) => {
                        if (response) {
                            this.sharedService.errorForm(this.localizationData.uom.uomcheck);
                        } else {
                            this.saveUom(saveOnly);
                        }
                    });
            }
        } else {
            this.saveUom(saveOnly);
        }
    }

    reset(): void {
        if (!this.editMode) {
            this.model = {
                uom: null, uomClass: null, uomDesc: null, operation: "I"
            };
            this.service.newModel(this.model);
        } else {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
        }
        this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
    }   
}
