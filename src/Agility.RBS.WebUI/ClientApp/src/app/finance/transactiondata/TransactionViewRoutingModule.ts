import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { TransactionViewListComponent } from "./TransactionViewListComponent";
import { TransactionViewResolver } from "./TransactionViewResolver";

const DomainRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
              component: TransactionViewListComponent,
                resolve:
                {
                  references: TransactionViewResolver
                }
            },
            
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(DomainRoutes)
    ]
})
export class TransactionViewRoutingModule { }
