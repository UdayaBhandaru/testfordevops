import { Component, OnInit, ViewChild, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, AjaxModel, AjaxResult } from "@agility/frameworkcore";
import { TransactionViewService } from "./TransactionViewService";
import { FormGroup, Validators, ValidatorFn, AbstractControl } from "@angular/forms";
import { SharedService } from "../../Common/SharedService";
import { GridEditComponent } from "../../Common/grid/GridEditComponent";
import { GridExportModel } from "../../Common/grid/GridExportModel";
import { InfoComponent } from "../../Common/InfoComponent";
import { GridOptions } from 'ag-grid-community';
import { TransactionDataModel } from './TransactionDataModel';
import { DepartmentDomainModel } from '../../Common/DomainData/DepartmentDomainModel';
import { ClassDomainModel } from '../../Common/DomainData/ClassDomainModel';
import { SubClassDomainModel } from '../../Common/DomainData/SubClassDomainModel';
import { SearchComponent } from '../../Common/SearchComponent';
import { DatePipe } from "@angular/common";

@Component({
  selector: "TransactionView-List",
  templateUrl: "./TransactionViewListComponent.html"
})
export class TransactionViewListComponent implements OnInit {

  public serviceDocument: ServiceDocument<TransactionDataModel>;
  PageTitle = "Transaction View Search";
  redirectUrl = "";
  @ViewChild("searchPanelTransaction") searchPanelTransaction: SearchComponent;
  componentName: any;
  columns: any[];
  model: TransactionDataModel;
  locationData: any[];
  categoryData: DepartmentDomainModel[];
  transactionTypeData: any[];
  fineLineData: ClassDomainModel[];
  segmentData: SubClassDomainModel[];
  fineLineList: ClassDomainModel[];
  segmentList: SubClassDomainModel[];
  itemTypeData: any[];
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  gridOptions: GridOptions;

  constructor(public service: TransactionViewService, private sharedService: SharedService, private router: Router, private datePipe: DatePipe, private ref: ChangeDetectorRef) {
  }

  ngAfterViewChecked(): void {
    this.ref.detectChanges();
  }

  ngOnInit(): void {
    this.columns = [
      {
        headerName: "Date", field: "date", tooltipField: "date", cellRenderer: (data) => {

          var dt = new Date(data.value);
          var cFieldValue = this.datePipe.transform(dt, "dd-MMM-yyyy");
          return data.value ? cFieldValue : '';
        }
      },
      { headerName: "Item", field: "item", tooltipField: "item" },
      { headerName: "Loc", field: "locationName", tooltipField: "locationName" },
      { headerName: "LocType", field: "locationType", tooltipField: "locationName" },
      { headerName: "Quantity", field: "quantity", tooltipField: "quantity" },
      { headerName: "Total Retail", field: "totalRetail", tooltipField: "totalRetail" },
      { headerName: "Total Cost", field: "totalCost", tooltipField: "totalCost" },
      { headerName: "Currency", field: "currency", tooltipField: "currency" },
      { headerName: "Transaction Type", field: "transactionType", tooltipField: "transactionType" },
      {
        headerName: "Post Date", field: "postDate", tooltipField: "postDate", cellRenderer: (data) => {
          var dt = new Date(data.value);
          var cFieldValue = this.datePipe.transform(dt, "dd-MMM-yyyy");
          return data.value ? cFieldValue : '';
        }
      }
    ];
    this.locationData = Object.assign([], this.service.serviceDocument.domainData["location"]);
    this.categoryData = Object.assign([], this.service.serviceDocument.domainData["category"]);
    this.transactionTypeData = Object.assign([], this.service.serviceDocument.domainData["transactionType"]);
    this.fineLineData = Object.assign([], this.service.serviceDocument.domainData["fineline"]);
    this.segmentData = Object.assign([], this.service.serviceDocument.domainData["segment"]);
    this.fineLineList = Object.assign([], this.service.serviceDocument.domainData["fineline"]);
    this.segmentList = Object.assign([], this.service.serviceDocument.domainData["segment"]);
    this.itemTypeData = Object.assign([], this.service.serviceDocument.domainData["itemType"]);
    this.searchPanelTransaction.dateColumns = ["fromDate", "toDate"];

    this.model = {
      fromDate: null, toDate: null, date: null, item: null, postDate: null, location: null, category: null, fineLine: null, itemType: null
      , segment: null, transactionTypeId: null, categoryName: null, currency: null, itemId: null, locationName: null, locationType: null
      , quantity: 0, totalCost: 0, totalRetail: 0, transactionType: null
    };
    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
    this.service.newModel(this.model);
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: any): void {
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.showSearchCriteria = event === "search" ? true : event;
      if (event === "search") {
        this.searchPanelTransaction.restrictSearch = false;
        this.searchPanelTransaction.search();
      } else if (event) {
        this.searchPanelTransaction.restrictSearch = true;
      }
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }

  changeCategory(): void {
    this.serviceDocument.dataProfile.profileForm.controls["fineLine"].setValue(null);
    this.fineLineData = this.fineLineList.filter(item => item.dept === +this.serviceDocument.dataProfile.profileForm.controls["category"].value);
    this.sharedService.domainData["fineLine"] = this.fineLineData;
  }

  changeFineLine(): void {
    this.serviceDocument.dataProfile.profileForm.controls["segment"].setValue(null);
    var transactionDataModel = new TransactionDataModel();
    transactionDataModel.category = +this.serviceDocument.dataProfile.profileForm.controls["category"].value;
    transactionDataModel.fineLine = +this.serviceDocument.dataProfile.profileForm.controls["fineLine"].value;
    this.service.subclass(transactionDataModel)
      .subscribe((response: AjaxModel<SubClassDomainModel[]>) => {
        if (response.result === AjaxResult.success) {
          this.segmentData = response.model;
          this.sharedService.domainData["segment"] = this.segmentData;
        } else {
          this.sharedService.errorForm(response.message);
        }
        //this.changeRef.detectChanges();
      }, (error: string) => { console.log(error); });
  }

  onsubmit() {
    this.serviceDocument.dataProfile.profileForm.controls["itemId"].setErrors({ 'error': null });
    this.serviceDocument.dataProfile.profileForm.controls["fromDate"].setErrors({ 'error': null });
    this.serviceDocument.dataProfile.profileForm.controls["toDate"].setErrors(null);
    if (this.serviceDocument.dataProfile.profileForm.controls["toDate"].value != null && this.serviceDocument.dataProfile.profileForm.controls["fromDate"].value == null) {
      this.serviceDocument.dataProfile.profileForm.controls["fromDate"].setErrors({ 'error': 'From Date should not be empty' });
    }
    if (this.serviceDocument.dataProfile.profileForm.controls["fromDate"].value != null && this.serviceDocument.dataProfile.profileForm.controls["toDate"].value == null) {
      this.serviceDocument.dataProfile.profileForm.controls["fromDate"].setErrors({ 'error': 'To Date should not be empty' });
    }

    if (this.serviceDocument.dataProfile.profileForm.controls["fromDate"].value > this.serviceDocument.dataProfile.profileForm.controls["toDate"].value) {
      this.serviceDocument.dataProfile.profileForm.controls["fromDate"].setErrors({ 'error': 'To Date should not be empty' });
    }

    if (this.serviceDocument.dataProfile.profileForm.controls["itemId"].value != null && this.serviceDocument.dataProfile.profileForm.controls["itemType"].value == null) {
      this.serviceDocument.dataProfile.profileForm.controls["itemId"].setErrors({ 'error': 'please select Item Type' });
    }

    if (this.serviceDocument.dataProfile.profileForm.controls["fromDate"].value == null && this.serviceDocument.dataProfile.profileForm.controls["toDate"].value == null && this.serviceDocument.dataProfile.profileForm.controls["itemId"].value == null) {
      this.serviceDocument.dataProfile.profileForm.controls["itemId"].setErrors({ 'error': 'Item should not be empty' });
      this.serviceDocument.dataProfile.profileForm.controls["fromDate"].setErrors({ 'error': 'From Date should not be empty' });
      this.serviceDocument.dataProfile.profileForm.controls["toDate"].setErrors({ 'error': 'To Date should not be empty' });
    }
    this.ref.detectChanges();
  }
}
