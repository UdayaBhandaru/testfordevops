import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { TransactionViewRoutingModule } from "./TransactionViewRoutingModule";
import { TransactionViewListComponent } from "./TransactionViewListComponent";
import { TransactionViewService } from "./TransactionViewService";
import { TransactionViewResolver } from "./TransactionViewResolver";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { RbsSharedModule } from "../../RbsSharedModule";

@NgModule({
    imports: [
        FrameworkCoreModule,
        TransactionViewRoutingModule,
        AgGridModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        TransactionViewListComponent,
       
    ],
    providers: [
        TransactionViewService,
        TransactionViewResolver
    ]
})
export class TransactionViewModule {
}
