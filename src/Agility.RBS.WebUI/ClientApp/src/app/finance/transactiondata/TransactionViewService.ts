import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument, AjaxModel } from "@agility/frameworkcore";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";
import { TransactionDataModel } from './TransactionDataModel';
import { SubClassDomainModel } from '../../Common/DomainData/SubClassDomainModel';

@Injectable()
export class TransactionViewService {
  serviceDocument: ServiceDocument<TransactionDataModel> = new ServiceDocument<TransactionDataModel>();
  searchData: any;
  constructor(private httpHelperService: HttpHelperService) { }

  newModel(model: TransactionDataModel): ServiceDocument<TransactionDataModel> {
    return this.serviceDocument.newModel(model);
  }

  list(): Observable<ServiceDocument<TransactionDataModel>> {
    return this.serviceDocument.list("/api/TransactionData/List");
  }

  search(): Observable<ServiceDocument<TransactionDataModel>> {
    return this.serviceDocument.search("/api/TransactionData/Search");
  }

  save(): Observable<ServiceDocument<TransactionDataModel>> {
    return this.serviceDocument.save("/api/TransactionData/Save", true);
  }

  subclass(model: TransactionDataModel): Observable<AjaxModel<SubClassDomainModel[]>> {
    var params = new HttpParams();
    return this.httpHelperService.get(`/api/Item/SubClassGetbyName?deptid=${String(model.category)}&classId=${String(model.fineLine)}`, params);
  }
}
