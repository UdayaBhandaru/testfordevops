import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { TransactionViewService } from "./TransactionViewService";
import { TransactionDataModel } from './TransactionDataModel';

@Injectable()
export class TransactionViewResolver implements Resolve<ServiceDocument<TransactionDataModel>> {
  constructor(private service: TransactionViewService) { }
  resolve(): Observable<ServiceDocument<TransactionDataModel>> {
        return this.service.list();
    }
}
