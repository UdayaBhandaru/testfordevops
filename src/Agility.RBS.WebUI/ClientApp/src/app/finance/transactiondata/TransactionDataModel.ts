export class TransactionDataModel {
  fromDate: Date;
  toDate: Date;
  date: Date;
  postDate: Date;
  category: number;
  categoryName: string;
  fineLine: number;
  locationName: string;
  locationType: string;
  quantity: number;
  totalRetail: number;
  totalCost: number;
  currency: string;
  location: number;
  segment?: number;
  itemId: string;
  itemType: string;
  transactionTypeId: string;
  transactionType: string;
  item: string;
}
