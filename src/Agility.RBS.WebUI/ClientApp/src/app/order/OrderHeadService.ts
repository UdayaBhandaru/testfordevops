import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument, WorkflowAction } from "@agility/frameworkcore";
import { OrderHeadModel } from "./OrderHeadModel";
import { HttpParams } from "@angular/common/http";
import { SharedService } from '../Common/SharedService';
import { HttpHelperService } from '../Common/HttpHelperService';

@Injectable()
export class OrderHeadService {
  serviceDocument: ServiceDocument<OrderHeadModel> = new ServiceDocument<OrderHeadModel>();

  constructor(private httpHelperService: HttpHelperService
    , private sharedService: SharedService) { }

  newModel(model: OrderHeadModel): ServiceDocument<OrderHeadModel> {
    return this.serviceDocument.newModel(model);
  }

  save(): Observable<ServiceDocument<OrderHeadModel>> {
    let currentAction: WorkflowAction = this.serviceDocument.dataProfile.actionService.allowedActions
      .find((v) => v.transitionClaim === "OrderStartCreatedClaim");
    if (!this.serviceDocument.dataProfile.dataModel.ordeR_NO && currentAction) {
      this.serviceDocument.dataProfile.profileForm.controls["status"].setValue("WS");
      this.serviceDocument.dataProfile.profileForm.controls["currentActionId"].setValue(currentAction.transitionClaim);
      return this.serviceDocument.submit("/api/OrderHead/Submit", true, () => this.setDate());
    } else {
      return this.serviceDocument.save("/api/OrderHead/Save", true, () => this.setDate());
    }
  }

  submit(): Observable<ServiceDocument<OrderHeadModel>> {
    this.serviceDocument.dataProfile.profileForm.controls["status"].setValue("SUB");
    return this.serviceDocument.submit("/api/OrderHead/Submit", true);
  }

  reject(): Observable<ServiceDocument<OrderHeadModel>> {
    return this.serviceDocument.submit("/api/OrderHead/Reject", true);
  }

  sendBackForReview(): Observable<ServiceDocument<OrderHeadModel>> {
    return this.serviceDocument.submit("/api/OrderHead/SendBackForReview", true);
  }

  addEdit(id: number): Observable<ServiceDocument<OrderHeadModel>> {
    if (+id === 0) {
      return this.serviceDocument.new("/api/OrderHead/New");
    } else {
      return this.serviceDocument.open("/api/OrderHead/Open", new HttpParams().set("id", id.toString()));
    }
  }

  getDataFromAPI(apiUrl: string): Observable<any> {
    let resp: Observable<any> = this.httpHelperService.get(apiUrl);
    return resp;
  }

  setDate(): void {
    let sd: OrderHeadModel = this.serviceDocument.dataProfile.dataModel;
    if (sd !== null) {
      if (sd.appDatetime != null) {
        sd.appDatetime = this.sharedService.setOffSet(sd.appDatetime);
      }

      if (sd.pickupDate != null) {
        sd.pickupDate = this.sharedService.setOffSet(sd.pickupDate);
      }

      if (sd.earliestShipDate != null) {
        sd.earliestShipDate = this.sharedService.setOffSet(sd.earliestShipDate);
      }

      if (sd.latestShipDate != null) {
        sd.latestShipDate = this.sharedService.setOffSet(sd.latestShipDate);
      }

      if (sd.notBeforeDate != null) {
        sd.notBeforeDate = this.sharedService.setOffSet(sd.notBeforeDate);
      }

      if (sd.notAfterDate != null) {
        sd.notAfterDate = this.sharedService.setOffSet(sd.notAfterDate);
      }
    }
  }
}
