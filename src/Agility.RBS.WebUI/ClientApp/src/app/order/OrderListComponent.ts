import { Component, OnInit, ViewChild, OnDestroy, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { Subscription } from "rxjs";
import { GridOptions, GridReadyEvent, IServerSideDatasource, IServerSideGetRowsParams, ColDef } from "ag-grid-community";
import { WorkflowStatusDomainModel } from '../Common/DomainData/WorkflowStatusDomainModel';
import { SupplierDomainModel } from '../Common/DomainData/SupplierDomainModel';
import { OrderSearchModel } from '../order/OrderSearchModel';
import { LocationDomainModel } from '../Common/DomainData/LocationDomainModel';
import { DomainDetailModel } from '../domain/DomainDetailModel';
import { SearchComponent } from '../Common/SearchComponent';
import { GridExportModel } from '../Common/grid/GridExportModel';
import { SharedService } from '../Common/SharedService';
import { OrderListService } from './OrderListService';
import { GridEditComponent } from '../Common/grid/GridEditComponent';
import { InfoComponent } from '../Common/InfoComponent';
import { DepartmentDomainModel } from '../Common/DomainData/DepartmentDomainModel';

@Component({
  selector: "order-list",
  templateUrl: "./OrderListComponent.html"
})

export class OrderListComponent implements OnInit, IServerSideDatasource, OnDestroy {
  public sheetName: string = "Order List";
  PageTitle = "Order List";
  redirectUrl = "Order/New/";
  componentName: any;
  additionalSearchParams: any;
  loadedPage: boolean;
  searchServiceSubscription: Subscription;
  workflowStatusList: WorkflowStatusDomainModel[];
  deptData: DepartmentDomainModel[];
  supplierData: SupplierDomainModel[];
  locationData: LocationDomainModel[];
  dateTypeData: DomainDetailModel[]; orderTypeData: DomainDetailModel[]; orderStatusData: DomainDetailModel[];
  apiUrl: string;

  serviceDocument: ServiceDocument<OrderSearchModel>;
  gridOptions: GridOptions;
  columns: any[];
  model: OrderSearchModel = {
    supplier: null, supName: null, deptId: null, deptDesc: null, locType: null, locTypeDesc: null, location: null, locationName: null
    , itemBarcode: null, itemDesc: null, ordeR_NO: null, orderType: null, orderTypeDesc: null, orderDesc: null, status: null
    , statusDesc: null, dateType: null, startDate: null, endDate: null, operation: null, tableName: null
  };

  isSearchParamGiven: boolean = false;
  searchProps: string[] = ["supplier", "deptId", "location", "itemBarcode", "itemDesc", "ordeR_NO", "orderType", "orderDesc", "status"
    , "dateType", "startDate", "endDate"];
  @ViewChild("searchPanel") searchPanel: SearchComponent;
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;

  constructor(public service: OrderListService, private sharedService: SharedService, private router: Router
    , private changeRef: ChangeDetectorRef) {
  }

  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { colId: "ORDER_NO", headerName: "Order Id", field: "ordeR_NO", tooltipField: "ordeR_NO", width: 80, sort: "desc" },
      { colId: "ORDER_TYPE_DESC", headerName: "Order Type", field: "orderTypeDesc", tooltipField: "orderTypeDesc", width: 120 },
      { colId: "SUP_NAME", headerName: "Supplier", field: "supName", tooltipField: "supName", width: 140 },
      { colId: "LOC_NAME", headerName: "Location", field: "locationName", tooltipField: "locationName", width: 140 },
      { colId: "DEPT_DESC", headerName: "Category", field: "deptDesc", tooltipField: "deptDesc", width: 140 },
      { colId: "STATUS", headerName: "Order Status", field: "status", tooltipField: "status", width: 100 },
      { colId: "STATUS_DESC", headerName: "Status", field: "statusDesc", tooltipField: "statusDesc", width: 100 },
      //{
      //    colId: "CREATED_DATE", headerName: "Created Date", field: "createdDate", tooltipField: "createdDate"
      //    , cellRendererFramework: GridDateComponent, width: 130
      //},
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 100
      }
    ];

    this.gridOptions = {
      onGridReady: (params: GridReadyEvent) => {
        params.api.setEnterpriseDatasource(this);
      },
      context: {
        componentParent: this
      },
      suppressRowClickSelection: true,
      suppressCellSelection: true,
      rowModelType: "enterprise",
      paginationPageSize: this.service.orderPaging.pageSize,
      enableServerSideSorting: true,
      enableServerSideFilter: true,
      cacheBlockSize: this.service.orderPaging.cacheSize,
    };

    this.supplierData = Object.assign([], this.service.serviceDocument.domainData["supplier"]);
    this.locationData = Object.assign([], this.service.serviceDocument.domainData["location"]);
    this.dateTypeData = this.service.serviceDocument.domainData["dateType"];
    this.deptData = Object.assign([], this.service.serviceDocument.domainData["department"]);
    this.orderTypeData = Object.assign([], this.service.serviceDocument.domainData["orderType"]);
    this.workflowStatusList = this.service.serviceDocument.domainData["workflowStatusList"];

    delete this.sharedService.domainData;
    this.sharedService.domainData = {
      location: this.locationData, dateType: this.dateTypeData, status: this.orderStatusData, orderType: this.orderTypeData
      , department: this.deptData, supplier: this.supplierData
    };

    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }

    this.serviceDocument = this.service.serviceDocument;
    this.apiUrl = "/api/Supplier/SuppliersGetName?name=";
    this.searchPanel.dateColumns = ["startDate", "endDate"];
  }

  getRows(params: IServerSideGetRowsParams): void {
    this.additionalSearchParams = { startRow: params.request.startRow, endRow: params.request.endRow, sortModel: params.request.sortModel };
    if (!this.loadedPage) {
      this.loadedPage = true;
      params.successCallback(
        this.serviceDocument.dataProfile.dataList.slice(params.request.startRow, params.request.endRow),
        this.service.serviceDocument.dataProfile.dataModel.totalRows);
    } else {
      this.searchServiceSubscription = this.service.search(this.additionalSearchParams).subscribe((sDoc) => {
        params.successCallback(sDoc.dataProfile.dataList, this.service.serviceDocument.dataProfile.dataModel.totalRows);
      });
    }
  }

  showSearchCriteriaChild(event: any): void {
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.showSearchCriteria = event === "search" ? true : event;
      if (event === "search") {
        for (let i: number = 0; i < this.searchProps.length; i++) {
          let controlName: string = this.searchProps[i];
          if (this.serviceDocument.dataProfile.profileForm.controls[controlName].value) {
            this.isSearchParamGiven = true;
            break;
          }
        }
        if (this.isSearchParamGiven) {
          this.searchPanel.restrictSearch = false;
          this.searchPanel.search();
          this.isSearchParamGiven = false;
        } else {
          this.sharedService.errorForm("At least one field is required to search");
        }
      } else if (event) {
        this.searchPanel.restrictSearch = true;
      }
    }
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
      this.sharedService.previousUrl = "../Order/List";
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl + cell.data.ordeR_NO
      , this.service.serviceDocument.dataProfile.dataList);
  }

  addValidation(): void {
    this.navigateToAdd();
  }

  private navigateToAdd(): void {
    this.router.navigate(["/" + this.redirectUrl + "0"], { skipLocationChange: true });
    this.sharedService.previousUrl = "../Order/List";
  }

  ngOnDestroy(): void {
    if (this.searchServiceSubscription) {
      this.searchServiceSubscription.unsubscribe();
    }
  }
}
