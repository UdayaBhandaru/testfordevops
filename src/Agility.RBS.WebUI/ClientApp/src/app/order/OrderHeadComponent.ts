import { Component, OnInit, TemplateRef, ViewChild, OnDestroy, ChangeDetectorRef } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ServiceDocument, CommonService, MessageResult, MessageType, AjaxModel, AjaxResult } from "@agility/frameworkcore";
import { MatDialogRef } from "@angular/material";
import { GridOptions, GridApi, RowNode } from "ag-grid-community";
import { Subscription } from "rxjs";
import { OrderHeadService } from "./OrderHeadService";
import { OrderHeadModel } from "./OrderHeadModel";
import { DomainDetailModel } from '../domain/DomainDetailModel';
import { LocationDomainModel } from '../Common/DomainData/LocationDomainModel';
import { CountryDomainModel } from '../Common/DomainData/CountryDomainModel';
import { InboxCommonModel } from '../Common/DomainData/InboxCommonModel';
import { SharedService } from '../Common/SharedService';
import { RbDialogService } from '../Common/controls/RbDialogService';
import { LoaderService } from '../Common/LoaderService';
import { DocumentsConstants } from '../Common/DocumentsConstants';
import { RbButton } from '../Common/controls/RbControlModels';
import { SupplierDomainModel } from '../Common/DomainData/SupplierDomainModel';
import { SupplierDetailDomainModel } from '../Common/DomainData/SupplierDetailDomainModel';
import { GridSelectComponent } from '../Common/grid/GridSelectComponent';
import { GridGetItemComponent } from '../Common/grid/GridGetItemComponent';
import { GridAmountComponent } from '../Common/grid/GridAmountComponent';
import { GridNumericComponent } from '../Common/grid/GridNumericComponent';
import { GridEditComponent } from '../Common/grid/GridEditComponent';
import { OrderDetailModel } from './OrderDetailModel';
import { GridDateComponent } from '../Common/grid/GridDateComponent';
import { OrderInfoComponent } from './OrderInfoComponent';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { PoTypeDomainModel } from '../Common/DomainData/PoTypeDomainModel';
import { OutLocationDomainModel } from '../Common/DomainData/OutLocationDomainModel';
import { DepartmentDomainModel } from '../Common/DomainData/DepartmentDomainModel';
import { OrderItemSelectModel } from './OrderItemSelectModel';

@Component({
  selector: "orderHead-add",
  templateUrl: "./OrderHeadComponent.html",
  styleUrls: ['./OrderHeadComponent.scss']
})
export class OrderHeadComponent implements OnInit, OnDestroy {
  screenName: any;
  _componentName = "Order";
  PageTitle: string = "Order";
  public serviceDocument: ServiceDocument<OrderHeadModel>;
  localizationData: any;
  primaryId: number; // used in CanDeactivate method, should be same name if we use candeactivate
  datesArray: any[] = [];
  componentParentName: OrderHeadComponent;
  editMode: boolean;
  apiUrl: string;
  indicatorData: DomainDetailModel[];
  orderTypeData: DomainDetailModel[];
  locTypeData: DomainDetailModel[];
  locationData: LocationDomainModel[]; locationList: LocationDomainModel[];
  deptData: DepartmentDomainModel[];
  supplierData: SupplierDomainModel[];
  purchaseTypeData: DomainDetailModel[];
  poTypeData: PoTypeDomainModel[];
  orderOriginData: DomainDetailModel[];
  importCountryData: CountryDomainModel[];
  billtoIdData: OutLocationDomainModel[];
  orderStatus: string;

  dialogRef: MatDialogRef<any>;
  private messageResult: MessageResult = new MessageResult();
  canNavigate: boolean;
  effectiveMinDate: Date = new Date();
  currentDate: Date = new Date();
  inboxCommonData: InboxCommonModel;
  routeServiceSubscription: Subscription;
  supplierDetailsServiceSubscription: Subscription;
  itemDetailsServiceSubscription: Subscription;
  saveServiceSubscription: Subscription;

  gridOptions: GridOptions;
  gridApi: GridApi;
  componentName: any;
  columns: any[];
  data: OrderDetailModel[] = [];
  dataReset: OrderDetailModel[] = [];
  orginalValue: string = "";
  addText: string = "+ Add Item";
  detailModel: OrderDetailModel;
  costSourceData: DomainDetailModel[];
  pForm: any;
  mandatoryOrderDetailsFields: string[] = [
    "item",
    "qtyOrdered",
    "unitCost",
    "unitCostInit",
    "estimatedInstockDate",
    "nonScaleInd"
  ];
  deletedRows: any[] = [];

  constructor(private route: ActivatedRoute, private router: Router, public sharedService: SharedService, private service: OrderHeadService
    , private loaderService: LoaderService, private changeRef: ChangeDetectorRef, private dialogService: RbDialogService) {
  }

  ngOnInit(): void {
    this.screenName = new DocumentsConstants().orderObjectName;
    this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
    this.orderTypeData = Object.assign([], this.service.serviceDocument.domainData["orderType"]);
    this.locTypeData = Object.assign([], this.service.serviceDocument.domainData["locType"]);
    this.locationList = Object.assign([], this.service.serviceDocument.domainData["location"]);
    this.deptData = Object.assign([], this.service.serviceDocument.domainData["department"]);
    this.supplierData = Object.assign([], this.service.serviceDocument.domainData["supplier"]);
    this.purchaseTypeData = Object.assign([], this.service.serviceDocument.domainData["purchaseType"]);
    this.poTypeData = Object.assign([], this.service.serviceDocument.domainData["poType"]);
    this.billtoIdData = Object.assign([], this.service.serviceDocument.domainData["billtoid"]);
    this.orderOriginData = Object.assign([], this.service.serviceDocument.domainData["orderOrigin"]);
    this.importCountryData = Object.assign([], this.service.serviceDocument.domainData["country"]);
    this.inboxCommonData = Object.assign({}, this.service.serviceDocument.domainData["inboxCommonData"]);
    this.indicatorData = Object.assign([], this.service.serviceDocument.domainData["indicator"]);
    this.costSourceData = Object.assign([], this.service.serviceDocument.domainData["costsrc"]);
    this.apiUrl = "/api/Supplier/SuppliersGetName?name=";

    this.componentParentName = this;

    this.columns = [
      {
        headerName: "Item", field: "item", cellRendererFramework: GridGetItemComponent
        , cellClassRules: {
          "invalid-grid-row": function (params: any): any { return !params.value; }
        }
        , tooltipField: "programMessage"
      },
      { headerName: "Item Name", field: "itemDesc", tooltipField: "itemDesc" },
      { headerName: "Pack Size", field: "defaultUnitSize", tooltipField: "defaultUnitSize" },
      {
        headerName: "Cost Source", field: "costSource", cellRendererFramework: GridSelectComponent
        , cellRendererParams: { references: this.costSourceData }, tooltipField: "programMessage"
      },
      {
        headerName: "Unit Cost", field: "unitCost", cellRendererFramework: GridAmountComponent
        , cellClassRules: {
          "invalid-grid-row": function (params: any): any { return !params.value; }
        }, tooltipField: "programMessage"
      },
      { headerName: "Final Unit Cost", field: "unitCostInit", tooltipField: "unitCostInit", cellRendererFramework: GridAmountComponent },
      {
        headerName: "Quantity", field: "qtyOrdered", cellRendererFramework: GridNumericComponent, width: 220
        , cellClassRules: {
          "invalid-grid-row": function (params: any): any { return !params.value; }
        }, tooltipField: "programMessage"
      },
      { headerName: "Total Cost", field: "totalCost", cellRendererFramework: GridAmountComponent, tooltipField: "programMessage" },
      {
        headerName: "Estimated", field: "estimatedInstockDate", tooltipField: "estimatedInstockDate | date:'dd-MMM-yyyy'"
        , cellRendererFramework: GridDateComponent, filter: "date", suppressSizeToFit: true, width: 150
      },
      {
        headerName: "Non-Scale", field: "nonScaleInd", cellRendererFramework: GridSelectComponent
        , cellRendererParams: { references: this.indicatorData }, tooltipField: "programMessage"
      },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 150
        , cellRendererParams: { restrictEditIcon: true }
      }
    ];

    this.gridOptions = {
      onGridReady: (params: any) => {
        this.gridApi = params.api;
      },
      //getRowStyle: (params) => {
      //  if (params.data.programMessage && params.data.programMessage.trim() !== "") {
      //    return { background: "#eaacacc9" };
      //  }
      //},
      context: {
        componentParent: this
      },
      enableColResize: true
    };
    if (this.service.serviceDocument.dataProfile.dataModel) {
      this.bind();
    }
  }

  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }

  bind(): void {
    this.routeServiceSubscription = this.route.params.subscribe((resp: any) => {
      this.primaryId = resp["id"];
    });


    if (this.primaryId.toString() !== "0") {
      this.editMode = true;
      this.PageTitle = `${this.PageTitle} - EDIT - ${this.primaryId}`;
      this.orderStatus = this.service.serviceDocument.dataProfile.dataModel.status;
      this.locationData = this.locationList.filter(item => item.locType
        === this.service.serviceDocument.dataProfile.profileForm.controls["locType"].value);
      if (this.service.serviceDocument.dataProfile.dataModel.orderDetails) {
        Object.assign(this.data, this.service.serviceDocument.dataProfile.dataModel.orderDetails);
        Object.assign(this.dataReset, this.service.serviceDocument.dataProfile.dataModel.orderDetails);
        this.orginalValue = JSON.stringify(this.dataReset);
      }
    } else {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign(this.service.serviceDocument.dataProfile.dataModel
        , { operation: "I", status: "Worksheet", importOrderInd: "N" }
        , { workflowInstance: this.service.serviceDocument.dataProfile.dataModel.workflowInstance });
      this.service.serviceDocument.dataProfile.profileForm.patchValue(this.service.serviceDocument.dataProfile.dataModel);
      this.PageTitle = `${this.PageTitle} - ADD`;
    }

    this.serviceDocument = this.service.serviceDocument;
    this.sharedService.fileCount = this.service.serviceDocument.dataProfile.dataModel.fileCount;
    this.sharedService.checkWfPermitions(this.serviceDocument);
  }

  save(saveOnly: boolean): void {
    if (this.orderStatus !== "RequestCompleted") {
      if (this.serviceDocument.dataProfile.profileForm.valid) {
        this.data = this.data.filter(id => id.unitCost !== null);
        if (this.data.length > 0) {
          if (this.validateData()) {
            this.saveSubscription(saveOnly);
          } else {
            this.sharedService.errorForm(this.localizationData.orderHead.orderdetailsrequired);
          }
        } else {
          this.sharedService.errorForm(this.localizationData.orderHead.orderdetailsatleastone);
        }
      } else {
        this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
      }
    }
  }

  saveAndContinue(): void {
    this.save(false);
  }

  saveSubscription(saveOnly: boolean): void {
    this.loaderService.display(true);
    if (this.deletedRows.length > 0) {
      this.deletedRows.forEach(x => this.data.push(x));
    }
    this.serviceDocument.dataProfile.profileForm.controls["orderDetails"] = new FormControl();
    this.serviceDocument.dataProfile.profileForm.controls["orderDetails"].setValue(this.data);
    this.saveServiceSubscription = this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.primaryId = this.service.serviceDocument.dataProfile.dataModel.ordeR_NO;
        this.sharedService.saveForm(`${this.localizationData.orderHead.orderheadsave} - Order No ${this.primaryId}`).subscribe(() => {
          if (saveOnly) {
            this.router.navigate(["/Order/List"], { skipLocationChange: true });
          } else {
            this.router.navigate(["/Blank"], {
              skipLocationChange: true, queryParams: {
                id: "/Order/New/" + this.primaryId
              }
            });
            this.loaderService.display(false);
          }
        }, (error: string) => { console.log(error); });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
    }, (error: string) => { console.log(error); });
    this.loaderService.display(false);
  }

  reset(): void {
    Object.keys(this.serviceDocument.dataProfile.profileForm.controls).forEach(control => {
      this.serviceDocument.dataProfile.profileForm.controls[control].setValue(this.serviceDocument.dataProfile.dataModel[control]);
    });
    this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
  }

  submitAction(): void {
    this.loaderService.display(true);
    this.service.submit().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.primaryId = this.service.serviceDocument.dataProfile.dataModel.ordeR_NO;
        this.sharedService.saveForm(`Submitted - Order No ${this.primaryId}`).subscribe(() => {
          this.router.navigate(["/Order/List"], { skipLocationChange: true });
        }, (error: string) => {
          console.log(error);
          this.loaderService.display(false);
        });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
        this.loaderService.display(false);
      }
    }, (error: string) => {
      console.log(error);
      this.loaderService.display(false);
    });
  }

  sendBackForReview(): void {
    this.loaderService.display(true);
    this.service.sendBackForReview().subscribe(() => {
      this.workflowActionResult("Sent back - Order No ", "/Order/List");
      this.loaderService.display(false);
    });
  }

  workflowActionResult(message: string, navigateUrl: string): void {
    if (this.service.serviceDocument.result.type === MessageType.success) {
      this.sharedService.saveForm(`${message}  ${this.primaryId}`).subscribe(() => {
        this.router.navigate([navigateUrl], { skipLocationChange: true });
      });
    } else {
      this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
    }
  }

  reject(): void {
    this.loaderService.display(true);
    this.service.reject().subscribe(() => {
      this.workflowActionResult("Rejected - Order No ", "/Order/List");
      this.loaderService.display(false);
    });
  }

  ngOnDestroy(): void {
    this.routeServiceSubscription.unsubscribe();
    if (this.supplierDetailsServiceSubscription) {
      this.supplierDetailsServiceSubscription.unsubscribe();
    }


    if (this.saveServiceSubscription) {
      this.saveServiceSubscription.unsubscribe();
    }

    if (this.itemDetailsServiceSubscription) {
      this.itemDetailsServiceSubscription.unsubscribe();
    }

    this.orderTypeData.length = 0;
    this.locTypeData.length = 0;
    this.locationList.length = 0;
    this.deptData.length = 0;
    this.purchaseTypeData.length = 0;
    this.poTypeData.length = 0;
    this.billtoIdData.length = 0;
    this.orderOriginData.length = 0;
    this.importCountryData.length = 0;
    this.indicatorData.length = 0;
    this.costSourceData.length = 0;
  }

  close(): void {
    this.messageResult.message = `<div>All your changes will be lost if you don't save them.</div>
                                    <div>Click on “Save” to Keep the changes.</div>
                                    <div>Click on “Don't save” to proceed further without saving changes.</div>
                                    <div>Click on “Cancel” to stay on the current page.</div>`;
    if ((this.serviceDocument.dataProfile.profileForm && this.serviceDocument.dataProfile.profileForm.dirty)
      || (this.serviceDocument.dataProfile.profileForm.dirty && !this.serviceDocument.dataProfile.profileForm.valid)) {
      this.dialogRef = this.dialogService.openMessageDialog("Warning", this.messageResult
        , [new RbButton("", "Cancel", "alertCancel"), new RbButton("", "Don't Save", "alertdontsave")
          , new RbButton("", "Save", "alertSave")], "37%", "", "iconsdontsave retailsNavIcons", "dontsaveAlert", "dontsaveAlertTitle");
      this.dialogRef.componentInstance.click.subscribe(
        btnName => {
          if (btnName === "Cancel") {
            this.dialogRef.close();
          }
          if (btnName === "Don't Save") {
            this.serviceDocument.dataProfile.profileForm.markAsUntouched();
            this.canNavigate = true;
            this.dialogRef.close();
            this.router.navigate([this.sharedService.previousUrl], { skipLocationChange: true });
          }
          if (btnName === "Save") {
            this.dialogRef.close();
            this.save(true);
            this.router.navigate([this.sharedService.previousUrl], { skipLocationChange: true });
          }
        });
    } else {
      this.canNavigate = true;
      this.router.navigate([this.sharedService.previousUrl], { skipLocationChange: true });
    }
  }

  changeDept(event: any): void {
    this.serviceDocument.dataProfile.profileForm.controls["buyer"].setValue(event.options.buyerId);
    this.serviceDocument.dataProfile.profileForm.controls["buyerName"].setValue(event.options.buyerName);
  }

  changeSupplier(event: any): void {
    let apiSupplierUrl: string = "/api/OrderHead/SupplerDetailsGet?supplier=" + event.options.supplier;
    this.supplierDetailsServiceSubscription = this.service.getDataFromAPI(apiSupplierUrl).subscribe((response: SupplierDetailDomainModel[]) => {
      if (response && response.length > 0) {
        this.serviceDocument.dataProfile.profileForm.patchValue({
          qcInd: response[0].qcInd,
          terms: response[0].termsId,
          termsDesc: response[0].termsDesc,
          freightTerms: response[0].freightTerms,
          freightDesc: response[0].freightTermsDesc,
          paymentMethod: response[0].paymentMethod,
          payMethodName: response[0].paymentMethodDesc,
          shipMethod: response[0].shipMethod,
          shipmentDesc: response[0].shipMethodDesc,
          currencyCode: response[0].currencyCode,
          exchangeRate: response[0].exchangeRate
        });
      }
    });
  }

  public locTypeChanged(): void {
    this.locationData = this.locationList.filter(item => item.locType === this.serviceDocument.dataProfile.profileForm.controls["locType"].value);
  }

  changeEarliestShipDate(): void {
    this.serviceDocument.dataProfile.profileForm.controls["latestShipDate"].setValue(null);
    this.serviceDocument.dataProfile.profileForm.controls["notBeforeDate"].setValue(null);
    this.serviceDocument.dataProfile.profileForm.controls["notAfterDate"].setValue(null);
  }

  changeNotBeforeDate(): void {
    this.serviceDocument.dataProfile.profileForm.controls["notAfterDate"].setValue(null);
  }

  getCurrencyAttribute(columnName: any): string {
    let currencyAttr: string;
    if (columnName in { "unitCost": 1, "totalCost": 1, "unitCostInit": 1 }) {
      currencyAttr = "costCurrency";
    }
    return currencyAttr;
  }

  select(cell: any, event: any): void {
    this.data[cell.rowIndex][cell.column.colId] = event.target.value;
    let rowNode: RowNode = this.gridApi.getRowNode(cell.rowIndex);
    rowNode.setData(rowNode.data);
  }

  update(cell: any, event: any): void {
    if (event.target.value) {
      this.data[cell.rowIndex][cell.column.colId] = event.target.value;
      if (cell.column.colId === "item") {
        this.itemBasedLogic(cell, event);
        this.data[cell.rowIndex]["qtyOrdered"] = null;
      } else if (cell.column.colId === "qtyOrdered" || cell.column.colId === "unitCostInit") {
        let total: number = null;
        let cost: number = this.data[cell.rowIndex]["unitCostInit"];
        let qtyOrdered: number = this.data[cell.rowIndex]["qtyOrdered"];
        if (cost && qtyOrdered) {
          total = qtyOrdered * cost;
        }
        this.data[cell.rowIndex]["totalCost"] = total;
      }
      let rowNode: RowNode = this.gridApi.getRowNode(cell.rowIndex);
      rowNode.setData(rowNode.data);
    }
  }

  open(cell: any, mode: string): void {
    this.sharedService.editObject = this.data[cell.rowIndex];
    //if (this.sharedService.editObject.costSource) {
    //  this.sharedService.editObject.costSourceDesc = this.costSourceData
    //    .filter(id => id.code === this.sharedService.editObject.costSource)[0].codeDesc;
    //}
    if (this.sharedService.editObject.nonScaleInd) {
      this.sharedService.editObject.nonScaleIndDesc = this.indicatorData
        .filter(id => id.code === this.sharedService.editObject.nonScaleInd)[0].codeDesc;
    }
    this.sharedService.viewForm(OrderInfoComponent, cell, "orderdtlsview", "", this.service.serviceDocument.dataProfile.dataList);
  }

  add($event: MouseEvent): void {
    $event.preventDefault();
    let fm: FormGroup = this.serviceDocument.dataProfile.profileForm;
    this.sharedService.validateForm(fm);
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      let filterData: OrderDetailModel[] = this.data.filter(id => id.unitCost === null && id.operation !== "D");
      if (filterData.length === 0) {
        this.newModel();
        this.data.unshift(this.detailModel);
        if (this.gridApi) {
          this.gridApi.setRowData(this.data);
        }
      }
    }
  }

  newModel(): void {
    this.pForm = this.serviceDocument.dataProfile.profileForm;
    this.detailModel = {
      item: null, unitRetail: null, qtyOrdered: null, unitCost: null, unitCostInit: null, costSource: "MANL", nonScaleInd: null
      , tsfPoLinkNo: null, estimatedInstockDate: this.pForm.controls["pickupDate"].value, operation: "I", originCountryId: null
      , itemDesc: null, defaultUnitSize: null
    };
  }

  itemBasedLogic(cell: any, event: any): void {
    this.pForm = this.serviceDocument.dataProfile.profileForm;
    if (this.data[cell.rowIndex]["item"]) {
      let itmGridData: OrderDetailModel[] = this.data ? this.data.filter(itm => (itm.item === this.data[cell.rowIndex]["item"])) : null;
      if (itmGridData.length > 1) {
        this.sharedService.errorForm(this.localizationData.orderHead.orderdetailscheck);
      } else {
        let apiOrderItemUrl: string = "/api/OrderHead/OrderItemGet?supplier=" + this.pForm.controls["supplier"].value
          + "&itemBarcode=" + this.data[cell.rowIndex]["item"]
          + "&dept=" + this.pForm.controls["dept"].value
          + "&location=" + this.pForm.controls["location"].value;
        this.itemDetailsServiceSubscription = this.service.getDataFromAPI(apiOrderItemUrl).subscribe((response: AjaxModel<OrderItemSelectModel[]>) => {
          if (response.result === AjaxResult.success) {
            this.data[cell.rowIndex]["unitCost"] = response.model[0].unitCost;
            this.data[cell.rowIndex]["unitCostInit"] = response.model[0].afterDiscUnitCost;
            this.data[cell.rowIndex]["costSource"] = "MANL";
            this.data[cell.rowIndex]["unitRetail"] = response.model[0].unitRetail;
            this.data[cell.rowIndex]["itemDesc"] = response.model[0].itemDesc;
            this.data[cell.rowIndex]["originCountryId"] = response.model[0].importCountry;
            this.data[cell.rowIndex]["defaultUnitSize"] = response.model[0].defaultUnitSize;
          } else {
            this.sharedService.errorForm(response.message);
            this.data[cell.rowIndex]["item"] = null;
          }
          let rowNode: RowNode = this.gridApi.getRowNode(cell.rowIndex);
          rowNode.setData(rowNode.data);
        }, (error: string) => { console.log(error); });
      }
    }
  }

  validateData(): boolean {
    let valid: boolean = true;
    this.data.forEach(row => {
      this.mandatoryOrderDetailsFields.forEach(field => {
        if (!row[field]) {
          valid = false;
        }
      });
    });
    return valid;
  }

  delete(cell: any): void {
    if (cell.data.operation === "U") {
      cell.data.operation = "D";
      this.deletedRows.push(cell.data);
    }
    this.data.splice(cell.rowIndex, 1);
    this.gridApi.setRowData(this.data);
  }
}
