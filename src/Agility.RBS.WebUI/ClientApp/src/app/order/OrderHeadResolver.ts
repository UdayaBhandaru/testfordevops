import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { OrderHeadModel } from "./OrderHeadModel";
import { OrderHeadService } from "./OrderHeadService";

@Injectable()
export class OrderHeadResolver implements Resolve<ServiceDocument<OrderHeadModel>> {
    constructor(private service: OrderHeadService) { }
    resolve(route: ActivatedRouteSnapshot): Observable<ServiceDocument<OrderHeadModel>> {
        return this.service.addEdit(route.params["id"]);
    }
}
