import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { OrderHeadResolver } from "./OrderHeadResolver";
import { OrderListComponent } from "./OrderListComponent";
import { OrderHeadComponent } from "./OrderHeadComponent";
import { OrderListResolver } from "./OrderListResolver";

const OrderRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "", redirectTo: "List", pathMatch: "full"
      },
      {
        path: "List",
        component: OrderListComponent,
        resolve:
        {
          references: OrderListResolver
        }
      },
      {
        path: "New/:id",
        component: OrderHeadComponent,
        resolve:
        {
          references: OrderHeadResolver
        }
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(OrderRoutes)
  ]
})
export class OrderRoutingModule { }
