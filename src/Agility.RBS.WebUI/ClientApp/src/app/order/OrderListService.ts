import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { OrderSearchModel } from "./OrderSearchModel";
import { SharedService } from "../Common/SharedService";

@Injectable()
export class OrderListService {
    serviceDocument: ServiceDocument<OrderSearchModel> = new ServiceDocument<OrderSearchModel>();
    orderPaging: {
        startRow: number,
        pageSize: number,
        cacheSize: number,
    } = {
        startRow: 0,
        pageSize: 10,
        cacheSize: 100,
    };

    constructor(private sharedService: SharedService) { }

    newModel(model: OrderSearchModel): ServiceDocument<OrderSearchModel> {
        return this.serviceDocument.newModel(model);
    }

    search(additionalParams: any): Observable<ServiceDocument<OrderSearchModel>> {
        return this.serviceDocument.search("/api/OrderSearch/Search", true, () => this.setSubClassId(additionalParams));
    }

    list(): Observable<ServiceDocument<OrderSearchModel>> {
        return this.serviceDocument.list("/api/OrderSearch/List");
    }

    setSubClassId(additionalParams: any): void {
        var sd: OrderSearchModel = this.serviceDocument.dataProfile.dataModel;
        if (additionalParams) {
            sd.startRow = additionalParams.startRow ? additionalParams.startRow : this.orderPaging.startRow;
            sd.endRow = additionalParams.endRow ? additionalParams.endRow : this.orderPaging.cacheSize;
            sd.sortModel = additionalParams.sortModel;
        } else {
            sd.startRow = this.orderPaging.startRow;
            sd.endRow = this.orderPaging.cacheSize;
        }

        if (sd !== null) {
            if (sd.startDate != null) {
                sd.startDate = this.sharedService.setOffSet(sd.startDate);
            }
            if (sd.endDate != null) {
                sd.endDate = this.sharedService.setOffSet(sd.endDate);
            }
        }
    }
}
