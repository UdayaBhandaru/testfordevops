import { Injectable } from "@angular/core";
import { Resolve } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { OrderSearchModel } from "./OrderSearchModel";
import { OrderListService } from "./OrderListService";

@Injectable()
export class OrderListResolver implements Resolve<ServiceDocument<OrderSearchModel>> {
    constructor(private service: OrderListService) { }
    resolve(): Observable<ServiceDocument<OrderSearchModel>> {
        return this.service.list();
    }
}
