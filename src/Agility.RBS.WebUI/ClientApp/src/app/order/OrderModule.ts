import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { AgGridSharedModule } from "../AgGridSharedModule";
import { TitleSharedModule } from "../TitleSharedModule";
import { DatePickerModule } from "../Common/DatePickerModule";
import { OrderRoutingModule } from "./OrderRoutingModule";

import { OrderListComponent } from "./OrderListComponent";
import { OrderListService } from "./OrderListService";
import { OrderListResolver } from "./OrderListResolver";

import { OrderHeadComponent } from "./OrderHeadComponent";
import { OrderHeadService } from "./OrderHeadService";
import { OrderHeadResolver } from "./OrderHeadResolver";

import { InboxService } from "../inbox/InboxService";
import { ShipmentService } from "../SupplyChainManagement/ShipmentService";
import { RbsSharedModule } from "../RbsSharedModule";
import { OrderInfoComponent } from './OrderInfoComponent';

@NgModule({
    imports: [
        FrameworkCoreModule,
        OrderRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        DatePickerModule,
        RbsSharedModule
    ],
    declarations: [
        OrderListComponent,
      OrderHeadComponent,
      OrderInfoComponent
    ],
    providers: [
        OrderListService,
        OrderListResolver,
        OrderHeadResolver,
        OrderHeadService,
        InboxService,
        ShipmentService
    ]
})
export class OrderModule {
    constructor() {
        LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
    }
}
