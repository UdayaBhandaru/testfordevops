﻿export class OrderSearchModel {
    supplier: string;
    supName: string;
    deptId: number;
    deptDesc: string;
    locType: string;
    locTypeDesc: string;
    location: number;
    locationName: string;
    itemBarcode: string;
    itemDesc: string;
    ordeR_NO: number;
    orderType: string;
    orderTypeDesc: string;
    orderDesc: string;
    status: string;
    statusDesc: string;
    dateType: string;
    startDate: Date;
    endDate: Date;
    operation: string;
    tableName: string;
    startRow?: number;
    endRow?: number;
    totalRows?: number;
    sortModel?: {}[];
    createdDate?: Date;
}