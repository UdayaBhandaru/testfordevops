import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { InvoiceMatchingDocSearchModel } from "./InvoiceMatchingDocSearchModel";
import { SharedService } from '../Common/SharedService';
import { HttpHelperService } from '../Common/HttpHelperService'

@Injectable()
export class InvoiceMatchingDocListService {    
    serviceDocument: ServiceDocument<InvoiceMatchingDocSearchModel> = new ServiceDocument<InvoiceMatchingDocSearchModel>();
    orderPaging: {
        startRow: number,
        pageSize: number,
        cacheSize: number,
    } = {
        startRow: 0,
        pageSize: 10,
        cacheSize: 100,
    };

  constructor(private httpHelperService: HttpHelperService,private sharedService: SharedService) { }

    newModel(model: InvoiceMatchingDocSearchModel): ServiceDocument<InvoiceMatchingDocSearchModel> {
        return this.serviceDocument.newModel(model);
    }

    search(additionalParams: any): Observable<ServiceDocument<InvoiceMatchingDocSearchModel>> {
      return this.serviceDocument.search("/api/InvoiceMatchingDocSearch/Search", true, () => this.setSubClassId(additionalParams));
    }

    list(): Observable<ServiceDocument<InvoiceMatchingDocSearchModel>> {
      return this.serviceDocument.list("/api/InvoiceMatchingDocSearch/List");
    }

    setSubClassId(additionalParams: any): void {
        var sd: InvoiceMatchingDocSearchModel = this.serviceDocument.dataProfile.dataModel;
        if (additionalParams) {
            sd.startRow = additionalParams.startRow ? additionalParams.startRow : this.orderPaging.startRow;
            sd.endRow = additionalParams.endRow ? additionalParams.endRow : this.orderPaging.cacheSize;
            sd.sortModel = additionalParams.sortModel;
        } else {
            sd.startRow = this.orderPaging.startRow;
            sd.endRow = this.orderPaging.cacheSize;
      }
      if (sd !== null) {
        if (sd.createDate != null) {
          sd.createDate = this.sharedService.setOffSet(sd.createDate);
        }
        //if (sd.toExtInvcDate != null) {
        //  sd.toExtInvcDate = this.sharedService.setOffSet(sd.toExtInvcDate);
        //}

        //if (sd.fromPaidDate != null) {
        //  sd.fromPaidDate = this.sharedService.setOffSet(sd.fromPaidDate);
        //}
        //if (sd.toPaidDate != null) {
        //  sd.toPaidDate = this.sharedService.setOffSet(sd.toPaidDate);
        //}
      }
  }
  getDataFromAPI(apiUrl: string): Observable<any> {
    let resp: Observable<any> = this.httpHelperService.get(apiUrl);
    return resp;
  }
}
