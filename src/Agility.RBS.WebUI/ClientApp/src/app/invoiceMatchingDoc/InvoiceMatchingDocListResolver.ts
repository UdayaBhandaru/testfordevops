import { Injectable } from "@angular/core";
import { Resolve } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { InvoiceMatchingDocSearchModel } from './InvoiceMatchingDocSearchModel';
import { InvoiceMatchingDocListService } from './InvoiceMatchingDocListService';

@Injectable()
export class InvoiceMatchingDocListResolver implements Resolve<ServiceDocument<InvoiceMatchingDocSearchModel>> {
  constructor(private service: InvoiceMatchingDocListService) { }
  resolve(): Observable<ServiceDocument<InvoiceMatchingDocSearchModel>> {
    return this.service.list();
  }
}
