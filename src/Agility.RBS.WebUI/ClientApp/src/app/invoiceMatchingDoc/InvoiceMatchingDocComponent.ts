import { Component, OnInit, TemplateRef, ViewChild, OnDestroy, ChangeDetectorRef } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ServiceDocument, CommonService, MessageResult, MessageType } from "@agility/frameworkcore";
import { MatDialogRef } from "@angular/material";
import { GridOptions, GridApi, RowNode, ColumnApi } from "ag-grid-community";
import { Subscription } from "rxjs";
import { FormControl } from '@angular/forms';
import { DomainDetailModel } from '../domain/DomainDetailModel';
import { LocationDomainModel } from '../Common/DomainData/LocationDomainModel';
import { LoaderService } from '../Common/LoaderService';
import { SharedService } from '../Common/SharedService';
import { RbDialogService } from '../Common/controls/RbDialogService';
import { DocumentsConstants } from '../Common/DocumentsConstants';
import { GridSelectComponent } from '../Common/grid/GridSelectComponent';
import { GridNumericComponent } from '../Common/grid/GridNumericComponent';
import { RbButton } from '../Common/controls/RbControlModels';
import { InvoiceMatchingDocService } from './InvoiceMatchingDocService';
import { TransShipmentDomainModel } from '../Common/DomainData/TransShipmentDomainModel';
import { PartnerDomainModel } from '../Common/DomainData/PartnerDomainModel';
import { InvoiceMatchingDocGroupHeadModel } from './InvoiceMatchingDocGroupHeadModel';
import { InvoiceMatchingDocHeadModel } from './InvoiceMatchingDocHeadModel';
import { GridDateComponent } from '../Common/grid/GridDateComponent';
import { InvoiceMatchingDocOrderDetailsModel } from './InvoiceMatchingDocOrderDetailsModel';
import { NonMerchandiseCostPopupComponent } from './NonMerchandiseCost/NonMerchandiseCostPopupComponent';
import { NonMerchandiseComponent } from './NonMerchandiseCost/NonMerchandiseComponent';
import { NonMerchandiseCostPopupService } from './NonMerchandiseCost/NonMerchandiseCostPopupService';
import { NonMerchandiseCostDomainModel } from './NonMerchandiseCost/NonMerchandiseCostDomainModel';
import { NonMerchandiseCostModel } from './NonMerchandiseCost/NonMerchandiseCostModel';
import { VatBreakdownPopupComponent } from './VatBreakdown/VatBreakdownPopupComponent';
import { VatBreakdownDomainModel } from './VatBreakdown/VatBreakdownDomainModel';
import { VatBreakdownPopupService } from './VatBreakdown/VatBreakdownPopupService';
import { VatRateModel } from '../valueaddedtax/vatrates/VatRateModel';


@Component({
  selector: "Invoice MatchingDoc-add",
  templateUrl: "./InvoiceMatchingDocComponent.html",
  styleUrls: ['./InvoiceMatchingDocComponent.scss']

})
export class InvoiceMatchingDocComponent implements OnInit, OnDestroy {
  screenName: any;
  _componentName = "InvoiceMatchingDoc";
  PageTitle: string = "Invoice Matching Doc";
  public serviceDocument: ServiceDocument<InvoiceMatchingDocGroupHeadModel>;
  localizationData: any;
  primaryId: number; // used in CanDeactivate method, should be same name if we use candeactivate
  datesArray: any[] = [];
  componentParentName: InvoiceMatchingDocComponent;
  editMode: boolean;
  apiUrl: string;
  obligationStatus: string;
  invoiceMatchingDocOrderDetails: InvoiceMatchingDocOrderDetailsModel[];
  NonMerchandiseCostList: NonMerchandiseCostModel[];
  VatRateData: VatRateModel[];
  findVatRateApiUrl = "/api/InvoiceMatchingVat/InvoiceMatchingVatDomainData";

  dialogRef: MatDialogRef<any>;
  private messageResult: MessageResult = new MessageResult();
  canNavigate: boolean;
  effectiveMinDate: Date = new Date();
  currentDate: Date = new Date();
  routeServiceSubscription: Subscription;
  supplierDetailsServiceSubscription: Subscription;
  itemDetailsServiceSubscription: Subscription;
  saveServiceSubscription: Subscription;
  partenerDetailsServiceSubscription: Subscription;
  orderDetailsServiceSubscription: Subscription;
  nonMerchandiseCostPopupSubscription: Subscription;
  nonMerchandiseCostListPopupSubscription: Subscription;
  vatBreakdownPopupSubscription: Subscription;
  vatBreakdownVatPopupSubscription: Subscription;
  gridOptions: GridOptions;
  gridApi: GridApi;
  gridColumnApi: ColumnApi;
  componentName: any;
  columns: any[];
  data: InvoiceMatchingDocHeadModel[] = [];
  dataReset: InvoiceMatchingDocHeadModel[] = [];
  orginalValue: string = "";
  addText: string = "+ Add Item";
  detailModel: InvoiceMatchingDocHeadModel;
  obligationData: DomainDetailModel[];
  partnerTypeData: DomainDetailModel[];
  partnerDomainData: PartnerDomainModel[];
  partnerData: PartnerDomainModel[];
  paymentTypeData: DomainDetailModel[];
  checkAuthData: DomainDetailModel[];
  statusData: DomainDetailModel[];
  shipmentData: TransShipmentDomainModel[];
  shipDetails: TransShipmentDomainModel[];
  documentTypeData: DomainDetailModel[];
  vendorTypeData: DomainDetailModel[];
  locationData: LocationDomainModel[];
  wfHeader: boolean = false;
  partnerValue: boolean;
  supplierValue: boolean;
  partnerType: boolean;
  partnerTypeDesc: string = "";
  InAlcInd: string = "";
  temp: {};
  pForm: any;
  result: number;
  wfCount: number;
  today: Date;
  mandatoryInvoiceMatchingDoclsFields: string[] = [
    "type",
    "location",
    "docDate",
    "vendor",
    "vendorType",
    "terms",
    "currencyCode",
    "exchangeRate",
    "totalCost",
    "totalQty",
    "totalCostIncVat"
  ];
  deletedRows: any[] = [];

  constructor(private route: ActivatedRoute, private router: Router, public sharedService: SharedService, private service: InvoiceMatchingDocService
    , private loaderService: LoaderService, private changeRef: ChangeDetectorRef, private dialogService: RbDialogService,
    private nonMerchandiseCostPopupService: NonMerchandiseCostPopupService, private vatBreakdownPopupService: VatBreakdownPopupService) {
  }

  ngOnInit(): void {
    this.screenName = new DocumentsConstants().obligationObjectName;
    this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);

    this.documentTypeData = Object.assign([], this.service.serviceDocument.domainData["documentType"]);
    this.vendorTypeData = Object.assign([], this.service.serviceDocument.domainData["vendorType"]);
    this.partnerDomainData = Object.assign([], this.service.serviceDocument.domainData["partner"]);
    this.locationData = Object.assign([], this.service.serviceDocument.domainData["location"]);
    this.apiUrl = "/api/ObligationHead/SuppliersObligationGetName?name=";
    this.componentParentName = this;

    this.columns = [
      {
        headerName: "Document Type", field: "type", cellRendererFramework: GridSelectComponent, cellClassRules: {
          "invalid-grid-row": function (params: any): any { return !params.value; }
        }, cellRendererParams: { references: this.documentTypeData, keyvaluepair: { key: "code", value: "codeDesc" } },
        tooltipField: "code", headerTooltip: "Document Type"
      },
      {
        headerName: "Order#", field: "orderNO", cellRendererFramework: GridNumericComponent
        , tooltipField: "orderNO", headerTooltip: "Order#"
      },
      {
        headerName: "Vendor Type", field: "vendorType", tooltipField: "vendorType", headerTooltip: "vendorType", hide: true
      },
      {
        headerName: "vendor Type Desc", field: "vendorTypeDesc", tooltipField: "vendorTypeDesc", headerTooltip: "Vendor Type Desc"
      },
      {
        headerName: "Vendor Name", field: "vendor", hide: true
      },
      {
        headerName: "Vendor Name", field: "vendorName", tooltipField: "vendorName", headerTooltip: "Vendor Name"
      },
      {
        headerName: "Document No", field: "rate", cellRendererFramework: GridNumericComponent
        , tooltipField: "rate", headerTooltip: "rate", hide: true
      },
      {
        headerName: "Document Date", field: "docDate", tooltipField: "Document Date | date:'dd-MMM - yyyy'"
        , cellRendererFramework: GridDateComponent, filter: "date", suppressSizeToFit: true, width: 150
      },
      {
        headerName: "Terms", field: "terms", tooltipField: "terms", headerTooltip: "Terms"
      },
      {
        headerName: "Location", field: "location", cellRendererFramework: GridSelectComponent, cellClassRules: {
          "invalid-grid-row": function (params: any): any { return !params.value; }
        }, cellRendererParams: { references: this.locationData, keyvaluepair: { key: "locationId", value: "locationDescription" } },
        tooltipField: "locationId", headerTooltip: "Location"
      },
      {
        headerName: "Location", field: "locName", tooltipField: "location", headerTooltip: "locName", hide: true
      },
      {
        headerName: "LocationType", field: "locType", hide: true
      },
      {
        headerName: "Total Qty", field: "totalQty", cellRendererFramework: GridNumericComponent
        , cellClassRules: {
          "invalid-grid-row": function (params: any): any { return !params.value; }
        }, tooltipField: "totalQty", headerTooltip: "Total Qty"
      },
      ,
      {
        headerName: "CurrencyCode", field: "currencyCode", hide: true
      }
      ,
      {
        headerName: "exchangeRate", field: "exchangeRate", hide: true
      },
      {
        headerName: "Reference#", field: "refDoc", cellRendererFramework: GridNumericComponent, tooltipField: "refDoc", headerTooltip: "Reference#"
      },
      {
        headerName: "Merchandise Cost", field: "merchCost", cellRendererFramework: GridNumericComponent
      },
      {
        headerName: "Non Merchandise Cost", field: "nonMerchCost", cellRendererFramework: NonMerchandiseComponent
      },
      {
        headerName: "Total Cost", field: "totalCost", tooltipField: "totalCost", headerTooltip: "Total Cost"
      },
      {
        headerName: "Total Vat", field: "totalVat", cellRendererFramework: NonMerchandiseComponent
      },
      {
        headerName: "Total Cost Inc Vat", field: "totalCostIncVat", tooltipField: "totalCostIncVat", headerTooltip: "Total CostInc Vat"
      }
    ];

    this.gridOptions = {
      onGridReady: (params: any) => {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
        if (this.gridColumnApi) {

        }
      },

      context: {
        componentParent: this
      },
      enableColResize: true
    };
    if (this.service.serviceDocument.dataProfile.dataModel) {
      this.bind();
    }
  }

  add($event: MouseEvent): void {
    $event.preventDefault();
    this.pForm = this.serviceDocument.dataProfile.profileForm;
    this.newModel();
    this.data.unshift(this.detailModel);
    let row = this.data.length;
    this.serviceDocument.dataProfile.profileForm.controls["calcCount"].setValue(row);
    if (this.gridApi) {
      this.gridApi.setRowData(this.data);
    }
  }

  newModel(): void {
    this.pForm = this.serviceDocument.dataProfile.profileForm;
    this.detailModel = {
      docId: null, type: null, typeDesc: null, status: null, statusDesc: null, orderNO: null, location: null, locName: null, locType: null, locTypeDesc: null,
      totalDiscount: null, groupId: null, parentId: null, docDate: null, createDate: null, createId: null, vendorType: null, vendorTypeDesc: null, vendor: null,
      vendorName: null, extDocId: null, ediUploadInd: null, terms: null, termsDscntPct: null, dueDate: null, paymentMethod: null, paymentMethodDesc: null, matchId: null,
      matchDate: null, approvalId: null, approvalDate: null, prePaidInd: null, prePaidId: null, postDate: null, currencyCode: null, exchangeRate: null, totalCost: null,
      totalQty: null, manuallyPaidInd: null, customDocRef1: null, customDocRef2: null, customDocRef3: null, customDocRef4: null, lastUpdateId: null, lastDateTime: null,
      freightType: null, freightTypeDesc: null, refDoc: null, refAuthNo: null, costFreMatch: null, detailMatched: null, bestTerms: null, bestTermsSource: null,
      bestTermsDate: null, bestTermsDateSource: null, varianceWithinTolerance: null, resolutionAdjustedTotalCost: null, resolutionAdjustedTotalQty: null,
      consignmentInd: null, dealId: null, rtvInd: null, discountDate: null, dealType: null, totalCostIncVat: null, vatDiscCreateDate: null, operation: "I",
      totalCostVat: null, merchCost: null, nonMerchCost: null, totalVat: null
    };
  }


  addToGrid($event: MouseEvent): void {
    this.pForm = this.serviceDocument.dataProfile.profileForm;
    if (this.pForm.value.type && this.pForm.value.supplier || this.pForm.value.vendor) {  
    $event.preventDefault();
    let valuevendor;
    if (this.pForm.controls["supplier"].value == null) {
      valuevendor = this.pForm.controls["vendor"].value;
    }
    else {
      valuevendor = this.pForm.controls["supplier"].value;
    }
    if (this.pForm.valid) {
      let emptyObject: any = {
        vendor: valuevendor, vendorName: this.pForm.controls["supNameDesc"].value, terms: this.pForm.controls["terms"].value, refDoc: this.pForm.controls["refDoc"].value,
        docDate: this.pForm.controls["docDate"].value, vendorType: this.pForm.controls["vendorType"].value, vendorTypeDesc: this.pForm.controls["vendorTypeDesc"].value,
        type: this.pForm.controls["type"].value, currencyCode: this.pForm.controls["currencyValue"].value, exchangeRate: this.pForm.controls["exchangeRate"].value, operation: "I", status: "WRKST"
      }

      let emptyObjectData: any = {
        vendor: valuevendor, vendorName: this.pForm.controls["vendorName"].value, terms: this.pForm.controls["terms"].value, refDoc: this.pForm.controls["refDoc"].value,
        docDate: this.pForm.controls["docDate"].value, vendorType: this.pForm.controls["vendorType"].value, vendorTypeDesc: this.pForm.controls["vendorTypeDesc"].value,
        type: this.pForm.controls["type"].value, currencyCode: this.pForm.controls["currencyValue"].value, exchangeRate: this.pForm.controls["exchangeRate"].value, operation: "I", status: "WRKST"
      }
      this.newModel();
      if (this.pForm.value.supplier === null) {
        this.data.push(Object.assign(emptyObjectData));
      } else {
        this.data.push(Object.assign(emptyObject));
      }
      if (this.pForm.value.type == "MRCHI") {
        this.sharedService.errorForm("Please Enter Valid Order# In Current Row in Detail Grid");
      }
      let row = this.data.length;
      this.serviceDocument.dataProfile.profileForm.controls["calcCount"].setValue(row);
      if (this.gridApi) {
        this.gridApi.setRowData(this.data);
      }
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }

    } else {
      this.sharedService.errorForm("Please Select Defaults");
    }
  }

  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }

  bind(): void {
    this.routeServiceSubscription = this.route.params.subscribe((resp: any) => {
      this.primaryId = resp["id"];
    });
    this.today = new Date();
    if (this.primaryId.toString() !== "0") {
      this.editMode = true;
      this.PageTitle = `${this.PageTitle} - EDIT - ${this.primaryId}`;
      if (this.service.serviceDocument.dataProfile.dataModel.invoiceMatchingDocHead) {
        Object.assign(this.data, this.service.serviceDocument.dataProfile.dataModel.invoiceMatchingDocHead);
        Object.assign(this.dataReset, this.service.serviceDocument.dataProfile.dataModel.invoiceMatchingDocHead);
        this.orginalValue = JSON.stringify(this.dataReset);
      }
      let value = 0;
      this.data.forEach(c => { value += c.totalCost });
      this.service.serviceDocument.dataProfile.profileForm.controls["calcCost"].setValue(value);
      let calcostValue = this.service.serviceDocument.dataProfile.profileForm.controls["controlTotalCost"].value;
      let diffcalcostValue = value - calcostValue;
      this.service.serviceDocument.dataProfile.profileForm.controls["varianceCost"].setValue(diffcalcostValue);

      this.result = this.service.serviceDocument.dataProfile.dataModel.controlRecCount - this.service.serviceDocument.dataProfile.dataModel.calcCount;
      this.service.serviceDocument.dataProfile.profileForm.controls["varianceCount"].setValue(this.result);
      this.wfCount = this.service.serviceDocument.dataProfile.profileForm.controls["varianceCount"].value;
      if (this.wfCount == 0) {
        this.wfHeader = true;
      }
    } else {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign(this.service.serviceDocument.dataProfile.dataModel
        , { operation: "I", createDate: new Date(), currencyCode: "KWD", status: "WRKST" }
        , { workflowInstance: this.service.serviceDocument.dataProfile.dataModel.workflowInstance });
      this.service.serviceDocument.dataProfile.profileForm.patchValue(this.service.serviceDocument.dataProfile.dataModel);
      this.PageTitle = `${this.PageTitle} - ADD`;
    }
    this.serviceDocument = this.service.serviceDocument;
    this.sharedService.checkWfPermitions(this.serviceDocument);
  }

  save(saveOnly: boolean): void {
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      if (this.data.length > 0) {
        if (this.validateData()) {
          this.saveSubscription(saveOnly);
        } else {
          this.sharedService.errorForm("Please Fill Invoice Matching Doc Head Mandatory fields");
        }
      } else {
        this.sharedService.errorForm("Please Add at least one invoice detail");
      }
    }
    else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  saveAndContinue(): void {
    this.save(false);
  }

  saveSubscription(saveOnly: boolean): void {
    this.serviceDocument.dataProfile.profileForm.controls["invoiceMatchingDocHead"] = new FormControl();
    this.serviceDocument.dataProfile.profileForm.controls["invoiceMatchingDocHead"].setValue(this.data);
    this.saveServiceSubscription = this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.primaryId = this.service.serviceDocument.dataProfile.dataModel.grouP_ID;
        this.sharedService.saveForm(`Invoice Matching Doc Saved Successfully ${this.primaryId}`).subscribe(() => {
          if (saveOnly) {
            this.router.navigate(["/invoiceMatchingDocument/List"], { skipLocationChange: true });
          } else {
            this.router.navigate(["/Blank"], {
              skipLocationChange: true, queryParams: {
                id: "/invoiceMatchingDocument/New/" + this.primaryId
              }
            });
            this.loaderService.display(false);
          }
        }, (error: string) => { console.log(error); });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
    }, (error: string) => { console.log(error); });
    this.loaderService.display(false);
  }

  reset(): void {
    Object.keys(this.serviceDocument.dataProfile.profileForm.controls).forEach(control => {
      this.serviceDocument.dataProfile.profileForm.controls[control].setValue(this.serviceDocument.dataProfile.dataModel[control]);
    });
    this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
  }

  submitAction(): void {
    this.loaderService.display(true);
    this.service.submit().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.primaryId = this.service.serviceDocument.dataProfile.dataModel.grouP_ID;
        this.sharedService.saveForm(`Submitted - InvoiceMatchingDoc No ${this.primaryId}`).subscribe(() => {
          this.router.navigate(["/invoiceMatchingDocument/List"], { skipLocationChange: true });
        }, (error: string) => {
          console.log(error);
          this.loaderService.display(false);
        });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
        this.loaderService.display(false);
      }
    }, (error: string) => {
      console.log(error);
      this.loaderService.display(false);
    });
  }

  sendBackForReview(): void {
    this.loaderService.display(true);
    this.service.sendBackForReview().subscribe(() => {
      this.workflowActionResult("Sent back - InvoiceMatchingDoc No ", "/invoiceMatchingDocument/List");
      this.loaderService.display(false);
    });
  }

  workflowActionResult(message: string, navigateUrl: string): void {
    if (this.service.serviceDocument.result.type === MessageType.success) {
      this.sharedService.saveForm(`${message}  ${this.primaryId}`).subscribe(() => {
        this.router.navigate([navigateUrl], { skipLocationChange: true });
      });
    } else {
      this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
    }
  }

  reject(): void {
    this.loaderService.display(true);
    this.service.reject().subscribe(() => {
      this.workflowActionResult("Rejected - InvoiceMatchingDoc No ", "/invoiceMatchingDocument/List");
      this.loaderService.display(false);
    });
  }

  ngOnDestroy(): void {
    this.routeServiceSubscription.unsubscribe();
    if (this.supplierDetailsServiceSubscription) {
      this.supplierDetailsServiceSubscription.unsubscribe();
    }


    if (this.saveServiceSubscription) {
      this.saveServiceSubscription.unsubscribe();
    }

    if (this.itemDetailsServiceSubscription) {
      this.itemDetailsServiceSubscription.unsubscribe();
    }
    this.vendorTypeData.length = 0;
    this.documentTypeData.length = 0;
  }

  close(): void {
    this.messageResult.message = `<div>All your changes will be lost if you don't save them.</div>
                                    <div>Click on “Save” to Keep the changes.</div>
                                    <div>Click on “Don't save” to proceed further without saving changes.</div>
                                    <div>Click on “Cancel” to stay on the current page.</div>`;
    if ((this.serviceDocument.dataProfile.profileForm && this.serviceDocument.dataProfile.profileForm.dirty)
      || (this.serviceDocument.dataProfile.profileForm.dirty && !this.serviceDocument.dataProfile.profileForm.valid)) {
      this.dialogRef = this.dialogService.openMessageDialog("Warning", this.messageResult
        , [new RbButton("", "Cancel", "alertCancel"), new RbButton("", "Don't Save", "alertdontsave")
          , new RbButton("", "Save", "alertSave")], "37%", "", "iconsdontsave retailsNavIcons", "dontsaveAlert", "dontsaveAlertTitle");
      this.dialogRef.componentInstance.click.subscribe(
        btnName => {
          if (btnName === "Cancel") {
            this.dialogRef.close();
          }
          if (btnName === "Don't Save") {
            this.serviceDocument.dataProfile.profileForm.markAsUntouched();
            this.canNavigate = true;
            this.dialogRef.close();
            this.router.navigate([this.sharedService.previousUrl], { skipLocationChange: true });
          }
          if (btnName === "Save") {
            this.dialogRef.close();
            this.save(true);
            this.router.navigate([this.sharedService.previousUrl], { skipLocationChange: true });
          }
        });
    } else {
      this.canNavigate = true;
      this.router.navigate([this.sharedService.previousUrl], { skipLocationChange: true });
    }
  }

  select(cell: any, event: any): void {
    this.data[cell.rowIndex][cell.column.colId] = event.target.value;
    if (cell.column.colId === "location") {
      this.data[cell.rowIndex]["location"] = event.target.value;
    }
    let rowNode: RowNode = this.gridApi.getRowNode(cell.rowIndex);
    rowNode.setData(rowNode.data);
  }


  validateData(): boolean {
    let valid: boolean = true;
    this.data.forEach(row => {
      this.mandatoryInvoiceMatchingDoclsFields.forEach(field => {
        if (!row[field]) {
          valid = false;
        }
      });
    });
    return valid;
  }

  update(cell: any, event: any): void {
    if (event.target.value) {
      this.data[cell.rowIndex][cell.column.colId] = event.target.value;
      if (cell.column.colId === "orderNO") {
        this.getOrderDetails(cell, event);
      }
      let dtvalue = 0;
      let vat = 0;
      let NonMerchCost = 0;
      if (cell.column.colId === "merchCost") {       
        cell.data.totalCost = Number(event.target.value);
        this.data.forEach(dt => dtvalue = dtvalue + dt.totalCost)
        if (cell.data.nonMerchCost) {
          NonMerchCost = cell.data.nonMerchCost;
        }
        if (cell.data.totalVat) {
          vat = cell.data.totalVat;
        }
        cell.data.totalCostIncVat = cell.data.totalCost + NonMerchCost + vat;
        let oldcalcCount = this.service.serviceDocument.dataProfile.dataModel.calcCost;
        let countcalc = dtvalue + oldcalcCount;
        this.serviceDocument.dataProfile.profileForm.controls["calcCost"].setValue(dtvalue);
      }
      let rowNode: RowNode = this.gridApi.getRowNode(cell.rowIndex);
      rowNode.setData(rowNode.data);
    }
  }

  getOrderDetails(cell: any, event: any): void {
    if (this.data[cell.rowIndex]["orderNO"]) {
      let apiOrderItemUrl: string = "/api/InvoiceMatchingDocHead/OrderDetailsGet?ordernO=" + this.data[cell.rowIndex]["orderNO"];
      this.orderDetailsServiceSubscription = this.service.getDataFromAPI(apiOrderItemUrl).subscribe((response: InvoiceMatchingDocOrderDetailsModel) => {
        if (response != null) {
          this.data[cell.rowIndex]["location"] = response.location;
          this.data[cell.rowIndex]["locName"] = response.locationName;
          this.data[cell.rowIndex]["terms"] = response.terms;
          this.data[cell.rowIndex]["vendorType"] = response.vendorTypeDesc;
          this.data[cell.rowIndex]["vendor"] = response.supllier.toString();
          this.data[cell.rowIndex]["vendorName"] = response.supName;
          this.data[cell.rowIndex]["exchangeRate"] = response.exchangeRate;
          this.data[cell.rowIndex]["currencyCode"] = response.currencyCode;
          this.data[cell.rowIndex]["locType"] = response.locationType;
          this.data[cell.rowIndex]["locTypeDesc"] = response.locactionTypeDesc;

          let rowNode: RowNode = this.gridApi.getRowNode(cell.rowIndex);
          rowNode.setData(rowNode.data);
        } else {
          this.sharedService.errorForm("Order Id Invalid");
        }
        
      }, (error: string) => { console.log(error); });
    }
  }

  change(cell: any, event: any): void {
    if (event.target) {
      if (event.target.checked) {
        this.data[cell.rowIndex]["inAlcInd"] = event.target.checked ? "1" : "0";
        this.gridApi.setRowData(this.data);
      }
    }
    this.temp = this.data[cell.rowIndex];
  }

  delete(cell: any): void {
    this.gridApi.updateRowData({ remove: [cell.data] });
    cell.data.operation = "D";
    this.deletedRows.push(cell.data);
    this.data.splice(cell.rowIndex, 1);
  }

  changeVendorType(event: any): void {
    if (event.options.codeDesc === "Supplier") {
      this.supplierValue = true;
      this.partnerValue = false;
      this.serviceDocument.dataProfile.profileForm.controls["terms"].reset();
      this.serviceDocument.dataProfile.profileForm.controls["vendor"].reset();
      this.serviceDocument.dataProfile.profileForm.controls["vendorName"].reset();
    }
    else {
      this.serviceDocument.dataProfile.profileForm.controls["vendor"].reset();
      this.partnerData = this.partnerDomainData.filter(id => id.partnerType == event.options.code);
      this.supplierValue = false;
      this.partnerValue = true;
      this.serviceDocument.dataProfile.profileForm.controls["terms"].reset();
      this.serviceDocument.dataProfile.profileForm.controls["supplier"].reset();
      this.serviceDocument.dataProfile.profileForm.controls["supNameDesc"].reset();
    }
    this.serviceDocument.dataProfile.profileForm.controls["vendorTypeDesc"].setValue(event.options.codeDesc);
  }

  changeSupplier(event: any): void {
    this.serviceDocument.dataProfile.profileForm.controls["terms"].setValue(event.options.terms);
    this.serviceDocument.dataProfile.profileForm.controls["supNameDesc"].setValue(event.options.supNameDesc);
    this.serviceDocument.dataProfile.profileForm.controls["exchangeRate"].setValue(event.options.exchangeRate);
    this.serviceDocument.dataProfile.profileForm.controls["currencyValue"].setValue(event.options.currencyCode);
  }

  getPartnerDetails(event: any): void {
    this.serviceDocument.dataProfile.profileForm.controls["terms"].setValue(event.options.terms);
    this.serviceDocument.dataProfile.profileForm.controls["vendorName"].setValue(event.options.partnerDesc);
    this.serviceDocument.dataProfile.profileForm.controls["exchangeRate"].setValue(event.options.exchangeRate);
    this.serviceDocument.dataProfile.profileForm.controls["currencyValue"].setValue(event.options.currency);
  }

  changControlCountInfo(event: any): void {
    if (event != "") {
      let vale = this.serviceDocument.dataProfile.profileForm.controls["calcCount"].value;
      this.result = event - vale;
      this.serviceDocument.dataProfile.profileForm.controls["varianceCount"].setValue(this.result);
    }
  }

  changControlTotalInfo(event: any): void {
    if (event != "") {
      let vale = this.serviceDocument.dataProfile.profileForm.controls["calcCost"].value;
      this.result = event - vale;
      this.serviceDocument.dataProfile.profileForm.controls["varianceCost"].setValue(this.result);
    }
  }
  public onDocumentsClick(cell: any): void {
    if (cell.data.docId > 0) {
      if (cell.column.colId == "nonMerchCost") {
        this.fetchNonMerchandiseCostList(cell);
      }
      if (cell.column.colId == "totalVat") {
        this.fetchVatBreakdownList(cell);
      }
    } else {
      this.sharedService.errorForm("Please add Total Cost Edit mode only");
    }
  }

  public fetchNonMerchandiseCostList(cell: any): void {

    this.nonMerchandiseCostPopupService.nonMerchandiseCostDomainData(cell.data.docId)
      .subscribe((response: NonMerchandiseCostDomainModel) => {
        if (response != null) {
          let data: any = {
            "DocId": cell.data.docId,
            "NonMerchandiseCost": response
          };
          this.nonMerchandiseCostPopupSubscription = this.sharedService.openDilogForFindImDoc(NonMerchandiseCostPopupComponent, data, "INVOICE")
            .subscribe((message: string) => {
              if (message) {
                cell.data.nonMerchCost = message;
                let value = 0
                if (cell.data.totalCost) {
                  value = cell.data.totalCost;
                }
                cell.data.totalCost = Number(value) + message;
                let oldcalcCount = this.serviceDocument.dataProfile.profileForm.controls["calcCost"].value;
                let countcalc = message + oldcalcCount;
                this.serviceDocument.dataProfile.profileForm.controls["calcCost"].setValue(countcalc);
                this.gridApi.setRowData(this.data);
              }
            });
          this.gridApi.setRowData(this.data);
          this.loaderService.display(false);
        }
      },
        (err: any) => { this.sharedService.errorForm(err); });
  }

  public fetchVatBreakdownList(cell: any): void {   
    this.findVatRateApiUrl = "/api/InvoiceMatchingVat/InvoiceMatchingVatDomainData?docId=" + cell.data.docId + "&docDate=" + new Date(cell.data.docDate).toLocaleDateString();
    this.vatBreakdownVatPopupSubscription = this.vatBreakdownPopupService.getItems(this.findVatRateApiUrl)
        .subscribe((response: VatBreakdownDomainModel[]) => {
          if (response != null) {
            let data: any = {
              "DocId": cell.data.docId,
              "InvoiceMatchingVat": response
            };
            this.vatBreakdownPopupSubscription = this.sharedService.openDilogForFindImDoc(VatBreakdownPopupComponent, data, "VAT")
              .subscribe((message: string) => {
                if (message) {
                  cell.data.totalVat = message;
                  let costVat = 0;
                  if (cell.data.totalCostIncVat) {
                    costVat = cell.data.totalCostIncVat;
                  }
                  cell.data.totalCostIncVat = Number(costVat) + message + cell.data.totalCost;
                  this.gridApi.setRowData(this.data);
                }
              });
            this.gridApi.setRowData(this.data);
            this.loaderService.display(false);
          }
        },
          (err: any) => { this.sharedService.errorForm(err); });
    }
  }

