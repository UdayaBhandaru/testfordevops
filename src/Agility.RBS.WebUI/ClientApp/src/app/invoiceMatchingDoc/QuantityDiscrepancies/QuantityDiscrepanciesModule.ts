import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from '../../AgGridSharedModule';
import { RbsSharedModule } from '../../RbsSharedModule';
import { TitleSharedModule } from '../../TitleSharedModule';
import { QuantityDiscrepanciesRoutingModule } from './QuantityDiscrepanciesRoutingModule';
import { QuantityDiscrepanciesListComponent } from './QuantityDiscrepanciesListComponent';
import { QuantityDiscrepanciesComponent } from './QuantityDiscrepanciesComponent';
import { QuantityDiscrepanciesService } from './QuantityDiscrepanciesService';
import { QuantityDiscrepanciesListResolver, QuantityDiscrepanciesResolver } from './QuantityDiscrepanciesResolver';


@NgModule({
    imports: [
        FrameworkCoreModule,
        QuantityDiscrepanciesRoutingModule,
        AgGridModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        QuantityDiscrepanciesListComponent,
        QuantityDiscrepanciesComponent
    ],
    providers: [
        QuantityDiscrepanciesService,
        QuantityDiscrepanciesListResolver,
        QuantityDiscrepanciesResolver
    ]
})
export class QuantityDiscrepanciesModule {
}
