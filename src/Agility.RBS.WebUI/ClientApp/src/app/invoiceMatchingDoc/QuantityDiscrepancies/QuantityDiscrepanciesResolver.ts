import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { QuantityDiscrepanciesService } from './QuantityDiscrepanciesService';
import { QuantityDiscrepanciesSearchModel } from './QuantityDiscrepanciesSearchModel';

@Injectable()
export class QuantityDiscrepanciesListResolver implements Resolve<ServiceDocument<QuantityDiscrepanciesSearchModel>> {
  constructor(private service: QuantityDiscrepanciesService) { }
  resolve(): Observable<ServiceDocument<QuantityDiscrepanciesSearchModel>> {
    return this.service.list();
  }

}

@Injectable()
export class QuantityDiscrepanciesResolver implements Resolve<ServiceDocument<QuantityDiscrepanciesSearchModel>> {
  constructor(private service: QuantityDiscrepanciesService) { }
  resolve(): Observable<ServiceDocument<QuantityDiscrepanciesSearchModel>> {
    return this.service.addEdit();
  }

}
