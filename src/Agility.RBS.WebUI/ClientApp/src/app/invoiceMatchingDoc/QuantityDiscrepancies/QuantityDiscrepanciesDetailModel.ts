export class QuantityDiscrepanciesDetailModel {  
  costDiscrepancyId?: number;
  docId?: number;
  rderNo?: number;
  item?: string; 
  itemDesc?: string;
  qtyInvoiced?: number; 
  qtyOrdered?: number;  
  qtyRecived?: number;  
  qtyVariance?: number;  
  qtyVatPerc?: number;
  outstandingVar?: number;
  resQtyTyp?: string;
  resQtyTypDesc?: string;
  resQty?: number;
  reasonCode?: string;
  reasonDesc?: string;
  actionCode?: string;
}
