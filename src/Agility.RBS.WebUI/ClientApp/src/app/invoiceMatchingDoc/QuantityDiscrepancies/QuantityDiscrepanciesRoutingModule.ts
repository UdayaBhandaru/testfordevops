import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { QuantityDiscrepanciesListComponent } from './QuantityDiscrepanciesListComponent';
import { QuantityDiscrepanciesListResolver, QuantityDiscrepanciesResolver } from './QuantityDiscrepanciesResolver';
import { QuantityDiscrepanciesComponent } from './QuantityDiscrepanciesComponent';

const ShipmentRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: QuantityDiscrepanciesListComponent,
                resolve:
                {
                  references: QuantityDiscrepanciesListResolver
                }
            },
            {
                path: "New/:id",
                component: QuantityDiscrepanciesComponent,
                resolve:
                {
                  references: QuantityDiscrepanciesResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(ShipmentRoutes)
    ]
})
export class QuantityDiscrepanciesRoutingModule { }
