import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from '../../Common/HttpHelperService';
import { SharedService } from '../../Common/SharedService';
import { QuantityDiscrepanciesSearchModel } from './QuantityDiscrepanciesSearchModel';

@Injectable()
export class QuantityDiscrepanciesService {
  serviceDocument: ServiceDocument<QuantityDiscrepanciesSearchModel> = new ServiceDocument<QuantityDiscrepanciesSearchModel>();
  searchData: any;
  orderPaging: {
    startRow: number,
    pageSize: number,
    cacheSize: number,
  } = {
      startRow: 0,
      pageSize: 10,
      cacheSize: 100,
    };
  constructor(private httpHelperService: HttpHelperService, private sharedService: SharedService) { }

  newModel(model: QuantityDiscrepanciesSearchModel): ServiceDocument<QuantityDiscrepanciesSearchModel> {
    return this.serviceDocument.newModel(model);
  }

  list(): Observable<ServiceDocument<QuantityDiscrepanciesSearchModel>> {
    return this.serviceDocument.list("/api/ImQtyDiscrepancy/List");
  }

  search(additionalParams: any): Observable<ServiceDocument<QuantityDiscrepanciesSearchModel>> {
    return this.serviceDocument.search("/api/ImQtyDiscrepancy/Search", true, () => this.setSubClassId(additionalParams));
  }

  addEdit(): Observable<ServiceDocument<QuantityDiscrepanciesSearchModel>> {
    return this.serviceDocument.list("/api/ImQtyDiscrepancy/New");
  }

  save(): Observable<ServiceDocument<QuantityDiscrepanciesSearchModel>> {
    return this.serviceDocument.save("/api/ImQtyDiscrepancy/Save", true);
  }

  setSubClassId(additionalParams: any): void {

    if (!this.serviceDocument.dataProfile) {
      this.serviceDocument = this.newModel({
        docId: null, location: null, locName: null, locType: null, orderNo: null, supplier: null, suppName: null, routingDate: null,
        resolveByDate: null, apReviewer: null, docType: null, docTypeDesc: null, PastDueInd: null, frightPaymentType: null, invoiceDate: null, noLineDisc: null
      });

    }

    var sd: QuantityDiscrepanciesSearchModel = this.serviceDocument.dataProfile.dataModel;
    if (additionalParams) {
      sd.startRow = additionalParams.startRow ? additionalParams.startRow : this.orderPaging.startRow;
      sd.endRow = additionalParams.endRow ? additionalParams.endRow : this.orderPaging.cacheSize;
      sd.sortModel = additionalParams.sortModel;
    } else {
      sd.startRow = this.orderPaging.startRow;
      sd.endRow = this.orderPaging.cacheSize;
    }
  }

}
