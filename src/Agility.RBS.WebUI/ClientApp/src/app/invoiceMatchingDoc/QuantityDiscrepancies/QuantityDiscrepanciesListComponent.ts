import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { GridOptions, IServerSideGetRowsParams, GridApi, GridReadyEvent, IServerSideDatasource } from 'ag-grid-community';
import { Subscription } from 'rxjs';
import { SharedService } from '../../Common/SharedService';
import { InfoComponent } from '../../Common/InfoComponent';
import { GridEditComponent } from '../../Common/grid/GridEditComponent';
import { GridDateComponent } from '../../Common/grid/GridDateComponent';
import { QuantityDiscrepanciesSearchModel } from './QuantityDiscrepanciesSearchModel';
import { QuantityDiscrepanciesService } from './QuantityDiscrepanciesService';
import { IndicatorComponent } from '../../Common/grid/IndicatorComponent';

@Component({
  selector: "QuantityDiscrepancies-List",
  templateUrl: "./QuantityDiscrepanciesListComponent.html",
  styleUrls: ['./QuantityDiscrepanciesListComponent.scss']
})
export class QuantityDiscrepanciesListComponent implements OnInit, IServerSideDatasource{
  public serviceDocument: ServiceDocument<QuantityDiscrepanciesSearchModel>;
  PageTitle = "Quantity Discrepancies List";
  redirectUrl = "imQtyDiscrepancy/New/";
  componentName: any;
  columns: any[];
  additionalSearchParams: any;
  searchServiceSubscription: Subscription;
  showSearchCriteria: boolean = true;  
  model: QuantityDiscrepanciesSearchModel = {
    docId: null, location: null, locName: null, locType: null, orderNo: null, supplier: null, suppName: null,  routingDate: null,
    resolveByDate: null, apReviewer: null, docType: null, docTypeDesc: null, PastDueInd: null, frightPaymentType: null, invoiceDate: null, noLineDisc: null};
  public gridOptions: GridOptions;
  gridApi: GridApi;

  constructor(public service: QuantityDiscrepanciesService, private sharedService: SharedService, private router: Router, private changeRef: ChangeDetectorRef) {
  }

  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }

  ngOnInit(): void {
    this.componentName = this;
    this.gridOptions = {
      onGridReady: (params: GridReadyEvent) => {
        params.api.setServerSideDatasource(this);
      },
      context: {
        componentParent: this
      },
      suppressRowClickSelection: true,
      suppressCellSelection: true,
      rowModelType: "serverSide",
      paginationPageSize: this.service.orderPaging.pageSize,
      pagination: true,
      enableServerSideSorting: true,
      enableServerSideFilter: true,
      cacheBlockSize: this.service.orderPaging.cacheSize,
    };
    this.columns = [
      { headerName: "Past Due Indicator", field: "PastDueInd", tooltipField: "PastDueInd", cellRendererFramework: IndicatorComponent },
      { headerName: "Resolve By Date", field: "resolveByDate", tooltipField: "resolveByDate",  cellRendererFramework: GridDateComponent  },
      { headerName: "Supplier", field: "supplier", tooltipField: "supplier" ,sort: "desc"},
      { headerName: "Supplier Name", field: "suppName", tooltipField: "suppName" },
      { headerName: "Documnet Type", field: "docTypeDesc", tooltipField: "docTypeDesc" },
      { headerName: "Location", field: "location", tooltipField: "location" },
      { headerName: "Order", field: "orderNo", tooltipField: "orderNo" },
      { headerName: "Fright Payment type", field: "frightPaymentType", tooltipField: "frightPaymentType" },
      { headerName: "Documnet", field: "docType", tooltipField: "docType" },
      { headerName: "Quantity Difference", field: "noLineDisc", tooltipField: "noLineDisc" },
      { headerName: "Invoice Date", field: "invoiceDate", tooltipField: "invoiceDate", cellRendererFramework: GridDateComponent },
      { headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent }
    ];
      this.service.newModel(this.model);
      this.serviceDocument = this.service.serviceDocument;
  }

  getRows(params: IServerSideGetRowsParams): void {
    this.additionalSearchParams = { startRow: params.request.startRow, endRow: params.request.endRow, sortModel: params.request.sortModel };
      this.searchServiceSubscription = this.service.search(this.additionalSearchParams).subscribe((sDoc) => {
        params.successCallback(sDoc.dataProfile.dataList, this.service.serviceDocument.dataProfile.dataModel.totalRows);
      });
  } 

  addValidation(): void {
    this.router.navigate(["/" + this.redirectUrl + "0"], { skipLocationChange: true });
    this.sharedService.previousUrl = "../imQtyDiscrepancy/List";
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl + cell.data.docId, this.service.serviceDocument.dataProfile.dataList);
  }
}
