import { Component, OnInit } from "@angular/core";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { Router } from "@angular/router";
import { GridOptions, GridApi, ColumnApi, RowNode } from 'ag-grid-community';
import { SharedService } from '../../Common/SharedService';
import { QuantityDiscrepanciesDetailModel } from './QuantityDiscrepanciesDetailModel';
import { QuantityDiscrepanciesService } from './QuantityDiscrepanciesService';
import { QuantityDiscrepanciesSearchModel } from './QuantityDiscrepanciesSearchModel';
import { ReasonCodeModel } from '../../administration/ReasonCode/ReasonCodeModel';
import { GridSelectComponent } from '../../Common/grid/GridSelectComponent';
import { FormControl } from '@angular/forms';

@Component({
  selector: "QuantityDiscrepancies",
  templateUrl: "./QuantityDiscrepanciesComponent.html",
  styleUrls: ['./QuantityDiscrepanciesComponent.scss']
})
export class QuantityDiscrepanciesComponent implements OnInit {
  PageTitle = "Quantity Discrepancies View";
  serviceDocument: ServiceDocument<QuantityDiscrepanciesSearchModel>;
  dataList: QuantityDiscrepanciesDetailModel[];
  columns: any[];
  editMode: boolean = false;
  editModel: any;
  componentParentName: QuantityDiscrepanciesComponent;
  reasonCodeData: ReasonCodeModel[];
  gridOptions: GridOptions;
  gridApi: GridApi;
  gridColumnApi: ColumnApi;
  model: QuantityDiscrepanciesSearchModel = {
    docId: null, location: null, locName: null, locType: null, orderNo: null, supplier: null, suppName: null, routingDate: null,
    resolveByDate: null, apReviewer: null, docType: null, docTypeDesc: null, PastDueInd: null, frightPaymentType: null, invoiceDate: null, noLineDisc: null
  };
  constructor(private service: QuantityDiscrepanciesService,
    private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.reasonCodeData = Object.assign([], this.service.serviceDocument.domainData["reasonCode"]);
    this.serviceDocument = this.service.serviceDocument;
    this.columns = [
      {
        headerName: "Reason Code", field: "reasonCode", cellRendererFramework: GridSelectComponent, cellClassRules: {
          "invalid-grid-row": function (params: any): any { return !params.value; }
        }, cellRendererParams: { references: this.reasonCodeData, keyvaluepair: { key: "reasonCodeId", value: "reasonCodeDescription" } },
        tooltipField: "code", headerTooltip: "ReasonCode Description"
      },
      { headerName: "Action", field: "actionCode", tooltipField: "action", headerTooltip: "Action" },
      { headerName: "Discrepancy Comments", field: "item", tooltipField: "item" },
      { headerName: "ITEM", field: "item", tooltipField: "item" },
      { headerName: "DESCRIPTION", field: "itemDesc", tooltipField: "itemDesc" },
      { headerName: "Qty Invoiced", field: "qtyInvoiced", tooltipField: "Qty Invoiced" },
      { headerName: "Qty Ordered", field: "qtyOrdered", tooltipField: "Qty Ordered" },
      { headerName: "Qty Recived", field: "qtyRecived", tooltipField: "Qty Recived" },
      { headerName: "Qty Variance", field: "qtyVariance", tooltipField: "Qty Variance" },
      { headerName: "Qty VatPerc", field: "qtyVatPerc", tooltipField: "Qty VatPerc" },
      { headerName: "Outstanding Var", field: "outstandingVar", tooltipField: "Outstanding Var" },
    ];
    this.gridOptions = {
      onGridReady: (params: any) => {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
      }
    };

    this.componentParentName = this;
    this.bind();
  }
  bind(): void {
    delete this.editModel;
    if (this.sharedService.editObject) {
      this.dataList = this.sharedService.editObject.imQtyDiscrepancyDetails;
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
      this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
      delete this.sharedService.editObject;
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  select(cell: any, event: any): void {
    this.dataList[cell.rowIndex][cell.column.colId] = event.target.value;
    if (cell.column.colId === "reasonCode") {
      this.dataList[cell.rowIndex]["reasonCode"] = event.target.value;
      var reasoncodeType = this.reasonCodeData.filter(id => id.reasonCodeId == event.target.value);
      this.dataList[cell.rowIndex]["actionCode"] = reasoncodeType[0].action;
    }
    let rowNode: RowNode = this.gridApi.getRowNode(cell.rowIndex);
    rowNode.setData(rowNode.data);
  }

  save(saveOnly: boolean): void {
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.saveSubscription(saveOnly);
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  saveAndContinue(): void {
    this.save(false);
  }

  saveSubscription(saveOnly: boolean): void {
    this.saveCostDiscrepancies(saveOnly);
  }
  saveCostDiscrepancies(saveOnly: boolean): void {
    this.serviceDocument.dataProfile.profileForm.controls["imQtyDiscrepancyDetails"] = new FormControl();
    this.serviceDocument.dataProfile.profileForm.controls["imQtyDiscrepancyDetails"].setValue(this.dataList);
    this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.sharedService.saveForm("Quantity Discrepancies Saved Successfully").subscribe(() => {

        }, (error: string) => { console.log(error); });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
    }, (error: string) => { console.log(error); });
  }

  reset(): void {

  }

}
