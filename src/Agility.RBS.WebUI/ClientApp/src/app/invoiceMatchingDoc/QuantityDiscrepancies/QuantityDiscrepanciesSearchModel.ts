import { QuantityDiscrepanciesDetailModel } from './QuantityDiscrepanciesDetailModel';

export class QuantityDiscrepanciesSearchModel {
  docId?: number;
  location?: number;
  locName?: string;
  locType?: string;
  orderNo?: number;
  supplier?: number;
  suppName?: string;
  routingDate?: Date;
  resolveByDate?: Date;
  apReviewer?: string;
  docType?: string;
  docTypeDesc?: string;
  PastDueInd?: string;
  frightPaymentType?: string;
  invoiceDate?: Date;
  noLineDisc?: number;
  startRow?: number;
  endRow?: number;
  totalRows?: number;
  sortModel?: {}[];
  quantityDiscrepanciesDetail?: QuantityDiscrepanciesDetailModel[];
}
