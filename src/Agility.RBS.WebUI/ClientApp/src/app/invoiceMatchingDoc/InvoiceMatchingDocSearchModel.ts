export class InvoiceMatchingDocSearchModel {
  groupId: number;
  createDate?: Date;
  createId: string;
  status: string;
  statusDesc: string;
  controlRecCount: number;
  calcCount: number;
  varrianceCount: number;
  controlTotalCost: number;
  calcCost: number;
  varrianceCost: number;
  currencyCode: string;
  lastUpdateId: string;
  lastUpdateDateTime?: Date;
  operation: string;
  tableName: string;
  startRow?: number;
  endRow?: number;
  totalRows?: number;
  sortModel?: {}[];  
}
