import { Component, OnInit, ViewChild, OnDestroy, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { Subscription } from "rxjs";
import { GridOptions, GridReadyEvent, IServerSideDatasource, IServerSideGetRowsParams, ColDef } from "ag-grid-community";
import { ImDocMaintenanceSearchModel } from './ImDocMaintenanceSearchModel';
import { GridExportModel } from '../../Common/grid/GridExportModel';
import { ImDocMaintenanceListService } from './ImDocMaintenanceListService';
import { SharedService } from '../../Common/SharedService';
import { GridDateComponent } from '../../Common/grid/GridDateComponent';
import { GridEditComponent } from '../../Common/grid/GridEditComponent';
import { InfoComponent } from '../../Common/InfoComponent';
import { DomainDetailModel } from '../../domain/DomainDetailModel';

@Component({
  selector: "ImDocMaintenance-list",
  templateUrl: "./ImDocMaintenanceListComponent.html"
})

export class ImDocMaintenanceListComponent implements OnInit, IServerSideDatasource, OnDestroy {
  public sheetName: string = "ImDocMaintenance List";
  PageTitle = "Invoice MatchingDoc Maintenance List";
  redirectUrl = "imDocMaintenance/New/";
  componentName: any;
  additionalSearchParams: any;
  loadedPage: boolean;
  searchServiceSubscription: Subscription;
  partenerDetailsServiceSubscription: Subscription;
  documentTypeData: DomainDetailModel[];
  vendorTypeData: DomainDetailModel[];

  //statusData: DomainDetailModel[];
  apiUrl: string;
  pForm: any;

  serviceDocument: ServiceDocument<ImDocMaintenanceSearchModel>;
  gridOptions: GridOptions;
  columns: any[];
  model: ImDocMaintenanceSearchModel = {
    docId: null, type: null, typeDesc: null, status: null, statusDesc: null, orderNo: null, location: null, locName: null, locType: null, groupId: null,
    docDate: null, vendorType: null, vendorTypeDesc: null, vendor: null, vendorDesc: null, extDocId: null, dueDate: null, refDoc: null, tableName: null
  };

  showSearchCriteria: boolean = true;  
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;

  constructor(public service: ImDocMaintenanceListService, private sharedService: SharedService, private router: Router
    , private changeRef: ChangeDetectorRef) {
  }

  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }

  ngOnInit(): void {
    this.documentTypeData = Object.assign([], this.service.serviceDocument.domainData["documentType"]);
    this.vendorTypeData = Object.assign([], this.service.serviceDocument.domainData["vendorType"]);
    this.componentName = this;
    this.columns = [
      { colId: "Doc_Type", headerName: "Doc Type", field: "typeDesc", tooltipField: "typeDesc" },
      { colId: "Doc_Id", headerName: "Doc Id", field: "docId", tooltipField: "docId", sort: "desc"},
      { colId: "STATUS", headerName: "Status", field: "statusDesc", tooltipField: "statusDesc" },
      { colId: "Doc_Date", headerName: "Doc Date", field: "docDate", tooltipField: "docDate", cellRendererFramework: GridDateComponent },
      { colId: "Order_No", headerName: "Order No", field: "orderNo", tooltipField: "orderNo"  },
      { colId: "Due_Date", headerName: "Due Date", field: "dueDate", tooltipField: "dueDate", cellRendererFramework: GridDateComponent },
      { colId: "Vendor", headerName: "Vendor", field: "vendor", tooltipField: "vendor"  },
      { colId: "Vendor_Desc", headerName: "Vendor Desc", field: "vendorDesc", tooltipField: "vendorDesc"  },
      { colId: "Location", headerName: "Location", field: "location", tooltipField: "location" },
      { colId: "Location_Desc", headerName: "Location Desc", field: "locName", tooltipField: "locName" },
      { colId: "Freight_Type", headerName: "Freight Type", field: "extDocId", tooltipField: "extDocId" },
      { colId: "Document_Amount", headerName: "Document Amount", field: "groupId", tooltipField: "groupId" },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 150
      }
    ];

    this.gridOptions = {
      onGridReady: (params: GridReadyEvent) => {
        params.api.setEnterpriseDatasource(this);
      },
      context: {
        componentParent: this
      },
      suppressRowClickSelection: true,
      suppressCellSelection: true,
      rowModelType: "enterprise",
      paginationPageSize: this.service.orderPaging.pageSize,
      enableServerSideSorting: true,
      enableServerSideFilter: true,
      cacheBlockSize: this.service.orderPaging.cacheSize,
    };    

    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }

    this.serviceDocument = this.service.serviceDocument;
    
  }

  getRows(params: IServerSideGetRowsParams): void {
    this.additionalSearchParams = { startRow: params.request.startRow, endRow: params.request.endRow, sortModel: params.request.sortModel };
    if (!this.loadedPage) {
      this.loadedPage = true;
      params.successCallback(
        this.serviceDocument.dataProfile.dataList.slice(params.request.startRow, params.request.endRow),
        this.service.serviceDocument.dataProfile.dataModel.totalRows);
    } else {
      this.searchServiceSubscription = this.service.search(this.additionalSearchParams).subscribe((sDoc) => {
        params.successCallback(sDoc.dataProfile.dataList, this.service.serviceDocument.dataProfile.dataModel.totalRows);
      });
    }
  }

  showSearchCriteriaChild(event: any): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
      this.sharedService.previousUrl = "../ImDocMaintenance/List";
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl + cell.data.groupId
      , this.service.serviceDocument.dataProfile.dataList);
  }
  addValidation(): void {
    this.router.navigate(["/" + this.redirectUrl + "0"], { skipLocationChange: true });
    this.sharedService.previousUrl = "../ImDocMaintenance/List";
  }
  
  ngOnDestroy(): void {
    if (this.searchServiceSubscription) {
      this.searchServiceSubscription.unsubscribe();
    }
  }  
}
