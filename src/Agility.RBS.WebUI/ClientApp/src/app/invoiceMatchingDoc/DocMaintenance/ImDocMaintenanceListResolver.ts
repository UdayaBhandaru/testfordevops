import { Injectable } from "@angular/core";
import { Resolve } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { ImDocMaintenanceSearchModel } from './ImDocMaintenanceSearchModel';
import { ImDocMaintenanceListService } from './ImDocMaintenanceListService';

@Injectable()
export class ImDocMaintenanceListResolver implements Resolve<ServiceDocument<ImDocMaintenanceSearchModel>> {
  constructor(private service: ImDocMaintenanceListService) { }
  resolve(): Observable<ServiceDocument<ImDocMaintenanceSearchModel>> {
    return this.service.list();
  }
}
