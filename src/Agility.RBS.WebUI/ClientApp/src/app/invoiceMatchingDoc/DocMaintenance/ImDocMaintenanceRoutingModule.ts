import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ImDocMaintenanceListComponent } from './ImDocMaintenanceListComponent';
import { ImDocMaintenanceListResolver } from './ImDocMaintenanceListResolver';
import { ImDocMaintenanceComponent } from './ImDocMaintenanceComponent';
import { ImDocMaintenanceResolver } from './ImDocMaintenanceResolver';

const OrderRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "", redirectTo: "List", pathMatch: "full"
      },
      {
        path: "List",
        component: ImDocMaintenanceListComponent,
        resolve:
        {
          references: ImDocMaintenanceListResolver
        }
      }
      ,
      {
        path: "New/:id",
        component: ImDocMaintenanceComponent,
        resolve:
        {
          references: ImDocMaintenanceResolver
        }
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(OrderRoutes)
  ]
})
export class ImDocMaintenanceRoutingModule { }
