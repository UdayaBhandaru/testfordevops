import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { ImDocMaintenanceSearchModel } from './ImDocMaintenanceSearchModel';
import { HttpHelperService } from '../../Common/HttpHelperService';
import { SharedService } from '../../Common/SharedService';

@Injectable()
export class ImDocMaintenanceListService {    
    serviceDocument: ServiceDocument<ImDocMaintenanceSearchModel> = new ServiceDocument<ImDocMaintenanceSearchModel>();
    orderPaging: {
        startRow: number,
        pageSize: number,
        cacheSize: number,
    } = {
        startRow: 0,
        pageSize: 10,
        cacheSize: 100,
    };

  constructor(private httpHelperService: HttpHelperService,private sharedService: SharedService) { }

    newModel(model: ImDocMaintenanceSearchModel): ServiceDocument<ImDocMaintenanceSearchModel> {
        return this.serviceDocument.newModel(model);
    }

    search(additionalParams: any): Observable<ServiceDocument<ImDocMaintenanceSearchModel>> {
      return this.serviceDocument.search("/api/ImDocMaintenanceSearch/Search", true, () => this.setSubClassId(additionalParams));
    }

    list(): Observable<ServiceDocument<ImDocMaintenanceSearchModel>> {
      return this.serviceDocument.list("/api/ImDocMaintenanceSearch/List");
    }

    setSubClassId(additionalParams: any): void {
        var sd: ImDocMaintenanceSearchModel = this.serviceDocument.dataProfile.dataModel;
        if (additionalParams) {
            sd.startRow = additionalParams.startRow ? additionalParams.startRow : this.orderPaging.startRow;
            sd.endRow = additionalParams.endRow ? additionalParams.endRow : this.orderPaging.cacheSize;
            sd.sortModel = additionalParams.sortModel;
        } else {
            sd.startRow = this.orderPaging.startRow;
            sd.endRow = this.orderPaging.cacheSize;
      }
      if (sd !== null) {
        if (sd.docDate != null) {
          sd.docDate = this.sharedService.setOffSet(sd.docDate);
        }
        if (sd.dueDate != null) {
          sd.dueDate = this.sharedService.setOffSet(sd.dueDate);
        }
      }
  }
  getDataFromAPI(apiUrl: string): Observable<any> {
    let resp: Observable<any> = this.httpHelperService.get(apiUrl);
    return resp;
  }
}
