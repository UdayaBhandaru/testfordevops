import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { ImDocMaintenanceListService } from './ImDocMaintenanceListService';
import { AgGridSharedModule } from '../../AgGridSharedModule';
import { TitleSharedModule } from '../../TitleSharedModule';
import { DatePickerModule } from '../../Common/DatePickerModule';
import { RbsSharedModule } from '../../RbsSharedModule';
import { ImDocMaintenanceListComponent } from './ImDocMaintenanceListComponent';
import { ImDocMaintenanceListResolver } from './ImDocMaintenanceListResolver';
import { ImDocMaintenanceRoutingModule } from './ImDocMaintenanceRoutingModule';
import { ImDocMaintenanceComponent } from './ImDocMaintenanceComponent';
import { ImDocMaintenanceService } from './ImDocMaintenanceService';
import { ImDocMaintenanceResolver } from './ImDocMaintenanceResolver';


@NgModule({
    imports: [
        FrameworkCoreModule,
        ImDocMaintenanceRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        DatePickerModule,
        RbsSharedModule
    ],
    declarations: [
      ImDocMaintenanceListComponent,
      ImDocMaintenanceComponent
    ],
    providers: [
      ImDocMaintenanceListService,
      ImDocMaintenanceListResolver,
      ImDocMaintenanceService,
      ImDocMaintenanceResolver

    ]
})
export class ImDocMaintenanceModule {
    constructor() {
        LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
    }
}
