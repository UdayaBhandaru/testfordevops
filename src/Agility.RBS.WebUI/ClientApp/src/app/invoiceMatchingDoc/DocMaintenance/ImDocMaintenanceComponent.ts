import { Component, OnInit, TemplateRef, ViewChild, OnDestroy, ChangeDetectorRef } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ServiceDocument, CommonService, MessageResult, MessageType } from "@agility/frameworkcore";
import { MatDialogRef } from "@angular/material";
import { GridOptions, GridApi, RowNode, ColumnApi } from "ag-grid-community";
import { Subscription } from "rxjs";
import { FormControl, FormGroup } from '@angular/forms';
import { ImDocMaintenanceHeadModel } from './ImDocMaintenanceHeadModel';
import { ImDocMaintenanceService } from './ImDocMaintenanceService';
import { SharedService } from '../../Common/SharedService';
import { RbDialogService } from '../../Common/controls/RbDialogService';
import { LoaderService } from '../../Common/LoaderService';
import { DomainDetailModel } from '../../domain/DomainDetailModel';

@Component({
  selector: "Invoice MatchingDoc-add",
  templateUrl: "./ImDocMaintenanceComponent.html",
  styleUrls: ['./ImDocMaintenanceComponent.scss']

})
export class ImDocMaintenanceComponent implements OnInit {
  screenName: any;
  _componentName = "InvoiceMatchingDoc";
  PageTitle: string = "Invoice Matching Doc Head";
  public serviceDocument: ServiceDocument<ImDocMaintenanceHeadModel>;
  localizationData: any;
  componentParentName: ImDocMaintenanceComponent;
  editMode: boolean;
  apiUrl: string; 

  dialogRef: MatDialogRef<any>;
  private messageResult: MessageResult = new MessageResult();
  canNavigate: boolean;
  routeServiceSubscription: Subscription;
  supplierDetailsServiceSubscription: Subscription;
  saveServiceSubscription: Subscription;
  editModel: ImDocMaintenanceHeadModel;
  model: ImDocMaintenanceHeadModel;
  gridOptions: GridOptions;
  gridApi: GridApi;
  gridColumnApi: ColumnApi;
  componentName: any;
  columns: any[];
  orginalValue: string = "";
  addText: string = "+ Add Item";    
  temp: {};
  pForm: any;
  result: number;  
  deletedRows: any[] = [];
  documentTypeData: DomainDetailModel[];
  vendorTypeData: DomainDetailModel[];
  domainRadioData: DomainDetailModel[];
  today: Date;
  constructor(private route: ActivatedRoute, private router: Router, public sharedService: SharedService, private service: ImDocMaintenanceService
    , private loaderService: LoaderService, private changeRef: ChangeDetectorRef, private dialogService: RbDialogService) {
  }

  ngOnInit(): void {
    this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
    this.documentTypeData = Object.assign([], this.service.serviceDocument.domainData["documentType"]);
    this.vendorTypeData = Object.assign([], this.service.serviceDocument.domainData["vendorType"]);
    this.domainRadioData = Object.assign([], this.service.serviceDocument.domainData["domainRadioData"]);
    this.bind();
  }

  bind(): void {
    delete this.editModel;
    if (this.sharedService.editObject) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
      this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
      delete this.sharedService.editObject;
      this.editMode = true;
    } else {
      this.model = {
        docId: null, type: null, typeDesc: null, status: null, statusDesc: null, orderNO: null, location: null, locName: null, locType: null, locTypeDesc: null,
        totalDiscount: null, groupId: null, parentId: null, docDate: null, createDate: null, createId: null, vendorType: null, vendorTypeDesc: null, vendor: null,
        vendorName: null, extDocId: null, ediUploadInd: null, terms: null, termsDscntPct: null, dueDate: null, paymentMethod: null, paymentMethodDesc: null, matchId: null,
        matchDate: null, approvalId: null, approvalDate: null, prePaidInd: null, prePaidId: null, postDate: null, currencyCode: null, exchangeRate: null, totalCost: null,
        totalQty: null, manuallyPaidInd: null, customDocRef1: null, customDocRef2: null, customDocRef3: null, customDocRef4: null, lastUpdateId: null, lastDateTime: null,
        freightType: null, freightTypeDesc: null, refDoc: null, refAuthNo: null, costFreMatch: null, detailMatched: null, bestTerms: null, bestTermsSource: null,
        bestTermsDate: null, bestTermsDateSource: null, varianceWithinTolerance: null, resolutionAdjustedTotalCost: null, resolutionAdjustedTotalQty: null,
        consignmentInd: null, dealId: null, rtvInd: null, discountDate: null, dealType: null, totalCostIncVat: null, vatDiscCreateDate: null, operation: "I"
      };
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  save(saveOnly: boolean): void {
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.saveSubscription(saveOnly);
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  saveAndContinue(): void {
    this.save(false);
  }

  saveSubscription(saveOnly: boolean): void {
    this.checkUom(saveOnly);
  }
  saveUom(saveOnly: boolean): void {
    this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.sharedService.saveForm(this.localizationData.uom.uomsave).subscribe(() => {
          if (saveOnly) {
            this.router.navigate(["/UOM/List"], { skipLocationChange: true });
          } else {
            if (!this.editMode) {
              this.model = {
                docId: null, type: null, typeDesc: null, status: null, statusDesc: null, orderNO: null, location: null, locName: null, locType: null, locTypeDesc: null,
                totalDiscount: null, groupId: null, parentId: null, docDate: null, createDate: null, createId: null, vendorType: null, vendorTypeDesc: null, vendor: null,
                vendorName: null, extDocId: null, ediUploadInd: null, terms: null, termsDscntPct: null, dueDate: null, paymentMethod: null, paymentMethodDesc: null, matchId: null,
                matchDate: null, approvalId: null, approvalDate: null, prePaidInd: null, prePaidId: null, postDate: null, currencyCode: null, exchangeRate: null, totalCost: null,
                totalQty: null, manuallyPaidInd: null, customDocRef1: null, customDocRef2: null, customDocRef3: null, customDocRef4: null, lastUpdateId: null, lastDateTime: null,
                freightType: null, freightTypeDesc: null, refDoc: null, refAuthNo: null, costFreMatch: null, detailMatched: null, bestTerms: null, bestTermsSource: null,
                bestTermsDate: null, bestTermsDateSource: null, varianceWithinTolerance: null, resolutionAdjustedTotalCost: null, resolutionAdjustedTotalQty: null,
                consignmentInd: null, dealId: null, rtvInd: null, discountDate: null, dealType: null, totalCostIncVat: null, vatDiscCreateDate: null, operation: "I"                
              };
              this.service.newModel(this.model);
            } else {
              Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
              this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.editModel);
              this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            }
          }
        }, (error: string) => { console.log(error); });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
    }, (error: string) => { console.log(error); });
  }

  checkUom(saveOnly: boolean): void {
    if (!this.editMode) {
      var form: FormGroup = this.serviceDocument.dataProfile.profileForm;
      if (form.controls["uomClass"].value && form.controls["uom"].value) {
        //this.service.isExistingUOM(form.controls["uomClass"].value, form.controls["uom"].value)
        //  .subscribe((response: any) => {
        //    if (response) {
        //      this.sharedService.errorForm(this.localizationData.uom.uomcheck);
        //    } else {
        //      this.saveUom(saveOnly);
        //    }
        //  });
      }
    } else {
      this.saveUom(saveOnly);
    }
  }

  reset(): void {
    if (!this.editMode) {
      this.model = {
        docId: null, type: null, typeDesc: null, status: null, statusDesc: null, orderNO: null, location: null, locName: null, locType: null, locTypeDesc: null,
        totalDiscount: null, groupId: null, parentId: null, docDate: null, createDate: null, createId: null, vendorType: null, vendorTypeDesc: null, vendor: null,
        vendorName: null, extDocId: null, ediUploadInd: null, terms: null, termsDscntPct: null, dueDate: null, paymentMethod: null, paymentMethodDesc: null, matchId: null,
        matchDate: null, approvalId: null, approvalDate: null, prePaidInd: null, prePaidId: null, postDate: null, currencyCode: null, exchangeRate: null, totalCost: null,
        totalQty: null, manuallyPaidInd: null, customDocRef1: null, customDocRef2: null, customDocRef3: null, customDocRef4: null, lastUpdateId: null, lastDateTime: null,
        freightType: null, freightTypeDesc: null, refDoc: null, refAuthNo: null, costFreMatch: null, detailMatched: null, bestTerms: null, bestTermsSource: null,
        bestTermsDate: null, bestTermsDateSource: null, varianceWithinTolerance: null, resolutionAdjustedTotalCost: null, resolutionAdjustedTotalQty: null,
        consignmentInd: null, dealId: null, rtvInd: null, discountDate: null, dealType: null, totalCostIncVat: null, vatDiscCreateDate: null, operation: "I"
      };
      this.service.newModel(this.model);
    } else {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    }
    this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
  }
  itemBasedLogic(): void { }
  getDataChage(event: any): void { }
  }

