import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument, WorkflowAction } from "@agility/frameworkcore";
import { HttpParams } from "@angular/common/http";
import { SharedService } from '../../Common/SharedService';
import { ImDocMaintenanceHeadModel } from './ImDocMaintenanceHeadModel';
import { HttpHelperService } from '../../Common/HttpHelperService';

@Injectable()
export class ImDocMaintenanceService {
  serviceDocument: ServiceDocument<ImDocMaintenanceHeadModel> = new ServiceDocument<ImDocMaintenanceHeadModel>();

  constructor(private httpHelperService: HttpHelperService, private sharedService: SharedService) { }

  newModel(model: ImDocMaintenanceHeadModel): ServiceDocument<ImDocMaintenanceHeadModel> {
    return this.serviceDocument.newModel(model);
  }

  save(): Observable<ServiceDocument<ImDocMaintenanceHeadModel>> {   
   return this.serviceDocument.save("/api/ImDocMaintenanceHead/Save", true);    
  }  

  addEdit(id: number): Observable<ServiceDocument<ImDocMaintenanceHeadModel>> {
    if (+id === 0) {
      return this.serviceDocument.new("/api/ImDocMaintenanceHead/New");
    } else {
      return this.serviceDocument.open("/api/ImDocMaintenanceHead/Open", new HttpParams().set("id", id.toString()));
    }
  }

  getDataFromAPI(apiUrl: string): Observable<any> {
    let resp: Observable<any> = this.httpHelperService.get(apiUrl);
    return resp;
  }  
}
