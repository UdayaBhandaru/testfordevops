export class ImDocMaintenanceSearchModel {
  docId?: number;
  type?: string;
  typeDesc?: string;
  status?: string;
  statusDesc?: string;
  orderNo?: number;
  location?: number;  
  locName?: string;
  locType?: string;
  groupId?: number;  
  docDate?: Date;
  vendorType?: string;
  vendorTypeDesc?: string;
  vendor?: string;
  vendorDesc?: string;
  extDocId?: string;
  dueDate?: Date;
  refDoc?: string;
  tableName: string;
  startRow?: number;
  endRow?: number;
  totalRows?: number;
  sortModel?: {}[];  
}
