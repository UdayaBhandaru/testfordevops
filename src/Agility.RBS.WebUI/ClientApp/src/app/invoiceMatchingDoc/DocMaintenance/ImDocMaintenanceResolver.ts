import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { ImDocMaintenanceHeadModel } from './ImDocMaintenanceHeadModel';
import { ImDocMaintenanceService } from './ImDocMaintenanceService';

@Injectable()
export class ImDocMaintenanceResolver implements Resolve<ServiceDocument<ImDocMaintenanceHeadModel>> {
  constructor(private service: ImDocMaintenanceService) { }
  resolve(route: ActivatedRouteSnapshot): Observable<ServiceDocument<ImDocMaintenanceHeadModel>> {
    return this.service.addEdit(route.params["id"]);
  }
}
