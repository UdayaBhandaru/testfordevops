import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument, WorkflowAction } from "@agility/frameworkcore";
import { InvoiceMatchingDocGroupHeadModel } from "./InvoiceMatchingDocGroupHeadModel";
import { HttpParams } from "@angular/common/http";
import { SharedService } from '../Common/SharedService';
import { HttpHelperService } from '../Common/HttpHelperService';

@Injectable()
export class InvoiceMatchingDocService {
  serviceDocument: ServiceDocument<InvoiceMatchingDocGroupHeadModel> = new ServiceDocument<InvoiceMatchingDocGroupHeadModel>();

  constructor(private httpHelperService: HttpHelperService, private sharedService: SharedService) { }

  newModel(model: InvoiceMatchingDocGroupHeadModel): ServiceDocument<InvoiceMatchingDocGroupHeadModel> {
    return this.serviceDocument.newModel(model);
  }

  save(): Observable<ServiceDocument<InvoiceMatchingDocGroupHeadModel>> {
    if (!this.serviceDocument.dataProfile.dataModel.grouP_ID) {
      let currentAction: WorkflowAction = this.serviceDocument.dataProfile.actionService.allowedActions
        .find((v) => v["transitionType"] === "NT");
      this.serviceDocument.dataProfile.profileForm.controls["currentActionId"].setValue(currentAction.transitionClaim);
      return this.serviceDocument.submit("/api/InvoiceMatchingDocHead/Submit", true);
    } else {
      return this.serviceDocument.save("/api/InvoiceMatchingDocHead/Save", true);
    }
  }

  submit(): Observable<ServiceDocument<InvoiceMatchingDocGroupHeadModel>> {
    return this.serviceDocument.submit("/api/InvoiceMatchingDocHead/Submit", true);
  }

  reject(): Observable<ServiceDocument<InvoiceMatchingDocGroupHeadModel>> {
    return this.serviceDocument.submit("/api/InvoiceMatchingDocHead/Reject", true);
  }

  sendBackForReview(): Observable<ServiceDocument<InvoiceMatchingDocGroupHeadModel>> {
    return this.serviceDocument.submit("/api/InvoiceMatchingDocHead/SendBackForReview", true);
  }

  addEdit(id: number): Observable<ServiceDocument<InvoiceMatchingDocGroupHeadModel>> {
    if (+id === 0) {
      return this.serviceDocument.new("/api/InvoiceMatchingDocHead/New");
    } else {
      return this.serviceDocument.open("/api/InvoiceMatchingDocHead/Open", new HttpParams().set("id", id.toString()));
    }
  }

  getDataFromAPI(apiUrl: string): Observable<any> {
    let resp: Observable<any> = this.httpHelperService.get(apiUrl);
    return resp;
  }  
}
