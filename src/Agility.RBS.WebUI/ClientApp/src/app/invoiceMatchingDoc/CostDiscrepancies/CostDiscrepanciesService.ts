import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { CostDiscrepanciesSearchModel } from './CostDiscrepanciesSearchModel';
import { HttpHelperService } from '../../Common/HttpHelperService';
import { SharedService } from '../../Common/SharedService';

@Injectable()
export class CostDiscrepanciesService {
  serviceDocument: ServiceDocument<CostDiscrepanciesSearchModel> = new ServiceDocument<CostDiscrepanciesSearchModel>();
  searchData: any;
  orderPaging: {
    startRow: number,
    pageSize: number,
    cacheSize: number,
  } = {
      startRow: 0,
      pageSize: 10,
      cacheSize: 100,
    };
  constructor(private httpHelperService: HttpHelperService, private sharedService: SharedService) { }

  newModel(model: CostDiscrepanciesSearchModel): ServiceDocument<CostDiscrepanciesSearchModel> {
    return this.serviceDocument.newModel(model);
  }

  list(): Observable<ServiceDocument<CostDiscrepanciesSearchModel>> {
    return this.serviceDocument.list("/api/ImCostDiscrepancy/List");
  }

  search(additionalParams: any): Observable<ServiceDocument<CostDiscrepanciesSearchModel>> {
    return this.serviceDocument.search("/api/ImCostDiscrepancy/Search", true, () => this.setSubClassId(additionalParams));
  }

  addEdit(): Observable<ServiceDocument<CostDiscrepanciesSearchModel>> {
    return this.serviceDocument.list("/api/ImCostDiscrepancy/New");
  }

  save(): Observable<ServiceDocument<CostDiscrepanciesSearchModel>> {   
    return this.serviceDocument.save("/api/ImCostDiscrepancy/Save", true);  
  }

  setSubClassId(additionalParams: any): void {

    if (!this.serviceDocument.dataProfile) {
      this.serviceDocument = this.newModel({
        docId: null, location: null, locName: null, locType: null, orderNo: null, supplier: null, suppName: null, currencyCode: null, routingDate: null,
        resolveByDate: null, totalCost: null, noLineDisc: null, dept: null, deptDesc: null, class: null, classDesc: null, businessRoleId: null, cashDscntInd: null,
        apReviewer: null, docType: null, docTypeDesc: null, costReviewDetail: null
      });

    }
    var sd: CostDiscrepanciesSearchModel = this.serviceDocument.dataProfile.dataModel;
    if (additionalParams) {
      sd.startRow = additionalParams.startRow ? additionalParams.startRow : this.orderPaging.startRow;
      sd.endRow = additionalParams.endRow ? additionalParams.endRow : this.orderPaging.cacheSize;
      sd.sortModel = additionalParams.sortModel;
    } else {
      sd.startRow = this.orderPaging.startRow;
      sd.endRow = this.orderPaging.cacheSize;
    }   
  }
}
