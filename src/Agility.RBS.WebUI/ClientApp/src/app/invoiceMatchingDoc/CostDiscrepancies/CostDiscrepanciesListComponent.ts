import { Component, OnInit,ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { GridOptions, GridReadyEvent, IServerSideGetRowsParams, GridApi, IServerSideDatasource } from 'ag-grid-community';
import { Subscription } from 'rxjs';
import { CostDiscrepanciesSearchModel } from './CostDiscrepanciesSearchModel';
import { CostDiscrepanciesService } from './CostDiscrepanciesService';
import { SharedService } from '../../Common/SharedService';
import { InfoComponent } from '../../Common/InfoComponent';
import { GridEditComponent } from '../../Common/grid/GridEditComponent';
import { GridDateComponent } from '../../Common/grid/GridDateComponent';

@Component({
  selector: "CostDiscrepancies-List",
  templateUrl: "./CostDiscrepanciesListComponent.html",
  styleUrls: ['./CostDiscrepanciesListComponent.scss']
})
export class CostDiscrepanciesListComponent implements OnInit, IServerSideDatasource {
  public serviceDocument: ServiceDocument<CostDiscrepanciesSearchModel>;
  PageTitle = "Cost Discrepancies List";
  redirectUrl = "imCostDiscrepancy/New/";
  componentName: any;
  columns: any[];
  additionalSearchParams: any;
  searchServiceSubscription: Subscription;
  showSearchCriteria: boolean = true;
  alwaysShow: boolean = true;

  model: CostDiscrepanciesSearchModel = {
    docId: null, location: null, locName: null, locType: null, orderNo: null, supplier: null, suppName: null, currencyCode: null, routingDate: null,
    resolveByDate: null, totalCost: null, noLineDisc: null, dept: null, deptDesc: null, class: null, classDesc: null, businessRoleId: null, cashDscntInd: null, apReviewer: null
    , docType: null, docTypeDesc: null, costReviewDetail: null
  };
  public gridOptions: GridOptions;
  gridApi: GridApi;

  constructor(public service: CostDiscrepanciesService, private sharedService: SharedService, private router: Router, private changeRef: ChangeDetectorRef) {
  }

  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }

  ngOnInit(): void {
    this.componentName = this;
    this.gridOptions = {
      onGridReady: (params: GridReadyEvent) => {
        params.api.setServerSideDatasource(this);
      },
      context: {
        componentParent: this
      },
      suppressRowClickSelection: true,
      suppressCellSelection: true,
      rowModelType: "serverSide",
      paginationPageSize: this.service.orderPaging.pageSize,
      pagination: true,
      enableServerSideSorting: true,
      enableServerSideFilter: true,
      cacheBlockSize: this.service.orderPaging.cacheSize,
    };
    this.columns = [
      { headerName: "Department", field: "dept", tooltipField: "dept", sort: "desc" },
      { headerName: "Class", field: "class", tooltipField: "class" },
      { headerName: "Supplier", field: "supplier", tooltipField: "supplier" },
      { headerName: "Supplier Name", field: "suppName", tooltipField: "suppName" },
      { headerName: "Documnet Type", field: "docType", tooltipField: "docType" },
      { headerName: "Resolve By Date", field: "resolveByDate", tooltipField: "resolveByDate", cellRendererFramework: GridDateComponent },
      { headerName: "Cash Document", field: "cashDscntInd", tooltipField: "cashDscntInd" },
      { headerName: "Route Date", field: "routingDate", tooltipField: "routingDate", cellRendererFramework: GridDateComponent },
      { headerName: "Order", field: "orderNo", tooltipField: "orderNo" },
      { headerName: "Location", field: "location", tooltipField: "location" },
      { headerName: "Location Desc", field: "locName", tooltipField: "locName" },
      { headerName: "No.of Line Exceptions", field: "noLineDisc", tooltipField: "noLineDisc"},
      { headerName: "Total Document Amount", field: "totalCost", tooltipField: "totalCost" },
      { headerName: "Currency", field: "currencyCode", tooltipField: "currencyCode" },
      { headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent }
    ];
       
    this.service.newModel(this.model);
    this.serviceDocument = this.service.serviceDocument;
  }

  getRows(params: IServerSideGetRowsParams): void {
    this.additionalSearchParams = { startRow: params.request.startRow, endRow: params.request.endRow, sortModel: params.request.sortModel };   
    this.searchServiceSubscription = this.service.search(this.additionalSearchParams).subscribe((sDoc) => {
      if (sDoc.dataProfile.dataList) {
        this.alwaysShow = true;
        params.successCallback(sDoc.dataProfile.dataList, this.service.serviceDocument.dataProfile.dataModel.totalRows);
      }
      else {
        this.alwaysShow = false;
      }
      });   
  }

  addValidation(): void {
    this.router.navigate(["/" + this.redirectUrl + "0"], { skipLocationChange: true });
    this.sharedService.previousUrl = "../imCostDiscrepancy/List";
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl + cell.data.dept, this.service.serviceDocument.dataProfile.dataList);
  }  
}
