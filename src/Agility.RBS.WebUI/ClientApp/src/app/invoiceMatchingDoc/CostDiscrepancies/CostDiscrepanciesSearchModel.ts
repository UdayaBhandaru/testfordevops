import { CostReviewDetailModel } from './CostReviewDetailModel';

export class CostDiscrepanciesSearchModel {
  docId?: number;
  location?: number;
  locName?: string;
  locType?: string;
  orderNo?: number;
  supplier?: number;
  suppName?: string;
  currencyCode?: string;
  routingDate?: Date;
  resolveByDate?: Date;
  totalCost?: number;
  noLineDisc?: number;
  dept?: number;
  deptDesc?: string;
  class?: number;
  classDesc?: string;
  businessRoleId?: number;
  cashDscntInd?: string;
  apReviewer?: string;
  docType?: string;
  docTypeDesc?: string;
  startRow?: number;
  endRow?: number;
  totalRows?: number;
  sortModel?: {}[];
  costReviewDetail?: CostReviewDetailModel[];
}
