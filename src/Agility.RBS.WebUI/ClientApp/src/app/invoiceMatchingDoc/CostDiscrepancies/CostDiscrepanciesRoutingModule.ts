import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CostDiscrepanciesListComponent } from './CostDiscrepanciesListComponent';
import { CostDiscrepanciesListResolver, CostReviewDetailResolver } from './CostDiscrepanciesResolver';
import { CostReviewDetailComponent } from './CostReviewDetailComponent';
const ShipmentRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: CostDiscrepanciesListComponent,
                resolve:
                {
                  references: CostDiscrepanciesListResolver
                }
            },
            {
                path: "New/:id",
                component: CostReviewDetailComponent,
                resolve:
                {
                  references: CostReviewDetailResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(ShipmentRoutes)
    ]
})
export class CostDiscrepanciesRoutingModule { }
