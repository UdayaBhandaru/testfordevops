import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { AgGridModule } from "ag-grid-angular/main";
import { CostDiscrepanciesRoutingModule } from './CostDiscrepanciesRoutingModule';
import { AgGridSharedModule } from '../../AgGridSharedModule';
import { RbsSharedModule } from '../../RbsSharedModule';
import { TitleSharedModule } from '../../TitleSharedModule';
import { CostDiscrepanciesListComponent } from './CostDiscrepanciesListComponent';
import { CostDiscrepanciesService } from './CostDiscrepanciesService';
import { CostDiscrepanciesListResolver, CostReviewDetailResolver } from './CostDiscrepanciesResolver';
import { CostReviewDetailComponent } from './CostReviewDetailComponent';


@NgModule({
    imports: [
        FrameworkCoreModule,
        CostDiscrepanciesRoutingModule,
        AgGridModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        CostDiscrepanciesListComponent,
        CostReviewDetailComponent
    ],
    providers: [
        CostDiscrepanciesService,
        CostDiscrepanciesListResolver,
        CostReviewDetailResolver
    ]
})
export class CostDiscrepanciesModule {
}
