import { Component, OnInit } from "@angular/core";
import { ServiceDocument, CommonService, MessageType} from "@agility/frameworkcore";
import { Router } from "@angular/router";
import { GridOptions,RowNode, GridApi, ColumnApi} from 'ag-grid-community';
import { SharedService } from '../../Common/SharedService';
import { CostReviewDetailModel } from './CostReviewDetailModel';
import { CostDiscrepanciesService } from './CostDiscrepanciesService';
import { CostDiscrepanciesSearchModel } from './CostDiscrepanciesSearchModel';
import { ReasonCodeModel } from '../../administration/ReasonCode/ReasonCodeModel';
import { GridSelectComponent } from '../../Common/grid/GridSelectComponent';
import { FormControl } from '@angular/forms';

@Component({
  selector: "CostReviewDetail",
  templateUrl: "./CostReviewDetailComponent.html",
  styleUrls: ['./CostReviewDetailComponent.scss']
})
export class CostReviewDetailComponent implements OnInit {
  PageTitle = "Cost Review Detail";
  serviceDocument: ServiceDocument<CostDiscrepanciesSearchModel>;
  columns: any[];
  dataList: CostReviewDetailModel[];
  editMode: boolean = false;
  editModel: any;
  componentParentName: CostReviewDetailComponent;
  localizationData: any; 
  gridOptions: GridOptions;
  reasonCodeData: ReasonCodeModel[];
  gridApi: GridApi;
  gridColumnApi: ColumnApi;
  model: CostDiscrepanciesSearchModel = {
    docId: null, location: null, locName: null, locType: null, orderNo: null, supplier: null, suppName: null, currencyCode: null, routingDate: null,
    resolveByDate: null, totalCost: null, noLineDisc: null, dept: null, deptDesc: null, class: null, classDesc: null, businessRoleId: null, cashDscntInd: null, apReviewer: null
    , docType: null, docTypeDesc: null }

  constructor(private service: CostDiscrepanciesService, private commonService: CommonService,
    private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.reasonCodeData = Object.assign([], this.service.serviceDocument.domainData["reasonCode"]);
    this.serviceDocument = this.service.serviceDocument;
    this.componentParentName = this;
    this.columns = [
      {
        headerName: "Reason Code", field: "reasonCode", cellRendererFramework: GridSelectComponent, cellClassRules: {
          "invalid-grid-row": function (params: any): any { return !params.value; }
        }, cellRendererParams: { references: this.reasonCodeData, keyvaluepair: { key: "reasonCodeId", value: "reasonCodeDescription" } },
        tooltipField: "code", headerTooltip: "ReasonCode Description"
      },
      { headerName: "Action", field: "action", tooltipField: "action", headerTooltip: "Action" },
      { headerName: "Discrepancy Comments", field: "item", tooltipField: "item" },
      { headerName: "ITEM", field: "item", tooltipField: "item"},
      { headerName: "DESCRIPTION", field: "itemDesc", tooltipField: "itemDesc" },
      { headerName: "Orig Order Cost", field: "orderUnitCost", tooltipField: "orderUnitCost" },
      { headerName: "Current Order Cost", field: "orderTotalCost", tooltipField: "orderTotalCost" },
      { headerName: "Document Cost", field: "docUnitCost", tooltipField: "docUnitCost" },
      { headerName: "Unit Cost Varriance Fav", field: "costVariance", tooltipField: "costVariance" },
      { headerName: "Unit Cost Varriance Percent Fav", field: "costVarPerc", tooltipField: "costVarPerc" },
      { headerName: "Order Cost Source", field: "orderCostsrc", tooltipField: "orderCostsrc" },
      { headerName: "Order Upc", field: "ordUpc", tooltipField: "ordUpc" },
      { headerName: "Vpn", field: "vpn", tooltipField: "vpn" },      
    ];
    this.gridOptions = {
      onGridReady: (params: any) => {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
      }
    };
    this.bind();
  }  

  bind(): void {
    delete this.editModel;
    if (this.sharedService.editObject) {
      this.dataList = this.sharedService.editObject.imCostReviewDetails;
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
      this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
      delete this.sharedService.editObject;
    }       
      this.serviceDocument = this.service.serviceDocument;
  }

  save(saveOnly: boolean): void {
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.saveSubscription(saveOnly);
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  saveAndContinue(): void {
    this.save(false);
  }

  saveSubscription(saveOnly: boolean): void {
    this.saveCostDiscrepancies(saveOnly);
  }
  saveCostDiscrepancies(saveOnly: boolean): void {
    this.serviceDocument.dataProfile.profileForm.controls["imCostReviewDetails"] = new FormControl();
    this.serviceDocument.dataProfile.profileForm.controls["imCostReviewDetails"].setValue(this.dataList);
    this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.sharedService.saveForm("Cost Discrepancies Saved Successfully").subscribe(() => {
          
        }, (error: string) => { console.log(error); });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
    }, (error: string) => { console.log(error); });
  }

  select(cell: any, event: any): void {
    this.dataList[cell.rowIndex][cell.column.colId] = event.target.value;
    if (cell.column.colId === "reasonCode") {
      this.dataList[cell.rowIndex]["reasonCode"] = event.target.value;
      var reasoncodeType = this.reasonCodeData.filter(id => id.reasonCodeId == event.target.value);
      this.dataList[cell.rowIndex]["action"] = reasoncodeType[0].action;
    }
    let rowNode: RowNode = this.gridApi.getRowNode(cell.rowIndex);
    rowNode.setData(rowNode.data);
  }

  reset(): void {

  }

}
