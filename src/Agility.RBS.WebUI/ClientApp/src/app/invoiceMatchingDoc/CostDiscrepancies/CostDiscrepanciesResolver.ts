import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { CostDiscrepanciesSearchModel } from './CostDiscrepanciesSearchModel';
import { CostDiscrepanciesService } from './CostDiscrepanciesService';

@Injectable()
export class CostDiscrepanciesListResolver implements Resolve<ServiceDocument<CostDiscrepanciesSearchModel>> {
   constructor(private service: CostDiscrepanciesService) { }
  resolve(): Observable<ServiceDocument<CostDiscrepanciesSearchModel>> {
    return this.service.list();
  }  
}

@Injectable()
export class CostReviewDetailResolver implements Resolve<ServiceDocument<CostDiscrepanciesSearchModel>> {
  constructor(private service: CostDiscrepanciesService) { }
  resolve(): Observable<ServiceDocument<CostDiscrepanciesSearchModel>> {
    return this.service.addEdit();
  }
}
