import { InvoiceMatchingDocHeadModel } from './InvoiceMatchingDocHeadModel';
//import { NonMerchandiseCostModel } from './NonMerchandiseCost/NonMerchandiseCostModel';

export class InvoiceMatchingDocGroupHeadModel {  
  grouP_ID?: number;
  createDate?: Date;
  createId: string;
  status: string;
  statusDesc: string;
  controlRecCount?: number;
  calcCount?: number;
  varianceCount?: number;
  controlTotalCost?: number;
  calcCost?: number;
  varianceCost?: number;
  currencyCode: string;
  lastUpdateId: string;
  lastUpdateDateTime?: Date;
  workflowInstance: any;
  operation: string;
  invoiceMatchingDocHead: InvoiceMatchingDocHeadModel[];
  docDate?: Date;
  type: string;
  vendorType: string;
  vendorTypeDesc: string;
  supplier?: number; 
  supNameDesc: string;
  partner?: number;
  partnerDesc: string;
  terms: string;
  refDoc?: number;
  vendor?: number;
  vendorName: string;
  exchangeRate: number;
  currencyValue: string;
}
