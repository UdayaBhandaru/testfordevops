import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpParams } from "@angular/common/http";
import { ServiceDocument } from "@agility/frameworkcore";
import { VatBreakdownModel } from "./VatBreakdownModel";
import { SharedService } from "../../Common/SharedService";
import { HttpHelperService } from "../../Common/HttpHelperService";
import { VatBreakdownDomainModel } from './VatBreakdownDomainModel';

@Injectable()
export class VatBreakdownPopupService {
  serviceDocument: ServiceDocument<VatBreakdownModel> = new ServiceDocument<VatBreakdownModel>(); 
  constructor(private sharedService: SharedService, private httpHelperService: HttpHelperService) { }

  docSearch(docId: number): Observable<any[]> {
    return this.httpHelperService.get("/api/InvoiceMatchingVat/GetInvoiceMatchingVatList", new HttpParams().set("docId", docId.toString()));
  }

  invoiceMatchingVatDomainData(id: number): Observable<VatBreakdownDomainModel> {
    return this.httpHelperService.get("/api/InvoiceMatchingVat/InvoiceMatchingVatDomainData", new HttpParams().set("id", id.toString()));
  }

  vatcodeSearch(docdate: string): Observable<any[]> {
    return this.httpHelperService.get("/api/InvoiceMatchingVat/GetVatRateList", new HttpParams().set("docdate", docdate));
  }

  addEdit(id: number): Observable<ServiceDocument<VatBreakdownModel>> {
    if (+id === 0) {
      return this.serviceDocument.new("/api/InvoiceMatchingVat/New");
    } else {
      return this.serviceDocument.open("/api/InvoiceMatchingVat/Open", new HttpParams().set("id", id.toString()));
    }
  }

  save(): Observable<ServiceDocument<VatBreakdownModel>> {
    return this.serviceDocument.save("/api/InvoiceMatchingVat/Save", true);
  }

  saveVat(model: VatBreakdownModel[]): Observable<boolean> {
    return this.httpHelperService.post("/api/InvoiceMatchingVat/saveVat", model);
  }

  newModel(model: VatBreakdownModel): ServiceDocument<VatBreakdownModel> {
    this.serviceDocument.newModel(model);
    return this.serviceDocument;
  }

  getItems(apiUrl: string): Observable<any[]> {
    return this.httpHelperService.get(apiUrl);
  }

}
