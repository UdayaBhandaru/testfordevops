export class VatBreakdownModel {
  docId: number;  
  vatCode: number;    
  vatRate: number;
  vatBasis: number;
  vatAmount: number;
  operation: string;
  totalValue: string;
  totalCount: string;
}
