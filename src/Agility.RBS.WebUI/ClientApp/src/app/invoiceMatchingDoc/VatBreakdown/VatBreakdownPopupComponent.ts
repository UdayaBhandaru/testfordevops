import { Component, OnInit, OnDestroy, Inject, ChangeDetectorRef } from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";
import { Router } from "@angular/router";
import { ServiceDocument, CommonService, FormMode, MessageType } from "@agility/frameworkcore";
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from "@angular/material";
import { GridOptions, GridReadyEvent, ColDef, RowNode, GridApi, ColumnApi } from "ag-grid-community";
import { GridSelectComponent } from "../../Common/grid/GridSelectComponent";
import { SharedService } from "../../Common/SharedService";
import { LoaderService } from "../../Common/LoaderService";
import { Subscription } from "rxjs";
import { ItemSelectService } from "../../Common/ItemSelectService";
import { GridNumericComponent } from "../../Common/grid/GridNumericComponent";
import { GridEditComponent } from '../../Common/grid/GridEditComponent';
import { SupplierDealService } from '../../Supplier/SupplierDeals/SupplierDealService';
import { VatBreakdownModel } from './VatBreakdownModel';
import { VatBreakdownPopupService } from './VatBreakdownPopupService';
import { VatCodeModel } from '../../valueaddedtax/vatcode/VatCodeModel';
import { VatBreakdownDomainModel } from './VatBreakdownDomainModel';
import { VatRateModel } from '../../valueaddedtax/vatrates/VatRateModel';

@Component({
  selector: "nonMerchandise-cost",
  templateUrl: "./VatBreakdownPopupComponent.html",
  styleUrls: ['./VatBreakdownPopupComponent.scss'],
  providers: [
    SupplierDealService,
    ItemSelectService,
    VatBreakdownPopupService
  ]
})

export class VatBreakdownPopupComponent implements OnInit {
  infoGroup: FormGroup;
  public serviceDocument: ServiceDocument<VatBreakdownModel>;
  detailModel: VatBreakdownModel;
  dataList: VatBreakdownModel[] = [];
  PageTitle: string = "Vat Breakdown";
  objectValue: string;
  objectName: string;
  docId: number;
  componentName: any;
  columns: {}[];
  gridOptions: GridOptions;
  redirectUrl: string;
  parentData: any[];  
  vatBreakdownDomainData: VatBreakdownDomainModel[];
  vatcodeData: VatCodeModel[];
  vatRateData: VatRateModel[];
  saveServiceSubscription: Subscription;
  saveSuccessSubscription: Subscription;
  localizationData: any;
  deletedRows: any[] = [];
  gridApi: GridApi;
  gridColumnApi: ColumnApi;
  editMode: boolean = false;
  effectiveMinDate: Date;
  message: string;
  textcoloumn: boolean;
  totalCount: string;
  mandatoryNonmarchandiseLocFields: string[] = [    
    "vatCode",
    "vatRate",
    "vatBasis"
  ]; 
  model: VatBreakdownModel;


  constructor(private service: VatBreakdownPopupService, private _commonService: CommonService, public dialog: MatDialog
    , private _router: Router, @Inject(MAT_DIALOG_DATA) public data: any, public dialogRef: MatDialogRef<any>
    , private sharedService: SharedService, private loaderService: LoaderService
    , private itemSelectService: ItemSelectService, private changeRef: ChangeDetectorRef
  ) {
    this.docId = data.DocId;
    this.vatcodeData = data.InvoiceMatchingVat.vatCodeType;
    this.vatRateData = data.InvoiceMatchingVat.vatRateType;
    if (data.InvoiceMatchingVat.invoiceMatchingVatList) {
      this.dataList = data.InvoiceMatchingVat.invoiceMatchingVatList;      
    }
    this.model = {
      docId: null, vatCode: null, vatRate: null, vatBasis: null, operation: "I", totalValue: null, totalCount: null, vatAmount: null
    };
    Object.assign(this.model, data.InvoiceMatchingVat.vatBreakdownModel);
    this.infoGroup = this._commonService.getFormGroup(this.model, FormMode.Open);
  }

  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }

  ngOnInit(): void {
    this.bind();
    if (this.dataList.length > 0) {
      this.textcoloumn = true;
      this.totalAmtCalc();
    }
  }

  bind(): void {
    this.setGridColumnsWithOptions();
    if (this.docId.toString() !== "0") {
      this.editMode = true;
      this.PageTitle = `${this.PageTitle} - EDIT - ${this.docId}`;
      
    } else {
      this.PageTitle = `${this.PageTitle} - ADD (Ducument ID - ${this.docId})`;
    }
  }
  setGridColumnsWithOptions(): void {
    this.columns = [
      {
        headerName: "Vat Basis", field: "vatBasis", cellRendererFramework: GridNumericComponent, tooltipField: "vatBasis", headerTooltip: "vatBasis"
      },
      {
        headerName: "Vat Code", field: "vatCode", cellRendererFramework: GridSelectComponent, cellClassRules: {
          "invalid-grid-row": function (params: any): any { return !params.value; }
        }, cellRendererParams: { references: this.vatcodeData, keyvaluepair: { key: "vatCode", value: "vatCodeDesc" } },
        tooltipField: "vatCode", headerTooltip: "vatCodeDesc"
      },     
      {
        headerName: "Vat Rate", field: "vatRate", cellRendererFramework: GridSelectComponent, cellClassRules: {
          "invalid-grid-row": function (params: any): any { return !params.value; }
        }, cellRendererParams: { references: this.vatRateData, keyvaluepair: { key: "vatRate", value: "vatRate" } },
        tooltipField: "vatRate", headerTooltip: "vatRate"
      },  
      {
        headerName: "Vat Amount", field: "vatAmount", cellRendererFramework: GridNumericComponent, tooltipField: "vatAmount", headerTooltip: "vatAmount"
      },  
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent
        , cellRendererParams: { restrictEditIcon: true }
      }
    ];

    this.gridOptions = {
      onGridReady: (params: any) => {
        this.gridApi = params.api;
        this.gridApi.sizeColumnsToFit();
        this.gridColumnApi = params.columnApi;
      },
      context: {
        componentParent: this
      },
      suppressPaginationPanel: true,
      suppressScrollOnNewData: false,
      paginationPageSize: 10,
      pagination: true,
      enableSorting: true,
      rowHeight: 40,
      enableColResize: true,
      rowSelection: "multiple",
      suppressRowClickSelection: true,
      suppressCellSelection: true,
      groupUseEntireRow: true,
      groupDefaultExpanded: -1,
      domLayout: "autoHeight",
      toolPanelSuppressSideButtons: true,
      embedFullWidthRows: true
    };
  }

  newModel(): void {
    this.detailModel = {
      docId: this.docId, vatCode: null, vatRate: null, vatBasis: null, operation: "I", totalValue: null, totalCount: null,vatAmount: null };
  }
  add($event: MouseEvent): void {
    $event.preventDefault();
    this.newModel();
    this.dataList.unshift(this.detailModel);
    this.textcoloumn = true;
    if (this.gridApi) {
      this.gridApi.setRowData(this.dataList);
    }
  }

  save(): void {
    if (this.infoGroup.valid) {
      this.saveSubscription();
    } else {
      this.sharedService.validateForm(this.infoGroup);
    }
  }

  saveSubscription(): void {
    if (this.dataList.length > 0) {
    if (this.validateData()) {     
      this.saveServiceSubscription = this.service.saveVat(this.dataList).subscribe((response: boolean) => {
        if (response === true) {
          this.saveSuccessSubscription = this.sharedService.saveForm(
            "Vat Saved Successfully").subscribe(() => {
              this.message = this.totalCount;
              this.dialogRef.close(this.message);
            });
        } else {
          this.sharedService.errorForm("error");
        }
      });
    } else {
      this.sharedService.errorForm("Please fill Mandatory fields in Grid");
    }
    } else {
      this.sharedService.errorForm("Atleast 1 Vat  Detail is required");
    }

  }

  validateData(): boolean {
    let valid: boolean = true;
    if (this.dataList != null && this.dataList.length > 0) {
      this.dataList.forEach(row => {
        this.mandatoryNonmarchandiseLocFields.forEach(field => {
          if (!row[field]) {
            valid = false;
          }
        });       
      });
    }
    return valid;
  }

  close(): void {
    this.message = this.totalCount;
    this.dialogRef.close(this.message);
  }

  select(cell: any, event: any): void {
    this.dataList[cell.rowIndex][cell.column.colId] = event.target.value;
    let vatcount = 0;
    if (cell.column.colId === "vatCode") {
      this.dataList[cell.rowIndex]["vatCode"] = event.target.value;
    }
    if (cell.column.colId === "vatRate") {
      this.dataList[cell.rowIndex]["vatRate"] = event.target.value;
      if (cell.data.vatBasis) {
        vatcount = cell.data.vatBasis;
      }
      cell.data.vatAmount = vatcount * Number(event.target.value) / 100;
      this.totalAmtCalc();
    }
    let rowNode: RowNode = this.gridApi.getRowNode(cell.rowIndex);
    rowNode.setData(rowNode.data);
  }

  update(cell: any, event: any): void {  
      this.dataList[cell.rowIndex][cell.column.colId] = event.target.value;
      let total: number = null;
      let entryvalue = event.target.value;
      let rate = cell.data.vatRate;
      if (cell.column.colId === "vatBasis") {
        this.dataList[cell.rowIndex]["vatBasis"] = event.target.value;
        total = Number(entryvalue) * Number(rate) / 100;
      }
      cell.data.vatAmount = total;
      this.totalAmtCalc();
      let rowNode: RowNode = this.gridApi.getRowNode(cell.rowIndex);
      rowNode.setData(rowNode.data);    
  }

  delete(cell: any): void {
    this.gridApi.updateRowData({ remove: [cell.data] });
    cell.data.operation = "D";
    this.deletedRows.push(cell.data);
    this.dataList.splice(cell.rowIndex, 1);
    this.newModel();
  }

  totalAmtCalc(): void {
    var totalCount= 0;   
    this.dataList.forEach(ctrl => {
      totalCount += Number(ctrl.vatAmount);
    });
    this.infoGroup.controls["totalCount"].setValue(totalCount);
    this.totalCount = this.infoGroup.controls["totalCount"].value;
  }
}
