import { VatCodeModel } from '../../valueaddedtax/vatcode/VatCodeModel';
import { VatBreakdownModel } from './VatBreakdownModel';

export class VatBreakdownDomainModel {
  vatCodeData?: VatCodeModel[];
  vatBreakdownModel: VatBreakdownModel;
}
