import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpParams } from "@angular/common/http";
import { ServiceDocument } from "@agility/frameworkcore";
import { NonMerchandiseCostModel } from "./NonMerchandiseCostModel";
import { SharedService } from "../../Common/SharedService";
import { HttpHelperService } from "../../Common/HttpHelperService";
import { NonMerchandiseCostDomainModel } from './NonMerchandiseCostDomainModel';

@Injectable()
export class NonMerchandiseCostPopupService {
  serviceDocument: ServiceDocument<NonMerchandiseCostModel> = new ServiceDocument<NonMerchandiseCostModel>(); 
  constructor(private sharedService: SharedService, private httpHelperService: HttpHelperService) { }

  docSearch(docId: number): Observable<any[]> {
    return this.httpHelperService.get("/api/NonMerchandiseCost/GetNonMerchandiseCostList", new HttpParams().set("docId", docId.toString()));
  }

  nonMerchandiseCostDomainData(id: number): Observable<NonMerchandiseCostDomainModel> {
    return this.httpHelperService.get("/api/NonMerchandiseCost/NonMerchandiseCostDomainData", new HttpParams().set("id", id.toString()));
  }

  

  addEdit(id: number): Observable<ServiceDocument<NonMerchandiseCostModel>> {
    if (+id === 0) {
      return this.serviceDocument.new("/api/NonMerchandiseCost/New");
    } else {
      return this.serviceDocument.open("/api/NonMerchandiseCost/Open", new HttpParams().set("id", id.toString()));
    }
  }

  save(): Observable<ServiceDocument<NonMerchandiseCostModel>> {
    return this.serviceDocument.save("/api/NonMerchandiseCost/Save", true);
  }

  saveNonmerchandisecost(model: NonMerchandiseCostModel[]): Observable<boolean> {
    return this.httpHelperService.post("/api/NonMerchandiseCost/NonmerchandisecostSave", model);
  }

  newModel(model: NonMerchandiseCostModel): ServiceDocument<NonMerchandiseCostModel> {
    this.serviceDocument.newModel(model);
    return this.serviceDocument;
  }

  getItems(apiUrl: string): Observable<any[]> {
    return this.httpHelperService.get(apiUrl);
  }
  
}
