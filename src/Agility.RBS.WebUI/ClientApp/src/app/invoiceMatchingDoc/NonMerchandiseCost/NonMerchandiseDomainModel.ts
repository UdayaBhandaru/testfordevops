export class NonMerchandiseDomainModel {
  nonMerchCode: string;
  nonMerchCodeDesc: string;
  serviceInd: string;
  operation: string;
}
