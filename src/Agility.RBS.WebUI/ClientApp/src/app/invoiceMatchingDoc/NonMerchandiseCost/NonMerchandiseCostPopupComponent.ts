import { Component, OnInit, OnDestroy, Inject, ChangeDetectorRef } from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";
import { Router } from "@angular/router";
import { ServiceDocument, CommonService, FormMode, MessageType } from "@agility/frameworkcore";
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from "@angular/material";
import { GridOptions, GridReadyEvent, ColDef, RowNode, GridApi, ColumnApi } from "ag-grid-community";
import { GridSelectComponent } from "../../Common/grid/GridSelectComponent";
import { SharedService } from "../../Common/SharedService";
import { LoaderService } from "../../Common/LoaderService";
import { Subscription } from "rxjs";
import { ItemSelectService } from "../../Common/ItemSelectService";
import { GridNumericComponent } from "../../Common/grid/GridNumericComponent";
import { GridEditComponent } from '../../Common/grid/GridEditComponent';
import { SupplierDealService } from '../../Supplier/SupplierDeals/SupplierDealService';
import { NonMerchandiseCostModel } from './NonMerchandiseCostModel';
import { NonMerchandiseCostPopupService } from './NonMerchandiseCostPopupService';
import { VatCodeModel } from '../../valueaddedtax/vatcode/VatCodeModel';
import { NonMerchandiseDomainModel } from './NonMerchandiseDomainModel';

@Component({
  selector: "nonMerchandise-cost",
  templateUrl: "./NonMerchandiseCostPopupComponent.html",
  styleUrls: ['./NonMerchandiseCostPopupComponent.scss'],
  providers: [
    SupplierDealService,
    ItemSelectService,
    NonMerchandiseCostPopupService
  ]
})

export class NonMerchandiseCostPopupComponent implements OnInit {
  infoGroup: FormGroup;
  public serviceDocument: ServiceDocument<NonMerchandiseCostModel>;
  detailModel: NonMerchandiseCostModel;
  dataList: NonMerchandiseCostModel[] = [];
  PageTitle: string = "Non Merchandise Cost";
  objectValue: string;
  objectName: string;
  docId: number;
  componentName: any;
  columns: {}[];
  gridOptions: GridOptions;
  redirectUrl: string;
  parentData: any[];
  nonMerchandiseData: NonMerchandiseDomainModel[];
  vatcodeData: VatCodeModel[];
  saveServiceSubscription: Subscription;
  saveSuccessSubscription: Subscription;
  localizationData: any;
  deletedRows: any[] = [];
  gridApi: GridApi;
  gridColumnApi: ColumnApi;
  editMode: boolean = false;
  effectiveMinDate: Date;
  message: string;
  textcoloumn: boolean;
  totalCount: string;
  mandatoryNonmarchandiseLocFields: string[] = [
    "nonMerchCode"
  ];
  model: NonMerchandiseCostModel;


  constructor(private service: NonMerchandiseCostPopupService, private _commonService: CommonService, public dialog: MatDialog
    , private _router: Router, @Inject(MAT_DIALOG_DATA) public data: any, public dialogRef: MatDialogRef<any>
    , private sharedService: SharedService, private loaderService: LoaderService
    , private itemSelectService: ItemSelectService, private changeRef: ChangeDetectorRef
  ) {
    this.docId = data.DocId;
    this.nonMerchandiseData = data.NonMerchandiseCost.nonmerchdiseType;
    this.vatcodeData = data.NonMerchandiseCost.vatCodeType;
    if (data.NonMerchandiseCost.nonMerchandiseCostlist) {
      this.dataList = data.NonMerchandiseCost.nonMerchandiseCostlist;
    }
    this.model = {
      docId: null, nonMerchCode: null, nonMerchCodeDesc: null, nonMerchAmt: null
      , vatCode: null, vatRate: null, operation: "I", totalValue: null
    };
    Object.assign(this.model, data.NonMerchandiseCost.nonMerchandiseCost);
    this.infoGroup = this._commonService.getFormGroup(this.model, FormMode.Open);
  }

  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }

  ngOnInit(): void {
    this.bind();
    if (this.dataList.length > 0) {
      this.textcoloumn = true;
      this.totalAmtCalc();
    }
  }

  bind(): void {
    this.setGridColumnsWithOptions();
    if (this.docId.toString() !== "0") {
      this.editMode = true;
      this.PageTitle = `${this.PageTitle} - EDIT - ${this.docId}`;
    } else {
      this.PageTitle = `${this.PageTitle} - ADD (Ducument ID - ${this.docId})`;
    }
  }
  setGridColumnsWithOptions(): void {
    this.columns = [
      {
        headerName: "Document Type", field: "nonMerchCode", cellRendererFramework: GridSelectComponent, cellClassRules: {
          "invalid-grid-row": function (params: any): any { return !params.value; }
        }, cellRendererParams: { references: this.nonMerchandiseData, keyvaluepair: { key: "nonMerchCode", value: "nonMerchCodeDesc" } },
        tooltipField: "nonMerchCode", headerTooltip: "nonMerchCodeDesc"
      },

      {
        headerName: "Amount", field: "nonMerchAmt", cellRendererFramework: GridNumericComponent, tooltipField: "nonMerchAmt", headerTooltip: "nonMerchAmt"
      },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent
        , cellRendererParams: { restrictEditIcon: true }
      }
    ];

    this.gridOptions = {
      onGridReady: (params: any) => {
        this.gridApi = params.api;
        this.gridApi.sizeColumnsToFit();
        this.gridColumnApi = params.columnApi;
      },
      context: {
        componentParent: this
      },
      suppressPaginationPanel: true,
      suppressScrollOnNewData: false,
      paginationPageSize: 10,
      pagination: true,
      enableSorting: true,
      rowHeight: 40,
      enableColResize: true,
      rowSelection: "multiple",
      suppressRowClickSelection: true,
      suppressCellSelection: true,
      groupUseEntireRow: true,
      groupDefaultExpanded: -1,
      domLayout: "autoHeight",
      toolPanelSuppressSideButtons: true,
      embedFullWidthRows: true
    };
  }

  newModel(): void {
    this.detailModel = {
      docId: this.docId, nonMerchCode: null, nonMerchCodeDesc: null, nonMerchAmt: null, vatCode: null, vatRate: 0, operation: "I", totalValue: null
    };
  }
  add($event: MouseEvent): void {
    $event.preventDefault();
    this.newModel();
    this.dataList.unshift(this.detailModel);
    this.textcoloumn = true;
    if (this.gridApi) {
      this.gridApi.setRowData(this.dataList);
    }
  }

  save(): void {
    if (this.infoGroup.valid) {      
          this.saveSubscription();       
      } else {
        this.sharedService.validateForm(this.infoGroup);
      }

    }

  saveSubscription(): void {
  if(this.dataList.length > 0) {
      if(this.validateData()) {
      this.saveServiceSubscription = this.service.saveNonmerchandisecost(this.dataList).subscribe((response: boolean) => {
        if (response === true) {
          this.saveSuccessSubscription = this.sharedService.saveForm(
            "Nonmerchandise Cost Saved Successfully").subscribe(() => {
              this.message = this.totalCount;
              this.dialogRef.close(this.message);
            });
        } else {
          this.sharedService.errorForm("error");
        }
      });
    } else {
      this.sharedService.errorForm("Please fill Mandatory fields in Grid");
      }
  } else {
    this.sharedService.errorForm("Atleast 1 Nonmerchandisecost  Detail is required");
  }
  }

  validateData(): boolean {
    let valid: boolean = true;
    if (this.dataList != null && this.dataList.length > 0) {
      this.dataList.forEach(row => {
        this.mandatoryNonmarchandiseLocFields.forEach(field => {
          if (!row[field]) {
            valid = false;
          }
        });
      });
    }
    return valid;
  }

  close(): void {
    this.dialogRef.close();
  }

  select(cell: any, event: any): void {
    this.dataList[cell.rowIndex][cell.column.colId] = event.target.value;
    if (cell.column.colId === "nonMerchCode") {
      this.dataList[cell.rowIndex]["nonMerchCode"] = event.target.value;
    }
    if (cell.column.colId === "vatCode") {
      this.dataList[cell.rowIndex]["vatCode"] = event.target.value;
    }
    let rowNode: RowNode = this.gridApi.getRowNode(cell.rowIndex);
    rowNode.setData(rowNode.data);
  }

  update(cell: any, event: any): void {
    this.dataList[cell.rowIndex][cell.column.colId] = event.target.value;
    this.totalAmtCalc();
    if (cell.column.colId === "nonMerchAmt") {
      this.dataList[cell.rowIndex]["nonMerchAmt"] = event.target.value;
    }
    let rowNode: RowNode = this.gridApi.getRowNode(cell.rowIndex);
    rowNode.setData(rowNode.data);
  }

  delete(cell: any): void {
    this.gridApi.updateRowData({ remove: [cell.data] });
    cell.data.operation = "D";
    this.deletedRows.push(cell.data);
    this.dataList.splice(cell.rowIndex, 1);
    this.newModel();
  }

  totalAmtCalc(): void {
    var totalCount = 0;
    this.dataList.forEach(ctrl => {
      totalCount += Number(ctrl.nonMerchAmt);
    });
    this.infoGroup.controls["totalValue"].setValue(totalCount);
    this.totalCount = this.infoGroup.controls["totalValue"].value;
  }
}
