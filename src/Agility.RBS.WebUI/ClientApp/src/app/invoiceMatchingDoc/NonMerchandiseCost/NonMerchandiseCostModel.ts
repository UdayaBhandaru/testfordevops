export class NonMerchandiseCostModel {
  docId: number;
  nonMerchCode: string;
  nonMerchCodeDesc: string;    
  nonMerchAmt: number;
  vatCode: string;    
  vatRate: number;
  operation: string;
  totalValue: string;
}
