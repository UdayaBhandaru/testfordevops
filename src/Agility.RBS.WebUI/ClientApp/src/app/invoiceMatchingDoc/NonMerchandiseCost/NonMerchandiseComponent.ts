import { Component } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular/main";
import { HttpHeaders, HttpParams, HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";

@Component({
  selector: "file-cell",
  template: `<div fxLayout="row" class="grid-actions">
       <a href="#" (click)="invokeParentMethod($event)"><mat-icon class="edit" matTooltip="Edit">
        </mat-icon>
        <span>({{documentCount}})</span></a>         
        </div>
        <style>
          .grid-actions a{
          position:relative;
          }
          .grid-actions a span{
          position: absolute;
              left: 22px;
              top: -3px;
              color: #23a89f;
          }
        .grid-actions a mat-icon {
          background: url('../../../assets/images/grid-action-icons.png') no-repeat center;
          width: 16px;
          height: 16px;
          display: block; }
          div.grid-actions a mat-icon.edit {
            background-position: 0 0px; }
          div.grid-actions a mat-icon.delete {
            background-position: 0 -32px; }
          div.grid-actions a mat-icon.view {
            position: absolute;
            left: 40px; }
</style>`
})
export class NonMerchandiseComponent implements ICellRendererAngularComp {
  public params: any;
  public documentCount: number = 0;
  public module: string;
  constructor(private httpClient: HttpClient) {

  }
  agInit(params: any): void {
    this.params = params;
    this.documentCount = this.params.value;
    this.module = this.params.data.moduleName;
  }

  refresh(params: any): boolean {
    return true;
  }

  public invokeParentMethod($event: MouseEvent): void {
    $event.preventDefault();
    this.params.context.componentParent.onDocumentsClick(this.params);
  }  
}
