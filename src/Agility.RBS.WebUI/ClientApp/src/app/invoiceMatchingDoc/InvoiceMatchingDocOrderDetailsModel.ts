export class InvoiceMatchingDocOrderDetailsModel {
  orderNo: number;
  supllier: number;
  supName: string;
  vendorTypeDesc: string;
  terms: string;
  locationType: string;
  locactionTypeDesc: string;
  location: number;
  locationName: string;
  currencyCode: string;
  exchangeRate: number;
}
