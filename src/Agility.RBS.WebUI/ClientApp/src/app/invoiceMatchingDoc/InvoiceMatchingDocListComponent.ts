import { Component, OnInit, ViewChild, OnDestroy, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { Subscription } from "rxjs";
import { GridOptions, GridReadyEvent, IServerSideDatasource, IServerSideGetRowsParams, ColDef } from "ag-grid-community";
import { InvoiceMatchingDocListService } from './InvoiceMatchingDocListService';
import { InvoiceMatchingDocSearchModel } from './InvoiceMatchingDocSearchModel';
import { DomainDetailModel } from "../domain/DomainDetailModel";
import { SearchComponent } from '../Common/SearchComponent';
import { GridExportModel } from '../Common/grid/GridExportModel';
import { SharedService } from '../Common/SharedService';
import { GridEditComponent } from '../Common/grid/GridEditComponent';
import { InfoComponent } from '../Common/InfoComponent';
import { GridDateComponent } from '../Common/grid/GridDateComponent';

@Component({
  selector: "obligation-list",
  templateUrl: "./InvoiceMatchingDocListComponent.html"
})

export class InvoiceMatchingDocListComponent implements OnInit, IServerSideDatasource, OnDestroy {
  public sheetName: string = "InvoiceMatchingDoc List";
  PageTitle = "Invoice MatchingDoc List";
  redirectUrl = "invoiceMatchingDocument/New/";
  componentName: any;
  additionalSearchParams: any;
  loadedPage: boolean;
  searchServiceSubscription: Subscription;
  partenerDetailsServiceSubscription: Subscription;

  statusData: DomainDetailModel[];
  apiUrl: string;
  pForm: any;

  serviceDocument: ServiceDocument<InvoiceMatchingDocSearchModel>;
  gridOptions: GridOptions;
  columns: any[];
  model: InvoiceMatchingDocSearchModel = {
    groupId: null, createDate: null, createId: null, status: null, statusDesc: null, controlRecCount: null, calcCount: null, varrianceCount: null, controlTotalCost: null, calcCost: null, varrianceCost: null, currencyCode: null, lastUpdateId: null, lastUpdateDateTime: null, operation: null, tableName: null
  };

  showSearchCriteria: boolean = true;  
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;

  constructor(public service: InvoiceMatchingDocListService, private sharedService: SharedService, private router: Router
    , private changeRef: ChangeDetectorRef) {
  }

  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }

  ngOnInit(): void {
    this.service.newModel(this.model);
    this.componentName = this;
    this.columns = [
      { colId: "GROUP_ID", headerName: "Group Id", field: "groupId", tooltipField: "groupId", sort: "desc", width: 150 },
      { colId: "ENTRY_DATE", headerName: "Entry Date", field: "createDate", tooltipField: "createDate", cellRendererFramework: GridDateComponent, width: 150 },
      //{ colId: "STATUS", headerName: "Status", field: "statusDesc", tooltipField: "statusDesc", width: 150 },
      { colId: "CONTROL_TOTAL", headerName: "Control Total", field: "controlTotalCost", tooltipField: "controlTotalCost", width: 180  },
      { colId: "CALCULATED_TOTAL", headerName: "Calculated Total", field: "calcCost", tooltipField: "calcCost", width: 180  },
      { colId: "VARIANCE_TOTAL", headerName: "Variance Total", field: "varrianceCost", tooltipField: "varrianceCost", width: 180  },
      { colId: "CONTROL_COUNT", headerName: "Control Count", field: "controlRecCount", tooltipField: "controlRecCount", width: 180  },
      { colId: "CALCULATED_COUNT", headerName: "Calculated Count", field: "calcCount", tooltipField: "calcCount", width: 180  },
      { colId: "VARRIANCE_COUNT", headerName: "Variance Count", field: "varrianceCount", tooltipField: "varrianceCount", width: 180 },
      { colId: "CURRENCY_CODE", headerName: "Currency Code", field: "currencyCode", tooltipField: "currencyCode", width: 180 },
      //{ colId: "CREATE_ID", headerName: "Create Id", field: "createId", tooltipField: "createId", width: 180 },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 150
      }
    ];

    this.gridOptions = {
      onGridReady: (params: GridReadyEvent) => {
        params.api.setEnterpriseDatasource(this);
      },
      context: {
        componentParent: this
      },
      suppressRowClickSelection: true,
      suppressCellSelection: true,
      rowModelType: "enterprise",
      paginationPageSize: this.service.orderPaging.pageSize,
      enableServerSideSorting: true,
      enableServerSideFilter: true,
      cacheBlockSize: this.service.orderPaging.cacheSize,
    };    

    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }

    this.serviceDocument = this.service.serviceDocument;
    
  }

  getRows(params: IServerSideGetRowsParams): void {
    this.additionalSearchParams = { startRow: params.request.startRow, endRow: params.request.endRow, sortModel: params.request.sortModel };
    if (!this.loadedPage) {
      this.loadedPage = true;
      params.successCallback(
        this.serviceDocument.dataProfile.dataList.slice(params.request.startRow, params.request.endRow),
        this.service.serviceDocument.dataProfile.dataModel.totalRows);
    } else {
      this.searchServiceSubscription = this.service.search(this.additionalSearchParams).subscribe((sDoc) => {
        params.successCallback(sDoc.dataProfile.dataList, this.service.serviceDocument.dataProfile.dataModel.totalRows);
      });
    }
  }

  showSearchCriteriaChild(event: any): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
      this.sharedService.previousUrl = "../invoiceMatchingDocument/List";
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl + cell.data.groupId
      , this.service.serviceDocument.dataProfile.dataList);
  }
  addValidation(): void {
    this.navigateToAdd();
  }

  private navigateToAdd(): void {
    this.router.navigate(["/" + this.redirectUrl + "0"], { skipLocationChange: true });
    this.sharedService.previousUrl = "../invoiceMatchingDocument/List";
  }

  ngOnDestroy(): void {
    if (this.searchServiceSubscription) {
      this.searchServiceSubscription.unsubscribe();
    }
  }  
}
