import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { InvoiceMatchingDocListComponent } from './InvoiceMatchingDocListComponent'
import { InvoiceMatchingDocListResolver } from './InvoiceMatchingDocListResolver'
import { InvoiceMatchingDocComponent } from './InvoiceMatchingDocComponent';
import { InvoiceMatchingDocResolver } from './InvoiceMatchingDocResolver';

const OrderRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "", redirectTo: "List", pathMatch: "full"
      },
      {
        path: "List",
        component: InvoiceMatchingDocListComponent,
        resolve:
        {
          references: InvoiceMatchingDocListResolver
        }
      },
      {
        path: "New/:id",
        component: InvoiceMatchingDocComponent,
        resolve:
        {
          references: InvoiceMatchingDocResolver
        }
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(OrderRoutes)
  ]
})
export class InvoiceMatchingDocRoutingModule { }
