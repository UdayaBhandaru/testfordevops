import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { InvoiceMatchingDocService } from './InvoiceMatchingDocService';
import { InvoiceMatchingDocGroupHeadModel } from './InvoiceMatchingDocGroupHeadModel';

@Injectable()
export class InvoiceMatchingDocResolver implements Resolve<ServiceDocument<InvoiceMatchingDocGroupHeadModel>> {
  constructor(private service: InvoiceMatchingDocService) { }
  resolve(route: ActivatedRouteSnapshot): Observable<ServiceDocument<InvoiceMatchingDocGroupHeadModel>> {
    return this.service.addEdit(route.params["id"]);
  }
}
