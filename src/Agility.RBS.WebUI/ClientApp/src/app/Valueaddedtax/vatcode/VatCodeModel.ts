export class VatCodeModel {
    vatCode?: string;
    vatCodeDesc?: string;
    priceVatInclusive?: string;
    priceVatInclusiveDesc?: string;
    vatRateOn?: string;
    vatRateOnDesc?: string;
    vatStatus?: string;
    vatStatusDesc?:string;
    operation?: string;
    tableName?: string;
}
