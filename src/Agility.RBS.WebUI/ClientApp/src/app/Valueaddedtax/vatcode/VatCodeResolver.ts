﻿import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { VatCodeService } from "./VatCodeService";
import { VatCodeModel } from "./VatCodeModel";
import { ServiceDocument } from "@agility/frameworkcore";
@Injectable()
export class VatCodeResolver implements Resolve<ServiceDocument<VatCodeModel>> {
    constructor(private service: VatCodeService) { }
    resolve(): Observable<ServiceDocument<VatCodeModel>> {
        return this.service.list();
    }
}

