import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { VatCodeModel } from "./VatCodeModel";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";

@Injectable()
export class VatCodeService {
    searchData: any;
    serviceDocument: ServiceDocument<VatCodeModel> = new ServiceDocument<VatCodeModel>();

    constructor(private httpHelperService: HttpHelperService) { }

    newModel(model: VatCodeModel): ServiceDocument<VatCodeModel> {
        return this.serviceDocument.newModel(model);
    }

    search(): Observable<ServiceDocument<VatCodeModel>> {
        return this.serviceDocument.search("/api/VatCode/Search");
    }

    save(): Observable<ServiceDocument<VatCodeModel>> {
        return this.serviceDocument.save("/api/VatCode/Save",true);
    }

    list(): Observable<ServiceDocument<VatCodeModel>> {
        return this.serviceDocument.list("/api/VatCode/List");
    }

  isExistingVatCode(vatCode: string): Observable<boolean> {
    return this.httpHelperService.get("/api/VatCode/IsExistingVatCode", new HttpParams().set("vatCode", vatCode));
    }
}
