﻿import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { VatCodeListComponent } from "./VatCodeListComponent";
import { VatCodeComponent } from "./VatCodeComponent";
import { VatCodeResolver } from "./VatCodeResolver";
const UtilityRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: VatCodeListComponent,
                resolve:
                {
                    serviceDocument: VatCodeResolver
                }
            },
            {
                path: "New",
                component: VatCodeComponent,
                resolve:
                {
                    serviceDocument: VatCodeResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(UtilityRoutes)
    ]
})
export class VatCodeRoutingModule { }
