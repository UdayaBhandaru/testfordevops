import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { VatCodeModel } from "./VatCodeModel";
import { VatRateModel } from "../vatrates/VatRateModel";
import { VatCodeService } from "./VatCodeService";
import { SharedService } from "../../Common/SharedService";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { FormGroup } from "@angular/forms";
import { VatRegionModel } from '../vatregion/VatRegionModel';

@Component({
  selector: "vat-code",
  templateUrl: "./VatCodeComponent.html"
})

export class VatCodeComponent implements OnInit {
  PageTitle = "VAT Code";
  serviceDocument: ServiceDocument<VatCodeModel>;
  priceVatInclusiveDtlData: DomainDetailModel[];
  vatStatusDtlData: DomainDetailModel[];
  vatRateOnTypeData: DomainDetailModel[];
  editMode: boolean = false;
  editModel: any;
  localizationData: any;
  model: VatCodeModel;
  defaultStatus: string;
  defaultIndicator: string;
  defaultCalcType: string;
  vatRatemodel: VatRateModel;
  vatRegionModel: VatRegionModel;

  constructor(private service: VatCodeService, private sharedService: SharedService, private router: Router
    , private ref: ChangeDetectorRef) {
  }

  ngAfterViewChecked(): void {
    this.ref.detectChanges();
  }

  ngOnInit(): void {
    this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
    this.bind();
  }

  bind(): void {
    delete this.editModel;
    if (this.sharedService.editObject) {
      this.editMode = true;
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
      this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
      delete this.sharedService.editObject;
    } else {
      this.model = {
        vatCode: null, vatCodeDesc: null, priceVatInclusive: null, vatRateOn: null,
        vatStatus: null, operation: "I"
      };
      this.service.serviceDocument.dataProfile.dataModel = this.model;
    }
    this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    this.serviceDocument = this.service.serviceDocument;
  }

  saveAndContinue(): void {
    this.save(false);
  }

  save(saveOnly: boolean): void {
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.saveSubscription(saveOnly);
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  saveSubscription(saveOnly: boolean): void {
    this.checkVatCode(saveOnly);
  }
  saveVatcode(saveOnly: boolean): void {
    this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.sharedService.saveForm(this.localizationData.vatCode.vatcodesave).subscribe(() => {
          if (saveOnly) {
            this.router.navigate(["/VatCode/List"], { skipLocationChange: true });
          } else {
            if (!this.editMode) {
              this.model = {
                vatCode: null, priceVatInclusive: this.defaultIndicator, vatRateOn: this.defaultCalcType
                , vatStatus: this.defaultStatus, operation: "I"
              };
              this.service.serviceDocument.dataProfile.dataModel = this.model;
            } else {
              Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
              this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.editModel);
            }
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
          }
        }, (error: string) => { console.log(error); });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
    }, (error: string) => {
      console.log(error);
      this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
    });
  }

  reset(): void {
    if (!this.editMode) {
      this.model = {
        vatCode: null, priceVatInclusive: this.defaultIndicator,
        vatRateOn: this.defaultCalcType, vatStatus: this.defaultStatus, operation: "I"
      };
      this.service.newModel(this.model);
    } else {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    }
    this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
  }

  checkVatCode(saveOnly: boolean): void {
    if (!this.editMode) {
      var form: FormGroup = this.serviceDocument.dataProfile.profileForm;
      if (form.controls["vatCode"].value) {
        this.service.isExistingVatCode(this.serviceDocument.dataProfile.profileForm.controls["vatCode"].value)
          .subscribe((response: boolean) => {
            if (response) {
              this.sharedService.errorForm(this.localizationData.vatCode.vatcodecheck);
            } else {
              this.saveVatcode(saveOnly);
            }
          });
      }
    } else {
      this.saveVatcode(saveOnly);
    }
  }

  openVatRates($event: MouseEvent): void {
    $event.preventDefault();
    this.vatRatemodel = {
      vatCode: this.service.serviceDocument.dataProfile.dataModel.vatCode,
      activeDate: null,
      createDate: null,
      createId: null,
      effectiveStartDate: null, effectiveEndDate: null,
      vatRate: null
    };
    this.sharedService.searchData = Object.assign({}, this.vatRatemodel);
    this.router.navigate(["/vatrates"], { skipLocationChange: true });
  }
  openVatRegion($event: MouseEvent): void {
    $event.preventDefault();
    this.vatRegionModel = {
      vatRegionCode: null,
      vatRegionType: null,
      vatRegion: null,
      vatRegionName: null,
      vatRegionStatus: null, vatRegionStatusDesc: null
    };
    this.sharedService.searchData = Object.assign({}, this.vatRatemodel);
    this.router.navigate(["/vatrates"], { skipLocationChange: true });
  }
}
