﻿import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { VatCodeRoutingModule } from "./VatCodeRoutingModule";
import { VatCodeListComponent } from "./VatCodeListComponent";
import { VatCodeComponent } from "./VatCodeComponent";
import { VatCodeService } from "./VatCodeService";
import { VatCodeResolver } from "./VatCodeResolver";
import { AgGridModule } from "ag-grid-angular/main";
import { LicenseManager } from "ag-grid-enterprise/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { RbsSharedModule } from "../../RbsSharedModule";
@NgModule({
    imports: [
        FrameworkCoreModule,
        VatCodeRoutingModule,
        AgGridModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        VatCodeListComponent,
        VatCodeComponent
    ],
    providers: [
        VatCodeService,
        VatCodeResolver
    ]
})
export class VatCodeModule {
    constructor() {
        LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
    }
}
