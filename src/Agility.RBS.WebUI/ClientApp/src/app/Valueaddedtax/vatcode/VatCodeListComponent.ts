import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { VatCodeModel } from "./VatCodeModel";
import { VatCodeService } from "./VatCodeService";
import { SharedService } from "../../Common/SharedService";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { GridEditComponent } from "../../Common/grid/GridEditComponent";
import { GridExportModel } from "../../Common/grid/GridExportModel";
import { InfoComponent } from "../../Common/InfoComponent";
import { GridOptions } from 'ag-grid-community';

@Component({
  selector: "vat-code-list",
  templateUrl: "./VatCodeListComponent.html"
})
export class VatCodeListComponent implements OnInit {
  public serviceDocument: ServiceDocument<VatCodeModel>;
  PageTitle = "Vat Code";
  redirectUrl = "VatCode";
  componentName: any;
  columns: any[];
  domainDtlData: DomainDetailModel[];
  model: VatCodeModel;
  showSearchCriteria: boolean = true;
  public gridOptions: GridOptions;

  constructor(public service: VatCodeService, private sharedService: SharedService, private router: Router
    , private ref: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "VAT CODE", field: "vatCode", tooltipField: "vatCode" },
      { headerName: "VAT CODE DESC", field: "vatCodeDesc", tooltipField: "vatCodeDesc" },
      {
        headerName: "ACTIONS", field: "Actions", cellRendererFramework: GridEditComponent, width: 80
      }
    ];
    this.domainDtlData = this.service.serviceDocument.domainData["status"];
    this.sharedService.domainData = { status: this.domainDtlData };
    this.model = { vatCode: null, vatStatus: null,vatCodeDesc:null};
    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
      delete this.service.searchData;
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }

  ngAfterViewChecked(): void {
    this.ref.detectChanges();
  }
}
