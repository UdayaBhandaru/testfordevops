import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { VatRegionModel } from "./VatRegionModel";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";

@Injectable()
export class VatRegionService {
    searchData: any;
    serviceDocument: ServiceDocument<VatRegionModel> = new ServiceDocument<VatRegionModel>();

    constructor(private httpHelperService: HttpHelperService) { }

    newModel(model: VatRegionModel): ServiceDocument<VatRegionModel> {
        return this.serviceDocument.newModel(model);
    }

    search(): Observable<ServiceDocument<VatRegionModel>> {
        return this.serviceDocument.search("/api/vatregion/Search");
    }

    save(): Observable<ServiceDocument<VatRegionModel>> {
        return this.serviceDocument.save("/api/vatregion/Save", true);
    }

    list(): Observable<ServiceDocument<VatRegionModel>> {
        return this.serviceDocument.list("/api/vatregion/List");
    }

  isExistingVatRegion(vatRegionName: string): Observable<boolean> {
    return this.httpHelperService.get("/api/vatregion/isExistingVatRegionCode", new HttpParams().set("vatRegionName", vatRegionName));
    }
}
