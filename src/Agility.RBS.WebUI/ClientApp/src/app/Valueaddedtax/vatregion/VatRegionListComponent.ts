import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { VatRegionModel } from "./VatRegionModel";
import { VatRegionService } from "./VatRegionService";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { GridEditComponent } from "../../Common/grid/GridEditComponent";
import { GridExportModel } from "../../Common/grid/GridExportModel";
import { InfoComponent } from "../../Common/InfoComponent";
import { SharedService } from "../../Common/SharedService";
import { GridOptions } from 'ag-grid-community';

@Component({
  selector: "vat-region-list",
  templateUrl: "./VatRegionListComponent.html"
})
export class VatRegionListComponent implements OnInit {
  public serviceDocument: ServiceDocument<VatRegionModel>;
  public gridOptions: GridOptions;
  PageTitle = "Vat Region";
  redirectUrl = "VatRegion";
  componentName: any;
  columns: any[];
  domainDtlData: DomainDetailModel[];
  companyDomainDtlData: DomainDetailModel[];
  model: VatRegionModel;
  showSearchCriteria: boolean = true;
  sIndex: number;

  constructor(public service: VatRegionService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "VatRegion", field: "vatRegion", width: 50, tooltipField: "vatRegion" },
      { headerName: "NAME", field: "vatRegionName", tooltipField: "vatRegionName" },
      { headerName: "Type", field: "vatRegionType", width: 25, tooltipField: "vatRegionType" },
      {
        headerName: "ACTIONS", field: "Actions", cellRendererFramework: GridEditComponent, width: 30
      }
    ];
    this.domainDtlData = this.service.serviceDocument.domainData["status"];
    this.companyDomainDtlData = this.service.serviceDocument.domainData["company"];
    this.sharedService.domainData = { status: this.domainDtlData };
    this.model = { vatRegionCode: null, vatRegionName: null, vatRegionStatus: null,vatRegion:null,vatRegionType:null };
    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
      delete this.service.searchData;
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, "VatRegion", this.service.serviceDocument.dataProfile.dataList);
  }
}
