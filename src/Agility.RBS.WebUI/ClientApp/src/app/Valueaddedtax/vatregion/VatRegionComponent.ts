import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { VatRegionModel } from "./VatRegionModel";
import { VatRegionService } from "./VatRegionService";
import { VatRateModel } from "../vatrates/VatRateModel";
import { SharedService } from "../../Common/SharedService";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { FormGroup } from "@angular/forms";
import { VatRegionTypeModel } from '../../Common/DomainData/VatRegionTypeModel';
import { VatCodeModel } from '../vatcode/VatCodeModel';

@Component({
  selector: "vat-region",
  templateUrl: "./VatRegionComponent.html"
})

export class VatRegionComponent implements OnInit {
  PageTitle = "VAT Region";
  serviceDocument: ServiceDocument<VatRegionModel>;
  vatRegionTypeData: VatRegionTypeModel[];
  editMode: boolean = false;
  editModel: any;
  localizationData: any;
  model: VatRegionModel;
  redirectUrl = "VatRegion";
  vatRateModel: VatRateModel;
  vatCodeModel: VatCodeModel;
  constructor(private service: VatRegionService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.vatRegionTypeData = Object.assign([], this.service.serviceDocument.domainData["vatRegionType"]);
    this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
    this.bind();
  }

  save(saveOnly: boolean): void {
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.saveSubscription(saveOnly);
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  saveAndContinue(): void {
    this.save(false);
  }
  reset(): void {
    if (!this.editMode) {
      this.model = { vatRegionCode: null, vatRegionName: null, vatRegionStatus: "A", operation: "I",vatRegion:null,vatRegionType:null };
      this.service.serviceDocument.dataProfile.dataModel = this.model;
      this.service.newModel(this.model);
    } else {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    }
    this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
  }

  bind(): void {
    delete this.editModel;
    if (this.sharedService.editObject) {
      this.editMode = true;
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
      this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
      delete this.sharedService.editObject;
    } else {
      this.model = { vatRegionCode: null, vatRegionName: null, vatRegionStatus: "A", operation: "I", vatRegion: null, vatRegionType: null };
      this.service.serviceDocument.dataProfile.dataModel = this.model;
    }
    this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    this.serviceDocument = this.service.serviceDocument;
  }  

  checkVatRegionCode(saveOnly: boolean): void {
    if (!this.editMode) {
      let form: FormGroup = this.serviceDocument.dataProfile.profileForm;
      if (form.controls["vatRegionName"].value) {
        this.service.isExistingVatRegion(this.serviceDocument.dataProfile.profileForm.controls["vatRegionName"].value).subscribe
          ((response: boolean) => {
            if (response) {
              this.sharedService.errorForm(this.localizationData.vatRegion.vatregioncheck);
            } else {
              this.saveVatRegion(saveOnly);
            }
          });
      }
    } else {
      this.saveVatRegion(saveOnly);
    }
  }

  saveSubscription(saveOnly: boolean): void {
    this.checkVatRegionCode(saveOnly);
  }

  saveVatRegion(saveOnly: boolean): void {
    this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.sharedService.saveForm(this.localizationData.vatRegion.vatregionsave).subscribe(() => {
          if (saveOnly) {
            this.router.navigate(["/" + this.redirectUrl + "/List"], { skipLocationChange: true });
          } else {
            if (!this.editMode) {
              this.model = { vatRegionCode: null, vatRegionName: null, vatRegionStatus: "A", operation: "I", vatRegion: null, vatRegionType: null };
              this.service.newModel(this.model);
            } else {
              Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
              this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.service.serviceDocument.dataProfile.dataModel);
              this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            }
          }
        }, (error: string) => { console.log(error); });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
    }, (error: string) => { console.log(error); });
  }

  openVatCodes($event: MouseEvent): void {
    $event.preventDefault();
    this.vatCodeModel = {
      vatCode: null,
      vatCodeDesc: null,
      priceVatInclusive: null,
      priceVatInclusiveDesc: null,
      vatRateOn: null, vatRateOnDesc: null,
      vatStatus: null,
      vatStatusDesc: null
    };
    this.sharedService.searchData = Object.assign({}, this.vatCodeModel);
    this.router.navigate(["/VatCode"], { skipLocationChange: true });
  }
}
