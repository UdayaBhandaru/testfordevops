﻿import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { VatRegionRoutingModule } from "./VatRegionRoutingModule";
import { VatRegionResolver } from "./VatRegionResolver";
import { AgGridModule } from "ag-grid-angular/main";
import { LicenseManager } from "ag-grid-enterprise/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { VatRegionComponent } from "./VatRegionComponent";
import { VatRegionListComponent } from "./VatRegionListComponent";
import { VatRegionService } from "./VatRegionService";
import { RbsSharedModule } from "../../RbsSharedModule";

@NgModule({
    imports: [
        FrameworkCoreModule,
        VatRegionRoutingModule,
        AgGridModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        VatRegionComponent,
        VatRegionListComponent
    ],
    providers: [
        VatRegionService,
        VatRegionResolver
    ]
})
export class VatRegionModule {
    constructor() {
        LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
    }
}
