﻿import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { VatRegionModel } from "./VatRegionModel";
import { VatRegionService } from "./VatRegionService";
@Injectable()
export class VatRegionResolver implements Resolve<ServiceDocument<VatRegionModel>> {
    constructor(private service: VatRegionService) { }
    resolve(): Observable<ServiceDocument<VatRegionModel>> {
        return this.service.list();
    }
}
