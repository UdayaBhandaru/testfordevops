﻿import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { VatRegionResolver} from "./VatRegionResolver";
import { VatRegionListComponent } from "./VatRegionListComponent";
import { VatRegionComponent } from "./VatRegionComponent";
const UtilityRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: VatRegionListComponent,
                resolve:
                {
                    serviceDocument: VatRegionResolver
                }
            },
            {
                path: "New",
                component: VatRegionComponent,
                resolve:
                {
                    serviceDocument: VatRegionResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(UtilityRoutes)
    ]
})
export class VatRegionRoutingModule { }
