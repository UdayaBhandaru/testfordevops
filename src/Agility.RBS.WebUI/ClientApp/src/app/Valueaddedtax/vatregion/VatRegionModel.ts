export class VatRegionModel {
    vatRegionCode: string;
    vatRegionType: string;
    vatRegion?: number;
    vatRegionName: string;
    vatRegionStatus: string;
    vatRegionStatusDesc?:string;
    operation?: string;
}
