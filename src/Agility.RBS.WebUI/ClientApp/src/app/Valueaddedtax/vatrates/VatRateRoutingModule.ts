﻿import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { VatRateListComponent } from "./VatRateListComponent";
import { VatRateComponent } from "./VatRateComponent";
import { VatRatesResolver} from "./VatRateResolver";
const UtilityRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: VatRateListComponent,
                resolve:
                {
                    serviceDocument: VatRatesResolver
                }
            },
            {
                path: "New",
                component: VatRateComponent,
                resolve:
                {
                    serviceDocument: VatRatesResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(UtilityRoutes)
    ]
})
export class VatRateRoutingModule { }
