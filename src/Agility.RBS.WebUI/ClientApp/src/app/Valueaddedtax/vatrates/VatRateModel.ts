export class VatRateModel {
   vatCode: string;
   activeDate?: Date;
   createDate?: Date;
   createId?: string;
   effectiveStartDate: Date;
   effectiveEndDate: Date;
   vatRate: number;
   operation?: string;
}
