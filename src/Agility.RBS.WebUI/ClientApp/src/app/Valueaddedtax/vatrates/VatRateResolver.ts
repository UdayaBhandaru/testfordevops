﻿import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { VatRateModel } from "./VatRateModel";
import { VatRatesService } from "./VatRatesService";
@Injectable()
export class VatRatesResolver implements Resolve<ServiceDocument<VatRateModel>> {
    constructor(private service: VatRatesService) { }
    resolve(): Observable<ServiceDocument<VatRateModel>> {
        return this.service.list();
    }
}
