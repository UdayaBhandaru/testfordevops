﻿import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { VatRateRoutingModule } from "./VatRateRoutingModule";
import { VatRatesResolver } from "./VatRateResolver";
import { AgGridModule } from "ag-grid-angular/main";
import { LicenseManager } from "ag-grid-enterprise/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { VatRateComponent } from "./VatRateComponent";
import { VatRateListComponent } from "./VatRateListComponent";
import { VatRatesService } from "./VatRatesService";
import { RbsSharedModule } from "../../RbsSharedModule";
@NgModule({
    imports: [
        FrameworkCoreModule,
        VatRateRoutingModule,
        AgGridModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        VatRateComponent,
        VatRateListComponent
    ],
    providers: [
        VatRatesService,
        VatRatesResolver
    ]
})
export class VatRateModule {
    constructor() {
        LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
    }
}
