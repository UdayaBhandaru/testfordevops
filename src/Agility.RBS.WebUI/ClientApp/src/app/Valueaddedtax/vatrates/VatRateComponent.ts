import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { VatRateModel } from "./VatRateModel";
import { VatRegionModel } from "../vatregion/VatRegionModel";
import { VatRatesService } from "./VatRatesService";
import { SharedService } from "../../Common/SharedService";
import { VatCodeModel } from "../../valueaddedtax/vatcode/VatCodeModel";
import { FormGroup } from "@angular/forms";
import { DomainDetailModel } from '../../domain/DomainDetailModel';

@Component({
  selector: "vat-rates",
  templateUrl: "./VatRateComponent.html"
})

export class VatRateComponent implements OnInit {
  PageTitle = "VAT Rates";
  vatRates: string = "vatrates";
  serviceDocument: ServiceDocument<VatRateModel>;
  vatCodes: VatCodeModel[];
  domainDtlData: DomainDetailModel[];
  editMode: boolean = false;
  editModel: any;
  localizationData: any;
  model: VatRateModel;
  redirectUrl: string = "vatrates";
  isStartDateDisabled: boolean = false;
  vatCodeModel: VatCodeModel;
  constructor(private service: VatRatesService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.domainDtlData = Object.assign([], this.service.serviceDocument.domainData["vatCode"]);
    this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
    this.bind();
  }

  save(saveOnly: boolean): void {
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      if (Date.parse(this.serviceDocument.dataProfile.profileForm.controls["activeDate"].value)
        > Date.parse(this.serviceDocument.dataProfile.profileForm.controls["createDate"].value)) {
        this.sharedService.errorForm("Active date can not be more then Created date");
        return;
      }
      if (this.serviceDocument.dataProfile.profileForm.controls["createDate"].value === null
        || this.serviceDocument.dataProfile.profileForm.controls["createDate"].value === "") {
        let date: Date = new Date(9999, 11, 31);
        this.serviceDocument.dataProfile.profileForm.controls["createDate"].setValue(date);
      }
      this.saveSubscription(saveOnly);
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  saveAndContinue(): void {
    this.save(false);
  }

  reset(): void {
    if (!this.editMode) {
      this.model = { effectiveEndDate: null, effectiveStartDate: null, vatCode: null, vatRate: null, operation: "I", createDate: null, activeDate: null, createId: null };
      this.service.serviceDocument.dataProfile.dataModel = this.model;
      this.service.newModel(this.model);
    } else {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    }
    this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
  }

  bind(): void {
    delete this.editModel;
    if (this.sharedService.editObject) {
      this.editMode = true;
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
      this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
      delete this.sharedService.editObject;
    } else {
      this.model = { effectiveEndDate: null, effectiveStartDate: null, vatCode: null, vatRate: null, operation: "I", createDate:null,activeDate:null,createId:null };
      this.service.serviceDocument.dataProfile.dataModel = this.model;
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  saveSubscription(saveOnly: boolean): void {
    this.checkVatRate(saveOnly);
  }

  saveVatRate(saveOnly: boolean): void {
    this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.sharedService.saveForm(this.localizationData.vatRates.vatratessave).subscribe(() => {
          if (saveOnly) {
            this.router.navigate(["/" + this.redirectUrl + "/List"], { skipLocationChange: true });
          } else {
            if (!this.editMode) {
              this.model = { effectiveEndDate: null, effectiveStartDate: null, vatCode: null, vatRate: null, operation: "I", createDate: null, activeDate: null, createId: null };
              this.service.newModel(this.model);
            } else {
              Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
              this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.service.serviceDocument.dataProfile.dataModel);
              this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            }
          }
        }, (error: string) => { console.log(error); });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
    }, (error: string) => { console.log(error); });
  }

  checkVatRate(saveOnly: boolean): void {
    if (!this.editMode) {
      let form: FormGroup = this.serviceDocument.dataProfile.profileForm;
      if (form.controls["vatCode"].value && form.controls["vatRate"].value) {
        this.service.isExistingVatRate(this.serviceDocument.dataProfile.profileForm.controls["vatCode"].value
          , this.serviceDocument.dataProfile.profileForm.controls["vatRate"].value)
          .subscribe((response: boolean) => {
            if (response) {
              this.sharedService.errorForm(this.localizationData.vatRates.vatratescheck);
            } else {
              this.saveVatRate(saveOnly);
            }
          });
      }
    } else {
      this.saveVatRate(saveOnly);
    }
  }

  public vatCodeChanged(): void {
    if (!this.editMode) {
      this.service.getMaxEffectDate(this.serviceDocument.dataProfile.profileForm.controls["vatCode"].value).subscribe
        ((response: any) => {
          if (response) {
            this.serviceDocument.dataProfile.profileForm.controls["activeDate"].setValue(response);
            this.isStartDateDisabled = true;
          } else {
            this.isStartDateDisabled = false;
            this.serviceDocument.dataProfile.profileForm.controls["activeDate"].setValue("");
          }
        });
    }
  }

  openVatCode($event: MouseEvent): void {
    $event.preventDefault();
    this.vatCodeModel = {
      vatCode: this.service.serviceDocument.dataProfile.dataModel.vatCode,
      vatCodeDesc: null,
      priceVatInclusive: null,
      priceVatInclusiveDesc: null,
      vatRateOn: null, vatRateOnDesc: null,
      vatStatus: null,
      vatStatusDesc: null
    };
    this.sharedService.searchData = Object.assign({}, this.vatCodeModel);
    this.router.navigate(["/VatCode"], { skipLocationChange: true });
  }
}
