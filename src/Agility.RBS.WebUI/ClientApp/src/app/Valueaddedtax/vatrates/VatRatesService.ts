﻿import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { VatRateModel } from "./VatRateModel";
import { SharedService } from "../../Common/SharedService";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";

@Injectable()
export class VatRatesService {
    searchData: any;
    serviceDocument: ServiceDocument<VatRateModel> = new ServiceDocument<VatRateModel>();

    constructor(private httpHelperService: HttpHelperService, private sharedService: SharedService) { }

    newModel(model: VatRateModel): ServiceDocument<VatRateModel> {
        return this.serviceDocument.newModel(model);
    }

    search(): Observable<ServiceDocument<VatRateModel>> {
        return this.serviceDocument.search("/api/vatrate/Search");
    }

    save(): Observable<ServiceDocument<VatRateModel>> {
        return this.serviceDocument.save("/api/vatrate/Save", true, () => this.setDate());
    }

    list(): Observable<ServiceDocument<VatRateModel>> {
        return this.serviceDocument.list("/api/vatrate/List");
    }

    getMaxEffectDate(code: string): Observable<Date> {
        return this.httpHelperService.get("/api/vatrate/MaxEffectDate", new HttpParams().set("pVatCode", code));
    }

    setDate(): void {
        let sd: VatRateModel = this.serviceDocument.dataProfile.dataModel;
        if (sd !== null) {
            if (sd.effectiveStartDate != null) {
                sd.effectiveStartDate = this.sharedService.setOffSet(sd.effectiveStartDate);
            }
            if (sd.effectiveEndDate != null) {
                sd.effectiveEndDate = this.sharedService.setOffSet(sd.effectiveEndDate);
            }
        }
    }

    isExistingVatRate(vatCode: string, vatRate: string): Observable<boolean> {
        return this.httpHelperService.get("/api/vatrate/IsExistingVatRate", new HttpParams().set("vatCode", vatCode).set("vatRate", vatRate));
    }
}