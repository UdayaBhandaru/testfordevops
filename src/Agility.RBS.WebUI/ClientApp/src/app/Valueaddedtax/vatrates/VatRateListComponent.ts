import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { VatRateModel } from "./VatRateModel";
import { VatRatesService } from "./VatRatesService";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { GridEditComponent } from "../../Common/grid/GridEditComponent";
import { GridExportModel } from "../../Common/grid/GridExportModel";
import { InfoComponent } from "../../Common/InfoComponent";
import { SharedService } from "../../Common/SharedService";
import { GridDateComponent } from "../../Common/grid/GridDateComponent";

@Component({
  selector: "vat-rate-list",
  templateUrl: "./VatRateListComponent.html"
})
export class VatRateListComponent implements OnInit {
  gridOptions: {};
  public serviceDocument: ServiceDocument<VatRateModel>;
  PageTitle = "Vat Rates";
  redirectUrl = "vatrates";
  componentName: any;
  columns: any[];
  domainDtlData: DomainDetailModel[];
  model: VatRateModel;
  showSearchCriteria: boolean = true;

  constructor(public service: VatRatesService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "VAT RATE", field: "vatRate",  tooltipField: "vatRate" },

      { headerName: "VAT CODE", field: "vatCode", tooltipField: "vatCode" },
      { headerName: "activeDate", field: "activeDate", tooltipField: "activeDate" },
      { headerName: "createDate", field: "createDate", tooltipField: "createDate" },
      {
        headerName: "ACTIONS", field: "Actions", cellRendererFramework: GridEditComponent, width: 100
      }
    ];
    this.domainDtlData = this.service.serviceDocument.domainData["vatCode"];
    this.model = { effectiveEndDate: null, effectiveStartDate: null, vatRate: null, vatCode: null };
    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
      delete this.service.searchData;
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }
}
