import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { VatRegionVatCodeModel } from "./VatRegionVatCodeModel";
import { VatRegionVatCodeService } from "./VatRegionVatCodeService";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { GridEditComponent } from "../../Common/grid/GridEditComponent";
import { GridExportModel } from "../../Common/grid/GridExportModel";
import { InfoComponent } from "../../Common/InfoComponent";
import { SharedService } from "../../Common/SharedService";
import { GridOptions } from 'ag-grid-community';

@Component({
  selector: "vatregion-vatcode-list",
  templateUrl: "./VatRegionVatCodeListComponent.html"
})

export class VatRegionVatCodeListComponent implements OnInit {
  public serviceDocument: ServiceDocument<VatRegionVatCodeModel>;
  public gridOptions: GridOptions;
  PageTitle = "VAT Region VAT Code";
  redirectUrl = "VatRegionVatCode";
  componentName: any;
  columns: any[];
  domainDtlData: DomainDetailModel[];
  vatCodesData: DomainDetailModel[];
  vatRegionCodesData: DomainDetailModel[];
  model: VatRegionVatCodeModel;
  showSearchCriteria: boolean = true;
  sIndex: number;

  constructor(public service: VatRegionVatCodeService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "VAT CODE", field: "vatCode", tooltipField: "vatCode" },
      { headerName: "VAT REGION CODE", field: "vatRegionCode", tooltipField: "vatRegionCode" },
      { headerName: "VAT REGION", field: "vatRegion", tooltipField: "vatRegion" },
      { headerName: "STATUS", field: "vatStatusDesc", width: 60, tooltipField: "vatStatusDesc" },
      {
        headerName: "ACTIONS", field: "Actions", cellRendererFramework: GridEditComponent, width: 60
      }
    ];

    this.domainDtlData = this.service.serviceDocument.domainData["status"];
    this.vatCodesData = this.service.serviceDocument.domainData["vatCode"];
    this.vatRegionCodesData = this.service.serviceDocument.domainData["vatRegionCode"];

    this.sharedService.domainData = {
      vatCode: this.vatCodesData,
      vatRegionCode: this.vatRegionCodesData,
      status: this.domainDtlData
    };
    this.model = { vatCode: null, vatRegionCode: null, vatStatus: null };
    if (this.service.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.service.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
      delete this.service.searchData;
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }
}
