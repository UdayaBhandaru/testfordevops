import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { VatRegionVatCodeModel } from "./VatRegionVatCodeModel";
import { VatRegionVatCodeService } from "./VatRegionVatCodeService";
import { VatCodeModel } from "../vatcode/VatCodeModel";
import { VatRateModel } from "../vatrates/VatRateModel";
import { VatRegionModel } from "../vatregion/VatRegionModel";
import { SharedService } from "../../Common/SharedService";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { FormGroup } from "@angular/forms";

@Component({
  selector: "vatregion-vatcode",
  templateUrl: "./VatRegionVatCodeComponent.html"
})

export class VatRegionVatCodeComponent implements OnInit {
  PageTitle = "VAT Region VAT Code";
  serviceDocument: ServiceDocument<VatRegionVatCodeModel>;
  domainDtlData: DomainDetailModel[];
  vatCodesData: VatCodeModel[];
  vatRegionCodesData: VatRegionModel[];
  vatCodeModel: VatCodeModel;
  vatRateModel: VatRateModel;
  vatRegionModel: VatRegionModel;
  editMode: boolean = false;
  editModel: any;
  defaultStatus: string;
  localizationData: any;
  model: VatRegionVatCodeModel;
  redirectURL = "VatRegionVatCode";
  constructor(private service: VatRegionVatCodeService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.domainDtlData = Object.assign([], this.service.serviceDocument.domainData["status"]);
    this.vatCodesData = Object.assign([], this.service.serviceDocument.domainData["vatCode"]);
    this.vatRegionCodesData = Object.assign([], this.service.serviceDocument.domainData["vatRegionCode"]);
    this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
    this.defaultStatus = this.domainDtlData.filter(itm => itm.requiredInd === "1")[0].code;
    this.bind();
  }

  bind(): void {
    delete this.editModel;
    if (this.sharedService.editObject) {
      this.editMode = true;
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
      this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
      delete this.sharedService.editObject;
    } else {
      this.vatCodesData = this.vatCodesData.filter(item => item.vatStatus === "A");
      this.vatRegionCodesData = this.vatRegionCodesData.filter(item => item.vatRegionStatus === "A");
      this.model = { vatCode: null, vatRegionCode: null, vatStatus: this.defaultStatus, operation: "I" };
      this.service.serviceDocument.dataProfile.dataModel = this.model;
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  saveAndContinue(): void {
    this.save(false);
  }

  save(saveOnly: boolean): void {
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.saveSubscription(saveOnly);
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  saveSubscription(saveOnly: boolean): void {
    this.checkVatRegionVatCode(saveOnly);
  }

  saveVatRegionVatCode(saveOnly: boolean): void {
    this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.sharedService.saveForm(this.localizationData.vatRegionVatCode.vatregionvatcodesave).subscribe(() => {
          if (saveOnly) {
            this.router.navigate(["/" + this.redirectURL + "/List"], { skipLocationChange: true });
          } else {
            if (!this.editMode) {
              this.model = { vatCode: null, vatRegionCode: null, vatStatus: this.defaultStatus, operation: "I" };
              this.service.serviceDocument.dataProfile.dataModel = this.model;
              this.service.newModel(this.model);
            } else {
              Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
              this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.service.serviceDocument.dataProfile.dataModel);
              this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            }
          }
        }, (error: string) => { console.log(error); });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
    }, (error: string) => { console.log(error); });
  }

  checkVatRegionVatCode(saveOnly: boolean): void {
    if (!this.editMode) {
      let form: FormGroup = this.serviceDocument.dataProfile.profileForm;
      if (form.controls["vatCode"].value && form.controls["vatRegionCode"].value) {
        this.service.isExistingVatRegionVatCode(this.serviceDocument.dataProfile.profileForm.controls["vatCode"].value
          , this.serviceDocument.dataProfile.profileForm.controls["vatRegionCode"].value)
          .subscribe((response: boolean) => {
            if (response) {
              this.sharedService.errorForm(this.localizationData.vatRegionVatCode.vatregionvatcodecheck);
            } else {
              this.saveVatRegionVatCode(saveOnly);
            }
          });
      }
    } else {
      this.saveVatRegionVatCode(saveOnly);
    }
  }

  reset(): void {
    if (!this.editMode) {
      this.model = { vatCode: null, vatRegionCode: null, vatStatus: this.defaultStatus, operation: "I" };
      this.service.serviceDocument.dataProfile.dataModel = this.model;
      this.service.newModel(this.model);
    } else {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    }
    this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
  }

  openVatCode($event: MouseEvent): void {
    $event.preventDefault();
    this.vatCodeModel = { vatCode: this.service.serviceDocument.dataProfile.dataModel.vatCode, vatStatus: null };
    this.sharedService.searchData = Object.assign({}, this.vatCodeModel);
    this.router.navigate(["/VatCode"], { skipLocationChange: true });
  }

  openVatRates($event: MouseEvent): void {
    $event.preventDefault();
    this.vatRateModel = {
      vatCode: this.service.serviceDocument.dataProfile.dataModel.vatCode, vatRate: null, effectiveStartDate: null
      , effectiveEndDate: null
    };
    this.sharedService.searchData = Object.assign({}, this.vatRateModel);
    this.router.navigate(["/vatrates"], { skipLocationChange: true });
  }

  openVatRegion($event: MouseEvent): void {
    $event.preventDefault();
    this.vatRegionModel = { vatRegionCode: this.service.serviceDocument.dataProfile.dataModel.vatRegionCode, vatRegionName: null, vatRegionStatus: null,vatRegion:null,vatRegionType:null };
    this.sharedService.searchData = Object.assign({}, this.vatRegionModel);
    this.router.navigate(["/VatRegion"], { skipLocationChange: true });
  }

  open(): void {
    // Need to implement
  }
}
