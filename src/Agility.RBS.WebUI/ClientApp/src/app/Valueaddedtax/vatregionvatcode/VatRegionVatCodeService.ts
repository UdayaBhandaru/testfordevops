﻿import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { VatRegionVatCodeModel } from "./VatRegionVatCodeModel";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";

@Injectable()
export class VatRegionVatCodeService {
    searchData: any;
    serviceDocument: ServiceDocument<VatRegionVatCodeModel> = new ServiceDocument<VatRegionVatCodeModel>();

    constructor(private httpHelperService: HttpHelperService) { }

    newModel(model: VatRegionVatCodeModel): ServiceDocument<VatRegionVatCodeModel> {
        return this.serviceDocument.newModel(model);
    }

    search(): Observable<ServiceDocument<VatRegionVatCodeModel>> {
        return this.serviceDocument.search("/api/VatRegionVatCode/Search");
    }

    save(): Observable<ServiceDocument<VatRegionVatCodeModel>> {
        return this.serviceDocument.save("/api/VatRegionVatCode/Save", true);
    }

    list(): Observable<ServiceDocument<VatRegionVatCodeModel>> {
        return this.serviceDocument.list("/api/VatRegionVatCode/List");
    }

    isExistingVatRegionVatCode(vatCode: string, vatRegionCode: string): Observable<boolean> {
        return this.httpHelperService.get("/api/VatRegionVatCode/isExistingVatRegionVatCode", new HttpParams().set("vatCode", vatCode)
            .set("vatRegionCode", vatRegionCode));
    }
}