﻿import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { VatRegionVatCodeModel } from "./VatRegionVatCodeModel";
import { VatRegionVatCodeService } from "./VatRegionVatCodeService";
@Injectable()
export class VatRegionVatCodeResolver implements Resolve<ServiceDocument<VatRegionVatCodeModel>> {
    constructor(private service: VatRegionVatCodeService) { }
    resolve(): Observable<ServiceDocument<VatRegionVatCodeModel>> {
        return this.service.list();
    }
}
