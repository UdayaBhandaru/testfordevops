﻿import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { VatRegionVatCodeResolver} from "./VatRegionVatCodeResolver";
import { VatRegionVatCodeListComponent } from "./VatRegionVatCodeListComponent";
import { VatRegionVatCodeComponent } from "./VatRegionVatCodeComponent";
const UtilityRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: VatRegionVatCodeListComponent,
                resolve:
                {
                    serviceDocument: VatRegionVatCodeResolver
                }
            },
            {
                path: "New",
                component: VatRegionVatCodeComponent,
                resolve:
                {
                    serviceDocument: VatRegionVatCodeResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(UtilityRoutes)
    ]
})
export class VatRegionVatCodeRoutingModule { }
