﻿import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { VatRegionVatCodeRoutingModule } from "./VatRegionVatCodeRoutingModule";
import { VatRegionVatCodeResolver } from "./VatRegionVatCodeResolver";
import { AgGridModule } from "ag-grid-angular/main";
import { LicenseManager } from "ag-grid-enterprise/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { VatRegionVatCodeComponent } from "./VatRegionVatCodeComponent";
import { VatRegionVatCodeListComponent } from "./VatRegionVatCodeListComponent";
import { VatRegionVatCodeService } from "./VatRegionVatCodeService";
import { RbsSharedModule } from "../../RbsSharedModule";


@NgModule({
    imports: [
        FrameworkCoreModule,
        VatRegionVatCodeRoutingModule,
        AgGridModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        VatRegionVatCodeComponent,
        VatRegionVatCodeListComponent
    ],
    providers: [
        VatRegionVatCodeService,
        VatRegionVatCodeResolver
    ]
})
export class VatRegionVatCodeModule {
    constructor() {
        LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
    }
}
