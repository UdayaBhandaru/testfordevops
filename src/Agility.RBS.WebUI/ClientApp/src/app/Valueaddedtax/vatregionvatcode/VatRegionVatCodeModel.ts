﻿export class VatRegionVatCodeModel {
    vatCode?: string;
    vatRegionCode?: string;
    vatStatus?: string;
    vatStatusDesc?:string;
    vatRegion?:string;
    operation?: string;
    tableName?: string;
}