import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { SecurityUserGroupModel } from "./SecurityUserGroupModel";
import { SecurityUserGroupService } from "./SecurityUserGroupService";
import { SharedService } from "../../Common/SharedService";
import { FormGroup } from "@angular/forms";

@Component({
  selector: "sec-user-group",
  templateUrl: "./SecurityUserGroupComponent.html"
})

export class SecurityUserGroupComponent implements OnInit {
  PageTitle = "SecurityUserGroup";
  redirectUrl = "securityusergroup";
  serviceDocument: ServiceDocument<SecurityUserGroupModel>;
  editModel: any;
  editMode: boolean = false;
  model: SecurityUserGroupModel;
  public groupList: { groupId: number, groupName: string, groupDescription: string };
  public userList: { userId: string, userName: string };
  constructor(private service: SecurityUserGroupService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.userList = this.service.serviceDocument.domainData["userList"];
    this.groupList = this.service.serviceDocument.domainData["groupList"];
    this.bind();
  }

  bind(): void {
    delete this.editModel;
    if (this.sharedService.editObject) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
      this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
      delete this.sharedService.editObject;
      this.editMode = true;
    } else {
      this.model = { userId: null, groupId: null, groupName: null, operation: "I", userName: null };
      this.service.serviceDocument.dataProfile.dataModel = this.model;
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  save(saveOnly: boolean): void {
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.saveLocalTrait(saveOnly);
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  saveLocalTrait(saveOnly: boolean): void {
    this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.sharedService.saveForm("Security Group saved successfully.").subscribe(() => {

          if (saveOnly) {
            this.router.navigate(["/" + this.redirectUrl + "/List"], { skipLocationChange: true });
          } else {
            if (!this.editMode) {
              this.model = { groupId: null, groupName: null, operation: null, userName: null, userId: null };
              this.service.serviceDocument.dataProfile.dataModel = this.model;
              this.service.newModel(this.model);
            } else {
              Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
              this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.service.serviceDocument.dataProfile.dataModel);
              this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            }
          }
        }, (error: string) => { console.log(error); });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
    }, (error: string) => { console.log(error); });
  }

  reset(): void {
    if (!this.editMode) {
      this.model = { groupId: null, groupName: null, operation: null, userName: null, userId: null };
      this.service.serviceDocument.dataProfile.dataModel = this.model;
      this.service.newModel(this.model);
    } else {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    }
    this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
  }
}
