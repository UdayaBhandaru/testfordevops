export class SecurityUserGroupModel {
  groupId: number;
  groupName: string;
  operation?: string;
  userId: string;
  userName: string;
}
