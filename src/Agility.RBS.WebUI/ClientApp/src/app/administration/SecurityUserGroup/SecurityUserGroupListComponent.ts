import { Component, OnInit } from "@angular/core";
import { ServiceDocument } from "@agility/frameworkcore";
import { GridEditComponent } from '../../Common/grid/GridEditComponent';
import { InfoComponent } from '../../Common/InfoComponent';
import { SharedService } from '../../Common/SharedService';
import { SecurityUserGroupService } from './SecurityUserGroupService';
import { SecurityUserGroupModel } from './SecurityUserGroupModel';
import { Router } from '@angular/router';

@Component({
  selector: "SecurityUserGroup-List",
  templateUrl: "./SecurityUserGroupListComponent.html"
})
export class SecurityUserGroupListComponent implements OnInit {

  public serviceDocument: ServiceDocument<SecurityUserGroupModel>;
  columns: any[];
  public componentName: any;
  model: SecurityUserGroupModel;
  public showSearchCriteria: boolean = true;

  constructor(public service: SecurityUserGroupService, private sharedService: SharedService, public router: Router) {
  }
  pageTitle = "Security Group";
  redirectUrl = "securityusergroup/New/";

  ngOnInit(): void {
    debugger;
    this.model = { userId: null, groupId: null, groupName: null, userName: null, operation: null };
    this.componentName = this;
    this.columns = [
      { headerName: "groupId", field: "groupId", tooltipField: "groupId" },
      { headerName: "groupName", field: "groupName", tooltipField: "groupName" },
      { headerName: "userId", field: "userId", tooltipField: "userId" },
      { headerName: "userName", field: "userName", tooltipField: "userName" }
    ];
    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
      this.sharedService.previousUrl = "../SecurityUserGroup/List";
    }

    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl + cell.data.groupId
      , this.service.serviceDocument.dataProfile.dataList);
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }
  addValidation(evnt: any): void {
    this.navigateToAdd();
  }

  private navigateToAdd(): void {
    this.router.navigate(["/" + this.redirectUrl + "0"], { skipLocationChange: true });
    this.sharedService.previousUrl = "../SecurityUserGroup/List";
  }
}
