import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { SecurityUserGroupComponent } from './SecurityUserGroupComponent';
import { SecurityUserGroupResolver } from './SecurityUserGroupResolver';
import { SecurityUserGroupListComponent } from './SecurityUserGroupListComponent';


const DomainRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "", redirectTo: "List", pathMatch: "full"
      },
      {
        path: "List",
        component: SecurityUserGroupListComponent,
        resolve:
        {
          references: SecurityUserGroupResolver
        }
      },
      {
        path: "New/:id",
        component: SecurityUserGroupComponent
      },

    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(DomainRoutes)
  ]
})
export class SecurityUserGroupRoutingModule { }
