import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";
import { SecurityUserGroupModel } from './SecurityUserGroupModel';
@Injectable()
export class SecurityUserGroupService {
  serviceDocument: ServiceDocument<SecurityUserGroupModel> = new ServiceDocument<SecurityUserGroupModel>();
  searchData: any;
  constructor(private httpHelperService: HttpHelperService) { }

  newModel(model: SecurityUserGroupModel): ServiceDocument<SecurityUserGroupModel> {
    return this.serviceDocument.newModel(model);
  }

  list(): Observable<ServiceDocument<SecurityUserGroupModel>> {
    return this.serviceDocument.list("/api/SecurityUserGroup/List");
  }

  save(): Observable<ServiceDocument<SecurityUserGroupModel>> {
    return this.serviceDocument.save("/api/SecurityUserGroup/Save", true);
  }

  search(): Observable<ServiceDocument<SecurityUserGroupModel>> {
    return this.serviceDocument.search("/api/SecurityUserGroup/List");
  }
}
