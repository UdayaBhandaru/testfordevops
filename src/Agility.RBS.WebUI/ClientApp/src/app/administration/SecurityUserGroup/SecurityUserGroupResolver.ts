import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { SecurityUserGroupModel } from './SecurityUserGroupModel';
import { SecurityUserGroupService } from './SecurityUserGroupService';

@Injectable()
export class SecurityUserGroupResolver implements Resolve<ServiceDocument<SecurityUserGroupModel>> {
  constructor(private service: SecurityUserGroupService) { }
  resolve(): Observable<ServiceDocument<SecurityUserGroupModel>> {
    return this.service.list();
  }
}
