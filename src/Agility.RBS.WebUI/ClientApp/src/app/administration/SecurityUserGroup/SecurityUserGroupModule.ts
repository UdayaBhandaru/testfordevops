import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { RbsSharedModule } from "../../RbsSharedModule";
import { SecurityUserGroupComponent } from './SecurityUserGroupComponent';
import { SecurityUserGroupListComponent } from './SecurityUserGroupListComponent';
import { SecurityUserGroupRoutingModule } from './SecurityUserGroupRoutingModule';
import { SecurityUserGroupService } from './SecurityUserGroupService';
import { SecurityUserGroupResolver } from './SecurityUserGroupResolver';

@NgModule({
  imports: [
    FrameworkCoreModule,
    SecurityUserGroupRoutingModule,
    AgGridModule,
    AgGridSharedModule,
    TitleSharedModule,
    RbsSharedModule
  ],
  declarations: [
    SecurityUserGroupComponent,
    SecurityUserGroupListComponent

  ],
  providers: [
    SecurityUserGroupService,
    SecurityUserGroupResolver
  ]
})
export class SecurityUserGroupModule {
}
