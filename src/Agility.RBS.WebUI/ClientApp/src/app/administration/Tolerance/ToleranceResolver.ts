import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { ToleranceService } from "./ToleranceService";
import { ToleranceModel } from './ToleranceModel';

@Injectable()
export class ToleranceResolver implements Resolve<ServiceDocument<ToleranceModel>> {
  constructor(private service: ToleranceService) { }
  resolve(): Observable<ServiceDocument<ToleranceModel>> {
    return this.service.list();
  }
}
