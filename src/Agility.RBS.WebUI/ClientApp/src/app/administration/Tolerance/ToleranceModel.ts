export class ToleranceModel {
  toleranceKey?: number;
  costQuantityIndicator: number;
  summaryLineIndicator: string;
  toleranceDocumentType: string;
  lowerLimitInclusive?: number;
  upperLimitExclusive?: number;
  FavorOf: string;
  toleranceValueType: string;
  toleranceValue?: number;
  updateId: string;
  operation: string;
}
