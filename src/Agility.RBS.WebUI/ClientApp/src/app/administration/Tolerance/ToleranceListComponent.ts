import { Component, OnInit } from "@angular/core";
import { ServiceDocument } from "@agility/frameworkcore";
import { ToleranceService } from "./ToleranceService";
import { ToleranceModel } from './ToleranceModel';
import { GridEditComponent } from '../../Common/grid/GridEditComponent';
import { InfoComponent } from '../../Common/InfoComponent';
import { SharedService } from '../../Common/SharedService';

@Component({
  selector: "Tolerance-List",
  templateUrl: "./ToleranceListComponent.html"
})
export class ToleranceListComponent implements OnInit {

  public serviceDocument: ServiceDocument<ToleranceModel>;
  columns: any[];
  public componentName: any;
  model: ToleranceModel;
  public showSearchCriteria: boolean = false;
  constructor(public service: ToleranceService, private sharedService: SharedService) {
  }
  PageTitle = "Tolerance";
  redirectUrl = "tolerance";

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "Tolerance Key", field: "toleranceKey", tooltipField: "toleranceKey" },
      { headerName: "Cost Quantity Indicator", field: "costQuantityIndicator", tooltipField: "costQuantityIndicator" },
      { headerName: "Summary Line Indicator", field: "summaryLineIndicator", tooltipField: "summaryLineIndicator" },
      { headerName: "Lower Limit Inclusive", field: "lowerLimitInclusive", tooltipField: "lowerLimitInclusive" },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 90
      }
    ];
    //this.model = { toleranceKey: null, costQuantityIndicator: null, summaryLineIndicator: null, toleranceDocumentType: null, lowerLimitInclusive: null, upperLimitExclusive: null, FavorOf: null, toleranceValueType: null, toleranceValue: null, updateId: null, operation: null };
    //if (this.sharedService.searchData) {

    //  this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
    //  this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    //} else {
    //  this.service.newModel(this.model);
    //}
    this.serviceDocument = this.service.serviceDocument;
  }

  open(cell: any, mode: string): void {
    // this.sharedService.searchData = this.service.serviceDocument.dataProfile.dataModel;
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

}
