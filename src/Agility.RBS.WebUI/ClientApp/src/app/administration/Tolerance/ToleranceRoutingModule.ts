import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ToleranceListComponent } from "./ToleranceListComponent";
import { ToleranceResolver } from "./ToleranceResolver";
import { ToleranceComponent } from "./ToleranceComponent";


const DomainRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "", redirectTo: "List", pathMatch: "full"
      },
      {
        path: "List",
        component: ToleranceListComponent,
        resolve:
        {
          references: ToleranceResolver
        }
      },
      {
        path: "New",
        component: ToleranceComponent
      },

    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(DomainRoutes)
  ]
})
export class ToleranceRoutingModule { }
