import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { ToleranceRoutingModule } from "./ToleranceRoutingModule";
import { ToleranceListComponent } from "./ToleranceListComponent";
import { ToleranceComponent } from "./ToleranceComponent"
import { ToleranceService } from "./ToleranceService";
import { ToleranceResolver } from "./ToleranceResolver";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { RbsSharedModule } from "../../RbsSharedModule";

@NgModule({
  imports: [
    FrameworkCoreModule,
    ToleranceRoutingModule,
    AgGridModule,
    AgGridSharedModule,
    TitleSharedModule,
    RbsSharedModule
  ],
  declarations: [
    ToleranceListComponent,
    ToleranceComponent

  ],
  providers: [
    ToleranceService,
    ToleranceResolver
  ]
})
export class ToleranceModule {
}
