import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";
import { ToleranceModel } from './ToleranceModel';
@Injectable()
export class ToleranceService {
  serviceDocument: ServiceDocument<ToleranceModel> = new ServiceDocument<ToleranceModel>();
  searchData: any;
  constructor(private httpHelperService: HttpHelperService) { }

  newModel(model: ToleranceModel): ServiceDocument<ToleranceModel> {
    return this.serviceDocument.newModel(model);
  }

  list(): Observable<ServiceDocument<ToleranceModel>> {
    return this.serviceDocument.list("/api/Tolerance/List");
  }

  save(): Observable<ServiceDocument<ToleranceModel>> {
    return this.serviceDocument.save("/api/Tolerance/Save", true);
  }

  search(): Observable<ServiceDocument<ToleranceModel>> {
    return this.serviceDocument.list("/api/Tolerance/List");
  }
}
