import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { MSModel } from './MSModel';
@Injectable()
export class MSService {
  serviceDocument: ServiceDocument<MSModel> = new ServiceDocument<MSModel>();
  searchData: any;
  constructor() { }

  newModel(model: MSModel): ServiceDocument<MSModel> {
    return this.serviceDocument.newModel(model);
  }

  list(): Observable<ServiceDocument<MSModel>> {
    return this.serviceDocument.list("/api/MessageSearch/List");
  }

  save(): Observable<ServiceDocument<MSModel>> {
    return this.serviceDocument.save("/api/MessageSearch/Save", true);
  }

  search(): Observable<ServiceDocument<MSModel>> {
    return this.serviceDocument.search("/api/MessageSearch/Search");
  }
}
