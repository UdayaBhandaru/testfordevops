import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { RbsSharedModule } from "../../RbsSharedModule";
import { MSComponent } from './MSComponent';
import { MSListComponent } from './MSListComponent';
import { MSRoutingModule } from './MSRoutingModule';
import { MSService } from './MSService';
import { MSResolver } from './MSResolver';

@NgModule({
  imports: [
    FrameworkCoreModule,
    MSRoutingModule,
    AgGridModule,
    AgGridSharedModule,
    TitleSharedModule,
    RbsSharedModule
  ],
  declarations: [
    MSComponent,
    MSListComponent

  ],
  providers: [
    MSService,
    MSResolver
  ]
})
export class MSModule {
}
