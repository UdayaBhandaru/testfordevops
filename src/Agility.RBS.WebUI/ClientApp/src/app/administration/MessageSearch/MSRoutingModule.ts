import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { MSComponent } from './MSComponent';
import { MSResolver } from './MSResolver';
import { MSListComponent } from './MSListComponent';


const DomainRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "", redirectTo: "List", pathMatch: "full"
      },
      {
        path: "List",
        component: MSListComponent,
        resolve:
        {
          references: MSResolver
        }
      },
      {
        path: "New/:id",
        component: MSComponent
      },

    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(DomainRoutes)
  ]
})
export class MSRoutingModule { }
