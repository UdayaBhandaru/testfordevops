import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { MSModel } from "./MSModel";
import { MSService } from "./MSService";
import { SharedService } from "../../Common/SharedService";
import { FormGroup, Validators } from "@angular/forms";
import { DomainDetailModel } from '../../domain/DomainDetailModel';
import { LanguageDomainModel } from '../../Common/DomainData/LanguageDomainModel';

@Component({
  selector: "msg-search",
  templateUrl: "./MSComponent.html"
})

export class MSComponent implements OnInit {
  PageTitle = "Message Search Window";
  redirectUrl = "MessageSearch";
  serviceDocument: ServiceDocument<MSModel>;
  editModel: MSModel;
  editMode: boolean = false;
  model: MSModel;
  indicatorList: any;
  errorTypeList: DomainDetailModel[];
  yesNoList: DomainDetailModel[];
  languagesList: LanguageDomainModel[];
  constructor(private service: MSService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.errorTypeList = this.sharedService.domainData["errorTypeList"]
    this.languagesList = this.sharedService.domainData["languagesList"]
    this.yesNoList = this.sharedService.domainData["yesNoList"]
    this.bind();
  }

  bind(): void {
    delete this.editModel;
    if (this.sharedService.editObject) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
      this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
      delete this.sharedService.editObject;
      this.editMode = true;
    } else {
      this.model = { key: null, errorType: null, isApproved: null, language: null, text: null, user: null, operation: "I" };
      this.service.serviceDocument.dataProfile.dataModel = this.model;
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  save(saveOnly: boolean): void {
    let fm: FormGroup = this.serviceDocument.dataProfile.profileForm;
    fm.controls["isApproved"].setValidators([Validators.required]);
    fm.controls["isApproved"].updateValueAndValidity();
    this.sharedService.validateForm(fm);
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.saved(saveOnly);
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  saved(saveOnly: boolean): void {
    this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.sharedService.saveForm("Message search text saved successfully.").subscribe(() => {

          if (saveOnly) {
            this.router.navigate(["/" + this.redirectUrl + "/List"], { skipLocationChange: true });
          } else {
            if (!this.editMode) {
              this.model = { key: null, errorType: null, isApproved: null, language: null, text: null, user: null, operation: "I" };
              this.service.serviceDocument.dataProfile.dataModel = this.model;
              this.service.newModel(this.model);
            } else {
              Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
              this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.service.serviceDocument.dataProfile.dataModel);
              this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            }
          }
        }, (error: string) => { console.log(error); });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
    }, (error: string) => { console.log(error); });
  }

  reset(): void {
    if (!this.editMode) {
      this.model = { key: null, errorType: null, isApproved: null, language: null, text: null, user: null, operation: "I" };
      this.service.serviceDocument.dataProfile.dataModel = this.model;
      this.service.newModel(this.model);
    } else {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    }
    this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
  }
}
