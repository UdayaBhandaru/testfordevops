import { Component, OnInit } from "@angular/core";
import { ServiceDocument } from "@agility/frameworkcore";
import { InfoComponent } from '../../Common/InfoComponent';
import { SharedService } from '../../Common/SharedService';
import { MSService } from './MSService';
import { MSModel } from './MSModel';
import { Router } from '@angular/router';
import { DomainDetailModel } from '../../domain/DomainDetailModel';
import { LanguageDomainModel } from '../../Common/DomainData/LanguageDomainModel';

@Component({
  selector: "MS-List",
  templateUrl: "./MSListComponent.html"
})
export class MSListComponent implements OnInit {

  public serviceDocument: ServiceDocument<MSModel>;
  columns: any[];
  public componentName: any;
  model: MSModel;
  public showSearchCriteria: boolean = true;
    errorTypeList: DomainDetailModel[];
  languagesList: LanguageDomainModel[];
  yesNoList: DomainDetailModel[];

  constructor(public service: MSService, private sharedService: SharedService, public router: Router) {
  }
  pageTitle = "Message Search Page";
  redirectUrl = "MessageSearch/New/";

  ngOnInit(): void {
    this.model = { key: null, errorType: null, isApproved: null, language: null, text: null, user: null, operation: "I" };
    this.componentName = this;
    this.sharedService.domainData["errorTypeList"]=this.errorTypeList = this.service.serviceDocument.domainData["errorTypeList"];
    this.sharedService.domainData["languagesList"] = this.languagesList = this.service.serviceDocument.domainData["languagesList"];
    this.sharedService.domainData["yesNoList"] = this.yesNoList = this.service.serviceDocument.domainData["yesNoList"];

    
    this.columns = [                                         
      { headerName: "key", field: "key", tooltipField: "key" },
      { headerName: "text", field: "text", tooltipField: "text" },
      { headerName: "error Type Desc", field: "errorTypeDesc", tooltipField: "errorTypeDesc" }
    ];
    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
      this.sharedService.previousUrl = "../MessageSearch/List";
    }

    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl + cell.data.groupId
      , this.service.serviceDocument.dataProfile.dataList);
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }
  addValidation(evnt: any): void {
    this.navigateToAdd();
  }

  private navigateToAdd(): void {
    this.router.navigate(["/" + this.redirectUrl + "0"], { skipLocationChange: true });
    this.sharedService.previousUrl = "../MessageSearch/List";
  }
}
