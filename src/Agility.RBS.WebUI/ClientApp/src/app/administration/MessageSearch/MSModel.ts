export class MSModel {
  key: string;
  text: string;
  errorType: string;
  language: number;
  isApproved: string;
  user: string;
  operation?: string;
}
