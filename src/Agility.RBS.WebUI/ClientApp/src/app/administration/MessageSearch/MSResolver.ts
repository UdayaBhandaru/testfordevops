import { Injectable } from "@angular/core";
import { Resolve } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { MSModel } from './MSModel';
import { MSService } from './MSService';

@Injectable()
export class MSResolver implements Resolve<ServiceDocument<MSModel>> {
  constructor(private service: MSService) { }
  resolve(): Observable<ServiceDocument<MSModel>> {
    return this.service.list();
  }
}
