import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { SVModel } from "./SVModel";
import { SVService } from "./SVService";
import { SharedService } from "../../Common/SharedService";
import { DomainDetailModel } from '../../domain/DomainDetailModel';
import { LanguageDomainModel } from '../../Common/DomainData/LanguageDomainModel';

@Component({
  selector: "sys-var",
  templateUrl: "./SVComponent.html"
})

export class SVComponent implements OnInit {
  PageTitle = "System Variables Window";
  redirectUrl = "SystemVariables";
  serviceDocument: ServiceDocument<SVModel>;
  editModel: SVModel;
  editMode: boolean = false;
  model: SVModel;
  indicatorList: any;
  errorTypeList: DomainDetailModel[];
  yesNoList: DomainDetailModel[];
  languagesList: LanguageDomainModel[];
  countryList: any;
  invHistLvlList: any;
  distributionRuleList: any;
  currencyList: any;
    allocMethodList: any;
    racRtvTsfIndList: any;
    storeToStoreList: any;
    storeToWhList: any;
    whToStoreList: any;
    whToWhList: any;
    stockLedgerProdLevelCodeList: any;
    stockLedgerTimeLevelCodeList: any;
    stockLedgerLocLevelCodeList: any;
    rcvCostAdjTypeList: any;
    otbProdLevelCodeList: any;
    otbTimeLevelCodeList: any;
  constructor(private service: SVService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.countryList = this.service.serviceDocument.domainData["countryList"];
    this.languagesList = this.service.serviceDocument.domainData["languagesList"];
    this.invHistLvlList = this.service.serviceDocument.domainData["invHistLvlList"];
    this.distributionRuleList = this.service.serviceDocument.domainData["distributionRuleList"];
    this.currencyList = this.service.serviceDocument.domainData["currencyList"];
    this.allocMethodList = this.service.serviceDocument.domainData["allocMethodList"];
    this.racRtvTsfIndList = this.service.serviceDocument.domainData["racRtvTsfIndList"];
    this.storeToStoreList = this.service.serviceDocument.domainData["storeToStoreList"];
    this.stockLedgerProdLevelCodeList = this.service.serviceDocument.domainData["stockLedgerProdLevelCodeList"];
    this.stockLedgerTimeLevelCodeList = this.service.serviceDocument.domainData["stockLedgerTimeLevelCodeList"];
    this.stockLedgerLocLevelCodeList = this.service.serviceDocument.domainData["stockLedgerLocLevelCodeList"];
    this.rcvCostAdjTypeList = this.service.serviceDocument.domainData["rcvCostAdjTypeList"];
    this.otbProdLevelCodeList = this.service.serviceDocument.domainData["otbProdLevelCodeList"];
    this.otbTimeLevelCodeList = this.service.serviceDocument.domainData["otbTimeLevelCodeList"];
    this.bind();
  }

  bind(): void {
    this.setModelNull("I");
    this.service.serviceDocument.dataProfile.dataModel = this.service.serviceDocument.dataProfile.dataList[0];
    this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    this.serviceDocument = this.service.serviceDocument;
  }

  setModelNull(operation: string): void {
    this.model = {
      addrCatalog: null, baseCountryId: null, operation: operation, primaryLang: null, invHistLevel: null, distributionRule: null,
      cdModulus: null, cdWeight8: null, cdWeight7: null, cdWeight6: null, cdWeight5: null, cdWeight4: null, cdWeight3: null, cdWeight2: null, cdWeight1: null
    };
  }

  save(saveOnly: boolean): void {
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.saved(saveOnly);
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  saved(saveOnly: boolean): void {
    this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.sharedService.saveForm("Saved successfully.").subscribe(() => {
          if (saveOnly) {
            this.router.navigate(["/" + this.redirectUrl + "/List"], { skipLocationChange: true });
          } else {
            if (!this.editMode) {
              this.setModelNull("I");
              this.service.serviceDocument.dataProfile.dataModel = this.model;
              this.service.newModel(this.model);
            } else {
              Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
              this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.service.serviceDocument.dataProfile.dataModel);
              this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            }
          }
        }, (error: string) => { console.log(error); });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
    }, (error: string) => { console.log(error); });
  }

  reset(): void {
    if (!this.editMode) {
      this.setModelNull("I");
      this.service.serviceDocument.dataProfile.dataModel = this.model;
      this.service.newModel(this.model);
    } else {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    }
    this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
  }
}
