import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { SVModel } from './SVModel';
@Injectable()
export class SVService {
  serviceDocument: ServiceDocument<SVModel> = new ServiceDocument<SVModel>();
  searchData: any;
  constructor() { }

  newModel(model: SVModel): ServiceDocument<SVModel> {
    return this.serviceDocument.newModel(model);
  }

  list(): Observable<ServiceDocument<SVModel>> {
    return this.serviceDocument.list("/api/SystemVariables/List");
  }

  save(): Observable<ServiceDocument<SVModel>> {
    return this.serviceDocument.save("/api/MessageSearch/Save", true);
  }

  search(): Observable<ServiceDocument<SVModel>> {
    return this.serviceDocument.search("/api/MessageSearch/Search");
  }
}
