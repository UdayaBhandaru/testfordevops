import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { SVComponent } from './SVComponent';
import { SVResolver } from './SVResolver';


const DomainRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "", redirectTo: "List", pathMatch: "full"
      },
      {
        path: "List",
        component: SVComponent,
        resolve:
        {
          references: SVResolver
        }
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(DomainRoutes)
  ]
})
export class SVRoutingModule { }
