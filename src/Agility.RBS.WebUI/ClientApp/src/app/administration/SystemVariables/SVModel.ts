export class SVModel {
  addrCatalog?: string;

  aipInd?: string;

  allocMethod?: string;

  applyProfPresStock?: string;

  ariInd?: string;

  autoApproveChildInd?: string;

  autoEan13Prefix?: number;

  baseCountryId?: string;

  billOfLadingHistoryMths?: string;

  billOfLadingInd?: string;

  billToLoc?: string;

  bracketCostingInd?: string;

  budShrinkInd?: string;

  calendar454Ind?: string;

  cdModulus?: number;

  cdWeight1?: number;
  cdWeight2?: number;
  cdWeight3?: number;
  cdWeight4?: number;
  cdWeight5?: number;
  cdWeight6?: number;
  cdWeight7?: number;
  cdWeight8?: number;

  checkDigitInd?: string;

  classLevelVatInd?: string;

  closeMthWithOpnCntInd?: string;

  closeOpenShipDays?: number;

  consolidationInd?: string;

  contractInd?: string;

  contractReplenishInd?: string;

  costLevel?: string;

  costMoney?: number;

  costOutStorage?: number;

  costOutStorageMeas?: string;

  costOutStorageUom?: string;

  costWhStorage?: number;

  costWhStorageMeas?: string;

  costWhStorageUom?: string;

  currencyCode?: string;

  cycleCountLagDays?: number;

  dailySalesDiscMths?: number;

  dataLevelSecurityInd?: string;

  dateEntry?: string;

  dbtMemoSendDays?: number;

  dealAgePriority?: string;

  dealHistoryMonths?: number;

  dealLeadDays?: number;

  dealTypePriority?: string;

  defaultAllocChrgInd?: string;

  defaultCaseName?: string;

  defaultDimensionUom?: string;

  defaultInnerName?: string;

  defaultOrderType?: string;

  defaultPackingMethod?: string;

  defaultPalletName?: string;

  defaultSizeProfile?: string;

  defaultStandardUom?: string;

  defaultUop?: string;

  defaultVatRegion?: number;

  defaultWeightUom?: string;

  deptLevelTransfers?: string;

  diffGroupMerchLevelCode?: string;

  diffGroupOrgLevelCode?: string;

  distributionRule?: string;

  domainLevel?: string;

  dummyCartonInd?: string;

  duplicateReceivingInd?: string;

  ediCostChgDays?: string;

  ediCostOverrideInd?: string;

  ediDailyRptLag?: number;

  ediNewItemDays?: number;

  ediRevDays?: number;

  elcInd?: string;

  extInvcMatchInd?: string;

  financialAp?: string;

  fobTitlePass?: string;

  fobTitlePassDesc?: string;

  forecastInd?: string;

  futureCostHistoryDays?: number;

  genConsignmentInvcFreq?: string;

  genConInvcItmSupLocInd?: string;

  glRollup?: string;

  groceryItemsInd?: string;

  ibResultsPurgeDays?: number;

  imagePath?: string;

  importHtsDate?: string;

  importInd?: string;

  increaseTsfQtyInd?: string;

  intercompanyTransferInd?: string;

  interfacePurgeDays?: number;

  invHistLevel?: string;

  invcDbtMaxPct?: number;

  invcMatchExtrDays?: number;

  invcMatchInd?: string;

  invcMatchMultSupInd?: string;

  invcMatchQtyInd?: string;

  latest_ship_days?: number;

  lcApplicant?: string;

  lc_exp_days?: number;

  lcFormType?: string;

  lcType?: string;

  level1Name?: string;

  level2Name?: string;

  level3Name?: string;

  locActivityInd?: string;

  locCloseHistMonths?: number;

  locDlvryInd?: string;

  locListOrgLevelCode?: string;

  locTraitOrgLevelCode?: string;

  lookAheadDays?: number;

  maxCumMarkonPct?: number;

  maxScalingIterations?: number;

  maxWeeksSupply?: number;

  measurementType?: string;

  merchHierAutoGenInd?: string;

  minCumMarkonPct?: number;

  multiCurrencyInd?: string;

  multichannelInd?: string;

  nomFlag1Label?: string;

  nomFlag2Label?: string;

  nomFlag3Label?: string;

  nomFlag4Label?: string;

  nomFlag5Label?: string;

  nwpInd?: string;

  nwpRetentionPeriod?: number;

  oracleFinancialsVers?: string;

  ordApprAmtCode?: string;

  ordApprCloseDelay?: number;

  ordAutoClosePartRcvdInd?: string;

  ordPackCompHeadInd?: string;

  ordPackCompInd?: string;

  ordPartRcvdCloseDelay?: number;

  ordWorksheetCleanUpDelay?: number;

  otbProdLevelCode?: string;

  otbSystemInd?: string;

  otbTimeLevelCode?: string;

  partnerIdUniqueInd?: string;

  planInd?: string;

  priceHistRetentionDays?: number;

  primaryLang?: number;

  racRtvTsfInd?: string;

  rcvCostAdjType?: string;

  rdwInd?: string;

  reclassApprOrderInd?: string;

  reclassDate?: Date;

  reclassSysMaintDateInd?: string;

  redistFactor?: number;

  rejectStoreOrdInd?: string;

  replAttrHistRetentionWeeks?: number;

  replOrderDays?: number;

  replOrderHistoryDays?: number;

  replPackHistWks?: number;

  replResultsAllInd?: string;

  replResultsPurgeDays?: number;

  retnSchedUpdDays?: number;

  roundLvl?: string;

  roundToCasePct?: number;

  roundToInnerPct?: number;

  roundToLayerPct?: number;

  roundToPalletPct?: number;

  rpmInd?: string;

  rpmRibInd?: string;

  rtmSimplifiedInd?: string;

  rtvNadLeadTime?: number;

  salesAuditInd?: string;

  seasonMerchLevelCode?: string;

  seasonOrgLevelCode?: string;

  secondaryDescInd?: string;

  selfBillInd?: string;

  shipSchedHistoryMths?: number;

  singleStylePoInd?: string;

  skulistOrgLevelCode?: string;

  softContractInd?: string;

  sorItemInd?: string;

  sorMerchHierInd?: string;

  sorOrgHierInd?: string;

  sorPurchaseOrderInd?: string;

  stakeAutoProcessing?: string;

  stakeCostVariance?: number;

  stakeLockoutDays?: number;

  stakeRetailVariance?: number;

  stakeReviewDays?: number;

  stakeUnitVariance?: number;

  startOfHalfMonth?: number;

  stdAvInd?: string;

  stkldgrVatInclRetlInd?: string;

  stockLedgerLocLevelCode?: string;

  stockLedgerProdLevelCode?: string;

  stockLedgerTimeLevelCode?: string;

  storageType?: string;

  storeOrdersPurgeDays?: number;

  storePackCompRcvInd?: string;

  suppPartAutoGen_ind?: string;

  tableOwner?: string;

  targetRoi?: number;

  ticketOverPct?: number;

  ticketTypeMerchLevelCode?: string;

  ticketTypeOrgLevelCode?: string;

  timeDisplay?: string;

  timeEntry?: string;

  tranDataRetainedDaysNo?: number;

  tsfAutoCloseStore?: string;

  tsfAutoCloseWh?: string;

  tsfForceCloseInd?: string;

  tsfMdStoreToStoreSndRcv?: string;

  tsfMdStoreToWhSndRcv?: string;

  tsfMdWhToStoreSndRcv?: string;

  tsfMdWhToWhSndRcv?: string;

  tsfMrtRetentionDays?: number;

  tsfHistoryMths?: number;

  tsfPriceExceedWacInd?: string;

  udaMerchLevelCode?: string;

  udaOrgLevelCode?: string;

  unavailStkordInvAdjInd?: string;

  updateItemHtsInd?: string;

  updateOrdeHtsInd?: string;

  vatInd?: string;

  whCrossLinkInd?: string;

  whStoreAssignHistDays?: string;

  whStoreAssignType?: string;

  wmsCompatibleInd?: string;

  wrongStReceiptInd?: string;

  programPhase?: string;

  programMessage?: string;

  pError?: string;

  operation?: string;
}
