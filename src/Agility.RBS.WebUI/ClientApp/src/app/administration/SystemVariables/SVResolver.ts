import { Injectable } from "@angular/core";
import { Resolve } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { SVModel } from './SVModel';
import { SVService } from './SVService';

@Injectable()
export class SVResolver implements Resolve<ServiceDocument<SVModel>> {
  constructor(private service: SVService) { }
  resolve(): Observable<ServiceDocument<SVModel>> {
    return this.service.list();
  }
}
