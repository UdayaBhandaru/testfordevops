import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { RbsSharedModule } from "../../RbsSharedModule";
import { SVComponent } from './SVComponent';
import { SVRoutingModule } from './SVRoutingModule';
import { SVService } from './SVService';
import { SVResolver } from './SVResolver';

@NgModule({
  imports: [
    FrameworkCoreModule,
    SVRoutingModule,
    AgGridModule,
    AgGridSharedModule,
    TitleSharedModule,
    RbsSharedModule
  ],
  declarations: [
    SVComponent
  ],
  providers: [
    SVService,
    SVResolver
  ]
})
export class SVModule {
}
