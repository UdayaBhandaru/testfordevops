import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";
import { GroupHierarchyModel } from './GroupHierarchyModel';
@Injectable()
export class GroupHierarchyService {
  serviceDocument: ServiceDocument<GroupHierarchyModel> = new ServiceDocument<GroupHierarchyModel>();
  searchData: any;
  constructor(private httpHelperService: HttpHelperService) { }

  newModel(model: GroupHierarchyModel): ServiceDocument<GroupHierarchyModel> {
    return this.serviceDocument.newModel(model);
  }

  list(): Observable<ServiceDocument<GroupHierarchyModel>> {
    return this.serviceDocument.list("/api/SecurityGroupHierarchy/List");
  }

  save(): Observable<ServiceDocument<GroupHierarchyModel>> {
    return this.serviceDocument.save("/api/SecurityGroupHierarchy/Save", true);
  }

  search(): Observable<ServiceDocument<GroupHierarchyModel>> {
    return this.serviceDocument.search("/api/SecurityGroupHierarchy/Search");
  }
}
