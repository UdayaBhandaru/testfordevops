import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { GroupHierarchyModel } from "./GroupHierarchyModel";
import { GroupHierarchyService } from "./GroupHierarchyService";
import { SharedService } from "../../Common/SharedService";
import { FormGroup } from "@angular/forms";
import { debug } from 'util';
import { LoaderService } from '../../Common/LoaderService';
import { Subscription } from 'rxjs';

@Component({
  selector: "sec-user-group",
  templateUrl: "./GroupHierarchyComponent.html"
})

export class GroupHierarchyComponent implements OnInit {
  PageTitle = "GroupHierarchy";
  redirectUrl = "SecurityGroupHierarchy";
  serviceDocument: ServiceDocument<GroupHierarchyModel>;
  editModel: any;
  editMode: boolean = false;
  model: GroupHierarchyModel;
  public merchHierLevels: any = [];
  public merchHierValues: any = [];
  public domainData: any;
  public securityGroups: any;
  saveServiceSubscription: Subscription;
  submitSuccessSubscription: Subscription;
  primaryId: number;
  constructor(
    private service: GroupHierarchyService,
    private sharedService: SharedService,
    private router: Router,
    private loaderService: LoaderService) {
  }

  ngOnInit(): void {
    this.domainData = this.sharedService.domainData["domainModel"];
    this.merchHierLevels = this.domainData.merchLevels;
    this.securityGroups = this.domainData.securityGroups;
    this.bind();
  }

  bind(): void {
    delete this.editModel;
    if (this.sharedService.editObject) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
      this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
      delete this.sharedService.editObject;
      this.editMode = true;
    } else {
      this.model = { groupId: null, operation: "I", merchLevel: null, merchLevelValue: null };
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  save(saveOnly: boolean): void {
    this.service.save().subscribe();

    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.saveSubscription(saveOnly);
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  saveAndContinue(): void {
    this.save(false);
  }

  saveSubscription(saveOnly: boolean): void {
    this.loaderService.display(true);
    this.saveServiceSubscription = this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.primaryId = this.service.serviceDocument.dataProfile.dataModel.groupId;
        this.submitSuccessSubscription = this.sharedService.saveForm(`Saved Successfully`).subscribe(() => {
          if (saveOnly) {
            this.sharedService.domainData["domainModel"] = {};
            this.router.navigate(["/SecurityGroupHierarchy/List"], { skipLocationChange: true });
          } else {
            this.router.navigate(["/Blank"], {
              skipLocationChange: true, queryParams: {
                id: "/SecurityGroupHierarchy/New/" + this.primaryId
              }
            });
          }
        });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
      this.loaderService.display(false);
    });
  }


  changeMerchLevel(eventData: any) {
    switch (eventData.options.code) {
      case "D":
        this.merchHierValues = this.domainData.divisions;
        break;
      case "G":
        this.merchHierValues = this.domainData.groups;
        break;
      case "P":
        this.merchHierValues = this.domainData.departments;
        break;
      default:
        this.merchHierValues = [];
    }
  }

  ngOnDestroy(): void {
    if (this.saveServiceSubscription) {
      this.saveServiceSubscription.unsubscribe();
    }
    if (this.submitSuccessSubscription) {
      this.submitSuccessSubscription.unsubscribe();

    }
  }

  reset(): void {
    Object.keys(this.serviceDocument.dataProfile.profileForm.controls).forEach(control => {
      this.serviceDocument.dataProfile.profileForm.controls[control].setValue(this.serviceDocument.dataProfile.dataModel[control]);
    });
    this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
  }
}
