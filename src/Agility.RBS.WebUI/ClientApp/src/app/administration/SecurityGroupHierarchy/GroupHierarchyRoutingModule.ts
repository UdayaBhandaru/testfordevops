import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { GroupHierarchyComponent } from './GroupHierarchyComponent';
import { GroupHierarchyResolver } from './GroupHierarchyResolver';
import { GroupHierarchyListComponent } from './GroupHierarchyListComponent';


const DomainRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "", redirectTo: "List", pathMatch: "full"
      },
      {
        path: "List",
        component: GroupHierarchyListComponent,
        resolve:
        {
          references: GroupHierarchyResolver
        }
      },
      {
        path: "New/:id",
        component: GroupHierarchyComponent
      },

    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(DomainRoutes)
  ]
})
export class GroupHierarchyRoutingModule { }
