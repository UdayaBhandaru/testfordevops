import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { RbsSharedModule } from "../../RbsSharedModule";
import { GroupHierarchyComponent } from './GroupHierarchyComponent';
import { GroupHierarchyListComponent } from './GroupHierarchyListComponent';
import { GroupHierarchyRoutingModule } from './GroupHierarchyRoutingModule';
import { GroupHierarchyService } from './GroupHierarchyService';
import { GroupHierarchyResolver } from './GroupHierarchyResolver';

@NgModule({
  imports: [
    FrameworkCoreModule,
    GroupHierarchyRoutingModule,
    AgGridModule,
    AgGridSharedModule,
    TitleSharedModule,
    RbsSharedModule
  ],
  declarations: [
    GroupHierarchyComponent,
    GroupHierarchyListComponent

  ],
  providers: [
    GroupHierarchyService,
    GroupHierarchyResolver
  ]
})
export class GroupHierarchyModule {
}
