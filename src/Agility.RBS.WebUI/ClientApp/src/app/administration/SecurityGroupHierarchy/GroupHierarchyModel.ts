export class GroupHierarchyModel {
  groupId: number;
  groupName?: string;
  operation?: string;
  merchLevel?: string;
  merchLevelValue?: number;
  merchLevelDesc?: string;
  merchLevelValueDesc?: string;
}
