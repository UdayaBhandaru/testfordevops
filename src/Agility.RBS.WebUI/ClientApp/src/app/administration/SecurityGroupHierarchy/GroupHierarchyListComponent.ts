import { Component, OnInit } from "@angular/core";
import { ServiceDocument } from "@agility/frameworkcore";
import { GridEditComponent } from '../../Common/grid/GridEditComponent';
import { InfoComponent } from '../../Common/InfoComponent';
import { SharedService } from '../../Common/SharedService';
import { GroupHierarchyService } from './GroupHierarchyService';
import { GroupHierarchyModel } from './GroupHierarchyModel';
import { Router } from '@angular/router';
import { debug } from 'util';

@Component({
  selector: "GroupHierarchy-List",
  templateUrl: "./GroupHierarchyListComponent.html"
})
export class GroupHierarchyListComponent implements OnInit {

  public serviceDocument: ServiceDocument<GroupHierarchyModel>;
  columns: any[];
  public componentName: any;
  model: GroupHierarchyModel;
  public showSearchCriteria: boolean = true;
  public merchHierLevels: any = [];
  public merchHierValues: any = [];
  public domainData: any;
  public securityGroups: any;
  constructor(public service: GroupHierarchyService, private sharedService: SharedService, public router: Router) {
  }
  pageTitle = "Security Group Hierarchy";
  redirectUrl = "SecurityGroupHierarchy/New/";

  ngOnInit(): void {
    debugger;
    this.sharedService.domainData["domainModel"] = this.domainData = this.service.serviceDocument.domainData["domainModel"];
    this.merchHierLevels = this.domainData.merchLevels;
    this.securityGroups = this.domainData.securityGroups;

    this.model = { groupId: null, operation: null, merchLevel: null, merchLevelValue:null };
    this.componentName = this;
    this.columns = [
      { headerName: "Group Id", field: "groupId", tooltipField: "groupId" },
      { headerName: "Merch Level", field: "merchLevel", tooltipField: "merchLevel" },
      { headerName: "Merch Level Desc", field: "merchLevelDesc", tooltipField: "merchLevelDesc" },
      { headerName: "MerchLevelValue", field: "merchLevelValue", tooltipField: "merchLevelValue" },
      { headerName: "MerchLevelValueDesc", field: "merchLevelValueDesc", tooltipField: "merchLevelValueDesc" }
    ];
    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
      this.sharedService.previousUrl = "../SecurityGroupHierarchy/List";
    }

    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl + cell.data.groupId
      , this.service.serviceDocument.dataProfile.dataList);
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }
  addValidation(evnt: any): void {
    this.navigateToAdd();
  }

  private navigateToAdd(): void {
    this.router.navigate(["/" + this.redirectUrl + "0"], { skipLocationChange: true });
    this.sharedService.previousUrl = "../SecurityGroupHierarchy/List";
  }

  changeMerchLevel(eventData: any) {
    console.log(eventData);

    switch (eventData.options.code) {
      case "D":
        this.merchHierValues = this.domainData.divisions;
        break;
      case "G":
        this.merchHierValues = this.domainData.groups;
        break;
      case "P":
        this.merchHierValues = this.domainData.departments;
        break;
      default:
        this.merchHierValues = [];
    }
  }
}
