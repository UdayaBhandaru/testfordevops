import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { GroupHierarchyModel } from './GroupHierarchyModel';
import { GroupHierarchyService } from './GroupHierarchyService';

@Injectable()
export class GroupHierarchyResolver implements Resolve<ServiceDocument<GroupHierarchyModel>> {
  constructor(private service: GroupHierarchyService) { }
  resolve(): Observable<ServiceDocument<GroupHierarchyModel>> {
    return this.service.list();
  }
}
