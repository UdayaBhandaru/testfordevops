import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { GroupOrgHierarchyModel } from './GroupOrgHierarchyModel';
import { GroupOrgHierarchyService } from './GroupOrgHierarchyService';

@Injectable()
export class GroupOrgHierarchyResolver implements Resolve<ServiceDocument<GroupOrgHierarchyModel>> {
  constructor(private service: GroupOrgHierarchyService) { }
  resolve(): Observable<ServiceDocument<GroupOrgHierarchyModel>> {
    return this.service.list();
  }
}
