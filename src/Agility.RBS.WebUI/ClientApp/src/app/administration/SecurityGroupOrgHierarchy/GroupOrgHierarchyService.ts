import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";
import { GroupOrgHierarchyModel } from './GroupOrgHierarchyModel';
@Injectable()
export class GroupOrgHierarchyService {
  serviceDocument: ServiceDocument<GroupOrgHierarchyModel> = new ServiceDocument<GroupOrgHierarchyModel>();
  searchData: any;
  constructor(private httpHelperService: HttpHelperService) { }

  newModel(model: GroupOrgHierarchyModel): ServiceDocument<GroupOrgHierarchyModel> {
    return this.serviceDocument.newModel(model);
  }

  list(): Observable<ServiceDocument<GroupOrgHierarchyModel>> {
    return this.serviceDocument.list("/api/SecurityGroupOrgHierarchy/List");
  }

  save(): Observable<ServiceDocument<GroupOrgHierarchyModel>> {
    return this.serviceDocument.save("/api/SecurityGroupOrgHierarchy/Save", true);
  }

  search(): Observable<ServiceDocument<GroupOrgHierarchyModel>> {
    return this.serviceDocument.search("/api/SecurityGroupOrgHierarchy/Search");
  }
}
