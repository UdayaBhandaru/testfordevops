import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { GroupOrgHierarchyComponent } from './GroupOrgHierarchyComponent';
import { GroupOrgHierarchyResolver } from './GroupOrgHierarchyResolver';
import { GroupOrgHierarchyListComponent } from './GroupOrgHierarchyListComponent';


const DomainRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "", redirectTo: "List", pathMatch: "full"
      },
      {
        path: "List",
        component: GroupOrgHierarchyListComponent,
        resolve:
        {
          references: GroupOrgHierarchyResolver
        }
      },
      {
        path: "New/:id",
        component: GroupOrgHierarchyComponent
      },

    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(DomainRoutes)
  ]
})
export class GroupOrgHierarchyRoutingModule { }
