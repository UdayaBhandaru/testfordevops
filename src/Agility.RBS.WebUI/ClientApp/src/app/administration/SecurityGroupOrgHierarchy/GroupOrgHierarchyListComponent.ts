import { Component, OnInit } from "@angular/core";
import { ServiceDocument } from "@agility/frameworkcore";
import { GridEditComponent } from '../../Common/grid/GridEditComponent';
import { InfoComponent } from '../../Common/InfoComponent';
import { SharedService } from '../../Common/SharedService';
import { GroupOrgHierarchyService } from './GroupOrgHierarchyService';
import { GroupOrgHierarchyModel } from './GroupOrgHierarchyModel';
import { Router } from '@angular/router';
import { debug } from 'util';

@Component({
  selector: "GroupOrgHierarchy-List",
  templateUrl: "./GroupOrgHierarchyListComponent.html"
})
export class GroupOrgHierarchyListComponent implements OnInit {

  public serviceDocument: ServiceDocument<GroupOrgHierarchyModel>;
  columns: any[];
  public componentName: any;
  model: GroupOrgHierarchyModel;
  public showSearchCriteria: boolean = true;
  public orgLevels: any = [];
  public orgLevelValues: any = [];
  public domainData: any;
  public securityGroups: any;
  constructor(public service: GroupOrgHierarchyService, private sharedService: SharedService, public router: Router) {
  }

  pageTitle = "Security Group Org Hierarchy";
  redirectUrl = "SecurityGroupOrgHierarchy/New/";

  ngOnInit(): void {
    this.sharedService.domainData["domainModel"] = this.domainData = this.service.serviceDocument.domainData["domainModel"];
    this.orgLevels = this.domainData.orgLevels;
    this.securityGroups = this.domainData.securityGroups;

    this.model = { groupId: null, operation: null, orgLevel: null, orgLevelValue:null };
    this.componentName = this;
    this.columns = [
      { headerName: "Group Id", field: "groupId", tooltipField: "groupId" },
      { headerName: "Org Level", field: "orgLevel", tooltipField: "orgLevel" },
      { headerName: "Org Level Desc", field: "orgLevelDesc", tooltipField: "orgLevelDesc" },
      { headerName: "Org Level Value", field: "orgLevelValue", tooltipField: "orgLevelValue" },
      { headerName: "Org Level Value Desc", field: "orgLevelValueDesc", tooltipField: "orgLevelValueDesc" }
    ];
    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
      this.sharedService.previousUrl = "../SecurityGroupOrgHierarchy/List";
    }

    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl + cell.data.groupId
      , this.service.serviceDocument.dataProfile.dataList);
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  addValidation(evnt: any): void {
    this.navigateToAdd();
  }

  private navigateToAdd(): void {
    this.router.navigate(["/" + this.redirectUrl + "0"], { skipLocationChange: true });
    this.sharedService.previousUrl = "../SecurityGroupOrgHierarchy/List";
  }

  changeOrgLevel(eventData: any) {
    switch (eventData.options.code) {
      case "C":
        this.orgLevelValues = this.domainData.chains;
        break;
      case "A":
        this.orgLevelValues = this.domainData.areas;
        break;
      case "R":
        this.orgLevelValues = this.domainData.regions;
        break;
      case "D":
        this.orgLevelValues = this.domainData.districts;
        break;
      default:
        this.orgLevelValues = [];
    }
  }
}
