export class GroupOrgHierarchyModel {
  groupId: number;
  groupName?: string;
  operation?: string;
  orgLevel?: string;
  orgLevelValue?: number;
  orgLevelDesc?: string;
  orgLevelValueDesc?: string;
}
