import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { GroupOrgHierarchyModel } from "./GroupOrgHierarchyModel";
import { GroupOrgHierarchyService } from "./GroupOrgHierarchyService";
import { SharedService } from "../../Common/SharedService";
import { FormGroup } from "@angular/forms";
import { debug } from 'util';
import { LoaderService } from '../../Common/LoaderService';
import { Subscription } from 'rxjs';

@Component({
  selector: "sec-user-group",
  templateUrl: "./GroupOrgHierarchyComponent.html"
})

export class GroupOrgHierarchyComponent implements OnInit {
  PageTitle = "GroupOrgHierarchy";
  redirectUrl = "SecurityGroupOrgHierarchy";
  serviceDocument: ServiceDocument<GroupOrgHierarchyModel>;
  editModel: any;
  editMode: boolean = false;
  model: GroupOrgHierarchyModel;
  public orgLevels: any = [];
  public orgLevelValues: any = [];
  public domainData: any;
  public securityGroups: any;
  saveServiceSubscription: Subscription;
  submitSuccessSubscription: Subscription;
  primaryId: number;
  constructor(
    private service: GroupOrgHierarchyService,
    private sharedService: SharedService,
    private router: Router,
    private loaderService: LoaderService) {
  }

  ngOnInit(): void {
    this.domainData = this.sharedService.domainData["domainModel"];
    this.orgLevels = this.domainData.orgLevels;
    this.securityGroups = this.domainData.securityGroups;
    this.bind();
  }

  bind(): void {
    delete this.editModel;
    if (this.sharedService.editObject) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
      this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
      delete this.sharedService.editObject;
      this.editMode = true;
    } else {
      this.model = { groupId: null, operation: "I", orgLevel: null,orgLevelValue: null };
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  save(saveOnly: boolean): void {
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.saveSubscription(saveOnly);
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  saveAndContinue(): void {
    this.save(false);
  }

  saveSubscription(saveOnly: boolean): void {
    this.loaderService.display(true);
    this.saveServiceSubscription = this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.primaryId = this.service.serviceDocument.dataProfile.dataModel.groupId;
        this.submitSuccessSubscription = this.sharedService.saveForm(`Saved Successfully`).subscribe(() => {
          if (saveOnly) {
            this.sharedService.domainData["domainModel"] = {};
            this.router.navigate(["/SecurityGroupOrgHierarchy/List"], { skipLocationChange: true });
          } else {
            this.router.navigate(["/Blank"], {
              skipLocationChange: true, queryParams: {
                id: "/SecurityGroupOrgHierarchy/New/" + this.primaryId
              }
            });
          }
        });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
      this.loaderService.display(false);
    });
  }



  changeOrgLevel(eventData: any) {
    switch (eventData.options.code) {
      case "C":
        this.orgLevelValues = this.domainData.chains;
        break;
      case "A":
        this.orgLevelValues = this.domainData.areas;
        break;
      case "R":
        this.orgLevelValues = this.domainData.regions;
        break;
      case "D":
        this.orgLevelValues = this.domainData.districts;
        break;
      default:
        this.orgLevelValues = [];
    }
  }

  ngOnDestroy(): void {
    if (this.saveServiceSubscription) {
      this.saveServiceSubscription.unsubscribe();
    }
    if (this.submitSuccessSubscription) {
      this.submitSuccessSubscription.unsubscribe();

    }
  }

  reset(): void {
    Object.keys(this.serviceDocument.dataProfile.profileForm.controls).forEach(control => {
      this.serviceDocument.dataProfile.profileForm.controls[control].setValue(this.serviceDocument.dataProfile.dataModel[control]);
    });
    this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
  }
}
