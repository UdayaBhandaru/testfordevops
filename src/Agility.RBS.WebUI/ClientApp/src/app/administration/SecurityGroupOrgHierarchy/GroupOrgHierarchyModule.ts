import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { RbsSharedModule } from "../../RbsSharedModule";
import { GroupOrgHierarchyComponent } from './GroupOrgHierarchyComponent';
import { GroupOrgHierarchyListComponent } from './GroupOrgHierarchyListComponent';
import { GroupOrgHierarchyRoutingModule } from './GroupOrgHierarchyRoutingModule';
import { GroupOrgHierarchyService } from './GroupOrgHierarchyService';
import { GroupOrgHierarchyResolver } from './GroupOrgHierarchyResolver';

@NgModule({
  imports: [
    FrameworkCoreModule,
    GroupOrgHierarchyRoutingModule,
    AgGridModule,
    AgGridSharedModule,
    TitleSharedModule,
    RbsSharedModule
  ],
  declarations: [
    GroupOrgHierarchyComponent,
    GroupOrgHierarchyListComponent

  ],
  providers: [
    GroupOrgHierarchyService,
    GroupOrgHierarchyResolver
  ]
})
export class GroupOrgHierarchyModule {
}
