import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { SaSystemOptionsListComponent } from "./SaSystemOptionsListComponent";
import { SaSystemOptionsResolver } from "./SaSystemOptionsResolver";
import { SaSystemOptionsComponent } from "./SaSystemOptionsComponent";


const DomainRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "", redirectTo: "List", pathMatch: "full"
      },
      {
        path: "List",
        component: SaSystemOptionsListComponent,
        resolve:
        {
          references: SaSystemOptionsResolver
        }
      },
      {
        path: "New",
        component: SaSystemOptionsComponent
      },

    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(DomainRoutes)
  ]
})
export class SaSystemOptionsRoutingModule { }
