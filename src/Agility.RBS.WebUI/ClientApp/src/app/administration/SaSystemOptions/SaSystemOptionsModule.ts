import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { SaSystemOptionsRoutingModule } from "./SaSystemOptionsRoutingModule";
import { SaSystemOptionsListComponent } from "./SaSystemOptionsListComponent";
import { SaSystemOptionsComponent } from "./SaSystemOptionsComponent"
import { SaSystemOptionsService } from "./SaSystemOptionsService";
import { SaSystemOptionsResolver } from "./SaSystemOptionsResolver";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { RbsSharedModule } from "../../RbsSharedModule";

@NgModule({
  imports: [
    FrameworkCoreModule,
    SaSystemOptionsRoutingModule,
    AgGridModule,
    AgGridSharedModule,
    TitleSharedModule,
    RbsSharedModule
  ],
  declarations: [
    SaSystemOptionsListComponent,
    SaSystemOptionsComponent

  ],
  providers: [
    SaSystemOptionsService,
    SaSystemOptionsResolver
  ]
})
export class SaSystemOptionsModule {
}
