export class SaSystemOptionsModel {
  daysBeforePurge?: number;
  dayPostSale: number;
  balanceLevelInd: string;
  maxDaysCompareDups?: number;
  compBaseDate?: number;
  compNoDays?: number;
  checkDupMissTran: string;
  unitOfWork: string;
  auditAfterImpInd: string;
  fuelDept: number;
  defaultChain: number;
  closeInOrder: string;
  escheatInd: string;
  partnerType: string;
  partnerId: string;
  autoValidateTranEmployeeId: string;
  viewSysCalcTotal: string;
  ccValReqd: string;
  wkstationTranAppendInd: string;
  ccSecLvlInd: string;
  updateId: string;
  operation: string;
}
