import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";
import { SaSystemOptionsModel } from './SaSystemOptionsModel';
@Injectable()
export class SaSystemOptionsService {
  serviceDocument: ServiceDocument<SaSystemOptionsModel> = new ServiceDocument<SaSystemOptionsModel>();
  searchData: any;
  constructor(private httpHelperService: HttpHelperService) { }

  newModel(model: SaSystemOptionsModel): ServiceDocument<SaSystemOptionsModel> {
    return this.serviceDocument.newModel(model);
  }

  list(): Observable<ServiceDocument<SaSystemOptionsModel>> {
    return this.serviceDocument.list("/api/SaSystemOptions/List");
  }

  save(): Observable<ServiceDocument<SaSystemOptionsModel>> {
    return this.serviceDocument.save("/api/SaSystemOptions/Save", true);
  }

  search(): Observable<ServiceDocument<SaSystemOptionsModel>> {
    return this.serviceDocument.list("/api/SaSystemOptions/List");
  }
}
