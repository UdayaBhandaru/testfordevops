import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { SaSystemOptionsService } from "./SaSystemOptionsService";
import { SaSystemOptionsModel } from './SaSystemOptionsModel';

@Injectable()
export class SaSystemOptionsResolver implements Resolve<ServiceDocument<SaSystemOptionsModel>> {
  constructor(private service: SaSystemOptionsService) { }
  resolve(): Observable<ServiceDocument<SaSystemOptionsModel>> {
    return this.service.list();
  }
}
