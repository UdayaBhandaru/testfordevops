import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { SaSystemOptionsModel } from "./SaSystemOptionsModel";
import { SaSystemOptionsService } from "./SaSystemOptionsService";
import { SharedService } from "../../Common/SharedService";
import { FormGroup } from "@angular/forms";

@Component({
  selector: "location-trait",
  templateUrl: "./SaSystemOptionsComponent.html"
})

export class SaSystemOptionsComponent implements OnInit {
  PageTitle = "SaSystemOptions";
  redirectUrl = "SaSystemOptions";
  serviceDocument: ServiceDocument<SaSystemOptionsModel>;
  editModel: any;
  editMode: boolean = false;
  localizationData: any;
  model: SaSystemOptionsModel;

  constructor(private service: SaSystemOptionsService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.bind();
  }

  bind(): void {
    delete this.editModel;
    if (this.sharedService.editObject) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
      this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
      delete this.sharedService.editObject;
      this.editMode = true;
    } else {
      this.model = {
        daysBeforePurge: null,
        dayPostSale: null,
        balanceLevelInd: null,
        maxDaysCompareDups: null,
        compBaseDate: null,
        compNoDays: null,
        checkDupMissTran: null,
        unitOfWork: null,
        auditAfterImpInd: null,
        fuelDept: null,
        defaultChain: null,
        closeInOrder: null,
        escheatInd: null,
        partnerType: null,
        partnerId: null,
        autoValidateTranEmployeeId: null,
        viewSysCalcTotal: null,
        ccValReqd: null,
        wkstationTranAppendInd: null,
        ccSecLvlInd: null,
        updateId: null,
        operation: null
      };
      this.service.serviceDocument.dataProfile.dataModel = this.model;
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  save(saveOnly: boolean): void {
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.saveLocalTrait(saveOnly);
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  saveLocalTrait(saveOnly: boolean): void {
    this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.sharedService.saveForm("SaSystemOptions saved successfully.").subscribe(() => {

          if (saveOnly) {
            this.router.navigate(["/" + this.redirectUrl + "/List"], { skipLocationChange: true });
          } else {
            if (!this.editMode) {
              this.model = {
                daysBeforePurge: null,
                dayPostSale: null,
                balanceLevelInd: null,
                maxDaysCompareDups: null,
                compBaseDate: null,
                compNoDays: null,
                checkDupMissTran: null,
                unitOfWork: null,
                auditAfterImpInd: null,
                fuelDept: null,
                defaultChain: null,
                closeInOrder: null,
                escheatInd: null,
                partnerType: null,
                partnerId: null,
                autoValidateTranEmployeeId: null,
                viewSysCalcTotal: null,
                ccValReqd: null,
                wkstationTranAppendInd: null,
                ccSecLvlInd: null,
                updateId: null,
                operation: null
              };
              this.service.serviceDocument.dataProfile.dataModel = this.model;
              this.service.newModel(this.model);
            } else {
              Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
              this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.service.serviceDocument.dataProfile.dataModel);
              this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            }
          }
        }, (error: string) => { console.log(error); });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
    }, (error: string) => { console.log(error); });
  }

  reset(): void {
    if (!this.editMode) {
      this.model = {
        daysBeforePurge: null,
        dayPostSale: null,
        balanceLevelInd: null,
        maxDaysCompareDups: null,
        compBaseDate: null,
        compNoDays: null,
        checkDupMissTran: null,
        unitOfWork: null,
        auditAfterImpInd: null,
        fuelDept: null,
        defaultChain: null,
        closeInOrder: null,
        escheatInd: null,
        partnerType: null,
        partnerId: null,
        autoValidateTranEmployeeId: null,
        viewSysCalcTotal: null,
        ccValReqd: null,
        wkstationTranAppendInd: null,
        ccSecLvlInd: null,
        updateId: null,
        operation: null
      };
      this.service.serviceDocument.dataProfile.dataModel = this.model;
      this.service.newModel(this.model);
    } else {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    }
    this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
  }
}
