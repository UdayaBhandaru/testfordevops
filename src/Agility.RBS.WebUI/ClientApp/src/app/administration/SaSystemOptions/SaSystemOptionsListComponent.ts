import { Component, OnInit } from "@angular/core";
import { ServiceDocument } from "@agility/frameworkcore";
import { SaSystemOptionsService } from "./SaSystemOptionsService";
import { SaSystemOptionsModel } from './SaSystemOptionsModel';
import { GridEditComponent } from '../../Common/grid/GridEditComponent';
import { InfoComponent } from '../../Common/InfoComponent';
import { SharedService } from '../../Common/SharedService';

@Component({
  selector: "SaSystemOptions-List",
  templateUrl: "./SaSystemOptionsListComponent.html"
})
export class SaSystemOptionsListComponent implements OnInit {

  public serviceDocument: ServiceDocument<SaSystemOptionsModel>;
  columns: any[];
  public componentName: any;
  model: SaSystemOptionsModel;
  public showSearchCriteria: boolean = false;
  constructor(public service: SaSystemOptionsService, private sharedService: SharedService) {
  }
  PageTitle = "SaSystemOptions";
  redirectUrl = "SaSystemOptions";

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "days Before Purge", field: "daysBeforePurge", tooltipField: "daysBeforePurge" },
      { headerName: "day Post Sale", field: "dayPostSale", tooltipField: "dayPostSale" },
      { headerName: "balance Level Ind", field: "balanceLevelInd", tooltipField: "balanceLevelInd" },
      { headerName: "max Days Compare Dups", field: "maxDaysCompareDups", tooltipField: "maxDaysCompareDups" },
      { headerName: "comp Base Date", field: "compBaseDate", tooltipField: "compBaseDate" },
      { headerName: "comp No Days", field: "compNoDays", tooltipField: "compNoDays" },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 90
      }
    ];
   
    this.serviceDocument = this.service.serviceDocument;
  }

  open(cell: any, mode: string): void {
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

}
