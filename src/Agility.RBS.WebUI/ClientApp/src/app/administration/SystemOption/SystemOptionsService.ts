import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";
import { SystemOptionsModel } from './SystemOptionsModel';
@Injectable()
export class SystemOptionsService {
  serviceDocument: ServiceDocument<SystemOptionsModel> = new ServiceDocument<SystemOptionsModel>();
  searchData: any;
  constructor(private httpHelperService: HttpHelperService) { }

  newModel(model: SystemOptionsModel): ServiceDocument<SystemOptionsModel> {
    return this.serviceDocument.newModel(model);
  }

  list(): Observable<ServiceDocument<SystemOptionsModel>> {
    return this.serviceDocument.list("/api/SystemOptions/List");
  }

  save(): Observable<ServiceDocument<SystemOptionsModel>> {
    return this.serviceDocument.save("/api/SystemOptions/Save", true);
  }
}
