import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { SystemOptionsListComponent } from "./SystemOptionsListComponent";
import { SystemOptionsResolver } from "./SystemOptionsResolver";
import { SystemOptionsComponent } from "./SystemOptionsComponent";


const DomainRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "", redirectTo: "List", pathMatch: "full"
      },
      {
        path: "List",
        component: SystemOptionsListComponent,
        resolve:
        {
          references: SystemOptionsResolver
        }
      },
      {
        path: "New",
        component: SystemOptionsComponent
      },

    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(DomainRoutes)
  ]
})
export class SystemOptionsRoutingModule { }
