import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { SystemOptionsRoutingModule } from "./SystemOptionsRoutingModule";
import { SystemOptionsListComponent } from "./SystemOptionsListComponent";
import { SystemOptionsComponent } from "./SystemOptionsComponent"
import { SystemOptionsService } from "./SystemOptionsService";
import { SystemOptionsResolver } from "./SystemOptionsResolver";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { RbsSharedModule } from "../../RbsSharedModule";

@NgModule({
  imports: [
    FrameworkCoreModule,
    SystemOptionsRoutingModule,
    AgGridModule,
    AgGridSharedModule,
    TitleSharedModule,
    RbsSharedModule
  ],
  declarations: [
    SystemOptionsListComponent,
    SystemOptionsComponent

  ],
  providers: [
    SystemOptionsService,
    SystemOptionsResolver
  ]
})
export class SystemOptionsModule {
}
