export class SystemOptionsModel {
  debitMemoSendDays?: number;
  closeOpenReceiptDays?: number;
  costResolutionDueDays?: number;
  qtyResolutionDueDays?: number;
  docHistDays?: number;
  debitMemoPrefixCost: string;
  debitMemoPrefixQty: string;
  debitMemoPrefixVat: string;
  creditMemoPrefixCost: string;
  creditMemoPrefixQty: string;
  creditNoteReqPrefixCost: string;
  creditNoteReqPrefixQty: string;
  creditNoteReqPrefixVat: string;
  postDatedDocDays?: number;
  maxTolerancePct?: number;
  daysBeforeDueDate?: number;
  defaultPayNowTerms: string;
  vatInd: string;
  calcTolerance?: number;
  vatValidationType: string;
  vatDocumentCreationLvl: string;
  defaultVatHeader: string;
  vatResolutionDueDays: number;
  calcToleranceIndicator: string;
  postBasedOnDocHeader: string;
  operation: string;
}
