import { Component, OnInit } from "@angular/core";
import { ServiceDocument } from "@agility/frameworkcore";
import { SystemOptionsService } from "./SystemOptionsService";
import { SystemOptionsModel } from './SystemOptionsModel';
import { GridEditComponent } from '../../Common/grid/GridEditComponent';
import { InfoComponent } from '../../Common/InfoComponent';
import { SharedService } from '../../Common/SharedService';

@Component({
  selector: "SystemOptions-List",
  templateUrl: "./SystemOptionsListComponent.html"
})
export class SystemOptionsListComponent implements OnInit {

  public serviceDocument: ServiceDocument<SystemOptionsModel>;
  columns: any[];
  public componentName: any;
  constructor(public service: SystemOptionsService, private sharedService: SharedService) {
  }
  PageTitle = "IM_System Options";
  redirectUrl = "systemoptions";
  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "Close Open Receipt Days", field: "closeOpenReceiptDays", tooltipField: "closeOpenReceiptDays " },
      { headerName: "Cost Resolution Due Days", field: "costResolutionDueDays", tooltipField: "description" },
      { headerName: "Debit Memo Prefix Cost", field: "debitMemoPrefixCost", tooltipField: "debitMemoPrefixCost" },
      { headerName: "Credit Memo Prefix Cost", field: "creditMemoPrefixCost", tooltipField: "creditMemoPrefixCost" },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 90
      }
    ];
    this.serviceDocument = this.service.serviceDocument;
  }

  open(cell: any, mode: string): void {
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }
}
