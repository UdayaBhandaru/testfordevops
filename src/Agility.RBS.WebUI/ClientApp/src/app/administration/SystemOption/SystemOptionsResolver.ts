import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { SystemOptionsService } from "./SystemOptionsService";
import { SystemOptionsModel } from './SystemOptionsModel';

@Injectable()
export class SystemOptionsResolver implements Resolve<ServiceDocument<SystemOptionsModel>> {
  constructor(private service: SystemOptionsService) { }
  resolve(): Observable<ServiceDocument<SystemOptionsModel>> {
    return this.service.list();
  }
}
