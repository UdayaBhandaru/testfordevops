import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { RbsSharedModule } from "../../RbsSharedModule";
import { SecurityGroupComponent } from './SecurityGroupComponent';
import { SecurityGroupListComponent } from './SecurityGroupListComponent';
import { SecurityGroupRoutingModule } from './SecurityGroupRoutingModule';
import { SecurityGroupService } from './SecurityGroupService';
import { SecurityGroupResolver } from './SecurityGroupResolver';

@NgModule({
  imports: [
    FrameworkCoreModule,
    SecurityGroupRoutingModule,
    AgGridModule,
    AgGridSharedModule,
    TitleSharedModule,
    RbsSharedModule
  ],
  declarations: [
    SecurityGroupComponent,
    SecurityGroupListComponent

  ],
  providers: [
    SecurityGroupService,
    SecurityGroupResolver
  ]
})
export class SecurityGroupModule {
}
