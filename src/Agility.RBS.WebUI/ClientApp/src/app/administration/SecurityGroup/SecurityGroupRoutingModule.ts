import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { SecurityGroupComponent } from './SecurityGroupComponent';
import { SecurityGroupResolver } from './SecurityGroupResolver';
import { SecurityGroupListComponent } from './SecurityGroupListComponent';


const DomainRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "", redirectTo: "List", pathMatch: "full"
      },
      {
        path: "List",
        component: SecurityGroupListComponent,
        resolve:
        {
          references: SecurityGroupResolver
        }
      },
      {
        path: "New/:id",
        component: SecurityGroupComponent
      },

    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(DomainRoutes)
  ]
})
export class SecurityGroupRoutingModule { }
