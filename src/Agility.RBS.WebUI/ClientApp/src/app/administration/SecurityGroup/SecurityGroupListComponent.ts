import { Component, OnInit } from "@angular/core";
import { ServiceDocument } from "@agility/frameworkcore";
import { GridEditComponent } from '../../Common/grid/GridEditComponent';
import { InfoComponent } from '../../Common/InfoComponent';
import { SharedService } from '../../Common/SharedService';
import { SecurityGroupService } from './SecurityGroupService';
import { SecurityGroupModel } from './SecurityGroupModel';
import { Router } from '@angular/router';

@Component({
  selector: "securitygroup-List",
  templateUrl: "./SecurityGroupListComponent.html"
})
export class SecurityGroupListComponent implements OnInit {

  public serviceDocument: ServiceDocument<SecurityGroupModel>;
  columns: any[];
  public componentName: any;
  model: SecurityGroupModel;
  public showSearchCriteria: boolean = true;

  constructor(public service: SecurityGroupService, private sharedService: SharedService, public router: Router) {
  }
  pageTitle = "Security Group";
  redirectUrl = "securitygroup/New/";

  ngOnInit(): void {
    debugger;
    this.model = { comments: null, groupId: null, groupName: null, role: null, operation: null };
    this.componentName = this;
    this.columns = [
      { headerName: "groupId", field: "groupId", tooltipField: "groupId" },
      { headerName: "groupName", field: "groupName", tooltipField: "groupName" },
      { headerName: "role", field: "role", tooltipField: "role" },
      { headerName: "comments", field: "comments", tooltipField: "comments" },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 90
      }
    ];
    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
      this.sharedService.previousUrl = "../securitygroup/List";
    }

    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl + cell.data.groupId
      , this.service.serviceDocument.dataProfile.dataList);
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }
  addValidation(evnt: any): void {
    this.navigateToAdd();
  }

  private navigateToAdd(): void {
    this.router.navigate(["/" + this.redirectUrl + "0"], { skipLocationChange: true });
    this.sharedService.previousUrl = "../securitygroup/List";
  }
}
