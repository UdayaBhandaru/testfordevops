import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";
import { SecurityGroupModel } from './SecurityGroupModel';
@Injectable()
export class SecurityGroupService {
  serviceDocument: ServiceDocument<SecurityGroupModel> = new ServiceDocument<SecurityGroupModel>();
  searchData: any;
  constructor(private httpHelperService: HttpHelperService) { }

  newModel(model: SecurityGroupModel): ServiceDocument<SecurityGroupModel> {
    return this.serviceDocument.newModel(model);
  }

  list(): Observable<ServiceDocument<SecurityGroupModel>> {
    return this.serviceDocument.list("/api/SecurityGroup/List");
  }

  save(): Observable<ServiceDocument<SecurityGroupModel>> {
    return this.serviceDocument.save("/api/SecurityGroup/Save", true);
  }

  search(): Observable<ServiceDocument<SecurityGroupModel>> {
    return this.serviceDocument.search("/api/SecurityGroup/List");
  }
}
