import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { SecurityGroupModel } from './SecurityGroupModel';
import { SecurityGroupService } from './SecurityGroupService';

@Injectable()
export class SecurityGroupResolver implements Resolve<ServiceDocument<SecurityGroupModel>> {
  constructor(private service: SecurityGroupService) { }
  resolve(): Observable<ServiceDocument<SecurityGroupModel>> {
    return this.service.list();
  }
}
