export class SecurityGroupModel {
  groupId: number;
  groupName: string;
  role: string;
  comments: string;
  operation: string;
}
