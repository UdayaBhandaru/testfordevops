import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { RegionalityUserGroupComponent } from './RegionalityUserGroupComponent';
import { RegionalityUserGroupResolver } from './RegionalityUserGroupResolver';
import { RegionalityUserGroupListComponent } from './RegionalityUserGroupListComponent';


const DomainRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "", redirectTo: "List", pathMatch: "full"
      },
      {
        path: "List",
        component: RegionalityUserGroupListComponent,
        resolve:
        {
          references: RegionalityUserGroupResolver
        }
      },
      {
        path: "New/:id",
        component: RegionalityUserGroupComponent
      },

    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(DomainRoutes)
  ]
})
export class RegionalityUserGroupRoutingModule { }
