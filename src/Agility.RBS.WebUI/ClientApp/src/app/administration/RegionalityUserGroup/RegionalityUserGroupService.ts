import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";
import { RegionalityUserGroupModel } from './RegionalityUserGroupModel';
@Injectable()
export class RegionalityUserGroupService {
  serviceDocument: ServiceDocument<RegionalityUserGroupModel> = new ServiceDocument<RegionalityUserGroupModel>();
  searchData: any;
  constructor(private httpHelperService: HttpHelperService) { }

  newModel(model: RegionalityUserGroupModel): ServiceDocument<RegionalityUserGroupModel> {
    return this.serviceDocument.newModel(model);
  }

  list(): Observable<ServiceDocument<RegionalityUserGroupModel>> {
    return this.serviceDocument.list("/api/SecurityUserGroup/List");
  }

  save(): Observable<ServiceDocument<RegionalityUserGroupModel>> {
    return this.serviceDocument.save("/api/SecurityUserGroup/Save", true);
  }

  search(): Observable<ServiceDocument<RegionalityUserGroupModel>> {
    return this.serviceDocument.search("/api/SecurityUserGroup/List");
  }
}
