import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { RegionalityUserGroupModel } from './RegionalityUserGroupModel';
import { RegionalityUserGroupService } from './RegionalityUserGroupService';

@Injectable()
export class RegionalityUserGroupResolver implements Resolve<ServiceDocument<RegionalityUserGroupModel>> {
  constructor(private service: RegionalityUserGroupService) { }
  resolve(): Observable<ServiceDocument<RegionalityUserGroupModel>> {
    return this.service.list();
  }
}
