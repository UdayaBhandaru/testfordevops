export class RegionalityUserGroupModel {
  groupId: number;
  groupName: string;
  operation?: string;
  userId: string;
  userName: string;
}
