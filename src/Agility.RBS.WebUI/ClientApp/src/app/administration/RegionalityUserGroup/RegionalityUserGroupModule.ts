import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { RbsSharedModule } from "../../RbsSharedModule";
import { RegionalityUserGroupComponent } from './RegionalityUserGroupComponent';
import { RegionalityUserGroupListComponent } from './RegionalityUserGroupListComponent';
import { RegionalityUserGroupRoutingModule } from './RegionalityUserGroupRoutingModule';
import { RegionalityUserGroupService } from './RegionalityUserGroupService';
import { RegionalityUserGroupResolver } from './RegionalityUserGroupResolver';

@NgModule({
  imports: [
    FrameworkCoreModule,
    RegionalityUserGroupRoutingModule,
    AgGridModule,
    AgGridSharedModule,
    TitleSharedModule,
    RbsSharedModule
  ],
  declarations: [
    RegionalityUserGroupComponent,
    RegionalityUserGroupListComponent

  ],
  providers: [
    RegionalityUserGroupService,
    RegionalityUserGroupResolver
  ]
})
export class RegionalityUserGroupModule {
}
