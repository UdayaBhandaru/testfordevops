import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { RpmSystemOptionsService } from "./RpmSystemOptionsService";
import { RpmSystemOptionsModel } from './RpmSystemOptionsModel';

@Injectable()
export class RpmSystemOptionsResolver implements Resolve<ServiceDocument<RpmSystemOptionsModel>> {
  constructor(private service: RpmSystemOptionsService) { }
  resolve(): Observable<ServiceDocument<RpmSystemOptionsModel>> {
    return this.service.list();
  }
}
