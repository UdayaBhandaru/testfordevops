import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { RpmSystemOptionsRoutingModule } from "./RpmSystemOptionsRoutingModule";
import { RpmSystemOptionsListComponent } from "./RpmSystemOptionsListComponent";
import { RpmSystemOptionsComponent } from "./RpmSystemOptionsComponent"
import { RpmSystemOptionsService } from "./RpmSystemOptionsService";
import { RpmSystemOptionsResolver } from "./RpmSystemOptionsResolver";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { RbsSharedModule } from "../../RbsSharedModule";

@NgModule({
  imports: [
    FrameworkCoreModule,
    RpmSystemOptionsRoutingModule,
    AgGridModule,
    AgGridSharedModule,
    TitleSharedModule,
    RbsSharedModule
  ],
  declarations: [
    RpmSystemOptionsListComponent,
    RpmSystemOptionsComponent

  ],
  providers: [
    RpmSystemOptionsService,
    RpmSystemOptionsResolver
  ]
})
export class RpmSystemOptionsModule {
}
