import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { RpmSystemOptionsModel } from "./RpmSystemOptionsModel";
import { RpmSystemOptionsService } from "./RpmSystemOptionsService";
import { SharedService } from "../../Common/SharedService";
import { FormGroup } from "@angular/forms";

@Component({
  selector: "location-trait",
  templateUrl: "./RpmSystemOptionsComponent.html"
})

export class RpmSystemOptionsComponent implements OnInit {
  PageTitle = "RpmSystemOptions";
  redirectUrl = "RpmSystemOptions";
  serviceDocument: ServiceDocument<RpmSystemOptionsModel>;
  editModel: any;
  editMode: boolean = false;
  localizationData: any;
  model: RpmSystemOptionsModel;

  constructor(private service: RpmSystemOptionsService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.bind();
  }

  bind(): void {
    delete this.editModel;
    if (this.sharedService.editObject) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
      this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
      delete this.sharedService.editObject;
      this.editMode = true;
    } else {
      this.model = {
        systemOptionsId:null,
        clearanceHistMonths: null,
        complexPromoAllowedInd: null,
        costCalculationMethod: null,
        defaultOutOfStockDays: null,
        defaultResetDate: null,
        eventIdRequired: null,
        multiItemLocPromoInd: null,
        openZoneUse: null,
        priceChangeProcessingDays: null,
        priceChangePromoOverlapInd: null,
        promoApplyOrder: null,
        promotionHistMonths: null,
        rejectHoldDaysPcClear: null,
        rejectHoldDaysPromo: null,
        recognizeWhAsLocations: null,
        salesCalculationMethod: null,
        updateItemAttributes: null,
        zeroCurrEndsInDecs: null,
        simInd: null,
        zoneRanging: null,
        exactDealDates: null,
        locationMoveProcessingDays: null,
        locationMovePurgeDays: null,
        dynamicAreaDiffInd: null,
        priceClrBckgrndCnflctInd: null,
        promoBckgrndCnflctInd: null,
        wrkshtBckgrndCnflctInd: null,
        allowBuyGetCyclesInd: null,
        lockVersion: null,
        dispPromHeadMkup: null,
        displayConflictsOnly: null,
        conflictHistoryDaysBefore: null,
        conflictHistoryDaysAfter: null,
        filterPriceChangeResults: null,
        fullPromoColDetail: null,
        fullPcColDetail: null,
        pcClrCalcMarkupInd: null,
        priceChangeSearchMax: null,
        clearanceSearchMax: null,
        promotionSearchMax: null,
        futRetSeedDaysBefore: null,
        defaultEffectiveDate: null,
        priceinquirySearchMax: null,
        promoEndDateRequired: null,
        clearanceResetInquiryMax: null,
        operation: null
      };
      this.service.serviceDocument.dataProfile.dataModel = this.model;
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  save(saveOnly: boolean): void {
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.saveLocalTrait(saveOnly);
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  saveLocalTrait(saveOnly: boolean): void {
    this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.sharedService.saveForm("RpmSystemOptions saved successfully.").subscribe(() => {

          if (saveOnly) {
            this.router.navigate(["/" + this.redirectUrl + "/List"], { skipLocationChange: true });
          } else {
            if (!this.editMode) {
              this.model = {
                systemOptionsId: null,
                clearanceHistMonths: null,
                complexPromoAllowedInd: null,
                costCalculationMethod: null,
                defaultOutOfStockDays: null,
                defaultResetDate: null,
                eventIdRequired: null,
                multiItemLocPromoInd: null,
                openZoneUse: null,
                priceChangeProcessingDays: null,
                priceChangePromoOverlapInd: null,
                promoApplyOrder: null,
                promotionHistMonths: null,
                rejectHoldDaysPcClear: null,
                rejectHoldDaysPromo: null,
                recognizeWhAsLocations: null,
                salesCalculationMethod: null,
                updateItemAttributes: null,
                zeroCurrEndsInDecs: null,
                simInd: null,
                zoneRanging: null,
                exactDealDates: null,
                locationMoveProcessingDays: null,
                locationMovePurgeDays: null,
                dynamicAreaDiffInd: null,
                priceClrBckgrndCnflctInd: null,
                promoBckgrndCnflctInd: null,
                wrkshtBckgrndCnflctInd: null,
                allowBuyGetCyclesInd: null,
                lockVersion: null,
                dispPromHeadMkup: null,
                displayConflictsOnly: null,
                conflictHistoryDaysBefore: null,
                conflictHistoryDaysAfter: null,
                filterPriceChangeResults: null,
                fullPromoColDetail: null,
                fullPcColDetail: null,
                pcClrCalcMarkupInd: null,
                priceChangeSearchMax: null,
                clearanceSearchMax: null,
                promotionSearchMax: null,
                futRetSeedDaysBefore: null,
                defaultEffectiveDate: null,
                priceinquirySearchMax: null,
                promoEndDateRequired: null,
                clearanceResetInquiryMax: null,
                operation: null
              };
              this.service.serviceDocument.dataProfile.dataModel = this.model;
              this.service.newModel(this.model);
            } else {
              Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
              this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.service.serviceDocument.dataProfile.dataModel);
              this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            }
          }
        }, (error: string) => { console.log(error); });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
    }, (error: string) => { console.log(error); });
  }

  reset(): void {
    if (!this.editMode) {
      this.model = {
        systemOptionsId: null,
        clearanceHistMonths: null,
        complexPromoAllowedInd: null,
        costCalculationMethod: null,
        defaultOutOfStockDays: null,
        defaultResetDate: null,
        eventIdRequired: null,
        multiItemLocPromoInd: null,
        openZoneUse: null,
        priceChangeProcessingDays: null,
        priceChangePromoOverlapInd: null,
        promoApplyOrder: null,
        promotionHistMonths: null,
        rejectHoldDaysPcClear: null,
        rejectHoldDaysPromo: null,
        recognizeWhAsLocations: null,
        salesCalculationMethod: null,
        updateItemAttributes: null,
        zeroCurrEndsInDecs: null,
        simInd: null,
        zoneRanging: null,
        exactDealDates: null,
        locationMoveProcessingDays: null,
        locationMovePurgeDays: null,
        dynamicAreaDiffInd: null,
        priceClrBckgrndCnflctInd: null,
        promoBckgrndCnflctInd: null,
        wrkshtBckgrndCnflctInd: null,
        allowBuyGetCyclesInd: null,
        lockVersion: null,
        dispPromHeadMkup: null,
        displayConflictsOnly: null,
        conflictHistoryDaysBefore: null,
        conflictHistoryDaysAfter: null,
        filterPriceChangeResults: null,
        fullPromoColDetail: null,
        fullPcColDetail: null,
        pcClrCalcMarkupInd: null,
        priceChangeSearchMax: null,
        clearanceSearchMax: null,
        promotionSearchMax: null,
        futRetSeedDaysBefore: null,
        defaultEffectiveDate: null,
        priceinquirySearchMax: null,
        promoEndDateRequired: null,
        clearanceResetInquiryMax: null,
        operation: null
      };
      this.service.serviceDocument.dataProfile.dataModel = this.model;
      this.service.newModel(this.model);
    } else {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    }
    this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
  }
}
