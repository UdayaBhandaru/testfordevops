import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { RpmSystemOptionsListComponent } from "./RpmSystemOptionsListComponent";
import { RpmSystemOptionsResolver } from "./RpmSystemOptionsResolver";
import { RpmSystemOptionsComponent } from "./RpmSystemOptionsComponent";


const DomainRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "", redirectTo: "List", pathMatch: "full"
      },
      {
        path: "List",
        component: RpmSystemOptionsListComponent,
        resolve:
        {
          references: RpmSystemOptionsResolver
        }
      },
      {
        path: "New",
        component: RpmSystemOptionsComponent
      },

    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(DomainRoutes)
  ]
})
export class RpmSystemOptionsRoutingModule { }
