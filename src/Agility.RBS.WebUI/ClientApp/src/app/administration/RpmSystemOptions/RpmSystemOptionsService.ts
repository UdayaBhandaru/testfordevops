import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";
import { RpmSystemOptionsModel } from './RpmSystemOptionsModel';
@Injectable()
export class RpmSystemOptionsService {
  serviceDocument: ServiceDocument<RpmSystemOptionsModel> = new ServiceDocument<RpmSystemOptionsModel>();
  searchData: any;
  constructor(private httpHelperService: HttpHelperService) { }

  newModel(model: RpmSystemOptionsModel): ServiceDocument<RpmSystemOptionsModel> {
    return this.serviceDocument.newModel(model);
  }

  list(): Observable<ServiceDocument<RpmSystemOptionsModel>> {
    return this.serviceDocument.list("/api/RpmSystemOptions/List");
  }

  save(): Observable<ServiceDocument<RpmSystemOptionsModel>> {
    return this.serviceDocument.save("/api/RpmSystemOptions/Save", true);
  }

  search(): Observable<ServiceDocument<RpmSystemOptionsModel>> {
    return this.serviceDocument.list("/api/RpmSystemOptions/List");
  }
}
