import { Component, OnInit } from "@angular/core";
import { ServiceDocument } from "@agility/frameworkcore";
import { RpmSystemOptionsService } from "./RpmSystemOptionsService";
import { RpmSystemOptionsModel } from './RpmSystemOptionsModel';
import { GridEditComponent } from '../../Common/grid/GridEditComponent';
import { InfoComponent } from '../../Common/InfoComponent';
import { SharedService } from '../../Common/SharedService';

@Component({
  selector: "RpmSystemOptions-List",
  templateUrl: "./RpmSystemOptionsListComponent.html"
})
export class RpmSystemOptionsListComponent implements OnInit {

  public serviceDocument: ServiceDocument<RpmSystemOptionsModel>;
  columns: any[];
  public componentName: any;
  model: RpmSystemOptionsModel;
  public showSearchCriteria: boolean = false;
  constructor(public service: RpmSystemOptionsService, private sharedService: SharedService) {
  }
  PageTitle = "RpmSystemOptions";
  redirectUrl = "RpmSystemOptions";

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "system Options Id", field: "systemOptionsId", tooltipField: "systemOptionsId" },
      { headerName: "clearance Hist Months", field: "clearanceHistMonths", tooltipField: "clearanceHistMonths" },
      { headerName: "complex Promo Allowed Ind", field: "complexPromoAllowedInd", tooltipField: "complexPromoAllowedInd" },
      { headerName: "cost Calculation Method", field: "costCalculationMethod", tooltipField: "costCalculationMethod" },
      { headerName: "default Out Of Stock Days", field: "defaultOutOfStockDays", tooltipField: "defaultOutOfStockDays" },
      { headerName: "default Reset Date", field: "defaultResetDate", tooltipField: "defaultResetDate" }, 
      {
          headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 90
      }
    ];
    
    this.serviceDocument = this.service.serviceDocument;
  }

  open(cell: any, mode: string): void {
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

}
