import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { NbSystemOptionsRoutingModule } from "./NbSystemOptionsRoutingModule";
import { NbSystemOptionsListComponent } from "./NbSystemOptionsListComponent";
import { NbSystemOptionsComponent } from "./NbSystemOptionsComponent"
import { NbSystemOptionsService } from "./NbSystemOptionsService";
import { NbSystemOptionsResolver } from "./NbSystemOptionsResolver";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { RbsSharedModule } from "../../RbsSharedModule";

@NgModule({
  imports: [
    FrameworkCoreModule,
    NbSystemOptionsRoutingModule,
    AgGridModule,
    AgGridSharedModule,
    TitleSharedModule,
    RbsSharedModule
  ],
  declarations: [
    NbSystemOptionsListComponent,
    NbSystemOptionsComponent

  ],
  providers: [
    NbSystemOptionsService,
    NbSystemOptionsResolver
  ]
})
export class NbSystemOptionsModule {
}
