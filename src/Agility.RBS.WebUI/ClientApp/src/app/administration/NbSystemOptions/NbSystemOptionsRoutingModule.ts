import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { NbSystemOptionsListComponent } from "./NbSystemOptionsListComponent";
import { NbSystemOptionsResolver } from "./NbSystemOptionsResolver";
import { NbSystemOptionsComponent } from "./NbSystemOptionsComponent";


const DomainRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "", redirectTo: "List", pathMatch: "full"
      },
      {
        path: "List",
        component: NbSystemOptionsListComponent,
        resolve:
        {
          references: NbSystemOptionsResolver
        }
      },
      {
        path: "New",
        component: NbSystemOptionsComponent
      },

    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(DomainRoutes)
  ]
})
export class NbSystemOptionsRoutingModule { }
