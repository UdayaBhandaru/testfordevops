import { Component, OnInit } from "@angular/core";
import { ServiceDocument } from "@agility/frameworkcore";
import { NbSystemOptionsService } from "./NbSystemOptionsService";
import { NbSystemOptionsModel } from './NbSystemOptionsModel';
import { GridEditComponent } from '../../Common/grid/GridEditComponent';
import { InfoComponent } from '../../Common/InfoComponent';
import { SharedService } from '../../Common/SharedService';

@Component({
  selector: "NbSystemOptions-List",
  templateUrl: "./NbSystemOptionsListComponent.html"
})
export class NbSystemOptionsListComponent implements OnInit {

  public serviceDocument: ServiceDocument<NbSystemOptionsModel>;
  columns: any[];
  public componentName: any;
  model: NbSystemOptionsModel;
  public showSearchCriteria: boolean = false;
  constructor(public service: NbSystemOptionsService, private sharedService: SharedService) {
  }
  PageTitle = "NbSystemOptions";
  redirectUrl = "NbSystemOptions";

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "trans formable CustInd", field: "transformableCustInd", tooltipField: "transformableCustInd" },
      { headerName: "check Payterm Enabled Indn", field: "checkPaytermEnabledIndn", tooltipField: "checkPaytermEnabledIndn" },
      { headerName: "dsd Lines PerPo", field: "dsdLinesPerPo", tooltipField: "dsdLinesPerPo" },

      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 90
      }
    ];
    
    this.serviceDocument = this.service.serviceDocument;
  }

  open(cell: any, mode: string): void {
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

}
