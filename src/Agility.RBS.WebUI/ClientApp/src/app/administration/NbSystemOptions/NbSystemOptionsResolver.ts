import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { NbSystemOptionsService } from "./NbSystemOptionsService";
import { NbSystemOptionsModel } from './NbSystemOptionsModel';

@Injectable()
export class NbSystemOptionsResolver implements Resolve<ServiceDocument<NbSystemOptionsModel>> {
  constructor(private service: NbSystemOptionsService) { }
  resolve(): Observable<ServiceDocument<NbSystemOptionsModel>> {
    return this.service.list();
  }
}
