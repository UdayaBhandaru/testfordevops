import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";
import { NbSystemOptionsModel } from './NbSystemOptionsModel';
@Injectable()
export class NbSystemOptionsService {
  serviceDocument: ServiceDocument<NbSystemOptionsModel> = new ServiceDocument<NbSystemOptionsModel>();
  searchData: any;
  constructor(private httpHelperService: HttpHelperService) { }

  newModel(model: NbSystemOptionsModel): ServiceDocument<NbSystemOptionsModel> {
    return this.serviceDocument.newModel(model);
  }

  list(): Observable<ServiceDocument<NbSystemOptionsModel>> {
    return this.serviceDocument.list("/api/NbSystemOptions/List");
  }

  save(): Observable<ServiceDocument<NbSystemOptionsModel>> {
    return this.serviceDocument.save("/api/NbSystemOptions/Save", true);
  }

  search(): Observable<ServiceDocument<NbSystemOptionsModel>> {
    return this.serviceDocument.list("/api/NbSystemOptions/List");
  }
}
