export class NbSystemOptionsModel {
  transformableCustInd: string;
  checkPaytermEnabledIndn: string;
  dsdLinesPerPo: string;  
  operation: string;
}
