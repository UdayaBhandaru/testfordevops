import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { SupplierOptionsListComponent } from "./SupplierOptionsListComponent";
import { SupplierOptionsResolver } from "./SupplierOptionsResolver";
import { SupplierOptionsComponent } from "./SupplierOptionsComponent"
const DomainRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "", redirectTo: "List", pathMatch: "full"
      },
      {
        path: "List",
        component: SupplierOptionsListComponent
      },
      {
        path: "New",
        component: SupplierOptionsComponent
      }

    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(DomainRoutes)
  ]
})
export class SupplierOptionsRoutingModule { }
