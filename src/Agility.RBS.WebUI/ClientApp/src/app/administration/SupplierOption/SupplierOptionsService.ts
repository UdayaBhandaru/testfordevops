import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { HttpHelperService } from "../../Common/HttpHelperService";
import { SupplierOptionsModel } from './SupplierOptionsModel';
@Injectable()
export class SupplierOptionsService {
  serviceDocument: ServiceDocument<SupplierOptionsModel> = new ServiceDocument<SupplierOptionsModel>();
  searchData: any;
  constructor(private httpHelperService: HttpHelperService) { }

  newModel(model: SupplierOptionsModel): ServiceDocument<SupplierOptionsModel> {
    return this.serviceDocument.newModel(model);
  }

  list(): Observable<ServiceDocument<SupplierOptionsModel>> {
    return this.serviceDocument.list("/api/SupplierOptions/List");
  }

  save(): Observable<ServiceDocument<SupplierOptionsModel>> {
    return this.serviceDocument.save("/api/SupplierOptions/Save", true);
  }

  search(): Observable<ServiceDocument<SupplierOptionsModel>> {
    return this.serviceDocument.search("/api/SupplierOptions/List");
  }
}
