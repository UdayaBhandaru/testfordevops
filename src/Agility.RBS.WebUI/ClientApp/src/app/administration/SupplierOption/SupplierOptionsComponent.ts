import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { SupplierOptionsModel } from "./SupplierOptionsModel";
import { SupplierOptionsService } from "./SupplierOptionsService";
import { SharedService } from "../../Common/SharedService";

@Component({
  selector: "supplier-trait",
  templateUrl: "./SupplierOptionsComponent.html"
})

export class SupplierOptionsComponent implements OnInit {
  PageTitle = "Supplier Options";
  redirectUrl = "supplieroptions";
  serviceDocument: ServiceDocument<SupplierOptionsModel>;
  editModel: any;
  editMode: boolean = false;
  localizationData: any;
  model: SupplierOptionsModel;

  constructor(private service: SupplierOptionsService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.bind();
  }

  bind(): void {
    delete this.editModel;
    if (this.sharedService.editObject) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
      this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
      delete this.sharedService.editObject;
      this.editMode = true;
    } else {
      this.model = { supplierName: null, supplier: null, sendDebitMemo: null, manuallyPaidIndicator: null, useInvoceTermsIndicator: null, rogDateAllowedIndicator: null, apReviewer: null, closeOpenShipmentDays: null, matchRecieptsOthersuppLiersIndicator: null, qtyDiscDayBeforeRte: null, operation: null };
      this.service.serviceDocument.dataProfile.dataModel = this.model;
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  save(saveOnly: boolean): void {
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.saveLocalTrait(saveOnly);
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  saveLocalTrait(saveOnly: boolean): void {
    this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.sharedService.saveForm("Supplier options saved successfully.").subscribe(() => {
          if (saveOnly) {
            this.router.navigate(["/" + this.redirectUrl + "/List"], { skipLocationChange: true });
          } else {
            if (!this.editMode) {
              this.model = { supplierName: null, supplier: null, sendDebitMemo: null, manuallyPaidIndicator: null, useInvoceTermsIndicator: null, rogDateAllowedIndicator: null, apReviewer: null, closeOpenShipmentDays: null, matchRecieptsOthersuppLiersIndicator: null, qtyDiscDayBeforeRte: null, operation: null };
              this.service.serviceDocument.dataProfile.dataModel = this.model;
              this.service.newModel(this.model);
            } else {
              Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
              this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.service.serviceDocument.dataProfile.dataModel);
              this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            }
          }
        }, (error: string) => { console.log(error); });

      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
    }, (error: string) => { console.log(error); });
  }

  reset(): void {
    if (!this.editMode) {
      this.model = { supplierName: null, supplier: null, sendDebitMemo: null, manuallyPaidIndicator: null, useInvoceTermsIndicator: null, rogDateAllowedIndicator: null, apReviewer: null, closeOpenShipmentDays: null, matchRecieptsOthersuppLiersIndicator: null, qtyDiscDayBeforeRte: null, operation: null };
      this.service.serviceDocument.dataProfile.dataModel = this.model;
      this.service.newModel(this.model);
    } else {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    }
    this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
  }
}
