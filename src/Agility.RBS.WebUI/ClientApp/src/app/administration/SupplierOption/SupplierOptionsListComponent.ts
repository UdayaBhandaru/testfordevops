import { Component, OnInit } from "@angular/core";
import { ServiceDocument } from "@agility/frameworkcore";
import { SupplierOptionsService } from "./SupplierOptionsService";
import { SupplierOptionsModel } from './SupplierOptionsModel';
import { GridEditComponent } from '../../Common/grid/GridEditComponent';
import { InfoComponent } from '../../Common/InfoComponent';
import { SharedService } from '../../Common/SharedService';

@Component({
  selector: "SupplierOptions-List",
  templateUrl: "./SupplierOptionsListComponent.html"
})
export class SupplierOptionsListComponent implements OnInit {

  public serviceDocument: ServiceDocument<SupplierOptionsModel>;
  columns: any[];
  public componentName: any;
  model: SupplierOptionsModel;
  public showSearchCriteria: boolean = true;
  constructor(public service: SupplierOptionsService, private sharedService: SharedService) {
  }
  PageTitle = "Supplier Options";
  redirectUrl = "supplieroptions";

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "Supplier", field: "supplier", tooltipField: "supplier" },
      { headerName: "Supplier Name", field: "supplierName", tooltipField: "supplierName" },
      { headerName: "Send Debit Memo", field: "sendDebitMemo", tooltipField: "sendDebitMemo" },
      { headerName: "Use Invoice Terms Indicator", field: "useInvoceTermsIndicator", tooltipField: "useInvoceTermsIndicator" },
      { headerName: "Manually Paid Indicator", field: "manuallyPaidIndicator", tooltipField: "manuallyPaidIndicator" },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 90
      }
    ];
    this.model = { supplierName: null, supplier: null, sendDebitMemo: null, manuallyPaidIndicator: null, useInvoceTermsIndicator: null, rogDateAllowedIndicator: null, apReviewer: null, closeOpenShipmentDays: null, matchRecieptsOthersuppLiersIndicator: null, qtyDiscDayBeforeRte: null, operation: null };
    if (this.sharedService.searchData) {

      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  open(cell: any, mode: string): void {
    this.sharedService.searchData = this.service.serviceDocument.dataProfile.dataModel;
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

}
