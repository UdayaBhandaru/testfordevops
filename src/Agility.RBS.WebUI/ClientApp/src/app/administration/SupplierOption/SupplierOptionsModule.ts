import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { SupplierOptionsRoutingModule } from "./SupplierOptionsRoutingModule";
import { SupplierOptionsListComponent } from "./SupplierOptionsListComponent";
import { SupplierOptionsComponent } from "./SupplierOptionsComponent";
import { SupplierOptionsService } from "./SupplierOptionsService";
import { SupplierOptionsResolver } from "./SupplierOptionsResolver";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { RbsSharedModule } from "../../RbsSharedModule";

@NgModule({
  imports: [
    FrameworkCoreModule,
    SupplierOptionsRoutingModule,
    AgGridModule,
    AgGridSharedModule,
    TitleSharedModule,
    RbsSharedModule
  ],
  declarations: [
    SupplierOptionsListComponent,
    SupplierOptionsComponent

  ],
  providers: [
    SupplierOptionsService,
    SupplierOptionsResolver
  ]
})
export class SupplierOptionsModule {
}
