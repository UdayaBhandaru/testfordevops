export class SupplierOptionsModel {
  supplier?: number;
  supplierName: string;
  sendDebitMemo: string;
  manuallyPaidIndicator: string;
  useInvoceTermsIndicator: string;
  rogDateAllowedIndicator: string;
  apReviewer: string;
  closeOpenShipmentDays?: number;
  matchRecieptsOthersuppLiersIndicator?: number;
  qtyDiscDayBeforeRte?: number;
  operation: string;
}
