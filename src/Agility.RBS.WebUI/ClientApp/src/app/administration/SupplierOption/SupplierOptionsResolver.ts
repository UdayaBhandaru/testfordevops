import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { SupplierOptionsService } from "./SupplierOptionsService";
import { SupplierOptionsModel } from './SupplierOptionsModel';

@Injectable()
export class SupplierOptionsResolver implements Resolve<ServiceDocument<SupplierOptionsModel>> {
  constructor(private service: SupplierOptionsService) { }
  resolve(): Observable<ServiceDocument<SupplierOptionsModel>> {
    return this.service.list();
  }
}
