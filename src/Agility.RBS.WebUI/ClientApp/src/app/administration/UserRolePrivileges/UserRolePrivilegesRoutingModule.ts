import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { UserRolePrivilegesComponent } from './UserRolePrivilegesComponent';
import { UserRolePrivilegesResolver } from './UserRolePrivilegesResolver';
import { UserRolePrivilegesListComponent } from './UserRolePrivilegesListComponent';


const DomainRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "", redirectTo: "List", pathMatch: "full"
      },
      {
        path: "List",
        component: UserRolePrivilegesListComponent,
        resolve:
        {
          references: UserRolePrivilegesResolver
        }
      },
      {
        path: "New/:id",
        component: UserRolePrivilegesComponent
      },

    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(DomainRoutes)
  ]
})
export class UserRolePrivilegesRoutingModule { }
