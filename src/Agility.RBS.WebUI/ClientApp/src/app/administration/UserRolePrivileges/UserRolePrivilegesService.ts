import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { UserRolePrivilegesModel } from './UserRolePrivilegesModel';
@Injectable()
export class UserRolePrivilegesService {
  serviceDocument: ServiceDocument<UserRolePrivilegesModel> = new ServiceDocument<UserRolePrivilegesModel>();
  searchData: any;
  constructor() { }

  newModel(model: UserRolePrivilegesModel): ServiceDocument<UserRolePrivilegesModel> {
    return this.serviceDocument.newModel(model);
  }

  list(): Observable<ServiceDocument<UserRolePrivilegesModel>> {
    return this.serviceDocument.list("/api/UserRolePrivileges/List");
  }

  save(): Observable<ServiceDocument<UserRolePrivilegesModel>> {
    return this.serviceDocument.save("/api/UserRolePrivileges/Save", true);
  }

  search(): Observable<ServiceDocument<UserRolePrivilegesModel>> {
    return this.serviceDocument.search("/api/UserRolePrivileges/Search");
  }
}
