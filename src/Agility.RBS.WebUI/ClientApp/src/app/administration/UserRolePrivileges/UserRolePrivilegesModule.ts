import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { RbsSharedModule } from "../../RbsSharedModule";
import { UserRolePrivilegesComponent } from './UserRolePrivilegesComponent';
import { UserRolePrivilegesListComponent } from './UserRolePrivilegesListComponent';
import { UserRolePrivilegesRoutingModule } from './UserRolePrivilegesRoutingModule';
import { UserRolePrivilegesService } from './UserRolePrivilegesService';
import { UserRolePrivilegesResolver } from './UserRolePrivilegesResolver';

@NgModule({
  imports: [
    FrameworkCoreModule,
    UserRolePrivilegesRoutingModule,
    AgGridModule,
    AgGridSharedModule,
    TitleSharedModule,
    RbsSharedModule
  ],
  declarations: [
    UserRolePrivilegesComponent,
    UserRolePrivilegesListComponent

  ],
  providers: [
    UserRolePrivilegesService,
    UserRolePrivilegesResolver
  ]
})
export class UserRolePrivilegesModule {
}
