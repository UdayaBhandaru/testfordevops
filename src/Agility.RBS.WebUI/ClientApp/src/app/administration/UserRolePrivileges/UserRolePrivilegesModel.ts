export class UserRolePrivilegesModel {
  role: string;
  ordApprAmt?: number;
  tsfApprInd?: string;
  operation?: string;
}
