import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { UserRolePrivilegesModel } from "./UserRolePrivilegesModel";
import { UserRolePrivilegesService } from "./UserRolePrivilegesService";
import { SharedService } from "../../Common/SharedService";
import { FormGroup } from "@angular/forms";

@Component({
  selector: "user-role-priv",
  templateUrl: "./UserRolePrivilegesComponent.html"
})

export class UserRolePrivilegesComponent implements OnInit {
  PageTitle = "User Role Privileges";
  redirectUrl = "UserRolePrivileges";
  serviceDocument: ServiceDocument<UserRolePrivilegesModel>;
  editModel: any;
  editMode: boolean = false;
  model: UserRolePrivilegesModel;
  indicatorList: any;
  roleList: { roleId: string, role: string };
  constructor(private service: UserRolePrivilegesService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.indicatorList = this.service.serviceDocument.domainData["indicator"];
    this.roleList = this.service.serviceDocument.domainData["roleList"];
    this.bind();
  }

  bind(): void {
    delete this.editModel;
    if (this.sharedService.editObject) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
      this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
      delete this.sharedService.editObject;
      this.editMode = true;
    } else {
      this.model = { role: null, ordApprAmt: null, tsfApprInd: null, operation: "I" };
      this.service.serviceDocument.dataProfile.dataModel = this.model;
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  save(saveOnly: boolean): void {
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.saveLocalTrait(saveOnly);
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  saveLocalTrait(saveOnly: boolean): void {
    this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.sharedService.saveForm("User Role Privileges saved successfully.").subscribe(() => {

          if (saveOnly) {
            this.router.navigate(["/" + this.redirectUrl + "/List"], { skipLocationChange: true });
          } else {
            if (!this.editMode) {
              this.model = { role: null, ordApprAmt: null, tsfApprInd: null, operation: "I" };
              this.service.serviceDocument.dataProfile.dataModel = this.model;
              this.service.newModel(this.model);
            } else {
              Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
              this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.service.serviceDocument.dataProfile.dataModel);
              this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            }
          }
        }, (error: string) => { console.log(error); });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
    }, (error: string) => { console.log(error); });
  }

  reset(): void {
    if (!this.editMode) {
      this.model = { role: null, ordApprAmt: null, tsfApprInd: null, operation: "I" };
      this.service.serviceDocument.dataProfile.dataModel = this.model;
      this.service.newModel(this.model);
    } else {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    }
    this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
  }
}
