import { Component, OnInit } from "@angular/core";
import { ServiceDocument } from "@agility/frameworkcore";
import { InfoComponent } from '../../Common/InfoComponent';
import { SharedService } from '../../Common/SharedService';
import { UserRolePrivilegesService } from './UserRolePrivilegesService';
import { UserRolePrivilegesModel } from './UserRolePrivilegesModel';
import { Router } from '@angular/router';

@Component({
  selector: "UserRolePrivileges-List",
  templateUrl: "./UserRolePrivilegesListComponent.html"
})
export class UserRolePrivilegesListComponent implements OnInit {

  public serviceDocument: ServiceDocument<UserRolePrivilegesModel>;
  columns: any[];
  public componentName: any;
  model: UserRolePrivilegesModel;
  public showSearchCriteria: boolean = true;
  roleList: { roleId: string, role: string };
  constructor(public service: UserRolePrivilegesService, private sharedService: SharedService, public router: Router) {
  }
  pageTitle = "User Role Privileges";
  redirectUrl = "UserRolePrivileges/New/";

  ngOnInit(): void {
    this.model = { role: null, ordApprAmt: null, tsfApprInd: null, operation: "I" };
    this.componentName = this;
    this.roleList = this.service.serviceDocument.domainData["roleList"];
    this.columns = [
      { headerName: "Role", field: "role", tooltipField: "role" },
      { headerName: "Ord Appr Amt", field: "ordApprAmt", tooltipField: "ordApprAmt" },
      { headerName: "Tsf Appr Ind", field: "tsfApprInd", tooltipField: "tsfApprInd" }
    ];
    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
      this.sharedService.previousUrl = "../UserRolePrivileges/List";
    }

    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl + cell.data.groupId
      , this.service.serviceDocument.dataProfile.dataList);
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }
  addValidation(evnt: any): void {
    this.navigateToAdd();
  }

  private navigateToAdd(): void {
    this.router.navigate(["/" + this.redirectUrl + "0"], { skipLocationChange: true });
    this.sharedService.previousUrl = "../UserRolePrivileges/List";
  }
}
