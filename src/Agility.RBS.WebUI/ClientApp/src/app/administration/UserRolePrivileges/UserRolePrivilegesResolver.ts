import { Injectable } from "@angular/core";
import { Resolve } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { UserRolePrivilegesModel } from './UserRolePrivilegesModel';
import { UserRolePrivilegesService } from './UserRolePrivilegesService';

@Injectable()
export class UserRolePrivilegesResolver implements Resolve<ServiceDocument<UserRolePrivilegesModel>> {
  constructor(private service: UserRolePrivilegesService) { }
  resolve(): Observable<ServiceDocument<UserRolePrivilegesModel>> {
    return this.service.list();
  }
}
