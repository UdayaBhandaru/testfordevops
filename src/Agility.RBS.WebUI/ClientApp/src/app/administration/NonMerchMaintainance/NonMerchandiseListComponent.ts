import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { GridOptions, ColDef } from 'ag-grid-community';
import { NonMerchandiseModel } from './NonMerchandiseModel';
import { SharedService } from '../../Common/SharedService';
import { GridEditComponent } from '../../Common/grid/GridEditComponent';
import { NonMerchandiseService } from './NonMerchandiseService';
import { InfoComponent } from '../../Common/InfoComponent';

@Component({
  selector: "non-merch-List",
  templateUrl: "./NonMerchandiseListComponent.html"
})
export class NonMerchandiseListComponent implements OnInit {
  public serviceDocument: ServiceDocument<NonMerchandiseModel>;
  PageTitle = "Non Merchandise code maintainane";
  redirectUrl = "NonMerchCode";
  componentName: any;
  columns: ColDef[];
  model: NonMerchandiseModel = { nonMerchandiseCode: null, nonMerchandiseCodeDesc: null, operation: "I" };
  showSearchCriteria: boolean = true;
  gridOptions: GridOptions;
  componentDomain: any;
  constructor(public service: NonMerchandiseService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.sharedService.domainData=this.componentDomain = this.service.serviceDocument.domainData["domain"];
    this.columns = [
      { headerName: "nonMerchandiseCode", field: "nonMerchandiseCode", tooltipField: "nonMerchandiseCode", width: 90 },
      { headerName: "nonMerchandiseCodeDesc", field: "nonMerchandiseCodeDesc", tooltipField: "nonMerchandiseCodeDesc" },
      {
        headerName: "ACTIONS", field: "Actions", cellRendererFramework: GridEditComponent, width: 40
      }
    ];
    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }
}
