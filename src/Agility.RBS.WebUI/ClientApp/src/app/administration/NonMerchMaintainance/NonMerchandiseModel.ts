import { NonMerchandiseDetailModel } from './NonMerchandiseDetailModel';

export class NonMerchandiseModel {
  nonMerchandiseCode: string;
  nonMerchandiseCodeDesc: string;
  operation: string;
  details?: NonMerchandiseDetailModel[];
}
