import { Injectable } from "@angular/core";
import { Resolve } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { NonMerchandiseModel } from './NonMerchandiseModel';
import { NonMerchandiseService } from './NonMerchandiseService';

@Injectable()
export class NonMerchandiseResolver implements Resolve<ServiceDocument<NonMerchandiseModel>> {
  constructor(private service: NonMerchandiseService) { }
  resolve(): Observable<ServiceDocument<NonMerchandiseModel>> {
    return this.service.list();
  }
}
