import { Component, OnInit } from "@angular/core";
import { ServiceDocument, CommonService, MessageResult, MessageType } from "@agility/frameworkcore";
import { Router } from "@angular/router";
import { NonMerchandiseModel } from './NonMerchandiseModel';
import { NonMerchandiseService } from './NonMerchandiseService';
import { SharedService } from '../../Common/SharedService';
import { ColDef, GridOptions } from 'ag-grid-community';
import { DomainDetailModel } from '../../domain/DomainDetailModel';
import { NonMerchandiseDetailModel } from './NonMerchandiseDetailModel';
import { GridInputComponent } from '../../Common/grid/GridInputComponent';
import { GridEditComponent } from '../../Common/grid/GridEditComponent';
import { IndicatorComponent } from '../../Common/grid/IndicatorComponent';
import { LoaderService } from '../../Common/LoaderService';
import { Subscription } from 'rxjs';
import { GridSelectComponent } from '../../Common/grid/GridSelectComponent';
import { FormControl } from '@angular/forms';

@Component({
  selector: "non-merchandise",
  templateUrl: "./NonMerchandiseComponent.html",
  styleUrls: ['NonMerchandiseComponent.scss'],
})
export class NonMerchandiseComponent implements OnInit {
  gridApi: any;
  PageTitle = "Non Merchandise Code Mentainance";
  sheetName: string = "NonMerchandiseCodeMaintainance";
  redirectUrl = "NonMerchCode";
  public gridOptions: GridOptions;
  serviceDocument: ServiceDocument<NonMerchandiseModel>;
  model: NonMerchandiseModel;
  data: any[] = [];
  sno: number = 0;
  domainDtlData: NonMerchandiseDetailModel[];
  temp: {};
  editMode: boolean = false;
  editModel: any;
  private messageResult: MessageResult = new MessageResult();
  componentParentName: NonMerchandiseComponent;
  localizationData: any;
  columns: ColDef[];
  saveServiceSubscription: Subscription;
  submitSuccessSubscription: Subscription;
  componentDomain: any;
  constructor(private service: NonMerchandiseService, private commonService: CommonService,
    private sharedService: SharedService, private router: Router, private loaderService: LoaderService) {
  }

  ngOnInit(): void {
    this.componentDomain = this.sharedService.domainData;
    this.model = { nonMerchandiseCode: null, nonMerchandiseCodeDesc: null, operation: "I" };
    if (this.sharedService.editObject) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
      this.data = this.service.serviceDocument.dataProfile.dataModel.details;
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
    this.bind();
    this.componentParentName = this;
  }

  bind(): void {
    this.columns = [
      {
        headerName: "Component", field: "componentId", cellRendererFramework: GridSelectComponent,
        cellRendererParams: { references: this.componentDomain, keyvaluepair: { key: "componentId", value: "description" } }, tooltipField: "component"
      },
      { headerName: "Description", field: "description", valueGetter: (params) => { return params.data.description; } },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 60, cellRendererParams: { restrictEditIcon: true }
      }
    ];

    this.gridOptions = {
      onGridReady: (params: any) => {
        this.gridApi = params.api;
        this.gridApi.startEditingCell({
          rowIndex: 0,
          colKey: "componentId"
        });
      },
      context: {
        componentParent: this
      }
    };
  }

  save(saveOnly: boolean): void {
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.saveSubscription(saveOnly);
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  saveAndContinue(): void {
    this.save(false);
  }

  add($event: MouseEvent): void {
    $event.preventDefault();
    if (this.data.length === 0) {
      this.service.serviceDocument.dataProfile.dataModel.details = [];
    }
    this.data.push({ componentId: "", operation: "I" });
    this.gridApi.setRowData(this.data);
  }


  delete(cell: any): void {
    this.data.splice(cell.rowIndex, 1);
    this.gridApi.setRowData(this.data);
  }

  reset(): void {
    this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
  }

  checkDomain(): boolean {
    if (!this.editMode) {
      let detailId: any = this.serviceDocument.dataProfile.profileForm.controls["domainId"].value;
      if (detailId) {
        this.service.isExistingDomain(detailId).subscribe((response: boolean) => {
          if (response) {
            this.sharedService.errorForm(this.localizationData.domain.domaindatacheck);
            this.serviceDocument.dataProfile.profileForm.controls["domainId"].setValue(null);
          }
        });
      }
    }
    return false;
  }

  change(cell: any, event: any): void {
    if (event.target) {
      if (event.target.checked) {
        this.data.forEach((detail: DomainDetailModel) => { detail.requiredInd = "N"; });
      }
      this.data[cell.rowIndex]["requiredInd"] = event.target.checked ? "Y" : "N";
    }
    this.temp = this.data[cell.rowIndex];
    this.gridApi.setRowData(this.data);
  }

  update(cell: any, event: any): void {
    if (cell.column.colId === "codeSeq" && this.data.some((data: DomainDetailModel, index: number) => {
      return +data.codeSeq === +event.target.value && index !== cell.rowIndex;
    })) {
      this.sharedService.errorForm(this.localizationData.domain.domainsequencecheck);
      event.target.value = null;
      this.data[cell.rowIndex]["codeSeq"] = null;
      return;
    } else if (cell.column.colId === "code" && this.data.some((data: DomainDetailModel, index: number) => {
      return data.code === event.target.value && index !== cell.rowIndex;
    })) {
      this.sharedService.errorForm(this.localizationData.domain.domaindetailscheck);
      event.target.value = null;
      this.data[cell.rowIndex]["code"] = null;
      return;
    } else {
      this.data[cell.rowIndex][cell.column.colId] = event.target.value;
    }
    this.temp = this.data[cell.rowIndex];
  }

  select(cell: any, event: any): void {
    this.data[cell.rowIndex][cell.column.colId] = event.target.value;
    this.temp = this.data[cell.rowIndex];
  }

  saveSubscription(saveOnly: boolean): void {
    this.loaderService.display(true);
    this.serviceDocument.dataProfile.profileForm.controls["details"] = new FormControl();
    this.serviceDocument.dataProfile.profileForm.controls["details"].setValue(this.data);
    this.saveServiceSubscription = this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.submitSuccessSubscription = this.sharedService.saveForm(`Saved successfully`).subscribe(() => {
          if (saveOnly) {
            this.router.navigate(["/NonMerchCode/List"], { skipLocationChange: true });
          } else {
            this.router.navigate(["/Blank"], {
              skipLocationChange: true, queryParams: {
                id: "/NonMerchCode/New/"
              }
            });
          }
        });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
      this.loaderService.display(false);
    });

  }
}
