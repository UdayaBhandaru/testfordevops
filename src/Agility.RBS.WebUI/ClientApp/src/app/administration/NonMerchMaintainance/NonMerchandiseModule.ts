import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { RbsSharedModule } from "../../RbsSharedModule";
import { NonMerchandiseResolver } from './NonMerchandiseResolver';
import { NonMerchandiseService } from './NonMerchandiseService';
import { NonMerchandiseComponent } from './NonMerchandiseComponent';
import { NonMerchandiseListComponent } from './NonMerchandiseListComponent';
import { NonMerchandiseRoutingModule } from './NonMerchandiseRoutingModule';

@NgModule({
  imports: [
    FrameworkCoreModule,
    NonMerchandiseRoutingModule,
    AgGridModule,
    AgGridSharedModule,
    TitleSharedModule,
    RbsSharedModule
  ],
  declarations: [
    NonMerchandiseListComponent,
    NonMerchandiseComponent

  ],
  providers: [
    NonMerchandiseService,
    NonMerchandiseResolver
  ]
})
export class NonMerchandiseModule {
}
