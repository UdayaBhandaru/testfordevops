import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { NonMerchandiseListComponent } from './NonMerchandiseListComponent';
import { NonMerchandiseResolver } from './NonMerchandiseResolver';
import { NonMerchandiseComponent } from './NonMerchandiseComponent';
const DomainRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "", redirectTo: "List", pathMatch: "full"
      },
      {
        path: "List",
        component: NonMerchandiseListComponent,
        resolve:
        {
          references: NonMerchandiseResolver
        }
      },
      {
        path: "New",
        component: NonMerchandiseComponent
      }

    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(DomainRoutes)
  ]
})
export class NonMerchandiseRoutingModule { }
