import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";
import { NonMerchandiseModel } from './NonMerchandiseModel';

@Injectable()
export class NonMerchandiseService {
  serviceDocument: ServiceDocument<NonMerchandiseModel> = new ServiceDocument<NonMerchandiseModel>();
  searchData: any;
  constructor(private httpHelperService: HttpHelperService) { }

  newModel(model: NonMerchandiseModel): ServiceDocument<NonMerchandiseModel> {
    return this.serviceDocument.newModel(model);
  }

  list(): Observable<ServiceDocument<NonMerchandiseModel>> {
    return this.serviceDocument.list("/api/NonMerchandiseCode/List");
  }

  search(): Observable<ServiceDocument<NonMerchandiseModel>> {
    return this.serviceDocument.search("/api/NonMerchandiseCode/Search");
  }

  save(): Observable<ServiceDocument<NonMerchandiseModel>> {
    return this.serviceDocument.save("/api/NonMerchandiseCode/Save", true);
  }

  isExistingDomain(name: string): Observable<boolean> {
    return this.httpHelperService.get("/api/NonMerchandiseCode/IsExistingDomain", new HttpParams().set("name", name));
  }
}
