import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { RegionalityGroupModel } from './RegionalityGroupModel';
import { RegionalityGroupService } from './RegionalityGroupService';

@Injectable()
export class RegionalityGroupResolver implements Resolve<ServiceDocument<RegionalityGroupModel>> {
  constructor(private service: RegionalityGroupService) { }
  resolve(): Observable<ServiceDocument<RegionalityGroupModel>> {
    return this.service.list();
  }
}
