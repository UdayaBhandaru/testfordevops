import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";
import { RegionalityGroupModel } from './RegionalityGroupModel';
@Injectable()
export class RegionalityGroupService {
  serviceDocument: ServiceDocument<RegionalityGroupModel> = new ServiceDocument<RegionalityGroupModel>();
  searchData: any;
  constructor(private httpHelperService: HttpHelperService) { }

  newModel(model: RegionalityGroupModel): ServiceDocument<RegionalityGroupModel> {
    return this.serviceDocument.newModel(model);
  }

  list(): Observable<ServiceDocument<RegionalityGroupModel>> {
    return this.serviceDocument.list("/api/SecurityGroup/List");
  }

  save(): Observable<ServiceDocument<RegionalityGroupModel>> {
    return this.serviceDocument.save("/api/SecurityGroup/Save", true);
  }

  search(): Observable<ServiceDocument<RegionalityGroupModel>> {
    return this.serviceDocument.search("/api/SecurityGroup/List");
  }
}
