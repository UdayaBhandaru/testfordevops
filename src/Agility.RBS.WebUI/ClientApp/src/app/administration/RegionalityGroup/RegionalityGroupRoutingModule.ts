import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { RegionalityGroupComponent } from './RegionalityGroupComponent';
import { RegionalityGroupResolver } from './RegionalityGroupResolver';
import { RegionalityGroupListComponent } from './RegionalityGroupListComponent';


const DomainRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "", redirectTo: "List", pathMatch: "full"
      },
      {
        path: "List",
        component: RegionalityGroupListComponent,
        resolve:
        {
          references: RegionalityGroupResolver
        }
      },
      {
        path: "New/:id",
        component: RegionalityGroupComponent
      },

    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(DomainRoutes)
  ]
})
export class RegionalityGroupRoutingModule { }
