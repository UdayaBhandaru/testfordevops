export class RegionalityGroupModel {
  groupId: number;
  groupName: string;
  role: string;
  comments: string;
  operation: string;
}
