import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { RbsSharedModule } from "../../RbsSharedModule";
import { RegionalityGroupComponent } from './RegionalityGroupComponent';
import { RegionalityGroupListComponent } from './RegionalityGroupListComponent';
import { RegionalityGroupRoutingModule } from './RegionalityGroupRoutingModule';
import { RegionalityGroupService } from './RegionalityGroupService';
import { RegionalityGroupResolver } from './RegionalityGroupResolver';

@NgModule({
  imports: [
    FrameworkCoreModule,
    RegionalityGroupRoutingModule,
    AgGridModule,
    AgGridSharedModule,
    TitleSharedModule,
    RbsSharedModule
  ],
  declarations: [
    RegionalityGroupComponent,
    RegionalityGroupListComponent

  ],
  providers: [
    RegionalityGroupService,
    RegionalityGroupResolver
  ]
})
export class RegionalityGroupModule {
}
