import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ReasonCodeListComponent } from "./ReasonCodeListComponent";
import { ReasonCodeResolver } from "./ReasonCodeResolver";
import { ReasonCodeComponent } from "./ReasonCodeComponent"
const DomainRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "", redirectTo: "List", pathMatch: "full"
      },
      {
        path: "List",
        component: ReasonCodeListComponent,
        resolve:
        {
          references: ReasonCodeResolver
        }
      },
      {
        path: "New",
        component: ReasonCodeComponent
      }

    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(DomainRoutes)
  ]
})
export class ReasonCodeRoutingModule { }
