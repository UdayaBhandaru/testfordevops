import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { ReasonCodeService } from "./ReasonCodeService";
import { ReasonCodeModel } from './ReasonCodeModel';

@Injectable()
export class ReasonCodeResolver implements Resolve<ServiceDocument<ReasonCodeModel>> {
  constructor(private service: ReasonCodeService) { }
  resolve(): Observable<ServiceDocument<ReasonCodeModel>> {
    return this.service.list();
  }
}
