import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { ReasonCodeRoutingModule } from "./ReasonCodeRoutingModule";
import { ReasonCodeListComponent } from "./ReasonCodeListComponent";
import { ReasonCodeComponent } from "./ReasonCodeComponent";
import { ReasonCodeService } from "./ReasonCodeService";
import { ReasonCodeResolver } from "./ReasonCodeResolver";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { RbsSharedModule } from "../../RbsSharedModule";

@NgModule({
  imports: [
    FrameworkCoreModule,
    ReasonCodeRoutingModule,
    AgGridModule,
    AgGridSharedModule,
    TitleSharedModule,
    RbsSharedModule
  ],
  declarations: [
    ReasonCodeListComponent,
    ReasonCodeComponent

  ],
  providers: [
    ReasonCodeService,
    ReasonCodeResolver
  ]
})
export class ReasonCodeModule {
}
