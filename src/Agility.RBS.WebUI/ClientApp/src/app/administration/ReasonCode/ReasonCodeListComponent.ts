import { Component, OnInit } from "@angular/core";
import { ServiceDocument } from "@agility/frameworkcore";
import { ReasonCodeService } from "./ReasonCodeService";
import { ReasonCodeModel } from './ReasonCodeModel';
import { GridEditComponent } from '../../Common/grid/GridEditComponent';
import { InfoComponent } from '../../Common/InfoComponent';
import { SharedService } from '../../Common/SharedService';
import { GridOptions } from 'ag-grid-community';
@Component({
  selector: "ReasonCode-List",
  templateUrl: "./ReasonCodeListComponent.html"
})
export class ReasonCodeListComponent implements OnInit {

  public serviceDocument: ServiceDocument<ReasonCodeModel>;
  columns: any[];
  public componentName: any;
  model: ReasonCodeModel;
  public showSearchCriteria: boolean = true;
  public gridOptions: GridOptions;
  constructor(public service: ReasonCodeService, private sharedService: SharedService) {
  }
  PageTitle = "Reason Code";
  redirectUrl = "reasoncode";

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "Reason Code Type", field: "reasonCodeType", tooltipField: "reasonCodeType" },
      { headerName: "Reason Code  Description", field: "reasonCodeDescription", tooltipField: "reasonCodeTypeDescription" },
      { headerName: "Action", field: "action", tooltipField: "action" },
      { headerName: "Reason Code Id", field: "reasonCodeId", tooltipField: "reasonCodeId" },
      { headerName: "Hint Comment", field: "hintComment", tooltipField: "hintComment" },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 90
      }
    ];
    this.model = { reasonCodeType: null, reasonCodeTypeDescription: null, reasonCodeId: null, reasonCodeDescription: null, action: null, actionDescription: null, commentRequiredIndicator: null, hintComment: null, deleteIndicator: null, operation: null };
    if (this.sharedService.searchData) {

      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  open(cell: any, mode: string): void {
    this.sharedService.searchData = this.service.serviceDocument.dataProfile.dataModel;
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

}
