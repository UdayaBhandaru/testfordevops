export class ReasonCodeModel {
  reasonCodeType: string;
  reasonCodeTypeDescription: string;
  reasonCodeId: string;
  reasonCodeDescription: string;
  action: string;
  actionDescription: string;
  commentRequiredIndicator: string;
  hintComment: string;
  deleteIndicator: string;
  operation: string;
}
