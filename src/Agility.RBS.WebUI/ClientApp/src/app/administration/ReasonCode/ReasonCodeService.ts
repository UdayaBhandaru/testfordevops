import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { HttpHelperService } from "../../Common/HttpHelperService";
import { ReasonCodeModel } from './ReasonCodeModel';
@Injectable()
export class ReasonCodeService {
  serviceDocument: ServiceDocument<ReasonCodeModel> = new ServiceDocument<ReasonCodeModel>();
  searchData: any;
  constructor(private httpHelperService: HttpHelperService) { }

  newModel(model: ReasonCodeModel): ServiceDocument<ReasonCodeModel> {
    return this.serviceDocument.newModel(model);
  }

  list(): Observable<ServiceDocument<ReasonCodeModel>> {
    return this.serviceDocument.list("/api/ReasonCode/List");
  }

  save(): Observable<ServiceDocument<ReasonCodeModel>> {
    return this.serviceDocument.save("/api/ReasonCode/Save", true);
  }

  search(): Observable<ServiceDocument<ReasonCodeModel>> {
    return this.serviceDocument.search("/api/ReasonCode/List");
  }
}
