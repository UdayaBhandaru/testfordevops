import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { UserAttributesResolver, UserAttributesListResolver } from './UserAttributesResolver';
import { UserAttributesListComponent } from './UserAttributesListComponent';
import { UserAttributesComponent } from './UserAttributesComponent';

const PromotionRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "", redirectTo: "List", pathMatch: "full"
      },
      {
        path: "List",
        component: UserAttributesListComponent,
        resolve:
        {
          serviceDocument: UserAttributesListResolver
        }
      },
      {
        path: "New/:id",
        component: UserAttributesComponent,
        resolve: {
          serviceDocument: UserAttributesResolver
        },
      }
    ]
  }
];

@NgModule({
    imports: [
        RouterModule.forChild(PromotionRoutes)
    ]
})
export class UserAttributesRoutingModule { }

