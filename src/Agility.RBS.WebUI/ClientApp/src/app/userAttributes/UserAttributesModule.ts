import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { AgGridSharedModule } from "../AgGridSharedModule";
import { TitleSharedModule } from "../TitleSharedModule";
import { DatePickerModule } from "../Common/DatePickerModule";
import { RbsSharedModule } from '../RbsSharedModule';
import { MatTreeModule } from '@angular/material/tree';
import { UserAttributesRoutingModule } from './UserAttributesRoutingModule';
import { UserAttributesComponent } from './UserAttributesComponent';
import { UserAttributesService } from './UserAttributesService';
import { UserAttributesListComponent } from './UserAttributesListComponent';
import { UserAttributesResolver, UserAttributesListResolver } from './UserAttributesResolver';

@NgModule({
  imports: [
    FrameworkCoreModule,
    UserAttributesRoutingModule,
    AgGridSharedModule,
    TitleSharedModule,
    DatePickerModule,
  ],
  declarations: [
    UserAttributesComponent,
    UserAttributesListComponent,
  ],
  providers: [
    UserAttributesService,
    UserAttributesListResolver,
    UserAttributesResolver,
  ]
})
export class UserAttributesModule {
  constructor() {
    LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
  }
}
