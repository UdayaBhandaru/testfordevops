import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { HttpParams } from "@angular/common/http";
import { UserAttributesModel } from './UserAttributesModel';

@Injectable()
export class UserAttributesService {
  serviceDocument: ServiceDocument<UserAttributesModel> = new ServiceDocument<UserAttributesModel>();

  constructor() { }

  newModel(model: UserAttributesModel): ServiceDocument<UserAttributesModel> {
    return this.serviceDocument.newModel(model);
  }
  save(): Observable<ServiceDocument<UserAttributesModel>> {
    return this.serviceDocument.save("/api/UserAttributes/Save", true);
  }


  addEdit(id: string): Observable<ServiceDocument<UserAttributesModel>> {
    if (+id === 0) {
      return this.serviceDocument.new("/api/UserAttributes/New");
    } else {
      return this.serviceDocument.open("/api/UserAttributes/Open", new HttpParams().set("id", id.toString()));
    }
  }

  search(): Observable<ServiceDocument<UserAttributesModel>> {
    return this.serviceDocument.search("/api/UserAttributes/Search", true);
  }

  list(): Observable<ServiceDocument<UserAttributesModel>> {
    return this.serviceDocument.list("/api/UserAttributes/List");
  }
}
