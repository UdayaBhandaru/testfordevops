import { Component, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { GridOptions, ColDef } from "ag-grid-community";
import { SearchComponent } from '../Common/SearchComponent';
import { SharedService } from '../Common/SharedService';
import { GridEditComponent } from '../Common/grid/GridEditComponent';
import { InfoComponent } from '../Common/InfoComponent';
import { UserAttributesModel } from './UserAttributesModel';
import { UserAttributesService } from './UserAttributesService';


@Component({
  selector: "user-attributes-list",
  templateUrl: "./UserAttributesListComponent.html"
})

export class UserAttributesListComponent implements OnInit {
  pageTitle = "User Attributes";
  redirectUrl = "UserAttributes/New/";
  serviceDocument: ServiceDocument<UserAttributesModel>;
  private gridOptions: GridOptions;
  columns: ColDef[];
  model: UserAttributesModel;
  isSearchParamGiven: boolean = false;
  @ViewChild("searchPanelPromotion") searchPanelPromotion: SearchComponent;
  gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  defaultDateType: string;
  componentName: this;
  searchProps = ["promO_EVENT_ID", "theme", "startDate", "endDate", "description"];
  constructor(public service: UserAttributesService, public sharedService: SharedService, public router: Router) {
  }

  ngOnInit(): void {

    this.componentName = this;
    this.columns =
      [
      { headerName: "User Id", field: "userId", tooltipField: "userId", width: 50, headerTooltip: "UserId" },
      { headerName: "user Name", field: "userName", tooltipField: "userName", width: 40, headerTooltip: "userName" },
      { headerName: "email", field: "email", tooltipField: "email", headerTooltip: "email" },
      { headerName: "phone", field: "phone", width: 45, headerTooltip: "phone" },
      //{ headerName: "empTypeDesc", field: "empTypeDesc", width: 45, headerTooltip: "empTypeDesc" },
        {
        headerName: "ACTIONS", field: "Actions", cellRendererFramework: GridEditComponent, width: 60
      }
      ];
    this.model = {
      userId: null, userName: null, lang: null, storeDefault: null, storeName: null, phone: null, fax: null, pager: null, email: null, printer: null, operation: "I"
    }
    this.service.newModel(this.model);
    this.serviceDocument = this.service.serviceDocument;
  }



  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
      this.sharedService.previousUrl = "../UserAttributes/List";
    }

    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl + cell.data.userId , this.service.serviceDocument.dataProfile.dataList);
  }

  addValidation(evnt: any): void {
    this.navigateToAdd();
  }

  private navigateToAdd(): void {
    this.router.navigate(["/" + this.redirectUrl + "0"], { skipLocationChange: true });
    this.sharedService.previousUrl = "../UserAttributes/List";
  }
}
