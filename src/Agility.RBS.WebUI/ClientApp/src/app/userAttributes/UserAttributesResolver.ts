import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { UserAttributesService } from './UserAttributesService';
import { UserAttributesModel } from './UserAttributesModel';



@Injectable()
export class UserAttributesListResolver implements Resolve<ServiceDocument<UserAttributesModel>> {
  constructor(private service: UserAttributesService) { }
  resolve(): Observable<ServiceDocument<UserAttributesModel>> {
        return this.service.list();
    }
}

@Injectable()
export class UserAttributesResolver implements Resolve<ServiceDocument<UserAttributesModel>> {
  constructor(private service: UserAttributesService) { }
  resolve(route: ActivatedRouteSnapshot): Observable<ServiceDocument<UserAttributesModel>> {
    return this.service.addEdit(route.params["id"]);
  }
}
