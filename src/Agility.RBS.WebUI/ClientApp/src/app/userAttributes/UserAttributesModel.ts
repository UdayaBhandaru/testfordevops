export class UserAttributesModel {
  userId: string;
  userName: string;
  lang: number;
  languageDesc?: string;
  storeDefault: number;
  storeName?: string;
  phone: string;
  fax: string;
  pager: string;
  email: string;
  printer: string;
  printerName?: string;
  operation?: string;
}
