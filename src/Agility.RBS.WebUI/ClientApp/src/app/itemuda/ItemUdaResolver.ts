import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { ItemUdaModel } from './ItemUdaModel';
import { ItemUdaService } from './ItemUdaService';

@Injectable()
export class ItemUdaResolver implements Resolve<ServiceDocument<ItemUdaModel>> {
  constructor(private service: ItemUdaService) { }
  resolve(route: ActivatedRouteSnapshot): Observable<ServiceDocument<ItemUdaModel>> {
        return this.service.addEdit(route.params["id"]);
    }
}
