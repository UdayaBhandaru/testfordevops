import { Component, OnInit } from "@angular/core";
import { ServiceDocument, FormMode, CommonService, MessageResult, MessageType } from "@agility/frameworkcore";
import { Router, ActivatedRoute } from "@angular/router";
import { FormControl } from "@angular/forms";
import { SharedService } from "../Common/SharedService";
import { GridOptions, RowNode } from "ag-grid-community";
import { IndicatorComponent } from "../Common/grid/IndicatorComponent";
import { GridInputComponent } from "../Common/grid/GridInputComponent";
import { GridEditComponent } from '../Common/grid/GridEditComponent';
import { ItemUdaValueModel } from './ItemUdaValueModel';
import { ItemUdaModel } from './ItemUdaModel';
import { ItemUdaService } from './ItemUdaService';
import { DomainDetailModel } from '../domain/DomainDetailModel';
import { DepartmentDomainModel } from '../Common/DomainData/DepartmentDomainModel';
import { ChainDomainModel } from '../Common/DomainData/ChainDomainModel';
import { Subscription } from 'rxjs';
import { LoaderService } from '../Common/LoaderService';
import { GridNumericComponent } from '../Common/grid/GridNumericComponent';

@Component({
  selector: "item-uda",
  templateUrl: "./ItemUdaComponent.html",
  styleUrls: ['ItemUdaComponent.scss'],
})
export class ItemUdaComponent implements OnInit {
  gridApi: any;
  PageTitle = "Item UDA";
  sheetName: string = "Item UDA";
  redirectUrl = "ItemUda";
  public gridOptions: GridOptions;
  serviceDocument: ServiceDocument<ItemUdaModel>;
  model: ItemUdaModel;
  columns: any[];
  data: ItemUdaValueModel[] = [];
  dataReset: ItemUdaValueModel[] = [];
  sno: number = 0;
  editMode: boolean = false;
  private messageResult: MessageResult = new MessageResult();
  componentParentName: ItemUdaComponent;
  localizationData: any;
  disptypeData: DomainDetailModel[];
  datatypeData: DomainDetailModel[];
  indicatorData: DomainDetailModel[];
  chainData: ChainDomainModel[];
  categoryData: DepartmentDomainModel[];
  primaryId: number;
  routeServiceSubscription: Subscription;
  saveServiceSubscription: Subscription;
  orginalValue: string = "";
  displayDetails: boolean = false;
  deletedRows: any[] = [];
  enableDataLength: boolean = false;
  udaValueModel: ItemUdaValueModel;

  constructor(private service: ItemUdaService, private commonService: CommonService, private route: ActivatedRoute,
    private sharedService: SharedService, private router: Router, private loaderService: LoaderService) {
  }

  ngOnInit(): void {
    this.componentParentName = this;
    this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
    this.disptypeData = this.service.serviceDocument.domainData["disptype"];
    this.datatypeData = this.service.serviceDocument.domainData["datatype"];
    this.chainData = this.service.serviceDocument.domainData["chain"];
    this.categoryData = this.service.serviceDocument.domainData["category"];
    this.indicatorData = this.service.serviceDocument.domainData["indicator"];

    this.columns = [
      { headerName: "UDA Values", field: "udaValue", cellRendererFramework: GridNumericComponent },
      { headerName: "Description", field: "udaValueDesc", cellRendererFramework: GridInputComponent },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 60
        , cellRendererParams: { restrictEditIcon: true }
      }
    ];

    this.gridOptions = {
      onGridReady: (params: any) => {
        this.gridApi = params.api;
      }
    };

    this.bind();
  }

  bind(): void {
    this.routeServiceSubscription = this.route.params.subscribe((resp: any) => {
      this.primaryId = resp["id"];
    });

    if (this.primaryId.toString() !== "0") {
      this.editMode = true;
      this.PageTitle = `${this.PageTitle} - EDIT - ${this.primaryId}`;
      if (this.service.serviceDocument.dataProfile.dataModel.displayType === "LV") {
        this.displayDetails = true;
      }

      if (this.service.serviceDocument.dataProfile.dataModel.itemUdaValues) {
        Object.assign(this.data, this.service.serviceDocument.dataProfile.dataModel.itemUdaValues);
        Object.assign(this.dataReset, this.service.serviceDocument.dataProfile.dataModel.itemUdaValues);
        this.orginalValue = JSON.stringify(this.dataReset);
      }
    } else {
      this.itemUdaModel();
      this.service.newModel(this.model);
      this.PageTitle = `${this.PageTitle} - ADD`;
    }

    this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    this.serviceDocument = this.service.serviceDocument;
  }

  itemUdaModel(): void {
    this.model = {
      operation: "I", dataLength: null, dataType: null, displayType: null, filterMerchId: null, filterOrgId: null, module: "ITEM"
      , udaDesc: null, singleValueInd: "N", udaId: null
    };
  }

  save(saveOnly: boolean): void {
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.saveSubscription(saveOnly);
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  saveAndContinue(): void {
    this.save(false);
  }

  add($event: MouseEvent): void {
    $event.preventDefault();
    let filterData: ItemUdaValueModel[] = this.data.filter(id => id.udaValueDesc === null);
    if (filterData.length === 0) {
      this.sno = this.data.length > 0 ? this.data.slice(0).sort((a: any, b: any) => { return a.udaValue - b.udaValue; })
        .reverse()[0].udaValue : this.sno;
      this.sno = +(this.sno) + 1;
      this.udaValueModel = {
        operation: "I", udaValue: this.sno, udaValueDesc: null
      };
      this.data.unshift(this.udaValueModel);
      if (this.gridApi) {
        this.gridApi.setRowData(this.data);
      }
    }
  }

  delete(cell: any): void {
    if (cell.data.operation === "U") {
      cell.data.operation = "D";
      this.deletedRows.push(cell.data);
    }
    this.data.splice(cell.rowIndex, 1);
    this.gridApi.setRowData(this.data);
  }

  reset(): void {

  }

  update(cell: any, event: any): void {
    if (cell.column.colId === "udaValueDesc") {
      if (this.serviceDocument.dataProfile.profileForm.controls["dataType"].value === "NUM") {
        if (!event.target.value.toString().match(/^[0-9]+(\.?[0-9]+)?$/)) {
          event.target.value = null;
          this.sharedService.errorForm("Description value should be only numbers");
          return;
        }
      }

      let dataLength: number = this.serviceDocument.dataProfile.profileForm.controls["dataLength"].value;
      if (dataLength) {
        if (event.target.value.length > dataLength) {
          event.target.value = null;
          this.sharedService.errorForm("Description length should be less than 'DataLength'");
          return;
        }
      }
    }

    this.data[cell.rowIndex][cell.column.colId] = event.target.value;
    let rowNode: RowNode = this.gridApi.getRowNode(cell.rowIndex);
    rowNode.setData(rowNode.data);
  }

  saveSubscription(saveOnly: boolean): void {
    this.loaderService.display(true);
    if (this.deletedRows.length > 0) {
      this.deletedRows.forEach(x => this.data.push(x));
    }
    if (this.data.length > 0) {
      this.serviceDocument.dataProfile.profileForm.controls["itemUdaValues"] = new FormControl();
      this.serviceDocument.dataProfile.profileForm.controls["itemUdaValues"].setValue(this.data);
    }

    this.saveServiceSubscription = this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.primaryId = this.service.serviceDocument.dataProfile.dataModel.udaId;
        this.sharedService.saveForm(`Saved Successfully - ID ${this.primaryId}`).subscribe(() => {
          if (saveOnly) {
            this.router.navigate(["/ItemUda/List"], { skipLocationChange: true });
          } else {
            this.router.navigate(["/Blank"], {
              skipLocationChange: true, queryParams: {
                id: "/ItemUda/New/" + this.primaryId
              }
            });
            this.loaderService.display(false);
          }
        }, (error: string) => { console.log(error); });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
    }, (error: string) => { console.log(error); });
    this.loaderService.display(false);
  }

  changeDisplayType(item: any): void {
    this.serviceDocument.dataProfile.profileForm.controls["dataType"].disable();
    this.serviceDocument.dataProfile.profileForm.controls["dataLength"].enable();
    if (item.options.code === "DT") {
      this.displayDetails = false;
      this.serviceDocument.dataProfile.profileForm.controls["dataLength"].disable();
      this.serviceDocument.dataProfile.profileForm.controls["dataType"].setValue("DATE");
    } else if (item.options.code === "LV") {
      this.displayDetails = true;
      this.serviceDocument.dataProfile.profileForm.controls["dataType"].enable();
      this.serviceDocument.dataProfile.profileForm.controls["dataType"].setValue(null);
    } else if (item.options.code === "FF") {
      this.displayDetails = false;
      this.serviceDocument.dataProfile.profileForm.controls["dataType"].setValue("ALPHA");
    }
  }

  changeDataType(item: any): void {
    if (this.serviceDocument.dataProfile.profileForm.controls["displayType"].value === "LV" && item.options.code === "DATE") {
      this.sharedService.errorForm("The Data Type cannot be set to 'Date' when Display Type is 'List of Values'");
      this.serviceDocument.dataProfile.profileForm.controls["dataType"].setValue(null);
    }
  }

  ngOnDestroy(): void {
    this.routeServiceSubscription.unsubscribe();
    if (this.saveServiceSubscription) {
      this.saveServiceSubscription.unsubscribe();
    }

    this.disptypeData.length = 0;
    this.datatypeData.length = 0;
    this.chainData.length = 0;
    this.categoryData.length = 0;
  }
}
