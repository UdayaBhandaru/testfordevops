import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { ItemUdaModel } from "./ItemUdaModel";
import { HttpParams } from '@angular/common/http';

@Injectable()
export class ItemUdaService {
  serviceDocument: ServiceDocument<ItemUdaModel> = new ServiceDocument<ItemUdaModel>();
  searchData: any;

  newModel(model: ItemUdaModel): ServiceDocument<ItemUdaModel> {
    return this.serviceDocument.newModel(model);
  }

  list(): Observable<ServiceDocument<ItemUdaModel>> {
    return this.serviceDocument.list("/api/ItemUserDefinedSearch/List");
  }

  search(): Observable<ServiceDocument<ItemUdaModel>> {
    return this.serviceDocument.search("/api/ItemUserDefinedSearch/Search");
  }

  save(): Observable<ServiceDocument<ItemUdaModel>> {
    return this.serviceDocument.save("/api/ItemUserDefined/Save", true);
  }

  addEdit(id: number): Observable<ServiceDocument<ItemUdaModel>> {
    if (+id === 0) {
      return this.serviceDocument.new("/api/ItemUserDefined/New");
    } else {
      return this.serviceDocument.open("/api/ItemUserDefined/Open", new HttpParams().set("id", id.toString()));
    }
  }
}
