import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { ItemUdaModel } from "./ItemUdaModel";
import { FormGroup } from "@angular/forms";
import { SharedService } from "../Common/SharedService";
import { GridEditComponent } from "../Common/grid/GridEditComponent";
import { GridExportModel } from "../Common/grid/GridExportModel";
import { InfoComponent } from "../Common/InfoComponent";
import { GridOptions } from 'ag-grid-community';
import { ItemUdaService } from './ItemUdaService';

@Component({
  selector: "item-uda-list",
  templateUrl: "./ItemUdaListComponent.html"
})
export class ItemUdaListComponent implements OnInit {
  public serviceDocument: ServiceDocument<ItemUdaModel>;
  PageTitle = "Item UDA";
  redirectUrl = "ItemUda/New/";
  componentName: any;
  columns: any[];
  model: ItemUdaModel = { udaId: null, udaDesc: null };
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  gridOptions: GridOptions;

  constructor(public service: ItemUdaService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "ID", field: "udaId", tooltipField: "udaId", width: 90 },
      { headerName: "DESCRIPTION", field: "udaDesc", tooltipField: "udaDesc" },
      { headerName: "DIALOG", field: "module", tooltipField: "module" },
      { headerName: "DISPLAY TYPE", field: "displayTypeDesc", tooltipField: "displayTypeDesc" },
      { headerName: "DATA TYPE", field: "dataTypeDesc", tooltipField: "dataTypeDesc" },
      {
        headerName: "ACTIONS", field: "Actions", cellRendererFramework: GridEditComponent, width: 40
      }
    ];

    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
      this.sharedService.previousUrl = "../ItemUda/List";
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl + cell.data.udaId, this.service.serviceDocument.dataProfile.dataList);
  }

  addValidation(): void {
    this.router.navigate(["/" + this.redirectUrl + "0"], { skipLocationChange: true });
    this.sharedService.previousUrl = "../Order/List";
  }
}
