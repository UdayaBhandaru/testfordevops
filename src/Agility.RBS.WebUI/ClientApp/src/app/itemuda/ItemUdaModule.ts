import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../AgGridSharedModule";
import { TitleSharedModule } from "../TitleSharedModule";
import { RbsSharedModule } from "../RbsSharedModule";
import { ItemUdaListComponent } from './ItemUdaListComponent';
import { ItemUdaComponent } from './ItemUdaComponent';
import { ItemUdaService } from './ItemUdaService';
import { ItemUdaResolver } from './ItemUdaResolver';
import { ItemUdaRoutingModule } from './ItemUdaRoutingModule';
import { ItemUdaListResolver } from './ItemUdaListResolver';

@NgModule({
  imports: [
    FrameworkCoreModule,
    ItemUdaRoutingModule,
    AgGridModule,
    AgGridSharedModule,
    TitleSharedModule,
    RbsSharedModule
  ],
  declarations: [
    ItemUdaListComponent,
    ItemUdaComponent
  ],
  providers: [
    ItemUdaService,
    ItemUdaResolver,
    ItemUdaListResolver
  ]
})
export class ItemUdaModule {
}
