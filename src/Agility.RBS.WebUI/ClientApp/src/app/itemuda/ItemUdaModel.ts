import { ItemUdaValueModel } from './ItemUdaValueModel';

export class ItemUdaModel {
  itemUdaValues?: ItemUdaValueModel[];
  udaId?: number;
  udaDesc: string;
  module?: string;
  displayType?: string;
  displayTypeDesc?: string;
  dataType?: string;
  dataTypeDesc?: string;
  dataLength?: number;
  singleValueInd?: string;
  filterOrgId?: number;
  filterOrgIdDesc?: string;
  filterMerchId?: number;
  filterMerchIdDesc?: string;
  filterMerchIdClass?: number;
  filterMerchIdClassDesc?: string;
  filterMerchIdSubclass?: number;
  filterMerchIdSubclassDesc?: string;
  operation?: string;
}
