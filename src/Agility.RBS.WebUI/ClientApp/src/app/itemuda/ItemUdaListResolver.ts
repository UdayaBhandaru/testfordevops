import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { ItemUdaModel } from "./ItemUdaModel";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { ItemUdaService } from './ItemUdaService';

@Injectable()
export class ItemUdaListResolver implements Resolve<ServiceDocument<ItemUdaModel>> {
    constructor(private service: ItemUdaService) { }
    resolve(): Observable<ServiceDocument<ItemUdaModel>> {
        return this.service.list();
    }
}
