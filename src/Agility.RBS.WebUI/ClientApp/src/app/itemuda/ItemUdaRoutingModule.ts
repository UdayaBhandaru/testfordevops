import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ItemUdaListComponent } from './ItemUdaListComponent';
import { ItemUdaComponent } from './ItemUdaComponent';
import { ItemUdaResolver } from './ItemUdaResolver';
import { ItemUdaListResolver } from './ItemUdaListResolver';

const ItemUdaRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "", redirectTo: "List", pathMatch: "full"
      },
      {
        path: "List",
        component: ItemUdaListComponent,
        resolve:
        {
          references: ItemUdaListResolver
        }
      },
      {
        path: "New/:id",
        component: ItemUdaComponent,
        resolve:
        {
          references: ItemUdaResolver
        }
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(ItemUdaRoutes)
  ]
})
export class ItemUdaRoutingModule { }
