import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { GridOptions } from "ag-grid-community";
import { CurrencyExchangeTypeModel } from './CurrencyExchangeTypeModel';
import { GridExportModel } from '../Common/grid/GridExportModel';
import { CurrencyExchangeTypeService } from './CurrencyExchangeTypeService';
import { SharedService } from '../Common/SharedService';
import { GridEditComponent } from '../Common/grid/GridEditComponent';
import { InfoComponent } from '../Common/InfoComponent';
@Component({
  selector: "CurrencyExchangeType-list",
  templateUrl: "./CurrencyExchangeTypeListComponent.html"
})
export class CurrencyExchangeTypeListComponent implements OnInit {
  PageTitle = "Currency Exchange Type";
  redirectUrl = "CurrencyExchangeType";
  componentName: any;
  serviceDocument: ServiceDocument<CurrencyExchangeTypeModel>;
  public gridOptions: GridOptions;
  columns: any[];
  model: CurrencyExchangeTypeModel;
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  public curnExData: { curnExType: string, curnExTypeDesc: string }[];

  constructor(public service: CurrencyExchangeTypeService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "fif Exchange Type", field: "fifExchangeType", tooltipField: "fifExchangeType" },
      { headerName: "rms Exchange Type", field: "rmsExchangeType", tooltipField: "rmsExchangeType" },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent
      }
    ];
    this.curnExData = [{ curnExType: "O", curnExTypeDesc: "Office" }, { curnExType: "C", curnExTypeDesc: "Corporate" }];

    this.model = { fifExchangeType: null, rmsExchangeType: null };

    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }
}
