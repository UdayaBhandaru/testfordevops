import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { CurrencyExchangeTypeListComponent } from "./CurrencyExchangeTypeListComponent";
import { CurrencyExchangeTypeService } from "./CurrencyExchangeTypeService";
import { CurrencyExchangeTypeRoutingModule } from "./CurrencyExchangeTypeRoutingModule";
import { CurrencyExchangeTypeComponent } from "./CurrencyExchangeTypeComponent";
import { CurrencyExchangeTypeListResolver, CurrencyExchangeTypeResolver } from "./CurrencyExchangeTypeResolver";
import { RbsSharedModule } from '../RbsSharedModule';
import { AgGridSharedModule } from '../AgGridSharedModule';
import { TitleSharedModule } from '../TitleSharedModule';

@NgModule({
    imports: [
        FrameworkCoreModule,
        CurrencyExchangeTypeRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        CurrencyExchangeTypeListComponent,
        CurrencyExchangeTypeComponent,
    ],
    providers: [
        CurrencyExchangeTypeService,
        CurrencyExchangeTypeListResolver,
        CurrencyExchangeTypeResolver,
    ]
})
export class CurrencyExchangeTypeModule {
    constructor() {
        LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
    }
}
