import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CurrencyExchangeTypeListComponent } from "./CurrencyExchangeTypeListComponent";
import { CurrencyExchangeTypeListResolver, CurrencyExchangeTypeResolver} from "./CurrencyExchangeTypeResolver";
import { CurrencyExchangeTypeComponent } from "./CurrencyExchangeTypeComponent";
const UtilityRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
              component: CurrencyExchangeTypeListComponent,
                resolve:
                {
                    serviceDocument: CurrencyExchangeTypeListResolver
                }
            },
            {
                path: "New",
                component: CurrencyExchangeTypeComponent,
                resolve:
                {
                    serviceDocument: CurrencyExchangeTypeResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(UtilityRoutes)
    ]
})
export class CurrencyExchangeTypeRoutingModule { }
