export class CurrencyExchangeTypeModel { 
  fifExchangeType?: number;
  rmsExchangeType: string;
  operation?: string;
}
