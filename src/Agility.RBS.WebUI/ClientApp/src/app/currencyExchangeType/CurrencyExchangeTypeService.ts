import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { CurrencyExchangeTypeModel } from "./CurrencyExchangeTypeModel";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from '../Common/HttpHelperService';

@Injectable()
export class CurrencyExchangeTypeService {
    serviceDocument: ServiceDocument<CurrencyExchangeTypeModel> = new ServiceDocument<CurrencyExchangeTypeModel>();
    constructor(private httpHelperService: HttpHelperService) { }

    newModel(model: CurrencyExchangeTypeModel): ServiceDocument<CurrencyExchangeTypeModel> {
        return this.serviceDocument.newModel(model);
    }

    search(): Observable<ServiceDocument<CurrencyExchangeTypeModel>> {
      return this.serviceDocument.search("/api/CurrencyExchangeType/Search");
    }

    save(): Observable<ServiceDocument<CurrencyExchangeTypeModel>> {
      return this.serviceDocument.save("/api/CurrencyExchangeType/Save", true);
    }

    list(): Observable<ServiceDocument<CurrencyExchangeTypeModel>> {
      return this.serviceDocument.list("/api/CurrencyExchangeType/List");
    }

    addEdit(): Observable<ServiceDocument<CurrencyExchangeTypeModel>> {
      return this.serviceDocument.list("/api/CurrencyExchangeType/New");
    }
  isExistingCurrencyExchangeType(fifExchangeType: string): Observable<boolean> {
    return this.httpHelperService.get("/api/CurrencyExchangeType/IsExistingCurrencyExchangeType", new HttpParams().set("fifExchangeType", fifExchangeType));
    }
}
