import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { CurrencyExchangeTypeService } from "./CurrencyExchangeTypeService";
import { CurrencyExchangeTypeModel } from "./CurrencyExchangeTypeModel";
@Injectable()
export class CurrencyExchangeTypeListResolver implements Resolve<ServiceDocument<CurrencyExchangeTypeModel>> {
    constructor(private service: CurrencyExchangeTypeService) { }
    resolve(): Observable<ServiceDocument<CurrencyExchangeTypeModel>> {
        return this.service.list();
    }
}

@Injectable()
export class CurrencyExchangeTypeResolver implements Resolve<ServiceDocument<CurrencyExchangeTypeModel>> {
    constructor(private service: CurrencyExchangeTypeService) { }
    resolve(): Observable<ServiceDocument<CurrencyExchangeTypeModel>> {
        return this.service.addEdit();
    }
}



