import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { CurrencyExchangeTypeModel } from './CurrencyExchangeTypeModel';
import { CurrencyExchangeTypeService } from './CurrencyExchangeTypeService';
import { SharedService } from '../Common/SharedService';

@Component({
  selector: "currency-eExchange-type",
  templateUrl: "./CurrencyExchangeTypeComponent.html"
})

export class CurrencyExchangeTypeComponent implements OnInit {
  PageTitle = "Currency Exchange Type ";
  redirectUrl = "CurrencyExchangeType";
    serviceDocument: ServiceDocument<CurrencyExchangeTypeModel>;
    editMode: boolean = false;
    editModel: any;
    localizationData: any;  
    model: CurrencyExchangeTypeModel;  
  public curnExData: { curnExType: string, curnExTypeDesc: string }[];

    constructor(private service: CurrencyExchangeTypeService, private sharedService: SharedService, private router: Router
        , private ref: ChangeDetectorRef) {
    }

  ngOnInit(): void {

    this.curnExData = [{ curnExType: "O", curnExTypeDesc: "Office" }, { curnExType: "C", curnExTypeDesc: "Corporate" }];

        this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
        this.bind();
    }

    bind(): void {
        delete this.editModel;
        if (this.sharedService.editObject) {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);         
            Object.assign(this.service.serviceDocument.dataProfile.dataModel, { operation: "U" });
            this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            delete this.sharedService.editObject;
            this.editMode = true;

        } else {            
          this.model = { fifExchangeType: null, rmsExchangeType: null, operation: "I"};
           
            this.service.serviceDocument.dataProfile.dataModel = this.model;
            this.service.newModel(this.model);
        }
        this.serviceDocument = this.service.serviceDocument;
    }

    save(saveOnly: boolean): void {
        if (this.serviceDocument.dataProfile.profileForm.valid) {
            this.saveSubscription(saveOnly);
        } else {
            this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
        }
    }

    saveSubscription(saveOnly: boolean): void {
      //this.checkCurrencyExchangeType(saveOnly);
      this.saveCurrencyExchangeType(saveOnly);
  }

    saveCurrencyExchangeType(saveOnly: boolean): void {
        this.service.save().subscribe(() => {
            if (this.service.serviceDocument.result.type === MessageType.success) {
                this.sharedService.saveForm("Currency Exchange Type saved successfully.").subscribe(() => {
                    if (saveOnly) {
                        this.router.navigate(["/" + this.redirectUrl + "/List"], { skipLocationChange: true });
                    } else {
                        if (!this.editMode) {
                          this.model = { fifExchangeType: null, rmsExchangeType: null, operation: "I" };                            
                            this.service.newModel(this.model);
                        } else {
                            Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
                            this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.service.serviceDocument.dataProfile.dataModel);
                            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
                        }
                    }
                }, (error: string) => { console.log(error); });
            } else {
                this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
            }
        }, (error: string) => { console.log(error); });
    }

    checkCurrencyExchangeType(saveOnly: boolean): void {
        if (!this.editMode) {
          this.service.isExistingCurrencyExchangeType(this.serviceDocument.dataProfile.profileForm.controls["fifExchangeType"].value).subscribe((response: any) => {
                if (response) {
                    this.sharedService.errorForm("CurrencyExchangeType already exisit.");
                } else {
                    this.saveCurrencyExchangeType(saveOnly);
                }
            });
        } else {
            this.saveCurrencyExchangeType(saveOnly);
        }
    }

    reset(): void {
        if (!this.editMode) {
            this.model = {
              fifExchangeType: null, rmsExchangeType: null, operation: "I"
            };
           
            this.service.serviceDocument.dataProfile.dataModel = this.model;
            this.service.newModel(this.model);

        } else {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);            
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
        }
        this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
    }
}
