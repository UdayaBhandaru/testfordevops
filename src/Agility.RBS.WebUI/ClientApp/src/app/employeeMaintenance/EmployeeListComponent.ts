import { Component, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { GridOptions, ColDef } from "ag-grid-community";
import { SearchComponent } from '../Common/SearchComponent';
import { SharedService } from '../Common/SharedService';
import { GridEditComponent } from '../Common/grid/GridEditComponent';
import { InfoComponent } from '../Common/InfoComponent';
import { EmployeeModel } from './EmployeeModel';
import { EmployeeService } from './EmployeeService';


@Component({
  selector: "employee-list",
  templateUrl: "./employeeListComponent.html"
})

export class EmployeeListComponent implements OnInit {
  pageTitle = "Employee";
  redirectUrl = "employee/New/";
  serviceDocument: ServiceDocument<EmployeeModel>;
  private gridOptions: GridOptions;
  columns: ColDef[];
  model: EmployeeModel;
  isSearchParamGiven: boolean = false;
  @ViewChild("searchPanelPromotion") searchPanelPromotion: SearchComponent;
  gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  defaultDateType: string;
  componentName: this;
  searchProps = ["promO_EVENT_ID", "theme", "startDate", "endDate", "description"];
  constructor(public service: EmployeeService, public sharedService: SharedService, public router: Router) {
  }

  ngOnInit(): void {

    this.componentName = this;
    this.columns =
      [
        { headerName: "UserId", field: "userId", tooltipField: "userId", width: 50, headerTooltip: "UserId" },
        { headerName: "name", field: "name", tooltipField: "name", width: 40, headerTooltip: "name" },
        { headerName: "email", field: "email", tooltipField: "email", headerTooltip: "email" },
      { headerName: "phone", field: "phone", width: 45, headerTooltip: "phone" },
      { headerName: "empTypeDesc", field: "empTypeDesc", width: 45, headerTooltip: "empTypeDesc" },
        {
        headerName: "ACTIONS", field: "Actions", cellRendererFramework: GridEditComponent, width: 60
      }
      ];
    this.model = { cashierInd: null, email: null, empId: null, empType: null, empTypeDesc: null, name: null, phone: null, salespersonInd: null, userId: null }
    this.service.newModel(this.model);
    this.serviceDocument = this.service.serviceDocument;
  }



  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
      this.sharedService.previousUrl = "../Employee/List";
    }

    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl + cell.data.empId
      , this.service.serviceDocument.dataProfile.dataList);
  }

  addValidation(evnt: any): void {
    this.navigateToAdd();
  }

  private navigateToAdd(): void {
    this.router.navigate(["/" + this.redirectUrl + "0"], { skipLocationChange: true });
    this.sharedService.previousUrl = "../Employee/List";
  }
}
