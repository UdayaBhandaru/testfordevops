import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { HttpParams } from "@angular/common/http";
import { EmployeeModel } from './EmployeeModel';

@Injectable()
export class EmployeeService {
  serviceDocument: ServiceDocument<EmployeeModel> = new ServiceDocument<EmployeeModel>();

  constructor() { }

  newModel(model: EmployeeModel): ServiceDocument<EmployeeModel> {
    return this.serviceDocument.newModel(model);
  }
  save(): Observable<ServiceDocument<EmployeeModel>> {
    return this.serviceDocument.save("/api/Employee/Save", true);
  }


  addEdit(id: number): Observable<ServiceDocument<EmployeeModel>> {
    if (+id === 0) {
      return this.serviceDocument.new("/api/Employee/New");
    } else {
      return this.serviceDocument.open("/api/Employee/Open", new HttpParams().set("id", id.toString()));
    }
  }

  search(): Observable<ServiceDocument<EmployeeModel>> {
    return this.serviceDocument.search("/api/Employee/Search", true);
  }

  list(): Observable<ServiceDocument<EmployeeModel>> {
    return this.serviceDocument.list("/api/Employee/List");
  }
}
