import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { AgGridSharedModule } from "../AgGridSharedModule";
import { TitleSharedModule } from "../TitleSharedModule";
import { DatePickerModule } from "../Common/DatePickerModule";
import { RbsSharedModule } from '../RbsSharedModule';
import { MatTreeModule } from '@angular/material/tree';
import { EmployeeRoutingModule } from './EmployeeRoutingModule';
import { EmployeeComponent } from './EmployeeComponent';
import { EmployeeService } from './EmployeeService';
import { EmployeeListComponent } from './EmployeeListComponent';
import { EmployeeResolver, EmployeeListResolver } from './EmployeeResolver';

@NgModule({
  imports: [
    FrameworkCoreModule,
    EmployeeRoutingModule,
    AgGridSharedModule,
    TitleSharedModule,
    DatePickerModule,
  ],
  declarations: [
    EmployeeComponent,
    EmployeeListComponent,
  ],
  providers: [
    EmployeeService,
    EmployeeListResolver,
    EmployeeResolver,
  ]
})
export class EmployeeModule {
  constructor() {
    LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
  }
}
