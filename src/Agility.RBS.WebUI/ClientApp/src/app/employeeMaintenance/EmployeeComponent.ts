import { Component, OnInit, ViewChild, OnDestroy } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ServiceDocument, MessageResult, MessageType } from "@agility/frameworkcore";
import { MatDialogRef } from '@angular/material';
import { SharedService } from '../Common/SharedService';
import { LoaderService } from '../Common/LoaderService';
import { RbDialogService } from '../Common/controls/RbDialogService';
import { ValidatorFn, AbstractControl, FormControl } from '@angular/forms';
import { RbButton } from '../Common/controls/RbControlModels';
import { EmployeeService } from './EmployeeService';
import { EmployeeModel } from './EmployeeModel';
import { DomainDetailModel } from '../domain/DomainDetailModel';

@Component({
  selector: "employee-add",
  templateUrl: "./EmployeeComponent.html"
})
export class EmployeeComponent implements OnInit, OnDestroy {
  _componentName = "Employee";
  PageTitle = "Employee";
  public serviceDocument: ServiceDocument<EmployeeModel>;
  primaryId: string;
  screenName: string = "Employee";
  editModel: EmployeeModel = {
    cashierInd: null, email: null, empId: null, empType: null, empTypeDesc: null, name: null, phone: null, userId: null, salespersonInd: null
  };
  editMode: boolean;
  model: EmployeeModel;
  dialogRef: MatDialogRef<any>;
  public messageResult: MessageResult = new MessageResult();
  canNavigate: boolean = false;

  submitSuccessSubscription: any;
  saveServiceSubscription: any;
  saveContinueSubscribtion: any;
  resetSubscribtion: any;
  routeServiceSubscription: any;
  saveSuccessSubscription: any;
  nextSubscription: any;
  rejectSubscription: any;
  treeData: any[] = [];
  columns: any[];
  componentName: this;
  public empTypeData: { empType: string, empTypeDesc: string }[];
  public domainDetailsIndicator: DomainDetailModel[] = [];
  constructor
    (
    public route: ActivatedRoute,
    public router: Router,
    public sharedService: SharedService,
    private service: EmployeeService,
    private loaderService: LoaderService,
    public dialogService: RbDialogService) {
  }

  ngOnInit(): void {
    this.empTypeData = [{ empType: "H", empTypeDesc: "HeadQuaters" }, { empType: "S", empTypeDesc: "Store" }];
    this.domainDetailsIndicator = this.service.serviceDocument.domainData["indicator"];
    this.bind();
  }

  bind(): void {
    this.routeServiceSubscription = this.route.params.subscribe((resp: any) => {
      this.primaryId = resp["id"];
    });
    if (this.primaryId.toString() !== "0") {
      this.editMode = true;
      this.primaryId = this.service.serviceDocument.dataProfile.dataModel.empId;
      this.PageTitle = `${this.PageTitle} - EDIT - ${this.primaryId}`;
    } else {
      this.model = {
        cashierInd: "N", email: null, empId: null, empType: null, empTypeDesc: null, name: null, phone: null, userId: null, salespersonInd: "N", operation: "I"
      };
      this.service.serviceDocument.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  save(saveOnly: boolean): void {
    this.saveSubscription(true);
  }

  saveAndContinue(): void {
    this.save(false);
  }

  saveSubscription(saveOnly: boolean): void {
    this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.loaderService.display(true);
      this.saveServiceSubscription = this.service.save().subscribe(() => {
        if (this.service.serviceDocument.result.type === MessageType.success) {
          this.primaryId = this.service.serviceDocument.dataProfile.dataModel.empId;
          this.submitSuccessSubscription = this.sharedService.saveForm(
            `Employee saved with EId: ${this.primaryId}`
          ).subscribe(() => {
            if (saveOnly) {
              this.router.navigate(["/employee/List"], { skipLocationChange: true });
            } else {
              this.router.navigate(["/Blank"], {
                skipLocationChange: true, queryParams: {
                  id: "/employee/New/" + this.primaryId
                }
              });
            }
          });
        } else {
          this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
        }
        this.loaderService.display(false);
      });
    }
  }

  ngOnDestroy(): void {
    if (this.saveContinueSubscribtion) {
      this.saveContinueSubscribtion.unsubscribe();
    }
    if (this.routeServiceSubscription) {
      this.routeServiceSubscription.unsubscribe();
    }
    if (this.saveServiceSubscription) {
      this.saveServiceSubscription.unsubscribe();
    }
    if (this.saveSuccessSubscription) {
      this.saveSuccessSubscription.unsubscribe();
    }
    if (this.submitSuccessSubscription) {
      this.submitSuccessSubscription.unsubscribe();
    }
    if (this.nextSubscription) {
      this.nextSubscription.unsubscribe();
    }
  }

  close(): void {
    this.messageResult.message = `<div>All your changes will be lost if you don't save them.</div>
                                    <div>Click on “Save” to Keep the changes.</div>
                                    <div>Click on “Don't save” to proceed further without saving changes.</div>
                                    <div>Click on “Cancel” to stay on the current page.</div>`;
    if ((this.serviceDocument.dataProfile.profileForm && this.serviceDocument.dataProfile.profileForm.dirty)
      || !this.serviceDocument.dataProfile.profileForm.valid) {
      if (!this.dialogRef) {
        this.dialogRef = this.dialogService.openMessageDialog("Warning", this.messageResult, [new RbButton("", "Cancel", "alertCancel")
          , new RbButton("", "Don't Save", "alertdontsave"), new RbButton("", "Save", "alertSave")], "37%", ""
          , "iconsdontsave retailsNavIcons", "dontsaveAlert", "dontsaveAlertTitle");
        this.dialogRef.componentInstance.click.subscribe(
          btnName => {
            if (btnName === "Cancel") {
              this.dialogRef.close();
            }
            if (btnName === "Don't Save") {
              this.serviceDocument.dataProfile.profileForm.markAsUntouched();
              this.canNavigate = true;
              this.dialogRef.close();
              this.router.navigate([this.sharedService.previousUrl], { skipLocationChange: true });
            }
            if (btnName === "Save") {
              this.dialogRef.close();
              this.save(true);
              this.router.navigate([this.sharedService.previousUrl], { skipLocationChange: true });
            }
          });
      }
    } else {
      this.canNavigate = true;
      this.router.navigate([this.sharedService.previousUrl], { skipLocationChange: true });
    }
  }
}

