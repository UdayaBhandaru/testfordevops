import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { EmployeeResolver, EmployeeListResolver } from './EmployeeResolver';
import { EmployeeListComponent } from './EmployeeListComponent';
import { EmployeeComponent } from './EmployeeComponent';

const PromotionRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "", redirectTo: "List", pathMatch: "full"
      },
      {
        path: "List",
        component: EmployeeListComponent,
        resolve:
        {
          serviceDocument: EmployeeListResolver
        }
      },
      {
        path: "New/:id",
        component: EmployeeComponent,
        resolve: {
          serviceDocument: EmployeeResolver
        },
      }
    ]
  }
];

@NgModule({
    imports: [
        RouterModule.forChild(PromotionRoutes)
    ]
})
export class EmployeeRoutingModule { }

