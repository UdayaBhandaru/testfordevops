export class EmployeeModel {
  empType: string;
  empTypeDesc: string;
  empId: string;
  cashierInd: string;
  salespersonInd: string;
  phone: string;
  name: string;
  userId: string;
  email: string;
  operation?: string;
}
