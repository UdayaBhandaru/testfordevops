import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { EmployeeService } from './EmployeeService';
import { EmployeeModel } from './EmployeeModel';



@Injectable()
export class EmployeeListResolver implements Resolve<ServiceDocument<EmployeeModel>> {
  constructor(private service: EmployeeService) { }
  resolve(): Observable<ServiceDocument<EmployeeModel>> {
        return this.service.list();
    }
}

@Injectable()
export class EmployeeResolver implements Resolve<ServiceDocument<EmployeeModel>> {
  constructor(private service: EmployeeService) { }
  resolve(route: ActivatedRouteSnapshot): Observable<ServiceDocument<EmployeeModel>> {
    return this.service.addEdit(route.params["id"]);
  }
}
