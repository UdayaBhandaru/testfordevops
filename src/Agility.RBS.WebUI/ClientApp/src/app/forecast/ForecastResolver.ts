﻿import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { ForecastHeadModel } from "./ForecastHeadModel";
import { ForecastService } from "./ForecastService";

@Injectable()
export class ForecastResolver implements Resolve<ServiceDocument<ForecastHeadModel>> {
    constructor(private service: ForecastService) { }
    resolve(route: ActivatedRouteSnapshot): Observable<ServiceDocument<ForecastHeadModel>> {
        return this.service.addEdit(route.params["id"]);
    }
}