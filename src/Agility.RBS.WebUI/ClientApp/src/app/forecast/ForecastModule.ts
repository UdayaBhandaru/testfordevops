﻿import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../AgGridSharedModule";
import { TitleSharedModule } from "../TitleSharedModule";
import { ForecastRoutingModule } from "./ForecastRoutingModule";
import { ForecastListComponent } from "./ForecastListComponent";
import { ForecastComponent } from "./ForecastComponent";
import { ForecastService } from "./ForecastService";
import { ForecastResolver } from "./ForecastResolver";
import { ForecastListResolver } from "./ForecastListResolver";
import { RbsSharedModule } from "../RbsSharedModule";

@NgModule({
    imports: [
        FrameworkCoreModule,
        ForecastRoutingModule,
        AgGridModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        ForecastListComponent,
        ForecastComponent
    ],
    providers: [
        ForecastService,
        ForecastResolver,
        ForecastListResolver
    ]
})
export class ForecastModule {
}
