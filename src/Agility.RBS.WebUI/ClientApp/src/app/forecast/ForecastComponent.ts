﻿import { Component, OnInit, TemplateRef, ViewChild, OnDestroy, ElementRef, ChangeDetectorRef } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ServiceDocument, CommonService, MessageResult, MessageType } from "@agility/frameworkcore";
import { MatDialogRef } from "@angular/material";
import { GridOptions, GridApi } from "ag-grid-community";
import { Subscription } from "rxjs";
import { SharedService } from "../Common/SharedService";
import { LoaderService } from "../Common/LoaderService";
import { SupplierDomainModel } from "../Common/DomainData/SupplierDomainModel";
import { DivisionDomainModel } from "../Common/DomainData/DivisionDomainModel";
import { DocumentsConstants } from "../Common/DocumentsConstants";
import { ForecastHeadModel } from "./ForecastHeadModel";
import { ForecastService } from "./ForecastService";
import { LocationDomainModel } from "../Common/DomainData/LocationDomainModel";
import { ForecastDetailModel } from "./ForecastDetailModel";

@Component({
    selector: "forecast",
    templateUrl: "./ForecastComponent.html"
})

export class ForecastComponent implements OnInit {
    componentName: any;
    columns: any[];
    public sheetName: string = "Forecast Detail";
    data: ForecastDetailModel[] = [];
    public gridOptions: GridOptions;

    _componentName = "Forecast";
    PageTitle: string = "Forecast";
    public serviceDocument: ServiceDocument<ForecastHeadModel>;
    localizationData: any;
    model: ForecastHeadModel;
    editMode: boolean; editModel: ForecastHeadModel;
    primaryId: number; // used in CanDeactivate method, should be same name if we use candeactivate
    routeServiceSubscription: Subscription;
    documentsConstants: DocumentsConstants = new DocumentsConstants();
    screenName: string;
    supplierType: string;
    showExcelLink: boolean;
    bulkStatus: string;
    locationData: LocationDomainModel[];
    gridApi: GridApi;

    constructor(private route: ActivatedRoute, public router: Router, private changeRef: ChangeDetectorRef
        , public sharedService: SharedService, private service: ForecastService, private loaderService: LoaderService) {
    }

    ngAfterViewChecked(): void {
        this.changeRef.detectChanges();
    }

    ngOnInit(): void {
        this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
        this.locationData = Object.assign([], this.service.serviceDocument.domainData["location"]);
        this.screenName = this.documentsConstants.forecastObjectName;
        this.componentName = this;
        this.columns = [
            { headerName: "Item", field: "item", tooltipField: "item" },
            { headerName: "Item Desc", field: "itemDesc", tooltipField: "itemDesc" },
            { headerName: "Department", field: "deptDesc", tooltipField: "deptDesc" },
            { headerName: "Category", field: "categoryDesc", tooltipField: "categoryDesc" },
            { headerName: "Fineline", field: "classDesc", tooltipField: "classDesc" },
            { headerName: "Segment", field: "subclassDesc", tooltipField: "subclassDesc" },
            { headerName: "F New Min", field: "newMin", tooltipField: "newMin" },
            { headerName: "F New Max", field: "newMax", tooltipField: "newMax" },
        ];
        this.gridOptions = {
            onGridReady: (params: any) => {
                this.gridApi = params.api;
            },
            context: {
                componentParent: this
            }
        };

        if (this.service.serviceDocument.dataProfile.dataModel) {
            this.bind();
        }
    }

    bind(): void {
        this.routeServiceSubscription = this.route.params.subscribe((resp: any) => {
            this.primaryId = resp["id"];
        });
        if (this.primaryId.toString() !== "0") {
            this.editMode = true;
            this.showExcelLink = true;
            this.PageTitle = `${this.PageTitle} - EDIT - ${this.primaryId}`;
        } else {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign(this.service.serviceDocument.dataProfile.dataModel
                , { operation: "I", status: "Worksheet" }
                , { workflowInstance: this.service.serviceDocument.dataProfile.dataModel.workflowInstance });
            this.service.serviceDocument.dataProfile.profileForm.patchValue(this.service.serviceDocument.dataProfile.dataModel);
            this.PageTitle = `${this.PageTitle} - ADD`;
        }
        if (this.editMode) {
            this.bulkStatus = this.service.serviceDocument.dataProfile.dataModel.status;
            if (this.service.serviceDocument.dataProfile.dataModel.forecastDetails) {
                Object.assign(this.data, this.service.serviceDocument.dataProfile.dataModel.forecastDetails);
            }
        }
        this.serviceDocument = this.service.serviceDocument;
        this.sharedService.fileCount = this.service.serviceDocument.dataProfile.dataModel.fileCount;
        this.sharedService.checkWfPermitions(this.serviceDocument);
    }

    save(saveOnly: boolean): void {
        if (this.bulkStatus !== "RequestCompleted") {
            if (this.serviceDocument.dataProfile.profileForm.valid) {
                this.saveSubscription(saveOnly);
            } else {
                this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
            }
        }
    }

    saveAndContinue(): void {
        this.save(false);
    }

    saveSubscription(saveOnly: boolean): void {
        this.loaderService.display(true);
        this.service.save().subscribe(() => {
            if (this.service.serviceDocument.result.type === MessageType.success) {
                this.primaryId = this.service.serviceDocument.dataProfile.dataModel.id;
                this.sharedService.saveForm(`${this.localizationData.forecast.forecastsave} - Request ID ${this.primaryId}`).subscribe(() => {
                    if (saveOnly) {
                        this.router.navigate(["/Forecast/List"], { skipLocationChange: true });
                    } else {
                        this.router.navigate(["/Blank"], {
                            skipLocationChange: true, queryParams: {
                                id: "/Forecast/New/" + this.primaryId
                            }
                        });
                        this.loaderService.display(false);
                        this.openExcel(true);
                    }
                }, (error: string) => { console.log(error); });
            } else {
                this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
            }
        }, (error: string) => { console.log(error); });
    }

    reset(): void {
        Object.keys(this.serviceDocument.dataProfile.profileForm.controls).forEach(control => {
            this.serviceDocument.dataProfile.profileForm.controls[control].setValue(this.serviceDocument.dataProfile.dataModel[control]);
        });
        this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
    }

    openExcel(saveOnly: boolean): void {
        let locName: string = this.locationData.filter(itm => itm.locationId
            === +this.serviceDocument.dataProfile.profileForm.controls["locationId"].value)[0].locationName;
        this.service.downloadExcel(this.service.serviceDocument.dataProfile.dataModel.id, locName);
    }

    ngOnDestroy(): void {
        this.routeServiceSubscription.unsubscribe();
    }

    submitAction(): void {
        this.loaderService.display(true);
        this.service.submit().subscribe(() => {
            if (this.service.serviceDocument.result.type === MessageType.success) {
                this.primaryId = this.service.serviceDocument.dataProfile.dataModel.id;
                this.sharedService.saveForm(`${this.localizationData.forecast.forecastsave} - Request ID ${this.primaryId}`).subscribe(() => {
                    this.router.navigate(["/Forecast/List"], { skipLocationChange: true });
                }, (error: string) => {
                    console.log(error);
                    this.loaderService.display(false);
                });
            } else {
                this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
                this.loaderService.display(false);
            }
        }, (error: string) => {
            console.log(error);
            this.loaderService.display(false);
        });
    }
}
