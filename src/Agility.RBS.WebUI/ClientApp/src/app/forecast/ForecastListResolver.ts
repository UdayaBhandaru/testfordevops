﻿import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { ForecastHeadModel } from "./ForecastHeadModel";
import { ForecastService } from "./ForecastService";

@Injectable()
export class ForecastListResolver implements Resolve<ServiceDocument<ForecastHeadModel>> {
    constructor(private service: ForecastService) { }
    resolve(): Observable<ServiceDocument<ForecastHeadModel>> {
        return this.service.list();
    }
}