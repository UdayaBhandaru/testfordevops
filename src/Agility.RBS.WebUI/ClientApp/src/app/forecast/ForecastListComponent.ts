import { Component, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { SharedService } from "../Common/SharedService";
import { GridEditComponent } from "../Common/grid/GridEditComponent";
import { GridExportModel } from "../Common/grid/GridExportModel";
import { InfoComponent } from "../Common/InfoComponent";
import { SupplierDomainModel } from "../Common/DomainData/SupplierDomainModel";
import { DomainDetailModel } from "../domain/DomainDetailModel";
import { UserDomainModel } from "../Common/DomainData/UserDomainModel";
import { DivisionDomainModel } from "../Common/DomainData/DivisionDomainModel";
import { WorkflowStatusDomainModel } from "../Common/DomainData/WorkflowStatusDomainModel";
import { SearchComponent } from "../Common/SearchComponent";
import { ForecastHeadModel } from "./ForecastHeadModel";
import { ForecastService } from "./ForecastService";
import { LocationDomainModel } from "../Common/DomainData/LocationDomainModel";
import { GridOptions } from 'ag-grid-community';

@Component({
  selector: "forecast-list",
  templateUrl: "./ForecastListComponent.html"
})
export class ForecastListComponent implements OnInit {
  public serviceDocument: ServiceDocument<ForecastHeadModel>;
  PageTitle = "Forecast";
  redirectUrl = "Forecast/New/";
  componentName: any;
  columns: any[];
  domainDtlData: DomainDetailModel[];
  model: ForecastHeadModel = {
    comments: null, id: null, createdBy: null, workflowInstance: null, status: null, locationId: null, locationDesc: null
    , operation: "I", tableName: null
  };
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  locationData: LocationDomainModel[];
  workflowStatusList: WorkflowStatusDomainModel[];
  gridOptions: GridOptions;

  constructor(public service: ForecastService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "ID", field: "id", tooltipField: "id", width: 60 },
      { headerName: "Comments", field: "comments", tooltipField: "comments" },
      { headerName: "Location", field: "locationName", tooltipField: "locationName", width: 100 },
      { headerName: "Status", field: "status", tooltipField: "status", width: 70 },
      {
        headerName: "ACTIONS", field: "Actions", width: 70, cellRendererFramework: GridEditComponent,
      }
    ];

    this.workflowStatusList = Object.assign([], this.service.serviceDocument.domainData["workflowStatusList"]);
    this.locationData = Object.assign([], this.service.serviceDocument.domainData["location"]);

    this.sharedService.domainData = {
      location: this.locationData
    };
    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl + cell.data.id, this.service.serviceDocument.dataProfile.dataList);
  }

  addValidation(): void {
    this.router.navigate(["/" + this.redirectUrl + "0"], { skipLocationChange: true });
    this.sharedService.previousUrl = "../Forecast/List";
  }
}
