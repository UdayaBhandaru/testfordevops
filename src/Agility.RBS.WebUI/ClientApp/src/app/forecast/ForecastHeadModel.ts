﻿import { ForecastDetailModel } from "./ForecastDetailModel";

export class ForecastHeadModel {
    comments?: string;
    operation?: string;
    id?: number;
    tableName?: string;
    locationId?: number;
    locationDesc?: string;
    createdBy?: string;
    workflowInstance: string;
    status?: string;
    fileCount?: number;
    forecastDetails?: ForecastDetailModel[]
}