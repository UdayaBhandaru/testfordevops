﻿import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ForecastListComponent } from "./ForecastListComponent";
import { ForecastListResolver } from "./ForecastListResolver";
import { ForecastComponent } from "./ForecastComponent";
import { ForecastResolver } from "./ForecastResolver";

const ForeCastRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: ForecastListComponent,
                resolve:
                {
                        references: ForecastListResolver
                }
            },
            {
                path: "New/:id",
                component: ForecastComponent,
                resolve:
                    {
                        references: ForecastResolver
                    }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(ForeCastRoutes)
    ]
})
export class ForecastRoutingModule { }
