﻿import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument, WorkflowAction } from "@agility/frameworkcore";
import { HttpHelperService } from "../Common/HttpHelperService";
import { HttpParams, HttpHeaders, HttpClient } from "@angular/common/http";
import { SharedService } from "../Common/SharedService";
import { ForecastHeadModel } from "./ForecastHeadModel";
import { startWith, map } from "rxjs/operators";

@Injectable()
export class ForecastService {
    serviceDocument: ServiceDocument<ForecastHeadModel> = new ServiceDocument<ForecastHeadModel>();
    searchData: any;
    constructor(private httpHelperService: HttpHelperService, private sharedService: SharedService, private httpClient: HttpClient) { }

    newModel(model: ForecastHeadModel): ServiceDocument<ForecastHeadModel> {
        return this.serviceDocument.newModel(model);
    }

    list(): Observable<ServiceDocument<ForecastHeadModel>> {
        return this.serviceDocument.list("/api/Forecast/List");
    }

    search(): Observable<ServiceDocument<ForecastHeadModel>> {
        return this.serviceDocument.search("/api/Forecast/Search", true);
    }

    save(): Observable<ServiceDocument<ForecastHeadModel>> {
        let currentAction: WorkflowAction = this.serviceDocument.dataProfile.actionService.allowedActions[0];
        if (!this.serviceDocument.dataProfile.dataModel.id && currentAction) {
            this.serviceDocument.dataProfile.profileForm.controls["currentActionId"].setValue(currentAction.transitionClaim);
            return this.serviceDocument.submit("/api/Forecast/Submit", true);
        } else {
            return this.serviceDocument.save("/api/Forecast/Save", true);
        }
    }

    submit(): Observable<ServiceDocument<ForecastHeadModel>> {
        return this.serviceDocument.submit("/api/Forecast/Submit", true);
    }

    addEdit(id: number): Observable<ServiceDocument<ForecastHeadModel>> {
        if (+id === 0) {
            return this.serviceDocument.new("/api/Forecast/New");
        } else {
            return this.serviceDocument.open("/api/Forecast/Open", new HttpParams().set("id", id.toString()));
        }
    }

    downloadExcel(id: number, locName: string): void {
        const type: any = "application/vnd.ms-excel";
        let headers1: HttpHeaders = new HttpHeaders();
        headers1.set("Accept", type);
        this.httpClient.get("/api/Forecast/OpenExcel",
            {
                headers: headers1, responseType: "blob" as "json"
                , params: new HttpParams().set("id", id.toString()).set("locName", locName)
            })
            .pipe(map((response: any) => {
                if (response instanceof Response) {
                    return response.blob();
                }
                return response;
            }))
            .subscribe(res => {
                console.log("start download:", res);
                let url: string = window.URL.createObjectURL(res);
                let a: HTMLAnchorElement = document.createElement("a");
                document.body.appendChild(a);
                a.setAttribute("style", "display: none");
                a.href = url;
                a.download = "Forecast-" + id.toString() + ".xlsm";
                a.click();
                window.URL.revokeObjectURL(url);
                a.remove();
            }, (error: string) => { console.log(error); });
    }
}