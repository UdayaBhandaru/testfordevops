﻿export class ForecastDetailModel {
    divisionDesc: string;
    deptDesc: string;
    categoryDesc: string;
    classDesc: string;
    subclassDesc: string;
    item: string;
    itemDesc: string;
    newMin: string;
    newMax: string;
}