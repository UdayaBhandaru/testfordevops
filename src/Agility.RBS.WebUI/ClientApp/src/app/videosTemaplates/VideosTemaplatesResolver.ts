import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { DomainDetailModel } from "../domain/DomainDetailModel";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { VideosTemamplatesService } from "./VideosTemaplatesService";
import { DomainHeaderModel } from '../domain/DomainHeaderModel';

@Injectable()
export class VideosTemamplatesResolver implements Resolve<ServiceDocument<DomainHeaderModel>> {
  constructor(private service: VideosTemamplatesService) { }
    resolve(): Observable<ServiceDocument<DomainHeaderModel>> {
        return this.service.list();
    }
}
