export class VideosTemamplatesDetailModel {
    code?: string;
    codeDesc?: string;
    requiredInd?: string = "0";
    codeSeq?: number;
    codeType?: string;
}
