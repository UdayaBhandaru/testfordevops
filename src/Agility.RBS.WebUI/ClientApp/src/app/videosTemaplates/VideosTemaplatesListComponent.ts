import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { VideosTemamplatesRoutingModule } from "./VideosTemaplatesRoutingModule";
import { VideosTemamplatesService } from "./VideosTemaplatesService";
import { VideosTemamplatesResolver } from "./VideosTemaplatesResolver";
import { FormGroup } from "@angular/forms";
import { SharedService } from "../Common/SharedService";
import { GridEditComponent } from "../Common/grid/GridEditComponent";
import { GridExportModel } from "../Common/grid/GridExportModel";
import { InfoComponent } from "../Common/InfoComponent";
import { GridOptions } from 'ag-grid-community';
import { DomainHeaderModel } from '../domain/DomainHeaderModel';
import { DomainDetailModel } from '../domain/DomainDetailModel';

@Component({
  selector: "Domain-List",
  templateUrl: "./VideosTemaplatesListComponent.html",
  styleUrls: ['./VideosTemaplatesListComponent.scss']
})
export class VideosTemamplatesListComponent implements OnInit {
  public serviceDocument: ServiceDocument<DomainHeaderModel>;
  PageTitle = "Help Videos";
  redirectUrl = "DomainHeader";
  componentName: any;
  columns: any[];
  domainDtlData: DomainDetailModel[];
  model: DomainHeaderModel = { domainId: null, domainDesc: null, domainStatus: null, domainStatusDesc: null };
  references: DomainDetailModel[];
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  gridOptions: GridOptions;

  constructor(public service: VideosTemamplatesService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "DoMAIN ID", field: "domainId", tooltipField: "domainId", width: 90 },
      { headerName: "DESCRIPTION", field: "domainDesc", tooltipField: "domainDesc" },
      //{ headerName: "STATUS", field: "domainStatusDesc", tooltipField: "domainStatusDesc", width: 40 },
      {
        headerName: "ACTIONS", field: "Actions", cellRendererFramework: GridEditComponent, width: 40
      }
    ];

    this.domainDtlData = this.service.serviceDocument.domainData["status"];
    this.sharedService.domainData = { status: this.domainDtlData };
    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }
}
