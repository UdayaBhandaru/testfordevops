import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../Common/HttpHelperService";
import { DomainHeaderModel } from '../domain/DomainHeaderModel';

@Injectable()
export class VideosTemamplatesService {
    serviceDocument: ServiceDocument<DomainHeaderModel> = new ServiceDocument<DomainHeaderModel>();
    searchData: any;
    constructor(private httpHelperService: HttpHelperService) { }

    newModel(model: DomainHeaderModel): ServiceDocument<DomainHeaderModel> {
        return this.serviceDocument.newModel(model);
    }

    list(): Observable<ServiceDocument<DomainHeaderModel>> {
        return this.serviceDocument.list("/api/UtilityDomain/List");
    }

    search(): Observable<ServiceDocument<DomainHeaderModel>> {
        return this.serviceDocument.search("/api/UtilityDomain/Search");
    }

    save(): Observable<ServiceDocument<DomainHeaderModel>> {
        return this.serviceDocument.save("/api/UtilityDomain/Save", true);
    }

    isExistingDomain(name: string): Observable<boolean> {
        return this.httpHelperService.get("/api/UtilityDomain/IsExistingDomain", new HttpParams().set("name", name));
    }
}
