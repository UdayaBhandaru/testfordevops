import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { VideosTemamplatesRoutingModule } from "./VideosTemaplatesRoutingModule";
import { VideosTemamplatesListComponent } from "./VideosTemaplatesListComponent";
import { VideosTemamplatesService } from "./VideosTemaplatesService";
import { VideosTemamplatesResolver } from "./VideosTemaplatesResolver";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../AgGridSharedModule";
import { TitleSharedModule } from "../TitleSharedModule";
import { RbsSharedModule } from "../RbsSharedModule";
import { MatVideoModule } from 'mat-video';

@NgModule({
    imports: [
        FrameworkCoreModule,
        VideosTemamplatesRoutingModule,
        AgGridModule,
        AgGridSharedModule,
        TitleSharedModule,
      RbsSharedModule,
      MatVideoModule
    ],
    declarations: [
       VideosTemamplatesListComponent,
       
    ],
    providers: [
        VideosTemamplatesService,
       VideosTemamplatesResolver
    ]
})
export class VideosTemamplatesModule {
}
