import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { VideosTemamplatesListComponent } from "./VideosTemaplatesListComponent";
import { VideosTemamplatesResolver } from "./VideosTemaplatesResolver";

const VideosTemamplatesRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: VideosTemamplatesListComponent,
                resolve:
                {
                  references: VideosTemamplatesResolver
                }
            },
        ]
    }
];

@NgModule({
    imports: [
      RouterModule.forChild(VideosTemamplatesRoutes)
    ]
})
export class VideosTemamplatesRoutingModule { }
