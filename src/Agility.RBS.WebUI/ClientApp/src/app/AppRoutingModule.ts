import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { BlankComponent } from "./Common/BlankComponent";
import { AppCustomPreloader } from "./AppCustomPreloader";
import { SubClassModule } from "./merchanthierarchy/subClass/SubClassModule";
import { VatCodeModule } from "./valueaddedtax/vatcode/VatCodeModule";
import { VatRateModule } from "./valueaddedtax/vatrates/VatRateModule";
import { VatRegionModule } from "./valueaddedtax/vatregion/VatRegionModule";
import { VatRegionVatCodeModule } from "./valueaddedtax/vatregionvatcode/VatRegionVatCodeModule";
const appRoutes: Routes = [
  {
    path: "Blank",
    component: BlankComponent
  },
  {
    path: "UOM",
    loadChildren: "./unitofmeasure/uom/UnitOfMeasureModule#UnitOfMeasureModule"
  },
  {
    path: "UOMConversion",
    loadChildren: "./unitofmeasure/uomconversion/UnitOfMeasureConversionModule#UnitOfMeasureConversionModule"
  },
  {
    path: "DomainHeader",
    loadChildren: "./domain/DomainModule#DomainModule"
  },
  {
    path: "Brand",
    loadChildren: "./brand/brand/BrandModule#BrandModule"
  },
  {
    path: "BrandCategory",
    loadChildren: "./brand/brandcategory/BrandCategoryModule#BrandCategoryModule"
  },
  {
    path: "CostChange",
    loadChildren: "./CostChange/CostManagementModule#CostManagementModule"
  },
  {
    path: "Country",
    loadChildren: "./country/CountryModule#CountryModule"
  },
  {
    path: "Currency",
    loadChildren: "./currency/CurrencyModule#CurrencyModule"
  },
  {
    path: "Company",
    loadChildren: "./organizationhierarchy/company/CompanyModule#CompanyModule"
  },

  {
    path: "employee",
    loadChildren: "./employeeMaintenance/EmployeeModule#EmployeeModule"
  },
  {
    path: "Format",
    loadChildren: "./organizationhierarchy/format/FormatModule#FormatModule"
  },
  {
    path: "Location",
    loadChildren: "./organizationhierarchy/location/LocationModule#LocationModule"
  },
  {
    path: "Region",
    loadChildren: "./organizationhierarchy/region/RegionModule#RegionModule"
  },
  {
    path: "OrgCountry",
    loadChildren: "./organizationhierarchy/orgcountry/OrgCountryModule#OrgCountryModule"
  },
  {
    path: "Home",
    loadChildren: "./home/HomeModule#HomeModule"
  }
  ,
  {
    path: "Inbox",
    loadChildren: "./inbox/InboxModule#InboxModule"
  }
  ,
  {
    path: "PriceInbox",
    loadChildren: "./processInbox/price/PriceInboxModule#PriceInboxModule"
  }
  ,
  {
    path: "CostInbox",
    loadChildren: "./processInbox/cost/CostInboxModule#CostInboxModule"
  }
  ,
  {
    path: "OrderInbox",
    loadChildren: "./processInbox/order/OrderInboxModule#OrderInboxModule"
  }
  ,
  {
    path: "BulkItemInbox",
    loadChildren: "./processInbox/bulkitem/BulkItemInboxModule#BulkItemInboxModule"
  }
  ,
  {
    path: "PromotionInbox",
    loadChildren: "./processInbox/promotion/PromoInboxModule#PromoInboxModule"
  }
  ,
  {
    path: "ItemCloseInbox",
    loadChildren: "./processInbox/itemclose/ItemCloseInboxModule#ItemCloseInboxModule"
  }
  ,
  {
    path: "SuplierInbox",
    loadChildren: "./processInbox/suplier/SuplierInboxModule#SuplierInboxModule"
  }
  ,
  {
    path: "ObligationInbox",
    loadChildren: "./processInbox/obligation/ObligationInboxModule#ObligationInboxModule"
  }
  ,
  {
    path: "TransferInbox",
    loadChildren: "./processInbox/transfer/TransferInboxModule#TransferInboxModule"
  }
  ,
  {
    path: "RtvInbox",
    loadChildren: "./processInbox/rtv/RtvInboxModule#RtvInboxModule"
  }
  ,
  {
    path: "ItemReactivateInbox",
    loadChildren: "./processInbox/itemreactivate/ItemReactivateInboxModule#ItemReactivateInboxModule"
  }
  ,
  {
    path: "Item",
    loadChildren: "./item/ItemModule#ItemModule",
    data: { preload: true }
  }
  ,
  {
    path: "ItemListHead",
    loadChildren: "./itemlist/ItemListModule#ItemListModule"
  },
  {
    path: "Group",
    loadChildren: "./merchanthierarchy/group/GroupModule#GroupModule"
  },
  {
    path: "Class",
    loadChildren: "./merchanthierarchy/class/ClassModule#ClassModule"
  },
  {
    path: "Department",
    loadChildren: "./merchanthierarchy/department/DepartmentModule#DepartmentModule"
  },
  {
    path: "Division",
    loadChildren: "./merchanthierarchy/division/DivisionModule#DivisionModule"
  },
  {
    path: "SubClass",
    loadChildren: "./merchanthierarchy/subClass/SubClassModule#SubClassModule"
  },
  {
    path: "PriceChange",
    loadChildren: "./pricechange/PriceChangeModule#PriceChangeModule"
  },

  {
    path: "Promotion",
    loadChildren: "./promo/PromotionModule#PromotionModule"
  },

  {
    path: "Phase",
    loadChildren: "./season/phase/PhaseModule#PhaseModule"
  },
  {
    path: "Season",
    loadChildren: "./season/season/SeasonModule#SeasonModule"
  },
  {
    path: "Roles",
    loadChildren: "./season/season/SeasonModule#SeasonModule"
  },
  {
    path: "Users",
    loadChildren: "./security/UsersSecurityModule#UsersSecurityModule"
  },
  {
    path: "SupplierMaster",
    loadChildren: "./Supplier/SupplierMaster/SupplierMasterModule#SupplierMasterModule"
  },
  {
    path: "SupplierAddressMaster",
    loadChildren: "./Supplier/SupplierAddressMaster/SupplierAddressMasterModule#SupplierAddressMasterModule"
  },
  {
    path: "VatCode",
    loadChildren: "./valueaddedtax/vatcode/VatCodeModule#VatCodeModule"
  },
  {
    path: "vatrates",
    loadChildren: "./valueaddedtax/vatrates/VatRateModule#VatRateModule"
  },
  {
    path: "VatRegion",
    loadChildren: "./valueaddedtax/vatregion/VatRegionModule#VatRegionModule"
  },
  {
    path: "VatRegionVatCode",
    loadChildren: "./valueaddedtax/vatregionvatcode/VatRegionVatCodeModule#VatRegionVatCodeModule"
  },
  {
    path: "Order",
    loadChildren: "./order/OrderModule#OrderModule"

  },
  {
    path: "SupplyChain",
    loadChildren: "./SupplyChainManagement/SupplyChainModule#SupplyChainModule"
  },
  {
    path: "Partner",
    loadChildren: "./partner/PartnerModule#PartnerModule"
  },
  {
    path: "Deal",
    loadChildren: "./DealManagement/DealModule#DealModule"

  },
  {
    path: "SupplierGroup",
    loadChildren: "./SupplierGroup/SupplierGroupModule#SupplierGroupModule"
  },
  {
    path: "BulkItem",
    loadChildren: "./bulkItem/BulkItemModule#BulkItemModule"
  },
  {
    path: "ItemSuperSearch",
    loadChildren: "./itemSuper/ItemSuperModule#ItemSuperModule"
  },
  {
    path: "ItemClose",
    loadChildren: "./ItemClose/ItemCloseHeadModule#ItemCloseHeadModule"
  }
  ,
  {
    path: "ItemReactive",
    loadChildren: "./ItemReactivate/ItemReactiveHeadModule#ItemReactiveHeadModule"
  },
  {
    path: "Forecast",
    loadChildren: "./forecast/ForecastModule#ForecastModule"
  },
  {
    path: "DsdOrders",
    loadChildren: "./DsdOrders/DsdOrderModule#DsdOrderModule"
  },
  {
    path: "Inventory",
    loadChildren: "./inventoryManagement/InventoryModule#InventoryModule"
  },
  {
    path: "ReturnToVendor",
    loadChildren: "./returnToVendor/ReturnToVendorModule#ReturnToVendorModule"
  },
  {
    path: "TransferEntity",
    loadChildren: "./transfer/TransferEntity/TransferEntityModule#TransferEntityModule"
  },
  {
    path: "TransferZone",
    loadChildren: "./transfer/transferzone/TransferZoneModule#TransferZoneModule"
  },
  {
    path: "Shipment",
    loadChildren: "./shipment/ShipmentModule#ShipmentModule"
  },
  {
    path: "TransactionView",
    loadChildren: "./finance/transactiondata/TransactionViewModule#TransactionViewModule"
  },
  {
    path: "Transfer",
    loadChildren: "./TransferManagement/TransferModule#TransferModule"

  },
  {
    path: "FreightType",
    loadChildren: "./Freight/FreightType/FreightTypeModule#FreightTypeModule"
  },
  {
    path: "FreightSize",
    loadChildren: "./Freight/FreightSize/FreightSizeModule#FreightSizeModule"
  },
  {
    path: "LocationTrait",
    loadChildren: "./Traits/LocationTrait/LocationTraitModule#LocationTraitModule"
  },
  {
    path: "SupplierTrait",
    loadChildren: "./Traits/SupplierTrait/SupplierTraitModule#SupplierTraitModule"
  },
  {
    path: "supplieroptions",
    loadChildren: "./administration/SupplierOption/SupplierOptionsModule#SupplierOptionsModule"
  },
  {
    path: "systemoptions",
    loadChildren: "./administration/SystemOption/SystemOptionsModule#SystemOptionsModule"
  },
  {
    path: "tolerance",
    loadChildren: "./administration/Tolerance/ToleranceModule#ToleranceModule"
  },
  {
    path: "securitygroup",
    loadChildren: "./administration/SecurityGroup/SecurityGroupModule#SecurityGroupModule"
  },
  {
    path: "UserRolePrivileges",
    loadChildren: "./administration/UserRolePrivileges/UserRolePrivilegesModule#UserRolePrivilegesModule"
  },
  {
    path: "SecurityGroupHierarchy",
    loadChildren: "./administration/SecurityGroupHierarchy/GroupHierarchyModule#GroupHierarchyModule"
  },
  {
    path: "SecurityGroupOrgHierarchy",
    loadChildren: "./administration/SecurityGroupOrgHierarchy/GroupOrgHierarchyModule#GroupOrgHierarchyModule"
  },

  {
    path: "RegionalityGroup",
    loadChildren: "./administration/RegionalityGroup/RegionalityGroupModule#RegionalityGroupModule"
  },
  {
    path: "RegionalityUserGroup",
    loadChildren: "./administration/RegionalityUserGroup/RegionalityUserGroupModule#RegionalityUserGroupModule"
  },
  {
    path: "reasoncode",
    loadChildren: "./administration/ReasonCode/ReasonCodeModule#ReasonCodeModule"
  },
  {
    path: "InventoryAdjutmentReason",
    loadChildren: "./inventoryManagement/invAdjustmentReason/InvAdjustmentReasonModule#InvAdjustmentReasonModule"
  },
  {
    path: "Competitors",
    loadChildren: "./competitors/CompetitorsModule#CompetitorsModule"
  },
  {
    path: "CompetitivePriceHistory",
    loadChildren: "./competitivePriceHistory/CompetitivePriceHistoryModule#CompetitivePriceHistoryModule"
  },
  {
    path: "Sales",
    loadChildren: "./sales/SalesModule#SalesModule"
  },
  {
    path: "Chain",
    loadChildren: "./organizationhierarchy/chain/ChainModule#ChainModule"
  }
  ,
  {
    path: "Area",
    loadChildren: "./organizationhierarchy/area/AreaModule#AreaModule"
  },
  {
    path: "District",
    loadChildren: "./organizationhierarchy/district/DistrictModule#DistrictModule"
  },
  {
    path: "CostChangeReason",
    loadChildren: "./costzone/costchangereason/CostChangeReasonModule#CostChangeReasonModule"
  },
  {
    path: "CostZoneGroup",
    loadChildren: "./costzone/costzonegroup/CostZoneGroupModule#CostZoneGroupModule"
  },
  {
    path: "StoreFormat",
    loadChildren: "./storeformat/StoreFormatModule#StoreFormatModule"
  },
  {
    path: "Obligation",
    loadChildren: "./transportationManagement/obligation/ObligationModule#ObligationModule"
  },
  {
    path: "DsdMinMax",
    loadChildren: "./dsdminmax/DsdMinMaxModule#DsdMinMaxModule"
  },
  {
    path: "Buyer",
    loadChildren: "./buyer/BuyerModule#BuyerModule"
  },
  {
    path: "PriceZoneGroup",
    loadChildren: "./pricezone/pricezonegroup/PriceZoneGroupModule#PriceZoneGroupModule"
  }
  ,
  {
    path: "CostZone",
    loadChildren: "./costzone/costzone/CostZoneModule#CostZoneModule"
  },
  {
    path: "Receiving",
    loadChildren: "./receiving/ReceivingModule#ReceivingModule"
  },
  {
    path: "ItemReclassification",
    loadChildren: "./itemreclassification/ItemReclassificationModule#ItemReclassificationModule"
  }
  ,
  {
    path: "Outloc",
    loadChildren: "./outloc/OutLocModule#OutLocModule"
  }
  ,
  {
    path: "GLCrossReference",
    loadChildren: "./inventoryManagement/GLCrossReference/GlCrossReferenceModule#GlCrossReferenceModule"
  },
  {
    path: "UserAttributes",
    loadChildren: "./userAttributes/UserAttributesModule#UserAttributesModule"
  },
  {
    path: "DynamicHierarchy",
    loadChildren: "./dynamicHierarchy/DynamicHierarchyModule#DynamicHierarchyModule"
  },
  {
    path: "WareHouse",
    loadChildren: "./organizationhierarchy/wareHouse/WareHouseModule#WareHouseModule"
  }
  ,
  {
    path: "Half",
    loadChildren: "./half/HalfModule#HalfModule"
  }
  ,
  {
    path: "CurrencyExchangeType",
    loadChildren: "./currencyExchangeType/CurrencyExchangeTypeModule#CurrencyExchangeTypeModule"
  }
  ,
  {
    path: "Document",
    loadChildren: "./document/DocumentModule#DocumentModule"
  },
  {
    path: "NbSystemOptions",
    loadChildren: "./administration/NbSystemOptions/NbSystemOptionsModule#NbSystemOptionsModule"
  },
  {
    path: "RpmSystemOptions",
    loadChildren: "./administration/RpmSystemOptions/RpmSystemOptionsModule#RpmSystemOptionsModule"
  },
  {
    path: "SaSystemOptions",
    loadChildren: "./administration/SaSystemOptions/SaSystemOptionsModule#SaSystemOptionsModule"
  },
  {
    path: "help",
    loadChildren: "./videosTemaplates/VideosTemaplatesModule#VideosTemamplatesModule"
  },
  {
    path: "invoiceMatchingDocument",
    loadChildren: "./invoiceMatchingDoc/InvoiceMatchingDocModule#InvoiceMatchingDocModule"
  },
  {
    path: "ShipmentTracking",
    loadChildren: "./shipmenttrack/ShipmentTrackingModule#ShipmentTrackingModule"
  },
  {
    path: "CurrencyRate",
    loadChildren: "./currencyrate/CurrencyRateModule#CurrencyRateModule"
  },
  {
    path: "imCostDiscrepancy",
    loadChildren: "./invoiceMatchingDoc/CostDiscrepancies/CostDiscrepanciesModule#CostDiscrepanciesModule"
  },
  {
    path: "imQtyDiscrepancy",
    loadChildren: "./invoiceMatchingDoc/QuantityDiscrepancies/QuantityDiscrepanciesModule#QuantityDiscrepanciesModule"
  },
  {
    path: "stockcount",
    loadChildren: "./stockCount/StockCountModule#StockCountModule"
  },
  {
    path: "Transportation",
    loadChildren: "./transportation/TransportationModule#TransportationModule"
  },
  {
    path: "allocation",
    loadChildren: "./allocation/AllocationModule#AllocationModule"
  },
  {
    path: "imDocMaintenance",
    loadChildren: "./invoiceMatchingDoc/DocMaintenance/ImDocMaintenanceModule#ImDocMaintenanceModule"
  }
  ,
  {
    path: "NonMerchCode",
    loadChildren: "./administration/NonMerchMaintainance/NonMerchandiseModule#NonMerchandiseModule"
  },
  {
    path: "MessageSearch",
    loadChildren: "./administration/MessageSearch/MSModule#MSModule"
  }
  ,
  {
    path: "AverageCostAdjustment",
    loadChildren: "./AverageCostAdjustment/AverageCostAdjustmentModule#AverageCostAdjustmentModule"
  },
  {
    path: "ItemPriceHistory",
    loadChildren: "./itempricehistory/ItemPriceHistoryModule#ItemPriceHistoryModule"
  },
  {
    path: "SystemVariables",
    loadChildren: "./administration/SystemVariables/SVModule#SVModule"
  },
  {
    path: "ItemUda",
    loadChildren: "./itemuda/ItemUdaModule#ItemUdaModule"
  },
  {
    path: "ReceiverCostAdjustment",
    loadChildren: "./ReceiverCostAdjustment/ReceiverCostAdjustmentModule#ReceiverCostAdjustmentModule"
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, { preloadingStrategy: AppCustomPreloader })
  ],
  providers: [AppCustomPreloader]
})
export class AppRoutingModule { }
