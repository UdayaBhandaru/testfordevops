import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import {  AverageCostAdjustmentResolver} from "./AverageCostAdjustmentResolver";
import { AverageCostAdjustmentComponent } from "./AverageCostAdjustmentComponent";
const UtilityRoutes: Routes = [
    {
        path: "",
        children: [
            {
            path: "", redirectTo: "New", pathMatch: "full"
            },            
            {
                path: "New",
                component: AverageCostAdjustmentComponent,
                resolve:
                {
                    serviceDocument: AverageCostAdjustmentResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(UtilityRoutes)
    ]
})
export class AverageCostAdjustmentRoutingModule { }
