export class AverageCostAdjustmentLocModel {
  item: string;
  itemDesc: string;
  category?: number;
  categoryDesc: string;
  loc?: number;
  locName: string;
  locType: string;
  locTypeDesc?: string;
  avCost?: number;
  unitCost?: number;
  newAvCost?: number;
  currencyCode: string;
  stockOnHand?: number;
  inTransitQty?: number;
  packCompIntran?: number;
  packCompSoh?: number; 
  operation?: string;
  tableName?: string;
  error?: string;
}
