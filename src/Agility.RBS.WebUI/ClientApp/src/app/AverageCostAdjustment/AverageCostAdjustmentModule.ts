import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { AverageCostAdjustmentService } from "./AverageCostAdjustmentService";
import { AverageCostAdjustmentRoutingModule } from "./AverageCostAdjustmentRoutingModule";
import { AverageCostAdjustmentComponent } from "./AverageCostAdjustmentComponent";
import { AverageCostAdjustmentResolver } from "./AverageCostAdjustmentResolver";
import { AgGridSharedModule } from '../AgGridSharedModule';
import { TitleSharedModule } from '../TitleSharedModule';
import { RbsSharedModule } from '../RbsSharedModule';

@NgModule({
    imports: [
        FrameworkCoreModule,
        AverageCostAdjustmentRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        AverageCostAdjustmentComponent,
    ],
    providers: [
        AverageCostAdjustmentService,
        AverageCostAdjustmentResolver,
    ]
})
export class AverageCostAdjustmentModule {
    constructor() {
        LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
    }
}
