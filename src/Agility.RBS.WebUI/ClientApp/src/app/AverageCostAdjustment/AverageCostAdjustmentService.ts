import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { AverageCostAdjustmentModel } from "./AverageCostAdjustmentModel";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from '../Common/HttpHelperService';
import { TransactionDataModel } from '../finance/transactiondata/TransactionDataModel';
import { SubClassDomainModel } from '../Common/DomainData/SubClassDomainModel';

@Injectable()
export class AverageCostAdjustmentService {
  serviceDocument: ServiceDocument<AverageCostAdjustmentModel> = new ServiceDocument<AverageCostAdjustmentModel>();
  constructor(private httpHelperService: HttpHelperService) { }

  newModel(model: AverageCostAdjustmentModel): ServiceDocument<AverageCostAdjustmentModel> {
    return this.serviceDocument.newModel(model);
  }

  search(): Observable<ServiceDocument<AverageCostAdjustmentModel>> {
    return this.serviceDocument.search("/api/AverageCostAdjustment/Search");
  }

  save(): Observable<ServiceDocument<AverageCostAdjustmentModel>> {
    return this.serviceDocument.save("/api/AverageCostAdjustment/Save", true);
  }

  list(): Observable<ServiceDocument<AverageCostAdjustmentModel>> {
    return this.serviceDocument.list("/api/AverageCostAdjustment/List");
  }

  addEdit(): Observable<ServiceDocument<AverageCostAdjustmentModel>> {
    return this.serviceDocument.list("/api/AverageCostAdjustment/New");
  }

  subclass(model: TransactionDataModel): Observable<SubClassDomainModel[]> {
    var params = new HttpParams();
    return this.httpHelperService.get(`/api/TransactionData/SubClass?deptid=${String(model.category)}&classId=${String(model.fineLine)}`, params);
  }

  isExistingCostZone(zoneGroupId: string): Observable<boolean> {
    return this.httpHelperService.get("/api/AverageCostAdjustment/IsExistingCostZone", new HttpParams().set("zoneGroupId", zoneGroupId));
  }
  getDataFromAPI(item: string): Observable<any> {
    return this.httpHelperService.get("/api/AverageCostAdjustment/GetItemAtteibutes", new HttpParams().set("item", item));
  }

  GetItemInfo(item: string): Observable<AverageCostAdjustmentModel[]> {
    return this.httpHelperService.get("api/AverageCostAdjustment/GetItemLocations", new HttpParams().set("item", item));
  }
}
