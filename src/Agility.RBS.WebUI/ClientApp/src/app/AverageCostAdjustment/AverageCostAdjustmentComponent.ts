import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ServiceDocument, MessageType, CommonService, MessageResult, FormMode } from "@agility/frameworkcore";
import { FormGroup, FormControl } from "@angular/forms";
import { GridOptions, RowNode } from 'ag-grid-community';
import { AverageCostAdjustmentModel } from './AverageCostAdjustmentModel';
import { DomainDetailModel } from '../domain/DomainDetailModel';
import { CostZoneGroupModel } from '../costzone/costzonegroup/CostZoneGroupModel';
import { AverageCostAdjustmentLocModel } from './AverageCostAdjustmentLocModel';
import { CurrencyDomainModel } from '../Common/DomainData/CurrencyDomainModel';
import { CostZoneGroupDomainModel } from '../Common/DomainData/CostZoneGroupDomainModel';
import { AverageCostAdjustmentService } from './AverageCostAdjustmentService';
import { SharedService } from '../Common/SharedService';
import { GridSelectComponent } from '../Common/grid/GridSelectComponent';
import { GridInputComponent } from '../Common/grid/GridInputComponent';
import { GridEditComponent } from '../Common/grid/GridEditComponent';
import { CommonModel } from '../Common/CommonModel';
import { DepartmentDomainModel } from '../Common/DomainData/DepartmentDomainModel';
import { ClassDomainModel } from '../Common/DomainData/ClassDomainModel';
import { SubClassDomainModel } from '../Common/DomainData/SubClassDomainModel';
import { TransactionDataModel } from '../finance/transactiondata/TransactionDataModel';
import { Subscription, Observable } from 'rxjs';
import { ItemDescpDetailsDomainModel } from '../dsdminmax/ItemDescpDetailsDomainModel';
import { LoaderService } from '../Common/LoaderService';
import { GridNumericComponent } from '../Common/grid/GridNumericComponent';
import { ItemCategeoryDetailsDomainModel } from './ItemCategeoryDetailsDomainModel';

@Component({
  selector: "AverageCostAdjustment",
  templateUrl: "./AverageCostAdjustmentComponent.html"
})

export class AverageCostAdjustmentComponent implements OnInit {
  PageTitle = "Average Cost Adjustment";
  serviceDocument: ServiceDocument<AverageCostAdjustmentModel>;
  averageCostAdjustmentModel: AverageCostAdjustmentModel;
  localizationData: any;
  domainDtlData: DomainDetailModel[]; defaultStatus: string;
  editMode: boolean = false; editModel: AverageCostAdjustmentModel;
  averageCostAdjustmentGroupmodel: CostZoneGroupModel;
  showCustomRequiredMessageforLoc: string;
  private messageResult: MessageResult = new MessageResult();
  componentParentName: AverageCostAdjustmentComponent;
  itemDetailsServiceSubscription: Subscription;
  saveServiceSubscription: Subscription;
  dataList: AverageCostAdjustmentModel[] = [];
  headDisplayModel: AverageCostAdjustmentModel[];
  gridApi: any;
  public gridOptions: GridOptions;
  columns: any[];
  data: AverageCostAdjustmentLocModel[] = [];
  temp: {};
  pForm: any;
  detailModel: AverageCostAdjustmentLocModel;
  deletedRows: any[] = [];

  constructor(private service: AverageCostAdjustmentService, private sharedService: SharedService, private route: ActivatedRoute
    , private router: Router, private ref: ChangeDetectorRef, private commonService: CommonService, private loaderService: LoaderService) {
  }

  mandatoryObligationDetailsFields: string[] = [
    "newAvCost"
  ];
  ngOnInit(): void {

    //this.headDisplayModel = Object.assign([], this.service.serviceDocument.domainData["location"]);
    this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
    this.bind();
    this.componentParentName = this;
  }


  bind(): void {
    this.addItemDetails();
  }

  private addItemDetails() {
    this.columns = [
      //{
      //  headerName: "Location", field: "loc", cellRendererFramework: GridSelectComponent, width: 180,
      //  cellRendererParams: { references: this.headDisplayModel, keyvaluepair: { key: "loc", value: "locName" } }
      //},
      { headerName: "Location", field: "locName", tooltipField: "locName", width: 180 },
      { headerName: "New Average Cost", field: "newAvCost", cellRendererFramework: GridInputComponent, width: 180 },
      //{ headerName: "Category", field: "category", cellRendererFramework: GridNumericComponent, width: 180, hide: true },
      //{ headerName: "Category Desc", field: "categoryDesc", tooltipField: "categoryDesc", width: 180 },
      //{ headerName: "locType", field: "locType", tooltipField: "locType", width: 180 },
      { headerName: "Average Cost", field: "avCost", tooltipField: "avCost", width: 180 },
      { headerName: "Unit Cost", field: "unitCost", tooltipField: "unitCost", width: 180 },
      { headerName: "Currency Code", field: "currencyCode", tooltipField: "currencyCode", width: 180 },
      { headerName: "Stock On Hand", field: "stockOnHand", tooltipField: "stockOnHand", width: 180 },
      { headerName: "In Transit Qty", field: "inTransitQty", tooltipField: "inTransitQty", width: 180 },
      { headerName: "Pack Comp In tran", field: "packCompIntran", tooltipField: "packCompIntran", width: 180 },
      { headerName: "Pack Comp Soh", field: "packCompSoh", tooltipField: "packCompSoh", width: 180, },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 150,
        cellRendererParams: { restrictEditIcon: true }
      }
    ];
    this.gridOptions = {
      onGridReady: (params: any) => {
        this.gridApi = params.api;
        this.gridApi.startEditingCell({
          rowIndex: 0,
          colKey: "loc"
        });
      },
      context: {
        componentParent: this
      },
      enableColResize: true
    };
    delete this.editModel;
    if (this.sharedService.editObject) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
      this.getDetails();
      this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
      Object.assign(this.service.serviceDocument.dataProfile.dataModel, { operation: "U" });
      let arr: AverageCostAdjustmentLocModel[] = [];
      this.service.serviceDocument.dataProfile.dataModel.averageCostAdjustmentLocList.forEach((x) => {
        arr.push(Object.assign({}, x));
      });
      this.editModel.averageCostAdjustmentLocList = arr;
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
      this.data = this.service.serviceDocument.dataProfile.dataModel.averageCostAdjustmentLocList;
      delete this.sharedService.editObject;
      this.editMode = true;
    }
    else {
      this.avgCostAdjmntModel();
      this.service.serviceDocument.dataProfile.dataModel = this.averageCostAdjustmentModel;
      this.data = this.service.serviceDocument.dataProfile.dataModel.averageCostAdjustmentLocList;
      this.service.newModel(this.averageCostAdjustmentModel);
    }
    this.service.serviceDocument.dataProfile.profileForm = this.commonService
      .getFormGroup(this.service.serviceDocument.dataProfile.dataModel, FormMode.Open);
    this.service.serviceDocument.dataProfile.profileForm.controls["averageCostAdjustmentLocList"] = new FormControl();
    this.serviceDocument = this.service.serviceDocument;
  }

  add($event: MouseEvent): void {

    this.pForm = this.serviceDocument.dataProfile.profileForm;
    $event.preventDefault();
    this.getDetails();
    let valuevendor;
    if (this.pForm.controls["item"].value == null) {
      this.sharedService.errorForm("Please select Item number");
    }
    else if (this.pForm.controls["loc"].value == null) {
      this.sharedService.errorForm("Please select Item Location");
    }
    else {
      if (this.pForm.valid) {
        var locListValidation = this.data.filter(id => id.loc == this.pForm.controls["loc"].value);
        if (locListValidation.length > 0) {
          this.sharedService.errorForm("Please select different Item Location to change new avg cost");
        }
        else {
          var locationType = this.dataList.filter(id => id.loc == this.pForm.controls["loc"].value);
          let emptyObjectData: any = {
            item: this.pForm.controls["item"].value, category: locationType[0].category, categoryDesc: locationType[0].categoryDesc, loc: locationType[0].loc, locName: locationType[0].locName,
            locType: locationType[0].locType, locTypeDesc: locationType[0].locTypeDesc, unitCost: locationType[0].unitCost, newAvCost: null, currencyCode: locationType[0].currencyCode, stockOnHand: locationType[0].stockOnHand, inTransitQty: locationType[0].inTransitQty,
            packCompIntran: locationType[0].packCompIntran, packCompSoh: locationType[0].packCompSoh, operation: "I"
          }
          this.newModel();
          this.data.push(Object.assign(emptyObjectData));
          let row = this.data.length;
          if (this.gridApi) {
            this.gridApi.setRowData(this.data);
          }
        }
      } else {
        this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
      }
    }
  }


  change(cell: any, event: any): void {
    if (event.target) {
      if (event.target.value) {
        this.data[cell.rowIndex]["newAvCost"] = event.target.value;
        this.gridApi.setRowData(this.data);
      }
    }
    this.temp = this.data[cell.rowIndex];
  }

  delete(cell: any): void {
    this.gridApi.updateRowData({ remove: [cell.data] });
    cell.data.operation = "D";
    this.deletedRows.push(cell.data);
    this.data.splice(cell.rowIndex, 1);
  }

  select(cell: any, event: any): void {
    //this.data = [];
    this.dataList[cell.rowIndex][cell.column.colId] = event.target.value;
    if (cell.column.colId === "loc") {
      this.dataList[cell.rowIndex]["loc"] = event.target.value;
      var locationType = this.dataList.filter(id => id.loc == event.target.value);
      this.data[cell.rowIndex]["category"] = locationType[0].category;
      this.data[cell.rowIndex]["categoryDesc"] = locationType[0].categoryDesc;
      this.data[cell.rowIndex]["loc"] = locationType[0].loc;
      this.data[cell.rowIndex]["locName"] = locationType[0].locName;
      this.data[cell.rowIndex]["locType"] = locationType[0].locType;
      this.data[cell.rowIndex]["locTypeDesc"] = locationType[0].locTypeDesc;
      this.data[cell.rowIndex]["avCost"] = locationType[0].avCost;
      this.data[cell.rowIndex]["unitCost"] = locationType[0].unitCost;
      this.data[cell.rowIndex]["newAvCost"] = locationType[0].newAvCost;
      this.data[cell.rowIndex]["currencyCode"] = locationType[0].currencyCode;
      this.data[cell.rowIndex]["stockOnHand"] = locationType[0].stockOnHand;
      this.data[cell.rowIndex]["inTransitQty"] = locationType[0].inTransitQty;
      this.data[cell.rowIndex]["packCompIntran"] = locationType[0].packCompIntran;
      this.data[cell.rowIndex]["packCompSoh"] = locationType[0].packCompSoh;

      let rowNode: RowNode = this.gridApi.getRowNode(cell.rowIndex);
      rowNode.setData(rowNode.data);

    }
  }
  reset(): void {

    if (!this.editMode) {
      this.avgCostAdjmntModel();
      this.temp = {};
      this.data = [];

      this.service.serviceDocument.dataProfile.dataModel = this.averageCostAdjustmentModel;
      this.data = this.service.serviceDocument.dataProfile.dataModel.averageCostAdjustmentLocList;
      this.service.newModel(this.averageCostAdjustmentModel);
    } else {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);
      Object.assign(this.service.serviceDocument.dataProfile.dataModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
      let arr: AverageCostAdjustmentLocModel[] = [];
      this.editModel.averageCostAdjustmentLocList.forEach((x) => {
        arr.push(Object.assign({}, x));
      });
      this.service.serviceDocument.dataProfile.dataModel.averageCostAdjustmentLocList = arr;
      this.data = this.service.serviceDocument.dataProfile.dataModel.averageCostAdjustmentLocList;
    }
    this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
  }

  

  save(saveOnly: boolean): void {
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      if (this.data.length > 0) {
        if (this.validateData()) {
          this.saveAverageCostAdjustment(saveOnly);
        } else {
          this.sharedService.errorForm("Please Fill item number Mandatory field");
        }
      } else {
        this.sharedService.errorForm("Atleast 1 New Average Cost Details is required");
      }
    }
    else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  saveAndContinue(): void {
    this.save(false);
  }

  saveAverageCostAdjustment(saveOnly: boolean): void {
    this.serviceDocument.dataProfile.profileForm.controls["averageCostAdjustmentLocList"] = new FormControl();
    this.serviceDocument.dataProfile.profileForm.controls["averageCostAdjustmentLocList"].setValue(this.data);
    this.saveServiceSubscription = this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.sharedService.saveForm("New Average cost saved  successfully.").subscribe(() => {
          if (saveOnly) {
            this.router.navigate(["/AverageCostAdjustment/List"], { skipLocationChange: true });
          } else {
            if (!this.editMode) {
              this.avgCostAdjmntModel();
              this.service.serviceDocument.dataProfile.dataModel = this.averageCostAdjustmentModel;
              this.data = this.service.serviceDocument.dataProfile.dataModel.averageCostAdjustmentLocList;
              this.service.newModel(this.averageCostAdjustmentModel);
            } else {
              Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
              this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.editModel);
              this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            }
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            this.serviceDocument = this.service.serviceDocument;
          }
        }, (error: string) => { console.log(error); });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
    }, (error: string) => { console.log(error); });
  }

  private avgCostAdjmntModel() {
    this.averageCostAdjustmentModel = {
      item: null, itemDesc: null, category: null, categoryDesc: null, loc: null, locName: null,
      locType: null, locTypeDesc: null, unitCost: null, newAvCost: null, currencyCode: null, stockOnHand: null, inTransitQty: null,
      packCompIntran: null, packCompSoh: null, operation: "I", averageCostAdjustmentLocList: []
    };
  }

  newModel(): void {
    this.pForm = this.serviceDocument.dataProfile.profileForm;
    this.detailModel = {
      item: null, itemDesc: null, category: null, categoryDesc: null, loc: null, locName: null,
      locType: null, locTypeDesc: null, unitCost: null, newAvCost: null, currencyCode: null, stockOnHand: null, inTransitQty: null,
      packCompIntran: null, packCompSoh: null, operation: "I"
    };
  }

  changItemInfo(event: any): void {
    if (this.serviceDocument.dataProfile.profileForm.controls["item"].value == null) {
      this.sharedService.errorForm("Please select Item Location");
    } else {
      this.itemDetailsServiceSubscription = this.service.getDataFromAPI(event).subscribe((response: ItemCategeoryDetailsDomainModel) => {
        if (response) {
          var reslt: any = response.categoryDesc;
          this.serviceDocument.dataProfile.profileForm.patchValue({
            categoryDesc: response.categoryDescscription, category: response.category
          });
          this.getDetails();
        }
      });
    }

  }

  getDetails(): void {
    this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.loaderService.display(true);
      this.service.GetItemInfo(this.serviceDocument.dataProfile.profileForm.controls["item"].value).subscribe(rd => {
        this.dataList = rd;
        if (this.dataList && this.dataList.length > 0) {
          this.headDisplayModel = this.dataList;
        }
        this.loaderService.display(false);
      });
    }
  }
  //changeDepartment(): void {
  //  this.serviceDocument.dataProfile.profileForm.controls["class"].setValue(null);
  //  this.classData = this.classList.filter(item => item.dept === +this.serviceDocument.dataProfile.profileForm.controls["dept"].value);
  //}


  update(cell: any, event: any): void {
    this.data[cell.rowIndex][cell.column.colId] = event.target.value;
    let rowNode: RowNode = this.gridApi.getRowNode(cell.rowIndex);
    rowNode.setData(rowNode.data);
  }

  validateData(): boolean {
    let valid: boolean = true;
    this.data.forEach(row => {
      this.mandatoryObligationDetailsFields.forEach(field => {
        if (!row[field]) {
          valid = false;
        }
      });
    });
    return valid;
  }

  ngOnDestroy(): void {
    if (this.itemDetailsServiceSubscription) {
      this.itemDetailsServiceSubscription.unsubscribe();
    }
    if (this.saveServiceSubscription) {
      this.saveServiceSubscription.unsubscribe();
    }
  }
}
