import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { AverageCostAdjustmentService } from "./AverageCostAdjustmentService";
import { AverageCostAdjustmentModel } from "./AverageCostAdjustmentModel";

@Injectable()
export class AverageCostAdjustmentResolver implements Resolve<ServiceDocument<AverageCostAdjustmentModel>> {
    constructor(private service: AverageCostAdjustmentService) { }
    resolve(): Observable<ServiceDocument<AverageCostAdjustmentModel>> {
        return this.service.addEdit();
    }
}



