export class SalesModel {
  fromDate?: Date;
  toDate?: Date;
  company?: number;
  companyName?: string;
  format?: number;
  formatName?: string;
  Store?: number;
  storeName?: string;
  totalRetail?: number;
  totalCost?: number;
  margin?: number;
  lyTotalRetail?: number;
  lyTotalCost?: number;
  LyMargin?: number;
  tabType?: string;
  programPhase?: string;
  prograMessage?: string;
  pError?: string;
}
