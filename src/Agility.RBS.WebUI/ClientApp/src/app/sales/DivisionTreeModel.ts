export class DivisionTreeModel {
  division: number;
  group?: number;
  divName: string;
  children: any[];
  parentId?: number;
  name: string = this.divName;
  type: string;
  id: number;
 
}

