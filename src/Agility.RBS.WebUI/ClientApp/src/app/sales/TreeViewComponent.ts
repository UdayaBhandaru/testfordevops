

import { SelectionModel } from '@angular/cdk/collections';
import { FlatTreeControl } from '@angular/cdk/tree';
import { Component, Injectable, Input } from '@angular/core';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { BehaviorSubject } from 'rxjs';
import { MatIconModule } from '@angular/material/icon';
import { FxContext } from '@agility/frameworkcore';
import { PromotionService } from '../promo/PromotionService';
import { DivisionTreeModel } from './DivisionTreeModel';
import { SalesService } from './SalesService';


/**
 * Node for to-do item
 */
export class DivisionTreeNodeModel {
  public nodeId: number;
  public level: number;
  public expandable: boolean;
  public name: string;
  public id: number;
  public parentId: number;
  public children: DivisionTreeNodeModel[];
  public divName: string;
  public type: string;
  public division: number;
  public group: number;
}

//export class RoleModel {
//  public format: number;
//  public formatName: string;
//  locationId: number;
//  locationName: string;
//  name: string;
//  public parentId: number;
//  public childRoles: RoleModel[];
//}
@Injectable()
export class ChecklistDatabase {
  public dataChange = new BehaviorSubject<DivisionTreeModel[]>([]);
  get data(): DivisionTreeModel[] { return this.dataChange.value; }

  constructor(private salesService: SalesService) {
    this.initialize();
  }
  private initialize() {
    this.salesService.getMerchTreeData().subscribe((roles: DivisionTreeModel[]) => {
      const data = this.buildFileTree(roles, 0);
      this.dataChange.next(data);
    });
  }
  private buildFileTree(obj: object, level: number): DivisionTreeModel[] {
    return Object.keys(obj).reduce<DivisionTreeModel[]>((accumulator, key) => {
      const value = obj[key];
      const node = new DivisionTreeModel();
      node.division = value.division;
      node.divName = value.divName;
      node.name = value.name;
      node.parentId = value.parentId;
      node.id = value.id;
      node.type = value.type;
      node.group = value.group;
      if (value.children && value.children.length > 0) {
        node.children = this.buildFileTree(value.children, level + 1);
      }

      return accumulator.concat(node);
    }, []);
  }

}

/**
 * @title Tree with checkboxes
 */
@Component({
  selector: 'treeViewMerch',
  templateUrl: 'TreeViewComponent.html',
  styleUrls: ['TreeViewComponent.css'],
})
export class TreeViewMerchComponent {
 //// @Input() treeData: any;
  public nodesData: DivisionTreeNodeModel[] = [];
  public checklistSelection = new SelectionModel<DivisionTreeNodeModel>(false);
  // private roleHierarchyForm: FormGroup;
  private nodeId: number = 0;
  private previousLevel: number = 0;
  /** Map from nested node to flattened node. This helps us to keep the same object for selection */
  private nestedNodeMap = new Map<DivisionTreeModel, DivisionTreeNodeModel>();
  public treeControl: FlatTreeControl<DivisionTreeNodeModel>;
  private treeFlattener: MatTreeFlattener<DivisionTreeModel, DivisionTreeNodeModel>;
  public dataSource: MatTreeFlatDataSource<DivisionTreeModel, DivisionTreeNodeModel>;

  /** Map from flat node to nested node. This helps us finding the nested node to be modified */
  flatNodeMap = new Map<DivisionTreeNodeModel, DivisionTreeModel>();

  /** A selected parent node to be inserted */
  selectedParent: DivisionTreeNodeModel | null = null;

  /** The new item's name */
  newItemName = '';

  
  constructor(private database: ChecklistDatabase, public fxContext: FxContext) {
    // this.treeFlattener = new MatTreeFlattener(this.transformer, this.getLevel,
    //   this.isExpandable, this.getChildren);
    // this.treeControl = new FlatTreeControl<TodoItemFlatNode>(this.getLevel, this.isExpandable);
    this.treeFlattener = new MatTreeFlattener(this.transformer, this.getLevel, this.isExpandable, this.getChildren);
    this.treeControl = new FlatTreeControl<DivisionTreeNodeModel>(this.getLevel, this.isExpandable);
    this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
    var dim = this.checklistSelection.selected;
    database.dataChange.subscribe(data => {
      this.dataSource.data = data;
    });
  }

  private getLevel = (node: DivisionTreeNodeModel) => node.level;
  private isExpandable = (node: DivisionTreeNodeModel) => node.expandable;
  private getChildren = (node: DivisionTreeModel): DivisionTreeModel[] => node.children;
  public hasChild = (_: number, nodeData: DivisionTreeNodeModel) => nodeData.expandable;
  public hasNoContent = (_: number, nodeData: DivisionTreeModel) => nodeData.name === "";

 
  private transformer = (node: DivisionTreeModel, level: number) => {
    const existingNode = this.nestedNodeMap.get(node);
    const flatNode = existingNode && existingNode.name === node.name ? existingNode : new DivisionTreeNodeModel();
    flatNode.name = node.name;
    flatNode.level = level;
    flatNode.expandable = !!node.children;
    flatNode.divName = node.divName;
    flatNode.division = node.division;
    flatNode.parentId = node.parentId;
    flatNode.type = node.type;
    flatNode.id = node.id;
    flatNode.group = node.group;
    if (level === 0 || this.previousLevel > level) {
      flatNode.nodeId = ++this.nodeId;
    } else {
      flatNode.nodeId = this.nodeId;
    }
    this.previousLevel = level;
    this.nodesData.push(flatNode);
    this.nestedNodeMap.set(node, flatNode);
    return flatNode;
  }
  /** Whether all the descendants of the node are selected. */
  descendantsAllSelected(node: DivisionTreeNodeModel): boolean {
    const parentId = node.parentId;
    const descendants = this.treeControl.getDescendants(node);
    
    const descAllSelected = descendants.every(child =>

      this.checklistSelection.isSelected(child)
    );
    return descAllSelected;
  }

  /** Whether part of the descendants are selected */
  descendantsPartiallySelected(node: DivisionTreeNodeModel): boolean {
    const descendants = this.treeControl.getDescendants(node);
    const result = descendants.some(child => this.checklistSelection.isSelected(child));
    return result && !this.descendantsAllSelected(node);
  }

  /** Toggle the to-do item selection. Select/deselect all the descendants node */
  todoItemSelectionToggle(node: DivisionTreeNodeModel): void {
    this.checklistSelection.toggle(node);
    const descendants = this.treeControl.getDescendants(node);
    this.checklistSelection.isSelected(node)
      ? this.checklistSelection.select(...descendants)
      : this.checklistSelection.deselect(...descendants);

    // Force update for the parent
    descendants.every(child =>
      this.checklistSelection.isSelected(child)
    );
    // this.checkAllParentsSelection(node);
  }

  /** Toggle a leaf to-do item selection. Check all the parents to see if they changed */
  todoLeafItemSelectionToggle(node: DivisionTreeNodeModel): void {
    this.checklistSelection.toggle(node);
    //this.checkAllParentsSelection(node);
  }

  /** Check root node checked state and change it accordingly */
  checkRootNodeSelection(node: DivisionTreeNodeModel): void {
    const nodeSelected = this.checklistSelection.isSelected(node);
    const descendants = this.treeControl.getDescendants(node);
    const descAllSelected = descendants.every(child =>
      this.checklistSelection.isSelected(child)
    );
    if (nodeSelected && !descAllSelected) {
      this.checklistSelection.deselect(node);
    } else if (!nodeSelected && descAllSelected) {
      this.checklistSelection.select(node);
    }
  }
}
