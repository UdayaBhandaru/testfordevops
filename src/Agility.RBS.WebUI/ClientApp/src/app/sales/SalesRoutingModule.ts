import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { SalesListComponent } from './SalesListComponent';
import { SalesListResolver } from './SalesListResolver';

const SalesRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component:SalesListComponent,
                resolve:
                {
                        references:SalesListResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
      RouterModule.forChild(SalesRoutes)
    ]
})
export class SalesRoutingModule { }
