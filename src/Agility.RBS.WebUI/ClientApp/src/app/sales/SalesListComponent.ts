import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { GridOptions, GridReadyEvent, ColumnApi, GridApi, ColDef } from "ag-grid-community";
import { ServiceDocument } from '@agility/frameworkcore';
import { SalesModel } from './SalesModel';
import { SalesService } from './SalesService';
import { TreeViewMerchComponent } from './TreeViewComponent';
import { FormControl } from '@angular/forms';
import { SharedService } from '../Common/SharedService';
import { LoaderService } from '../Common/LoaderService';


@Component({
  selector: "sales",
  templateUrl: "./SalesListComponent.html",
  styleUrls: ['./SalesListComponent.scss']
})
export class SalesListComponent implements OnInit {
  gridColumnApi: ColumnApi;
  gridApi: GridApi;
  public gridOptions: GridOptions;
  public componentName: any;
  public columns: ColDef[];
  public redirectUrl: string = "Item/New/BasicDetails/";
  public filteredDataList = [];
  public serviceDocument: ServiceDocument<SalesModel> = new ServiceDocument<SalesModel>();
  model: SalesModel;
  @ViewChild(TreeViewMerchComponent)
  treeViewMerchRef: TreeViewMerchComponent;
  public gridData: SalesModel[] = [];
  public selectedTabIndex: number;
  totalRetail = 0; totalCost = 0; margin = 0; lyTotalRetail = 0; lyTotalCost = 0; lyMargin = 0;
  constructor(private salesService: SalesService, private sharedService: SharedService, private loaderService: LoaderService) {
  }
  PageTitle = "Sales Module";
  public ngOnInit(): void {

    this.treeViewMerchRef.checklistSelection.changed.subscribe(m => {
      if (m.added.length > 0) {
        this.search();
      }
      else {
        this.gridData = [];
        this.getGridData(this.selectedTabIndex);
      }
    });
    this.model = { fromDate: null, toDate: null };
    this.serviceDocument = this.salesService.newModel(this.model);
    this.componentName = this;
    this.columns = [
      {
        headerName: "Company", field: "company", tooltipField: "company", suppressFilter: true, headerTooltip: "Company"
      },
      {
        headerName: "Format Name", field: "formatName", tooltipField: "formatName", width: 89, headerTooltip: "Format Name", hide: true
      },
      ,
      {
        headerName: "Store Name", field: "storeName", tooltipField: "storeName", width: 89, headerTooltip: "Store Name", hide: true
      },
      {
        headerName: "Total Retail", field: "totalRetail", tooltipField: "totalRetail", suppressFilter: true, headerTooltip: "Total Retail"
      },
      {
        headerName: "Total Cost", field: "totalCost", tooltipField: "TotalCost", width: 89, headerTooltip: "Total Cost"
      },
      {
        headerName: "Margin", field: "margin", tooltipField: "Margin", width: 89, headerTooltip: "Margin"
      },
      {
        headerName: "Last Year Total Retail", field: "lyTotalRetail", tooltipField: "lyTotalRetail", width: 89, headerTooltip: "Last Year Total Retail"
      },
      {
        headerName: "Last Year Total Cost", field: "lyTotalCost", tooltipField: "LyTotalCost", width: 89, headerTooltip: "Last Year Total Cost"
      },
      {
        headerName: "Last Year Margin", field: "lyMargin", tooltipField: "LyTotalCost", width: 89, headerTooltip: "Last Year Margin"
      }
    ];
    this.gridOptions = {
      onGridReady: (params: GridReadyEvent): void => {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
        this.gridApi.setPinnedBottomRowData([{
          company: "Totals", formatName: "-", storeName: "-",
          totalRetail: this.totalRetail, totalCost: this.totalCost, margin: this.margin,
          lyTotalRetail: this.lyTotalRetail, lyTotalCost: this.lyTotalCost, lyMargin: this.lyMargin
        }]);
        if (this.gridColumnApi) {
          if (this.selectedTabIndex === 0) {
            this.gridColumnApi.setColumnsVisible(["formatName", "storeName"], false);
          } else if (this.selectedTabIndex === 1) {
            this.gridColumnApi.setColumnsVisible(["formatName"], true);
            this.gridColumnApi.setColumnsVisible(["storeName"], true);
          } else if (this.selectedTabIndex === 2) {
            this.gridColumnApi.setColumnsVisible(["formatName"], true);
            this.gridColumnApi.setColumnsVisible(["storeName"], false);
          }
        }
      },
      context: {
        componentParent: this
      },
      getRowStyle: (params) => {
        if (params.node.rowPinned) {
          return { "font-weight": "bold" };
        }
      }
    };


  }

  public search(): void {
    let selIndx = this.selectedTabIndex;
    this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      if (!this.treeViewMerchRef.checklistSelection.selected.length) {
        this.sharedService.errorForm("Please select one location in merchandise hierarchy/Tree");
      }
      else {
        this.serviceDocument.dataProfile.profileForm.controls["merHierarchy"] = new FormControl();
        this.serviceDocument.dataProfile.profileForm.controls["merHierarchy"].setValue(this.treeViewMerchRef.checklistSelection.selected);
        this.loaderService.display(true);
        this.salesService.search().subscribe((e: ServiceDocument<SalesModel>) => {
          this.gridData = e.dataProfile.dataList;
          this.getGridData(selIndx);
          this.loaderService.display(false);
        });
      }
    }
  }
  public reset(): void {
    this.model = { fromDate: null, toDate: null };
    if (this.serviceDocument.dataProfile.profileForm.controls["merHierarchy"]) {
      this.serviceDocument.dataProfile.profileForm.controls["merHierarchy"].setValue("");
    }
    this.treeViewMerchRef.checklistSelection.deselect(...this.treeViewMerchRef.checklistSelection.selected);
    this.treeViewMerchRef.treeControl.collapseAll();
    this.serviceDocument = this.salesService.newModel(this.model);
    this.gridData = [];
    this.selectedTabIndex = 0;
    this.getGridData(0);
  }
  private getGridData(selectedTabIndex) {
    if (!this.gridData) {
      this.gridData = [];
    }
    switch (selectedTabIndex) {
      case 0:
        this.filteredDataList = this.gridData.filter(s => s.tabType == "C")
        break;
      case 1:
        this.filteredDataList = this.gridData.filter(s => s.tabType == "F")


        break;
      case 2:
        this.filteredDataList = this.gridData.filter(s => s.tabType == "L")
        break;
      default:
        this.filteredDataList = this.gridData.filter(s => s.tabType == "C")
        break;
    }

    this.totalRetail = 0; this.totalCost = 0; this.margin = 0; this.lyTotalRetail = 0; this.lyTotalCost = 0; this.lyMargin = 0;
    this.filteredDataList.forEach(c => {
      this.totalRetail = this.totalRetail + c.totalRetail;
      this.totalCost = this.totalCost + c.totalCost;
      this.margin = this.margin + c.margin;
      this.lyTotalRetail = this.lyTotalRetail + c.lyTotalRetail;
      this.lyTotalCost = this.lyTotalCost + c.lyTotalCost;
      this.lyMargin = this.lyMargin + c.lyMargin;

    });

  }

  public tabOnClick(selectedTabIndex) {
    this.selectedTabIndex = selectedTabIndex;
    this.getGridData(selectedTabIndex);
    if (this.gridColumnApi) {
      if (this.selectedTabIndex === 0) {
        this.gridColumnApi.setColumnsVisible(["formatName", "storeName"], false);
      } else if (this.selectedTabIndex === 1) {
        this.gridColumnApi.setColumnsVisible(["formatName"], true);
        this.gridColumnApi.setColumnsVisible(["storeName"], false);
      } else if (this.selectedTabIndex === 2) {
        this.gridColumnApi.setColumnsVisible(["formatName"], true);
        this.gridColumnApi.setColumnsVisible(["storeName"], true);
      }
      else {
        this.gridColumnApi.setColumnsVisible(["formatName", "storeName"], false);
      }
    }
  }
}
