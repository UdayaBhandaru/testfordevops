import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument, WorkflowAction } from "@agility/frameworkcore";
import { HttpHelperService } from "../Common/HttpHelperService";
import { HttpParams } from "@angular/common/http";
import { SalesModel } from './SalesModel';
import { DivisionTreeModel } from './DivisionTreeModel';

@Injectable()
export class SalesService {
  serviceDocument: ServiceDocument<SalesModel> = new ServiceDocument<SalesModel>();
    constructor(private httpHelperService: HttpHelperService) { }

  newModel(model: SalesModel): ServiceDocument<SalesModel> {
        return this.serviceDocument.newModel(model);
    }

  search(): Observable<ServiceDocument<SalesModel>> {
    return this.serviceDocument.search("/api/Sales/Search");
  }

  list(): Observable<ServiceDocument<SalesModel>> {
    return this.serviceDocument.list("/api/Sales/Se1arch");
  }

  getMerchTreeData(): Observable<DivisionTreeModel[]>{
   return this.httpHelperService.get("/api/Sales/GetMerchTreeview");
  }
}
