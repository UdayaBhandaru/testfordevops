import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { SalesModel } from './SalesModel';
import { SalesService } from './SalesService';
import { DivisionTreeModel } from './DivisionTreeModel';

@Injectable()
export class SalesListResolver implements Resolve<DivisionTreeModel[]> {
    constructor(private service: SalesService) { }
  resolve(): Observable<DivisionTreeModel[]> {
      return this.service.getMerchTreeData();
    }
}
