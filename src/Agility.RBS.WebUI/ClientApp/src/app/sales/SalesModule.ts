import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../AgGridSharedModule";
import { TitleSharedModule } from "../TitleSharedModule";
import { RbsSharedModule } from "../RbsSharedModule";
import { SalesRoutingModule } from './SalesRoutingModule';
import { SalesService } from './SalesService';
import { SalesListResolver } from './SalesListResolver';
import { SalesListComponent } from './SalesListComponent';
import { MatTreeModule } from '@angular/material/tree';
import { TreeViewMerchComponent, ChecklistDatabase } from './TreeViewComponent';

@NgModule({
  imports: [
    FrameworkCoreModule,
    SalesRoutingModule,
    AgGridModule,
    AgGridSharedModule,
    TitleSharedModule,
    RbsSharedModule,
    MatTreeModule
  ],
  declarations: [
    SalesListComponent,
    TreeViewMerchComponent
  ],
  providers: [
    SalesService,
    SalesListResolver,
    ChecklistDatabase
  ]
})
export class SalesModule {
}
