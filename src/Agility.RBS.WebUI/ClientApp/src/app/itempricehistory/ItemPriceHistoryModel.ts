export class ItemPriceHistoryModel {
  fromDate?: Date;
  toDate?: Date;
  locType?: string;
  locTypeDesc?: string;
  loc?: number;
  locName?: string;
  tranType?: string;
  tranTypeDesc?: string;
  item?: string;
  itemDesc?: string;
  tranDate?: Date;
  unitCost?: number;
  unitRetail?: number;
  uom?: string;
  localCurr?: string;
}
