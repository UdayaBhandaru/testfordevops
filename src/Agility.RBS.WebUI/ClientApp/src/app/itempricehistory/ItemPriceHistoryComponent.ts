import { Component, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { SharedService } from "../Common/SharedService";
import { GridExportModel } from "../Common/grid/GridExportModel";
import { ItemPriceHistoryService } from "./ItemPriceHistoryService";
import { ItemPriceHistoryModel } from "./ItemPriceHistoryModel";
import { DomainDetailModel } from "../domain/DomainDetailModel";
import { SearchComponent } from "../Common/SearchComponent";
import { GridOptions } from 'ag-grid-community';
import { LocationDomainModel } from '../Common/DomainData/LocationDomainModel';
import { GridDateComponent } from '../Common/grid/GridDateComponent';
import { GridAmountComponent } from '../Common/grid/GridAmountComponent';

@Component({
  selector: "item-price-history",
  templateUrl: "./ItemPriceHistoryComponent.html"
})
export class ItemPriceHistoryComponent implements OnInit {
  public serviceDocument: ServiceDocument<ItemPriceHistoryModel>;
  PageTitle = "Item Price History";
  componentName: any;
  columns: any[];
  model: ItemPriceHistoryModel = {
    fromDate: null, toDate: null, locType: null, loc: null, tranType: null, item: null, itemDesc: null, tranTypeDesc: null
  };
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  transtypeData: DomainDetailModel[];
  locationData: LocationDomainModel[];
  @ViewChild("searchPanel") searchPanel: SearchComponent;
  gridOptions: GridOptions;

  constructor(public service: ItemPriceHistoryService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "Tran. Date", field: "tranDate", tooltipField: "tranDate", cellRendererFramework: GridDateComponent },
      { headerName: "Item", field: "item", tooltipField: "item" },
      { headerName: "Loc. Type", field: "locTypeDesc", tooltipField: "locTypeDesc" },
      { headerName: "Location", field: "locName", tooltipField: "locName" },
      { headerName: "Local Unit Cost", field: "unitCost", tooltipField: "unitCost", cellRendererFramework: GridAmountComponent },
      { headerName: "Local Unit Retail", field: "unitRetail", tooltipField: "unitRetail", cellRendererFramework: GridAmountComponent },
      { headerName: "UOM", field: "uom", tooltipField: "uom" },
      { headerName: "Local Currency", field: "localCurr", tooltipField: "localCurr" }
    ];

    this.locationData = Object.assign([], this.service.serviceDocument.domainData["location"]);
    this.transtypeData = Object.assign([], this.service.serviceDocument.domainData["transType"]);
    this.sharedService.domainData = {
      location: this.locationData
    };
    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
    this.searchPanel.dateColumns = ["fromDate", "toDate"];
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }
}
