import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { ItemPriceHistoryService } from "./ItemPriceHistoryService";
import { ItemPriceHistoryModel } from "./ItemPriceHistoryModel";

@Injectable()
export class ItemPriceHistoryResolver implements Resolve<ServiceDocument<ItemPriceHistoryModel>> {
    constructor(private service: ItemPriceHistoryService) { }
    resolve(): Observable<ServiceDocument<ItemPriceHistoryModel>> {
        return this.service.list();
    }
}
