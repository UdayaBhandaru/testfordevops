import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../AgGridSharedModule";
import { TitleSharedModule } from "../TitleSharedModule";
import { RbsSharedModule } from "../RbsSharedModule";
import { ItemPriceHistoryRoutingModule } from './ItemPriceHistoryRoutingModule';
import { ItemPriceHistoryComponent } from './ItemPriceHistoryComponent';
import { ItemPriceHistoryService } from './ItemPriceHistoryService';
import { ItemPriceHistoryResolver } from './ItemPriceHistoryResolver';

@NgModule({
  imports: [
    FrameworkCoreModule,
    ItemPriceHistoryRoutingModule,
    AgGridModule,
    AgGridSharedModule,
    TitleSharedModule,
    RbsSharedModule
  ],
  declarations: [
    ItemPriceHistoryComponent
  ],
  providers: [
    ItemPriceHistoryService,
    ItemPriceHistoryResolver
  ]
})
export class ItemPriceHistoryModule {
}
