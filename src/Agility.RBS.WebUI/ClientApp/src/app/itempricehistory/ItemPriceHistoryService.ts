import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument, WorkflowAction } from "@agility/frameworkcore";
import { HttpHelperService } from "../Common/HttpHelperService";
import { HttpParams } from "@angular/common/http";
import { SharedService } from "../Common/SharedService";
import { ItemPriceHistoryModel } from './ItemPriceHistoryModel';

@Injectable()
export class ItemPriceHistoryService {
  serviceDocument: ServiceDocument<ItemPriceHistoryModel> = new ServiceDocument<ItemPriceHistoryModel>();
  searchData: any;
  constructor(private httpHelperService: HttpHelperService, private sharedService: SharedService) { }

  newModel(model: ItemPriceHistoryModel): ServiceDocument<ItemPriceHistoryModel> {
    return this.serviceDocument.newModel(model);
  }

  list(): Observable<ServiceDocument<ItemPriceHistoryModel>> {
    return this.serviceDocument.list("/api/ItemPriceHistory/List");
  }

  search(): Observable<ServiceDocument<ItemPriceHistoryModel>> {
    return this.serviceDocument.search("/api/ItemPriceHistory/Search", true, () => this.setBulkItemDates());
  }

  setBulkItemDates(): void {
    var sd: ItemPriceHistoryModel = this.serviceDocument.dataProfile.dataModel;
    if (sd !== null) {
      if (sd.fromDate != null) {
        sd.fromDate = this.sharedService.setOffSet(sd.fromDate);
      }
      if (sd.toDate != null) {
        sd.toDate = this.sharedService.setOffSet(sd.toDate);
      }
    }
  }
}
