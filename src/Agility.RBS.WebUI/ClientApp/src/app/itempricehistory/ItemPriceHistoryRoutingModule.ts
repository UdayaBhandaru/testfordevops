import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ItemPriceHistoryComponent } from "./ItemPriceHistoryComponent";
import { ItemPriceHistoryResolver } from "./ItemPriceHistoryResolver";

const ItemPriceHistoryRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: ItemPriceHistoryComponent,
                resolve:
                {
                        references: ItemPriceHistoryResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
      RouterModule.forChild(ItemPriceHistoryRoutes)
    ]
})
export class ItemPriceHistoryRoutingModule { }
