export class PriceChangeSearchModel {
  pricE_CHANGE_ID: number;
  priceChangeDesc: string;
  itemBarcode?: string;
  itemDesc?: string;
  priceStatus?: string;
  priceStatusDesc?: string;
  startDate: Date;
  endDate: Date;
  reason: string;
  reasonDesc: string;
  operation?: string;
  programPhase?: string;
  programMessage?: string;
  error?: string;
  createdDate?: Date;
  createdBy?: string;
  lastUpdatedBy?: string;
  lastUpdatedDate?: Date;
  startRow?: number;
  endRow?: number;
  totalRows?: number;
  sortModel?: {}[];
  supplierId?: number;
  supplierName?: string;
}
