import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { PriceChangeHeadModel } from "./PriceChangeHeadModel";
import { PriceChangeService } from "./PriceChangeService";

@Injectable()
export class PriceChangeResolver implements Resolve<ServiceDocument<PriceChangeHeadModel>> {
    constructor(private service: PriceChangeService) { }
    resolve(route: ActivatedRouteSnapshot): Observable<ServiceDocument<PriceChangeHeadModel>> {
        return this.service.addEdit(route.params["id"]);
    }
}
