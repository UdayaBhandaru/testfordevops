import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument, WorkflowAction } from "@agility/frameworkcore";
import { PriceChangeHeadModel } from "./PriceChangeHeadModel";
import { HttpParams, HttpHeaders, HttpClient } from "@angular/common/http";
import { SharedService } from "../Common/SharedService";
import { map } from "rxjs/operators";

@Injectable()
export class PriceChangeService {
  serviceDocument: ServiceDocument<PriceChangeHeadModel> = new ServiceDocument<PriceChangeHeadModel>();
  searchData: any;
  constructor(private sharedService: SharedService, private httpClient: HttpClient) { }

  newModel(model: PriceChangeHeadModel): ServiceDocument<PriceChangeHeadModel> {
    return this.serviceDocument.newModel(model);
  }

  list(): Observable<ServiceDocument<PriceChangeHeadModel>> {
    return this.serviceDocument.list("/api/PriceChange/List");
  }

  search(): Observable<ServiceDocument<PriceChangeHeadModel>> {
    return this.serviceDocument.search("/api/PriceChange/Search", true, () => this.setPriceChangeDates());
  }

  save(): Observable<ServiceDocument<PriceChangeHeadModel>> {
    let currentAction: WorkflowAction = this.serviceDocument.dataProfile.actionService.allowedActions[0];
    if (!this.serviceDocument.dataProfile.dataModel.pricE_CHANGE_ID && currentAction) {
      this.serviceDocument.dataProfile.profileForm.controls["currentActionId"].setValue(currentAction.transitionClaim);
      return this.serviceDocument.submit("/api/PriceChange/Submit", true);
    } else {
      return this.serviceDocument.save("/api/PriceChange/Save", true);
    }
  }

  submit(): Observable<ServiceDocument<PriceChangeHeadModel>> {
    return this.serviceDocument.submit("/api/PriceChange/Submit", true);
  }

  addEdit(id: number): Observable<ServiceDocument<PriceChangeHeadModel>> {
    if (+id === 0) {
      return this.serviceDocument.new("/api/PriceChange/New");
    } else {
      return this.serviceDocument.open("/api/PriceChange/Open", new HttpParams().set("id", id.toString()));
    }
  }

  setPriceChangeDates(): void {
    var sd: PriceChangeHeadModel = this.serviceDocument.dataProfile.dataModel;
    if (sd !== null) {
      if (sd.startDate != null) {
        sd.startDate = this.sharedService.setOffSet(sd.startDate);
      }
      if (sd.endDate != null) {
        sd.endDate = this.sharedService.setOffSet(sd.endDate);
      }
    }
  }

  ////
  openPriceDetailExcel(): void {
    this.downloadExcel();
  }

  downloadExcel(): void {
    const type: any = "application/vnd.ms-excel";
    let id: number = this.serviceDocument.dataProfile.dataModel.pricE_CHANGE_ID;
    if (id) {
      let headers1: HttpHeaders = new HttpHeaders();
      headers1.set("Accept", type);
      this.httpClient.get("/api/PriceChange/openPriceDetailExcel",
        { headers: headers1, responseType: "blob" as "json", params: new HttpParams().set("id", id.toString()) })
        .pipe(map((response: any) => {
          if (response instanceof Response) {
            return response.blob();
          }
          return response;
        }))
        .subscribe(res => {
          let url: string = window.URL.createObjectURL(res);
          let a: HTMLAnchorElement = document.createElement("a");
          document.body.appendChild(a);

          a.setAttribute("style", "display: none");
          a.href = url;
          a.download = "PriceChange-" + id + ".xlsm";
          a.click();
          window.URL.revokeObjectURL(url);
          a.remove();
        }, (error: string) => { console.log(error); });
    }
  }
}
