import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { PriceChangeResolver } from "./PriceChangeResolver";
import { PriceChangeListComponent } from "./PriceChangeListComponent";
import { PriceChangeComponent } from "./PriceChangeComponent";
import { PriceChangeListResolver } from "./PriceChangeListResolver";

const PriceChangeRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: PriceChangeListComponent,
                resolve:
                {
                        references: PriceChangeListResolver
                }
            },
            {
                path: "New/:id",
                component: PriceChangeComponent,
                resolve:
                    {
                        references: PriceChangeResolver
                    }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(PriceChangeRoutes)
    ]
})
export class PriceChangeRoutingModule { }
