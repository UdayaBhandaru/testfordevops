import { PriceChangeDetailModel } from "./PriceChangeDetailModel";

export class PriceChangeHeadModel {
  priceChangeDetails: PriceChangeDetailModel[];
  pricE_CHANGE_ID: number;
  reason: string;
  priceChangeDesc: string;
  itemBarcode?: string;
  itemDesc?: string;
  priceStatusDesc?: string;
  reasonDesc: string;
  startDate: Date;
  endDate: Date;
  runDate: Date;
  tableName: string;
  priceStatus?: string;
  operation?: string;
  programPhase?: string;
  programMessage?: string;
  error?: string;
  createdDate?: Date;
  createdBy?: string;

  lastUpdatedBy?: string;
  lastUpdatedDate?: Date;
  workflowInstance?: string;
  wfNxtStateInd?: number;
  lineItemCnt?: number;
  noMarginItemCnt?: number;
  fileCount?: number;
}
