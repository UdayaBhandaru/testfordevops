import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../AgGridSharedModule";
import { TitleSharedModule } from "../TitleSharedModule";
import { PriceChangeService } from "./PriceChangeService";
import { PriceChangeResolver } from "./PriceChangeResolver";
import { PriceChangeRoutingModule } from "./PriceChangeRoutingModule";
import { PriceChangeListComponent } from "./PriceChangeListComponent";
import { PriceChangeComponent } from "./PriceChangeComponent";
import { PriceChangeListResolver } from "./PriceChangeListResolver";
import { RbsSharedModule } from "../RbsSharedModule";
import { PriceChangeListService } from './PriceChangeListService';

@NgModule({
    imports: [
        FrameworkCoreModule,
        PriceChangeRoutingModule,
        AgGridModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        PriceChangeListComponent,
        PriceChangeComponent
    ],
    providers: [
      PriceChangeService,
      PriceChangeListService,
        PriceChangeResolver,
        PriceChangeListResolver
    ]
})
export class PriceChangeModule {
}
