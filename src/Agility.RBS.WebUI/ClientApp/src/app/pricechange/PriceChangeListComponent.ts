import { Component, OnInit, ViewChild, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { GridOptions, ColDef, GridReadyEvent, IServerSideGetRowsParams } from "ag-grid-community";
import { GridEditComponent } from "../Common/grid/GridEditComponent";
import { InfoComponent } from "../Common/InfoComponent";
import { SharedService } from "../Common/SharedService";
import { GridExportModel } from "../Common/grid/GridExportModel";
import { GridDateComponent } from "../Common/grid/GridDateComponent";
import { SearchComponent } from "../Common/SearchComponent";
import { WorkflowStatusDomainModel } from "../Common/DomainData/WorkflowStatusDomainModel";
import { PriceChangeListService } from './PriceChangeListService';
import { PriceChangeSearchModel } from './PriceChangeSearchModel';
import { Subscription } from 'rxjs';

@Component({
  selector: "price-change-list",
  templateUrl: "./PriceChangeListComponent.html"
})

export class PriceChangeListComponent implements OnInit {
  public PageTitle = "Price Management";
  redirectUrl = "PriceChange/New/";
  componentName: any;
  workflowStatusList: WorkflowStatusDomainModel[];
  serviceDocument: ServiceDocument<PriceChangeSearchModel>;
  gridOptions: GridOptions;
  columns: ColDef[];
  model: PriceChangeSearchModel;
  isSearchParamGiven: boolean = false;
  @ViewChild("searchPanel") searchPanel: SearchComponent;
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  defaultDateType: string;
  public sheetName: string = "Price Change List";
  additionalSearchParams: any;
  loadedPage: boolean;
  searchServiceSubscription: Subscription;

  constructor(public service: PriceChangeListService, private sharedService: SharedService, private router: Router
    , private changeRef: ChangeDetectorRef) {
  }

  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { colId: "PRICE_CHANGE_ID", headerName: "Price Change Id", field: "pricE_CHANGE_ID", tooltipField: "pricE_CHANGE_ID", sort: "desc" },
      { headerName: "Description", field: "priceChangeDesc", tooltipField: "priceChangeDesc" },
      { headerName: "Reason", field: "reasonDesc", tooltipField: "reasonDesc" },
      //{ headerName: "Supplier", field: "supplierName", tooltipField: "supplierName" },
      { headerName: "Effective From", field: "startDate", tooltipField: "startDate", cellRendererFramework: GridDateComponent },
      { headerName: "Effective To", field: "endDate", tooltipField: "endDate", cellRendererFramework: GridDateComponent },
      { headerName: "Status", field: "priceStatusDesc", tooltipField: "priceStatusDesc" },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent
      }
    ];

    this.gridOptions = {
      onGridReady: (params: GridReadyEvent) => {
        params.api.setEnterpriseDatasource(this);
      },
      context: {
        componentParent: this
      },
      suppressRowClickSelection: true,
      suppressCellSelection: true,
      rowModelType: "enterprise",
      paginationPageSize: this.service.orderPaging.pageSize,
      enableServerSideSorting: true,
      enableServerSideFilter: true,
      cacheBlockSize: this.service.orderPaging.cacheSize,
    };

    this.workflowStatusList = Object.assign([], this.service.serviceDocument.domainData["workflowStatusList"]);

    this.model = {
      pricE_CHANGE_ID: null, priceChangeDesc: null, itemBarcode: null, itemDesc: null, priceStatus: null
      , createdBy: null, reason: null, reasonDesc: null, startDate: null, endDate: null, createdDate: null
    };

    this.sharedService.domainData = {
      status: this.workflowStatusList
    };

    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }

    this.serviceDocument = this.service.serviceDocument;
    this.searchPanel.dateColumns = ["startDate", "endDate"];
  }

  getRows(params: IServerSideGetRowsParams): void {
    this.additionalSearchParams = { startRow: params.request.startRow, endRow: params.request.endRow, sortModel: params.request.sortModel };
    if (!this.loadedPage) {
      this.loadedPage = true;
      params.successCallback(
        this.serviceDocument.dataProfile.dataList.slice(params.request.startRow, params.request.endRow),
        this.service.serviceDocument.dataProfile.dataModel.totalRows);
    } else {
      this.searchServiceSubscription = this.service.search(this.additionalSearchParams).subscribe((sDoc) => {
        params.successCallback(sDoc.dataProfile.dataList, this.service.serviceDocument.dataProfile.dataModel.totalRows);
      });
    }
  }


  showSearchCriteriaChild(event: any): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
      this.sharedService.previousUrl = "../PriceChange/List";
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl + cell.data.pricE_CHANGE_ID, this.service.serviceDocument.dataProfile.dataList);
  }

  addValidation(): void {
    this.router.navigate(["/" + this.redirectUrl + "0"], { skipLocationChange: true });
    this.sharedService.previousUrl = "../PriceChange/List";
  }

  ngOnDestroy(): void {
    if (this.searchServiceSubscription) {
      this.searchServiceSubscription.unsubscribe();
    }
  }
}
