import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { PriceChangeListService } from "./PriceChangeListService";
import { PriceChangeSearchModel } from './PriceChangeSearchModel';

@Injectable()
export class PriceChangeListResolver implements Resolve<ServiceDocument<PriceChangeSearchModel>> {
    constructor(private service: PriceChangeListService) { }
  resolve(): Observable<ServiceDocument<PriceChangeSearchModel>> {
        return this.service.list();
    }
}
