export class PriceChangeDetailModel {
  department?: string;
  item?: string;
  itemDesc?: string;
  reason?: string;
  ssOld?: string;
  ssNew?: string;
  sssOld?: string;
  sssNew?: string;
  whsOld?: string;
  whsNew?: string;
  csOld?: string;
  csNew?: string;
  marginSsNew: string;
  marginSssNew: string;
  marginWhsNew: string;
  marginCsNew: string;
}
