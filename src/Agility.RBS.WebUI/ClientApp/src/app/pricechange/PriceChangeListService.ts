import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { PriceChangeSearchModel } from "./PriceChangeSearchModel";
import { SharedService } from "../Common/SharedService";

@Injectable()
export class PriceChangeListService {
  serviceDocument: ServiceDocument<PriceChangeSearchModel> = new ServiceDocument<PriceChangeSearchModel>();
  orderPaging: {
    startRow: number,
    pageSize: number,
    cacheSize: number,
  } = {
      startRow: 0,
      pageSize: 10,
      cacheSize: 100,
    };

  constructor(private sharedService: SharedService) { }

  newModel(model: PriceChangeSearchModel): ServiceDocument<PriceChangeSearchModel> {
    return this.serviceDocument.newModel(model);
  }

  search(additionalParams: any): Observable<ServiceDocument<PriceChangeSearchModel>> {
    return this.serviceDocument.search("/api/PriceChangeSearch/Search", true, () => this.setSubClassId(additionalParams));
  }

  list(): Observable<ServiceDocument<PriceChangeSearchModel>> {
    return this.serviceDocument.list("/api/PriceChangeSearch/List");
  }

  setSubClassId(additionalParams: any): void {
    var sd: PriceChangeSearchModel = this.serviceDocument.dataProfile.dataModel;
    if (additionalParams) {
      sd.startRow = additionalParams.startRow ? additionalParams.startRow : this.orderPaging.startRow;
      sd.endRow = additionalParams.endRow ? additionalParams.endRow : this.orderPaging.cacheSize;
      sd.sortModel = additionalParams.sortModel;
    } else {
      sd.startRow = this.orderPaging.startRow;
      sd.endRow = this.orderPaging.cacheSize;
    }

    if (sd !== null) {
      if (sd.startDate != null) {
        sd.startDate = this.sharedService.setOffSet(sd.startDate);
      }
      if (sd.endDate != null) {
        sd.endDate = this.sharedService.setOffSet(sd.endDate);
      }
    }
  }
}
