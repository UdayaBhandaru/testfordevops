import { Component, OnInit, TemplateRef, ViewChild, OnDestroy, ElementRef, ChangeDetectorRef } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ServiceDocument, CommonService, MessageResult, MessageType } from "@agility/frameworkcore";
import { MatDialogRef } from "@angular/material";
import { GridOptions, GridApi } from "ag-grid-community";
import { Subscription } from "rxjs";
import { PriceChangeHeadModel } from "./PriceChangeHeadModel";
import { PriceChangeService } from "./PriceChangeService";
import { SharedService } from "../Common/SharedService";
import { LoaderService } from "../Common/LoaderService";
import { SupplierDomainModel } from "../Common/DomainData/SupplierDomainModel";
import { DocumentsConstants } from "../Common/DocumentsConstants";
import { PriceChangeDetailModel } from "./PriceChangeDetailModel";
import { HttpParams, HttpHeaders, HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { DomainDetailModel } from '../domain/DomainDetailModel';
import { RpmCodeDomainModel } from '../Common/DomainData/RpmCodeDomainModel';
import { GridAmountComponent } from '../Common/grid/GridAmountComponent';

@Component({
  selector: "price-change",
  templateUrl: "./PriceChangeComponent.html"
})

export class PriceChangeComponent implements OnInit {
  _componentName = "Price Change";
  PageTitle: string = "Price Change";
  public serviceDocument: ServiceDocument<PriceChangeHeadModel>;
  localizationData: any;
  model: PriceChangeHeadModel;
  editMode: boolean;
  primaryId: number; // used in CanDeactivate method, should be same name if we use candeactivate
  priceChangeReasonsData: RpmCodeDomainModel[];
  routeServiceSubscription: Subscription;
  documentsConstants: DocumentsConstants = new DocumentsConstants();
  screenName: string;
  showExcelLink: boolean;
  priceStatus: string;
  componentName: any;
  columns: any[];
  public sheetName: string = "PriceChange Detail";
  data: PriceChangeDetailModel[] = [];
  public gridOptions: GridOptions;
  gridApi: GridApi;
  effectiveMinDate: Date = new Date();

  constructor(private route: ActivatedRoute, public router: Router, private changeRef: ChangeDetectorRef, private httpClient: HttpClient
    , public sharedService: SharedService, private service: PriceChangeService, private loaderService: LoaderService) {
  }

  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }

  ngOnInit(): void {
    this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
    this.priceChangeReasonsData = this.service.serviceDocument.domainData["priceChangeReasons"];
    this.screenName = this.documentsConstants.priceObjectName;
    this.componentName = this;
    this.columns = [
      { headerName: "Item", field: "item", tooltipField: "item" },
      { headerName: "Item Desc", field: "itemDesc", tooltipField: "itemDesc" },
      { headerName: "SS Old", field: "ssOld", tooltipField: "ssOld", cellRendererFramework: GridAmountComponent },
      { headerName: "SS New", field: "ssNew", tooltipField: "ssNew", cellRendererFramework: GridAmountComponent },
      { headerName: "SS- Old", field: "sssOld", tooltipField: "sssOld", cellRendererFramework: GridAmountComponent },
      { headerName: "SS- New", field: "sssNew", tooltipField: "sssNew", cellRendererFramework: GridAmountComponent },
      { headerName: "WHS Old", field: "whsOld", tooltipField: "whsOld", cellRendererFramework: GridAmountComponent },
      { headerName: "WHS New", field: "whsNew", tooltipField: "whsNew", cellRendererFramework: GridAmountComponent }
      ////{ headerName: "CS Old", field: "csOld", tooltipField: "csOld" },
      ////{ headerName: "CS New", field: "csNew", tooltipField: "csNew" },
    ];
    this.gridOptions = {
      onGridReady: (params: any) => {
        this.gridApi = params.api;
      },
      context: {
        componentParent: this
      }
    };

    if (this.service.serviceDocument.dataProfile.dataModel) {
      this.bind();
    }
  }

  bind(): void {
    this.routeServiceSubscription = this.route.params.subscribe((resp: any) => {
      this.primaryId = resp["id"];
    });
    if (this.primaryId.toString() !== "0") {
      this.editMode = true;
      this.effectiveMinDate = this.service.serviceDocument.dataProfile.profileForm.controls["startDate"].value;
      this.showExcelLink = true;
      this.PageTitle = `${this.PageTitle} - EDIT - ${this.primaryId}`;
    } else {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign(this.service.serviceDocument.dataProfile.dataModel
        , { operation: "I", priceStatus: "WORKSHEET" }, { workflowInstance: this.service.serviceDocument.dataProfile.dataModel.workflowInstance });
      this.service.serviceDocument.dataProfile.profileForm.patchValue(this.service.serviceDocument.dataProfile.dataModel);
      this.effectiveMinDate = new Date();
      let today: Date = new Date(), tomarrow: Date = new Date();
      tomarrow.setDate(today.getDate() + 1);
      this.service.serviceDocument.dataProfile.profileForm.controls["startDate"].setValue(tomarrow);
      this.PageTitle = `${this.PageTitle} - ADD`;
    }

    if (this.editMode) {
      this.priceStatus = this.service.serviceDocument.dataProfile.dataModel.priceStatus;
      if (this.service.serviceDocument.dataProfile.dataModel.priceChangeDetails) {
        Object.assign(this.data, this.service.serviceDocument.dataProfile.dataModel.priceChangeDetails);
      }
    }
    this.serviceDocument = this.service.serviceDocument;
    this.sharedService.fileCount = this.service.serviceDocument.dataProfile.dataModel.fileCount;
    this.sharedService.checkWfPermitions(this.serviceDocument);
  }

  save(saveOnly: boolean): void {
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.saveSubscription(saveOnly);
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  saveAndContinue(): void {
    this.save(false);
  }

  saveSubscription(saveOnly: boolean): void {
    this.loaderService.display(true);
    this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.primaryId = this.service.serviceDocument.dataProfile.dataModel.pricE_CHANGE_ID;
        this.sharedService.saveForm(`${this.localizationData.priceChangeHead.pricechangeheadsave} - Request ID ${this.primaryId}`).subscribe(() => {
          if (saveOnly) {
            this.router.navigate(["/PriceChange/List"], { skipLocationChange: true });
          } else {
            this.router.navigate(["/Blank"], {
              skipLocationChange: true, queryParams: {
                id: "/PriceChange/New/" + this.primaryId
              }
            });
            this.loaderService.display(false);
            this.openPriceDetailExcel();


          }
        }, (error: string) => { console.log(error); });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
    }, (error: string) => { console.log(error); });
  }

  reset(): void {
    Object.keys(this.serviceDocument.dataProfile.profileForm.controls).forEach(control => {
      this.serviceDocument.dataProfile.profileForm.controls[control].setValue(this.serviceDocument.dataProfile.dataModel[control]);
    });
    this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
    let today: Date = new Date(), tomarrow: Date = new Date();
    tomarrow.setDate(today.getDate() + 1);
    this.service.serviceDocument.dataProfile.profileForm.controls["startDate"].setValue(tomarrow);
  }

  ngOnDestroy(): void {
    this.routeServiceSubscription.unsubscribe();
  }

  submitAction(): void {
    this.loaderService.display(true);
    this.service.submit().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.primaryId = this.service.serviceDocument.dataProfile.dataModel.pricE_CHANGE_ID;
        this.sharedService.saveForm(`${this.localizationData.priceChangeHead.pricechangeheadsave} - Request ID ${this.primaryId}`).subscribe(() => {
          this.router.navigate(["/PriceChange/List"], { skipLocationChange: true });
        }, (error: string) => {
          console.log(error);
          this.loaderService.display(false);
        });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
        this.loaderService.display(false);
      }
    }, (error: string) => {
      console.log(error);
      this.loaderService.display(false);
    });
  }

  openPriceDetailExcel(): void {
    this.service.openPriceDetailExcel();
  }
}
