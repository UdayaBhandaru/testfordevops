import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, ViewChildren, QueryList, ViewChild, HostListener } from "@angular/core";
import { Router, NavigationStart, NavigationEnd, NavigationCancel, NavigationError } from "@angular/router";
import { FxContext } from "@agility/frameworkcore";
import { AccountService } from "./account/AccountService";
import { SharedService } from "./Common/SharedService";
import { LoaderService } from "./Common/LoaderService";
// import { FavouriteService } from "./favourites/FavouriteService";
// import { FavouriteModel } from "./favourites/FavouriteModel";
import { KeepAliveSessionService } from "./Common/KeepAliveSessionService";
import { MatSidenav } from "@angular/material";
import { Location } from "@angular/common";
import { ChangePasswordComponent } from "./account/ChangePasswordComponent";
import { Subscription } from "rxjs";
import { ChangePasswordModel } from "./account/ChangePasswordModel";

@Component({
  selector: "app-root",
  templateUrl: "./AppComponent.html",
  styleUrls: ['./AppComponent.scss']
})

export class AppComponent implements OnInit {
  public displayMenu: boolean;
  public menus: any;
  // public fm: FavouriteModel;
  // public favouritesData: FavouriteModel[];
  public i: number = 0;
  public secretClick: number = 0;
  showLoader: boolean;
  idleState = "Not started.";
  timedOut = false;
  lastPing?: Date = null;
  @ViewChild("sidenav") sidenav: MatSidenav;
  findItemsServiceSubscription: Subscription;
  redirectUrl: string;

  constructor(public fxContext: FxContext, public router: Router, private ref: ChangeDetectorRef, private sharedService: SharedService
    , private loaderService: LoaderService, private keepAliveSessionService: KeepAliveSessionService, private accountService: AccountService
    , private location: Location) {

    router.events.subscribe((event: any) => {
      this.navigationInterceptor(event);
      if (event.url) {
        if (sharedService.routes.find(x => event.url && event.url.indexOf(x) > -1)) {
          this.sidenav.close();
          this.displayMenu = event.url !== "/ItemSuperSearch" ? false : true;
        } else {
          this.sidenav.open();
          this.displayMenu = true;
        }
      }
    });
    this.keepAliveSessionService.setKeepAliveSessionConfiguration();
  }

  navigationInterceptor(event: any): void {
    if (event instanceof NavigationStart) {
      this.showLoader = true;
    }
    if (event instanceof NavigationEnd) {
      this.showLoader = false;
      this.sharedService.previousUrl = this.sharedService.currentUrl;
      this.sharedService.currentUrl = event.url;

    }
    if (event instanceof NavigationCancel) {
      this.showLoader = false;
    }
    if (event instanceof NavigationError) {
      this.showLoader = false;
    }
  }

  sideNavopen(): boolean {
    if (this.fxContext.IsAuthenticated) {
      const width: number = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
      if (width > 768) {
        return true;
      } else {
        return false;
      }
    } else {
      this.sidenav.close();
      return false;
    }
  }

  ngOnInit(): void {
    this.loaderService.status.subscribe((val: boolean) => {
      this.showLoader = val;
    });
    if (this.location.path().indexOf("Forgot") > 0) {
      this.router.navigate([this.location.path()]);
    } else {
      this.router.navigate(["/Account/"]);
    }
  }

  open(cell: any, mode: string): void {
    this.sharedService.viewChangePasswordForm(ChangePasswordComponent, cell, mode, this.redirectUrl, null);
  }

  logout(): void {
    this.accountService.logout().subscribe(() => {
      this.router.navigate(["/Account"]);
    });
  }

  @HostListener("window:beforeunload", ["$event"])
  beforeunloadHandler(event: any): void {
    this.logout();
  }

  ngOnDestroy(): void {
    this.loaderService.status.unsubscribe();
  }
}
