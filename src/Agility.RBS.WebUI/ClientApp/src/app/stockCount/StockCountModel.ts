export class StockCountModel {
  empType: string;
  empTypeDesc: string;
  empId: string;
  cashierInd: string;
  salespersonInd: string;
  phone: string;
  name: string;
  userId: string;
  email: string;
  operation?: string;

  reportGroup?: number;
  departmentId?: number;
  categoryId?: number;
  fineLineId?: number;
  segmentId?: number;
  itemId?: number;
  chainId?: number;
  countryId?: number;
  formatId?: number;
  zoneId?: number;
}
