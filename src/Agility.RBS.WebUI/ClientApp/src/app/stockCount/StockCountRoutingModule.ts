import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { StockCountListResolver, StockCountResolver } from './StockCountResolver';
import { StockCountComponent } from './stockCountComponent';
import { StockCountListComponent } from './StockCountListComponent';

const PromotionRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "", redirectTo: "List", pathMatch: "full"
      },
      {
        path: "List",
        component:StockCountListComponent,
        resolve:
        {
          serviceDocument: StockCountListResolver
        }
      },
      {
        path: "New/:id",
        component:StockCountComponent,
        resolve: {
          serviceDocument: StockCountResolver
        },
      }
    ]
  }
];

@NgModule({
    imports: [
        RouterModule.forChild(PromotionRoutes)
    ]
})
export class StockCountRoutingModule { }

