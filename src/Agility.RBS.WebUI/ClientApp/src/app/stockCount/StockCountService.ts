import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { HttpParams } from "@angular/common/http";
import {  StockCountModel } from './StockCountModel';

@Injectable()
export class StockCountService {
  serviceDocument: ServiceDocument<StockCountModel> = new ServiceDocument<StockCountModel>();

  constructor() { }

  newModel(model: StockCountModel): ServiceDocument<StockCountModel> {
    return this.serviceDocument.newModel(model);
  }
  save(): Observable<ServiceDocument<StockCountModel>> {
    return this.serviceDocument.save("/api/Employee/Save", true);
  }


  addEdit(id: number): Observable<ServiceDocument<StockCountModel>> {
    if (+id === 0) {
      return this.serviceDocument.new("/api/Employee/New");
    } else {
      return this.serviceDocument.open("/api/Employee/Open", new HttpParams().set("id", id.toString()));
    }
  }

  search(): Observable<ServiceDocument<StockCountModel>> {
    return this.serviceDocument.search("/api/Employee/Search", true);
  }

  list(): Observable<ServiceDocument<StockCountModel>> {
    return this.serviceDocument.list("/api/Employee/List");
  }
}
