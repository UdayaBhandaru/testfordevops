import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { StockCountService } from './StockCountService';
import { StockCountModel } from './StockCountModel';



@Injectable()
export class StockCountListResolver implements Resolve<ServiceDocument<StockCountModel>> {
  constructor(private service: StockCountService) { }
  resolve(): Observable<ServiceDocument<StockCountModel>> {
        return this.service.list();
    }
}

@Injectable()
export class StockCountResolver implements Resolve<ServiceDocument<StockCountModel>> {
  constructor(private service: StockCountService) { }
  resolve(route: ActivatedRouteSnapshot): Observable<ServiceDocument<StockCountModel>> {
    return this.service.addEdit(route.params["id"]);
  }
}
