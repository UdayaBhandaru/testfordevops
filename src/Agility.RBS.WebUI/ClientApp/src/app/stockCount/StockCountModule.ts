import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { AgGridSharedModule } from "../AgGridSharedModule";
import { TitleSharedModule } from "../TitleSharedModule";
import { DatePickerModule } from "../Common/DatePickerModule";
import { RbsSharedModule } from '../RbsSharedModule';
import { MatTreeModule } from '@angular/material/tree';
import { StockCountRoutingModule } from './StockCountRoutingModule';
import { StockCountListComponent } from './StockCountListComponent';
import { StockCountComponent } from './stockCountComponent';
import { StockCountService } from './StockCountService';
import { StockCountListResolver, StockCountResolver } from './StockCountResolver';

@NgModule({
  imports: [
    FrameworkCoreModule,
    StockCountRoutingModule,
    AgGridSharedModule,
    TitleSharedModule,
    DatePickerModule,
  ],
  declarations: [
    StockCountComponent,
 StockCountListComponent
  ],
  providers: [
    StockCountService,
   StockCountListResolver,
   StockCountResolver,
  ]
})
export class StockCountModule {
  constructor() {
    LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
  }
}
