import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { GridOptions } from "ag-grid-community";
import { HalfModel } from './HalfModel';
import { GridExportModel } from '../Common/grid/GridExportModel';
import { HalfService } from './HalfService';
import { SharedService } from '../Common/SharedService';
import { GridEditComponent } from '../Common/grid/GridEditComponent';
import { InfoComponent } from '../Common/InfoComponent';
@Component({
  selector: "Half-list",
  templateUrl: "./HalfListComponent.html"
})
export class HalfListComponent implements OnInit {
  PageTitle = "Half";
  redirectUrl = "Half";
  componentName: any;
  serviceDocument: ServiceDocument<HalfModel>;
  public gridOptions: GridOptions;
  columns: any[];
  model: HalfModel;
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;

  constructor(public service: HalfService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "half No", field: "halfNo", tooltipField: "halfNo" },
      { headerName: "half Name", field: "halfName", tooltipField: "halfName" },
      { headerName: "half Date", field: "halfDate", tooltipField: "halfDate" },

      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent
      }
    ];
    
    this.model = { halfNo: null, halfDate: null, halfName: null };

    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }
}
