export class HalfModel { 
  halfNo?: number;
  halfName: string;
  halfDate: string;
  operation?: string;
}
