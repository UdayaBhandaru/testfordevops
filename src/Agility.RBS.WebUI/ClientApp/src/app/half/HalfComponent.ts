import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { HalfModel } from './HalfModel';
import { HalfService } from './HalfService';
import { SharedService } from '../Common/SharedService';

@Component({
  selector: "Half",
  templateUrl: "./HalfComponent.html"
})

export class HalfComponent implements OnInit {
  PageTitle = "Half";
  redirectUrl = "Half";
    serviceDocument: ServiceDocument<HalfModel>;
    editMode: boolean = false;
    editModel: any;
    localizationData: any;  
    model: HalfModel;  


    constructor(private service: HalfService, private sharedService: SharedService, private router: Router
        , private ref: ChangeDetectorRef) {
    }

    ngOnInit(): void {
        this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
        this.bind();
    }

    bind(): void {
        delete this.editModel;
        if (this.sharedService.editObject) {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);         
            Object.assign(this.service.serviceDocument.dataProfile.dataModel, { operation: "U" });
            this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            delete this.sharedService.editObject;
            this.editMode = true;

        } else {            
          this.model = { halfNo: null, halfName: null, halfDate: null, operation: "I"};
           
            this.service.serviceDocument.dataProfile.dataModel = this.model;
            this.service.newModel(this.model);
        }
        this.serviceDocument = this.service.serviceDocument;
    }

    save(saveOnly: boolean): void {
        if (this.serviceDocument.dataProfile.profileForm.valid) {
            this.saveSubscription(saveOnly);
        } else {
            this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
        }
    }

    saveSubscription(saveOnly: boolean): void {
       // this.checkHalf(saveOnly);
      this.saveHalf(saveOnly);
  }

    saveHalf(saveOnly: boolean): void {
        this.service.save().subscribe(() => {
            if (this.service.serviceDocument.result.type === MessageType.success) {
                this.sharedService.saveForm("Half saved successfully.").subscribe(() => {
                    if (saveOnly) {
                        this.router.navigate(["/" + this.redirectUrl + "/List"], { skipLocationChange: true });
                    } else {
                        if (!this.editMode) {
                          this.model = { halfNo: null, halfName: null, halfDate: null, operation: "I" };                            
                            this.service.newModel(this.model);
                        } else {
                            Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
                            this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.service.serviceDocument.dataProfile.dataModel);
                            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
                        }
                    }
                }, (error: string) => { console.log(error); });
            } else {
                this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
            }
        }, (error: string) => { console.log(error); });
    }

    checkHalf(saveOnly: boolean): void {
        if (!this.editMode) {
          this.service.isExistingHalf(this.serviceDocument.dataProfile.profileForm.controls["halfNo"].value).subscribe((response: any) => {
                if (response) {
                    this.sharedService.errorForm("Half already exisit.");
                } else {
                    this.saveHalf(saveOnly);
                }
            });
        } else {
            this.saveHalf(saveOnly);
        }
    }

    reset(): void {
        if (!this.editMode) {
            this.model = {
              halfNo: null, halfName: null, halfDate: null, operation: "I"
            };
           
            this.service.serviceDocument.dataProfile.dataModel = this.model;
            this.service.newModel(this.model);

        } else {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);            
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
        }
        this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
    }
}
