import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { HalfListComponent } from "./HalfListComponent";
import { HalfListResolver, HalfResolver} from "./HalfResolver";
import { HalfComponent } from "./HalfComponent";
const UtilityRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
              component: HalfListComponent,
                resolve:
                {
                    serviceDocument: HalfListResolver
                }
            },
            {
                path: "New",
                component: HalfComponent,
                resolve:
                {
                    serviceDocument: HalfResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(UtilityRoutes)
    ]
})
export class HalfRoutingModule { }
