import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { HalfModel } from "./HalfModel";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from '../Common/HttpHelperService';

@Injectable()
export class HalfService {
    serviceDocument: ServiceDocument<HalfModel> = new ServiceDocument<HalfModel>();
    constructor(private httpHelperService: HttpHelperService) { }

    newModel(model: HalfModel): ServiceDocument<HalfModel> {
        return this.serviceDocument.newModel(model);
    }

    search(): Observable<ServiceDocument<HalfModel>> {
      return this.serviceDocument.search("/api/Half/Search");
    }

    save(): Observable<ServiceDocument<HalfModel>> {
      return this.serviceDocument.save("/api/Half/Save", true);
    }

    list(): Observable<ServiceDocument<HalfModel>> {
      return this.serviceDocument.list("/api/Half/List");
    }

    addEdit(): Observable<ServiceDocument<HalfModel>> {
      return this.serviceDocument.list("/api/Half/New");
    }
  isExistingHalf(halfNo: number): Observable<boolean> {
    return this.httpHelperService.get("/api/Half/IsExistingHalf", new HttpParams().set("halfNo", halfNo.toString()));
    }
}
