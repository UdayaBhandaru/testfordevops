import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { HalfService } from "./HalfService";
import { HalfModel } from "./HalfModel";
@Injectable()
export class HalfListResolver implements Resolve<ServiceDocument<HalfModel>> {
    constructor(private service: HalfService) { }
    resolve(): Observable<ServiceDocument<HalfModel>> {
        return this.service.list();
    }
}

@Injectable()
export class HalfResolver implements Resolve<ServiceDocument<HalfModel>> {
    constructor(private service: HalfService) { }
    resolve(): Observable<ServiceDocument<HalfModel>> {
        return this.service.addEdit();
    }
}



