import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { HalfListComponent } from "./HalfListComponent";
import { HalfService } from "./HalfService";
import { HalfRoutingModule } from "./HalfRoutingModule";
import { HalfComponent } from "./HalfComponent";
import { HalfListResolver, HalfResolver } from "./HalfResolver";
import { RbsSharedModule } from '../RbsSharedModule';
import { AgGridSharedModule } from '../AgGridSharedModule';
import { TitleSharedModule } from '../TitleSharedModule';

@NgModule({
    imports: [
        FrameworkCoreModule,
        HalfRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        HalfListComponent,
        HalfComponent,
    ],
    providers: [
        HalfService,
        HalfListResolver,
        HalfResolver,
    ]
})
export class HalfModule {
    constructor() {
        LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
    }
}
