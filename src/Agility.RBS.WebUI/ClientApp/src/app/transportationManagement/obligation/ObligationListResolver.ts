import { Injectable } from "@angular/core";
import { Resolve } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { ObligationSearchModel } from './ObligationSearchModel';
import { ObligationListService } from './ObligationListService';

@Injectable()
export class ObligationListResolver implements Resolve<ServiceDocument<ObligationSearchModel>> {
  constructor(private service: ObligationListService) { }
  resolve(): Observable<ServiceDocument<ObligationSearchModel>> {
        return this.service.list();
    }
}
