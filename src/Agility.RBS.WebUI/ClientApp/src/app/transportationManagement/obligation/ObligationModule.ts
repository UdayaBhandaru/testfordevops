import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { ObligationRoutingModule } from './ObligationRoutingModule';
import { ObligationListComponent } from './ObligationListComponent';
import { ObligationListService } from './ObligationListService';
import { ObligationListResolver } from './ObligationListResolver';
import { AgGridSharedModule } from '../../AgGridSharedModule';
import { DatePickerModule } from '../../Common/DatePickerModule';
import { TitleSharedModule } from '../../TitleSharedModule';
import { RbsSharedModule } from '../../RbsSharedModule';
import { ObligationHeadComponent } from './ObligationHeadComponent';
import { ObligationHeadResolver } from './ObligationHeadResolver';
import { ObligationHeadService } from './ObligationHeadService';
import { InboxService } from '../../inbox/InboxService';
import { ShipmentService } from '../../SupplyChainManagement/ShipmentService';

@NgModule({
    imports: [
        FrameworkCoreModule,
        ObligationRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        DatePickerModule,
        RbsSharedModule
    ],
    declarations: [
        ObligationListComponent,
        ObligationHeadComponent
    ],
    providers: [
        ObligationListService,
        ObligationListResolver,
        ObligationHeadResolver,
        ObligationHeadService,
        InboxService,
        ShipmentService
    ]
})
export class ObligationModule {
    constructor() {
        LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
    }
}
