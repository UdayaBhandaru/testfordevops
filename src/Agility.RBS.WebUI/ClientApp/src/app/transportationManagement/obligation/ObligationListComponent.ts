import { Component, OnInit, ViewChild, OnDestroy, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { Subscription } from "rxjs";
import { GridOptions, GridReadyEvent, IServerSideDatasource, IServerSideGetRowsParams, ColDef } from "ag-grid-community";
import { ObligationListService } from './ObligationListService';
import { ObligationSearchModel } from './ObligationSearchModel';
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { SearchComponent } from '../../Common/SearchComponent';
import { GridExportModel } from '../../Common/grid/GridExportModel';
import { SharedService } from '../../Common/SharedService';
import { GridEditComponent } from '../../Common/grid/GridEditComponent';
import { InfoComponent } from '../../Common/InfoComponent';
import { GridDateComponent } from '../../Common/grid/GridDateComponent';
import { TransShipmentDomainModel } from '../../Common/DomainData/TransShipmentDomainModel';
import { PartnerDomainModel } from '../../Common/DomainData/PartnerDomainModel';
import { SupplierObigationDomainModel } from '../../Common/DomainData/SupplierObligationDomainModel';

@Component({
  selector: "obligation-list",
  templateUrl: "./ObligationListComponent.html"
})

export class ObligationListComponent implements OnInit, IServerSideDatasource, OnDestroy {
  public sheetName: string = "Obligation List";
  PageTitle = "Obligation List";
  redirectUrl = "Obligation/New/";
  componentName: any;
  additionalSearchParams: any;
  loadedPage: boolean;
  searchServiceSubscription: Subscription;
  partenerDetailsServiceSubscription: Subscription;
  shipmentDetailsServiceSubscription: Subscription;
  supplierData: SupplierObigationDomainModel[];
  obligationData: DomainDetailModel[];
  partnerTypeData: DomainDetailModel[];
  paymentTypeData: DomainDetailModel[];
  statusData: DomainDetailModel[];
  checkAuthData: DomainDetailModel[];
  partnerData: PartnerDomainModel[];
  shipmentData: TransShipmentDomainModel[];
  currencyData: DomainDetailModel[];
  apiUrl: string;
  pForm: any;

  serviceDocument: ServiceDocument<ObligationSearchModel>;
  gridOptions: GridOptions;
  columns: any[];
  model: ObligationSearchModel = {
    obligationKey: null, obligationLevel: null, obligationLevelDesc: null, keyValue1: null, keyValue2: null, keyValue3: null, keyValue4: null, keyValue5: null
    , keyValue6: null, partnerType: null, partnerTypeDesc: null, partnerId: null, partnerName: null, supplier: null, supName: null
    , extInvcNo: null, fromExtInvcDate: null, toExtInvcDate: null, fromPaidDate: null, toPaidDate: null, paymentMethod: null, paymentMethodDesc: null, checkAuthNo: null
    , currencyCode: null, exchangeRate: null, status: null, operation: null, tableName: null
  };

  isSearchParamGiven: boolean = false;
  searchProps: string[] = ["obligationKey", "obligationLevel", "keyValue1", "partnerType", "partnerId", "supplier", "extInvcNo", "paymentMethod", "status"
    , "partnerTypeDesc", "fromExtInvcDate", "toExtInvcDate", "fromPaidDate","toPaidDate"];
  @ViewChild("searchPanel") searchPanel: SearchComponent;
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;

  constructor(public service: ObligationListService, private sharedService: SharedService, private router: Router
    , private changeRef: ChangeDetectorRef) {
  }

  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { colId: "OBLIGATION_KEY", headerName: "Obligation", field: "obligationKey", tooltipField: "obligationKey", width: 100, sort: "desc" },
      { colId: "VESSEL", headerName: "Vessel", field: "keyValue1", tooltipField: "keyValue1", width: 120 },
      { colId: "VOYAGE_FLIGHT", headerName: "Voyage Flight", field: "keyValue2", tooltipField: "keyValue2", width: 140 },
      { colId: "EST_DEPART_DATE", headerName: "Est Depart Date", field: "fromExtInvcDate", tooltipField: "fromExtInvcDate", width: 140, cellRendererFramework: GridDateComponent },
      { colId: "PARTNER_TYPE", headerName: "Partner Type", field: "partnerTypeDesc", tooltipField: "partnerTypeDesc", width: 140 },
      { colId: "PARTNER", headerName: "Partner", field: "partnerId", tooltipField: "partnerId", width: 100 },
      { colId: "PARTNER_DESC", headerName: "Partner Description", field: "partnerName", tooltipField: "partnerName", width: 180 },
      { colId: "SUP_NAME", headerName: "Supplier", field: "supName", tooltipField: "supName", width: 100 },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 100
      }
    ];

    this.gridOptions = {
      onGridReady: (params: GridReadyEvent) => {
        params.api.setEnterpriseDatasource(this);
      },
      context: {
        componentParent: this
      },
      suppressRowClickSelection: true,
      suppressCellSelection: true,
      rowModelType: "enterprise",
      paginationPageSize: this.service.orderPaging.pageSize,
      enableServerSideSorting: true,
      enableServerSideFilter: true,
      cacheBlockSize: this.service.orderPaging.cacheSize,
    };

    this.obligationData = Object.assign([], this.service.serviceDocument.domainData["obligation"].filter(id => id.requiredInd == "Y"));
    this.partnerTypeData = Object.assign([],this.service.serviceDocument.domainData["partnerType"]);
    this.paymentTypeData = Object.assign([],this.service.serviceDocument.domainData["paymentType"]);
    this.statusData = Object.assign([],this.service.serviceDocument.domainData["status"]);
    this.checkAuthData = Object.assign([], this.service.serviceDocument.domainData["checkAuth"]);
    this.currencyData = Object.assign([], this.service.serviceDocument.domainData["currency"]);
    this.partnerData = Object.assign([], this.service.serviceDocument.domainData["partner"]);

    delete this.sharedService.domainData;
    this.sharedService.domainData = {
      supplier: this.supplierData, obligation: this.obligationData, partnerType: this.partnerTypeData,
      paymentType: this.paymentTypeData, status: this.statusData, checkAuth: this.checkAuthData, currency: this.currencyData,partner: this.partnerData
    };

    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }

    this.serviceDocument = this.service.serviceDocument;
    this.apiUrl = "/api/ObligationHead/SuppliersObligationGetName?name=";
    this.searchPanel.dateColumns = ["fromExtInvcDate", "toExtInvcDate", "fromPaidDate","toPaidDate"];

  }

  getRows(params: IServerSideGetRowsParams): void {
    this.additionalSearchParams = { startRow: params.request.startRow, endRow: params.request.endRow, sortModel: params.request.sortModel };
    if (!this.loadedPage) {
      this.loadedPage = true;
      params.successCallback(
        this.serviceDocument.dataProfile.dataList.slice(params.request.startRow, params.request.endRow),
        this.service.serviceDocument.dataProfile.dataModel.totalRows);
    } else {
      this.searchServiceSubscription = this.service.search(this.additionalSearchParams).subscribe((sDoc) => {
        params.successCallback(sDoc.dataProfile.dataList, this.service.serviceDocument.dataProfile.dataModel.totalRows);
      });
    }
  }

  showSearchCriteriaChild(event: any): void {
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.pForm = this.serviceDocument.dataProfile.profileForm;
      if (this.pForm.controls["obligationLevel"].value) {
      this.showSearchCriteria = event === "search" ? true : event;
      if (event === "search") {
        for (let i: number = 0; i < this.searchProps.length; i++) {
          let controlName: string = this.searchProps[i];
          if (this.serviceDocument.dataProfile.profileForm.controls[controlName].value) {
            this.isSearchParamGiven = true;
            break;
          }
        }
        if (this.isSearchParamGiven) {
          this.searchPanel.restrictSearch = false;
          this.searchPanel.search();
          this.isSearchParamGiven = false;
        } else {
          this.sharedService.errorForm("At least one field is required to search");
        }
      } else if (event) {
        this.searchPanel.restrictSearch = true;
      }
    } else {
      this.sharedService.errorForm("Please select the Obligation Level");
    }
    }
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
      this.sharedService.previousUrl = "../Obligation/List";
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl + cell.data.obligationKey
      , this.service.serviceDocument.dataProfile.dataList);
  }
  addValidation(): void {
    this.navigateToAdd();
  }

  private navigateToAdd(): void {
    this.router.navigate(["/" + this.redirectUrl + "0"], { skipLocationChange: true });
    this.sharedService.previousUrl = "../Obligation/List";
  }

  ngOnDestroy(): void {
    if (this.searchServiceSubscription) {
      this.searchServiceSubscription.unsubscribe();
    }
  }


  FillShipmentData(event: any): void {
    this.serviceDocument.dataProfile.profileForm.controls["keyValue2"].setValue(event.options.shipVoyageFltId);
    this.serviceDocument.dataProfile.profileForm.controls["keyValue3"].setValue(event.options.shipEstDeptDate);
    this.serviceDocument.dataProfile.profileForm.controls["keyValue4"].setValue(event.options.shipVesselId);
  }

  getShipmentInfo(event: any): void {
    if (event) {
      let apiShipmentUrl: string = "/api/ObligationHead/ShipmentDetailsGet?shipNo=" + event;
      this.shipmentDetailsServiceSubscription = this.service.getDataFromAPI(apiShipmentUrl).subscribe((response: TransShipmentDomainModel) => {
        if (response != null) {
          this.serviceDocument.dataProfile.profileForm.controls["keyValue2"].setValue(response.shipVoyageFltId);
          this.serviceDocument.dataProfile.profileForm.controls["keyValue3"].setValue(response.shipEstDeptDate);
          this.serviceDocument.dataProfile.profileForm.controls["keyValue4"].setValue(response.shipVesselId);
        } else {
          this.sharedService.errorForm("Shipment No Invalid");
        }

      }, (error: string) => { console.log(error); });
    }
  }

}
