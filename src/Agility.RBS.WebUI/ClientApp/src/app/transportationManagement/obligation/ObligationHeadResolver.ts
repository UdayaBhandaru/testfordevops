import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { ObligationHeadService } from './ObligationHeadService';
import { ObligationHeadModel } from './ObligationHeadModel';

@Injectable()
export class ObligationHeadResolver implements Resolve<ServiceDocument<ObligationHeadModel>> {
  constructor(private service: ObligationHeadService) { }
  resolve(route: ActivatedRouteSnapshot): Observable<ServiceDocument<ObligationHeadModel>> {
        return this.service.addEdit(route.params["id"]);
    }
}
