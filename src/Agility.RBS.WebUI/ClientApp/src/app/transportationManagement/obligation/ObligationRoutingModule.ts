import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ObligationListComponent } from './ObligationListComponent';
import { ObligationListResolver } from './ObligationListResolver';
import { ObligationHeadComponent } from './ObligationHeadComponent';
import { ObligationHeadResolver } from './ObligationHeadResolver';

const OrderRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "", redirectTo: "List", pathMatch: "full"
      },
      {
        path: "List",
        component: ObligationListComponent,
        resolve:
        {
          references: ObligationListResolver
        }
      },
      {
        path: "New/:id",
        component: ObligationHeadComponent,
        resolve:
        {
          references: ObligationHeadResolver
        }
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(OrderRoutes)
  ]
})
export class ObligationRoutingModule { }
