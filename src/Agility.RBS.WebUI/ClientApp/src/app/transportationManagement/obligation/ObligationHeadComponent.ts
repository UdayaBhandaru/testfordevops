import { Component, OnInit, TemplateRef, ViewChild, OnDestroy, ChangeDetectorRef } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ServiceDocument, CommonService, MessageResult, MessageType } from "@agility/frameworkcore";
import { MatDialogRef } from "@angular/material";
import { GridOptions, GridApi, RowNode } from "ag-grid-community";
import { Subscription } from "rxjs";
import { FormControl } from '@angular/forms';
import { DomainDetailModel } from '../../domain/DomainDetailModel';
import { LocationDomainModel } from '../../Common/DomainData/LocationDomainModel';
import { ObligationHeadModel } from './ObligationHeadModel';
import { LoaderService } from '../../Common/LoaderService';
import { SharedService } from '../../Common/SharedService';
import { RbDialogService } from '../../Common/controls/RbDialogService';
import { DocumentsConstants } from '../../Common/DocumentsConstants';
import { GridSelectComponent } from '../../Common/grid/GridSelectComponent';
import { GridNumericComponent } from '../../Common/grid/GridNumericComponent';
import { GridEditComponent } from '../../Common/grid/GridEditComponent';
import { RbButton } from '../../Common/controls/RbControlModels';
import { ObligationDetailModel } from './ObligationDetailModel';
import { ObligationHeadService } from './ObligationHeadService';
import { IndicatorComponent } from '../../Common/grid/IndicatorComponent';
import { UomModel } from '../../unitofmeasure/uom/UomModel';
import { TransShipmentDomainModel } from '../../Common/DomainData/TransShipmentDomainModel';
import { ComponentTraitDomainModel } from '../../Common/DomainData/ComponentTraitDomainModel';
import { PartnerDomainModel } from '../../Common/DomainData/PartnerDomainModel';
import { SupplierObigationDomainModel } from '../../Common/DomainData/SupplierObligationDomainModel';

@Component({
  selector: "ObligationHead-add",
  templateUrl: "./ObligationHeadComponent.html",
  styleUrls: ['./ObligationHeadComponent.scss']
})
export class ObligationHeadComponent implements OnInit, OnDestroy {
  screenName: any;
  _componentName = "Obligation";
  PageTitle: string = "Obligation";
  public serviceDocument: ServiceDocument<ObligationHeadModel>;
  localizationData: any;
  primaryId: number; // used in CanDeactivate method, should be same name if we use candeactivate
  datesArray: any[] = [];
  componentParentName: ObligationHeadComponent;
  editMode: boolean;
  apiUrl: string;
  locationData: LocationDomainModel[]; 
  supplierData: SupplierObigationDomainModel[];
  obligationStatus: string;

  dialogRef: MatDialogRef<any>;
  private messageResult: MessageResult = new MessageResult();
  canNavigate: boolean;
  effectiveMinDate: Date = new Date();
  currentDate: Date = new Date();
  routeServiceSubscription: Subscription;
  supplierDetailsServiceSubscription: Subscription;
  itemDetailsServiceSubscription: Subscription;
  saveServiceSubscription: Subscription;
  partenerDetailsServiceSubscription: Subscription;
  shipmentDetailsServiceSubscription: Subscription;

  gridOptions: GridOptions;
  gridApi: GridApi;
  componentName: any;
  columns: any[];
  data: ObligationDetailModel[] = [];
  dataReset: ObligationDetailModel[] = [];
  orginalValue: string = "";
  addText: string = "+ Add Item";
  detailModel: ObligationDetailModel;
  obligationData: DomainDetailModel[];
  partnerTypeData: DomainDetailModel[];
  partnerData: PartnerDomainModel[];
  paymentTypeData: DomainDetailModel[];
  checkAuthData: DomainDetailModel[];
  componentSourceData: ComponentTraitDomainModel[];
  uomFromData: UomModel[];
  shipmentData: TransShipmentDomainModel[];
  domainRadioData: DomainDetailModel[];
  today: Date;
  partner: boolean;
  supp: boolean;
  partnerType: boolean;
  partnerTypeValue: string = "";
  InAlcInd: string = "";
  temp: {};
  pForm: any;
  mandatoryObligationDetailsFields: string[] = [
    "compId",
    "amt",
    "qty",
    "inAlcInd"
  ];
  deletedRows: any[] = [];

  constructor(private route: ActivatedRoute, private router: Router, public sharedService: SharedService, private service: ObligationHeadService
    , private loaderService: LoaderService, private changeRef: ChangeDetectorRef, private dialogService: RbDialogService) {
  }

  ngOnInit(): void {
    this.screenName = new DocumentsConstants().obligationObjectName;
    this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);

    this.obligationData = Object.assign([], this.service.serviceDocument.domainData["obligation"].filter(id => id.requiredInd == "Y"));
    this.partnerTypeData = Object.assign([], this.service.serviceDocument.domainData["partnerType"]);
    this.paymentTypeData = Object.assign([], this.service.serviceDocument.domainData["paymentType"]);
    this.checkAuthData = Object.assign([], this.service.serviceDocument.domainData["checkAuth"]);
    this.componentSourceData = Object.assign([], this.service.serviceDocument.domainData["component"]);
    this.uomFromData = this.service.serviceDocument.domainData["uom"];
    this.domainRadioData = Object.assign([], this.service.serviceDocument.domainData["domainRadioData"]);
    this.supplierData = Object.assign([], this.service.serviceDocument.domainData["supplier"]);
    this.partnerData = Object.assign([], this.service.serviceDocument.domainData["partner"]);
    this.apiUrl = "/api/ObligationHead/SuppliersObligationGetName?name=";
    this.componentParentName = this;

    this.columns = [
      {
        headerName: "Component", field: "compId", cellRendererFramework: GridSelectComponent, cellClassRules: {
          "invalid-grid-row": function (params: any): any { return !params.value; }
        }, cellRendererParams: { references: this.componentSourceData, keyvaluepair: { key: "compId", value: "compDesc" } },
        tooltipField: "compId", headerTooltip: "compDesc"},
      {
        headerName: "Allocation Basis", field: "allocBasicUom", cellRendererFramework: GridSelectComponent
        , cellRendererParams: { references: this.uomFromData, keyvaluepair: { key: "uom", value: "uomDesc" } },
        tooltipField: "allocBasicUom", headerTooltip: "allocBasicUom"},
      {
        headerName: "In ALC", field: "inAlcInd", cellRendererFramework: IndicatorComponent,
        tooltipField: "inAlcInd", headerTooltip: "inAlcInd", width: 100 },
      {
        headerName: "Rate", field: "rate", cellRendererFramework: GridNumericComponent
        , tooltipField: "rate", headerTooltip: "rate", width: 140 },
      {
        headerName: "Per Count", field: "perCount", cellRendererFramework: GridNumericComponent
        , tooltipField: "perCount", headerTooltip: "perCount", width: 140 
      },
      {
        headerName: "Per Count UOM", field: "perCountUom", cellRendererFramework: GridSelectComponent
        , cellRendererParams: { references: this.uomFromData, keyvaluepair: { key: "uom", value: "uomDesc" } },
        tooltipField: "perCountUom", headerTooltip: "perCountUom" },
      {
        headerName: "Quantity", field: "qty", cellRendererFramework: GridNumericComponent  , cellClassRules: {
          "invalid-grid-row": function (params: any): any { return !params.value; }}
        , tooltipField: "qty", headerTooltip: "qty", width: 140 
      },
      //{
      //  headerName: "Quantity UOM", field: "allocType", cellRendererFramework: GridSelectComponent
      //  , cellRendererParams: { references: this.uomFromData, keyvaluepair: { key: "uom", value: "uomDesc" } },
      //  tooltipField: "perCount", headerTooltip: "perCount"},
      { headerName: "Amount", field: "amt", cellRendererFramework: GridNumericComponent
      , cellClassRules: {
        "invalid-grid-row": function (params: any): any { return !params.value; }
        }, tooltipField: "amt", headerTooltip: "amt", width: 140  },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 130
        , cellRendererParams: { restrictEditIcon: true }
      }
    ];

    this.gridOptions = {
      onGridReady: (params: any) => {
        this.gridApi = params.api;
      },
      
      context: {
        componentParent: this
      },
       enableColResize: true
    };
    if (this.service.serviceDocument.dataProfile.dataModel) {
      this.bind();
    }
  }

  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }

  bind(): void {
    this.routeServiceSubscription = this.route.params.subscribe((resp: any) => {
      this.primaryId = resp["id"];
    });
    this.today = new Date();

    if (this.primaryId.toString() !== "0") {
      this.editMode = true;
      this.PageTitle = `${this.PageTitle} - EDIT - ${this.primaryId}`;
      this.obligationStatus = this.service.serviceDocument.dataProfile.dataModel.keyValue5;
      this.InAlcInd = this.service.serviceDocument.dataProfile.dataModel.obligationDetails[0].inAlcInd;
      if (this.obligationStatus == "P") {
        this.partner = true;
        this.partnerType = true;
        this.supp = false;
      }
      else if (this.obligationStatus == "S") {
        this.supp = true;
        this.partner = false;
        this.partnerType = false; 
      }
      this.service.serviceDocument.dataProfile.dataModel.obligationDetails.forEach((field => field.inAlcInd = "1"));
      this.service.serviceDocument.dataProfile.profileForm.patchValue({ keyValue1: +this.service.serviceDocument.dataProfile.dataModel.keyValue1 });
      if (this.service.serviceDocument.dataProfile.dataModel.obligationDetails) {
        Object.assign(this.data, this.service.serviceDocument.dataProfile.dataModel.obligationDetails);
        Object.assign(this.dataReset, this.service.serviceDocument.dataProfile.dataModel.obligationDetails);
        this.orginalValue = JSON.stringify(this.dataReset);
      }
    } else {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign(this.service.serviceDocument.dataProfile.dataModel
        , { operation: "I", status: "P", keyValue5:"S"}
        , { workflowInstance: this.service.serviceDocument.dataProfile.dataModel.workflowInstance });
      this.service.serviceDocument.dataProfile.profileForm.patchValue(this.service.serviceDocument.dataProfile.dataModel);
      this.supp = true;    
      this.PageTitle = `${this.PageTitle} - ADD`;
    }

    this.serviceDocument = this.service.serviceDocument;
    this.sharedService.checkWfPermitions(this.serviceDocument);
  }

  save(saveOnly: boolean): void {
    if (this.serviceDocument.dataProfile.profileForm.valid)
    {
      if (this.data.length > 0) {
      if (this.validateData()) {
        this.saveSubscription(saveOnly);
      } else {
        this.sharedService.errorForm("Please Fill Obligatin Details Mandatory fields");
      }
    } else {
        this.sharedService.errorForm("Atleast 1 Obligatin Detail is required");
    }
    }
    else
    {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  saveAndContinue(): void {
    this.save(false);
  }

  saveSubscription(saveOnly: boolean): void {   
    this.serviceDocument.dataProfile.profileForm.controls["obligationDetails"] = new FormControl();
    this.serviceDocument.dataProfile.profileForm.controls["obligationDetails"].setValue(this.data);
    this.saveServiceSubscription = this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.primaryId = this.service.serviceDocument.dataProfile.dataModel.obligatioN_KEY;
        this.sharedService.saveForm(`Obligation Saved Successfully ${this.primaryId}`).subscribe(() => {
          if (saveOnly) {
            this.router.navigate(["/Obligation/List"], { skipLocationChange: true });
          } else {
            this.router.navigate(["/Blank"], {
              skipLocationChange: true, queryParams: {
                id: "/Obligation/New/" + this.primaryId
              }
            });
            this.loaderService.display(false);
          }
        }, (error: string) => { console.log(error); });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
    }, (error: string) => { console.log(error); });
    this.loaderService.display(false);
  }

  reset(): void {
    Object.keys(this.serviceDocument.dataProfile.profileForm.controls).forEach(control => {
      this.serviceDocument.dataProfile.profileForm.controls[control].setValue(this.serviceDocument.dataProfile.dataModel[control]);
    });
    this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
  }

  submitAction(): void {
    this.loaderService.display(true);
    this.service.submit().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.primaryId = this.service.serviceDocument.dataProfile.dataModel.obligatioN_KEY;
        this.sharedService.saveForm(`Submitted - Obligation No ${this.primaryId}`).subscribe(() => {
          this.router.navigate(["/Obligation/List"], { skipLocationChange: true });
        }, (error: string) => {
          console.log(error);
          this.loaderService.display(false);
        });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
        this.loaderService.display(false);
      }
    }, (error: string) => {
      console.log(error);
      this.loaderService.display(false);
    });
  }

  sendBackForReview(): void {
    this.loaderService.display(true);
    this.service.sendBackForReview().subscribe(() => {
      this.workflowActionResult("Sent back - Obligation No ", "/Obligation/List");
      this.loaderService.display(false);
    });
  }

  workflowActionResult(message: string, navigateUrl: string): void {
    if (this.service.serviceDocument.result.type === MessageType.success) {
      this.sharedService.saveForm(`${message}  ${this.primaryId}`).subscribe(() => {
        this.router.navigate([navigateUrl], { skipLocationChange: true });
      });
    } else {
      this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
    }
  }

  reject(): void {
    this.loaderService.display(true);
    this.service.reject().subscribe(() => {
      this.workflowActionResult("Rejected - Obligation No ", "/Obligation/List");
      this.loaderService.display(false);
    });
  }

  ngOnDestroy(): void {
    this.routeServiceSubscription.unsubscribe();
    if (this.supplierDetailsServiceSubscription) {
      this.supplierDetailsServiceSubscription.unsubscribe();
    }
   

    if (this.saveServiceSubscription) {
      this.saveServiceSubscription.unsubscribe();
    }

    if (this.itemDetailsServiceSubscription) {
      this.itemDetailsServiceSubscription.unsubscribe();
    }
    this.obligationData.length = 0; 
    this.partnerTypeData.length = 0; 
    this.paymentTypeData.length = 0; 
    this.checkAuthData.length = 0; 
    this.uomFromData.length = 0; 
    this.domainRadioData.length = 0;
  }

  close(): void {
    this.messageResult.message = `<div>All your changes will be lost if you don't save them.</div>
                                    <div>Click on “Save” to Keep the changes.</div>
                                    <div>Click on “Don't save” to proceed further without saving changes.</div>
                                    <div>Click on “Cancel” to stay on the current page.</div>`;
    if ((this.serviceDocument.dataProfile.profileForm && this.serviceDocument.dataProfile.profileForm.dirty)
      || (this.serviceDocument.dataProfile.profileForm.dirty && !this.serviceDocument.dataProfile.profileForm.valid)) {
      this.dialogRef = this.dialogService.openMessageDialog("Warning", this.messageResult
        , [new RbButton("", "Cancel", "alertCancel"), new RbButton("", "Don't Save", "alertdontsave")
          , new RbButton("", "Save", "alertSave")], "37%", "", "iconsdontsave retailsNavIcons", "dontsaveAlert", "dontsaveAlertTitle");
      this.dialogRef.componentInstance.click.subscribe(
        btnName => {
          if (btnName === "Cancel") {
            this.dialogRef.close();
          }
          if (btnName === "Don't Save") {
            this.serviceDocument.dataProfile.profileForm.markAsUntouched();
            this.canNavigate = true;
            this.dialogRef.close();
            this.router.navigate([this.sharedService.previousUrl], { skipLocationChange: true });
          }
          if (btnName === "Save") {
            this.dialogRef.close();
            this.save(true);
            this.router.navigate([this.sharedService.previousUrl], { skipLocationChange: true });
          }
        });
    } else {
      this.canNavigate = true;
      this.router.navigate([this.sharedService.previousUrl], { skipLocationChange: true });
    }
  } 

  select(cell: any, event: any): void {
    this.data[cell.rowIndex][cell.column.colId] = event.target.value;
    let rowNode: RowNode = this.gridApi.getRowNode(cell.rowIndex);
    rowNode.setData(rowNode.data);
  }  

  add($event: MouseEvent): void {
    $event.preventDefault();
    this.pForm = this.serviceDocument.dataProfile.profileForm;
    if (this.pForm.controls["obligationLevel"].value) {      
        this.newModel();
        this.data.unshift(this.detailModel);
        if (this.gridApi) {
          this.gridApi.setRowData(this.data);
        }     
    } else {
      this.sharedService.errorForm("Please select the Obligation Level");
    }
  }

  newModel(): void {
    this.pForm = this.serviceDocument.dataProfile.profileForm;
    this.detailModel = {
      obligatioN_KEY: null, compId: null, compDesc: null, allocType: null, allocTypeDesc: null, allocBasicUom: null,
      amt: null, qty: null, rate: null, perCount: null, perCountUom: null, inAlcInd: null, operation: "I", componentSource: null, uomSource: null
    };
  }

  validateData(): boolean {
    let valid: boolean = true;
    this.data.forEach(row => {
      this.mandatoryObligationDetailsFields.forEach(field => {
        if (!row[field]) {
          valid = false;
        }
      });
    });
    return valid;
  }  

  update(cell: any, event: any): void {
    if (event.target.value) {
      this.data[cell.rowIndex][cell.column.colId] = event.target.value;
      if (cell.column.colId === "item") {
        this.data[cell.rowIndex]["qtyOrdered"] = null;
      } else if (cell.column.colId === "qtyOrdered" || cell.column.colId === "unitCostInit") {
        let total: number = null;
        let cost: number = this.data[cell.rowIndex]["unitCostInit"];
        let qtyOrdered: number = this.data[cell.rowIndex]["qtyOrdered"];
        if (cost && qtyOrdered) {
          total = qtyOrdered * cost;
        }
        this.data[cell.rowIndex]["totalCost"] = total;
      }
      let rowNode: RowNode = this.gridApi.getRowNode(cell.rowIndex);
      rowNode.setData(rowNode.data);
    }
  }

  change(cell: any, event: any): void {
    if (event.target) {
      if (event.target.checked) {
        this.data[cell.rowIndex]["inAlcInd"] = event.target.checked ? "1" : "0";
        this.gridApi.setRowData(this.data);
      }
    }
    this.temp = this.data[cell.rowIndex];
  }

  delete(cell: any): void {
    this.gridApi.updateRowData({ remove: [cell.data] });
    cell.data.operation = "D";
    this.deletedRows.push(cell.data);
    this.data.splice(cell.rowIndex, 1);
  }
  getPartnerData(event: any): void {
    this.partnerData = this.partnerData.filter(id => id.partnerType == event.options.code);
    this.serviceDocument.dataProfile.profileForm.controls["partnerId"].enable();
  }

  getPartnerDetails(event: any): void {
      this.serviceDocument.dataProfile.profileForm.controls["currencyCode"].setValue(event.options.currency);
      this.serviceDocument.dataProfile.profileForm.controls["exchangeRate"].setValue(event.options.exchangeRate);
  }

  FillShipmentData(event: any): void {
    this.serviceDocument.dataProfile.profileForm.controls["keyValue2"].setValue(event.options.shipVoyageFltId);
    this.serviceDocument.dataProfile.profileForm.controls["keyValue3"].setValue(event.options.shipEstDeptDate);
    this.serviceDocument.dataProfile.profileForm.controls["keyValue4"].setValue(event.options.shipVesselId);
  }

  changeSupplier(event: any): void {
    this.serviceDocument.dataProfile.profileForm.controls["currencyCode"].setValue(event.options.currencyCode);
    this.serviceDocument.dataProfile.profileForm.controls["exchangeRate"].setValue(event.options.exchangeRate);
  }

  getDataChage(event: any): void {
    if (event.value == "P") {
      this.partner = true;
      this.partnerType = true; 
      this.supp = false; 
      this.serviceDocument.dataProfile.profileForm.controls["supplier"].reset();
    }
    else if (event.value == "S"){
      this.supp = true;
      this.partner = false;
      this.partnerType = false; 
      this.serviceDocument.dataProfile.profileForm.controls["partnerId"].reset();
      this.serviceDocument.dataProfile.profileForm.controls["partnerType"].reset();
    }
  }

  getShipmentInfo(event: any): void {
    if (event) {
      let apiShipmentUrl: string = "/api/ObligationHead/ShipmentDetailsGet?shipNo=" + event;
      this.shipmentDetailsServiceSubscription = this.service.getDataFromAPI(apiShipmentUrl).subscribe((response: TransShipmentDomainModel) => {
        if (response != null) {
          this.serviceDocument.dataProfile.profileForm.controls["keyValue2"].setValue(response.shipVoyageFltId);
          this.serviceDocument.dataProfile.profileForm.controls["keyValue3"].setValue(response.shipEstDeptDate);
          this.serviceDocument.dataProfile.profileForm.controls["keyValue4"].setValue(response.shipVesselId);
        } else {
          this.sharedService.errorForm("Shipment Number Is Not Valid");
          this.serviceDocument.dataProfile.profileForm.controls["keyValue1"].reset();
        }

      }, (error: string) => { console.log(error); });
    }
  }
}
