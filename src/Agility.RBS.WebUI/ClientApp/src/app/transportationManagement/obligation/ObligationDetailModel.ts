export class ObligationDetailModel {
  obligatioN_KEY?: number;
  compId: string;
  compDesc: string;
  allocType: string;
  allocTypeDesc: string;
  allocBasicUom: string;
  amt?: number;
  qty?: number;
  rate?: number;
  perCount?: number;
  perCountUom: string;
  inAlcInd: string;
  componentSource: string;
  uomSource: string;
  operation: string;
}
