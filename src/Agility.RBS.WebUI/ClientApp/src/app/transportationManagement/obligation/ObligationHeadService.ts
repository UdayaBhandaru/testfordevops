import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument, WorkflowAction } from "@agility/frameworkcore";
import { ObligationHeadModel } from "./ObligationHeadModel";
import { HttpParams } from "@angular/common/http";
import { SharedService } from '../../Common/SharedService';
import { HttpHelperService } from '../../Common/HttpHelperService';
import { debug } from 'util';

@Injectable()
export class ObligationHeadService {
  serviceDocument: ServiceDocument<ObligationHeadModel> = new ServiceDocument<ObligationHeadModel>();

  constructor(private httpHelperService: HttpHelperService
    , private sharedService: SharedService) { }

  newModel(model: ObligationHeadModel): ServiceDocument<ObligationHeadModel> {
    return this.serviceDocument.newModel(model);
  }

  save(): Observable<ServiceDocument<ObligationHeadModel>> {
    if (!this.serviceDocument.dataProfile.dataModel.obligatioN_KEY) {
      let currentAction: WorkflowAction = this.serviceDocument.dataProfile.actionService.allowedActions
        .find((v) => v["transitionType"] === "NT");
      this.serviceDocument.dataProfile.profileForm.controls["currentActionId"].setValue(currentAction.transitionClaim);
      return this.serviceDocument.submit("/api/ObligationHead/Submit", true);
    } else {
      return this.serviceDocument.save("/api/ObligationHead/Save", true);
    }
  }

  submit(): Observable<ServiceDocument<ObligationHeadModel>> {
    return this.serviceDocument.submit("/api/ObligationHead/Submit", true);
  }

  reject(): Observable<ServiceDocument<ObligationHeadModel>> {
    return this.serviceDocument.submit("/api/ObligationHead/Reject", true);
  }

  sendBackForReview(): Observable<ServiceDocument<ObligationHeadModel>> {
    return this.serviceDocument.submit("/api/ObligationHead/SendBackForReview", true);
  }

  addEdit(id: number): Observable<ServiceDocument<ObligationHeadModel>> {
    if (+id === 0) {
      return this.serviceDocument.new("/api/ObligationHead/New");
    } else {
      return this.serviceDocument.open("/api/ObligationHead/Open", new HttpParams().set("id", id.toString()));
    }
  }

  getDataFromAPI(apiUrl: string): Observable<any> {
    let resp: Observable<any> = this.httpHelperService.get(apiUrl);
    return resp;
  }
}
