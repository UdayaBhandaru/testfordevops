import { ObligationDetailModel } from './ObligationDetailModel';

export class ObligationHeadModel {  
  obligatioN_KEY: number;
  obligationLevel: string;
  obligationLevelDesc: string;
  keyValue1: string;
  keyValue2: string;
  keyValue3: string;
  keyValue4: string;
  keyValue5: string;
  keyValue6: string;
  partnerType: string;
  partnerTypeDesc: string;
  partnerId: string;
  partnerName: string;
  supplier10: number;
  supplierName: string;
  extInvcNo: string;
  extInvcDate: Date;
  paidDate: Date;
  paidAmt: number;
  paymentMethod: string;
  paymentMethodDesc: string;
  checkAuthNo: string;
  currencyCode: string;
  exchangeRate: Date;
  status: string;
  commentDesc: string;
  workflowInstance: any;
  operation: string;
  obligationDetails: ObligationDetailModel[];
  estDepartDate: Date;
}
