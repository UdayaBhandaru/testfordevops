﻿import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../Common/HttpHelperService";
import { Http, Response, RequestOptions, URLSearchParams } from "@angular/http";
import { PartnerModel } from "./PartnerModel";

@Injectable()
export class PartnerService {
    serviceDocument: ServiceDocument<PartnerModel> = new ServiceDocument<PartnerModel>();
    constructor(private httpHelperService: HttpHelperService) { }
    newModel(model: PartnerModel): ServiceDocument<PartnerModel> {
        return this.serviceDocument.newModel(model);
    }

    search(): Observable<ServiceDocument<PartnerModel>> {
        return this.serviceDocument.search("/api/Partner/Search");
    }

    save(): Observable<ServiceDocument<PartnerModel>> {
        return this.serviceDocument.save("/api/Partner/Save", true);
    }

    list(): Observable<ServiceDocument<PartnerModel>> {
        return this.serviceDocument.list("/api/Partner/List");
    }

    addEdit(): Observable<ServiceDocument<PartnerModel>> {
        return this.serviceDocument.list("/api/Partner/New");
    }

    isExistingPartner(partnerId: string ): Observable<boolean> {
        return this.httpHelperService.get("/api/Partner/IsExistingPartner", new HttpParams().set("partnerId", partnerId));
    }
}