import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { SharedService } from "../Common/SharedService";
import { DomainDetailModel } from "../domain/DomainDetailModel";
import { PartnerModel } from "./PartnerModel";
import { PartnerService } from "./PartnerService";
import { CommonModel } from "../Common/CommonModel";
import { LanguageDomainModel } from "../Common/DomainData/LanguageDomainModel";
import { CurrencyDomainModel } from "../Common/DomainData/CurrencyDomainModel";
import { PartnerDomainModel } from "../Common/DomainData/PartnerDomainModel";
import { OrgCountryDomainModel } from "../Common/DomainData/OrgCountryDomainModel";
import { TermsDomainModel } from "../Common/DomainData/TermsDomainModel";
import { VatRegionDomainModel } from "../Common/DomainData/VatRegionDomainModel";
import { CountryDomainModel } from "../Common/DomainData/CountryDomainModel";
import { LocationDomainModel } from "../Common/DomainData/LocationDomainModel";
import { Validators, FormGroup } from "@angular/forms";
import { PartnerTypeDomainModel } from '../Common/DomainData/PartnerTypeDomainModel';

@Component({
    selector: "Partner",
    templateUrl: "./PartnerComponent.html"
})

export class PartnerComponent implements OnInit {
    PageTitle = "Partner";
    serviceDocument: ServiceDocument<PartnerModel>;
    partnerModel: PartnerModel;
    localizationData: any;
    domainDtlData: DomainDetailModel[]; defaultStatus: string;
    editMode: boolean = false; editModel: PartnerModel;
    model: PartnerModel;
    brandData: CommonModel[];
    termsData: TermsDomainModel[];
    vatRegionCodesData: VatRegionDomainModel[];
    languageData: LanguageDomainModel[];
    currencyData: CurrencyDomainModel[];
    countryData: CountryDomainModel[];
    locationData: LocationDomainModel[];
    partnerTypeData: DomainDetailModel[];
   indicatorData: DomainDetailModel[];
  invcPayLocData: DomainDetailModel[];
    constructor(private service: PartnerService, private sharedService: SharedService
        , private router: Router, private ref: ChangeDetectorRef) {
    }

    ngAfterViewChecked(): void {
        this.ref.detectChanges();
    }

  ngOnInit(): void {
        this.indicatorData = Object.assign([], this.service.serviceDocument.domainData["indicatorStatus"]);
        this.partnerTypeData = Object.assign([], this.service.serviceDocument.domainData["partnerType"]);
        this.currencyData = Object.assign([], this.service.serviceDocument.domainData["currency"]);
        this.languageData = Object.assign([], this.service.serviceDocument.domainData["language"]);
        this.countryData = Object.assign([], this.service.serviceDocument.domainData["country"]);
        this.termsData = Object.assign([], this.service.serviceDocument.domainData["terms"]);
        this.vatRegionCodesData = Object.assign([], this.service.serviceDocument.domainData["vatRegion"]);
    this.locationData = Object.assign([], this.service.serviceDocument.domainData["location"]);
    this.invcPayLocData = Object.assign([], this.service.serviceDocument.domainData["invcPayLoc"]);

        this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
        this.bind();
    }
    ngAfterViewInit(): void {
        this.addValidators();
    }
    bind(): void {
        delete this.editModel;
        if (this.sharedService.editObject) {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
            this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
            Object.assign(this.service.serviceDocument.dataProfile.dataModel, {
                operation: "U", invcPayLoc: +this.editModel.invcPayLoc, invcReceiveLoc: +this.editModel.invcReceiveLoc
            });
            this.editMode = true;
            delete this.sharedService.editObject;
        } else {
            this.parentModel();
            this.service.serviceDocument.dataProfile.dataModel = this.model;
        }
        this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
        this.serviceDocument = this.service.serviceDocument;
    }
    addValidators(): void {
        let lfg: any = this.serviceDocument.dataProfile.profileForm;
        lfg.controls["contactEmail"].setValidators([Validators.required, this.sharedService.emailValidator("Invalid Email Address")]);
    }

    save(saveOnly: boolean): void {
        if (this.serviceDocument.dataProfile.profileForm.valid) {
            this.saveSubscription(saveOnly);
        } else {
            this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
        }
    }

    saveAndContinue(): void {
        this.save(false);
    }

    saveSubscription(saveOnly: boolean): void {
        this.checkPartner(saveOnly);
    }
    savePartner(saveOnly: boolean): void {
        this.service.save().subscribe(() => {
            if (this.service.serviceDocument.result.type === MessageType.success) {
                this.sharedService.saveForm(this.localizationData.partner.partnersave).subscribe(() => {
                    if (saveOnly) {
                        this.router.navigate(["/Partner/List"], { skipLocationChange: true });
                    } else {
                        if (!this.editMode) {
                            this.parentModel();
                            this.service.newModel(this.model);
                        } else {
                            Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
                            this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.editModel);
                        }
                        this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
                        this.serviceDocument = this.service.serviceDocument;
                    }
                }, (error: string) => { console.log(error); });
            } else {
                this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
            }
        }, (error: string) => { console.log(error); });
    }
     private checkPartner(saveOnly: boolean): void {
        if (!this.editMode) {
            let form: any = this.serviceDocument.dataProfile.profileForm;
            this.service.isExistingPartner(form.controls["partnerId"].value).subscribe
                ((response: boolean) => {
                    if (response) {
                        this.sharedService.errorForm(this.localizationData.partner.partnercheck);
                    } else {
                        this.savePartner(saveOnly);
                    }
                });
        } else {
            this.savePartner(saveOnly);
        }
     }
    reset(): void {
        if (!this.editMode) {
            this.parentModel();
            this.service.newModel(this.model);
        } else {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
        }
        this.serviceDocument = this.service.serviceDocument;
        this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
    }
    private parentModel(): void {

        this.model = {
            partnerType: null,
            partnerTypeDesc: null,
            partnerId: null,
            partnerDesc: null,
            currencyCode: null,
            currencyDesc: null,
            lang: null,
            status: null,
            statusDesc: null,
            contactName: null,
            contactPhone: null,
            contactFax: null,
            contactTelex: null,
            contactEmail: null,
            mfgId: null,
            principleCountryId: null,
            principleCountryName: null,
            lineOfCredit: null,
            outstandCredit: null,
            openCredit: null,
            ytdCredit: null,
            ytdDrawdowns: null,
            taxId: null,
            terms: null,
            termsDesc: null,
            servicePerfReqInd: null,
            invcPayLoc: null,
            invcPayLocName: null,
            invcReceiveLoc: null,
            invcReceiveLocName: null,
            importCountryId: null,
            importCountryName: null,
            primaryIaInd: "Y",
            commentDesc: null,
            tsfEntityId: null,
            vatRegion: null,
            vatRegionName: null,
            operation: "I"
        };
    }
}
