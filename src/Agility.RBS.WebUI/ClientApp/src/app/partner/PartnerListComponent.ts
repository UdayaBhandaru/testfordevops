import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { SharedService } from "../Common/SharedService";
import { DomainDetailModel } from "../domain/DomainDetailModel";
import { GridEditComponent } from "../Common/grid/GridEditComponent";
import { InfoComponent } from "../Common/InfoComponent";
import { GridExportModel } from "../Common/grid/GridExportModel";
import { FormGroup } from "@angular/forms";
import { PartnerModel } from "./PartnerModel";
import { PartnerService } from "./PartnerService";
import { CommonModel } from "../Common/CommonModel";
import { GridOptions } from 'ag-grid-community';

@Component({
  selector: "partner",
  templateUrl: "./PartnerListComponent.html"
})
export class PartnerListComponent implements OnInit {
  public serviceDocument: ServiceDocument<PartnerModel>;
  PageTitle = "Partner";
  sheetName: string = "Partner";
  redirectUrl = "Partner";
  componentName: any;
  columns: any[];
  domainDtlData: DomainDetailModel[];
  partnerTypeData: DomainDetailModel[];
  model: PartnerModel = {
    partnerType: null,
    partnerTypeDesc: null,
    partnerId: null,
    partnerDesc: null,
    currencyCode: null,
    currencyDesc: null,
    lang: null,
    status: null,
    statusDesc: null,
    contactName: null,
    contactPhone: null,
    contactFax: null,
    contactTelex: null,
    contactEmail: null,
    mfgId: null,
    principleCountryId: null,
    principleCountryName: null,
    lineOfCredit: null,
    outstandCredit: null,
    openCredit: null,
    ytdCredit: null,
    ytdDrawdowns: null,
    taxId: null,
    terms: null,
    termsDesc: null,
    servicePerfReqInd: null,
    invcPayLoc: null,
    invcPayLocName: null,
    invcReceiveLoc: null,
    invcReceiveLocName: null,
    importCountryId: null,
    importCountryName: null,
    primaryIaInd: null,
    commentDesc: null,
    tsfEntityId: null,
    vatRegion: null,
    vatRegionName: null,
    operation: "I"
  };
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  gridOptions: GridOptions;

  constructor(public service: PartnerService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "Partner ID", field: "partnerId", tooltipField: "partnerId", width: 40 },
      { headerName: "Partner Type", field: "partnerTypeDesc", tooltipField: "partnerTypeDesc", width: 40 },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 25
      }
    ];
    this.domainDtlData = this.service.serviceDocument.domainData["status"];
    this.partnerTypeData = Object.assign([], this.service.serviceDocument.domainData["partnerType"]);
    delete this.sharedService.domainData;
    this.sharedService.domainData = { status: this.domainDtlData, partnerType: this.partnerTypeData };
    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }
}
