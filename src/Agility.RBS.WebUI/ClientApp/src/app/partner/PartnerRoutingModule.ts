﻿import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { PartnerResolver } from "./PartnerResolver";
import { PartnerListComponent } from "./PartnerListComponent";
import { PartnerComponent } from "./PartnerComponent";
const PartnerRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: PartnerListComponent,
                resolve:
                {
                    serviceDocument: PartnerResolver
                }
            },
            {
                path: "New",
                component: PartnerComponent,
                resolve:
                {
                    serviceDocument: PartnerResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(PartnerRoutes)
    ]
})
export class PartnerRoutingModule { }
