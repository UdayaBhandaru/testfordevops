﻿import { Injectable } from "@angular/core";
import { Resolve } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { PartnerModel } from "./PartnerModel";
import { PartnerService } from "./PartnerService";
@Injectable()
export class PartnerListResolver implements Resolve<ServiceDocument<PartnerModel>> {
    constructor(private service: PartnerService) { }
    resolve(): Observable<ServiceDocument<PartnerModel>> {
        return this.service.list();
    }
}

@Injectable()
export class PartnerResolver implements Resolve<ServiceDocument<PartnerModel>> {
    constructor(private service: PartnerService) { }
    resolve(): Observable<ServiceDocument<PartnerModel>> {
        return this.service.addEdit();
    }
}



