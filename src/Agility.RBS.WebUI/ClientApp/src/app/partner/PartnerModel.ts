﻿export class PartnerModel {
    partnerType: string;
    partnerTypeDesc: string;
    partnerId: string;
    partnerDesc: string;
    currencyCode: string;
    currencyDesc: string;
    lang?: number;
    status: string;
    statusDesc: string;
    contactName: string;
    contactPhone: string;
    contactFax: string;
    contactTelex: string;
    contactEmail: string;
    mfgId: string;
    principleCountryId: string;
    principleCountryName: string;
    lineOfCredit?: number;
    outstandCredit?: number;
    openCredit?: number;
    ytdCredit?: number;
    ytdDrawdowns?: number;
    taxId: string;
    terms: string;
    termsDesc: string;
    servicePerfReqInd: string;
    invcPayLoc: string;
    invcPayLocName: string;
    invcReceiveLoc: string;
    invcReceiveLocName: string;
    importCountryId: string;
    importCountryName: string;
    primaryIaInd: string;
    commentDesc: string;
    tsfEntityId?: number;
    vatRegion: string;
    vatRegionName: string;
    operation?: string;
}