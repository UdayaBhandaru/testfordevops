import { Component, Inject, ChangeDetectorRef } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { CommonService, FormMode, AjaxModel, AjaxResult } from "@agility/frameworkcore";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { TransTotalModel } from './TransTotalModel';
import { DomainDetailModel } from '../domain/DomainDetailModel';
import { SharedService } from '../Common/SharedService';
import { Subscription } from 'rxjs';
import { TransportationService } from './TransportationService';

@Component({
  selector: "totalspopup",
  templateUrl: "./TotalsPopupComponent.html",
  styleUrls: ['./TotalsPopupComponent.scss'],
  providers: [
    TransportationService,
  ]
})

export class TotalsPopupComponent {
  infoGroup: FormGroup;
  objectValue: number;
  objectName: string;
  model: TransTotalModel;
  totalLevels: DomainDetailModel[];
  searchServiceSubscription: Subscription;

  constructor(private service: TransportationService, private commonService: CommonService, public sharedService: SharedService, private changeRef: ChangeDetectorRef
    , public dialog: MatDialog, @Inject(MAT_DIALOG_DATA)
  public data: {
    objectName: string, objectValue: number
  }) {
    this.model = {
      transportationId: null, totalsLevel: null, totalUnits: null, unitsUom: null, totalCartons: null, cartonsUom: null, totalGrossWeight: null
      , grossWeightUom: null, totalNetWeight: null, netWeightUom: null, totalCube: null, cubeUom: null, totalValue: null, currencyCode: null
    };
    this.totalLevels = this.sharedService.editObject;
    this.infoGroup = this.commonService.getFormGroup(this.model, FormMode.View);
  }

  ngOnInit(): void {
    this.objectValue = this.data.objectValue;
    this.objectName = this.data.objectName;
  }

  close(): void {
    this.dialog.closeAll();
  }

  changeTotalsLevel(item: any): void {
    var totalsApiUrl = "/api/Transportation/TransTotalsGet?transportationId=" + this.objectValue + "&totalsLevel=" + item.options.code;
    this.searchServiceSubscription = this.service.getDataFromAPI(totalsApiUrl)
      .subscribe((response: AjaxModel<TransTotalModel[]>) => {
        if (response.result === AjaxResult.success) {
          var totalModel: TransTotalModel = response.model[0];
          this.infoGroup.patchValue({
            totalUnits: totalModel.totalUnits, unitsUom: totalModel.unitsUom, totalCartons: totalModel.totalCartons
            , cartonsUom: totalModel.cartonsUom, totalGrossWeight: totalModel.totalGrossWeight, grossWeightUom: totalModel.grossWeightUom
            , totalNetWeight: totalModel.totalNetWeight, netWeightUom: totalModel.netWeightUom, totalCube: totalModel.totalCube
            , cubeUom: totalModel.cubeUom, totalValue: totalModel.totalValue, currencyCode: totalModel.currencyCode
          });
        } else {
          this.sharedService.errorForm(response.message);
        }
        this.changeRef.detectChanges();
      }, (error: string) => { console.log(error); });
  }
}
