import { Component, OnInit, OnDestroy, ViewChild } from "@angular/core";
import { ServiceDocument } from "@agility/frameworkcore";
import { SharedService } from "../Common/SharedService";
import { Router } from "@angular/router";
import { GridExportModel } from "../Common/grid/GridExportModel";
import { FormGroup } from "@angular/forms";
import { GridEditComponent } from "../Common/grid/GridEditComponent";
import { InfoComponent } from "../Common/InfoComponent";
import { SearchComponent } from "../Common/SearchComponent";
import { Subscription } from "rxjs";
import { GridOptions, IServerSideDatasource, IServerSideGetRowsParams, GridReadyEvent } from 'ag-grid-community';
import { TransportationSearchModel } from './TransportationSearchModel';
import { TransportationListService } from './TransportationListService';
import { GridDateComponent } from '../Common/grid/GridDateComponent';

@Component({
  selector: "transportation-list",
  templateUrl: "./TransportationListComponent.html"
})
export class TransportationListComponent implements OnInit, IServerSideDatasource, OnDestroy {
  public serviceDocument: ServiceDocument<TransportationSearchModel>;
  PageTitle = "Transportation";
  redirectUrl = "Transportation/New/";
  componentName: any;
  columns: any[];
  additionalSearchParams: any;
  loadedPage: boolean;
  model: TransportationSearchModel = {
    transportationId: null, status: null, estimatedDepartDate: null, item: null, itemDesc: null, vesselId: null, voyageFltId: null
    , orderNo: null, shipmentNo: null, statusDesc: null
  };

  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  public gridOptions: GridOptions;
  @ViewChild("searchPanel") searchPanel: SearchComponent;
  searchServiceSubscription: Subscription;

  constructor(public service: TransportationListService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "Transportation ID", field: "transportationId", tooltipField: "transportationId", colId: "TRANSPORTATION_ID", sort: "desc" },
      { headerName: "Vessel ID", field: "vesselId", tooltipField: "vesselId", colId: "VESSEL_ID" },
      { headerName: "Voyage/Flight ID", field: "voyageFltId", tooltipField: "voyageFltId", colId: "VOYAGE_FLT_ID" },
      { headerName: "Est. Depart. Date", field: "estimatedDepartDate", tooltipField: "estimatedDepartDate", colId: "ESTIMATED_DEPART_DATE", cellRendererFramework: GridDateComponent },
      { headerName: "Order No.", field: "orderNo", tooltipField: "orderNo", colId: "ORDER_NO" },
      { headerName: "Item", field: "item", tooltipField: "item", colId: "ITEM" },
      { headerName: "Status", field: "statusDesc", tooltipField: "statusDesc", colId: "STATUS_DESC" },
      {
        headerName: "ACTIONS", field: "Actions", cellRendererFramework: GridEditComponent
      }
    ];

    this.gridOptions = {
      onGridReady: (params: GridReadyEvent) => {
        params.api.setEnterpriseDatasource(this);
      },
      context: {
        componentParent: this
      },
      suppressRowClickSelection: true,
      suppressCellSelection: true,
      rowModelType: "enterprise",
      paginationPageSize: this.service.transPaging.pageSize,
      enableServerSideSorting: true,
      enableServerSideFilter: true,
      cacheBlockSize: this.service.transPaging.cacheSize,
    };


    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  getRows(params: IServerSideGetRowsParams): void {
    this.additionalSearchParams = { startRow: params.request.startRow, endRow: params.request.endRow, sortModel: params.request.sortModel };
    if (!this.loadedPage) {
      this.loadedPage = true;
      params.successCallback(
        this.serviceDocument.dataProfile.dataList.slice(params.request.startRow, params.request.endRow),
        this.service.serviceDocument.dataProfile.dataModel.totalRows);
    } else {
      this.searchServiceSubscription = this.service.search(this.additionalSearchParams).subscribe((sDoc) => {
        params.successCallback(sDoc.dataProfile.dataList, this.service.serviceDocument.dataProfile.dataModel.totalRows);
      });
    }
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl + cell.data.transportationId, this.service.serviceDocument.dataProfile.dataList);
  }

  ngOnDestroy(): void {
    if (this.searchServiceSubscription) {
      this.searchServiceSubscription.unsubscribe();
    }
  }

  addValidation(): void {
    this.router.navigate(["/" + this.redirectUrl + "0"], { skipLocationChange: true });
    this.sharedService.previousUrl = "../Transportation/List";
  }
}
