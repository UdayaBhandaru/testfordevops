import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { TransportationListComponent } from './TransportationListComponent';
import { TransportationListResolver } from './TransportationListResolver';
import { TransportationComponent } from './TransportationComponent';
import { TransportationResolver } from './TransportationResolver';

const TransportationRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: TransportationListComponent,
                resolve:
                {
                    references: TransportationListResolver
                }
            },
            {
                path: "New/:id",
                component: TransportationComponent,
                resolve:
                {
                    references: TransportationResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(TransportationRoutes)
    ]
})
export class TransportationRoutingModule { }
