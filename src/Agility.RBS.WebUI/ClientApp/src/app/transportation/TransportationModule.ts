import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../AgGridSharedModule";
import { TitleSharedModule } from "../TitleSharedModule";
import { RbsSharedModule } from "../RbsSharedModule";
import { TransportationRoutingModule } from './TransportationRoutingModule';
import { TransportationComponent } from './TransportationComponent';
import { TransportationListComponent } from './TransportationListComponent';
import { TransportationService } from './TransportationService';
import { TransportationListService } from './TransportationListService';
import { TransportationResolver } from './TransportationResolver';
import { TransportationListResolver } from './TransportationListResolver';

@NgModule({
    imports: [
        FrameworkCoreModule,
        TransportationRoutingModule,
        AgGridModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        TransportationComponent,
        TransportationListComponent
    ],
    providers: [
        TransportationService,
        TransportationListService,
        TransportationListResolver,
        TransportationResolver
    ]
})
export class TransportationModule {
}
