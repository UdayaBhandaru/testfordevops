import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { TransportationModel } from './TransportationModel';
import { TransportationService } from './TransportationService';

@Injectable()
export class TransportationResolver implements Resolve<ServiceDocument<TransportationModel>> {
  constructor(private service: TransportationService) { }
  resolve(route: ActivatedRouteSnapshot): Observable<ServiceDocument<TransportationModel>> {
        return this.service.addEdit(route.params["id"]);
    }
}
