import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument, WorkflowAction } from "@agility/frameworkcore";
import { TransportationSearchModel } from './TransportationSearchModel';

@Injectable()
export class TransportationListService {
  serviceDocument: ServiceDocument<TransportationSearchModel> = new ServiceDocument<TransportationSearchModel>();
  transPaging: {
    startRow: number,
    pageSize: number,
    cacheSize: number,
  } = {
      startRow: 0,
      pageSize: 10,
      cacheSize: 100,
    };

  constructor() { }

  newModel(model: TransportationSearchModel): ServiceDocument<TransportationSearchModel> {
    return this.serviceDocument.newModel(model);
  }

  list(): Observable<ServiceDocument<TransportationSearchModel>> {
    return this.serviceDocument.list("/api/TransportationSearch/List");
  }

  search(additionalParams: any): Observable<ServiceDocument<TransportationSearchModel>> {
    return this.serviceDocument.search("/api/TransportationSearch/Search", true, () => this.setSubClassId(additionalParams));
  }

  setSubClassId(additionalParams: any): void {
    var sd: TransportationSearchModel = this.serviceDocument.dataProfile.dataModel;
    if (additionalParams) {
      sd.startRow = additionalParams.startRow ? additionalParams.startRow : this.transPaging.startRow;
      sd.endRow = additionalParams.endRow ? additionalParams.endRow : this.transPaging.cacheSize;
      sd.sortModel = additionalParams.sortModel;
    } else {
      sd.startRow = this.transPaging.startRow;
      sd.endRow = this.transPaging.cacheSize;
    }
  }
}
