export class TransportationSearchModel {
  transportationId: number;
  vesselId: string;
  voyageFltId: string;
  estimatedDepartDate: Date;
  orderNo: number;
  item: string;
  itemDesc: string;
  shipmentNo: string;
  status: string;
  statusDesc: string;
  error?: string;
  operation?: string;
  startRow?: number;
  endRow?: number;
  totalRows?: number;
  sortModel?: {}[];
}
