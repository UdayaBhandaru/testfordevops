import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument, WorkflowAction } from "@agility/frameworkcore";
import { SharedService } from "../Common/SharedService";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../Common/HttpHelperService";
import { TransportationModel } from './TransportationModel';

@Injectable()
export class TransportationService {
  serviceDocument: ServiceDocument<TransportationModel> = new ServiceDocument<TransportationModel>();
  constructor(private httpHelperService: HttpHelperService, private sharedService: SharedService) { }

  newModel(model: TransportationModel): ServiceDocument<TransportationModel> {
    return this.serviceDocument.newModel(model);
  }

  list(): Observable<ServiceDocument<TransportationModel>> {
    return this.serviceDocument.list("/api/Transportation/List");
  }

  search(): Observable<ServiceDocument<TransportationModel>> {
    return this.serviceDocument.search("/api/Transportation/Search");
  }

  save(): Observable<ServiceDocument<TransportationModel>> {
    return this.serviceDocument.save("/api/Transportation/Save", true);
  }

  addEdit(id: number): Observable<ServiceDocument<TransportationModel>> {
    if (+id === 0) {
      return this.serviceDocument.new("/api/Transportation/New");
    } else {
      return this.serviceDocument.open("/api/Transportation/Open", new HttpParams().set("id", id.toString()));
    }
  }

  getDataFromAPI(apiUrl: string): Observable<any> {
    var resp: any = this.httpHelperService.get(apiUrl);
    return resp;
  }

  setDate(): void {
    //    let sd: TransportationHeadModel = this.serviceDocument.dataProfile.dataModel;
    //    if (sd !== null) {
    //        if (sd.lastRebuildDate != null) {
    //            sd.lastRebuildDate = this.sharedService.setOffSet(sd.lastRebuildDate);
    //        }
    //    }
  }
}
