export class TransTotalModel {
  transportationId?: number;
  shipmentNo?: string;
  totalsLevel: string;
  totalUnits?: number;
  unitsUom: string;
  totalCartons?: number;
  cartonsUom: string;
  totalGrossWeight?: number;
  grossWeightUom: string;
  totalNetWeight?: number;
  netWeightUom: string;
  totalCube?: number;
  cubeUom: string;
  totalValue?: number;
  currencyCode: string;
}
