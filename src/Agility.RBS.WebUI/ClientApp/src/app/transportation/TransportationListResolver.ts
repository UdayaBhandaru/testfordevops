import { Injectable } from "@angular/core";
import { Resolve } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { TransportationSearchModel } from './TransportationSearchModel';
import { TransportationListService } from './TransportationListService';

@Injectable()
export class TransportationListResolver implements Resolve<ServiceDocument<TransportationSearchModel>> {
  constructor(private service: TransportationListService) { }
  resolve(): Observable<ServiceDocument<TransportationSearchModel>> {
        return this.service.list();
    }
}
