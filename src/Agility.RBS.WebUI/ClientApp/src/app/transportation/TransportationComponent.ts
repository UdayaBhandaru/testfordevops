import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ServiceDocument, MessageType, FormMode, CommonService } from "@agility/frameworkcore";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { SharedService } from "../Common/SharedService";
import { Subscription } from "rxjs";
import { DocumentsConstants } from "../Common/DocumentsConstants";
import { LoaderService } from "../Common/LoaderService";
import { DomainDetailModel } from "../domain/DomainDetailModel";
import { TransportationService } from './TransportationService';
import { TransportationModel } from './TransportationModel';
import { NbDomainDetailModel } from '../Common/DomainData/NbDomainDetailModel';
import { PartnerDomainModel } from '../Common/DomainData/PartnerDomainModel';
import { CountryDomainModel } from '../Common/DomainData/CountryDomainModel';
import { OutLocationDomainModel } from '../Common/DomainData/OutLocationDomainModel';
import { UomModel } from '../unitofmeasure/uom/UomModel';
import { FreightTypeModel } from '../Freight/FreightType/FreightTypeModel';
import { TotalsPopupComponent } from './TotalsPopupComponent';

@Component({
  selector: "transportation",
  templateUrl: "./TransportationComponent.html"
})
export class TransportationComponent implements OnInit {
  PageTitle: string = "Transportation";
  documentsConstants: DocumentsConstants = new DocumentsConstants();
  public serviceDocument: ServiceDocument<TransportationModel>;
  localizationData: any;
  model: TransportationModel;
  editMode: boolean;
  statusData: DomainDetailModel[];
  indicatorData: DomainDetailModel[];
  transModeData: DomainDetailModel[];
  vesselScacData: NbDomainDetailModel[];
  consolidatorData: PartnerDomainModel[];
  uomData: UomModel[];
  countryData: CountryDomainModel[];
  outlocData: OutLocationDomainModel[];
  packingData: DomainDetailModel[];
  uomQtyData: UomModel[];
  uomWtData: UomModel[];
  freighttypeData: FreightTypeModel[];
  totalLevelData: DomainDetailModel[];
  contentData: any;
  screenName: string;
  routeServiceSubscription: Subscription;
  primaryId: number;

  constructor(private service: TransportationService, public sharedService: SharedService, private changeRef: ChangeDetectorRef
    , private router: Router, private route: ActivatedRoute, private loaderService: LoaderService, private commonService: CommonService) {
  }

  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }

  ngOnInit(): void {
    this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
    this.statusData = this.service.serviceDocument.domainData["status"];
    this.indicatorData = this.service.serviceDocument.domainData["indicator"];
    this.transModeData = this.service.serviceDocument.domainData["transmode"];
    this.vesselScacData = this.service.serviceDocument.domainData["vesselscac"];
    this.consolidatorData = this.service.serviceDocument.domainData["consolidator"];
    this.uomData = this.service.serviceDocument.domainData["uom"];
    this.countryData = this.service.serviceDocument.domainData["country"];
    this.outlocData = this.service.serviceDocument.domainData["outloc"];
    this.packingData = this.service.serviceDocument.domainData["packing"];
    this.freighttypeData = this.service.serviceDocument.domainData["freighttype"];
    this.totalLevelData = this.service.serviceDocument.domainData["totallevel"];
    this.uomQtyData = this.uomData.filter(id => id.uomClass === "QTY");
    this.uomWtData = this.uomData.filter(id => id.uomClass === "MASS");
    this.screenName = this.documentsConstants.transObjectName;
    if (this.service.serviceDocument.dataProfile.dataModel) {
      this.bind();
    }
  }

  bind(): void {
    this.routeServiceSubscription = this.route.params.subscribe((resp: any) => {
      this.primaryId = resp["id"];
    });

    if (this.primaryId.toString() !== "0") {
      this.editMode = true;
      this.PageTitle = `${this.PageTitle} - EDIT - ${this.primaryId}`;
    }

    this.serviceDocument = this.service.serviceDocument;
  }

  save(saveOnly: boolean): void {
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.saveSubscription(saveOnly);
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  saveAndContinue(): void {
    this.save(false);
  }

  saveSubscription(saveOnly: boolean): void {
    this.loaderService.display(true);

    this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.primaryId = this.service.serviceDocument.dataProfile.dataModel.transportationId;
        this.sharedService.saveForm(`Transportation saved successfully - Request ID ${this.primaryId}`).subscribe(() => {
          if (saveOnly) {
            this.router.navigate(["/Transportation/List"], { skipLocationChange: true });
          } else {
            this.router.navigate(["/Blank"], {
              skipLocationChange: true, queryParams: {
                id: "/Transportation/New/" + this.primaryId
              }
            });
            this.loaderService.display(false);
          }
        }, (error: string) => { console.log(error); });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
    }, (error: string) => { console.log(error); });
    this.loaderService.display(false);
  }

  reset(): void {
    Object.keys(this.serviceDocument.dataProfile.profileForm.controls).forEach(control => {
      this.serviceDocument.dataProfile.profileForm.controls[control].setValue(this.serviceDocument.dataProfile.dataModel[control]);
    });
    this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
  }

  ngOnDestroy(): void {
    //if (this.itemDetailsServiceSubscription) {
    //  this.itemDetailsServiceSubscription.unsubscribe();
    //}

    this.statusData.length = 0;
    this.indicatorData.length = 0;
    this.transModeData.length = 0;
    // this.vesselScacData.length = 0;
    this.consolidatorData.length = 0;
    this.uomData.length = 0;
    this.countryData.length = 0;
    this.outlocData.length = 0;
    this.packingData.length = 0;
    this.totalLevelData.length = 0;
  }

  totals(): void {
    this.sharedService.openPopupWithDataList(TotalsPopupComponent, this.totalLevelData, "Transportation Totals", "Transportation"
      , this.primaryId);
  }
}
