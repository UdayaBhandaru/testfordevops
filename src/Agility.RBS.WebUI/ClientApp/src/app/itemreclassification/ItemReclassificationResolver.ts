import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { ItemReclassificationModel } from './ItemReclassificationModel';
import { ItemReclassificationService } from './ItemReclassificationService';

@Injectable()
export class ItemReclassificationResolver implements Resolve<ServiceDocument<ItemReclassificationModel>> {
  constructor(private service: ItemReclassificationService) { }
  resolve(): Observable<ServiceDocument<ItemReclassificationModel>> {
        return this.service.list();
    }
}
