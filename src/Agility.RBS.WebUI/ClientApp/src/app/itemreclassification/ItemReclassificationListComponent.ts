import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { SharedService } from "../Common/SharedService";
import { GridEditComponent } from "../Common/grid/GridEditComponent";
import { GridExportModel } from "../Common/grid/GridExportModel";
import { InfoComponent } from "../Common/InfoComponent";
import { GridOptions } from 'ag-grid-community';
import { ItemReclassificationService } from './ItemReclassificationService';
import { ItemReclassificationModel } from './ItemReclassificationModel';
import { DepartmentDomainModel } from '../Common/DomainData/DepartmentDomainModel';
import { GridDateComponent } from '../Common/grid/GridDateComponent';

@Component({
  selector: "item-reclass-list",
  templateUrl: "./ItemReclassificationListComponent.html"
})
export class ItemReclassificationListComponent implements OnInit {
  public serviceDocument: ServiceDocument<ItemReclassificationModel>;
  PageTitle = "Item Reclassification";
  redirectUrl = "ItemReclassification/New/";
  componentName: any;
  columns: any[];
  model: ItemReclassificationModel = {
    reclassNo: null, reclassDesc: null, reclassDate: null, item: null, itemDesc: null
  };
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  gridOptions: GridOptions;
  categoryData: DepartmentDomainModel[];

  constructor(public service: ItemReclassificationService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "Reclass No", field: "reclassNo", tooltipField: "reclassNo" },
      { headerName: "Description", field: "reclassDesc", tooltipField: "reclassDesc" },
      { headerName: "Date", field: "reclassDate", tooltipField: "reclassDate", cellRendererFramework: GridDateComponent },
      { headerName: "Item", field: "item", tooltipField: "item" },
      { headerName: "Item Desc", field: "itemDesc", tooltipField: "itemDesc" },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 90
      }
    ];

    this.categoryData = Object.assign([], this.service.serviceDocument.domainData["dept"]);

    this.sharedService.domainData = {
      category: this.categoryData
    };

    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }
}
