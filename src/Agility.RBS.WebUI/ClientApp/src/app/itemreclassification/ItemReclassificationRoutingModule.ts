import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ItemReclassificationResolver } from './ItemReclassificationResolver';
import { ItemReclassificationComponent } from './ItemReclassificationComponent';
import { ItemReclassificationListComponent } from './ItemReclassificationListComponent';

const ItemReclassificationRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "", redirectTo: "List", pathMatch: "full"
      },
      {
        path: "List",
        component: ItemReclassificationListComponent,
        resolve:
        {
          references: ItemReclassificationResolver
        }
      },
      {
        path: "New/:id",
        component: ItemReclassificationComponent,
        resolve:
        {
          references: ItemReclassificationResolver
        }
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(ItemReclassificationRoutes)
  ]
})
export class ItemReclassificationRoutingModule { }
