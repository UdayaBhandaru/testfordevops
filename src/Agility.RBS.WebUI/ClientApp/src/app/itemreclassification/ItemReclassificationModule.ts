import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../AgGridSharedModule";
import { TitleSharedModule } from "../TitleSharedModule";
import { RbsSharedModule } from "../RbsSharedModule";
import { ItemReclassificationRoutingModule } from './ItemReclassificationRoutingModule';
import { ItemReclassificationService } from './ItemReclassificationService';
import { ItemReclassificationResolver } from './ItemReclassificationResolver';
import { ItemReclassificationListComponent } from './ItemReclassificationListComponent';
import { ItemReclassificationComponent } from './ItemReclassificationComponent';

@NgModule({
  imports: [
    FrameworkCoreModule,
    ItemReclassificationRoutingModule,
    AgGridModule,
    AgGridSharedModule,
    TitleSharedModule,
    RbsSharedModule
  ],
  declarations: [
    ItemReclassificationListComponent,
    ItemReclassificationComponent
  ],
  providers: [
    ItemReclassificationService,
    ItemReclassificationResolver
  ]
})
export class ItemReclassificationModule {
}
