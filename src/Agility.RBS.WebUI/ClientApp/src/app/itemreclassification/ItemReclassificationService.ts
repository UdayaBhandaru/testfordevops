import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { ItemReclassificationModel } from "./ItemReclassificationModel";
import { SharedService } from '../Common/SharedService';
import { HttpHelperService } from '../Common/HttpHelperService';

@Injectable()
export class ItemReclassificationService {
  serviceDocument: ServiceDocument<ItemReclassificationModel> = new ServiceDocument<ItemReclassificationModel>();
  searchData: any;
  constructor(private sharedService: SharedService, private httpHelperService: HttpHelperService) { }

  newModel(model: ItemReclassificationModel): ServiceDocument<ItemReclassificationModel> {
    return this.serviceDocument.newModel(model);
  }

  list(): Observable<ServiceDocument<ItemReclassificationModel>> {
    return this.serviceDocument.list("/api/ItemReclassification/List");
  }

  search(): Observable<ServiceDocument<ItemReclassificationModel>> {
    return this.serviceDocument.search("/api/ItemReclassification/Search");
  }

  save(): Observable<ServiceDocument<ItemReclassificationModel>> {
    return this.serviceDocument.save("/api/ItemReclassification/Save", true, () => this.setDate());
  }

  getDataFromAPI(apiUrl: string): Observable<any> {
    var resp: any = this.httpHelperService.get(apiUrl);
    return resp;
  }

  setDate(): void {
    let sd: ItemReclassificationModel = this.serviceDocument.dataProfile.dataModel;
    if (sd !== null) {
      if (sd.reclassDate != null) {
        sd.reclassDate = this.sharedService.setOffSet(sd.reclassDate);
      }
    }
  }
}
