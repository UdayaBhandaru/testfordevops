import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { ItemReclassificationModel } from './ItemReclassificationModel';
import { ItemReclassificationService } from './ItemReclassificationService';
import { SharedService } from '../Common/SharedService';
import { DepartmentDomainModel } from '../Common/DomainData/DepartmentDomainModel';
import { ClassDomainModel } from '../Common/DomainData/ClassDomainModel';
import { SubClassDomainModel } from '../Common/DomainData/SubClassDomainModel';
import { Subscription } from 'rxjs';

@Component({
  selector: "item-reclass",
  templateUrl: "./ItemReclassificationComponent.html"
})

export class ItemReclassificationComponent implements OnInit {
  PageTitle = "Item Reclassification";
  redirectUrl = "ItemReclassification";
  serviceDocument: ServiceDocument<ItemReclassificationModel>;
  editMode: boolean = false;
  editModel: any;
  pForm: FormGroup;
  localizationData: any;
  model: ItemReclassificationModel;
  deptData: DepartmentDomainModel[];
  classList: ClassDomainModel[]; classData: ClassDomainModel[];
  subclass: SubClassDomainModel[]; subclassData: SubClassDomainModel[];
  apiUrl: string;
  subClassGetbyNameServiceSubscription: Subscription;
  searchItemGetServiceSubscription: Subscription;
  effectiveMinDate: Date;

  constructor(private service: ItemReclassificationService, private sharedService: SharedService, private router: Router, private ref: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
    this.deptData = Object.assign([], this.service.serviceDocument.domainData["department"]);
    this.classList = Object.assign([], this.service.serviceDocument.domainData["class"]);
    this.bind();
  }

  ngAfterViewChecked(): void {
    this.ref.detectChanges();
  }

  bind(): void {
    this.effectiveMinDate = new Date();
    delete this.editModel;
    if (this.sharedService.editObject) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
      this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
      delete this.sharedService.editObject;
      this.editMode = true;
    } else {
      this.model = {
        reclassNo: null, reclassDesc: null, reclassDate: null, item: null, itemDesc: null, toDept: null, toClass: null
        , toSubclass: null, operation: "I", toDeptDesc: null, toSubclassDesc: null, toClassDesc: null, deptDesc: null
        , classDesc: null, subclassDesc: null
      };
      this.service.serviceDocument.dataProfile.dataModel = this.model;
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  save(saveOnly: boolean): void {
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.saveSubscription(saveOnly);
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  saveAndContinue(): void {
    this.save(false);
  }

  saveSubscription(saveOnly: boolean): void {
    this.saveCategory(saveOnly);
  }

  saveCategory(saveOnly: boolean): void {
    this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.sharedService.saveForm("Item Reclassification saved successfully").subscribe(() => {
          if (saveOnly) {
            this.router.navigate(["/" + this.redirectUrl + "/List"], { skipLocationChange: true });
          } else {
            if (!this.editMode) {
              this.model = {
                reclassNo: null, reclassDesc: null, reclassDate: null, item: null, itemDesc: null, toDept: null, toClass: null
                , toSubclass: null, operation: "I", toDeptDesc: null, toSubclassDesc: null, toClassDesc: null
              };
              this.service.serviceDocument.dataProfile.dataModel = this.model;
              this.service.newModel(this.model);
            } else {
              Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
              this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.service.serviceDocument.dataProfile.dataModel);
              this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            }
          }
        }, (error: string) => { console.log(error); });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
    }, (error: string) => { console.log(error); });
  }

  changeDepartment(): void {
    this.serviceDocument.dataProfile.profileForm.controls["toClass"].setValue(null);
    this.classData = this.classList.filter(item => item.dept === +this.serviceDocument.dataProfile.profileForm.controls["toDept"].value);
  }

  itemBasedLogic(): void {
    this.pForm = this.serviceDocument.dataProfile.profileForm;
    if (this.pForm.controls["item"].value != null) {
      this.apiUrl = "/api/ItemList/SearchItemGet?item=" + this.pForm.controls["item"].value;
      this.searchItemGetServiceSubscription = this.service.getDataFromAPI(this.apiUrl).subscribe(response => {
        if (response) {

          this.serviceDocument.dataProfile.profileForm.patchValue({
            itemDesc: response.itemDesc,
            deptDesc: response.categoryDesc,
            classDesc: response.classDesc,
            subclassDesc: response.subclassDesc
          });
        } else {
          this.sharedService.errorForm(this.localizationData.itemList.itemlistitemvalid);
        }
      });
    }
  }

  changeClass(item: any): void {
    this.serviceDocument.dataProfile.profileForm.controls["toSubclass"].setValue(null);
    this.apiUrl = "/api/ItemBasicInfo/SubClassGetbyName?deptId=" + this.serviceDocument.dataProfile.profileForm.controls["toDept"].value
      + "&classId=" + item.options.class;

    this.subClassGetbyNameServiceSubscription = this.service.getDataFromAPI(this.apiUrl).subscribe(response => {
      this.subclass = response;
    });
  }

  ngOnDestroy(): void {
    if (this.searchItemGetServiceSubscription) {
      this.searchItemGetServiceSubscription.unsubscribe();
    }
    if (this.subClassGetbyNameServiceSubscription) {
      this.subClassGetbyNameServiceSubscription.unsubscribe();
    }
  }

  reset(): void {
    if (!this.editMode) {
      this.model = {
        reclassNo: null, reclassDesc: null, reclassDate: null, item: null, itemDesc: null, toDept: null, toClass: null
        , toSubclass: null, operation: "I", toDeptDesc: null, toSubclassDesc: null, toClassDesc: null
      };
      this.service.serviceDocument.dataProfile.dataModel = this.model;
      this.service.newModel(this.model);
    } else {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    }
    this.serviceDocument = this.service.serviceDocument;
    this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
  }
}

