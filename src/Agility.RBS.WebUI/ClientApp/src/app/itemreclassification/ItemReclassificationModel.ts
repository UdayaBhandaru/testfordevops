export class ItemReclassificationModel {
  operation?: string;
  reclassNo: number;
  reclassDesc: string;
  reclassDate: Date;
  toDept?: number;
  toDeptDesc?: string;
  toClass?: number;
  toClassDesc?: string;
  toSubclass?: number;
  toSubclassDesc?: string;
  item: string;
  itemDesc: string;
  deptDesc?: string;
  classDesc?: string;
  subclassDesc?: string;
}
