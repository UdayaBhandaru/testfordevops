import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { SharedService } from "../Common/SharedService";
import { GridEditComponent } from "../Common/grid/GridEditComponent";
import { GridExportModel } from "../Common/grid/GridExportModel";
import { InfoComponent } from "../Common/InfoComponent";
import { ItemCloseHeadService } from "./ItemCloseHeadService";
import { ItemCloseHeadModel } from "./ItemCloseHeadModel";
import { GridOptions } from 'ag-grid-community';
import { WorkflowStatusDomainModel } from '../Common/DomainData/WorkflowStatusDomainModel';

@Component({
  selector: "ItemClose-List",
  templateUrl: "./ItemCloseHeadListComponent.html"
})
export class ItemCloseHeadListComponent implements OnInit {
  public serviceDocument: ServiceDocument<ItemCloseHeadModel>;
  PageTitle = "Close Item";
  redirectUrl = "ItemClose/New/";
  componentName: any;
  columns: any[];
  model: ItemCloseHeadModel = {
    id: null, reason: null, status: null
  };
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  gridOptions: GridOptions;
  workflowStatusList: WorkflowStatusDomainModel[];

  constructor(public service: ItemCloseHeadService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "Id", field: "id", tooltipField: "Id", width: 50 },
      { headerName: "Description", field: "reason", tooltipField: "reason", width: 150 },
      { headerName: "Status", field: "status", tooltipField: "status", width: 150 },
      {
        headerName: "ACTIONS", field: "Actions", cellRendererFramework: GridEditComponent, width: 40
      }
    ];

    this.workflowStatusList = Object.assign([], this.service.serviceDocument.domainData["workflowStatusList"]);

    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl + cell.data.id, this.service.serviceDocument.dataProfile.dataList);
  }

  addValidation(): void {
    this.router.navigate(["/" + this.redirectUrl + "0"], { skipLocationChange: true });
    this.sharedService.previousUrl = "../ItemClose/List";
  }
}
