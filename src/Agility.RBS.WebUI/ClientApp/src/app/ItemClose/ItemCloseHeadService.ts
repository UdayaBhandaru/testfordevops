import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument, WorkflowAction } from "@agility/frameworkcore";
import { HttpHelperService } from "../Common/HttpHelperService";
import { ItemCloseHeadModel } from "./ItemCloseHeadModel";
import { HttpParams, HttpHeaders, HttpClient } from "@angular/common/http";
import { map } from 'rxjs/operators';

@Injectable()
export class ItemCloseHeadService {
  serviceDocument: ServiceDocument<ItemCloseHeadModel> = new ServiceDocument<ItemCloseHeadModel>();
  searchData: any;
  constructor(private httpHelperService: HttpHelperService, private httpClient: HttpClient) { }

  newModel(model: ItemCloseHeadModel): ServiceDocument<ItemCloseHeadModel> {
    return this.serviceDocument.newModel(model);
  }

  list(): Observable<ServiceDocument<ItemCloseHeadModel>> {
    return this.serviceDocument.list("/api/ItemClose/List");
  }

  search(): Observable<ServiceDocument<ItemCloseHeadModel>> {
    return this.serviceDocument.search("/api/ItemClose/Search");
  }

  save(): Observable<ServiceDocument<ItemCloseHeadModel>> {
    if (!this.serviceDocument.dataProfile.dataModel.id) {
      this.serviceDocument.dataProfile.profileForm.controls["currentActionId"].setValue("ItemCloseToWkSheetClaim");
      return this.serviceDocument.submit("/api/ItemClose/Submit", true);
    } else {
      return this.serviceDocument.save("/api/ItemClose/Save", true);
    }
  }

  submit(): Observable<ServiceDocument<ItemCloseHeadModel>> {
    return this.serviceDocument.submit("/api/ItemClose/Submit", true);
  }

  addEdit(id: number): Observable<ServiceDocument<ItemCloseHeadModel>> {
    if (+id === 0) {
      return this.serviceDocument.new("/api/ItemClose/New");
    } else {
      return this.serviceDocument.open("/api/ItemClose/Open", new HttpParams().set("id", id.toString()));
    }
  }

  openItemCloseExcel(): Observable<any> {
    return this.serviceDocument.open("/api/ItemClose/OpenItemCloseExcel");
  }

  downloadExcel(id: number, reason: string): void {
    const type: any = "application/vnd.ms-excel";
    var apiUrl = "/api/ItemClose/OpenItemCloseExcel";

    let headers1: HttpHeaders = new HttpHeaders();
    headers1.set("Accept", type);
    this.httpClient.get(apiUrl,
      {
        headers: headers1, responseType: "blob" as "json"
        , params: new HttpParams().set("id", id.toString()).set("reason", reason)
      })
      .pipe(map((response: any) => {
        if (response instanceof Response) {
          return response.blob();
        }
        return response;
      }))
      .subscribe(res => {
        console.log("start download:", res);
        let url: string = window.URL.createObjectURL(res);
        let a: HTMLAnchorElement = document.createElement("a");
        document.body.appendChild(a);
        a.setAttribute("style", "display: none");
        a.href = url;
        a.download = "ItemClose-" + id.toString() + ".xlsm";
        a.click();
        window.URL.revokeObjectURL(url);
        a.remove();
      }, (error: string) => { console.log(error); });
  }
}
