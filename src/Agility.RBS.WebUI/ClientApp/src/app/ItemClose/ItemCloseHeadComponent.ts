import { Component, OnInit, TemplateRef, ViewChild, OnDestroy, ElementRef, ChangeDetectorRef } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ServiceDocument, CommonService, MessageResult, MessageType } from "@agility/frameworkcore";
import { MatDialogRef } from "@angular/material";
import { GridOptions } from "ag-grid-community";
import { Subscription } from "rxjs";
import { ItemCloseHeadModel } from "./ItemCloseHeadModel";
import { ItemCloseHeadService } from "./ItemCloseHeadService";
import { SharedService } from "../Common/SharedService";
import { LoaderService } from "../Common/LoaderService";
import { DocumentsConstants } from "../Common/DocumentsConstants";
import { ItemDetailModel } from './ItemDetailModel';

@Component({
  selector: "ItemClose",
  templateUrl: "./ItemCloseHeadComponent.html"
})

export class ItemCloseHeadComponent implements OnInit {
  _componentName = "Close Item";
  PageTitle = "Close Item";
  public serviceDocument: ServiceDocument<ItemCloseHeadModel>;
  localizationData: any;
  model: ItemCloseHeadModel;
  editMode: boolean; editModel: ItemCloseHeadModel;
  primaryId: number; // used in CanDeactivate method, should be same name if we use candeactivate

  routeServiceSubscription: Subscription;
  documentsConstants: DocumentsConstants = new DocumentsConstants();
  screenName: string;
  itemStatus: string;
  componentName: any;
  columns: any[];
  data: ItemDetailModel[] = [];
  gridOptions: GridOptions;

  constructor(private route: ActivatedRoute, public router: Router, private changeRef: ChangeDetectorRef
    , public sharedService: SharedService, private service: ItemCloseHeadService, private loaderService: LoaderService) {
  }

  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }

  ngOnInit(): void {
    this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
    this.screenName = this.documentsConstants.ItemCloseObjectName;
    this.componentName = this;
    this.columns = [
      { headerName: "Item", field: "item", tooltipField: "item" },
      { headerName: "Item Desc", field: "itemDesc", tooltipField: "itemDesc" }
    ];

    if (this.service.serviceDocument.dataProfile.dataModel) {
      this.bind();
    }
  }

  bind(): void {
    this.routeServiceSubscription = this.route.params.subscribe((resp: any) => {
      this.primaryId = +resp["id"];
    });
    if (this.primaryId.toString() !== "0") {
      this.editMode = true;
    } else {
      this.service.serviceDocument.dataProfile.dataModel =
        Object.assign(this.service.serviceDocument.dataProfile.dataModel
          , { operation: "I", status: "Worksheet" }
          , { workflowInstance: this.service.serviceDocument.dataProfile.dataModel.workflowInstance });
      this.service.serviceDocument.dataProfile.profileForm.patchValue(this.service.serviceDocument.dataProfile.dataModel);
    }
    if (this.editMode) {
      this.itemStatus = this.service.serviceDocument.dataProfile.dataModel.status;
      if (this.service.serviceDocument.dataProfile.dataModel.itemDetails) {
        Object.assign(this.data, this.service.serviceDocument.dataProfile.dataModel.itemDetails);
      }
      this.service.serviceDocument.dataProfile.dataModel = Object.assign(this.service.serviceDocument.dataProfile.dataModel
        , { operation: "I" }, { workflowInstance: this.service.serviceDocument.dataProfile.dataModel.workflowInstance });
      this.service.serviceDocument.dataProfile.profileForm.patchValue(this.service.serviceDocument.dataProfile.dataModel);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  save(saveOnly: boolean): void {

    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.saveSubscription(saveOnly);
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  saveAndContinue(): void {
    this.save(false);
  }

  saveSubscription(saveOnly: boolean): void {
    this.loaderService.display(true);
    this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.primaryId = this.service.serviceDocument.dataProfile.dataModel.id;
        this.sharedService.saveForm(`Saved successfully - Request ID ${this.primaryId}`).subscribe(() => {
          if (saveOnly) {
            this.router.navigate(["/ItemClose/List"], { skipLocationChange: true });
          } else {
            this.openItemCloseExcel();
            this.router.navigate(["/Blank"], { skipLocationChange: true, queryParams: { id: "/ItemClose/New/" + this.primaryId } });
            this.loaderService.display(false);
          }
        }, (error: string) => { console.log(error); });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
    }, (error: string) => { console.log(error); });
  }

  reset(): void {
    Object.keys(this.serviceDocument.dataProfile.profileForm.controls).forEach(control => {
      this.serviceDocument.dataProfile.profileForm.controls[control].setValue(this.serviceDocument.dataProfile.dataModel[control]);
    });
    this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
  }


  ngOnDestroy(): void {
    this.routeServiceSubscription.unsubscribe();
  }

  submitAction(): void {
    this.loaderService.display(true);
    this.service.submit().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.primaryId = this.service.serviceDocument.dataProfile.dataModel.id;
        this.sharedService.saveForm(`Saved successfully - Request ID  ${this.primaryId}`).subscribe(() => {
          this.router.navigate(["/ItemClose/List"], { skipLocationChange: true });
        }, (error: string) => {
          console.log(error);
          this.loaderService.display(false);
        });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
        this.loaderService.display(false);
      }
    }, (error: string) => {
      console.log(error);
      this.loaderService.display(false);
    });
  }
  openItemCloseExcel(): void {
    this.service.downloadExcel(this.service.serviceDocument.dataProfile.dataModel.id, this.service.serviceDocument.dataProfile.dataModel.reason);
  }
}
