import { ItemDetailModel } from './ItemDetailModel';

export class ItemCloseHeadModel {
  reason?: string;
  operation?: string;
  id?: number;
  workflowInstance?: string;
  status?: string;
  itemDetails?: ItemDetailModel[]
}
