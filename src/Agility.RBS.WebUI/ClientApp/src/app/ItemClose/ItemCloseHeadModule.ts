﻿import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../AgGridSharedModule";
import { TitleSharedModule } from "../TitleSharedModule";
import { ItemCloseHeadService } from "./ItemCloseHeadService";
import { ItemCloseHeadResolver } from "./ItemCloseHeadResolver";
import { ItemCloseHeadRoutingModule } from "./ItemCloseHeadRoutingModule";
import { ItemCloseHeadListComponent } from "./ItemCloseHeadListComponent";
import { ItemCloseHeadComponent } from "./ItemCloseHeadComponent";
import { ItemCloseHeadListResolver } from "./ItemCloseHeadListResolver";
import { RbsSharedModule } from "../RbsSharedModule";

@NgModule({
    imports: [
        FrameworkCoreModule,
        ItemCloseHeadRoutingModule,
        AgGridModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        ItemCloseHeadListComponent,
        ItemCloseHeadComponent
    ],
    providers: [
        ItemCloseHeadService,
        ItemCloseHeadResolver,
        ItemCloseHeadListResolver
    ]
})
export class ItemCloseHeadModule {
}
