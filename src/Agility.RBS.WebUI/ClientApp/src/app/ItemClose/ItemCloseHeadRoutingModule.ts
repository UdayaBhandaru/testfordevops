﻿import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ItemCloseHeadResolver } from "./ItemCloseHeadResolver";
import { ItemCloseHeadListComponent } from "./ItemCloseHeadListComponent";
import { ItemCloseHeadComponent } from "./ItemCloseHeadComponent";
import { ItemCloseHeadListResolver } from "./ItemCloseHeadListResolver";

const ItemCloseHeadRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: ItemCloseHeadListComponent,
                resolve:
                {
                        references: ItemCloseHeadListResolver
                }
            },
            {
                path: "New/:id",
                component: ItemCloseHeadComponent,
                resolve:
                    {
                        references: ItemCloseHeadResolver
                    }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(ItemCloseHeadRoutes)
    ]
})
export class ItemCloseHeadRoutingModule { }
