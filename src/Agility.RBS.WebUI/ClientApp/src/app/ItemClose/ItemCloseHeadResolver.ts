﻿import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { ItemCloseHeadModel } from "./ItemCloseHeadModel";
import { ItemCloseHeadService } from "./ItemCloseHeadService";

@Injectable()
export class ItemCloseHeadResolver implements Resolve<ServiceDocument<ItemCloseHeadModel>> {
    constructor(private service: ItemCloseHeadService) { }
    resolve(route: ActivatedRouteSnapshot): Observable<ServiceDocument<ItemCloseHeadModel>> {
        return this.service.addEdit(route.params["id"]);
    }
}