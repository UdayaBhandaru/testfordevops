﻿import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { ItemCloseHeadService } from "./ItemCloseHeadService";
import { ItemCloseHeadModel } from "./ItemCloseHeadModel";

@Injectable()
export class ItemCloseHeadListResolver implements Resolve<ServiceDocument<ItemCloseHeadModel>> {
    constructor(private service: ItemCloseHeadService) { }
    resolve(): Observable<ServiceDocument<ItemCloseHeadModel>> {
        return this.service.list();
    }
}