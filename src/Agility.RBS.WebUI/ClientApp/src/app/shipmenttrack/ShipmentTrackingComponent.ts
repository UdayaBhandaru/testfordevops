import { Component, OnInit, ViewChild, OnDestroy, HostListener, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType, CommonService, AjaxModel, AjaxResult } from "@agility/frameworkcore";
import { FormGroup, Validators, FormControl } from "@angular/forms";
import { GridOptions, GridApi, ColumnApi, GridReadyEvent, RowNode } from "ag-grid-community";
import { Subscription } from "rxjs";
import { GridExportModel } from "../Common/grid/GridExportModel";
import { SharedService } from "../Common/SharedService";
import { LoaderService } from "../Common/LoaderService";
import { DocumentsConstants } from "../Common/DocumentsConstants";
import { ShipmentTrackingService } from './ShipmentTrackingService';
import { ShipmentModel } from './ShipmentModel';
import { DomainDetailModel } from '../domain/DomainDetailModel';
import { CompanyDomainModel } from '../Common/DomainData/CompanyDomainModel';
import { DivisionDomainModel } from '../Common/DomainData/DivisionDomainModel';
import { CountryDomainModel } from '../Common/DomainData/CountryDomainModel';
import { OutLocationDomainModel } from '../Common/DomainData/OutLocationDomainModel';
import { PartnerDomainModel } from '../Common/DomainData/PartnerDomainModel';
import { NbDomainDetailModel } from '../Common/DomainData/NbDomainDetailModel';
import { SupplierDomainModel } from '../Common/DomainData/SupplierDomainModel';
import { CurrencyDomainModel } from '../Common/DomainData/CurrencyDomainModel';
import { VatCodeDomainModel } from '../Common/DomainData/VatCodeDomainModel';
import { GridEditComponent } from '../Common/grid/GridEditComponent';
import { GridSelectComponent } from '../Common/grid/GridSelectComponent';
import { GridNumericComponent } from '../Common/grid/GridNumericComponent';
import { GridInputComponent } from '../Common/grid/GridInputComponent';
import { ShipContainerModel } from './ShipContainerModel';
import { ShipEventModel } from './ShipEventModel';
import { ShipDocModel } from './ShipDocModel';
import { ShipOrderModel } from './ShipOrderModel';
import { GridDateComponent } from '../Common/grid/GridDateComponent';
import { FreightTypeModel } from '../Freight/FreightType/FreightTypeModel';
import { TransOrderModel } from './TransOrderModel';

@Component({
  selector: "shipment-track",
  templateUrl: "./ShipmentTrackingComponent.html",
  styleUrls: ['./ShipmentTrackingComponent.scss']
})
export class ShipmentTrackingComponent implements OnInit, OnDestroy {
  loadedPage: boolean;
  PageTitle = "Shipment Tracking";
  sheetName: string = "Shipment Tracking";
  saveServiceSubscription: Subscription;

  public serviceDocument: ServiceDocument<ShipmentModel>;
  componentName: any;
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  model: ShipmentModel;
  selectedTab: number;

  shipmentTypeData: DomainDetailModel[];
  statusData: DomainDetailModel[];
  companyData: CompanyDomainModel[];
  divisionData: DivisionDomainModel[];
  overStatusData: NbDomainDetailModel[];
  shipLineData: NbDomainDetailModel[];
  freighttypeData: FreightTypeModel[];
  countryData: CountryDomainModel[];
  outlocData: OutLocationDomainModel[];
  contentData: NbDomainDetailModel[];
  brokerData: PartnerDomainModel[];
  delayrsnData: NbDomainDetailModel[];
  heldrsnData: DomainDetailModel[];
  ifrajtypeData: NbDomainDetailModel[];
  supplierData: SupplierDomainModel[];
  currencyData: CurrencyDomainModel[];
  vatcodeData: VatCodeDomainModel[];
  consizeData: NbDomainDetailModel[];
  munshiftData: NbDomainDetailModel[];

  apiUrlSupplier: string;
  componentParentName: ShipmentTrackingComponent;

  columnContainer: any[];
  dataContainer: ShipContainerModel[] = [];
  dataContainerReset: ShipContainerModel[] = [];
  containerModel: ShipContainerModel;
  gridApiContainer: GridApi;
  gridOptionsContainer: GridOptions;

  columnEvent: any[];
  dataEvent: ShipEventModel[] = [];
  dataEventReset: ShipEventModel[] = [];
  eventModel: ShipEventModel;
  gridApiEvent: GridApi;
  gridOptionsEvent: GridOptions;

  columnDocument: any[];
  dataDocument: ShipDocModel[] = [];
  dataDocumentReset: ShipDocModel[] = [];
  docModel: ShipDocModel;
  gridApiDoc: GridApi;
  gridOptionsDoc: GridOptions;

  columnOrder: any[];
  dataOrder: ShipOrderModel[] = [];
  dataOrderReset: ShipOrderModel[] = [];
  ordModel: ShipOrderModel;
  gridApiOrd: GridApi;
  gridOptionsOrder: GridOptions;
  addText: string = "+ Add Item";
  editMode: boolean = false;
  editModel: any;
  orderDetailsServiceSubscription: Subscription;
  shipDetailsServiceSubscription: Subscription;
  interfaceServiceSubscription: Subscription;

  constructor(private sharedService: SharedService, private router: Router, private service: ShipmentTrackingService
    , private loaderService: LoaderService, private commonService: CommonService, private ref: ChangeDetectorRef) {
    this.selectedTab = 0;
  }

  ngOnInit(): void {
    this.shipmentTypeData = Object.assign([], this.service.serviceDocument.domainData["shipmenttype"]);
    this.statusData = Object.assign([], this.service.serviceDocument.domainData["status"]);
    this.companyData = Object.assign([], this.service.serviceDocument.domainData["company"]);
    this.divisionData = Object.assign([], this.service.serviceDocument.domainData["division"]);
    this.overStatusData = Object.assign([], this.service.serviceDocument.domainData["overallstatus"]);
    this.shipLineData = Object.assign([], this.service.serviceDocument.domainData["shipline"]);
    this.freighttypeData = Object.assign([], this.service.serviceDocument.domainData["freighttype"]);
    this.countryData = Object.assign([], this.service.serviceDocument.domainData["country"]);
    this.outlocData = Object.assign([], this.service.serviceDocument.domainData["outloc"]);
    this.contentData = Object.assign([], this.service.serviceDocument.domainData["content"]);
    this.brokerData = Object.assign([], this.service.serviceDocument.domainData["broker"]);
    this.delayrsnData = Object.assign([], this.service.serviceDocument.domainData["delayrsn"]);
    this.heldrsnData = Object.assign([], this.service.serviceDocument.domainData["heldrsn"]);
    this.ifrajtypeData = Object.assign([], this.service.serviceDocument.domainData["ifrajtype"]);
    this.currencyData = Object.assign([], this.service.serviceDocument.domainData["currency"]);
    this.vatcodeData = Object.assign([], this.service.serviceDocument.domainData["vatcode"]);
    this.consizeData = Object.assign([], this.service.serviceDocument.domainData["consize"]);
    this.munshiftData = Object.assign([], this.service.serviceDocument.domainData["munshift"]);

    this.componentParentName = this;

    this.columnContainer = [
      { headerName: "Container Id", field: "containerId", tooltipField: "containerId", cellRendererFramework: GridInputComponent },
      {
        headerName: "Container Size", field: "containerSize", cellRendererFramework: GridSelectComponent
        , cellRendererParams: { references: this.consizeData }, tooltipField: "programMessage"
      },
      { headerName: "Item Count", field: "itemCount", tooltipField: "itemCount", cellRendererFramework: GridNumericComponent },
      { headerName: "Weight", field: "totalWeight", tooltipField: "totalWeight", cellRendererFramework: GridNumericComponent },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 150
        , cellRendererParams: { restrictEditIcon: true }
      }
    ];
    this.gridOptionsContainer = {
      onGridReady: (params: any) => {
        this.gridApiContainer = params.api;
      }
    };

    this.columnEvent = [
      { headerName: "Event Id", field: "eventId", tooltipField: "eventId", cellRendererFramework: GridInputComponent },
      { headerName: "Event Value", field: "eventValue", tooltipField: "eventValue", cellRendererFramework: GridNumericComponent },
      { headerName: "Event Text", field: "eventText", tooltipField: "eventText", cellRendererFramework: GridInputComponent },
      {
        headerName: "Event Date", field: "eventDate", tooltipField: "eventDate | date:'dd-MMM - yyyy'"
        , cellRendererFramework: GridDateComponent, filter: "date", suppressSizeToFit: true, width: 150
      },
      { headerName: "Comments", field: "comments", tooltipField: "comments", cellRendererFramework: GridInputComponent },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 150
        , cellRendererParams: { restrictEditIcon: true }
      }
    ];

    this.gridOptionsEvent = {
      onGridReady: (params: any) => {
        this.gridApiEvent = params.api;
      }
    };

    this.columnDocument = [
      { headerName: "Doc Id", field: "docId", tooltipField: "docId", cellRendererFramework: GridInputComponent },
      {
        headerName: "Expected Date", field: "docExpDate", tooltipField: "docExpDate | date:'dd-MMM - yyyy'"
        , cellRendererFramework: GridDateComponent, filter: "date", suppressSizeToFit: true, width: 150
      },
      {
        headerName: "Actual Date", field: "docActDate", tooltipField: "docActDate | date:'dd-MMM - yyyy'"
        , cellRendererFramework: GridDateComponent, filter: "date", suppressSizeToFit: true, width: 150
      },
      { headerName: "Comments", field: "docComments", tooltipField: "docComments", cellRendererFramework: GridInputComponent },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 150
        , cellRendererParams: { restrictEditIcon: true }
      }
    ];
    this.gridOptionsDoc = {
      onGridReady: (params: any) => {
        this.gridApiDoc = params.api;
      }
    };

    this.columnOrder = [
      { headerName: "Order", field: "orderId", tooltipField: "orderId", cellRendererFramework: GridInputComponent },
      { headerName: "Supplier", field: "supName", tooltipField: "supName" },
      { headerName: "Order Date", field: "writtenDate", tooltipField: "writtenDate", cellRendererFramework: GridDateComponent },
      { headerName: "Country", field: "countryDesc", tooltipField: "countryDesc" },
      { headerName: "Currency", field: "currencyCode", tooltipField: "currencyCode" },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 150
        , cellRendererParams: { restrictEditIcon: true }
      }
    ];
    this.gridOptionsOrder = {
      onGridReady: (params: any) => {
        this.gridApiOrd = params.api;
      }
    };

    this.apiUrlSupplier = "/api/Supplier/SuppliersGetName?name=";
    this.loaderService.display(true);
    this.componentName = this;
    this.bind();
    this.loaderService.display(false);
  }

  bind(): void {
    delete this.editModel;
    if (this.sharedService.editObject) {
      this.supplierData = this.sharedService.editObject.suppliers;
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
      this.PageTitle = `${this.PageTitle} - EDIT - ${this.service.serviceDocument.dataProfile.dataModel.shipId}`;

      this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
      if (this.service.serviceDocument.dataProfile.dataModel.shipContainers.length > 0) {
        Object.assign(this.dataContainer, this.service.serviceDocument.dataProfile.dataModel.shipContainers);
        Object.assign(this.dataContainerReset, this.service.serviceDocument.dataProfile.dataModel.shipContainers);
      } else {
        this.newConModel();
        this.dataContainer.unshift(this.containerModel);
        if (this.gridApiContainer) {
          this.gridApiContainer.setRowData(this.dataContainer);
        }
      }

      if (this.service.serviceDocument.dataProfile.dataModel.shipEvents.length > 0) {
        Object.assign(this.dataEvent, this.service.serviceDocument.dataProfile.dataModel.shipEvents);
        Object.assign(this.dataEventReset, this.service.serviceDocument.dataProfile.dataModel.shipEvents);
      } else {
        this.newEventModel();
        this.dataEvent.unshift(this.eventModel);
        if (this.gridApiEvent) {
          this.gridApiEvent.setRowData(this.dataEvent);
        }
      }

      if (this.service.serviceDocument.dataProfile.dataModel.shipDocs.length > 0) {
        Object.assign(this.dataDocument, this.service.serviceDocument.dataProfile.dataModel.shipDocs);
        Object.assign(this.dataDocumentReset, this.service.serviceDocument.dataProfile.dataModel.shipDocs);
      } else {
        this.newDocModel();
        this.dataDocument.unshift(this.docModel);
        if (this.gridApiDoc) {
          this.gridApiDoc.setRowData(this.dataDocument);
        }
      }

      if (this.service.serviceDocument.dataProfile.dataModel.shipOrders.length > 0) {
        Object.assign(this.dataOrder, this.service.serviceDocument.dataProfile.dataModel.shipOrders);
        Object.assign(this.dataOrderReset, this.service.serviceDocument.dataProfile.dataModel.shipOrders);
      } else {
        this.newOrderModel();
        this.dataOrder.unshift(this.ordModel);
        if (this.gridApiOrd) {
          this.gridApiOrd.setRowData(this.dataOrder);
        }
      }

      this.editMode = true;
      delete this.sharedService.editObject;

    } else {
      this.PageTitle = `${this.PageTitle} - ADD`;
      this.newModel();
      this.service.newModel(this.model);
      this.newConModel();
      this.dataContainer.unshift(this.containerModel);
      if (this.gridApiContainer) {
        this.gridApiContainer.setRowData(this.dataContainer);
      }

      this.newEventModel();
      this.dataEvent.unshift(this.eventModel);
      if (this.gridApiEvent) {
        this.gridApiEvent.setRowData(this.dataEvent);
      }

      this.newDocModel();
      this.dataDocument.unshift(this.docModel);
      if (this.gridApiDoc) {
        this.gridApiDoc.setRowData(this.dataDocument);
      }

      this.newOrderModel();
      this.dataOrder.unshift(this.ordModel);
      if (this.gridApiOrd) {
        this.gridApiOrd.setRowData(this.dataOrder);
      }
    }
    this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    this.serviceDocument = this.service.serviceDocument;
  }

  ngAfterViewChecked(): void {
    this.ref.detectChanges();
  }

  saveAndContinue(): void {
    this.save(false);
  }

  save(saveOnly: boolean): void {
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      let valid: boolean = true;
      let emptyProperties: string = "<br/><ul>";
      this.dataContainer = this.dataContainer.filter(id => id.containerSize !== null || id.itemCount !== null || id.totalWeight !== null);
      this.dataContainer.forEach((item: ShipContainerModel) => {
        Object.keys(item).forEach((prop: string) => {
          if (prop === "containerId") {
            if (item[prop] === "" || item[prop] === null) {
              emptyProperties += emptyProperties.indexOf(prop.toUpperCase()) > -1 ? "" : "<li>" + prop.toUpperCase() + "</li>";
              valid = false;
            }
          }
        });
        emptyProperties += "</ul>";
      });
      if (!valid) {
        this.sharedService.errorForm("Please provide following details in Container" + emptyProperties.slice(0, -1));
      } else {
        this.saveSubscription(saveOnly);
      }
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  saveSubscription(saveOnly: boolean): void {
    this.loaderService.display(true);
    if (this.dataContainer.length > 0) {
      this.serviceDocument.dataProfile.profileForm.controls["shipContainers"] = new FormControl();
      this.serviceDocument.dataProfile.profileForm.controls["shipContainers"].setValue(this.dataContainer);
    }

    this.dataEvent = this.dataEvent.filter(id => id.eventId !== null || id.eventValue !== null || id.eventText !== null || id.eventDate !== null || id.comments !== null);
    if (this.dataEvent.length > 0) {
      this.serviceDocument.dataProfile.profileForm.controls["shipEvents"] = new FormControl();
      this.serviceDocument.dataProfile.profileForm.controls["shipEvents"].setValue(this.dataEvent);
    }

    this.dataDocument = this.dataDocument.filter(id => id.docId !== null || id.docExpDate !== null || id.docActDate !== null || id.docComments !== null);
    if (this.dataDocument.length > 0) {
      this.serviceDocument.dataProfile.profileForm.controls["shipDocs"] = new FormControl();
      this.serviceDocument.dataProfile.profileForm.controls["shipDocs"].setValue(this.dataDocument);
    }

    this.dataOrder = this.dataOrder.filter(id => id.orderId !== null);
    if (this.dataOrder.length > 0) {
      this.serviceDocument.dataProfile.profileForm.controls["shipOrders"] = new FormControl();
      this.serviceDocument.dataProfile.profileForm.controls["shipOrders"].setValue(this.dataOrder);
    }

    this.saveServiceSubscription = this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.sharedService.saveForm(`Shipment Tracking saved successfully`).subscribe(() => {
          if (saveOnly) {
            this.router.navigate(["/ShipmentTracking/List"], { skipLocationChange: true });
          } else {
            let apishipmentUrl: string = "/api/ShipmentTracking/ShipmentTrackOpen?shipId=" + this.service.serviceDocument.dataProfile.dataModel.shipId;
            this.shipDetailsServiceSubscription = this.service.getDataFromAPI(apishipmentUrl).subscribe((response: ShipmentModel) => {
              if (response) {
                this.sharedService.editObject = response;
                this.router.navigate(["/Blank"], {
                  skipLocationChange: true, queryParams: {
                    id: "/ShipmentTracking/New/"
                  }
                });
              }
            }, (error: string) => { console.log(error); });
            this.loaderService.display(false);
          }
        }, (error: string) => { console.log(error); });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
    }, (error: string) => { console.log(error); });
    this.loaderService.display(false);
  }

  reset(): void {
    this.resetSuperData();
    this.newModel();
    this.service.newModel(this.model);
    this.serviceDocument = this.service.serviceDocument;
  }

  resetSuperData(): void {
    this.selectedTab = 0;
  }

  newModel(): void {
    this.model = {
      shipId: null, shipmentType: null, typeDesc: null, shipStatus: null, statusDesc: null, shipCompany: null, companyDesc: null
      , shipDivision: null, divisionDesc: null, shipFinalStatus: null, finalStatusDesc: null, shipLine: null, shipLineDesc: null
      , shipAwbNo: null, shipFreightType: null, freightTypeDesc: null, shipCountryOrigin: null, orgCountryName: null, shipPortOfOrigin: null
      , orgPortName: null, shipDestination: null, destinationName: null, shipDestPort: null, destPortName: null, shipPolicyNo: null
      , shipDocRcptDate: null, shipOrigRcptDate: null, shipAnDate: null, shipAtaDate: null, shipContents: null, contentsDesc: null
      , shipBrokerDate: null, shipBroker: null, brokerName: null, shipClearanceDate: null, shipDelayReason: null, delayReasonDesc: null
      , shipBayanDate: null, shipBayanNo: null, shipHeldDate: null, shipHeldReason: null, heldReasonDesc: null, ifrajType: null
      , ifrajTypeDesc: null, ifrajReqDate: null, ifrajReceiptDate: null, shipInvoiceNo: null, shipInvoiceDate: null, shipInvoiceCurr: null
      , shipInvoiceAmt: null, shipFreightCharge: null, shipTranspAmt: null, shipInsuranceAmt: null, shipClearanceAmt: null, shipDemurageAmt: null
      , shipDemurageReason: null, shipFinishingExp: null, shipCustomsDuty: null, shipVatPer: null, shipVatAmt: null, shipVesselId: null
      , shipVoyageFltId: null, shipEstDeptDate: null, transferStatus: null, shipMunShiftNo: null, bladiyaFileNo: null, shipEtaDate: null
      , shipSupplier: null, supName: null, operation: "I"
    };
  }

  newConModel(): void {
    this.containerModel = {
      operation: "I", containerId: null, containerSize: null, itemCount: null, totalWeight: null, shipId: null, tableName: "ShipContainer"
    };
  }

  newEventModel(): void {
    this.eventModel = {
      operation: "I", eventId: null, eventText: null, eventValue: null, eventDate: null, shipId: null, comments: null, docEntryDate: null
      , tableName: "ShipEvent", lineSeq: null
    };
  }

  newDocModel(): void {
    this.docModel = {
      operation: "I", docId: null, docActDate: null, shipId: null, docComments: null, docExpDate: null, tableName: "ShipDoc", lineSeq: null
    };
  }

  newOrderModel(): void {
    this.ordModel = {
      operation: "I", shipId: null, orderId: null, tableName: "ShipOrder"
    };
  }

  addContainer($event: MouseEvent): void {
    $event.preventDefault();
    let filterData: ShipContainerModel[] = this.dataContainer.filter(id => id.containerId === null);
    if (filterData.length === 0) {
      this.newConModel();
      this.dataContainer.unshift(this.containerModel);
      if (this.gridApiContainer) {
        this.gridApiContainer.setRowData(this.dataContainer);
      }
    }
  }

  addEvent($event: MouseEvent): void {
    $event.preventDefault();
    let filterData: ShipEventModel[] = this.dataEvent.filter(id => id.comments === null && id.eventDate === null
      && id.eventId === null && id.eventText === null && id.eventValue === null);
    if (filterData.length === 0) {
      this.newEventModel();
      this.dataEvent.unshift(this.eventModel);
      if (this.gridApiEvent) {
        this.gridApiEvent.setRowData(this.dataEvent);
      }
    }
  }

  addDoc($event: MouseEvent): void {
    $event.preventDefault();
    let filterData: ShipDocModel[] = this.dataDocument.filter(id => id.docComments === null && id.docId === null && id.docActDate === null
      && id.docExpDate === null);
    if (filterData.length === 0) {
      this.newDocModel();
      this.dataDocument.unshift(this.docModel);
      if (this.gridApiDoc) {
        this.gridApiDoc.setRowData(this.dataDocument);
      }
    }
  }

  addOrder($event: MouseEvent): void {
    $event.preventDefault();
    let filterData: ShipOrderModel[] = this.dataOrder.filter(id => id.orderId === null);
    if (filterData.length === 0) {
      this.newOrderModel();
      this.dataOrder.unshift(this.ordModel);
      if (this.gridApiOrd) {
        this.gridApiOrd.setRowData(this.dataOrder);
      }
    }
  }

  update(cell: any, event: any): void {
    if (event.target.value) {
      if (cell.column.colId === "containerId" || cell.column.colId === "itemCount" || cell.column.colId === "totalWeight") {
        this.dataContainer[cell.rowIndex][cell.column.colId] = event.target.value;
        let rowNode: RowNode = this.gridApiContainer.getRowNode(cell.rowIndex);
        rowNode.setData(rowNode.data);
      } else if (cell.column.colId === "eventId" || cell.column.colId === "eventValue" || cell.column.colId === "eventText"
        || cell.column.colId === "eventDate" || cell.column.colId === "comments") {
        this.dataEvent[cell.rowIndex][cell.column.colId] = event.target.value;
        let rowNode: RowNode = this.gridApiEvent.getRowNode(cell.rowIndex);
        rowNode.setData(rowNode.data);
      } else if (cell.column.colId === "docId" || cell.column.colId === "docExpDate" || cell.column.colId === "docActDate" || cell.column.colId === "docComments") {
        this.dataDocument[cell.rowIndex][cell.column.colId] = event.target.value;
        let rowNode: RowNode = this.gridApiDoc.getRowNode(cell.rowIndex);
        rowNode.setData(rowNode.data);
      } else if (cell.column.colId === "orderId") {
        this.dataOrder[cell.rowIndex][cell.column.colId] = event.target.value;
        this.orderDetails(cell);
        let rowNode: RowNode = this.gridApiOrd.getRowNode(cell.rowIndex);
        rowNode.setData(rowNode.data);
      }
    }
  }

  orderDetails(cell: any): void {
    //var pForm = this.serviceDocument.dataProfile.profileForm;
    if (this.dataOrder[cell.rowIndex]["orderId"]) {
      let itmGridData: ShipOrderModel[] = this.dataOrder ? this.dataOrder.filter(itm => (itm.orderId === this.dataOrder[cell.rowIndex]["orderId"])) : null;
      if (itmGridData.length > 1) {
        this.sharedService.errorForm("Order No already exists");
      } else {
        let apiOrderUrl: string = "/api/ShipmentTracking/OrderDetailsGet?orderId=" + this.dataOrder[cell.rowIndex]["orderId"];

        this.orderDetailsServiceSubscription = this.service.getDataFromAPI(apiOrderUrl).subscribe((response: TransOrderModel) => {
          if (response) {
            this.dataOrder[cell.rowIndex]["supName"] = response.supName;
            this.dataOrder[cell.rowIndex]["countryDesc"] = response.countryDesc;
            this.dataOrder[cell.rowIndex]["currencyCode"] = response.currencyCode;
            this.dataOrder[cell.rowIndex]["writtenDate"] = response.writtenDate;
          } else {
            this.sharedService.errorForm("No Records available with Order No:" + this.dataOrder[cell.rowIndex]["orderId"]);
            this.dataOrder[cell.rowIndex]["orderId"] = null;
          }
          let rowNode: RowNode = this.gridApiOrd.getRowNode(cell.rowIndex);
          rowNode.setData(rowNode.data);
        }, (error: string) => { console.log(error); });
      }
    }
  }

  select(cell: any, event: any): void {
    if (cell.column.colId === "containerSize") {
      this.dataContainer[cell.rowIndex][cell.column.colId] = event.target.value;
      let rowNode: RowNode = this.gridApiContainer.getRowNode(cell.rowIndex);
      rowNode.setData(rowNode.data);
    }
  }

  ngOnDestroy(): void {
    if (this.saveServiceSubscription) {
      this.saveServiceSubscription.unsubscribe();
    }
    if (this.orderDetailsServiceSubscription) {
      this.orderDetailsServiceSubscription.unsubscribe();
    }
    if (this.shipDetailsServiceSubscription) {
      this.shipDetailsServiceSubscription.unsubscribe();
    }

    if (this.interfaceServiceSubscription) {
      this.interfaceServiceSubscription.unsubscribe();
    }
    
    this.shipmentTypeData.length = 0;
    this.statusData.length = 0;
    this.companyData.length = 0;
    this.divisionData.length = 0;
    this.overStatusData.length = 0;
    this.shipLineData.length = 0;
    this.freighttypeData.length = 0;
    this.countryData.length = 0;
    this.outlocData.length = 0;
    this.contentData.length = 0;
    this.brokerData.length = 0;
    this.delayrsnData.length = 0;
    this.heldrsnData.length = 0;
    this.ifrajtypeData.length = 0;
  }

  delete(cell: any): void {
    if (cell.data.tableName === "ShipContainer") {
      this.dataContainer.splice(cell.rowIndex, 1);
      if (this.dataContainer.length === 0) {
        this.newConModel();
        this.dataContainer.unshift(this.containerModel);
      }
      if (this.gridApiContainer) {
        this.gridApiContainer.setRowData(this.dataContainer);
      }
    } else if (cell.data.tableName === "ShipDoc") {
      this.dataDocument.splice(cell.rowIndex, 1);
      if (this.dataDocument.length === 0) {
        this.newDocModel();
        this.dataDocument.unshift(this.docModel);
      }
      if (this.gridApiDoc) {
        this.gridApiDoc.setRowData(this.dataDocument);
      }

    } else if (cell.data.tableName === "ShipEvent") {
      this.dataEvent.splice(cell.rowIndex, 1);
      if (this.dataEvent.length === 0) {
        this.newEventModel();
        this.dataEvent.unshift(this.eventModel);
      }
      if (this.gridApiEvent) {
        this.gridApiEvent.setRowData(this.dataEvent);
      }
    } else if (cell.data.tableName === "ShipOrder") {
      this.dataOrder.splice(cell.rowIndex, 1);
      if (this.dataOrder.length === 0) {
        this.newOrderModel();
        this.dataOrder.unshift(this.ordModel);
      }
      if (this.gridApiOrd) {
        this.gridApiOrd.setRowData(this.dataOrder);
      }
    }
  }

  interfacetoTrans(): void {
    let apiInterfaceUrl: string = "/api/ShipmentTracking/InterfacetoTrans?shipId=" + this.serviceDocument.dataProfile.profileForm.controls["shipId"].value;
    this.interfaceServiceSubscription = this.service.getDataFromAPI(apiInterfaceUrl).subscribe((response: AjaxModel<boolean>) => {
      if (response.result === AjaxResult.success) {
        this.sharedService.successForm("Transportation created successfully");
      } else {
        this.sharedService.errorForm(response.message);
      }
    }, (error: string) => { console.log(error); });
  }

  rollbackTrans(): void {
    let apiInterfaceUrl: string = "/api/ShipmentTracking/RollbackTrans?shipId=" + this.serviceDocument.dataProfile.profileForm.controls["shipId"].value;
    this.interfaceServiceSubscription = this.service.getDataFromAPI(apiInterfaceUrl).subscribe((response: AjaxModel<boolean>) => {
      if (response.result === AjaxResult.success) {
        this.sharedService.successForm("Transportation has been Rolled back");
      } else {
        this.sharedService.errorForm(response.message);
      }
    }, (error: string) => { console.log(error); });
  }
}
