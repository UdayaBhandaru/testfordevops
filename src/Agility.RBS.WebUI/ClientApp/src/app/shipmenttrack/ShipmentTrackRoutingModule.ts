import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ShipmentTrackingComponent } from './ShipmentTrackingComponent';
import { ShipmentTrackingResolver } from './ShipmentTrackingResolver';
import { ShipmentTrackingListComponent } from './ShipmentTrackingListComponent';
import { ShipmentTrackingListResolver } from './ShipmentTrackingListResolver';

const ShipTrackRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "", redirectTo: "List", pathMatch: "full"
      },
      {
        path: "List",
        component: ShipmentTrackingListComponent,
        resolve:
        {
          references: ShipmentTrackingListResolver
        }
      },
      {
        path: "New",
        component: ShipmentTrackingComponent,
        resolve:
        {
          serviceDocument: ShipmentTrackingResolver
        }
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(ShipTrackRoutes)
  ]
})
export class ShipmentTrackRoutingModule { }
