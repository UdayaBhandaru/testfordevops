export class ShipOrderModel {
  shipId?: number;
  seqNo?: number;
  orderId?: string;
  createDate?: Date;
  operation?: string;
  tableName?: string;

  writtenDate?: Date;
  supName?: string;
  countryDesc?: string;
  currencyCode?: string;
}
