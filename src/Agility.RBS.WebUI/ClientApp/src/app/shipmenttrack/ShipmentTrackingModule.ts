import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../AgGridSharedModule";
import { TitleSharedModule } from "../TitleSharedModule";
import { RbsSharedModule } from "../RbsSharedModule";
import { ShipmentTrackRoutingModule } from './ShipmentTrackRoutingModule';
import { ShipmentTrackingComponent } from './ShipmentTrackingComponent';
import { ShipmentTrackingService } from './ShipmentTrackingService';
import { ShipmentTrackingResolver } from './ShipmentTrackingResolver';
import { ShipmentTrackingListComponent } from './ShipmentTrackingListComponent';
import { ShipmentTrackingListResolver } from './ShipmentTrackingListResolver';

@NgModule({
  imports: [
    FrameworkCoreModule,
    ShipmentTrackRoutingModule,
    AgGridModule,
    AgGridSharedModule,
    TitleSharedModule,
    RbsSharedModule
  ],
  declarations: [
    ShipmentTrackingListComponent,
    ShipmentTrackingComponent
  ],
  providers: [
    ShipmentTrackingListResolver,
    ShipmentTrackingService,
    ShipmentTrackingResolver
  ]
})
export class ShipmentTrackingModule {
}
