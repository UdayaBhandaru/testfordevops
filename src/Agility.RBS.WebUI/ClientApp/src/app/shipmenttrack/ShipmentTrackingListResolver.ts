import { Injectable } from "@angular/core";
import { Resolve } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { ShipmentModel } from './ShipmentModel';
import { ShipmentTrackingService } from './ShipmentTrackingService';

@Injectable()
export class ShipmentTrackingListResolver implements Resolve<ServiceDocument<ShipmentModel>> {
  constructor(private service: ShipmentTrackingService) { }
  resolve(): Observable<ServiceDocument<ShipmentModel>> {
        return this.service.list();
    }
}
