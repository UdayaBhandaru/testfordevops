import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { HttpHelperService } from "../Common/HttpHelperService";
import { ShipmentModel } from "./ShipmentModel";
import { SharedService } from '../Common/SharedService';

@Injectable()
export class ShipmentTrackingService {
  serviceDocument: ServiceDocument<ShipmentModel> = new ServiceDocument<ShipmentModel>();
  shipNo: number;
  constructor(private httpHelperService: HttpHelperService, private sharedService: SharedService) { }

  newModel(model: ShipmentModel): ServiceDocument<ShipmentModel> {
    return this.serviceDocument.newModel(model);
  }

  list(): Observable<ServiceDocument<ShipmentModel>> {
    return this.serviceDocument.list("/api/ShipmentTracking/List");
  }

  search(): Observable<ServiceDocument<ShipmentModel>> {
    return this.serviceDocument.search("/api/ShipmentTracking/Search");
  }

  addEdit(): Observable<ServiceDocument<ShipmentModel>> {
    return this.serviceDocument.new("/api/ShipmentTracking/New");
  }

  save(): Observable<ServiceDocument<ShipmentModel>> {
    return this.serviceDocument.save("/api/ShipmentTracking/Save", true, () => this.setDate());
  }

  getDataFromAPI(apiUrl: string): Observable<any> {
    var resp: any = this.httpHelperService.get(apiUrl);
    return resp;
  }

  setDate(): void {
    let sd: ShipmentModel = this.serviceDocument.dataProfile.dataModel;
    if (sd !== null) {
      if (sd.shipEstDeptDate != null) {
        sd.shipEstDeptDate = this.sharedService.setOffSet(sd.shipEstDeptDate);
      }

      if (sd.shipDocRcptDate != null) {
        sd.shipDocRcptDate = this.sharedService.setOffSet(sd.shipDocRcptDate);
      }

      if (sd.shipOrigRcptDate != null) {
        sd.shipOrigRcptDate = this.sharedService.setOffSet(sd.shipOrigRcptDate);
      }

      if (sd.shipAnDate != null) {
        sd.shipAnDate = this.sharedService.setOffSet(sd.shipAnDate);
      }

      if (sd.shipEtaDate != null) {
        sd.shipEtaDate = this.sharedService.setOffSet(sd.shipEtaDate);
      }

      if (sd.shipAtaDate != null) {
        sd.shipAtaDate = this.sharedService.setOffSet(sd.shipAtaDate);
      }

      if (sd.shipBrokerDate != null) {
        sd.shipBrokerDate = this.sharedService.setOffSet(sd.shipBrokerDate);
      }

      if (sd.shipClearanceDate != null) {
        sd.shipClearanceDate = this.sharedService.setOffSet(sd.shipClearanceDate);
      }

      if (sd.shipBayanDate != null) {
        sd.shipBayanDate = this.sharedService.setOffSet(sd.shipBayanDate);
      }

      if (sd.shipHeldDate != null) {
        sd.shipHeldDate = this.sharedService.setOffSet(sd.shipHeldDate);
      }

      if (sd.ifrajReqDate != null) {
        sd.ifrajReqDate = this.sharedService.setOffSet(sd.ifrajReqDate);
      }

      if (sd.ifrajReceiptDate != null) {
        sd.ifrajReceiptDate = this.sharedService.setOffSet(sd.ifrajReceiptDate);
      }

      if (sd.shipInvoiceDate != null) {
        sd.shipInvoiceDate = this.sharedService.setOffSet(sd.shipInvoiceDate);
      }
    }
  }
}
