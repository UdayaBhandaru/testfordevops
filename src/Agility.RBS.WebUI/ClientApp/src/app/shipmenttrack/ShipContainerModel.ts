export class ShipContainerModel {
  shipId?: number;
  containerId?: string;
  containerSize?: string;
  itemCount?: number;
  totalWeight?: number;
  operation?: string;
  tableName?: string;
}
