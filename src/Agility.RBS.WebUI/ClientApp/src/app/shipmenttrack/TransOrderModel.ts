export class TransOrderModel {
  orderNo?: string;
  writtenDate?: Date;
  supplier?: string;
  supName?: string;
  countryId?: string;
  countryDesc?: string;
  currencyCode?: string;
}
