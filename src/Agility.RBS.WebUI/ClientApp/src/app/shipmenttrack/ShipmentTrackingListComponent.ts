import { Component, OnInit } from "@angular/core";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { SharedService } from "../Common/SharedService";
import { Router } from "@angular/router";
import { ShipmentModel } from './ShipmentModel';
import { ShipmentTrackingService } from './ShipmentTrackingService';
import { LoaderService } from '../Common/LoaderService';

@Component({
  selector: "ship-track-list",
  templateUrl: "./ShipmentTrackingListComponent.html"
})
export class ShipmentTrackingListComponent implements OnInit {
  public serviceDocument: ServiceDocument<ShipmentModel>;
  PageTitle = "Shipment Tracking";
  redirectUrl = "ShipmentTracking/New/";
  loadedPage: boolean;
  model: ShipmentModel = {
    shipId: null, shipVesselId: null, shipVoyageFltId: null, shipEstDeptDate: null
  };

  constructor(public service: ShipmentTrackingService, private sharedService: SharedService, private router: Router
    , private loaderService: LoaderService) {
  }

  ngOnInit(): void {
    this.service.newModel(this.model);
    this.serviceDocument = this.service.serviceDocument;
  }

  search(): void {
    var pForm: any = this.serviceDocument.dataProfile.profileForm;
    if (pForm.controls["shipId"].value || (pForm.controls["shipVesselId"].value && pForm.controls["shipVoyageFltId"].value && pForm.controls["shipEstDeptDate"].value)) {
      this.service.search().subscribe((response: any) => {
        if (this.service.serviceDocument.result.type === MessageType.success) {
          if (response.dataProfile.dataList) {
            this.sharedService.editObject = response.dataProfile.dataList[0];
            this.router.navigate(["/" + this.redirectUrl], { skipLocationChange: true });
            this.sharedService.previousUrl = "../ShipmentTracking/List";
          } else {
            this.sharedService.errorForm("No records found for given search criteria");
            this.loaderService.display(false);
          }
        } else {
          this.sharedService.showSTDialog(this.service.serviceDocument.result.innerException
            , this.service.serviceDocument.result.stackTrace || response.model.StackTraceString);
          this.loaderService.display(false);
        }
      }, (error: string) => {
        this.sharedService.errorForm(error);
        this.loaderService.display(false);
      });
    } else {
      this.sharedService.errorForm("Please enter the data in Vessel ID and Voyage Fit ID and Estimated Dept Date or Job #");
      this.loaderService.display(false);
    }
  }

  add(): void {
    this.router.navigate(["/" + this.redirectUrl], { skipLocationChange: true });
    this.sharedService.previousUrl = "../ShipmentTracking/List";
  }

  reset(): void {
    this.service.newModel(this.model);
    delete this.sharedService.searchData;
    this.serviceDocument = this.service.serviceDocument;
  }
}
