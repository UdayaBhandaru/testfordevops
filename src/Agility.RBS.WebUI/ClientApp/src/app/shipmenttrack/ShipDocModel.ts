export class ShipDocModel {
  shipId?: number;
  docId?: string;
  docExpDate?: Date;
  docActDate?: Date;
  docComments?: string;
  operation?: string;
  tableName?: string;
  lineSeq: number;
}
