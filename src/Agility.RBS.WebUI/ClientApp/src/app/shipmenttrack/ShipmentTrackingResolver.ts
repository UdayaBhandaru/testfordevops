import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { ShipmentTrackingService } from "./ShipmentTrackingService";
import { ShipmentModel } from './ShipmentModel';

@Injectable()
export class ShipmentTrackingResolver implements Resolve<ServiceDocument<ShipmentModel>> {
  constructor(private service: ShipmentTrackingService) { }
  resolve(): Observable<ServiceDocument<ShipmentModel>> {
    return this.service.addEdit();
  }
}
