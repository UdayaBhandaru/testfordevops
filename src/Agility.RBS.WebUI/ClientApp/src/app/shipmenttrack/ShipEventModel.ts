export class ShipEventModel {
  shipId?: number;
  eventId?: string;
  eventValue?: number;
  eventText?: string;
  eventDate?: Date;
  docEntryDate?: Date;
  comments?: string;
  operation?: string;
  tableName?: string;
  lineSeq: number;
}
