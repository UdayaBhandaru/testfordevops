import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { FreightTypeRoutingModule } from "./FreightTypeRoutingModule";
import { FreightTypeListComponent } from "./FreightTypeListComponent";
import { FreightTypeService } from "./FreightTypeService";
import { FreightTypeResolver } from "./FreightTypeResolver";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { RbsSharedModule } from "../../RbsSharedModule";

@NgModule({
  imports: [
    FrameworkCoreModule,
    FreightTypeRoutingModule,
    AgGridModule,
    AgGridSharedModule,
    TitleSharedModule,
    RbsSharedModule
  ],
  declarations: [
    FreightTypeListComponent,

  ],
  providers: [
    FreightTypeService,
    FreightTypeResolver
  ]
})
export class FreightTypeModule {
}
