import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { FreightTypeService } from "./FreightTypeService";
import { FreightTypeModel } from './FreightTypeModel';

@Injectable()
export class FreightTypeResolver implements Resolve<ServiceDocument<FreightTypeModel>> {
  constructor(private service: FreightTypeService) { }
  resolve(): Observable<ServiceDocument<FreightTypeModel>> {
    return this.service.list();
  }
}
