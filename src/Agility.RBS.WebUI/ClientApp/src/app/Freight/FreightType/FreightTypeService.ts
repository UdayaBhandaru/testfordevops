import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";
import { FreightTypeModel } from './FreightTypeModel';
@Injectable()
export class FreightTypeService {
  serviceDocument: ServiceDocument<FreightTypeModel> = new ServiceDocument<FreightTypeModel>();
  searchData: any;
  constructor(private httpHelperService: HttpHelperService) { }

  newModel(model: FreightTypeModel): ServiceDocument<FreightTypeModel> {
    return this.serviceDocument.newModel(model);
  }

  list(): Observable<ServiceDocument<FreightTypeModel>> {
    return this.serviceDocument.list("/api/FreightType/List");
  }
}
