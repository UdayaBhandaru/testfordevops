import { Component, OnInit } from "@angular/core";
import { ServiceDocument } from "@agility/frameworkcore";
import { FreightTypeService } from "./FreightTypeService";
import { FreightTypeModel } from './FreightTypeModel';
import { GridOptions } from 'ag-grid-community';

@Component({
  selector: "FreightType-List",
  templateUrl: "./FreightTypeListComponent.html",
  styleUrls: ['./FreightTypeListComponent.scss']
})
export class FreightTypeListComponent implements OnInit {
  public componentName: any;
  public  gridOptions: GridOptions;
  public serviceDocument: ServiceDocument<FreightTypeModel>;
  columns: any[];

  constructor(public service: FreightTypeService) {   
  }
  PageTitle = "Freight Type";
  ngOnInit(): void {
    this.columns = [
      { headerName: "Freight Type", field: "freightType", tooltipField: "freightType" },
      { headerName: "Description", field: "freightTypeDescription", tooltipField: "freightTypeDescription" }
    ];
    this.serviceDocument = this.service.serviceDocument;
  }
}
