import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { FreightTypeListComponent } from "./FreightTypeListComponent";
import { FreightTypeResolver } from "./FreightTypeResolver";

const DomainRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "", redirectTo: "List", pathMatch: "full"
      },
      {
        path: "List",
        component: FreightTypeListComponent,
        resolve:
        {
          references: FreightTypeResolver
        }
      },

    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(DomainRoutes)
  ]
})
export class FreightTypeRoutingModule { }
