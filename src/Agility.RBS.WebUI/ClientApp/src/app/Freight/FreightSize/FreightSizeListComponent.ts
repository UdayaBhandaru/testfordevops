import { Component, OnInit } from "@angular/core";
import { ServiceDocument } from "@agility/frameworkcore";
import { FreightSizeService } from "./FreightSizeService";
import { FreightSizeModel } from './FreightSizeModel';
import { GridOptions } from 'ag-grid-community';

@Component({
  selector: "FreightSize-List",
  templateUrl: "./FreightSizeListComponent.html",
  styleUrls: ['./FreightSizeListComponent.scss']
})
export class FreightSizeListComponent implements OnInit {
  public componentName: any;
  public showSearchCriteria: boolean = true;
  public  gridOptions: GridOptions;
  public serviceDocument: ServiceDocument<FreightSizeModel>;
  public columns: any[];
  constructor(public service: FreightSizeService) {
  }
  PageTitle = "Freight Size";
  ngOnInit(): void {
    this.columns = [
      { headerName: "Freight Type", field: "freightSize", tooltipField: "freightSize" },
      { headerName: "Description", field: "freightSizeDescription", tooltipField: "freightSizeDescription" }
    ];
    this.serviceDocument = this.service.serviceDocument;
  }
}
