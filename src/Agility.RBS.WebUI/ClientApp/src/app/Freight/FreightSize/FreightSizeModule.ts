import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { FreightSizeRoutingModule } from "./FreightSizeRoutingModule";
import { FreightSizeListComponent } from "./FreightSizeListComponent";
import { FreightSizeService } from "./FreightSizeService";
import { FreightSizeResolver } from "./FreightSizeResolver";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { RbsSharedModule } from "../../RbsSharedModule";

@NgModule({
  imports: [
    FrameworkCoreModule,
    FreightSizeRoutingModule,
    AgGridModule,
    AgGridSharedModule,
    TitleSharedModule,
    RbsSharedModule
  ],
  declarations: [
    FreightSizeListComponent,

  ],
  providers: [
    FreightSizeService,
    FreightSizeResolver
  ]
})
export class FreightSizeModule {
}
