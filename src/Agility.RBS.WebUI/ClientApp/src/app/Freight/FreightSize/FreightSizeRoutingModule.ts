import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { FreightSizeListComponent } from "./FreightSizeListComponent";
import { FreightSizeResolver } from "./FreightSizeResolver";

const DomainRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "", redirectTo: "List", pathMatch: "full"
      },
      {
        path: "List",
        component: FreightSizeListComponent,
        resolve:
        {
          references: FreightSizeResolver
        }
      },

    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(DomainRoutes)
  ]
})
export class FreightSizeRoutingModule { }
