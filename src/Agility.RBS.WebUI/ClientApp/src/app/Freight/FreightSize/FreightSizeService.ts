import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { HttpHelperService } from "../../Common/HttpHelperService";
import { FreightSizeModel } from './FreightSizeModel';
@Injectable()
export class FreightSizeService {
  serviceDocument: ServiceDocument<FreightSizeModel> = new ServiceDocument<FreightSizeModel>();
  searchData: any;
  constructor(private httpHelperService: HttpHelperService) { }

  newModel(model: FreightSizeModel): ServiceDocument<FreightSizeModel> {
    return this.serviceDocument.newModel(model);
  }

  list(): Observable<ServiceDocument<FreightSizeModel>> {
    return this.serviceDocument.list("/api/FreightSize/List");
  }
}
