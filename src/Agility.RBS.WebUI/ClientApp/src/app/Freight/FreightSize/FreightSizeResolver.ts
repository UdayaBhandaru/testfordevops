import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { FreightSizeService } from "./FreightSizeService";
import { FreightSizeModel } from './FreightSizeModel';

@Injectable()
export class FreightSizeResolver implements Resolve<ServiceDocument<FreightSizeModel>> {
  constructor(private service: FreightSizeService) { }
  resolve(): Observable<ServiceDocument<FreightSizeModel>> {
    return this.service.list();
  }
}
