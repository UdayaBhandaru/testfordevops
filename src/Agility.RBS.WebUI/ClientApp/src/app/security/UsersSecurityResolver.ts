﻿import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { UsersService } from "./users/UsersService";
import { UserModel } from "./users/UserModel";

@Injectable()
export class UsersListResolver implements Resolve<ServiceDocument<UserModel>> {
    constructor(private service: UsersService) { }
    resolve(): Observable<ServiceDocument<UserModel>> {
        return this.service.list();
    }
}

@Injectable()
export class UsersResolver implements Resolve<ServiceDocument<UserModel>> {
    constructor(private service: UsersService) { }
    resolve(): Observable<ServiceDocument<UserModel>> {
        return this.service.addEdit();
    }
}




