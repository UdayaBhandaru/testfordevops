import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { SharedService } from "../../Common/SharedService";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { UsersService } from "./UsersService";
import { OrgCountryModel } from "../../organizationhierarchy/orgcountry/OrgCountryModel";
import { CompanyModel } from "../../organizationhierarchy/company/CompanyModel";
import { RegionModel } from "../../organizationhierarchy/region/RegionModel";
import { FormatModel } from "../../organizationhierarchy/format/FormatModel";
import { UserModel } from "./UserModel";
import { UserProfileModel } from "../../account/UserProfileModel";
import { FrameWorkRoleModel } from "./FrameWorkRoleModel";
import { UserOrgHierarchyModel } from "./UserOrgHierarchyModel";
import { LocationModel } from '../../organizationhierarchy/location/LocationModel';
@Component({
  selector: "users",
  templateUrl: "./UsersComponent.html",
  styleUrls: ['./UsersComponent.scss']
})
export class UsersComponent implements OnInit {
  PageTitle = "Users Master";
  serviceDocument: ServiceDocument<UserModel>;
  domainDtlData: DomainDetailModel[];
  companyData: CompanyModel[];
  companySelectedData: CompanyModel[] = [];
  orgCountryData: OrgCountryModel[] = [];
  orgCountrySelectedData: OrgCountryModel[] = [];
  regionData: RegionModel[] = [];
  regionSelectedData: RegionModel[] = [];
  formatData: FormatModel[] = [];
  formatSelectedData: FormatModel[] = [];
  formatList: FormatModel[];
  regionList: RegionModel[];
  orgCountryList: OrgCountryModel[];
  locationData: LocationModel[];
  locationSelectedData: LocationModel[] = [];
  rolesData: FrameWorkRoleModel[];
  rolesSelectedData: FrameWorkRoleModel[] = [];
  indicatorData: DomainDetailModel[];
  users: UserProfileModel[];
  localizationData: any;
  editModel: any;
  editMode: boolean = false;
  defaultStatus: string;
  model: UserModel;
  modelReportingManager: UserModel[];
  fileName: string;
  fileDataAsBase64: string;
  imageSrc: any;
  userId: number;
  userPicture: any;
  fileUploadErrorMessage: string;
  showCustomRequiredMessageforRole: string;
  showCustomRequiredMessageForOrgInfo: string;
  userUpdatedMsg: string;
  roleswRemovedData: FrameWorkRoleModel[] = [];

  constructor(private service: UsersService, private sharedService: SharedService, private route: ActivatedRoute
    , private router: Router) {
  }

  ngOnInit(): void {
    this.bind();
  }

  bind(): void {
    this.route.data.subscribe(() => {
      this.loadUsersDomainDetails();
      delete this.editModel;
      if (this.sharedService.editObject) {
        this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
        this.rolesSelectedData = this.service.serviceDocument.dataProfile.dataModel.rolesList;
        this.BuildOrgHierarchyForEdit();
        // this.userPicture = this.service.serviceDocument.dataProfile.dataModel.userPicPath;
        this.imageSrc = this.service.serviceDocument.dataProfile.dataModel.userPicPath;
        this.fileName = this.service.serviceDocument.dataProfile.dataModel.fileName;
        this.fileDataAsBase64 = this.service.serviceDocument.dataProfile.dataModel.fileDataAsBase64;
        Object.assign(this.service.serviceDocument.dataProfile.dataModel, { operation: "U" });
        this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
        this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
        delete this.sharedService.editObject;
        this.editMode = true;
      } else {
        this.setUserModel();
        this.service.newModel(this.model);
        this.service.serviceDocument.dataProfile.profileForm.controls["email"].setValidators(Validators.email);
      }
      this.serviceDocument = this.service.serviceDocument;
    });
  }

  save(saveOnly: boolean): void {
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.saveSubscription(saveOnly);
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  saveAndContinue(): void {
    this.save(false);
  }

  saveSubscription(saveOnly: boolean): void {
    this.checkUser(saveOnly);
  }
  saveUsers(saveOnly: boolean): void {
    this.locationSelectedData = this.locationSelectedData.filter(id => id.operation !== "D");
    this.companySelectedData = this.companySelectedData.filter(id => id.operation !== "D");

    if (this.serviceDocument.dataProfile.profileForm.controls["usersLocationId"].value != null) {
      this.locationSelectedData = this.locationSelectedData;
    }
    if (this.rolesSelectedData == null || this.rolesSelectedData.length === 0) {
      this.showCustomRequiredMessageforRole = "Please Enter User Roles";
    } else if (this.companySelectedData.length === 0 && this.locationSelectedData.length === 0) {
      this.showCustomRequiredMessageForOrgInfo = "Please Enter Either Location Or Hierachy Based Access";
    } else if ((this.locationSelectedData === null || this.locationSelectedData.length === 0) && this.companySelectedData.length === 0) {
      this.showCustomRequiredMessageForOrgInfo = "Please select either location permissions or Organization hierachy based access";
    } else {
      if (!this.editMode) {
        this.service.save(this.companySelectedData, this.orgCountrySelectedData, this.regionSelectedData, this.formatSelectedData
          , this.locationSelectedData, this.rolesSelectedData, this.fileName, this.fileDataAsBase64, this.roleswRemovedData).subscribe(() => {
            if (this.service.serviceDocument.result.type === MessageType.success) {
              this.sharedService.saveForm(this.localizationData.users.userssave + " :Your password sent to your registered email id"
                + "@Password Is:" + this.serviceDocument.dataProfile.dataModel.password).subscribe(() => {
                  if (saveOnly) {
                    this.router.navigate(["/Users/List"], { skipLocationChange: true });
                  } else {
                    if (!this.editMode) {
                      this.setUserModel();
                      this.service.newModel(this.model);
                    } else {
                      Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
                      this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.editModel);
                      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
                      this.cleanUp();
                    }
                  }
                });
              if (!this.editMode) {
                this.cleanUp();
              }
            } else {
              this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
            }
          });
      } else {
        this.userUpdatedMsg = "User updated successfully.";
        this.service.save(this.companySelectedData, this.orgCountrySelectedData, this.regionSelectedData, this.formatSelectedData
          , this.locationSelectedData, this.rolesSelectedData, this.fileName, this.fileDataAsBase64, this.roleswRemovedData).subscribe(() => {
            if (this.service.serviceDocument.result.type === MessageType.success) {
              this.sharedService.saveForm(this.userUpdatedMsg).subscribe(() => {
                if (saveOnly) {
                  this.router.navigate(["/Users/List"], { skipLocationChange: true });
                } else {
                  if (!this.editMode) {
                    this.setUserModel();
                    this.service.newModel(this.model);
                  } else {
                    Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
                    this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.editModel);
                    this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
                  }
                }
              });
              if (!this.editMode) {
                this.cleanUp();
              }
            } else {
              this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
            }
          });
      }
      this.showCustomRequiredMessageforRole = null;
      this.showCustomRequiredMessageForOrgInfo = null;
    }
  }

  checkUser(saveOnly: boolean): void {
    if (!this.editMode) {
      this.service.isExistingUsers(this.serviceDocument.dataProfile.profileForm.controls["userName"].value
        , this.serviceDocument.dataProfile.profileForm.controls["email"].value)
        .subscribe((response: boolean) => {
          if (response) {
            this.sharedService.errorForm(this.localizationData.users.userscheck);
          } else {
            this.saveUsers(saveOnly);
          }
        });
    } else {
      this.saveUsers(saveOnly);
    }
  }

  reset(): void {
    if (!this.editMode) {
      this.setUserModel();
      this.service.newModel(this.model);
      this.cleanUp();
    } else {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    }
    this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
  }

  fileChange(event: any): void {
    this.fileUploadErrorMessage = event.ErrorMessage;
    this.fileName = null;

    if (!this.fileUploadErrorMessage) {
      this.fileName = event.filename;
      this.fileDataAsBase64 = event.filedataasbase64;
      this.imageSrc = null;
      this.imageSrc = event.filedata;
      this.fileUploadErrorMessage = null;
    } else {
      this.fileName = null;
      this.fileDataAsBase64 = null;
      this.imageSrc = null;
    }
  }

  changeManager(event: any): void {
    this.serviceDocument.dataProfile.profileForm.controls["mgrEmailAddr"]
      .setValue(this.modelReportingManager.filter(item => item.userId.indexOf(
        this.serviceDocument.dataProfile.profileForm.controls["reportingMgrId"].value) !== -1)[0].email);
  }

  cleanUp(): void {
    this.companySelectedData = [];
    this.orgCountrySelectedData = [];
    this.regionSelectedData = [];
    this.formatSelectedData = [];
    this.locationSelectedData = [];
    this.rolesSelectedData = [];
    this.orgCountryData = [];
    this.regionData = [];
    this.formatData = [];
    this.clearUserImage();
  }

  uploadedFileReference(event: any): void {
    this.userPicture = event.fileReference;
  }

  clearUserImage(): void {
    this.userPicture.nativeElement.value = "";
    this.imageSrc = null;
  }

  selectedChipsCompany($event: any): void {
    let companyModel: CompanyModel;
    companyModel = $event.options;
    if (this.companySelectedData != null && this.companySelectedData.length > 0 && !$event.isRemoved) {
      $event.options.operation = "I";
      this.locationSelectedData = [];
      this.showCustomRequiredMessageforRole = null;
      this.showCustomRequiredMessageForOrgInfo = null;
      this.orgCountryData = this.orgCountryList.filter(item => item.companyId === +$event.options.companyId);
      this.serviceDocument.dataProfile.profileForm.controls["companyId"].setValue(null);
      this.serviceDocument.dataProfile.profileForm.controls["countryCode"].setValue(null);
      this.serviceDocument.dataProfile.profileForm.controls["regionCode"].setValue(null);
      this.serviceDocument.dataProfile.profileForm.controls["formatCode"].setValue(null);
    }

    if ($event.isRemoved) {
      $event.options.operation = "D";
      this.orgCountrySelectedData.filter(item => item.companyId === +$event.options.companyId).forEach(x => x.operation = "D");
      this.regionSelectedData.filter(item => item.companyId === +$event.options.companyId).forEach(x => x.operation = "D");
      this.formatSelectedData.filter(item => item.companyId === +$event.options.companyId).forEach(x => x.operation = "D");
    }
  }

  selectedChipsCountry($event: any): void {
    let countryModel: OrgCountryModel;
    countryModel = $event.options;
    if (this.orgCountrySelectedData != null && this.orgCountrySelectedData.length > 0 && !$event.isRemoved) {
      $event.options.operation = "I";
      this.regionData = this.regionList.filter(item => item.countryCode === $event.options.countryCode
        && item.companyId === +$event.options.companyId);
      this.serviceDocument.dataProfile.profileForm.controls["countryCode"].setValue(null);
      this.serviceDocument.dataProfile.profileForm.controls["regionCode"].setValue(null);
      this.serviceDocument.dataProfile.profileForm.controls["formatCode"].setValue(null);
    }
    if ($event.isRemoved) {
      $event.options.operation = "D";
      this.regionSelectedData.filter(item => item.countryCode === $event.options.countryCode
        && item.companyId === +$event.options.companyId).forEach(x => x.operation = "D");
      this.formatSelectedData.filter(item => item.countryCode === $event.options.countryCode
        && item.companyId === +$event.options.companyId
      ).forEach(x => x.operation = "D");
    }
  }

  selectedChipsRegion($event: any): void {
    let regionModel: RegionModel;
    regionModel = $event.options;
    if (this.regionSelectedData != null && this.regionSelectedData.length > 0 && !$event.isRemoved) {
      $event.options.operation = "I";
      this.formatData = this.formatList.filter(item => item.countryCode === $event.options.countryCode
        && item.companyId === +$event.options.companyId
        && item.regionCode === $event.options.regionCode
      );

      this.serviceDocument.dataProfile.profileForm.controls["regionCode"].setValue(null);
      this.serviceDocument.dataProfile.profileForm.controls["formatCode"].setValue(null);
    }
    if ($event.isRemoved) {
      $event.options.operation = "D";
      this.formatSelectedData.filter(item => item.countryCode === $event.options.countryCode
        && item.companyId === +$event.options.companyId
        && item.regionCode === $event.options.regionCode).forEach(x => x.operation = "D");
    }

  }

  selectedChipsFormat($event: any): void {
    let formatModel: FormatModel;
    formatModel = $event.options;
    if (this.formatSelectedData != null && this.formatSelectedData.length > 0 && !$event.isRemoved) {
      $event.options.operation = "I";
      this.serviceDocument.dataProfile.profileForm.controls["formatCode"].setValue(null);
    }
    if ($event.isRemoved) {
      $event.options.operation = "D";
    }
  }

  selectedChipsLocation($event: any): void {
    let locationModel: LocationModel;
    locationModel = $event.options;
    if (this.locationSelectedData != null && this.locationSelectedData.length > 0 && !$event.isRemoved) {
      $event.options.operation = "I";
      this.companySelectedData = [];
      this.orgCountrySelectedData = [];
      this.regionSelectedData = [];
      this.formatSelectedData = [];
      this.showCustomRequiredMessageforRole = null;
      this.showCustomRequiredMessageForOrgInfo = null;
      this.serviceDocument.dataProfile.profileForm.controls["usersLocationId"].setValue(null);
    }
    if ($event.isRemoved) {
      $event.options.operation = "D";
    }
  }

  selectedChipsRole($event: any): void {
    if (this.rolesSelectedData != null && this.rolesSelectedData.length > 0 && !$event.isRemoved) {
      this.showCustomRequiredMessageforRole = null;
      this.showCustomRequiredMessageForOrgInfo = null;
      this.serviceDocument.dataProfile.profileForm.controls["usersRoleId"].setValue(null);
    }
    if ($event.isRemoved) {
      this.roleswRemovedData.push($event.options);
    }
  }

  showFileDilaog(event: any): void {
    this.userPicture.nativeElement.click();
  }

  private loadUsersDomainDetails(): void {
    this.indicatorData = Object.assign([], this.service.serviceDocument.domainData["indicator"]);
    this.companyData = Object.assign([], this.service.serviceDocument.domainData["company"]);
    this.orgCountryList = Object.assign([], this.service.serviceDocument.domainData["orgcountry"]);
    this.regionList = Object.assign([], this.service.serviceDocument.domainData["region"]);
    this.formatList = Object.assign([], this.service.serviceDocument.domainData["format"]);
    this.locationData = Object.assign([], this.service.serviceDocument.domainData["location"]);
    this.rolesData = Object.assign([], this.service.serviceDocument.domainData["roles"]);
    this.domainDtlData = Object.assign([], this.service.serviceDocument.domainData["status"]);
    this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
    this.defaultStatus = this.domainDtlData.filter(itm => itm.requiredInd === "1")[0].code;
    this.modelReportingManager = Object.assign([], this.service.serviceDocument.domainData["users"]);
    this.setOperation(0, this.companyData);
    this.setOperation(0, this.orgCountryData);
    this.setOperation(0, this.regionData);
    this.setOperation(0, this.formatData);
    this.setOperation(0, this.locationData);
  }

  private setUserModel(): void {
    this.model = new UserModel();
    this.model = {
      userId: null, reportingMgrId: null,
      reportingMgrName: null, mgrEmailAddr: null, companyId: null, countryCode: null, formatCode: null,
      regionCode: null, usersLocationId: null, userPicPath: null, inputValue: null, id: null, name: null,
      userName: null, email: null, usersRoleId: null, rolesList: [], rolesRemovedData: [], status: this.defaultStatus, operation: "I",
      tableName: null, fileName: null, fileDataAsBase64: null, password: null,
      userOrgHierarchyModel: null
    };
  }

  private BuildOrgHierarchyForEdit(): void {
    let modelData: any = {};
    let userOrgHierarchyModel: UserOrgHierarchyModel = null;
    for (let index: number = 0; index < this.service.serviceDocument.dataProfile.dataModel.userOrgHierarchyModel.length; index++) {
      userOrgHierarchyModel = this.service.serviceDocument.dataProfile.dataModel.userOrgHierarchyModel[index];
      modelData = {};
      modelData.id = userOrgHierarchyModel.id;
      modelData.userCompanyModel = userOrgHierarchyModel.userCompanyModel;
      modelData.userCountryModel = userOrgHierarchyModel.userCountryModel;
      modelData.userRegionModel = userOrgHierarchyModel.userRegionModel;
      modelData.userFormatModel = userOrgHierarchyModel.userFormatModel;
      modelData.userLocationModel = userOrgHierarchyModel.userLocationModel;
      if (userOrgHierarchyModel) {
        if (userOrgHierarchyModel.userCompanyModel && !this.isCompanyExists(modelData)) {
          this.setOperation(userOrgHierarchyModel.id, userOrgHierarchyModel.userCompanyModel);
          this.companySelectedData.push(userOrgHierarchyModel.userCompanyModel);
        }

        if (userOrgHierarchyModel.userCountryModel && !this.isCountryExists(modelData)) {
          this.setOperation(userOrgHierarchyModel.id, userOrgHierarchyModel.userCountryModel);
          this.orgCountrySelectedData.push(userOrgHierarchyModel.userCountryModel);
        }

        if (userOrgHierarchyModel.userRegionModel && !this.isRegionExists(modelData)) {
          this.setOperation(userOrgHierarchyModel.id, userOrgHierarchyModel.userRegionModel);
          this.regionSelectedData.push(userOrgHierarchyModel.userRegionModel);
        }

        if (userOrgHierarchyModel.userFormatModel && !this.isFormatExists(modelData)) {
          this.setOperation(userOrgHierarchyModel.id, userOrgHierarchyModel.userFormatModel);
          this.formatSelectedData.push(userOrgHierarchyModel.userFormatModel);
        }
        if (userOrgHierarchyModel.userLocationModel && !this.isLocation(modelData)) {
          this.setOperation(userOrgHierarchyModel.id, userOrgHierarchyModel.userLocationModel);
          this.locationSelectedData.push(userOrgHierarchyModel.userLocationModel);
        }

      }
    }
  }

  private setOperation(id: number, model: any): void {
    if (id > 0) {
      model.operation = "U";
    } else {
      model.operation = "I";
    }
  }
  private isCompanyExists(modelData: any): boolean {
    if (!this.companySelectedData || (this.companySelectedData && this.companySelectedData.length === 0)) {
      return false;
    } else if (this.companySelectedData &&
      this.companySelectedData.length > 0 &&
      this.companySelectedData.filter(c => c.company === modelData.userCompanyModel.companyId).length === 0) {
      return false;
    }
    return true;
  }

  private isCountryExists(modelData: any): boolean {
    if (!this.orgCountrySelectedData || (this.orgCountrySelectedData && this.orgCountrySelectedData.length === 0)) {
      return false;
    } else if (this.orgCountrySelectedData &&
      this.orgCountrySelectedData.length > 0 &&
      this.orgCountrySelectedData.filter(c => c.companyId === modelData.userCompanyModel.companyId
        && c.countryCode === modelData.userCountryModel.countryCode).length === 0) {
      return false;
    }
    return true;
  }

  private isRegionExists(modelData: any): boolean {
    if (!this.regionSelectedData || (this.regionSelectedData && this.regionSelectedData.length === 0)) {
      return false;
    } else if (this.regionSelectedData &&
      this.regionSelectedData.length > 0 &&
      this.regionSelectedData.filter(c => c.companyId === +modelData.userCompanyModel.companyId
        && c.countryCode === modelData.userCountryModel.countryCode
        && c.regionCode === modelData.userRegionModel.regionCode
      ).length === 0) {
      return false;
    }
    return true;
  }

  private isFormatExists(modelData: any): boolean {
    if (!this.formatSelectedData || (this.formatSelectedData && this.formatSelectedData.length === 0)) {
      return false;
    } else if (this.formatSelectedData &&
      this.formatSelectedData.length > 0 &&
      this.formatSelectedData.filter(c => c.companyId === +modelData.userCompanyModel.companyId
        && c.countryCode === modelData.userCountryModel.countryCode
        && c.regionCode === modelData.userRegionModel.regionCode
        && c.formatCode === modelData.userFormatModel.formatCode
      ).length === 0) {
      return false;
    }
    return true;
  }
  private isLocation(modelData: any): boolean {

    if (!this.locationSelectedData || (this.locationSelectedData && this.locationSelectedData.length === 0)) {
      return false;
    } else if (this.locationSelectedData &&
      this.locationSelectedData.length > 0 &&
      this.locationSelectedData.filter(c => c.companyId === +modelData.userLocationModel.companyId
        && c.countryCode === modelData.userLocationModel.countryCode
        && c.regionCode === modelData.userLocationModel.regionCode
        && c.formatCode === modelData.userLocationModel.formatCode &&
        c.locationId === modelData.userLocationModel.locationId
      ).length === 0) {
      return false;
    }
    return true;
  }

  ValidateOption(event: any): boolean {
    if (event && event.option) {
      return event.option.operation !== "D";
    } else {
      return true;
    }
  }
}
