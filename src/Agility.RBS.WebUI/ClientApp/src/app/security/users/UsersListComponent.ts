import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup, FormBuilder, FormControl, Validators, ValidatorFn, AbstractControl } from "@angular/forms";
import { SharedService } from "../../Common/SharedService";
import { GridOptions } from "ag-grid-community";
import { GridExportModel } from "../../Common/grid/GridExportModel";
import { InfoComponent } from "../../Common/InfoComponent";
import { GridEditComponent } from "../../Common/grid/GridEditComponent";
import { UsersService } from "./UsersService";
import { UserModel } from "./UserModel";
import { FrameWorkRoleModel } from "./FrameWorkRoleModel";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { CompanyDomainModel } from "../../Common/DomainData/CompanyDomainModel";
import { OrgCountryDomainModel } from "../../Common/DomainData/OrgCountryDomainModel";
import { RegionDomainModel } from "../../Common/DomainData/RegionDomainModel";
import { FormatDomainModel } from "../../Common/DomainData/FormatDomainModel";
import { LocationDomainModel } from "../../Common/DomainData/LocationDomainModel";

@Component({
  selector: "users-list",
  templateUrl: "./UsersListComponent.html",
  styleUrls: ['./UsersListComponent.scss']
})

export class UsersListComponent implements OnInit {
  public serviceDocument: ServiceDocument<UserModel>;
  public gridOptions: GridOptions;
  PageTitle = "Users";
  redirectUrl = "Users";
  componentName: any;
  domainDtlData: DomainDetailModel[];
  companyData: CompanyDomainModel[];
  orgCountryData: OrgCountryDomainModel[] = [];
  orgCountryList: OrgCountryDomainModel[];
  regionData: RegionDomainModel[] = [];
  regionList: RegionDomainModel[];
  formatData: FormatDomainModel[] = [];
  formatList: FormatDomainModel[];
  locationData: LocationDomainModel[];
  rolesData: FrameWorkRoleModel[];

  localizationData: any;
  indicatorData: DomainDetailModel[];
  columns: any[];
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  userId: string;
  defaultStatus: string;

  model: UserModel = {
    userId: null, reportingMgrId: null,
    reportingMgrName: null, mgrEmailAddr: null, companyId: null, countryCode: null, formatCode: null,
    regionCode: null, usersLocationId: null, userPicPath: null, inputValue: null, id: null, name: null,
    userName: null, email: null, usersRoleId: null, rolesList: [], rolesRemovedData: [], status: null, operation: null,
    tableName: null, fileName: null, fileDataAsBase64: null, password: null
  };

  formControlLocation: FormControl = new FormControl();
  constructor(public service: UsersService, private route: ActivatedRoute, private sharedService: SharedService
    , private router: Router) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "Name", field: "userName", tooltipField: "userName" },
      { headerName: "Email", field: "email", tooltipField: "email" },
      { headerName: "Roles", field: "usersRoleId", tooltipField: "usersRoleId", width: 100 },
      { headerName: "Reporting Manager", field: "reportingMgrName", tooltipField: "reportingMgrName", width: 100 },
      { headerName: "Status", field: "statusDesc", tooltipField: "statusDesc", width: 60 },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 60
      }
    ];
    this.loadUsersDomainData();
    // delete this.sharedService.domainData;
    this.sharedService.domainData = {
      status: this.domainDtlData, userLocData: this.locationData, userRolesData: this.rolesData
      , company: this.companyData
    };
    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.orgCountryData = this.orgCountryList.filter(item => item.companyId === this.service.serviceDocument.dataProfile.dataModel.companyId);
      this.regionData = this.regionList.filter(item => item.countryCode === this.service.serviceDocument.dataProfile.dataModel.countryCode
        && item.companyId === this.service.serviceDocument.dataProfile.dataModel.companyId);
      this.sharedService.domainData["country"] = this.orgCountryData;
      this.sharedService.domainData["region"] = this.regionData;
      this.sharedService.domainData["formatData"] = this.formatData;
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);

    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: any): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }

  private loadUsersDomainData(): void {
    this.indicatorData = Object.assign([], this.service.serviceDocument.domainData["indicator"]);
    this.companyData = Object.assign([], this.service.serviceDocument.domainData["company"]);
    this.orgCountryList = Object.assign([], this.service.serviceDocument.domainData["orgcountry"]);
    this.regionList = Object.assign([], this.service.serviceDocument.domainData["region"]);
    this.formatList = Object.assign([], this.service.serviceDocument.domainData["format"]);
    this.locationData = Object.assign([], this.service.serviceDocument.domainData["location"]);
    this.domainDtlData = Object.assign([], this.service.serviceDocument.domainData["status"]);
    this.rolesData = Object.assign([], this.service.serviceDocument.domainData["roles"]);
    this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
    this.cleanUp();
  }

  cleanUp(): void {
    this.orgCountryData = [];
    this.regionData = [];
    this.formatData = [];
  }

  changeCompany(): void {
    this.serviceDocument.dataProfile.profileForm.controls["countryCode"].setValue(null);
    this.serviceDocument.dataProfile.profileForm.controls["regionCode"].setValue(null);
    this.serviceDocument.dataProfile.profileForm.controls["formatCode"].setValue(null);
    this.orgCountryData = this.orgCountryList
      .filter(item => item.companyId === +this.serviceDocument.dataProfile.profileForm.controls["companyId"].value);
    this.sharedService.domainData["country"] = this.orgCountryData;
  }

  changeCountry(): void {
    this.serviceDocument.dataProfile.profileForm.controls["regionCode"].setValue(null);
    this.serviceDocument.dataProfile.profileForm.controls["formatCode"].setValue(null);
    this.regionData = this.regionList.filter(item => item.countryCode === this.serviceDocument.dataProfile.profileForm.controls["countryCode"].value
      && item.companyId === +this.serviceDocument.dataProfile.profileForm.controls["companyId"].value);
    this.sharedService.domainData["region"] = this.regionData;
  }

  changeRegion(): void {
    this.serviceDocument.dataProfile.profileForm.controls["formatCode"].setValue(null);
    this.formatData = this.formatList.filter(item => item.countryCode === this.serviceDocument.dataProfile.profileForm.controls["countryCode"].value
      && item.companyId === +this.serviceDocument.dataProfile.profileForm.controls["companyId"].value
      && item.regionCode === this.serviceDocument.dataProfile.profileForm.controls["regionCode"].value);
    this.sharedService.domainData["formatData"] = this.formatData;
  }
}
