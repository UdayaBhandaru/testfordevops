﻿import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { UserModel } from "./UserModel";
import { FrameWorkRoleModel } from "./FrameWorkRoleModel";
import { UserOrgHierarchyModel } from "./UserOrgHierarchyModel";
import { CompanyModel } from "../../organizationhierarchy/company/CompanyModel";
import { OrgCountryModel } from "../../organizationhierarchy/orgcountry/OrgCountryModel";
import { RegionModel } from "../../organizationhierarchy/region/RegionModel";
import { FormatModel } from "../../organizationhierarchy/format/FormatModel";
import { LocationModel } from "../../organizationhierarchy/location/LocationModel";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";

@Injectable()
export class UsersService {
    companyModel: CompanyModel[];
    orgCountryModel: OrgCountryModel[];
    regionModel: RegionModel[];
    formatModel: FormatModel[];
    locationtModel: LocationModel[];
    rolesModel: FrameWorkRoleModel[];
    serviceDocument: ServiceDocument<UserModel> = new ServiceDocument<UserModel>();
    constructor(private httpHelperService: HttpHelperService) { }
    newModel(model: UserModel): ServiceDocument<UserModel> {
        return this.serviceDocument.newModel(model);
    }

    search(): Observable<ServiceDocument<UserModel>> {
        return this.serviceDocument.search("/api/Users/Search");
    }
    save(companyModel: CompanyModel[], orgCountryModel: OrgCountryModel[], regionModel: RegionModel[], formatModel: FormatModel[]
        , locationtModel: LocationModel[], rolesModel: FrameWorkRoleModel[], fileName: string, fileDataAsBase64: string
        , roleswRemovedData: FrameWorkRoleModel[]
    ): Observable<ServiceDocument<UserModel>> {
        return this.serviceDocument.save("/api/Users/Save", true, () => this.updateUserModelWithCustomProperties(companyModel, orgCountryModel
            , regionModel, formatModel, locationtModel, rolesModel, fileName, fileDataAsBase64, roleswRemovedData));
    }

    list(): Observable<ServiceDocument<UserModel>> {
        return this.serviceDocument.list("/api/Users/List");
    }
    addEdit(): Observable<ServiceDocument<UserModel>> {
        return this.serviceDocument.list("/api/Users/New");
    }

    isExistingUsers(userName: string, email: string): Observable<boolean> {
        return this.httpHelperService.get("/api/Users/IsExistingUsers", new HttpParams().set("userName", userName)
            .set("email", email));
    }

    isExistingUserId(email: string): Observable<boolean> {
        return this.httpHelperService.get("/api/Users/IsExistingUserId", new HttpParams().set("email", email));
    }

    updateUserModelWithCustomProperties(companyModel: CompanyModel[], orgCountryModel: OrgCountryModel[], regionModel: RegionModel[]
        , formatModel: FormatModel[], locationtModel: LocationModel[], rolesModel: FrameWorkRoleModel[], fileName: string
        , fileDataAsBase64: string
        , roleswRemovedData: FrameWorkRoleModel[]): any {
        this.setUsersCustomModelForSave(companyModel, orgCountryModel, regionModel, formatModel, locationtModel, rolesModel, roleswRemovedData);
        this.serviceDocument.dataProfile.dataModel.fileName = fileName;
        this.serviceDocument.dataProfile.dataModel.fileDataAsBase64 = fileDataAsBase64;
    }

    private setUsersCustomModelForSave(companyModel: CompanyModel[], orgCountryModel: OrgCountryModel[], regionModel: RegionModel[],
        formatModel: FormatModel[], locationtModel: LocationModel[], rolesModel: FrameWorkRoleModel[],
        roleswRemovedData: FrameWorkRoleModel[]): void {
        let modelData: any = {};
        this.serviceDocument.dataProfile.dataModel.usersLocationId = null;
        this.serviceDocument.dataProfile.dataModel.companyId = null;
        this.serviceDocument.dataProfile.dataModel.countryCode = null;
        this.serviceDocument.dataProfile.dataModel.regionCode = null;
        this.serviceDocument.dataProfile.dataModel.formatCode = null;
        this.serviceDocument.dataProfile.dataModel.usersRoleId = null;
        this.serviceDocument.dataProfile.dataModel.rolesList = rolesModel;
        this.serviceDocument.dataProfile.dataModel.rolesRemovedData = roleswRemovedData;

        modelData.companyModel = companyModel;
        modelData.orgCountryModel = orgCountryModel;
        modelData.regionModel = regionModel;
        modelData.formatModel = formatModel;
        modelData.locationtModel = locationtModel;

        if (modelData.companyModel != null && modelData.companyModel.length > 0) {
            this.serviceDocument.dataProfile.dataModel.userOrgHierarchyModel = this.buildUserOrgHierarchy(modelData);
        } else {
            this.serviceDocument.dataProfile.dataModel.userOrgHierarchyModel = this.buildUserOrgHierarchyLocation(modelData);
        }
    }

    private buildUserOrgHierarchyLocation(modelData: any): UserOrgHierarchyModel[] {
        let userOrgHierarchyModel: UserOrgHierarchyModel[] = [];
        let locationtModel: LocationModel;
        let orgHierarchyDataLocation: any;
        let uh: UserOrgHierarchyModel = null;
        if (modelData.locationtModel != null && modelData.locationtModel.length > 0) {
            for (let cmIndex: number = 0; cmIndex < modelData.locationtModel.length; cmIndex++) {
                locationtModel = modelData.locationtModel[cmIndex];
                uh = new UserOrgHierarchyModel();
                uh.userLocationModel = locationtModel;
                userOrgHierarchyModel.push(uh);
            }
        }
        return userOrgHierarchyModel;
    }

    private processLocationModel(orgHierarchyDataLocation: any, userOrgHierarchyModel: UserOrgHierarchyModel[]): void {
        let uh: UserOrgHierarchyModel = null;
        let orgHierarchyDataLoc: any;
        orgHierarchyDataLoc = {};
        orgHierarchyDataLoc.locationtModel = orgHierarchyDataLocation.locationtModel;
        uh = this.createUserOrgHierarchy(orgHierarchyDataLoc);
        userOrgHierarchyModel.push(uh);
    }

    private createUserOrgHierarchy(orgHierarchyData: any): UserOrgHierarchyModel {
        let uh: UserOrgHierarchyModel = new UserOrgHierarchyModel();
        uh.userId = this.serviceDocument.dataProfile.dataModel.userId;
        uh.userCompanyModel = orgHierarchyData.companyModel;
        uh.userCountryModel = orgHierarchyData.countryModel;
        uh.userRegionModel = orgHierarchyData.regionModel;
        uh.userFormatModel = orgHierarchyData.formatModel;
        uh.userLocationModel = orgHierarchyData.locationModel;
        return uh;
    }

    private buildUserOrgHierarchy(modelData: any): UserOrgHierarchyModel[] {
        let userOrgHierarchyModel: UserOrgHierarchyModel[] = [];
        let companyModel: CompanyModel;
        let orgHierarchyDataCompany: any;
        if (modelData.companyModel != null && modelData.companyModel.length > 0) {
            for (let cmIndex: number = 0; cmIndex < modelData.companyModel.length; cmIndex++) {
                companyModel = modelData.companyModel[cmIndex];
                orgHierarchyDataCompany = {};
                orgHierarchyDataCompany.companyModel = companyModel;
                orgHierarchyDataCompany.modelData = modelData;
                this.processCountryModel(orgHierarchyDataCompany, userOrgHierarchyModel);
            }
        }
        return userOrgHierarchyModel;
    }

    private processCountryModel(orgHierarchyDataCompany: any, userOrgHierarchyModel: UserOrgHierarchyModel[]): void {
        let uh: UserOrgHierarchyModel = null;
        let orgHierarchyDataCountry: any;
        let userCtry: OrgCountryModel[] = orgHierarchyDataCompany.modelData.orgCountryModel ?
            orgHierarchyDataCompany.modelData.orgCountryModel.
                filter(ctry => ctry.companyId === orgHierarchyDataCompany.companyModel.companyId) : null;
        if (userCtry && userCtry.length > 0) {
            for (let ctIndex: number = 0; ctIndex < userCtry.length; ctIndex++) {
                orgHierarchyDataCountry = {};
                orgHierarchyDataCountry.companyModel = orgHierarchyDataCompany.companyModel;
                orgHierarchyDataCountry.countryModel = userCtry[ctIndex];
                orgHierarchyDataCountry.modelData = orgHierarchyDataCompany.modelData;
                this.processRegionModel(orgHierarchyDataCountry, userOrgHierarchyModel);
            }
        } else {
            orgHierarchyDataCountry = {};
            orgHierarchyDataCountry.companyModel = orgHierarchyDataCompany.companyModel;
            uh = this.createUserOrgHierarchy(orgHierarchyDataCountry);
            userOrgHierarchyModel.push(uh);
        }

    }

    private processRegionModel(orgHierarchyDataCountry: any, userOrgHierarchyModel: UserOrgHierarchyModel[]): void {
        let uh: UserOrgHierarchyModel = null;
        let regionModel: RegionModel;
        let orgHierarchyDataRegion: any;
        let userReg: RegionModel[] = orgHierarchyDataCountry.modelData.regionModel ?
            orgHierarchyDataCountry.modelData.regionModel.
                filter(reg => reg.companyId === orgHierarchyDataCountry.companyModel.companyId
                    && reg.countryCode === orgHierarchyDataCountry.countryModel.countryCode) : null;
        if (userReg && userReg.length > 0) {
            for (let regIndex: number = 0; regIndex < userReg.length; regIndex++) {
                regionModel = userReg[regIndex];
                orgHierarchyDataRegion = {};
                orgHierarchyDataRegion.companyModel = orgHierarchyDataCountry.companyModel;
                orgHierarchyDataRegion.countryModel = orgHierarchyDataCountry.countryModel;
                orgHierarchyDataRegion.regionModel = regionModel;
                orgHierarchyDataRegion.modelData = orgHierarchyDataCountry.modelData;
                this.processFormatModel(orgHierarchyDataRegion, userOrgHierarchyModel);
            }
        } else {
            orgHierarchyDataRegion = {};
            orgHierarchyDataRegion.companyModel = orgHierarchyDataCountry.companyModel;
            orgHierarchyDataRegion.countryModel = orgHierarchyDataCountry.countryModel;
            uh = this.createUserOrgHierarchy(orgHierarchyDataRegion);
            userOrgHierarchyModel.push(uh);
        }
    }

    private processFormatModel(orgHierarchyDataRegion: any, userOrgHierarchyModel: UserOrgHierarchyModel[]): void {
        let uh: UserOrgHierarchyModel = null;
        let formatModel: FormatModel;
        let orgHierarchyDataFormat: any;
        let userFormat: FormatModel[] = orgHierarchyDataRegion.modelData.formatModel ?
            orgHierarchyDataRegion.modelData.formatModel.
                filter(reg => reg.companyId === orgHierarchyDataRegion.companyModel.companyId
                    && reg.countryCode === orgHierarchyDataRegion.countryModel.countryCode
                    && reg.regionCode === orgHierarchyDataRegion.regionModel.regionCode) : null;
        if (userFormat && userFormat.length > 0) {
            for (let fIndex: number = 0; fIndex < userFormat.length; fIndex++) {
                formatModel = userFormat[fIndex];
                orgHierarchyDataFormat = {};
                orgHierarchyDataFormat.companyModel = orgHierarchyDataRegion.companyModel;
                orgHierarchyDataFormat.countryModel = orgHierarchyDataRegion.countryModel;
                orgHierarchyDataFormat.regionModel = orgHierarchyDataRegion.regionModel;
                orgHierarchyDataFormat.formatModel = formatModel;
                uh = this.createUserOrgHierarchy(orgHierarchyDataFormat);
                userOrgHierarchyModel.push(uh);
            }
        } else {
            orgHierarchyDataFormat = {};
            orgHierarchyDataFormat.companyModel = orgHierarchyDataRegion.companyModel;
            orgHierarchyDataFormat.countryModel = orgHierarchyDataRegion.countryModel;
            orgHierarchyDataFormat.regionModel = orgHierarchyDataRegion.regionModel;
            uh = this.createUserOrgHierarchy(orgHierarchyDataFormat);
            userOrgHierarchyModel.push(uh);
        }
    }

    cleanUp(): void {
        this.orgCountryModel = null;
        this.regionModel = null;
        this.formatModel = null;
        this.locationtModel = null;
        this.rolesModel = null;
        this.companyModel = null;
    }
}