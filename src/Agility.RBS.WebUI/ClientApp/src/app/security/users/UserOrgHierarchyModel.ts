﻿import { CompanyModel } from "../../organizationhierarchy/company/CompanyModel";
import { OrgCountryModel } from "../../organizationhierarchy/orgcountry/OrgCountryModel";
import { RegionModel } from "../../organizationhierarchy/region/RegionModel";
import { FormatModel } from "../../organizationhierarchy/format/FormatModel";
import { LocationModel } from "../../organizationhierarchy/location/LocationModel";

export class UserOrgHierarchyModel {
    id?: number;
    userId: string;
    userCompanyModel?: CompanyModel;
    userCountryModel?: OrgCountryModel;
    userRegionModel?: RegionModel;
    userFormatModel?: FormatModel;
    userLocationModel?: LocationModel;

    constructor() {
        this.userCompanyModel = new CompanyModel();
        this.userCountryModel = new OrgCountryModel();
        this.userRegionModel = new RegionModel();
        this.userFormatModel = new FormatModel();
        this.userLocationModel = new LocationModel();
    }
}