﻿export class UserRolesModel {
    id: string;
    name: string;
    userId: string;
    RoleName: string;
    ProgramPhase: string;
    ProgramMessage: string;
    Error: string;
    createdUpdatedBy?: string;
    createdUpdatedDate?: Date;
    lastUpdatedBy?: string;
    lastUpdatedDate?: Date;
    status?: string;
    operation?: string;
    tableName?: string;
}