﻿import { ClaimModel } from "@agility/frameworkcore";
export class RoleModel {
    id: string;
    name: string;
    roleType: number;
    roleTypeText: string;
    claims: ClaimModel[];
}