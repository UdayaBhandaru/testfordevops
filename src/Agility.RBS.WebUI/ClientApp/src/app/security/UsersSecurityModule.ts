﻿import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { AgGridSharedModule } from "../AgGridSharedModule";
import { TitleSharedModule } from "../TitleSharedModule";
import { UsersResolver, UsersListResolver } from "./UsersSecurityResolver";
import { UsersService } from "./users/UsersService";
import { UsersListComponent } from "./users/UsersListComponent";
import { UsersComponent } from "./users/UsersComponent";
import { UsersSecurityRoutingModule } from "./UsersSecurityRoutingModule";
import { RbsSharedModule } from "../RbsSharedModule";

@NgModule({
    imports: [
        FrameworkCoreModule,
        UsersSecurityRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        UsersListComponent,
        UsersComponent
    ],
    providers: [

        UsersService,
        UsersResolver,
        UsersListResolver

    ]
})
export class UsersSecurityModule {
    constructor() {
    LicenseManager.setLicenseKey("ag-Grid_Evaluation_License_Not_For_Production_1Devs14_September_2017__MTUwNTM0MzYwMDAwMA==0701904853ad9b97653c0b5c434d81b0");
    }
}
