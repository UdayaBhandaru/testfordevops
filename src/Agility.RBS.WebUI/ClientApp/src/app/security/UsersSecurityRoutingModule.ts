﻿import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { UsersComponent } from "./users/UsersComponent";
import { UsersListComponent } from "./users/UsersListComponent";
import { UsersResolver, UsersListResolver } from "./UsersSecurityResolver";
import { UsersService } from "./users/UsersService";
const UtilityRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: UsersListComponent,
                resolve:
                {
                    serviceDocument: UsersListResolver
                }
            },
            {
                path: "New",
                component: UsersComponent,
                resolve:
                {
                    serviceDocument: UsersResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(UtilityRoutes)
    ]
})
export class UsersSecurityRoutingModule { }
