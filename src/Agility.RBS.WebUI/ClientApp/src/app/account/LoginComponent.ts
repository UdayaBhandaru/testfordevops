import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, FormControl, Validators, ValidatorFn, AbstractControl } from "@angular/forms";
import { Router } from "@angular/router";
import { LoginViewModel } from "./LoginViewModel";
import { FxContext, CommonService, ServiceDocument, UserProfile, IdentityMenuModel, MessageType } from "@agility/frameworkcore";
import { AccountService } from "./AccountService";
import { KeepAliveSessionService } from "../Common/KeepAliveSessionService";
import { SharedService } from "../Common/SharedService";
// import { CommonService } from "framework/CommonService";
// import { UserProfile } from "account/UserProfile";
// import { IdentityMenuModel } from "account/IdentityMenuModel";
// import { ServiceDocument } from "framework/servicedocument/ServiceDocument";
// import { ServiceDocumentResult } from "framework/servicedocument/ServiceDocumentResult"
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { PrivacyComponent } from "./PrivacyComponent";
import { TermsAndConditionsComponent } from "./TermsAndConditionsComponent";
import { UserDetailModel } from "../account/UserDetailModel";

@Component({

    selector: "login",
    templateUrl: "./LoginComponent.html",
    styleUrls: ['./LoginComponent.scss']

})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    dataModel: LoginViewModel;
    serviceDocument: ServiceDocument<UserProfile>;
    constructor(private fxContext: FxContext
        , private formBuilder: FormBuilder
        , private router: Router
        , private sharedService: SharedService
        , private commonService: CommonService
        , private service: AccountService
        , private keepAliveSessionService: KeepAliveSessionService
        , public dialog: MatDialog) {
        fxContext.IsAuthenticated = false;
        this.keepAliveSessionService.stop();
    }

    ngOnInit(): void {
        this.dataModel = new LoginViewModel();
        this.dataModel.email = "";
        this.dataModel.password = "";
        this.dataModel.rememberMe = false;
        this.loginForm = this.commonService.getFormGroup(this.dataModel, 0);
        // adding multple validators including custom validators."
        this.loginForm.controls["email"].setValidators([Validators.required, this.sharedService.emailValidator("Invalid Email Address")]);
        this.loginForm.controls["password"].setValidators([Validators.required]);
    }

    public validate(): any {
        this.service.login(this.loginForm.value).subscribe((response) => {
            if (response.result !== null && response.result.type !== undefined && response.result.type === MessageType.success) {
                if (this.service.serviceDocument.result.type !== MessageType.success) {
                    this.commonService.showAlert(this.service.serviceDocument.result.message, "", 5000);
                } else if (this.service.serviceDocument.result.type === MessageType.success) {
                    this.fxContext.IsAuthenticated = true;
                    this.fxContext.userProfile = this.service.serviceDocument.userProfile;
                    this.sharedService.userRoles = this.service.serviceDocument.userProfile.userRoles;
                    this.sharedService.loggedinUser = this.service.serviceDocument.userProfile.name;
                    if (this.fxContext.userProfile) {
                        //this.service.getUserDetails(this.service.serviceDocument.userProfile.userId).subscribe((response: UserDetailModel) => {
                        //  this.sharedService.loggedinUserName = "";// response.userName;
                        //  this.sharedService.loggedinUserPic = ""; //response.userPicPath;
                        //});
                        this.sharedService.menus = this.fxContext.userProfile.menus.filter(itm => itm.groupMenu === 1);
                        // this.addMenuItemsToFavourites();
                    }

                    this.keepAliveSessionService.start();
                    this.router.navigate(["/Inbox"]);
                } else {
                    this.commonService.showAlert(this.service.serviceDocument.result.message, "Please try again", 5000);
                }
            } else if (response["message"] !== null) {
                this.commonService.showAlert(response["message"], "Please try again", 5000);
            }
        }, () => {
            this.commonService.showAlert("Login Failed. Please try again.");
        });
    }

    open(): void {
        this.router.navigate(["/Account/ForgotPassword"]);
    }

    openDialog(): void {
        let dialogRef: MatDialogRef<PrivacyComponent, any> = this.dialog.open(PrivacyComponent, {
            width: "80%",
            height: "80%"
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log("The dialog was closed");

        });
    }
    openDialogTerms(): void {
        let dialogRef: MatDialogRef<TermsAndConditionsComponent, any> = this.dialog.open(TermsAndConditionsComponent, {
            width: "80%",
            height: "80%"
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log("The dialog was closed");

        });
    }

    // addMenuItemsToFavourites(): void {
    //    //Dynamically adding the favorite menus
    //    this.fm = { userId: null, favouritePagePath: null, favouritePage: null }
    //    this.favService.newModel(this.fm);
    //    this.favService.search().subscribe((response: any) => {

    //        this.favouritesData = this.favService.serviceDocument.dataProfile.dataList;
    //        console.log(this.favouritesData);
    //        if (this.favouritesData != null && this.favouritesData.length > 0) {
    //            var imFav: any = this.menus.filter(itm => itm.menuText == "Favourite");
    //            imFav.items = [];
    //            for (var i = 0; i < this.favouritesData.length; i++) {
    //                var favData = this.favouritesData[i];
    //                var im: any = this.menus.filter(itm => (itm.menuText == favData.favouritePagePath
    // && itm.url.contains(favData.favouritePagePath)));
    //                var cm = { menuId: im.menuId, claimId: im.claimId,
    // menuText: im.menuText, parentMenuId: imFav.menuId, parentMenuText: "Favourite"
    // , groupMenuInd: false, displayOrder: i, url: im.url, iconCss: im.iconCss, deletedInd: im.deletedInd
    // };
    //                imFav.items.push(cm);
    //            }
    //        }

    //    });
    // }
}
