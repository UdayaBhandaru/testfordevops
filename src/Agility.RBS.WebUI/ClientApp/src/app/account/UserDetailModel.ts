﻿export class UserDetailModel {
    userName: string;
    userId: string;
    userPicPath: string;
}