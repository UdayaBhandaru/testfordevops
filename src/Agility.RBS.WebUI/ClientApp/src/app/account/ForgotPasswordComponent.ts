import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, FormControl, Validators, ValidatorFn, AbstractControl } from "@angular/forms";
import { Router, ActivatedRoute, Params } from "@angular/router";
import { LoginViewModel } from "./LoginViewModel";
import { FxContext, CommonService, ServiceDocument, UserProfile, IdentityMenuModel, MessageType } from "@agility/frameworkcore";
import { AccountService } from "./AccountService";
import { ForgotPasswordModel } from "./ForgotPasswordModel";
import { PasswordValidation } from "./PasswordValidation";
import { SharedService } from "../Common/SharedService";
import { Subscription } from "rxjs";
@Component({

    selector: "forgotPassword",
  templateUrl: "./ForgotPasswordComponent.html",
  styleUrls: ['./ForgotPasswordComponent.scss']
})
export class ForgotPasswordComponent implements OnInit {
    forgotPasswordForm: FormGroup;
    dataModel: ForgotPasswordModel;
    serviceDocument: ServiceDocument<UserProfile>;
    errorMessage: string;
    pwdPattern: string;
    isValidFormSubmitted = null;
    showPasswordDiv: boolean;
    resetPassword: boolean;
    email: any;
    showResetBtn: boolean;
    constructor(private fxContext: FxContext, private formBuilder: FormBuilder,
        private router: Router, private activatedRoute: ActivatedRoute,
        private accountService: AccountService,
        private commonService: CommonService,
        private route: ActivatedRoute, private sharedService: SharedService) {
        // xContext.IsAuthenticated = false;
    }

    ngOnInit(): void {
        this.showResetBtn = false;
        this.showPasswordDiv = false;
        let validators: ValidatorFn[] = [];
        this.dataModel = new ForgotPasswordModel();
        this.dataModel.newPassword = "";
        this.dataModel.confirmPassword = "";
        this.dataModel.email = "";
        this.forgotPasswordForm = this.commonService.getFormGroup(this.dataModel, 0);
        validators.push(PasswordValidation.ValidPassword(this.errorMessage, this.forgotPasswordForm.controls["newPassword"]));
        validators.push(PasswordValidation.MatchPassword(this.errorMessage, this.forgotPasswordForm.controls["confirmPassword"]
            , this.forgotPasswordForm.controls["newPassword"]));
        validators.push(Validators.required);
        this.forgotPasswordForm.controls["newPassword"].setValidators(validators[0]);
        this.forgotPasswordForm.controls["confirmPassword"].setValidators(validators);
        this.forgotPasswordForm.controls["email"].setValidators([Validators.required, this.sharedService.emailValidator("Invalid Email Address")]);
        this.showPasswordDiv = false;
        this.resetPassword = true;
        this.activatedRoute.params.subscribe((params: Params) => {
            this.email = params["email"];
            if (this.email != null) {
                this.email = this.email + "@agility.com";
                this.forgotPasswordForm.controls["email"].setValue(this.email);
                this.showResetBtn = true;
                this.showPasswordDiv = true;
            } else {
                this.forgotPasswordForm.controls["email"].setValue(null);
                this.showResetBtn = false;
                this.showPasswordDiv = false;
            }
        });

    }

    validate(): void {
        this.forgetPassword();
    }

    backToHome(): void {
        this.router.navigate(["/Account/"]);
    }

    forgetPassword(): void {
        if (this.forgotPasswordForm.controls["newPassword"].value && this.forgotPasswordForm.controls["confirmPassword"].value &&
            this.forgotPasswordForm.controls["email"].value) {
            this.accountService.restPassword(this.forgotPasswordForm.value).subscribe((response: boolean) => {
                if (response) {
                    this.sharedService.saveForm("Password Reset successfully. Please login with Changed Password.");
                    this.router.navigate(["/Account/"]);
                } else {
                    this.sharedService.errorForm("Some Error occured while reset your password");
                }
            }, () => {
                this.sharedService.errorForm("Password should not matched Please try again.");
            });
        } else {
            this.accountService.forgotPassword(this.forgotPasswordForm.controls["email"].value).subscribe((response: boolean) => {
                if (response) {
                    this.sharedService.saveForm("Reset link sent successfully to your registered email.Please use this link to reset your password.");
                    this.router.navigate(["/Account/"]);
                } else {
                    this.sharedService.errorForm("Please enter valid information.");
                }
            }, () => {
                this.sharedService.errorForm("Email not valid Please try again.");
            });
        }
    }
}
