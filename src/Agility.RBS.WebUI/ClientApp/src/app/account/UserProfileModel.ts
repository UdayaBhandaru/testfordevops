﻿export class UserProfileModel {
    name: string;
    userName: string;
    email: string;
    userToken: string;
    userId: string;
    organization: number;
}