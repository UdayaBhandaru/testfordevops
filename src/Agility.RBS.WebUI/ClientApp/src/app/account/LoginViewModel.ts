﻿export class LoginViewModel {
    email: string;
    password: string;
    rememberMe: boolean;
}