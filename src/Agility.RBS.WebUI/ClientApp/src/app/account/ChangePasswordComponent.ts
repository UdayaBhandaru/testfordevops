import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, FormControl, Validators, ValidatorFn, AbstractControl } from "@angular/forms";
import { Router } from "@angular/router";
import { LoginViewModel } from "./LoginViewModel";
import { FxContext, CommonService, ServiceDocument, UserProfile, IdentityMenuModel, MessageType } from "@agility/frameworkcore";
import { AccountService } from "./AccountService";
import { ChangePasswordModel } from "./ChangePasswordModel";
import { PasswordValidation } from "./PasswordValidation";
import { SharedService } from "../Common/SharedService";
import { MatDialog, MatDialogRef } from "@angular/material";
@Component({
    selector: "changepassword",
  templateUrl: "./ChangePasswordComponent.html",
  styleUrls: ['./ChangePasswordComponent.scss']
})
export class ChangePasswordComponent implements OnInit {
    changePasswordForm: FormGroup;
    dataModel: ChangePasswordModel;
    serviceDocument: ServiceDocument<UserProfile>;
    errorMessage: string;
    pwdPattern: string;
    isValidFormSubmitted = null;
    changePassword: boolean;
    constructor(private fxContext: FxContext, private formBuilder: FormBuilder,
        private router: Router, private accountService: AccountService, private commonService: CommonService, private service: AccountService
        , private sharedService: SharedService, private dialogRef: MatDialogRef<ChangePasswordComponent>) {
        fxContext.IsAuthenticated = false;
    }

    ngOnInit(): void {
        this.fxContext.IsAuthenticated = true;
        let validators: ValidatorFn[] = [];
        this.dataModel = new ChangePasswordModel();
        this.dataModel.currentPassword = "";
        this.dataModel.newPassword = "";
        this.dataModel.confirmPassword = "";
        this.dataModel.email = "";
        this.errorMessage = "";
        this.changePassword = true;
        this.changePasswordForm = this.commonService.getFormGroup(this.dataModel, 0);
        validators.push(PasswordValidation.ValidPassword(this.errorMessage, this.changePasswordForm.controls["newPassword"]));
        validators.push(PasswordValidation.MatchPassword(this.errorMessage, this.changePasswordForm.controls["confirmPassword"]
            , this.changePasswordForm.controls["newPassword"]));
        validators.push(Validators.required);
        this.changePasswordForm.controls["newPassword"].setValidators(validators[0]);
        this.changePasswordForm.controls["confirmPassword"].setValidators(validators);
        this.changePasswordForm.controls["email"].setValidators([Validators.required, this.sharedService.emailValidator("Invalid Email Address")]);
        this.changePasswordForm.controls["email"].setValue(this.fxContext.userProfile.userName);
    }

    save(): void {
        if (this.changePasswordForm.controls["currentPassword"].value) {
            this.accountService.changePassword(this.changePasswordForm.value).subscribe((
            ) => {
                if (this.service.serviceDocument.result.type !== MessageType.success) {
                    this.sharedService.errorForm("Password update Failed.Please enter valid details");
                    this.changePassword = false;
                } else {
                    this.sharedService.saveForm("Password updated successfully. Please login with New Password.").subscribe(() => {
                        if (this.changePassword) {
                            parent.location.href = "http://10.138.71.249:8767/Account/Login";
                        } else {
                            this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
                        }
                    },
                        (error: string) => { console.log(error); });
                }
            }, () => {
                this.sharedService.errorForm("Password should not matched Please try again.");
            });
        } else {
            this.sharedService.errorForm("Password update Failed.Please enter valid details");
        }
    }

    backToHome(): void {
         this.dialogRef.close();
    }
    close(): void {
          this.dialogRef.close();
    }
}
