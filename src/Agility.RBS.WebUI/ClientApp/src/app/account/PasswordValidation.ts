﻿import { AbstractControl, ValidatorFn } from "@angular/forms";
export class PasswordValidation {

    static MatchPassword(errorMessage: string, AC: AbstractControl, passwordControl: AbstractControl): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } => {
            let password: any = passwordControl.value; // to get value in input tag
            let confirmPassword: any = AC.value; // to get value in input tag
            if (password !== confirmPassword) {
                AC.setErrors({ MatchPassword: true });
                errorMessage = "Passwords not matched";
                return { errorMessage };
            } else {
                return null;
            }
        };
    }
    static ValidPassword(errorMessage: string, passwordControl: AbstractControl): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } => {
            let password: any = passwordControl.value; // to get value in input tag
            if (password) {
                let illegalChars: RegExp = /[\W_]/; // allow only letters and numbers
                if (password.value) {
                    passwordControl.setErrors({ ValidPassword: true });
                    errorMessage = "You didnt enter a password.\n";
                    return { errorMessage };
                } else if ((password.length < 7) || (password.length > 15)) {

                    passwordControl.setErrors({ ValidPassword: true });
                    errorMessage = "The password is the wrong length. \n";
                    return { errorMessage };
                } else if (illegalChars.test(password.value)) {
                    passwordControl.setErrors({ ValidPassword: true });
                    errorMessage = "The password contains illegal characters.\n";
                    return { errorMessage };

                } else if ((password.search(/[a-zA-Z]+/) === -1) || (password.search(/[0-9]+/) === -1)) {
                    passwordControl.setErrors({ ValidPassword: true });
                    errorMessage = "The password must contain at least one numeral.\n";
                    return { errorMessage };
                }
            }
        };
    }
}