﻿import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, FormControl } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { RegisterViewModel } from "./RegisterViewModel";
import { FxContext, CommonService, ServiceDocument, UserProfile, MessageType } from "@agility/frameworkcore";
import { AccountService } from "./AccountService";
import { OrganizationViewModel } from "./OrganizationViewModel";
import { startWith, map } from "rxjs/operators";

@Component({
    selector: "register",
    templateUrl: "./RegisterComponent.html"
})
export class RegisterComponent implements OnInit {
    stateCtrl: FormControl;
    filteredStates: any;
    idVal: string = "id";
    textVal: string = "name";
    organizations = [
        { organizationId: 1, name: "aes", organizationEmailAddress: "aes@agility.com", organizationPhoneNumber: "9780987098709709" },
        { organizationId: 2, name: "cus", organizationEmailAddress: "cus@agility.com", organizationPhoneNumber: "9780987098709709" }
    ];

    registerForm: FormGroup;
    dataModel: RegisterViewModel;
    organizationModel: OrganizationViewModel[];
    constructor(private route: ActivatedRoute, private accountService: AccountService, private formBuilder: FormBuilder, private router: Router,
        private commonService: CommonService
    ) {
        this.stateCtrl = new FormControl();
        this.filteredStates = this.stateCtrl.valueChanges
            .pipe(startWith(null),
            map(name => this.filterOrganizations(name)));
    }
    ngOnInit(): void {
        this.organizationModel = this.accountService.organizationModel;
        this.dataModel = new RegisterViewModel();
        this.dataModel.name = "";
        this.dataModel.userName = "";
        this.dataModel.organization = new OrganizationViewModel();
        this.dataModel.email = "";
        this.dataModel.password = "";
        this.dataModel.confirmPassword = "";
        this.registerForm = this.commonService.getFormGroup(this.dataModel, 0);
    }
    filterOrganizations(val: string): any {
        return val ? this.organizations.filter(s => new RegExp(`^${val}`, "gi").test(s.name))
            : this.organizations;
    }

    displayFn(val: any): any {
        return val ? val[this.textVal] : val;
    }
    validate(): void {
        this.accountService.register(this.registerForm.value).subscribe((
        ) => {
            if (+this.accountService.serviceDocument["result"] !== MessageType.success) {
                this.commonService.showAlert(this.accountService.serviceDocument["message"], "Register Failed. Please try again.", 5000);
            } else {
                this.router.navigate(["/Home"]);
            }
        }, () => {
            this.commonService.showAlert(this.accountService.serviceDocument["message"], "Register Failed. Please try again.", 5000);
        });
    }
}