﻿import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";

import { Observable } from "rxjs";
import { AccountService } from "./AccountService";
import { OrganizationViewModel } from "./OrganizationViewModel";

@Injectable()
export class RegisterNewResolver implements Resolve<OrganizationViewModel[]> {
    constructor(private service: AccountService) { }

    resolve(route: ActivatedRouteSnapshot): Observable<OrganizationViewModel[]> {
        return this.service.getOrganizations();
    }
}