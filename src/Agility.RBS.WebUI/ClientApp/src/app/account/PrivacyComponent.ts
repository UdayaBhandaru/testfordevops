import { Component, OnInit, Inject } from "@angular/core";
import { FormGroup, FormBuilder, FormControl, Validators, ValidatorFn, AbstractControl } from "@angular/forms";
import { Router } from "@angular/router";
import { LoginViewModel } from "./LoginViewModel";
import { FxContext, CommonService, ServiceDocument, UserProfile, IdentityMenuModel, MessageType } from "@agility/frameworkcore";
import { AccountService } from "./AccountService";
import { KeepAliveSessionService } from "../Common/KeepAliveSessionService";
import { SharedService } from "../Common/SharedService";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";

@Component({
  selector: "privacy",
  templateUrl: "./PrivacyComponent.html",
  styleUrls: ['./PrivacyComponent.scss']
})
export class PrivacyComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<PrivacyComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit(): void {
    // need to be Implemented
  }

  close(): void {
    // need to be Implemented
  }
}
