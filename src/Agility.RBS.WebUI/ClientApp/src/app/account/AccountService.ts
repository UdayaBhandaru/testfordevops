import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { LoginViewModel } from "./LoginViewModel";
import { RegisterViewModel } from "./RegisterViewModel";
import { UserProfile, ServiceDocument } from "@agility/frameworkcore";
import { OrganizationViewModel } from "./OrganizationViewModel";
import { UserProfileModel } from "./UserProfileModel";
import { ChangePasswordModel } from "./ChangePasswordModel";
import { ForgotPasswordModel } from "../account/ForgotPasswordModel";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../Common/HttpHelperService";
import { UserDetailModel } from "../account/UserDetailModel";

@Injectable()
export class AccountService {
    serviceDocument: ServiceDocument<UserProfile> = new ServiceDocument<UserProfile>();
    organizationModel: OrganizationViewModel[];
    constructor(private httpHelperService: HttpHelperService) {
    }

    login(loginModel: LoginViewModel): Observable<ServiceDocument<UserProfile>> {
      return this.serviceDocument.login("/api/account/login", loginModel);
    }

    register(registerModel: RegisterViewModel): Observable<any> {
        return this.httpHelperService.post("/api/account/register", registerModel);
    }

    getOrganizations(): Observable<OrganizationViewModel[]> {
        return this.httpHelperService.get("/api/account/getOrganizations");
    }

    getUsers(): Observable<UserProfileModel[]> {
        return this.httpHelperService.get("/api/account/List");
    }

    changePassword(changePasswordModel: ChangePasswordModel): Observable<any> {
        return this.httpHelperService.post("/api/account/changePassword", changePasswordModel);
    }

    forgotPassword(email: string): Observable<boolean> {
        return this.httpHelperService.get("/api/account/forgotPassword", new HttpParams().set("email", email));
    }

    restPassword(forgotPasswordModel: ForgotPasswordModel): Observable<boolean> {
        return this.httpHelperService.post("/api/account/resetPassword", forgotPasswordModel);
    }

    logout(): any {
        return this.httpHelperService.get("/api/account/LogOut");
    }

    getUserDetails(userId: string): Observable<UserDetailModel> {
        return this.httpHelperService.get("/api/users/GetUserDetails", new HttpParams().set("userId", userId));
    }
}
