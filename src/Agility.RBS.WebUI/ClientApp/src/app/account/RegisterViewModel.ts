﻿import { OrganizationViewModel } from "./OrganizationViewModel";
export class RegisterViewModel {
    name: string;
    userName: string;
    email: string;
    password: string;
    confirmPassword: string;
    organization: OrganizationViewModel;
}