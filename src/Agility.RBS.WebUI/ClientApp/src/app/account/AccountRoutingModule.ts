﻿import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { RegisterNewResolver } from "./RegisterResolver";
import { LoginComponent } from "./LoginComponent";
import { RegisterComponent } from "./RegisterComponent";
import { ChangePasswordComponent } from "./ChangePasswordComponent";
import { ForgotPasswordComponent } from "./ForgotPasswordComponent";

const AccountRoutes: Routes = [
    {
        path: "Account",
        children: [
            {
                path: "", redirectTo: "Login", pathMatch: "full"
            },
            {
                path: "Login",
                component: LoginComponent,
            },
            {
                path: "Register",
                component: RegisterComponent,
                resolve: {
                    OrganizationViewModel: RegisterNewResolver
                }
            },
            {
                path: "ChangePassword",
                component: ChangePasswordComponent,
            },
            {
                path: "ForgotPassword",
                component: ForgotPasswordComponent,
            },
            {
                path: "ForgotPassword/:email",
                component: ForgotPasswordComponent,
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(AccountRoutes)
    ]
})
export class AccountRoutingModule { }
