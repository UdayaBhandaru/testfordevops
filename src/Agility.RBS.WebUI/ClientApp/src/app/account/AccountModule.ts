﻿import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { AccountRoutingModule } from "./AccountRoutingModule";
import { LoginComponent } from "./LoginComponent";
import { RegisterComponent } from "./RegisterComponent";
import { RegisterNewResolver } from "./RegisterResolver";
import { AccountService } from "./AccountService";
import { ChangePasswordComponent } from "./ChangePasswordComponent";
import { ForgotPasswordComponent } from "./ForgotPasswordComponent";
import { PrivacyComponent } from "./PrivacyComponent";
import { FavouriteService } from "../favourites/FavouriteService";
import { DocumentsModule } from "../Documents/DocumentsModule";
import { TermsAndConditionsComponent } from "./TermsAndConditionsComponent";
import { RbInputComponent } from "../Common/controls/RbInputComponent";
import { AgGridSharedModule } from "../AgGridSharedModule";
import { RbsSharedModule } from "../RbsSharedModule";

@NgModule({
    imports: [
        FrameworkCoreModule,
        AccountRoutingModule,
        DocumentsModule,
        AgGridSharedModule,
        RbsSharedModule
    ],
    declarations: [
        LoginComponent,
        RegisterComponent,
        ChangePasswordComponent,
        ForgotPasswordComponent,
        PrivacyComponent,
        TermsAndConditionsComponent
    ],
    providers: [
        AccountService,
        RegisterNewResolver,
        FavouriteService
    ],
    entryComponents: [PrivacyComponent, TermsAndConditionsComponent]
})
export class AccountModule {
}