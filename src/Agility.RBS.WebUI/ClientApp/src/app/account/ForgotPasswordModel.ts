﻿export class ForgotPasswordModel {
    newPassword: string;
    confirmPassword: string;
    email: string;
}