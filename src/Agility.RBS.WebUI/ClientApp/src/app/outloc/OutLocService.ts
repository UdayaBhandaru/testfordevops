import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { HttpParams } from "@angular/common/http";
import { OutLocModel } from "./OutLocModel";
import { HttpHelperService } from '../Common/HttpHelperService';

@Injectable()
export class OutLocService {
    serviceDocument: ServiceDocument<OutLocModel> = new ServiceDocument<OutLocModel>();
    constructor(private httpHelperService: HttpHelperService) { }

    newModel(model: OutLocModel): ServiceDocument<OutLocModel> {
        return this.serviceDocument.newModel(model);
    }

    search(): Observable<ServiceDocument<OutLocModel>> {
      return this.serviceDocument.search("/api/Outloc/Search");
    }

    save(): Observable<ServiceDocument<OutLocModel>> {
      return this.serviceDocument.save("/api/Outloc/Save", true);
    }

    list(): Observable<ServiceDocument<OutLocModel>> {
      return this.serviceDocument.list("/api/Outloc/List");
    }

    addEdit(): Observable<ServiceDocument<OutLocModel>> {
      return this.serviceDocument.list("/api/Outloc/New");
    }
  isExistingOutLoc(outLocId: string): Observable<boolean> {
    return this.httpHelperService.get("/api/Outloc/isExistingOutLoc", new HttpParams().set("outLocId", outLocId));
    }
}
