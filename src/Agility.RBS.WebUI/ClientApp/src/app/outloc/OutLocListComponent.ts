import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { GridOptions } from "ag-grid-community";
import { OutLocModel } from './OutLocModel';
import { OutLocService } from './OutLocService';
import { GridEditComponent } from '../Common/grid/GridEditComponent';
import { SharedService } from '../Common/SharedService';
import { GridExportModel } from '../Common/grid/GridExportModel';
import { InfoComponent } from '../Common/InfoComponent';
import { CurrencyDomainModel } from '../Common/DomainData/CurrencyDomainModel';
import { CostZoneLocModel } from '../costzone/costzone/CostZoneLocModel';
import { DomainDetailModel } from '../domain/DomainDetailModel';
import { CountryDomainModel } from '../Common/DomainData/CountryDomainModel';
import { StateDomainModel } from '../Common/DomainData/StateDomainModel';
import { RegionModel } from '../organizationhierarchy/region/RegionModel';

@Component({
  selector: "OutLoc-list",
  templateUrl: "./OutLocListComponent.html"
})
export class OutLocListComponent implements OnInit {
  PageTitle = "Out Side Location";
  redirectUrl = "Outloc";
  componentName: any;
  serviceDocument: ServiceDocument<OutLocModel>;
  public gridOptions: GridOptions;
  columns: any[];
  model: OutLocModel = {
    outLocId: null, outLocTypeDesc: null, outLocType: null, outLocDesc: null, outLocCurrency: null, outLocAdd1: null,
    outLocAdd2: null, outLocCity: null, outLocState: null, outLocCountryId: null, outLocCountryName: null, outLocPost: null,
    outLocVatRegion: null, contactName: null, contactPhone: null, contactFax: null,
    contactTelex: null, contactEmail: null, operation: "I"
  };
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;

  currencyData: CurrencyDomainModel[];
  locationData: CostZoneLocModel[];
  indicatorData: DomainDetailModel[];
  locTypeData: DomainDetailModel[];
  countryDomainData: CountryDomainModel[];
  stateDomainData: StateDomainModel[];
  regionData: RegionModel[];

  constructor(public service: OutLocService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "out Location Id", field: "outLocId", tooltipField: "outLocId" },
      { headerName: "out Loc Type", field: "outLocType", tooltipField: "outLocType" },
      { headerName: "out Loc Type Desc", field: "outLocTypeDesc", tooltipField: "outLocTypeDesc" },
      { headerName: "out Loc Currency", field: "outLocCurrency", tooltipField: "outLocCurrency" },
      { headerName: "out Loc Add1", field: "outLocAdd1", tooltipField: "outLocAdd1" },
      { headerName: "out Loc Add2", field: "outLocAdd2", tooltipField: "outLocAdd2" },
      { headerName: "out Loc City", field: "outLocCity", tooltipField: "outLocCity" },
      { headerName: "out Loc State", field: "outLocState", tooltipField: "outLocState" },
      { headerName: "out Loc Country Name", field: "outLocCountryName", tooltipField: "outLocCountryName" },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent
      }
    ];

    this.currencyData = Object.assign([], this.service.serviceDocument.domainData["currency"]);
    this.locationData = Object.assign([], this.service.serviceDocument.domainData["location"]);
    this.indicatorData = Object.assign([], this.service.serviceDocument.domainData["indicatorStatus"]);
    this.locTypeData = Object.assign([], this.service.serviceDocument.domainData["locationsType"]);
    this.countryDomainData = Object.assign([], this.service.serviceDocument.domainData["country"]);
    this.stateDomainData = Object.assign([], this.service.serviceDocument.domainData["state"]);
    this.regionData = Object.assign([], this.service.serviceDocument.domainData["regions"]);  

    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }
}
