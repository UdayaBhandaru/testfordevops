import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { OutLocListComponent } from "./OutLocListComponent";
import { OutLocService } from "./OutLocService";
import { OutLocRoutingModule } from "./OutLocRoutingModule";
import { OutLocComponent } from "./OutLocComponent";
import { OutLocListResolver, OutLocResolver } from "./OutLocResolver";
import { AgGridSharedModule } from '../AgGridSharedModule';
import { TitleSharedModule } from '../TitleSharedModule';
import { RbsSharedModule } from '../RbsSharedModule';


@NgModule({
    imports: [
        FrameworkCoreModule,
        OutLocRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        OutLocListComponent,
        OutLocComponent,
    ],
    providers: [
        OutLocService,
        OutLocListResolver,
        OutLocResolver,
    ]
})
export class OutLocModule {
    constructor() {
        LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
    }
}
