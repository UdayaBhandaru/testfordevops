export class OutLocModel {   
  outLocType: string;
  outLocTypeDesc: string;
  outLocId: string;  
  outLocDesc: string;
  outLocCurrency: string;
  outLocAdd1: string;
  outLocAdd2: string;
  outLocCity: string;
  outLocState: string;
  outLocCountryId: string;
  outLocCountryName: string;
  outLocPost: string;
  outLocVatRegion?: number;
  contactName: string;
  contactPhone: string;
  contactFax: string;
  contactTelex: string;
  contactEmail: string;
  operation: string;
}
