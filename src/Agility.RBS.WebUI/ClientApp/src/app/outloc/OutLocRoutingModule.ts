import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { OutLocListComponent } from "./OutLocListComponent";
import { OutLocListResolver, OutLocResolver} from "./OutLocResolver";
import { OutLocComponent } from "./OutLocComponent";
const UtilityRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
              component: OutLocListComponent,
                resolve:
                {
                    serviceDocument: OutLocListResolver
                }
            },
            {
                path: "New",
                component: OutLocComponent,
                resolve:
                {
                    serviceDocument: OutLocResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(UtilityRoutes)
    ]
})
export class OutLocRoutingModule { }
