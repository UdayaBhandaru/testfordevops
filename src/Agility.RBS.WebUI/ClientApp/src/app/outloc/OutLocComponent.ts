import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { OutLocModel } from './OutLocModel';
import { OutLocService } from './OutLocService';
import { SharedService } from '../Common/SharedService';
import { DomainDetailModel } from '../domain/DomainDetailModel';
import { CostZoneLocModel } from '../costzone/costzone/CostZoneLocModel';
import { CurrencyDomainModel } from '../Common/DomainData/CurrencyDomainModel';
import { CountryDomainModel } from '../Common/DomainData/CountryDomainModel';
import { StateDomainModel } from '../Common/DomainData/StateDomainModel';
import { RegionModel } from '../organizationhierarchy/region/RegionModel';

@Component({
  selector: "Outloc",
  templateUrl: "./OutLocComponent.html"
})

export class OutLocComponent implements OnInit {
  PageTitle = "Out Side Location";
  redirectUrl = "Outloc";
    serviceDocument: ServiceDocument<OutLocModel>;
    editMode: boolean = false;
    editModel: any;
    localizationData: any;  
    model: OutLocModel;  
  currencyData: CurrencyDomainModel[];
  locationData: CostZoneLocModel[];
  indicatorData: DomainDetailModel[];
  locTypeData: DomainDetailModel[];
  countryDomainData: CountryDomainModel[];
  stateDomainData: StateDomainModel[];
  regionData: RegionModel[];

    constructor(private service: OutLocService, private sharedService: SharedService, private router: Router
        , private ref: ChangeDetectorRef) {
    }

  ngOnInit(): void {
    this.currencyData = Object.assign([], this.service.serviceDocument.domainData["currency"]);
    this.locationData = Object.assign([], this.service.serviceDocument.domainData["location"]);
    this.indicatorData = Object.assign([], this.service.serviceDocument.domainData["indicatorStatus"]);
    this.locTypeData = Object.assign([], this.service.serviceDocument.domainData["locationsType"]);
    this.countryDomainData = Object.assign([], this.service.serviceDocument.domainData["country"]);
    this.stateDomainData = Object.assign([], this.service.serviceDocument.domainData["state"]);
    this.regionData = Object.assign([], this.service.serviceDocument.domainData["regions"]);
    this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
        this.bind();
    }

    bind(): void {
        delete this.editModel;
        if (this.sharedService.editObject) {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);         
            Object.assign(this.service.serviceDocument.dataProfile.dataModel, { operation: "U" });
            this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            delete this.sharedService.editObject;
            this.editMode = true;

        } else {            
          this.model = {
            outLocId: null, outLocTypeDesc: null, outLocType: null, outLocDesc: null, outLocCurrency: null, outLocAdd1: null,
            outLocAdd2: null, outLocCity: null, outLocState: null, outLocCountryId: null, outLocCountryName: null, outLocPost: null,
            outLocVatRegion: null, contactName: null, contactPhone: null, contactFax: null,
            contactTelex: null, contactEmail: null,operation: "I"};           
            this.service.newModel(this.model);
        }
        this.serviceDocument = this.service.serviceDocument;
    }

    save(saveOnly: boolean): void {
        if (this.serviceDocument.dataProfile.profileForm.valid) {
            this.saveSubscription(saveOnly);
        } else {
            this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
        }
    }

    saveSubscription(saveOnly: boolean): void {
      //this.checkOutLoc(saveOnly);
      this.saveOutLoc(saveOnly);

  }

    saveOutLoc(saveOnly: boolean): void {
        this.service.save().subscribe(() => {
            if (this.service.serviceDocument.result.type === MessageType.success) {
                this.sharedService.saveForm("OutLoc saved successfully.").subscribe(() => {
                    if (saveOnly) {
                        this.router.navigate(["/" + this.redirectUrl + "/List"], { skipLocationChange: true });
                    } else {
                        if (!this.editMode) {
                          this.model = {
                            outLocId: null, outLocTypeDesc: null, outLocType: null, outLocDesc: null, outLocCurrency: null, outLocAdd1: null,
                            outLocAdd2: null, outLocCity: null, outLocState: null, outLocCountryId: null, outLocCountryName: null, outLocPost: null,
                            outLocVatRegion: null, contactName: null, contactPhone: null, contactFax: null,
                            contactTelex: null, contactEmail: null, operation: "I" };                            
                            this.service.newModel(this.model);
                        } else {
                            Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
                            this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.service.serviceDocument.dataProfile.dataModel);
                            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
                        }
                    }
                }, (error: string) => { console.log(error); });
            } else {
                this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
            }
        }, (error: string) => { console.log(error); });
    }

    checkOutLoc(saveOnly: boolean): void {
        if (!this.editMode) {
          this.service.isExistingOutLoc(this.serviceDocument.dataProfile.profileForm.controls["outLocId"].value).subscribe((response: any) => {
                if (response) {
                    this.sharedService.errorForm("OutLoc already exisit.");
                } else {
                    this.saveOutLoc(saveOnly);
                }
            });
        } else {
            this.saveOutLoc(saveOnly);
        }
    }

    reset(): void {
        if (!this.editMode) {
            this.model = {
              outLocId: null, outLocTypeDesc:null, outLocType: null, outLocDesc: null, outLocCurrency: null, outLocAdd1: null,
              outLocAdd2: null, outLocCity: null, outLocState: null, outLocCountryId: null, outLocCountryName:null, outLocPost: null,
              outLocVatRegion: null, contactName: null, contactPhone: null, contactFax: null,
              contactTelex: null, contactEmail: null, operation: "I"
            };
           
            this.service.serviceDocument.dataProfile.dataModel = this.model;
            this.service.newModel(this.model);

        } else {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);            
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
        }
        this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
    }
}
