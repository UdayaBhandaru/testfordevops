import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { OutLocService } from "./OutLocService";
import { OutLocModel } from "./OutLocModel";
@Injectable()
export class OutLocListResolver implements Resolve<ServiceDocument<OutLocModel>> {
    constructor(private service: OutLocService) { }
    resolve(): Observable<ServiceDocument<OutLocModel>> {
        return this.service.list();
    }
}

@Injectable()
export class OutLocResolver implements Resolve<ServiceDocument<OutLocModel>> {
    constructor(private service: OutLocService) { }
    resolve(): Observable<ServiceDocument<OutLocModel>> {
        return this.service.addEdit();
    }
}



