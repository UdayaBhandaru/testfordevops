import { Component, OnInit, TemplateRef, ViewChild, OnDestroy, ElementRef, ChangeDetectorRef } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ServiceDocument, CommonService, MessageResult, MessageType } from "@agility/frameworkcore";
import { GridOptions, GridApi } from "ag-grid-community";
import { Subscription } from "rxjs";
import { InboxCommonModel } from '../Common/DomainData/InboxCommonModel';
import { SupplierDomainModel } from '../Common/DomainData/SupplierDomainModel';
import { DivisionDomainModel } from '../Common/DomainData/DivisionDomainModel';
import { SharedService } from '../Common/SharedService';
import { LoaderService } from '../Common/LoaderService';
import { RbDialogService } from '../Common/controls/RbDialogService';
import { CostChangeService } from './CostChangeService';
import { DocumentsConstants } from '../Common/DocumentsConstants';
import { CostChangeHeadModel } from './CostChangeHeadModel';
import { CostChangeDetailModel } from './CostChangeDetailModel';

@Component({
  selector: "costChangeHead-add",
  templateUrl: "./CostChangeHeadComponent.html"
})
export class CostChangeHeadComponent implements OnInit, OnDestroy {
  _componentName = "Cost Change Head";
  PageTitle: string = "Cost Change";
  documentsConstants: DocumentsConstants = new DocumentsConstants();
  screenName: string;
  public serviceDocument: ServiceDocument<CostChangeHeadModel>;
  localizationData: any;
  // costChangeReqId: number;
  primaryId: number; // used in CanDeactivate method, should be same name if we use candeactivate
  editModel: CostChangeHeadModel = {
    cosT_CHANGE: null, costChangeDesc: null, reason: null, costChangeOrigin: null, costChangeStatus: null, effectiveStartDate: null
    , effectiveEndDate: null, runDate: null, approvalDate: null, approvedBy: null, operation: null, tableName: null, wfNxtStateInd: null
    , organizationId: null, modifiedBy: null, deletedInd: null, modifiedDate: null, createdDate: null, createdBy: null
    , workflowInstance: null, lineItemCnt: 0, noMarginItemCnt: 0, divisionId: null, supplierId: null, divisionDesc: null, supplierName: null
  };
  editMode: boolean;
  effectiveMinDate: Date;
  inboxCommonData: InboxCommonModel;
  SupplierDomainModel: SupplierDomainModel[];
  divisionData: DivisionDomainModel[];
  supplierData: SupplierDomainModel[];
  apiUrl: string;
  saveServiceSubscription: any;
  submitServiceSubscription: any;
  submitSuccessSubscription: Subscription;
  saveSubscribtion: any;
  saveContinueSubscribtion: any;
  resetSubscribtion: any;
  closeSubscribtion: any;
  routeServiceSubscription: any;
  saveSuccessSubscription: any;
  rejectSubscription: any;
  rejectSuccessSubscription: any;
  nextSubscription: any;
  public messageResult: MessageResult = new MessageResult();
  dialogRef: any;
  canNavigate: boolean;
  costChangeReasonsData: any;
  componentName: any;
  columns: any[];
  public sheetName: string = "BulkItem Detail";
  data: CostChangeDetailModel[] = [];
  public gridOptions: GridOptions;
  gridApi: GridApi;
  costStatus: string;

  constructor(
    private route: ActivatedRoute, public router: Router, private changeRef: ChangeDetectorRef,
    public sharedService: SharedService, private commonService: CommonService, private loaderService: LoaderService,
    public dialogService: RbDialogService, public service: CostChangeService) {
  }

  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }

  ngOnInit(): void {
    this.costChangeReasonsData = Object.assign([], this.service.serviceDocument.domainData["costChangeReasons"]);
    this.divisionData = Object.assign([], this.service.serviceDocument.domainData["division"]);
    this.supplierData = Object.assign([], this.service.serviceDocument.domainData["supplier"]);
    this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
    this.screenName = this.documentsConstants.costObjectName;
    this.apiUrl = "/api/Supplier/SuppliersGetName?name=";
    this.componentName = this;
    this.columns = [
      { headerName: "Item", field: "item", tooltipField: "item" },
      { headerName: "Item Desc", field: "itemDescription", tooltipField: "itemDescription" },
      { headerName: "Department", field: "departmentDescription", tooltipField: "departmentDescription" }
    ];

    if (this.service.serviceDocument.dataProfile.dataModel) {
      this.bind();
    }
  }

  bind(): void {
    this.routeServiceSubscription = this.route.params.subscribe((resp: any) => {
      this.primaryId = resp["id"];
    });
    if (this.primaryId.toString() !== "0") {
      this.editMode = true;
      this.PageTitle = `${this.PageTitle} - EDIT - ${this.primaryId}`;
    } else {
      this.service.serviceDocument.dataProfile.dataModel =
        Object.assign(this.service.serviceDocument.dataProfile.dataModel
          , { operation: "I", costChangeStatus: "WORKSHEET" }
          , { workflowInstance: this.service.serviceDocument.dataProfile.dataModel.workflowInstance });
      this.service.serviceDocument.dataProfile.profileForm.patchValue(this.service.serviceDocument.dataProfile.dataModel);
      this.PageTitle = `${this.PageTitle} - ADD`;
    }

    if (this.editMode) {
      this.costStatus = this.service.serviceDocument.dataProfile.dataModel.costChangeStatus;
      if (this.service.serviceDocument.dataProfile.dataModel.costChangeDetails) {
        Object.assign(this.data, this.service.serviceDocument.dataProfile.dataModel.costChangeDetails);
      }
    }
    this.serviceDocument = this.service.serviceDocument;
    this.sharedService.fileCount = this.service.serviceDocument.dataProfile.dataModel.fileCount;
    this.sharedService.checkWfPermitions(this.serviceDocument);
  }

  save(saveOnly: boolean): void {
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.saveSubscription(saveOnly);
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  saveAndContinue(): void {
    this.save(false);
  }

  saveSubscription(saveOnly: boolean): void {
    this.loaderService.display(true);
    this.saveServiceSubscription = this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.primaryId = this.service.serviceDocument.dataProfile.dataModel.cosT_CHANGE;
        this.saveSuccessSubscription = this.sharedService.saveForm(
          `${this.localizationData.costChangeHead.costchangeheadsave} - Cost Change Request ID ${this.primaryId}`
        ).subscribe(() => {
          if (saveOnly) {
            this.router.navigate(["/CostChange/List"], { skipLocationChange: true });
          } else {

            this.router.navigate(["/Blank"], {
              skipLocationChange: true, queryParams: { id: "/CostChange/New/" + this.primaryId }
            });

            this.openCostManagementExcel();
            this.loaderService.display(false);
          }
        });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
      this.loaderService.display(false);
    });
  }

  submitAction(): void {
    this.loaderService.display(true);
    this.submitServiceSubscription = this.service.submit().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.primaryId = this.service.serviceDocument.dataProfile.dataModel.cosT_CHANGE;
        this.submitSuccessSubscription = this.sharedService.saveForm(`Submitted - Cost Change Request ID ${this.primaryId}`).subscribe(() => {
          this.router.navigate(["/CostChange/List"], { skipLocationChange: true });
        });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
      this.loaderService.display(false);
    });
  }

  ngOnDestroy(): void {
    if (this.saveSubscribtion) {
      this.saveSubscribtion.unsubscribe();
    }
    if (this.saveContinueSubscribtion) {
      this.saveContinueSubscribtion.unsubscribe();
    }
    if (this.closeSubscribtion) {
      this.closeSubscribtion.unsubscribe();
    }
    if (this.resetSubscribtion) {
      this.resetSubscribtion.unsubscribe();
    }
    if (this.routeServiceSubscription) {
      this.routeServiceSubscription.unsubscribe();
    }


    if (this.saveServiceSubscription) {
      this.saveServiceSubscription.unsubscribe();
    }
    if (this.submitServiceSubscription) {
      this.submitServiceSubscription.unsubscribe();
    }
    if (this.saveSuccessSubscription) {
      this.saveSuccessSubscription.unsubscribe();
    }
    if (this.submitSuccessSubscription) {
      this.submitSuccessSubscription.unsubscribe();
    }
    if (this.rejectSubscription) {
      this.rejectSubscription.unsubscribe();
    }
    if (this.rejectSuccessSubscription) {
      this.rejectSuccessSubscription.unsubscribe();
    }
    if (this.nextSubscription) {
      this.nextSubscription.unsubscribe();
    }

  }

  reset(): void {
    Object.keys(this.serviceDocument.dataProfile.profileForm.controls).forEach(control => {
      this.serviceDocument.dataProfile.profileForm.controls[control].setValue(this.serviceDocument.dataProfile.dataModel[control]);
    });
    this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
  }

  reject(): void {
    this.loaderService.display(true);
    this.rejectSubscription = this.service.reject().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.primaryId = this.service.serviceDocument.dataProfile.dataModel.cosT_CHANGE;
        this.rejectSuccessSubscription = this.sharedService.saveForm(`Rejected - Cost Change Request ID ${this.primaryId}`).subscribe(() => {
          this.router.navigate(["/CostChange/List"], { skipLocationChange: true });
        });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
      this.loaderService.display(false);
    });
  }

  openCostManagementExcel(): void {
    this.sharedService.downloadExcel(this.service.serviceDocument.dataProfile.dataModel.cosT_CHANGE, "CostChange");
  }
}
