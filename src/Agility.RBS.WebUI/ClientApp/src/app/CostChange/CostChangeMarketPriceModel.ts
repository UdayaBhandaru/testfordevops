﻿export class CostChangeMarketPriceModel {
    competitor: string;
    compName: string;
    item: string;
    compRetail?: number;
}