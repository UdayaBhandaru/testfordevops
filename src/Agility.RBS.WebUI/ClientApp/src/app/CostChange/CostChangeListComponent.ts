import { Component, OnInit, ViewChild, OnDestroy, ChangeDetectorRef } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ServiceDocument, FxContext } from "@agility/frameworkcore";
import { FormGroup, FormBuilder, FormControl, Validators, ValidatorFn, AbstractControl } from "@angular/forms";
import { Subscription } from "rxjs";
import { IServerSideDatasource, GridOptions, ColDef, GridReadyEvent, IServerSideGetRowsParams } from 'ag-grid-community';
import { DomainDetailModel } from '../domain/DomainDetailModel';
import { WorkflowStatusDomainModel } from '../Common/DomainData/WorkflowStatusDomainModel';
import { SupplierDomainModel } from '../Common/DomainData/SupplierDomainModel';
import { SharedService } from '../Common/SharedService';
import { CommonModel } from '../Common/CommonModel';
import { UserDomainModel } from '../Common/DomainData/UserDomainModel';
import { SearchComponent } from '../Common/SearchComponent';
import { CostChangeService } from './CostChangeService';
import { GridDateComponent } from '../Common/grid/GridDateComponent';
import { GridEditComponent } from '../Common/grid/GridEditComponent';
import { InfoComponent } from '../Common/InfoComponent';
import { CostChangeHeadModel } from './CostChangeHeadModel';
import { CostChangeSearchModel } from './CostChangeSearchModel';
import { CostChangeListService } from './CostChangeListService';
import { DivisionDomainModel } from '../Common/DomainData/DivisionDomainModel';
import { CostChangeReasonDomainModel } from '../Common/DomainData/CostChangeReasonDomainModel';

@Component({
  selector: "costChange-list",
  templateUrl: "./CostChangeListComponent.html"
})
export class CostChangeListComponent implements OnInit, IServerSideDatasource, OnDestroy {
  public dmyData: any[] = [];
  public serviceDocument: ServiceDocument<CostChangeSearchModel>;
  PageTitle = "Cost Management";
  redirectUrl = "CostChange/New/";
  private gridOptions: GridOptions;
  componentName: any;
  additionalSearchParams: any;
  loadedPage: boolean;
  searchServiceSubscription: Subscription;
  domainDtlData: DomainDetailModel[];
  workflowStatusList: WorkflowStatusDomainModel[];
  supplierData: SupplierDomainModel[];
  columns: ColDef[];
  model: CostChangeSearchModel;
  apiUrl: string;
  showSearchCriteria: boolean = true;
  @ViewChild("searchPanelCostChg") searchPanelCostChg: SearchComponent;
  isSearchParamGiven: boolean = false;
  defaultDateType: string;
  sheetName: string = "Cost Management";
  costChangeReasonsData: CostChangeReasonDomainModel;
  divisionData: DivisionDomainModel[];

  constructor(public service: CostChangeListService, private sharedService: SharedService, private changeRef: ChangeDetectorRef
    , private router: Router, private fxContext: FxContext) {
  }

  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { colId: "cosT_CHANGE_ID", headerName: "Cost Change ID", field: "cosT_CHANGE_ID", tooltipField: "cosT_CHANGE_ID", sort: "desc" },
      { colId: "COST_CHANGE_DESC", headerName: "Description", field: "costChangeDesc", tooltipField: "costChangeDesc" },
      { colId: "SUPP_NAME", headerName: "Supplier", field: "supName", tooltipField: "supName" },
      { colId: "DIVISION_DESC", headerName: "Division", field: "divisionName", tooltipField: "divisionName" },
      { colId: "REASON_DESC", headerName: "Reason", field: "reasonDesc", tooltipField: "reasonDesc" },
      { colId: "COST_STATUS_DESC", headerName: "Status", field: "costChangeStatusDesc", tooltipField: "costChangeStatusDesc", width: 100 },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 100
      }
    ];

    this.gridOptions = {
      onGridReady: (params: GridReadyEvent) => {
        params.api.setServerSideDatasource(this);
      },
      context: {
        componentParent: this
      },
      rowModelType: "serverSide",
      paginationPageSize: this.service.costChgPaging.pageSize,
      pagination: true,
      enableServerSideSorting: true,
      enableServerSideFilter: true,
      cacheBlockSize: this.service.costChgPaging.cacheSize
    };

    // let tokenn: any = this.fxContext.userProfile.userAccessToken = this.service.serviceDocument.domainData["tokenTuple"].item3;
    //let tokenn: any = this.service.serviceDocument.domainData["tokenTuple"].item3;
    //localStorage.setItem("auth_token", tokenn);
    //this.service.jwt();

    this.supplierData = Object.assign([], this.service.serviceDocument.domainData["supplier"]);
    this.workflowStatusList = Object.assign([], this.service.serviceDocument.domainData["workflowStatusList"]);
    this.costChangeReasonsData = Object.assign([], this.service.serviceDocument.domainData["costChangeReasons"]);
    this.divisionData = Object.assign([], this.service.serviceDocument.domainData["division"]);
    this.apiUrl = "/api/Supplier/SuppliersGetName?name=";

    this.sharedService.domainData = {
      supplier: this.supplierData, division: this.divisionData, reason: this.costChangeReasonsData
    };

    this.model = {
      cosT_CHANGE_ID: null, costChangeDesc: null, reason: null, costChangeStatus: null, supplierId: null, itemBarcode: null, itemDesc: null, divisionId: null
    };

    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  getRows(params: IServerSideGetRowsParams): void {
    this.additionalSearchParams = { startRow: params.request.startRow, endRow: params.request.endRow, sortModel: params.request.sortModel };
    if (!this.loadedPage) {
      this.loadedPage = true;
      params.successCallback(
        this.serviceDocument.dataProfile.dataList.slice(params.request.startRow, params.request.endRow),
        this.service.serviceDocument.dataProfile.dataModel.totalRows);
    } else {
      this.searchServiceSubscription = this.service.search(this.additionalSearchParams).subscribe((sDoc) => {
        params.successCallback(sDoc.dataProfile.dataList, this.service.serviceDocument.dataProfile.dataModel.totalRows);
      });
    }
  }

  showSearchCriteriaChild(event: any): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
      this.sharedService.previousUrl = "../CostChange/List";
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl + cell.data.cosT_CHANGE_ID, this.service.serviceDocument.dataProfile.dataList);
  }

  addValidation(): void {
    this.router.navigate(["/" + this.redirectUrl + "0"], { skipLocationChange: true });
    this.sharedService.previousUrl = "../CostChange/List";
  }

  ngOnDestroy(): void {
    if (this.searchServiceSubscription) {
      this.searchServiceSubscription.unsubscribe();
    }
  }
}
