export class CostChangeItemPriceModel {
    currentPrice?: number;
    priceCurrencyCode: string;
    margin?: number;
    exchangeRate?: number;
    format: string;
    formatName: string;
    netMargin?: number;
}
