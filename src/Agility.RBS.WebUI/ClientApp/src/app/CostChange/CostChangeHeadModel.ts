import { CostChangeDetailModel } from './CostChangeDetailModel';

export class CostChangeHeadModel {
  cosT_CHANGE?: number;
  costChangeDesc?: string;
  reason?: number;
  costChangeOrigin?: string;
  costChangeStatus?: string;
  effectiveStartDate?: Date;
  effectiveEndDate?: Date;
  runDate?: Date;
  approvalDate?: Date;
  approvedBy?: string;
  operation: string;
  tableName?: string;
  wfNxtStateInd?: number;
  organizationId: number;
  modifiedBy?: string;
  deletedInd?: boolean;
  modifiedDate?: Date;
  createdDate?: Date;
  createdBy?: string;
  workflowInstance?:string;
  lineItemCnt?: number;
  noMarginItemCnt?: number;
  fileCount?: number;
  divisionId?:number;
  divisionDesc?: string;
  supplierId?: number;
  supplierName?: string;
  costChangeDetails?: CostChangeDetailModel[]
}
