import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../AgGridSharedModule";
import { TitleSharedModule } from "../TitleSharedModule";
import { CostManagementRoutingModule } from "./CostManagementRoutingModule";
import { CostChangeListResolver, CostChangeNewResolver } from "./CostManagementResolver";
import { LicenseManager } from "ag-grid-enterprise/main";
import { InboxService } from "../inbox/InboxService";
import { RbsSharedModule } from "../RbsSharedModule";
import { CostChangeHeadComponent } from './CostChangeHeadComponent';
import { CostChangeListComponent } from './CostChangeListComponent';
import { CostChangeService } from './CostChangeService';
import { CostChangeListService } from './CostChangeListService';


@NgModule({
  imports: [
    FrameworkCoreModule,
    CostManagementRoutingModule,
    AgGridModule,
    AgGridSharedModule,
    TitleSharedModule,
    RbsSharedModule
  ],
  declarations: [
    CostChangeListComponent,
    CostChangeHeadComponent,
  ],
  providers: [
    CostChangeListResolver,
    CostChangeNewResolver,
    CostChangeService,
    InboxService,
    CostChangeListService
  ]
})
export class CostManagementModule {
  constructor() {
    LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
  }
}
