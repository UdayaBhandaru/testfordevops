import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { CostChangeSearchModel } from "./CostChangeSearchModel";
import { SharedService } from "../Common/SharedService";
import { HttpHeaders } from '@angular/common/http';

@Injectable()
export class CostChangeListService {
  serviceDocument: ServiceDocument<CostChangeSearchModel> = new ServiceDocument<CostChangeSearchModel>();
  costChgPaging: {
    startRow: number,
    pageSize: number,
    cacheSize: number,
  } = {
      startRow: 0,
      pageSize: 10,
      cacheSize: 100,
    };

  constructor(private sharedService: SharedService) { }

  newModel(model: CostChangeSearchModel): ServiceDocument<CostChangeSearchModel> {
    return this.serviceDocument.newModel(model);
  }

  search(additionalParams: any): Observable<ServiceDocument<CostChangeSearchModel>> {
    return this.serviceDocument.search("/api/CostChange/Search", true, () => this.setSubClassId(additionalParams));
  }

  list(): Observable<ServiceDocument<CostChangeSearchModel>> {
    return this.serviceDocument.list("/api/CostChange/List");
  }

  jwt(): any {
    let headers: HttpHeaders = new HttpHeaders();
    headers.set("content-type", "application/json");
    let authToken: string = localStorage.getItem("auth_token");
    headers.set("authorization", `bearer ${authToken}`);
  }

  setSubClassId(additionalParams: any): void {
    var sd: CostChangeSearchModel = this.serviceDocument.dataProfile.dataModel;
    if (additionalParams) {
      sd.startRow = additionalParams.startRow ? additionalParams.startRow : this.costChgPaging.startRow;
      sd.endRow = additionalParams.endRow ? additionalParams.endRow : this.costChgPaging.cacheSize;
      sd.sortModel = additionalParams.sortModel;
    } else {
      sd.startRow = this.costChgPaging.startRow;
      sd.endRow = this.costChgPaging.cacheSize;
    }
  }
}
