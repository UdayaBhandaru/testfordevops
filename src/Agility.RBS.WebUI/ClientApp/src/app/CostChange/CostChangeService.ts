import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument, WorkflowAction } from "@agility/frameworkcore";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { RequestOptions, ResponseContentType, RequestOptionsArgs } from "@angular/http";
import { SharedService } from '../Common/SharedService';
import { CostChangeHeadModel } from './CostChangeHeadModel';

@Injectable()
export class CostChangeService {
  serviceDocument: ServiceDocument<CostChangeHeadModel> = new ServiceDocument<CostChangeHeadModel>();
  constructor(private sharedService: SharedService, private httpClient: HttpClient) { }

  newModel(model: CostChangeHeadModel): ServiceDocument<CostChangeHeadModel> {
    return this.serviceDocument.newModel(model);
  }

  jwt(): any {
    let headers: HttpHeaders = new HttpHeaders();
    headers.set("content-type", "application/json");
    let authToken: string = localStorage.getItem("auth_token");
    headers.set("authorization", `bearer ${authToken}`);
  }

  openCostManagementExcel(): Observable<any> {
    return this.serviceDocument.open("/api/CostChangeHead/OpenCostManagementExcel");
  }

  save(): Observable<ServiceDocument<CostChangeHeadModel>> {
    let currentAction: WorkflowAction = this.serviceDocument.dataProfile.actionService.allowedActions
      .find((v) => v.transitionClaim === "CostStartToCreatedClaim");
    if (!this.serviceDocument.dataProfile.dataModel.cosT_CHANGE && currentAction) {
      this.serviceDocument.dataProfile.profileForm.controls["currentActionId"].setValue(currentAction.transitionClaim);
      return this.serviceDocument.submit("/api/CostChangeHead/Submit", true);
    } else {
      return this.serviceDocument.save("/api/CostChangeHead/Save", true, () => this.setDate());
    }
  }

  submit(): Observable<ServiceDocument<CostChangeHeadModel>> {
    return this.serviceDocument.submit("/api/CostChangeHead/Submit", true);
  }

  addEdit(id: number): Observable<ServiceDocument<CostChangeHeadModel>> {
    if (+id === 0) {
      return this.serviceDocument.new("/api/CostChangeHead/New");
    } else {
      return this.serviceDocument.open("/api/CostChangeHead/Open", new HttpParams().set("id", id.toString()));
    }
  }

  setDate(): void {
    let sd: CostChangeHeadModel = this.serviceDocument.dataProfile.dataModel;
    if (sd !== null) {
      if (sd.effectiveStartDate != null) {
        sd.effectiveStartDate = this.sharedService.setOffSet(sd.effectiveStartDate);
      }
      if (sd.effectiveEndDate != null) {
        sd.effectiveEndDate = this.sharedService.setOffSet(sd.effectiveEndDate);
      }
    }
  }
  reject(): Observable<ServiceDocument<CostChangeHeadModel>> {
    return this.serviceDocument.submit("/api/CostChangeHead/Reject", true);
  }
}
