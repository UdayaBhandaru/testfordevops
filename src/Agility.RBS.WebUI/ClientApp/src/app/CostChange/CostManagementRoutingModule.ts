import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CostChangeListComponent } from './CostChangeListComponent';
import { CostChangeListResolver, CostChangeNewResolver } from './CostManagementResolver';
import { CostChangeHeadComponent } from './CostChangeHeadComponent';

const CostMgmtRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "", redirectTo: "List", pathMatch: "full"
      },
      {
        path: "List",
        component: CostChangeListComponent,
        resolve:
        {
          serviceDocument: CostChangeListResolver
        }
      },
      {
        path: "New/:id",
        component: CostChangeHeadComponent,
        resolve: {
          serviceDocument: CostChangeNewResolver
        }
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(CostMgmtRoutes)
  ]
})
export class CostManagementRoutingModule { }
