export class CostChangeSearchModel {
  reason?: number;
  divisionId?:number;
  divisionName?: string;
  supplierId?: number;
  supName?: string;
  cosT_CHANGE_ID?: number;
  costChangeDesc?: string;
  costChangeStatus?: string;
  costChangeStatusDesc?: string;
  itemBarcode?: string;
  itemDesc?: string;
  reasonDesc?: string;
  startRow?: number;
  endRow?: number;
  totalRows?: number;
  sortModel?: {}[];
}
