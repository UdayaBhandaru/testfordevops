﻿import { CostChangeItemPriceModel } from "./CostChangeItemPriceModel";
import { CostChangeMarketPriceModel } from "./CostChangeMarketPriceModel";

export class CostChangeItemGroupsModel {
    supplierId?: number;
    suppName: string;
    rebates?: number;
    itemBarcode: string;
    itemDesc: string;
    unitOfMeasure: string;
    uomDesc: string;
    uomUnit: string;
    parentItem: string;
    parentItemDesc: string;
    deptId?: number;
    deptDesc: string;
    itemTier: string;
    caseCost?: number;
    costCurCode: string;
    unitCost?: number;
    annualDiscount?: number;
    netUnitCost?: number;
    variance?: number;
    averageCategoryMargin?: number;
    marketPrice?: number;
    operation: string;
    itemPrices: CostChangeItemPriceModel[];
    listOfCostChangeMarketPriceExceclModels: CostChangeMarketPriceModel[];
}