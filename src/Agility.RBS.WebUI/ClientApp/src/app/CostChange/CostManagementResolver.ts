import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { CostChangeHeadModel } from './CostChangeHeadModel';
import { CostChangeService } from './CostChangeService';
import { CostChangeListService } from './CostChangeListService';
import { CostChangeSearchModel } from './CostChangeSearchModel';

@Injectable()
export class CostChangeListResolver implements Resolve<ServiceDocument<CostChangeSearchModel>> {
    constructor(private service: CostChangeListService) { }
  resolve(): Observable<ServiceDocument<CostChangeSearchModel>> {
        return this.service.list();
    }
}

@Injectable()
export class CostChangeNewResolver implements Resolve<ServiceDocument<CostChangeHeadModel>> {
  constructor(private service: CostChangeService) { }
  resolve(route: ActivatedRouteSnapshot): Observable<ServiceDocument<CostChangeHeadModel>> {
    return this.service.addEdit(route.params["id"].toString());
  }
}
