import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { RbsSharedModule } from '../../RbsSharedModule';
import { MatExpansionModule } from '@angular/material';
import { TitleSharedModule } from '../../TitleSharedModule';
import { AgGridSharedModule } from '../../AgGridSharedModule';
import { ObligationInboxRoutingModule } from './ObligationInboxRoutingModule';
import { ObligationInboxComponent } from './ObligationInboxComponent';
import { ObligationInboxService } from './ObligationInboxService';
import { ObligationInboxResolver } from './ObligationInboxResolver';

@NgModule({
    imports: [
        FrameworkCoreModule,
        ObligationInboxRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        MatExpansionModule,
        RbsSharedModule
    ],
    declarations: [
        ObligationInboxComponent
    ],
    providers: [
        ObligationInboxService,
        ObligationInboxResolver
    ]
})
export class ObligationInboxModule {
}
