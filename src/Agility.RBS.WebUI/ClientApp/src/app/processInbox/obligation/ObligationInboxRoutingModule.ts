import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ObligationInboxComponent } from './ObligationInboxComponent';
import { ObligationInboxResolver } from './ObligationInboxResolver';


const InboxRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
              component: ObligationInboxComponent,
                resolve:
                {
                  serviceDocument: ObligationInboxResolver
                }
            }
        ]
    }
];


@NgModule({
    imports: [
        RouterModule.forChild(InboxRoutes)
    ]
})
export class ObligationInboxRoutingModule { }
