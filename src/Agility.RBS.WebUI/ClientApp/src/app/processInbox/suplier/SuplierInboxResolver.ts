import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument, FxContext } from "@agility/frameworkcore";
import { InboxModel } from '../../inbox/InboxModel';
import { SuplierInboxService } from './SuplierInboxService';

@Injectable()
export class SuplierInboxResolver implements Resolve<ServiceDocument<InboxModel>> {
    userDataObj: any;
  constructor(private service: SuplierInboxService, private _fxContext: FxContext) { }
    resolve(route: ActivatedRouteSnapshot): Observable<ServiceDocument<InboxModel>> {
        this.userDataObj = this._fxContext.userProfile;
      return this.service.list("PDG","SUPPLIER");
    }
}
