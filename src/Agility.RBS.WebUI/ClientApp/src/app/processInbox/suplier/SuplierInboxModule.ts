import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { RbsSharedModule } from '../../RbsSharedModule';
import { MatExpansionModule } from '@angular/material';
import { TitleSharedModule } from '../../TitleSharedModule';
import { AgGridSharedModule } from '../../AgGridSharedModule';
import { SuplierInboxRoutingModule } from './SuplierInboxRoutingModule';
import { SuplierInboxComponent } from './SuplierInboxComponent';
import { SuplierInboxService } from './SuplierInboxService';
import { SuplierInboxResolver } from './SuplierInboxResolver';

@NgModule({
  imports: [
    FrameworkCoreModule,
    SuplierInboxRoutingModule,
    AgGridSharedModule,
    TitleSharedModule,
    MatExpansionModule,
    RbsSharedModule
  ],
  declarations: [
    SuplierInboxComponent
  ],
  providers: [
    SuplierInboxService,
    SuplierInboxResolver
  ]
})
export class SuplierInboxModule {
}
