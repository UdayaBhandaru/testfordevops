import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { SuplierInboxComponent } from './SuplierInboxComponent';
import { SuplierInboxResolver } from './SuplierInboxResolver';


const InboxRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
              component:SuplierInboxComponent,
                resolve:
                {
                  serviceDocument: SuplierInboxResolver
                }
            }
        ]
    }
];


@NgModule({
    imports: [
        RouterModule.forChild(InboxRoutes)
    ]
})
export class SuplierInboxRoutingModule { }
