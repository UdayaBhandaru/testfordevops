import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { RbsSharedModule } from '../../RbsSharedModule';
import { MatExpansionModule } from '@angular/material';
import { TitleSharedModule } from '../../TitleSharedModule';
import { AgGridSharedModule } from '../../AgGridSharedModule';
import { PromoInboxRoutingModule } from './PromoInboxRoutingModule';
import { PromoInboxComponent } from './PromoInboxComponent';
import { PromoInboxService } from './PromoInboxService';
import { PromoInboxResolver } from './PromoInboxResolver';

@NgModule({
    imports: [
        FrameworkCoreModule,
        PromoInboxRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        MatExpansionModule,
        RbsSharedModule
    ],
    declarations: [
        PromoInboxComponent
    ],
    providers: [
        PromoInboxService,
        PromoInboxResolver
    ]
})
export class PromoInboxModule {
}
