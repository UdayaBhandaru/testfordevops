import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { PromoInboxComponent } from './PromoInboxComponent';
import { PromoInboxResolver } from './PromoInboxResolver';


const InboxRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: PromoInboxComponent,
                resolve:
                {
                    serviceDocument: PromoInboxResolver
                }
            }
        ]
    }
];


@NgModule({
    imports: [
        RouterModule.forChild(InboxRoutes)
    ]
})
export class PromoInboxRoutingModule { }
