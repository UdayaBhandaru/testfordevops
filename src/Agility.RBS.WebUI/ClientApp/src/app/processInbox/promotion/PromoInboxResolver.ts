import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument, FxContext } from "@agility/frameworkcore";
import { PromoInboxService } from './PromoInboxService';
import { InboxModel } from '../../inbox/InboxModel';

@Injectable()
export class PromoInboxResolver implements Resolve<ServiceDocument<InboxModel>> {
    userDataObj: any;
  constructor(private service: PromoInboxService, private _fxContext: FxContext) { }
    resolve(route: ActivatedRouteSnapshot): Observable<ServiceDocument<InboxModel>> {
        this.userDataObj = this._fxContext.userProfile;
        return this.service.list("PDG","PROMO");
    }
}
