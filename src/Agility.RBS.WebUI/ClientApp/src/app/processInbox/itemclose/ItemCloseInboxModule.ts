import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { RbsSharedModule } from '../../RbsSharedModule';
import { MatExpansionModule } from '@angular/material';
import { TitleSharedModule } from '../../TitleSharedModule';
import { AgGridSharedModule } from '../../AgGridSharedModule';
import { ItemCloseInboxRoutingModule } from './ItemCloseInboxRoutingModule';
import { ItemCloseInboxResolver } from './ItemCloseInboxResolver';
import { ItemCloseInboxService } from './ItemCloseInboxService';
import { ItemCloseInboxComponent } from './ItemCloseInboxComponent';

@NgModule({
    imports: [
        FrameworkCoreModule,
        ItemCloseInboxRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        MatExpansionModule,
        RbsSharedModule
    ],
    declarations: [
        ItemCloseInboxComponent
    ],
    providers: [
        ItemCloseInboxService,
        ItemCloseInboxResolver
    ]
})
export class ItemCloseInboxModule {
}
