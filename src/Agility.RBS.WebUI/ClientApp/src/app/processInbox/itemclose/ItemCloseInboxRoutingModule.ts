import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ItemCloseInboxComponent } from './ItemCloseInboxComponent';
import { ItemCloseInboxResolver } from './ItemCloseInboxResolver';


const InboxRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: ItemCloseInboxComponent,
                resolve:
                {
                  serviceDocument: ItemCloseInboxResolver
                }
            }
        ]
    }
];


@NgModule({
    imports: [
        RouterModule.forChild(InboxRoutes)
    ]
})
export class ItemCloseInboxRoutingModule { }
