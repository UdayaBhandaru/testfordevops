import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { RtvInboxComponent } from './RtvInboxComponent';
import { RtvInboxResolver } from './RtvInboxResolver';


const InboxRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: RtvInboxComponent,
                resolve:
                {
                    serviceDocument: RtvInboxResolver
                }
            }
        ]
    }
];


@NgModule({
    imports: [
        RouterModule.forChild(InboxRoutes)
    ]
})
export class RtvInboxRoutingModule { }
