import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { RbsSharedModule } from '../../RbsSharedModule';
import { MatExpansionModule } from '@angular/material';
import { TitleSharedModule } from '../../TitleSharedModule';
import { AgGridSharedModule } from '../../AgGridSharedModule';
import { RtvInboxService } from './RtvInboxService';
import { RtvInboxResolver } from './RtvInboxResolver';
import { RtvInboxComponent } from './RtvInboxComponent';
import { RtvInboxRoutingModule } from './RtvInboxRoutingModule';

@NgModule({
    imports: [
        FrameworkCoreModule,
        RtvInboxRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        MatExpansionModule,
        RbsSharedModule
    ],
    declarations: [
        RtvInboxComponent
    ],
    providers: [
        RtvInboxService,
        RtvInboxResolver
    ]
})
export class RtvInboxModule {
}
