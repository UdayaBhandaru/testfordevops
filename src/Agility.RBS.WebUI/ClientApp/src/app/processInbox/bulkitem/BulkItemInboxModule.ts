import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { RbsSharedModule } from '../../RbsSharedModule';
import { MatExpansionModule } from '@angular/material';
import { TitleSharedModule } from '../../TitleSharedModule';
import { AgGridSharedModule } from '../../AgGridSharedModule';
import { BulkItemInboxRoutingModule } from './BulkItemInboxRoutingModule';
import { BulkItemInboxComponent } from './BulkItemInboxComponent';
import { BulkItemInboxService } from './bulkitemInboxService';
import { BulkItemInboxResolver } from './BulkItemInboxResolver';

@NgModule({
    imports: [
        FrameworkCoreModule,
        BulkItemInboxRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        MatExpansionModule,
        RbsSharedModule
    ],
    declarations: [
        BulkItemInboxComponent
    ],
    providers: [
        BulkItemInboxService,
        BulkItemInboxResolver
    ]
})
export class BulkItemInboxModule {
}
