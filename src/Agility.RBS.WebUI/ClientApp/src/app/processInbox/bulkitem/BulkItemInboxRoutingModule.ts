import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { BulkItemInboxComponent } from './BulkItemInboxComponent';
import { BulkItemInboxResolver } from './BulkItemInboxResolver';


const InboxRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
              component: BulkItemInboxComponent,
                resolve:
                {
                  serviceDocument: BulkItemInboxResolver
                }
            }
        ]
    }
];


@NgModule({
    imports: [
        RouterModule.forChild(InboxRoutes)
    ]
})
export class BulkItemInboxRoutingModule { }
