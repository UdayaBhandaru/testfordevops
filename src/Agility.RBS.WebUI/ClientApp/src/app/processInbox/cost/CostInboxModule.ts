import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { RbsSharedModule } from '../../RbsSharedModule';
import { MatExpansionModule } from '@angular/material';
import { TitleSharedModule } from '../../TitleSharedModule';
import { AgGridSharedModule } from '../../AgGridSharedModule';
import { CostInboxService } from './CostInboxService';
import { CostInboxResolver } from './CostInboxResolver';
import { CostInboxComponent } from './CostInboxComponent';
import { CostInboxRoutingModule } from './CostInboxRoutingModule';

@NgModule({
    imports: [
        FrameworkCoreModule,
        CostInboxRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        MatExpansionModule,
        RbsSharedModule
    ],
    declarations: [
        CostInboxComponent
    ],
    providers: [
        CostInboxService,
        CostInboxResolver
    ]
})
export class CostInboxModule {
}
