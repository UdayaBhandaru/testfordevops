import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CostInboxComponent } from './CostInboxComponent';
import { CostInboxResolver } from './CostInboxResolver';


const InboxRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: CostInboxComponent,
                resolve:
                {
                    serviceDocument: CostInboxResolver
                }
            }
        ]
    }
];


@NgModule({
    imports: [
        RouterModule.forChild(InboxRoutes)
    ]
})
export class CostInboxRoutingModule { }
