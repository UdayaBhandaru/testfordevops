import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { GridOptions, GridReadyEvent, ColumnApi, GridApi } from "ag-grid-community";
import { ServiceDocument } from '@agility/frameworkcore';
import { DatePipe } from '@angular/common';
import { FormGroup, FormControl } from '@angular/forms';
import { InboxModel } from '../../inbox/InboxModel';
import { WorkflowHistoryModel } from '../../inbox/WorkflowHistoryModel';
import { DocumentHeaderModel } from '../../Documents/DocumentHeaderModel';
import { InboxCountCommonModel } from '../../Common/DomainData/InboxCountCommonModel';
import { DocumentsConstants } from '../../Common/DocumentsConstants';
import { SharedService } from '../../Common/SharedService';
import { DocumentsService } from '../../Documents/DocumentsService';
import { GridImageComponent } from '../../inbox/GridImageComponent';
import { InboxRequestIdComponent } from '../../inbox/InboxRequestIdComponent';
import { GridDateComponent } from '../../Common/grid/GridDateComponent';
import { InboxPriorityComponent } from '../../inbox/InboxPriorityComponent';
import { InboxFilesComponent } from '../../inbox/InboxFilesComponent';
import { InboxActionsComponent } from '../../inbox/InboxActionsComponent';
import { InboxCommentsComponent } from '../../inbox/InboxCommentsComponent';
import { InboxPopupComponent } from '../../inbox/InboxPopupComponent';
import { WorkflowHistoryPopupComponent } from '../../inbox/WorkflowHistoryPopupComponent';
import { DocumentsPopupComponent } from '../../Documents/DocumentsPopupComponent';
import { DocumentDetailModel } from '../../Documents/DocumentDetailModel';
import { TransferInboxService } from './TransferInboxService';


@Component({
  selector: "inbox",
  templateUrl: "./TransferInboxComponent.html",
  styleUrls: ['./TransferInboxcomponent.scss']
})
export class TransferInboxComponent implements OnInit {
  gridColumnApi: ColumnApi;
  gridApi: GridApi;
  public viewRequests: string;
  public serviceDocument: ServiceDocument<InboxModel>;
  public selectedValue: string;
  public gridOptions: GridOptions;
  public componentName: any;
  public columns: {}[];
  public selectedTabIndex: number = 0;
  public filteredDataList: InboxModel[] = []; public clonedFilteredData: InboxModel[] = [];
  public redirectUrl: string;
  public workflowHistoryListObj: WorkflowHistoryModel[] = [];
  public documentHeaderDataList: DocumentHeaderModel[] = [];
  public documentDetailsDataList: DocumentDetailModel[] = [];
  public inboxCountsData: InboxCountCommonModel;
  public PageTitle: string = "TRANSFER INBOX";
  public searchFormGroup: FormGroup;
  documentsConstants: DocumentsConstants = new DocumentsConstants();

  constructor(private inboxService: TransferInboxService, private route: ActivatedRoute,
    private sharedService: SharedService, private router: Router, private datePipe: DatePipe,
    private documentService: DocumentsService) {
  }

  public ngOnInit(): void {
    this.componentName = this;
    this.searchFormGroup = new FormGroup({
      "search": new FormControl()
    });

    this.columns = [
      {
        headerName: "Sender", field: "senderName", cellRendererFramework: GridImageComponent,
        width: 170, filter: "agTextColumnFilter", suppressSizeToFit: true
      },

      {
        headerName: "Request Id", field: "profileInstanceID", cellRendererFramework: InboxRequestIdComponent,
        width: 110, filter: "agTextColumnFilter", suppressSizeToFit: true
      },
      {
        headerName: "Recepient", field: "recipientName", width: 157, filter: "agTextColumnFilter", suppressSizeToFit: true, hide: true
      },
      {
        headerName: "GroupColumn", field: "dateGroup", tooltipField: "GroupColumn", hide: "true", filter: "agTextColumnFilter",
        rowGroup: true,
        cellRendererParams: {
          suppressCount: true,
          checkbox: false,
          padding: 20,

        }
      },
      {
        headerName: "Received", field: "reqDateCreated", tooltipField: "reqDateCreated | date:'dd-MMM - yyyy'"
        , width: 89, cellRendererFramework: GridDateComponent, filter: "date", suppressSizeToFit: true
      },
      {
        headerName: "Status", field: "actionName", tooltipField: "actionName", width: 182, filter: "agTextColumnFilter", suppressSizeToFit: true
      },
      {
        headerName: "Priority", field: "status", width: 102, cellRendererFramework: InboxPriorityComponent
        , suppressSizeToFit: true
      },
      {
        headerName: "Subject", field: "subject", tooltipField: "subject", suppressFilter: true
      },
      {
        headerName: "Due", field: "dueDays", tooltipField: "dueDays", width: 89, suppressFilter: true,
        suppressSizeToFit: true
      },
      {
        headerName: "Files", field: "fileCount", cellRendererFramework: InboxFilesComponent, maxWidth: 74,
        suppressFilter: true
      },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: InboxActionsComponent, width: 98,
        suppressFilter: true, suppressSizeToFit: true
      }
    ];
    this.gridOptions = {
      onGridReady: (params: GridReadyEvent): void => {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;

        if (this.gridColumnApi) {
          if (this.selectedTabIndex === 0) {
            this.gridColumnApi.setColumnsVisible(["senderName", "actionName"], true);
            this.gridColumnApi.setColumnVisible("recipientName", false);
          } else if (this.selectedTabIndex === 1) {
            this.gridColumnApi.setColumnsVisible(["recipientName", "actionName", "recipientName"], true);
            this.gridColumnApi.setColumnVisible("senderName", false);
          } else if (this.selectedTabIndex === 2) {
            this.gridColumnApi.setColumnsVisible(["senderName", "recipientName"], true);
            this.gridColumnApi.setColumnVisible("actionName", false);
          } else if (this.selectedTabIndex === 3) {
            this.gridColumnApi.setColumnVisible("senderName", true);
            this.gridColumnApi.setColumnVisible("recipientName", false);
            this.gridColumnApi.setColumnVisible("actionName", false);
          }
        }
      },
      context: {
        componentParent: this
      },
      getRowHeight: function (params: any): number {
        if (params.node.group) {
          return 20;
        } else {
          return 40;
        }
      },
      enableSorting: false,
      rowSelection: "multiple",
      suppressRowClickSelection: true,
      suppressCellSelection: true,
      groupUseEntireRow: true,
      groupDefaultExpanded: -1,
    };

    this.route.data.subscribe(() => {
      this.serviceDocument = this.inboxService.serviceDocument;
      this.filteredDataList = this.inboxService.serviceDocument.dataProfile.dataList; // page loaded with First Pending tab
      Object.assign(this.clonedFilteredData, this.filteredDataList);
      this.filterData();
    });
    this.inboxCountsData = Object.assign({}, this.inboxService.serviceDocument.domainData["inboxCounts"]);

  }

  public open(cell: any, mode: string): void {
    if (mode === "view") {
      this.fetchQuickViewItem(cell, mode);
    } else if (mode === "history") {
      this.viewWorkflowHistoryFromInbox(cell, mode);
    } else if (mode === "comments") {
      this.sharedService.openDilogWithInputData(InboxCommentsComponent, cell, "Comments");
    }
  }

  public fetchQuickViewItem(cell: any, mode: string): void {
    this.inboxService.itemQuickView(cell.data.dataProfileID, cell.data.workFlowID, cell.data.profileInstanceID).subscribe(() => {
      this.sharedService.openPopupWithDataList(
        InboxPopupComponent,
        this.inboxService.serviceDocumentQuickView.dataProfile.dataList,
        "Inbox QuickView",
        cell.data.moduleName,
        cell.data.profileInstanceID,
        false,
        { viewMode: this.selectedTabIndex !== 0, cellData: cell.data }
      );
    });
  }

  public routeToReqPage(params: any, moduleName: string): void {
    this.sharedService.previousUrl = "../Inbox";
    this.sharedService.setToolbarSave(this.selectedTabIndex === 0 || this.selectedTabIndex == null || this.selectedTabIndex === 3);
    this.sharedService.fileCount = params.data.fileCount;
    this.redirectUrl = this.documentsConstants.modulesHeadPageRoutes.find(x => x.key === moduleName || x.key.toUpperCase() === moduleName.toUpperCase()).value;
    this.router.navigate(["/" + this.redirectUrl + +(params.value)], { skipLocationChange: true });
  }

  public routeToItemPage(cell: any): void {
    this.sharedService.previousUrl = "../Inbox";
    this.sharedService.setToolbarSave(this.selectedTabIndex === 0 || this.selectedTabIndex == null || this.selectedTabIndex === 3);
    this.router.navigate(["/" + this.redirectUrl + +(cell.value)], { skipLocationChange: true });
  }

  public viewWorkflowHistoryFromInbox(cell: any, mode: string): void {
    this.inboxService.fetchWorkflowHistory(cell.data.moduleName, +(cell.data.profileInstanceID)).subscribe(() => {
      this.workflowHistoryListObj = this.inboxService.serviceDocumentWorkflowHistory.dataProfile.dataList;
      this.sharedService.openPopupWithDataList(
        WorkflowHistoryPopupComponent,
        this.workflowHistoryListObj,
        "Inbox Workflow",
        cell.data.moduleName,
        cell.data.profileInstanceID
      );
    });
  }

  public viewDueRequests(view: string): void {

    if (this.viewRequests !== "due" || this.selectedTabIndex !== 0 && this.viewRequests === null) {
      this.viewRequests = "due";
      this.tabOnClick(null);
    } else {
      this.viewRequests = null;
      this.tabOnClick(null);
    }
  }

  public viewHighPriorityRequests(view: string): void {
    if (this.viewRequests !== "highPriority" || this.selectedTabIndex !== 0 && this.viewRequests === null) {
      this.viewRequests = "highPriority";
      this.tabOnClick(null);
    } else {
      this.viewRequests = null;
      this.tabOnClick(null);
    }
  }

  public tabOnClick(eventData: number): void {
    this.clonedFilteredData = [];
    if (eventData === null) {
      this.selectedTabIndex = 0;
    } else {
      this.selectedTabIndex = eventData;
      this.viewRequests = null;
    }
    this.filteredDataList = [];
    let inboxTabType: string;
    if (this.selectedTabIndex === 0) {
      inboxTabType = "PDG";
    } else if (this.selectedTabIndex === 1) {
      inboxTabType = "SNT";
    } else if (this.selectedTabIndex === 2) {
      inboxTabType = "COM";
    } else if (this.selectedTabIndex === 3) {
      inboxTabType = "DFT";
    }
    this.inboxService.list(inboxTabType,"TRANSFER").subscribe(() => {
      this.serviceDocument = this.inboxService.serviceDocument;
      this.filteredDataList = this.inboxService.serviceDocument.dataProfile.dataList;
      if (!this.filteredDataList) {
        this.filteredDataList = [];
      }
      if (this.viewRequests === "due") {
        this.filteredDataList = this.filteredDataList.filter((r: InboxModel) => r.dueDays === "Overdue");
      } else if (this.viewRequests === "highPriority") {
        this.filteredDataList = this.filteredDataList.filter((r: InboxModel) => r.status === "H");
      }
      Object.assign(this.clonedFilteredData, this.filteredDataList);
      this.inboxCountsData = Object.assign({}, this.inboxService.serviceDocument.domainData["inboxCounts"]);
      this.filterData();
    });
  }

  public onDocumentsClick(cell: any): void {
    this.fetchDocumentList(cell);
  }

  public fetchDocumentList(cell: any): void {
    this.documentService.fetchDocumentList(cell.data.moduleName, cell.data.profileInstanceID).subscribe(() => {
      if (this.documentService.serviceDocument.dataProfile.dataList) {
        this.documentHeaderDataList = this.documentService.serviceDocument.dataProfile.dataList;
        this.documentDetailsDataList = this.documentHeaderDataList[0].documentDetails;
      }
      this.sharedService.openPopupWithDataList(
        DocumentsPopupComponent,
        this.documentDetailsDataList,
        "Document Upload",
        cell.data.moduleName,
        cell.data.profileInstanceID,
        false
      );
    });
  }

  public filterData(clearData: boolean = false): void {
    let searchValue: string = null;
    if (!clearData) {
      searchValue = this.searchFormGroup.controls["search"].value;
    } else {
      this.searchFormGroup.controls["search"].setValue(null);
    }
    if (searchValue) {
      this.filteredDataList = this.filteredDataList.filter((x: InboxModel) => {
        return Object.keys(x).find((key: string) => {
          return (x["moduleName"] + " " + x[key]).toLowerCase().indexOf(searchValue.toLowerCase()) > -1;
        });
      });
    } else {
      this.filteredDataList = this.clonedFilteredData;
      if (this.gridApi) {
        this.gridApi.redrawRows();
      }
    }
  }

  filterGrid($event: MouseEvent, clearData: boolean = false): void {
    this.filterData(clearData);
  }
}
