import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { RbsSharedModule } from '../../RbsSharedModule';
import { MatExpansionModule } from '@angular/material';
import { TitleSharedModule } from '../../TitleSharedModule';
import { AgGridSharedModule } from '../../AgGridSharedModule';
import { TransferInboxRoutingModule } from './TransferInboxRoutingModule';
import { TransferInboxComponent } from './TransferInboxComponent';
import { TransferInboxService } from './TransferInboxService';
import { TransferInboxResolver } from './TransferInboxResolver';

@NgModule({
    imports: [
        FrameworkCoreModule,
        TransferInboxRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        MatExpansionModule,
        RbsSharedModule
    ],
    declarations: [
        TransferInboxComponent
    ],
    providers: [
        TransferInboxService,
        TransferInboxResolver
    ]
})
export class TransferInboxModule {
}
