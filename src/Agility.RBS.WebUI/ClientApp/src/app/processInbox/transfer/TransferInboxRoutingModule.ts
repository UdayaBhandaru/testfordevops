import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { TransferInboxComponent } from './TransferInboxComponent';
import { TransferInboxResolver } from './TransferInboxResolver';


const InboxRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
              component: TransferInboxComponent,
                resolve:
                {
                  serviceDocument: TransferInboxResolver
                }
            }
        ]
    }
];


@NgModule({
    imports: [
        RouterModule.forChild(InboxRoutes)
    ]
})
export class TransferInboxRoutingModule { }
