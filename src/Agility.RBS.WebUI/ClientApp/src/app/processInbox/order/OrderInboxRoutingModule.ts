import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { OrderInboxComponent } from './OrderInboxComponent';
import { OrderInboxResolver } from './OrderInboxResolver';


const InboxRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: OrderInboxComponent,
                resolve:
                {
                    serviceDocument: OrderInboxResolver
                }
            }
        ]
    }
];


@NgModule({
    imports: [
        RouterModule.forChild(InboxRoutes)
    ]
})
export class OrderInboxRoutingModule { }
