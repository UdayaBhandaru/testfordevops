import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { RbsSharedModule } from '../../RbsSharedModule';
import { MatExpansionModule } from '@angular/material';
import { TitleSharedModule } from '../../TitleSharedModule';
import { AgGridSharedModule } from '../../AgGridSharedModule';
import { OrderInboxRoutingModule } from './OrderInboxRoutingModule';
import { OrderInboxComponent } from './OrderInboxComponent';
import { OrderInboxService } from './OrderInboxService';
import { OrderInboxResolver } from './OrderInboxResolver';

@NgModule({
    imports: [
        FrameworkCoreModule,
        OrderInboxRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        MatExpansionModule,
        RbsSharedModule
    ],
    declarations: [
        OrderInboxComponent
    ],
    providers: [
        OrderInboxService,
        OrderInboxResolver
    ]
})
export class OrderInboxModule {
}
