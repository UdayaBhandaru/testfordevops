import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument, FxContext } from "@agility/frameworkcore";
import { PriceInboxService } from './PriceInboxService';
import { InboxModel } from '../../inbox/InboxModel';

@Injectable()
export class PriceInboxResolver implements Resolve<ServiceDocument<InboxModel>> {
    userDataObj: any;
    constructor(private service: PriceInboxService, private _fxContext: FxContext) { }
    resolve(route: ActivatedRouteSnapshot): Observable<ServiceDocument<InboxModel>> {
        this.userDataObj = this._fxContext.userProfile;
        return this.service.list("PDG","PRICE");
    }
}
