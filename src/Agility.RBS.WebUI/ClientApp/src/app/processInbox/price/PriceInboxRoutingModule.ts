import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { PriceInboxResolver } from './PriceInboxResolver';
import { PriceInboxComponent } from './PriceInboxComponent';


const InboxRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: PriceInboxComponent,
                resolve:
                {
                    serviceDocument: PriceInboxResolver
                }
            }
        ]
    }
];


@NgModule({
    imports: [
        RouterModule.forChild(InboxRoutes)
    ]
})
export class PriceInboxRoutingModule { }
