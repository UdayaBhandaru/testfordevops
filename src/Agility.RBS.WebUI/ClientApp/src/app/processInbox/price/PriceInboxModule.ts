import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { PriceInboxComponent } from './PriceInboxComponent';
import { PriceInboxService } from './PriceInboxService';
import { PriceInboxResolver } from './PriceInboxResolver';
import { RbsSharedModule } from '../../RbsSharedModule';
import { MatExpansionModule } from '@angular/material';
import { PriceInboxRoutingModule } from './PriceInboxRoutingModule';
import { TitleSharedModule } from '../../TitleSharedModule';
import { AgGridSharedModule } from '../../AgGridSharedModule';

@NgModule({
    imports: [
        FrameworkCoreModule,
        PriceInboxRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        MatExpansionModule,
        RbsSharedModule
    ],
    declarations: [
        PriceInboxComponent
    ],
    providers: [
        PriceInboxService,
        PriceInboxResolver
    ]
})
export class PriceInboxModule {
}
