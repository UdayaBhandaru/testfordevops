import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { RbsSharedModule } from '../../RbsSharedModule';
import { MatExpansionModule } from '@angular/material';
import { TitleSharedModule } from '../../TitleSharedModule';
import { AgGridSharedModule } from '../../AgGridSharedModule';
import { ItemReactivateInboxRoutingModule } from './ItemReactivateInboxRoutingModule';
import { ItemReactivateInboxComponent } from './ItemReactivateInboxComponent';
import { ItemReactivateInboxService } from './ItemReactivateInboxService';
import { ItemReactivateInboxResolver } from './ItemReactivateInboxResolver';

@NgModule({
    imports: [
        FrameworkCoreModule,
        ItemReactivateInboxRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        MatExpansionModule,
        RbsSharedModule
    ],
    declarations: [
        ItemReactivateInboxComponent
    ],
    providers: [
        ItemReactivateInboxService,
        ItemReactivateInboxResolver
    ]
})
export class ItemReactivateInboxModule {
}
