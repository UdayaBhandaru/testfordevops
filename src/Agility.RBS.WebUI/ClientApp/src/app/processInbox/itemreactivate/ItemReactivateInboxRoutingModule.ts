import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ItemReactivateInboxComponent } from './ItemReactivateInboxComponent';
import { ItemReactivateInboxResolver } from './ItemReactivateInboxResolver';


const InboxRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
              component: ItemReactivateInboxComponent,
                resolve:
                {
                  serviceDocument: ItemReactivateInboxResolver
                }
            }
        ]
    }
];


@NgModule({
    imports: [
        RouterModule.forChild(InboxRoutes)
    ]
})
export class ItemReactivateInboxRoutingModule { }
