﻿import { SharedService } from "../Common/SharedService";
import { RbDialogService } from "../Common/controls/RbDialogService";
import { MessageResult } from "@agility/frameworkcore";
import { MatDialogRef } from "@angular/material";
import { Router } from "@angular/router";

export abstract class ComponentCanDeactivate {
    abstract canDeactivate(): boolean;

    canNavigate: boolean;
    primaryId: number;
    messageResult: MessageResult = new MessageResult();
    serviceDocument: any;
    dialogRef: MatDialogRef<any>;
    data: any;
    orginalValue: string;

    constructor(public sharedService: SharedService, public dialogService: RbDialogService, public router: Router) {
    }

    close(): boolean {
        return true;
    }

    save(saveOnly: boolean): void {
        // need to implement
    }

}