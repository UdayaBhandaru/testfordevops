﻿import { CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { ComponentCanDeactivate } from "./ComponentCanDeactivate";
import { Observable, of } from "rxjs";
import { RbButton } from "../Common/controls/RbControlModels";

export default class RbHeadGuard implements CanDeactivate<ComponentCanDeactivate> {
    canDeactivate(component: ComponentCanDeactivate, route: ActivatedRouteSnapshot, state: RouterStateSnapshot
        , nextState: RouterStateSnapshot): any {
        if (!component.canNavigate) {
            if (+component.primaryId === 0) {
                component.sharedService.errorForm("You Can't navigate other tabs until you save");
                return of(false);
            } else {
                component.messageResult.message = `<div>All your changes will be lost if you don't save them.</div>
                                    <div>Click on “Save” to Keep the changes.</div>
                                    <div>Click on “Don't save” to proceed further without saving changes.</div>
                                    <div>Click on “Cancel” to stay on the current page.</div>`;
                if ((component.serviceDocument.dataProfile.profileForm && component.serviceDocument.dataProfile.profileForm.dirty)
                    || (component.serviceDocument.dataProfile.profileForm.dirty && !component.serviceDocument.dataProfile.profileForm.valid)) {
                    component.dialogRef = component.dialogService.openMessageDialog("Warning", component.messageResult
                        , [new RbButton("", "Cancel", "alertCancel")
                        , new RbButton("", "Don't Save", "alertdontsave")
                        , new RbButton("", "Save", "alertSave")], "37%", "", "iconsdontsave retailsNavIcons", "dontsaveAlert", "dontsaveAlertTitle");
                    return component.dialogRef.componentInstance.click.subscribe(
                        btnName => {
                            if (btnName === "Cancel") {
                                component.dialogRef.close();
                            } else if (btnName === "Don't Save") {
                                component.serviceDocument.dataProfile.profileForm.markAsUntouched();
                                component.canNavigate = true;
                                component.dialogRef.close();
                                component.router.navigate([nextState.url], { skipLocationChange: true });
                            } else if (btnName === "Save") {
                                component.dialogRef.close();
                                component.save(true);
                                component.canNavigate = true;
                                component.router.navigate([nextState.url], { skipLocationChange: true });
                            }
                        });
                }
            }
        }
        return of(true);
    }
}