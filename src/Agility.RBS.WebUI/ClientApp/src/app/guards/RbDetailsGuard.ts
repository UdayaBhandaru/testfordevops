﻿import { CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { Observable, of } from "rxjs";
import { ComponentCanDeactivate } from "./ComponentCanDeactivate";
import { RbButton } from "../Common/controls/RbControlModels";

export default class RbDetailsGuard implements CanDeactivate<ComponentCanDeactivate> {
    canDeactivate(component: ComponentCanDeactivate
        , route: ActivatedRouteSnapshot
        , state: RouterStateSnapshot
        , nextState: RouterStateSnapshot
    ): any {
        let data: string = (component.data != null && component.data.length > 0) ? JSON.stringify(component.data) : "";
        if (component.canNavigate) {
            return of(true);
        } else if (data !== component.orginalValue) {
            component.messageResult.message = `<div>All your changes will be lost if you don't save them.</div>
                                    <div>Click on “Save” to Keep the changes.</div>
                                    <div>Click on “Don't save” to proceed further without saving changes.</div>
                                    <div>Click on “Cancel” to stay on the current page.</div>`;
            component.dialogRef = component.dialogService.openMessageDialog("Warning", component.messageResult
                , [new RbButton("", "Cancel", "alertCancel"), new RbButton("", "Don't Save", "alertdontsave")
                    , new RbButton("", "Save", "alertSave")], "37%", "", "", "dontsaveAlert", "dontsaveAlertTitle");
            return component.dialogRef.componentInstance.click.subscribe(btnName => {
                if (btnName === "Don't Save") {
                    component.canNavigate = true;
                    component.router.navigate([nextState.url], { skipLocationChange: true });
                }
                if (btnName === "Save") {
                    component.canNavigate = true;
                    component.save(false);
                    // component.router.navigate([nextState.url], { skipLocationChange: true });
                }
            });
        } else {
            return of(true);
        }
    }
}