import { Component, OnInit, TemplateRef, ViewChild, OnDestroy, ChangeDetectorRef, AfterViewChecked } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ServiceDocument, CommonService, MessageResult, MessageType } from "@agility/frameworkcore";
import { GridOptions, GridApi, RowNode } from "ag-grid-community";
import { MatDialogRef } from "@angular/material";
import { Subscription } from "rxjs";
import { FormControl, FormGroup } from "@angular/forms";
import { SharedService } from "../../Common/SharedService";
import { LoaderService } from "../../Common/LoaderService";
import { InvAdjustmentService } from "../InvAdjustmentService";
import { InvAdjustmentModel } from "../InvAdjustmentModel";
import { GridSelectComponent } from "../../Common/grid/GridSelectComponent";
import { GridNumericComponent } from "../../Common/grid/GridNumericComponent";
import { GridDeleteComponent } from "../../Common/grid/GridDeleteComponent";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { LocationDomainModel } from "../../Common/DomainData/LocationDomainModel";
import { CommonModel } from "../../Common/CommonModel";
import { GridMerchLvlValComponent } from "../../Common/grid/GridMerchLvlValComponent";
import { ItemInventoryModel } from "../ItemInventoryModel";
import { RbDialogService } from "../../Common/controls/RbDialogService";
import { ItemBasicNewModel } from '../../itemSuper/ItemBasicNewModel';


@Component({
  selector: "InvAdjustmentByItem",
  templateUrl: "./InvAdjustmentByItemComponent.html",
  styleUrls: ['./InvAdjustmentByItemComponent.scss']
})
export class InvAdjustmentByItemComponent implements OnInit, AfterViewChecked, OnDestroy {
  public serviceDocument: ServiceDocument<InvAdjustmentModel>;
  componentName: any;
  PageTitle: string;
  localizationData: any;
  gridOptions: GridOptions;
  gridApi: GridApi;
  columns: {}[];
  model: InvAdjustmentModel;
  data: InvAdjustmentModel[];
  deletedRows: any[] = [];
  saveServiceSubscription: Subscription;
  saveSuccessSubscription: Subscription;
  inventoryStatusData: DomainDetailModel[];
  locTypeData: DomainDetailModel[];
  locationData: LocationDomainModel[]; storeLocationsData: LocationDomainModel[]; whsLocationsData: LocationDomainModel[];
  reasonData: CommonModel[];
  searchServiceSubscription: Subscription; searchService2Subscription: Subscription;
  mandatoryFieldsInGrid: string[] = [
    "locationType",
    "locationId",
    "adjQty",
    "reason"
  ];
  stockApiUrl: string; getItemBasicApiUrl: string;
  currentRowIndex: number;
  public messageResult: MessageResult = new MessageResult();
  canNavigate: boolean = false;
  orginalValue: string = "";
  dialogRef: MatDialogRef<any>;

  constructor(public route: ActivatedRoute, public router: Router
    , private sharedService: SharedService, private service: InvAdjustmentService
    , private commonService: CommonService, private loaderService: LoaderService, private changeRef: ChangeDetectorRef
    , public dialogService: RbDialogService) {
    this.PageTitle = "Inventory Adjustment By Item";
    this.data = [];
    this.stockApiUrl = "/api/InvAdjustment/GetItemRelatedData";
    this.getItemBasicApiUrl = "/api/ItemSuper/GetItemBasicDetails";
    this.currentRowIndex = 0;
  }

  ngOnInit(): void {
    this.componentName = this;
    this.inventoryStatusData = Object.assign([], this.service.serviceDocument.domainData["inventoryStatus"]);
    this.locationData = Object.assign([], this.service.serviceDocument.domainData["location"]);
    this.storeLocationsData = this.locationData.filter(item => item.locType === "S");
    this.whsLocationsData = this.locationData.filter(item => item.locType === "W");
    this.locTypeData = Object.assign([], this.service.serviceDocument.domainData["locType"]);
    this.reasonData = Object.assign([], this.service.serviceDocument.domainData["invAdjustmentReason"]);
    this.bind();
  }

  bind(): void {
    this.newInvAdjustmentModel();
    this.service.newModel(this.model);
    this.serviceDocument = this.service.serviceDocument;
    this.setGridColumnsWithOptions();
  }

  setGridColumnsWithOptions(): void {
    this.columns = [
      {
        headerName: "Loc Type", field: "locationType", cellRendererFramework: GridSelectComponent
        , cellRendererParams: { references: this.locTypeData }
        , cellClassRules: {
          "invalid-grid-row": function (params: any): any { return !params.value; }
        }, tooltipField: "locationType", headerTooltip: "Location Type", width: 120
      },
      {
        headerName: "Location", field: "locationId", cellRendererFramework: GridMerchLvlValComponent
        , cellRendererParams: {
          storeLocationsData: this.storeLocationsData, whsLocationsData: this.whsLocationsData
          , keyvaluepair: { key: "locationId", value: "locationDescription" }
        }
        , cellClassRules: {
          "invalid-grid-row": function (params: any): any { return !params.value; }
        }, tooltipField: "locationId", headerTooltip: "Location", width: 140
      },
      {
        headerName: "Division", field: "divisionDesc", tooltipField: "divisionDesc"
        , headerTooltip: "Division", width: 120
      },
      {
        headerName: "Department", field: "deptDesc", tooltipField: "deptDesc"
        , headerTooltip: "Department", width: 120
      },
      {
        headerName: "Category", field: "categoryDesc", tooltipField: "categoryDesc"
        , headerTooltip: "Category", width: 120
      },
      {
        headerName: "Reason", field: "reason", cellRendererFramework: GridSelectComponent
        , cellRendererParams: { references: this.reasonData, keyvaluepair: { key: "reason", value: "reasonDesc" } }
        , cellClassRules: {
          "invalid-grid-row": function (params: any): any { return !params.value; }
        }, tooltipField: "reason", headerTooltip: "Reason"
      },
      {
        headerName: "Adj Qty", field: "adjQty"
        , cellRendererFramework: GridNumericComponent
        , cellClassRules: {
          "invalid-grid-row": function (params: any): any { return !params.value; }
        }, tooltipField: "adjQty", headerTooltip: "Adjustment Qty", width: 100
      },
      {
        headerName: "Total Stock", field: "totalStock", tooltipField: "totalStock", headerTooltip: "Total Stock", width: 140
      },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridDeleteComponent, suppressCellSelection: true
        , width: 100, headerTooltip: "Actions"
      }
    ];

    this.gridOptions = {
      onGridReady: (params: any) => {
        this.gridApi = params.api;
      },
      context: {
        componentParent: this
      },
      enableColResize: true
    };
  }

  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }

  itemBasedLogic(): void {
    this.serviceDocument.dataProfile.profileForm.controls["itemDesc"].setValue(null);
    this.serviceDocument.dataProfile.profileForm.controls["divisionDesc"].setValue(null);
    this.serviceDocument.dataProfile.profileForm.controls["deptDesc"].setValue(null);
    this.serviceDocument.dataProfile.profileForm.controls["categoryDesc"].setValue(null);
    this.serviceDocument.dataProfile.profileForm.controls["classDesc"].setValue(null);
    this.serviceDocument.dataProfile.profileForm.controls["subClassDesc"].setValue(null);
    let item: string = this.serviceDocument.dataProfile.profileForm.controls["item"].value;
    if (item) {
      this.loaderService.display(true);
      this.searchServiceSubscription = this.service.getItemRelatedData(this.getItemBasicApiUrl, item)
        .subscribe((response: ItemBasicNewModel) => {
          if (this.service.serviceDocument.result.type === MessageType.success) {
            if (response) {
              this.service.serviceDocument.dataProfile.profileForm.patchValue({
                itemDesc: response.descUp,
                divisionDesc: response.divisionDesc,
                deptDesc: response.deptDesc,
                categoryDesc: response.categoryDesc,
                classDesc: response.classDesc,
                subClassDesc: response.subclassDesc
              });
            } else {
              this.commonService.showAlert("No data found", "", 1000);
            }
          } else {
            this.commonService.showAlert(this.service.serviceDocument.result.innerException, "", 1000);
          }
          this.changeRef.detectChanges();
          this.loaderService.display(false);
        }, (error: string) => {
          this.loaderService.display(false);
        });
    } else {
      this.sharedService.errorForm("Please enter item");
    }
  }

  add($event: MouseEvent): void {
    $event.preventDefault();
    if (this.service.serviceDocument.dataProfile.profileForm.controls["item"].value) {
      if (this.data.length === 0 || this.data.length > 0 && this.validateRow()) {
        this.addNewRow();
      } else {
        this.sharedService.errorForm("Please enter mandatory fields for current row before adding new row");
      }
    } else {
      this.sharedService.errorForm("Please enter item before adding to grid");
    }
  }

  validateRow(): boolean {
    let valid: boolean = true;
    this.mandatoryFieldsInGrid.forEach(field => {
      if (!this.data[this.currentRowIndex][field]) {
        valid = false;
      }
    });
    return valid;
  }

  addNewRow(): void {
    this.newInvAdjustmentModel();
    this.model["item"] = this.service.serviceDocument.dataProfile.profileForm.controls["item"].value;
    this.model["itemDesc"] = this.service.serviceDocument.dataProfile.profileForm.controls["itemDesc"].value;
    this.model["invStatus"] = this.service.serviceDocument.dataProfile.profileForm.controls["invStatus"].value;
    this.data.unshift(this.model);
    if (this.gridApi) {
      this.gridApi.setRowData(this.data);
    }
  }

  select(cell: any, event: any): void {
    this.data[cell.rowIndex][cell.column.colId] = event.target.value;
    if (cell.column.colId === "locationType") {
      this.data[cell.rowIndex]["locationId"] = null;
    } else if (cell.column.colId === "locationId") {
      this.data[cell.rowIndex]["totalStock"] = null;
      this.data[cell.rowIndex]["divisionDesc"] = null;
      this.data[cell.rowIndex]["deptDesc"] = null;
      this.data[cell.rowIndex]["categoryDesc"] = null;
      this.data[cell.rowIndex]["classDesc"] = null;
      this.data[cell.rowIndex]["subClassDesc"] = null;
      this.searchService2Subscription = this.service.getItemRelatedData(this.stockApiUrl, this.data[cell.rowIndex]["item"],
        this.data[cell.rowIndex]["locationType"], this.data[cell.rowIndex]["locationId"])
        .subscribe((response: ItemInventoryModel) => {
          if (response) {
            this.data[cell.rowIndex]["totalStock"] = response.stockOnHand;
            this.data[cell.rowIndex]["divisionDesc"] = response.divisionDesc;
            this.data[cell.rowIndex]["deptDesc"] = response.deptDesc;
            this.data[cell.rowIndex]["categoryDesc"] = response.categoryDesc;
            this.data[cell.rowIndex]["classDesc"] = response.classDesc;
            this.data[cell.rowIndex]["subClassDesc"] = response.subClassDesc;
            let rowNode: RowNode = this.gridApi.getRowNode(cell.rowIndex);
            rowNode.setData(rowNode.data);
          } else {
            this.sharedService.errorForm("Data not available for this location");
          }
          this.changeRef.detectChanges();
        }, (error: string) => { console.log(error); });
    }
    let rowNode: RowNode = this.gridApi.getRowNode(cell.rowIndex);
    rowNode.setData(rowNode.data);
    this.currentRowIndex = cell.rowIndex;
  }

  update(cell: any, event: any): void {
    this.data[cell.rowIndex][cell.column.colId] = event.target.value;
    let rowNode: RowNode = this.gridApi.getRowNode(cell.rowIndex);
    rowNode.setData(rowNode.data);
    this.currentRowIndex = cell.rowIndex;
  }

  newInvAdjustmentModel(): void {
    this.model = {
      adjQty: null, invStatus: null, item: null, itemDesc: null
      , locationId: null, locationType: null, reason: null, listOfInvAdjustment: [], locName: null, totalStock: null
      , divisionDesc: null, deptDesc: null, categoryDesc: null, classDesc: null, subClassDesc: null
    };
  }

  delete(cell: any): void {
    this.gridApi.updateRowData({ remove: [cell.data] });
    this.deletedRows.push(cell.data);
    this.data.splice(cell.rowIndex, 1);
    if (this.data.length === 0) {
      this.newInvAdjustmentModel();
      this.model["item"] = this.service.serviceDocument.dataProfile.profileForm.controls["item"].value;
      this.model["itemDesc"] = this.service.serviceDocument.dataProfile.profileForm.controls["itemDesc"].value;
      this.model["invStatus"] = this.service.serviceDocument.dataProfile.profileForm.controls["invStatus"].value;
      this.data.unshift(this.model);
      if (this.gridApi) {
        this.gridApi.setRowData(this.data);
      }
    }
  }

  save(saveOnly?: boolean): void {
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.saveSubscription();
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  saveSubscription(): void {
    if (this.data.length > 0 || this.deletedRows.length > 0) {
      if (this.validateData()) {
        this.serviceDocument.dataProfile.profileForm.controls["listOfInvAdjustment"] = new FormControl();
        this.serviceDocument.dataProfile.profileForm.controls["listOfInvAdjustment"].setValue(this.data);
        this.saveServiceSubscription = this.service.save().subscribe(() => {
          if (this.service.serviceDocument.result.type === MessageType.success) {
            this.saveSuccessSubscription = this.sharedService.saveForm("Adjustment Saved Successfully").subscribe(() => {
              this.router.navigate(["/Blank"], {
                skipLocationChange: true, queryParams: {
                  id: "/Inventory/InvAdjustmentByItem"
                }
              });
            });
          } else {
            this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
          }
        });
      } else {
        this.sharedService.errorForm("Please fill Mandatory fields in Grid");
      }
    } else {
      this.sharedService.errorForm("Atleast 1 row is required");
    }
  }

  validateData(): boolean {
    let valid: boolean = true;
    if (this.data != null && this.data.length > 0) {
      this.data.forEach(row => {
        this.mandatoryFieldsInGrid.forEach(field => {
          if (!row[field]) {
            valid = false;
          }
        });
      });
    }
    return valid;
  }

  reset(): void {
    this.newInvAdjustmentModel();
    this.service.newModel(this.model);
    this.serviceDocument = this.service.serviceDocument;
    this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
    this.data = [];
  }

  ngOnDestroy(): void {
    if (this.saveServiceSubscription) {
      this.saveServiceSubscription.unsubscribe();
    }
    if (this.saveSuccessSubscription) {
      this.saveSuccessSubscription.unsubscribe();
    }
    if (this.searchServiceSubscription) {
      this.searchServiceSubscription.unsubscribe();
    }
    if (this.searchService2Subscription) {
      this.searchService2Subscription.unsubscribe();
    }
  }
}
