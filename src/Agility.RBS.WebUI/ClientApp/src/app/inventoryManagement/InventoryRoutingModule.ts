import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { InvAdjustmentByItemComponent } from "./invAdjustmentByItem/InvAdjustmentByItemComponent";
import { InvAdjustmentResolver } from "./InvAdjustmentResolver";
import { InvAdjustmentByLocationComponent } from "./invAdjustmentByLocation/InvAdjustmentByLocationComponent";
import RbDetailsGuard from "../guards/RbDetailsGuard";


const InventoryRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "InvAdjustmentByItem", pathMatch: "full"
            },
            {
                path: "InvAdjustmentByItem",
                component: InvAdjustmentByItemComponent,
                resolve:
                {
                    references: InvAdjustmentResolver
                },
                //canDeactivate: [RbDetailsGuard]
            },
            {
                path: "InvAdjustmentByLocation",
                component: InvAdjustmentByLocationComponent,
                resolve:
                {
                    references: InvAdjustmentResolver
                },
                //canDeactivate: [RbDetailsGuard]
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(InventoryRoutes)
    ]
})
export class InventoryRoutingModule { }
