﻿export class ItemInventoryModel {
    item: string;
    itemDesc: string;
    stockOnHand?: number;
    divisionDesc: string;
    deptDesc: string;
    categoryDesc: string;
    classDesc: string;
    subClassDesc: string;
}