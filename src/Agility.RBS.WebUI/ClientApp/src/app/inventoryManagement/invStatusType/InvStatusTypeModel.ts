export class InvStatusTypeModel {
  invStatus: number;
  invStatusDesc: string;
  operation?: string;  
}
