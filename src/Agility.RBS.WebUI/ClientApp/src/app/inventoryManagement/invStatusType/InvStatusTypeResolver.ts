import { Injectable } from "@angular/core";
import { Resolve } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { InvStatusTypeModel } from './InvStatusTypeModel';
import { InvStatusTypeService } from './InvStatusTypeService';
@Injectable()
export class InvStatusTypeListResolver implements Resolve<ServiceDocument<InvStatusTypeModel>> {
  constructor(private service: InvStatusTypeService) { }
  resolve(): Observable<ServiceDocument<InvStatusTypeModel>> {
        return this.service.list();
    }
}

@Injectable()
export class InvStatusTypeResolver implements Resolve<ServiceDocument<InvStatusTypeModel>> {
  constructor(private service: InvStatusTypeService) { }
  resolve(): Observable<ServiceDocument<InvStatusTypeModel>> {
        return this.service.addEdit();
    }
}


