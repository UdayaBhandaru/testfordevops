import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";
import { InvStatusTypeModel } from './InvStatusTypeModel';

@Injectable()
export class InvStatusTypeService {
  serviceDocument: ServiceDocument<InvStatusTypeModel> = new ServiceDocument<InvStatusTypeModel>();
    constructor(private httpHelperService: HttpHelperService) { }

  newModel(model: InvStatusTypeModel): ServiceDocument<InvStatusTypeModel> {
        return this.serviceDocument.newModel(model);
    }

  search(): Observable<ServiceDocument<InvStatusTypeModel>> {
    return this.serviceDocument.search("/api/InvStatusType/Search");
    }

  save(): Observable<ServiceDocument<InvStatusTypeModel>> {
    return this.serviceDocument.save("/api/InvStatusType/Save", true);
    }

  list(): Observable<ServiceDocument<InvStatusTypeModel>> {
    return this.serviceDocument.list("/api/InvStatusType/List");
    }

  addEdit(): Observable<ServiceDocument<InvStatusTypeModel>> {
    return this.serviceDocument.list("/api/InvStatusType/New");
    }

  isExistingInvStatusType(tsfZone: number, tsfZoneDesc: string): Observable<boolean> {
    return this.httpHelperService.get("/api/InvStatusType/IsExistingInvStatusType", new HttpParams().set("tsfZone", tsfZone.toString()).set("tsfZoneDesc", tsfZoneDesc));
    }
}
