import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { SharedService } from "../../Common/SharedService";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { FormControl, FormGroup } from "@angular/forms";
import { InvStatusTypeModel } from './InvStatusTypeModel';
import { InvStatusTypeService } from './InvStatusTypeService';

@Component({
    selector: "invstatustype",
  templateUrl: "./InvStatusTypeComponent.html",
  styleUrls: ['./InvStatusTypeComponent.scss']
})
export class InvStatusTypeComponent implements OnInit {
    PageTitle = "InvStatus Type";
    serviceDocument: ServiceDocument<InvStatusTypeModel>;
    localizationData: any;
    editModel: InvStatusTypeModel;
    editMode: boolean = false;
    defaultStatus: string;
    model: InvStatusTypeModel;

  constructor(private service: InvStatusTypeService, private sharedService: SharedService, private router: Router) {
    }

    ngOnInit(): void {
        this.bind();
    }

    bind(): void {
        delete this.editModel;
        if (this.sharedService.editObject) {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
            this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            delete this.sharedService.editObject;
            this.editMode = true;
        } else {
            this.model = {
              invStatus: null, invStatusDesc: null, operation: "I"
            };
            this.service.newModel(this.model);
        }
        this.serviceDocument = this.service.serviceDocument;
    }

    save(saveOnly: boolean): void {
        if (this.serviceDocument.dataProfile.profileForm.valid) {
            this.saveSubscription(saveOnly);
        } else {
            this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
        }
    }

    saveAndContinue(): void {
        this.save(false);
    }

    saveSubscription(saveOnly: boolean): void {
        this.checkTransferZone(saveOnly);
    }
    saveTransferZone(saveOnly: boolean): void {
        this.service.save().subscribe(() => {
            if (this.service.serviceDocument.result.type === MessageType.success) {
              this.sharedService.saveForm("InvStatus Type Saved Successfully").subscribe(() => {
                    if (saveOnly) {
                      this.router.navigate(["/InvStatusType/List"], { skipLocationChange: true });
                    } else {
                        if (!this.editMode) {
                            this.model = {
                              invStatus: null, invStatusDesc: null, operation: "I"
                            };
                            this.service.newModel(this.model);
                        } else {
                            Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
                            this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.editModel);
                            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
                        }
                    }
                }, (error: string) => { console.log(error); });
            } else {
                this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
            }
        }, (error: string) => { console.log(error); });
    }

    checkTransferZone(saveOnly: boolean): void {
        if (!this.editMode) {
            var form: FormGroup = this.serviceDocument.dataProfile.profileForm;
          if (form.controls["invStatus"].value && form.controls["invStatusDesc"].value) {
            this.service.isExistingInvStatusType(form.controls["invStatus"].value, form.controls["invStatusDesc"].value)
                    .subscribe((response: any) => {
                        if (response) {
                            this.sharedService.errorForm("Record alredy exits");
                        } else {
                            this.saveTransferZone(saveOnly);
                        }
                    });
            }
        } else {
            this.saveTransferZone(saveOnly);
        }
    }

    reset(): void {
        if (!this.editMode) {
            this.model = {
              invStatus: null, invStatusDesc: null, operation: "I"
            };
            this.service.newModel(this.model);
        } else {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
        }
        this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
    }    
}
