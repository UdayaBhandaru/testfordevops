import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { InvStatusTypeListComponent } from './InvStatusTypeListComponent';
import { InvStatusTypeResolver } from './InvStatusTypeResolver';
import { InvStatusTypeComponent } from './InvStatusTypeComponent';

const UtilityRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: InvStatusTypeListComponent,
                resolve:
                {
                  serviceDocument: InvStatusTypeResolver
                }
            },
            {
                path: "New",
                component: InvStatusTypeComponent,
                resolve:
                {
                  serviceDocument: InvStatusTypeResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(UtilityRoutes)
    ]
})
export class InvStatusTypeRoutingModule { }
