﻿import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { InvAdjustmentModel } from "./InvAdjustmentModel";
import { InvAdjustmentService } from "./InvAdjustmentService";

@Injectable()
export class InvAdjustmentResolver implements Resolve<ServiceDocument<InvAdjustmentModel>> {
    constructor(private service: InvAdjustmentService) { }
    resolve(): Observable<ServiceDocument<InvAdjustmentModel>> {
        return this.service.list();
    }
}