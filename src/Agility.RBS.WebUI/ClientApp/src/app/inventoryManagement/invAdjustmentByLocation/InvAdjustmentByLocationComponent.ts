import { Component, OnInit, TemplateRef, ViewChild, OnDestroy, ChangeDetectorRef, AfterViewChecked } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ServiceDocument, CommonService, MessageResult, MessageType } from "@agility/frameworkcore";
import { GridOptions, GridApi, RowNode } from "ag-grid-community";
import { MatDialogRef } from "@angular/material";
import { Subscription } from "rxjs";
import { FormControl } from "@angular/forms";
import { SharedService } from "../../Common/SharedService";
import { LoaderService } from "../../Common/LoaderService";
import { InvAdjustmentService } from "../InvAdjustmentService";
import { InvAdjustmentModel } from "../InvAdjustmentModel";
import { GridSelectComponent } from "../../Common/grid/GridSelectComponent";
import { GridNumericComponent } from "../../Common/grid/GridNumericComponent";
import { GridDeleteComponent } from "../../Common/grid/GridDeleteComponent";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { LocationDomainModel } from "../../Common/DomainData/LocationDomainModel";
import { CommonModel } from "../../Common/CommonModel";
import { GridGetItemComponent } from "../../Common/grid/GridGetItemComponent";
import { ItemInventoryModel } from "../ItemInventoryModel";
import { RbDialogService } from "../../Common/controls/RbDialogService";


@Component({
  selector: "InvAdjustmentByLocation",
  templateUrl: "./InvAdjustmentByLocationComponent.html",
  styleUrls: ['./InvAdjustmentByLocationComponent.scss']

})
export class InvAdjustmentByLocationComponent implements OnInit, AfterViewChecked, OnDestroy {
  public serviceDocument: ServiceDocument<InvAdjustmentModel>;
  componentName: any;
  PageTitle: string;
  localizationData: any;
  gridOptions: GridOptions;
  gridApi: GridApi;
  columns: {}[];
  model: InvAdjustmentModel;
  data: InvAdjustmentModel[];
  deletedRows: any[] = [];
  saveServiceSubscription: Subscription;
  saveSuccessSubscription: Subscription;
  inventoryStatusData: DomainDetailModel[];
  locTypeData: DomainDetailModel[];
  locationData: LocationDomainModel[];
  locationList: LocationDomainModel[];
  reasonData: CommonModel[];
  searchServiceSubscription: Subscription; searchService2Subscription: Subscription;
  mandatoryFieldsInGrid: string[] = [
    "item",
    "adjQty",
    "reason"
  ];
  stockApiUrl: string;
  currentRowIndex: number;
  public messageResult: MessageResult = new MessageResult();
  canNavigate: boolean = false;
  orginalValue: string = "";
  dialogRef: MatDialogRef<any>;

  constructor(public route: ActivatedRoute, public router: Router
    , private sharedService: SharedService, private service: InvAdjustmentService
    , private commonService: CommonService, private loaderService: LoaderService, private changeRef: ChangeDetectorRef
    , public dialogService: RbDialogService) {
    this.PageTitle = "Inventory Adjustment By Location";
    this.data = [];
    this.stockApiUrl = "/api/InvAdjustment/GetItemRelatedData";
    this.currentRowIndex = 0;
  }

  ngOnInit(): void {
    this.componentName = this;
    this.inventoryStatusData = Object.assign([], this.service.serviceDocument.domainData["inventoryStatus"]);
    this.locationList = Object.assign([], this.service.serviceDocument.domainData["location"]);
    this.locTypeData = Object.assign([], this.service.serviceDocument.domainData["locType"]);
    this.reasonData = Object.assign([], this.service.serviceDocument.domainData["invAdjustmentReason"]);
    this.bind();
  }

  bind(): void {
    this.newInvAdjustmentModel();
    this.service.newModel(this.model);
    this.serviceDocument = this.service.serviceDocument;
    this.setGridColumnsWithOptions();
  }

  setGridColumnsWithOptions(): void {
    this.columns = [
      {
        headerName: "Item", field: "item", cellRendererFramework: GridGetItemComponent
        , cellClassRules: {
          "invalid-grid-row": function (params: any): any { return !params.value; }
        }, tooltipField: "item", headerTooltip: "Item", width: 140
      },
      {
        headerName: "Desc", field: "itemDesc", tooltipField: "itemDesc", headerTooltip: "Description", width: 160
      },
      {
        headerName: "Division", field: "divisionDesc", tooltipField: "divisionDesc"
        , headerTooltip: "Division", width: 120
      },
      {
        headerName: "Department", field: "deptDesc", tooltipField: "deptDesc"
        , headerTooltip: "Department", width: 120
      },
      {
        headerName: "Category", field: "categoryDesc", tooltipField: "categoryDesc"
        , headerTooltip: "Category", width: 120
      },
      {
        headerName: "Reason", field: "reason", cellRendererFramework: GridSelectComponent
        , cellRendererParams: { references: this.reasonData, keyvaluepair: { key: "reason", value: "reasonDesc" } }
        , cellClassRules: {
          "invalid-grid-row": function (params: any): any { return !params.value; }
        }, tooltipField: "reason", headerTooltip: "Reason"
      },
      {
        headerName: "Adj Qty", field: "adjQty"
        , cellRendererFramework: GridNumericComponent
        , cellClassRules: {
          "invalid-grid-row": function (params: any): any { return !params.value; }
        }, tooltipField: "adjQty", headerTooltip: "Adjustment Qty", width: 100
      },
      {
        headerName: "Total Stock", field: "totalStock", tooltipField: "totalStock", headerTooltip: "Total Stock", width: 140
      },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridDeleteComponent, suppressCellSelection: true
        , width: 60, headerTooltip: "Actions"
      }
    ];

    this.gridOptions = {
      onGridReady: (params: any) => {
        this.gridApi = params.api;
      },
      context: {
        componentParent: this
      },
      enableColResize: true
    };
  }

  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }

  locationBasedLogic(event: any): void {
    this.serviceDocument.dataProfile.profileForm.controls["locationId"].setValue(null);
    this.locationData = this.locationList.filter(item => item.locType === this.serviceDocument.dataProfile.profileForm.controls["locationType"].value);
  }

  add($event: MouseEvent): void {
    $event.preventDefault();
    if (this.service.serviceDocument.dataProfile.profileForm.controls["locationId"].value) {
      if (this.data.length === 0 || this.data.length > 0 && this.validateRow()) {
        this.addNewRow();
      } else {
        this.sharedService.errorForm("Please enter mandatory fields for current row before adding new row");
      }
    } else {
      this.sharedService.errorForm("Please select location before adding new row to grid");
    }
  }

  validateRow(): boolean {
    let valid: boolean = true;
    this.mandatoryFieldsInGrid.forEach(field => {
      if (!this.data[this.currentRowIndex][field]) {
        valid = false;
      }
    });
    return valid;
  }

  addNewRow(): void {
    this.newInvAdjustmentModel();
    this.model["locationId"] = this.service.serviceDocument.dataProfile.profileForm.controls["locationId"].value;
    this.model["locationType"] = this.service.serviceDocument.dataProfile.profileForm.controls["locationType"].value;
    this.model["invStatus"] = this.service.serviceDocument.dataProfile.profileForm.controls["invStatus"].value;
    this.data.unshift(this.model);
    if (this.gridApi) {
      this.gridApi.setRowData(this.data);
    }
  }

  newInvAdjustmentModel(): void {
    this.model = {
      adjQty: null, invStatus: null, item: null, itemDesc: null
      , locationId: null, locationType: null, reason: null, listOfInvAdjustment: [], locName: null, totalStock: null
      , divisionDesc: null, deptDesc: null, categoryDesc: null, classDesc: null, subClassDesc: null
    };
  }

  select(cell: any, event: any): void {
    this.data[cell.rowIndex][cell.column.colId] = event.target.value;
    let rowNode: RowNode = this.gridApi.getRowNode(cell.rowIndex);
    rowNode.setData(rowNode.data);
    this.currentRowIndex = cell.rowIndex;
  }

  update(cell: any, event: any): void {
    this.data[cell.rowIndex][cell.column.colId] = event.target.value;
    if (cell.column.colId === "item") {
      this.itemBasedLogic(cell, event);
    }
    let rowNode: RowNode = this.gridApi.getRowNode(cell.rowIndex);
    rowNode.setData(rowNode.data);
    this.currentRowIndex = cell.rowIndex;
  }

  itemBasedLogic(cell: any, event: any): void {
    if (this.data[cell.rowIndex]["item"]) {
      let itmGridDataList: InvAdjustmentModel[] = this.data ? this.data
        .filter(itm => (itm.item === this.data[cell.rowIndex]["item"])) : null;
      if (itmGridDataList.length > 1) {
        this.sharedService.errorForm("Item already exists in grid");
      } else {
        this.data[cell.rowIndex]["itemDesc"] = null;
        this.data[cell.rowIndex]["totalStock"] = null;
        this.data[cell.rowIndex]["divisionDesc"] = null;
        this.data[cell.rowIndex]["deptDesc"] = null;
        this.data[cell.rowIndex]["categoryDesc"] = null;
        this.data[cell.rowIndex]["classDesc"] = null;
        this.data[cell.rowIndex]["subClassDesc"] = null;
        this.searchService2Subscription = this.service.getItemRelatedData(this.stockApiUrl, this.data[cell.rowIndex]["item"],
          this.data[cell.rowIndex]["locationType"], this.data[cell.rowIndex]["locationId"])
          .subscribe((response: ItemInventoryModel) => {
            if (response) {
              this.data[cell.rowIndex]["itemDesc"] = response.itemDesc;
              this.data[cell.rowIndex]["totalStock"] = response.stockOnHand;
              this.data[cell.rowIndex]["divisionDesc"] = response.divisionDesc;
              this.data[cell.rowIndex]["deptDesc"] = response.deptDesc;
              this.data[cell.rowIndex]["categoryDesc"] = response.categoryDesc;
              this.data[cell.rowIndex]["classDesc"] = response.classDesc;
              this.data[cell.rowIndex]["subClassDesc"] = response.subClassDesc;
              let rowNode: RowNode = this.gridApi.getRowNode(cell.rowIndex);
              rowNode.setData(rowNode.data);
            } else {
              this.sharedService.errorForm("Please enter valid item");
            }
            this.changeRef.detectChanges();
          }, (error: string) => { console.log(error); });
      }
    }
  }

  delete(cell: any): void {
    this.gridApi.updateRowData({ remove: [cell.data] });
    this.deletedRows.push(cell.data);
    this.data.splice(cell.rowIndex, 1);
    if (this.data.length === 0) {
      this.newInvAdjustmentModel();
      this.model["locationId"] = this.service.serviceDocument.dataProfile.profileForm.controls["locationId"].value;
      this.model["locationType"] = this.service.serviceDocument.dataProfile.profileForm.controls["locationType"].value;
      this.model["invStatus"] = this.service.serviceDocument.dataProfile.profileForm.controls["invStatus"].value;
      this.data.unshift(this.model);
      if (this.gridApi) {
        this.gridApi.setRowData(this.data);
      }
    }
  }

  save(saveOnly?: boolean): void {
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.saveSubscription();
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  saveSubscription(): void {
    if (this.data.length > 0 || this.deletedRows.length > 0) {
      if (this.validateData()) {
        this.serviceDocument.dataProfile.profileForm.controls["listOfInvAdjustment"] = new FormControl();
        this.serviceDocument.dataProfile.profileForm.controls["listOfInvAdjustment"].setValue(this.data);
        this.saveServiceSubscription = this.service.save().subscribe(() => {
          if (this.service.serviceDocument.result.type === MessageType.success) {
            this.saveSuccessSubscription = this.sharedService.saveForm("Adjustment Saved Successfully").subscribe(() => {
              this.router.navigate(["/Blank"], {
                skipLocationChange: true, queryParams: {
                  id: "/Inventory/InvAdjustmentByLocation"
                }
              });
            });
          } else {
            this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
          }
        });
      } else {
        this.sharedService.errorForm("Please fill Mandatory fields in Grid");
      }
    } else {
      this.sharedService.errorForm("Atleast 1 row is required");
    }
  }

  validateData(): boolean {
    let valid: boolean = true;
    if (this.data != null && this.data.length > 0) {
      this.data.forEach(row => {
        this.mandatoryFieldsInGrid.forEach(field => {
          if (!row[field]) {
            valid = false;
          }
        });
      });
    }
    return valid;
  }

  reset(): void {
    this.newInvAdjustmentModel();
    this.service.newModel(this.model);
    this.serviceDocument = this.service.serviceDocument;
    this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
    this.data = [];
  }

  ngOnDestroy(): void {
    if (this.saveServiceSubscription) {
      this.saveServiceSubscription.unsubscribe();
    }
    if (this.saveSuccessSubscription) {
      this.saveSuccessSubscription.unsubscribe();
    }
    if (this.searchServiceSubscription) {
      this.searchServiceSubscription.unsubscribe();
    }
    if (this.searchService2Subscription) {
      this.searchService2Subscription.unsubscribe();
    }
  }
}
