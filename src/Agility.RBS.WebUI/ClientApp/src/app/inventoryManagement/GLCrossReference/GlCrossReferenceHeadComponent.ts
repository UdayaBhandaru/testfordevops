import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { SharedService } from "../../Common/SharedService";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { FormControl, FormGroup } from "@angular/forms";
import { GlCrossReferenceHeadModel } from './GlCrossReferenceHeadModel';
import { GlCrossReferenceHeadService } from './GlCrossReferenceHeadService';
import { DepartmentDomainModel } from '../../Common/DomainData/DepartmentDomainModel';
import { ClassDomainModel } from '../../Common/DomainData/ClassDomainModel';
import { SubClassDomainModel } from '../../Common/DomainData/SubClassDomainModel';
import { TransactionDataModel } from '../../finance/transactiondata/TransactionDataModel';
import { CostZoneLocModel } from '../../costzone/costzone/CostZoneLocModel';

@Component({
  selector: "GlCrossReference",
  templateUrl: "./GlCrossReferenceHeadComponent.html",
  styleUrls: ['./GlCrossReferenceHeadComponent.scss']
})
export class GlCrossReferenceHeadComponent implements OnInit {
  PageTitle = "General Ledger Cross Reference";
  serviceDocument: ServiceDocument<GlCrossReferenceHeadModel>;

  constructor(public service: GlCrossReferenceHeadService, private sharedService: SharedService, private router: Router, private changeRef: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    this.serviceDocument = this.service.serviceDocument;
  }

  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }
}
