import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { RbsSharedModule } from "../../RbsSharedModule";
import { GlCrossReferenceRoutingModule } from './GlCrossReferenceRoutingModule';
import { GlCrossReferenceListComponent } from './GlCrossReferenceListComponent';
import { GlCrossReferenceHeadComponent } from './GlCrossReferenceHeadComponent';
import { GlCrossReferenceHeadService } from './GlCrossReferenceHeadService';
import { GlCrossReferenceListService } from './GlCrossReferenceListService';
import { GlCrossReferenceListResolver } from './GlCrossReferenceListResolver';
import { GlCrossReferenceHeadResolver } from './GlCrossReferenceHeadResolver';

@NgModule({
  imports: [
    FrameworkCoreModule,
    GlCrossReferenceRoutingModule,
    AgGridSharedModule,
    TitleSharedModule,
    RbsSharedModule
  ],
  declarations: [
    GlCrossReferenceListComponent,
    GlCrossReferenceHeadComponent
  ],
  providers: [

    GlCrossReferenceListService,
    GlCrossReferenceListResolver,
    GlCrossReferenceHeadService,
    GlCrossReferenceHeadResolver
  ]
})
export class GlCrossReferenceModule {
  constructor() {
    LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
  }
}
