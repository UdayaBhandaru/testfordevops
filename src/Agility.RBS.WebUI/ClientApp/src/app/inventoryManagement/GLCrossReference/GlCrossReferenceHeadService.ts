import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";
import { GlCrossReferenceHeadModel } from './GlCrossReferenceHeadModel';
import { SubClassDomainModel } from '../../Common/DomainData/SubClassDomainModel';
import { TransactionDataModel } from '../../finance/transactiondata/TransactionDataModel';
import { SharedService } from '../../Common/SharedService';

@Injectable()
export class GlCrossReferenceHeadService {
  serviceDocument: ServiceDocument<GlCrossReferenceHeadModel> = new ServiceDocument<GlCrossReferenceHeadModel>();

  constructor(private httpHelperService: HttpHelperService, private sharedService: SharedService) { }

  newModel(model: GlCrossReferenceHeadModel): ServiceDocument<GlCrossReferenceHeadModel> {
    return this.serviceDocument.newModel(model);
  }

  addEdit(id: number): Observable<ServiceDocument<GlCrossReferenceHeadModel>> {
    if (+id === 0) {
      return this.serviceDocument.new("/api/GlCrossReferenceHead/New");
    } else {
      return this.serviceDocument.open("/api/GlCrossReferenceHead/Open", new HttpParams().set("id", id.toString()));
    }
  }

}
