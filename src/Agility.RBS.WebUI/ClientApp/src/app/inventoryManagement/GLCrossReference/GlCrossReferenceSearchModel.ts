export class GlCrossReferenceSearchModel {
fifGlCrossRefId: number;
dept: number;
deptName: string;
class: number;
className: string;
subClass?: number;
subClassName: string;
location: number;
locName: string;
tranCode: number;
costRetailFlag: string;
lineType: string;
lineTypeDesc: string;
tranRefNo: string;
tranCodeDesc: string;
nbVatCode: string;
operation: string;
startRow?: number;
endRow?: number;
totalRows?: number;
sortModel?: {}[];
}
