import { Injectable } from "@angular/core";
import { Resolve } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { GlCrossReferenceListService } from './GlCrossReferenceListService';
import { GlCrossReferenceSearchModel } from './GlCrossReferenceSearchModel';
@Injectable()
export class GlCrossReferenceListResolver implements Resolve<ServiceDocument<GlCrossReferenceSearchModel>> {
  constructor(private service: GlCrossReferenceListService) { }
  resolve(): Observable<ServiceDocument<GlCrossReferenceSearchModel>> {
    return this.service.list();
  }
}



