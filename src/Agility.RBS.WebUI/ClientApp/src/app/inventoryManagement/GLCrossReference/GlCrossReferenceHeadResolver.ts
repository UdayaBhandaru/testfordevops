import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { GlCrossReferenceHeadModel } from './GlCrossReferenceHeadModel';
import { GlCrossReferenceHeadService } from './GlCrossReferenceHeadService';


@Injectable()
export class GlCrossReferenceHeadResolver implements Resolve<ServiceDocument<GlCrossReferenceHeadModel>> {
  constructor(private service: GlCrossReferenceHeadService) { }
  resolve(route: ActivatedRouteSnapshot): Observable<ServiceDocument<GlCrossReferenceHeadModel>> {
    return this.service.addEdit(route.params["id"]);
  }
}

