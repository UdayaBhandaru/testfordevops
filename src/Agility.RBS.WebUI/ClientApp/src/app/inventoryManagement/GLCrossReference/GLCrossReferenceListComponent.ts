import { Component, OnInit, ChangeDetectorRef, NgModule, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { SharedService } from "../../Common/SharedService";
import { GridOptions, IServerSideGetRowsParams, GridReadyEvent } from "ag-grid-community";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { InfoComponent } from "../../Common/InfoComponent";
import { GridExportModel } from "../../Common/grid/GridExportModel";
import { GridEditComponent } from "../../Common/grid/GridEditComponent";
import { GlCrossReferenceListService } from './GlCrossReferenceListService';
import { DepartmentDomainModel } from '../../Common/DomainData/DepartmentDomainModel';
import { ClassDomainModel } from '../../Common/DomainData/ClassDomainModel';
import { SubClassDomainModel } from '../../Common/DomainData/SubClassDomainModel';
import { TransactionDataModel } from '../../finance/transactiondata/TransactionDataModel';
import { Subscription } from 'rxjs';
import { GlCrossReferenceSearchModel } from './GlCrossReferenceSearchModel';
import { SearchComponent } from '../../Common/SearchComponent';

@Component({
  selector: "GlCrossReference-list",
  templateUrl: "./GlCrossReferenceListComponent.html",
  styleUrls: ['./GlCrossReferenceListComponent.scss']
})
export class GlCrossReferenceListComponent implements OnInit {
  public serviceDocument: ServiceDocument<GlCrossReferenceSearchModel>;
  PageTitle = "General Ledger Cross Reference";
  redirectUrl = "GLCrossReference/New/";
  componentName: any;
  columns: any[];
  @ViewChild("searchPanel") searchPanel: SearchComponent;
  model: GlCrossReferenceSearchModel = {
    fifGlCrossRefId: null, dept: null, deptName: null, class: null, className: null,
    subClass: null, subClassName: null, location: null, locName: null, tranCode: null,
    costRetailFlag: null, lineType: null, lineTypeDesc: null, tranRefNo: null,
    tranCodeDesc: null, nbVatCode: null, operation: "I"
  };

  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  gridOptions: GridOptions;
  locationData: any[];
  categoryData: DepartmentDomainModel[];
  fineLineData: ClassDomainModel[];
  segmentData: SubClassDomainModel[];
  fineLineList: ClassDomainModel[];
  segmentList: SubClassDomainModel[];
  domainDtlData: DomainDetailModel[];
  additionalSearchParams: any;
  loadedPage: boolean;
  searchServiceSubscription: Subscription;

  constructor(public service: GlCrossReferenceListService, private sharedService: SharedService, private router: Router, private ref: ChangeDetectorRef) {
  }

  ngAfterViewChecked(): void {
    this.ref.detectChanges();
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "fifGlCrossRefId", field: "fifGlCrossRefId", tooltipField: "fifGlCrossRefId", width: 60 },
      { headerName: "deptName", field: "deptName", tooltipField: "deptName", width: 90 },
      { headerName: "className", field: "className", tooltipField: "className", width: 90 },
      { headerName: "className", field: "className", tooltipField: "className", width: 90 },
      { headerName: "subClassName", field: "subClassName", tooltipField: "subClassName", width: 90 },
      { headerName: "locName", field: "locName", tooltipField: "locName", width: 90 },
      {
        headerName: "ACTIONS", field: "Actions", cellRendererFramework: GridEditComponent, width: 60
      }
    ];

    this.gridOptions = {
      onGridReady: (params: GridReadyEvent) => {
        params.api.setEnterpriseDatasource(this);
      },
      context: {
        componentParent: this
      },
      suppressRowClickSelection: true,
      suppressCellSelection: true,
      rowModelType: "enterprise",
      paginationPageSize: this.service.orderPaging.pageSize,
      enableServerSideSorting: true,
      enableServerSideFilter: true,
      cacheBlockSize: this.service.orderPaging.cacheSize,
    };


    this.locationData = Object.assign([], this.service.serviceDocument.domainData["location"]);
    this.categoryData = Object.assign([], this.service.serviceDocument.domainData["category"]);
    this.fineLineData = Object.assign([], this.service.serviceDocument.domainData["fineline"]);
    this.segmentData = Object.assign([], this.service.serviceDocument.domainData["segment"]);
    this.fineLineList = Object.assign([], this.service.serviceDocument.domainData["fineline"]);
    this.segmentList = Object.assign([], this.service.serviceDocument.domainData["segment"]);
    this.domainDtlData = Object.assign([], this.service.serviceDocument.domainData["vatCode"]);

    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: any): void {
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.showSearchCriteria = event === "search" ? true : event;
      if (event === "search") {
        this.searchPanel.restrictSearch = false;
        this.searchPanel.search();
      } else if (event) {
        this.searchPanel.restrictSearch = true;
      }
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
      this.sharedService.previousUrl = "../GlCrossReferenceList/List";
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl + cell.data.fifGlCrossRefId
      , this.service.serviceDocument.dataProfile.dataList);
  }

  changeCategory(): void {
    this.serviceDocument.dataProfile.profileForm.controls["class"].setValue(null);
    this.fineLineData = this.fineLineList.filter(item => item.dept === +this.serviceDocument.dataProfile.profileForm.controls["dept"].value);
    this.sharedService.domainData["class"] = this.fineLineData;
  }

  changeFineLine(): void {
    this.serviceDocument.dataProfile.profileForm.controls["subClass"].setValue(null);
    var transactionDataModel = new TransactionDataModel();
    transactionDataModel.category = +this.serviceDocument.dataProfile.profileForm.controls["dept"].value;
    transactionDataModel.fineLine = +this.serviceDocument.dataProfile.profileForm.controls["class"].value;
    this.service.subclass(transactionDataModel).subscribe((data) => { this.segmentData = data; this.sharedService.domainData["subClass"] = this.segmentData; });
  }

  getRows(params: IServerSideGetRowsParams): void {
    this.additionalSearchParams = { startRow: params.request.startRow, endRow: params.request.endRow, sortModel: params.request.sortModel };
    if (!this.loadedPage) {
      this.loadedPage = true;
      params.successCallback(
        this.serviceDocument.dataProfile.dataList.slice(params.request.startRow, params.request.endRow),
        this.service.serviceDocument.dataProfile.dataModel.totalRows);
    } else {
      this.searchServiceSubscription = this.service.search(this.additionalSearchParams).subscribe((sDoc) => {
        params.successCallback(sDoc.dataProfile.dataList, this.service.serviceDocument.dataProfile.dataModel.totalRows);
      });
    }
  }
  ngOnDestroy(): void {
    if (this.searchServiceSubscription) {
      this.searchServiceSubscription.unsubscribe();
    }
  }

}
