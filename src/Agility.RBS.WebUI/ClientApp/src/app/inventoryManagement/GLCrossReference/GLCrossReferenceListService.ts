import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";
import { SubClassDomainModel } from '../../Common/DomainData/SubClassDomainModel';
import { TransactionDataModel } from '../../finance/transactiondata/TransactionDataModel';
import { GlCrossReferenceSearchModel } from './GlCrossReferenceSearchModel';
import { SharedService } from '../../Common/SharedService';

@Injectable()
export class GlCrossReferenceListService {
  serviceDocument: ServiceDocument<GlCrossReferenceSearchModel> = new ServiceDocument<GlCrossReferenceSearchModel>();
  orderPaging: {
    startRow: number,
    pageSize: number,
    cacheSize: number,
  } = {
      startRow: 1,
      pageSize: 10,
      cacheSize: 100,
    };
  constructor(private httpHelperService: HttpHelperService, private sharedService: SharedService) { }

  newModel(model: GlCrossReferenceSearchModel): ServiceDocument<GlCrossReferenceSearchModel> {
    return this.serviceDocument.newModel(model);
  }

  search(additionalParams: any): Observable<ServiceDocument<GlCrossReferenceSearchModel>> {
    return this.serviceDocument.search("/api/GlCrossReferenceList/Search", true, () => this.setSubClassId(additionalParams));
  }

  list(): Observable<ServiceDocument<GlCrossReferenceSearchModel>> {
    return this.serviceDocument.list("/api/GlCrossReferenceList/List");
  }

  subclass(model: TransactionDataModel): Observable<SubClassDomainModel[]> {
    var params = new HttpParams();
    return this.httpHelperService.get(`/api/TransactionData/SubClass?deptid=${String(model.category)}&classId=${String(model.fineLine)}`, params);
  }

  setSubClassId(additionalParams: any): void {
    var sd: GlCrossReferenceSearchModel = this.serviceDocument.dataProfile.dataModel;
    if (additionalParams) {
      sd.startRow = additionalParams.startRow ? additionalParams.startRow : this.orderPaging.startRow;
      sd.endRow = additionalParams.endRow ? additionalParams.endRow : this.orderPaging.cacheSize;
      sd.sortModel = additionalParams.sortModel;
    } else {
      sd.startRow = this.orderPaging.startRow;
      sd.endRow = this.orderPaging.cacheSize;
    }
  }
}
