import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { GlCrossReferenceListComponent } from './GlCrossReferenceListComponent';
import { GlCrossReferenceHeadResolver } from './GlCrossReferenceHeadResolver';
import { GlCrossReferenceHeadComponent } from './GlCrossReferenceHeadComponent';
import { GlCrossReferenceListResolver } from './GlCrossReferenceListResolver';

const UtilityRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "", redirectTo: "List", pathMatch: "full"
      },
      {
        path: "List",
        component: GlCrossReferenceListComponent,
        resolve:
          {
            serviceDocument: GlCrossReferenceListResolver
          }
      },
      {
        path: "New/:id",
        component: GlCrossReferenceHeadComponent,
        resolve:
          {
            serviceDocument: GlCrossReferenceHeadResolver
          }
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(UtilityRoutes)
  ]
})
export class GlCrossReferenceRoutingModule { }
