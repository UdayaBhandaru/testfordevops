﻿import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../AgGridSharedModule";
import { TitleSharedModule } from "../TitleSharedModule";
import { InventoryRoutingModule } from "./InventoryRoutingModule";
import { InvAdjustmentByItemComponent } from "./invAdjustmentByItem/InvAdjustmentByItemComponent";
import { InvAdjustmentService } from "./InvAdjustmentService";
import { InvAdjustmentResolver } from "./InvAdjustmentResolver";
import { InvAdjustmentByLocationComponent } from "./invAdjustmentByLocation/InvAdjustmentByLocationComponent";
import { RbsSharedModule } from "../RbsSharedModule";

@NgModule({
    imports: [
        FrameworkCoreModule,
        InventoryRoutingModule,
        AgGridModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        InvAdjustmentByItemComponent,
        InvAdjustmentByLocationComponent
    ],
    providers: [
        InvAdjustmentService,
        InvAdjustmentResolver
    ]
})
export class InventoryModule {
}
