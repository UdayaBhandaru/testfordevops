﻿export class InvAdjustmentModel {
    item: string;
    itemDesc: string;
    invStatus: string;
    locationType: string;
    locationId?: number;
    locName: string;
    adjQty?: number;
    reason?: number;
    totalStock?: number;
    divisionDesc: string;
    deptDesc: string;
    categoryDesc: string;
    classDesc: string;
    subClassDesc: string;
    listOfInvAdjustment: InvAdjustmentModel[];
}