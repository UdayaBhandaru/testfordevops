import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { InvAdjustmentReasonListComponent } from './InvAdjustmentReasonListComponent';
import { InvAdjustmentReasonResolver } from './InvAdjustmentReasonResolver';
import { InvAdjustmentReasonComponent } from './InvAdjustmentReasonComponent';

const UtilityRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: InvAdjustmentReasonListComponent,
                resolve:
                {
                  serviceDocument: InvAdjustmentReasonResolver
                }
            },
            {
                path: "New",
                component: InvAdjustmentReasonComponent,
                resolve:
                {
                  serviceDocument: InvAdjustmentReasonResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(UtilityRoutes)
    ]
})
export class InvAdjustmentReasonRoutingModule { }
