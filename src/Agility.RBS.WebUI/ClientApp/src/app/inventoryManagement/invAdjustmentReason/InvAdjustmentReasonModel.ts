export class InvAdjustmentReasonModel {
  reason: number;
  reasonDesc: string;
  operation?: string;  
}
