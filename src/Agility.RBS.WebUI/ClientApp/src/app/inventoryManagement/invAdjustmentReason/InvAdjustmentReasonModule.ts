import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { RbsSharedModule } from "../../RbsSharedModule";
import { InvAdjustmentReasonRoutingModule } from './InvAdjustmentReasonRoutingModule';
import { InvAdjustmentReasonListComponent } from './InvAdjustmentReasonListComponent';
import { InvAdjustmentReasonComponent } from './InvAdjustmentReasonComponent';
import { InvAdjustmentReasonService } from './InvAdjustmentReasonService';
import { InvAdjustmentReasonResolver, InvAdjustmentReasonListResolver } from './InvAdjustmentReasonResolver';

@NgModule({
    imports: [
        FrameworkCoreModule,
        InvAdjustmentReasonRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        InvAdjustmentReasonListComponent,
        InvAdjustmentReasonComponent
    ],
    providers: [
        InvAdjustmentReasonService,
        InvAdjustmentReasonResolver,
        InvAdjustmentReasonListResolver
    ]
})
export class InvAdjustmentReasonModule {
    constructor() {
        LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
    }
}
