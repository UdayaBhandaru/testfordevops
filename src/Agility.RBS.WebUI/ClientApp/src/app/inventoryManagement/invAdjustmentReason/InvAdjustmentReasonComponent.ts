import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { SharedService } from "../../Common/SharedService";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { FormControl, FormGroup } from "@angular/forms";
import { InvAdjustmentReasonModel } from './InvAdjustmentReasonModel';
import { InvAdjustmentReasonService } from './InvAdjustmentReasonService';

@Component({
    selector: "invadjustmentreason",
  templateUrl: "./InvAdjustmentReasonComponent.html",
  styleUrls: ['./InvAdjustmentReasonComponent.scss']
})
export class InvAdjustmentReasonComponent implements OnInit {
    PageTitle = "Inventory Adjustment Reason";
    serviceDocument: ServiceDocument<InvAdjustmentReasonModel>;
    localizationData: any;
    editModel: InvAdjustmentReasonModel;
    editMode: boolean = false;
    defaultStatus: string;
    model: InvAdjustmentReasonModel;

  constructor(public service: InvAdjustmentReasonService, private sharedService: SharedService, private router: Router, private changeRef: ChangeDetectorRef) {
    }

    ngOnInit(): void {
        this.bind();
    }

    bind(): void {
        delete this.editModel;
        if (this.sharedService.editObject) {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
            this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            delete this.sharedService.editObject;
            this.editMode = true;
        } else {
            this.model = {
              reason: null, reasonDesc: null, operation: "I"
            };
            this.service.newModel(this.model);
        }
        this.serviceDocument = this.service.serviceDocument;
    }

    save(saveOnly: boolean): void {
        if (this.serviceDocument.dataProfile.profileForm.valid) {
            this.saveSubscription(saveOnly);
        } else {
            this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
        }
    }

    //saveAndContinue(): void {
    //    this.save(false);
    //}

    saveSubscription(saveOnly: boolean): void {
        this.checkInvAdjReason(saveOnly);
    }
    saveInvAdjReason(saveOnly: boolean): void {
        this.service.save().subscribe(() => {
            if (this.service.serviceDocument.result.type === MessageType.success) {
              this.sharedService.saveForm("Inventory Adjustment Reason Saved Successfully").subscribe(() => {
                    if (saveOnly) {
                      this.router.navigate(["/InventoryAdjutmentReason/List"], { skipLocationChange: true });
                    } else {
                        if (!this.editMode) {
                            this.model = {
                              reason: null, reasonDesc: null, operation: "I"
                            };
                            this.service.newModel(this.model);
                        } else {
                            Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
                            this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.editModel);
                            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
                        }
                    }
                }, (error: string) => { console.log(error); });
            } else {
                this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
            }
        }, (error: string) => { console.log(error); });
    }

    checkInvAdjReason(saveOnly: boolean): void {
      if (!this.editMode) {
        var form: FormGroup = this.serviceDocument.dataProfile.profileForm;
        if (form.controls["reason"].value && form.controls["reasonDesc"].value) {
          this.service.isExistingInvAdjustmentReason(form.controls["reason"].value, form.controls["reasonDesc"].value)
            .subscribe((response: any) => {
              if (response) {
                this.sharedService.errorForm("Record alredy exits");
              } else {
                this.saveInvAdjReason(saveOnly);
              }
            });
        }
      } else {
            this.saveInvAdjReason(saveOnly);
        }
    }

    reset(): void {
        if (!this.editMode) {
            this.model = {
              reason: null, reasonDesc: null, operation: "I"
            };
            this.service.newModel(this.model);
        } else {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
        }
        this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
  }

  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }
}
