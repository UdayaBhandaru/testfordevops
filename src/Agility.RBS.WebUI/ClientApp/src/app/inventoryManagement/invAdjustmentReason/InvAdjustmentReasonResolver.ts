import { Injectable } from "@angular/core";
import { Resolve } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { InvAdjustmentReasonModel } from './InvAdjustmentReasonModel';
import { InvAdjustmentReasonService } from './InvAdjustmentReasonService';
@Injectable()
export class InvAdjustmentReasonListResolver implements Resolve<ServiceDocument<InvAdjustmentReasonModel>> {
  constructor(private service: InvAdjustmentReasonService) { }
  resolve(): Observable<ServiceDocument<InvAdjustmentReasonModel>> {
        return this.service.list();
    }
}

@Injectable()
export class InvAdjustmentReasonResolver implements Resolve<ServiceDocument<InvAdjustmentReasonModel>> {
  constructor(private service: InvAdjustmentReasonService) { }
  resolve(): Observable<ServiceDocument<InvAdjustmentReasonModel>> {
        return this.service.addEdit();
    }
}


