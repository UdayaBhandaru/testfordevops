import { Component, OnInit, ChangeDetectorRef, NgModule } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { SharedService } from "../../Common/SharedService";
import { GridOptions } from "ag-grid-community";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { InfoComponent } from "../../Common/InfoComponent";
import { GridExportModel } from "../../Common/grid/GridExportModel";
import { GridEditComponent } from "../../Common/grid/GridEditComponent";
import { InvAdjustmentReasonModel } from './InvAdjustmentReasonModel';
import { InvAdjustmentReasonService } from './InvAdjustmentReasonService';

@Component({
  selector: "inventoryAdjutmentReason-list",
  templateUrl: "./InvAdjustmentReasonListComponent.html",
  styleUrls: ['./InvAdjustmentReasonListComponent.scss']
})
export class InvAdjustmentReasonListComponent implements OnInit {
  public serviceDocument: ServiceDocument<InvAdjustmentReasonModel>;
  PageTitle = "Inventory Adjustment Reason";
  redirectUrl = "InventoryAdjutmentReason";
  componentName: any;
  columns: any[];
  model: InvAdjustmentReasonModel = {
    reason: null, reasonDesc: null
  };
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  gridOptions: GridOptions;

  constructor(public service: InvAdjustmentReasonService, private sharedService: SharedService, private router: Router, private ref: ChangeDetectorRef) {
  }

  ngAfterViewChecked(): void {
    this.ref.detectChanges();
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "ID", field: "reason", tooltipField: "reason", width: 60 },
      { headerName: "DESCRIPTION", field: "reasonDesc", tooltipField: "reasonDesc", width: 90 },
      {
        headerName: "ACTIONS", field: "Actions", cellRendererFramework: GridEditComponent, width: 60
      }
    ];

    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }
}
