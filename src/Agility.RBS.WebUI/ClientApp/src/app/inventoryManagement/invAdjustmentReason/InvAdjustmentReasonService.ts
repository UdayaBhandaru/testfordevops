import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";
import { InvAdjustmentReasonModel } from './InvAdjustmentReasonModel';

@Injectable()
export class InvAdjustmentReasonService {
  serviceDocument: ServiceDocument<InvAdjustmentReasonModel> = new ServiceDocument<InvAdjustmentReasonModel>();
    constructor(private httpHelperService: HttpHelperService) { }

  newModel(model: InvAdjustmentReasonModel): ServiceDocument<InvAdjustmentReasonModel> {
        return this.serviceDocument.newModel(model);
    }

  search(): Observable<ServiceDocument<InvAdjustmentReasonModel>> {
    return this.serviceDocument.search("/api/InvAdjustmentReason/Search");
    }

  save(): Observable<ServiceDocument<InvAdjustmentReasonModel>> {
    return this.serviceDocument.save("/api/InvAdjustmentReason/Save", true);
    }

  list(): Observable<ServiceDocument<InvAdjustmentReasonModel>> {
    return this.serviceDocument.list("/api/InvAdjustmentReason/List");
    }

  addEdit(): Observable<ServiceDocument<InvAdjustmentReasonModel>> {
    return this.serviceDocument.list("/api/InvAdjustmentReason/New");
    }

  isExistingInvAdjustmentReason(reason: string, reasonDesc: string): Observable<boolean> {
    return this.httpHelperService.get("/api/InvAdjustmentReason/IsExistingInvAdjustmentReason", new HttpParams().set("reason", reason).set("reasonDesc", reasonDesc));
    }
}
