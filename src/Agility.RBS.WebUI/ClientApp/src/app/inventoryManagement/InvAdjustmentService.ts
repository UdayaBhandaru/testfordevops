import { Injectable } from "@angular/core";
import { HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { InvAdjustmentModel } from "./InvAdjustmentModel";
import { HttpHelperService } from "../Common/HttpHelperService";

@Injectable()
export class InvAdjustmentService {
    serviceDocument: ServiceDocument<InvAdjustmentModel> = new ServiceDocument<InvAdjustmentModel>();

    constructor(private httpHelperService: HttpHelperService) {

    }

    list(): Observable<ServiceDocument<InvAdjustmentModel>> {
        return this.serviceDocument.list("/api/InvAdjustment/List");
    }

    newModel(model: InvAdjustmentModel): ServiceDocument<InvAdjustmentModel> {
        return this.serviceDocument.newModel(model);
    }

    getDataBasedOnId(id: string, columnName: string): Observable<any> {
        return this.httpHelperService.get("/api/InvAdjustment/GetDataBasedOnId", new HttpParams().set("id", id)
            .append("columnName", columnName));
    }

    getItemRelatedData(apiUrl: string, item: string, locationType?: string, locationId?: number): Observable<any> {
        let params: HttpParams = new HttpParams().set("itemCode", item);
        if (locationType && locationId) {
            params = new HttpParams().set("itemCode", item)
                .append("locType", locationType).append("locId", locationId.toString());
        }
        return this.httpHelperService.get(apiUrl, params);
    }

    save(): Observable<ServiceDocument<InvAdjustmentModel>> {
        return this.serviceDocument.save("/api/InvAdjustment/Save", true);
    }
}
