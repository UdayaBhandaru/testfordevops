import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AllocationListComponent } from './AllocationListComponent';
import { AllocationListResolver } from './AllocationListResolver';

const Receiving: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
              component: AllocationListComponent,
                resolve:
                {
                  references: AllocationListResolver
                }
            }
            
        ]
    }
];

@NgModule({
    imports: [
      RouterModule.forChild(Receiving)
    ]
})
export class AllocationRoutingModule { }
