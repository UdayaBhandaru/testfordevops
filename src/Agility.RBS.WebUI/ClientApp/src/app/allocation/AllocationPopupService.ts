import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpParams } from "@angular/common/http";
import { ServiceDocument } from "@agility/frameworkcore";
import { AllocationHeaderModel } from './AllocationHeaderModel';
import { HttpHelperService } from '../Common/HttpHelperService';
import { SharedService } from '../Common/SharedService';
import { AllocationDomainModel } from './AllocationDomainModel';

@Injectable()
export class AllocationPopupService {
  serviceDocument: ServiceDocument<AllocationHeaderModel> = new ServiceDocument<AllocationHeaderModel>(); 
  constructor(private sharedService: SharedService, private httpHelperService: HttpHelperService) { }  

  saveAllocation(model: AllocationHeaderModel): Observable<boolean> {
    return this.httpHelperService.post("/api/AllocationHead/saveAllocation", model);
  }

  newModel(model: AllocationHeaderModel): ServiceDocument<AllocationHeaderModel> {
    this.serviceDocument.newModel(model);
    return this.serviceDocument;
  }

  allocationDomainData(id: number): Observable<AllocationDomainModel> {
    return this.httpHelperService.get("/api/AllocationHead/AllocationDomainData", new HttpParams().set("item", id.toString()));
  }

  getItems(apiUrl: string): Observable<any[]> {
    return this.httpHelperService.get(apiUrl);
  }

}
