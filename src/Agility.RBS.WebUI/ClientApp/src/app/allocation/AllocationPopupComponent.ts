import { Component, OnInit, Inject, ChangeDetectorRef } from "@angular/core";
import { FormGroup} from "@angular/forms";
import { Router } from "@angular/router";
import { ServiceDocument, CommonService, FormMode} from "@agility/frameworkcore";
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from "@angular/material";
import { GridOptions, RowNode, GridApi, ColumnApi } from "ag-grid-community";
import { Subscription } from "rxjs";
import { AllocationPopupService } from './AllocationPopupService';
import { AllocationHeaderModel } from './AllocationHeaderModel';
import { SharedService } from '../Common/SharedService';
import { LoaderService } from '../Common/LoaderService';
import { GridSelectComponent } from '../Common/grid/GridSelectComponent';
import { GridNumericComponent } from '../Common/grid/GridNumericComponent';
import { GridEditComponent } from '../Common/grid/GridEditComponent';
import { AllocationDetailModel } from './AllocationDetailModel';
import { AllocationItemLocationModel } from './AllocationItemLocationModel';

@Component({
  selector: "nonMerchandise-cost",
  templateUrl: "./AllocationPopupComponent.html",
  styleUrls: ['./AllocationPopupComponent.scss'],
  providers: [
    AllocationPopupService
  ]
})

export class AllocationPopupComponent implements OnInit {
  infoGroup: FormGroup;
  public serviceDocument: ServiceDocument<AllocationHeaderModel>;
  detailModel: AllocationDetailModel;
  dataList: AllocationDetailModel[] = [];
  PageTitle: string = "Allocation";
  item: string;
  orderedQty: number;
  columns: {}[];
  componentName: any;
  gridOptions: GridOptions;
  saveServiceSubscription: Subscription;
  saveSuccessSubscription: Subscription;
  deletedRows: any[] = [];
  gridApi: GridApi;
  gridColumnApi: ColumnApi;
  editMode: boolean = false;
  btShowHide: boolean = false;
  locationData: AllocationItemLocationModel[];
  mandatoryNonmarchandiseLocFields: string[] = [
    "previousQty",
    "toLoc",
    "toLocType"
  ];
  model: AllocationHeaderModel;


  constructor(private service: AllocationPopupService, private _commonService: CommonService, public dialog: MatDialog
    , private _router: Router, @Inject(MAT_DIALOG_DATA) public data: any, public dialogRef: MatDialogRef<any>
    , private sharedService: SharedService, private loaderService: LoaderService
    , private changeRef: ChangeDetectorRef
  ) {
    this.item = data.AllocationData.item;
    this.locationData = data.AllocationDomainData.locationData;
    this.orderedQty = data.AllocationData.qtyOrdered;
    this.model = {
      allocNo: null, allocDesc: null, orderNo: null, item: null, operation: "I", itemDesc: null, fromLoc: null,
      fromLocName: null, releaseDate: null, ordQty: null
    };
    Object.assign(this.model, data.AllocationData.allocationdata);
    this.infoGroup = this._commonService.getFormGroup(this.model, FormMode.Open);
  }

  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }

  ngOnInit(): void {
    this.infoGroup.controls["ordQty"].setValue(this.orderedQty);
    this.bind();    
  }

  bind(): void {
    this.setGridColumnsWithOptions();
    if (this.item.toString() !== "0") {
      this.editMode = true;
      this.PageTitle = `${this.PageTitle} - EDIT - ${this.item}`;
      if (this.data.AllocationData.allocationdata.length > 0 && this.data.AllocationData.allocationdata[0].allocationDetails) {
        this.dataList = [];
        Object.assign(this.dataList, this.model[0].allocationDetails);
      }
      if (this.data.AllocationData.allocationdata != 0) {
        this.infoGroup.controls["allocNo"].setValue(this.model[0].allocNo);
        this.infoGroup.controls["allocDesc"].setValue(this.model[0].allocDesc);
      }
    } else {
      this.PageTitle = `${this.PageTitle} - ADD (Ducument ID - ${this.item})`;
    }
  }
  setGridColumnsWithOptions(): void {
    this.columns = [      
      {
        headerName: "Location", field: "toLoc", cellRendererFramework: GridSelectComponent, cellClassRules: {
          "invalid-grid-row": function (params: any): any { return !params.value; }
        }, cellRendererParams: { references: this.locationData, keyvaluepair: { key: "location", value: "locName"} },
        tooltipField: "location", headerTooltip: "locName"
      },
      {
        headerName: "Previous Qty", field: "qtyAllocated", cellRendererFramework: GridNumericComponent, hide: true,
        cellClassRules: {
          "invalid-grid-row": function (params: any): any { return !params.value; }
        }, tooltipField: "qtyAllocated", headerTooltip: "qtyAllocated"
      },
      {
        headerName: "Distributed Qty", field: "previousQty", cellRendererFramework: GridNumericComponent,
        cellClassRules: {
          "invalid-grid-row": function (params: any): any { return !params.value; }
        }, tooltipField: "previousQty", headerTooltip: "previousQty"
      },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent
        , cellRendererParams: { restrictEditIcon: true }
      }
    ];

    this.gridOptions = {
      onGridReady: (params: any) => {
        this.gridApi = params.api;
        this.gridApi.sizeColumnsToFit();
        this.gridColumnApi = params.columnApi;
      },
      context: {
        componentParent: this
      },
      suppressPaginationPanel: true,
      suppressScrollOnNewData: false,
      paginationPageSize: 10,
      pagination: true,
      enableSorting: true,
      rowHeight: 40,
      enableColResize: true,
      rowSelection: "multiple",
      suppressRowClickSelection: true,
      suppressCellSelection: true,
      groupUseEntireRow: true,
      groupDefaultExpanded: -1,
      domLayout: "autoHeight",
      toolPanelSuppressSideButtons: true,
      embedFullWidthRows: true
    };
  }

  newModel(): void {
    this.detailModel = {
      toLoc: null, toLocName: null, toLocType: null, qtyAllocated: null, operation: "I", previousQty:null
    };
  }
  add($event: MouseEvent): void {
    $event.preventDefault();
    this.newModel();
    this.dataList.unshift(this.detailModel);
    if (this.gridApi) {
      this.gridApi.setRowData(this.dataList);
    }
  }

  save(): void {
    if (this.infoGroup.valid) {
      this.saveSubscription();
    } else {
      this.sharedService.validateForm(this.infoGroup);
    }
  }

  saveSubscription(): void {
    if (this.dataList.length > 0) {
      if (this.validateData()) {
        if (this.deletedRows.length > 0) {
          this.deletedRows.forEach(x => this.dataList.push(x));
        }
        let allocationHeader: AllocationHeaderModel = {          
          allocNo: this.infoGroup.controls["allocNo"].value, allocDesc: this.infoGroup.controls["allocDesc"].value
          , ordQty: this.infoGroup.controls["ordQty"].value, item: this.data.AllocationData.item, itemDesc: this.data.AllocationData.itemDesc, orderNo: this.data.AllocationData.orderNo
          , fromLoc: this.data.AllocationData.location, fromLocName: this.data.AllocationData.locName, releaseDate: this.data.AllocationData.estDate, allocationDetails: this.dataList, operation: this.data.AllocationData.allocationdata == 0 ? this.model.operation : this.model[0].operation
        };
        this.saveServiceSubscription = this.service.saveAllocation(allocationHeader).subscribe((response: boolean) => {
          if (response === true) {
            this.saveSuccessSubscription = this.sharedService.saveForm(
              "Allocation Saved Successfully").subscribe(() => {
                this.dialogRef.close();
              });
          } else {
            this.sharedService.errorForm("error");
          }
        });
      } else {
        this.sharedService.errorForm("Please fill Mandatory fields in Grid");
      }
    } else {
      this.sharedService.errorForm("Atleast One Allocation  Detail is required");
    }

  }

  validateData(): boolean {
    let valid: boolean = true;
    if (this.dataList != null && this.dataList.length > 0) {
      this.dataList.forEach(row => {
        this.mandatoryNonmarchandiseLocFields.forEach(field => {
          if (!row[field]) {
            valid = false;
          }
        });
      });
    }
    return valid;
  }

  close(): void {
    this.dialogRef.close();
  }

  select(cell: any, event: any): void {
    this.dataList[cell.rowIndex][cell.column.colId] = event.target.value;
    if (cell.column.colId === "toLoc") {
      this.dataList[cell.rowIndex]["toLoc"] = event.target.value;
      var locationType = this.locationData.filter(id => id.location == event.target.value);
      this.dataList[cell.rowIndex]["toLocType"] = locationType[0].locType;
      this.dataList[cell.rowIndex]["toLocName"] = locationType[0].locName;
    }
    let rowNode: RowNode = this.gridApi.getRowNode(cell.rowIndex);
    rowNode.setData(rowNode.data);
  }

  update(cell: any, event: any): void {
    let qty: number = 0;
    this.dataList[cell.rowIndex][cell.column.colId] = event.target.value;
    if (cell.column.colId === "previousQty") {
      qty = this.dataList[cell.rowIndex]["qtyAllocated"];
      if (qty != null) {
        let value = event.target.value - qty;
        this.dataList[cell.rowIndex]["qtyAllocated"] = value;
      }
      this.dataList[cell.rowIndex]["previousQty"] = event.target.value;
    }
    this.calcOrderQty();
    let rowNode: RowNode = this.gridApi.getRowNode(cell.rowIndex);
    rowNode.setData(rowNode.data);
  }

  delete(cell: any): void {
    this.gridApi.updateRowData({ remove: [cell.data] });
    cell.data.operation = "D";
    this.deletedRows.push(cell.data);
    this.dataList.splice(cell.rowIndex, 1);
    this.newModel();
  }

  calcOrderQty(): void {
    var totalCount = 0;
    this.dataList.forEach(ctrl => {
      totalCount += Number(ctrl.previousQty);
    });    
    var data = totalCount;
    if (data > this.orderedQty) {
      this.btShowHide = true;
      this.sharedService.errorForm("Distributed Quantity Is Greater Then Order Quantity");
    } else { this.btShowHide = false; }
  }
}
