import { AllocationDetailModel } from './AllocationDetailModel';

export class AllocationHeaderModel {
  allocNo?: number;
  allocDesc?: string;
  orderNo?: number;  
  item?: string;
  itemDesc?: string;
  fromLoc?: number;
  fromLocName? : string;
  releaseDate?: Date;
  allocationDetails?: AllocationDetailModel[];
  ordQty?: string;
  operation: string;
}
