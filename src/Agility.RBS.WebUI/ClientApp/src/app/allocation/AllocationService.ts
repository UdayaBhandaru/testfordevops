import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument, WorkflowAction } from "@agility/frameworkcore";
import { HttpHelperService } from "../Common/HttpHelperService";
import { HttpParams } from "@angular/common/http";
import { SharedService } from "../Common/SharedService";
import { AllocationHeaderModel } from './AllocationHeaderModel';
import { AllocationDetailModel } from './AllocationDetailModel';
import { AllocationOrderDetailModel } from './AllocationOrderDetailModel';

@Injectable()
export class AllocationService {
  serviceDocument: ServiceDocument<AllocationOrderDetailModel> = new ServiceDocument<AllocationOrderDetailModel>();
  searchData: any;
  constructor(private httpHelperService: HttpHelperService, private sharedService: SharedService) { }

  newModel(model: AllocationOrderDetailModel): ServiceDocument<AllocationOrderDetailModel> {
    return this.serviceDocument.newModel(model);
  }

  list(): Observable<ServiceDocument<AllocationOrderDetailModel>> {
    return this.serviceDocument.list("/api/Allocation/List");
  }  

  GetReceivedOrders(orderNo: string): Observable<AllocationOrderDetailModel[]> {
    return this.httpHelperService.get("api/Allocation/GetAllocationOrders", new HttpParams().set("orderNo", orderNo.toString()));
  }

  getDataFromAPI(apiUrl: string): Observable<any> {
    let resp: Observable<any> = this.httpHelperService.get(apiUrl);
    return resp;
  }
}
