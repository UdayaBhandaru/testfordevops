export class AllocationOrderHdrDetailModel {
  orderNo?: number;
  supplier?: number;
  supName?: string;
  locType?: string;
  location?: number;
  locName?: string; 
}
