export class AllocationDetailModel {
  toLoc?: number;
  toLocName?: string;
  toLocType?: string;
  qtyAllocated?: number;
  operation: string;
  previousQty?: number;
}
