import { AllocationHeaderModel } from './AllocationHeaderModel';

export class AllocationOrderDetailModel {
  orderNo?: number;
  item?: string;
  itemDesc?: string;
  location?: string;
  locName?: string;
  estDate?: Date;
  qtyOrdered?: number;
  supName: string;
  allocationHeader?: AllocationHeaderModel;
}
