import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../AgGridSharedModule";
import { TitleSharedModule } from "../TitleSharedModule";
import { RbsSharedModule } from "../RbsSharedModule";
import { AllocationListComponent } from './AllocationListComponent';
import { AllocationService } from './AllocationService';
import { AllocationListResolver } from './AllocationListResolver';
import { AllocationRoutingModule } from './AllocationRoutingModule';
import { AllocationPopupComponent } from './AllocationPopupComponent';
import { AllocationPopupService } from './AllocationPopupService';

@NgModule({
  imports: [
    FrameworkCoreModule,
    AllocationRoutingModule,
    AgGridModule,
    AgGridSharedModule,
    TitleSharedModule,
    RbsSharedModule
  ],
  declarations: [
    AllocationListComponent
  ],
  providers: [
    AllocationService,
    AllocationListResolver,
    AllocationPopupService
  ]
})
export class AllocationModule {
}
