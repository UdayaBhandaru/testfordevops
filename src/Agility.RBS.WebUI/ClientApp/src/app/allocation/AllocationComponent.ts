import { Component } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular/main";

@Component({
  selector: "child-cell",
  template: `<div fxLayout="row" class="grid-actions inbox-styles-action">           
            <a href="#" (click)="invokeParentMethod($event,'preAllocate')" matTooltip="PreAllocate" [matTooltipPosition]="position">
                 <span class="preAllocate">PreAllocate</span></a>
        </div>`
})
export class AllocationComponent implements ICellRendererAngularComp {
  public params: any;
  public position: string = "above";

  agInit(params: any): void {
    this.params = params;
  }

  refresh(params: any): boolean {
    return true;
  }

  public invokeParentMethod($event: MouseEvent, mode: string): void {
    $event.preventDefault();
    this.params.context.componentParent.open(this.params, mode);
  }
}
