import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { AllocationService } from './AllocationService';
import { AllocationOrderDetailModel } from './AllocationOrderDetailModel';

@Injectable()
export class AllocationListResolver implements Resolve<ServiceDocument<AllocationOrderDetailModel>> {
  constructor(private service:  AllocationService) { }
  resolve(): Observable<ServiceDocument<AllocationOrderDetailModel>> {
        return this.service.list();
    }
}
