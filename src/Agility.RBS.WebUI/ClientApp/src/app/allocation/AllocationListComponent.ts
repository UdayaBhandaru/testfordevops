import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument} from "@agility/frameworkcore";
import { GridOptions, ColDef, GridReadyEvent, GridApi } from 'ag-grid-community';
import { SharedService } from '../Common/SharedService';
import { LoaderService } from '../Common/LoaderService';
import { AllocationService } from './AllocationService';
import { AllocationOrderDetailModel } from './AllocationOrderDetailModel';
import { Subscription } from 'rxjs';
import { AllocationOrderHdrDetailModel } from './AllocationOrderHdrDetailModel';
import { AllocationPopupComponent } from './AllocationPopupComponent';
import { AllocationPopupService } from './AllocationPopupService';
import { AllocationDomainModel } from './AllocationDomainModel';
import { AllocationComponent } from './AllocationComponent';

@Component({
  selector: "allocation-List",
  templateUrl: "./AllocationListComponent.html"
})
export class AllocationListComponent implements OnInit {
  public serviceDocument: ServiceDocument<AllocationOrderDetailModel>;
  dataList: AllocationOrderDetailModel[] = [];
  PageTitle = "Allocation";
  componentName: any;
  columns: ColDef[];
  OrderDetailsServiceSubscription: Subscription;
  model: AllocationOrderDetailModel = {
    orderNo: null, item: null, itemDesc: null, qtyOrdered: null, location: null, locName: null, estDate: null, supName: null
  };
  gridOptions: GridOptions;
  gridApi: GridApi;
  constructor(public service: AllocationService, private sharedService: SharedService, private router: Router, private loaderService: LoaderService,
    private changeRef: ChangeDetectorRef, private allocationPopupService: AllocationPopupService) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { colId: "item", headerName: "Item", field: "item", tooltipField: "item" },
      { colId: "itemDesc", headerName: "Item Desc", field: "itemDesc", tooltipField: "itemDesc" },
      { colId: "qtyOrdered", headerName: "Quantity Ordered", field: "qtyOrdered", tooltipField: "qtyOrdered" },
      { headerName: "Actions", field: "Actions", cellRendererFramework: AllocationComponent },
    ];
    this.gridOptions = {
      onGridReady: (params: GridReadyEvent): void => {
        this.gridApi = params.api;
        params.api.startEditingCell({ rowIndex: 0, colKey: "receivedQuantity" });

      }
    }
    this.service.newModel(this.model);
    this.serviceDocument = this.service.serviceDocument;
  }  

  getDetails(): void {
    this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.service.GetReceivedOrders(this.serviceDocument.dataProfile.profileForm.controls["orderNo"].value).subscribe(rd => {
        this.dataList = rd;
      });
    }
  }

  getOrderInfo(event: any): void {
    if (event) {
      let apiOrderUrl: string = "/api/Allocation/AllocationOrderDetailsGet?orderNo=" + event;
      this.OrderDetailsServiceSubscription = this.service.getDataFromAPI(apiOrderUrl).subscribe((response: AllocationOrderHdrDetailModel) => {
        if (response != null) {
          this.serviceDocument.dataProfile.profileForm.controls["locName"].setValue(response.locName);
          this.serviceDocument.dataProfile.profileForm.controls["supName"].setValue(response.supName);
        } else {
          this.sharedService.errorForm("Order Number Is Not Valid");
          this.serviceDocument.dataProfile.profileForm.controls["orderNo"].reset();
        }

      }, (error: string) => { console.log(error); });
    }
  }

  public open(cell: any, mode: string): void {
    if (mode === "preAllocate") {
      this.fetchAllocation(cell, mode);
    }
  }
  public fetchAllocation(cell: any, mode: string): void {

    this.allocationPopupService.allocationDomainData(cell.data.item)
      .subscribe((response: AllocationDomainModel) => {
        if (response != null) {
          let data: any = {
            "AllocationData": cell.data,
            "AllocationDomainData": response
          };
          this.sharedService.openDilogForFindImDoc(AllocationPopupComponent, data,  "Allocation",
          );
        }
      },
      (err: any) => { this.sharedService.errorForm(err); });
  }

  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }  
}
