import { Component, OnInit } from "@angular/core";
import { ServiceDocument } from "@agility/frameworkcore";
import { SupplierTraitService } from "./SupplierTraitService";
import { SupplierTraitModel } from './SupplierTraitModel';
import { GridEditComponent } from '../../Common/grid/GridEditComponent';
import { InfoComponent } from '../../Common/InfoComponent';
import { SharedService } from '../../Common/SharedService';

@Component({
  selector: "SupplierTrait-List",
  templateUrl: "./SupplierTraitListComponent.html"
})
export class SupplierTraitListComponent implements OnInit {

  public serviceDocument: ServiceDocument<SupplierTraitModel>;
  columns: any[];
  public componentName: any;

  constructor(public service: SupplierTraitService, private sharedService: SharedService) {
  }
  PageTitle = "Supplier Trait";
  redirectUrl = "SupplierTrait";
  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "Supplier Trait", field: "supplierTraitId", tooltipField: "supplierTraitId" },
      { headerName: "Description", field: "description", tooltipField: "description" },
      { headerName: "Master Supplier Indicator", field: "masterSupplierIndicator", tooltipField: "masterSupplierIndicator" },
      { headerName: "Master Supplier", field: "masterSupplier", tooltipField: "masterSupplier" },
      { headerName: "operation", field: "operation", tooltipField: "operation" },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 90
      }
    ];
    this.serviceDocument = this.service.serviceDocument;
  }

  open(cell: any, mode: string): void {
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }
}
