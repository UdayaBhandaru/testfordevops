import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { HttpHelperService } from "../../Common/HttpHelperService";
import { SupplierTraitModel } from './SupplierTraitModel';
@Injectable()
export class SupplierTraitService {
  serviceDocument: ServiceDocument<SupplierTraitModel> = new ServiceDocument<SupplierTraitModel>();
  searchData: any;
  constructor(private httpHelperService: HttpHelperService) { }

  newModel(model: SupplierTraitModel): ServiceDocument<SupplierTraitModel> {
    return this.serviceDocument.newModel(model);
  }

  list(): Observable<ServiceDocument<SupplierTraitModel>> {
    return this.serviceDocument.list("/api/SupplierTrait/List");
  }

  save(): Observable<ServiceDocument<SupplierTraitModel>> {
    return this.serviceDocument.save("/api/SupplierTrait/Save", true);
  }
}
