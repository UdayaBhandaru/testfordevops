import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { SupplierTraitService } from "./SupplierTraitService";
import { SupplierTraitModel } from './SupplierTraitModel';

@Injectable()
export class SupplierTraitResolver implements Resolve<ServiceDocument<SupplierTraitModel>> {
  constructor(private service: SupplierTraitService) { }
  resolve(): Observable<ServiceDocument<SupplierTraitModel>> {
    return this.service.list();
  }
}
