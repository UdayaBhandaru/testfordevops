import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { SupplierTraitListComponent } from "./SupplierTraitListComponent";
import { SupplierTraitResolver } from "./SupplierTraitResolver";
import { SupplierTraitComponent } from "./SupplierTraitComponent"
const DomainRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "", redirectTo: "List", pathMatch: "full"
      },
      {
        path: "List",
        component: SupplierTraitListComponent,
        resolve:
        {
          references: SupplierTraitResolver
        }
      },
      {
        path: "New",
        component: SupplierTraitComponent
      }

    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(DomainRoutes)
  ]
})
export class SupplierTraitRoutingModule { }
