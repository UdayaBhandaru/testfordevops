export class SupplierTraitModel {
  supplierTraitId: number;
  description: string;
  masterSupplierIndicator: string;
  masterSupplier: string;
  operation: string;
}
