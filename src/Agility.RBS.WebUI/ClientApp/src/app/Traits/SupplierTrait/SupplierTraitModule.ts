import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { SupplierTraitRoutingModule } from "./SupplierTraitRoutingModule";
import { SupplierTraitListComponent } from "./SupplierTraitListComponent";
import { SupplierTraitComponent } from "./SupplierTraitComponent";
import { SupplierTraitService } from "./SupplierTraitService";
import { SupplierTraitResolver } from "./SupplierTraitResolver";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { RbsSharedModule } from "../../RbsSharedModule";

@NgModule({
  imports: [
    FrameworkCoreModule,
    SupplierTraitRoutingModule,
    AgGridModule,
    AgGridSharedModule,
    TitleSharedModule,
    RbsSharedModule
  ],
  declarations: [
    SupplierTraitListComponent,
    SupplierTraitComponent

  ],
  providers: [
    SupplierTraitService,
    SupplierTraitResolver
  ]
})
export class SupplierTraitModule {
}
