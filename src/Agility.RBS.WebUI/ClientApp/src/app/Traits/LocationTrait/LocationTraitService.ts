import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";
import { LocationTraitModel } from './LocationTraitModel';
@Injectable()
export class LocationTraitService {
  serviceDocument: ServiceDocument<LocationTraitModel> = new ServiceDocument<LocationTraitModel>();
  searchData: any;
  constructor(private httpHelperService: HttpHelperService) { }

  newModel(model: LocationTraitModel): ServiceDocument<LocationTraitModel> {
    return this.serviceDocument.newModel(model);
  }

  list(): Observable<ServiceDocument<LocationTraitModel>> {
    return this.serviceDocument.list("/api/LocationTrait/List");
  }

  save(): Observable<ServiceDocument<LocationTraitModel>> {
    return this.serviceDocument.save("/api/LocationTrait/Save", true);
  }
}
