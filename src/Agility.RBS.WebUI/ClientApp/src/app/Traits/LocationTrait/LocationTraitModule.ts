import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LocationTraitRoutingModule } from "./LocationTraitRoutingModule";
import { LocationTraitListComponent } from "./LocationTraitListComponent";
import { LocationTraitComponent } from "./LocationTraitComponent"
import { LocationTraitService } from "./LocationTraitService";
import { LocationTraitResolver } from "./LocationTraitResolver";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { RbsSharedModule } from "../../RbsSharedModule";

@NgModule({
  imports: [
    FrameworkCoreModule,
    LocationTraitRoutingModule,
    AgGridModule,
    AgGridSharedModule,
    TitleSharedModule,
    RbsSharedModule
  ],
  declarations: [
    LocationTraitListComponent,
    LocationTraitComponent

  ],
  providers: [
    LocationTraitService,
    LocationTraitResolver
  ]
})
export class LocationTraitModule {
}
