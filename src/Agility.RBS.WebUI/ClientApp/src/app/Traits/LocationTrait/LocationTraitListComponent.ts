import { Component, OnInit } from "@angular/core";
import { ServiceDocument } from "@agility/frameworkcore";
import { LocationTraitService } from "./LocationTraitService";
import { LocationTraitModel } from './LocationTraitModel';
import { GridEditComponent } from '../../Common/grid/GridEditComponent';
import { InfoComponent } from '../../Common/InfoComponent';
import { SharedService } from '../../Common/SharedService';
import { GridOptions } from 'ag-grid-community';
@Component({
  selector: "LocationTrait-List",
  templateUrl: "./LocationTraitListComponent.html",
  styleUrls: ['./LocationTraitListComponent.scss']
})
export class LocationTraitListComponent implements OnInit {

  public serviceDocument: ServiceDocument<LocationTraitModel>;
  columns: any[];
  public  gridOptions: GridOptions;
  public componentName: any;
  constructor(public service: LocationTraitService, private sharedService: SharedService) {
  }
  PageTitle = "Location Trait";
  redirectUrl = "LocationTrait";
  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "Location Trait", field: "loacationTraitId", tooltipField: "loacationTraitId " },
      { headerName: "Description", field: "description", tooltipField: "description" },
      { headerName: "Filter Organization", field: "filterOrganizationId", tooltipField: "filterOrganizationId" },
      { headerName: "operation", field: "operation", tooltipField: "operation" },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 90
      }
    ];
    this.serviceDocument = this.service.serviceDocument;
  }

  open(cell: any, mode: string): void {
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }
}
