export class LocationTraitModel {
  locationTraitId: number;
  description: string;
  filterOrganizationId: string;
  operation: string;
}
