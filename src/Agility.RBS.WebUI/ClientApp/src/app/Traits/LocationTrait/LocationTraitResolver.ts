import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { LocationTraitService } from "./LocationTraitService";
import { LocationTraitModel } from './LocationTraitModel';

@Injectable()
export class LocationTraitResolver implements Resolve<ServiceDocument<LocationTraitModel>> {
  constructor(private service: LocationTraitService) { }
  resolve(): Observable<ServiceDocument<LocationTraitModel>> {
    return this.service.list();
  }
}
