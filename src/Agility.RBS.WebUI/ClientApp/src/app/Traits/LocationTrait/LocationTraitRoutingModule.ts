import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LocationTraitListComponent } from "./LocationTraitListComponent";
import { LocationTraitResolver } from "./LocationTraitResolver";
import { LocationTraitComponent } from "./LocationTraitComponent";


const DomainRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "", redirectTo: "List", pathMatch: "full"
      },
      {
        path: "List",
        component: LocationTraitListComponent,
        resolve:
        {
          references: LocationTraitResolver
        }
      },
      {
        path: "New",
        component: LocationTraitComponent
      },

    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(DomainRoutes)
  ]
})
export class LocationTraitRoutingModule { }
