﻿import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { AgGridSharedModule } from "../AgGridSharedModule";
import { TitleSharedModule } from "../TitleSharedModule";
import { SupplierGroupRoutingModule } from "./SupplierGroupRoutingModule";
import { SupplierGroupComponent } from "./SupplierGroupComponent";
import { SupplierGroupListComponent } from "./SupplierGroupListComponent";
import { SupplierGroupService } from "./SupplierGroupService";
import { SupplierGroupResolver } from "./SupplierGroupResolver";
import { RbsSharedModule } from "../RbsSharedModule";
@NgModule({
    imports: [
        FrameworkCoreModule,
        SupplierGroupRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        SupplierGroupListComponent,
        SupplierGroupComponent
    ],
    providers: [
        SupplierGroupService,
        SupplierGroupResolver
    ]
})
export class SupplierGroupModule {
    constructor() {
        LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
    }
}
