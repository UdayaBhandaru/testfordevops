import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { SharedService } from "../Common/SharedService";
import { DomainDetailModel } from "../domain/DomainDetailModel";
import { SupplierGroupModel } from "./SupplierGroupModel";
import { SupplierGroupService } from "./SupplierGroupService";
import { CommonModel } from "../Common/CommonModel";
@Component({
    selector: "supplierGroup",
    templateUrl: "./SupplierGroupComponent.html"
})

export class SupplierGroupComponent implements OnInit {
    PageTitle = "SupplierGroup";
    serviceDocument: ServiceDocument<SupplierGroupModel>;
    supplierGroupModel: SupplierGroupModel;
    localizationData: any;
    domainDtlData: DomainDetailModel[]; defaultStatus: string;
    editMode: boolean = false; editModel: SupplierGroupModel;
    model: SupplierGroupModel;
    supplierGroupData: CommonModel[];

    constructor(private service: SupplierGroupService, private sharedService: SharedService
        , private router: Router, private ref: ChangeDetectorRef) {
    }

    ngAfterViewChecked(): void {
        this.ref.detectChanges();
    }

    ngOnInit(): void {
        this.domainDtlData = Object.assign([], this.service.serviceDocument.domainData["status"]);
        this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
        this.defaultStatus = this.domainDtlData.filter(itm => itm.requiredInd === "Y")[0].code;
        this.bind();
    }

    bind(): void {
        delete this.editModel;
        if (this.sharedService.editObject) {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
            this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
            this.editMode = true;
            delete this.sharedService.editObject;
        } else {
            this.model = {
              id: null, name: null, status: this.defaultStatus
                , statusDesc: null, operation: "I"
            };
            this.service.serviceDocument.dataProfile.dataModel = this.model;
        }
        this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
        this.serviceDocument = this.service.serviceDocument;
    }

    save(saveOnly: boolean): void {
        if (this.serviceDocument.dataProfile.profileForm.valid) {
            this.saveSubscription(saveOnly);
        } else {
            this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
        }
    }

    saveAndContinue(): void {
        this.save(false);
    }

    saveSubscription(saveOnly: boolean): void {
        this.checkSupplierGroup(saveOnly);
    }
    saveSupplierGroup(saveOnly: boolean): void {
        this.service.save().subscribe(() => {
            if (this.service.serviceDocument.result.type === MessageType.success) {
                this.sharedService.saveForm(this.localizationData.supplierGroup.suppliergroupsave).subscribe(() => {
                    if (saveOnly) {
                        this.router.navigate(["/SupplierGroup/List"], { skipLocationChange: true });
                    } else {
                        if (!this.editMode) {
                            this.model = {
                              id: null, name: null, status: this.defaultStatus, statusDesc: null, operation: "I"
                            };
                            this.service.newModel(this.model);
                        } else {
                            Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
                            this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.editModel);
                        }
                        this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
                        this.serviceDocument = this.service.serviceDocument;
                    }
                }, (error: string) => { console.log(error); });
            } else {
                this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
            }
        }, (error: string) => { console.log(error); });
    }

    checkSupplierGroup(saveOnly: boolean): void {
        if (!this.editMode) {
            this.service.isExistingSupplierGroup(this.serviceDocument.dataProfile.profileForm.controls["id"].value
                , this.serviceDocument.dataProfile.profileForm.controls["name"].value)
                .subscribe((response: any) => {
                    if (response) {
                        this.sharedService.errorForm(this.localizationData.supplierGroup.suppliergroupcheck);
                    } else {
                        this.saveSupplierGroup(saveOnly);
                    }
                });
        } else {
            this.saveSupplierGroup(saveOnly);
        }
    }

    reset(): void {
        if (!this.editMode) {
            this.model = {
              id: null, name: null, status: this.defaultStatus, statusDesc: null, operation: "I"
            };
            this.service.newModel(this.model);
        } else {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
        }
        this.serviceDocument = this.service.serviceDocument;
        this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
    }
}
