import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { SharedService } from "../Common/SharedService";
import { DomainDetailModel } from "../domain/DomainDetailModel";
import { GridEditComponent } from "../Common/grid/GridEditComponent";
import { InfoComponent } from "../Common/InfoComponent";
import { GridExportModel } from "../Common/grid/GridExportModel";
import { FormGroup } from "@angular/forms";
import { SupplierGroupModel } from "./SupplierGroupModel";
import { SupplierGroupService } from "./SupplierGroupService";
import { GridOptions } from 'ag-grid-community';

@Component({
  selector: "supplierGroup",
  templateUrl: "./SupplierGroupListComponent.html"
})
export class SupplierGroupListComponent implements OnInit {
  public serviceDocument: ServiceDocument<SupplierGroupModel>;
  PageTitle = "Supplier Group";
  sheetName: string = "SupplierGroup";
  redirectUrl = "SupplierGroup";
  componentName: any;
  columns: any[];
  domainDtlData: DomainDetailModel[];
  model: SupplierGroupModel;
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  gridOptions: GridOptions;

  constructor(public service: SupplierGroupService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "ID", field: "id", tooltipField: "id", width: 60 },
      { headerName: "Name", field: "name", tooltipField: "name" },
      { headerName: "Status", field: "statusDesc", tooltipField: "statusDesc", width: 45 },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 50
      }
    ];
    this.domainDtlData = this.service.serviceDocument.domainData["status"];
    delete this.sharedService.domainData;
    this.sharedService.domainData = { status: this.domainDtlData };
    this.model = { id: null, name: null, status: null, statusDesc: null };
    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }
}
