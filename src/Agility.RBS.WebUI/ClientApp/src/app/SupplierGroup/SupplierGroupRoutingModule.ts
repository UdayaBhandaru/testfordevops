﻿import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { SupplierGroupResolver} from "./SupplierGroupResolver";
import { SupplierGroupComponent } from "./SupplierGroupComponent";
import { SupplierGroupListComponent } from "./SupplierGroupListComponent";
const SupplierGroupRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: SupplierGroupListComponent,
                resolve:
                {
                    serviceDocument: SupplierGroupResolver
                }
            },
            {
                path: "New",
                component: SupplierGroupComponent,
                resolve:
                {
                    serviceDocument: SupplierGroupResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(SupplierGroupRoutes)
    ]
})
export class SupplierGroupRoutingModule { }
