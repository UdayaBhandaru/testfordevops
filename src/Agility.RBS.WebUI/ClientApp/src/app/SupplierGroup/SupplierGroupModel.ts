﻿export class SupplierGroupModel {
    id: number;
    name: string;
    status: string;
    statusDesc: string;
    operation?: string;
}