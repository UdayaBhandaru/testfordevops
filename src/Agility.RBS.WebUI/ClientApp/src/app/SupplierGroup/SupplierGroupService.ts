﻿import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { SupplierGroupModel } from "./SupplierGroupModel";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../Common/HttpHelperService";

@Injectable()
export class SupplierGroupService {
    serviceDocument: ServiceDocument<SupplierGroupModel> = new ServiceDocument<SupplierGroupModel>();
    constructor(private httpHelperService: HttpHelperService) { }
    newModel(model: SupplierGroupModel): ServiceDocument<SupplierGroupModel> {
        return this.serviceDocument.newModel(model);
    }

    search(): Observable<ServiceDocument<SupplierGroupModel>> {
        return this.serviceDocument.search("/api/SupplierGroup/Search");
    }

    save(): Observable<ServiceDocument<SupplierGroupModel>> {
        return this.serviceDocument.save("/api/SupplierGroup/Save", true);
    }

    list(): Observable<ServiceDocument<SupplierGroupModel>> {
        return this.serviceDocument.list("/api/SupplierGroup/List");
    }

    addEdit(): Observable<ServiceDocument<SupplierGroupModel>> {
        return this.serviceDocument.list("/api/SupplierGroup/New");
    }

    isExistingSupplierGroup(id: number, name:string): Observable<boolean> {
        return this.httpHelperService.get("/api/SupplierGroup/IsExistingSupplierGroup", new HttpParams().set("id", id.toString()).set("name", name));
    }
}