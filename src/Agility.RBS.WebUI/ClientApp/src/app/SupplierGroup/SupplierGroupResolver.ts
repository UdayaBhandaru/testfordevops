﻿import { Injectable } from "@angular/core";
import { Resolve } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { SupplierGroupModel } from "./SupplierGroupModel";
import { SupplierGroupService } from "./SupplierGroupService";
@Injectable()
export class SupplierGroupListResolver implements Resolve<ServiceDocument<SupplierGroupModel>> {
    constructor(private service: SupplierGroupService) { }
    resolve(): Observable<ServiceDocument<SupplierGroupModel>> {
        return this.service.list();
    }
}

@Injectable()
export class SupplierGroupResolver implements Resolve<ServiceDocument<SupplierGroupModel>> {
    constructor(private service: SupplierGroupService) { }
    resolve(): Observable<ServiceDocument<SupplierGroupModel>> {
        return this.service.addEdit();
    }
}



