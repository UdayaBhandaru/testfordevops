import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from '../Common/HttpHelperService';
import { CompetitivePriceHistoryModel } from './CompetitivePriceHistoryModel';

@Injectable()
export class CompetitivePriceHistoryService {
  serviceDocument: ServiceDocument<CompetitivePriceHistoryModel> = new ServiceDocument<CompetitivePriceHistoryModel>();
    constructor(private httpHelperService: HttpHelperService) { }

  newModel(model: CompetitivePriceHistoryModel): ServiceDocument<CompetitivePriceHistoryModel> {
        return this.serviceDocument.newModel(model);
    }

  search(): Observable<ServiceDocument<CompetitivePriceHistoryModel>> {
    return this.serviceDocument.search("/api/CompetitivePriceHistory/Search");
    }

  save(): Observable<ServiceDocument<CompetitivePriceHistoryModel>> {
    return this.serviceDocument.save("/api/CompetitivePriceHistory/Save", true);
    }

  list(): Observable<ServiceDocument<CompetitivePriceHistoryModel>> {
    return this.serviceDocument.list("/api/CompetitivePriceHistory/List");
    }

  addEdit(): Observable<ServiceDocument<CompetitivePriceHistoryModel>> {
    return this.serviceDocument.list("/api/CompetitivePriceHistory/New");
    }

  isExistingCompetitivePriceHistory(competitor: number, compName: string): Observable<boolean> {
    return this.httpHelperService.get("/api/CompetitivePriceHistory/IsExistingCompetitivePriceHistory", new HttpParams().set("competitor", competitor.toString()).set("compName", compName));
    }
}
