import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { AgGridSharedModule } from '../AgGridSharedModule';
import { TitleSharedModule } from '../TitleSharedModule';
import { RbsSharedModule } from '../RbsSharedModule';
import { CompetitivePriceHistoryRoutingModule } from './CompetitivePriceHistoryRoutingModule';
import { CompetitivePriceHistoryListComponent } from './CompetitivePriceHistoryListComponent';
import { CompetitivePriceHistoryComponent } from './CompetitivePriceHistoryComponent';
import { CompetitivePriceHistoryService } from './CompetitivePriceHistoryService';
import { CompetitivePriceHistoryResolver, CompetitivePriceHistoryListResolver } from './CompetitivePriceHistoryResolver';

@NgModule({
    imports: [
        FrameworkCoreModule,
        CompetitivePriceHistoryRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        CompetitivePriceHistoryListComponent,
        CompetitivePriceHistoryComponent
    ],
    providers: [
        CompetitivePriceHistoryService,
        CompetitivePriceHistoryResolver,
        CompetitivePriceHistoryListResolver
    ]
})
export class CompetitivePriceHistoryModule {
    constructor() {
        LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
    }
}
