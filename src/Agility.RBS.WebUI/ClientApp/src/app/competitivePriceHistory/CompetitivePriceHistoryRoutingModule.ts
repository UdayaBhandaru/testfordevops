import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CompetitivePriceHistoryListComponent } from './CompetitivePriceHistoryListComponent';
import { CompetitivePriceHistoryListResolver, CompetitivePriceHistoryResolver } from './CompetitivePriceHistoryResolver';
import { CompetitivePriceHistoryComponent } from './CompetitivePriceHistoryComponent';

const UtilityRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: CompetitivePriceHistoryListComponent,
                resolve:
                {
                  serviceDocument: CompetitivePriceHistoryListResolver
                }
            },
            {
                path: "New",
              component: CompetitivePriceHistoryComponent,
                resolve:
                {
                  serviceDocument: CompetitivePriceHistoryResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(UtilityRoutes)
    ]
})
export class CompetitivePriceHistoryRoutingModule { }
