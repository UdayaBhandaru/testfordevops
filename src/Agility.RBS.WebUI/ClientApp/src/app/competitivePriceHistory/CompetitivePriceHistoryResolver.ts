import { Injectable } from "@angular/core";
import { Resolve } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { CompetitivePriceHistoryModel } from './CompetitivePriceHistoryModel';
import { CompetitivePriceHistoryService } from './CompetitivePriceHistoryService';
@Injectable()
export class CompetitivePriceHistoryListResolver implements Resolve<ServiceDocument<CompetitivePriceHistoryModel>> {
  constructor(private service: CompetitivePriceHistoryService) { }
  resolve(): Observable<ServiceDocument<CompetitivePriceHistoryModel>> {
        return this.service.list();
    }
}

@Injectable()
export class CompetitivePriceHistoryResolver implements Resolve<ServiceDocument<CompetitivePriceHistoryModel>> {
  constructor(private service: CompetitivePriceHistoryService) { }
  resolve(): Observable<ServiceDocument<CompetitivePriceHistoryModel>> {
        return this.service.addEdit();
    }
}


