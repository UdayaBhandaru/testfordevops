import { Component, OnInit, ChangeDetectorRef, NgModule } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { GridOptions } from "ag-grid-community";
import { SharedService } from '../Common/SharedService';
import { InfoComponent } from '../Common/InfoComponent';
import { GridExportModel } from '../Common/grid/GridExportModel';
import { GridEditComponent } from '../Common/grid/GridEditComponent';
import { CompetitivePriceHistoryModel } from './CompetitivePriceHistoryModel';
import { CompetitivePriceHistoryService } from './CompetitivePriceHistoryService';
import { GridDateComponent } from '../Common/grid/GridDateComponent';

@Component({
  selector: "competitivePriceHistory-list",
  templateUrl: "./CompetitivePriceHistoryListComponent.html"
})
export class CompetitivePriceHistoryListComponent implements OnInit {
  public serviceDocument: ServiceDocument<CompetitivePriceHistoryModel>;
  PageTitle = "Competitive Price History";
  redirectUrl = "CompetitivePriceHistory";
  componentName: any;
  columns: any[];
  model: CompetitivePriceHistoryModel = { item: null, refItem: null, compStore: null, compStoreDesc: null, recDate: null, compRetail: null, compRetailType: null, multiUnits: null, multiUnitRetail: null, promStartDate: null, promEndDate: null, offerType: null, postDate: null, rpmFull: null };
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  gridOptions: GridOptions;

  constructor(public service: CompetitivePriceHistoryService, private sharedService: SharedService, private router: Router, private ref: ChangeDetectorRef) {
  }

  ngAfterViewChecked(): void {
    this.ref.detectChanges();
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "ITEM", field: "item", tooltipField: "item", width: 60 },
      { headerName: "COMPSTOREDESC", field: "compStoreDesc", tooltipField: "compStoreDesc", width: 90 },
      { headerName: "RECDATE", field: "recDate", tooltipField: "recDate", width: 90, cellRendererFramework: GridDateComponent },
      { headerName: "COMPRETAIL", field: "compRetail", tooltipField: "compRetail", width: 90 },
      {
        headerName: "ACTIONS", field: "Actions", cellRendererFramework: GridEditComponent, width: 60
      }
    ];

    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }
}
