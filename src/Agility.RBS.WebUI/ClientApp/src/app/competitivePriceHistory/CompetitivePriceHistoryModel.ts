export class CompetitivePriceHistoryModel {
  item?: number;
  refItem?: string;
  compStore?: number;
  compStoreDesc?: string;
  recDate?: Date;
  compRetail?: number;
  compRetailType?: string;
  multiUnits?: number;
  multiUnitRetail?: number;
  promStartDate?: Date;
  promEndDate?: Date;
  offerType?: string;
  postDate?: Date;
  rpmFull?: string;
  operation?: string;  
}
