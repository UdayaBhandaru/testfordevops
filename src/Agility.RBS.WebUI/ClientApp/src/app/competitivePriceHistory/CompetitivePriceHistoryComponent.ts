import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { FormControl, FormGroup } from "@angular/forms";
import { SharedService } from '../Common/SharedService';
import { CountryDomainModel } from '../Common/DomainData/CountryDomainModel';
import { CompetitivePriceHistoryModel } from './CompetitivePriceHistoryModel';
import { CompetitivePriceHistoryService } from './CompetitivePriceHistoryService';

@Component({
    selector: "CompetitivePriceHistory",
    templateUrl: "./CompetitivePriceHistoryComponent.html"
})
export class CompetitivePriceHistoryComponent implements OnInit {
    PageTitle = "Competitive Price History";
    serviceDocument: ServiceDocument<CompetitivePriceHistoryModel>;
    localizationData: any;
    editModel: CompetitivePriceHistoryModel;
    editMode: boolean = false;
    defaultStatus: string;
    model: CompetitivePriceHistoryModel;
    countryData: CountryDomainModel[];


  constructor(private service: CompetitivePriceHistoryService, private sharedService: SharedService, private router: Router) {
    }

  ngOnInit(): void {
        this.countryData = Object.assign([], this.service.serviceDocument.domainData["country"]);
        this.bind();
    }

    bind(): void {
        delete this.editModel;
        if (this.sharedService.editObject) {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
            this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            delete this.sharedService.editObject;
            this.editMode = true;
        } else {
          this.model = { operation: "I", item: null, refItem: null, compStore: null, compStoreDesc: null, recDate: null, compRetail: null, compRetailType: null, multiUnits: null, multiUnitRetail: null, promStartDate: null, promEndDate: null, offerType: null, postDate: null, rpmFull: null};
            this.service.newModel(this.model);
        }
      this.serviceDocument = this.service.serviceDocument;
    }

    save(saveOnly: boolean): void {
        if (this.serviceDocument.dataProfile.profileForm.valid) {
            this.saveSubscription(saveOnly);
        } else {
            this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
        }
    }

    saveAndContinue(): void {
        this.save(false);
    }

    saveSubscription(saveOnly: boolean): void {
      this.saveCompetitivePriceHist(saveOnly);
    }
    saveCompetitivePriceHist(saveOnly: boolean): void {
        this.service.save().subscribe(() => {
            if (this.service.serviceDocument.result.type === MessageType.success) {
              this.sharedService.saveForm("CompetitivePriceHistory Saved Successfully").subscribe(() => {
                    if (saveOnly) {
                      this.router.navigate(["/CompetitivePriceHistory/List"], { skipLocationChange: true });
                    } else {
                        if (!this.editMode) {
                            this.model = {
                              operation: "I", item: null, refItem: null, compStore: null, compStoreDesc: null, recDate: null,
                              compRetail: null, compRetailType: null, multiUnits: null, multiUnitRetail: null,
                              promStartDate: null, promEndDate: null, offerType: null, postDate: null, rpmFull: null
                            };
                            this.service.newModel(this.model);
                        } else {
                            Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
                            this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.editModel);
                            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
                        }
                    }
                }, (error: string) => { console.log(error); });
            } else {
                this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
            }
        }, (error: string) => { console.log(error); });
    }

    checkCompetitors(saveOnly: boolean): void {
        if (!this.editMode) {
            var form: FormGroup = this.serviceDocument.dataProfile.profileForm;
          if (form.controls["competitor"].value && form.controls["compName"].value) {
            this.service.isExistingCompetitivePriceHistory(form.controls["competitor"].value, form.controls["compName"].value)
                    .subscribe((response: any) => {
                        if (response) {
                            this.sharedService.errorForm("Record alredy exits");
                        } else {
                            this.saveCompetitivePriceHist(saveOnly);
                        }
                    });
            }
        } else {
            this.saveCompetitivePriceHist(saveOnly);
        }
    }

    reset(): void {
        if (!this.editMode) {
            this.model = {
              compRetail: null, operation: "I"
            };
            this.service.newModel(this.model);
        } else {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
        }
        this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
    }    
}
