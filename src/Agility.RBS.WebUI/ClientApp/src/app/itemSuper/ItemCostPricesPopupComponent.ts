import { Component, Inject } from "@angular/core";
import { DatePipe } from "@angular/common";
import { FormGroup } from "@angular/forms";
import { Subscription } from "rxjs";
import { CommonService, FormMode } from "@agility/frameworkcore";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { SharedService } from "../Common/SharedService";
import { ItemPriceModel } from "./ItemPriceModel";
import { GridOptions, GridApi } from "ag-grid-community";
import { ItemCostPriceSuperModel } from "./ItemCostPriceSuperModel";
import { CostChangeItemGroupsModel } from '../CostChange/CostChangeItemGroupsModel';
import { CostChangeItemPriceModel } from '../CostChange/CostChangeItemPriceModel';
import { CostChangeMarketPriceModel } from '../CostChange/CostChangeMarketPriceModel';

@Component({
  selector: "ItemCostPricesPopup",
  templateUrl: "./ItemCostPricesPopupComponent.html",
  styleUrls: ['./ItemCostPricesPopupComponent.scss']
})

export class ItemCostPricesPopupComponent {
  infoGroup: FormGroup;
  itemPricesData: ItemPriceModel[] = [];
  objectValue: number;
  objectName: string;
  sharedSubscription: Subscription;
  componentName: any;
  columns: {}[];
  gridOptions: GridOptions;
  gridApi: GridApi;
  childObjData: ItemCostPriceSuperModel[];
  model: ItemCostPriceSuperModel;
  defaultModel: ItemCostPriceSuperModel;
  rbsGridCustomOptions: { showSearchCriteria: boolean } = { showSearchCriteria: false };

  constructor(private service: SharedService, private commonService: CommonService
    , public dialog: MatDialog
    , @Inject(MAT_DIALOG_DATA)
    public data: {
      dataList: CostChangeItemGroupsModel[]
      , objectName: string, objectValue: number, invokingModule: string
    }
    , private datePipe: DatePipe) {
    this.defaultModel = {
      annualDiscount: null, caseCost: null, compRetail1: null, compRetail2: null, compRetail3: null, compRetail4: null
      , currentPriceCS: null, currentPriceSS: null
      , currentPriceSSM: null, currentPriceWHS: null, marginCS: null, marginSS: null, marginSSM: null, marginWHS: null
      , netUnitCost: null, unitCost: null, item: null, componentQuantity: null
    };
    this.infoGroup = this.commonService.getFormGroup(this.defaultModel, FormMode.View);
    this.childObjData = [];
  }

  ngOnInit(): void {
    if (this.data !== null) {
      let parentObject: CostChangeItemGroupsModel = this.data.dataList.filter(x => +x.itemBarcode === this.data.objectValue)[0];
      this.model = this.fillCostPriceDetails(parentObject);
      this.infoGroup = this.commonService.getFormGroup(this.model, FormMode.View);
      let childObjects: CostChangeItemGroupsModel[] = this.data.dataList.filter(x => +x.parentItem === this.data.objectValue);
      childObjects.forEach(x => {
        this.childObjData.push(this.fillCostPriceDetails(x));
      });
      this.objectValue = this.data.objectValue;
      this.objectName = this.data.objectName;
    } else {
      this.itemPricesData = [];
    }
    this.componentName = this;
    this.setGridColumnsWithOptions();
  }

  fillCostPriceDetails(obj: CostChangeItemGroupsModel): ItemCostPriceSuperModel {
    let itemCostPriceSuperModel: ItemCostPriceSuperModel = this.newModel();
    itemCostPriceSuperModel["item"] = +obj.itemBarcode;
    itemCostPriceSuperModel["componentQuantity"] = obj.uomUnit;
    itemCostPriceSuperModel["annualDiscount"] = obj.annualDiscount;
    itemCostPriceSuperModel["caseCost"] = obj.caseCost;
    itemCostPriceSuperModel["netUnitCost"] = obj.netUnitCost;
    itemCostPriceSuperModel["unitCost"] = obj.unitCost;
    if (obj.itemPrices && obj.itemPrices.length > 0) {
      obj.itemPrices.forEach((x: CostChangeItemPriceModel) => {
        if (x.format === "SS-") {
          itemCostPriceSuperModel["currentPriceSSM"] = x.currentPrice;
          itemCostPriceSuperModel["marginSSM"] = this.service.calculateMargin(x.currentPrice, obj.netUnitCost, 1);
        } else if (x.format === "SS") {
          itemCostPriceSuperModel["currentPriceSS"] = x.currentPrice;
          itemCostPriceSuperModel["marginSS"] = this.service.calculateMargin(x.currentPrice, obj.netUnitCost, 1);
        } else if (x.format === "WHS") {
          itemCostPriceSuperModel["currentPriceWHS"] = x.currentPrice;
          itemCostPriceSuperModel["marginWHS"] = this.service.calculateMargin(x.currentPrice, obj.netUnitCost, 1);
        } else if (x.format === "CS") {
          itemCostPriceSuperModel["currentPriceCS"] = x.currentPrice;
          itemCostPriceSuperModel["marginCS"] = this.service.calculateMargin(x.currentPrice, obj.netUnitCost, 1);
        }
      });
    }
    if (obj.listOfCostChangeMarketPriceExceclModels && obj.listOfCostChangeMarketPriceExceclModels.length > 0) {
      obj.listOfCostChangeMarketPriceExceclModels.forEach((x: CostChangeMarketPriceModel) => {
        if (x.competitor === "404") {
          itemCostPriceSuperModel["compRetail1"] = x.compRetail;
        } else if (x.competitor === "402") {
          itemCostPriceSuperModel["compRetail2"] = x.compRetail;
        } else if (x.competitor === "403") {
          itemCostPriceSuperModel["compRetail3"] = x.compRetail;
        } else if (x.competitor === "412") {
          itemCostPriceSuperModel["compRetail4"] = x.compRetail;
        }
      });
    }
    return itemCostPriceSuperModel;
  }

  newModel(): ItemCostPriceSuperModel {
    let itemCostPriceSuperModel: ItemCostPriceSuperModel = {
      annualDiscount: null, caseCost: null, compRetail1: null, compRetail2: null, compRetail3: null, compRetail4: null
      , currentPriceCS: null, currentPriceSS: null
      , currentPriceSSM: null, currentPriceWHS: null, marginCS: null, marginSS: null, marginSSM: null, marginWHS: null
      , netUnitCost: null, unitCost: null, item: null, componentQuantity: null
    };
    return itemCostPriceSuperModel;
  }

  setGridColumnsWithOptions(): void {
    this.columns = [
      {
        headerName: "Comp Qty", field: "componentQuantity", tooltipField: "componentQuantity", width: 120
      },
      {
        headerName: "Cost", children: [
          {
            headerName: "Case Cost", field: "caseCost", tooltipField: "caseCost", width: 120
          },
          {
            headerName: "Unit Cost", field: "unitCost", tooltipField: "unitCost", width: 120
          },
          {
            headerName: "Annual Disc", field: "annualDiscount", tooltipField: "annualDiscount", width: 120
          },
          {
            headerName: "Net Unit Cost", field: "netUnitCost", tooltipField: "netUnitCost", width: 140
          }
        ]
      },
      {
        headerName: "RSP", children: [
          {
            headerName: "SS-", field: "currentPriceSSM", tooltipField: "currentPriceSSM", width: 80
          },
          {
            headerName: "SS", field: "currentPriceSS", tooltipField: "currentPriceSS", width: 80
          },
          {
            headerName: "WHS", field: "currentPriceWHS", tooltipField: "currentPriceWHS", width: 80
          },
          {
            headerName: "CS", field: "currentPriceCS", tooltipField: "currentPriceCS", width: 80
          }
        ]
      },
      {
        headerName: "Margin", children: [
          {
            headerName: "SS-", field: "marginSSM", tooltipField: "marginSSM", width: 80
            , cellClassRules: {
              "invalid-grid-row": function (params: any): any { return params.value < 0; }
            }
          },
          {
            headerName: "SS", field: "marginSS", tooltipField: "marginSS", width: 80
            , cellClassRules: {
              "invalid-grid-row": function (params: any): any { return params.value < 0; }
            }
          },
          {
            headerName: "WHS", field: "marginWHS", tooltipField: "marginWHS", width: 80
            , cellClassRules: {
              "invalid-grid-row": function (params: any): any { return params.value < 0; }
            }
          },
          {
            headerName: "CS", field: "marginCS", tooltipField: "marginCS", width: 80
            , cellClassRules: {
              "invalid-grid-row": function (params: any): any { return params.value < 0; }
            }
          }
        ]
      },
      {
        headerName: "Market price", children: [
          {
            headerName: "Lulu", field: "compRetail1", tooltipField: "compRetail1", width: 100
          },
          {
            headerName: "Carrefour", field: "compRetail2", tooltipField: "compRetail2", width: 100
          },
          {
            headerName: "COOP", field: "compRetail3", tooltipField: "compRetail3", width: 100
          },
          {
            headerName: "Saveco", field: "compRetail4", tooltipField: "compRetail4", width: 100
          }
        ]
      }
    ];

    this.gridOptions = {
      onGridReady: (params: any) => {
        this.gridApi = params.api;
      },
      context: {
        componentParent: this
      },
      suppressPaginationPanel: true,
      suppressScrollOnNewData: false,
      paginationPageSize: 10,
      pagination: true,
      enableSorting: true,
      rowHeight: 40,
      enableColResize: true,
      rowSelection: "multiple",
      suppressRowClickSelection: true,
      suppressCellSelection: true,
      groupUseEntireRow: true,
      groupDefaultExpanded: -1,
      domLayout: "autoHeight",
      toolPanelSuppressSideButtons: true,
      embedFullWidthRows: true
    };
  }

  close(): void {
    this.dialog.closeAll();
  }
}
