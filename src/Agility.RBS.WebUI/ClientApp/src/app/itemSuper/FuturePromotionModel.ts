export class FuturePromotionModel {
  deptDesc?: string;
  classDesc?: string;
  subclassDesc: string;
  locName: string;
  sellingUomDesc: string;
  actionDate?: Date;
  pcChangeTypeDesc: string;
  pcChangeAmount: number;
  promotion1Id?: number;
  promotion1IdDesc: string;
}
