import { Component, Inject } from "@angular/core";
import { DatePipe } from "@angular/common";
import { FormGroup } from "@angular/forms";
import { Subscription } from "rxjs";
import { CommonService, FormMode } from "@agility/frameworkcore";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { SharedService } from "../Common/SharedService";
import { ItemBasicNewModel } from './ItemBasicNewModel';

@Component({
    selector: "ItemBasicsPopup",
    templateUrl: "./ItemBasicPopupComponent.html",
    styleUrls: ['./ItemBasicPopupComponent.scss']
})

export class ItemBasicPopupComponent {
    infoGroup: FormGroup;
    sharedSubscription: Subscription;
    itemCode: number;

    constructor(private service: SharedService, private commonService: CommonService
        , public dialog: MatDialog, @Inject(MAT_DIALOG_DATA) public data: ItemBasicNewModel
        , private datePipe: DatePipe) {
        this.infoGroup = this.commonService.getFormGroup(this.data, FormMode.View);
        this.itemCode = this.data.item;
        if (this.infoGroup.controls["itemSpecificationValue"].value) {
            var date: Date = new Date(this.infoGroup.controls["itemSpecificationValue"].value);
            this.infoGroup.controls["itemSpecificationValue"].setValue(this.datePipe.transform(date, "dd-MMM-yyyy"));
        }
    }

    close(): void {
        this.dialog.closeAll();
    }
}
