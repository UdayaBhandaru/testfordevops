import { ItemAdditionalUomModel } from "./ItemAdditionalUomModel";

export class ItemBasicNewModel {
    item?: number;
    itemType: string;
    itemTypeDesc?: string;
    itemParent?: string;
    itemGrandparent?: string;
    packInd: string;
    simplePackInd: string;
    itemLevel?: string;
    tranLevel?: string;
    divisionId?: number;
    divisionDesc: string;
    deptId?: number;
    deptDesc: string;
    categoryId?: number;
    categoryDesc: string;
    classId?: number;
    classDesc?: string;
    subClassId?: number;
    subclassDesc: string;
    itemAggregateInd?: string;
    diff1?: string;
    diff1AggregateInd?: string;
    diff2?: string;
    diff2AggregateInd?: string;
    diff3?: string;
    diff3AggregateInd?: string;
    diff4?: string;
    diff4AggregateInd?: string;
    diff5?: string;
    diff5AggregateInd?: string;
    diff6?: string;
    diff6AggregateInd?: string;
    diff7?: string;
    diff7AggregateInd?: string;
    descLong: string;
    descUp: string;
    secondaryDesc: string;
    shortDesc: string;
    onlineDesc: string;
    suppDesc: string;
    brandId?: number;
    unitOfMeasure: string;
    uomUnit: string;
    compQty?: number;
    compUom: string;
    packingDesc: string;
    systemDesc: string;
    status: string;
    standardUom: string;
    uomConvFactor?: number;
    handlingTemp: string;
    handlingSensitivity: string;
    catchWeightInd?: string;
    defaultWastePct?: number;
    constDimenInd: string;
    packType: string;
    orderAsType: string;
    comments?: string;
    giftWrapInd: string;
    shipAloneInd: string;
    itemXformInd: string;
    perishableInd: string;
    langId: string;
    programPhase?: string;
    programMessage?: string;
    pError?: string;
    operation: string;
    supplier?: number;
    sellable: string;
    orderable: string;
    inventory: string;
    vpn?: number;
    barcode: string;
    workflowInstance: string;
    fashionIndicator: string;
    subComments?: string;
    priority?: string;
    startRow?: number;
    endRow?: number;
    totalRows?: number;
    sortModel?: {}[];
    fileCount?: number;
    itemStatus: string;
    itemTier: string;
    expiryFlag: string;
    shortDescArabic: string;
    itemSpecification: string;
    itemSpecificationValue: string;
    itemNature: string;
    itemNatureValue: string;
    itemAdditionalUoms?: ItemAdditionalUomModel[];
}
