import { Component, Inject } from "@angular/core";
import { DatePipe } from "@angular/common";
import { FormGroup, FormControl } from "@angular/forms";
import { Subscription } from "rxjs";
import { CommonService, FormMode } from "@agility/frameworkcore";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { SharedService } from "../Common/SharedService";
import { ItemSuperSearchService } from "./ItemSuperSearchService";
import { UomDomainModel } from "../Common/DomainData/UomDomainModel";
import { CommonModel } from "../Common/CommonModel";
import { LoaderService } from "../Common/LoaderService";
import { ItemSupplierCountryDimensionModel } from './ItemSupplierCountryDimensionModel';

@Component({
    selector: "ItemDimensionsPopup",
    templateUrl: "./ItemDimensionsPopupComponent.html",
    styleUrls: ['./ItemDimensionsPopupComponent.scss'],
    providers: [
        ItemSuperSearchService
    ]
})

export class ItemDimensionsPopupComponent {
  infoGroup: FormGroup;
  sharedSubscription: Subscription;
  itemCode: number;
  LWHData: UomDomainModel[]; weightUOM: UomDomainModel[]; volUOM: UomDomainModel[]; uomData: UomDomainModel[];
  presentationMethodData: CommonModel[] = [];
  dimObjectData: CommonModel[] = [];
  tareTypeData: CommonModel[] = [];
  isEditable: boolean;

  constructor(private service: SharedService, private commonService: CommonService
    , public dialog: MatDialog, @Inject(MAT_DIALOG_DATA) public data: ItemSupplierCountryDimensionModel
    , private datePipe: DatePipe, private itemSuperSearchService: ItemSuperSearchService, private loaderService: LoaderService) {
    this.infoGroup = this.commonService.getFormGroup(this.data, 0);
    this.itemCode = this.data.item;
  }

  ngOnInit(): void {
    this.uomData = Object.assign([], this.data.domainData["uomList"]);
    this.LWHData = this.uomData.filter(x => x.uomClass === "DIMEN");
    this.weightUOM = this.uomData.filter(x => x.uomClass === "MASS");
    this.volUOM = this.uomData.filter(x => x.uomClass === "VOL");
    this.dimObjectData = Object.assign([], this.data.domainData["dimensionObjects"]);
    this.presentationMethodData = Object.assign([], this.data.domainData["presentationMethods"]);
    this.tareTypeData = Object.assign([], this.data.domainData["tareTypes"]);
    this.isEditable = this.service.checkRoleForItemSuper();
  }

  public save(): any {
    this.itemSuperSearchService.saveItemDimensions(this.infoGroup.value).subscribe((response: boolean) => {
      if (response === true) {
        this.service.saveForm(`Dimensions Saved Successfully - Item ${this.itemCode}`).subscribe(() => {
          this.loaderService.display(true);
          this.itemSuperSearchService.getItemDimensions(this.data.item, this.data.importCountry).subscribe((response: ItemSupplierCountryDimensionModel) => {
              if (response) {
                this.infoGroup = this.commonService.getFormGroup(response, 0);
              }
              this.loaderService.display(false);
            }, (error: string) => {
              this.loaderService.display(false);
            });
        });
      }
    }, () => {

    });
  }

  close(): void {
    this.dialog.closeAll();
  }
}
