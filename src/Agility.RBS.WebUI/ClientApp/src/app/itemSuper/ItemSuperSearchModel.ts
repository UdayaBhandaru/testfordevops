import { ItemSuperModel } from "./ItemSuperModel";

export class ItemSuperSearchModel {
  item?: string;
  itemType: string;
  itemTypeDesc?: string;
  itemParent?: string;
  itemGrandparent?: string;
  packInd: string;

  divisionId?: number;
  divisionDesc: string;
  deptId?: number;
  deptDesc: string;
  groupId?: number;
  groupDesc: string;
  classId?: number;
  classDesc?: string;
  subClassId?: number;
  subclassDesc: string;

  descLong: string;
  brandId?: number;
  systemDesc: string;
  status: string;
  standardUom: string;

  langId: string;
  programPhase?: string;
  programMessage?: string;
  pError?: string;
  operation: string;
  supplier?: number;
  sellable: string;
  orderable: string;
  inventory: string;
  vpn?: number;
  barcode: string;
  subComments?: string;

  startRow?: number;
  endRow?: number;
  totalRows?: number;
  sortModel?: {}[];

  itemStatus: string;
  itemTier: string;
  expiryFlag: string;
  expiryInbound?: number;
  expiryOutbound?: number;
  shortDescArabic: string;

  superItemList: ItemSuperModel[];
}
