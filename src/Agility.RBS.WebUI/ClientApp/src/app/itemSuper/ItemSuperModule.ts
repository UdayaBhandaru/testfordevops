﻿import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../AgGridSharedModule";
import { TitleSharedModule } from "../TitleSharedModule";
import { ItemSuperSearchService } from "./ItemSuperSearchService";
import { ItemSharedService } from "../item/ItemSharedService";
import { ItemSuperRoutingModule } from "./ItemSuperRoutingModule";
import { ItemSuperResolver } from "./ItemSuperResolver";
import { ItemSuperSearchComponent } from "./ItemSuperSearchComponent";
import { RbsSharedModule } from "../RbsSharedModule";

@NgModule({
    imports: [
        FrameworkCoreModule,
        ItemSuperRoutingModule,
        AgGridModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        ItemSuperSearchComponent
    ],
    providers: [
        ItemSuperSearchService,
        ItemSharedService,
        ItemSuperResolver
    ]
})
export class ItemSuperModule {
}
