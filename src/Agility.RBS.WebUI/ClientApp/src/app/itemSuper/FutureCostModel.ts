export class FutureCostModel {
  supName: string;
  locName: string;
  locTypeDesc: string;
  activeDate: Date;
  baseCost: number;
  netCost: number;
  netNetCost: number;
  deadNetNetCost: number;
  pricingCost: number;
  calcDate: Date;
}
