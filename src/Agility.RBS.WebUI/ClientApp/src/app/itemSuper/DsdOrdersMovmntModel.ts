﻿export class DsdOrdersMovmntModel {
    orderDate?: Date;
    supplier?: number;
    supName: string;
    orderNumber?: number;
    location?: number;
    locName: string;
    orderQty?: number;
    packSize?: number;
    costPrice?: number;
    retailPrice?: number;
    marginPct?: number;
    totalCost?: number;
    totalPrice?: number;
}