import { Component, OnInit, ViewChild, OnDestroy, HostListener } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType, CommonService, AjaxResult, AjaxModel } from "@agility/frameworkcore";
import { FormGroup, Validators, FormControl } from "@angular/forms";
import { GridOptions, GridApi, ColumnApi, GridReadyEvent } from "ag-grid-community";
import { Subscription } from "rxjs";
import { GridExportModel } from "../Common/grid/GridExportModel";
import { SharedService } from "../Common/SharedService";
import { DomainDetailModel } from "../domain/DomainDetailModel";
import { LoaderService } from "../Common/LoaderService";
import { SupplierDomainModel } from "../Common/DomainData/SupplierDomainModel";
import { SearchComponent } from "../Common/SearchComponent";
import { DivisionDomainModel } from "../Common/DomainData/DivisionDomainModel";
import { DepartmentDomainModel } from "../Common/DomainData/DepartmentDomainModel";
import { GroupDomainModel } from "../Common/DomainData/GroupDomainModel";
import { ClassDomainModel } from "../Common/DomainData/ClassDomainModel";
import { SubClassDomainModel } from "../Common/DomainData/SubClassDomainModel";
import { WorkflowStatusDomainModel } from "../Common/DomainData/WorkflowStatusDomainModel";
import { BrandDomainModel } from "../Common/DomainData/BrandDomainModel";
import { ItemSuperSearchService } from "./ItemSuperSearchService";
import { ItemSuperModel } from "./ItemSuperModel";
import { CountryDomainModel } from "../Common/DomainData/CountryDomainModel";
import { ItemDimensionsPopupComponent } from "./ItemDimensionsPopupComponent";
import { SupplierSuperModel } from "../Supplier/SupplierMaster/SupplierSuperModel";
import { ItemCostPricesPopupComponent } from "./ItemCostPricesPopupComponent";
import { DocumentsConstants } from "../Common/DocumentsConstants";
import { ItemStockModel } from "./ItemStockModel";
import { GridDateComponent } from "../Common/grid/GridDateComponent";
import { ItemSharedService } from "../item/ItemSharedService";
import { ItemOrderModel } from "./ItemOrderModel";
import { ItemBasicPopupComponent } from "./ItemBasicPopupComponent";
import { LocWiseWeeksDataModel } from "./LocWiseWeeksDataModel";
import { ItemSalesModel } from "./ItemSalesModel";
import { WhsMovmntModel } from "./WhsMovmntModel";
import { ItemSupplierPopupComponent } from "./ItemSupplierPopupComponent";
import { ItemRangingPopupComponent } from "./ItemRangingPopupComponent";
import { ItemRangingSuperModel } from "./ItemRangingSuperModel";
import { UomDomainModel } from '../Common/DomainData/UomDomainModel';
import { CostChangeItemGroupsModel } from '../CostChange/CostChangeItemGroupsModel';
import { RbAutoCompleteComponent } from '../Common/controls/RbAutoCompleteComponent';
import { ItemFuturePricePopupComponent } from './ItemFuturePricePopupComponent';
import { FuturePriceModel } from './FuturePriceModel';
import { ItemFutureCostPopupComponent } from './ItemFutureCostPopupComponent';
import { ItemPromotionPopupComponent } from './ItemPromotionPopupComponent';
import { FuturePromotionModel } from './FuturePromotionModel';
import { BarcodeModel } from './BarcodeModel';
import { ItemBarcodePopupComponent } from './ItemBarcodePopupComponent';
import { ItemSupplierCountryDimensionModel } from './ItemSupplierCountryDimensionModel';
import { ItemBasicNewModel } from './ItemBasicNewModel';

@Component({
  selector: "item-list",
  templateUrl: "./ItemSuperSearchComponent.html",
  styleUrls: ['./ItemSuperSearchComponent.scss']
})
export class ItemSuperSearchComponent implements OnInit, OnDestroy {
  @HostListener("document:keydown", ["$event"])
  onKeyDown(event: KeyboardEvent): void {
    if (event.keyCode === 13) {
      this.search();
    } else if (event.keyCode === 37) {
      this.onPrevious();
    } else if (event.keyCode === 39) {
      this.onNext();
    }
  }
  loadedPage: boolean;
  PageTitle = "Product Main Details";
  sheetName: string = "Item Super";
  searchServiceSubscription: Subscription;
  searchBarcodeServiceSubscription: Subscription;
  subClassGetbyNameServiceSubscription: Subscription;
  itemDimensionsGetServiceSubscription: Subscription;
  supplierSuperGetServiceSubscription: Subscription;
  itemCostPricesGetServiceSubscription: Subscription;
  itemStockGetServiceSubscription: Subscription;
  itemOrderGetServiceSubscription: Subscription;
  itemBasicGetServiceSubscription: Subscription;
  itemWarehouseMvmntGetServiceSubscription: Subscription;
  itemDsdMvmntGetServiceSubscription: Subscription;
  itemRangingGetServiceSubscription: Subscription;
  itembarcodesGetServiceSubscription: Subscription;

  itemTypeData: DomainDetailModel[];
  itemYesNoData: DomainDetailModel[];

  divisionData: DivisionDomainModel[];
  group: GroupDomainModel[]; groupData: GroupDomainModel[];
  department: DepartmentDomainModel[]; departmentData: DepartmentDomainModel[];
  class: ClassDomainModel[]; classData: ClassDomainModel[];
  subclass: SubClassDomainModel[]; subclassData: SubClassDomainModel[];
  workflowStatusList: WorkflowStatusDomainModel[];
  brandData: BrandDomainModel[];
  uomData: UomDomainModel[];
  iTierData: DomainDetailModel[];
  statusIndicator: DomainDetailModel[];
  supplierData: SupplierDomainModel[];
  countryData: CountryDomainModel[];

  public serviceDocument: ServiceDocument<ItemSuperModel>;
  itemSuperGroup: FormGroup;
  componentName: any;
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  model: ItemSuperModel;
  additionalSearchParams: any;
  apiUrl: string;
  formControl: FormControl;
  isDataLoaded: boolean = false;
  @ViewChild("searchPanel") searchPanel: SearchComponent;
  invalid: boolean = false;
  currentItemSuperObject: ItemSuperModel;
  indexOfCurrentItem: number;
  isDisableProductBtns: boolean; showPrevNextBtns: boolean;
  supplierColumns: {}[]; stockColumns: {}[];
  orderColumns: {}[]; salesColumns: {}[];
  warehouseMovementColumns: {}[];
  supplierGridOptions: GridOptions; stockGridOptions: GridOptions;
  orderGridOptions: GridOptions; salesGridOptions: GridOptions;
  warehouseMovementGridOptions: GridOptions;
  gridApi: GridApi; gridColumnApi: ColumnApi;
  documentsConstants: DocumentsConstants;
  itemStockData: ItemStockModel[];
  selectedTab: number;
  getItemSuppliersApiUrl: string; getItemCostPricesApiUrl: string;
  getItemStockApiUrl: string; getItemOrdersApiUrl: string;
  getItemBasicApiUrl: string; getItemSalesApiUrl: string; getFuturePricesApiUrl: string; getFutureCostsApiUrl: string;
  getBarcodeApiUrl: string;
  getPromotionApiUrl: string;
  getItemWarehouseMovementApiUrl: string; getItemDsdMovementApiUrl: string;
  itemOrderData: ItemOrderModel[]; itemSalesData: ItemSalesModel[];
  whsMovmntData: WhsMovmntModel[];
  totalUnits: number;
  currentRecordPostion: number;
  apiUrlSupplier: string; apiUrlBrand: string;
  isPackItem: boolean;
  @ViewChild(RbAutoCompleteComponent) autoViewRef: RbAutoCompleteComponent;

  constructor(private sharedService: SharedService, private router: Router
    , private service: ItemSuperSearchService, private loaderService: LoaderService
    , private commonService: CommonService, private itemSharedService: ItemSharedService) {
    this.documentsConstants = new DocumentsConstants();
    this.selectedTab = 0;
    this.getItemSuppliersApiUrl = "/api/Supplier/GetSupplierSuperData";
    this.getFuturePricesApiUrl = "/api/PriceChange/GetFuturePriceData";
    this.getFutureCostsApiUrl = "/api/CostChange/GetFutureCostData";
    this.getPromotionApiUrl = "/api/PromotionList/GetFuturePromotionData";
    this.getItemCostPricesApiUrl = "/api/CostManagement/CostChangeItemGetForParentChildItems";
    this.getItemStockApiUrl = "/api/ItemSuper/GetItemStock";
    this.getItemOrdersApiUrl = "/api/ItemSuper/GetItemOrders";
    this.getItemBasicApiUrl = "/api/ItemSuper/GetItemBasicDetails";
    this.getItemSalesApiUrl = "/api/ItemSuper/GetItemSales";
    this.getItemWarehouseMovementApiUrl = "/api/ItemSuper/GetItemWarehouseMovement";
    this.getItemDsdMovementApiUrl = "/api/ItemSuper/GetItemDsdMovement";
    this.getBarcodeApiUrl = "/api/ItemSuper/BarcodeDetailsGet";
    this.isDisableProductBtns = true;
    this.showPrevNextBtns = false;
    this.itemStockData = [];
    this.itemOrderData = [];
    this.itemSalesData = [];
    this.whsMovmntData = [];
    this.totalUnits = 0;
  }

  ngOnInit(): void {
    this.formControl = new FormControl("", Validators.required);
    this.loaderService.display(true);
    this.componentName = this;

    this.itemTypeData = this.service.serviceDocument.domainData["itemType"];
    this.divisionData = this.service.serviceDocument.domainData["division"];
    this.groupData = this.service.serviceDocument.domainData["group"];
    this.departmentData = Object.assign([], this.service.serviceDocument.domainData["department"]);
    this.classData = Object.assign([], this.service.serviceDocument.domainData["class"]);
    this.itemYesNoData = Object.assign([], this.service.serviceDocument.domainData["itemYesNo"]);
    this.workflowStatusList = Object.assign([], this.service.serviceDocument.domainData["workflowStatusList"]);
    this.statusIndicator = Object.assign([], this.service.serviceDocument.domainData["itemStatus"]);
    this.uomData = Object.assign([], this.service.serviceDocument.domainData["uomList"]);
    this.iTierData = Object.assign([], this.service.serviceDocument.domainData["itemtier"]);
    this.countryData = Object.assign([], this.service.serviceDocument.domainData["country"]);

    //this.vatcodeData = Object.assign([], this.service.serviceDocument.domainData["vatcode"]);
    //this.basicData = Object.assign([], this.service.serviceDocument.domainData["basic"]);
    //this.lastRcvdStatData = Object.assign([], this.service.serviceDocument.domainData["lastRcvdStat"]);
    //this.productTypeData = Object.assign([], this.service.serviceDocument.domainData["productType"]);
    //this.tradingData = Object.assign([], this.service.serviceDocument.domainData["trading"]);
    //this.retailTypeData = Object.assign([], this.service.serviceDocument.domainData["retailType"]);
    this.apiUrlSupplier = "/api/Supplier/SuppliersGetName?name=";
    this.apiUrlBrand = "/api/ItemSuper/BrandsGetName?name=";

    this.newModel();
    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
    this.loaderService.display(false);
    this.setItemStockGridColumnsWithOptions();
    this.setItemOrderGridColumnsWithOptions();
    this.setItemSalesGridColumnsWithOptions();
    this.setItemWarehouseMvmntGridColumnsWithOptions();
  }

  newModel(): void {
    this.model = {
      allowDsd: null, brand: null, country: null, crossRef: null, dsdExpiry: null, expiryDays: null, freeTxt: null
      , item: null, itemSize: null, itemStatus: null, itemType: null, kgConversion: null, labelType: null, lastRecdStat: null
      , localItem: null, lotStatusChnge: null, mfngNo: null, minStock: null, originCountry: null, otherSuppliers: null
      , packDesc: null, recordQry: null, retailType: null, scaleNo: null, scaleRetail: null, shipper: null, skRng: null
      , sku: null, supplierProd: null, trading: null, vatOnRet: null, vatCode: null
      , wholesale: null, consumable: null, forecasted: null, securityTag: null, localPluChgd: null, shrinkWrap: null, liquidate: null
      , avCost: null, retailSp: null, marginPct: null, onOrder: null, pickMultiple: null, purch: null, shellPct: null
      , stockValue: null, storeOh: null, storeSv: null, barcode: null, orderableInd: null, sellableInd: null, inventoryInd: null
      , itemTypeDesc: null, supplier: null, vpn: null, divisionId: null, deptId: null
      , groupId: null, classId: null, subClassId: null, classDesc: null, groupDesc: null, descLong: null
      , divisionDesc: null, deptDesc: null, operation: null, shortDescArabic: null
      , sellableUom: null, status: null, subClassDesc: null, itemGrandparent: null, itemTier: null
      , itemBarcode: null, itemStatusDesc: null, productType: null, expiryFlag: null, expiryInbound: null, expiryOutbound: null
      , supplierDesc: null, brandDesc: null, itemTierDesc: null, sellableUomDesc: null, packInd: null
    };
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  search(): void {
    this.loaderService.display(true);
    this.searchServiceSubscription = this.service.search(this.additionalSearchParams).subscribe((response: any) => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.serviceDocument = this.service.serviceDocument;
        this.showSearchCriteria = false;
        if (this.serviceDocument.dataProfile.dataList != null &&
          this.serviceDocument.dataProfile.dataList.length > 0) {
          this.indexOfCurrentItem = 0;
          this.currentRecordPostion = 1;
          this.model = this.serviceDocument.dataProfile.dataList[this.indexOfCurrentItem];
          this.serviceDocument.dataProfile.profileForm = this.commonService.getFormGroup(this.model, 2);
          this.isPackItem = this.model.packInd === "Y";
          this.isDisableProductBtns = false;
          this.showPrevNextBtns = this.serviceDocument.dataProfile.dataList.length > 1 ? true : false;
          this.resetSuperData();
          this.loaderService.display(false);
        } else {
          this.sharedService.errorForm("No records found for given search criteria");
          this.loaderService.display(false);
        }
      } else {
        this.sharedService.showSTDialog(this.service.serviceDocument.result.innerException
          , this.service.serviceDocument.result.stackTrace || response.model.StackTraceString);
        this.loaderService.display(false);
      }
    }, (error: string) => {
      this.sharedService.errorForm(error);
      this.loaderService.display(false);
    });
  }

  reset(): void {
    this.autoViewRef.ngOnInit();
    this.resetSuperData();
    this.newModel();
    this.service.newModel(this.model);
    this.serviceDocument = this.service.serviceDocument;
    this.isDisableProductBtns = true;
    this.showPrevNextBtns = false;
  }

  resetSuperData(): void {
    this.selectedTab = 0;
    this.itemStockData = [];
    this.itemOrderData = [];
    this.itemSalesData = [];
    this.whsMovmntData = [];
  }

  onPrevious(): void {
    this.getAndSetIndexOfEditObjFromToolBarList();
    if (this.indexOfCurrentItem > 0) {
      this.loaderService.display(true);
      this.indexOfCurrentItem--;
      this.currentRecordPostion--;
      this.model = this.serviceDocument.dataProfile.dataList[this.indexOfCurrentItem];
      this.serviceDocument.dataProfile.profileForm = this.commonService.getFormGroup(this.model, 2);
      this.isPackItem = this.model.packInd === "Y";
      this.resetSuperData();
      setTimeout(() => {
        this.loaderService.display(false);
      }, 500);
    } else {
      this.sharedService.errorForm("This is the first record");
    }
  }

  onNext(): void {
    this.getAndSetIndexOfEditObjFromToolBarList();
    if (this.indexOfCurrentItem < this.serviceDocument.dataProfile.dataList.length - 1) {
      this.loaderService.display(true);
      this.indexOfCurrentItem++;
      this.currentRecordPostion++;
      this.model = this.serviceDocument.dataProfile.dataList[this.indexOfCurrentItem];
      this.serviceDocument.dataProfile.profileForm = this.commonService.getFormGroup(this.model, 2);
      this.isPackItem = this.model.packInd === "Y";
      this.resetSuperData();
      setTimeout(() => {
        this.loaderService.display(false);
      }, 500);
    } else {
      let message: string = this.currentRecordPostion === 100 ? "This is the 100th record" : "No more records available";
      this.sharedService.errorForm(message);
    }
  }

  getAndSetIndexOfEditObjFromToolBarList(): number {
    return this.indexOfCurrentItem = this.serviceDocument.dataProfile.dataList.indexOf(this.model);
  }

  loadItemDimensions(): void {
    this.loaderService.display(true);
    this.itemDimensionsGetServiceSubscription = this.service.getItemDimensions(this.model.item, this.model.originCountry)
      .subscribe((response: ItemSupplierCountryDimensionModel) => {
        if (response) {
          this.sharedService.openDilogWithInputData(ItemDimensionsPopupComponent, response, "item Dimensions View");
        } else {
          this.sharedService.errorForm(`Dimensions data not available for this Item: ${this.model.item}`);
        }
        this.loaderService.display(false);
      }, (error: string) => {
        this.loaderService.display(false);
      });
  }

  loadSupplierSuperData(): void {
    this.loaderService.display(true);
    this.supplierSuperGetServiceSubscription = this.service.getItemRelatedData(this.getItemSuppliersApiUrl, this.model.item)
      .subscribe((response: SupplierSuperModel[]) => {
        if (response != null && response.length > 0) {
          this.sharedService.openPopupWithDataList(ItemSupplierPopupComponent, response, "Item Supplier"
            , this.documentsConstants.itemPrintObjectName, this.model.item);
        } else {
          this.sharedService.errorForm(`Supplier data not available for this Item: ${this.model.item}`);
        }
        this.loaderService.display(false);
      }, (error: string) => {
        this.loaderService.display(false);
      });
  }

  loadItemCostPrices(): void {
    this.loaderService.display(true);
    this.itemCostPricesGetServiceSubscription = this.service.getItemRelatedData(this.getItemCostPricesApiUrl, this.model.item, this.model.supplier)
      .subscribe((response: AjaxModel<CostChangeItemGroupsModel[]>) => {
        if (response.result === AjaxResult.success) {
          this.sharedService.openPopupWithDataList(ItemCostPricesPopupComponent, response.model, "Item Cost & Prices"
            , this.documentsConstants.itemPrintObjectName, this.model.item);
        } else {
          this.sharedService.errorForm(`Cost & Prices data not available for this Item: ${this.model.item}`);
        }
        this.loaderService.display(false);
      }, (error: string) => {
        this.loaderService.display(false);
      });
  }

  loadItemFuturePrices(): void {
    this.loaderService.display(true);
    this.supplierSuperGetServiceSubscription = this.service.getItemRelatedData(this.getFuturePricesApiUrl, this.model.item)
      .subscribe((response: FuturePriceModel[]) => {
        if (response != null && response.length > 0) {
          this.sharedService.openPopupWithDataList(ItemFuturePricePopupComponent, response, "Future Prices"
            , this.documentsConstants.itemPrintObjectName, this.model.item);
        } else {
          this.sharedService.errorForm(`Future Price data not available for this Item: ${this.model.item}`);
        }
        this.loaderService.display(false);
      }, (error: string) => {
        this.loaderService.display(false);
      });
  }

  loadItemFutureCost(): void {
    this.loaderService.display(true);
    this.supplierSuperGetServiceSubscription = this.service.getItemRelatedData(this.getFutureCostsApiUrl, this.model.item)
      .subscribe((response: FuturePriceModel[]) => {
        if (response != null && response.length > 0) {
          this.sharedService.openPopupWithDataList(ItemFutureCostPopupComponent, response, "Future Cost"
            , this.documentsConstants.itemPrintObjectName, this.model.item);
        } else {
          this.sharedService.errorForm(`Future Cost data not available for this Item: ${this.model.item}`);
        }
        this.loaderService.display(false);
      }, (error: string) => {
        this.loaderService.display(false);
      });
  }

  loadItemBarcode(): void {
    this.loaderService.display(true);
    this.itembarcodesGetServiceSubscription = this.service.getItemRelatedData(this.getBarcodeApiUrl, this.model.item)
      .subscribe((response: AjaxModel<BarcodeModel[]>) => {
        if (response.result === AjaxResult.success) {
          if (response.model != null && response.model.length > 0) {
            this.sharedService.openPopupWithDataList(ItemBarcodePopupComponent, response.model, "Future Cost"
              , this.documentsConstants.itemPrintObjectName, this.model.item);
          } else {
            this.sharedService.errorForm(`Barcode data not available for this Item: ${this.model.item}`);
          }

        } else {
          this.sharedService.errorForm(response.message);
        }
        this.loaderService.display(false);
      }, (error: string) => {
        this.sharedService.errorForm(error);
        this.loaderService.display(false);
      });
  }

  loadItemPromotion(): void {
    this.loaderService.display(true);
    this.supplierSuperGetServiceSubscription = this.service.getItemRelatedData(this.getPromotionApiUrl, this.model.item)
      .subscribe((response: FuturePromotionModel[]) => {
        if (response != null && response.length > 0) {
          this.sharedService.openPopupWithDataList(ItemPromotionPopupComponent, response, "Promotion"
            , this.documentsConstants.itemPrintObjectName, this.model.item);
        } else {
          this.sharedService.errorForm(`Promotion data not available for this Item: ${this.model.item}`);
        }
        this.loaderService.display(false);
      }, (error: string) => {
        this.loaderService.display(false);
      });
  }

  public tabOnClick(eventData: number): void {
    let selectedTabIndex: number = (eventData !== null) ? eventData : 0;
    if (selectedTabIndex === 1) {
      this.loadItemStock();
    } else if (selectedTabIndex === 2) {
      this.loadItemSales();
    } else if (selectedTabIndex === 3) {
      this.loadItemWarehouseMovement();
    } else if (selectedTabIndex === 5) {
      this.loadItemOrders();
    }
  }

  loadItemStock(): void {
    this.loaderService.display(true);
    this.itemStockGetServiceSubscription = this.service.getItemRelatedData(this.getItemStockApiUrl, this.model.item)
      .subscribe((response: ItemStockModel[]) => {
        if (response != null && response.length > 0) {
          this.itemStockData = response;
        } else {
          this.sharedService.errorForm(`Stock data not available for this Item: ${this.model.item}`);
        }
        this.loaderService.display(false);
      }, (error: string) => {
        this.loaderService.display(false);
      });
  }

  setItemStockGridColumnsWithOptions(): void {
    this.stockColumns = [
      {
        headerName: "Location", field: "locationDescription", tooltipField: "locationDescription", width: 100, headerTooltip: "Location"
      },
      {
        headerName: "Av Cost", field: "avCost", tooltipField: "avCost", width: 120, headerTooltip: "Av Cost"
      },
      {
        headerName: "Unit Cost", field: "unitCost", tooltipField: "unitCost", width: 120, headerTooltip: "Unit Cost"
        , cellStyle: { 'text-align': "right" }
      },
      {
        headerName: "Stock On Hand", field: "stockOnHand", tooltipField: "stockOnHand"
        , width: 140, headerTooltip: "Stock On Hand"
      },
      {
        headerName: "SOH Update Date", field: "sohUpdateDatetime", tooltipField: "sohUpdateDatetime"
        , cellRendererFramework: GridDateComponent, width: 140, headerTooltip: "SOH Update Date"
      },
      {
        headerName: "In Transit Qty", field: "inTransitQty", tooltipField: "inTransitQty"
        , width: 140, headerTooltip: "In Transit Qty"
      },
      {
        headerName: "Pack Comp Intran", field: "packCompIntran", tooltipField: "packCompIntran"
        , width: 100, headerTooltip: "Pack Comp Intran"
      },
      {
        headerName: "Pack Comp SOH", field: "packCompSoh", tooltipField: "packCompSoh"
        , width: 140, headerTooltip: "Pack Comp SOH"
      },
      {
        headerName: "Tsf Reserved Qty", field: "tsfReservedQty", tooltipField: "tsfReservedQty"
        , width: 140, headerTooltip: "Tsf Reserved Qty"
      },
      {
        headerName: "Pack Comp Resv", field: "packCompResv", tooltipField: "packCompResv"
        , width: 120, headerTooltip: "Pack Comp Resv"
      },
      {
        headerName: "Tsf Expected Qty", field: "tsfExpectedQty", tooltipField: "tsfExpectedQty"
        , width: 140, headerTooltip: "Tsf Expected Qty"
      },
      {
        headerName: "Pack Comp Exp", field: "packCompExp", tooltipField: "packCompExp"
        , width: 140, headerTooltip: "Pack Comp Exp"
      },
      {
        headerName: "Non Sellable Qty", field: "nonSellableQty", tooltipField: "nonSellableQty"
        , width: 140, headerTooltip: "Non Sellable Qty"
      },
      {
        headerName: "Created Date", field: "createDatetime", tooltipField: "createDatetime"
        , cellRendererFramework: GridDateComponent, width: 140, headerTooltip: "Created Date"
      },
      {
        headerName: "Last Updated Date", field: "lastUpdateDatetime", tooltipField: "lastUpdateDatetime"
        , cellRendererFramework: GridDateComponent, width: 160, headerTooltip: "Last Updated Date"
      },
      {
        headerName: "Last Update Id", field: "lastUpdateId", tooltipField: "lastUpdateId"
        , width: 140, headerTooltip: "Last Update Id"
      },
      {
        headerName: "First Received", field: "firstReceived", tooltipField: "firstReceived"
        , cellRendererFramework: GridDateComponent, width: 140, headerTooltip: "First Received"
      },
      {
        headerName: "Last Received", field: "lastReceived", tooltipField: "lastReceived"
        , cellRendererFramework: GridDateComponent, width: 140, headerTooltip: "Last Received"
      },
      {
        headerName: "Qty Received", field: "qtyReceived", tooltipField: "qtyReceived"
        , width: 120, headerTooltip: "Qty Received"
      },
      {
        headerName: "First Sold", field: "firstSold", tooltipField: "firstSold"
        , cellRendererFramework: GridDateComponent, width: 120, headerTooltip: "First Sold"
      },
      {
        headerName: "Last Sold", field: "lastSold", tooltipField: "lastSold"
        , cellRendererFramework: GridDateComponent, width: 120, headerTooltip: "Last Sold"
      },
      {
        headerName: "Qty Sold", field: "qtySold", tooltipField: "qtySold", width: 120, headerTooltip: "Qty Sold"
      },
      {
        headerName: "Primary Supp", field: "primarySupp", tooltipField: "primarySupp"
        , width: 140, headerTooltip: "Primary Supp"
      },
      {
        headerName: "Primary Supp Name", field: "primarySuppName", tooltipField: "primarySuppName"
        , width: 140, headerTooltip: "Primary Supp Name"
      },
      {
        headerName: "Primary Cntry", field: "primaryCntry", tooltipField: "primaryCntry"
        , width: 140, headerTooltip: "Primary Cntry"
      },
      {
        headerName: "Pack Comp NonSellable", field: "packCompNonSellable", tooltipField: "packCompNonSellable"
        , width: 140, headerTooltip: "Pack Comp NonSellable"
      }
    ];

    this.stockGridOptions = {
      onGridReady: (params: GridReadyEvent): void => {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;

        if (this.gridColumnApi) {
          this.gridColumnApi.setColumnsVisible(["packCompIntran", "packCompSoh", "packCompResv"
            , "packCompExp", "packCompNonSellable"], this.isPackItem);
        }
      },
      context: {
        componentParent: this
      },
      enableColResize: true
    };
  }

  loadItemOrders(): void {
    this.loaderService.display(true);
    this.itemOrderGetServiceSubscription = this.service.getItemRelatedData(this.getItemOrdersApiUrl, this.model.item)
      .subscribe((response: ItemOrderModel[]) => {
        if (response != null && response.length > 0) {
          this.itemOrderData = response;
        } else {
          this.sharedService.errorForm(`Orders data not available for this Item: ${this.model.item}`);
        }
        this.loaderService.display(false);
      }, (error: string) => {
        this.loaderService.display(false);
      });
  }

  setItemOrderGridColumnsWithOptions(): void {
    this.orderColumns = [
      {
        headerName: "Order#", field: "orderNo", tooltipField: "orderNo", width: 100, headerTooltip: "Order#"
      },
      {
        headerName: "Ordered Date", field: "orderedDate", tooltipField: "orderedDate"
        , cellRendererFramework: GridDateComponent, width: 140, headerTooltip: "Ordered Date"
      },
      {
        headerName: "Received Date", field: "receivedDate", tooltipField: "receivedDate"
        , cellRendererFramework: GridDateComponent, width: 140, headerTooltip: "Received Date"
      },
      {
        headerName: "Supplier", field: "supplierDescription", tooltipField: "supplierDescription"
        , width: 160, headerTooltip: "Supplier"
      },
      {
        headerName: "Status", field: "status", tooltipField: "status", width: 100, headerTooltip: "Status"
      },
      {
        headerName: "Line#", field: "lineNo", tooltipField: "lineNo", width: 100, headerTooltip: "Line#"
      },
      {
        headerName: "Fob", field: "fob", tooltipField: "fob", width: 100, headerTooltip: "Fob"
      },
      {
        headerName: "RcvCst", field: "receivedCost", tooltipField: "receivedCost", width: 100, headerTooltip: "RcvCst"
      },
      {
        headerName: "Qty", field: "receivedQty", tooltipField: "receivedQty", width: 100, headerTooltip: "Qty"
      },
      {
        headerName: "Dsc%", field: "discountPct", tooltipField: "discountPct", width: 100, headerTooltip: "Dsc%"
      },
      {
        headerName: "Rcvd", field: "received", tooltipField: "received", width: 100, headerTooltip: "Rcvd"
      },
      {
        headerName: "Location", field: "locationDescription", tooltipField: "locationDescription"
        , width: 120, headerTooltip: "Location"
      },
      {
        headerName: "Parent", field: "parentOrder", tooltipField: "parentOrder", width: 120, headerTooltip: "Parent"
      }
    ];

    this.orderGridOptions = {
      onGridReady: (params: any) => {
        this.gridApi = params.api;
      },
      context: {
        componentParent: this
      },
      enableColResize: true
    };
  }

  loadItemBasicDetails(): void {
    this.loaderService.display(true);
    this.itemBasicGetServiceSubscription = this.service.getItemBasicDetails(this.getItemBasicApiUrl, this.model.item.toString())
      .subscribe((response: ItemBasicNewModel) => {
        if (response) {
          this.sharedService.openDilogWithInputData(ItemBasicPopupComponent, response, "item Basic View");
        } else {
          this.sharedService.errorForm(`Basic data not available for this Item: ${this.model.item}`);
        }
        this.loaderService.display(false);
      }, (error: string) => {
        this.loaderService.display(false);
      });
  }

  loadItemSales(): void {
    this.loaderService.display(true);
    this.itemOrderGetServiceSubscription = this.service.getItemRelatedData(this.getItemSalesApiUrl, this.model.item)
      .subscribe((response: LocWiseWeeksDataModel[]) => {
        if (response != null && response.length > 0) {
          this.itemSalesData = [];
          this.totalUnits = 0;
          response.forEach(x => {
            this.itemSalesData.push(this.fillSalesModel(x));
          });
        } else {
          this.sharedService.errorForm(`Sales data not available for this Item: ${this.model.item}`);
        }
        this.loaderService.display(false);
      }, (error: string) => {
        this.loaderService.display(false);
      });
  }

  fillSalesModel(obj: LocWiseWeeksDataModel): ItemSalesModel {
    let itemSalesModel: ItemSalesModel = this.newSalesModel();
    itemSalesModel["locationId"] = obj.locationId;
    itemSalesModel["locName"] = obj.locName;

    for (let i: number = 0; i < obj.listOfWeeksData.length; i++) {
      itemSalesModel[`week${obj.listOfWeeksData[i].weekId}Val`] = obj.listOfWeeksData[i].weekVal;
      this.totalUnits += obj.listOfWeeksData[i].weekVal;
    }
    return itemSalesModel;
  }

  newSalesModel(): ItemSalesModel {
    let itemSalesModel: ItemSalesModel = {
      locationId: null, locName: null
      , week1Val: 0, week2Val: 0, week3Val: 0, week4Val: 0, week5Val: 0, week6Val: 0, week7Val: 0
      , week8Val: 0, week9Val: 0, week10Val: 0
      , week11Val: 0, week12Val: 0, week13Val: 0, week14Val: 0, week15Val: 0, week16Val: 0, week17Val: 0
      , week18Val: 0, week19Val: 0, week20Val: 0
      , week21Val: 0, week22Val: 0, week23Val: 0, week24Val: 0, week25Val: 0, week26Val: 0, week27Val: 0
      , week28Val: 0, week29Val: 0, week30Val: 0
      , week31Val: 0, week32Val: 0, week33Val: 0, week34Val: 0, week35Val: 0, week36Val: 0, week37Val: 0
      , week38Val: 0, week39Val: 0, week40Val: 0
      , week41Val: 0, week42Val: 0, week43Val: 0, week44Val: 0, week45Val: 0, week46Val: 0, week47Val: 0
      , week48Val: 0, week49Val: 0, week50Val: 0, week51Val: 0, week52Val: 0
    };
    return itemSalesModel;
  }

  setItemSalesGridColumnsWithOptions(): void {
    this.salesColumns = [
      {
        headerName: "Location", field: "locName", tooltipField: "locName", width: 100
      },
      {
        headerName: "Week 52", field: "week52Val", tooltipField: "week52Val", width: 100
      },
      {
        headerName: "Week 51", field: "week51Val", tooltipField: "week51Val", width: 100
      },
      {
        headerName: "Week 50", field: "week50Val", tooltipField: "week50Val", width: 100
      },
      {
        headerName: "Week 49", field: "week49Val", tooltipField: "week49Val", width: 100
      },
      {
        headerName: "Week 48", field: "week48Val", tooltipField: "week48Val", width: 100
      },
      {
        headerName: "Week 47", field: "week47Val", tooltipField: "week47Val", width: 100
      },
      {
        headerName: "Week 46", field: "week46Val", tooltipField: "week46Val", width: 100
      },
      {
        headerName: "Week 45", field: "week45Val", tooltipField: "week45Val", width: 100
      },
      {
        headerName: "Week 44", field: "week44Val", tooltipField: "week44Val", width: 100
      },
      {
        headerName: "Week 43", field: "week43Val", tooltipField: "week43Val", width: 100
      },
      {
        headerName: "Week 42", field: "week42Val", tooltipField: "week42Val", width: 100
      },
      {
        headerName: "Week 41", field: "week41Val", tooltipField: "week41Val", width: 100
      },
      {
        headerName: "Week 40", field: "week40Val", tooltipField: "week40Val", width: 100
      },
      {
        headerName: "Week 39", field: "week39Val", tooltipField: "week39Val", width: 100
      },
      {
        headerName: "Week 38", field: "week38Val", tooltipField: "week38Val", width: 100
      },
      {
        headerName: "Week 37", field: "week37Val", tooltipField: "week37Val", width: 100
      },
      {
        headerName: "Week 36", field: "week36Val", tooltipField: "week36Val", width: 100
      },
      {
        headerName: "Week 35", field: "week35Val", tooltipField: "week35Val", width: 100
      },
      {
        headerName: "Week 34", field: "week34Val", tooltipField: "week34Val", width: 100
      },
      {
        headerName: "Week 33", field: "week33Val", tooltipField: "week33Val", width: 100
      },
      {
        headerName: "Week 32", field: "week32Val", tooltipField: "week32Val", width: 100
      },
      {
        headerName: "Week 31", field: "week31Val", tooltipField: "week31Val", width: 100
      },
      {
        headerName: "Week 30", field: "week30Val", tooltipField: "week30Val", width: 100
      },
      {
        headerName: "Week 29", field: "week29Val", tooltipField: "week29Val", width: 100
      },
      {
        headerName: "Week 28", field: "week28Val", tooltipField: "week28Val", width: 100
      },
      {
        headerName: "Week 27", field: "week27Val", tooltipField: "week27Val", width: 100
      },
      {
        headerName: "Week 26", field: "week26Val", tooltipField: "week26Val", width: 100
      },
      {
        headerName: "Week 25", field: "week25Val", tooltipField: "week25Val", width: 100
      },
      {
        headerName: "Week 24", field: "week24Val", tooltipField: "week24Val", width: 100
      },
      {
        headerName: "Week 23", field: "week23Val", tooltipField: "week23Val", width: 100
      },
      {
        headerName: "Week 22", field: "week22Val", tooltipField: "week22Val", width: 100
      },
      {
        headerName: "Week 21", field: "week21Val", tooltipField: "week21Val", width: 100
      },
      {
        headerName: "Week 20", field: "week20Val", tooltipField: "week20Val", width: 100
      },
      {
        headerName: "Week 19", field: "week19Val", tooltipField: "week19Val", width: 100
      },
      {
        headerName: "Week 18", field: "week18Val", tooltipField: "week18Val", width: 100
      },
      {
        headerName: "Week 17", field: "week17Val", tooltipField: "week17Val", width: 100
      },
      {
        headerName: "Week 16", field: "week16Val", tooltipField: "week16Val", width: 100
      },
      {
        headerName: "Week 15", field: "week15Val", tooltipField: "week15Val", width: 100
      },
      {
        headerName: "Week 14", field: "week14Val", tooltipField: "week14Val", width: 100
      },
      {
        headerName: "Week 13", field: "week13Val", tooltipField: "week13Val", width: 100
      },
      {
        headerName: "Week 12", field: "week12Val", tooltipField: "week12Val", width: 100
      },
      {
        headerName: "Week 11", field: "week11Val", tooltipField: "week11Val", width: 100
      },
      {
        headerName: "Week 10", field: "week10Val", tooltipField: "week10Val", width: 100
      },
      {
        headerName: "Week 9", field: "week9Val", tooltipField: "week9Val", width: 100
      },
      {
        headerName: "Week 8", field: "week8Val", tooltipField: "week8Val", width: 100
      },
      {
        headerName: "Week 7", field: "week7Val", tooltipField: "week7Val", width: 100
      },
      {
        headerName: "Week 6", field: "week6Val", tooltipField: "week6Val", width: 100
      },
      {
        headerName: "Week 5", field: "week5Val", tooltipField: "week5Val", width: 100
      },
      {
        headerName: "Week 4", field: "week4Val", tooltipField: "week4Val", width: 100
      },
      {
        headerName: "Week 3", field: "week3Val", tooltipField: "week3Val", width: 100
      },
      {
        headerName: "Week 2", field: "week2Val", tooltipField: "week2Val", width: 100
      },
      {
        headerName: "Week 1", field: "week1Val", tooltipField: "week1Val", width: 100
      }
    ];

    this.salesGridOptions = {
      onGridReady: (params: any) => {
        this.gridApi = params.api;
      },
      context: {
        componentParent: this
      },
      enableColResize: true
    };
  }

  loadItemWarehouseMovement(): void {
    this.loaderService.display(true);
    this.itemWarehouseMvmntGetServiceSubscription = this.service.getItemRelatedData(this.getItemWarehouseMovementApiUrl, this.model.item)
      .subscribe((response: WhsMovmntModel[]) => {
        if (response != null && response.length > 0) {
          this.whsMovmntData = response;
        } else {
          this.sharedService.errorForm(`Warehouse Movement data not available for this Item: ${this.model.item}`);
        }
        this.loaderService.display(false);
      }, (error: string) => {
        this.loaderService.display(false);
      });
  }

  setItemWarehouseMvmntGridColumnsWithOptions(): void {
    this.warehouseMovementColumns = [
      {
        headerName: "From Loc", field: "fromLoc", tooltipField: "fromLoc", width: 100, headerTooltip: "From Loc"
      },
      {
        headerName: "To Loc", field: "toLoc", tooltipField: "toLoc", width: 100, headerTooltip: "To Loc"
      },
      {
        headerName: "Create Date", field: "createDate", tooltipField: "createDate"
        , cellRendererFramework: GridDateComponent, width: 120, headerTooltip: "Create Date"
      },
      {
        headerName: "Apprvl Date", field: "approvalDate", tooltipField: "approvalDate"
        , cellRendererFramework: GridDateComponent, width: 120, headerTooltip: "Apprvl Date"
      },

      {
        headerName: "Tsf Price", field: "tsfPrice", tooltipField: "tsfPrice", width: 100, headerTooltip: "Tsf Price"
      },
      {
        headerName: "Tsf Qty", field: "tsfQty", tooltipField: "tsfQty", width: 100, headerTooltip: "Tsf Qty"
      },

      {
        headerName: "Whs Pack Size", field: "whsPackSize", tooltipField: "whsPackSize"
        , width: 140, headerTooltip: "Whs Pack Size"
      },
      {
        headerName: "Supp Pack Size", field: "suppPackSize", tooltipField: "suppPackSize"
        , width: 140, headerTooltip: "Supp Pack Size"
      },
      {
        headerName: "Whs Stock", field: "whsStock", tooltipField: "whsStock", width: 120, headerTooltip: "Whs Stock"
      },

      {
        headerName: "Comp Pack Size", field: "compPackSize", tooltipField: "compPackSize"
        , width: 140, headerTooltip: "Comp Pack Size"
      }
    ];

    this.warehouseMovementGridOptions = {
      onGridReady: (params: any) => {
        this.gridApi = params.api;
      },
      context: {
        componentParent: this
      },
      enableColResize: true
    };
  }

  loadItemRangingDetails(): void {
    this.loaderService.display(true);
    this.itemRangingGetServiceSubscription = this.service.getItemRangingDetails(this.model.item)
      .subscribe((response: ItemRangingSuperModel) => {
        if (response && response.dataList && response.dataList.length > 0) {
          this.sharedService.domainData = { "sourceMethods": response.domainData["sourceMethods"] };
          this.sharedService.openPopupWithDataList(ItemRangingPopupComponent, response.dataList, "Item Ranging"
            , this.documentsConstants.itemPrintObjectName, this.model.item);
        } else {
          this.sharedService.errorForm(`Ranging details not available for this Item: ${this.model.item}`);
        }
        this.loaderService.display(false);
      }, (error: string) => {
        this.loaderService.display(false);
      });
  }

  changeDivision(item: any): void {
    let obj: any = this;
    this.itemSharedService.changeDivision(obj, item);
  }

  changeDepartment(item: any): void {
    let obj: any = this;
    this.itemSharedService.changeDepartment(obj, item);
  }

  changeGroup(item: any): void {
    let obj: any = this;
    this.itemSharedService.changeGroup(obj, item);
  }

  changeClass(item: any): void {
    let obj: any = this;
    this.itemSharedService.changeClass(obj, item);
  }

  ngOnDestroy(): void {
    if (this.searchServiceSubscription) {
      this.searchServiceSubscription.unsubscribe();
    }
    if (this.searchBarcodeServiceSubscription) {
      this.searchBarcodeServiceSubscription.unsubscribe();
    }
    if (this.subClassGetbyNameServiceSubscription) {
      this.subClassGetbyNameServiceSubscription.unsubscribe();
    }
    if (this.itemDimensionsGetServiceSubscription) {
      this.itemDimensionsGetServiceSubscription.unsubscribe();
    }
    if (this.supplierSuperGetServiceSubscription) {
      this.supplierSuperGetServiceSubscription.unsubscribe();
    }
    if (this.itemCostPricesGetServiceSubscription) {
      this.itemCostPricesGetServiceSubscription.unsubscribe();
    }
    if (this.itemStockGetServiceSubscription) {
      this.itemStockGetServiceSubscription.unsubscribe();
    }
    if (this.itemOrderGetServiceSubscription) {
      this.itemOrderGetServiceSubscription.unsubscribe();
    }
    if (this.itemBasicGetServiceSubscription) {
      this.itemBasicGetServiceSubscription.unsubscribe();
    }
    if (this.itemWarehouseMvmntGetServiceSubscription) {
      this.itemWarehouseMvmntGetServiceSubscription.unsubscribe();
    }
    if (this.itemDsdMvmntGetServiceSubscription) {
      this.itemDsdMvmntGetServiceSubscription.unsubscribe();
    }
    if (this.itemRangingGetServiceSubscription) {
      this.itemRangingGetServiceSubscription.unsubscribe();
    }

    if (this.itembarcodesGetServiceSubscription) {
      this.itembarcodesGetServiceSubscription.unsubscribe();
    }

    this.itemTypeData.length = 0;
    this.divisionData.length = 0;
    this.departmentData.length = 0;
    this.groupData.length = 0;
    this.classData.length = 0;
    this.itemYesNoData.length = 0;
    this.uomData.length = 0;
  }

  orderableChanged(): void {
    if (this.serviceDocument.dataProfile.profileForm.controls["orderable"].value === "Y") {
      this.serviceDocument.dataProfile.profileForm.controls["inventory"].setValue("Y");
      this.serviceDocument.dataProfile.profileForm.controls["inventory"].disable();
    } else {
      this.serviceDocument.dataProfile.profileForm.controls["inventory"].enable();
    }
  }
}
