﻿export class ItemSalesModel {
    locationId: number;
    locName: string;

    week1Val?: number;
    week2Val?: number;
    week3Val?: number;
    week4Val?: number;
    week5Val?: number;
    week6Val?: number;
    week7Val?: number;
    week8Val?: number;
    week9Val?: number;
    week10Val?: number;

    week11Val?: number;
    week12Val?: number;
    week13Val?: number;
    week14Val?: number;
    week15Val?: number;
    week16Val?: number;
    week17Val?: number;
    week18Val?: number;
    week19Val?: number;
    week20Val?: number;

    week21Val?: number;
    week22Val?: number;
    week23Val?: number;
    week24Val?: number;
    week25Val?: number;
    week26Val?: number;
    week27Val?: number;
    week28Val?: number;
    week29Val?: number;
    week30Val?: number;

    week31Val?: number;
    week32Val?: number;
    week33Val?: number;
    week34Val?: number;
    week35Val?: number;
    week36Val?: number;
    week37Val?: number;
    week38Val?: number;
    week39Val?: number;
    week40Val?: number;

    week41Val?: number;
    week42Val?: number;
    week43Val?: number;
    week44Val?: number;
    week45Val?: number;
    week46Val?: number;
    week47Val?: number;
    week48Val?: number;
    week49Val?: number;
    week50Val?: number;
    week51Val?: number;
    week52Val?: number;
}