export class BarcodeModel {
  itemBarcode?: string;
  barcodeType?: string;
  standardUom?: string;
  itemDesc?: string;
  compQty?: number;
}
