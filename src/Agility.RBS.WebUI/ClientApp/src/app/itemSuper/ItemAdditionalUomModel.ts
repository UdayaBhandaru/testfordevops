export class ItemAdditionalUomModel {
  item?: number;
  itemParent?: number;
  standardUom: string;
  standardUomValue: string;
  shortDesc: string;
  onlineDesc: string;
  shortDescArabic: string;
  systemDesc: string;
  regularRetail?: number;
  operation?: string;
  unitCost?: number;
  caseCost?: number;
  innerSize?: number;
  caseSize?: number;
}
