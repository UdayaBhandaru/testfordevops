import { Component, Inject } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { Subscription } from "rxjs";
import { CommonService } from "@agility/frameworkcore";
import { MatDialog, MAT_DIALOG_DATA } from "@angular/material";
import { SharedService } from "../Common/SharedService";
import { GridOptions, GridApi } from "ag-grid-community";
import { BarcodeModel } from './BarcodeModel';

@Component({
  selector: "item-barcode-popup",
  templateUrl: "./ItemBarcodePopupComponent.html",
  styleUrls: ['./ItemBarcodePopupComponent.scss']
})

export class ItemBarcodePopupComponent {
  infoGroup: FormGroup;
  objectValue: number;
  objectName: string;
  sharedSubscription: Subscription;
  componentName: any;
  barcodeColumns: {}[];
  gridOptions: GridOptions;
  gridApi: GridApi;
  barcodeData: BarcodeModel[];

  constructor(private service: SharedService, private commonService: CommonService
    , public dialog: MatDialog
    , @Inject(MAT_DIALOG_DATA)
    public data: {
      dataList: BarcodeModel[], objectName: string, objectValue: number, invokingModule: string
    }) {
  }

  ngOnInit(): void {
    if (this.data !== null) {
      this.objectValue = this.data.objectValue;
      this.objectName = this.data.objectName;
      this.barcodeData = this.service.editObject;
    } else {
      this.barcodeData = [];
    }
    this.componentName = this;
    this.barcodeColumns = [
      { headerName: "Item Description", field: "itemDesc", tooltipField: "itemDesc" },
      { headerName: "UOM", field: "standardUom", tooltipField: "standardUom" },
      { headerName: "Value", field: "compQty", tooltipField: "compQty" },
      { headerName: "Type", field: "barcodeType", tooltipField: "barcodeType" },
      { headerName: "Barcode", field: "itemBarcode", tooltipField: "itemBarcode" }
    ];
  }

  close(): void {
    this.dialog.closeAll();
  }
}
