﻿import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { ItemSuperSearchService } from "./ItemSuperSearchService";
import { ItemSuperModel } from "./ItemSuperModel";

@Injectable()
export class ItemSuperResolver implements Resolve<ServiceDocument<ItemSuperModel>> {
    constructor(private service: ItemSuperSearchService) { }
    resolve(): Observable<ServiceDocument<ItemSuperModel>> {
        return this.service.list();
    }
}