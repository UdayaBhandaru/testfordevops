﻿export class ItemOrderModel {
    orderNo?: number;
    divisionId?: number;
    divisionDesc: string;
    orderedDate?: Date;
    receivedDate?: Date;
    supplierId?: number;
    suppName: string;
    status: string;
    lineNo?: number;
    fob?: number;
    receivedCost?: number;
    receivedQty?: number;
    discountPct?: number;
    received?: number;
    locationId?: number;
    locName: string;
    parentOrder?: number;
    supplierDescription: string;
    divisionDescription: string;
    locationDescription: string;
}