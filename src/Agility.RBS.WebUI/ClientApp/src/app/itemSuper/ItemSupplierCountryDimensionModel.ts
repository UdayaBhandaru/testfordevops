export class ItemSupplierCountryDimensionModel {
  item: number;
  companyId: number;
  companyName?: string;
  countryCode: string;
  countryName?: string;
  importCountry: string;
  supplier: number;
  suppName?: string;
  dimObject: string;
  presentationMethod: string;
  length?: number;
  width?: number;
  height?: number;
  lwhUom: string;
  weight?: number;
  netWeight?: number;
  weightUom: string;
  liquidVolume?: number;
  liquidVolumeUom: string;
  statCube?: number;
  tareWeight?: number;
  tareType: string;
  status?: string;
  operation?: string;

  // below are used in ItemSuper Dimensions Popup
  dimObjectDesc: string;
  presentationMethodDesc: string;
  tareTypeDesc: string;
  domainData?= {};

  itemSupplierCountryDimensions?: ItemSupplierCountryDimensionModel[];
  createdBy?: string;
}
