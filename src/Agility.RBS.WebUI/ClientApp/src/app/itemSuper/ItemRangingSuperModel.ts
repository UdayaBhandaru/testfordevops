﻿import { ItemRangingModel } from "./ItemRangingModel";

export class ItemRangingSuperModel {
    domainData: {};
    dataList: ItemRangingModel[];
    item?: number;
}