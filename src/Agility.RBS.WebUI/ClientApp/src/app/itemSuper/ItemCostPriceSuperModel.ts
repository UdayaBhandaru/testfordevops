﻿export class ItemCostPriceSuperModel {
    item?: number;
    componentQuantity: string;
    caseCost?: number;
    unitCost?: number;
    annualDiscount?: number;
    netUnitCost?: number;

    currentPriceSSM?: number;
    currentPriceSS?: number;
    currentPriceWHS?: number;
    currentPriceCS?: number;

    marginSSM?: number;
    marginSS?: number;
    marginWHS?: number;
    marginCS?: number;

    compRetail1?: number;
    compRetail2?: number;
    compRetail3?: number;
    compRetail4?: number;
}