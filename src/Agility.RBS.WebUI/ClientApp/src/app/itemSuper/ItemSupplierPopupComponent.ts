import { Component, Inject } from "@angular/core";
import { DatePipe } from "@angular/common";
import { FormGroup } from "@angular/forms";
import { Subscription } from "rxjs";
import { CommonService, FormMode } from "@agility/frameworkcore";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { SharedService } from "../Common/SharedService";
import { GridOptions, GridApi } from "ag-grid-community";
import { SupplierSuperModel } from "../Supplier/SupplierMaster/SupplierSuperModel";

@Component({
    selector: "ItemSupplierPopup",
  templateUrl: "./ItemSupplierPopupComponent.html",
  styleUrls: ['./ItemSupplierPopupComponent.scss']
})

export class ItemSupplierPopupComponent {
    infoGroup: FormGroup;
    objectValue: number;
    objectName: string;
    sharedSubscription: Subscription;
    componentName: any;
    supplierColumns: {}[];
    supplierGridOptions: GridOptions;
    gridApi: GridApi;
    supplierSuperData: SupplierSuperModel[];

    constructor(private service: SharedService, private commonService: CommonService
        , public dialog: MatDialog
        , @Inject(MAT_DIALOG_DATA)
        public data: {
            dataList: SupplierSuperModel[]
            , objectName: string, objectValue: number, invokingModule: string
        }
        , private datePipe: DatePipe) {
    }

    ngOnInit(): void {
        if (this.data !== null) {
            this.objectValue = this.data.objectValue;
            this.objectName = this.data.objectName;
            this.supplierSuperData = this.data.dataList;
        } else {
            this.supplierSuperData = [];
        }
        this.componentName = this;
        this.setSupplierGridColumnsWithOptions();
    }

    setSupplierGridColumnsWithOptions(): void {
        this.supplierColumns = [
            {
                headerName: "Supplier", field: "supplierDescription", tooltipField: "supplierDescription", maxwidth: 150
            },
            {
                headerName: "Address", field: "fullAddress", tooltipField: "fullAddress", width: 350
            },
            {
                headerName: "Rebate", field: "fixedRebatePercent", tooltipField: "fixedRebatePercent", width: 100
            },
            {
                headerName: "Primary", field: "primarySupplier", tooltipField: "primarySupplier", width: 100
            },
            {
                headerName: "Suppl type", field: "supplierTypeName", tooltipField: "supplierTypeName", width: 120
            },
            {
                headerName: "Auto Repl", field: "autoReplenishment", tooltipField: "autoReplenishment", width: 120
            },
            {
                headerName: "Order multiple", field: "orderMultiple", tooltipField: "orderMultiple"
            }
        ];

        this.supplierGridOptions = {
            onGridReady: (params: any) => {
              this.gridApi = params.api;
              this.gridApi.sizeColumnsToFit();
            },
            context: {
                componentParent: this
            },
            enableColResize: true
        };
    }

    close(): void {
        this.dialog.closeAll();
    }
}
