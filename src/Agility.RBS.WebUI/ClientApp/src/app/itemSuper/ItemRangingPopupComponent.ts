import { Component, Inject } from "@angular/core";
import { DatePipe } from "@angular/common";
import { FormGroup, FormControl } from "@angular/forms";
import { Subscription } from "rxjs";
import { CommonService, FormMode } from "@agility/frameworkcore";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { SharedService } from "../Common/SharedService";
import { GridOptions, GridApi, RowNode } from "ag-grid-community";
import { DomainDetailModel } from "../domain/DomainDetailModel";
import { ItemSuperSearchService } from "./ItemSuperSearchService";
import { LoaderService } from "../Common/LoaderService";
import { ItemRangingModel } from "./ItemRangingModel";
import { GridSelectComponent } from "../Common/grid/GridSelectComponent";
import { ItemRangingSuperModel } from "./ItemRangingSuperModel";

@Component({
  selector: "ItemRangingPopup",
  templateUrl: "./ItemRangingPopupComponent.html",
  styleUrls: ['./ItemRangingPopupComponent.scss'],
  providers: [
    ItemSuperSearchService
  ]
})

export class ItemRangingPopupComponent {
  infoGroup: FormGroup;
  objectValue: number;
  objectName: string;
  sharedSubscription: Subscription;
  componentName: any;
  rangingColumns: {}[]; columnChilds: any[];
  rangingGridOptions: GridOptions; public gridOptionChilds: GridOptions;
  gridApi: GridApi; gridApiChild: GridApi;
  rangingDetailsData: ItemRangingModel[];
  addText: string = "+ Add To Grid";
  companyName: string; countryName: string; regionName: string; formatName: string; locationName: string;
  item: number;
  node: any;
  deletedRows: any[] = [];
  itemRangingApiUrl: string;
  sourceMethodData: DomainDetailModel[];
  isEditable: boolean;

  constructor(private sharedService: SharedService, private commonService: CommonService
    , public dialog: MatDialog
    , @Inject(MAT_DIALOG_DATA)
    public data: {
      dataList: ItemRangingModel[]
      , objectName: string, objectValue: number, invokingModule: string
    }
    , private datePipe: DatePipe, private loaderService: LoaderService
    , private itemSuperSearchService: ItemSuperSearchService) {

  }

  ngOnInit(): void {
    if (this.data !== null) {
      this.objectValue = this.data.objectValue;
      this.objectName = this.data.objectName;
      this.rangingDetailsData = this.data.dataList;
    } else {
      this.rangingDetailsData = [];
    }
    this.sourceMethodData = Object.assign([], this.sharedService.domainData["sourceMethods"]);
    this.componentName = this;
    this.setRangingGridColumnsWithOptions();
    this.isEditable = this.sharedService.checkRoleForItemSuper();
  }

  setRangingGridColumnsWithOptions(): void {
    this.rangingColumns = [
      { headerName: "", field: "", checkboxSelection: true, width: 80 },
      { headerName: "Location", field: "locationDescription", tooltipField: "locationDescription", width: 500 },
      {
        headerName: "Sourcing method", field: "sourceMethod", tooltipField: "sourceMethod"
        , cellRendererFramework: GridSelectComponent
        , cellRendererParams: { references: this.sourceMethodData }
      }
    ];

    this.rangingGridOptions = {
      onRowSelected: this.onRowSelected,
      onGridReady: (params: any) => {
        this.gridApi = params.api;
        this.gridApi.sizeColumnsToFit();
      },
      context: {
        componentParent: this
      },
      enableColResize: true
    };
  }

  onRowSelected(event: any): void {
    if (event.node.selected) {

    } else if (event.context.componentParent.node !== event.node) {
      // when we select other row when one row already selected
    } else {

    }
  }

  select(cell: any, event: any): void {
    this.rangingDetailsData[cell.rowIndex][cell.column.colId] = event.target.value;
    let rowNode: RowNode = this.gridApi.getRowNode(cell.rowIndex);
    rowNode.setData(rowNode.data);
  }

  save(): void {
    let itmRangingSuperModel: ItemRangingSuperModel = new ItemRangingSuperModel();
    itmRangingSuperModel.item = this.objectValue;
    itmRangingSuperModel.dataList = this.rangingDetailsData;
    this.itemSuperSearchService.saveItemRangingDetails(itmRangingSuperModel).subscribe((response: boolean) => {
      if (response === true) {
        this.sharedService.saveForm(`Ranging Details Saved Successfully - Item ${this.objectValue}`).subscribe(() => {
          this.loaderService.display(true);
          this.itemSuperSearchService.getItemRangingDetails(this.objectValue).subscribe((response: ItemRangingSuperModel) => {
            if (response && response.dataList && response.dataList.length > 0) {
              this.rangingDetailsData = response.dataList;
            }
            this.loaderService.display(false);
          }, (error: string) => {
            this.loaderService.display(false);
          });
        });
      }
    }, () => {

    });
  }

  resetControls(): void {

  }

  close(): void {
    this.dialog.closeAll();
  }
}
