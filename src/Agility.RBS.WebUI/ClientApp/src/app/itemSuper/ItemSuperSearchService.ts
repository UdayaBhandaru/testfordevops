import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { SharedService } from "../Common/SharedService";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../Common/HttpHelperService";
import { ItemSuperModel } from "./ItemSuperModel";
import { ItemRangingSuperModel } from "./ItemRangingSuperModel";
import { ItemSupplierCountryDimensionModel } from './ItemSupplierCountryDimensionModel';
import { ItemBasicNewModel } from './ItemBasicNewModel';

@Injectable()
export class ItemSuperSearchService {
    serviceDocument: ServiceDocument<ItemSuperModel> = new ServiceDocument<ItemSuperModel>();
    itemSuperPaging: {
        startRow: number,
        pageSize: number,
        cacheSize: number,
    } = {
            startRow: 0,
            pageSize: 10,
            cacheSize: 100,
        };

    constructor(private httpHelperService: HttpHelperService, private sharedService: SharedService) { }

    newModel(model: ItemSuperModel): ServiceDocument<ItemSuperModel> {
        return this.serviceDocument.newModel(model);
    }

    list(): Observable<ServiceDocument<ItemSuperModel>> {
        return this.serviceDocument.list("/api/ItemSuper/List");
    }

    search(additionalParams: any): Observable<ServiceDocument<ItemSuperModel>> {
        return this.serviceDocument.search("/api/ItemSuper/Search", true, () => this.setSubClassId(additionalParams));
    }

    getDataFromAPI(apiUrl: string): Observable<any> {
        var resp: any = this.httpHelperService.get(apiUrl);
        return resp;
    }

    getItemDimensions(itemCode: number, importCountryId: string): Observable<ItemSupplierCountryDimensionModel> {
        return this.httpHelperService.get("/api/ItemSuper/GetItemDimensions", new HttpParams().set("itemCode", itemCode.toString()).
            append("importCountryId", importCountryId));
    }

    getItemBasicDetails(apiUrl: string, itemCode: string): Observable<ItemBasicNewModel> {
        return this.httpHelperService.get(apiUrl, new HttpParams().set("itemCode", itemCode));
    }

    getItemRelatedData(apiUrl: string, itemCode: number, supplierId?: number): Observable<any> {
        let params: HttpParams = new HttpParams().set("itemNo", itemCode.toString());
        if (supplierId) {
            params = new HttpParams().set("itemNo", itemCode.toString()).append("supplier", supplierId.toString());
        }
        return this.httpHelperService.get(apiUrl, params);
    }

    saveItemDimensions(dimensionModel: ItemSupplierCountryDimensionModel): Observable<boolean> {
        return this.httpHelperService.post("/api/ItemSuper/SaveItemDimensions", dimensionModel);
    }

    getItemRangingDetails(itemCode: number): Observable<ItemRangingSuperModel> {
        return this.httpHelperService.get("/api/ItemSuper/GetItemRangingDetails", new HttpParams().set("itemCode", itemCode.toString()));
    }

    saveItemRangingDetails(rangingDetailsData: ItemRangingSuperModel): Observable<boolean> {
        return this.httpHelperService.post("/api/ItemSuper/SaveItemRangingDetails", rangingDetailsData);
    }

    setSubClassId(additionalParams: any): void {
        if (additionalParams) {
            this.serviceDocument.dataProfile.dataModel.startRow = additionalParams.startRow ? additionalParams.startRow : this.itemSuperPaging.startRow;
            this.serviceDocument.dataProfile.dataModel.endRow = additionalParams.endRow ? additionalParams.endRow : this.itemSuperPaging.cacheSize;
            this.serviceDocument.dataProfile.dataModel.sortModel = additionalParams.sortModel;
        } else {
            this.serviceDocument.dataProfile.dataModel.startRow = this.itemSuperPaging.startRow;
            this.serviceDocument.dataProfile.dataModel.endRow = this.itemSuperPaging.cacheSize;
        }
    }
}
