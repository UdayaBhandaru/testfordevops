export class ItemRangingModel {
  locationId: number;
  locName: string;
  formatCode: string;
  sourceMethod: string;
  locationDescription: string;
}
