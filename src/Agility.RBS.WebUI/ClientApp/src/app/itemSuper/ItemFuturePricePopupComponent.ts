import { Component, Inject } from "@angular/core";
import { DatePipe } from "@angular/common";
import { FormGroup } from "@angular/forms";
import { Subscription } from "rxjs";
import { CommonService, FormMode } from "@agility/frameworkcore";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { SharedService } from "../Common/SharedService";
import { GridOptions, GridApi } from "ag-grid-community";
import { FuturePriceModel } from './FuturePriceModel';
import { GridDateComponent } from '../Common/grid/GridDateComponent';
import { GridAmountComponent } from '../Common/grid/GridAmountComponent';

@Component({
  selector: "item-futureprice-popup",
  templateUrl: "./ItemFuturePricePopupComponent.html",
  styleUrls: ['./ItemFuturePricePopupComponent.scss']
})

export class ItemFuturePricePopupComponent {
  infoGroup: FormGroup;
  objectValue: number;
  objectName: string;
  sharedSubscription: Subscription;
  componentName: any;
  futurePriceColumns: {}[];
  gridOptions: GridOptions;
  gridApi: GridApi;
  futurePriceData: FuturePriceModel[];

  constructor(private service: SharedService, private commonService: CommonService
    , public dialog: MatDialog
    , @Inject(MAT_DIALOG_DATA)
    public data: {
      dataList: FuturePriceModel[]
      , objectName: string, objectValue: number, invokingModule: string
    }
    , private datePipe: DatePipe) {
  }

  ngOnInit(): void {
    if (this.data !== null) {
      this.objectValue = this.data.objectValue;
      this.objectName = this.data.objectName;
      this.futurePriceData = this.service.editObject;
    } else {
      this.futurePriceData = [];
    }
    this.componentName = this;
    this.futurePriceColumns = [
      { headerName: "Category", field: "deptDesc", tooltipField: "deptDesc" },
      { headerName: "Fineline", field: "classDesc", tooltipField: "classDesc" },
      { headerName: "Segment", field: "subclassDesc", tooltipField: "subclassDesc" },
      { headerName: "Location", field: "locName", tooltipField: "locName" },
      { headerName: "Action Date", field: "actionDate", tooltipField: "actionDate", cellRendererFramework: GridDateComponent },
      //{ headerName: "PC Change Type", field: "pcChangeTypeDesc", tooltipField: "pcChangeTypeDesc" },
      { headerName: "PC Change Amount", field: "pcChangeAmount", tooltipField: "pcChangeAmount", cellRendererFramework: GridAmountComponent },
      //{ headerName: "Promotion1", field: "promotion1IdDesc", tooltipField: "promotion1IdDesc" }
    ];
  }

  close(): void {
    this.dialog.closeAll();
  }
}
