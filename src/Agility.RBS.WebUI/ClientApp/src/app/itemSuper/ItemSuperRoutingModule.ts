﻿import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ItemSuperSearchComponent } from "./ItemSuperSearchComponent";
import { ItemSuperResolver } from "./ItemSuperResolver";

const ItemListRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: ItemSuperSearchComponent,
                resolve:
                {
                    references: ItemSuperResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(ItemListRoutes)
    ]
})
export class ItemSuperRoutingModule { }
