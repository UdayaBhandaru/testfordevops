﻿export class ItemPriceModel {
    locationId?: number;
    locName: string;
    fob: string;
    disc: string;
    rsp?: number;
    rspMrg?: number;
    ssp?: number;
    sspMrg?: number;
    psp?: number;
    pspMrg?: number;
    sclsp?: number;
    allowed: string;
    ranged: string;
    dsdAllowed: string;
    whseAllowed: string;
    vatCode: string;
    orderOnHand?: number;
    locDescription: string;
}