import { Component, Inject } from "@angular/core";
import { DatePipe } from "@angular/common";
import { FormGroup } from "@angular/forms";
import { Subscription } from "rxjs";
import { CommonService, FormMode } from "@agility/frameworkcore";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { SharedService } from "../Common/SharedService";
import { GridOptions, GridApi } from "ag-grid-community";
import { GridDateComponent } from '../Common/grid/GridDateComponent';
import { FutureCostModel } from './FutureCostModel';
import { GridAmountComponent } from '../Common/grid/GridAmountComponent';

@Component({
  selector: "item-futurecost-popup",
  templateUrl: "./ItemFutureCostPopupComponent.html",
  styleUrls: ['./ItemFutureCostPopupComponent.scss']
})

export class ItemFutureCostPopupComponent {
  infoGroup: FormGroup;
  objectValue: number;
  objectName: string;
  sharedSubscription: Subscription;
  componentName: any;
  futureCostColumns: {}[];
  gridOptions: GridOptions;
  gridApi: GridApi;
  futureCostData: FutureCostModel[];

  constructor(private service: SharedService, private commonService: CommonService
    , public dialog: MatDialog
    , @Inject(MAT_DIALOG_DATA)
    public data: {
      dataList: FutureCostModel[]
      , objectName: string, objectValue: number, invokingModule: string
    }
    , private datePipe: DatePipe) {
  }

  ngOnInit(): void {
    if (this.data !== null) {
      this.objectValue = this.data.objectValue;
      this.objectName = this.data.objectName;
      this.futureCostData = this.service.editObject;
    } else {
      this.futureCostData = [];
    }
    this.componentName = this;
    this.futureCostColumns = [
      { headerName: "Supplier", field: "supName", tooltipField: "supName" },
      { headerName: "Location", field: "locName", tooltipField: "locName" },
      { headerName: "Location Type", field: "locTypeDesc", tooltipField: "locTypeDesc" },
      { headerName: "Active Date", field: "activeDate", tooltipField: "activeDate", cellRendererFramework: GridDateComponent },
      { headerName: "Base Cost", field: "baseCost", tooltipField: "baseCost", cellRendererFramework: GridAmountComponent }
    ];
  }

  close(): void {
    this.dialog.closeAll();
  }
}
