﻿import { WeeksDataModel } from "./WeeksDataModel";

export class LocWiseWeeksDataModel {
    locationId?: number;
    locName: string;
    listOfWeeksData: WeeksDataModel[];
}