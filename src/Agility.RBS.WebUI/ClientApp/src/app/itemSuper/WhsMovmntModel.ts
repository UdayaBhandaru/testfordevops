﻿export class WhsMovmntModel {
    fromLoc?: number;
    toLoc?: number;
    locName: string;
    status: string;
    createDate?: Date;
    approvalDate?: Date;
    tsfType: string;
    invStatus?: number;
    tsfPrice?: number;
    tsfQty?: number;
    fillQty?: number;
    distroQty?: number;
    selectedQty?: number;
    cancelledQty?: number;
    whsPackSize?: number;
    suppPackSize?: number;
    whsStock?: number;
    overridePackSize: string;
    compPackSize?: number;
}