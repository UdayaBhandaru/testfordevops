import { Component, Inject } from "@angular/core";
import { DatePipe } from "@angular/common";
import { FormGroup } from "@angular/forms";
import { Subscription } from "rxjs";
import { CommonService, FormMode } from "@agility/frameworkcore";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { SharedService } from "../Common/SharedService";
import { GridOptions, GridApi } from "ag-grid-community";
import { GridDateComponent } from '../Common/grid/GridDateComponent';
import { FuturePromotionModel } from './FuturePromotionModel';

@Component({
  selector: "item-futurecost-popup",
  templateUrl: "./ItemPromotionPopupComponent.html",
  styleUrls: ['./ItemPromotionPopupComponent.scss']
})

export class ItemPromotionPopupComponent {
  infoGroup: FormGroup;
  objectValue: number;
  objectName: string;
  sharedSubscription: Subscription;
  componentName: any;
  futurePromotionColumns: {}[];
  gridOptions: GridOptions;
  gridApi: GridApi;
  futurePromotionData: FuturePromotionModel[];

  constructor(private service: SharedService, private commonService: CommonService
    , public dialog: MatDialog
    , @Inject(MAT_DIALOG_DATA)
    public data: {
      dataList: FuturePromotionModel[]
      , objectName: string, objectValue: number, invokingModule: string
    }
    , private datePipe: DatePipe) {
  }

  ngOnInit(): void {
    if (this.data !== null) {
      this.objectValue = this.data.objectValue;
      this.objectName = this.data.objectName;
      this.futurePromotionData = this.service.editObject;
    } else {
      this.futurePromotionData = [];
    }
    this.componentName = this;
    this.futurePromotionColumns = [
      { headerName: "Category", field: "deptDesc", tooltipField: "deptDesc" },
      { headerName: "Fineline", field: "classDesc", tooltipField: "classDesc" },
      { headerName: "Segment", field: "subclassDesc", tooltipField: "subclassDesc" },
      { headerName: "Location", field: "locName", tooltipField: "locName" },
      { headerName: "UOM", field: "sellingUomDesc", tooltipField: "sellingUomDesc" },
      { headerName: "Action Date", field: "actionDate", tooltipField: "actionDate", cellRendererFramework: GridDateComponent },
      //{ headerName: "PC Change Type", field: "pcChangeTypeDesc", tooltipField: "pcChangeTypeDesc" },
      { headerName: "PC Change Amount", field: "pcChangeAmount", tooltipField: "pcChangeAmount", cellStyle: { 'text-align': "right" } },
      { headerName: "Promotion1", field: "promotion1IdDesc", tooltipField: "promotion1IdDesc" }
    ];
  }

  close(): void {
    this.dialog.closeAll();
  }
}
