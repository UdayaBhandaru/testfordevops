import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { DocumentListComponent } from "./DocumentListComponent";
import { DocumentListResolver, DocumentResolver} from "./DocumentResolver";
import { DocumentComponent } from './Documentomponent';
const UtilityRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
              component: DocumentListComponent,
                resolve:
                {
                    serviceDocument: DocumentListResolver
                }
            },
            {
                path: "New",
                component: DocumentComponent,
                resolve:
                {
                    serviceDocument: DocumentResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(UtilityRoutes)
    ]
})
export class DocumentRoutingModule { }
