import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { DocumentModel } from "./DocumentModel";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from '../Common/HttpHelperService';

@Injectable()
export class DocumentService {
    serviceDocument: ServiceDocument<DocumentModel> = new ServiceDocument<DocumentModel>();
    constructor(private httpHelperService: HttpHelperService) { }

    newModel(model: DocumentModel): ServiceDocument<DocumentModel> {
        return this.serviceDocument.newModel(model);
    }

    search(): Observable<ServiceDocument<DocumentModel>> {
      return this.serviceDocument.search("/api/Document/Search");
    }

    save(): Observable<ServiceDocument<DocumentModel>> {
      return this.serviceDocument.save("/api/Document/Save", true);
    }

    list(): Observable<ServiceDocument<DocumentModel>> {
      return this.serviceDocument.list("/api/Document/List");
    }

    addEdit(): Observable<ServiceDocument<DocumentModel>> {
      return this.serviceDocument.list("/api/Document/New");
    }
  isExistingDocument(docId: number): Observable<boolean> {
    return this.httpHelperService.get("/api/Document/IsExistingDocument", new HttpParams().set("docId", docId.toString()));
    }
}
