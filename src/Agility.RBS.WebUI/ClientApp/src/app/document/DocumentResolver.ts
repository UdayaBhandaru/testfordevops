import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { DocumentService } from "./DocumentService";
import { DocumentModel } from "./DocumentModel";
@Injectable()
export class DocumentListResolver implements Resolve<ServiceDocument<DocumentModel>> {
    constructor(private service: DocumentService) { }
    resolve(): Observable<ServiceDocument<DocumentModel>> {
        return this.service.list();
    }
}

@Injectable()
export class DocumentResolver implements Resolve<ServiceDocument<DocumentModel>> {
    constructor(private service: DocumentService) { }
    resolve(): Observable<ServiceDocument<DocumentModel>> {
        return this.service.addEdit();
    }
}



