export class DocumentModel { 
  docId?: number;
  docDesc: string;
  docType: string;
  lcInd: string;
  swiftTag: string;
  seqNo?: number;
  text: string;
  operation?: string;
}
