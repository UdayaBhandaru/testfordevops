import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { DocumentListComponent } from "./DocumentListComponent";
import { DocumentService } from "./DocumentService";
import { DocumentRoutingModule } from "./DocumentRoutingModule";
import { DocumentListResolver, DocumentResolver } from "./DocumentResolver";
import { RbsSharedModule } from '../RbsSharedModule';
import { AgGridSharedModule } from '../AgGridSharedModule';
import { TitleSharedModule } from '../TitleSharedModule';
import { DocumentComponent } from './Documentomponent';

@NgModule({
    imports: [
        FrameworkCoreModule,
        DocumentRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        DocumentListComponent,
        DocumentComponent,
    ],
    providers: [
        DocumentService,
        DocumentListResolver,
        DocumentResolver,
    ]
})
export class DocumentModule {
    constructor() {
        LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
    }
}
