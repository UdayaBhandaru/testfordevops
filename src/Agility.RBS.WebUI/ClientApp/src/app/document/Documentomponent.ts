import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { DocumentModel } from './DocumentModel';
import { DocumentService } from './DocumentService';
import { SharedService } from '../Common/SharedService';
import { DomainDetailModel } from '../domain/DomainDetailModel';

@Component({
  selector: "Document",
  templateUrl: "./DocumentComponent.html"
})

export class DocumentComponent implements OnInit {
  PageTitle = "Document";
  redirectUrl = "Document";
    serviceDocument: ServiceDocument<DocumentModel>;
    editMode: boolean = false;
    editModel: any;
    localizationData: any;  
    model: DocumentModel;  
  docTypeData: DomainDetailModel[];
  indicatorData: DomainDetailModel[];

    constructor(private service: DocumentService, private sharedService: SharedService, private router: Router
        , private ref: ChangeDetectorRef) {
    }

  ngOnInit(): void {
    this.docTypeData = Object.assign([], this.service.serviceDocument.domainData["docType"]);
    this.indicatorData = Object.assign([], this.service.serviceDocument.domainData["indicatorStatus"]);
        this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
        this.bind();
    }

    bind(): void {
        delete this.editModel;
        if (this.sharedService.editObject) {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);         
            Object.assign(this.service.serviceDocument.dataProfile.dataModel, { operation: "U" });
            this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            delete this.sharedService.editObject;
            this.editMode = true;

        } else {            
          this.model = { docId: null, docDesc: null, docType: null, lcInd: null, swiftTag: null, seqNo:null, text: null, operation: "I" };
           
            this.service.serviceDocument.dataProfile.dataModel = this.model;
            this.service.newModel(this.model);
        }
        this.serviceDocument = this.service.serviceDocument;
    }

    save(saveOnly: boolean): void {
        if (this.serviceDocument.dataProfile.profileForm.valid) {
            this.saveSubscription(saveOnly);
        } else {
            this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
        }
    }

    saveSubscription(saveOnly: boolean): void {
        this.checkDocument(saveOnly);
  }

    saveDocument(saveOnly: boolean): void {
        this.service.save().subscribe(() => {
            if (this.service.serviceDocument.result.type === MessageType.success) {
                this.sharedService.saveForm("Document saved successfully.").subscribe(() => {
                    if (saveOnly) {
                        this.router.navigate(["/" + this.redirectUrl + "/List"], { skipLocationChange: true });
                    } else {
                        if (!this.editMode) {
                          this.model = { docId: null, docDesc: null, docType: null, lcInd: null, swiftTag: null, seqNo: null, text: null, operation: "I" };                            
                            this.service.newModel(this.model);
                        } else {
                            Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
                            this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.service.serviceDocument.dataProfile.dataModel);
                            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
                        }
                    }
                }, (error: string) => { console.log(error); });
            } else {
                this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
            }
        }, (error: string) => { console.log(error); });
    }

    checkDocument(saveOnly: boolean): void {
        if (!this.editMode) {
          this.service.isExistingDocument(this.serviceDocument.dataProfile.profileForm.controls["docId"].value).subscribe((response: any) => {
                if (response) {
                    this.sharedService.errorForm("Document already exisit.");
                } else {
                    this.saveDocument(saveOnly);
                }
            });
        } else {
            this.saveDocument(saveOnly);
        }
    }

    reset(): void {
        if (!this.editMode) {
            this.model = {
              docId: null, docDesc: null, docType: null, lcInd: null, swiftTag: null, seqNo: null, text: null, operation: "I" 
            };
           
            this.service.serviceDocument.dataProfile.dataModel = this.model;
            this.service.newModel(this.model);

        } else {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);            
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
        }
        this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
    }
}
