import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { GridOptions } from "ag-grid-community";
import { DocumentModel } from './DocumentModel';
import { GridExportModel } from '../Common/grid/GridExportModel';
import { DocumentService } from './DocumentService';
import { SharedService } from '../Common/SharedService';
import { GridEditComponent } from '../Common/grid/GridEditComponent';
import { InfoComponent } from '../Common/InfoComponent';
@Component({
  selector: "Document-list",
  templateUrl: "./DocumentListComponent.html"
})
export class DocumentListComponent implements OnInit {
  PageTitle = "Document";
  redirectUrl = "Document";
  componentName: any;
  serviceDocument: ServiceDocument<DocumentModel>;
  public gridOptions: GridOptions;
  columns: any[];
  model: DocumentModel;
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;

  constructor(public service: DocumentService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "doc Id", field: "docId", tooltipField: "docId" },
      { headerName: "doc Desc", field: "docDesc", tooltipField: "docDesc" },
      { headerName: "doc Type", field: "docType", tooltipField: "docType" },
      { headerName: "lc Ind", field: "lcInd", tooltipField: "lcInd" },
      { headerName: "swift Tag", field: "swiftTag", tooltipField: "swiftTag" },
      { headerName: "seq No", field: "seqNo", tooltipField: "seqNo" },
      { headerName: "text", field: "text", tooltipField: "text" },

      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent
      }
    ];
    
    this.model = { docId: null, docDesc: null, docType: null, lcInd: null, swiftTag: null, seqNo: null, text: null};

    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }
}
