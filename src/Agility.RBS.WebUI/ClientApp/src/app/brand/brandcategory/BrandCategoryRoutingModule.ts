﻿import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { BrandCategoryResolver } from "./BrandCategoryResolver";
import { BrandCategoryListComponent } from "./BrandCategoryListComponent";
import { BrandCategoryComponent } from "./BrandCategoryComponent";
const BrandRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: BrandCategoryListComponent,
                resolve:
                {
                    serviceDocument: BrandCategoryResolver
                }
            },
            {
                path: "New",
                component: BrandCategoryComponent,
                resolve:
                {
                    serviceDocument: BrandCategoryResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(BrandRoutes)
    ]
})
export class BrandCategoryRoutingModule { }
