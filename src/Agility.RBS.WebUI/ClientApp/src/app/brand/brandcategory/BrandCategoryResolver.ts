﻿import { Injectable } from "@angular/core";
import { Resolve } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { BrandCategoryModel } from "./BrandCategoryModel";
import { BrandCategoryService } from "./BrandCategoryService";
@Injectable()
export class BrandCategoryListResolver implements Resolve<ServiceDocument<BrandCategoryModel>> {
    constructor(private service: BrandCategoryService) { }
    resolve(): Observable<ServiceDocument<BrandCategoryModel>> {
        return this.service.list();
    }
}

@Injectable()
export class BrandCategoryResolver implements Resolve<ServiceDocument<BrandCategoryModel>> {
    constructor(private service: BrandCategoryService) { }
    resolve(): Observable<ServiceDocument<BrandCategoryModel>> {
        return this.service.addEdit();
    }
}