import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { SharedService } from "../../Common/SharedService";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { BrandCategoryModel } from "./BrandCategoryModel";
import { BrandCategoryService } from "./BrandCategoryService";
import { CommonModel } from "../../Common/CommonModel";
import { BrandModel } from "../brand/BrandModel";
import { FormGroup } from "@angular/forms";

@Component({
    selector: "brand-category",
    templateUrl: "./BrandCategoryComponent.html"
})

export class BrandCategoryComponent implements OnInit {
    PageTitle = "Brand Category";
    serviceDocument: ServiceDocument<BrandCategoryModel>;
    brandCategoryModel: BrandCategoryModel;
    localizationData: any;
    domainDtlData: DomainDetailModel[]; defaultStatus: string;
    editMode: boolean = false; editModel: BrandCategoryModel;
    model: BrandCategoryModel;
    brandData: CommonModel[];
    brandModel: BrandModel;
    constructor(private service: BrandCategoryService, private sharedService: SharedService
        , private router: Router, private ref: ChangeDetectorRef) {
    }

    ngAfterViewChecked(): void {
        this.ref.detectChanges();
    }

    ngOnInit(): void {
        this.brandData = Object.assign([], this.service.serviceDocument.domainData["brand"]);
        this.domainDtlData = Object.assign([], this.service.serviceDocument.domainData["status"]);
        this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
        this.defaultStatus = this.domainDtlData.filter(itm => itm.requiredInd === "1")[0].code;
        this.bind();
    }

    bind(): void {
        delete this.editModel;
        if (this.sharedService.editObject) {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
            this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
            this.editMode = true;
            Object.assign(this.service.serviceDocument.dataProfile.dataModel, { operation: "U", brandId: this.editModel.brandId.toString() });

            delete this.sharedService.editObject;
        } else {
            this.brandData = this.brandData.filter(item => item.status === "A");
            this.model = { brandId: null, brandCatId: null, brandKeyCat: null, brandCatStatus: this.defaultStatus , brandCatStatusDesc: null, operation: "I" };
            this.service.serviceDocument.dataProfile.dataModel = this.model;
        }
        this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
        this.serviceDocument = this.service.serviceDocument;
    }

    save(saveOnly: boolean): void {
        if (this.serviceDocument.dataProfile.profileForm.valid) {
            this.saveSubscription(saveOnly);
        } else {
            this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
        }
    }

    saveAndContinue(): void {
        this.save(false);
    }

    saveSubscription(saveOnly: boolean): void {
        this.checkBrandCategory(saveOnly);
    }
    saveBrandCategory(saveOnly: boolean): void {
        this.service.save().subscribe(() => {
            if (this.service.serviceDocument.result.type === MessageType.success) {
                this.sharedService.saveForm(this.localizationData.brandCategory.brandcategorysave).subscribe(() => {
                    if (saveOnly) {
                        this.router.navigate(["/BrandCategory/List"], { skipLocationChange: true });
                    } else {
                        if (!this.editMode) {
                            this.model = {
                                brandId: null, brandCatId: null, brandKeyCat: null, brandCatStatus: this.defaultStatus
                                , brandCatStatusDesc: null, operation: "I"
                            };
                            this.service.newModel(this.model);
                        } else {
                            Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
                            this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.editModel);
                        }
                        this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
                        this.serviceDocument = this.service.serviceDocument;
                    }
                }, (error: string) => { console.log(error); });
            } else {
                this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
            }
        }, (error: string) => { console.log(error); });
    }

    reset(): void {
        if (!this.editMode) {
            this.model = { brandId: null, brandCatId: null, brandKeyCat: null, brandCatStatus: this.defaultStatus, brandCatStatusDesc: null, operation: "I" };
            this.service.newModel(this.model);
        } else {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);
            Object.assign(this.service.serviceDocument.dataProfile.dataModel, { brandId: this.editModel.brandId.toString() });

            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
        }
        this.serviceDocument = this.service.serviceDocument;
        this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
    }

 private checkBrandCategory(saveOnly: boolean): void {
     if (!this.editMode) {
         var form: FormGroup = this.serviceDocument.dataProfile.profileForm;
            this.service.isExistingBrandCategory(form.controls["brandId"].value, form.controls["brandCatId"].value).subscribe
                ((response: boolean) => {
                    if (response) {
                        this.sharedService.errorForm(this.localizationData.brandCategory.brandcategorycheck);
                    } else {
                        this.saveBrandCategory(saveOnly);
                    }
                });
        } else {
    this.saveBrandCategory(saveOnly);
        }
    }

    openBrandMaster($event: MouseEvent): void {
        $event.preventDefault();
        let dataModel: BrandCategoryModel = this.service.serviceDocument.dataProfile.dataModel;
        this.brandModel = {
            brandId: dataModel.brandId, brandName: null, brandValue: null,operation: null
        };
        this.sharedService.searchData = Object.assign({}, this.brandModel);
        this.router.navigate(["/Brand"], { skipLocationChange: true });
    }
}
