﻿import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { BrandCategoryModel } from "./BrandCategoryModel";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";

@Injectable()
export class BrandCategoryService {
    serviceDocument: ServiceDocument<BrandCategoryModel> = new ServiceDocument<BrandCategoryModel>();
    constructor(private httpHelperService: HttpHelperService) { }
    newModel(model: BrandCategoryModel): ServiceDocument<BrandCategoryModel> {
        return this.serviceDocument.newModel(model);
    }

    search(): Observable<ServiceDocument<BrandCategoryModel>> {
        return this.serviceDocument.search("/api/BrandCategory/Search");
    }

    save(): Observable<ServiceDocument<BrandCategoryModel>> {
        return this.serviceDocument.save("/api/BrandCategory/Save", true);
    }

    list(): Observable<ServiceDocument<BrandCategoryModel>> {
        return this.serviceDocument.list("/api/BrandCategory/List");
    }

    addEdit(): Observable<ServiceDocument<BrandCategoryModel>> {
        return this.serviceDocument.list("/api/BrandCategory/New");
    }

    isExistingBrandCategory(brandId: number, brandCatId: number): Observable<boolean> {
        return this.httpHelperService.get("/api/BrandCategory/IsExistingBrandCategory", new HttpParams().set("brandId", brandId.toString())
            .set("brandCatId", brandCatId.toString()));
    }
}