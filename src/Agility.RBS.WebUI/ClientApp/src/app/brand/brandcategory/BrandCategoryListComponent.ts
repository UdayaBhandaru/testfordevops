import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { SharedService } from "../../Common/SharedService";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { GridEditComponent } from "../../Common/grid/GridEditComponent";
import { InfoComponent } from "../../Common/InfoComponent";
import { GridExportModel } from "../../Common/grid/GridExportModel";
import { FormGroup } from "@angular/forms";
import { BrandCategoryModel } from "./BrandCategoryModel";
import { BrandCategoryService } from "./BrandCategoryService";
import { CommonModel } from "../../Common/CommonModel";
import { GridOptions } from 'ag-grid-community';

@Component({
  selector: "brand-category",
  templateUrl: "./BrandCategoryListComponent.html"
})
export class BrandCategoryListComponent implements OnInit {
  public serviceDocument: ServiceDocument<BrandCategoryModel>;
  PageTitle = "Brand Category";
  sheetName: string = "BrandCategory";
  redirectUrl = "BrandCategory";
  componentName: any;
  columns: any[];
  domainDtlData: DomainDetailModel[];
  model: BrandCategoryModel;
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  brandData: CommonModel[];
  gridOptions: GridOptions;

  constructor(public service: BrandCategoryService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "Brand", field: "brandName", tooltipField: "brandName", width: 45 },
      { headerName: "Brand Category Id", field: "brandCatId", tooltipField: "brandCatId", width: 60 },
      { headerName: "Brand Key Category", field: "brandKeyCat", tooltipField: "brandKeyCat" },
      { headerName: "Status", field: "brandCatStatusDesc", tooltipField: "brandCatStatusDesc", width: 35 },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 45
      }
    ];
    this.domainDtlData = this.service.serviceDocument.domainData["status"];
    this.brandData = this.service.serviceDocument.domainData["brand"];
    this.brandData.forEach(x => x.id = +x.id);

    delete this.sharedService.domainData;
    this.sharedService.domainData = { status: this.domainDtlData, brand: this.brandData };
    this.model = { brandId: null, brandCatId: null, brandKeyCat: null, brandCatStatus: null, brandCatStatusDesc: null };
    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }
}
