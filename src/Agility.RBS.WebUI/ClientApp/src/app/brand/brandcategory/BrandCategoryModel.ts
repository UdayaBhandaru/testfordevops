﻿export class BrandCategoryModel {
    brandId: number;
    brandName?: string;
    brandCatId: number;
    brandKeyCat: string;
    brandCatStatus: string;
    brandCatStatusDesc: string;
    operation?: string;
}