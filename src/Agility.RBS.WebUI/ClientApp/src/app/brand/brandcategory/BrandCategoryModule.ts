import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { BrandCategoryRoutingModule } from "./BrandCategoryRoutingModule";
import { BrandCategoryResolver } from "./BrandCategoryResolver";
import { BrandCategoryComponent } from "./BrandCategoryComponent";
import { BrandCategoryListComponent } from "./BrandCategoryListComponent";
import { BrandCategoryService } from "./BrandCategoryService";
import { RbsSharedModule } from "../../RbsSharedModule";

@NgModule({
    imports: [
        FrameworkCoreModule,
        BrandCategoryRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        BrandCategoryListComponent,
        BrandCategoryComponent,
    ],
    providers: [
        BrandCategoryService,
        BrandCategoryResolver,
    ]
})
export class BrandCategoryModule {
    constructor() {
        LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
    }
}
