﻿import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { BrandResolver} from "./BrandResolver";
import { BrandComponent } from "./BrandComponent";
import { BrandListComponent } from "./BrandListComponent";
const BrandRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: BrandListComponent,
                resolve:
                {
                    serviceDocument: BrandResolver
                }
            },
            {
                path: "New",
                component: BrandComponent,
                resolve:
                {
                    serviceDocument: BrandResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(BrandRoutes)
    ]
})
export class BrandRoutingModule { }
