import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { SharedService } from "../../Common/SharedService";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { GridEditComponent } from "../../Common/grid/GridEditComponent";
import { InfoComponent } from "../../Common/InfoComponent";
import { GridExportModel } from "../../Common/grid/GridExportModel";
import { FormGroup } from "@angular/forms";
import { BrandModel } from "./BrandModel";
import { BrandService } from "./BrandService";
import { GridOptions } from 'ag-grid-community';

@Component({
  selector: "brand",
  templateUrl: "./BrandListComponent.html"
})
export class BrandListComponent implements OnInit {
  public serviceDocument: ServiceDocument<BrandModel>;
  PageTitle = "Brand";
  sheetName: string = "Brand";
  redirectUrl = "Brand";
  componentName: any;
  columns: any[];
  domainDtlData: DomainDetailModel[];
  model: BrandModel;
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  gridOptions: GridOptions;

  constructor(public service: BrandService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "Brand Id", field: "brandId", tooltipField: "brandId", width: 60 },
      { headerName: "Brand Name", field: "brandName", tooltipField: "brandName" },
      { headerName: "Brand Value", field: "brandValue", tooltipField: "brandValue" },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 60
      }
    ];
    this.domainDtlData = this.service.serviceDocument.domainData["status"];
    delete this.sharedService.domainData;
    this.sharedService.domainData = { status: this.domainDtlData };
    this.model = { brandId: null, brandName: null, brandValue:null};
    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }
}
