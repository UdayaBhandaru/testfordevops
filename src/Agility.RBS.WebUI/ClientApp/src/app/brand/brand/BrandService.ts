import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { BrandModel } from "./BrandModel";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";

@Injectable()
export class BrandService {
    serviceDocument: ServiceDocument<BrandModel> = new ServiceDocument<BrandModel>();
    constructor(private httpHelperService: HttpHelperService) { }
    newModel(model: BrandModel): ServiceDocument<BrandModel> {
        return this.serviceDocument.newModel(model);
    }

    search(): Observable<ServiceDocument<BrandModel>> {
        return this.serviceDocument.search("/api/Brand/Search");
    }

    save(): Observable<ServiceDocument<BrandModel>> {
        return this.serviceDocument.save("/api/Brand/Save", true);
    }

    list(): Observable<ServiceDocument<BrandModel>> {
        return this.serviceDocument.list("/api/Brand/List");
    }

    addEdit(): Observable<ServiceDocument<BrandModel>> {
        return this.serviceDocument.list("/api/Brand/New");
    }

  isExistingBrand(brandValue: number, brandName:string): Observable<boolean> {
    return this.httpHelperService.get("/api/Brand/IsExistingBrand", new HttpParams().set("brandValue", brandValue.toString()).set("brandName", brandName));
    }
}
