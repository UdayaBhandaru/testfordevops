import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { SharedService } from "../../Common/SharedService";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { BrandModel } from "./BrandModel";
import { BrandService } from "./BrandService";
import { CommonModel } from "../../Common/CommonModel";
import { BrandCategoryModel } from "../brandcategory/BrandCategoryModel";
@Component({
    selector: "brand",
    templateUrl: "./BrandComponent.html"
})

export class BrandComponent implements OnInit {
    PageTitle = "Brand";
    serviceDocument: ServiceDocument<BrandModel>;
    brandModel: BrandModel;
    brandCatgModel: BrandCategoryModel;
    localizationData: any;
    domainDtlData: DomainDetailModel[]; defaultStatus: string;
    editMode: boolean = false; editModel: BrandModel;
    model: BrandModel;
    brandData: CommonModel[];

    constructor(private service: BrandService, private sharedService: SharedService
        , private router: Router, private ref: ChangeDetectorRef) {
    }

    ngAfterViewChecked(): void {
        this.ref.detectChanges();
    }

    ngOnInit(): void {
        this.domainDtlData = Object.assign([], this.service.serviceDocument.domainData["status"]);
        this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
        this.bind();
    }

    bind(): void {
        delete this.editModel;
        if (this.sharedService.editObject) {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
            this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
            this.editMode = true;
            delete this.sharedService.editObject;
        } else {
            this.model = {
                brandId: null, brandName: null, brandValue: null, operation: "I"
            };
            this.service.serviceDocument.dataProfile.dataModel = this.model;
        }
        this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
        this.serviceDocument = this.service.serviceDocument;
    }

    save(saveOnly: boolean): void {
        if (this.serviceDocument.dataProfile.profileForm.valid) {
            this.saveSubscription(saveOnly);
        } else {
            this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
        }
    }

    saveAndContinue(): void {
        this.save(false);
    }

    saveSubscription(saveOnly: boolean): void {
        this.checkBrand(saveOnly);
    }
    saveBrand(saveOnly: boolean): void {
        this.service.save().subscribe(() => {
            if (this.service.serviceDocument.result.type === MessageType.success) {
                this.sharedService.saveForm(this.localizationData.brand.brandsave).subscribe(() => {
                    if (saveOnly) {
                        this.router.navigate(["/Brand/List"], { skipLocationChange: true });
                    } else {
                        if (!this.editMode) {
                            this.model = {
                                brandId: null, brandName: null, brandValue: null, operation: "I"
                            };
                            this.service.newModel(this.model);
                        } else {
                            Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
                            this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.editModel);
                        }
                        this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
                        this.serviceDocument = this.service.serviceDocument;
                    }
                }, (error: string) => { console.log(error); });
            } else {
                this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
            }
        }, (error: string) => { console.log(error); });
    }
    checkBrand(saveOnly: boolean): void {
        if (!this.editMode) {
            this.service.isExistingBrand(this.serviceDocument.dataProfile.profileForm.controls["brandValue"].value
                , this.serviceDocument.dataProfile.profileForm.controls["brandName"].value)
                .subscribe((response: any) => {
                    if (response) {
                        this.sharedService.errorForm(this.localizationData.brand.brandcheck);
                    } else {
                        this.saveBrand(saveOnly);
                    }
                });
        } else {
            this.saveBrand(saveOnly);
        }
    }

    reset(): void {
        if (!this.editMode) {
            this.model = {
              brandId: null, brandName: null, brandValue: null, operation: "I"
            };
            this.service.newModel(this.model);
        } else {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
        }
        this.serviceDocument = this.service.serviceDocument;
        this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
    }
    
}
