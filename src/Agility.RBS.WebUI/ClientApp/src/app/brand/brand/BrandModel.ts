export class BrandModel {
    brandId: number;
    brandName: string;
    brandValue?: number;
    operation?: string;
}
