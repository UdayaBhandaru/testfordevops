﻿import { Injectable } from "@angular/core";
import { Resolve } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { BrandModel } from "./BrandModel";
import { BrandService } from "./BrandService";
@Injectable()
export class BrandListResolver implements Resolve<ServiceDocument<BrandModel>> {
    constructor(private service: BrandService) { }
    resolve(): Observable<ServiceDocument<BrandModel>> {
        return this.service.list();
    }
}

@Injectable()
export class BrandResolver implements Resolve<ServiceDocument<BrandModel>> {
    constructor(private service: BrandService) { }
    resolve(): Observable<ServiceDocument<BrandModel>> {
        return this.service.addEdit();
    }
}