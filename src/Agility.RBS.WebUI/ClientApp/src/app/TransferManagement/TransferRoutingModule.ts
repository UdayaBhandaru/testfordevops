import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { TransferListComponent } from "./List/TransferListComponent";
import { TransferListResolver } from "./TransferResolver";
import { TransferComponent } from "./TransferComponent";
import { TransferHeadResolver } from "./Head/TransferHeadResolver";
import { TransferHeadComponent } from "./Head/TransferHeadComponent";
import { TransferDetailsResolver } from "./Details/TransferDetailsResolver";
import { TransferDetailsComponent } from "./Details/TransferDetailsComponent";
import RbHeadGuard from "../guards/RbHeadGuard";
import RbDetailsGuard from "../guards/RbDetailsGuard";

const TransferRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
              component: TransferListComponent,
                resolve:
                {
                    serviceDocument: TransferListResolver
                }
            },
            {
                path: "New",
                component: TransferComponent,
                children: [
                    {
                        path: "", redirectTo: "TransferHead/", pathMatch: "full"
                    },
                    {
                        path: "TransferHead/:id",
                      component: TransferHeadComponent,
                        resolve: {
                          serviceDocument: TransferHeadResolver
                        },
                        //canDeactivate: [RbHeadGuard]
                    },
                    {
                        path: "TransferDetails/:id",
                        component: TransferDetailsComponent,
                        resolve: {
                            serviceDocument: TransferDetailsResolver
                        },
                        //canDeactivate: [RbDetailsGuard]
                    }
                ]
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(TransferRoutes)
    ]
})
export class TransferRoutingModule { }

