import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { AgGridSharedModule } from "../AgGridSharedModule";
import { TitleSharedModule } from "../TitleSharedModule";
import { DatePickerModule } from "../Common/DatePickerModule";
import { TransferRoutingModule } from "./TransferRoutingModule";
import { TransferComponent } from "./TransferComponent";

import { TransferListComponent } from "./List/TransferListComponent";
import { TransferService } from "./TransferService";
import { TransferSharedService } from "./TransferSharedService";
import { TransferListResolver } from "./TransferResolver";

import { TransferHeadComponent } from "./Head/TransferHeadComponent";
import { TransferHeadService } from "./Head/TransferHeadService";
import { TransferHeadResolver } from "./Head/TransferHeadResolver";

import { TransferDetailsComponent } from "./Details/TransferDetailsComponent";
import { TransferDetailsService } from "./Details/TransferDetailsService";
import { TransferDetailsResolver } from "./Details/TransferDetailsResolver";
import { InboxService } from "../inbox/InboxService";
import { ShipmentService } from "../SupplyChainManagement/ShipmentService";
import { RbsSharedModule } from "../RbsSharedModule";

@NgModule({
    imports: [
        FrameworkCoreModule,
        TransferRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        DatePickerModule,
        RbsSharedModule
    ],
    declarations: [
         TransferListComponent,
       TransferComponent,
      TransferHeadComponent,
        TransferDetailsComponent
    ],
    providers: [
      TransferService,
       TransferSharedService,
        TransferListResolver,
      TransferHeadResolver,
      TransferHeadService,
      TransferDetailsResolver,
      TransferDetailsService,
        InboxService,
        ShipmentService
    ]
})
export class TransferModule {
    constructor() {
        LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
    }
}
