import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpParams } from "@angular/common/http";
import { MessageType } from "@agility/frameworkcore";
import { SharedService } from "../Common/SharedService";
import { RbCommonService } from "../Common/RbCommonService";

@Injectable()
export class TransferSharedService extends RbCommonService {
  tsF_NO: number;
  fromLoc: number;
  fromLocType: string;
  toLocType: string;
  toLoc: number;
  tsfType: string;
    deptId: number;
    supplier: number;
    location: string;
    locType: string;
    deptDesc: string;
    supName: string;
    notBeforeDate: Date;
    notAfterDate: Date;
    headColumns = [{ title: "Details", dataKey: "details" }, { title: "Values", dataKey: "values" }];
    detailColumns = ["Seq No", "Barcode / Mfg #", "Item", "Item Type", "Item Desc / Brand", "No. Of Cases", "Case Size", "No. Of Units",
        "Std. Unit", "Dis. Value", "Dis. Type", "Initial Unit Cost", "Initial Total Cost", "Final Unit Cost", "Final Total Cost"];
    supplierColumns = [{ title: "Supplier Details", dataKey: "supplierDetails" }, { title: "Values", dataKey: "values" }];
    orderChangeHeadStatus: string;

    mandatoryOrderDetailsFields: string[] = [
        "supplier",
        "item",
        "location",
        "locType",
        "unitRetail",
        "qtyOrdered",
        "unitCost",
        "unitCostInit"
    ];

    constructor(public sharedService: SharedService) {
        super(sharedService);
    }
 
}
