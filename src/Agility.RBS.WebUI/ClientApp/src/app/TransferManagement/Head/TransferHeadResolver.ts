import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { TransferHeadModel } from "../Head/TransferHeadModel";
import { TransferHeadService } from "./TransferHeadService";

@Injectable()
export class TransferHeadResolver implements Resolve<ServiceDocument<TransferHeadModel>> {
  constructor(private service: TransferHeadService) { }
  resolve(route: ActivatedRouteSnapshot): Observable<ServiceDocument<TransferHeadModel>> {
        return this.service.addEdit(route.params["id"]);
    }
}
