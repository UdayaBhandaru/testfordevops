import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument, WorkflowAction } from "@agility/frameworkcore";
import { SharedService } from "../../Common/SharedService";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";
import { TransferSharedService } from "../TransferSharedService";
import { TransferHeadModel } from './TransferHeadModel';

@Injectable()
export class TransferHeadService {
  serviceDocument: ServiceDocument<TransferHeadModel> = new ServiceDocument<TransferHeadModel>();

  constructor(private httpHelperService: HttpHelperService
    , private sharedService: SharedService
    , private transferSharedService: TransferSharedService) { }

  newModel(model: TransferHeadModel): ServiceDocument<TransferHeadModel> {
    return this.serviceDocument.newModel(model);
  }

  save(): Observable<ServiceDocument<TransferHeadModel>> {
    //let currentAction: WorkflowAction = this.serviceDocument.dataProfile.actionService.allowedActions
    //    .find((v) => v.transitionClaim === "OrderStartCreatedClaim");
    //if (!this.serviceDocument.dataProfile.dataModel.tsF_NO && currentAction) {
    //    this.serviceDocument.dataProfile.profileForm.controls["status"].setValue("WS");
    //    this.serviceDocument.dataProfile.profileForm.controls["currentActionId"].setValue(currentAction.transitionClaim);
    //  return this.serviceDocument.submit("/api/TransferHead/Submit", true, () => this.setDate());
    //} else {
    //  return this.serviceDocument.save("/api/TransferHead/Save", true, () => this.setDate());
    //}
    //return this.serviceDocument.save("/api/TransferHead/Save", true, () => this.setDate());


    let currentAction: WorkflowAction = this.serviceDocument.dataProfile.actionService.allowedActions
      .find((v) => v.transitionClaim === "TransferStartSubmitClaim");
    if (!this.serviceDocument.dataProfile.dataModel.tsF_NO && currentAction) {
      //this.serviceDocument.dataProfile.profileForm.controls["status"].setValue("WS");
      this.serviceDocument.dataProfile.profileForm.controls["currentActionId"].setValue(currentAction.transitionClaim);
      return this.serviceDocument.submit("/api/TransferHead/Submit", true);
    } else {
      return this.serviceDocument.save("/api/TransferHead/Save", true);
    }

  }

  submit(): Observable<ServiceDocument<TransferHeadModel>> {
    this.serviceDocument.dataProfile.profileForm.controls["status"].setValue("SUB");
    return this.serviceDocument.submit("/api/TransferHead/Submit", true);
  }

  reject(): Observable<ServiceDocument<TransferHeadModel>> {
    return this.serviceDocument.submit("/api/TransferHead/Reject", true);
  }

  sendBackForReview(): Observable<ServiceDocument<TransferHeadModel>> {
    return this.serviceDocument.submit("/api/TransferHead/SendBackForReview", true);
  }

  addEdit(id: number): Observable<ServiceDocument<TransferHeadModel>> {
    if (+id === 0) {
      this.transferSharedService.tsF_NO = null;
      return this.serviceDocument.new("/api/TransferHead/New");
    } else {
      return this.serviceDocument.open("/api/TransferHead/Open", new HttpParams().set("id", id.toString()));
    }
  }

  getDataFromAPI(apiUrl: string): Observable<any> {
    let resp: Observable<any> = this.httpHelperService.get(apiUrl);
    return resp;
  }
}
