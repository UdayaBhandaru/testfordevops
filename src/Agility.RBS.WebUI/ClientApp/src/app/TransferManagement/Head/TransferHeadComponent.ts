import { Component, OnInit, TemplateRef, ViewChild, OnDestroy, ChangeDetectorRef } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ServiceDocument, CommonService, MessageResult, MessageType } from "@agility/frameworkcore";
import { SharedService } from "../../Common/SharedService";
import { LoaderService } from "../../Common/LoaderService";
import { MatDialogRef } from "@angular/material";
import { GridOptions } from "ag-grid-community";
import { Subscription } from "rxjs";
import { TransferHeadService } from "./TransferHeadService";
import { TransferSharedService } from "../TransferSharedService";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { TransferHeadModel } from "../Head/TransferHeadModel";
import { CommonModel } from "../../Common/CommonModel";
import { DeptBuyerDomainModel } from "../../Common/DomainData/DeptBuyerDomainModel";
import { SupplierDomainModel } from "../../Common/DomainData/SupplierDomainModel";
import { PromotionDomainModel } from "../../Common/DomainData/PromotionDomainModel";
import { CountryDomainModel } from "../../Common/DomainData/CountryDomainModel";
import { SupplierAddressDomainModel } from "../../Common/DomainData/SupplierAddressDomainModel";
import { LocationTypeDomainModel } from "../../Common/DomainData/LocationTypeDomainModel";
import { PartnerDomainModel } from "../../Common/DomainData/PartnerDomainModel";
import { RbButton } from "../../Common/controls/RbControlModels";
import { RbDialogService } from "../../Common/controls/RbDialogService";
import { DocumentsConstants } from "../../Common/DocumentsConstants";
import { InboxCommonModel } from "../../Common/DomainData/InboxCommonModel";
import { SupplierDetailDomainModel } from "../../Common/DomainData/SupplierDetailDomainModel";
import { TransferEntityDomainModel } from '../../Common/DomainData/TransferEntityDomainModel';
import { GroupDomainModel } from '../../Common/DomainData/GroupDomainModel';


@Component({
    selector: "transfer-Head-add",
  templateUrl: "./TransferHeadComponent.html",
  styleUrls: ['./TransferHeadComponent.scss']
    //styles: [`
    //  :host {
    //   flex-direction:column;
    //    display: block !important;
    //  }`]
})
export class TransferHeadComponent implements OnInit, OnDestroy {
    screenName: any;
    _componentName = "Transfer Head";
  public serviceDocument: ServiceDocument<TransferHeadModel>;
    localizationData: any;    
    primaryId: number; // used in CanDeactivate method, should be same name if we use candeactivate
  datesArray: any[] = [];
  defaultStatus: string;
  model: TransferHeadModel = {
    tsF_NO: null,
    tsfParentNo: null,
    fromLocType: null,
    fromLoc: null,
    toLocType: null,
    toLoc: null,
    expDcDate: null,
    dept: null,
    deptDesc: null,
    inventoryType: null,
    tsfType: null,
    status: null,
    statusDesc: null,
    freightCode: null,
    routingCode: null,
    createDate: null,
    createId: null,
    approvalDate: null,
    approvalId: null,
    deliveryDate: null,
    closeDate: null,
    extRefNo: null,
    repltsfApproveInd: null,
    commentDesc: null,
    expDcEowDate: null,
    mrtNo: null,
    notAfterDate: null,
    contextType: null,
    contextValue: null,
    restockPct: null,
    operation: null,
    programPhase: null,
    programMessage: null,
    error: null,
    organizationId: null,
    modifiedBy: null,
    deletedInd: null,
    modifiedDate: null,
    createdDate: null,
    createdBy: null,
    fileCount: null,
    workflowInstance:null
    };
    editMode: boolean;
  apiUrl: string;

  transferTypeData: DomainDetailModel[];
  contextTypeData: DomainDetailModel[];
  indicatorData: DomainDetailModel[];
  tsfEntityData: TransferEntityDomainModel[];
  groupData: GroupDomainModel[];
  locationData: LocationTypeDomainModel[];
  locTypeData: DomainDetailModel[];
  domainStatusData: DomainDetailModel[];
  inventoryStatusData: DomainDetailModel[];

  fromLocationData: LocationTypeDomainModel[];
  fromLocationsFileredList: LocationTypeDomainModel[];

  toLocationData: LocationTypeDomainModel[];
  toLocationsFileredList: LocationTypeDomainModel[];

    dialogRef: MatDialogRef<any>;
    private messageResult: MessageResult = new MessageResult();
    canNavigate: boolean;
    saveContinueSubscribtion: Subscription;
    resetSubscribtion: Subscription;
    saveSubscribtion: Subscription;
    closeSubscribtion: Subscription;
    submitSubscription: Subscription;
    needInfoSubscription: Subscription;
    rejectSubscription: Subscription;
    discardSubscription: Subscription;
    rejectServiceSubscription: Subscription;
    saveServiceSubscription: Subscription;
    routeServiceSubscription: Subscription;
    saveSuccessSubscription: Subscription;
    workflowSuccessSubscription: Subscription;
    submitServiceSubscription: Subscription;
    sendBkReviewServiceSubscription: Subscription;
    supplierDetailsServiceSubscription: Subscription;
    exchangeRateServiceSubscription: Subscription;
    nextSubscription: Subscription;
    effectiveMinDate: Date = new Date();
    currentDate: Date = new Date();
    inboxCommonData: InboxCommonModel;

  constructor(private route: ActivatedRoute, private router: Router, public sharedService: SharedService, private service: TransferHeadService
    , private commonService: CommonService, private loaderService: LoaderService, private orderMgmtSharedService: TransferSharedService
        , private changeRef: ChangeDetectorRef, private dialogService: RbDialogService) {

        this.saveSubscribtion = orderMgmtSharedService.saveAnnounced$.subscribe((response: string) => {
            if (response === this._componentName) {
                this.save(true);
            }
        });

        this.saveContinueSubscribtion = orderMgmtSharedService.saveContinueAnnounced$.subscribe((response: string) => {
            if (response === this._componentName) {
                this.save(false);
            }
        });

        this.resetSubscribtion = orderMgmtSharedService.resetAnnounced$.subscribe((response: string) => {
            if (response === this._componentName) {
                this.reset();
            }
        });

        this.closeSubscribtion = orderMgmtSharedService.closeAnnounced$.subscribe((response: string) => {
            if (response === this._componentName) {
                this.close();
            }
        });

        this.nextSubscription = orderMgmtSharedService.nextAnnounced$.subscribe((response: string) => {
            if (response === this._componentName) {
                this.nextTab();
            }
        });
    }

    ngOnInit(): void {
        this.screenName = new DocumentsConstants().orderObjectName;
      this.bind();    
    }

    ngAfterViewChecked(): void {
        this.changeRef.detectChanges();
    }

    bind(): void {
        this.routeServiceSubscription = this.route.params.subscribe((resp: any) => {
            this.primaryId = resp["id"];
      });

      this.transferTypeData = Object.assign([], this.service.serviceDocument.domainData["transferType"]);
      this.contextTypeData = Object.assign([], this.service.serviceDocument.domainData["contextType"]);
      this.locationData = Object.assign([], this.service.serviceDocument.domainData["locType"]);
      this.tsfEntityData = Object.assign([], this.service.serviceDocument.domainData["tsfEntity"]);
      this.groupData = Object.assign([], this.service.serviceDocument.domainData["group"]);
      this.locTypeData = Object.assign([], this.service.serviceDocument.domainData["locationsType"]);
      this.domainStatusData = Object.assign([], this.service.serviceDocument.domainData["statusType"]);
      this.inventoryStatusData = Object.assign([], this.service.serviceDocument.domainData["inventoryType"]);
      this.fromLocationsFileredList = Object.assign([], this.service.serviceDocument.domainData["locType"]);
      this.toLocationsFileredList = Object.assign([], this.service.serviceDocument.domainData["locType"]);

        if (this.primaryId.toString() !== "0") {
          this.editMode = true;

          this.fromLocationData = this.fromLocationsFileredList.filter(item => item.locType === this.service.serviceDocument.dataProfile.dataModel.fromLocType);
          this.toLocationData = this.toLocationsFileredList.filter(item => item.locType === this.service.serviceDocument.dataProfile.dataModel.toLocType);    
    

            this.datesArray.sort((a: any, b: any) => {
                let frmDate: Date = a.date ? new Date(a.date) : new Date();
                let toDate: Date = b.date ? new Date(b.date) : new Date();
                return frmDate.getTime() - toDate.getTime();
          });

          this.orderMgmtSharedService.fromLocType = this.service.serviceDocument.dataProfile.dataModel.fromLocType;
          this.orderMgmtSharedService.fromLoc = this.service.serviceDocument.dataProfile.dataModel.fromLoc;
          this.orderMgmtSharedService.toLocType = this.service.serviceDocument.dataProfile.dataModel.toLocType;
          this.orderMgmtSharedService.toLoc = this.service.serviceDocument.dataProfile.dataModel.toLoc;
          this.orderMgmtSharedService.tsfType = this.service.serviceDocument.dataProfile.dataModel.tsfType;
          this.orderMgmtSharedService.tsF_NO = this.service.serviceDocument.dataProfile.dataModel.tsF_NO; 

        } else {
          let fromDate: Date = new Date();
          fromDate.setDate(fromDate.getDate());
          this.service.serviceDocument.dataProfile.dataModel = Object.assign(this.service.serviceDocument.dataProfile.dataModel
            , { operation: "I", status: "WORKSHEET" }
            , { workflowInstance: this.service.serviceDocument.dataProfile.dataModel.workflowInstance }, { operation: "I", createDate: fromDate, tsfType: "MR", status:"I"});
          
            //this.service.serviceDocument.dataProfile.dataModel = Object.assign(this.service.serviceDocument.dataProfile.dataModel
            //  , this.model, { operation: "I", createDate: fromDate, tsfType: "MR"});
          this.service.serviceDocument.dataProfile.profileForm.patchValue(this.service.serviceDocument.dataProfile.dataModel);         
        }
        //this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
       this.orderMgmtSharedService.orderChangeHeadStatus = this.service.serviceDocument.dataProfile.dataModel.statusDesc;
        this.serviceDocument = this.service.serviceDocument;        

        //this.orderMgmtSharedService.actionType = this.inboxCommonData.actionType;
        //this.sharedService.fileCount = this.service.serviceDocument.dataProfile.dataModel.fileCount;
        this.sharedService.checkWfPermitions(this.serviceDocument);
    }

    save(saveOnly: boolean): void {
        if (this.serviceDocument.dataProfile.profileForm.valid) {
            this.saveSubscription(saveOnly);
        } else {
            this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
        }
    }

    saveAndContinue(): void {
        this.save(false);
    }

    submitAction(): void {
        this.loaderService.display(true);
        this.submitServiceSubscription = this.service.submit().subscribe(() => {
          this.workflowActionResult("Submitted - Transfer No ", "/Transfer/List");
            this.loaderService.display(false);
        });
    }

    sendBackForReview(): void {
        this.loaderService.display(true);
        this.sendBkReviewServiceSubscription = this.service.sendBackForReview().subscribe(() => {
          this.workflowActionResult("Sent back - Transfer No ", "/Transfer/List");
            this.loaderService.display(false);
        });
    }

    workflowActionResult(message: string, navigateUrl: string): void {
        if (this.service.serviceDocument.result.type === MessageType.success) {
            this.workflowSuccessSubscription = this.sharedService.saveForm(`${message}  ${this.primaryId}`).subscribe(() => {
                this.router.navigate([navigateUrl], { skipLocationChange: true });
            });
        } else {
            this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
        }
    }

    reject(): void {
        this.loaderService.display(true);
        this.rejectServiceSubscription = this.service.reject().subscribe(() => {
          this.workflowActionResult("Rejected - Transfer No ", "/Transfer/List");
            this.loaderService.display(false);
        });
    }

  saveSubscription(saveOnly: boolean): void {
    this.loaderService.display(true);
    this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.primaryId = this.service.serviceDocument.dataProfile.dataModel.tsF_NO;
        this.sharedService.saveForm(`Transfer Head Saved Successfully - Transfer No ${this.primaryId}`).subscribe(() => {
          if (saveOnly) {
            this.router.navigate(["/Transfer/List"], { skipLocationChange: true });
          } else {
            this.orderMgmtSharedService.fromLocType = this.service.serviceDocument.dataProfile.dataModel.fromLocType;
            this.orderMgmtSharedService.fromLoc = this.service.serviceDocument.dataProfile.dataModel.fromLoc;
            this.orderMgmtSharedService.toLocType = this.service.serviceDocument.dataProfile.dataModel.toLocType;
            this.orderMgmtSharedService.toLoc = this.service.serviceDocument.dataProfile.dataModel.toLoc;
            this.orderMgmtSharedService.tsfType = this.service.serviceDocument.dataProfile.dataModel.tsfType;
            this.orderMgmtSharedService.tsF_NO = this.service.serviceDocument.dataProfile.dataModel.tsF_NO; 
            this.router.navigate(["/Blank"], { skipLocationChange: true, queryParams: { id: "/Transfer/New/TransferDetails/" + this.primaryId } });
          }
        });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
      this.loaderService.display(false);
    }, (error: string) => { console.log(error); });
  }

    ngOnDestroy(): void {
        this.saveSubscribtion.unsubscribe();
        this.saveContinueSubscribtion.unsubscribe();
        this.resetSubscribtion.unsubscribe();
        this.closeSubscribtion.unsubscribe();
        this.routeServiceSubscription.unsubscribe();
        if (this.rejectServiceSubscription) {
            this.rejectServiceSubscription.unsubscribe();
        }
        if (this.saveServiceSubscription) {
            this.saveServiceSubscription.unsubscribe();
        }
        if (this.workflowSuccessSubscription) {
            this.workflowSuccessSubscription.unsubscribe();
        }
        if (this.saveSuccessSubscription) {
            this.saveSuccessSubscription.unsubscribe();
        }
        if (this.submitServiceSubscription) {
            this.submitServiceSubscription.unsubscribe();
        }
        if (this.sendBkReviewServiceSubscription) {
            this.sendBkReviewServiceSubscription.unsubscribe();
        }
        if (this.supplierDetailsServiceSubscription) {
            this.supplierDetailsServiceSubscription.unsubscribe();
        }
        if (this.exchangeRateServiceSubscription) {
            this.exchangeRateServiceSubscription.unsubscribe();
        }
        this.nextSubscription.unsubscribe();
    }

    reset(): void {
        Object.keys(this.serviceDocument.dataProfile.profileForm.controls).forEach(control => {
            this.serviceDocument.dataProfile.profileForm.controls[control].setValue(this.serviceDocument.dataProfile.dataModel[control]);
        });
        this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
    }

    close(): void {
        this.messageResult.message = `<div>All your changes will be lost if you don't save them.</div>
                                    <div>Click on “Save” to Keep the changes.</div>
                                    <div>Click on “Don't save” to proceed further without saving changes.</div>
                                    <div>Click on “Cancel” to stay on the current page.</div>`;
        if ((this.serviceDocument.dataProfile.profileForm && this.serviceDocument.dataProfile.profileForm.dirty)
            || (this.serviceDocument.dataProfile.profileForm.dirty && !this.serviceDocument.dataProfile.profileForm.valid)) {
            this.dialogRef = this.dialogService.openMessageDialog("Warning", this.messageResult
                , [new RbButton("", "Cancel", "alertCancel"), new RbButton("", "Don't Save", "alertdontsave")
                    , new RbButton("", "Save", "alertSave")], "37%", "", "iconsdontsave retailsNavIcons", "dontsaveAlert", "dontsaveAlertTitle");
            this.dialogRef.componentInstance.click.subscribe(
                btnName => {
                    if (btnName === "Cancel") {
                        this.dialogRef.close();
                    }
                    if (btnName === "Don't Save") {
                        this.serviceDocument.dataProfile.profileForm.markAsUntouched();
                        this.canNavigate = true;
                        this.dialogRef.close();
                        this.router.navigate([this.sharedService.previousUrl], { skipLocationChange: true });
                    }
                    if (btnName === "Save") {
                        this.dialogRef.close();
                        this.save(true);
                        this.router.navigate([this.sharedService.previousUrl], { skipLocationChange: true });
                    }
                });
        } else {
            this.canNavigate = true;
            //this.router.navigate([this.sharedService.previousUrl], { skipLocationChange: true });
          this.router.navigate(["/Transfer/List"], { skipLocationChange: true });
        }
    }

    changeDept(event: any): void {
        this.serviceDocument.dataProfile.profileForm.controls["buyer"].setValue(event.options.buyerId);
        this.serviceDocument.dataProfile.profileForm.controls["buyerName"].setValue(event.options.buyerName);
    }

    nextTab(): void {
        if (this.primaryId) {
          this.router.navigate(["/Transfer/New/TransferDetails/" + this.primaryId], { skipLocationChange: true });
        }
  }
  
  changeContextType($event: any): void {   
    this.serviceDocument.dataProfile.profileForm.patchValue({
      contextValue: $event.options.code
    });
  }
  ChangeFromLocatoins(event: any): void {
    if (event.options.code === "S") {
      this.fromLocationData = this.fromLocationsFileredList.filter(item => item.locType === "S")
    }
    else if (event.options.code === "W") {
      this.fromLocationData = this.fromLocationsFileredList.filter(item => item.locType === "W")
    }
  }
  ChangeToLocatoinsData(event: any): void {
    if (event.options.code === "S") {
      this.toLocationData = this.toLocationsFileredList.filter(item => item.locType === "S")
      this.validateLocatoins(event);
    }
    else if (event.options.code === "W") {
      this.toLocationData = this.toLocationsFileredList.filter(item => item.locType === "W")
      this.validateLocatoins(event);
    }
  }
  validateLocatoins(event: any): void {
    let form: any = this.serviceDocument.dataProfile.profileForm;
    if (form.controls["fromLocType"].value === "S" && form.controls["toLocType"].value === "S" && form.controls["fromLoc"].value != null && form.controls["toLoc"].value != null && form.controls["fromLoc"].value === form.controls["toLoc"].value) {
      this.sharedService.errorForm("Please choose different locations for store type");     
    }
    else if (form.controls["fromLocType"].value === "W" && form.controls["toLocType"].value === "W" && form.controls["fromLoc"].value != null && form.controls["toLoc"].value != null && form.controls["fromLoc"].value === form.controls["toLoc"].value) {
      this.sharedService.errorForm("Please choose different locations for ware house type");        
    }
  }
}
