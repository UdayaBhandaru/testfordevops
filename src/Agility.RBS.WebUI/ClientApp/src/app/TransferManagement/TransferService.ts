import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { TransferSearchModel } from "./List/TransferSearchModel";
import { SharedService } from "../Common/SharedService";

@Injectable()
export class TransferService {
  serviceDocument: ServiceDocument<TransferSearchModel> = new ServiceDocument<TransferSearchModel>();

  constructor(private sharedService: SharedService) { }

  newModel(model: TransferSearchModel): ServiceDocument<TransferSearchModel> {
    return this.serviceDocument.newModel(model);
  }

  search(additionalParams: any): Observable<ServiceDocument<TransferSearchModel>> {
    return this.serviceDocument.search("/api/TransferSearch/Search", true);
  }

  list(): Observable<ServiceDocument<TransferSearchModel>> {
    return this.serviceDocument.list("/api/TransferSearch/List");
  }
}
