import { Injectable } from "@angular/core";
import { Resolve } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { TransferSearchModel } from "./List/TransferSearchModel";
import { TransferService } from "./TransferService";

@Injectable()
export class TransferListResolver implements Resolve<ServiceDocument<TransferSearchModel>> {
  constructor(private service: TransferService) { }
  resolve(): Observable<ServiceDocument<TransferSearchModel>> {
        return this.service.list();
    }
}
