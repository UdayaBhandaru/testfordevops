import { Component, ViewChild, OnInit, OnDestroy, ChangeDetectorRef } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MatDialogRef } from "@angular/material";
import { Subscription } from "rxjs";
import { CommonService, ServiceDocument, FxContext, IdentityMenuModel, MessageResult } from "@agility/frameworkcore";
import { SharedService } from "../Common/SharedService";
import { TransferSharedService } from "./TransferSharedService";
import { DocumentsConstants } from "../Common/DocumentsConstants";
import { InboxService } from "../inbox/InboxService";
import { WorkflowHistoryModel } from "../inbox/WorkflowHistoryModel";
import { WorkflowHistoryPopupComponent } from "../inbox/WorkflowHistoryPopupComponent";
import { LoaderService } from "../Common/LoaderService";
import { RbPrintPopupComponent } from "../Documents/RbPrintPopupComponent";
import { CommonPrintModel } from "../Common/CommonPrintModel";
import { ShipmentService } from "../SupplyChainManagement/ShipmentService";
import { OrderShipmentRequestModel } from "../SupplyChainManagement/OrderShipmentRequestModel";
import { OrderShipmentResponseModel } from "../SupplyChainManagement/OrderShipmentResponseModel";
import { ShipmentPopupComponent } from "../SupplyChainManagement/ShipmentPopupComponent";
import { RbMessageDialogComponent } from "../Common/controls/RbMessageDialogComponent";


@Component({
  selector: "transfer",
  templateUrl: "./TransferComponent.html",
  styleUrls: ['./TransferComponent.scss']
})
export class TransferComponent implements OnInit, OnDestroy {
  dialogRef: MatDialogRef<RbMessageDialogComponent>;
  public menus: IdentityMenuModel[] = [];
  public subMenus: IdentityMenuModel[] = [];
  selected: string;
  childComponentName: string;
  PageTitle: string;
  public tsF_NO: number = 0;
  isWorkFlowStarted: boolean;
  currentItemWfStatus: string;
  documentsConstants: DocumentsConstants = new DocumentsConstants();
  objectName: string;
  workflowHistoryListObj: WorkflowHistoryModel[] = [];
  workflowHistoryServiceSubscription: Subscription;
  printServiceSubscription: Subscription;
  shipmentListServiceSubscription: Subscription;
  constructor(
    public fxContext: FxContext
    , public orderMgmtSharedService: TransferSharedService
    , private commonService: CommonService
    , private route: ActivatedRoute
    , private _sharedService: SharedService
    , private router: Router
    , private _inboxService: InboxService
    , private loaderService: LoaderService
    , private shipmentService: ShipmentService
    , private changeRef: ChangeDetectorRef
  ) {
  }

  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }

  save(): void {
    this.orderMgmtSharedService.announceSave(this.childComponentName);
  }

  saveAndContinue(): void {
    this.orderMgmtSharedService.announceSaveContinue(this.childComponentName);
  }

  reset(): void {
    this.orderMgmtSharedService.announceReset(this.childComponentName);
  }

  notify(component: any): void {
    this.childComponentName = component._componentName;
    this.setPageTitle();
  }

  close(): void {
    this.orderMgmtSharedService.announceClose(this.childComponentName);
  }

  ngOnInit(): void {
    this.tsF_NO = this.orderMgmtSharedService.tsF_NO ? this.orderMgmtSharedService.tsF_NO : 0;
    this.selected = "Transfer";
    //this.subMenus = this.fxContext.userProfile.menus.filter(itm => itm.groupMenu === 6);
    //this.subMenus = this.fxContext.userProfile.menus.filter(itm => itm.menuText === "Inventory Management");
    //this.subMenus = this.fxContext.userProfile.menus.filter(itm => itm.groupMenu === 1);
    //this.subMenus = this.fxContext.userProfile.menus.filter(itm => itm.menuText === "Transfer");
    this.menus = this.fxContext.userProfile.menus.filter(itm => itm.menuText === "Transfer");
    this.setPageTitle();
    this.objectName = this.documentsConstants.orderObjectName;
  }

  private setPageTitle(): void {
    this.PageTitle = `${this.childComponentName} - ADD`;
    if (this.tsF_NO && this.tsF_NO.toString() !== "0") {
      this.PageTitle = `${this.childComponentName} - EDIT`;
      this.PageTitle = `${this.PageTitle} - ${this.tsF_NO}`;
    }
    this.changeRef.detectChanges();
  }

  select(url: string): string {
    this.tsF_NO = this.orderMgmtSharedService.tsF_NO ? this.orderMgmtSharedService.tsF_NO : 0;
    return url + this.tsF_NO;
  }

  isActive(item: string): boolean {
    return this.selected === item;
  }
  viewWorkflowHistoryFromOrder($event: MouseEvent): void {
    $event.preventDefault();
    this.workflowHistoryServiceSubscription = this._inboxService.fetchWorkflowHistory(this.objectName, this.tsF_NO).subscribe(() => {

      this.workflowHistoryListObj = this._inboxService.serviceDocumentWorkflowHistory.dataProfile.dataList;

      this._sharedService.openPopupWithDataList(
        WorkflowHistoryPopupComponent,
        this.workflowHistoryListObj,
        "Inbox Workflow",
        this.objectName,
        this.tsF_NO);
    });
  }

  nextTab(): void {
    this.orderMgmtSharedService.announceNext(this.childComponentName);
  }

  previousTab(): void {
    this.orderMgmtSharedService.announcePrevious(this.childComponentName);
  }

  ngOnDestroy(): void {
    if (this.workflowHistoryServiceSubscription) {
      this.workflowHistoryServiceSubscription.unsubscribe();
    }
    if (this.printServiceSubscription) {
      this.printServiceSubscription.unsubscribe();
    }
    if (this.shipmentListServiceSubscription) {
      this.shipmentListServiceSubscription.unsubscribe();
    }
  }
}
