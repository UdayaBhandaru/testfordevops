export class TransferDetailsModel {
  tsF_NO: number;
  itemType?: string;
  item?: string;
  diffId?: string;
  itemList?: number;
  tsfType?: string;
  invStatus?: number;
  qtyType?: string;
  tsfQty: number;
  adjustType?: string;
  adjustValue: number;
  fromLocType?: string;
  fromLoc?: number;
  toLocType?: string;
  toLoc?: number;
  dept?: number;
  entityType?: string;
  deptDesc?: string;
  restockPct?: number;
  operation?: string;
  programPhase?: string;
  programMessage?: string;
  error?: string;
  unitOfTransfer: string;
  stockOnHand?: number;
  unitCost?: number;
  transferPrice?: number;
  itemDesc?: string;

  unitRetail?: number;
  tranQty?: number;
  shipQty?: number;
  recvQty?: number;

  transferDetails?: TransferDetailsModel[];
  workflowStatus?: string;
  workflowStatusDesc?: string;
  createdBy?: string;
}
