export class ItemQtyDomainModel {
  tsF_NO?: number;
  item: string;
  itemNumberType: string;
  itemDesc: string;
  unitCost?: number;
  stockOnHand?: number;
  unitOfTransfer: string;
  unitRetail?: number;
  tranQty?: number;
  shipQty?: number;
  recvQty?: number;
}
