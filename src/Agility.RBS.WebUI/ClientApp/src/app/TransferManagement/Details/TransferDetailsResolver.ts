import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { TransferDetailsModel } from "./TransferDetailsModel";
import { TransferDetailsService } from "./TransferDetailsService";

@Injectable()
export class TransferDetailsResolver implements Resolve<ServiceDocument<TransferDetailsModel>> {
    constructor(private service: TransferDetailsService) { }
  resolve(route: ActivatedRouteSnapshot): Observable<ServiceDocument<TransferDetailsModel>> {
        return this.service.list(route.params["id"]);
    }
}
