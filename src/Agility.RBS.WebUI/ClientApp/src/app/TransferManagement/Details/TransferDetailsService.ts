import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument, WorkflowAction } from "@agility/frameworkcore";
import { TransferDetailsModel } from "./TransferDetailsModel";
import { SharedService } from "../../Common/SharedService";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from '../../Common/HttpHelperService';

@Injectable()
export class TransferDetailsService {
  serviceDocument: ServiceDocument<TransferDetailsModel> = new ServiceDocument<TransferDetailsModel>();

  constructor(private sharedService: SharedService, private httpHelperService: HttpHelperService,) { }

  search(): Observable<ServiceDocument<TransferDetailsModel>> {
      return this.serviceDocument.search("/api/TransferDetails/Search");
    }

  newModel(model: TransferDetailsModel): ServiceDocument<TransferDetailsModel> {
        return this.serviceDocument.newModel(model);
    }

  save(): Observable<ServiceDocument<TransferDetailsModel>> {
      return this.serviceDocument.save("/api/TransferDetails/Save", true);
    }

  list(tsF_NO: number): Observable<ServiceDocument<TransferDetailsModel>> {
    return this.serviceDocument.open("/api/TransferDetails/List", new HttpParams().set("tsF_NO", tsF_NO.toString()));
  }

  getDataFromAPI(item: string, tsF_NO: string): Observable<any> {
    var resp: any = this.httpHelperService.get("/api/TransferDetails/ItemQtyDetailsGet", new HttpParams().set("item", item).append("tsF_NO", tsF_NO));
    return resp;
  }  
}
