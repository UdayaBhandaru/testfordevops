import { Component, OnInit, TemplateRef, ViewChild, OnDestroy, ChangeDetectorRef, AfterViewChecked } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { FormGroup, FormControl } from "@angular/forms";
import { ServiceDocument, CommonService, MessageResult, MessageType } from "@agility/frameworkcore";
import { SharedService } from "../../Common/SharedService";
import { LoaderService } from "../../Common/LoaderService";
import { MatDialogRef } from "@angular/material";
import { GridOptions, RowNode, GridCell, GridApi } from "ag-grid-community";
import { Subscription } from "rxjs";
import { TransferDetailsModel } from "./TransferDetailsModel";
import { TransferDetailsService } from "./TransferDetailsService";
import { TransferSharedService } from "../TransferSharedService";
import { GridEditComponent } from "../../Common/grid/GridEditComponent";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { LocationDomainModel } from "../../Common/DomainData/LocationDomainModel";
import { PriceZoneDomainModel } from "../../Common/DomainData/PriceZoneDomainModel";
import { CommonModel } from "../../Common/CommonModel";
import { GridDeleteComponent } from "../../Common/grid/GridDeleteComponent";
import { RbDialogService } from "../../Common/controls/RbDialogService";
import { GridAmountComponent } from "../../Common/grid/GridAmountComponent";
import { GridNumericComponent } from "../../Common/grid/GridNumericComponent";
import { GridSelectComponent } from "../../Common/grid/GridSelectComponent";
import { GridDateComponent } from "../../Common/grid/GridDateComponent";
import { GridGetItemComponent } from "../../Common/grid/GridGetItemComponent";
import { DocumentsService } from "../../Documents/DocumentsService";
import { TransferEntityDomainModel } from '../../Common/DomainData/TransferEntityDomainModel';
import { GroupDomainModel } from '../../Common/DomainData/GroupDomainModel';
import { LocationTypeDomainModel } from '../../Common/DomainData/LocationTypeDomainModel';
import { ItemQtyDomainModel } from './ItemQtyDomainModel';
import { Observable } from "rxjs";
import { RbMessageDialogComponent } from "../../Common/controls/RbMessageDialogComponent";


@Component({
  selector: "transfer-details",
  templateUrl: "./TransferDetailsComponent.html",
  styleUrls: ['./TransferDetailsComponent.scss']
})
export class TransferDetailsComponent implements OnInit, OnDestroy {
  dialogRef: MatDialogRef<RbMessageDialogComponent>;
  tsF_NO: number;
  public serviceDocument: ServiceDocument<TransferDetailsModel>;
  public gridOptions: GridOptions;
  PageTitle = "Transfer Details";
  transferType: string;
  _componentName = "TransferDetails"; // don't remove this, used as global param.
  pForm: any;
  editMode: boolean;
  componentName: any;
  columns: any[];
  public sheetName: string = "TransferDetails";
  data: TransferDetailsModel[] = [];
  dataReset: TransferDetailsModel[] = [];
  model: TransferDetailsModel;

  inventoryStatusData: DomainDetailModel[];
  domainStatusData: DomainDetailModel[];
  transferTypeData: DomainDetailModel[];
  tsfEntityData: TransferEntityDomainModel[];
  clrItemListData: DomainDetailModel[];
  transferQtyTypeData: DomainDetailModel[];
  transferPriceAdjTypesData: DomainDetailModel[];
  groupData: GroupDomainModel[];
  locationData: LocationTypeDomainModel[];
  locTypeData: DomainDetailModel[];
  uomTransferData: DomainDetailModel[];
  localizationData: any;

  deletedRows: any[] = [];
  companyCount: number;
  node: any;
  saveContinueSubscribtion: Subscription;
  resetSubscribtion: Subscription;
  saveSubscribtion: Subscription;
  closeSubscribtion: Subscription;
  routeServiceSubscription: Subscription;
  nextSubscription: Subscription;
  previousSubscription: Subscription;
  itemDetailsServiceSubscription: Subscription;
  public messageResult: MessageResult = new MessageResult();
  canNavigate: boolean = false;
  addText: string = "+ Add To Grid";
  orginalValue: string = "";
  apiUrl: string;
  gridApi: GridApi;
  unitOfTransfer: string;

  constructor(private route: ActivatedRoute, private service: TransferDetailsService, private sharedService: SharedService,
    private transferSharedService: TransferSharedService, private commonService: CommonService, public router: Router,
    public dialogService: RbDialogService, private changeRef: ChangeDetectorRef, private docService: DocumentsService) {
    this.saveSubscribtion = transferSharedService.saveAnnounced$.subscribe((response: any) => {
      if (response === this._componentName) {
        this.save(true);
      }
    });
    this.saveContinueSubscribtion = transferSharedService.saveContinueAnnounced$.subscribe((response: string) => {
      if (response === this._componentName) {
        this.save(false);
      }
    });
    this.resetSubscribtion = transferSharedService.resetAnnounced$.subscribe((response: string) => {
      if (response === this._componentName) {
        this.reset();
      }
    });
    this.closeSubscribtion = transferSharedService.closeAnnounced$.subscribe((response: string) => {
      if (response === this._componentName) {
        this.close();
      }
    });
    this.nextSubscription = transferSharedService.nextAnnounced$.subscribe((response: string) => {
      if (response === this._componentName) {
        this.previousTab();
      }
    });
    this.previousSubscription = transferSharedService.previousAnnounced$.subscribe((response: string) => {
      if (response === this._componentName) {
        this.previousTab();
      }
    });

  }

  ngOnInit(): void {
    this.routeServiceSubscription = this.route.params.subscribe((resp: any) => {
      this.tsF_NO = +resp["id"];
    });
    this.componentName = this;
    this.columns = [
      { headerName: "", field: "", checkboxSelection: true, width: 40 },
      { headerName: "item", field: "item", tooltipField: "item" },
      { headerName: "item Desc", field: "itemDesc", tooltipField: "itemDesc" },
      { headerName: "From Loc UnitCost", field: "unitCost", tooltipField: "unitCost" },
      { headerName: "From Loc UnitRetail", field: "unitRetail", tooltipField: "unitRetail" },
      { headerName: "Transfer Price", field: "transferPrice", tooltipField: "transferPrice" },
      { headerName: "Transfer Qty", field: "tranQty", tooltipField: "tranQty" },
      { headerName: "Ship Qty", field: "shipQty", tooltipField: "shipQty" },
      { headerName: "Recived Qty", field: "recvQty", tooltipField: "recvQty" },
      { headerName: "Unit of Transfer", field: "unitOfTransfer", tooltipField: "unitOfTransfer" },
      //{
      //  headerName: "Actions", field: "Actions", cellRendererFramework: GridDeleteComponent, suppressCellSelection: true, width: 100
      //},
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 150
        , cellRendererParams: { restrictEditIcon: true }
      }
    ];
    this.gridOptions = {
      onRowSelected: this.onRowSelected,
      onGridReady: (params: any) => {
        this.gridApi = params.api;
      },
      context: {
        componentParent: this
      }
    };

    this.locTypeData = Object.assign([], this.service.serviceDocument.domainData["locationsType"]);
    this.locationData = Object.assign([], this.service.serviceDocument.domainData["locType"]);
    this.inventoryStatusData = Object.assign([], this.service.serviceDocument.domainData["inventoryType"]);
    this.groupData = Object.assign([], this.service.serviceDocument.domainData["group"]);
    this.clrItemListData = Object.assign([], this.service.serviceDocument.domainData["clrItemList"]);
    this.transferQtyTypeData = Object.assign([], this.service.serviceDocument.domainData["transferQtyType"]);
    this.transferTypeData = Object.assign([], this.service.serviceDocument.domainData["transferType"]);
    this.transferPriceAdjTypesData = Object.assign([], this.service.serviceDocument.domainData["transferPriceAdjTypes"]);    
    this.uomTransferData = Object.assign([], this.service.serviceDocument.domainData["uomTransfer"]);
    this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
    this.serviceDocument = this.service.serviceDocument;
   

    if (this.service.serviceDocument.dataProfile.dataList) {
      Object.assign(this.data, this.service.serviceDocument.dataProfile.dataList);
      Object.assign(this.dataReset, this.service.serviceDocument.dataProfile.dataList);
      this.orginalValue = JSON.stringify(this.dataReset);
    }
    this.newModel();
    this.serviceDocument = this.service.serviceDocument;
    this.companyCount = this.data.length;
  }

  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }

  add($event: MouseEvent): void {
    this.pForm = this.serviceDocument.dataProfile.profileForm;
    $event.preventDefault();
    if (this.pForm.valid) {
      this.unitOfTransfer = this.uomTransferData.filter(itm => itm.code === this.pForm.controls["unitOfTransfer"].value)[0].code.toString();
      let itmCompanyData: TransferDetailsModel[] = this.data ? this.data.filter(itm => itm.item === this.pForm.controls["item"].value) : null;
      if (!itmCompanyData || itmCompanyData.length !== 1) {
        let emptyObject: any = {
          item: this.pForm.controls["item"].value, itemDesc: this.pForm.controls["itemDesc"].value, unitCost: this.pForm.controls["unitCost"].value,
          unitOfTransfer: this.unitOfTransfer, tsF_NO: this.tsF_NO, "operation": "I", fromLoc: this.transferSharedService.fromLoc,
          fromLocType: this.transferSharedService.fromLocType, toLoc: this.transferSharedService.toLoc, itemList: this.pForm.controls["itemList"].value,
          toLocType: this.transferSharedService.toLocType, entityType: "A", transferPrice: this.pForm.controls["transferPrice"].value, "transferDetails": []
        };
        if (!this.data) {
          this.data = [];
        }
        this.service.serviceDocument.dataProfile.profileForm.controls["tsF_NO"].setValue(this.tsF_NO);
        this.service.serviceDocument.dataProfile.profileForm.controls["item"].setValue(this.pForm.controls["item"].value);
        this.service.serviceDocument.dataProfile.profileForm.controls["fromLocType"].setValue(this.transferSharedService.fromLocType);
        this.service.serviceDocument.dataProfile.profileForm.controls["fromLoc"].setValue(this.transferSharedService.fromLoc);
        this.service.serviceDocument.dataProfile.profileForm.controls["toLocType"].setValue(this.transferSharedService.toLocType);
        this.service.serviceDocument.dataProfile.profileForm.controls["toLoc"].setValue(this.transferSharedService.toLoc);
        this.service.serviceDocument.dataProfile.profileForm.controls["entityType"].setValue("A");
        this.service.serviceDocument.dataProfile.profileForm.controls["transferPrice"].setValue(this.pForm.controls["transferPrice"].value);
        this.service.serviceDocument.dataProfile.profileForm.controls["unitOfTransfer"].setValue(this.unitOfTransfer);
        this.service.serviceDocument.dataProfile.profileForm.controls["unitCost"].setValue(this.pForm.controls["unitCost"].value);
        this.service.serviceDocument.dataProfile.profileForm.controls["itemList"].setValue(this.pForm.controls["itemList"].value);
        this.service.serviceDocument.dataProfile.profileForm.controls["itemType"].setValue("I");
        this.service.serviceDocument.dataProfile.profileForm.controls["tsfType"].setValue(this.transferSharedService.tsfType);        
        this.service.serviceDocument.dataProfile.profileForm.controls["qtyType"].setValue(this.pForm.controls["qtyType"].value);
        this.service.serviceDocument.dataProfile.profileForm.controls["operation"].setValue("I");
        this.data.push(Object.assign(emptyObject, this.service.serviceDocument.dataProfile.profileForm.value));
        this.resetControls();
        if (this.gridApi) {
          this.gridApi.setRowData(this.data);
        }
      } else if (itmCompanyData) {
        if (this.node && this.node.selected) {
          Object.assign(itmCompanyData[0], this.service.serviceDocument.dataProfile.profileForm.value);
          if (itmCompanyData[0].item) {
            itmCompanyData[0].operation = "U",
            itmCompanyData[0].entityType = "A";
          }
          this.node.setSelected(false);
        } else {
          this.sharedService.errorForm(this.localizationData.itemCompany.itemcompanycheck);
        }
      }
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  onRowSelected(event: any): void {
    let context: any = event.context.componentParent;
    if (event.node.selected) {
      context.serviceDocument.dataProfile.profileForm = context.commonService.getFormGroup(event.data, 2);
      context.node = event.node;
      context.addText = " Update";
      if (event.data.operation === "U") {
        context.editMode = true;
      }      
    } else if (event.context.componentParent.node !== event.node) {
    } else {
      context.addText = "+ Add To Grid";
      context.newModel();
      context.serviceDocument = context.service.serviceDocument;
      context.locations = [];
      context.editMode = false;
    }
  }

  open(cell: any): void {
    this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, cell.data);
    this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    this.serviceDocument = this.service.serviceDocument;
  }

  save(saveOnly: boolean): void {
    this.saveSubscription(saveOnly);
  }

  saveAndContinue(): void {
    this.save(false);
  }

  saveSubscription(saveOnly: boolean): void {
    let obj: any = this;
    if (this.data.length > 0 || this.deletedRows.length > 0) {
      this.transferSharedService.save(obj, saveOnly, "transferDetails", "/Transfer/New/TransferDetails/"
        , `${"Transfer Detail Saved Successfully"} - Transfer ID ${this.tsF_NO}`, this.tsF_NO);
    } else {
      this.sharedService.errorForm("Atleast one Tranfer Detail is required.");
    }
  }

  delete(cell: any): void {
    this.transferSharedService.deleteRecord(this.data, this.deletedRows, this.gridApi, cell.rowIndex);
  }

  resetControls(): void {
    this.newModel();
    this.serviceDocument = this.service.serviceDocument;
  }
 
  newModel(): void {
    this.model = {
      tsF_NO: null,
      itemType: "I",
      item: null,
      diffId: null,
      itemList: null,
      tsfType: null,
      invStatus: null,
      qtyType: null,
      tsfQty: null,
      adjustType: null,
      adjustValue: null,
      fromLocType: null,
      fromLoc: null,
      toLocType: null,
      toLoc: null,
      dept: null,
      deptDesc: null,
      entityType: null,
      restockPct: null,
      operation: null,
      programPhase: null,
      programMessage: null,
      error: null,
      transferDetails: [],
      unitOfTransfer: null,
      stockOnHand: null,
      unitCost: null,
      transferPrice: null,
      itemDesc: null,
      workflowStatus: null,
      workflowStatusDesc: null,
       createdBy: null
    };
    this.service.newModel(this.model);
  } 

  previousTab(): void {
    this.router.navigate(["/Transfer/New/TransferHead/" + this.tsF_NO], { skipLocationChange: true });
  }
  changItemInfo(event: any): void {    
    this.itemDetailsServiceSubscription = this.service.getDataFromAPI(event, this.tsF_NO.toString()).subscribe((response: ItemQtyDomainModel[]) => {
      if (response && response.length > 0) {
        this.serviceDocument.dataProfile.profileForm.patchValue({
          stockOnHand: response[0].stockOnHand,
          itemDesc: response[0].itemDesc,
          unitOfTransfer: response[0].unitOfTransfer,        
          unitCost: response[0].unitCost

        });
      }
    });
  }

  reset(): void {
    this.data = [];
    this.resetControls();
    Object.assign(this.data, this.dataReset);
  }

  close(): void {   
    this.router.navigate(["/Transfer/List"], { skipLocationChange: true });
  }
  changeCost(event: any): void {
    if (event.options.code === "DA") {

      this.serviceDocument.dataProfile.profileForm.controls["transferPrice"].setValue(this.serviceDocument.dataProfile.profileForm.controls["unitCost"].value - this.serviceDocument.dataProfile.profileForm.controls["adjustValue"].value)

    } else if (event.options.code === "S") {
      this.serviceDocument.dataProfile.profileForm.controls["transferPrice"].setValue(this.serviceDocument.dataProfile.profileForm.controls["adjustValue"].value)
    }
    else if (event.options.code === "DP") {

    }
  }

  ngOnDestroy(): void {
    this.saveSubscribtion.unsubscribe();
    this.saveContinueSubscribtion.unsubscribe();
    this.resetSubscribtion.unsubscribe();
    this.closeSubscribtion.unsubscribe();
    this.previousSubscription.unsubscribe();
    this.nextSubscription.unsubscribe();
    this.routeServiceSubscription.unsubscribe();
    this.locationData.length = 0;
    this.data.length = 0;
    this.dataReset.length = 0;
    if (this.itemDetailsServiceSubscription) {
      this.itemDetailsServiceSubscription.unsubscribe();
    }
  }

}
