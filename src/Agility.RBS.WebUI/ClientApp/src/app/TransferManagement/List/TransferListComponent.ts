import { Component, OnInit, ViewChild, OnDestroy, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { Subscription } from "rxjs";
import { GridEditComponent } from "../../Common/grid/GridEditComponent";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { InfoComponent } from "../../Common/InfoComponent";
import { SharedService } from "../../Common/SharedService";
import { GridExportModel } from "../../Common/grid/GridExportModel";
import { GridDateComponent } from "../../Common/grid/GridDateComponent";
import { TransferService } from "../TransferService";
import { SupplierDomainModel } from "../../Common/DomainData/SupplierDomainModel";
import { SearchComponent } from "../../Common/SearchComponent";
import { TransferSharedService } from "../TransferSharedService";
import { TransferSearchModel } from "../List/TransferSearchModel";
import { CommonModel } from "../../Common/CommonModel";
import { GridOptions, GridReadyEvent, IServerSideDatasource, IServerSideGetRowsParams, ColDef } from "ag-grid-community";
import { WorkflowStatusDomainModel } from "../../Common/DomainData/WorkflowStatusDomainModel";
import { GroupDomainModel } from "../../Common/DomainData/GroupDomainModel";

@Component({
  selector: "transfer-list",
  templateUrl: "./TransferListComponent.html"
})

export class TransferListComponent implements OnInit {
  public sheetName: string = "Transfer List";
  PageTitle = "Transfer List";
  redirectUrl = "Transfer/New/TransferHead/";
  redirectDetailUrl = "Transfer/New/TransferDetails/";
  componentName: any;
  additionalSearchParams: any;
  loadedPage: boolean;
  searchServiceSubscription: Subscription;
  workflowStatusList: WorkflowStatusDomainModel[];
  groupData: GroupDomainModel[];
  supplierData: SupplierDomainModel[];
  locationData: CommonModel[];
  dateTypeData: DomainDetailModel[]; orderTypeData: DomainDetailModel[]; orderStatusData: DomainDetailModel[];
  transferTypeData: DomainDetailModel[];
  serviceDocument: ServiceDocument<TransferSearchModel>;
  columns: any[];
  model: TransferSearchModel = {
    tsF_NO: null,
    TsfParentNo: null,
    fromLocType: null,
    fromLoc: null,
    toLocType: null,
    toLoc: null,
    ExpDcDate: null,
    dept: null,
    inventoryType: null,
    tsfType: null,
    freightCode: null,
    RoutingCode: null,
    approvalDate: null,
    approvalId: null,
    createDate: null,
    createId: null,
    deliveryDate: null,
    closeDate: null,
    extRefNo: null,
    replTsfApproveInd: null,
    commentDesc: null,
    expDcEowDate: null,
    mrtNo: null,
    notAfterDate: null,
    contextType: null,
    contextValue: null,
    restockPct: null,
    organizationId: null,
    modifiedBy: null,
    deletedInd: null,
    modifiedDate: null,
    createdDate: null,
    createdBy: null,
    workflowInstance: null,
    Operation: null,
    ProgramPhase: null,
    ProgramMessage: null,
    Error: null,
    fileCount: null,
    fromLocDesc: null,
    toLocDesc: null,
    tsfTypeDesc: null,
    statusType: null,
    statusDesc: null
  };

  isSearchParamGiven: boolean = false;
  searchProps: string[] = ["tsF_NO", "tsfType", "fromLoc", "toLoc"];
  @ViewChild("searchPanel") searchPanel: SearchComponent;
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  gridOptions: GridOptions;

  constructor(public service: TransferService, private sharedService: SharedService, private router: Router
    , private priceMgmtSharedService: TransferSharedService, private changeRef: ChangeDetectorRef) {
  }

  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { colId: "tsF_NO", headerName: "Transfer No", field: "tsF_NO", tooltipField: "tsF_NO", width: 80, sort: "desc" },
      { colId: "tsfTypeDesc", headerName: "Transfer Type", field: "tsfTypeDesc", tooltipField: "tsfTypeDesc", width: 140 },
      { colId: "fromLocDesc", headerName: "From Location", field: "fromLocDesc", tooltipField: "fromLocDesc", width: 120 },
      { colId: "toLocDesc", headerName: "To Location", field: "toLocDesc", tooltipField: "toLocDesc", width: 140 },
      { colId: "statusDesc", headerName: "WORKFLOW STATUS", field: "statusDesc", tooltipField: "statusDesc", width: 100 },
      { colId: "statusType", headerName: "Transfer Status", field: "statusType", tooltipField: "statusType", width: 140 },

      //{ colId: "DEPT_DESC", headerName: "Department", field: "deptDesc", tooltipField: "deptDesc", width: 140 },
      //{
      //    colId: "CREATED_DATE", headerName: "Created Date", field: "createdDate", tooltipField: "createdDate"
      //    , cellRendererFramework: GridDateComponent, width: 130
      //},
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 100
      }
    ];

    this.transferTypeData = Object.assign([], this.service.serviceDocument.domainData["transferType"]);

    this.supplierData = Object.assign([], this.service.serviceDocument.domainData["supplier"]);
    this.locationData = Object.assign([], this.service.serviceDocument.domainData["location"]);
    this.locationData.forEach(x => x.id = +x.id);
    this.dateTypeData = this.service.serviceDocument.domainData["dateType"];
    this.groupData = Object.assign([], this.service.serviceDocument.domainData["group"]);
    this.orderTypeData = Object.assign([], this.service.serviceDocument.domainData["orderType"]);
    this.workflowStatusList = this.service.serviceDocument.domainData["workflowStatusList"];

    delete this.sharedService.domainData;
    this.sharedService.domainData = {
      location: this.locationData, dateType: this.dateTypeData, status: this.orderStatusData, orderType: this.orderTypeData
      , department: this.groupData, supplier: this.supplierData
    };

    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }

    this.serviceDocument = this.service.serviceDocument;
    this.searchPanel.dateColumns = ["startDate", "endDate"];
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
      this.sharedService.previousUrl = "../Transfer/List";
      this.priceMgmtSharedService.tsF_NO = cell.data.tsF_NO;
      this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl + cell.data.tsF_NO, this.service.serviceDocument.dataProfile.dataList);
    }
  }

  add(): void {
    this.router.navigate(["/" + this.redirectUrl + "0"], { skipLocationChange: true });
    this.sharedService.previousUrl = "../Transfer/List";
  }

  ngOnDestroy(): void {
    if (this.searchServiceSubscription) {
      this.searchServiceSubscription.unsubscribe();
    }
  }
}
