import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FrameworkCoreModule, CommonService } from "@agility/frameworkcore";
import { AppRoutingModule } from "./AppRoutingModule";
import { AppComponent } from "./AppComponent";
import { AccountModule } from "./account/AccountModule";
import { SharedService } from "./Common/SharedService";
import { MatMenuModule, MatProgressSpinnerModule, MatSidenavModule } from "@angular/material";
import { LoaderService } from "./Common/LoaderService";
import { BlankComponent } from "./Common/BlankComponent";
import { NgIdleKeepaliveModule } from "@ng-idle/keepalive"; // this includes the core NgIdleModule but includes keepalive providers for easy wireup
import { KeepAliveSessionService } from "./Common/KeepAliveSessionService";
import { SidenavComponent } from "./Common/SidenavComponent";
import { HttpHelperService } from "./Common/HttpHelperService";
import { RbDialogService } from "./Common/controls/RbDialogService";
import RbHeadGuard from "./guards/RbHeadGuard";
import RbDetailsGuard from "./guards/RbDetailsGuard";
import { RbsSharedModule } from "./RbsSharedModule";
import { MatVideoModule } from 'mat-video';

@NgModule({
  declarations: [
    AppComponent,
    BlankComponent,
    SidenavComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    AccountModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    FrameworkCoreModule.forRoot(),
    MatMenuModule,
    MatProgressSpinnerModule,
    NgIdleKeepaliveModule.forRoot(),
    MatSidenavModule,
    RbsSharedModule,
    MatVideoModule
  ],
  providers: [
    CommonService,
    SharedService,
    LoaderService,
    KeepAliveSessionService,
    HttpHelperService,
    RbDialogService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
