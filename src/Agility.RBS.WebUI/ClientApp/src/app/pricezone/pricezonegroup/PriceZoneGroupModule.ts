import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { ComanyListComponent } from "./PriceZoneGroupListComponent";
import { PriceZoneGroupService } from "./PriceZoneGroupService";
import { PriceZoneGroupRoutingModule } from "./PriceZoneGroupRoutingModule";
import { PriceZoneGroupComponent } from "./PriceZoneGroupComponent";
import { PriceZoneGroupListResolver, PriceZoneGroupResolver } from "./PriceZoneGroupResolver";
import { RbsSharedModule } from "../../RbsSharedModule";

@NgModule({
    imports: [
        FrameworkCoreModule,
        PriceZoneGroupRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        ComanyListComponent,
        PriceZoneGroupComponent,
    ],
    providers: [
        PriceZoneGroupService,
        PriceZoneGroupListResolver,
        PriceZoneGroupResolver,
    ]
})
export class PriceZoneGroupModule {
    constructor() {
        LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
    }
}
