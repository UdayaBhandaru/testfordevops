import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { PriceZoneGroupModel } from "./PriceZoneGroupModel";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";

@Injectable()
export class PriceZoneGroupService {
    serviceDocument: ServiceDocument<PriceZoneGroupModel> = new ServiceDocument<PriceZoneGroupModel>();
    constructor(private httpHelperService: HttpHelperService) { }

    newModel(model: PriceZoneGroupModel): ServiceDocument<PriceZoneGroupModel> {
        return this.serviceDocument.newModel(model);
    }

    search(): Observable<ServiceDocument<PriceZoneGroupModel>> {
      return this.serviceDocument.search("/api/PriceZoneGroup/Search");
    }

    save(): Observable<ServiceDocument<PriceZoneGroupModel>> {
      return this.serviceDocument.save("/api/PriceZoneGroup/Save", true);
    }

    list(): Observable<ServiceDocument<PriceZoneGroupModel>> {
      return this.serviceDocument.list("/api/PriceZoneGroup/List");
    }

    addEdit(): Observable<ServiceDocument<PriceZoneGroupModel>> {
      return this.serviceDocument.list("/api/PriceZoneGroup/New");
    }

  isExistingPriceZoneGroup(description: string): Observable<boolean> {
    return this.httpHelperService.get("/api/PriceZoneGroup/IsExistingPriceZoneGroup", new HttpParams().set("description", description));
    }
}
