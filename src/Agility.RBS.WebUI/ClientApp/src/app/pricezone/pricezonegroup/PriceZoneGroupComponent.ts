import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { SharedService } from "../../Common/SharedService";
import { PriceZoneGroupModel } from "./PriceZoneGroupModel";
import { PriceZoneGroupService } from "./PriceZoneGroupService";
import { StateDomainModel } from '../../Common/DomainData/StateDomainModel';
import { CountryDomainModel } from '../../Common/DomainData/CountryDomainModel';
import { CurrencyDomainModel } from '../../Common/DomainData/CurrencyDomainModel';
import { CostLevelModel } from '../../Common/DomainData/CostLevelModel';
import { PriceLevelModel } from '../../Common/DomainData/PriceLevelModel';

@Component({
  selector: "PriceZoneGroup",
  templateUrl: "./PriceZoneGroupComponent.html"
})

export class PriceZoneGroupComponent implements OnInit {
  PageTitle = "Price Zone Group";
  redirectUrl = "PriceZoneGroup";
    serviceDocument: ServiceDocument<PriceZoneGroupModel>;
    editMode: boolean = false;
    editModel: any;
    localizationData: any;  
    model: PriceZoneGroupModel;  
  countryDomainData: CountryDomainModel[];
  cityDomainData: StateDomainModel[];
  currencyData: CurrencyDomainModel[];
  priceLevelModel: PriceLevelModel[];

    constructor(private service: PriceZoneGroupService, private sharedService: SharedService, private router: Router
        , private ref: ChangeDetectorRef) {
    }

    ngOnInit(): void {
      this.countryDomainData = Object.assign([], this.service.serviceDocument.domainData["country"]);

      this.currencyData = Object.assign([], this.service.serviceDocument.domainData["currency"]);
      this.priceLevelModel = Object.assign([], this.service.serviceDocument.domainData["priceLevel"]);

        this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
        this.bind();
    }

    bind(): void {
        delete this.editModel;
        if (this.sharedService.editObject) {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);         
          Object.assign(this.service.serviceDocument.dataProfile.dataModel, { operation: "U"});
            this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            delete this.sharedService.editObject;
            this.editMode = true;

        } else {            
          this.model = { zoneGroupId: null, priceLevel: null, priceLevelDesc: null, description: null, operation: "I"};
           
            this.service.serviceDocument.dataProfile.dataModel = this.model;
            this.service.newModel(this.model);
        }
        this.serviceDocument = this.service.serviceDocument;
    }

    save(saveOnly: boolean): void {
        if (this.serviceDocument.dataProfile.profileForm.valid) {
            this.saveSubscription(saveOnly);
        } else {
            this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
        }
    }

    saveSubscription(saveOnly: boolean): void {
        this.checkPriceZoneGroup(saveOnly);
  }

    savePriceZoneGroup(saveOnly: boolean): void {
        this.service.save().subscribe(() => {
            if (this.service.serviceDocument.result.type === MessageType.success) {
                this.sharedService.saveForm("PriceZoneGroup saved successfully.").subscribe(() => {
                    if (saveOnly) {
                        this.router.navigate(["/" + this.redirectUrl + "/List"], { skipLocationChange: true });
                    } else {
                        if (!this.editMode) {
                          this.model = { zoneGroupId: null, priceLevel: null, priceLevelDesc: null, description:null, operation: "I" };                            
                            this.service.newModel(this.model);
                        } else {
                            Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
                            this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.service.serviceDocument.dataProfile.dataModel);
                            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
                        }
                    }
                }, (error: string) => { console.log(error); });
            } else {
                this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
            }
        }, (error: string) => { console.log(error); });
    }

    checkPriceZoneGroup(saveOnly: boolean): void {
        if (!this.editMode) {
          this.service.isExistingPriceZoneGroup(this.serviceDocument.dataProfile.profileForm.controls["description"].value).subscribe((response: any) => {
                if (response) {
                    this.sharedService.errorForm("PriceZoneGroup already exisit.");
                } else {
                    this.savePriceZoneGroup(saveOnly);
                }
            });
        } else {
            this.savePriceZoneGroup(saveOnly);
        }
    }

    reset(): void {
        if (!this.editMode) {
            this.model = {
              zoneGroupId: null, priceLevel: null, priceLevelDesc: null, description: null, operation: "I"
            };
           
            this.service.serviceDocument.dataProfile.dataModel = this.model;
            this.service.newModel(this.model);

        } else {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);            
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
        }
        this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
    }
}
