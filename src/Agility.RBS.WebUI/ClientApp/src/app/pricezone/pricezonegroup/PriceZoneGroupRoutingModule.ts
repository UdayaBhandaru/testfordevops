import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ComanyListComponent } from "./PriceZoneGroupListComponent";
import { PriceZoneGroupListResolver, PriceZoneGroupResolver} from "./PriceZoneGroupResolver";
import { PriceZoneGroupComponent } from "./PriceZoneGroupComponent";
const UtilityRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: ComanyListComponent,
                resolve:
                {
                    serviceDocument: PriceZoneGroupListResolver
                }
            },
            {
                path: "New",
                component: PriceZoneGroupComponent,
                resolve:
                {
                    serviceDocument: PriceZoneGroupResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(UtilityRoutes)
    ]
})
export class PriceZoneGroupRoutingModule { }
