export class PriceZoneGroupModel { 
  zoneGroupId: number;
  priceLevel?: string;
  priceLevelDesc?: string;
  description: string;
  operation?: string;
}
