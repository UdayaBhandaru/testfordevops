import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { PriceZoneGroupService } from "./PriceZoneGroupService";
import { PriceZoneGroupModel } from "./PriceZoneGroupModel";
@Injectable()
export class PriceZoneGroupListResolver implements Resolve<ServiceDocument<PriceZoneGroupModel>> {
    constructor(private service: PriceZoneGroupService) { }
    resolve(): Observable<ServiceDocument<PriceZoneGroupModel>> {
        return this.service.list();
    }
}

@Injectable()
export class PriceZoneGroupResolver implements Resolve<ServiceDocument<PriceZoneGroupModel>> {
    constructor(private service: PriceZoneGroupService) { }
    resolve(): Observable<ServiceDocument<PriceZoneGroupModel>> {
        return this.service.addEdit();
    }
}



