import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { ReceiverCostAdjustmentService } from "./ReceiverCostAdjustmentService";
import { ReceiverCostAdjustmentRoutingModule } from "./ReceiverCostAdjustmentRoutingModule";
import { ReceiverCostAdjustmentComponent } from "./ReceiverCostAdjustmentComponent";
import { ReceiverCostAdjustmentResolver } from "./ReceiverCostAdjustmentResolver";
import { AgGridSharedModule } from '../AgGridSharedModule';
import { TitleSharedModule } from '../TitleSharedModule';
import { RbsSharedModule } from '../RbsSharedModule';

@NgModule({
    imports: [
        FrameworkCoreModule,
        ReceiverCostAdjustmentRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        ReceiverCostAdjustmentComponent,
    ],
    providers: [
        ReceiverCostAdjustmentService,
        ReceiverCostAdjustmentResolver,
    ]
})
export class ReceiverCostAdjustmentModule {
    constructor() {
        LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
    }
}
