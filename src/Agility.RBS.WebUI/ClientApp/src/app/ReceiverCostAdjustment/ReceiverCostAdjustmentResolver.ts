import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { ReceiverCostAdjustmentService } from "./ReceiverCostAdjustmentService";
import { ReceiverCostAdjustmentModel } from "./ReceiverCostAdjustmentModel";

@Injectable()
export class ReceiverCostAdjustmentResolver implements Resolve<ServiceDocument<ReceiverCostAdjustmentModel>> {
    constructor(private service: ReceiverCostAdjustmentService) { }
    resolve(): Observable<ServiceDocument<ReceiverCostAdjustmentModel>> {
        return this.service.addEdit();
    }
}



