export class SupplerDetailsModel {  
  orderNo?: number;
  supplier?: number;
  supName: string;
  status: string;
  statusDesc: string;
  exchangeRate?: number;
  currencyCode: string;
  suppDesription: string;
}
