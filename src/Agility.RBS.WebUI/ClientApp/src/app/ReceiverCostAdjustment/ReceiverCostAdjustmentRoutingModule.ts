import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import {  ReceiverCostAdjustmentResolver} from "./ReceiverCostAdjustmentResolver";
import { ReceiverCostAdjustmentComponent } from "./ReceiverCostAdjustmentComponent";
const UtilityRoutes: Routes = [
    {
        path: "",
        children: [
            {
            path: "", redirectTo: "New", pathMatch: "full"
            },            
            {
                path: "New",
                component: ReceiverCostAdjustmentComponent,
                resolve:
                {
                    serviceDocument: ReceiverCostAdjustmentResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(UtilityRoutes)
    ]
})
export class ReceiverCostAdjustmentRoutingModule { }
