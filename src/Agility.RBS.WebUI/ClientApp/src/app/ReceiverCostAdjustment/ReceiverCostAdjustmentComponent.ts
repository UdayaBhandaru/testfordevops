import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ServiceDocument, MessageType, CommonService, FormMode } from "@agility/frameworkcore";
import { FormGroup, FormControl } from "@angular/forms";
import { RowNode } from 'ag-grid-community';
import { ReceiverCostAdjustmentModel } from './ReceiverCostAdjustmentModel';
import { DomainDetailModel } from '../domain/DomainDetailModel';
import { CostZoneGroupModel } from '../costzone/costzonegroup/CostZoneGroupModel';
import { ReceiverCostAdjustmentLocModel } from './ReceiverCostAdjustmentLocModel';
import { CurrencyDomainModel } from '../Common/DomainData/CurrencyDomainModel';
import { CostZoneGroupDomainModel } from '../Common/DomainData/CostZoneGroupDomainModel';
import { ReceiverCostAdjustmentService } from './ReceiverCostAdjustmentService';
import { SharedService } from '../Common/SharedService';
import { GridSelectComponent } from '../Common/grid/GridSelectComponent';
import { GridInputComponent } from '../Common/grid/GridInputComponent';
import { GridEditComponent } from '../Common/grid/GridEditComponent';
import { CommonModel } from '../Common/CommonModel';
import { DepartmentDomainModel } from '../Common/DomainData/DepartmentDomainModel';
import { ClassDomainModel } from '../Common/DomainData/ClassDomainModel';
import { SubClassDomainModel } from '../Common/DomainData/SubClassDomainModel';
import { TransactionDataModel } from '../finance/transactiondata/TransactionDataModel';
import { Subscription, Observable } from 'rxjs';
import { ItemDescpDetailsDomainModel } from '../dsdminmax/ItemDescpDetailsDomainModel';
import { LoaderService } from '../Common/LoaderService';
import { GridNumericComponent } from '../Common/grid/GridNumericComponent';
import { SupplerDetailsModel } from './SupplerDetailsModel';

@Component({
  selector: "ReceiverCostAdjustment",
  templateUrl: "./ReceiverCostAdjustmentComponent.html"
})

export class ReceiverCostAdjustmentComponent implements OnInit {
  PageTitle = "Receiver Cost Adjustment";
  serviceDocument: ServiceDocument<ReceiverCostAdjustmentModel>;
  receiverCostAdjustmentModel: ReceiverCostAdjustmentModel;
  localizationData: any;
  editMode: boolean = false;
  editModel: ReceiverCostAdjustmentModel;
  componentParentName: ReceiverCostAdjustmentComponent;
  itemDetailsServiceSubscription: Subscription;
  saveServiceSubscription: Subscription;
  dataList: ReceiverCostAdjustmentModel[] = [];
  headDisplayModel: ReceiverCostAdjustmentModel[];
  columns: any[];
  data: ReceiverCostAdjustmentLocModel[] = [];
  temp: {};
  pForm: any;
  detailModel: ReceiverCostAdjustmentLocModel;
  deletedRows: any[] = [];

  constructor(private service: ReceiverCostAdjustmentService, private sharedService: SharedService, private route: ActivatedRoute
    , private router: Router, private ref: ChangeDetectorRef, private commonService: CommonService, private loaderService: LoaderService) {
  }

  ngOnInit(): void {

    this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
    this.bind();
    this.componentParentName = this;
  }

  bind(): void {
    this.addItemDetails();
  }

  private addItemDetails() {   
    delete this.editModel;
    if (this.sharedService.editObject) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
      this.getDetails();
      this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
      Object.assign(this.service.serviceDocument.dataProfile.dataModel, { operation: "U" });
      let arr: ReceiverCostAdjustmentLocModel[] = [];
      this.service.serviceDocument.dataProfile.dataModel.receiverCostAdjustmentLocList.forEach((x) => {
        arr.push(Object.assign({}, x));
      });
      this.editModel.receiverCostAdjustmentLocList = arr;
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
      this.data = this.service.serviceDocument.dataProfile.dataModel.receiverCostAdjustmentLocList;
      delete this.sharedService.editObject;
      this.editMode = true;
    }
    else {
      this.avgCostAdjmntModel();
      this.service.serviceDocument.dataProfile.dataModel = this.receiverCostAdjustmentModel;
      this.data = this.service.serviceDocument.dataProfile.dataModel.receiverCostAdjustmentLocList;
      this.service.newModel(this.receiverCostAdjustmentModel);
    }
    this.service.serviceDocument.dataProfile.profileForm = this.commonService
      .getFormGroup(this.service.serviceDocument.dataProfile.dataModel, FormMode.Open);
    this.service.serviceDocument.dataProfile.profileForm.controls["receiverCostAdjustmentLocList"] = new FormControl();
    this.serviceDocument = this.service.serviceDocument;
  }

  reset(): void {
    if (!this.editMode) {
      this.avgCostAdjmntModel();
      this.temp = {};
      this.data = [];
      this.service.serviceDocument.dataProfile.dataModel = this.receiverCostAdjustmentModel;
      this.data = this.service.serviceDocument.dataProfile.dataModel.receiverCostAdjustmentLocList;
      this.service.newModel(this.receiverCostAdjustmentModel);
    } else {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);
      Object.assign(this.service.serviceDocument.dataProfile.dataModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
      let arr: ReceiverCostAdjustmentLocModel[] = [];
      this.editModel.receiverCostAdjustmentLocList.forEach((x) => {
        arr.push(Object.assign({}, x));
      });
      this.service.serviceDocument.dataProfile.dataModel.receiverCostAdjustmentLocList = arr;
      this.data = this.service.serviceDocument.dataProfile.dataModel.receiverCostAdjustmentLocList;
    }
    this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
  }

  save(saveOnly: boolean): void {
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      if (this.serviceDocument.dataProfile.profileForm.controls["item"].value == null )
      {
        this.sharedService.errorForm("Please select Item");
      }
      else if (this.serviceDocument.dataProfile.profileForm.controls["newAvCost"].value == null)
      {
        this.sharedService.errorForm("Please enter New Avg Cost");

      }
      else
      {
        this.saveReceiverCostAdjustment(saveOnly);
      }
    }
    else
    {
     this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }    
  }

  saveAndContinue(): void {
    this.save(false);
  }

  saveReceiverCostAdjustment(saveOnly: boolean): void {
    this.serviceDocument.dataProfile.profileForm.controls["receiverCostAdjustmentLocList"] = new FormControl();
    this.serviceDocument.dataProfile.profileForm.controls["receiverCostAdjustmentLocList"].setValue(this.data);
    this.saveServiceSubscription = this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.sharedService.saveForm("New Receive cost saved  successfully.").subscribe(() => {
          if (saveOnly) {
            this.router.navigate(["/ReceiverCostAdjustment/List"], { skipLocationChange: true });
          } else {
            if (!this.editMode) {
              this.avgCostAdjmntModel();
              this.service.serviceDocument.dataProfile.dataModel = this.receiverCostAdjustmentModel;
              this.data = this.service.serviceDocument.dataProfile.dataModel.receiverCostAdjustmentLocList;
              this.service.newModel(this.receiverCostAdjustmentModel);
            } else {
              Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
              this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.editModel);
              this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            }
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            this.serviceDocument = this.service.serviceDocument;
          }
        }, (error: string) => { console.log(error); });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
    }, (error: string) => { console.log(error); });
  }

  private avgCostAdjmntModel() {
    this.receiverCostAdjustmentModel = {
      orderNo: null, item: null, itemDesc: null, location: null, locName: null, avCost: null,
      category: null, categoryDesc: null, qtyOrdered: null, qtyReceived: null, unitCost: null, newUnitCost: null,
      stockOnHand: null, inTransitQty: null, purchaseCost: null, newAvCost: null, landedCost: null,
      newLandedCost: null, currencyCode: null, suppCostAdj: null, applyAllLoc: null,
      receiverCostAdjustmentLocList: [], operation: "I", supplier: null, supName: null, status: null, statusDesc: null, exchangeRate: null, suppDesription: null
    };
  }

  newModel(): void {
    this.pForm = this.serviceDocument.dataProfile.profileForm;
    this.detailModel = {
      orderNo: null, item: null, itemDesc: null, location: null, locName: null, avCost: null,
      category: null, categoryDesc: null, qtyOrdered: null, qtyReceived: null, unitCost: null, newUnitCost: null,
      stockOnHand: null, inTransitQty: null, purchaseCost: null, newAvCost: null, landedCost: null,
      newLandedCost: null, currencyCode: null, suppCostAdj: null, applyAllLoc: null, operation: "I",
      supplier: null, supName: null, status: null, statusDesc: null, exchangeRate: null, suppDesription: null
    };
  }

  ChangeSupplierInfo(event: any): void {
    if (this.serviceDocument.dataProfile.profileForm.controls["orderNo"].value == null) {
      this.sharedService.errorForm("Please select Order No");
    } else {
      this.itemDetailsServiceSubscription = this.service.getDataFromAPI(event).subscribe((response: SupplerDetailsModel) => {
        if (response) {
          this.serviceDocument.dataProfile.profileForm.patchValue({
            supplier: response[0].supplier, suppDesription: response[0].suppDesription, currencyCode: response[0].currencyCode
            , statusDesc: response[0].statusDesc
          });
          this.getDetails();
        }
      });
    }
  }

  getItemData(event: any): void {
    if (this.serviceDocument.dataProfile.profileForm.controls["item"].value == null) {
      this.sharedService.errorForm("Please select Item Location");
    } else {
      if (this.dataList && this.dataList.length > 0) {
        var locationType = this.dataList.filter(item => item.itemDesc == event.options.itemDesc);
        this.serviceDocument.dataProfile.profileForm.patchValue({
          qtyOrdered: locationType[0].qtyOrdered, qtyReceived: locationType[0].qtyReceived, avCost: locationType[0].avCost
          , stockOnHand: locationType[0].stockOnHand, inTransitQty: locationType[0].inTransitQty, purchaseCost: locationType[0].purchaseCost
          , purchaseCostNew: locationType[0].purchaseCostNew, landedCost: locationType[0].landedCost
          , newLandedCost: locationType[0].newLandedCost, currencyCode: locationType[0].currencyCode

        });
      }
    }
  }

  getDetails(): void {
    this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.loaderService.display(true);
      this.service.GetItemInfo(this.serviceDocument.dataProfile.profileForm.controls["orderNo"].value).subscribe(rd => {
        this.dataList = rd;
        if (this.dataList && this.dataList.length > 0) {
          this.headDisplayModel = this.dataList;
        }
        this.loaderService.display(false);
      });
    }
  }

  ngOnDestroy(): void {
    if (this.itemDetailsServiceSubscription) {
      this.itemDetailsServiceSubscription.unsubscribe();
    }
    if (this.saveServiceSubscription) {
      this.saveServiceSubscription.unsubscribe();
    }
  }
}
