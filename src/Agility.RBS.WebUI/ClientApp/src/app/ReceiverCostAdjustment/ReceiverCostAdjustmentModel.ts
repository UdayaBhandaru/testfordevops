import { ReceiverCostAdjustmentLocModel } from './ReceiverCostAdjustmentLocModel';

export class ReceiverCostAdjustmentModel {
  orderNo?:number
  item: string;
  itemDesc: string;  
  location?: number;
  locName: string;
  avCost?: number;
  category?: number;
  categoryDesc: string;
  qtyOrdered?: number;
  qtyReceived?: number;
  unitCost?: number;
  newUnitCost?: number;
  stockOnHand?: number;
  inTransitQty?: number;
  purchaseCost?: number;
  purchaseCostNew?: number;  
  newAvCost: number;
  landedCost: number;
  newLandedCost: number;
  currencyCode: string;
  suppCostAdj: string;
  applyAllLoc: string;
  receiverCostAdjustmentLocList?:
  ReceiverCostAdjustmentLocModel[];
  operation?: string;
  tableName?: string;
  supplier?: number;
  supName: string;
  status: string;
  statusDesc: string;
  exchangeRate?: number;
  suppDesription: string;
}
