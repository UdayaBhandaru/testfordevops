import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { ReceiverCostAdjustmentModel } from "./ReceiverCostAdjustmentModel";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from '../Common/HttpHelperService';
import { TransactionDataModel } from '../finance/transactiondata/TransactionDataModel';
import { SubClassDomainModel } from '../Common/DomainData/SubClassDomainModel';

@Injectable()
export class ReceiverCostAdjustmentService {
  serviceDocument: ServiceDocument<ReceiverCostAdjustmentModel> = new ServiceDocument<ReceiverCostAdjustmentModel>();
  constructor(private httpHelperService: HttpHelperService) { }

  newModel(model: ReceiverCostAdjustmentModel): ServiceDocument<ReceiverCostAdjustmentModel> {
    return this.serviceDocument.newModel(model);
  }

  search(): Observable<ServiceDocument<ReceiverCostAdjustmentModel>> {
    return this.serviceDocument.search("/api/ReceiverCostAdjustment/Search");
  }

  save(): Observable<ServiceDocument<ReceiverCostAdjustmentModel>> {
    return this.serviceDocument.save("/api/ReceiverCostAdjustment/Save", true);
  }

  list(): Observable<ServiceDocument<ReceiverCostAdjustmentModel>> {
    return this.serviceDocument.list("/api/ReceiverCostAdjustment/List");
  }

  addEdit(): Observable<ServiceDocument<ReceiverCostAdjustmentModel>> {
    return this.serviceDocument.list("/api/ReceiverCostAdjustment/New");
  }

  subclass(model: TransactionDataModel): Observable<SubClassDomainModel[]> {
    var params = new HttpParams();
    return this.httpHelperService.get(`/api/TransactionData/SubClass?deptid=${String(model.category)}&classId=${String(model.fineLine)}`, params);
  }

  isExistingCostZone(zoneGroupId: string): Observable<boolean> {
    return this.httpHelperService.get("/api/ReceiverCostAdjustment/IsExistingCostZone", new HttpParams().set("zoneGroupId", zoneGroupId));
  }

  getDataFromAPI(orderNo: number): Observable<any> {
    return this.httpHelperService.get("/api/ReceiverCostAdjustment/GetItemAtteibutes", new HttpParams().set("orderNo", orderNo.toString()));
  }

  GetItemInfo(orderNo: number): Observable<ReceiverCostAdjustmentModel[]> {
    return this.httpHelperService.get("api/ReceiverCostAdjustment/GetItemLocations", new HttpParams().set("orderNo", orderNo.toString()));
  }
}
