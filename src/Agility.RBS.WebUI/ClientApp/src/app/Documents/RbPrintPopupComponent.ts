import { Component, OnInit, OnDestroy, Inject } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { SharedService } from "../Common/SharedService";
import { DatePipe } from "@angular/common";
import { Router, ActivatedRoute } from "@angular/router";
import { ServiceDocument, CommonService, FormMode } from "@agility/frameworkcore";
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from "@angular/material";
import { RbPrintService } from "../Common/RbPrintService";
import { DocumentsConstants } from "../Common/DocumentsConstants";
import { CommonPrintModel } from "../Common/CommonPrintModel";
import { ReportSource } from "../Common/models/reports/ReportSource";
import { ReportSourceParams } from "../Common/models/reports/ReportSourceParams";
import { ReportServer } from "../Common/models/reports/ReportServer";

@Component({
  selector: "rbPrint-popup",
  templateUrl: "./RbPrintPopupComponent.html"
})

export class RbPrintPopupComponent implements OnInit {
  componentName: any;
  objectValue: string;
  objectName: string;
  iframeSource: any;
  documentsConstants: DocumentsConstants = new DocumentsConstants();
  reportSource: ReportSource;
  reportServer: ReportServer;
  viewerContainerStyle = {
    position: 'relative',
    width: '1000px',
    height: '800px',
    ['font-family']: 'ms sans serif'
  };

  constructor(public dialog: MatDialog,
    private _sharedService: SharedService, private route: ActivatedRoute, private rbPrintService: RbPrintService,
    @Inject(MAT_DIALOG_DATA) public data: {
      inputData: { headObject: CommonPrintModel, detailObject: CommonPrintModel[], supplierObject?: CommonPrintModel }
      , otherInputData: { screenId: string, screenName: string }
    }) {
  }

  ngOnInit(): void {
    this.reportSource = new ReportSource();
    this.reportSource.parameters = new ReportSourceParams();
    this.reportServer = new ReportServer();
    this.reportServer.url = "http://inhyddrshaik:83";
    this.reportServer.username = "raheem.learner@gmail.com";
    this.reportServer.password = "vali@123";
    if (this.data.otherInputData.screenName === "ORDER") {
      this.reportSource.report = "Samples/OrderPrintPage.trdp";
      this.reportSource.parameters.orderNo = + this.data.otherInputData.screenId;

    } else if (this.data.otherInputData.screenName === "COST CHANGE") {
      this.reportSource.report = "Samples/CostPrint.trdp";
      this.reportSource.parameters.costChangeId = + this.data.otherInputData.screenId;

    }
    this.componentName = this;
    this.objectValue = this.data.otherInputData.screenId;
    this.objectName = this.data.otherInputData.screenName;
    let doc: any = this.getPrintObject();
    this.iframeSource = doc.output("datauristring");
  }

  downloadPdf(): void {
    let doc: any = this.getPrintObject();
    doc.save(`${this.data.otherInputData.screenName}_${this.data.otherInputData.screenId}.pdf`);
  }

  getPrintObject(): any {
    let doc: any;
    if (this.objectName === this.documentsConstants.itemObjectName) {
      doc = this.rbPrintService.printWithMultipleDetails(
        this.data.inputData.headObject, this.data.inputData.detailObject
        , { screenId: this.data.otherInputData.screenId, screenName: this.data.otherInputData.screenName });
    } else {
      doc = this.rbPrintService.print(
        this.data.inputData.headObject, this.data.inputData.detailObject
        , { screenId: this.data.otherInputData.screenId, screenName: this.data.otherInputData.screenName }
        , this.data.inputData.supplierObject);
    }
    doc.setProperties({
      title: `${this.data.otherInputData.screenName}_${this.data.otherInputData.screenId}`
    });
    return doc;
  }

  close(): void {
    this.dialog.closeAll();
  }
}
