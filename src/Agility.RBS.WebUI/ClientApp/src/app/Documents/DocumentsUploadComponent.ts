import { Component, OnInit, OnDestroy, EventEmitter, Output, Input, ViewChild } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { SharedService } from "../Common/SharedService";
import { DatePipe } from "@angular/common";
import { Router } from "@angular/router";
import { ServiceDocument, CommonService, FormMode } from "@agility/frameworkcore";
import { MatDialogRef, MatDialog } from "@angular/material";
import { DocumentsService } from "./DocumentsService";

const fileSizeValidationMessage: string = ": attachment size should be less than 20 MB";
const fileTypeValidationMessage: string = ": File Format is not allowed";
const failedUploadMessage: string = "Document upload failed: ";
const successUploadMessage: string = "Document uploaded successfully";

@Component({
    selector: "document-upload",
  templateUrl: "./DocumentsUploadComponent.html",
  styleUrls: ['./DocumentsUploadComponent.scss']
})

export class DocumentsUploadComponent implements OnInit {
    @Output() notify: EventEmitter<string> = new EventEmitter<string>();
    @Input() objectName: string;
    @Input() objectValue: string;
    @Input() isUpdateHeader: boolean;
    @Input() objectStateID: string;
    componentName: any;
    public fileList: any[] = [];
    enableUpload: boolean;
    maxFileSize: number = 20971520; // configurable currently set as 20 MiB
    allowedFileTypes = [
        "application/vnd.openxmlformats-officedocument.presentationml.presentation",
        "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        "application/pdf",
        "application/msword",
        "application/vnd.ms-excel",
        "application/vnd.ms-powerpoint"
    ];
    validationMessage: string;
    showValidation: boolean;
    afterUploadMessage: string;
    isPostUpload: boolean;
    afterUploadMsgCss: string;
    listOfValidationFailedFiles: string[];
    listOfValidationPassedFiles: string[];
    documentSelectCtrl: any;
    fileUploadErrorMessage: string;
    fileName: string;
    fileDataAsBase64: string;

    constructor(private _service: DocumentsService, public dialog: MatDialog) {

    }

    ngOnInit(): void {
        this.enableUpload = false;
        this.isPostUpload = false;
        this.showValidation = false;
        this.listOfValidationFailedFiles = [];
        this.listOfValidationPassedFiles = [];
    }

    fileChange(event: any): void {
        this.fileUploadErrorMessage = event.ErrorMessage;
        this.isPostUpload = false;
        this.listOfValidationFailedFiles = [];
        this.listOfValidationPassedFiles = [];

        if (!this.fileUploadErrorMessage) {
            this.fileName = event.filename;
            this.fileDataAsBase64 = event.filedataasbase64;
            this.fileList = [];
            this.fileList.push({ "fileName": this.fileName, "fileData": this.fileDataAsBase64 });
            this.listOfValidationPassedFiles.push(this.fileName);
            this.fileUploadErrorMessage = null;
        } else {
            this.fileDataAsBase64 = null;
            this.listOfValidationFailedFiles.push(this.fileUploadErrorMessage);
        }
        this.showValidation = (this.listOfValidationFailedFiles.length > 0) ? true : false;
        this.enableUpload = (this.listOfValidationPassedFiles.length > 0) ? true : false;
    }

    uploadedFileReference(event: any): void {
        this.documentSelectCtrl = event.fileReference;
    }

    showFileDialog(event: any): void {
        event.preventDefault();
        this.documentSelectCtrl.nativeElement.click();
    }

    uploadDocument(event: any): void {
        event.preventDefault();
        if (this.fileList.length > 0) {
            this._service.uploadDocument(this.fileList, this.objectName, this.objectValue, this.isUpdateHeader, this.objectStateID).subscribe(
                (response: any) => {
                    this.afterUploadMessage = successUploadMessage;
                    this.isPostUpload = true;
                    if (response.message) {
                        this.handleUploadFailure(response.message);
                    } else {
                        this.afterUploadMsgCss = "success-alert-fileUpload";
                        this.notify.emit("Update List Section");
                        this.listOfValidationPassedFiles = [];
                        this.listOfValidationFailedFiles = [];
                        this.resetFileUploadControl();
                    }
                },
                (resp: any) => {
                    this.isPostUpload = true;
                    this.handleUploadFailure(resp);
                }
            );
        }
    }

    resetFileUploadControl(): void {
        delete this.documentSelectCtrl.nativeElement.value;
        this.enableUpload = false;
    }

    handleUploadFailure(data: any): void {
        this.afterUploadMessage = failedUploadMessage + data;
        this.afterUploadMsgCss = "error-alert-fileUpload";
        console.log(data);
    }

    fileValidations(fileType: string, fileSize: number): string {
        let validationOutput: string;

        if (fileSize < this.maxFileSize) {
            validationOutput = "Validation Pass";
        } else {
            validationOutput = "Size Validation Failed";
        }

        return validationOutput;
    }
}
