﻿import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { AgGridSharedModule } from "../AgGridSharedModule";
import { TitleSharedModule } from "../TitleSharedModule";
import { DocumentsUploadComponent } from "./DocumentsUploadComponent";
import { DocumentsService } from "./DocumentsService";
import { DocumentsPopupComponent } from "./DocumentsPopupComponent";
import { SafePipe } from "./SafePipe";
import { RbPrintPopupComponent } from "./RbPrintPopupComponent";
import { RbPrintService } from "../Common/RbPrintService";
import { TelerikReportingModule } from "@progress/telerik-angular-report-viewer";
import { RbsSharedModule } from "../RbsSharedModule";

@NgModule({
    imports: [
        FrameworkCoreModule,
        AgGridSharedModule,
        TitleSharedModule,
        TelerikReportingModule,
        RbsSharedModule
    ],
    declarations: [
        DocumentsUploadComponent,
        DocumentsPopupComponent,
        //SafePipe,
        RbPrintPopupComponent
    ],
    entryComponents: [
        DocumentsPopupComponent,
        RbPrintPopupComponent
    ],
    providers: [
        DocumentsService,
        RbPrintService
    ]
})
export class DocumentsModule {
}
