﻿import { Component, OnInit, OnDestroy, Inject } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { SharedService } from "../Common/SharedService";
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from "@angular/material";


@Component({
    selector: "document-preview",
    templateUrl: "./DocumentPreviewComponent.html"
})
export class DocumentPreviewComponent {
    public fileName: string;
    public currentDocumentVirtualPath: string;

    constructor(public dialog: MatDialog, public dialogRef: MatDialogRef<any>
        , @Inject(MAT_DIALOG_DATA) public data: {
            fileName: string, filePath: string
        }) {

    }

    ngOnInit(): void {
        this.fileName = this.data.fileName;
        this.currentDocumentVirtualPath = this.data.filePath;
    }

    close(): void {
        this.dialogRef.close();
    }
}