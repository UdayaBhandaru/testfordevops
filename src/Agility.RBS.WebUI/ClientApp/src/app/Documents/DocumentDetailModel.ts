﻿export class DocumentDetailModel {
    id?: number;

    filePath: string;

    fileName: string;

    fileData: string;

    documentStatus: string = "A";

    operation: string;

    documentHeaderID?: number;

    stateID: string;
    stateName: string;

    createdBy?: string;
    createdDate?: Date;
    modifiedBy?: string;
    modifiedDate?: Date;
}