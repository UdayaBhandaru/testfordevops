﻿import { Component } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular/main";

@Component({
    selector: "child-cell",
    template: `<div fxLayout="row" class="grid-actions">
            <a href="#" (click)="invokeParentMethod($event,'preview')" matTooltip="Preview">
                        <mat-icon class="Quickview"></mat-icon></a>
            <a href="api/Documents/DownloadDocument?documentDetailId={{params.data.id}}" matTooltip="Download">
                        <span class="receivingIcon retailsNavIcons"></span></a>
            <a href="#" (click)="invokeParentMethod($event,'delete')" matTooltip="Delete" *ngIf="isDeletable">
                        <mat-icon class="delete"></mat-icon></a>
        </div>`
})
export class DocumentsActionsComponent implements ICellRendererAngularComp {
    public params: any;
    isDeletable: boolean;

    agInit(params: any): void {
        this.params = params;
        if (params.stateId && params.userId) {
            this.isDeletable = params.data.stateID === params.stateId && params.data.createdBy === params.userId;
        }
    }

    refresh(params: any): boolean {
        return true;
    }

    public invokeParentMethod($event: MouseEvent, mode: string): void {
        $event.preventDefault();
        this.params.context.componentParent.open(this.params, mode);
    }
}