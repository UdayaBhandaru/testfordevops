import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpParams } from "@angular/common/http";
import { ServiceDocument, ServiceDocumentResult } from "@agility/frameworkcore";
import { DocumentHeaderModel } from "./DocumentHeaderModel";
import { DocumentDetailModel } from "./DocumentDetailModel";
import { HttpHelperService } from "../Common/HttpHelperService";
import { OrderDetailModel } from "../order/OrderDetailModel";

@Injectable()
export class DocumentsService {
  serviceDocument: ServiceDocument<DocumentHeaderModel> = new ServiceDocument<DocumentHeaderModel>();
  constructor(private httpHelperService: HttpHelperService) { }

  uploadDocument(fileListObj: any[], objectName: string, objectValue: string, isUpdateHeader: boolean, objectStateID: string): Observable<boolean> {
    let apiUrl: string = "/api/Documents/UploadDocuments";
    let documentHeaderObj: DocumentHeaderModel = new DocumentHeaderModel();
    documentHeaderObj = {
      objectName: objectName, objectValue: objectValue, documentDetails: [],
      operation: isUpdateHeader ? "U" : "I", id: null, tableName: null
    };
    let documentDetailObj: DocumentDetailModel;
    for (let i: number = 0; i < fileListObj.length; i++) {
      documentDetailObj = new DocumentDetailModel();
      documentDetailObj = {
        fileData: fileListObj[i].fileData, fileName: fileListObj[i].fileName,
        filePath: null, operation: "I", documentStatus: "A", id: null, documentHeaderID: null, stateID: objectStateID, stateName: null
      };
      documentHeaderObj.documentDetails.push(documentDetailObj);
    }

    return this.httpHelperService.post(apiUrl, documentHeaderObj);
  }

  fetchDocumentList(objectName: string, objectValue: string): Observable<ServiceDocument<DocumentHeaderModel>> {
    return this.serviceDocument.list("/api/Documents/GetListOfDocuments", new HttpParams().set("objectName", objectName).set("objectValue", objectValue));
  }

  uploadFile(file: any, model: any): Observable<ServiceDocument<OrderDetailModel>> {
    return this.httpHelperService.post("/api/OrderDetails/Import", file, new HttpParams().set("model", model));
  }

  deleteDocument(objectName: string, objectValue: string, documentDetailModelObj: DocumentDetailModel): Observable<boolean> {
    let apiUrl: string = "/api/Documents/DeleteDocuments";
    let documentHeaderObj: DocumentHeaderModel = new DocumentHeaderModel();
    documentHeaderObj = {
      objectName: objectName, objectValue: objectValue, documentDetails: [],
      operation: "U", id: null, tableName: null
    };
    documentDetailModelObj.operation = "D";
    documentHeaderObj.documentDetails.push(documentDetailModelObj);
    return this.httpHelperService.post(apiUrl, documentHeaderObj);
  }
}
