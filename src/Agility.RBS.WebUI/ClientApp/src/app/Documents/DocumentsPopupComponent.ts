import { Component, OnInit, OnDestroy, Inject } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { SharedService } from "../Common/SharedService";
import { DatePipe } from "@angular/common";
import { Router, ActivatedRoute } from "@angular/router";
import { GridOptions, GridApi } from "ag-grid-community";
import { ServiceDocument, CommonService, FormMode, MessageResult } from "@agility/frameworkcore";
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from "@angular/material";
import { DocumentsService } from "./DocumentsService";
import { DocumentDetailModel } from "./DocumentDetailModel";
import { DocumentHeaderModel } from "./DocumentHeaderModel";
import { DocumentsConstants } from "../Common/DocumentsConstants";
import { RbDialog, RbButton } from "../Common/controls/RbControlModels";
import { RbDialogService } from "../Common/controls/RbDialogService";
import { GridDateComponent } from "../Common/grid/GridDateComponent";
import { DocumentsActionsComponent } from "./DocumentsActionsComponent";
import { DocumentPreviewComponent } from "./DocumentPreviewComponent";

@Component({
    selector: "document-popup",
    templateUrl: "./DocumentsPopupComponent.html",
    styleUrls: ['./DocumentsPopupComponent.scss']
})

export class DocumentsPopupComponent implements OnInit {
    showUploadPanel: boolean;
    componentName: any;
    documentsDataList: DocumentDetailModel[];
    objectValue: string;
    objectName: string;
    currentFileName: string;
    currentDocumentDetailId: number;
    documentHeaderDataList: DocumentHeaderModel[];
    currentDocumentVirtualPath: any;
    showPreviewPanel: boolean;
    public selected: number;
    showAttachFiles: boolean;
    moduleDescription: string;
    documentsConstants: DocumentsConstants;
    documentsModules: any[];
    objectStateID: string;
    loggedInUserID: string;
    isDeletable: boolean;
    rbDialog: RbDialog;
    private messageResult: MessageResult;
    dialogRef: MatDialogRef<any>;
    gridOptions: GridOptions;
    gridApi: GridApi;
    columns: {}[];

    constructor(private service: DocumentsService, public dialog: MatDialog,
        private sharedService: SharedService, private route: ActivatedRoute, private dialogService: RbDialogService,
        @Inject(MAT_DIALOG_DATA) public data: {
            objectName: string, objectValue: string, dataList: DocumentDetailModel[], showAttachFiles: boolean
        }) {
        this.documentsDataList = [];
        this.documentHeaderDataList = [];
        this.documentsConstants = new DocumentsConstants();
        this.documentsModules = [
            { "key": this.documentsConstants.supplierObjectName, "value": this.documentsConstants.supplierPrintObjectName }
            , { "key": this.documentsConstants.itemObjectName, "value": this.documentsConstants.itemPrintObjectName }
            , { "key": this.documentsConstants.bulkitemObjectName, "value": this.documentsConstants.bulkitemPrintObjectName }
            , { "key": this.documentsConstants.costObjectName, "value": this.documentsConstants.costPrintObjectName }
            , { "key": this.documentsConstants.priceObjectName, "value": this.documentsConstants.pricePrintObjectName }
            , { "key": this.documentsConstants.promoObjectName, "value": this.documentsConstants.promoPrintObjectName }
            , { "key": this.documentsConstants.dealObjectName, "value": this.documentsConstants.dealPrintObjectName }
            , { "key": this.documentsConstants.orderObjectName, "value": this.documentsConstants.orderPrintObjectName }
            , { "key": this.documentsConstants.shipmentObjectName, "value": this.documentsConstants.shipmentPrintObjectName }
            , { "key": this.documentsConstants.costLineObjectName, "value": this.documentsConstants.costLinePrintObjectName }
            , { "key": this.documentsConstants.priceLineObjectName, "value": this.documentsConstants.priceLinePrintObjectName }
        ];
        this.rbDialog = new RbDialog();
        this.messageResult = new MessageResult();
    }

    ngOnInit(): void {
        this.componentName = this;
        this.objectValue = this.data.objectValue;
        this.objectName = this.data.objectName;
        if (this.data.dataList) {
            this.documentsDataList = this.data.dataList;
        }
        this.objectStateID = this.sharedService.stateID;
        this.loggedInUserID = this.sharedService.loggedinUser;
        this.showUploadPanel = false;
        this.showPreviewPanel = false;
        this.showAttachFiles = this.data.showAttachFiles;
        this.moduleDescription = this.documentsModules.find(x => x.key === this.data.objectName).value;
        this.setGridColumnsWithOptions();
    }

    setGridColumnsWithOptions(): void {
        this.columns = [
            {
                headerName: "Document Name", field: "fileName"
                , tooltipField: "fileName", headerTooltip: "Document Name", width: 180
            },
            {
                headerName: "Uploaded Date", field: "createdDate", cellRendererFramework: GridDateComponent
                , tooltipField: "createdDate", headerTooltip: "Uploaded Date", width: 180
            },
            {
                headerName: "Uploaded By", field: "createdBy", tooltipField: "createdBy"
                , headerTooltip: "Uploaded By", width: 180
            },
            {
                headerName: "Uploaded at State", field: "stateName", tooltipField: "stateName"
                , headerTooltip: "Uploaded at State", width: 180
            },
            {
                headerName: "Actions", field: "Actions"
                , cellRendererFramework: DocumentsActionsComponent
                , cellRendererParams: { stateId: this.objectStateID, userId: this.loggedInUserID }
                , suppressCellSelection: true, width: 100, headerTooltip: "Actions"
            }
        ];

        this.gridOptions = {
            onGridReady: (params: any) => {
              this.gridApi = params.api;
              this.gridApi.sizeColumnsToFit();
            },
            context: {
                componentParent: this
            },
            enableColResize: true
        };
    }

    toggleUploadPanel(mode: string): void {
        this.showUploadPanel = mode === "upload";
    }

    close(): void {
        this.dialog.closeAll();
    }

    onNotify(message: string): void {
        if (message === "Update List Section") {
            this.getUpdatedDocumentsList();
        }
    }

    loadSelectedDocument($event: MouseEvent, documentDetailId: number): void {
        $event.preventDefault();
        this.currentDocumentDetailId = documentDetailId;
        let currentDocumentDetailModelObj: DocumentDetailModel;
        currentDocumentDetailModelObj = this.documentsDataList.find(x => x.id === this.currentDocumentDetailId);
        if (currentDocumentDetailModelObj) {
            this.currentFileName = currentDocumentDetailModelObj.fileName;
            this.currentDocumentVirtualPath = "about:blank";
            let self: DocumentsPopupComponent = this;
            setTimeout(() => {
                self.currentDocumentVirtualPath = currentDocumentDetailModelObj.filePath;
            }, 100);
        }
        this.showPreviewPanel = true;
        this.showUploadPanel = false;
        this.isDeletable = currentDocumentDetailModelObj.stateID === this.objectStateID && currentDocumentDetailModelObj.createdBy === this.loggedInUserID;
        this.setStep(documentDetailId, this.selected);
    }

    getUpdatedDocumentsList(): void {
        this.service.fetchDocumentList(this.objectName, this.objectValue).subscribe(() => {
            if (this.service.serviceDocument.dataProfile.dataList) {
                this.documentHeaderDataList = this.service.serviceDocument.dataProfile.dataList;
                this.documentsDataList = this.documentHeaderDataList[0].documentDetails;
                this.sharedService.fileCount = this.documentsDataList.length;
            }
        },
            err => console.log(err)
        );
    }

    setStep(presentDocument: number, pastDocument: number): void {
        this.selected = (presentDocument !== pastDocument) ? presentDocument : 0;
    }

    open(cell: any, mode: string): void {
        if (mode === "delete") {
            this.deleteDocument(cell.data.id);
        } else if (mode === "preview") {
            let data: any = { fileName: cell.data.fileName, filePath: cell.data.filePath };
            this.sharedService.openDilogWithInputData(DocumentPreviewComponent, data, "document preview");
        }
    }

    deleteDocument(id: number): void {
        this.rbDialog = new RbDialog();
        this.messageResult.message = `<div>Are you sure you want to delete the document ?</div>`;
        this.dialogRef = this.dialogService.openMessageDialog("Warning", this.messageResult
            , [new RbButton("", "No", "alertdontsave"), new RbButton("", "Yes", "alertSave")]
            , "37%", "", "iconsdontsave retailsNavIcons", "dontsaveAlert", "dontsaveAlertTitle");
        this.dialogRef.componentInstance.click.subscribe(
            btnName => {
                if (btnName === "No") {
                    this.dialogRef.close();
                }
                if (btnName === "Yes") {
                    this.dialogRef.close();
                    this.deleteSelectedDocument(id);
                }
            });
    }

    deleteSelectedDocument(documentDetailId: number): void {
        let currentDocumentDetailModelObj: DocumentDetailModel = this.documentsDataList.find(x => x.id === documentDetailId);
        this.service.deleteDocument(this.objectName, this.objectValue, currentDocumentDetailModelObj).subscribe(
            (response: any) => {

            },
            (resp: any) => {

            }
        );
    }
}
