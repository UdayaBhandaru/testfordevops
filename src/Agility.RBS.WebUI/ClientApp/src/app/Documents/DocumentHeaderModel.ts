﻿import { DocumentDetailModel } from "./DocumentDetailModel";

export class DocumentHeaderModel {

    id?: number;

    objectName: string;

    objectValue: string;

    documentDetails?: DocumentDetailModel[];

    operation: string;

    tableName: string;
}