﻿import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { ReturnToVendorHeadModel } from "./ReturnToVendorHeadModel";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { ReturnToVendorService } from "./ReturnToVendorService";

@Injectable()
export class ReturnToVendorResolver implements Resolve<ServiceDocument<ReturnToVendorHeadModel>> {
    constructor(private service: ReturnToVendorService) { }
    resolve(route: ActivatedRouteSnapshot): Observable<ServiceDocument<ReturnToVendorHeadModel>> {
        return this.service.addEdit(route.params["id"]);
    }
}