﻿import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument, WorkflowAction } from "@agility/frameworkcore";
import { ReturnToVendorHeadModel } from "./ReturnToVendorHeadModel";
import { ReturnToVendorDetailModel } from "./ReturnToVendorDetailModel";
import { SharedService } from "../Common/SharedService";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../Common/HttpHelperService";

@Injectable()
export class ReturnToVendorService {
    serviceDocument: ServiceDocument<ReturnToVendorHeadModel> = new ServiceDocument<ReturnToVendorHeadModel>();
    constructor(private httpHelperService: HttpHelperService, private sharedService: SharedService) { }

    newModel(model: ReturnToVendorHeadModel): ServiceDocument<ReturnToVendorHeadModel> {
        return this.serviceDocument.newModel(model);
    }

    list(): Observable<ServiceDocument<ReturnToVendorHeadModel>> {
        return this.serviceDocument.list("/api/ReturnToVendor/List");
    }

    search(): Observable<ServiceDocument<ReturnToVendorHeadModel>> {
        return this.serviceDocument.search("/api/ReturnToVendor/Search");
    }

    save(): Observable<ServiceDocument<ReturnToVendorHeadModel>> {
        let currentAction: WorkflowAction = this.serviceDocument.dataProfile.actionService.allowedActions[0];
        if (!this.serviceDocument.dataProfile.dataModel.rtV_ORDER_NO && currentAction) {
            this.serviceDocument.dataProfile.profileForm.controls["currentActionId"].setValue(currentAction.transitionClaim);
            return this.serviceDocument.submit("/api/ReturnToVendor/Submit", true);
        } else {
            return this.serviceDocument.save("/api/ReturnToVendor/Save", true);
        }
    }

    submit(): Observable<ServiceDocument<ReturnToVendorHeadModel>> {
        return this.serviceDocument.submit("/api/ReturnToVendor/Submit", true);
    }

    addEdit(id: number): Observable<ServiceDocument<ReturnToVendorHeadModel>> {
        if (+id === 0) {
            return this.serviceDocument.new("/api/ReturnToVendor/New");
        } else {
            return this.serviceDocument.open("/api/ReturnToVendor/Open", new HttpParams().set("id", id.toString()));
        }
    }

    getDataFromAPI(apiUrl: string): Observable<any> {
        var resp: any = this.httpHelperService.get(apiUrl);
        return resp;
    }

    setDate(): void {
    //    let sd: ReturnToVendorHeadModel = this.serviceDocument.dataProfile.dataModel;
    //    if (sd !== null) {
    //        if (sd.lastRebuildDate != null) {
    //            sd.lastRebuildDate = this.sharedService.setOffSet(sd.lastRebuildDate);
    //        }
    //    }
    }
}