import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument, WorkflowAction } from "@agility/frameworkcore";
import { SharedService } from "../Common/SharedService";
import { HttpHelperService } from "../Common/HttpHelperService";
import { ReturnToVendorSearchModel } from './ReturnToVendorSearchModel';

@Injectable()
export class ReturnToVendorListService {
  serviceDocument: ServiceDocument<ReturnToVendorSearchModel> = new ServiceDocument<ReturnToVendorSearchModel>();
  orderPaging: {
    startRow: number,
    pageSize: number,
    cacheSize: number,
  } = {
      startRow: 0,
      pageSize: 10,
      cacheSize: 100,
    };

  constructor() { }

  newModel(model: ReturnToVendorSearchModel): ServiceDocument<ReturnToVendorSearchModel> {
    return this.serviceDocument.newModel(model);
  }

  list(): Observable<ServiceDocument<ReturnToVendorSearchModel>> {
    return this.serviceDocument.list("/api/ReturntoVendorSearch/List");
  }

  search(additionalParams: any): Observable<ServiceDocument<ReturnToVendorSearchModel>> {
    return this.serviceDocument.search("/api/ReturntoVendorSearch/Search", true, () => this.setSubClassId(additionalParams));
  }

  setSubClassId(additionalParams: any): void {
    var sd: ReturnToVendorSearchModel = this.serviceDocument.dataProfile.dataModel;
    if (additionalParams) {
      sd.startRow = additionalParams.startRow ? additionalParams.startRow : this.orderPaging.startRow;
      sd.endRow = additionalParams.endRow ? additionalParams.endRow : this.orderPaging.cacheSize;
      sd.sortModel = additionalParams.sortModel;
    } else {
      sd.startRow = this.orderPaging.startRow;
      sd.endRow = this.orderPaging.cacheSize;
    }
  }
}
