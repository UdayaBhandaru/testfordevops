export class ReturnToVendorSearchModel {
  rtV_ORDER_NO: number;
  supplier: number;
  supplierName?: string;
  status: string;
  locType?: string;
  location: number;
  locationName?: string;
  error?: string;
  operation?: string;
  tableName?: string;
  createdDate?: Date;
  startRow?: number;
  endRow?: number;
  totalRows?: number;
  sortModel?: {}[];
}
