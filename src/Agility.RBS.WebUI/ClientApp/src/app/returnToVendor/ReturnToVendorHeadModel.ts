import { ReturnToVendorDetailModel } from "./ReturnToVendorDetailModel";

export class ReturnToVendorHeadModel {
  rtV_ORDER_NO: number;
  supplier: number;
  supplierName?: string;
  status: string;
  statusInd?: number;
  locType?: string;
  location: number;
  locationName?: string;
  totalOrderAmt?: number;
  totalRtvCost?: number;
  shipToAdd1?: string;
  shipToAdd2?: string;
  shipToAdd3?: string;
  state?: string;
  stateName?: string;
  shipToCity?: string;
  shipToPcode?: string;
  shipToCountryId?: string;
  shipToCountry?: string;
  retAuthNum?: string;
  courier?: string;
  freight?: number
  completedDate?: Date;
  extRefNo?: string;
  commentDesc?: string;
  mrtNo?: number;
  notAfterDate?: Date;
  restockPct?: number;
  restockCost?: number;
  item?: string;
  returntoVendorDetails?: ReturnToVendorDetailModel[];
  minRetAmt?: string;
  currencyCode?: string;
  error?: string;
  operation?: string;
  tableName?: string;
  createdBy?: string;
  modifiedBy?: string;
  createdDate?: Date;
  modifiedDate?: Date;
  workflowInstance?: string;

  items?: string;
  itemDesc?: string;
  returnedQtys?: number;
  unitCostLocs?: number;
  reasons?: string;
  restockPcts?: number;
  fileCount?: number;

  contactName?: string;
  contactPhone?: string;
  contactFax?: string;
}
