﻿import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ReturnToVendorHeadComponent } from "./ReturnToVendorHeadComponent";
import { ReturnToVendorListComponent } from "./ReturnToVendorListComponent";
import { ReturnToVendorResolver } from "./ReturnToVendorResolver";
import { ReturnToVendorListResolver } from "./ReturnToVendorListResolver";

const ReturnToVendorRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: ReturnToVendorListComponent,
                resolve:
                {
                    references: ReturnToVendorListResolver
                }
            },
            {
                path: "New/:id",
                component: ReturnToVendorHeadComponent,
                resolve:
                {
                    references: ReturnToVendorResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(ReturnToVendorRoutes)
    ]
})
export class ReturnToVendorRoutingModule { }
