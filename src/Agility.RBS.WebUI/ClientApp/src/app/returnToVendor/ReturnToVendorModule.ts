import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { ReturnToVendorRoutingModule } from "./ReturnToVendorRoutingModule";
import { ReturnToVendorHeadComponent } from "./ReturnToVendorHeadComponent";
import { ReturnToVendorListComponent } from "./ReturnToVendorListComponent";
import { ReturnToVendorService } from "./ReturnToVendorService";
import { ReturnToVendorResolver } from "./ReturnToVendorResolver";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../AgGridSharedModule";
import { TitleSharedModule } from "../TitleSharedModule";
import { ReturnToVendorListResolver } from "./ReturnToVendorListResolver";
import { RbsSharedModule } from "../RbsSharedModule";
import { ReturnToVendorListService } from './ReturnToVendorListService';

@NgModule({
    imports: [
        FrameworkCoreModule,
        ReturnToVendorRoutingModule,
        AgGridModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        ReturnToVendorHeadComponent,
        ReturnToVendorListComponent
    ],
    providers: [
        ReturnToVendorService,
        ReturnToVendorListService,
        ReturnToVendorListResolver,
        ReturnToVendorResolver
    ]
})
export class ReturnToVendorModule {
}
