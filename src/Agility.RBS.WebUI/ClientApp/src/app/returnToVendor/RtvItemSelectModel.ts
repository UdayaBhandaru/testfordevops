export class RtvItemSelectModel {
  item?: string;
  itemDesc?: string;
  supplier?: number;
  suppName?: string;
  locationId?: number;
  locName?: string;
  locType?: string;
  importCountry?: string;
  unitCost?: number;
  unitRetail?: number;
  costCurrency?: string;
  retailCurrency?: string;
  supCurrency?: string;
}
