import { Component, Inject } from "@angular/core";
import { DatePipe } from "@angular/common";
import { FormGroup } from "@angular/forms";
import { Subscription } from "rxjs";
import { CommonService, FormMode } from "@agility/frameworkcore";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { SharedService } from "../Common/SharedService";
import { GridOptions, GridApi } from "ag-grid-community";
import { SupplierAddressDomainModel } from "../Common/DomainData/SupplierAddressDomainModel";

@Component({
    selector: "ShipementInfoPopup",
  templateUrl: "./ShipmentInfoPopupComponent.html",
  styleUrls: ['./ShipmentInfoPopupComponent.scss'],
})

export class ShipmentInfoPopupComponent {
    infoGroup: any;

    constructor(private service: SharedService, private commonService: CommonService, public dialog: MatDialog
        , @Inject(MAT_DIALOG_DATA) public model: SupplierAddressDomainModel) {
        this.infoGroup = this.commonService.getFormGroup(this.model, 0);
        //this.addressData = this.model.addresses
    }

    ngOnInit(): void {
    }

    close(): void {
        this.dialog.closeAll();
    }

    changeAddress(event: any): void {
        this.infoGroup = this.commonService.getFormGroup(event.options, 0);
    }
}
