export class ReturnToVendorDetailModel {
  detailSeqNo?: number;
  items?: string;
  itemDesc?: string;
  returnedQtys: number;
  fromDisps?: string;
  toDisps?: string;
  unitCostExts?: number;
  unitCostSupps?: number;
  unitCostLocs?: number;
  reasons: string;
  reasonName: string;
  restockPcts?: number;
  invStatuses?: string;
  mcReturnedQtys?: number;
  weights?: number;
  weightUoms?: string;
  weightCuoms?: number;
  mcWeightCuoms?: number;
  Cuoms?: string;
  operation?: string;
}
