import { Component, OnInit, OnDestroy, ViewChild } from "@angular/core";
import { ServiceDocument } from "@agility/frameworkcore";
import { SharedService } from "../Common/SharedService";
import { Router } from "@angular/router";
import { GridExportModel } from "../Common/grid/GridExportModel";
import { FormGroup } from "@angular/forms";
import { GridEditComponent } from "../Common/grid/GridEditComponent";
import { InfoComponent } from "../Common/InfoComponent";
import { WorkflowStatusDomainModel } from "../Common/DomainData/WorkflowStatusDomainModel";
import { SupplierDomainModel } from "../Common/DomainData/SupplierDomainModel";
import { SearchComponent } from "../Common/SearchComponent";
import { LocationDomainModel } from "../Common/DomainData/LocationDomainModel";
import { Subscription } from "rxjs";
import { GridOptions, IServerSideDatasource, IServerSideGetRowsParams, GridReadyEvent } from 'ag-grid-community';
import { ReturnToVendorSearchModel } from './ReturnToVendorSearchModel';
import { ReturnToVendorListService } from './ReturnToVendorListService';

@Component({
  selector: "ReturnToVendormst",
  templateUrl: "./ReturnToVendorListComponent.html"
})
export class ReturnToVendorListComponent implements OnInit, IServerSideDatasource, OnDestroy {
  public serviceDocument: ServiceDocument<ReturnToVendorSearchModel>;
  PageTitle = "Return to Vendor";
  redirectUrl = "ReturnToVendor/New/";
  componentName: any;
  columns: any[];
  additionalSearchParams: any;
  loadedPage: boolean;
  model: ReturnToVendorSearchModel = {
    rtV_ORDER_NO: null, location: null, status: null, supplier: null
  };

  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  supplierData: SupplierDomainModel[];
  workflowStatusList: WorkflowStatusDomainModel[];
  locationData: LocationDomainModel[];
  apiUrl: string; apiUrlSupplier: string;
  public gridOptions: GridOptions;
  @ViewChild("searchPanel") searchPanel: SearchComponent;
  searchServiceSubscription: Subscription;

  constructor(public service: ReturnToVendorListService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "Order No.", field: "rtV_ORDER_NO", tooltipField: "rtV_ORDER_NO" },
      { headerName: "Supplier", field: "supplierName", tooltipField: "supplierName" },
      { headerName: "Location", field: "locationName", tooltipField: "locationName" },
      { headerName: "Status", field: "status", tooltipField: "status" },
      {
        headerName: "ACTIONS", field: "Actions", cellRendererFramework: GridEditComponent, width: 40
      }
    ];

    this.gridOptions = {
      onGridReady: (params: GridReadyEvent) => {
        params.api.setEnterpriseDatasource(this);
      },
      context: {
        componentParent: this
      },
      suppressRowClickSelection: true,
      suppressCellSelection: true,
      rowModelType: "enterprise",
      paginationPageSize: this.service.orderPaging.pageSize,
      enableServerSideSorting: true,
      enableServerSideFilter: true,
      cacheBlockSize: this.service.orderPaging.cacheSize,
    };

    this.apiUrlSupplier = "/api/Supplier/SuppliersGetName?name=";
    this.supplierData = this.service.serviceDocument.domainData["supplier"];
    this.workflowStatusList = this.service.serviceDocument.domainData["workflowStatusList"];
    this.locationData = this.service.serviceDocument.domainData["location"];
    this.sharedService.domainData = {
      location: this.locationData,
    };

    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  getRows(params: IServerSideGetRowsParams): void {
    this.additionalSearchParams = { startRow: params.request.startRow, endRow: params.request.endRow, sortModel: params.request.sortModel };
    if (!this.loadedPage) {
      this.loadedPage = true;
      params.successCallback(
        this.serviceDocument.dataProfile.dataList.slice(params.request.startRow, params.request.endRow),
        this.service.serviceDocument.dataProfile.dataModel.totalRows);
    } else {
      this.searchServiceSubscription = this.service.search(this.additionalSearchParams).subscribe((sDoc) => {
        params.successCallback(sDoc.dataProfile.dataList, this.service.serviceDocument.dataProfile.dataModel.totalRows);
      });
    }
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl + cell.data.rtV_ORDER_NO, this.service.serviceDocument.dataProfile.dataList);
  }

  ngOnDestroy(): void {
    if (this.searchServiceSubscription) {
      this.searchServiceSubscription.unsubscribe();
    }
  }

  addValidation(): void {
    this.router.navigate(["/" + this.redirectUrl + "0"], { skipLocationChange: true });
    this.sharedService.previousUrl = "../ReturnToVendor/List";
  }
}
