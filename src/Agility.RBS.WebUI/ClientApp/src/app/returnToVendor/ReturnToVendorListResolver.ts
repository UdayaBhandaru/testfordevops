import { Injectable } from "@angular/core";
import { Resolve } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { ReturnToVendorListService } from './ReturnToVendorListService';
import { ReturnToVendorSearchModel } from './ReturnToVendorSearchModel';

@Injectable()
export class ReturnToVendorListResolver implements Resolve<ServiceDocument<ReturnToVendorSearchModel>> {
    constructor(private service: ReturnToVendorListService) { }
  resolve(): Observable<ServiceDocument<ReturnToVendorSearchModel>> {
        return this.service.list();
    }
}
