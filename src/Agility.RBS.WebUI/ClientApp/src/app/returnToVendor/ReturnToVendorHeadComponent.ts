import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ServiceDocument, MessageType, FormMode, CommonService } from "@agility/frameworkcore";
import { ReturnToVendorHeadModel } from "./ReturnToVendorHeadModel";
import { ReturnToVendorService } from "./ReturnToVendorService";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { SharedService } from "../Common/SharedService";
import { ReturnToVendorDetailModel } from "./ReturnToVendorDetailModel";
import { Subscription } from "rxjs";
import { SupplierDomainModel } from "../Common/DomainData/SupplierDomainModel";
import { GridOptions, GridApi, RowNode } from "ag-grid-community";
import { DocumentsConstants } from "../Common/DocumentsConstants";
import { LoaderService } from "../Common/LoaderService";
import { SupplierAddressDomainModel } from "../Common/DomainData/SupplierAddressDomainModel";
import { SupplierDetailDomainModel } from "../Common/DomainData/SupplierDetailDomainModel";
import { LocationDomainModel } from "../Common/DomainData/LocationDomainModel";
import { ShipmentInfoPopupComponent } from "./ShipmentInfoPopupComponent";
import { DomainDetailModel } from "../domain/DomainDetailModel";
import { GridDeleteComponent } from '../Common/grid/GridDeleteComponent';
import { RtvItemSelectModel } from './RtvItemSelectModel';
import { GridAmountComponent } from '../Common/grid/GridAmountComponent';

@Component({
  selector: "Return-Vendor-head",
  templateUrl: "./ReturnToVendorHeadComponent.html"
})
export class ReturnToVendorHeadComponent implements OnInit {
  _componentName = "Return To Vendor";
  PageTitle: string = "Return To Vendor";
  public serviceDocument: ServiceDocument<ReturnToVendorHeadModel>;
  localizationData: any;
  model: ReturnToVendorHeadModel;
  editMode: boolean;
  supplierData: SupplierDomainModel[];
  searchServiceSubscription: Subscription;
  supplierDetailsServiceSubscription: Subscription;
  itemDetailsServiceSubscription: Subscription;
  routeServiceSubscription: Subscription;

  screenName: string;
  rtvStatus: string;
  componentName: any;
  columns: any[];
  public sheetName: string = "Return to Vendor Detail";
  data: ReturnToVendorDetailModel[] = [];
  dataReset: ReturnToVendorDetailModel[] = [];
  public gridOptions: GridOptions;
  gridApi: GridApi;
  documentsConstants: DocumentsConstants = new DocumentsConstants();
  detailModel: ReturnToVendorDetailModel;
  apiUrl: string; apiUrlSupplier: string;
  supplierAddrData: SupplierAddressDomainModel[];
  locTypeData: DomainDetailModel[];
  reasonData: DomainDetailModel[];
  locationData: LocationDomainModel[]; locations: LocationDomainModel[];
  primaryId: number;
  addText: string = "+ Add To Grid";
  node: any;
  pForm: any;
  deletedRows: any[] = [];
  propList: string[] = ["items", "returnedQtys", "reasons"];

  constructor(private service: ReturnToVendorService, public sharedService: SharedService, private changeRef: ChangeDetectorRef
    , private router: Router, private route: ActivatedRoute, private loaderService: LoaderService, private commonService: CommonService) {
  }

  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }

  ngOnInit(): void {
    this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
    this.supplierData = this.service.serviceDocument.domainData["supplier"];
    this.locTypeData = Object.assign([], this.service.serviceDocument.domainData["locType"]);
    this.reasonData = Object.assign([], this.service.serviceDocument.domainData["reason"]);
    this.locations = Object.assign([], this.service.serviceDocument.domainData["location"]);
    this.screenName = this.documentsConstants.rtvObjectName;
    this.apiUrlSupplier = "/api/Supplier/SuppliersGetName?name=";
    this.componentName = this;
    this.columns = [
      { headerName: "", field: "", checkboxSelection: true, width: 30 },
      { headerName: "Item", field: "items", tooltipField: "items" },
      { headerName: "Item Desc", field: "itemDesc", tooltipField: "itemDesc" },
      { headerName: "Quantity Requested", field: "returnedQtys", tooltipField: "returnedQtys" },
      { headerName: "RTV Unit Cost", field: "unitCostSupps", tooltipField: "unitCostSupps", cellRendererFramework: GridAmountComponent },
      { headerName: "Final RTV Cost", field: "unitCostSupps", tooltipField: "unitCostSupps", cellRendererFramework: GridAmountComponent },
      { headerName: "Reason", field: "reasonName", tooltipField: "reasonName" },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridDeleteComponent, width: 60
      }
    ];
    this.gridOptions = {
      onRowSelected: this.onRowSelected,
      onGridReady: (params: any) => {
        this.gridApi = params.api;
      },
      context: {
        componentParent: this
      }
    };

    if (this.service.serviceDocument.dataProfile.dataModel) {
      this.bind();
    }
  }

  bind(): void {
    this.routeServiceSubscription = this.route.params.subscribe((resp: any) => {
      this.primaryId = resp["id"];
    });

    if (this.primaryId.toString() !== "0") {
      this.editMode = true;
      this.PageTitle = `${this.PageTitle} - EDIT - ${this.primaryId}`;
      this.rtvStatus = this.service.serviceDocument.dataProfile.dataModel.status;
      this.locationData = this.locations.filter(item => item.locType
        === this.service.serviceDocument.dataProfile.profileForm.controls["locType"].value);
    } else {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign(this.service.serviceDocument.dataProfile.dataModel
        , { operation: "I", status: "Worksheet" }
        , { workflowInstance: this.service.serviceDocument.dataProfile.dataModel.workflowInstance });
      this.service.serviceDocument.dataProfile.profileForm.patchValue(this.service.serviceDocument.dataProfile.dataModel);
      this.PageTitle = `${this.PageTitle} - ADD`;
    }

    this.serviceDocument = this.service.serviceDocument;
    if (this.service.serviceDocument.dataProfile.dataModel.returntoVendorDetails) {
      Object.assign(this.data, this.service.serviceDocument.dataProfile.dataModel.returntoVendorDetails);
      Object.assign(this.dataReset, this.service.serviceDocument.dataProfile.dataModel.returntoVendorDetails);
    }
    this.sharedService.fileCount = this.service.serviceDocument.dataProfile.dataModel.fileCount;
    this.sharedService.checkWfPermitions(this.serviceDocument);
  }

  add($event: MouseEvent): void {
    this.pForm = this.serviceDocument.dataProfile.profileForm;
    $event.preventDefault();
    this.propList.forEach(x => this.pForm.controls[x].setValidators([Validators.required]));
    this.propList.forEach(x => this.pForm.controls[x].updateValueAndValidity());

    if (this.pForm.valid) {
      let returnToVendorDetailData: ReturnToVendorDetailModel[] = this.data ? this.data.filter(itm => itm.items === this.pForm.controls["items"].value) : null;
      if (!returnToVendorDetailData || returnToVendorDetailData.length !== 1) {
        var reasonName = this.reasonData.filter(id => id.code === this.pForm.controls["reasons"].value)[0].codeDesc;
        let emptyObject: ReturnToVendorDetailModel = {
          items: this.pForm.controls["items"].value, itemDesc: this.pForm.controls["itemDesc"].value
          , operation: "I", reasonName: reasonName, returnedQtys: this.pForm.controls["returnedQtys"].value
          , restockPcts: this.pForm.controls["restockPcts"].value, unitCostSupps: this.pForm.controls["unitCostSupps"].value
          , reasons: this.pForm.controls["reasons"].value
        };
        if (!this.data) {
          this.data = [];
        }

        this.data.push(Object.assign(emptyObject, this.service.serviceDocument.dataProfile.profileForm.value));
        if (this.gridApi) {
          this.gridApi.setRowData(this.data);
        }
        this.resetDetails();
        this.totalAmtCalc();
      } else if (returnToVendorDetailData) {
        if (this.node && this.node.selected) {
          Object.assign(returnToVendorDetailData[0], this.service.serviceDocument.dataProfile.profileForm.value);
          if (returnToVendorDetailData[0].detailSeqNo) {
            returnToVendorDetailData[0].operation = "U";
          }
          this.node.setSelected(false);
        } else {
          this.sharedService.errorForm("Item already exist");
        }
      }
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  save(saveOnly: boolean): void {
    if (this.rtvStatus !== "RequestCompleted") {
      this.propList.forEach(x => this.pForm.controls[x].clearValidators());
      this.propList.forEach(x => this.pForm.controls[x].updateValueAndValidity());
      if (this.serviceDocument.dataProfile.profileForm.valid) {
        this.saveSubscription(saveOnly);
      } else {
        this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
      }
    }
  }

  saveAndContinue(): void {
    this.save(false);
  }

  saveSubscription(saveOnly: boolean): void {
    this.loaderService.display(true);
    if (this.deletedRows.length > 0) {
      this.deletedRows.forEach(x => this.data.push(x));
    }
    this.serviceDocument.dataProfile.profileForm.controls["returntoVendorDetails"] = new FormControl();
    this.serviceDocument.dataProfile.profileForm.controls["returntoVendorDetails"].setValue(this.data);

    this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.primaryId = this.service.serviceDocument.dataProfile.dataModel.rtV_ORDER_NO;
        this.sharedService.saveForm(`RTV saved successfully - Request ID ${this.primaryId}`).subscribe(() => {
          if (saveOnly) {
            this.router.navigate(["/ReturnToVendor/List"], { skipLocationChange: true });
          } else {
            this.router.navigate(["/Blank"], {
              skipLocationChange: true, queryParams: {
                id: "/ReturnToVendor/New/" + this.primaryId
              }
            });
            this.loaderService.display(false);
          }
        }, (error: string) => { console.log(error); });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
    }, (error: string) => { console.log(error); });
    this.loaderService.display(false);
  }

  reset(): void {
    Object.keys(this.serviceDocument.dataProfile.profileForm.controls).forEach(control => {
      this.serviceDocument.dataProfile.profileForm.controls[control].setValue(this.serviceDocument.dataProfile.dataModel[control]);
    });
    this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
  }

  submitAction(): void {
    this.loaderService.display(true);
    this.service.submit().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.primaryId = this.service.serviceDocument.dataProfile.dataModel.rtV_ORDER_NO;
        this.sharedService.saveForm(`${this.localizationData.bulkItem.bulkitemsave} - Request ID ${this.primaryId}`).subscribe(() => {
          this.router.navigate(["/ReturnToVendor/List"], { skipLocationChange: true });
        }, (error: string) => {
          console.log(error);
          this.loaderService.display(false);
        });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
        this.loaderService.display(false);
      }
    }, (error: string) => {
      console.log(error);
      this.loaderService.display(false);
    });
  }

  changeSupplier(event: any): void {
    this.apiUrl = "/api/ReturnToVendor/SupplierDetailAddrGet?supplier=" + event.options.supplier;
    this.supplierDetailsServiceSubscription = this.service.getDataFromAPI(this.apiUrl).subscribe((response: SupplierAddressDomainModel[]) => {
      if (response && response.length > 0) {
        this.serviceDocument.dataProfile.profileForm.patchValue({
          currencyCode: response[0].currencyCode,
          minRetAmt: response[0].retMinDolAmt,
          restockPct: response[0].handlingPct,
          shipToAdd1: response[0].add1,
          shipToAdd2: response[0].add2,
          shipToAdd3: response[0].add3,
          state: response[0].state,
          stateName: response[0].stateDesc,
          shipToCity: response[0].city,
          shipToPcode: response[0].post,
          shipToCountryId: response[0].countryId,
          shipToCountry: response[0].countryDesc,
          contactName: response[0].contactName,
          contactPhone: response[0].contactPhone,
          contactFax: response[0].contactFax

        });
      }
    });
  }

  onRowSelected(event: any): void {
    let context: any = event.context.componentParent;
    if (event.node.selected) {
      context.serviceDocument.dataProfile.profileForm = context.commonService.getFormGroup(event.data, 2);
      context.node = event.node;
      context.addText = " Update";
      if (event.data.operation === "U") {
        context.editMode = true;
      }
    } else if (event.context.componentParent.node !== event.node) {
      // when we select other row when one row already selected
    } else {
      context.addText = "+ Add To Grid";
      context.resetDetails();
      context.serviceDocument = context.service.serviceDocument;
      context.locations = [];
      context.editMode = false;
    }
  }

  public locTypeChanged(): void {
    this.locationData = this.locations.filter(item => item.locType === this.serviceDocument.dataProfile.profileForm.controls["locType"].value);
  }

  ngOnDestroy(): void {
    if (this.supplierDetailsServiceSubscription) {
      this.supplierDetailsServiceSubscription.unsubscribe();
    }
    if (this.searchServiceSubscription) {
      this.searchServiceSubscription.unsubscribe();
    }
    if (this.itemDetailsServiceSubscription) {
      this.itemDetailsServiceSubscription.unsubscribe();
    }
  }

  itemBasedLogic(): void {
    this.pForm = this.serviceDocument.dataProfile.profileForm;
    if (this.pForm.controls["items"].value) {
      this.apiUrl = "/api/ReturnToVendor/RtvItemGet?itemBarcode=" + this.pForm.controls["items"].value
        + "&supplier=" + this.pForm.controls["supplier"].value + "&location=" + this.pForm.controls["location"].value;
      this.itemDetailsServiceSubscription = this.service.getDataFromAPI(this.apiUrl).subscribe((response: RtvItemSelectModel[]) => {
        if (response && response.length > 0) {
          this.serviceDocument.dataProfile.profileForm.patchValue({
            itemDesc: response[0].itemDesc,
            unitCostSupps: response[0].unitCost
          });
        } else {
          this.sharedService.errorForm("No Records available with the Item:" + this.pForm.controls["items"].value);
          this.resetDetails();
        }
        this.changeRef.detectChanges();
      }, (error: string) => { console.log(error); });
    }
  }

  loadShipInfo(): void {
    this.pForm = this.serviceDocument.dataProfile.profileForm;
    this.loaderService.display(true);
    let modelShip: SupplierAddressDomainModel = {
      add1: this.pForm.controls["shipToAdd1"].value, add2: this.pForm.controls["shipToAdd2"].value, add3: this.pForm.controls["shipToAdd3"].value
      , city: this.pForm.controls["shipToCity"].value, contactFax: this.pForm.controls["contactFax"].value
      , contactName: this.pForm.controls["contactName"].value, contactPhone: this.pForm.controls["contactPhone"].value
      , countryId: this.pForm.controls["shipToCountryId"].value, countryDesc: this.pForm.controls["shipToCountry"].value
      , post: this.pForm.controls["shipToPcode"].value, state: this.pForm.controls["stateName"].value, supplier: this.pForm.controls["supplier"].value
    };
    this.sharedService.openDilogForFindItems(ShipmentInfoPopupComponent, modelShip, "Ship Info");
    this.loaderService.display(false);
  }

  resetDetails(): void {
    this.pForm = this.serviceDocument.dataProfile.profileForm;
    this.pForm.controls["items"].setValue(null);
    this.pForm.controls["itemDesc"].setValue(null);
    this.pForm.controls["unitCostSupps"].setValue(null);
    this.pForm.controls["returnedQtys"].setValue(null);
    this.pForm.controls["reasons"].setValue(null);
    this.pForm.controls["restockPcts"].setValue(null);
  }

  totalAmtCalc(): void {
    this.pForm = this.serviceDocument.dataProfile.profileForm;
    var totalOrderAmt: number = 0;
    var restockCost: number = 0;
    this.data.forEach(ctrl => {
      totalOrderAmt += ctrl.returnedQtys * ctrl.unitCostSupps;
      restockCost += totalOrderAmt * ctrl.restockPcts / 100;
    });
    totalOrderAmt = +totalOrderAmt.toFixed(3);
    restockCost = +restockCost.toFixed(3);
    this.pForm.controls["totalOrderAmt"].setValue(totalOrderAmt);
    this.pForm.controls["restockCost"].setValue(restockCost);
    this.pForm.controls["totalRtvCost"].setValue(totalOrderAmt - restockCost);
  }
}
