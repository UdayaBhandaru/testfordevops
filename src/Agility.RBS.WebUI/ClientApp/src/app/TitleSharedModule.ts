import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { TitleComponent } from "./Common/TitleComponent";
import { RbNumericComponent } from "./Common/controls/RbNumericComponent";
import { RbFileUpload } from "./Common/controls/RbFileUpload";
import { DatePipe } from "@angular/common";
import { RbTextAreaComponent } from "./Common/controls/RbTextAreaComponent";
import { RbsSharedModule } from "./RbsSharedModule";
import { UpperCaseTextDirective } from "./Common/controls/RbUpperCaseInputDirective";

@NgModule({
    imports: [
        FrameworkCoreModule, RbsSharedModule
    ],
    declarations: [
        TitleComponent, RbNumericComponent, RbFileUpload, RbTextAreaComponent
    ],
    exports: [
        TitleComponent,
        RbNumericComponent,
        RbFileUpload,
        RbTextAreaComponent, RbsSharedModule
    ],
    providers: [
        DatePipe
    ]
})
export class TitleSharedModule {
}
