import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { CurrencyService } from "./CurrencyService";
import { CurrencyModel } from "./CurrencyModel";
@Injectable()
export class CurrencyListResolver implements Resolve<ServiceDocument<CurrencyModel>> {
    constructor(private service: CurrencyService) { }
    resolve(): Observable<ServiceDocument<CurrencyModel>> {
        return this.service.list();
    }
}

@Injectable()
export class CurrencyResolver implements Resolve<ServiceDocument<CurrencyModel>> {
    constructor(private service: CurrencyService) { }
    resolve(): Observable<ServiceDocument<CurrencyModel>> {
        return this.service.addEdit();
    }
}



