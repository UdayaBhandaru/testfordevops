export class CurrencyDetailModel { 
  currencyCode: string;
  currencyDesc: string;
  effectiveDate: Date;
  exchangeType: string;
  exchangeRate: number
  operation?: string;
}
