import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { CurrencyModel } from "./CurrencyModel";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../Common/HttpHelperService";

@Injectable()
export class CurrencyService {
    serviceDocument: ServiceDocument<CurrencyModel> = new ServiceDocument<CurrencyModel>();
    constructor(private httpHelperService: HttpHelperService) { }

    newModel(model: CurrencyModel): ServiceDocument<CurrencyModel> {
        return this.serviceDocument.newModel(model);
    }

    search(): Observable<ServiceDocument<CurrencyModel>> {
      return this.serviceDocument.search("/api/Currency/Search");
    }

    save(): Observable<ServiceDocument<CurrencyModel>> {
      return this.serviceDocument.save("/api/Currency/Save", true);
    }

    list(): Observable<ServiceDocument<CurrencyModel>> {
      return this.serviceDocument.list("/api/Currency/List");
    }

    addEdit(): Observable<ServiceDocument<CurrencyModel>> {
      return this.serviceDocument.list("/api/Currency/New");
    }

  isExistingCurrency(zoneGroupId: string): Observable<boolean> {
    return this.httpHelperService.get("/api/Currency/IsExistingCurrency", new HttpParams().set("zoneGroupId", zoneGroupId));
    }
}
