import { CurrencyDetailModel } from './CurrencyDetailModel';

export class CurrencyModel {

  currencyCode: string;
  currencyDesc: string;
  currencyCostFmt: string;
  currencyRtlFmt: string;
  currencyCostDec?: number;
  currencyRtlDec?: number;
  currencyIsoCode: string;
  currencyIsoNumeric: string;
  currencySymbol: string;
  currencyMajor: string;
  currencyMinor: string;
  currencyDecMark: string;
  currencyTSep: string;
  currencyHtmlCode: string;
  Error: string;
  Operation: string;
  currencyStatus: string;
  currencyStatusDesc: string;
  operation?: string;
  tableName?: string;
  currencyDetailList?:
  CurrencyDetailModel[];
}
