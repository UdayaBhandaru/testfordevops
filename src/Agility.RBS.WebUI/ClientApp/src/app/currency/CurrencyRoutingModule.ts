import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CurrencyListComponent } from "./CurrencyListComponent";
import { CurrencyListResolver, CurrencyResolver} from "./CurrencyResolver";
import { CurrencyComponent } from "./CurrencyComponent";
const UtilityRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
              component: CurrencyListComponent,
                resolve:
                {
                    serviceDocument: CurrencyListResolver
                }
            },
            {
                path: "New",
                component: CurrencyComponent,
                resolve:
                {
                    serviceDocument: CurrencyResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(UtilityRoutes)
    ]
})
export class CurrencyRoutingModule { }
