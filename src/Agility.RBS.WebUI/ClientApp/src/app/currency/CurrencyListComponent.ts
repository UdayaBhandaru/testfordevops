import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { GridOptions } from "ag-grid-community";
import { CurrencyModel } from "./CurrencyModel";
import { CurrencyService } from "./CurrencyService";
import { CurrencyDetailModel } from './CurrencyDetailModel';
import { GridExportModel } from '../Common/grid/GridExportModel';
import { DomainDetailModel } from '../domain/DomainDetailModel';
import { CurrencyDomainModel } from '../Common/DomainData/CurrencyDomainModel';
import { DepartmentDomainModel } from '../Common/DomainData/DepartmentDomainModel';
import { SharedService } from '../Common/SharedService';
import { GridEditComponent } from '../Common/grid/GridEditComponent';
import { InfoComponent } from '../Common/InfoComponent';

@Component({
  selector: "Currency-list",
  templateUrl: "./CurrencyListComponent.html"
})
export class CurrencyListComponent implements OnInit {
  PageTitle = "Currency";
  redirectUrl = "Currency";
  componentName: any;
  serviceDocument: ServiceDocument<CurrencyModel>;
  public gridOptions: GridOptions;
  columns: any[];
  model: CurrencyModel;
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  domainDtlData: DomainDetailModel[];
  locationData: CurrencyDetailModel[];
  locationSelectedData: CurrencyDetailModel[] = [];
  primaryIndicatorData: DomainDetailModel[];
  currencyData: CurrencyDomainModel[];
  departmentData: DepartmentDomainModel[];

  constructor(public service: CurrencyService, private sharedService: SharedService, private route: ActivatedRoute,
    private router: Router) {
  }

  ngOnInit(): void {

    this.departmentData = Object.assign([], this.service.serviceDocument.domainData["department"]);

    this.componentName = this;
    this.columns = [
      //{ headerName: "Zone Group", field: "zoneGroupDesc", tooltipField: "zoneGroupDesc", width: 40 },
      { headerName: "currency Code", field: "currencyCode", tooltipField: "currencyCode", width: 30 },
      { headerName: "currency Desc", field: "currencyDesc", tooltipField: "currencyDesc", width: 60},
      { headerName: "currency Cost Fmt", field: "currencyCostFmt", tooltipField: "currencyCostFmt", width: 60 },
      { headerName: "currency Rtl Fmt", field: "currencyRtlFmt", tooltipField: "currencyRtlFmt", width: 60 },
      { headerName: "currency Cost Dec", field: "currencyCostDec", tooltipField: "currencyCostDec", width: 60 },
      { headerName: "currency Rtl Dec", field: "currencyRtlDec", tooltipField: "currencyRtlDec", width: 60 },  
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 35
      }
    ];
    this.route.data.subscribe(() => {
      this.loadCurrencyDomainData();
      this.setZopneModel();
      if (this.sharedService.searchData) {
        this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData, this.sharedService.searchData.Zone);
        this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
      } else {
        this.service.newModel(this.model);
      }
      this.serviceDocument = this.service.serviceDocument;
    });
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }

  private loadCurrencyDomainData(): void {
    this.currencyData = Object.assign([], this.service.serviceDocument.domainData["currency"]);
    this.sharedService.domainData = {
      currencyStatusDomainData: this.domainDtlData, currencyLocDrdDomainData: this.locationData
    };
  }

  private setZopneModel(): void {
    this.model = new CurrencyModel();
    this.currencyHeaderModel();
  }

  cleanUp(): void {
    this.locationSelectedData = [];
  }
  private currencyHeaderModel() {
    this.model = {
    currencyCode: null, currencyDesc: null, currencyCostFmt: null, currencyRtlFmt: null, currencyCostDec: null,
      currencyRtlDec: null, currencyIsoCode: null, currencyIsoNumeric: null, currencySymbol: null, currencyMajor: null,
      currencyMinor: null, currencyDecMark: null, currencyTSep: null, currencyHtmlCode: null, Error: null, Operation: null,
      currencyStatus: null, currencyStatusDesc: null,   currencyDetailList: []
    };
  }
}
