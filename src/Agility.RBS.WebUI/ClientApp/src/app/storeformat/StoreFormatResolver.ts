import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { StoreFormatService } from "./StoreFormatService";
import { StoreFormatModel } from "./StoreFormatModel";
@Injectable()
export class StoreFormatListResolver implements Resolve<ServiceDocument<StoreFormatModel>> {
    constructor(private service: StoreFormatService) { }
    resolve(): Observable<ServiceDocument<StoreFormatModel>> {
        return this.service.list();
    }
}

@Injectable()
export class StoreFormatResolver implements Resolve<ServiceDocument<StoreFormatModel>> {
    constructor(private service: StoreFormatService) { }
    resolve(): Observable<ServiceDocument<StoreFormatModel>> {
        return this.service.addEdit();
    }
}



