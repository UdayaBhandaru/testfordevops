import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { StoreFormatModel } from './StoreFormatModel';
import { StoreFormatService } from './StoreFormatService';
import { SharedService } from '../Common/SharedService';

@Component({
  selector: "StoreFormat",
  templateUrl: "./StoreFormatComponent.html"
})

export class StoreFormatComponent implements OnInit {
  PageTitle = "Store Format";
  redirectUrl = "StoreFormat";
    serviceDocument: ServiceDocument<StoreFormatModel>;
    editMode: boolean = false;
    editModel: any;
    localizationData: any;  
    model: StoreFormatModel;  


    constructor(private service: StoreFormatService, private sharedService: SharedService, private router: Router
        , private ref: ChangeDetectorRef) {
    }

    ngOnInit(): void {
        this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
        this.bind();
    }

    bind(): void {
        delete this.editModel;
        if (this.sharedService.editObject) {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);         
            Object.assign(this.service.serviceDocument.dataProfile.dataModel, { operation: "U" });
            this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            delete this.sharedService.editObject;
            this.editMode = true;

        } else {            
          this.model = { storeFormat: null, formatName: null, operation: "I"};
           
            this.service.serviceDocument.dataProfile.dataModel = this.model;
            this.service.newModel(this.model);
        }
        this.serviceDocument = this.service.serviceDocument;
    }

    save(saveOnly: boolean): void {
        if (this.serviceDocument.dataProfile.profileForm.valid) {
            this.saveSubscription(saveOnly);
        } else {
            this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
        }
    }

    saveSubscription(saveOnly: boolean): void {
        this.checkStoreFormat(saveOnly);
  }

    saveStoreFormat(saveOnly: boolean): void {
        this.service.save().subscribe(() => {
            if (this.service.serviceDocument.result.type === MessageType.success) {
                this.sharedService.saveForm("StoreFormat saved successfully.").subscribe(() => {
                    if (saveOnly) {
                        this.router.navigate(["/" + this.redirectUrl + "/List"], { skipLocationChange: true });
                    } else {
                        if (!this.editMode) {
                          this.model = { storeFormat: null, formatName: null, operation: "I" };                            
                            this.service.newModel(this.model);
                        } else {
                            Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
                            this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.service.serviceDocument.dataProfile.dataModel);
                            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
                        }
                    }
                }, (error: string) => { console.log(error); });
            } else {
                this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
            }
        }, (error: string) => { console.log(error); });
    }

    checkStoreFormat(saveOnly: boolean): void {
        if (!this.editMode) {
          this.service.isExistingStoreFormat(this.serviceDocument.dataProfile.profileForm.controls["storeFormat"].value).subscribe((response: any) => {
                if (response) {
                    this.sharedService.errorForm("StoreFormat already exisit.");
                } else {
                    this.saveStoreFormat(saveOnly);
                }
            });
        } else {
            this.saveStoreFormat(saveOnly);
        }
    }

    reset(): void {
        if (!this.editMode) {
            this.model = {
              storeFormat: null, formatName: null, operation: "I"
            };
           
            this.service.serviceDocument.dataProfile.dataModel = this.model;
            this.service.newModel(this.model);

        } else {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);            
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
        }
        this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
    }
}
