import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { StoreFormatListComponent } from "./StoreFormatListComponent";
import { StoreFormatService } from "./StoreFormatService";
import { StoreFormatRoutingModule } from "./StoreFormatRoutingModule";
import { StoreFormatComponent } from "./StoreFormatComponent";
import { StoreFormatListResolver, StoreFormatResolver } from "./StoreFormatResolver";
import { RbsSharedModule } from '../RbsSharedModule';
import { AgGridSharedModule } from '../AgGridSharedModule';
import { TitleSharedModule } from '../TitleSharedModule';

@NgModule({
    imports: [
        FrameworkCoreModule,
        StoreFormatRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        StoreFormatListComponent,
        StoreFormatComponent,
    ],
    providers: [
        StoreFormatService,
        StoreFormatListResolver,
        StoreFormatResolver,
    ]
})
export class StoreFormatModule {
    constructor() {
        LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
    }
}
