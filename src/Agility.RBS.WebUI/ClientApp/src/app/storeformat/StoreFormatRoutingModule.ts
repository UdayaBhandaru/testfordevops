import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { StoreFormatListComponent } from "./StoreFormatListComponent";
import { StoreFormatListResolver, StoreFormatResolver} from "./StoreFormatResolver";
import { StoreFormatComponent } from "./StoreFormatComponent";
const UtilityRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
              component: StoreFormatListComponent,
                resolve:
                {
                    serviceDocument: StoreFormatListResolver
                }
            },
            {
                path: "New",
                component: StoreFormatComponent,
                resolve:
                {
                    serviceDocument: StoreFormatResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(UtilityRoutes)
    ]
})
export class StoreFormatRoutingModule { }
