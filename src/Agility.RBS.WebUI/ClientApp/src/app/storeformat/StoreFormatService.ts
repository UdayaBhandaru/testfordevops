import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { StoreFormatModel } from "./StoreFormatModel";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from '../Common/HttpHelperService';

@Injectable()
export class StoreFormatService {
    serviceDocument: ServiceDocument<StoreFormatModel> = new ServiceDocument<StoreFormatModel>();
    constructor(private httpHelperService: HttpHelperService) { }

    newModel(model: StoreFormatModel): ServiceDocument<StoreFormatModel> {
        return this.serviceDocument.newModel(model);
    }

    search(): Observable<ServiceDocument<StoreFormatModel>> {
      return this.serviceDocument.search("/api/StoreFormat/Search");
    }

    save(): Observable<ServiceDocument<StoreFormatModel>> {
      return this.serviceDocument.save("/api/StoreFormat/Save", true);
    }

    list(): Observable<ServiceDocument<StoreFormatModel>> {
      return this.serviceDocument.list("/api/StoreFormat/List");
    }

    addEdit(): Observable<ServiceDocument<StoreFormatModel>> {
      return this.serviceDocument.list("/api/StoreFormat/New");
    }
  isExistingStoreFormat(storeFormat: number): Observable<boolean> {
    return this.httpHelperService.get("/api/StoreFormat/IsExistingStoreFormat", new HttpParams().set("storeFormat", storeFormat.toString()));
    }
}
