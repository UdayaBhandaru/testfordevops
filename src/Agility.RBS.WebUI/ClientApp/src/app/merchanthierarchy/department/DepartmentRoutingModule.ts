﻿import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { DepartmentListResolver, DepartmentResolver } from "./DepartmentResolver";
import { DepartmentListComponent } from "./DepartmentListComponent";
import { DepartmentComponent } from "./DepartmentComponent";
const MerchantHierarchyRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: DepartmentListComponent,
                resolve:
                {
                    serviceDocument: DepartmentListResolver
                }
            },
            {
                path: "New",
                component: DepartmentComponent,
                resolve:
                {
                    serviceDocument: DepartmentResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(MerchantHierarchyRoutes)
    ]
})
export class DepartmentRoutingModule { }
