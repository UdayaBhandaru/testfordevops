﻿import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { DepartmentRoutingModule } from "./DepartmentRoutingModule";
import { DepartmentListResolver, DepartmentResolver } from "./DepartmentResolver";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { DepartmentListComponent } from "./DepartmentListComponent";
import { DepartmentComponent } from "./DepartmentComponent";
import { DepartmentService } from "./DepartmentService";
import { RbsSharedModule } from "../../RbsSharedModule";
@NgModule({
    imports: [
        FrameworkCoreModule,
        DepartmentRoutingModule,
        AgGridModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        DepartmentListComponent,
        DepartmentComponent,
    ],
    providers: [
        DepartmentListResolver, DepartmentResolver, DepartmentService
    ]
})
export class DepartmentModule {
}
