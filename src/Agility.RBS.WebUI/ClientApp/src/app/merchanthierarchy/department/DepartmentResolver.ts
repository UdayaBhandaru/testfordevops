﻿import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { DepartmentModel } from "./DepartmentModel";
import { DepartmentService } from "./DepartmentService";
@Injectable()
export class DepartmentListResolver implements Resolve<ServiceDocument<DepartmentModel>> {
    constructor(private service: DepartmentService) { }
    resolve(): Observable<ServiceDocument<DepartmentModel>> {
        return this.service.list();
    }
}

@Injectable()
export class DepartmentResolver implements Resolve<ServiceDocument<DepartmentModel>> {
    constructor(private service: DepartmentService) { }
    resolve(): Observable<ServiceDocument<DepartmentModel>> {
        return this.service.addEdit();
    }
}
