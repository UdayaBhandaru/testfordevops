﻿import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { DepartmentModel } from "./DepartmentModel";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";

@Injectable()
export class DepartmentService {
    serviceDocument: ServiceDocument<DepartmentModel> = new ServiceDocument<DepartmentModel>();
    constructor(private httpHelperService: HttpHelperService) { }

    newModel(model: DepartmentModel): ServiceDocument<DepartmentModel> {
        return this.serviceDocument.newModel(model);
    }

    search(): Observable<ServiceDocument<DepartmentModel>> {
        return this.serviceDocument.search("/api/department/Search");
    }

    save(): Observable<ServiceDocument<DepartmentModel>> {
        return this.serviceDocument.save("/api/department/Save", true);
    }

    list(): Observable<ServiceDocument<DepartmentModel>> {
        return this.serviceDocument.list("/api/department/List");
    }

    addEdit(): Observable<ServiceDocument<DepartmentModel>> {
        return this.serviceDocument.list("/api/department/New");
    }

    isExistingDept(groupId: number, id: number): Observable<boolean> {
        return this.httpHelperService.get("/api/department/IsExistingDepartment", new HttpParams().set("id", id.toString())
            .set("groupId", groupId.toString()));
    }
}