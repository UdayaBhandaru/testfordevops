export class DepartmentModel {
    groupNo: number;
    groupName?: string;
    dept: number;
    deptName: string;
    buyer?: number;
    merch?: number;
    merchName?: string;
    buyerName?: string;
    profitCalcType?: string;
    purchaseType?: string;
    budInt?: number;
    budMkup?: number;
    totalMarketAmt?: number;
    markupCalcType?: string;
    otbCalcType?: string;
    maxAvgCounter?: number;
    avgTolerancePct?: number;
    deptVatInclInd?: string;
    tableName?: string;
    operation?: string;
}


