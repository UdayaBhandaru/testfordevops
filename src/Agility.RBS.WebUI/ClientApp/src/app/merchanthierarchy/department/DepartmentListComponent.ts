import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { DepartmentModel } from "./DepartmentModel";
import { DepartmentService } from "./DepartmentService";
import { FormGroup } from "@angular/forms";
import { SharedService } from "../../Common/SharedService";
import { GridEditComponent } from "../../Common/grid/GridEditComponent";
import { GridExportModel } from "../../Common/grid/GridExportModel";
import { InfoComponent } from "../../Common/InfoComponent";
import { GroupDomainModel } from "../../Common/DomainData/GroupDomainModel";
import { GridOptions } from 'ag-grid-community';

@Component({
  selector: "department-list",
  templateUrl: "./DepartmentListComponent.html"
})

export class DepartmentListComponent implements OnInit {
  serviceDocument: ServiceDocument<DepartmentModel>;
  PageTitle = "Category";
  redirectUrl = "Department";
  componentName: any;
  groupData: GroupDomainModel[];
  columns: any[];
  model: DepartmentModel;
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  gridOptions: GridOptions;

  constructor(public service: DepartmentService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "Department", field: "groupName", tooltipField: "groupName" },
      { headerName: "Id", field: "dept", tooltipField: "dept" },
      { headerName: "Name", field: "deptName", tooltipField: "deptName" },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 90
      }
    ];

    this.groupData = Object.assign([], this.service.serviceDocument.domainData["group"]);

    this.sharedService.domainData = {
      group: this.groupData
    };

    this.model = { groupNo: null, dept: null, deptName: null };
    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }
}
