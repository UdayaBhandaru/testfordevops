import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { DepartmentModel } from "./DepartmentModel";
import { DepartmentService } from "./DepartmentService";
import { SharedService } from "../../Common/SharedService";
import { GroupModel } from "../group/GroupModel";
import { ClassModel } from "../class/ClassModel";
import { FormGroup, Validators } from "@angular/forms";
import { BuyerDomainModel } from '../../Common/DomainData/BuyerDomainModel';
import { MerchDomainModel } from '../../Common/DomainData/MerchDomainModel';
import { GroupDomainModel } from '../../Common/DomainData/GroupDomainModel';
import { DomainDetailModel } from '../../domain/DomainDetailModel';
import { radioButtonValidator } from '../../organizationhierarchy/location/LocationComponent';

@Component({
  selector: "department",
  templateUrl: "./DepartmentComponent.html"
})

export class DepartmentComponent implements OnInit {
  PageTitle = "Category";
  redirectUrl = "Department";
  serviceDocument: ServiceDocument<DepartmentModel>;
  editMode: boolean = false;
  editModel: any;
  localizationData: any;
  groupData: GroupDomainModel[];
  model: DepartmentModel;
  classModel: ClassModel;
  groupModel: GroupModel;
  domainData: any[];
  buyerData: BuyerDomainModel[];
  merchData: MerchDomainModel[];

  profitCalcTypeDtlData: DomainDetailModel[];
  otbCalcTypeDtlData: DomainDetailModel[];
  purchTypesDtlData: DomainDetailModel[];
  markupCalTypeDtlData: DomainDetailModel[];
  indicatorData: DomainDetailModel[];
  mandotryFieldList: string[] = [     
    "markupCalcType",
    "deptVatInclInd",
    "otbCalcType",
    "profitCalcType"
  ];

  constructor(private service: DepartmentService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.groupData = Object.assign([], this.service.serviceDocument.domainData["group"]);
    this.domainData = [{ "code": 1, "codeName": "YES" }, { "code": 2, "codeName": "NO" }];
    this.buyerData = this.service.serviceDocument.domainData["buyer"];
    this.merchData = this.service.serviceDocument.domainData["merch"];
    this.profitCalcTypeDtlData = Object.assign([], this.service.serviceDocument.domainData["profitCalcType"]);
    this.otbCalcTypeDtlData = Object.assign([], this.service.serviceDocument.domainData["otbCalcType"]);
    this.purchTypesDtlData = Object.assign([], this.service.serviceDocument.domainData["purchTypes"]);
    this.markupCalTypeDtlData = Object.assign([], this.service.serviceDocument.domainData["markupCalType"]);
    this.indicatorData = Object.assign([], this.service.serviceDocument.domainData["indicatorStatus"]);


    this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
    this.bind();
  }
  ngAfterViewInit(): void {
    this.addValidators();
  }

  bind(): void {
    delete this.editModel;
    if (this.sharedService.editObject) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
      this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
      delete this.sharedService.editObject;
      this.editMode = true;
    } else {
      this.model = {
        groupNo: null, dept: null, deptName: null, operation: "I", avgTolerancePct: 1.00, budInt: null, budMkup: null
        , buyer: null, deptVatInclInd: null, markupCalcType: null, maxAvgCounter: 1, merch: null, otbCalcType: null
        , profitCalcType: null, purchaseType: null, totalMarketAmt: null
      };
      this.service.serviceDocument.dataProfile.dataModel = this.model;
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  save(saveOnly: boolean): void {
    let fm: FormGroup = this.serviceDocument.dataProfile.profileForm;
    this.mandotryFieldList.forEach(x => fm.controls[x].setValidators([Validators.required]));
    this.mandotryFieldList.forEach(x => fm.controls[x].updateValueAndValidity());
    this.sharedService.validateForm(fm);
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.saveSubscription(saveOnly);
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }


  addValidators(): void {
    let lfg: FormGroup = this.serviceDocument.dataProfile.profileForm;
    this.mandotryFieldList.forEach(x => lfg.controls[x].setValidators([Validators.required, radioButtonValidator("Required*")]));
  }

  saveSubscription(saveOnly: boolean): void {
    this.checkCategory(saveOnly);
  }

  saveCategory(saveOnly: boolean): void {
    this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.sharedService.saveForm("Category saved successfully").subscribe(() => {
          if (saveOnly) {
            this.router.navigate(["/" + this.redirectUrl + "/List"], { skipLocationChange: true });
          } else {
            if (!this.editMode) {
              this.model = {
                groupNo: null, dept: null, deptName: null, operation: "I", avgTolerancePct: 1.00, budInt: null, budMkup: null
                , buyer: null, deptVatInclInd: null, markupCalcType: null, maxAvgCounter: 1, merch: null, otbCalcType: null
                , profitCalcType: null, purchaseType: null, totalMarketAmt: null
              };
              this.service.serviceDocument.dataProfile.dataModel = this.model;
              this.service.newModel(this.model);
            } else {
              Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
              this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.service.serviceDocument.dataProfile.dataModel);
              this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            }
          }
        }, (error: string) => { console.log(error); });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
    }, (error: string) => { console.log(error); });
  }

  checkCategory(saveOnly: boolean): void {
    if (!this.editMode) {
      let form: FormGroup = this.serviceDocument.dataProfile.profileForm;
      if (form.controls["dept"].value && form.controls["groupNo"].value) {
        this.service.isExistingDept(form.controls["groupNo"].value, form.controls["dept"].value).subscribe
          ((response: boolean) => {
            if (response) {
              this.sharedService.errorForm(this.localizationData.department.departmentcheck);
            } else {
              this.saveCategory(saveOnly);
            }
          });
      }
    } else {
      this.saveCategory(saveOnly);
    }
  }

    reset(): void {
        if (!this.editMode) {
            this.model = {
                groupNo: null, dept: null, deptName: null, operation: "I", avgTolerancePct: 1.00, budInt: null, budMkup: null
                , buyer: null, deptVatInclInd: null, markupCalcType: null, maxAvgCounter: 1, merch: null, otbCalcType: null
                , profitCalcType: null, purchaseType: null, totalMarketAmt: null
            };
            this.service.serviceDocument.dataProfile.dataModel = this.model;
            this.service.newModel(this.model);
        } else {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
        }
        this.serviceDocument = this.service.serviceDocument;
        this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
    }
}
