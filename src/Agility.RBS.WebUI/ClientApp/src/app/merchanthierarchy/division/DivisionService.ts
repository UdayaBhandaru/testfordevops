﻿import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { DivisionModel } from "./DivisionModel";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";

@Injectable()
export class DivisionService {
    serviceDocument: ServiceDocument<DivisionModel> = new ServiceDocument<DivisionModel>();
    constructor(private httpHelperService: HttpHelperService) { }

    newModel(model: DivisionModel): ServiceDocument<DivisionModel> {
        return this.serviceDocument.newModel(model);
    }

    search(): Observable<ServiceDocument<DivisionModel>> {
        return this.serviceDocument.search("/api/Division/Search");
    }

    save(): Observable<ServiceDocument<DivisionModel>> {
        return this.serviceDocument.save("/api/Division/Save", true);
    }

    list(): Observable<ServiceDocument<DivisionModel>> {
        return this.serviceDocument.list("/api/Division/List");
    }

    addEdit(): Observable<ServiceDocument<DivisionModel>> {
        return this.serviceDocument.list("/api/Division/New");
    }

    isExistingDivision(id: number): Observable<boolean> {
        return this.httpHelperService.get("/api/Division/IsExistingDivision", new HttpParams().set("id", id.toString()));
    }
}