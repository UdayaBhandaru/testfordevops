﻿import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { DivisionModel } from "./DivisionModel";
import { DivisionService } from "./DivisionService";
@Injectable()
export class DivisionListResolver implements Resolve<ServiceDocument<DivisionModel>> {
    constructor(private service: DivisionService) { }
    resolve(): Observable<ServiceDocument<DivisionModel>> {
        return this.service.list();
    }
}

@Injectable()
export class DivisionResolver implements Resolve<ServiceDocument<DivisionModel>> {
    constructor(private service: DivisionService) { }
    resolve(): Observable<ServiceDocument<DivisionModel>> {
        return this.service.addEdit();
    }
}
