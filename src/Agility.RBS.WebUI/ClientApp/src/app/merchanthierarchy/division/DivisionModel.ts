﻿export class DivisionModel {
    division: number;
    divName: string;
    buyer: number;
    buyerName?: string;
    merch: number;
    merchName?: string;
    totalMarketAmt: number;
    operation?: string;
    tableName?: string;
}