import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, FormMode } from "@agility/frameworkcore";
import { DivisionModel } from "./DivisionModel";
import { DivisionService } from "./DivisionService";
import { FormGroup } from "@angular/forms";
import { SharedService } from "../../Common/SharedService";
import { GridOptions } from "ag-grid-community";
import { GridEditComponent } from "../../Common/grid/GridEditComponent";
import { GridExportModel } from "../../Common/grid/GridExportModel";
import { InfoComponent } from "../../Common/InfoComponent";
import { BuyerDomainModel } from "../../Common/DomainData/BuyerDomainModel";
import { MerchDomainModel } from "../../Common/DomainData/MerchDomainModel";

@Component({
  selector: "division-list",
  templateUrl: "./DivisionListComponent.html"
})
export class DivisionListComponent implements OnInit {
  serviceDocument: ServiceDocument<DivisionModel>;
  PageTitle = "Division";
  redirectUrl = "Division";
  componentName: any;
  buyerData: BuyerDomainModel[];
  merchData: MerchDomainModel[];
  columns: any[];
  model: DivisionModel;
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  gridOptions: GridOptions;

  constructor(public service: DivisionService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "Division", field: "division", tooltipField: "division", width: 50 },
      { headerName: "Name", field: "divName", tooltipField: "divName" },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 80
      }
    ];

    this.buyerData = this.service.serviceDocument.domainData["buyer"];
    this.merchData = this.service.serviceDocument.domainData["merch"];

    this.sharedService.domainData = { buyer: this.buyerData, merch: this.merchData };
    this.model = { division: null, divName: null, buyer: null, merch: null, totalMarketAmt: null };

    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }
}
