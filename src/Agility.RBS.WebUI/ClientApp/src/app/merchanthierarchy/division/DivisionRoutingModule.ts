﻿import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { DivisionListResolver, DivisionResolver } from "./DivisionResolver";
import { DivisionListComponent } from "./DivisionListComponent";
import { DivisionComponent } from "./DivisionComponent";
const MerchantHierarchyRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: DivisionListComponent,
                resolve:
                {
                    serviceDocument: DivisionListResolver
                }
            },
            {
                path: "New",
                component: DivisionComponent,
                resolve:
                {
                    serviceDocument: DivisionResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(MerchantHierarchyRoutes)
    ]
})
export class DivisionRoutingModule { }
