import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { DivisionModel } from "./DivisionModel";
import { DivisionService } from "./DivisionService";
import { SharedService } from "../../Common/SharedService";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { UserProfileModel } from "../../account/UserProfileModel";
import { GroupModel } from "../group/GroupModel";
import { DepartmentModel } from "../department/DepartmentModel";
import { FormGroup } from "@angular/forms";
import { BuyerDomainModel } from "../../Common/DomainData/BuyerDomainModel";
import { MerchDomainModel } from "../../Common/DomainData/MerchDomainModel";

@Component({
    selector: "division",
  templateUrl: "./DivisionComponent.html"
})

export class DivisionComponent implements OnInit {
    PageTitle = "Division";
    redirectUrl = "Division";
    serviceDocument: ServiceDocument<DivisionModel>;
    editMode: boolean = false;
    showProxy: boolean = false;
    editModel: any;
    localizationData: any;
    checkManager: boolean = true;
    defaultStatus: string;
    buyerData: BuyerDomainModel[];
    merchData: MerchDomainModel[];
    domainDtlData: DomainDetailModel[];
    model: DivisionModel;
    departmentModel: GroupModel;
    categoryModel: DepartmentModel;
    constructor(private service: DivisionService, private sharedService: SharedService, private router: Router) {
    }

    ngOnInit(): void {
        this.buyerData = this.service.serviceDocument.domainData["buyer"];
        this.merchData = this.service.serviceDocument.domainData["merch"];
        this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
        this.bind();
    }

    bind(): void {
        delete this.editModel;
        if (this.sharedService.editObject) {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
            this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            delete this.sharedService.editObject;
            this.editMode = true;
        } else {
            this.model = { division: null, divName: null, buyer: null, merch: null, totalMarketAmt: null, operation: "I" };
            this.service.serviceDocument.dataProfile.dataModel = this.model;
            this.service.newModel(this.model);
        }
        this.serviceDocument = this.service.serviceDocument;
    }

    checkDivision(saveOnly: boolean): void {
        if (!this.editMode) {
            let form: FormGroup = this.serviceDocument.dataProfile.profileForm;
            this.service.isExistingDivision(form.controls["division"].value)
                .subscribe((response: boolean) => {
                    if (response) {
                        this.sharedService.errorForm(this.localizationData.division.divisioncheck);
                    } else {
                        this.saveDivision(saveOnly);
                    }
                });
        } else {
            this.saveDivision(saveOnly);
        }
    }

    save(saveOnly: boolean): void {
        if (this.serviceDocument.dataProfile.profileForm.valid) {
            this.saveSubscription(saveOnly);
        } else {
            this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
        }
    }

    saveSubscription(saveOnly: boolean): void {
        this.checkDivision(saveOnly);
    }

    private saveDivision(saveOnly: boolean): void {
        this.service.save().subscribe(() => {
            if (this.service.serviceDocument.result.type === MessageType.success) {
                this.sharedService.saveForm(this.localizationData.division.divisionsave).subscribe(() => {
                    if (saveOnly) {
                        this.router.navigate(["/Division/List"], { skipLocationChange: true });
                    } else {
                        if (!this.editMode) {
                            this.model = { division: null, divName: null, buyer: null, merch: null, totalMarketAmt: null, operation: "I" };
                            this.service.serviceDocument.dataProfile.dataModel = this.model;
                            this.service.newModel(this.model);
                        } else {
                            Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
                            this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.editModel);
                            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
                        }
                    }
                }, (error: string) => { console.log(error); });
            } else {
                this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
            }
        }, (error: string) => { console.log(error); });
    }

    reset(): void {
        if (!this.editMode) {
            this.model = { division: null, divName: null, buyer: null, merch: null, totalMarketAmt: null, operation: "I" };
            this.service.newModel(this.model);
        } else {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
        }
        this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
    }

    openDepartmentMaster($event: MouseEvent): void {
        $event.preventDefault();
        this.departmentModel = {
            division: this.service.serviceDocument.dataProfile.dataModel.division
            , groupNo: null, groupName: null, buyer: null, merch: null, operation: null
        };
        this.sharedService.searchData = Object.assign({}, this.departmentModel);
        this.router.navigate(["/Department"], { skipLocationChange: true });
    }
}
