﻿import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { DivisionRoutingModule } from "./DivisionRoutingModule";
import { DivisionListResolver, DivisionResolver } from "./DivisionResolver";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { DivisionListComponent } from "./DivisionListComponent";
import { DivisionComponent } from "./DivisionComponent";
import { DivisionService } from "./DivisionService";
import { RbsSharedModule } from "../../RbsSharedModule";
@NgModule({
    imports: [
        FrameworkCoreModule,
        DivisionRoutingModule,
        AgGridModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        DivisionListComponent,
        DivisionComponent
    ],
    providers: [
        DivisionListResolver, DivisionResolver, DivisionService
    ]
})
export class DivisionModule {
}
