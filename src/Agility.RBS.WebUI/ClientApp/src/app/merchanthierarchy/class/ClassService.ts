import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { ClassModel } from "./ClassModel";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";

@Injectable()
export class ClassService {
    serviceDocument: ServiceDocument<ClassModel> = new ServiceDocument<ClassModel>();
    constructor(private httpHelperService: HttpHelperService) { }

    newModel(model: ClassModel): ServiceDocument<ClassModel> {
        return this.serviceDocument.newModel(model);
    }

    search(): Observable<ServiceDocument<ClassModel>> {
      return this.serviceDocument.search("/api/Class/Search");
    }

    save(): Observable<ServiceDocument<ClassModel>> {
      return this.serviceDocument.save("/api/Class/Save", true);
    }

    list(): Observable<ServiceDocument<ClassModel>> {
      return this.serviceDocument.list("/api/Class/List");
    }

    addEdit(): Observable<ServiceDocument<ClassModel>> {
      return this.serviceDocument.list("/api/Class/New");
    }

  isExistingClass(zoneGroupId: string): Observable<boolean> {
    return this.httpHelperService.get("/api/Class/IsExistingClass", new HttpParams().set("zoneGroupId", zoneGroupId));
    }
}
