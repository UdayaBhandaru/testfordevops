import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { ClassService } from "./ClassService";
import { ClassModel } from "./ClassModel";
@Injectable()
export class ClassListResolver implements Resolve<ServiceDocument<ClassModel>> {
    constructor(private service: ClassService) { }
    resolve(): Observable<ServiceDocument<ClassModel>> {
        return this.service.list();
    }
}

@Injectable()
export class ClassResolver implements Resolve<ServiceDocument<ClassModel>> {
    constructor(private service: ClassService) { }
    resolve(): Observable<ServiceDocument<ClassModel>> {
        return this.service.addEdit();
    }
}



