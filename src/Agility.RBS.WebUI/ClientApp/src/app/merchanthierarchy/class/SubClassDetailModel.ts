export class SubClassDetailModel {
  class?: number;
  className?: string;
  dept: number;
  deptName?: string;  
  subclass?: number;
  subName: string;
  operation?: string;
}
