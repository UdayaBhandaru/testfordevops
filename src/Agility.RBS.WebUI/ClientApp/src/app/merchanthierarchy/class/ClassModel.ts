import { SubClassDetailModel } from './SubClassDetailModel';

export class ClassModel {
  dept: number;
  deptDesc?: string;
  class: number;
  className: string;
  classVatInd?: string;
  operation?: string;
  tableName?: string;
  subClassDetailList?: SubClassDetailModel[];
}
