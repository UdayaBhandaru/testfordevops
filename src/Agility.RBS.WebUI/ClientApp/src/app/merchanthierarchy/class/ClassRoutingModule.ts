import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ClassListComponent } from "./ClassListComponent";
import { ClassListResolver, ClassResolver } from "./ClassResolver";
import { ClassComponent } from "./ClassComponent";
const UtilityRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "", redirectTo: "List", pathMatch: "full"
      },
      {
        path: "List",
        component: ClassListComponent,
        resolve:
        {
          serviceDocument: ClassListResolver
        }
      },
      {
        path: "New",
        component: ClassComponent,
        resolve:
        {
          serviceDocument: ClassResolver
        }
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(UtilityRoutes)
  ]
})
export class ClassRoutingModule { }
