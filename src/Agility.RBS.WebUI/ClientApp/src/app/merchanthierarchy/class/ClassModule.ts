import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { ClassListComponent } from "./ClassListComponent";
import { ClassService } from "./ClassService";
import { ClassRoutingModule } from "./ClassRoutingModule";
import { ClassComponent } from "./ClassComponent";
import { ClassListResolver, ClassResolver } from "./ClassResolver";
import { RbsSharedModule } from "../../RbsSharedModule";

@NgModule({
    imports: [
        FrameworkCoreModule,
        ClassRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        ClassListComponent,
        ClassComponent,
    ],
    providers: [
        ClassService,
        ClassListResolver,
        ClassResolver,
    ]
})
export class ClassModule {
    constructor() {
        LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
    }
}
