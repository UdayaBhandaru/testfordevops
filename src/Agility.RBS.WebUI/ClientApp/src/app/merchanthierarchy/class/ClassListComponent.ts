import { Component, OnInit } from "@angular/core";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { GridOptions } from "ag-grid-community";
import { GridEditComponent } from "../../Common/grid/GridEditComponent";
import { InfoComponent } from "../../Common/InfoComponent";
import { ClassModel } from "./ClassModel";
import { ClassService } from "./ClassService";
import { SharedService } from "../../Common/SharedService";
import { GridExportModel } from "../../Common/grid/GridExportModel";
import { DepartmentDomainModel } from '../../Common/DomainData/DepartmentDomainModel';

@Component({
  selector: "Class-list",
  templateUrl: "./ClassListComponent.html"
})
export class ClassListComponent implements OnInit {
  PageTitle = "FineLine";
  redirectUrl = "Class";
  componentName: any;
  serviceDocument: ServiceDocument<ClassModel>;
  public gridOptions: GridOptions;
  columns: any[];
  model: ClassModel;
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  departmentData: DepartmentDomainModel[];

  constructor(public service: ClassService, private sharedService: SharedService) {
  }

  ngOnInit(): void {
    this.departmentData = this.service.serviceDocument.domainData["department"];
    this.componentName = this;
    this.columns = [
      { headerName: "Category", field: "dept", tooltipField: "dept", width: 30 },
      { headerName: "FineLine", field: "className", tooltipField: "className" },
      { headerName: "class", field: "class", tooltipField: "class" },
      { headerName: "Class Vat Indicator", field: "classVatInd", tooltipField: "classVatInd", width: 45 },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 35
      }
    ];
    this.classHeaderModel();
    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData, this.sharedService.searchData.Zone);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }

  private classHeaderModel() {
    this.model = {
      dept: null, class: null, className: null
    };
  }
}
