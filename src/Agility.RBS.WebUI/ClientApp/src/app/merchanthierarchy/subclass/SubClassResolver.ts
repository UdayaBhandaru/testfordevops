﻿import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { SubClassModel } from "./SubClassModel";
import { SubClassService } from "./SubClassService";
@Injectable()
export class SubClassListResolver implements Resolve<ServiceDocument<SubClassModel>> {
    constructor(private service: SubClassService) { }
    resolve(): Observable<ServiceDocument<SubClassModel>> {
        return this.service.list();
    }
}

@Injectable()
export class SubClassResolver implements Resolve<ServiceDocument<SubClassModel>> {
    constructor(private service: SubClassService) { }
    resolve(): Observable<ServiceDocument<SubClassModel>> {
        return this.service.addEdit();
    }
}
