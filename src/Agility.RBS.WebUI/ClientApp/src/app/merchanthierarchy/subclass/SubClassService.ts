﻿import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { SubClassModel } from "./SubClassModel";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";

@Injectable()
export class SubClassService {
    serviceDocument: ServiceDocument<SubClassModel> = new ServiceDocument<SubClassModel>();
    constructor(private httpHelperService: HttpHelperService) { }

    newModel(model: SubClassModel): ServiceDocument<SubClassModel> {
        return this.serviceDocument.newModel(model);
    }

    search(): Observable<ServiceDocument<SubClassModel>> {
        return this.serviceDocument.search("/api/subclass/Search");
    }

    list(): Observable<ServiceDocument<SubClassModel>> {
        return this.serviceDocument.list("/api/subclass/List");
    }

    addEdit(): Observable<ServiceDocument<SubClassModel>> {
        return this.serviceDocument.list("/api/subclass/New");
    }

    save(): Observable<ServiceDocument<SubClassModel>> {
        return this.serviceDocument.save("/api/subclass/Save", true);
    }

    isExistingSubClass(deptId: number, classId: number, id: number): Observable<boolean> {
        return this.httpHelperService.get("/api/subclass/IsExistingSubClass", new HttpParams().set("id", id.toString())
            .set("deptId", deptId.toString()).set("classId", classId.toString()));
    }
}