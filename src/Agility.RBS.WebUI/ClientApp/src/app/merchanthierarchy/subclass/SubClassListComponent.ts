import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { SubClassModel } from "./SubClassModel";
import { SubClassService } from "./SubClassService";
import { FormGroup } from "@angular/forms";
import { GridOptions } from "ag-grid-community";
import { GridExportModel } from '../../Common/grid/GridExportModel';
import { DepartmentDomainModel } from '../../Common/DomainData/DepartmentDomainModel';
import { ClassDomainModel } from '../../Common/DomainData/ClassDomainModel';
import { SharedService } from '../../Common/SharedService';
import { GridEditComponent } from '../../Common/grid/GridEditComponent';
import { InfoComponent } from '../../Common/InfoComponent';

@Component({
  selector: "sub-class-list",
  templateUrl: "./SubClassListComponent.html"
})
export class SubClassListComponent implements OnInit {
  PageTitle = "Segment";
  redirectUrl = "SubClass";
  componentName: any;
  serviceDocument: ServiceDocument<SubClassModel>;
  deptData: DepartmentDomainModel[];
  classData: ClassDomainModel[]; classList: ClassDomainModel[];
  columns: any[];
  model: SubClassModel;
  public gridOptions: GridOptions;
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;

  constructor(public service: SubClassService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "Category", field: "deptName", tooltipField: "deptName", width: 120 },
      { headerName: "Fine Line", field: "className", tooltipField: "className", width: 100 },
      { headerName: "Segment", field: "subclass", tooltipField: "subclass", width: 60 },
      { headerName: "Name", field: "subName", tooltipField: "subName" },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 90
      }
    ];

    this.deptData = Object.assign([], this.service.serviceDocument.domainData["department"]);
    this.classList = Object.assign([], this.service.serviceDocument.domainData["class"]);
    this.classData = this.classList;
    this.sharedService.domainData = { department: this.deptData };
    this.model = { dept: null, class: null, subclass: null, subName: null };
    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.classData = this.classList.filter(item => item.dept === this.service.serviceDocument.dataProfile.dataModel.dept);
      this.sharedService.domainData["class"] = this.classData;
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }

  changeDepartment(): void {
    this.serviceDocument.dataProfile.profileForm.controls["class"].setValue(null);
    this.classData = this.classList.filter(item => item.dept === +this.serviceDocument.dataProfile.profileForm.controls["dept"].value);
    this.sharedService.domainData["class"] = this.classData;
  }

  reset(): void {
    this.classData = this.classList;
  }
}
