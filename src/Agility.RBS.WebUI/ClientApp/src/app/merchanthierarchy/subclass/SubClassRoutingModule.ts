﻿import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { SubClassListResolver, SubClassResolver } from "./SubClassResolver";
import { SubClassListComponent } from "./SubClassListComponent";
import { SubClassComponent } from "./SubClassComponent";
const MerchantHierarchyRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: SubClassListComponent,
                resolve:
                {
                    serviceDocument: SubClassListResolver
                }
            },
            {
                path: "New",
                component: SubClassComponent,
                resolve:
                {
                    serviceDocument: SubClassResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(MerchantHierarchyRoutes)
    ]
})
export class SubClassRoutingModule { }
