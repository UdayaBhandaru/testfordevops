import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { SubClassModel } from "./SubClassModel";
import { SubClassService } from "./SubClassService";
import { SharedService } from "../../Common/SharedService";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
//import { ClassModel } from "../class/ClassModel";
import { CompanyDomainModel } from "../../Common/DomainData/CompanyDomainModel";
import { DivisionDomainModel } from "../../Common/DomainData/DivisionDomainModel";
import { GroupDomainModel } from "../../Common/DomainData/GroupDomainModel";
import { DepartmentDomainModel } from "../../Common/DomainData/DepartmentDomainModel";
import { ClassDomainModel } from "../../Common/DomainData/ClassDomainModel";
import { UserDomainModel } from "../../Common/DomainData/UserDomainModel";
import { FormGroup } from "@angular/forms";

@Component({
    selector: "sub-class",
  templateUrl: "./SubClassComponent.html"
})

export class SubClassComponent implements OnInit {
    PageTitle = "Segment";
    redirectUrl = "SubClass";
    serviceDocument: ServiceDocument<SubClassModel>;
    editModel: any;
    editMode: boolean = false;
    localizationData: any;
    deptData: DepartmentDomainModel[];
    classData: ClassDomainModel[]; classList: ClassDomainModel[];
    model: SubClassModel;
    //classModel: ClassModel;

    constructor(private service: SubClassService, private sharedService: SharedService, private router: Router) {
    }

    ngOnInit(): void {
        this.deptData = Object.assign([], this.service.serviceDocument.domainData["department"]);
        this.classList = Object.assign([], this.service.serviceDocument.domainData["class"]);
        this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
        this.bind();
    }

    bind(): void {
        delete this.editModel;
        if (this.sharedService.editObject) {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
            this.classData = this.classList.filter(item => item.dept === this.service.serviceDocument.dataProfile.dataModel.dept);
            this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            delete this.sharedService.editObject;
            this.editMode = true;
        } else {
            this.model = { dept: null, class: null, subName: null, subclass: null, operation: "I" };
            this.service.serviceDocument.dataProfile.dataModel = this.model;
            this.service.newModel(this.model);
        }
        this.serviceDocument = this.service.serviceDocument;
    }

    save(saveOnly: boolean): void {
        if (this.serviceDocument.dataProfile.profileForm.valid) {
            this.saveSubscription(saveOnly);
        } else {
            this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
        }
    }

    saveSubscription(saveOnly: boolean): void {
        this.checkSubClass(saveOnly);
    }

    saveSubclass(saveOnly: boolean): void {
        this.service.save().subscribe(() => {
            if (this.service.serviceDocument.result.type === MessageType.success) {
                this.sharedService.saveForm(this.localizationData.subClass.subclasssave).subscribe(() => {
                    if (saveOnly) {
                        this.router.navigate(["/" + this.redirectUrl + "/List"], { skipLocationChange: true });
                    } else {
                        if (!this.editMode) {
                            this.model = { dept: null, class: null, subName: null, subclass: null, operation: "I" };
                            this.service.serviceDocument.dataProfile.dataModel = this.model;
                            this.service.newModel(this.model);
                        } else {
                            Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
                            this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.service.serviceDocument.dataProfile.dataModel);
                            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
                        }
                    }
                }, (error: string) => { console.log(error); });
            } else {
                this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
            }
        }, (error: string) => { console.log(error); });
    }

    changeDepartment(): void {
        this.serviceDocument.dataProfile.profileForm.controls["class"].setValue(null);
        this.classData = this.classList.filter(item => item.dept === +this.serviceDocument.dataProfile.profileForm.controls["dept"].value);
    }
    
    checkSubClass(saveOnly: boolean): void {
        if (!this.editMode) {
            let form: FormGroup = this.serviceDocument.dataProfile.profileForm;
            if (form.controls["dept"].value && form.controls["class"].value && form.controls["subclass"].value) {
                this.service.isExistingSubClass(form.controls["dept"].value, form.controls["class"].value, form.controls["subclass"].value)
                    .subscribe((response: boolean) => {
                        if (response) {
                            this.sharedService.errorForm(this.localizationData.subClass.subclasscheck);
                        } else {
                            this.saveSubclass(saveOnly);
                        }
                    });
            }
        } else {
            this.saveSubclass(saveOnly);
        }
    }

    reset(): void {
        if (!this.editMode) {
            this.model = { dept: null, class: null, subName: null, subclass: null, operation: "I" };
            this.service.serviceDocument.dataProfile.dataModel = this.model;
            this.service.newModel(this.model);
        } else {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
        }
        this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
  }

  //openClassMaster($event: MouseEvent): void {
  //  $event.preventDefault();
  //  this.classModel = {
  //    dept: this.service.serviceDocument.dataProfile.dataModel.dept,
  //    deptDesc:null,
  //    class: this.service.serviceDocument.dataProfile.dataModel.class,
  //    className: null,
  //    classVatInd: null
  //  };
  //  this.sharedService.searchData = Object.assign({}, this.classModel);
  //  this.router.navigate(["/Class"], { skipLocationChange: true });
  //}

}
