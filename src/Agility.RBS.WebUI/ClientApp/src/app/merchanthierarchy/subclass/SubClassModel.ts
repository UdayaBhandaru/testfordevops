﻿export class SubClassModel {
    dept: number;
    deptName?: string;
    class: number;
    className?: string;
    subclass: number;
    subName: string;
    operation?: string;
}