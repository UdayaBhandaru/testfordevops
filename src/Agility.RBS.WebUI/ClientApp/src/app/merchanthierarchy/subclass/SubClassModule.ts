﻿import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { SubClassRoutingModule } from "./SubClassRoutingModule";
import { SubClassListResolver, SubClassResolver } from "./SubClassResolver";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { SubClassListComponent } from "./SubClassListComponent";
import { SubClassComponent } from "./SubClassComponent";
import { SubClassService } from "./SubClassService";
import { RbsSharedModule } from "../../RbsSharedModule";
@NgModule({
    imports: [
        FrameworkCoreModule,
        SubClassRoutingModule,
        AgGridModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        SubClassListComponent,
        SubClassComponent
    ],
    providers: [
        SubClassListResolver, SubClassResolver, SubClassService
    ]
})
export class SubClassModule {
}
