import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { GroupModel } from "./GroupModel";
import { GroupService } from "./GroupService";
import { SharedService } from "../../Common/SharedService";
import { DivisionModel } from "../division/DivisionModel";
import { DepartmentModel } from "../department/DepartmentModel";
import { DivisionDomainModel } from "../../Common/DomainData/DivisionDomainModel";
import { FormGroup } from "@angular/forms";
import { DeptBuyerDomainModel } from '../../Common/DomainData/DeptBuyerDomainModel';

@Component({
  selector: "group",
  templateUrl: "./GroupComponent.html"
})
export class GroupComponent implements OnInit {
  PageTitle = "Department";
  redirectUrl = "Group";
  serviceDocument: ServiceDocument<GroupModel>;
  editMode: boolean = false;
  editModel: any;
  localizationData: any;
  divisionData: DivisionDomainModel[];
  divsionModel: DivisionModel;
  categoryModel: DepartmentModel;
  model: GroupModel;
  showCustomRequiredMessage: string;
  buyerData: DeptBuyerDomainModel[];
  merchData: any[];///cli

  constructor(private service: GroupService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
    this.divisionData = this.service.serviceDocument.domainData["division"];
    this.buyerData = this.service.serviceDocument.domainData["buyer"];
    this.merchData = this.service.serviceDocument.domainData["merch"];
    this.bind();
  }

  bind(): void {
    delete this.editModel;
    if (this.sharedService.editObject) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
      this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
      delete this.sharedService.editObject;
      this.editMode = true;
    } else {
      this.model = {
        division: null, groupNo: null, groupName: null, buyer: null, merch: null, operation: "I"
      };
      this.service.serviceDocument.dataProfile.dataModel = this.model;
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  save(saveOnly: boolean): void {
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.saveSubscription(saveOnly);
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  saveSubscription(saveOnly: boolean): void {
    this.checkGroup(saveOnly);
  }

  saveGroup(saveOnly: boolean): void {
    this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.sharedService.saveForm("Department Saved Successfully").subscribe(() => {
          if (saveOnly) {
            this.router.navigate(["/" + this.redirectUrl + "/List"], { skipLocationChange: true });
          } else {
            if (!this.editMode) {
              this.model = {
                division: null, groupNo: null, groupName: null, buyer: null, merch: null, operation: "I"
              };
              this.service.serviceDocument.dataProfile.dataModel = this.model;
              this.service.newModel(this.model);
            } else {
              Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
              this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.service.serviceDocument.dataProfile.dataModel);
              this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            }
            let groupComponentScrollPrintElement: HTMLElement = document.getElementById("printMainComponentDivID");
            groupComponentScrollPrintElement.scrollTop = 0;
          }
        }, (error: string) => { console.log(error); });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
    }, (error: string) => { console.log(error); });
    this.showCustomRequiredMessage = null;
  }

  reset(): void {
    if (!this.editMode) {
      this.model = {
        division: null, groupNo: null, groupName: null, buyer: null, merch: null, operation: "I"
      };
      this.service.serviceDocument.dataProfile.dataModel = this.model;
      this.service.newModel(this.model);
    } else {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    }
    this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
  }

  checkGroup(saveOnly: boolean): void {
    if (!this.editMode) {
      let form: FormGroup = this.serviceDocument.dataProfile.profileForm;
      if (form.controls["division"].value && form.controls["groupNo"].value) {
        this.service.isExistingGroup(form.controls["division"].value, form.controls["groupNo"].value)
          .subscribe((response: boolean) => {
            if (response) {
              this.sharedService.errorForm(this.localizationData.group.groupcheck);
            } else {
              this.saveGroup(saveOnly);
            }
          });
      }
    } else {
      this.saveGroup(saveOnly);
    }
  }
}
