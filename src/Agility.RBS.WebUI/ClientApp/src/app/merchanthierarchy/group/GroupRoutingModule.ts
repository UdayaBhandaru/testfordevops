﻿import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { GroupListResolver, GroupResolver } from "./GroupResolver";
import { GroupListComponent } from "./GroupListComponent";
import { GroupComponent } from "./GroupComponent";
const MerchantHierarchyRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: GroupListComponent,
                resolve:
                {
                    serviceDocument: GroupListResolver
                }
            },
            {
                path: "New",
                component: GroupComponent,
                resolve:
                {
                    serviceDocument: GroupResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(MerchantHierarchyRoutes)
    ]
})
export class GroupRoutingModule { }
