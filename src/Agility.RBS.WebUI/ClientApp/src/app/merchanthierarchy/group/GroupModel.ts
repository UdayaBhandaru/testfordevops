﻿export class GroupModel {
    division: number;
    divName?: string;
    groupNo: number;
    groupName: string;
    buyer: number;
    merch: number;
    merchName?: string
    buyerName?: string
    operation: string
    tableName?: string
}


