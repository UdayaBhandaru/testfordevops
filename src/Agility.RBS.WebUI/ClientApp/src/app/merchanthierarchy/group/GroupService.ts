﻿import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { GroupModel } from "./GroupModel";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";

@Injectable()
export class GroupService {
    serviceDocument: ServiceDocument<GroupModel> = new ServiceDocument<GroupModel>();
    constructor(private httpHelperService: HttpHelperService) { }

    newModel(model: GroupModel): ServiceDocument<GroupModel> {
        return this.serviceDocument.newModel(model);
    }

    search(): Observable<ServiceDocument<GroupModel>> {
        return this.serviceDocument.search("/api/group/Search");
    }

    save(): Observable<ServiceDocument<GroupModel>> {
        return this.serviceDocument.save("/api/group/Save");
    }

    list(): Observable<ServiceDocument<GroupModel>> {
        return this.serviceDocument.list("/api/group/List");
    }

    addEdit(): Observable<ServiceDocument<GroupModel>> {
        return this.serviceDocument.list("/api/group/New");
    }

    isExistingGroup(divisionId: number, id: number): Observable<boolean> {
        return this.httpHelperService.get("/api/group/IsExistingGroup", new HttpParams().set("id", id.toString())
            .set("divisionId", divisionId.toString()));
    }
}