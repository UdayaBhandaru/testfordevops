import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { GroupModel } from "./GroupModel";
import { GroupService } from "./GroupService";
import { FormGroup } from "@angular/forms";
import { SharedService } from "../../Common/SharedService";
import { GridEditComponent } from "../../Common/grid/GridEditComponent";
import { GridExportModel } from "../../Common/grid/GridExportModel";
import { InfoComponent } from "../../Common/InfoComponent";
import { DivisionDomainModel } from "../../Common/DomainData/DivisionDomainModel";
import { GridOptions } from 'ag-grid-community';

@Component({
  selector: "group-list",
  templateUrl: "./GroupListComponent.html"
})
export class GroupListComponent implements OnInit {
  serviceDocument: ServiceDocument<GroupModel>;
  PageTitle = "Department";
  redirectUrl = "Group";
  componentName: any;
  columns: any[];
  divisionData: DivisionDomainModel[];
  model: GroupModel;
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  gridOptions: GridOptions;

  constructor(public service: GroupService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "Division", field: "divName", tooltipField: "divName" },
      { headerName: "Group No", field: "groupNo", tooltipField: "groupNo", width: 55 },
      { headerName: "Name", field: "groupName", tooltipField: "groupName" },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 60
      }
    ];

    this.divisionData = Object.assign([], this.service.serviceDocument.domainData["division"]);

    this.model = { division: null, groupNo: null, groupName: null, buyer: null, merch: null, operation: null };

    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.sharedService.domainData["division"] = this.divisionData;
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }
}
