﻿import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { GroupRoutingModule } from "./GroupRoutingModule";
import { GroupListResolver, GroupResolver } from "./GroupResolver";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { GroupListComponent } from "./GroupListComponent";
import { GroupComponent } from "./GroupComponent";
import { GroupService } from "./GroupService";
import { RbsSharedModule } from "../../RbsSharedModule";
@NgModule({
    imports: [
        FrameworkCoreModule,
        GroupRoutingModule,
        AgGridModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        GroupListComponent,
        GroupComponent
    ],
    providers: [
        GroupListResolver, GroupResolver, GroupService
    ]
})
export class GroupModule {
}
