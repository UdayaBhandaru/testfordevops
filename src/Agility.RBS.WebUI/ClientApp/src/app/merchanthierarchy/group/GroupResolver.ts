﻿import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { GroupModel } from "./GroupModel";
import { GroupService } from "./GroupService";
@Injectable()
export class GroupListResolver implements Resolve<ServiceDocument<GroupModel>> {
    constructor(private service: GroupService) { }
    resolve(): Observable<ServiceDocument<GroupModel>> {
        return this.service.list();
    }
}

@Injectable()
export class GroupResolver implements Resolve<ServiceDocument<GroupModel>> {
    constructor(private service: GroupService) { }
    resolve(): Observable<ServiceDocument<GroupModel>> {
        return this.service.addEdit();
    }
}
