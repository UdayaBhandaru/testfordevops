﻿export class FavouriteModel {
    userId: string;
    favouritePagePath?: string;
    favouritePage?: string;
    createdUpdatedBy?: string;
    createdUpdatedDate?: Date;
    lastUpdatedBy?: string;
    lastUpdatedDate?: Date;
    operation?: string;
}