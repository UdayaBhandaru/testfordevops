﻿import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { FavouriteModel } from "./FavouriteModel";

@Injectable()
export class FavouriteService {
    serviceDocument: ServiceDocument<FavouriteModel> = new ServiceDocument<FavouriteModel>();
    favouriteModel = new FavouriteModel();
    searchData: any;

    newModel(model: FavouriteModel): ServiceDocument<FavouriteModel> {
        return this.serviceDocument.newModel(model);
    }

    search(): Observable<ServiceDocument<FavouriteModel>> {
        return this.serviceDocument.search("/api/favourite/Search");
    }

    save(): Observable<ServiceDocument<FavouriteModel>> {
        return this.serviceDocument.save("/api/favourite/Save");
    }

    remove(): Observable<ServiceDocument<FavouriteModel>> {
        return this.serviceDocument.save("/api/favourite/Remove");
    }

    list(): Observable<ServiceDocument<FavouriteModel>> {
        return this.serviceDocument.list("/api/favourite/List");
    }

    addEdit(): Observable<ServiceDocument<FavouriteModel>> {
        return this.serviceDocument.list("/api/favourite/New");
    }
}