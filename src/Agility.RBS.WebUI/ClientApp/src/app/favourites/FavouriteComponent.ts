﻿import { Component, OnInit, TemplateRef, ViewChild, ChangeDetectionStrategy, Input } from "@angular/core";
import { MatDialogRef } from "@angular/material";
import { Router, ActivatedRoute } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup, FormBuilder, FormControl, Validators, ValidatorFn, AbstractControl } from "@angular/forms";
import { FormMode } from "@agility/frameworkcore";
import { FavouriteModel } from "./FavouriteModel";
import { FavouriteService } from "./FavouriteService";
import { SharedService } from "../Common/SharedService";

@Component({
    selector: "rbs-favourite",
    templateUrl: "./FavouriteComponent.html"
})
export class FavouriteComponent implements OnInit {
    @Input() pageTitle: string;
    @Input() redirectUrl: string;

    serviceDocument: ServiceDocument<FavouriteModel>;
    editModel: any;
    addMode: boolean = true;
    localizationData: any;
    model: FavouriteModel;

    constructor(private service: FavouriteService, private sharedService: SharedService, private route: ActivatedRoute
        , private router: Router) {
    }

    ngOnInit(): void {
        // favouriteComponent
    }

    save(): void {
        this.model = { userId: null, favouritePagePath: this.redirectUrl, favouritePage: this.pageTitle, operation: "I" };
        this.service.newModel(this.model);
        this.service.serviceDocument.dataProfile.dataModel = this.model;

        if (this.addMode) {
            this.service.save().subscribe();
        } else {
            this.service.remove();
        }
    }
}