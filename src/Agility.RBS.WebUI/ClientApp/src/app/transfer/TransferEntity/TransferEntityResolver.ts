import { Injectable } from "@angular/core";
import { Resolve } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { TransferEntityModel } from './TransferEntityModel';
import { TransferEntityService } from './TransferEntityService';
@Injectable()
export class TransferEntityListResolver implements Resolve<ServiceDocument<TransferEntityModel>> {
  constructor(private service: TransferEntityService) { }
  resolve(): Observable<ServiceDocument<TransferEntityModel>> {
        return this.service.list();
    }
}

@Injectable()
export class TransferEntityResolver implements Resolve<ServiceDocument<TransferEntityModel>> {
  constructor(private service: TransferEntityService) { }
  resolve(): Observable<ServiceDocument<TransferEntityModel>> {
        return this.service.addEdit();
    }
}


