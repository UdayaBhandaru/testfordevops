import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { RbsSharedModule } from "../../RbsSharedModule";
import { TransferEntityRoutingModule } from './TransferEntityRoutingModule';
import { TransferEntityListComponent } from './TransferEntityListComponent';
import { TransferEntityComponent } from './TransferEntityComponent';
import { TransferEntityService } from './TransferEntityService';
import { TransferEntityResolver, TransferEntityListResolver } from './TransferEntityResolver';

@NgModule({
    imports: [
        FrameworkCoreModule,
        TransferEntityRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        TransferEntityListComponent,
        TransferEntityComponent
    ],
    providers: [
        TransferEntityService,
        TransferEntityResolver,
        TransferEntityListResolver
    ]
})
export class TransferEntityModule {
    constructor() {
        LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
    }
}
