import { Component, OnInit, ChangeDetectorRef, NgModule } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { SharedService } from "../../Common/SharedService";
import { GridOptions } from "ag-grid-community";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { InfoComponent } from "../../Common/InfoComponent";
import { GridExportModel } from "../../Common/grid/GridExportModel";
import { GridEditComponent } from "../../Common/grid/GridEditComponent";
import { TransferEntityModel } from './TransferEntityModel';
import { TransferEntityService } from './TransferEntityService';

@Component({
  selector: "transfer-list",
  templateUrl: "./TransferEntityListComponent.html"
})
export class TransferEntityListComponent implements OnInit {
  public serviceDocument: ServiceDocument<TransferEntityModel>;
  PageTitle = "Transfer Entity";
  redirectUrl = "TransferEntity";
  componentName: any;
  columns: any[];
  model: TransferEntityModel = {
    tsfEntityId: null, tsfEntityDesc: null
  };
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  gridOptions: GridOptions;

  constructor(public service: TransferEntityService, private sharedService: SharedService, private router: Router, private ref: ChangeDetectorRef) {
  }

  ngAfterViewChecked(): void {
    this.ref.detectChanges();
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "ID", field: "tsfEntityId", tooltipField: "tsfEntityId", width: 60 },
      { headerName: "DESCRIPTION", field: "tsfEntityDesc", tooltipField: "tsfEntityDesc", width: 90 },
      {
        headerName: "ACTIONS", field: "Actions", cellRendererFramework: GridEditComponent, width: 60
      }
    ];

    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }
}
