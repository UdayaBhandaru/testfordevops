import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { SharedService } from "../../Common/SharedService";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { FormControl, FormGroup } from "@angular/forms";
import { TransferEntityModel } from './TransferEntityModel';
import { TransferEntityService } from './TransferEntityService';
import { TransferEntityLocationsModel } from './TransferEntityLocationsModel';
import { LoaderService } from '../../Common/LoaderService';
import { Subscription } from 'rxjs';
import { TsfLocationsPopupComponent } from './TsfLocationsPopupComponent';
import { DocumentsConstants } from '../../Common/DocumentsConstants';

@Component({
  selector: "transferentity",
  templateUrl: "./TransferEntityComponent.html"
})
export class TransferEntityComponent implements OnInit {
  PageTitle = "Transfer Entity";
  serviceDocument: ServiceDocument<TransferEntityModel>;
  //uomClassData: DomainDetailModel[];
  localizationData: any;
  editModel: TransferEntityModel;
  editMode: boolean = false;
  defaultStatus: string;
  model: TransferEntityModel;
  tsfLocModel: TransferEntityLocationsModel;
  getFutureCostsApiUrl: string;
  tsfLocationsServiceSubscription: Subscription;
  documentsConstants: DocumentsConstants;
  constructor(private service: TransferEntityService, private sharedService: SharedService, private router: Router, private loaderService: LoaderService) {
    this.getFutureCostsApiUrl = "/api/TransferZone/GetAvialbleLocationsData"
    this.documentsConstants = new DocumentsConstants();
  }

  ngOnInit(): void {
    this.bind();
  }

  bind(): void {
    delete this.editModel;
    if (this.sharedService.editObject) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
      this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
      delete this.sharedService.editObject;
      this.editMode = true;
    } else {
      this.model = {
        tsfEntityId: null, tsfEntityDesc: null, operation: "I"
      };
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  save(saveOnly: boolean): void {
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.saveSubscription(saveOnly);
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  saveAndContinue(): void {
    this.save(false);
  }

  saveSubscription(saveOnly: boolean): void {
    this.checkTransferEntity(saveOnly);
  }
  saveTransferEntity(saveOnly: boolean): void {
    this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.sharedService.saveForm("Transfer Entity Saved Successfully").subscribe(() => {
          if (saveOnly) {
            this.router.navigate(["/TransferEntity/List"], { skipLocationChange: true });
          } else {
            if (!this.editMode) {
              this.model = {
                tsfEntityId: null, tsfEntityDesc: null, operation: "I"
              };
              this.service.newModel(this.model);
            } else {
              Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
              this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.editModel);
              this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            }
          }
        }, (error: string) => { console.log(error); });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
    }, (error: string) => { console.log(error); });
  }

  checkTransferEntity(saveOnly: boolean): void {
    if (!this.editMode) {
      var form: FormGroup = this.serviceDocument.dataProfile.profileForm;
      if (form.controls["tsfEntityId"].value && form.controls["tsfEntityDesc"].value) {
        this.service.isExistingTransferEntity(form.controls["tsfEntityId"].value, form.controls["tsfEntityDesc"].value)
          .subscribe((response: any) => {
            if (response) {
              this.sharedService.errorForm("Record alredy exits");
            } else {
              this.saveTransferEntity(saveOnly);
            }
          });
      }
    } else {
      this.saveTransferEntity(saveOnly);
    }
  }

  reset(): void {
    if (!this.editMode) {
      this.model = {
        tsfEntityId: null, tsfEntityDesc: null, operation: "I"
      };
      this.service.newModel(this.model);
    } else {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    }
    this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
  }

  loadLocations(): void {
    if (this.serviceDocument.dataProfile.profileForm.controls["tsfEntityId"].value === null) {
      this.sharedService.errorForm(`Please enter Transfer Entity Id for locations view`);
    }
    else {
      this.loaderService.display(true);
      this.tsfLocationsServiceSubscription = this.service.getItemRelatedData(this.getFutureCostsApiUrl, this.serviceDocument.dataProfile.profileForm.controls["tsfEntityId"].value)
        .subscribe((response: TransferEntityLocationsModel[]) => {
          if (response != null && response.length > 0) {
            this.sharedService.openPopupWithDataList(TsfLocationsPopupComponent, response, "Locations"
              , this.documentsConstants.itemPrintObjectName, this.serviceDocument.dataProfile.profileForm.controls["tsfEntityId"].value);
          } else {
            this.sharedService.errorForm(`No Locations found for this Transer Entity: ${this.serviceDocument.dataProfile.profileForm.controls["tsfEntityId"].value}`);
          }
          this.loaderService.display(false);
        }, (error: string) => {
          this.loaderService.display(false);
        });
    }
  }

  ngOnDestroy(): void {
    if (this.tsfLocationsServiceSubscription) {
      this.tsfLocationsServiceSubscription.unsubscribe();
    }
  }
}
