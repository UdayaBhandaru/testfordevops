import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { TransferEntityListComponent } from './TransferEntityListComponent';
import { TransferEntityResolver } from './TransferEntityResolver';
import { TransferEntityComponent } from './TransferEntityComponent';

const UtilityRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: TransferEntityListComponent,
                resolve:
                {
                  serviceDocument: TransferEntityResolver
                }
            },
            {
                path: "New",
                component: TransferEntityComponent,
                resolve:
                {
                  serviceDocument: TransferEntityResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(UtilityRoutes)
    ]
})
export class TransferEntityRoutingModule { }
