import { Component, Inject } from "@angular/core";
import { DatePipe } from "@angular/common";
import { FormGroup } from "@angular/forms";
import { Subscription } from "rxjs";
import { CommonService, FormMode } from "@agility/frameworkcore";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { GridOptions, GridApi } from "ag-grid-community";
import { TransferEntityLocationsModel } from './TransferEntityLocationsModel';
import { SharedService } from '../../Common/SharedService';

@Component({
  selector: "TsfLocationsPopup",
  templateUrl: "./TsfLocationsPopupComponent.html",
  styleUrls: ['./TsfLocationsPopupComponent.scss']
})

export class TsfLocationsPopupComponent {
    infoGroup: FormGroup;
    objectValue: number;
    objectName: string;
    sharedSubscription: Subscription;
    componentName: any;
    supplierColumns: {}[];
    locationsGridOptions: GridOptions;
    gridApi: GridApi;
  locationsData: TransferEntityLocationsModel[];

    constructor(private service: SharedService, private commonService: CommonService
        , public dialog: MatDialog
        , @Inject(MAT_DIALOG_DATA)
        public data: {
            dataList: TransferEntityLocationsModel[]
            , objectName: string, objectValue: number, invokingModule: string
        }
        , private datePipe: DatePipe) {
    }

    ngOnInit(): void {
        if (this.data !== null) {
            this.objectValue = this.data.objectValue;
            this.objectName = this.data.objectName;
          this.locationsData = this.data.dataList;
        } else {
          this.locationsData = [];
        }
        this.componentName = this;
        this.setLocationsGridColumnsWithOptions();
    }

  setLocationsGridColumnsWithOptions(): void {
        this.supplierColumns = [
            {
            headerName: "locationId", field: "locationId", tooltipField: "locationId", maxwidth: 200
            },
            {
              headerName: "locName", field: "locName", tooltipField: "fullAddress", width: 350
            },
            {
              headerName: "locType", field: "locType", tooltipField: "locType", width: 200
            }
        ];

    this.locationsGridOptions = {
            onGridReady: (params: any) => {
              this.gridApi = params.api;
              this.gridApi.sizeColumnsToFit();
            },
            context: {
                componentParent: this
            },
            enableColResize: true
        };
    }

    close(): void {
        this.dialog.closeAll();
    }
}
