import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";
import { TransferEntityModel } from './TransferEntityModel';

@Injectable()
export class TransferEntityService {
  serviceDocument: ServiceDocument<TransferEntityModel> = new ServiceDocument<TransferEntityModel>();
    constructor(private httpHelperService: HttpHelperService) { }

  newModel(model: TransferEntityModel): ServiceDocument<TransferEntityModel> {
        return this.serviceDocument.newModel(model);
    }

  search(): Observable<ServiceDocument<TransferEntityModel>> {
    return this.serviceDocument.search("/api/TransferEntity/Search");
    }

  save(): Observable<ServiceDocument<TransferEntityModel>> {
    return this.serviceDocument.save("/api/TransferEntity/Save", true);
    }

  list(): Observable<ServiceDocument<TransferEntityModel>> {
    return this.serviceDocument.list("/api/TransferEntity/List");
    }

  addEdit(): Observable<ServiceDocument<TransferEntityModel>> {
    return this.serviceDocument.list("/api/TransferEntity/New");
    }

  isExistingTransferEntity(tsfEntityId: number, tsfEntityDesc: string): Observable<boolean> {
    return this.httpHelperService.get("/api/TransferEntity/IsExistingTransferEntity", new HttpParams().set("tsfEntityId", tsfEntityId.toString()).set("tsfEntityDesc", tsfEntityDesc));
  }

  getItemRelatedData(apiUrl: string, tsfentity: string): Observable<any> {
    let params: HttpParams = new HttpParams().set("tsfEntityTypeParam", tsfentity);    
    return this.httpHelperService.get(apiUrl, params);
  }
}
