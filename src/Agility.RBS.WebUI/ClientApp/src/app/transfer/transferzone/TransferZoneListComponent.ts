import { Component, OnInit, ChangeDetectorRef, NgModule } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { SharedService } from "../../Common/SharedService";
import { GridOptions } from "ag-grid-community";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { InfoComponent } from "../../Common/InfoComponent";
import { GridExportModel } from "../../Common/grid/GridExportModel";
import { GridEditComponent } from "../../Common/grid/GridEditComponent";
import { TransferZoneModel } from './TransferZoneModel';
import { TransferZoneService } from './TransferZoneService';

@Component({
  selector: "transfer-zone-list",
  templateUrl: "./TransferZoneListComponent.html"
})
export class TransferZoneListComponent implements OnInit {
  public serviceDocument: ServiceDocument<TransferZoneModel>;
  PageTitle = "Transfer Zone";
  redirectUrl = "TransferZone";
  componentName: any;
  columns: any[];
  model: TransferZoneModel = {
    tsfZone: null, tsfZoneDesc: null
  };
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  gridOptions: GridOptions;

  constructor(public service: TransferZoneService, private sharedService: SharedService, private router: Router, private ref: ChangeDetectorRef) {
  }

  ngAfterViewChecked(): void {
    this.ref.detectChanges();
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "ZONE", field: "tsfZone", tooltipField: "tsfZone", width: 60 },
      { headerName: "DESCRIPTION", field: "tsfZoneDesc", tooltipField: "tsfZoneDesc", width: 90 },
      {
        headerName: "ACTIONS", field: "Actions", cellRendererFramework: GridEditComponent, width: 60
      }
    ];

    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }
}
