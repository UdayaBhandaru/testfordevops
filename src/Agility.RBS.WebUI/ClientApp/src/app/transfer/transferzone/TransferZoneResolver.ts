import { Injectable } from "@angular/core";
import { Resolve } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { TransferZoneModel } from './TransferZoneModel';
import { TransferZoneService } from './TransferZoneService';
@Injectable()
export class TransferZoneListResolver implements Resolve<ServiceDocument<TransferZoneModel>> {
  constructor(private service: TransferZoneService) { }
  resolve(): Observable<ServiceDocument<TransferZoneModel>> {
        return this.service.list();
    }
}

@Injectable()
export class TransferZoneResolver implements Resolve<ServiceDocument<TransferZoneModel>> {
  constructor(private service: TransferZoneService) { }
  resolve(): Observable<ServiceDocument<TransferZoneModel>> {
        return this.service.addEdit();
    }
}


