export class TransferZoneModel {
  tsfZone: number;
  tsfZoneDesc: string;
  operation?: string;  
}
