import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { SharedService } from "../../Common/SharedService";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { FormControl, FormGroup } from "@angular/forms";
import { TransferZoneModel } from './TransferZoneModel';
import { TransferZoneService } from './TransferZoneService';
import { DocumentsConstants } from '../../Common/DocumentsConstants';
import { LoaderService } from '../../Common/LoaderService';
import { Subscription } from 'rxjs';
import { TransferEntityLocationsModel } from '../TransferEntity/TransferEntityLocationsModel';
import { TsfLocationsPopupComponent } from '../TransferEntity/TsfLocationsPopupComponent';

@Component({
    selector: "transferezone",
    templateUrl: "./TransferZoneComponent.html"
})
export class TransferZoneComponent implements OnInit {
    PageTitle = "Transfer Zone";
    serviceDocument: ServiceDocument<TransferZoneModel>;
    localizationData: any;
    editModel: TransferZoneModel;
    editMode: boolean = false;
    defaultStatus: string;
    model: TransferZoneModel;
  tsfLocModel: TransferEntityLocationsModel;
  getTransferZoneApiUrl: string;
  tsfLocationsServiceSubscription: Subscription;
  documentsConstants: DocumentsConstants;
  constructor(private service: TransferZoneService, private sharedService: SharedService, private router: Router, private loaderService: LoaderService)
    {
    this.getTransferZoneApiUrl = "/api/TransferZone/GetAvialbleLocationsData"
    this.documentsConstants = new DocumentsConstants();
    }

    ngOnInit(): void {
        this.bind();
    }

    bind(): void {
        delete this.editModel;
        if (this.sharedService.editObject) {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
            this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            delete this.sharedService.editObject;
            this.editMode = true;
        } else {
            this.model = {
              tsfZone: null, tsfZoneDesc: null, operation: "I"
            };
            this.service.newModel(this.model);
        }
        this.serviceDocument = this.service.serviceDocument;
    }

    save(saveOnly: boolean): void {
        if (this.serviceDocument.dataProfile.profileForm.valid) {
            this.saveSubscription(saveOnly);
        } else {
            this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
        }
    }

    saveAndContinue(): void {
        this.save(false);
    }

    saveSubscription(saveOnly: boolean): void {
        this.checkTransferZone(saveOnly);
    }
    saveTransferZone(saveOnly: boolean): void {
        this.service.save().subscribe(() => {
            if (this.service.serviceDocument.result.type === MessageType.success) {
              this.sharedService.saveForm("Transfer Zone Saved Successfully").subscribe(() => {
                    if (saveOnly) {
                        this.router.navigate(["/TransferZone/List"], { skipLocationChange: true });
                    } else {
                        if (!this.editMode) {
                            this.model = {
                              tsfZone: null, tsfZoneDesc: null, operation: "I"
                            };
                            this.service.newModel(this.model);
                        } else {
                            Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
                            this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.editModel);
                            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
                        }
                    }
                }, (error: string) => { console.log(error); });
            } else {
                this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
            }
        }, (error: string) => { console.log(error); });
    }

    checkTransferZone(saveOnly: boolean): void {
        if (!this.editMode) {
            var form: FormGroup = this.serviceDocument.dataProfile.profileForm;
          if (form.controls["tsfZone"].value && form.controls["tsfZoneDesc"].value) {
            this.service.isExistingTransferZone(form.controls["tsfZone"].value, form.controls["tsfZoneDesc"].value)
                    .subscribe((response: any) => {
                        if (response) {
                            this.sharedService.errorForm("Record alredy exits");
                        } else {
                            this.saveTransferZone(saveOnly);
                        }
                    });
            }
        } else {
            this.saveTransferZone(saveOnly);
        }
    }

    reset(): void {
        if (!this.editMode) {
            this.model = {
              tsfZone: null, tsfZoneDesc: null, operation: "I"
            };
            this.service.newModel(this.model);
        } else {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
        }
        this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
  }

  loadLocations(): void {
    if (this.serviceDocument.dataProfile.profileForm.controls["tsfZone"].value === null) {
      this.sharedService.errorForm(`Please enter Transfer Zone Id for locations view`);

    }
    else {

      this.loaderService.display(true);
      this.tsfLocationsServiceSubscription = this.service.getItemRelatedData(this.getTransferZoneApiUrl, this.serviceDocument.dataProfile.profileForm.controls["tsfZone"].value)
        .subscribe((response: TransferEntityLocationsModel[]) => {
          if (response != null && response.length > 0) {
            this.sharedService.openPopupWithDataList(TsfLocationsPopupComponent, response, "Locations"
              , this.documentsConstants.itemPrintObjectName, this.serviceDocument.dataProfile.profileForm.controls["tsfZone"].value);
          } else {
            this.sharedService.errorForm(`No Locations found for this Transer Zone: ${this.serviceDocument.dataProfile.profileForm.controls["tsfZone"].value}`);
          }
          this.loaderService.display(false);
        }, (error: string) => {
          this.loaderService.display(false);
        });
    }
  }
}
