import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";
import { TransferZoneModel } from './TransferZoneModel';

@Injectable()
export class TransferZoneService {
  serviceDocument: ServiceDocument<TransferZoneModel> = new ServiceDocument<TransferZoneModel>();
    constructor(private httpHelperService: HttpHelperService) { }

  newModel(model: TransferZoneModel): ServiceDocument<TransferZoneModel> {
        return this.serviceDocument.newModel(model);
    }

  search(): Observable<ServiceDocument<TransferZoneModel>> {
    return this.serviceDocument.search("/api/TransferZone/Search");
    }

  save(): Observable<ServiceDocument<TransferZoneModel>> {
    return this.serviceDocument.save("/api/TransferZone/Save", true);
    }

  list(): Observable<ServiceDocument<TransferZoneModel>> {
    return this.serviceDocument.list("/api/TransferZone/List");
    }

  addEdit(): Observable<ServiceDocument<TransferZoneModel>> {
    return this.serviceDocument.list("/api/TransferZone/New");
    }

  isExistingTransferZone(tsfZone: number, tsfZoneDesc: string): Observable<boolean> {
    return this.httpHelperService.get("/api/TransferZone/IsExistingTransferZone", new HttpParams().set("tsfZone", tsfZone.toString()).set("tsfZoneDesc", tsfZoneDesc));
  }
  getItemRelatedData(apiUrl: string, tsferZone: string): Observable<any> {
    let params: HttpParams = new HttpParams().set("tsfZoneTypeParam", tsferZone);
    return this.httpHelperService.get(apiUrl, params);
  }
}
