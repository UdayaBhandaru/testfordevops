import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { TransferZoneListComponent } from './TransferZoneListComponent';
import { TransferZoneResolver } from './TransferZoneResolver';
import { TransferZoneComponent } from './TransferZoneComponent';

const UtilityRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: TransferZoneListComponent,
                resolve:
                {
                  serviceDocument: TransferZoneResolver
                }
            },
            {
                path: "New",
                component: TransferZoneComponent,
                resolve:
                {
                  serviceDocument: TransferZoneResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(UtilityRoutes)
    ]
})
export class TransferZoneRoutingModule { }
