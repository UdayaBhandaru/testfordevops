import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { ShipmentDetailModel } from "./ShipmentDetailModel";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../Common/HttpHelperService";

@Injectable()
export class ShipmentDetailService {
  serviceDocument: ServiceDocument<ShipmentDetailModel> = new ServiceDocument<ShipmentDetailModel>();
  
  constructor(private httpHelperService: HttpHelperService) { }

  newModel(model: ShipmentDetailModel): ServiceDocument<ShipmentDetailModel> {
    return this.serviceDocument.newModel(model);
  }    

  save(): Observable<ServiceDocument<ShipmentDetailModel>> {
    return this.serviceDocument.save("/api/ShipmentDetail/Save", true);
  }

  addEdit(id: number): Observable<ServiceDocument<ShipmentDetailModel>> {
    if (+id === 0) {
      return this.serviceDocument.new("/api/ShipmentDetail/New");
    } else {
      return this.serviceDocument.open("/api/ShipmentDetail/Open", new HttpParams().set("id", id.toString()));
    }
  } 
    
  }
