import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { ShipmentSearchModel } from "./ShipmentSearchModel";
import { ShipmentDetailModel } from "./ShipmentDetailModel";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../Common/HttpHelperService";
import { SharedService } from '../Common/SharedService';

@Injectable()
export class ShipmentService {
  serviceDocument: ServiceDocument<ShipmentSearchModel> = new ServiceDocument<ShipmentSearchModel>();
  searchData: any;
  orderPaging: {
    startRow: number,
    pageSize: number,
    cacheSize: number,
  } = {
      startRow: 0,
      pageSize: 10,
      cacheSize: 100,
    };
  constructor(private httpHelperService: HttpHelperService, private sharedService: SharedService) { }

  newModel(model: ShipmentSearchModel): ServiceDocument<ShipmentSearchModel> {
    return this.serviceDocument.newModel(model);
  }

  list(): Observable<ServiceDocument<ShipmentSearchModel>> {
    return this.serviceDocument.list("/api/Shipment/List");
  }

  search(additionalParams: any): Observable<ServiceDocument<ShipmentSearchModel>> {
    return this.serviceDocument.search("/api/Shipment/Search", true, () => this.setSubClassId(additionalParams));
  }

  save(): Observable<ServiceDocument<ShipmentSearchModel>> {
    return this.serviceDocument.save("/api/Shipment/Save", true);
  }

  isExistingDomain(name: string): Observable<boolean> {
    return this.httpHelperService.get("/api/Shipment/IsExistingDomain", new HttpParams().set("name", name));
  }

  setSubClassId(additionalParams: any): void {
    var sd: ShipmentSearchModel = this.serviceDocument.dataProfile.dataModel;
    if (additionalParams) {
      sd.startRow = additionalParams.startRow ? additionalParams.startRow : this.orderPaging.startRow;
      sd.endRow = additionalParams.endRow ? additionalParams.endRow : this.orderPaging.cacheSize;
      sd.sortModel = additionalParams.sortModel;
    } else {
      sd.startRow = this.orderPaging.startRow;
      sd.endRow = this.orderPaging.cacheSize;
    }
    if (sd !== null) {
      if (sd.shipDateBefore != null) {
        sd.shipDateBefore = this.sharedService.setOffSet(sd.shipDateBefore);
      }
      if (sd.shipDateAfter != null) {
        sd.shipDateAfter = this.sharedService.setOffSet(sd.shipDateAfter);
      }

      if (sd.receiveDateAfter != null) {
        sd.receiveDateAfter = this.sharedService.setOffSet(sd.receiveDateAfter);
      }
      if (sd.receiveDateBefore != null) {
        sd.receiveDateBefore = this.sharedService.setOffSet(sd.receiveDateBefore);
      }
    }
  }
}
