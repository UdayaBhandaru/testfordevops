import { Component, OnInit } from "@angular/core";
import { ServiceDocument, FormMode, CommonService, MessageResult, MessageType } from "@agility/frameworkcore";
import { Router } from "@angular/router";
import { SharedService } from "../Common/SharedService";
import { ShipmentDetailModel } from "./ShipmentDetailModel";
import { ShipmentDetailService } from './ShipmentDetailService';
import { GridDateComponent } from '../Common/grid/GridDateComponent';
import { GridOptions } from 'ag-grid-community';

@Component({
  selector: "shipment",
  templateUrl: "./ShipmentComponent.html",
  styleUrls: ['./ShipmentComponent.scss']
})
export class ShipmentComponent implements OnInit {
  PageTitle = "Shipment View";
  sheetName: string = "Domain Data";
  redirectUrl = "DomainHeader";
  serviceDocument: ServiceDocument<ShipmentDetailModel>;
  model: ShipmentDetailModel;
  columns: any[];
  data: any[] = [];
  sno: number = 0;
  domainDtlData: ShipmentDetailModel[];
  temp: {};
  editMode: boolean = false;
  editModel: any;
  private messageResult: MessageResult = new MessageResult();
  componentParentName: ShipmentComponent;
  localizationData: any; 
  gridOptions: GridOptions;

  constructor(private service: ShipmentDetailService, private commonService: CommonService,
    private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.serviceDocument = this.service.serviceDocument;
    this.columns = [
      { headerName: "ITEM", field: "item", tooltipField: "item"},
      { headerName: "DESCRIPTION", field: "itemDesc", tooltipField: "itemDesc" },
      { headerName: "DISTRIBUTION NUMBER", field: "distroNo", tooltipField: "distroNo" },
      { headerName: "CARTON", field: "cartonNO", tooltipField: "cartonNO" },
      { headerName: "TEMPERED IND", field: "temperedInd", tooltipField: "temperedInd" },
      { headerName: "QTY EXPECTED", field: "quantityExpected", tooltipField: "quantityExpected" },
      { headerName: "QTY RECEIVED", field: "quantityReceived", tooltipField: "quantityReceived" },
      { headerName: "QTY MATCHED", field: "quantityMatched", tooltipField: "quantityMatched" },
      { headerName: "RECONCILE USER", field: "reconcileUser", tooltipField: "reconcileUser" },
      { headerName: "RECONCILE DATE", field: "reconcileDate", tooltipField: "reconcileDate", cellRendererFramework: GridDateComponent },
      { headerName: "ACTUAL RECEIVING STORE", field: "actualReceivingStore", tooltipField: "actualReceivingStore" },
      { headerName: "MATCH INVOICE ID", field: "matchInvoiceId", tooltipField: "matchInvoiceId" }
    ];    
    this.componentParentName = this;
  }
}
