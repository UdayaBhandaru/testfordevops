import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { ShipmentRoutingModule } from "./ShipmentRoutingModule";
import { ShipmentListComponent } from "./ShipmentListComponent";
import { ShipmentComponent } from "./ShipmentComponent";
import { ShipmentService } from "./ShipmentService";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../AgGridSharedModule";
import { TitleSharedModule } from "../TitleSharedModule";
import { RbsSharedModule } from "../RbsSharedModule";
import { ShipmentDetailResolver } from './ShipmentDetailResolver';
import { ShipmentDetailService } from "./ShipmentDetailService";
import { ShipmentListResolver } from './ShipmentResolver';


@NgModule({
    imports: [
        FrameworkCoreModule,
        ShipmentRoutingModule,
        AgGridModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        ShipmentListComponent,
        ShipmentComponent
    ],
    providers: [
        ShipmentService,
        ShipmentListResolver,
        ShipmentDetailService,
        ShipmentDetailResolver
    ]
})
export class ShipmentModule {
}
