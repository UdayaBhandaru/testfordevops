export class ShipmentDetailModel {  
  shipment?: string;
  orderNo?: string;
  bolNo?: string;
  item?: string;
  asn?: string;
  supplier?: number;
  receiveDate?: Date;
  shipDate?: Date;
  notBeforeDate?: Date;
  notAfterDate?: Date;
  courier?: string;
  noOfBoxes?: number;
  comments?: string;
  statusDesc?: string;
  statusCode?: string;
  shipmentDetails?: any[];
  shipOrginDesc?: string;
  toLocName?: string;
}
