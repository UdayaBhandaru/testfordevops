export class ShipmentSearchModel {
  shipment?: number;
  orderNo?: number;
  bolNo?: string;
  asn?: string;
  supplier?: number;
  shipDateBefore?: Date;
  shipDateAfter?: Date;
  receiveDateBefore?: Date;
  receiveDateAfter?: Date;
  statusCode?: string;
  statusDesc?: string;
  toLoc?: string;
  toLocName?: string;
  item?: string;
  longId?: number;
  startRow?: number;
  endRow?: number;
  totalRows?: number;
  sortModel?: {}[];
  typeId: number;
}
