import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { ShipmentSearchModel } from "./ShipmentSearchModel";
import { ShipmentDetailModel } from "./ShipmentDetailModel";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { ShipmentService } from "./ShipmentService";

@Injectable()
export class ShipmentListResolver implements Resolve<ServiceDocument<ShipmentSearchModel>> {
  constructor(private service: ShipmentService) { }
  resolve(): Observable<ServiceDocument<ShipmentSearchModel>> {
    return this.service.list();
  }
}
