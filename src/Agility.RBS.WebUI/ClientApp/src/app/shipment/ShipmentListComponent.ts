import { Component, OnInit, ViewChild, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { ShipmentService } from "./ShipmentService";
import { FormGroup } from "@angular/forms";
import { SharedService } from "../Common/SharedService";
import { ShipmentDetailModel } from "./ShipmentDetailModel";
import { GridEditComponent } from "../Common/grid/GridEditComponent";
import { GridExportModel } from "../Common/grid/GridExportModel";
import { InfoComponent } from "../Common/InfoComponent";
import { GridOptions, GridReadyEvent, IServerSideGetRowsParams, GridApi } from 'ag-grid-community';
import { DomainDetailModel } from '../domain/DomainDetailModel';
import { LocationDomainModel } from '../Common/DomainData/LocationDomainModel';
import { SupplierDomainModel } from '../Common/DomainData/SupplierDomainModel';
import { SearchComponent } from '../Common/SearchComponent';
import { ShipmentSearchModel } from './ShipmentSearchModel';
import { Subscription } from 'rxjs';
import { GridDateComponent } from '../Common/grid/GridDateComponent';

@Component({
  selector: "Shipment-List",
  templateUrl: "./ShipmentListComponent.html",
  styleUrls: ['./ShipmentListComponent.scss']
})
export class ShipmentListComponent implements OnInit {
  public serviceDocument: ServiceDocument<ShipmentSearchModel>;
  PageTitle = "Shipment List";
  redirectUrl = "Shipment/New/";
  componentName: any;
  columns: any[];
  domainDtlData: ShipmentDetailModel[];
  additionalSearchParams: any;
  loadedPage: boolean;
  searchServiceSubscription: Subscription;
  model: ShipmentSearchModel = {
    shipment: null, orderNo: null, bolNo: null, item: null, asn: null, supplier: null, statusCode: null, toLoc: null, toLocName: null,
    shipDateAfter: null, shipDateBefore: null, receiveDateBefore: null, receiveDateAfter: null, statusDesc: null, typeId:null  };
  references: ShipmentDetailModel[];
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  gridOptions: GridOptions;
  loactionData: LocationDomainModel[];
  statusData: DomainDetailModel[];
  supplierData: SupplierDomainModel[];
  typeData: DomainDetailModel[];
  gridApi: GridApi;
  apiUrl: string;


  isSearchParamGiven: boolean = false;
  searchProps: string[] = ["shipment", "orderNo", "bolNo", "item", "asn", "supplier", "statusCode", "toLoc", "shipDateAfter", "shipDateBefore", "receiveDateBefore","receiveDateAfter"];
  @ViewChild("searchPanel") searchPanel: SearchComponent;
  constructor(public service: ShipmentService, private sharedService: SharedService, private router: Router, private changeRef: ChangeDetectorRef) {
  }

  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "SHIPMENT", field: "shipment", tooltipField: "shipment", width: 110, sort: "desc" },
      { headerName: "ORDER NO", field: "orderNo", tooltipField: "orderNo", width: 110},
      { headerName: "BOL NO", field: "bolNo", tooltipField: "bolNo", width: 90 },
      { headerName: "SHIPMENT BEFORE DATE", field: "shipDateBefore", tooltipField: "shipDateBefore", cellRendererFramework: GridDateComponent, width: 180},
      { headerName: "RECEIVE BEFORE DATE", field: "receiveDateBefore", tooltipField: "receiveDateBefore", cellRendererFramework: GridDateComponent, width: 180 },
      { headerName: "STATUS", field: "statusDesc", tooltipField: "statusDesc", width: 90 },
      { headerName: "To Loc Type", field: "toLocName", tooltipField: "toLocName", width: 140 },
      { headerName: "To Loc", field: "toLoc", tooltipField: "toLoc", width: 90},
      {
        headerName: "ACTIONS", field: "Actions", cellRendererFramework: GridEditComponent, width: 100
      }
    ];

    this.gridOptions = {
      onGridReady: (params: GridReadyEvent) => {
        params.api.setServerSideDatasource(this);
      },
      context: {
        componentParent: this
      },
      enableColResize: true,
      suppressRowClickSelection: true,
      suppressCellSelection: true,
      rowModelType: "enterprise",
      paginationPageSize: this.service.orderPaging.pageSize,
      enableServerSideSorting: true,
      enableServerSideFilter: true,
      cacheBlockSize: this.service.orderPaging.cacheSize,      
    };

    this.supplierData = this.service.serviceDocument.domainData["supplier"];
    this.loactionData = this.service.serviceDocument.domainData["location"];
    this.statusData = this.service.serviceDocument.domainData["status"];
    this.typeData = this.service.serviceDocument.domainData["type"];
    this.apiUrl = "/api/Supplier/SuppliersGetName?name=";

    //delete this.sharedService.domainData;
    this.sharedService.domainData = { status: this.statusData, location: this.loactionData, supplier: this.supplierData,type:this.typeData };

    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
    this.searchPanel.dateColumns = ["shipDateBefore", "shipDateAfter", "receiveDateAfter", "receiveDateBefore"];
  }

  getRows(params: IServerSideGetRowsParams): void {
    this.additionalSearchParams = { startRow: params.request.startRow, endRow: params.request.endRow, sortModel: params.request.sortModel };
    if (!this.loadedPage) {
      this.loadedPage = true;
      params.successCallback(
        this.serviceDocument.dataProfile.dataList.slice(params.request.startRow, params.request.endRow),
        this.service.serviceDocument.dataProfile.dataModel.totalRows);
    } else {
      this.searchServiceSubscription = this.service.search(this.additionalSearchParams).subscribe((sDoc) => {
        params.successCallback(sDoc.dataProfile.dataList, this.service.serviceDocument.dataProfile.dataModel.totalRows);
      });
    }
  }

  showSearchCriteriaChild(event: any): void {
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.showSearchCriteria = event === "search" ? true : event;
      if (event === "search") {
        for (let i: number = 0; i < this.searchProps.length; i++) {
          let controlName: string = this.searchProps[i];
          if (this.serviceDocument.dataProfile.profileForm.controls[controlName].value) {
            this.isSearchParamGiven = true;
            break;
          }
        }
        if (this.isSearchParamGiven) {
          this.searchPanel.restrictSearch = false;
          this.searchPanel.search();
          this.isSearchParamGiven = false;
        } else {
          this.sharedService.errorForm("At least one field is required to search");
        }
      } else if (event) {
        this.searchPanel.restrictSearch = true;
      }
    }
  }

  addValidation(): void {
    this.navigateToAdd();
  }

  private navigateToAdd(): void {
    this.router.navigate(["/" + this.redirectUrl + "0"], { skipLocationChange: true });
    this.sharedService.previousUrl = "../Shipment/List";
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl + cell.data.shipment, this.service.serviceDocument.dataProfile.dataList);
  }
}
