import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { ShipmentDetailModel } from "./ShipmentDetailModel";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { ShipmentDetailService } from "./ShipmentDetailService";

@Injectable()
export class ShipmentDetailResolver implements Resolve<ServiceDocument<ShipmentDetailModel>> {
  constructor(private service: ShipmentDetailService) { }
  resolve(route: ActivatedRouteSnapshot): Observable<ServiceDocument<ShipmentDetailModel>> {
    return this.service.addEdit(route.params["id"]);
  }
}
