import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ShipmentListComponent } from "./ShipmentListComponent";
import { ShipmentListResolver } from "./ShipmentResolver";
import { ShipmentComponent } from "./ShipmentComponent";
import { ShipmentDetailResolver } from './ShipmentDetailResolver';
const ShipmentRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: ShipmentListComponent,
                resolve:
                {
                  references: ShipmentListResolver
                }
            },
            {
                path: "New/:id",
                component: ShipmentComponent,
                resolve:
                {
                  references: ShipmentDetailResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(ShipmentRoutes)
    ]
})
export class ShipmentRoutingModule { }
