﻿import { Injectable } from "@angular/core";
import { DealSearchModel } from "./DealSearchModel";
import { DealService } from "./DealService";
import { Resolve, ActivatedRouteSnapshot, Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";

@Injectable()
export class DealResolver implements Resolve<ServiceDocument<DealSearchModel>> {
    constructor(private service: DealService, private router: Router) { }
    resolve(route: ActivatedRouteSnapshot): Observable<ServiceDocument<DealSearchModel>> {
        return this.service.list();
    }
}
