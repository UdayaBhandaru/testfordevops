﻿import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument, WorkflowAction } from "@agility/frameworkcore";
import { DealItemLocationModel } from "./DealItemLocationModel";
import { SharedService } from "../../Common/SharedService";
import { HttpParams } from "@angular/common/http";

@Injectable()
export class DealItemLocationService {
    serviceDocument: ServiceDocument<DealItemLocationModel> = new ServiceDocument<DealItemLocationModel>();

    constructor(private sharedService: SharedService) { }

    search(): Observable<ServiceDocument<DealItemLocationModel>> {
        return this.serviceDocument.search("/api/DealItemLocation/Search");
    }

    newModel(model: DealItemLocationModel): ServiceDocument<DealItemLocationModel> {
        return this.serviceDocument.newModel(model);
    }

    save(): Observable<ServiceDocument<DealItemLocationModel>> {
        return this.serviceDocument.save("/api/DealItemLocation/Save", true, () => this.setDate());
    }

    list(id: number): Observable<ServiceDocument<DealItemLocationModel>> {
        // this.psm.DealRequest = id;
        return this.serviceDocument.open("/api/DealItemLocation/List", new HttpParams().set("id", id.toString()));
    }

    setDate(): void {
        let sd: DealItemLocationModel = this.serviceDocument.dataProfile.dataModel;

        sd.listOfDealItemLocations.forEach(x => {
            if (x.effectiveStartDate != null) {
                x.effectiveStartDate = this.sharedService.setOffSet(x.effectiveStartDate);
            }
            if (x.effectiveEndDate != null) {
                x.effectiveEndDate = this.sharedService.setOffSet(x.effectiveEndDate);
            }
        });
    }
}