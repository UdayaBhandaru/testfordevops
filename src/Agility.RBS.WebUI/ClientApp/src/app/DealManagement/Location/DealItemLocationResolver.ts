﻿import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { DealItemLocationModel } from "./DealItemLocationModel";
import { DealItemLocationService } from "./DealItemLocationService";

@Injectable()
export class DealItemLocationResolver implements Resolve<ServiceDocument<DealItemLocationModel>> {
    constructor(private service: DealItemLocationService) { }
    resolve(route: ActivatedRouteSnapshot): Observable<ServiceDocument<DealItemLocationModel>> {
        return this.service.list(route.params["id"]);
    }
}