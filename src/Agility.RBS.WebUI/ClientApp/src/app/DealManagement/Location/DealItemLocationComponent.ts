import { Component, OnInit, TemplateRef, ViewChild, OnDestroy, ChangeDetectorRef, AfterViewChecked } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ServiceDocument, CommonService, MessageResult, MessageType } from "@agility/frameworkcore";
import { SharedService } from "../../Common/SharedService";
import { LoaderService } from "../../Common/LoaderService";
import { MatDialogRef } from "@angular/material";
import { GridOptions } from "ag-grid-community";
import { Subscription } from "rxjs";
import { GridEditComponent } from "../../Common/grid/GridEditComponent";
import { ItemSelectComponent } from "../../Common/ItemSelectComponent";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { CommonModel } from "../../Common/CommonModel";
import { GridDeleteComponent } from "../../Common/grid/GridDeleteComponent";
import { DealItemLocationModel } from "./DealItemLocationModel";
import { DealItemLocationService } from "./DealItemLocationService";
import { DealSharedService } from "../DealSharedService";
import { RbDialogService } from "../../Common/controls/RbDialogService";
import { DocumentsConstants } from "../../Common/DocumentsConstants";
import { GridDateComponent } from "../../Common/grid/GridDateComponent";
import { DealDetailIdModel } from "./DealDetailIdModel";
import { CostChgItemSelectModel } from '../../CostChange/CostChgItemSelectModel';

@Component({
  selector: "deal-component-location",
  templateUrl: "./DealItemLocationComponent.html"
})
export class DealItemLocationComponent implements OnInit, AfterViewChecked, OnDestroy {
  _componentName = "Deal Location";
  PageTitle = "Deal Location";
  public serviceDocument: ServiceDocument<DealItemLocationModel>;
  public gridOptions: GridOptions;
  localizationData: any;
  dealRequestId: number;
  dealComponentId: number;
  model: DealItemLocationModel = {
    dealDetailId: null, dealId: this.dealRequestId, discountType: null, discountValue: null
    , effectiveEndDate: this.dealSharedService.dealObject.endDate, effectiveStartDate: this.dealSharedService.dealObject.startDate
    , exclInd: null, item: null, itemDesc: null
    , merchLevel: null, merchLevelDesc: null, merchLevelValue: null, merchLevelValueDesc: null
    , orgLevel: null, orgLevelDesc: null, orgLevelValue: null, orgLevelValueDesc: null
    , seqNo: null, tableName: null, operation: "I"
    , thresholdFreeItem: null, thresholdFreeQty: null, thresholdQty: null, listOfDealItemLocations: []
    , createdBy: null, createdDate: null, lastUpdatedBy: null, lastUpdatedDate: null
    , dealDescription: this.dealSharedService.dealObject.dealDescription, dealType: this.dealSharedService.dealObject.dealTypeDesc
    , supplier: this.dealSharedService.dealObject.supplierDesc, discountTypeDesc: null
  };
  editModel: DealItemLocationModel;
  dialogRef: MatDialogRef<any>;
  data: DealItemLocationModel[] = [];
  dataReset: DealItemLocationModel[] = [];
  detailIdsData: DealDetailIdModel[] = [];
  columns: any[];
  componentName: any;
  merchLevelValueData: CommonModel[];
  departmentsData: CommonModel[];
  categoriesData: CommonModel[];
  orgLevelData: DomainDetailModel[];
  merchLevelData: DomainDetailModel[];
  discountTypeData: DomainDetailModel[];
  orgLevelValueData: CommonModel[];
  locationData: CommonModel[];
  costZoneData: CommonModel[];
  indicatorData: DomainDetailModel[];
  deletedRows: any[] = [];
  dealLocationsCount: number;
  saveContinueSubscribtion: Subscription;
  resetSubscribtion: Subscription;
  saveSubscribtion: Subscription;
  closeSubscribtion: Subscription;
  searchServiceSubscription: Subscription;
  findItemsServiceSubscription: Subscription;
  previousSubscription: Subscription;
  documentsConstants: DocumentsConstants = new DocumentsConstants();
  public messageResult: MessageResult = new MessageResult();
  canNavigate: boolean = false;
  addText: string = "+ Add To Grid";
  pForm: FormGroup;
  node: any;
  public sheetName: string = "Deal Component Location";
  editMode: boolean = false;
  initialFields: string[] = [
    "item"
  ];

  mandatoryDealLocationFields: string[] = [
    "dealDetailId"
  ];
  orginalValue: string = "";

  constructor(private route: ActivatedRoute, private service: DealItemLocationService, private sharedService: SharedService
    , public dealSharedService: DealSharedService, private commonService: CommonService, public router: Router
    , private changeRef: ChangeDetectorRef, public dialogService: RbDialogService) {
    this.saveSubscribtion = dealSharedService.saveAnnounced$.subscribe((response: any) => {
      if (response === this._componentName) {
        this.save(true);
      }
    });
    this.saveContinueSubscribtion = dealSharedService.saveContinueAnnounced$.subscribe((response: string) => {
      if (response === this._componentName) {
        this.save(false);
      }
    });
    this.resetSubscribtion = dealSharedService.resetAnnounced$.subscribe((response: string) => {
      if (response === this._componentName) {
        this.reset();
      }
    });
    this.closeSubscribtion = dealSharedService.closeAnnounced$.subscribe((response: string) => {
      if (response === this._componentName) {
        this.close();
      }
    });
    this.previousSubscription = dealSharedService.previousAnnounced$.subscribe((response: string) => {
      if (response === this._componentName) {
        this.previousTab();
      }
    });
  }

  ngOnInit(): void {
    this.dealRequestId = this.dealSharedService.dealId;
    this.componentName = this;
    this.service.serviceDocument.dataProfile.dataModel = this.model;
    if (this.service.serviceDocument.dataProfile.dataList) {
      Object.assign(this.data, this.service.serviceDocument.dataProfile.dataList);
      Object.assign(this.dataReset, this.service.serviceDocument.dataProfile.dataList);
      this.orginalValue = JSON.stringify(this.dataReset);
    }
    this.service.newModel(this.model);
    this.service.serviceDocument.dataProfile.profileForm.patchValue(this.service.serviceDocument.dataProfile.dataModel);
    this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
    this.serviceDocument = this.service.serviceDocument;
    this.initialFields.forEach(x => {
      this.serviceDocument.dataProfile.profileForm.controls[x].disable();
    });

    this.columns = [
      { headerName: "Item", field: "item", tooltipField: "item" },
      { headerName: "Item Description", field: "itemDesc", tooltipField: "itemDesc" },
      { headerName: "Merch Level", field: "merchLevelDesc", tooltipField: "merchLevelDesc" },
      { headerName: "Merch Level Value", field: "merchLevelValueDesc", tooltipField: "merchLevelValueDesc" },
      { headerName: "Org Level", field: "orgLevelDesc", tooltipField: "orgLevelDesc" },
      { headerName: "Org Level Value", field: "orgLevelValueDesc", tooltipField: "orgLevelValueDesc" },
      {
        headerName: "Effective Start Date", field: "effectiveStartDate", tooltipField: "effectiveStartDate | date:'dd-MMM - yyyy'",
        cellRendererFramework: GridDateComponent
      },
      {
        headerName: "Effective End Date", field: "effectiveEndDate", tooltipField: "effectiveEndDate | date:'dd-MMM - yyyy'",
        cellRendererFramework: GridDateComponent
      },
      { headerName: "Discount Type", field: "discountTypeDesc", tooltipField: "discountTypeDesc" },
      { headerName: "Discount Value", field: "discountValue", tooltipField: "discountValue" },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridDeleteComponent, suppressCellSelection: true, width: 60
      }
    ];
    this.gridOptions = {
      onRowSelected: this.onRowSelected,
      onGridReady: () => {
        this.gridOptions.api.sizeColumnsToFit();
      },
      context: {
        componentParent: this
      }
    };
    this.gridOptions = Object.assign(this.gridOptions, this.dealSharedService.gridOptions);
    this.gridOptions.defaultColDef.headerCheckboxSelection = false;

    this.orgLevelData = Object.assign([], this.service.serviceDocument.domainData["orgLevel"]);
    this.merchLevelData = Object.assign([], this.service.serviceDocument.domainData["merchLevel"]);
    this.merchLevelData.push({ requiredInd: "ITEM", code: "ITEM" });
    this.discountTypeData = Object.assign([], this.service.serviceDocument.domainData["discountType"]);
    this.locationData = Object.assign([], this.service.serviceDocument.domainData["location"]);
    this.costZoneData = Object.assign([], this.service.serviceDocument.domainData["costZone"]);
    this.locationData.forEach(x => x.id = +x.id);
    this.costZoneData.forEach(x => x.id = +x.id);
    this.departmentsData = this.service.serviceDocument.domainData["departments"];
    this.categoriesData = this.service.serviceDocument.domainData["categories"];
    this.departmentsData.forEach(x => x.id = +x.id);
    this.categoriesData.forEach(x => x.id = +x.id);
    this.indicatorData = Object.assign([], this.service.serviceDocument.domainData["indicator"]);
    this.detailIdsData = Object.assign([], this.service.serviceDocument.domainData["detailIds"]);
    this.dealLocationsCount = this.data.length;
    this.service.serviceDocument.dataProfile.profileForm.controls["dealId"].setValue(this.dealRequestId);
  }

  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }

  onRowSelected(event: any): void {
    if (event.node.selected) {
      let context: any = event.context.componentParent;
      context.serviceDocument.dataProfile.profileForm = context.commonService.getFormGroup(event.data, 2);
      context.initialFields.forEach(x => {
        context.serviceDocument.dataProfile.profileForm.controls[x].disable();
      });
      if (context.serviceDocument.dataProfile.profileForm.controls["orgLevel"].value === "CZ") {
        context.orgLevelValueData = Object.assign([], context.costZoneData);
      } else {
        context.orgLevelValueData = Object.assign([], context.locationData);
      }

      let sd: string = context.serviceDocument.dataProfile.profileForm.controls["merchLevel"].value;
      if (sd === "DEPT") {
        context.merchLevelValueData = context.departmentsData;
      }
      if (sd === "CATEGORY") {
        context.merchLevelValueData = context.categoriesData;
      }

      context.node = event.node;
      context.addText = " Update";
      let formObj: any = context.serviceDocument.dataProfile.profileForm;
      if (formObj.controls["createdBy"].value) {
        context.editMode = true;
      } else {
        context.editMode = false;
      }
      context.changeRef.detectChanges();
    } else if (event.context.componentParent.node !== event.node) {
      // when we select other row when one row already selected
    } else {
      let context: any = event.context.componentParent;
      context.addText = "+ Add To Grid";
      context.service.newModel(context.model);
      context.serviceDocument = context.service.serviceDocument;
      context.serviceDocument.dataProfile.profileForm.controls["dealId"].setValue(context.dealRequestId);
      context.editMode = false;
      context.changeRef.detectChanges();
    }
  }

  openItemSearch(): void {
    let data: any = [{ "Name": "CZ" }];
    let itemsExistCount: number = 0;
    this.findItemsServiceSubscription = this.sharedService.openDilogForFindItems(ItemSelectComponent, data, "Filter")
      .subscribe((selectedList: CostChgItemSelectModel[]) => {
        if (selectedList) {
          selectedList.forEach(x => {
            let dealItmLocModel: DealItemLocationModel = {
              exclInd: null, item: x.itemBarcode, itemDesc: x.itemDesc
              , merchLevel: "ITEM", merchLevelDesc: "ITEM", merchLevelValue: null, merchLevelValueDesc: null
              , orgLevel: x.orgLvl, orgLevelDesc: x.orgLvlDesc, orgLevelValue: +(x.orgLvlVal), orgLevelValueDesc: x.orgLvlValDesc
              , dealDetailId: null, dealId: this.dealRequestId, discountType: null, discountValue: null, operation: "I"
              , dealDescription: this.dealSharedService.dealObject.dealDescription
              , dealType: this.dealSharedService.dealObject.dealTypeDesc
              , supplier: this.dealSharedService.dealObject.supplierDesc
              , effectiveEndDate: this.dealSharedService.dealObject.endDate
              , effectiveStartDate: this.dealSharedService.dealObject.startDate
              , seqNo: null, tableName: null, thresholdFreeItem: null, thresholdFreeQty: null, thresholdQty: null
              , listOfDealItemLocations: []
              , createdBy: null, createdDate: null, lastUpdatedBy: null, lastUpdatedDate: null, discountTypeDesc: null
            };

            let itmGridData: DealItemLocationModel[] = this.data ? this.data.filter(itm => (itm.item === x.itemBarcode)) : null;
            if (itmGridData.length === 0) {
              this.data.unshift(dealItmLocModel);
            } else {
              itemsExistCount = itemsExistCount + 1;
            }
          });
          if (itemsExistCount > 0) {
            this.sharedService.errorForm(itemsExistCount + " " + this.localizationData.deal.dealitemloccheck);
          }
          if (this.gridOptions.api) {
            this.gridOptions.api.setRowData(this.data);
          }
          this.changeRef.detectChanges();
        }
      });
  }

  save(saveOnly: boolean): void {
    this.saveSubscription(saveOnly);
  }

  saveAndContinue(): void {
    this.save(false);
  }

  validateData(): boolean {
    let valid: boolean = true;
    this.data.forEach(row => {
      this.mandatoryDealLocationFields.forEach(field => {
        if (!row[field]) {
          valid = false;
        }
      });
    });
    return valid;
  }

  saveSubscription(saveOnly: boolean): void {
    if (this.serviceDocument.dataProfile.profileForm.dirty) {
      this.sharedService.errorForm("The form is filled but the grid has not been updated.");
      return;
    }
    let obj: any = this;
    if (this.data.length > 0 || this.deletedRows.length > 0) {
      if (this.validateData()) {
        let redirectURL: string = "/Deal/New/DealItemLocation/";
        this.dealSharedService.save(obj, saveOnly, "listOfDealItemLocations", redirectURL
          , `${this.localizationData.deal.dealitemlocsave} - Deal Request ${this.dealRequestId}`
          , this.dealRequestId);
      } else {
        this.sharedService.errorForm(this.localizationData.deal.dealitemlocreq);
      }
    } else {
      this.sharedService.errorForm(this.localizationData.deal.dealitemlocatleastone);
    }
  }

  reset(): void {
    this.data = [];
    this.resetControls();
    Object.assign(this.data, this.dataReset);
  }

  close(): void {
    let obj: any = this;
    this.dealSharedService.close(obj, this.dealLocationsCount);
  }

  delete(cell: any): void {
    this.dealSharedService.deleteRecord(this.data, this.deletedRows, this.gridOptions.api, cell.rowIndex);
  }

  resetControls(): void {
    this.service.newModel(this.model);
    this.serviceDocument = this.service.serviceDocument;
    this.service.serviceDocument.dataProfile.profileForm.controls["dealId"].setValue(this.dealRequestId);
  }

  ngOnDestroy(): void {
    this.saveSubscribtion.unsubscribe();
    this.saveContinueSubscribtion.unsubscribe();
    this.resetSubscribtion.unsubscribe();
    this.closeSubscribtion.unsubscribe();
    if (this.searchServiceSubscription) {
      this.searchServiceSubscription.unsubscribe();
    }
    if (this.findItemsServiceSubscription) {
      this.findItemsServiceSubscription.unsubscribe();
    }
    this.previousSubscription.unsubscribe();
  }

  itemBasedLogic(): void {
    let formObj: FormGroup = this.serviceDocument.dataProfile.profileForm;
    if (formObj.controls["item"].value) {
      this.searchServiceSubscription = this.service.search().subscribe(() => {
        if (this.service.serviceDocument.result.type === MessageType.success) {
          if (this.service.serviceDocument.dataProfile.dataList && this.service.serviceDocument.dataProfile.dataList.length > 0) {
            let dataModel: DealItemLocationModel = this.service.serviceDocument.dataProfile.dataList[0];
            this.service.serviceDocument.dataProfile.profileForm.patchValue({
              orgLevel: dataModel.orgLevel,
              itemDesc: dataModel.itemDesc
            });
          } else {
            this.sharedService.errorForm(this.localizationData.deal.dealitemlocinvaliditem);
            this.service.newModel(this.model);
            this.serviceDocument = this.service.serviceDocument;
            this.service.serviceDocument.dataProfile.profileForm.controls["dealId"].setValue(this.dealRequestId);
          }
        } else {
          this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
        }
        this.changeRef.detectChanges();
      });
    }
  }

  updateMerchLevelValueData(event: any): void {
    this.serviceDocument.dataProfile.profileForm.controls["merchLevelValue"].setValue(null);
    let sd: string = this.serviceDocument.dataProfile.profileForm.controls["merchLevel"].value;

    let sdOrgLevelValue: any = this.serviceDocument.dataProfile.profileForm.controls["orgLevelValue"].value;

    if (sd === "ITEM") {
      this.merchLevelValueData = null;
      if (sdOrgLevelValue != null && sdOrgLevelValue !== undefined) {
        this.serviceDocument.dataProfile.profileForm.controls["item"].enable();
      }
      this.serviceDocument.dataProfile.profileForm.controls["merchLevelValue"].disable();
    } else {
      this.serviceDocument.dataProfile.profileForm.controls["merchLevelValue"].enable();
      this.serviceDocument.dataProfile.profileForm.controls["item"].disable();
      if (sd === "DEPT") {
        this.merchLevelValueData = this.departmentsData;
      } else if (sd === "CATEGORY") {
        this.merchLevelValueData = this.categoriesData;
      }
    }
  }

  updateOrgLevel(event: any): void {
    this.serviceDocument.dataProfile.profileForm.controls["orgLevelValue"].setValue(null);
    var sd: string = this.serviceDocument.dataProfile.profileForm.controls["orgLevel"].value;
    if (sd === "CZ") {
      this.orgLevelValueData = this.costZoneData;
    } else {
      this.orgLevelValueData = this.locationData;
    }
    this.serviceDocument.dataProfile.profileForm.controls["item"].disable();
  }

  updateOrgLevelValue(event: any): void {
    if (this.serviceDocument.dataProfile.profileForm.controls["merchLevel"].value === "ITEM") {
      this.serviceDocument.dataProfile.profileForm.controls["item"].enable();
      this.serviceDocument.dataProfile.profileForm.controls["merchLevelValue"].disable();
    }
  }

  add($event: MouseEvent): void {
    this.pForm = this.serviceDocument.dataProfile.profileForm;
    $event.preventDefault();
    if (this.pForm.valid) {
      let orgLevelDesc: string = this.orgLevelData.filter(itm => itm.code === this.pForm.controls["orgLevel"].value)[0].code;
      let orgLevelValueDesc: string = this.orgLevelValueData.filter(itm => itm.id === this.pForm.controls["orgLevelValue"].value)[0].name;
      let merchLevelDesc: string = this.merchLevelData.filter(itm => itm.code === this.pForm.controls["merchLevel"].value)[0].code;
      let discountTypeDesc: string = this.discountTypeData.filter(itm => itm.code
        === this.pForm.controls["discountType"].value)[0].code;
      var itmGridData: DealItemLocationModel[] = this.data ? this.data.filter(itm => (itm.item === this.pForm.controls["item"].value)) : null;
      this.service.serviceDocument.dataProfile.profileForm.controls["orgLevelDesc"].setValue(orgLevelDesc);
      this.service.serviceDocument.dataProfile.profileForm.controls["orgLevelValueDesc"].setValue(orgLevelValueDesc);
      this.service.serviceDocument.dataProfile.profileForm.controls["merchLevelDesc"].setValue(merchLevelDesc);
      this.service.serviceDocument.dataProfile.profileForm.controls["discountTypeDesc"].setValue(discountTypeDesc);
      if (this.pForm.controls["merchLevel"].value !== "ITEM") {
        let merchLevelValueDesc: string = this.merchLevelValueData.filter(itm => itm.id === this.pForm.controls["merchLevelValue"].value)[0].name;
        this.service.serviceDocument.dataProfile.profileForm.controls["merchLevelValueDesc"].setValue(merchLevelValueDesc);
      }

      if (!itmGridData || itmGridData.length !== 1) {
        if (!this.data) {
          this.data = [];
        }
        this.service.serviceDocument.dataProfile.profileForm.controls["operation"].setValue("I");
        this.data.push(this.service.serviceDocument.dataProfile.profileForm.getRawValue());
        this.resetControls();
        if (this.gridOptions.api) {
          this.gridOptions.api.setRowData(this.data);
        }
      } else if (itmGridData) {
        if (this.node && this.node.selected) {
          Object.assign(itmGridData[0], this.serviceDocument.dataProfile.profileForm.value);
          if (itmGridData[0].createdBy) {
            itmGridData[0].operation = "U";
          }
          if (this.gridOptions.api) {
            this.gridOptions.api.setRowData(this.data);
          }
          this.node.setSelected(false);
        } else {
          this.sharedService.errorForm(this.localizationData.deal.dealitemloccheck);
        }
        this.service.newModel(this.model);
      }
    } else {
      this.serviceDocument.dataProfile.profileForm.controls["discountType"].setValidators([Validators.required]);
      this.serviceDocument.dataProfile.profileForm.controls["discountType"].updateValueAndValidity();
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  previousTab(): void {
    this.router.navigate(["/Deal/New/DealDetail/" + this.dealRequestId], { skipLocationChange: true });
  }
}
