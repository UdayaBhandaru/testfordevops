﻿import { Component, ViewChild, OnInit, ChangeDetectorRef } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MatDialogRef } from "@angular/material";
import { CommonService, ServiceDocument, FxContext, IdentityMenuModel, MessageResult } from "@agility/frameworkcore";
import { Subscription } from "rxjs";
import { InboxService } from "../inbox/InboxService";
import { WorkflowHistoryModel } from "../inbox/WorkflowHistoryModel";
import { WorkflowHistoryPopupComponent } from "../inbox/WorkflowHistoryPopupComponent";
import { SharedService } from "../Common/SharedService";
import { DealSharedService } from "./DealSharedService";
import { RbMessageDialogComponent } from "../Common/controls/RbMessageDialogComponent";
import { LoaderService } from "../Common/LoaderService";
import { DealHeadPrintModel } from "./Header/DealHeadPrintModel";
import { CommonPrintModel } from "../Common/CommonPrintModel";
import { DocumentsConstants } from "../Common/DocumentsConstants";
import { RbPrintPopupComponent } from "../Documents/RbPrintPopupComponent";


@Component({
    selector: "deal",
    templateUrl: "./DealComponent.html"
})
export class DealComponent implements OnInit {
    dialogRef: MatDialogRef<RbMessageDialogComponent>;
    selected: string;
    childComponentName: string;
    PageTitle: string;
    public dealId: number = 0;
    workflowHistoryListObj: WorkflowHistoryModel[] = [];
    isWorkFlowStarted: boolean;
    currentItemWfStatus: string;
    objectName: string;
    public menus: IdentityMenuModel[] = [];
    printData: DealHeadPrintModel[];
    localizationData: any;
    documentsConstants: DocumentsConstants = new DocumentsConstants();
    printServiceSubscription: Subscription;
    workflowHistoryServiceSubscription: Subscription;

    constructor(
        public fxContext: FxContext
        , private dealSharedService: DealSharedService
        , private commonService: CommonService
        , private route: ActivatedRoute
        , private inboxService: InboxService
        , private sharedService: SharedService
        , private router: Router
        , private ref: ChangeDetectorRef
        , private loaderService: LoaderService
    ) {

    }

    save(): void {
        this.dealSharedService.announceSave(this.childComponentName);
    }

    saveAndContinue(): void {
        this.dealSharedService.announceSaveContinue(this.childComponentName);
    }

    reset(): void {
        this.dealSharedService.announceReset(this.childComponentName);
    }

    notify(component: any): void {
        this.childComponentName = component._componentName;
        this.setPageTitle();
    }

    close(): void {
        this.dealSharedService.announceClose(this.childComponentName);
    }

    ngOnInit(): void {
        this.dealId = this.dealSharedService.dealId ? this.dealSharedService.dealId : 0;
        this.selected = "DEAL";
        this.menus = this.fxContext.userProfile.menus.filter(itm => itm.groupMenu === 8);
        this.setPageTitle();
        this.objectName = this.documentsConstants.dealObjectName;
    }

    ngAfterViewChecked(): void {
        this.ref.detectChanges();
    }

    private setPageTitle(): void {
        this.PageTitle = `${this.childComponentName} - ADD`;
        if (this.dealId && this.dealId !== 0) {
            this.PageTitle = `${this.childComponentName} - EDIT`;
            this.PageTitle = `${this.PageTitle} - ${this.dealId}`;
        }
    }

    select(url: string): string {
        this.dealId = this.dealSharedService.dealId ? this.dealSharedService.dealId : 0;
        return url + this.dealId;
    }

    isActive(item: string): boolean {
        return this.selected === item;
    }

    executePrintCommand(): void {
        this.loaderService.display(true);
        this.printServiceSubscription = this.dealSharedService.fetchPrintData(this.dealId).subscribe(() => {
            this.printData = this.dealSharedService.serviceDocument.dataProfile.dataList;
            this.localizationData = Object.assign({}, this.dealSharedService.serviceDocument.localizationData);
            this.loaderService.display(false);
            if (this.printData != null && this.printData.length > 0) {
                let dealHead: DealHeadPrintModel = this.printData[0];
                if (dealHead.dealDetails != null && dealHead.dealDetails.length > 0) {
                    let detailObject: CommonPrintModel[] = [{
                        tableName: this.documentsConstants.dealDetailsObjectName, columns: this.dealSharedService.detailColumns
                        , data: this.dealSharedService.buildDetailObject(dealHead.dealDetails)
                    }];
                    let headObject: CommonPrintModel = {
                        tableName: this.documentsConstants.dealHeadObjectName, columns: this.dealSharedService.headColumns
                        , data: this.dealSharedService.buildHeadObject(dealHead)
                    };
                    this.sharedService.openDilogWithInputData(
                        RbPrintPopupComponent,
                        {
                            headObject: headObject, detailObject: detailObject
                        },
                        "Deal Print",
                        {
                            screenId: this.dealId.toString(), screenName: this.documentsConstants.dealObjectName
                        });
                } else {
                    this.sharedService.errorForm(this.localizationData.rbsPrint.noprintdata);
                }
            } else {
                this.sharedService.errorForm(this.localizationData.rbsPrint.noprintdata);
            }
        },
            err => {
                this.sharedService.errorForm(this.dealSharedService.serviceDocument.result.innerException);
            }
        );
    }

    nextTab(): void {
        this.dealSharedService.announceNext(this.childComponentName);
    }

    previousTab(): void {
        this.dealSharedService.announcePrevious(this.childComponentName);
    }

    viewWorkflowHistoryFromDeal($event: MouseEvent): void {
        $event.preventDefault();
        this.workflowHistoryServiceSubscription = this.inboxService.fetchWorkflowHistory(this.objectName, this.dealId).subscribe(() => {

            this.workflowHistoryListObj = this.inboxService.serviceDocumentWorkflowHistory.dataProfile.dataList;

            this.sharedService.openPopupWithDataList(
                WorkflowHistoryPopupComponent,
                this.workflowHistoryListObj,
                "Inbox Workflow",
                this.objectName,
                this.dealId);
        });
    }

    ngOnDestroy(): void {
        this.dealSharedService.dealWorkflowForm.reset();
        if (this.printServiceSubscription) {
            this.printServiceSubscription.unsubscribe();
        }
        if (this.workflowHistoryServiceSubscription) {
            this.workflowHistoryServiceSubscription.unsubscribe();
        }
    }
}