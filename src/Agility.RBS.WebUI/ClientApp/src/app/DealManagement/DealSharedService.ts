﻿import { Injectable } from "@angular/core";
import { FormGroup, FormControl, Validators, FormBuilder } from "@angular/forms";
import { HttpParams } from "@angular/common/http";
import { MessageType, ServiceDocument } from "@agility/frameworkcore";
import { SharedService } from "../Common/SharedService";
import { RbCommonService } from "../Common/RbCommonService";
import { Observable, Subject } from "rxjs";
import { DealHeadPrintModel } from "./Header/DealHeadPrintModel";
import { DealDetailPrintModel } from "./Detail/DealDetailPrintModel";

@Injectable()
export class DealSharedService extends RbCommonService {
    public dealId: number;
    public dealObject: {
        dealId: number,
        dealTypeDesc: string,
        supplierDesc: string,
        supplierId: number,
        dealDescription: string,
        startDate: Date,
        endDate: Date
    };

    dealWorkflowForm: FormGroup;
    serviceDocument: ServiceDocument<DealHeadPrintModel> = new ServiceDocument<DealHeadPrintModel>();
    headColumns = ["Deal ID", "Deal Type", "Deal Description", "Supplier", "External Reference No", "Recalculated Orders", "Start Date", "End Date"];
    detailColumns = ["Deal Detail ID", "Deal Component Type", "Application Order", "Collect Start Date", "Collect End Date"
        , "Cost Application Indicator", "Price Cost Application Indicator", "Threshold Value Type", "Transaction Discound Indicator"];

    constructor(public sharedService: SharedService, private fb: FormBuilder) {
        super(sharedService);
        this.dealWorkflowForm = this.fb.group({
            priority: ["", Validators.required],
            subComments: ""
        });
    }

    submitActions(that: any, reditectURL: string, localizationData: string, operation: string): void {
        if (that.serviceDocument.dataProfile.profileForm.valid) {
            that.serviceDocument.dataProfile.profileForm.controls["priority"].setValue(this.dealWorkflowForm.controls["priority"].value);
            that.serviceDocument.dataProfile.profileForm.controls["subComments"].setValue(this.dealWorkflowForm.controls["subComments"].value);
            if (operation === "submit") {
                that.service.submit().subscribe(() => {
                    this.commonSubmitActionResult(that, reditectURL, localizationData, true);
                });
            } else if (operation === "reject") {
                that.service.reject().subscribe(() => {
                    this.commonSubmitActionResult(that, reditectURL, localizationData);
                });
            } else if (operation === "needInfo") {
                that.service.sendBackForReview().subscribe(() => {
                    this.commonSubmitActionResult(that, reditectURL, localizationData);
                });
            } else {
                this.sharedService.errorForm("operation is not valid");
            }
        } else {
            that.sharedService.validateForm(that.serviceDocument.dataProfile.profileForm);
            this.sharedService.errorForm("Please fill required fields");
        }
    }

    commonSubmitActionResult(that: any, reditectURL: string, localizationData: string, setDealId?: boolean): void {
        if (that.service.serviceDocument.result.type === MessageType.success) {
            that.sharedService.saveForm(that.service.serviceDocument.dataProfile.dataModel.item + " " + localizationData).subscribe(() => {
                if (setDealId) {
                    this.dealId = that.dealId = +that.service.serviceDocument.dataProfile.dataModel.dealId;
                }
                that.router.navigate([reditectURL], { skipLocationChange: true });
            });
        } else {
            that.sharedService.errorForm(that.service.serviceDocument.result.innerException);
        }
    }

    fetchPrintData(dealId: number): Observable<ServiceDocument<DealHeadPrintModel>> {
        return this.serviceDocument.open("/api/DealPrint/List", new HttpParams().set("dealId", dealId.toString()));
    }

    buildHeadObject(dealHead: DealHeadPrintModel): any[] {
        let headTableObj: any[] = [];
        let effSDate: string = new Date(dealHead.startDate).toDateString();
        let effEDate: string = new Date(dealHead.closeDate).toDateString();
        let headObj: any = [dealHead.deaL_ID, dealHead.dealType, dealHead.description,
        dealHead.supplier, dealHead.externalRefNo, dealHead.recalculatedOrders, effSDate, effEDate];
        headTableObj.push(headObj);
        return headTableObj;
    }

    buildDetailObject(dealDetails: DealDetailPrintModel[]): any[] {
        let detailTableObj: any[] = [];
        dealDetails.forEach(x => {
            let detailObj: any = [x.dealDetailId, x.dealCompDesc, x.applicationOrder,
            new Date(x.collectStartDate).toDateString(),
            new Date(x.collectEndDate).toDateString(), x.costApplicationInd, x.priceCostApplicationInd, x.thresholdValueType, x.tranDiscountInd];
            detailTableObj.push(detailObj);
        });
        return detailTableObj;
    }
}