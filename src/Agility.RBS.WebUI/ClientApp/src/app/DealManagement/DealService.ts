﻿import { Injectable } from "@angular/core";
import { ServiceDocument, CommonService } from "@agility/frameworkcore";
import { DealSearchModel } from "./DealSearchModel";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";

@Injectable()
export class DealService {
    serviceDocument: ServiceDocument<DealSearchModel> = new ServiceDocument<DealSearchModel>();
    dealMasterPaging: {
        startRow: number,
        pageSize: number,
        cacheSize: number,
    } = {
        startRow: 0,
        pageSize: 10,
        cacheSize: 100,
    };

    constructor(private commonService: CommonService) { }

    list(): Observable<ServiceDocument<DealSearchModel>> {
        return this.serviceDocument.list("/api/DealSearch/List");
    }

    newModel(model: DealSearchModel): ServiceDocument<DealSearchModel> {
        return this.serviceDocument.newModel(model);
    }

    search(additionalParams: any): Observable<ServiceDocument<DealSearchModel>> {
        return this.serviceDocument.search("/api/DealSearch/Search", true, () => this.setSubClassId(additionalParams));
    }

    setSubClassId(additionalParams: any): void {
        if (additionalParams) {
            this.serviceDocument.dataProfile.dataModel.startRow = additionalParams.startRow ? additionalParams.startRow : this.dealMasterPaging.startRow;
            this.serviceDocument.dataProfile.dataModel.endRow = additionalParams.endRow ? additionalParams.endRow : this.dealMasterPaging.cacheSize;
            this.serviceDocument.dataProfile.dataModel.sortModel = additionalParams.sortModel;
        } else {
            this.serviceDocument.dataProfile.dataModel.startRow = this.dealMasterPaging.startRow;
            this.serviceDocument.dataProfile.dataModel.endRow = this.dealMasterPaging.cacheSize;
        }
    }
}