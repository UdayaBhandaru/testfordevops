﻿import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../AgGridSharedModule";
import { TitleSharedModule } from "../TitleSharedModule";
import { DealRoutingModule } from "./DealRoutingModule";
import { DealHeadComponent } from "./Header/DealHeadComponent";
import { DealComponent } from "./DealComponent";
import { DealService } from "./DealService";
import { DealListComponent } from "./DealListComponent";
import { DealResolver } from "./DealResolver";
import { MatExpansionModule } from "@angular/material";
import { DealHeadService } from "./Header/DealHeadService";
import { DealHeadResolver } from "./Header/DealHeadResolver";
import { DealSharedService } from "./DealSharedService";
import { LicenseManager } from "ag-grid-enterprise/main";
import { DealDetailComponent } from "./Detail/DealDetailComponent";
import { DealDetailResolver } from "./Detail/DealDetailResolver";
import { DealDetailService } from "./Detail/DealDetailService";
import { DealItemLocationComponent } from "./Location/DealItemLocationComponent";
import { DealItemLocationResolver } from "./Location/DealItemLocationResolver";
import { DealItemLocationService } from "./Location/DealItemLocationService";
import { InboxService } from "../inbox/InboxService";
import { RbsSharedModule } from "../RbsSharedModule";

@NgModule({
    imports: [
        FrameworkCoreModule,
        DealRoutingModule,
        AgGridModule,
        AgGridSharedModule,
        TitleSharedModule,
        MatExpansionModule,
        RbsSharedModule
    ],
    declarations: [
        DealComponent,
        DealHeadComponent,
        DealListComponent,
        DealDetailComponent,
        DealItemLocationComponent
    ],
    providers: [
        DealService,
        DealResolver,
        DealHeadService,
        DealHeadResolver,
        DealSharedService,
        DealDetailResolver,
        DealDetailService,
        DealItemLocationResolver,
        DealItemLocationService,
        InboxService
    ]

})
export class DealModule {
    constructor() {
        LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
    }
}

