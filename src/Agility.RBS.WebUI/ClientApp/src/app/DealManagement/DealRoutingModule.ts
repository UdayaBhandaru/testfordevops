﻿import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { DealListComponent } from "./DealListComponent";
import { DealComponent } from "./DealComponent";
import { DealHeadComponent } from "./Header/DealHeadComponent";
import { DealResolver } from "./DealResolver";
import { DealHeadResolver } from "./Header/DealHeadResolver";
import { DealDetailComponent } from "./Detail/DealDetailComponent";
import { DealDetailResolver } from "./Detail/DealDetailResolver";
import { DealItemLocationComponent } from "./Location/DealItemLocationComponent";
import { DealItemLocationResolver } from "./Location/DealItemLocationResolver";
import RbHeadGuard from "../guards/RbHeadGuard";
import RbDetailsGuard from "../guards/RbDetailsGuard";

const DealRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: DealListComponent,
                resolve:
                    {
                        serviceDocument: DealResolver
                    }
            },
            {
                path: "New",
                component: DealComponent,
                children: [
                    {
                        path: "", redirectTo: "DealHead/", pathMatch: "full"
                    },
                    {
                        path: "DealHead/:id",
                        component: DealHeadComponent,
                        resolve: {
                            serviceDocument: DealHeadResolver
                        },
                        canDeactivate: [RbHeadGuard]
                    },
                    {
                        path: "DealDetail/:id",
                        component: DealDetailComponent,
                        resolve: {
                            serviceDocument: DealDetailResolver
                        },
                        canDeactivate: [RbDetailsGuard]
                    },
                    {
                        path: "DealItemLocation/:id",
                        component: DealItemLocationComponent,
                        resolve: {
                            serviceDocument: DealItemLocationResolver
                        },
                        canDeactivate: [RbDetailsGuard]
                    }
                ]
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(DealRoutes)
    ]
})
export class DealRoutingModule { }
