﻿export class DealSearchModel {
    supplier?: number;
    supplierName: string;
    itemBarcode: string;
    itemDescription: string;
    orgLevel: string;
    orgLevelValue?: number;
    merchLevel: string;
    merchLevelValue?: number;
    deaL_ID?: number;
    dealDescription: string;
    dealTypeId?: string;
    dealType: string;
    dealStatusId?: string;
    dealStatus: string;
    dateTypeId?: string;
    dateType: string;
    fromDate?: Date;
    toDate?: Date;
    operation: string;
    tableName: string;
    dealDetailId?: number;
    startRow?: number;
    endRow?: number;
    totalRows?: number;
    sortModel?: {}[];
}