﻿import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpParams } from "@angular/common/http";
import { ServiceDocument } from "@agility/frameworkcore";
import { DealDetailModel } from "./DealDetailModel";
import { SharedService } from "../../Common/SharedService";

@Injectable()
export class DealDetailService {
    serviceDocument: ServiceDocument<DealDetailModel> = new ServiceDocument<DealDetailModel>();

    constructor(private sharedService: SharedService) { }

    newModel(model: DealDetailModel): ServiceDocument<DealDetailModel> {
        return this.serviceDocument.newModel(model);
    }

    save(): Observable<ServiceDocument<DealDetailModel>> {
        return this.serviceDocument.save("/api/DealDetail/Save", true, () => this.setDate());
    }

    list(id: number): Observable<ServiceDocument<DealDetailModel>> {
        return this.serviceDocument.open("/api/DealDetail/List", new HttpParams().set("id", id.toString()));
    }

    setDate(): void {
        let sd: DealDetailModel = this.serviceDocument.dataProfile.dataModel;

        sd.dealDetails.forEach(x => {
            if (x.collectStartDate != null) {
                x.collectStartDate = this.sharedService.setOffSet(x.collectStartDate);
            }
            if (x.collectEndDate != null) {
                x.collectEndDate = this.sharedService.setOffSet(x.collectEndDate);
            }
        });
    }
}