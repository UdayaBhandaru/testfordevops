﻿export class DealDetailPrintModel {
    dealId: number;
    dealDetailId: number;
    dealType: string;
    dealDescription: string;
    dealCompDesc: string;
    supplier: string;
    dealCompType: string;
    applicationOrder?: number;
    collectStartDate?: Date;
    collectEndDate?: Date;
    costApplicationInd: string;
    priceCostApplicationInd: string;
    thresholdValueType: string;
    tranDiscountInd: string;
    comments: string;
}