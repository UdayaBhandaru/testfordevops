﻿export class DealDetailModel {
    dealId?: number;
    dealDetailId?: number;
    dealCompDesc: string;
    dealCompType: string;
    applicationOrder?: number;
    collectStartDate?: Date;
    collectEndDate?: Date;
    costApplicationInd: string;
    priceCostApplicationInd: string;
    dealClass: string;
    dealClassDesc: string;
    thresholdValueType: string;
    tranDiscountInd: string;
    comments: string;
    operation: string;
    tableName?: string;
    dealDetails: DealDetailModel[];
    status: string;
    createdBy?: string;

    // get the data from the head
    dealType: string;
    dealDescription: string;
    supplier: string;
    supplierId?: number;
}