﻿import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { DealDetailModel } from "./DealDetailModel";
import { DealDetailService } from "./DealDetailService";

@Injectable()
export class DealDetailResolver implements Resolve<ServiceDocument<DealDetailModel>> {
    constructor(private service: DealDetailService) { }
    resolve(route: ActivatedRouteSnapshot): Observable<ServiceDocument<DealDetailModel>> {
        return this.service.list(route.params["id"]);
    }
}