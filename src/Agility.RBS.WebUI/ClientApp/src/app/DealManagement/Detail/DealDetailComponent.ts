import { Component, OnInit, ChangeDetectorRef, OnDestroy, AfterViewChecked } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ServiceDocument, CommonService, MessageType, MessageResult } from "@agility/frameworkcore";
import { FormGroup, FormControl } from "@angular/forms";
import { SharedService } from "../../Common/SharedService";
import { GridOptions } from "ag-grid-community";
import { DealDetailService } from "./DealDetailService";
import { DealDetailModel } from "./DealDetailModel";
import { GridDeleteComponent } from "../../Common/grid/GridDeleteComponent";
import { DealSharedService } from "../DealSharedService";
import { Observable } from "rxjs";
import { Subscription } from "rxjs";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { MatDialogRef } from "@angular/material";
import { GridDateComponent } from "../../Common/grid/GridDateComponent";
import { RbDialogService } from "../../Common/controls/RbDialogService";

@Component({
  selector: "deal-detail",
  templateUrl: "./DealDetailComponent.html"
})
export class DealDetailComponent implements OnInit, AfterViewChecked, OnDestroy {
  dialogRef: MatDialogRef<any>;
  dealId: number;
  public serviceDocument: ServiceDocument<DealDetailModel>;
  public gridOptions: GridOptions;
  PageTitle = "Details";
  _componentName = "Deal Details"; // don't remove this, used as global param.
  pForm: FormGroup;
  componentName: any;
  columns: any[];
  public sheetName: string = "Deal Details";
  data: DealDetailModel[] = [];
  dataReset: DealDetailModel[] = [];
  model: DealDetailModel = {
    dealId: this.dealId, dealDetailId: null
    , dealType: this.dealSharedService.dealObject.dealTypeDesc, dealCompDesc: null
    , supplier: this.dealSharedService.dealObject.supplierDesc
    , supplierId: this.dealSharedService.dealObject.supplierId, dealCompType: null
    , collectStartDate: this.dealSharedService.dealObject.startDate, collectEndDate: this.dealSharedService.dealObject.endDate
    , applicationOrder: null, costApplicationInd: null
    , priceCostApplicationInd: null, dealClass: null, dealClassDesc: null, thresholdValueType: null
    , tranDiscountInd: null, comments: null, dealDetails: [], status: "WKS", operation: "I"
    , dealDescription: this.dealSharedService.dealObject.dealDescription
  };
  localizationData: any;
  deletedRows: any[] = [];
  dealDetailsCount: number;
  node: any;
  saveContinueSubscribtion: Subscription;
  resetSubscribtion: Subscription;
  saveSubscribtion: Subscription;
  closeSubscribtion: Subscription;
  routeServiceSubscription: Subscription;
  previousSubscription: Subscription;
  nextSubscription: Subscription;
  public messageResult: MessageResult = new MessageResult();
  canNavigate: boolean = false;
  addText: string = "+ Add To Grid";
  dealCompName: string;
  dealCompTypes: DomainDetailModel[];
  domainDetailsIndicator: DomainDetailModel[];
  orginalValue: string = "";

  constructor(private service: DealDetailService, private route: ActivatedRoute, private sharedService: SharedService
    , public router: Router, private changeRef: ChangeDetectorRef, private commonService: CommonService
    , public dealSharedService: DealSharedService
    , public dialogService: RbDialogService) {
    this.saveSubscribtion = dealSharedService.saveAnnounced$.subscribe((response: any) => {
      if (response === this._componentName) {
        this.save(true);
      }
    });
    this.saveContinueSubscribtion = dealSharedService.saveContinueAnnounced$.subscribe((response: string) => {
      if (response === this._componentName) {
        this.save(false);
      }
    });
    this.resetSubscribtion = dealSharedService.resetAnnounced$.subscribe((response: string) => {
      if (response === this._componentName) {
        this.reset();
      }
    });
    this.closeSubscribtion = dealSharedService.closeAnnounced$.subscribe((response: string) => {
      if (response === this._componentName) {
        this.close();
      }
    });
    this.previousSubscription = dealSharedService.previousAnnounced$.subscribe((response: string) => {
      if (response === this._componentName) {
        this.previousTab();
      }
    });
    this.nextSubscription = dealSharedService.nextAnnounced$.subscribe((response: string) => {
      if (response === this._componentName) {
        this.nextTab();
      }
    });
  }

  reset(): void {
    this.data = [];
    this.resetControls();
    Object.assign(this.data, this.dataReset);
  }

  close(): void {
    let obj: any = this;
    this.dealSharedService.close(obj, this.dealDetailsCount);
  }

  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }

  ngOnInit(): void {
    this.routeServiceSubscription = this.route.params.subscribe((resp: any) => {
      this.dealId = +resp["id"];
    });

    this.componentName = this;
    this.service.serviceDocument.dataProfile.dataModel = this.model;
    this.columns = [
      { headerName: "Detail Id", field: "dealDetailId", tooltipField: "dealDetailId" },
      { headerName: "Deal Component Type", field: "dealCompDesc", tooltipField: "dealCompDesc" },
      {
        headerName: "Collect Start Date", field: "collectStartDate", tooltipField: "collectStartDate",
        cellRendererFramework: GridDateComponent
      },
      {
        headerName: "Collect End Date", field: "collectEndDate", tooltipField: "collectEndDate",
        cellRendererFramework: GridDateComponent
      },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridDeleteComponent, suppressCellSelection: true, width: 20
      }
    ];
    this.gridOptions = {
      onRowSelected: this.onRowSelected,
      onGridReady: () => {
        this.gridOptions.api.sizeColumnsToFit();
      },
      context: {
        componentParent: this
      }
    };
    this.gridOptions = Object.assign(this.gridOptions, this.dealSharedService.gridOptions);
    this.gridOptions.defaultColDef.headerCheckboxSelection = false;
    this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
    if (this.service.serviceDocument.dataProfile.dataList) {
      Object.assign(this.data, this.service.serviceDocument.dataProfile.dataList);
      Object.assign(this.dataReset, this.service.serviceDocument.dataProfile.dataList);
      this.orginalValue = JSON.stringify(this.dataReset);
    }
    this.dealCompTypes = Object.assign([], this.service.serviceDocument.domainData["dealcomptype"]);
    this.domainDetailsIndicator = Object.assign([], this.service.serviceDocument.domainData["indicator"]);
    this.service.newModel(this.model);
    this.service.serviceDocument.dataProfile.profileForm.patchValue(this.service.serviceDocument.dataProfile.dataModel);
    this.serviceDocument = this.service.serviceDocument;
    this.dealDetailsCount = this.data.length;
    this.service.serviceDocument.dataProfile.profileForm.controls["dealId"].setValue(this.dealId);
  }

  onRowSelected(event: any): void {
    let that: any = event.context.componentParent;
    if (event.node.selected) {
      that.serviceDocument.dataProfile.profileForm = that.commonService.getFormGroup(event.data, 2);
      that.node = event.node;
      that.addText = " Update";
      that.changeRef.detectChanges();
    } else if (that.node !== event.node) {
      // when we select other row when one row already selected
    } else {
      that.addText = "+ Add To Grid";
      that.service.newModel(that.model);
      that.serviceDocument = that.service.serviceDocument;
      that.serviceDocument.dataProfile.profileForm.controls["dealId"].setValue(that.dealId);
      that.editMode = false;
      that.changeRef.detectChanges();
    }
  }

  add($event: MouseEvent): void {
    this.pForm = this.serviceDocument.dataProfile.profileForm;
    $event.preventDefault();
    if (this.pForm.valid) {
      this.dealCompName = this.dealCompTypes.filter(x => x.code === this.pForm.controls["dealCompType"].value)[0].code;
      let dealDetailData: DealDetailModel[] = this.data ? this.data.filter(itm => itm.dealCompType === this.pForm.controls["dealCompType"].value
        && itm.collectStartDate === this.pForm.controls["collectStartDate"].value
        && itm.collectEndDate === this.pForm.controls["collectEndDate"].value) : null;
      this.service.serviceDocument.dataProfile.profileForm.controls["dealCompDesc"].setValue(this.dealCompName);
      if (!dealDetailData || dealDetailData.length !== 1) {
        if (!this.data) {
          this.data = [];
        }
        this.service.serviceDocument.dataProfile.profileForm.controls["operation"].setValue("I");
        this.data.push(this.service.serviceDocument.dataProfile.profileForm.getRawValue());
        this.resetControls();
        if (this.gridOptions.api) {
          this.gridOptions.api.setRowData(this.data);
        }
      } else if (dealDetailData) {
        if (this.node && this.node.selected) {
          Object.assign(dealDetailData[0], this.service.serviceDocument.dataProfile.profileForm.value);
          if (dealDetailData[0].createdBy) {
            dealDetailData[0].operation = "U";
          }
          if (this.gridOptions.api) {
            this.gridOptions.api.setRowData(this.data);
          }
          this.node.setSelected(false);
        } else {
          this.sharedService.errorForm(this.localizationData.deal.dealdetailcheck);
        }
        this.service.newModel(this.model);
      }
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  save(saveOnly: boolean): void {
    this.saveSubscription(saveOnly);
  }

  saveAndContinue(): void {
    this.save(false);
  }

  saveSubscription(saveOnly: boolean): void {
    let obj: any = this;
    if (this.data.length > 0 || this.deletedRows.length > 0) {
      this.dealSharedService.save(obj, saveOnly, "dealDetails", "/Deal/New/DealDetail/"
        , `${this.localizationData.deal.dealdetailsave} - Deal ID ${this.dealId}`, this.dealId);
    } else {
      this.sharedService.errorForm(this.localizationData.deal.dealcheck);
    }
  }

  delete(cell: any): void {
    this.dealSharedService.deleteRecord(this.data, this.deletedRows, this.gridOptions.api, cell.rowIndex);
  }

  resetControls(): void {
    this.service.newModel(this.model);
    this.serviceDocument = this.service.serviceDocument;
    this.service.serviceDocument.dataProfile.profileForm.controls["dealId"].setValue(this.dealId);
  }

  ngOnDestroy(): void {
    this.saveSubscribtion.unsubscribe();
    this.saveContinueSubscribtion.unsubscribe();
    this.resetSubscribtion.unsubscribe();
    this.closeSubscribtion.unsubscribe();
    this.routeServiceSubscription.unsubscribe();
    this.previousSubscription.unsubscribe();
    this.nextSubscription.unsubscribe();
  }

  previousTab(): void {
    this.router.navigate(["/Deal/New/DealHead/" + this.dealId], { skipLocationChange: true });
  }

  nextTab(): void {
    if (this.dealId) {
      this.router.navigate(["/Deal/New/DealItemLocation/" + this.dealId], { skipLocationChange: true });
    }
  }
}
