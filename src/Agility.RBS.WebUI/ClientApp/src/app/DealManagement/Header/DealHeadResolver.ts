﻿import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { DealHeadService } from "./DealHeadService";
import { DealHeadModel } from "./DealHeadModel";

@Injectable()
export class DealHeadResolver implements Resolve<ServiceDocument<DealHeadModel>> {
    constructor(private service: DealHeadService) { }
    resolve(route: ActivatedRouteSnapshot): Observable<ServiceDocument<DealHeadModel>> {
        return this.service.addEdit(route.params["id"]);
    }
}