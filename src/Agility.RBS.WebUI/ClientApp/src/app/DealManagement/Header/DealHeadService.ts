﻿import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpParams } from "@angular/common/http";
import { ServiceDocument, WorkflowAction } from "@agility/frameworkcore";
import { DealHeadModel } from "./DealHeadModel";
import { SharedService } from "../../Common/SharedService";

@Injectable()
export class DealHeadService {
    serviceDocument: ServiceDocument<DealHeadModel> = new ServiceDocument<DealHeadModel>();

    constructor(private sharedService: SharedService) { }

    addEdit(id: number): Observable<ServiceDocument<DealHeadModel>> {
        if (+id === 0) {
            return this.serviceDocument.new("/api/DealHead/New");
        } else {
            return this.serviceDocument.open("/api/DealHead/Open", new HttpParams().set("id", id.toString()));
        }
    }

    discard(): Observable<ServiceDocument<DealHeadModel>> {
        return this.serviceDocument.delete("/api/DealHead/Delete");
    }

    submit(): Observable<ServiceDocument<DealHeadModel>> {
        this.serviceDocument.dataProfile.profileForm.controls["status"].setValue("SUB");
        return this.serviceDocument.submit("/api/DealHead/Submit", true);
    }

    save(): Observable<ServiceDocument<DealHeadModel>> {
        this.serviceDocument.dataProfile.profileForm.controls["status"].setValue("WST");
        let currentAction: WorkflowAction = this.serviceDocument.dataProfile.actionService.allowedActions
            .find((v) => v.transitionClaim === "DealStartCreatedClaim");
        if (!this.serviceDocument.dataProfile.dataModel.deaL_ID && currentAction) {
            this.serviceDocument.dataProfile.profileForm.controls["currentActionId"].setValue(currentAction.transitionClaim);
            return this.serviceDocument.submit("/api/DealHead/Submit", true, () => this.setDate());
        } else {
            return this.serviceDocument.save("/api/DealHead/Save", true, () => this.setDate());
        }
    }

    setDate(): void {
        let sd: DealHeadModel = this.serviceDocument.dataProfile.dataModel;
        if (sd !== null) {
            if (sd.startDate != null) {
                sd.startDate = this.sharedService.setOffSet(sd.startDate);
            }

            if (sd.closeDate != null) {
                sd.closeDate = this.sharedService.setOffSet(sd.closeDate);
            }
        }
    }
}