﻿import { Component, OnInit, TemplateRef, ViewChild, ChangeDetectorRef, OnDestroy } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { DealHeadModel } from "./DealHeadModel";
import { MatDialogRef } from "@angular/material";
import { ServiceDocument, CommonService, MessageResult, MessageType } from "@agility/frameworkcore";
import { Subscription } from "rxjs";
import { SharedService } from "../../Common/SharedService";
import { LoaderService } from "../../Common/LoaderService";
import { DealHeadService } from "./DealHeadService";
import { DealSharedService } from "../DealSharedService";
import { SupplierDomainModel } from "../../Common/DomainData/SupplierDomainModel";
import { RbButton } from "../../Common/controls/RbControlModels";
import { RbDialogService } from "../../Common/controls/RbDialogService";
import { CurrencyDomainModel } from "../../Common/DomainData/CurrencyDomainModel";

@Component({
    selector: "deal-basic",
    templateUrl: "./DealHeadComponent.html"
})
export class DealHeadComponent implements OnInit, OnDestroy {
    _componentName = "Deal Head";
    localizationData: any;
    public serviceDocument: ServiceDocument<DealHeadModel>;
    primaryId: number; // used in CanDeactivate method, should be same name if we use candeactivate
    supplier: SupplierDomainModel[] = [];
    domainDetailsIndicator: DomainDetailModel[];
    currencyData: CurrencyDomainModel[];
    dealTypeData: DomainDetailModel[];
    editMode: boolean;
    model: DealHeadModel;
    dialogRef: MatDialogRef<any>;
    private messageResult: MessageResult = new MessageResult();
    canNavigate: boolean = false;
    saveContinueSubscribtion: Subscription;
    resetSubscribtion: Subscription;
    saveSubscribtion: Subscription;
    closeSubscribtion: Subscription;
    submitSubscription: Subscription;
    needInfoSubscription: Subscription;
    rejectSubscription: Subscription;
    discardSubscription: Subscription;
    routeServiceSubscription: Subscription;
    saveServiceSubscription: Subscription;
    saveSuccessSubscription: Subscription;
    nextSubscription: Subscription;
    effectiveMinDate: Date;

    constructor(private route: ActivatedRoute, private router: Router, private ref: ChangeDetectorRef,
        private sharedService: SharedService, private service: DealHeadService, private dealSharedService: DealSharedService
        , private commonService: CommonService, private loaderService: LoaderService
        , private dialogService: RbDialogService) {
        this.saveSubscribtion = dealSharedService.saveAnnounced$.subscribe((response: string) => {
            if (response === this._componentName) {
                this.save(true);
            }
        });

        this.saveContinueSubscribtion = dealSharedService.saveContinueAnnounced$.subscribe((response: string) => {
            if (response === this._componentName) {
                this.save(false);
            }
        });

        this.resetSubscribtion = dealSharedService.resetAnnounced$.subscribe((response: string) => {
            if (response === this._componentName) {
                this.reset();
            }
        });

        this.closeSubscribtion = dealSharedService.closeAnnounced$.subscribe((response: string) => {
            if (response === this._componentName) {
                this.close();
            }
        });
        this.nextSubscription = dealSharedService.nextAnnounced$.subscribe((response: string) => {
            if (response === this._componentName) {
                this.nextTab();
            }
        });
    }

    ngAfterViewChecked(): void {
        this.ref.detectChanges();
    }

    ngOnInit(): void {
        this.bind();
    }

    bind(): void {
        this.routeServiceSubscription = this.route.params.subscribe((resp: any) => {
            this.primaryId = resp["id"];
        });
        this.dealSharedService.dealId = this.primaryId;
        this.dealTypeData = Object.assign([], this.service.serviceDocument.domainData["dealtype"]);
        this.supplier = Object.assign([], this.service.serviceDocument.domainData["supplier"]);
        this.domainDetailsIndicator = Object.assign([], this.service.serviceDocument.domainData["indicator"]);
        this.currencyData = Object.assign([], this.service.serviceDocument.domainData["currency"]);
        if (this.primaryId.toString() !== "0") {
            this.editMode = true;
            this.dealSharedService.dealObject = {
                dealId: this.service.serviceDocument.dataProfile.dataModel.deaL_ID,
                dealTypeDesc: this.dealTypeData.find(x => x.code === this.service.serviceDocument.dataProfile.dataModel.dealType).code,
                dealDescription: this.service.serviceDocument.dataProfile.dataModel.description,
                supplierId: this.service.serviceDocument.dataProfile.dataModel.supplier,
                supplierDesc: this.supplier.find(x => x.supplier === this.service.serviceDocument.dataProfile.dataModel.supplier).supNameDesc,
                startDate: this.service.serviceDocument.dataProfile.dataModel.startDate,
                endDate: this.service.serviceDocument.dataProfile.dataModel.closeDate
            };
            this.effectiveMinDate = this.service.serviceDocument.dataProfile.profileForm.controls["startDate"].value;
        } else {
            this.model = Object.assign({}, this.sharedService.itemListData);
            this.service.serviceDocument.dataProfile.dataModel = Object.assign(this.service.serviceDocument.dataProfile.dataModel
                , this.model, { operation: "I" }, { workFlowInstance: this.service.serviceDocument.dataProfile.dataModel.workFlowInstance });
            this.service.serviceDocument.dataProfile.profileForm.patchValue(this.service.serviceDocument.dataProfile.dataModel);
            this.effectiveMinDate = new Date();
        }
        this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
        this.serviceDocument = this.service.serviceDocument;
        this.sharedService.fileCount = this.service.serviceDocument.dataProfile.dataModel.fileCount;
    }

    save(saveOnly: boolean): void {
        if (this.serviceDocument.dataProfile.profileForm.valid) {
            this.saveSubscription(saveOnly);
        } else {
            this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
        }
    }

    saveAndContinue(): void {
        this.save(false);
    }

    saveSubscription(saveOnly: boolean): void {
        this.loaderService.display(true);
        this.saveServiceSubscription = this.service.save().subscribe(() => {
            if (this.service.serviceDocument.result.type === MessageType.success) {
                this.primaryId = this.service.serviceDocument.dataProfile.dataModel.deaL_ID;
                this.dealSharedService.dealId = this.primaryId;
                this.dealSharedService.dealObject = {
                    dealId: this.service.serviceDocument.dataProfile.dataModel.deaL_ID,
                    dealTypeDesc: this.dealTypeData.find(x => x.code
                        === this.service.serviceDocument.dataProfile.dataModel.dealType).code,
                    dealDescription: this.service.serviceDocument.dataProfile.dataModel.description,
                    supplierId: this.service.serviceDocument.dataProfile.dataModel.supplier,
                    supplierDesc: this.supplier.find(x => x.supplier === this.service.serviceDocument.dataProfile.dataModel.supplier).supNameDesc,
                    startDate: this.service.serviceDocument.dataProfile.dataModel.startDate,
                    endDate: this.service.serviceDocument.dataProfile.dataModel.closeDate
                };
                this.saveSuccessSubscription = this.sharedService.saveForm(
                    `${this.localizationData.deal.dealsave} - Deal ID ${this.primaryId}`).subscribe(() => {
                        if (saveOnly) {
                            this.router.navigate(["/Deal/List"], { skipLocationChange: true });
                        } else {
                            this.router.navigate(["/Blank"], {
                                skipLocationChange: true
                                , queryParams: { id: "/Deal/New/DealDetail/" + this.primaryId }
                            });
                        }
                    });
            } else {
                this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
            }
            this.loaderService.display(false);
        });
    }

    submit(flag: boolean): void {
        this.loaderService.display(true);
        this.service.submit().subscribe(() => {
            if (this.service.serviceDocument.result.type === MessageType.success) {
                this.sharedService.saveForm(`Submitted - Deal Id ${this.primaryId}`).subscribe(() => {
                    this.router.navigate(["/Deal/List"], { skipLocationChange: true });
                });
            } else {
                this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
            }
            this.loaderService.display(false);
        });
    }

    ngOnDestroy(): void {
        this.saveSubscribtion.unsubscribe();
        this.saveContinueSubscribtion.unsubscribe();
        this.resetSubscribtion.unsubscribe();
        this.closeSubscribtion.unsubscribe();
        this.routeServiceSubscription.unsubscribe();
        this.nextSubscription.unsubscribe();
        if (this.saveServiceSubscription) {
            this.saveServiceSubscription.unsubscribe();
        }
        if (this.saveSuccessSubscription) {
            this.saveSuccessSubscription.unsubscribe();
        }
    }

    reset(): void {
        Object.keys(this.serviceDocument.dataProfile.profileForm.controls).forEach(control => {
            this.serviceDocument.dataProfile.profileForm.controls[control].setValue(this.serviceDocument.dataProfile.dataModel[control]);
        });
        this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
    }

    close(): void {
        this.messageResult.message = `<div>All your changes will be lost if you don't save them.</div>
                                    <div>Click on “Save” to Keep the changes.</div>
                                    <div>Click on “Don't save” to proceed further without saving changes.</div>
                                    <div>Click on “Cancel” to stay on the current page.</div>`;
        if ((this.serviceDocument.dataProfile.profileForm && this.serviceDocument.dataProfile.profileForm.dirty)
            || (this.serviceDocument.dataProfile.profileForm.dirty && !this.serviceDocument.dataProfile.profileForm.valid)) {
            this.dialogRef = this.dialogService.openMessageDialog("Warning", this.messageResult, [new RbButton("", "Cancel", "alertCancel")
                , new RbButton("", "Don't Save", "alertdontsave")
                , new RbButton("", "Save", "alertSave")], "37%", "", "iconsdontsave retailsNavIcons", "dontsaveAlert", "dontsaveAlertTitle");
            this.dialogRef.componentInstance.click.subscribe(
                btnName => {
                    if (btnName === "Cancel") {
                        this.dialogRef.close();
                    }
                    if (btnName === "Don't Save") {
                        this.serviceDocument.dataProfile.profileForm.markAsUntouched();
                        this.canNavigate = true;
                        this.dialogRef.close();
                        this.router.navigate([this.sharedService.previousUrl], { skipLocationChange: true });
                    }
                    if (btnName === "Save") {
                        this.dialogRef.close();
                        this.save(true);
                        this.router.navigate([this.sharedService.previousUrl], { skipLocationChange: true });
                    }
                });
        } else {
            this.canNavigate = true;
            this.router.navigate([this.sharedService.previousUrl], { skipLocationChange: true });
        }
    }

    nextTab(): void {
        if (this.primaryId) {
            this.router.navigate(["/Deal/New/DealDetail/" + this.primaryId], { skipLocationChange: true });
        }
    }
}
