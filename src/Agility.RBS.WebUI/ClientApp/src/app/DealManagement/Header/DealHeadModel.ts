﻿export class DealHeadModel {
    deaL_ID: number;
    dealType: string;
    description: string;
    supplier?: number;
    currencyCode: string;
    externalRefNo: string;
    orderNo?: number;
    recalculateApprovedOrders: string;
    startDate?: Date;
    closeDate?: Date;
    comments: string;
    fileCount?: number;
    operation: string;
    workFlowInstance: string;
    organizationId: number;
    modifiedBy: string;
    deletedInd: boolean;
    modifiedDate: Date;
    createdDate: Date;
    createdBy: string;
}