﻿import { DealDetailPrintModel } from "../Detail/DealDetailPrintModel";

export class DealHeadPrintModel {
    dealDetails: DealDetailPrintModel[];
    dealType: string;
    deaL_ID: number;
    description: string;
    supplier: string;
    currencyCode: string;
    externalRefNo: string;
    orderNo? : number;
    recalculatedOrders: string;
    startDate? : Date;
    closeDate? : Date;
    comments: string;
}