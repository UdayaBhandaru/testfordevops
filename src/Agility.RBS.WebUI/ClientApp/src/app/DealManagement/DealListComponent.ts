import { Component, OnInit, ChangeDetectorRef, ViewChild } from "@angular/core";
import { ServiceDocument } from "@agility/frameworkcore";
import { DealSearchModel } from "./DealSearchModel";
import { ColDef, GridOptions, GridReadyEvent, IServerSideGetRowsParams, IServerSideDatasource } from "ag-grid-community";
import { GridExportModel } from "../Common/grid/GridExportModel";
import { FormGroup, FormControl } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { SharedService } from "../Common/SharedService";
import { DealService } from "../DealManagement/DealService";
import { LoaderService } from "../Common/LoaderService";
import { GridEditComponent } from "../Common/grid/GridEditComponent";
import { SearchComponent } from "../Common/SearchComponent";
import { InfoComponent } from "../Common/InfoComponent";
import { SupplierMasterModel } from "../Supplier/SupplierMaster/SupplierMasterModel";
import { DomainDetailModel } from "../domain/DomainDetailModel";
import { DealSharedService } from "../DealManagement/DealSharedService";
import { CommonModel } from "../Common/CommonModel";
import { GridDateComponent } from "../Common/grid/GridDateComponent";

@Component({
  selector: "deal-list",
  templateUrl: "./DealListComponent.html"
})
export class DealListComponent implements OnInit, IServerSideDatasource {
  loadedPage: boolean;
  PageTitle = "Deal Management";
  sheetName: string = "Deal Management";
  redirectUrl = "Deal/New/DealHead/";
  serviceDocument: ServiceDocument<DealSearchModel>;
  componentName: any;
  columns: ColDef[];
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  model: DealSearchModel;
  additionalSearchParams: any;
  formControl: FormControl;
  gridOptions: GridOptions;
  @ViewChild("searchPanelDeal") searchPanelDeal: SearchComponent;
  invalid: boolean = false;
  supplierData: SupplierMasterModel[] = [];
  dealTypeData: DomainDetailModel[];
  dealStatusData: DomainDetailModel[];
  dateTypeData: DomainDetailModel[];
  searchProps: string[] = [
    "supplier", "itemBarcode", "itemDescription", "orgLevelValue", "merchLevelValue",
    "deaL_ID", "dealDescription", "dealTypeId", "dealStatusId",
    "dateType", "fromDate", "toDate"];
  isSearchParamGiven: boolean = false;
  orgLevelData: DomainDetailModel[];
  merchLevelData: DomainDetailModel[];
  orgLevelValueData: CommonModel[] = [];
  locationData: CommonModel[];
  costZoneData: CommonModel[];
  departmentsData: CommonModel[];
  categoriesData: CommonModel[];
  merchLevelValueData: CommonModel[];

  ngOnInit(): void {
    this.columns = [
      { colId: "DEAL_ID", headerName: "Deal Id", field: "deaL_ID", tooltipField: "deaL_ID", width: 60, sort: "desc" },
      { colId: "DEAL_DESC", headerName: "Deal Description", field: "dealDescription", tooltipField: "dealDescription", width: 140 },
      { colId: "DEAL_TYPE_DESC", headerName: "Deal Type", field: "dealType", tooltipField: "dealType", width: 120 },
      { colId: "SUPPLIER_NAME", headerName: "Supplier", field: "supplierName", tooltipField: "supplierName", width: 140 },
      {
        colId: "FROM_DATE", headerName: "Effective From", field: "fromDate", tooltipField: "fromDate | date:'dd-MMM - yyyy'"
        , cellRendererFramework: GridDateComponent, width: 120
      },
      {
        colId: "TO_DATE", headerName: "Effective To", field: "toDate", tooltipField: "toDate | date:'dd-MMM - yyyy'"
        , cellRendererFramework: GridDateComponent, width: 120
      },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 60, suppressSorting: true
      }
    ];
    this.gridOptions = {
      sortingOrder: ["asc", "desc"],
      defaultColDef: {
        filterParams: {
          newRowsAction: "keep"
        }
      },
      onGridReady: (params: GridReadyEvent) => {
        this.gridOptions.api.sizeColumnsToFit();
        params.api.setEnterpriseDatasource(this);
      },
      context: {
        componentParent: this
      },
      headerHeight: 150,
      suppressPaginationPanel: true,
      suppressScrollOnNewData: false,
      rowHeight: 40,
      enableColResize: true,
      suppressRowClickSelection: true,
      suppressCellSelection: true,
      domLayout: "autoHeight",
      embedFullWidthRows: true,
      rowModelType: "enterprise",
      paginationPageSize: this.service.dealMasterPaging.pageSize,
      pagination: true,
      enableServerSideSorting: true,
      enableServerSideFilter: true,
      cacheBlockSize: this.service.dealMasterPaging.cacheSize
    };

    this.supplierData = Object.assign([], this.service.serviceDocument.domainData["supplier"]);
    this.dealTypeData = Object.assign([], this.service.serviceDocument.domainData["dealtype"]);
    this.dealStatusData = Object.assign([], this.service.serviceDocument.domainData["dealstatus"]);
    this.dateTypeData = Object.assign([], this.service.serviceDocument.domainData["datetype"]);
    this.orgLevelData = Object.assign([], this.service.serviceDocument.domainData["orgLevel"]);
    this.merchLevelData = Object.assign([], this.service.serviceDocument.domainData["merchLevel"]);
    this.locationData = Object.assign([], this.service.serviceDocument.domainData["location"]);
    this.costZoneData = Object.assign([], this.service.serviceDocument.domainData["costZone"]);
    this.departmentsData = Object.assign([], this.service.serviceDocument.domainData["departments"]);
    this.categoriesData = Object.assign([], this.service.serviceDocument.domainData["categories"]);
    this.locationData.forEach(x => x.id = +x.id);
    this.costZoneData.forEach(x => x.id = +x.id);
    this.departmentsData.forEach(x => x.id = +x.id);
    this.categoriesData.forEach(x => x.id = +x.id);

    delete this.sharedService.domainData;
    this.sharedService.domainData = {
      "supplier": this.supplierData,
      "dealTypeData": this.dealTypeData,
      "dealstatus": this.dealStatusData,
      "dateType": this.dateTypeData,
      "orgLevelData": this.orgLevelData,
      "merchLevelData": this.merchLevelData
    };

    this.model = {
      supplier: null,
      supplierName: null,
      itemBarcode: null,
      itemDescription: null,
      deaL_ID: null,
      dealDescription: null,
      dealTypeId: null,
      dealType: null,
      dealStatusId: null,
      dealStatus: null,
      dateTypeId: null,
      dateType: null,
      fromDate: null,
      toDate: null,
      operation: null,
      tableName: null,
      dealDetailId: null,
      orgLevel: null,
      orgLevelValue: null,
      merchLevel: null,
      merchLevelValue: null
    };

    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
    this.searchPanelDeal.dateColumns = ["fromDate", "toDate"];
  }

  constructor(private route: ActivatedRoute, private sharedService: SharedService
    , private router: Router, public service: DealService
    , private cdr: ChangeDetectorRef
    , private loaderService: LoaderService
    , private dealSharedService: DealSharedService) {
  }

  ngAfterViewChecked(): void {
    this.cdr.detectChanges();
  }

  getRows(params: IServerSideGetRowsParams): void {
    this.additionalSearchParams = { startRow: params.request.startRow, endRow: params.request.endRow, sortModel: params.request.sortModel };
    if (!this.loadedPage) {
      this.loadedPage = true;
      params.successCallback(
        this.serviceDocument.dataProfile.dataList.slice(params.request.startRow, params.request.endRow),
        this.service.serviceDocument.dataProfile.dataModel.totalRows);
    } else {
      this.service.search(this.additionalSearchParams).subscribe((sDoc) => {
        params.successCallback(sDoc.dataProfile.dataList, this.service.serviceDocument.dataProfile.dataModel.totalRows);
      });
    }
  }

  showSearchCriteriaChild(event: any): void {
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.showSearchCriteria = event === "search" ? true : event;
      if (event === "search") {
        for (let i: number = 0; i < this.searchProps.length; i++) {
          let controlName: string = this.searchProps[i];
          if (this.serviceDocument.dataProfile.profileForm.controls[controlName].value) {
            this.isSearchParamGiven = true;
            break;
          }
        }
        if (this.isSearchParamGiven) {
          this.searchPanelDeal.restrictSearch = false;
          this.searchPanelDeal.search();
          this.isSearchParamGiven = false;
        } else {
          this.sharedService.errorForm("At least one field is required to search");
        }
      } else if (event) {
        this.searchPanelDeal.restrictSearch = true;
      }
    }
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
      this.sharedService.previousUrl = "../Deal/List";
    }
    this.dealSharedService.dealId = cell.data.deaL_ID;
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl + cell.data.deaL_ID, this.service.serviceDocument.dataProfile.dataList);
  }

  addValidation(): void {
    this.router.navigate(["/" + this.redirectUrl + "0"], { skipLocationChange: true });
    this.sharedService.previousUrl = "../Deal/List";
  }

  updateDependentControls(event: any): void {
    this.serviceDocument.dataProfile.profileForm.controls["orgLevelValue"].setValue(null);
    if (this.serviceDocument.dataProfile.profileForm.controls["orgLevel"].value === "LOC") {
      this.orgLevelValueData = this.locationData;
    } else {
      this.orgLevelValueData = this.costZoneData;
    }
    this.sharedService.domainData["orgLevelValueData"] = this.orgLevelValueData;
  }

  getMerchLevelValueData(event: any): void {
    this.serviceDocument.dataProfile.profileForm.controls["merchLevelValue"].setValue(null);
    var sd: string = this.serviceDocument.dataProfile.profileForm.controls["merchLevel"].value;
    if (sd === "DEPT") {
      this.merchLevelValueData = this.departmentsData;
    } else if (sd === "CATEGORY") {
      this.merchLevelValueData = this.categoriesData;
    } else {
      this.merchLevelValueData = null;
    }
    this.sharedService.domainData["merchLevelValueData"] = this.merchLevelValueData;
  }

  reset(event: Event): void {
    delete this.orgLevelValueData;
    delete this.merchLevelValueData;
  }
}
