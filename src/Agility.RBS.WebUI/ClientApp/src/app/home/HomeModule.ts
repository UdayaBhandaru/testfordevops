﻿import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { HomeRoutingModule } from "./HomeRoutingModule";
import { HomeComponent } from "./HomeComponent";
import { FusionChartsModule } from "angular2-fusioncharts";
// import FusionCharts library and chart modules
import * as FusionCharts from "fusioncharts";
import * as Charts from "fusioncharts/fusioncharts.charts";
import * as Widgets from "fusioncharts/fusioncharts.widgets";
import * as FintTheme from "fusioncharts/themes/fusioncharts.theme.fint";
import { HomeService } from "./HomeService";
import { NgGridModule } from "angular2-grid";
import { DashboardResolver } from "../Dashboard/DashboardResolver";
import { RBSDashboardComponent } from "../Dashboard/RBSDashboardComponent";
import { ChartOptionsService } from "../Dashboard/ChartOptionsService";
import { RBSWidgetComponent } from "../Dashboard/RBSWidgetComponent";
import { RbsSharedModule } from "../RbsSharedModule";

// pass the fusioncharts library and chart modules
FusionChartsModule.fcRoot(FusionCharts, Charts, FintTheme, Widgets);

@NgModule({
    imports: [
        FrameworkCoreModule,
        HomeRoutingModule,
        FusionChartsModule,
        NgGridModule,
        RbsSharedModule
    ],
    declarations: [
        HomeComponent,
        RBSDashboardComponent,
        RBSWidgetComponent
    ],
    providers: [
        HomeService,
        DashboardResolver,
        ChartOptionsService
    ]
})
export class HomeModule {
}
