﻿import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { DashboardModel } from "../Dashboard/DashboardModel";
import { PageWidgetModel } from "../Dashboard/PageWidgetModel";
import { HttpHelperService } from "../Common/HttpHelperService";

@Injectable()
export class HomeService {
    serviceDocument: ServiceDocument<DashboardModel> = new ServiceDocument<DashboardModel>();
    constructor(private httpHelperService: HttpHelperService) { }

    list(): Observable<ServiceDocument<DashboardModel>> {
        return this.serviceDocument.list("/api/Dashboard/GetDashBoardData");
    }

    saveWidgetCustomization(pageWidgetListObj: PageWidgetModel[]): Observable<number> {
        let apiUrl: string = "/api/Dashboard/PageWidgetSave";
        return this.httpHelperService.post(apiUrl, pageWidgetListObj);
    }
}