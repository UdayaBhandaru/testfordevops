import { Component, OnInit } from "@angular/core";
import { HomeService } from "./HomeService";
import { DashboardModel } from "../Dashboard/DashboardModel";
import { WidgetOptionsModel } from "../Dashboard/WidgetOptionsModel";
import { NgGrid, NgGridItem, NgGridConfig, NgGridItemConfig, NgGridItemEvent } from "angular2-grid";
import { Route, ActivatedRoute } from "@angular/router";
import { SharedService } from "../Common/SharedService";

@Component({
    selector: "home",
  templateUrl: "./HomeComponent.html",
  styleUrls: ['./HomeComponent.scss']
})
export class HomeComponent implements OnInit {
    dataSource: Object = [];
    deptSales: Object = [];
    storeSales: Object = [];
    type_of_sales: Object = [];
    speedoMeter: Object = [];
    sales_last_year: Object = [];
    sales_last_sublevel_year: Object = [];
    sales_first_year: object = [];
    sales_first_sublevel_year: object = [];
    sales_second_year: object = [];
    sales_second_sublevel_year: object = [];
    sales_fourth_year: object = [];
    sales_fourth_sublevel_year: object = [];
    sales_five_year: Object = [];
    sales_five_sublevel_year: Object = [];
    sales_six_year: object = [];
    sales_six_sublevel_year: object = [];
    sales_seven_year: object = [];
    sales_seven_sublevel_year: object = [];
    sales_nine_year: object = [];
    sales_nine_sublevel_year: object = [];
    dashBoardDataList: DashboardModel[] = [];
    data: DashboardModel[] = [];
    previousDate: Date;

    constructor(private _homeService: HomeService, private route: ActivatedRoute, private sharedService: SharedService) {
    }

    ngOnInit(): void {
        if (this.route.snapshot.data["data"].dataProfile) {
            this.data = this.route.snapshot.data["data"].dataProfile.dataList;
        }
        let requiredDate: Date = new Date();
        requiredDate.setDate(requiredDate.getDate() - 1);
        this.previousDate = requiredDate;

        this.sales_last_year = {
            "chart": {
                "yAxisName": "Sales",
                "numberPrefix": "",
                "paletteColors": "#00a65a",
                "bgColor": "#ffffff",
                "showBorder": "0",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "placeValuesInside": "1",
                "valueFontColor": "#ffffff",
                "showAxisLines": "1",
                "axisLineAlpha": "25",
                "divLineAlpha": "10",
                "alignCaptionWithCanvas": "0",
                "showAlternateVGridColor": "0",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5"
            },
            "data": [
                {
                    "label": "Sales Variance %",
                    "value": "1.3%"

                }

            ]
        };
        this.sales_last_sublevel_year = {
            "chart": {
                "yAxisName": "Sales",
                "numberPrefix": "",
                "paletteColors": "#00a65a",
                "bgColor": "#ffffff",
                "showBorder": "0",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "placeValuesInside": "1",
                "valueFontColor": "#ffffff",
                "showAxisLines": "1",
                "axisLineAlpha": "25",
                "divLineAlpha": "10",
                "alignCaptionWithCanvas": "0",
                "showAlternateVGridColor": "0",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5"
            },
            "data": [
                {
                    "label": "Sales Variance Value",
                    "value": "3735"

                }


            ]
        };
        this.sales_first_year = {
            "chart": {
                "yAxisName": "Margin",
                "numberPrefix": "",
                "paletteColors": "#f39c12",
                "bgColor": "#ffffff",
                "showBorder": "0",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "placeValuesInside": "1",
                "valueFontColor": "#ffffff",
                "showAxisLines": "1",
                "axisLineAlpha": "25",
                "divLineAlpha": "10",
                "alignCaptionWithCanvas": "0",
                "showAlternateVGridColor": "0",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5"
            },
            "data": [
                {
                    "label": "Margin Variance %",
                    "value": "-6.99%"

                }

            ]
        };
        this.sales_first_sublevel_year = {
            "chart": {
                "yAxisName": "Margin",
                "numberPrefix": "",
                "paletteColors": "#f39c12",
                "bgColor": "#ffffff",
                "showBorder": "0",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "placeValuesInside": "1",
                "valueFontColor": "#ffffff",
                "showAxisLines": "1",
                "axisLineAlpha": "25",
                "divLineAlpha": "10",
                "alignCaptionWithCanvas": "0",
                "showAlternateVGridColor": "0",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5"
            },
            "data": [
                {
                    "label": "Margin Variance Value",
                    "value": "-3645.2"

                }


            ]
        };
        this.sales_second_year = {
            "chart": {
                "yAxisName": "Customer Count",
                "numberPrefix": "",
                "paletteColors": "#dd4b39",
                "bgColor": "#ffffff",
                "showBorder": "0",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "placeValuesInside": "1",
                "valueFontColor": "#ffffff",
                "showAxisLines": "1",
                "axisLineAlpha": "25",
                "divLineAlpha": "10",
                "alignCaptionWithCanvas": "0",
                "showAlternateVGridColor": "0",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5"
            },
            "data": [
                {
                    "label": "Customer Count Variance %",
                    "value": "-.46%"

                }

            ]
        };
        this.sales_second_sublevel_year = {
            "chart": {
                "yAxisName": "Customer Count",
                "numberPrefix": "",
                "paletteColors": "#dd4b39",
                "bgColor": "#ffffff",
                "showBorder": "0",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "placeValuesInside": "1",
                "valueFontColor": "#ffffff",
                "showAxisLines": "1",
                "axisLineAlpha": "25",
                "divLineAlpha": "10",
                "alignCaptionWithCanvas": "0",
                "showAlternateVGridColor": "0",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5"
            },
            "data": [
                {
                    "label": "Customer Count Variance Value",
                    "value": "-210"

                }


            ]
        };
        this.sales_fourth_year = {
            "chart": {
                "yAxisName": "Average Spend",
                "numberPrefix": "",
                "paletteColors": "#00c0ef",
                "bgColor": "#ffffff",
                "showBorder": "0",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "placeValuesInside": "1",
                "valueFontColor": "#ffffff",
                "showAxisLines": "1",
                "axisLineAlpha": "25",
                "divLineAlpha": "10",
                "alignCaptionWithCanvas": "0",
                "showAlternateVGridColor": "0",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5"
            },
            "data": [
                {
                    "label": "Average Spend Variance %",
                    "value": "1.77%"

                }

            ]
        };
        this.sales_fourth_sublevel_year = {
            "chart": {
                "yAxisName": "Average Spend",
                "numberPrefix": "",
                "paletteColors": "#00c0ef",
                "bgColor": "#ffffff",
                "showBorder": "0",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "placeValuesInside": "1",
                "valueFontColor": "#ffffff",
                "showAxisLines": "1",
                "axisLineAlpha": "25",
                "divLineAlpha": "10",
                "alignCaptionWithCanvas": "0",
                "showAlternateVGridColor": "0",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5"
            },
            "data": [
                {
                    "label": "Average Spend Variance Value",
                    "value": ".1"

                }


            ]
        };
        this.sales_five_year = {
            "chart": {
                "yAxisName": "Sales",
                "numberPrefix": "",
                "paletteColors": "#00a65a",
                "bgColor": "#ffffff",
                "showBorder": "0",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "placeValuesInside": "1",
                "valueFontColor": "#ffffff",
                "showAxisLines": "1",
                "axisLineAlpha": "25",
                "divLineAlpha": "10",
                "alignCaptionWithCanvas": "0",
                "showAlternateVGridColor": "0",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5"
            },
            "data": [
                {
                    "label": "Sales Variance %",
                    "value": ".90%"

                }
            ]
        };
        this.sales_five_sublevel_year = {
            "chart": {
                "yAxisName": "Sales",
                "numberPrefix": "",
                "paletteColors": "#00a65a",
                "bgColor": "#ffffff",
                "showBorder": "0",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "placeValuesInside": "1",
                "valueFontColor": "#ffffff",
                "showAxisLines": "1",
                "axisLineAlpha": "25",
                "divLineAlpha": "10",
                "alignCaptionWithCanvas": "0",
                "showAlternateVGridColor": "0",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5"
            },
            "data": [
                {
                    "label": "Sales Variance Value",
                    "value": "2598.45"

                }


            ]
        };
        this.sales_six_year = {
            "chart": {
                "yAxisName": "Margin",
                "numberPrefix": "",
                "paletteColors": "#f39c12",
                "bgColor": "#ffffff",
                "showBorder": "0",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "placeValuesInside": "1",
                "valueFontColor": "#ffffff",
                "showAxisLines": "1",
                "axisLineAlpha": "25",
                "divLineAlpha": "10",
                "alignCaptionWithCanvas": "0",
                "showAlternateVGridColor": "0",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5"
            },
            "data": [
                {
                    "label": "Margin Variance %",
                    "value": "-11.68%"

                }

            ]
        };
        this.sales_six_sublevel_year = {
            "chart": {
                "yAxisName": "Margin",
                "numberPrefix": "",
                "paletteColors": "#f39c12",
                "bgColor": "#ffffff",
                "showBorder": "0",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "placeValuesInside": "1",
                "valueFontColor": "#ffffff",
                "showAxisLines": "1",
                "axisLineAlpha": "25",
                "divLineAlpha": "10",
                "alignCaptionWithCanvas": "0",
                "showAlternateVGridColor": "0",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5"
            },
            "data": [
                {
                    "label": "Margin Variance Value",
                    "value": "-6417.89"

                }


            ]
        };
        this.sales_seven_year = {
            "chart": {
                "yAxisName": "Customer Count",
                "numberPrefix": "",
                "paletteColors": "#dd4b39",
                "bgColor": "#ffffff",
                "showBorder": "0",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "placeValuesInside": "1",
                "valueFontColor": "#ffffff",
                "showAxisLines": "1",
                "axisLineAlpha": "25",
                "divLineAlpha": "10",
                "alignCaptionWithCanvas": "0",
                "showAlternateVGridColor": "0",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5"
            },
            "data": [
                {
                    "label": "Customer Count Variance %",
                    "value": "-2.29%"

                }
            ]
        };
        this.sales_seven_sublevel_year = {
            "chart": {
                "yAxisName": "Customer Count",
                "numberPrefix": "",
                "paletteColors": "#dd4b39",
                "bgColor": "#ffffff",
                "showBorder": "0",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "placeValuesInside": "1",
                "valueFontColor": "#ffffff",
                "showAxisLines": "1",
                "axisLineAlpha": "25",
                "divLineAlpha": "10",
                "alignCaptionWithCanvas": "0",
                "showAlternateVGridColor": "0",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5"
            },
            "data": [
                {
                    "label": "Customer Count Variance Value",
                    "value": "-1063"

                }


            ]
        };
        this.sales_nine_year = {
            "chart": {
                "yAxisName": "Average Spend",
                "numberPrefix": "",
                "paletteColors": "#00c0ef",
                "bgColor": "#ffffff",
                "showBorder": "0",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "placeValuesInside": "1",
                "valueFontColor": "#ffffff",
                "showAxisLines": "1",
                "axisLineAlpha": "25",
                "divLineAlpha": "10",
                "alignCaptionWithCanvas": "0",
                "showAlternateVGridColor": "0",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5"
            },
            "data": [
                {
                    "label": "Average Spend Variance Value",
                    "value": "0.20"

                }
            ]
        };
        this.sales_nine_sublevel_year = {
            "chart": {
                "yAxisName": "Average Spend",
                "numberPrefix": "",
                "paletteColors": "#00c0ef",
                "bgColor": "#ffffff",
                "showBorder": "0",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "placeValuesInside": "1",
                "valueFontColor": "#ffffff",
                "showAxisLines": "1",
                "axisLineAlpha": "25",
                "divLineAlpha": "10",
                "alignCaptionWithCanvas": "0",
                "showAlternateVGridColor": "0",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5"
            },
            "data": [
                {
                    "label": "Average Spend Variance %",
                    "value": "3.27%"

                }


            ]
        };
        this.speedoMeter = {
            "chart": {
                "caption": "Sales vs Budget vs Last Year",

                "lowerLimit": "0",
                "upperLimit": "573,450",
                "theme": "fint"
            },
            "colorRange": {
                "color": [
                    {
                        "minValue": "0",
                        "maxValue": "50",
                        "code": "#08b4d8"
                    },
                    {
                        "minValue": "50",
                        "maxValue": "60",
                        "code": "#03a9ef"
                    },
                    {
                        "minValue": "60",
                        "maxValue": "100",
                        "code": "#0998d9"
                    }
                ]
            },
            "dials": {
                "dial": [
                    {
                        "value": "65"
                    }
                ]
            }
        };
    }

    saveWidgetCustomization(): void {
        let message: string;
        this._homeService.saveWidgetCustomization(this.data[0].pageWidgets).subscribe((response: any) => {
            if (response === 1) {
                message = "Widgets Customization Saved successfully";
                this.sharedService.saveForm(message).subscribe(() => {
                    // no returntype
                }, (error: string) => { console.log(error); });
            } else {
                message = "Please Try again";
                this.sharedService.errorForm(message);
            }
        }, (resp: any) => { console.log(resp); });
    }

    onNotify(eventData: DashboardModel[]): void {
        this.data = eventData;
    }
}
