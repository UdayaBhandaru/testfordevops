﻿import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { HomeComponent } from "./HomeComponent";
import { DashboardResolver } from "../Dashboard/DashboardResolver";

const AccountRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "Index", pathMatch: "full"
            },
            {
                path: "Index",
                component: HomeComponent,
                resolve: {
                    data: DashboardResolver
                }
            },
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(AccountRoutes)
    ]
})
export class HomeRoutingModule { }
