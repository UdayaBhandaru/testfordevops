import { Component, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { GridOptions, ColDef } from "ag-grid-community";
import { PromotionHeadModel } from '../promo/PromotionHeadModel';
import { GridExportModel } from '../Common/grid/GridExportModel';
import { SearchComponent } from '../Common/SearchComponent';
import { PromotionService } from './PromotionService';
import { SharedService } from '../Common/SharedService';
import { GridDateComponent } from '../Common/grid/GridDateComponent';
import { GridEditComponent } from '../Common/grid/GridEditComponent';
import { InfoComponent } from '../Common/InfoComponent';


@Component({
  selector: "promotion-list",
  templateUrl: "./PromotionListComponent.html"
})

export class PromotionListComponent implements OnInit {
  pageTitle = "Promotion";
  redirectUrl = "Promotion/New/";
  serviceDocument: ServiceDocument<PromotionHeadModel>;
  private gridOptions: GridOptions;
  columns: ColDef[];
  model: PromotionHeadModel;
  isSearchParamGiven: boolean = false;
  @ViewChild("searchPanelPromotion") searchPanelPromotion: SearchComponent;
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  defaultDateType: string;
  componentName: this;
  searchProps = ["promO_EVENT_ID", "theme", "startDate", "endDate", "description"];
  constructor(public service: PromotionService, public sharedService: SharedService, public router: Router) {
  }

  ngOnInit(): void {
   
    this.componentName = this;
    this.columns =
      [
      { headerName: "ID", field: "promO_EVENT_ID", tooltipField: "promO_EVENT_ID", width: 50 ,headerTooltip:"ID"},
      { headerName: "Theme", field: "theme", tooltipField: "theme", headerTooltip: "Theme" },
      { headerName: "Start Date", field: "startDate", tooltipField: "startDate", cellRendererFramework: GridDateComponent, width: 60, headerTooltip:"Start Date" },
        { headerName: "End Date", field: "endDate", tooltipField: "endDate", cellRendererFramework: GridDateComponent, width: 60 ,headerTooltip:"End Date"},
        { headerName: "Description", field: "description", tooltipField: "description", width: 40 ,headerTooltip:"Description"},
        { headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 45 ,headerTooltip:"Action"}
      ];

    this.model = {
      description: null, endDate: null, lockVersion: null, operation: "I", PromoEventDisplayId: null,
      promO_EVENT_ID: null, theme: null, programPhase: null, programMessage: null, pError: null, startDate: null
    };
    this.service.newModel(this.model);
    this.serviceDocument = this.service.serviceDocument;
    this.searchPanelPromotion.dateColumns = ["startDate", "endDate"];
  }



  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
      this.sharedService.previousUrl = "../Promotion/List";
    }
    
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl + cell.data.promO_EVENT_ID
      , this.service.serviceDocument.dataProfile.dataList);
  }

  addValidation(evnt:any): void {
    this.navigateToAdd();
  }

  private navigateToAdd(): void {
    this.router.navigate(["/" + this.redirectUrl + "0"], { skipLocationChange: true });
    this.sharedService.previousUrl = "../Promotion/List";
  }
}
