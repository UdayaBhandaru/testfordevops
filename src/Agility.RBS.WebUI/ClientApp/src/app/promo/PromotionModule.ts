import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { AgGridSharedModule } from "../AgGridSharedModule";
import { TitleSharedModule } from "../TitleSharedModule";
import { DatePickerModule } from "../Common/DatePickerModule";
import { PromotionRoutingModule } from "./PromotionRoutingModule";
import { RbsSharedModule } from '../RbsSharedModule';
import { PromotionListComponent } from './PromotionListComponent';
import { PromotionHeadComponent } from './PromotionHeadComponent';
import { PromotionService } from './PromotionService';
import { PromotionListResolver, PromotionResolver } from './PromotionResolver';
import { InboxService } from '../inbox/InboxService';
import { TreeViewComponent, ChecklistDatabase } from '../Common/TreeViewComponent';
import { MatTreeModule } from '@angular/material/tree';

@NgModule({
  imports: [
    FrameworkCoreModule,
    PromotionRoutingModule,
    AgGridSharedModule,
    TitleSharedModule,
    DatePickerModule,
    RbsSharedModule,
    MatTreeModule
  ],
  declarations: [
    PromotionListComponent,
    PromotionHeadComponent,
    TreeViewComponent
  ],
  providers: [
    PromotionService,
    PromotionListResolver,
    PromotionResolver,
    InboxService,
    ChecklistDatabase
  ]
})
export class PromotionModule {
  constructor() {
    LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
  }
}
