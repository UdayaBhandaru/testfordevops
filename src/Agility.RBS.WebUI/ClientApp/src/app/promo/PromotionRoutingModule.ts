import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import RbHeadGuard from "../guards/RbHeadGuard";
import RbDetailsGuard from "../guards/RbDetailsGuard";
import { PromotionListResolver, PromotionResolver } from './PromotionResolver';
import { PromotionListComponent } from './PromotionListComponent';
import { PromotionHeadComponent } from './PromotionHeadComponent';

const PromotionRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "", redirectTo: "List", pathMatch: "full"
      },
      {
        path: "List",
        component: PromotionListComponent,
        resolve:
        {
          serviceDocument: PromotionListResolver
        }
      },
      {
        path: "New/:id",
        component: PromotionHeadComponent,
        resolve: {
          serviceDocument: PromotionResolver
        },
      }
    ]
  }
];

@NgModule({
    imports: [
        RouterModule.forChild(PromotionRoutes)
    ]
})
export class PromotionRoutingModule { }

