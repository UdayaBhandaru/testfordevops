import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { PromotionService } from "./PromotionService";
import { PromotionHeadModel } from './PromotionHeadModel';



@Injectable()
export class PromotionListResolver implements Resolve<ServiceDocument<PromotionHeadModel>> {
    constructor(private service: PromotionService) { }
    resolve(): Observable<ServiceDocument<PromotionHeadModel>> {
        return this.service.list();
    }
}

@Injectable()
export class PromotionResolver implements Resolve<ServiceDocument<PromotionHeadModel>> {
  constructor(private service: PromotionService) { }
  resolve(route: ActivatedRouteSnapshot): Observable<ServiceDocument<PromotionHeadModel>> {
    return this.service.addEdit(route.params["id"]);
  }
}
