import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument, WorkflowAction } from "@agility/frameworkcore";
import { SharedService } from "../Common/SharedService";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../Common/HttpHelperService";
import { PromotionHeadModel } from './PromotionHeadModel';
import { debug } from 'util';

@Injectable()
export class PromotionService {
  serviceDocument: ServiceDocument<PromotionHeadModel> = new ServiceDocument<PromotionHeadModel>();

  constructor(private httpHelperService: HttpHelperService, private sharedService: SharedService) { }

  newModel(model: PromotionHeadModel): ServiceDocument<PromotionHeadModel> {
    return this.serviceDocument.newModel(model);
  }
  save(): Observable<ServiceDocument<PromotionHeadModel>> {
    if (!this.serviceDocument.dataProfile.dataModel.promO_EVENT_ID) {
      let currentAction: WorkflowAction = this.serviceDocument.dataProfile.actionService.allowedActions
        .find((v) => v["transitionType"] === "NT");
      this.serviceDocument.dataProfile.profileForm.controls["currentActionId"].setValue(currentAction.transitionClaim);
      return this.serviceDocument.submit("/api/PromotionList/Submit", true);
    } else {
      return this.serviceDocument.save("/api/PromotionList/Save", true, () => this.setDate());
    }
  }

  submit(): Observable<ServiceDocument<PromotionHeadModel>> {
    return this.serviceDocument.submit("/api/PromotionList/Submit", true);
  }
  addEdit(id: number): Observable<ServiceDocument<PromotionHeadModel>> {
    if (+id === 0) {
      return this.serviceDocument.new("/api/PromotionList/New");
    } else {
      return this.serviceDocument.open("/api/PromotionList/Open", new HttpParams().set("id", id.toString()));
    }
  }
  search(): Observable<ServiceDocument<PromotionHeadModel>> {
    return this.serviceDocument.search("/api/PromotionList/Search", true, () => this.setDate());
  }

  list(): Observable<ServiceDocument<PromotionHeadModel>> {
    return this.serviceDocument.list("/api/PromotionList/List");
  }

  reject(): Observable<ServiceDocument<PromotionHeadModel>> {
    return this.serviceDocument.submit("/api/PromotionList/Reject", true);
  }

  isExistingPriceChangeDataModel(promotionDesc: string, itemBarcode: string): Observable<boolean> {
    return this.httpHelperService.get("/api/promotion/IsExistingPromotion", new HttpParams().set("promotionDesc", promotionDesc)
      .set("itemBarcode", itemBarcode));
  }

  setDate(): void {
    let sd: PromotionHeadModel = this.serviceDocument.dataProfile.dataModel;
    if (sd !== null) {
      if (sd.startDate != null) {
        sd.startDate = this.sharedService.setOffSet(sd.startDate);
      }
      if (sd.endDate != null) {
        sd.endDate = this.sharedService.setOffSet(sd.endDate);
      }
    }
  }
  GetMerchTreeview() : any{
    return this.httpHelperService.get('api/PromotionList/GetMerchTreeview');
  }
}
