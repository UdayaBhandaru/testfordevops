import { Component, OnInit, TemplateRef, ViewChild, OnDestroy } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ServiceDocument, CommonService, MessageResult, MessageType } from "@agility/frameworkcore";
import { PromotionHeadModel } from './PromotionHeadModel';
import { MatDialogRef } from '@angular/material';
import { SharedService } from '../Common/SharedService';
import { PromotionService } from './PromotionService';
import { LoaderService } from '../Common/LoaderService';
import { RbDialogService } from '../Common/controls/RbDialogService';
import { Validators, FormGroup, ValidatorFn, AbstractControl, FormControl } from '@angular/forms';
import { RbButton } from '../Common/controls/RbControlModels';
import { DocumentsConstants } from '../Common/DocumentsConstants';
import { ChecklistDatabase, TreeViewComponent, RoleNodeModel } from '../Common/TreeViewComponent';

@Component({
  selector: "PromotionHead-add",
  templateUrl: "./PromotionHeadComponent.html",
  styleUrls: ['PromotionHeadComponent.scss'],

})
export class PromotionHeadComponent implements OnInit, OnDestroy {
  _componentName = "Promotion Head";
  PageTitle = "Promotion ";
  public serviceDocument: ServiceDocument<PromotionHeadModel>;
  localizationData: any;
  primaryId: number;
  screenName: string = "PROMO";
  editModel: PromotionHeadModel = {
    description: null, endDate: null, lockVersion: null, operation: "I", PromoEventDisplayId: null,
    promO_EVENT_ID: null, theme: null, programPhase: null, programMessage: null, pError: null, startDate: null
  };
  editMode: boolean;
  model: PromotionHeadModel;
  dialogRef: MatDialogRef<any>;
  public messageResult: MessageResult = new MessageResult();
  canNavigate: boolean = false;

  submitServiceSubscription: any;
  submitSuccessSubscription: any;
  saveServiceSubscription: any;
  saveContinueSubscribtion: any;
  resetSubscribtion: any;
  rejectSuccessSubscription: any;
  routeServiceSubscription: any;
  saveSuccessSubscription: any;
  nextSubscription: any;
  rejectSubscription: any;
  treeData: any[] = [];
  columns: any[];
  componentName: this;

  @ViewChild(TreeViewComponent)
  treeViewRef: TreeViewComponent;

  public promoTypeData: { "promoTypeKey": string; "promoTypeValue": string; }[] = [];

  constructor
    (
    public route: ActivatedRoute,
    public router: Router,
    public sharedService: SharedService,
    private service: PromotionService,
    private loaderService: LoaderService,
    public dialogService: RbDialogService,
    public checklistDatabase: ChecklistDatabase
    ) {
    this.promoTypeData = [{ promoTypeKey: "p", promoTypeValue: "Promotion" },
    { promoTypeKey: "c", promoTypeValue: "Clearence" }];
  }

  ngOnInit(): void {
    this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
    this.componentName = this;
    this.columns =
      [
        { headerName: "Item", field: "item", tooltipField: "item" },
        { headerName: "Description", field: "description", tooltipField: "description" }

      ];


    this.bind();
  }

  bind(): void {
    this.routeServiceSubscription = this.route.params.subscribe((resp: any) => {
      this.primaryId = resp["id"];
    });
    this.treeData = Object.assign([], this.service.serviceDocument.domainData["merchTree"]);
    if (this.primaryId.toString() !== "0") {
      this.editMode = true;
      this.primaryId = this.service.serviceDocument.dataProfile.dataModel.promO_EVENT_ID;
      this.PageTitle = `${this.PageTitle} - EDIT - ${this.primaryId}`;
    } else {
      this.model = {
        description: null, endDate: null, lockVersion: null, operation: "I", PromoEventDisplayId: null,
        promO_EVENT_ID: null, theme: null, programPhase: null, programMessage: null, pError: null, startDate: null
      };
      this.service.serviceDocument.dataProfile.dataModel = Object.assign(this.service.serviceDocument.dataProfile.dataModel
        , { operation: "I" }
        , { workflowInstance: this.service.serviceDocument.dataProfile.dataModel.workflowInstance });
      this.service.serviceDocument.dataProfile.profileForm.patchValue(this.service.serviceDocument.dataProfile.dataModel);
    }
    this.serviceDocument = this.service.serviceDocument;
    this.sharedService.checkWfPermitions(this.serviceDocument);
    setTimeout(() => {
      this.setTreeView();
    }, 1000);
    this.treeViewRef.treeControl.collapseAll();
  }

  setTreeView() {
    let list1 = this.serviceDocument.dataProfile.dataModel["formatLocList"];
    let list2 = this.treeViewRef.nodesData;
    let nodes: any[];
    list1.forEach(function (e) {
      if (!nodes) {
        nodes = [];
      }
      nodes = nodes.concat(list2.filter(f => f.locationId === e.location));
    });
    this.treeViewRef.checklistSelection.select(...nodes);
  }

  save(saveOnly: boolean): void {
    this.serviceDocument.dataProfile.profileForm.controls["orgHierarchy"] = new FormControl();
    this.serviceDocument.dataProfile.profileForm.controls["orgHierarchy"].setValue(this.treeViewRef.checklistSelection.selected);

    if (this.serviceDocument.dataProfile.profileForm.valid) {
      if (this.treeViewRef.checklistSelection.selected.length === 0) {
        this.sharedService.errorForm("Please select formats/locations");
      }
      else {
        this.saveSubscription(saveOnly);
      }
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  saveAndContinue(): void {
    this.save(false);
  }

  submitAction(): void {
    this.loaderService.display(true);
    this.submitServiceSubscription = this.service.submit().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.primaryId = this.service.serviceDocument.dataProfile.dataModel.promO_EVENT_ID;
        this.submitSuccessSubscription = this.sharedService.saveForm(`Submited - Promotion Request ID  ${this.primaryId}`).subscribe(() => {
          this.router.navigate(["/Promotion/List"], { skipLocationChange: true });
        });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
      this.loaderService.display(false);
    });
  }

  reject(): void {
    this.loaderService.display(true);
    this.rejectSubscription = this.service.reject().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.primaryId = this.service.serviceDocument.dataProfile.dataModel.promO_EVENT_ID;
        this.rejectSuccessSubscription = this.sharedService.saveForm(`Rejected - Promotion Request ID ${this.primaryId}`).subscribe(() => {
          this.router.navigate(["/Promotion/List"], { skipLocationChange: true });
        });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
      this.loaderService.display(false);
    });
  }
  saveSubscription(saveOnly: boolean): void {
    this.loaderService.display(true);
    this.saveServiceSubscription = this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.primaryId = this.service.serviceDocument.dataProfile.dataModel.promO_EVENT_ID;
        this.submitSuccessSubscription = this.sharedService.saveForm(
          `${this.localizationData.promotionHead.promotionheadsave} - Promotion Request ID ${this.primaryId}`
        ).subscribe(() => {
          if (saveOnly) {
            this.router.navigate(["/Promotion/List"], { skipLocationChange: true });
          } else {
            this.openCostManagementExcel();
            this.router.navigate(["/Blank"], {
              skipLocationChange: true, queryParams: {
                id: "/Promotion/New/" + this.primaryId
              }
            });
          }
        });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
      this.loaderService.display(false);
    });
  }

  ngOnDestroy(): void {
    if (this.saveContinueSubscribtion) {
      this.saveContinueSubscribtion.unsubscribe();
    }
    if (this.routeServiceSubscription) {
      this.routeServiceSubscription.unsubscribe();
    }
    if (this.saveServiceSubscription) {
      this.saveServiceSubscription.unsubscribe();
    }
    if (this.saveSuccessSubscription) {
      this.saveSuccessSubscription.unsubscribe();
    }
    if (this.submitServiceSubscription) {
      this.submitServiceSubscription.unsubscribe();
    }
    if (this.submitSuccessSubscription) {
      this.submitSuccessSubscription.unsubscribe();
    }
    if (this.nextSubscription) {
      this.nextSubscription.unsubscribe();
    }
    if (this.rejectSuccessSubscription) {
      this.rejectSuccessSubscription.unsubscribe();
    }
  }

  reset(): void {
    this.treeViewRef.checklistSelection.deselect(...this.treeViewRef.checklistSelection.selected);
    if (this.editMode) {
      this.setTreeView();
    }
    this.treeViewRef.treeControl.collapseAll();
    this.serviceDocument.dataProfile.profileForm.controls["orgHierarchy"].reset();
    Object.keys(this.serviceDocument.dataProfile.profileForm.controls).forEach(control => {
      this.serviceDocument.dataProfile.profileForm.controls[control].setValue(this.serviceDocument.dataProfile.dataModel[control]);
    });
    this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
  }

  close(): void {
    this.messageResult.message = `<div>All your changes will be lost if you don't save them.</div>
                                    <div>Click on “Save” to Keep the changes.</div>
                                    <div>Click on “Don't save” to proceed further without saving changes.</div>
                                    <div>Click on “Cancel” to stay on the current page.</div>`;
    if ((this.serviceDocument.dataProfile.profileForm && this.serviceDocument.dataProfile.profileForm.dirty)
      || !this.serviceDocument.dataProfile.profileForm.valid) {
      if (!this.dialogRef) {
        this.dialogRef = this.dialogService.openMessageDialog("Warning", this.messageResult, [new RbButton("", "Cancel", "alertCancel")
          , new RbButton("", "Don't Save", "alertdontsave"), new RbButton("", "Save", "alertSave")], "37%", ""
          , "iconsdontsave retailsNavIcons", "dontsaveAlert", "dontsaveAlertTitle");
        this.dialogRef.componentInstance.click.subscribe(
          btnName => {
            if (btnName === "Cancel") {
              this.dialogRef.close();
            }
            if (btnName === "Don't Save") {
              this.serviceDocument.dataProfile.profileForm.markAsUntouched();
              this.canNavigate = true;
              this.dialogRef.close();
              this.router.navigate([this.sharedService.previousUrl], { skipLocationChange: true });
            }
            if (btnName === "Save") {
              this.dialogRef.close();
              this.save(true);
              this.router.navigate([this.sharedService.previousUrl], { skipLocationChange: true });
            }
          });
      }
    } else {
      this.canNavigate = true;
      this.router.navigate([this.sharedService.previousUrl], { skipLocationChange: true });
    }
  }



  nextTab(): void {
    if (this.primaryId) {
      this.router.navigate(["/Promotion/New/" + this.primaryId], { skipLocationChange: true });
    }
  }

  openCostManagementExcel(): void {
    this.sharedService.downloadExcel(this.service.serviceDocument.dataProfile.dataModel.promO_EVENT_ID, "Promotion");
  }
}

export function radioButtonValidator(errorMessage: string): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } => {
    if (control.value != null && control.value.length > 0) {
      return null;
    } else {
      return { errorMessage };
    }
  };
}
