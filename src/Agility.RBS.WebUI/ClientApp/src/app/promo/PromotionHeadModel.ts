export class PromotionHeadModel {
  promO_EVENT_ID?: number;
  PromoEventDisplayId: string;
  description: string;
  theme: string;
  startDate: Date;
  endDate: Date;
  lockVersion: number;
  operation: string;
  programPhase: string;
  programMessage: string;
  pError: string;
  workflowInstance?: any;
  orgHierarchy?: any[];
  promoDetails?: any[];
  promoType?: string;
  formatLocList?: any[];
}
