import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { SharedService } from "../Common/SharedService";
import { DsdOrderHeadModel } from './DsdOrderHeadModel';
import { HttpHelperService } from '../Common/HttpHelperService';
import { HttpParams } from '@angular/common/http';

@Injectable()
export class DsdOrderService {
  serviceDocument: ServiceDocument<DsdOrderHeadModel> = new ServiceDocument<DsdOrderHeadModel>();
  constructor(private sharedService: SharedService, private httpHelperService: HttpHelperService) { }

  newModel(model: DsdOrderHeadModel): ServiceDocument<DsdOrderHeadModel> {
    return this.serviceDocument.newModel(model);
  }

  list(): Observable<ServiceDocument<DsdOrderHeadModel>> {
    return this.serviceDocument.list("/api/DsdOrderHead/List");
  }

  search(): Observable<ServiceDocument<DsdOrderHeadModel>> {
    return this.serviceDocument.search("/api/DsdOrderHead/Search", true, () => this.setDsdOrderDates());
  }

  dsdOrdersApprove(orderNo: string): Observable<boolean> {
    return this.httpHelperService.get("/api/DsdOrderHead/DsdOrderApprove", new HttpParams().set("orderNo", orderNo));
  }

  setDsdOrderDates(): void {
    var sd: DsdOrderHeadModel = this.serviceDocument.dataProfile.dataModel;
    if (sd !== null) {
      if (sd.runDate != null) {
        sd.runDate = this.sharedService.setOffSet(sd.runDate);
      }
    }
  }
}
