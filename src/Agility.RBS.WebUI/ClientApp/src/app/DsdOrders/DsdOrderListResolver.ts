import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { DsdOrderService } from "../DsdOrders/DsdOrderService";
import { DsdOrderHeadModel } from './DsdOrderHeadModel';


@Injectable()
export class DsdOrderListResolver implements Resolve<ServiceDocument<DsdOrderHeadModel>> {
  constructor(private service: DsdOrderService) { }
  resolve(): Observable<ServiceDocument<DsdOrderHeadModel>> {
    return this.service.list();
  }
}
