import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { DsdOrderHeadService } from './DsdOrderHeadService';
import { OrderHeadModel } from '../order/OrderHeadModel';

@Injectable()
export class DsdOrderResolver implements Resolve<ServiceDocument<OrderHeadModel>> {
    constructor(private service: DsdOrderHeadService) { }
    resolve(route: ActivatedRouteSnapshot): Observable<ServiceDocument<OrderHeadModel>> {
        return this.service.addEdit(route.params["id"]);
    }
}
