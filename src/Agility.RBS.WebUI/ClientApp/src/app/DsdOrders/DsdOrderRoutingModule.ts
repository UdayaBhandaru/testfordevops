import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { DsdOrderListComponent } from "../DsdOrders/DsdOrderListComponent";
import { DsdOrderListResolver } from './DsdOrderListResolver';
import { DsdOrderResolver } from './DsdOrderResolver';
import { DsdOrderComponent } from './DsdOrderComponent';

const DsdOrderRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
          {
            path: "List",
            component: DsdOrderListComponent,
            resolve:
            {
              references: DsdOrderListResolver
            }
          },
          {
            path: "New/:id",
            component: DsdOrderComponent,
            resolve:
            {
              references: DsdOrderResolver
            }
          }
        ]
    }
];

@NgModule({
    imports: [
      RouterModule.forChild(DsdOrderRoutes)
    ]
})
export class DsdOrderRoutingModule { }

