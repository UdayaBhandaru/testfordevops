import { Component, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { SharedService } from "../Common/SharedService";
import { GridEditComponent } from "../Common/grid/GridEditComponent";
import { GridExportModel } from "../Common/grid/GridExportModel";
import { InfoComponent } from "../Common/InfoComponent";
import { DomainDetailModel } from "../domain/DomainDetailModel";
import { SearchComponent } from "../Common/SearchComponent";
import { GridOptions, RowNode, GridApi } from 'ag-grid-community';
import { RbAutoCompleteComponent } from '../Common/controls/RbAutoCompleteComponent';
import { DsdOrderHeadModel } from './DsdOrderHeadModel';
import { DsdOrderService } from './DsdOrderService';
import { LocationDomainModel } from '../Common/DomainData/LocationDomainModel';
import { GridDateComponent } from '../Common/grid/GridDateComponent';
import { LoaderService } from '../Common/LoaderService';

@Component({
  selector: "dsd-order-List",
  templateUrl: "./DsdOrderListComponent.html"
})
export class DsdOrderListComponent implements OnInit {
  public serviceDocument: ServiceDocument<DsdOrderHeadModel>;
  PageTitle = "DSD AR Order";
  redirectUrl = "DsdOrders/New/";
  componentName: any;
  columns: any[];
  @ViewChild(RbAutoCompleteComponent) autoViewRef: RbAutoCompleteComponent;
  model: DsdOrderHeadModel = {
    location: null, runDate: null, status: null
  };
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  @ViewChild("searchPanel") searchPanel: SearchComponent;
  gridOptions: GridOptions;
  gridApi: GridApi;
  locationData: LocationDomainModel[];
  statusData: DomainDetailModel[];

  constructor(public service: DsdOrderService, private sharedService: SharedService, private router: Router
    , private loaderService: LoaderService) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "Run Date", field: "runDate", tooltipField: "runDate", cellRendererFramework: GridDateComponent },
      { headerName: "Supplier", field: "supplier", tooltipField: "supplier" },
      { headerName: "Supp Name", field: "supName", tooltipField: "supName" },
      { headerName: "Location", field: "location", tooltipField: "location" },
      { headerName: "Order No", field: "rmsOrderNo", tooltipField: "rmsOrderNo" },
      { headerName: "Order Value", field: "orderQty", tooltipField: "orderQty" },
      { headerName: "Status", field: "status", tooltipField: "status" },
      {
        headerName: "ACTIONS", field: "Actions", cellRendererFramework: GridEditComponent
        , cellRendererParams: { restrictApproveIcon: true }
      }
    ];

    this.gridOptions = {
      onGridReady: (params: any) => {
        this.gridApi = params.api;
      },
      context: {
        componentParent: this
      }
    };

    this.locationData = Object.assign([], this.service.serviceDocument.domainData["location"]);
    this.statusData = Object.assign([], this.service.serviceDocument.domainData["status"]);

    this.sharedService.domainData = {
      location: this.locationData,
      status: this.statusData
    };
    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.autoViewRef.ngOnInit();
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl + cell.data.rmsOrderNo, this.service.serviceDocument.dataProfile.dataList);
  }

  approve(cell: any): void {
    this.loaderService.display(true);
    if (cell.data.status !== "A") {
      this.service.dsdOrdersApprove(cell.data.rmsOrderNo).subscribe
        ((response: any) => {
          if (response) {
            this.sharedService.saveForm(`Approved Successfully - Order No ${cell.data.rmsOrderNo}`).subscribe(() => {
              this.serviceDocument.dataProfile.dataList[cell.rowIndex]["status"] = "A";
              let rowNode: RowNode = this.gridApi.getRowNode(cell.rowIndex);
              rowNode.setData(rowNode.data);
              this.loaderService.display(false);
            }, (error: string) => { console.log(error); });
          }
        });
    }
    this.loaderService.display(false);
  }

  addValidation(): void {
    this.router.navigate(["/" + this.redirectUrl + "0"], { skipLocationChange: true });
    this.sharedService.previousUrl = "../DsdOrders/List";
  }
}

