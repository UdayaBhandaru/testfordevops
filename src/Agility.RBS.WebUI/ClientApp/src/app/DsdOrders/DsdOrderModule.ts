import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { AgGridSharedModule } from "../AgGridSharedModule";
import { TitleSharedModule } from "../TitleSharedModule";
import { DsdOrderListComponent } from "../DsdOrders/DsdOrderListComponent";
import { DsdOrderService } from "../DsdOrders/DsdOrderService";
import { DsdOrderRoutingModule } from "../DsdOrders/DsdOrderRoutingModule";
import { RbsSharedModule } from "../RbsSharedModule";
import { DsdOrderListResolver } from './DsdOrderListResolver';
import { DsdOrderComponent } from './DsdOrderComponent';
import { DsdOrderHeadService } from './DsdOrderHeadService';
import { DsdOrderResolver } from './DsdOrderResolver';


@NgModule({
  imports: [
    FrameworkCoreModule,
    AgGridSharedModule,
    TitleSharedModule,
    DsdOrderRoutingModule,
    RbsSharedModule
  ],
  declarations: [
    DsdOrderListComponent,
    DsdOrderComponent
  ],
  providers: [
    DsdOrderListResolver,
    DsdOrderService,
    DsdOrderHeadService,
    DsdOrderResolver
  ]
})
export class DsdOrderModule {
  constructor() {
    LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
  }
}
