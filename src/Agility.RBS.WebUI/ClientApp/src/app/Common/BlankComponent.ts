﻿import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
    selector: "blank",
    templateUrl: "./BlankComponent.html"
})
export class BlankComponent {
    constructor(private route: ActivatedRoute, private router: Router) {
        route.queryParams.subscribe((resp) => {
            this.router.navigate([resp["id"]], { skipLocationChange: true });
        });
    }
}