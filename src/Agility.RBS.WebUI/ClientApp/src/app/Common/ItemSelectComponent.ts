import { Component, Input, Output, EventEmitter, OnInit, ElementRef, Inject } from "@angular/core";
import { ServiceDocument } from "@agility/frameworkcore";
import { MatDialog, MatDialogRef } from "@angular/material/dialog";
import { ItemSelectModel } from "../Common/ItemSelectModel";
import { GridOptions, ColDef, SelectionChangedEvent, GridApi } from "ag-grid-community";
import { ItemSelectService } from "../Common/ItemSelectService";
import { MAT_DIALOG_DATA } from "@angular/material/dialog";
import { DomainDetailModel } from "../domain/DomainDetailModel";
import { LocationDomainModel } from "../Common/DomainData/LocationDomainModel";
import { CommonModel } from "../Common/CommonModel";
import { FormGroup } from "@angular/forms";
import { SharedService } from "./SharedService";
import { BrandDomainModel } from "../Common/DomainData/BrandDomainModel";
import { ItemListDomainModel } from "../Common/DomainData/ItemListDomainModel";

@Component({
  selector: "ItemSelect",
  templateUrl: "./ItemSelectComponent.html",
  styleUrls: ['./ItemSelectComponent.scss'],
  providers: [
    ItemSelectService
  ]
})
export class ItemSelectComponent implements OnInit {
  Array: Array<any>;

  dataList: any[] = [];
  componentName: this;
  columns: ColDef[];
  gridOptions: GridOptions;
  selectedItemList: ItemSelectComponent[] = [];
  storedItemList: any[] = [];
  selectRowCount: number = 0;
  locTypeData: DomainDetailModel[];
  changelevelData: DomainDetailModel[];
  locationData: LocationDomainModel[]; locationList: LocationDomainModel[];
  orgLevelValueData: CommonModel[]; priceZoneData: any[]; costZoneData: any[];
  itemListData: ItemListDomainModel[];
  brandData: BrandDomainModel[];
  categoryData: any;
  supplierData: any;
  departmentData: any;
  divisionData: any;
  itemTypeData: any;

  showGrid: boolean = false;
  parentData: any[];
  screenName: string = "";
  deptId: number = null;
  supplier: number = null;
  loctype: string = null;
  location: string = "";

  supplierDisable: boolean = false;
  locationDisable: boolean = false;
  deptDisable: boolean = false;
  zoneVisible: boolean = false;
  apiUrl: string;
  gridApi: GridApi;

  requiredFields = [
    "supplierId", "itemBarcode", "itemDesc", "itemList", "brandId", "divisionId", "deptId", "categoryId"
  ];

  public serviceDocument: ServiceDocument<ItemSelectModel>;
  constructor(private service: ItemSelectService, public dialog: MatDialog, public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any, private sharedService: SharedService) {
    this.parentData = data;
    if (this.parentData[0].Name === "ORD") {
      this.screenName = this.parentData[0].Name;
      this.supplier = this.parentData[1].Name;
      this.deptId = this.parentData[2].Name;
      this.location = this.parentData[3].Name;
      this.loctype = this.parentData[4].Name;
    } else {
      this.screenName = this.parentData[0].Name;
    }
    this.serviceDocument = this.service.newModel(<ItemSelectModel>{
      brandId: null, brandName: null, categoryId: null, deptDesc: null, deptId: this.deptId
      , divisionDesc: null, divisionId: null, itemBarcode: null, itemDesc: null, itemList: null, locType: this.loctype
      , listDesc: null, orgLvl: null, orgLvlDesc: null, orgLvlVal: this.location, supplierId: this.supplier, suppName: null
      , screenId: this.screenName, costCurrencyCode: null, priceCurrencyCode: null, margin: null
      , sellingUom: null, sellingUomDesc: null, discountType: null
    });
  }

  ngOnInit(): void {
    this.componentName = this;
    this.service.list().subscribe((sDoc) => {
      this.serviceDocument = this.service.serviceDocument;
      if (this.serviceDocument.dataProfile != null && this.serviceDocument.dataProfile.profileForm == null) {
        this.serviceDocument = this.service.newModel(<ItemSelectModel>{
          brandId: null, brandName: null, categoryId: null, deptDesc: null, deptId: this.deptId
          , divisionDesc: null, divisionId: null, itemBarcode: null, itemDesc: null, itemList: null, locType: this.loctype
          , listDesc: null, orgLvl: "LOC", orgLvlDesc: null, orgLvlVal: this.location, supplierId: this.supplier, suppName: null
          , screenId: this.screenName, costCurrencyCode: null, priceCurrencyCode: null, margin: null
          , sellingUom: null, sellingUomDesc: null, discountType: null
        });
      }

      this.locTypeData = Object.assign([], this.service.serviceDocument.domainData["locType"]);
      this.locationList = Object.assign([], this.service.serviceDocument.domainData["location"]);
      this.supplierData = Object.assign([], this.service.serviceDocument.domainData["supplier"]);
      this.divisionData = this.service.serviceDocument.domainData["division"];
      this.departmentData = this.service.serviceDocument.domainData["department"];
      this.categoryData = this.service.serviceDocument.domainData["category"];
      this.brandData = this.service.serviceDocument.domainData["brand"];
      this.itemListData = this.service.serviceDocument.domainData["itemlist"];

      if (this.screenName === "PZ") {
        this.priceZoneData = Object.assign([], this.service.serviceDocument.domainData["pricezone"]);
        this.priceZoneData.forEach(x => x.id = +x.id);
        this.changelevelData = Object.assign([], this.service.serviceDocument.domainData["priceOrgLevel"]);
        this.supplierDisable = true;
      } else if (this.screenName === "ORD") {
        this.locationData = this.locationList.filter(item => item.locType === this.serviceDocument.dataProfile.profileForm.controls["locType"].value);
        this.supplierDisable = true;
        if (this.location) {
          this.locationDisable = true;
        }
        if (this.deptId) {
          this.deptDisable = true;
        }
      } else if (this.screenName === "CZ") {
        this.costZoneData = Object.assign([], this.service.serviceDocument.domainData["costzone"]);
        this.costZoneData.forEach(x => x.id = +x.id);
        this.changelevelData = Object.assign([], this.service.serviceDocument.domainData["costOrgLevel"]);
      }
    });
  }

  moveSelectedClose(): void {
    if (this.gridOptions.api) {
      this.storedItemList = this.moveSelectedContinue();
      this.dialogRef.close(this.storedItemList);
    } else {
      this.sharedService.errorForm("Nothing selected");
    }
  }

  moveSelectedContinue(): any[] {
    if (this.gridOptions.api) {
      this.selectedItemList = this.gridOptions.api.getSelectedRows();
      this.storedItemList = this.storedItemList.concat(this.selectedItemList);
      this.gridOptions.api.deselectAll();
      this.showGrid = false;
      this.dataList = [];
    }
    return this.storedItemList;
  }

  close(): void {
    this.dialogRef.close(this.storedItemList);
  }

  applyFilter(): void {
    let form: FormGroup = this.serviceDocument.dataProfile.profileForm;
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      let valid: boolean = false;
      this.requiredFields.forEach(ctrl => {
        if (form.controls[ctrl].value) {
          valid = true;
        }
      });
      if (!valid) {
        this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
        this.sharedService.errorForm("At least one field is required to apply filter");
      } else {
        if (this.screenName === "CZ" || this.screenName === "PZ") {
          this.apiUrl = (this.screenName === "CZ") ? "/api/ItemSelect/GetCostChgItemList" : "/api/ItemSelect/GetPriceChgItemList";
          this.service.getItemsFromAPI(this.apiUrl, this.serviceDocument.dataProfile.profileForm.value).subscribe(response => {
            this.dataList = response;
            if (this.dataList === null) {
              this.dataList = [];
            }
            this.showGrid = true;
          });
        } else {
          this.service.search().subscribe(() => {
            this.dataList = this.service.serviceDocument.dataProfile.dataList;
            if (this.dataList === null) {
              this.dataList = [];
            }
            this.showGrid = true;
          });
        }
        this.columns = [
          {
            headerName: "Item", field: "itemBarcode", suppressMovable: true, suppressNavigable: true, pinned: true,
            checkboxSelection: true
          },
          {
            headerName: "Item Description", field: "itemDesc"
          },

          {
            headerName: "Supplier", field: "suppName", hide: (this.screenName === "ITM" || this.screenName === "PZ")
          },
          {
            headerName: "Org Level", field: "orgLvlDesc", hide: this.screenName === "ITM"
          },

          {
            headerName: "Org Level Value", field: "orgLvlValDesc", hide: this.screenName === "ITM"
          },

          {
            headerName: "Division", field: "divisionDesc"
          },
          {
            headerName: "Department", field: "deptDesc"
          },
          {
            headerName: "Category", field: "categoryDesc"
          }
        ];
        this.gridOptions = {
          onGridReady: (params: any) => {
            this.gridApi = params.api;
          },
          context: {
            componentParent: this
          }
        };
      }
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  clearFilter(): void {
    this.dataList = [];
    this.selectedItemList = [];
    this.serviceDocument = this.service.newModel(<ItemSelectModel>{
      brandId: null, brandName: null, categoryId: null, deptDesc: null, deptId: this.deptId
      , divisionDesc: null, divisionId: null, itemBarcode: null, itemDesc: null, itemList: null, locType: this.loctype
      , listDesc: null, orgLvl: "LOC", orgLvlDesc: null, orgLvlVal: "", supplierId: this.supplier, suppName: null
      , screenId: this.screenName, costCurrencyCode: null, priceCurrencyCode: null, margin: null
      , sellingUom: null, sellingUomDesc: null, discountType: null
    });
    this.showGrid = false;
    this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
  }

  updateDependentControls(): void {
    let pForm: FormGroup = this.serviceDocument.dataProfile.profileForm;
    if (pForm.controls["orgLvl"].value === "PZ") {
      this.orgLevelValueData = Object.assign([], this.priceZoneData);
      this.zoneVisible = true;
    } else if (pForm.controls["orgLvl"].value === "CZ") {
      this.orgLevelValueData = Object.assign([], this.costZoneData);
      this.zoneVisible = true;
    } else {
      this.zoneVisible = false;
    }
  }

  public locTypeChanged(): void {
    this.locationData = this.locationList.filter(item => item.locType === this.serviceDocument.dataProfile.profileForm.controls["locType"].value);
  }
}
