import { Component } from "@angular/core";
import { NavigationStart, NavigationEnd, NavigationCancel, NavigationError } from "@angular/router";
import { SharedService } from "./SharedService";
import { LoaderService } from "./LoaderService";
// import { FavouriteService } from "../favourites/FavouriteService";
// import { FavouriteModel } from "../favourites/FavouriteModel";

@Component({
  selector: "rbs-sidenav",
  templateUrl: "./SideNavComponent.html",
  styleUrls: ['./SideNavComponent.scss']
})

export class SidenavComponent {
  public displayMenu: boolean;
  public selected: string;
  public selected1: string;
  // public fm: FavouriteModel;
  // public favouritesData: FavouriteModel[];
  showLoader: boolean;
  idleState = "Not started.";
  timedOut = false;
  lastPing?: Date = null;

  setStep(presentUrl: string, pastUrl: string): void {
    if (presentUrl !== pastUrl) {
      this.selected = presentUrl;
    } else {
      this.selected = "";
    }
  }

  setStep1(presentUrl: string, pastUrl: string): void {
    if (presentUrl !== pastUrl) {
      this.selected1 = presentUrl;
    } else {
      this.selected1 = "";
    }
  }

  constructor(public sharedService: SharedService) {
  }

  navigationInterceptor(event: any): void {
    if (event instanceof NavigationStart) {
      this.showLoader = true;
    }
    if (event instanceof NavigationEnd) {
      this.showLoader = false;
    }
    if (event instanceof NavigationCancel) {
      this.showLoader = false;
    }
    if (event instanceof NavigationError) {
      this.showLoader = false;
    }
  }
}
