import { Component, Input, OnInit, Inject } from "@angular/core";
import { MAT_DIALOG_DATA } from "@angular/material";
import { Subject } from "rxjs";

import { RbButton, RbDialog } from "./RbControlModels";

@Component(
  {
    selector: "rb-message-dialog",
    template: `<span class="fx-dialog-icon {{ _iconCss }}"></span>
                <div mat-dialog-title class="fx-dialog-title mat-dialog-title {{ _titleCss }}">{{ _title }}</div>
                <div mat-dialog-content class="fx-dialog-content"><div [innerHTML]="_content"></div></div>
                <div mat-dialog-actions class="fx-dialog-actions">
                    <button *ngIf="_showOk" mat-raised-button mat-dialog-close class="fx-button">Ok</button>
                    <button *ngFor="let btn of _buttonList" mat-button (click)="_onClick(btn.displayName)" class="fx-button">{{ btn.displayName }}</button>
                </div>`
  }
)
export class RbMessageDialogComponent implements OnInit {
  public _title: string;
  public _content: string;
  public _showOk: boolean = true;
  public _iconCss: string;
  public _titleCss: string;
  public _contentCss: string;
  public _buttonList: RbButton[];

  private clickAnnouncedSource = new Subject<string>();
  click = this.clickAnnouncedSource.asObservable();

  constructor(@Inject(MAT_DIALOG_DATA) private data: RbDialog) {
    this._title = data.title;
    this._content = data.content;
    this._titleCss = data.titleCss !== "" ? data.titleCss : "";
    this._contentCss = data.contentCss;
    this._buttonList = data.buttonList;
    this._iconCss = data.iconCss;
  }

  ngOnInit(): void {
    if (this._buttonList !== undefined && this._buttonList.length > 0) {
      this._showOk = false;
    }
  }

  private _onClick(btnName: string): void {
    this.clickAnnouncedSource.next(btnName);
  }
}
