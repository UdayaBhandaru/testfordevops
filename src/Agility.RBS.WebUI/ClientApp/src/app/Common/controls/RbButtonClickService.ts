﻿import { Injectable } from "@angular/core";
import { Subject } from "rxjs";

@Injectable()
export class RbButtonClickService {
    // observable string sources
    public clickAnnouncedSource = new Subject<string>();
    // observable string streams
    clickAnnounced$ = this.clickAnnouncedSource.asObservable();
    // service message commands
    announceclick(btnName: string): void {
        this.clickAnnouncedSource.next(btnName);
    }
}
