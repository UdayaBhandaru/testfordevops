import { Component, Input, Output, EventEmitter, OnInit, ChangeDetectorRef } from "@angular/core";
import { FormGroup, FormControl, FormBuilder, Validators } from "@angular/forms";
import { WorkflowAction, CommonService, FormMode, ServiceDocument, DataProfileDocument, ProfileModel } from "@agility/frameworkcore";
import { HttpHelperService } from "../../Common/HttpHelperService";
import { HttpParams } from "@angular/common/http";
import { SharedService } from "../../Common/SharedService";

@Component({
  selector: "rb-wf-header",
  templateUrl: "./RbFlowHeaderComponent.html"
})
export class RbFlowHeaderComponent implements OnInit {
  private filteredAllowedActions: any[] = [];
  decisionData: any[];
  recipientsData: { recipientID: string, recipientName: string, stateId: string }[] = [];
  @Input() dataProfile: DataProfileDocument<any>;
  @Input() formGroup: FormGroup;// curent form/profileform
  @Input() allowedActions: WorkflowAction[];
  @Input() cssClass: string;
  @Output() submitClick = new EventEmitter();
  @Output() rejectClick = new EventEmitter();
  @Output() sendBackClick = new EventEmitter();
  @Input() hideTransitionClaims: String[];
  @Input() negativeMarginCount: number;
  @Output() discardClick = new EventEmitter();
  @Input() moduleName: string;
  @Input() priority: string;
  private disablePartApproveBtn: boolean | number = false;
  priorityDropDownData = [{ key: "H", value: "High" }, { key: "M", value: "Medium" }, { key: "L", value: "Low" }];
  public workflowForm: FormGroup;
  public submissionType: { Reject: string, sendback: string, submit: string };
  disableRecipientsList: string[] = [];
  constructor(private ref: ChangeDetectorRef, private commonService: CommonService, private fb: FormBuilder,
    private httpHelperService: HttpHelperService, private sharedService: SharedService) {

    this.workflowForm = this.fb.group({
      priority: "M",
      comments: null,
      decision: [null],
      recipientID: [null]
    });
    this.submissionType = { Reject: "REJ", sendback: "SBR", submit: "CHGS" };

  }

  ngOnInit(): void {
    if (this.allowedActions && this.allowedActions.length > 0) {
      this.filteredAllowedActions = this.allowedActions.filter(a => a["transitionType"] === "NT");
      if (this.dataProfile.dataModel.workflowInstance.stateId !== "Worksheet" && this.dataProfile.dataModel.workflowInstance.stateId !== "worksheet") {
        this.decisionData = this.filteredAllowedActions;
        this.decisionData.push({
          actionId: "reject", actionName: "Reject", claimType: "", claimValue: "", description: "Reject",
          fromStateId: "", image: null, toStateId: null, transitionClaim: "reject"
        }
          // , {
          //    actionId: "sendback", actionName: "Send back", claimType: "", claimValue: "", description: "Send back",
          //    fromStateId: "", image: null, toStateId: null, transitionClaim: "sendback"
          // }
        );

      } else {
        if (this.negativeMarginCount > 0) {
          this.decisionData = this.filteredAllowedActions.filter(a => a["transitionClaim"] === "ToMarginApproverClaim");
        } else {
          this.decisionData = this.filteredAllowedActions.filter(a => a["transitionClaim"] !== "ToMarginApproverClaim");
        }
      }
    }
    this.workflowForm.controls["priority"].setValue(this.priority);
    this.sharedService.stateID = this.dataProfile.dataModel.workflowInstance.workflowStateId;
  }

  submitAction(): void {
    let submitionType: string;
    switch (this.workflowForm.controls["decision"].value) {
      case "reject":
        submitionType = this.submissionType.Reject;
        this.setWorkflowForm(this.rejectClick, submitionType);
        break;
      case "sendback":
        submitionType = this.submissionType.sendback;
        this.setWorkflowForm(this.sendBackClick, submitionType);
        break;
      default:
        submitionType = this.submissionType.submit;
        this.setWorkflowForm(this.submitClick, submitionType);
    }
  }
  private setWorkflowForm(emitEvent: EventEmitter<{}>, submitionType: string): void {
    this.workflowForm.controls["decision"].setValidators([Validators.required]);
    this.workflowForm.controls["recipientID"].setValidators([Validators.required]);
    this.workflowForm.controls["priority"].setValidators([Validators.required]);
    this.workflowForm.controls["decision"].updateValueAndValidity();
    this.workflowForm.controls["recipientID"].updateValueAndValidity();
    this.workflowForm.controls["priority"].updateValueAndValidity();
    this.sharedService.validateForm(this.workflowForm);
    if (this.workflowForm.valid) {
      if (!this.disableRecipient()) {
        let recipient: any = this.recipientsData.find(r => r.recipientID === this.workflowForm.controls["recipientID"].value);
        if (!this.workflowForm.controls["recipientName"]) {
          this.workflowForm.addControl("recipientName", new FormControl(""));
        }
        if (submitionType !== this.submissionType.submit) {
          this.workflowForm.addControl("stateId", new FormControl(""));
          this.workflowForm.patchValue({ "stateId": recipient.stateId });
        }
        this.workflowForm.patchValue({ "recipientName": recipient.recipientName });
      }
      this.formGroup.controls["currentActionId"].setValue(this.workflowForm.controls["decision"].value);
      this.formGroup.setControl("workflowForm", this.workflowForm);
      emitEvent.emit(event);
    } else {
      this.sharedService.errorForm("Please fill required fields");
    }
  }

  private discard(): void {
    this.discardClick.emit(event);
  }

  ngAfterViewInit(): void {
    this.ref.detectChanges();
  }

  ngAfterViewChecked(): void {
    this.ref.detach();
    this.ref.detectChanges();
  }

  decisionChanged(): void {
    let submitionType: string = "";
    let stateId: string = this.dataProfile.dataModel.workflowInstance.workflowStateId;
    switch (this.workflowForm.controls["decision"].value) {
      case "reject":
        submitionType = this.submissionType.Reject;
        break;
      case "sendback":
        submitionType = this.submissionType.sendback;
        break;
      default:
        {
          submitionType = this.submissionType.submit;
          if (this.disableRecipient()) {
            this.workflowForm.controls["recipientID"].disable();
          }
          stateId = this.decisionData.find(a => a.transitionClaim === this.workflowForm.controls["decision"].value).toStateId;
        }

    }
    let wInstance: any = this.dataProfile.dataModel.workflowInstance;
    this.httpHelperService.get("/api/inbox/RecipientsGet", new HttpParams()
      .set("dataProfileId", wInstance.dataProfileId)
      .append("profileInstanceId", wInstance.profileInstanceId)
      .append("workFlowId", "")
      .append("stateId", stateId)
      .append("actionType", submitionType)
      .append("moduleName", this.moduleName)
    ).subscribe((resp: any[]) => {
      if (Array.isArray(resp)) {
        this.recipientsData = resp;
      }
    });
  }

  private disableRecipient(): boolean {
    return this.disableRecipientsList.some(s => s === this.workflowForm.controls["decision"].value);
  }
}
