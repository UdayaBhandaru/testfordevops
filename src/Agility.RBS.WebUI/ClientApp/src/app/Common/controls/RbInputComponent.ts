﻿import { Component, Input, OnInit, Output, EventEmitter, Directive, ElementRef, HostListener, ChangeDetectorRef } from "@angular/core";
import { FormGroup, FormControl, AbstractControl, NgControl } from "@angular/forms";

@Component({
    selector: "rb-input",
    template: `<mat-form-field [formGroup]="formGroup">
  <input matInput type="{{type}}" placeholder="{{placeholder}}" formControlName="{{controlName}}" name="{{controlName}}"
[required]="required" [readonly]="readonly" maxlength="{{maxlength}}" (blur)="_blur($event)" (keydown)="_onChange($event)" id="{{controlName}}"
(input)="_onInput($event)">
</mat-form-field><ng-content></ng-content>`
})

export class RbInputComponent implements OnInit {
    @Input() formGroup: FormGroup;
    @Input() controlName: string;
    @Input() type: string;
    @Input() placeholder: string;
    @Input() name: string;
    @Input() required: boolean;
    @Input() disabled: boolean;
    @Input() readonly: boolean;
    @Input() maxlength: number;
    @Input() cssClass: string;
    @Input() upperCase: boolean = false;
    @Output() fxBlur = new EventEmitter();
    @Output() fxChange = new EventEmitter();
    private _control: AbstractControl;

    constructor(public ref: ChangeDetectorRef) {
    }

    ngOnInit(): void {
        if (this.type === undefined || this.type === "") {
            this.type = "text";
        }
        this._control = this.formGroup.controls[this.controlName];
        if (this._control) {
            if (this.disabled && this.disabled.toString().toLowerCase() === "true") {
                this._control.disable();
            }
        }
    }
    _blur(event: any): void {
        this.fxBlur.emit(event);
    }

    _onInput(event: any): void {
        let curPos: number = 0;
        if (this.upperCase && event.data !== undefined) {
            curPos = event.srcElement.selectionStart;
            this._control.setValue(this._control.value.toUpperCase());
            event.srcElement.selectionStart = curPos;
            event.srcElement.selectionEnd = curPos;
        }
    }

    _onChange(event: any): void {
        this.fxChange.emit(event);
    }

    ngAfterViewChecked(): void {
        this.ref.detectChanges();
    }
}
