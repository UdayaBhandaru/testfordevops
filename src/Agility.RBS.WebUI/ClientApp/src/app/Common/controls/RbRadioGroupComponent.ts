﻿import { Component, Input, Output, EventEmitter } from "@angular/core";
import { FormGroup, FormControl, AbstractControl } from "@angular/forms";
import { MatRadioChange } from "@angular/material";

@Component(
    {
        selector: "rb-radio-group",
        template: `
            <div [formGroup]="formGroup" class="fx-radiogroup {{cssClass}}">
                <mat-radio-group formControlName="{{controlName}}" ngDefaultControl (change)="_onChange($event)">
                <mat-radio-button *ngFor="let option of optionsData" style="margin:5px"
                    value="{{option[optionsKey]}}">{{option[optionsValue]}}</mat-radio-button>
                </mat-radio-group>
            </div>`
    }
)
export class RbRadioGroupComponent {
    @Input() formGroup: FormGroup;
    @Input() controlName: string;
    @Input() optionsData: any;
    @Input() optionsKey: any;
    @Input() optionsValue: any;
    @Input() cssClass: string;
    @Input() disabled: boolean;
    @Output() rbChange: EventEmitter<MatRadioChange> = new EventEmitter<MatRadioChange>();
    private _control: AbstractControl;

    ngOnInit(): void {
        let self: RbRadioGroupComponent = this;
        self._control = self.formGroup.controls[self.controlName];
        if (self._control) {
            if (self.disabled) {
                self._control.disable();
            }
        }
    }

    _onChange(event: MatRadioChange): void {
        if (this.rbChange != null && this.rbChange.observers.length > 0) {
            this.rbChange.emit(event);
        }
    }
}