﻿import { Component, Input, Output, EventEmitter } from "@angular/core";
import { FormGroup, AbstractControl } from "@angular/forms";

@Component({
    selector: "rb-date",
    template: `<mat-form-field [formGroup]="formGroup" class="fx-date">
        <input matInput [min]="minDate" [max]="maxDate" [matDatepicker]="picker" placeholder="{{placeholder}}" name="{{name}}"
formControlName="{{controlName}}" [disabled]="disabled" (dateChange)="_onChange($event)" [required]="required" [readonly]="readonly"/>
    <mat-datepicker-toggle [for]="picker" [disabled]="disabled"></mat-datepicker-toggle>
</mat-form-field>
<mat-datepicker #picker></mat-datepicker>`
})
export class RbDateComponent {
    @Input() formGroup: FormGroup;
    @Input() controlName: string;
    @Input() placeholder: string;
    @Input() required: boolean;
    @Input() disabled: boolean;
    @Input() readonly: boolean;
    @Input() cssClass: string;
    @Input() name: string;
    @Input() minDate: any;
    @Input() maxDate: any;
    @Output() fxChange = new EventEmitter();
    private _control: AbstractControl;
    private _self: RbDateComponent;

    ngOnInit(): void {
        let _self: any = this;
        _self._control = this.formGroup.controls[_self.controlName];
        if (_self._control) {
            if (_self.disabled && _self.disabled.toString().toLowerCase() === "true") {
                _self._control.disable();
            }
        }
    }

    _onChange(event: any): void {
        this.fxChange.emit(event);
    }
}