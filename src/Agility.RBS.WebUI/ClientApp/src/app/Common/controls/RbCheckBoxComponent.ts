import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import { FormGroup, FormControl, AbstractControl } from "@angular/forms";
import { MatCheckboxChange } from "@angular/material";

@Component(
    {
        selector: "rb-checkbox",
        template: `
            <div [formGroup]="formGroup">
                <mat-checkbox formControlName="{{controlName}}"
                [checked]="checked" value="value" [required]="required">{{text}}</mat-checkbox>
            </div>`
    }
)
export class RbCheckBoxComponent implements OnInit {
    @Input() formGroup: FormGroup;
    @Input() controlName: string;
    @Input() align: string;
    @Input() checked: boolean;
    @Input() disabled: boolean;
    @Input() required: boolean;
    @Input() readonly: boolean;
    @Input() value: string;
    @Input() text: string;
    @Input() cssClass: string;
    @Output() rbChange: EventEmitter<MatCheckboxChange> = new EventEmitter<MatCheckboxChange>();
    private _control: AbstractControl;

    ngOnInit(): void {
        if (this.align === undefined || this.align === "") {
            this.align = "start";
        }
        this._control = this.formGroup.controls[this.controlName];
        if (this._control) {
            if (this.disabled && this.disabled.toString().toLowerCase() === "true") {
                this._control.disable();
            }
        }
    }

    _onChange(event: MatCheckboxChange): void {
        if (this.rbChange != null) {
            this.rbChange.emit(event);
        }
    }
}
