import { Component, Input, OnInit, Output, EventEmitter, Directive, ElementRef, HostListener, ChangeDetectorRef } from "@angular/core";
import { FormGroup, FormControl, AbstractControl, NgControl, ValidatorFn } from "@angular/forms";
import { isNumeric } from "rxjs/internal-compatibility";

@Component({
  selector: "rb-amount",
  template: `<mat-form-field [formGroup]="formGroup"> 
     <input matInput type="text" placeholder="{{placeholder}}" formControlName="{{controlName}}" name="{{controlName}}"
      value="{{formGroup.get(controlName).value | number:'1.4'}}"
[required]="required" [readonly]="readonly" maxlength="{{maxlength}}" (blur)="blur($event)" id="{{controlName}}" autocomplete="off">
        </mat-form-field>
<ng-content></ng-content>`
})

export class RbAmountComponent implements OnInit {
  @Input() formGroup: FormGroup;
  @Input() controlName: string;
  @Input() type: string;
  @Input() placeholder: string;
  @Input() name: string;
  @Input() required: boolean;
  @Input() disabled: boolean;
  @Input() readonly: boolean;
  @Input() maxlength: number;
  @Input() cssClass: string;
  @Input() upperCase: boolean = false;
  @Output() fxBlur = new EventEmitter();
  @Output() fxChange = new EventEmitter();
  private _control: AbstractControl;
  validators: ValidatorFn[] = [];
  readonly requiredErrorMessage: string = "Required";
  readonly invalidInputErrorMessage: string = "Please enter a numeric value";
  readonly minErrorMessage: string = "Please enter a value not less than ";
  readonly maxErrorMessage: string = "Please enter a value not more than ";
  readonly numberDecimalErrorMessage: string = "Please enter decimals not more than ";
  @Input() allowDecimals: boolean;
  @Input() numberOfDecimals: number;
  private formControl: AbstractControl;

  constructor(public ref: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    this.formControl = <AbstractControl>this.formGroup.get(this.controlName);
    this.validators.push(this.validateInput(""));
    this.formControl.setValidators(this.validators);
    if (this.disabled && this.disabled.toString().toLowerCase() === "true") {
      this.formControl.disable();
    }
  }

  blur(event: any): void {
    let self: RbAmountComponent = this;
    self.fxBlur.emit(event);
  }

  validateInput(errorMessage: string): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      errorMessage = "";
      if (this.required) {
        if (!control.value) {
          errorMessage = this.requiredErrorMessage;
          return { errorMessage };
        }
      }

      if (control.value && !isNumeric(control.value)) {
        errorMessage = this.invalidInputErrorMessage;
        return { errorMessage };
      }

      if (this.allowDecimals && this.numberOfDecimals) {
        if (!this.validateInputDecimal(control.value, this.numberOfDecimals)) {
          errorMessage = `${this.numberDecimalErrorMessage} ${this.numberOfDecimals}`;
          return { errorMessage };
        }
      }
    };
  }

  validateKeyCodes(event: any): any {
    if (this.allowDecimals) {
      return this.onlyDecimalNumberKey(event);
    } else {
      return this.onlyNumberKey(event);
    }
  }

  onlyNumberKey(event: any): any {
    return (event.charCode === 8 || event.charCode === 0) ? null :
      (event.charCode === 45) || (event.charCode >= 48 && event.charCode <= 57);
  }

  onlyDecimalNumberKey(event: any): any {
    let charCode: any = (event.which) ? event.which : event.keyCode;
    if (this.formControl.value) {
      let precission: any = this.formControl.value.toString().indexOf(".") > -1 ? this.formControl.value.toString().split(".")[1] : null;
      if (precission && precission.length === +this.numberOfDecimals) {
        return false;
      }
      return this.onlyNumber(charCode);
    }
    return this.onlyNumber(charCode);
  }

  onlyNumber(charCode: any): boolean {
    if (charCode === 45) {
      return true;
    }
    if (charCode !== 46 && charCode > 31
      && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  validateInputDecimal(inputValue: string, numberOfDecimals: number): boolean {
    let splitInputValue: string[];
    if (inputValue) {
      splitInputValue = inputValue.toString().split(".");
      if (splitInputValue.length > 1) {
        let decimalPart: string = splitInputValue[1];
        if (decimalPart && decimalPart.length > numberOfDecimals) {
          return false;
        }
      }
    }
    return true;
  }

  ngAfterViewChecked(): void {
    if (this.formControl && this.disabled) {
      this.formControl.disable();
    }
  }
}
