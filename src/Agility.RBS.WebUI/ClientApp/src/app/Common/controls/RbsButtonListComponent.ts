import { Component, Input, Output, EventEmitter, OnInit } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { WorkflowAction } from "@agility/frameworkcore";


@Component({
  selector: "rbs-button-list",
  template: `<div>
    <button  class="fx-button resetbtn" mat-button (click)="discard()"  *ngIf="showDiscard">Discard</button>
    <button  class="fx-button resetbtn" mat-button (click)="reject()"
        *ngIf="!showRejectBtn &&filteredAllowedActions && filteredAllowedActions.length > 0">Reject</button>
    <button class="fx-button resetbtn" mat-button (click)="sendBackForReview()"
        *ngIf="!showRejectBtn && filteredAllowedActions && filteredAllowedActions.length > 0">Send Back</button>
                <button *ngFor="let action of filteredAllowedActions" class="fx-button inbox-submit-info-btn searchbtn sendtodata-btn" mat-button
                    (click)="_setAction(action.transitionClaim)"  style="margin-right:10px;">
                    <img [src]="action.image" *ngIf="action.image">
                    <span *ngIf="!action.image">{{action.actionName}}</span>
                </button>
            </div>`
})
export class RbsButtonListComponent implements OnInit {
  ngOnInit(): void {
    if (this.allowedActions && this.allowedActions.length > 0) {
      this.filteredAllowedActions = this.allowedActions.filter((a) => { return a.transitionClaim !== "sendToCreatedState"; });
      this.showRejectBtn = this.allowedActions.some((a) => {
        return (a.transitionClaim === "sendToCreatedState" || a.transitionClaim === "SendItemToDataAdminClaim"
          || a.transitionClaim === "sendItemFromCreatedToDataAdminClaim" || a.transitionClaim === "OrderStartCreatedClaim"
          || a.transitionClaim === "OrderCreatedtoCommercialMgrClaim");
      });
    }
  }
  @Input() showDiscard: boolean;
  @Input() formGroup: FormGroup;
  @Input() allowedActions: WorkflowAction[];
  @Input() cssClass: string;
  @Output() fxClick = new EventEmitter();
  @Output() rejectClick = new EventEmitter();
  @Output() sendBackClick = new EventEmitter();
  public filteredAllowedActions: WorkflowAction[];
  public showRejectBtn: boolean;
  @Output() discardClick = new EventEmitter();

  private _setAction(transitionClaim: string): void {
    this.formGroup.controls["currentActionId"].setValue(transitionClaim);
    this.fxClick.emit();
  }
  private reject(): void {
    this.rejectClick.emit();
  }
  private sendBackForReview(): void {
    this.sendBackClick.emit();
  }
  private discard(): void {
    this.discardClick.emit();
  }
}
