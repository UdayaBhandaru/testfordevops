﻿import { Component, Input } from "@angular/core";
import { FormGroup, AbstractControl } from "@angular/forms";

@Component({
    selector: "rb-textarea",
    template: `
<mat-form-field [formGroup]="formGroup" class="fx-textarea {{cssClass}}">
  <textarea matInput placeholder="{{placeholder}}" formControlName="{{controlName}}"
 rows="{{rows}}" cols="{{cols}}" matAutosizeMinRows="{{autosizeMinRows}}" matAutosizeMaxRows="{{autosizeMaxRows}}"
matTextareaAutosize #autosize="matTextareaAutosize" maxlength="{{maxlength}}"
[required]="required" [readonly]="readonly" [rbUpperCase]="upperCase"></textarea>
</mat-form-field><ng-content></ng-content>`
})
export class RbTextAreaComponent {
    @Input() formGroup: FormGroup;
    @Input() controlName: string;
    @Input() placeholder: string;
    @Input() required: boolean;
    @Input() disabled: boolean;
    @Input() readonly: boolean;
    @Input() rows: number;
    @Input() cols: number;
    @Input() autosizeMinRows: number;
    @Input() autosizeMaxRows: number;
    @Input() maxlength: number;
    @Input() cssClass: string;
    @Input() upperCase: boolean = false;
    private _control: AbstractControl;

    constructor() {
        this.rows = this.rows ? this.rows : 5;
        this.cols = this.cols ? this.cols : 40;
    }

    ngOnInit(): void {
        this._control = this.formGroup.controls[this.controlName];
        if (this._control) {
            if (this.disabled && this.disabled.toString().toLowerCase() === "true") {
                this._control.disable();
            }
        }
    }
}
