import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import { FormGroup, FormControl, AbstractControl } from "@angular/forms";

@Component({
        selector: "rb-select",
        template: `<div [formGroup]="formGroup" class="fx-select">
            <mat-form-field>
               <mat-select placeholder="{{placeholder}}" formControlName="{{controlName}}" [required]="required" 
                [multiple]="multiple">
                <mat-option *ngFor="let optionObject of optionsData" [value]="optionObject[optionKey]" (onSelectionChange) = "_change($event)">
                                        {{optionObject[optionValue]}}
                    </mat-option>
                </mat-select>
                </mat-form-field>
            </div>`
})
export class RbSelectComponent implements OnInit {
    @Input() formGroup: FormGroup;
    @Input() controlName: string;
    @Input() optionsData: any;
    @Input() optionValue: any;
    @Input() optionKey: any;
    @Input() placeholder: string;
    @Input() cssClass?: string;
    @Input() required: boolean;
    @Input() disabled?: boolean;
    @Input() multiple: boolean;
    @Output() selectedTextEvent: EventEmitter<string> = new EventEmitter();
    @Output() rbChange = new EventEmitter();
    private _control: AbstractControl;

    ngOnInit(): void {
        this._control = this.formGroup.controls[this.controlName];
        if (this._control) {
            if (this.disabled && this.disabled.toString().toLowerCase() === "true") {
                this._control.disable();
            }
        }
    }

    _change(event: any): void {
        this.selectedTextEvent.emit(event.source.viewValue);
        this.rbChange.emit(event);
    }
}
