﻿import { Injectable, ViewChild, TemplateRef } from "@angular/core";
import { MatDialog, MatDialogConfig, MatDialogRef } from "@angular/material";
import { RbMessageDialogComponent } from "./RbMessageDialogComponent";
import { RbButton, RbDialog } from "./RbControlModels";
import { ServiceDocumentResult, MessageResult } from "@agility/frameworkcore";

@Injectable()
export class RbDialogService {
    _config: MatDialogConfig = {
        disableClose: false,
        width: "",
        height: "",
        position: {
            top: "",
            bottom: "",
            left: "",
            right: ""
        },
        data: new RbDialog()
    };
    rbDialog: RbDialog = new RbDialog();
    dialogRef: MatDialogRef<any>;

    constructor(private dialog: MatDialog) {
    }

    openDialog(rbDialog: RbDialog): MatDialogRef<any> {
        this._fillData(rbDialog);
        if (rbDialog.templateRef !== undefined) {
            return this.dialog.open(rbDialog.templateRef, this._config);
        } else if (rbDialog.componentRef !== undefined) {
            this._config.data = rbDialog.data;
            return this.dialog.open(rbDialog.componentRef, this._config);
        } else {
            return this.dialog.open(RbMessageDialogComponent, this._config);
        }
    }

    openRbMessageDialog(rbDialog: RbDialog): MatDialogRef<RbMessageDialogComponent> {
        this._fillData(rbDialog);
        return this.dialog.open(RbMessageDialogComponent, this._config);
    }

    openMessageDialog(title: string, result: MessageResult, buttonList: RbButton[] = [], width: string = "", height: string = ""
        , iconCss: string = "", titleCss: string = "", contentCss: string = "", maxWidth: string = ""): MatDialogRef<RbMessageDialogComponent> {
        return this._openDialog(title, result.message, buttonList, width, height, iconCss, titleCss, contentCss, maxWidth);
    }

    private _openDialog(title: string, content: string, buttonList: RbButton[] = [], width: string = "", height: string = ""
        , iconCss: string = "", titleCss: string = "", contentCss: string = "", maxWidth: string= ""): MatDialogRef<RbMessageDialogComponent> {
        this.rbDialog.title = title;
        this.rbDialog.content = content;
        this.rbDialog.width = width !== "" ? width : "";
        this.rbDialog.height = height !== "" ? height : "";
        this.rbDialog.iconCss = iconCss !== "" ? iconCss : "";
        this.rbDialog.titleCss = titleCss !== "" ? titleCss : "";
        this.rbDialog.contentCss = contentCss !== "" ? contentCss : "";
        this.rbDialog.buttonList = buttonList;
        this.rbDialog.maxWidth = maxWidth !== "" ? maxWidth : "";

        this._fillData(this.rbDialog);
        this.dialogRef = this.dialog.open(RbMessageDialogComponent, this._config);
        this.dialogRef.componentInstance.click.subscribe(
            () => {
                this.dialogRef.close();
            });

        return this.dialogRef;
    }

    private _fillData(rbDialog: RbDialog): void {
        this._config.disableClose = rbDialog.disableClose;
        this._config.width = (rbDialog.width == null || rbDialog.width === undefined) ? "" : rbDialog.width;
        this._config.height = (rbDialog.height == null || rbDialog.height === undefined) ? "" : rbDialog.height;
        this._config.maxWidth = (rbDialog.maxWidth == null || rbDialog.maxWidth === undefined) ? "" : rbDialog.maxWidth;
        this._config.position = (rbDialog.position == null || rbDialog.position === undefined) ? {
            top: "",
            bottom: "",
            left: "",
            right: ""
        } : rbDialog.position;

        this._config.data = rbDialog;
    }
}
