import { TemplateRef } from "@angular/core";

export class RbDialog {
  disableClose: boolean;
  width?: string;
  maxWidth?: string;
  height?: string;
  position: {
    top?: string;
    bottom?: string;
    left?: string;
    right?: string;
  };
  title: string;
  content: string;
  iconCss: string;
  titleCss: string;
  contentCss: string;
  buttonList: RbButton[];
  templateRef: TemplateRef<any>;
  componentRef: any;
  data: any;
  message: string;
}

export class RbButton {
  name: string;
  displayName: string;
  cssClass: string;

  public constructor(name: string, displayname: string, cssClass: string) {
    let self: RbButton = this;
    self.name = name;
    self.displayName = displayname;
    self.cssClass = cssClass;
  }
}
