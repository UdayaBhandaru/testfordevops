import { Component, Input, Output, EventEmitter, ChangeDetectorRef, ViewChild } from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";
import { Observable, Subscription } from "rxjs";
import { MatAutocompleteTrigger } from "@angular/material";
import { startWith, map } from "rxjs/operators";

@Component({
  selector: "rb-autocomplete-chips",
  templateUrl: "./RbAutoCompleteComponentWithChips.html"
})
export class RbAutoCompleteComponentWithChips {
  @Input() formGroup: FormGroup;
  @Input() formControl: FormControl;
  @Input() placeholder: string;
  @Input() optionsData: any;
  @Input() optionKey: any;
  @Input() optionValue: any;
  @Input() cssClass: string;
  @Input() removableChip: boolean;
  @Input() chipOrientation: string;
  @Input() selectedChips: any;
  @Input() imageSrc: any;
  @Input() imageClass: any;
  @Input() logicalRemove: boolean = false;
  public filteredOptions: Observable<any>;
  @Output() rbChange = new EventEmitter();
  @Output() rbChipSelected = new EventEmitter();
  private chipClass: string;
  private chipOrientationVertical = "vertical";
  private chipOrientationVerticalStyle = "mat-chip-list-stacked";
  @ViewChild(MatAutocompleteTrigger) trigger: MatAutocompleteTrigger;
  @ViewChild("autoCompleteElement") autoCompleteElement: any;
  @Input() callback: Function;
  private subscription: Subscription;
  private expression: any;
  constructor(private ref: ChangeDetectorRef) {
    this.expression = "(option: any) => option.operation != 'D'";
  }

  ngOnInit(): void {
    let self: RbAutoCompleteComponentWithChips = this;
    self.filteredOptions = self.formControl.valueChanges
      .pipe(startWith(null)
        , map(x => x ? self.filter(x) : self.getFilteredData().slice()));
    if (this.chipOrientationVertical === this.chipOrientation) {
      this.chipClass = this.chipOrientationVerticalStyle;
    } else {
      this.chipClass = "";
    }
  }

  ngAfterViewChecked(): void {
    var self: RbAutoCompleteComponentWithChips = this;
    this.ref.detach();
    this.ref.detectChanges();
    if (!self.formControl.value) {
      self.filteredOptions = self.formControl.valueChanges
        .pipe(startWith(null)
          , map(function (x: any): any { return x ? self.filter(x) : self.getFilteredData().slice(); }));
    }
  }

  ngAfterViewInit(): void {
    this.subscribeToClosingActions();
  }

  ngOnDestroy(): void {
    if (this.rbChange.observers.length > 0) {
      this.rbChange.unsubscribe();
      this.rbChange = null;
    }

    if (this.rbChipSelected.observers.length > 0) {
      this.rbChipSelected.unsubscribe();
      this.rbChipSelected = null;
    }

    if (this.callback) {
      this.callback = null;
    }
  }

  private filter(val: string): any {
    let self: RbAutoCompleteComponentWithChips = this;
    return val ? self.getFilteredData().filter((option: any) => new RegExp(`^${val}`, "gi").test(option[self.optionValue]))
      : self.optionsData;
  }

  public displayFn(): any {
    let self: RbAutoCompleteComponentWithChips = this;
    return (val: any) => val ? val[self.optionValue] : val;
  }

  public onOptionSelect(event: any): void {
    let option: any = Object.assign({}, event.option.value);
    let self: RbAutoCompleteComponentWithChips = this;
    if (this.selectedChips.findIndex(x => x[this.optionKey] === option[this.optionKey]) === -1
      || this.validateExpression(option[this.optionKey])) {
      this.selectedChips.push(option);
      self.rbChange.emit({ event: event, options: option });
      self.emitChipOptions(event, option, false);
      this.formControl.setValue(null);
      this.autoCompleteElement.nativeElement.blur();
    } else {
      option.operation = "I";
      this.formControl.setValue(null);
      this.autoCompleteElement.nativeElement.blur();
    }
  }

  private getFilteredData(): any {
    return this.arrayDiffObj(this.optionsData, this.selectedChips.filter((x) => this.validateExpression(x)), this.optionKey);
  }
  private remove(option: any): void {
    let index: any = this.selectedChips.filter((x) => this.validateExpression(x)).indexOf(option);
    var self: any = this;
    if (index >= 0) {
      if (!this.logicalRemove) {
        this.selectedChips.splice(index, 1);
      }
      self.emitChipOptions(event, option, true);
    }
  }

  private emitChipOptions(event: any, option: any, isRemoved: boolean): void {
    var self: RbAutoCompleteComponentWithChips = this;
    self.rbChipSelected.emit({ event: event, options: option, selectedChips: this.selectedChips, isRemoved: isRemoved });
  }

  private subscribeToClosingActions(): any {
    if (!!this.subscription && !this.subscription.closed) {
      this.subscription.unsubscribe();
    }
    this.subscription = this.trigger.panelClosingActions
      .subscribe((e: any) => {
        if (typeof this.formControl.value !== typeof Object) {
          this.formControl.setValue(null);
        }
      },
        err => this.subscribeToClosingActions(),
        () => this.subscribeToClosingActions());
  }

  private arrayDiffObj(s: any[], v: any[], key: string): any {
    let reducedIds: any = v.map((o) => o[key]);
    return s.filter((obj: any) => reducedIds.indexOf(obj[key]) === -1);
  }

  private filterChipsDisplayData(): any {
    return this.selectedChips.filter((option: any) => this.validateExpression(option));
  }

  private validateExpression(option: any): any {
    if (this.callback) {
      return this.callback({ option: option });
    } else {
      return true;
    }
  }
}
