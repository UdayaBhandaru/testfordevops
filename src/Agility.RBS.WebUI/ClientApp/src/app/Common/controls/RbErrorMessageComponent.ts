﻿import { Component, Input } from "@angular/core";
import { FormGroup, FormControl, ValidationErrors } from "@angular/forms";
import { ValidationService } from "@agility/frameworkcore";

@Component({
    selector: "rb-error-messages",
    template: `<div *ngIf="errorMessage !== null">{{errorMessage}}</div>`
})
export class RbErrorMessageComponent {
    @Input() control: FormControl;

    get errorMessage(): any {
        if (this.control) {
            let errors: ValidationErrors = <ValidationErrors>this.control.errors;
            for (let propertyName in errors) {
                if (errors.hasOwnProperty(propertyName) && this.control.touched) {
                    return ValidationService.getValidatorErrorMessage(propertyName, errors[propertyName]) || errors[propertyName];
                }
            }
        }
        return null;
    }
}