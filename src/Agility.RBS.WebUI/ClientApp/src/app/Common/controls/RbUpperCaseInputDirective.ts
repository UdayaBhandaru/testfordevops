﻿import { Directive, ElementRef, Input } from "@angular/core";
@Directive({
    selector: "[rbUpperCase]",
    host: {
        "(input)": "toUpperCase($event.target.value)"
    },
})
export class UpperCaseTextDirective  {

    @Input("rbUpperCase") public upperCase: boolean;
    constructor(private ref: ElementRef) {
    }

    public toUpperCase(value: any) {
        if (this.upperCase) {
        this.ref.nativeElement.value = value.toUpperCase();
        }
    }
}
