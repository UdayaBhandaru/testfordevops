import { Component, Inject } from "@angular/core";
import { MAT_DIALOG_DATA } from "@angular/material";
import { RbDialog } from "./controls/RbControlModels";

@Component({
  selector: "stack-trace",
  templateUrl: "./StackTraceComponent.html"
})
export class StackTraceComponent {
  _show: boolean = false;
  constructor(@Inject(MAT_DIALOG_DATA) public data: RbDialog) {
  }
}
