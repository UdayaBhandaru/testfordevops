﻿export class ReportSourceParams {
    orderNo?: number;
    costChangeId?: number;
    item?: number;
    divisionId?: number;
    deptId?: number;
    categoryId?: number;
    classId?: number;
    subClassId?: number;
    fromDate?: Date;
    toDate?: Date;
    dateType?: string;
    orderType?: string;
    supplier?: number;
    location?: number;
    reason?: number;
}