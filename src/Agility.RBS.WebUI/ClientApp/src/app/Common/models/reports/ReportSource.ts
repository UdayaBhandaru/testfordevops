import { ReportSourceParams } from './ReportSourceParams';


export class ReportSource {
    report: string;
    parameters: ReportSourceParams;
}
