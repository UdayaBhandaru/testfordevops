﻿export class TreeViewModel {
    expanded: boolean = false;
    toggle(): void {
        this.expanded = !this.expanded;
    }
}