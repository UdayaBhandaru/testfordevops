import { Component, Input } from "@angular/core";

@Component({
    selector: "page-title",
    template: `<span> {{PageTitle}}</span>`
})
export class TitleComponent {
    @Input() PageTitle = "";
}

