import { Component } from "@angular/core";
import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
    selector: "child-cell",
  template: `<div fxLayout="row" fxLayoutAlign="end center" class="grid-actions"><span>{{ params.value | number:'1.4'}} {{params.value ? currency:""}}</span>
        </div>`
})
export class GridAmountComponent implements ICellRendererAngularComp {
    public params: any;
    public currency: string;

    agInit(params: any): void {
        this.params = params;
       // let currencyAttr: any = this.params.context.componentParent.getCurrencyAttribute(params.colDef.field);
       // this.currency = params.data[currencyAttr];
    }

    refresh(params: any): boolean {
        return true;
    }
}
