import { Component } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular/main";

@Component({
    selector: "child-cell",
    template: `<div fxLayout="row" class="grid-actions">
           <input type="number" (blur)="invokeParentMethod($event)" [value]="params.value" *ngIf="!hideControl"
                class="gridNumeric"
                [disabled]="params.colDef.field === 'discountValue' && params.data.merchLevel === '12'
                    || params.colDef.field === 'newCost' && params.data.merchLevel !== '12'" />
            <span *ngIf="hideControl">{{params.value}}</span>
            </div>
            <style>
                .gridNumeric{
                    width: 100%;
                    margin-top: -4px;
                 }
               .invalid-grid-row .gridNumeric {
                   border: 1px solid $orange !important;
                   padding: 7px 3px 3px;
                   width: 100%;
               }
            </style>`
})
export class GridNumericComponent implements ICellRendererAngularComp {
    public params: any;
    public hideControl: boolean = false;

    agInit(params: any): void {
        this.params = params;
        if (params.colDef.field === "unitCostInit") {
            if (params.data.costSource === "MNL") {
                this.hideControl = false;
            } else {
                this.hideControl = true;
            }
        }
    }

    refresh(params: any): boolean {
        return true;
    }

    public invokeParentMethod($event: MouseEvent): void {
        $event.preventDefault();
        this.params.context.componentParent.update(this.params, $event);
    }
}
