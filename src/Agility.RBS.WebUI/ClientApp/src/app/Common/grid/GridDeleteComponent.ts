import { Component } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular/main";
import { SharedService } from "../SharedService";
import { MatDialogRef } from "@angular/material";
import { MessageResult } from "@agility/frameworkcore";
import { RbButton } from "../../Common/controls/RbControlModels";
import { RbDialogService } from "../../Common/controls/RbDialogService";

@Component({
    selector: "child-cell",
    template: `<div fxLayout="row" class="grid-actions">
            <a href="#" (click)="invokeParentMethod($event)"><mat-icon class="delete" matTooltip="Delete"></mat-icon></a>
        </div>
  <style>
    .grid-actions a mat-icon {
      background: url('../../../assets/images/grid-action-icons.png') no-repeat center;
      width: 16px;
      height: 16px;
      display: block; }
      div.grid-actions a mat-icon.delete {
        background-position: 0 -32px; }
      }

    </style>`
})
export class GridDeleteComponent implements ICellRendererAngularComp {
    public params: any;
    dialogRef: MatDialogRef<any>;
    private messageResult: MessageResult = new MessageResult();

    agInit(params: any): void {
        this.params = params;
    }

    refresh(params: any): boolean {
        return true;
    }

    constructor(private sharedService: SharedService
        , private _dialogService: RbDialogService) {

    }

    public invokeParentMethod($event: MouseEvent): void {
        $event.preventDefault();
        this.messageResult.message = `Are you sure you want to delete this record? `;
        this.dialogRef = this._dialogService.openMessageDialog("Warning", this.messageResult, [new RbButton("", "Yes", "alertCancel")
            , new RbButton("", "No", "alertdontsave")], "30%", "", "iconsdontsave retailsNavIcons", "dontsaveAlert", "dontsaveAlertTitle");
        this.dialogRef.componentInstance.click.subscribe(
            btnName => {
                if (btnName === "Yes") {
                    this.params.context.componentParent.delete(this.params);
                }
            });
    }
}
