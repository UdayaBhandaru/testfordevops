﻿import { Component } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular/main";

@Component({
    selector: "child-cell",
    template: `<div fxLayout="row" class="grid-actions">
                <a href="#" (click)="invokeLinkClick($event)"><mat-icon class="view" matTooltip="View Details" ></mat-icon></a>
             </div>`
})
export class GridLinkRendererComponent implements ICellRendererAngularComp {
    public params: any;

    agInit(params: any): void {
        this.params = params;
    }

    refresh(params: any): boolean {
        return true;
    }

    public invokeLinkClick($event: MouseEvent): void {
        $event.preventDefault();
        this.params.context.componentParent.invokeLinkClick(this.params, $event);
    }
}