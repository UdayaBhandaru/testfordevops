import { Component } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular/main";

@Component({
    selector: "child-cell",
    template: `<div fxLayout="row" class="grid-actions">
           <input type="checkbox" (change)="invokeParentMethod($event)" [checked]="params.value==='1'" />
        </div>`
})
export class IndicatorComponent implements ICellRendererAngularComp {
    public params: any;

    agInit(params: any): void {
        this.params = params;
    }

    refresh(params: any): boolean {
        return true;
    }

    public invokeParentMethod($event: MouseEvent): void {
        $event.preventDefault();
        this.params.context.componentParent.change(this.params, $event);
    }
}
