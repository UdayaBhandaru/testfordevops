import { Component } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular/main";

@Component({
    selector: "child-cell",
    template: `<div fxLayout="row" class="grid-actions">
           <input type="text" (blur)="invokeParentMethod($event)" [value]="params.value" [hidden]="params.data.createdBy
                && params.colDef.field==='domainDetailId'">
            <span [hidden]="!params.data.createdBy" *ngIf="params.colDef.field==='domainDetailId'">{{params.value}}</span>
            </div>`
})
export class GridInputComponent implements ICellRendererAngularComp {
    public params: any;

    agInit(params: any): void {
        this.params = params;
    }

    refresh(params: any): boolean {
        return true;
    }

    public invokeParentMethod($event: MouseEvent): void {
        $event.preventDefault();
        this.params.context.componentParent.update(this.params, $event);
    }
}
