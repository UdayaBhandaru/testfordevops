import { Component } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular/main";
import { CommonModel } from "../CommonModel";

@Component({
  selector: "child-cell",
  template: `<div *ngIf="isMerchLevelValue">
        <div fxLayout="row" class="grid-actions" *ngIf="params.data.merchLevel === '2'">
          <select style="width:100%;" (change)="invokeParentMethod($event)" [value]="selectedValue" [disabled]="isDisable">
            <option *ngFor="let reference of divisionData" [value]="reference[key]">{{reference[value]}}</option>
        </select>
        <span *ngIf="hideControl">{{domainDescription}}</span>
        </div>
        <div fxLayout="row" class="grid-actions" *ngIf="params.data.merchLevel === '3'">
          <select style="width:100%;" (change)="invokeParentMethod($event)" [value]="selectedValue" [disabled]="isDisable">
            <option *ngFor="let reference of departmentData" [value]="reference[key]">{{reference[value]}}</option>
        </select>
        <span *ngIf="hideControl">{{domainDescription}}</span>
        </div>
        <div fxLayout="row" class="grid-actions" *ngIf="params.data.merchLevel === '4'">
          <select style="width:100%;" (change)="invokeParentMethod($event)" [value]="selectedValue" [disabled]="isDisable">
            <option *ngFor="let reference of categoryData" [value]="reference[key]">{{reference[value]}}</option>
        </select>
        <span *ngIf="hideControl">{{domainDescription}}</span>
        </div>
        <div fxLayout="row" class="grid-actions" *ngIf="params.data.merchLevel === 'BRAND'">
          <select style="width:100%;" (change)="invokeParentMethod($event)" [value]="selectedValue" [disabled]="isDisable">
            <option *ngFor="let reference of brandsData" [value]="reference['brandId']">{{reference['brandDescription']}}</option>
        </select>
        <span *ngIf="hideControl">{{domainDescription}}</span>
        </div>
        </div>
        <div *ngIf="isOrgLevelValue">
        <div fxLayout="row" class="grid-actions" *ngIf="params.data.orgLevel === 'PZ'">
          <select style="width:100%;" (change)="invokeParentMethod($event)" [value]="selectedValue" [disabled]="isDisable">
            <option *ngFor="let reference of priceZoneData" [value]="reference[key]">{{reference[value]}}</option>
        </select>
        <span *ngIf="hideControl">{{domainDescription}}</span>
        </div>
        <div fxLayout="row" class="grid-actions" *ngIf="params.data.orgLevel === 'LOC'">
          <select style="width:100%;" (change)="invokeParentMethod($event)" [value]="selectedValue" [disabled]="isDisable">
            <option *ngFor="let reference of locationData" [value]="reference[key]">{{reference[value]}}</option>
        </select>
        <span *ngIf="hideControl">{{domainDescription}}</span>
        </div>
        </div>
        <div *ngIf="isLocType">
        <div fxLayout="row" class="grid-actions" *ngIf="params.data.locationType === 'S'">
          <select style="width:100%;" (change)="invokeParentMethod($event)" [value]="selectedValue" [disabled]="isDisable">
            <option *ngFor="let reference of storeLocationsData" [value]="reference[key]">{{reference[value]}}</option>
        </select>
        <span *ngIf="hideControl">{{domainDescription}}</span>
        </div>
        <div fxLayout="row" class="grid-actions" *ngIf="params.data.locationType === 'W'">
          <select style="width:100%;" (change)="invokeParentMethod($event)" [value]="selectedValue" [disabled]="isDisable">
            <option *ngFor="let reference of whsLocationsData" [value]="reference[key]">{{reference[value]}}</option>
        </select>
        <span *ngIf="hideControl">{{domainDescription}}</span>
        </div>
        </div>`
})
export class GridMerchLvlValComponent implements ICellRendererAngularComp {
  public params: any;
  public selectedValue: any;
  public domainDescription: string;
  public hideControl: boolean = false;
  public isDisable: boolean = false;
  public key: string | number = "id";
  public value: string | number = "description";
  public divisionData: CommonModel[] = [];
  public departmentData: CommonModel[] = [];
  public categoryData: CommonModel[] = [];
  public brandsData: any[] = [];
  public locationData: CommonModel[] = [];
  public priceZoneData: CommonModel[] = [];
  public isMerchLevelValue: boolean = false;
  public isOrgLevelValue: boolean = false;
  public isLocType: boolean = false;
  public storeLocationsData: any[] = [];
  public whsLocationsData: any[] = [];

  agInit(params: any): void {
    this.params = params;
    this.selectedValue = params.value;
    if (params.colDef.field === "merchLevelValue") {
      this.isMerchLevelValue = true;
      this.divisionData = params.divisionData;
      this.departmentData = params.deptData;
      this.categoryData = params.ctgData;
      this.brandsData = params.brandsData;
    } else if (params.colDef.field === "locationId") {
      this.isLocType = true;
      this.storeLocationsData = params.storeLocationsData;
      this.whsLocationsData = params.whsLocationsData;
    } else {
      this.isOrgLevelValue = true;
      this.locationData = params.locationData;
      this.priceZoneData = params.priceZoneData;
    }

    if (params.keyvaluepair) {
      this.key = params.keyvaluepair.key;
      this.value = params.keyvaluepair.value;
    }
    if (params.data.operation === "U") {
      this.isDisable = true;
    }
  }

  refresh(params: any): boolean {
    return true;
  }

  public invokeParentMethod($event: MouseEvent, mode: string): void {
    $event.preventDefault();
    this.params.context.componentParent.select(this.params, $event);
  }
}
