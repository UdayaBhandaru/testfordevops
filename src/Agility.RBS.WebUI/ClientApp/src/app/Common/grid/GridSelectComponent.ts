import { Component } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular/main";

@Component({
    selector: "child-cell",
    template: `<div fxLayout="row" class="grid-actions">
          <select style="width:100%;" (change)="invokeParentMethod($event)" [value]="selectedValue" [hidden]="hideControl" [disabled]="isDisable">
            <option *ngFor="let reference of params.references" [value]="reference[key]">{{reference[value]}}</option>
        </select>
        <span *ngIf="hideControl">{{domainDescription}}</span>
        </div>
        <style>
            select {
                  padding: 5px 0px;
                  width: 75%;
             }
        </style>`
})
export class GridSelectComponent implements ICellRendererAngularComp {
    public params: any;
    public selectedValue: any;
    public domainDescription: string;
    public hideControl: boolean = false;
    public isDisable: boolean = false;
    public key: string | number = "code";
    public value: string | number = "codeDesc";

    agInit(params: any): void {
        this.params = params;
        this.selectedValue = params.value;
        if (params.keyvaluepair) {
            if (params.colDef.field === "location") {
                if (params.data.unitCostInit || params.data.headLocation) {
                    this.hideControl = true;
                    this.domainDescription = params.data.locationName;
                }
            }
            this.key = params.keyvaluepair.key;
            this.value = params.keyvaluepair.value;
        }
        if (params.colDef.field === "isExpiry") {
            this.hideControl = true;
            this.domainDescription = params.references.find(x => x.code === params.value).code;
        }
      if (params.colDef.field === "discountType" || (params.data.operation === "U" && params.colDef.field !== "itemType"
        && params.colDef.field !== "sourceMethod" && params.colDef.field !== "reasonCode")) {
            this.isDisable = true;
        }
    }

    refresh(params: any): boolean {
        return true;
    }

    public invokeParentMethod($event: MouseEvent): void {
        $event.preventDefault();
        this.params.context.componentParent.select(this.params, $event);
    }
}
