import { Component } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular/main";
import { RbButton } from "../controls/RbControlModels";
import { MessageResult } from "@agility/frameworkcore";
import { MatDialogRef } from "@angular/material";
import { RbDialogService } from "../controls/RbDialogService";

@Component({
  selector: "child-cell",
  template: `<div fxLayout="row" class="grid-actions">
            <a href="#" (click)="invokeParentMethod($event,'edit')"><mat-icon class="edit" matTooltip="Edit" *ngIf="showEditIcon"></mat-icon></a>
            <a href="#" (click)="invokeParentMethod($event, 'delete')"><mat-icon class="delete" matTooltip="Delete" *ngIf="showDeleteIcon"></mat-icon></a>
            <a href="#" (click)="invokeParentMethod($event, 'approve')"><mat-icon class="" matTooltip="Approve" *ngIf="showApproveIcon"></mat-icon></a>
        </div>
      <style>
    .grid-actions a mat-icon {
      background: url('../../../assets/images/grid-action-icons.png') no-repeat center;
      width: 16px;
      height: 16px;
      display: block; }
      div.grid-actions a mat-icon.edit {
        background-position: 0 0px; }
      div.grid-actions a mat-icon.delete {
        background-position: 0 -32px; }
      div.grid-actions a mat-icon.view {
        position: absolute;
        left: 40px; }
      div.grid-actions .history {
        background-position: -2016px -26px !important;
        width: 13px;
        height: 13px;
        display: block;
        margin-left: 10px;
        margin-top: 2px; }
      div.grid-actions .comments {
        background-position: -1986px -26px !important;
        width: 13px;
        height: 13px;
        display: block;
        margin-left: 10px;
        margin-top: 2px; }
      div.grid-actions .receivingIcon {
        background-position: -1470px -260px;
        width: 17px;
        height: 16px;
        display: block; }
      div.grid-actions .grid-image {
        height: 20px;
        width: 20px;
        background-color: #fc8605;
        border-radius: 50%;
        color: white;
        text-align: center;
        line-height: 1.7em; }
      div.grid-actions .grid-contact-person {
        margin: 3px 0px 0px 4px; }

    </style>`
})
export class GridEditComponent implements ICellRendererAngularComp {
  public params: any;
  public showViewIcon: boolean = true;
  public showEditIcon: boolean = true;
  public showApproveIcon: boolean = false;
  public showDeleteIcon: boolean;
  dialogRef: MatDialogRef<any>;
  private messageResult: MessageResult = new MessageResult();

  constructor(private _dialogService: RbDialogService) {

  }

  agInit(params: any): void {
    this.params = params;
    if (params.restrictViewIcon) {
      this.showViewIcon = false;
    }
    if (params.restrictEditIcon) {
      this.showEditIcon = false;
      this.showDeleteIcon = true;
    }
    if (params.restrictDeleteIcon) {
      this.showDeleteIcon = false;
    }
    if (params.restrictApproveIcon) {
      this.showApproveIcon = true;
    }
  }

  refresh(params: any): boolean {
    return true;
  }

  public invokeParentMethod($event: MouseEvent, mode: string): void {
    $event.preventDefault();
    if (mode === "delete") {
      this.messageResult.message = `Are you sure you want to delete this record? `;
      this.dialogRef = this._dialogService.openMessageDialog("Warning", this.messageResult, [new RbButton("", "Yes", "alertCancel")
        , new RbButton("", "No", "alertdontsave")], "30%", "", "iconsdontsave retailsNavIcons", "dontsaveAlert", "dontsaveAlertTitle");
      this.dialogRef.componentInstance.click.subscribe(
        btnName => {
          if (btnName === "Yes") {
            this.params.context.componentParent.delete(this.params);
          }
        });
    }
    else if (mode === "approve") {
      this.messageResult.message = `Are you sure you want to approve this record? `;
      this.dialogRef = this._dialogService.openMessageDialog("Warning", this.messageResult, [new RbButton("", "Yes", "alertCancel")
        , new RbButton("", "No", "alertdontsave")], "30%", "", "iconsdontsave retailsNavIcons", "dontsaveAlert", "dontsaveAlertTitle");
      this.dialogRef.componentInstance.click.subscribe(
        btnName => {
          if (btnName === "Yes") {
            this.params.context.componentParent.approve(this.params);
          }
        });
    } else {
      this.params.context.componentParent.open(this.params, mode);
    }
  }
}
