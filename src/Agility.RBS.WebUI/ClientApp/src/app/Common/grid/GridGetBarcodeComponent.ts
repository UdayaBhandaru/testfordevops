import { Component } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular/main";

@Component({
  selector: "child-cell",
  template: `<div fxLayout="row" class="grid-actions">
            <span class=""><a (click)="invokeParentMethod($event)" class="fliter-item">Generate</a></span>
        </div>`
})
export class GridGetBarcodeComponent implements ICellRendererAngularComp {
  public params: any;

  agInit(params: any): void {
    this.params = params;
  }

  refresh(params: any): boolean {
    return true;
  }

  public invokeParentMethod($event: MouseEvent): void {
    $event.preventDefault();
    this.params.context.componentParent.generateBarcode(this.params);
  }
}
