import { Component } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular/main";

@Component({
  selector: "child-date-cell",
  template: `<div fxLayout="row" *ngIf="!hideControl" class="grid-actions input-type-date"
                style="border: 1px solid rgb(169, 169, 169);margin-top: -3px;height: 29px;padding:3px;">
            <input matInput [matDatepicker]="dp3" [value]="params.value" [hidden]="hideControl" (dateChange)="invokeParentMethod($event)"
                [readonly]="readonly" [min]="minDate" [max]="maxDate"/>
            <mat-datepicker-toggle matSuffix [for]="dp3" [hidden]="hideControl" style="margin-top:-10px;"></mat-datepicker-toggle>
            <mat-datepicker #dp3 disabled="false" [hidden]="hideControl" ></mat-datepicker>
        </div>
    <div fxLayout="row" *ngIf="hideControl"><span>{{params.value | date:"dd-MMM-yyyy"}}</span></div>`
})
export class GridDateComponent implements ICellRendererAngularComp {
  public params: any;
  public hideControl: boolean = false;
  public minDate: Date;
  public maxDate: Date;

  agInit(params: any): void {
    this.params = params;
    if (params.colDef.field === "estimatedInstockDate") {
      this.minDate = params.data.headNotBeforeDate;
      this.maxDate = params.data.headNotAfterDate;
      this.hideControl = false;
    } else if (params.colDef.field === "expiryDate" && params.data.isExpiry === "Y") {
      this.hideControl = false;
    } else if (params.colDef.field === "effectiveStartDate" || params.colDef.field === "effectiveEndDate") {
      this.minDate = params.data.headStartDate;
      this.maxDate = params.data.headEndDate;
      this.hideControl = false;
    } else if (params.colDef.field === "expiryDate") {
      this.hideControl = false;
    } else if (params.colDef.field === "docDate" || params.colDef.field === "eventDate" || params.colDef.field === "docActDate" || params.colDef.field === "docExpDate" || params.colDef.field === "effectiveDate" || params.colDef.field === "startDate" || params.colDef.field === "endDate") {
        this.hideControl = false;
        this.maxDate = params.data.docDate;
    } else {
      this.hideControl = true;
    }
  }

  refresh(params: any): boolean {
    return true;
  }

  public invokeParentMethod($event: MouseEvent, mode: string): void {
    this.params.context.componentParent.update(this.params, $event);
  }
}
