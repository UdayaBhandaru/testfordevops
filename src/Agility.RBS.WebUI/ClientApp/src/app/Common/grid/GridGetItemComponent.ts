import { Component } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular/main";
import { SharedService } from "../SharedService";
import { MatDialogRef } from "@angular/material";
import { MessageResult } from "@agility/frameworkcore";
import { RbDialogService } from "../../Common/controls/RbDialogService";

@Component({
    selector: "child-cell",
    template: `<div fxLayout="row" class="grid-actions">
          <input type="text" (blur)="invokeParentMethod($event)" [value]="params.value"
            [hidden]="params.data.unitCost" [disabled]="(params.data.merchLevel && params.data.merchLevel !== '12') || isDisable" />
            <span [hidden]="!params.data.unitCost">{{params.value}}</span>
        </div>
        <style>
          .grid-actions{
          position:relative;
          }
        </style>`
})
export class GridGetItemComponent implements ICellRendererAngularComp {
    public params: any;
    dialogRef: MatDialogRef<any>;
    private messageResult: MessageResult = new MessageResult();
    public isDisable: boolean = false;

    agInit(params: any): void {
        this.params = params;
        if (params.data.operation === "U") {
            this.isDisable = true;
        }
    }

    refresh(params: any): boolean {
        return true;
    }

    constructor(private sharedService: SharedService
        , private _dialogService: RbDialogService) {

    }

    public invokeParentMethod($event: MouseEvent): void {
        $event.preventDefault();
        this.params.context.componentParent.update(this.params, $event);
    }
}
