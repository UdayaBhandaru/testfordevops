import { Component } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular/main";

@Component({
    selector: "file-cell",
    template: `<div fxLayout="row">
       <a href="#" (click)="invokeParentMethod($event)" *ngIf="showDocuments"><mat-icon class="file-actions retailsNavIcons"
            matTooltip="Attachment" [matTooltipPosition]="position"></mat-icon></a>
        </div>`
      })
export class GridFilesComponent implements ICellRendererAngularComp {
    public params: any;
    private position: string = "above";
    showDocuments: boolean = false;

    agInit(params: any): void {
        this.params = params;
        if (this.params.value) {
            this.showDocuments = true;
        }
    }

    refresh(params: any): boolean {
        return true;
    }

    public invokeParentMethod($event: MouseEvent): void {
        $event.preventDefault();
        this.params.context.componentParent.onDocumentsClick(this.params);
    }
}
