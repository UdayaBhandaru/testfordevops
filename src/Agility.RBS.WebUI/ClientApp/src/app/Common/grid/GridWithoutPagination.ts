import { Component, Input, OnInit, Output, ChangeDetectorRef, HostListener, EventEmitter } from "@angular/core";
import { GridOptions, GridApi, ColDef } from "ag-grid-community";
// import "ag-grid-enterprise";
@Component({
    selector: "rbs-grid-without-pagination",
  templateUrl: "./GridWithoutPagination.html",
  styleUrls: ['./GridWithoutPagination.scss']
})
export class GridWithoutPagination implements OnInit {
    defaultColDef: any;
    @Input() autoGroupColumnDef: any;
    @Input() gridOptions: GridOptions;
    @Output() onGridReadyEvt = new EventEmitter();
    @Input() rowData: any;
    @Input() columnDefs: any;
    @Input() showGrid: boolean;
    @Input() alwaysShow: boolean;
    @Input() componentParentName;
    @Input() rbsGridCustomOptions: { showSearchCriteria: boolean };
    @Input() totalRows: number;
    defaultCustomOptions: { showSearchCriteria: boolean } = { showSearchCriteria: true };
    customOptions: { showSearchCriteria: boolean } = { showSearchCriteria: true };

    @Input() rowSelection: string;
    @Input() groupSelectsChildren: boolean;
    @Input() isDataLoaded: boolean;
    constructor(private ref: ChangeDetectorRef) {
    }

    ngOnInit(): void {
        let defaultGridOptions: any = {
            onGridReady: () => {
                if (this.gridOptions && this.gridOptions.api) {
                    this.gridOptions.api.setRowData(this.rowData);
                    this.agGridSizeColumnsToFit();
                    this.onGridReadyEvt.emit(null);

                }
            },
            onGridSizeChanged: () => { this.agGridSizeColumnsToFit(); },
            context: {
                componentParent: this.componentParentName
            },
            suppressPaginationPanel: true,
            suppressScrollOnNewData: false,
            sortingOrder: ["asc", "desc"],
            domLayout: "autoHeight",
            embedFullWidthRows: true,
            headerHeight: 45,
            rowHeight: 40,
            pagination: false,
            enableSorting: true,
            toolPanelSuppressSideButtons: true,
            defaultColDef: {
                filterParams: {
                    newRowsAction: "keep"
                }
            }
        };
        this.defaultColDef = Object.assign(
            { editable: true, filterParams: { newRowsAction: "keep" } }, this.defaultColDef
        );
        this.gridOptions = Object.assign(defaultGridOptions, this.gridOptions);
    }

    @HostListener("window:resize") onResize(): void {
        this.agGridSizeColumnsToFit();
    }

    ngAfterViewChecked(): void {
        this.ref.detectChanges();
    }

    private agGridSizeColumnsToFit(): void {
        setTimeout(() => {
            if (this.gridOptions && this.gridOptions.api && !this.gridOptions.enableColResize) {
                this.gridOptions.api.sizeColumnsToFit();
            }
        }, 1000);
    }

    onBtFirst(): void {
        this.gridOptions.api.paginationGoToFirstPage();
    }
    onBtLast(): void {
        this.gridOptions.api.paginationGoToLastPage();
    }
    onBtPrevious(): void {
        this.gridOptions.api.paginationGoToPreviousPage();
    }
    onBtNext(): void {
        this.gridOptions.api.paginationGoToNextPage();
    }
    onPageSizeChanged(newPageSize: any): void {
        this.gridOptions.api.paginationSetPageSize(+(newPageSize.target.value));
    }

    getPageDetails(): string {
        let api: GridApi = this.gridOptions.api;
        if (api) {
            let start: string = `${(api.paginationGetCurrentPage() * api.paginationGetPageSize()) + 1}`;
            let total: number = this.totalRows ? this.totalRows : (api.paginationGetRowCount() === null ? (this.rowData ? this.rowData.length : 0) : api.paginationGetRowCount());
            let end: number = (api.paginationGetCurrentPage() * api.paginationGetPageSize() + api.paginationGetPageSize());
            end = end > total ? total : end;
            return `${start} - ${end} of ${total}`;
        }
        return "";
    }

    onBtExport(): void {
        this.gridOptions.api.exportDataAsExcel({
            sheetName: this.componentParentName.sheetName
            , fileName: this.componentParentName.PageTitle
        });
    }

    setAutoHeight(): void {
        this.gridOptions.api.setDomLayout("autoHeight");
        document.querySelector("#rbaggrid").setAttribute("height", "");
    }

    setFixedHeight(): void {
        this.gridOptions.api.setDomLayout("normal");
        (document.querySelector("#rbaggrid")).setAttribute("height", "400px");
    }
}
