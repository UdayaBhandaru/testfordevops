﻿export class GridExportModel {
    skipHeader?: boolean = false;
    columnGroups?: boolean = false;
    skipFooters?: boolean = false;
    skipGroups?: boolean = false;
    skipFloatingTop?: boolean = false;
    skipFloatingBottom?: boolean = false;
    suppressQuotes?: boolean = false;
    fileName?: string;
    allColumns?: boolean = false;
    onlySelected?: boolean = false;
    onlySelectedAllPages?: boolean = false;
}