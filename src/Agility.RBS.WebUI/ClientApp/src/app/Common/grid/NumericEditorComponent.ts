import { AfterViewInit, Component, ViewChild, ViewContainerRef } from "@angular/core";
import { ICellEditorAngularComp } from "ag-grid-angular";
import { ICellEditorParams } from "ag-grid-community";

@Component({
  selector: "numeric-cell",
  template: `<input #input (keydown)="onKeyDown($event)" [(ngModel)]="value" style="width: 100%">`
})
export class NumericEditorComponent implements ICellEditorAngularComp, AfterViewInit {
  private params: ICellEditorParams;
  public value: number;
  private cancelBeforeStart: boolean = false;

  @ViewChild("input", { read: ViewContainerRef }) public input;

  constructor() {
  }
  agInit(params: ICellEditorParams): void {
    this.params = params;
    this.value = this.params.value;
    this.cancelBeforeStart = params.charPress && ("1234567890".indexOf(params.charPress) < 0);
  }

  getValue(): any {
    return this.value;
  }

  isCancelBeforeStart(): boolean {
    return this.cancelBeforeStart;
  }

  isCancelAfterEnd(): boolean {
    return false;
  }

  onKeyDown(event: any): void {
    if (!this.isKeyPressedNumeric(event)) {
      if (event.preventDefault) event.preventDefault();
    } else if (this.isKeyPressedNavigation(event)) {
      event.stopPropagation();
    }
  }

  isKeyPressedNavigation(event) {
    return event.keyCode === 39
      || event.keyCode === 37;

  };
  ngAfterViewInit(): void {
    setTimeout(() => {
      this.input.element.nativeElement.focus();
    });
  }

  private getCharCodeFromEvent(event: any): any {
    event = event || window.event;
    return (typeof event.which === "undefined") ? event.keyCode : event.which;
  }

  private isCharNumeric(charStr: any): boolean {
    return !!/\d/.test(charStr);
  }

  private isKeyPressedNumeric(event: any): boolean {
    const charCode: any = this.getCharCodeFromEvent(event);
    const charStr: any = event.key ? event.key : String.fromCharCode(charCode);
    return this.isCharNumeric(charStr) || event.keyCode === 8;
  }
}
