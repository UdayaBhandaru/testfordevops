﻿export class DeliveryPolicyDomainModel {
    deliveryPolicyID: string;

    deliveryPolicyName: string;

    deliveryPolicyDesc: string;

    deliveryPolicyStatus: string;
}
