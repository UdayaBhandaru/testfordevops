export class DepartmentDomainModel {
    dept: number;
    deptName: string;
    groupNo: number;
    deptDescription: string;
}
