﻿export class ItemSupplierCompanyModel {
    item: number;
    companyId: number;
    companyName?: string;
    supplier: number;
    suppName?: string;
}