﻿export class CompanyDomainModel {
    companyId: number;
    companyName: string;
    companyNameDesc: string;
    companyStatus: string;
}