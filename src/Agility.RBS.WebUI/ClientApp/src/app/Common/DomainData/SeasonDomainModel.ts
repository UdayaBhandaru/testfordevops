export class SeasonDomainModel {
    seasonId: number;
    seasonName: string;
    seasonDescription: string;
}
