﻿export class FormatDomainModel {
    companyId: number;
    countryCode: string;
    regionCode: string;
    formatCode: string;
    formatName: string;
    formatStatus: string;
    formatDescription: string;
    operation?: string;
}