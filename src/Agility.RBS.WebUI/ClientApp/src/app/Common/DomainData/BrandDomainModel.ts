﻿export class BrandDomainModel {
    brandId: number;
    brandName: string;
    brandDescription: string;
}