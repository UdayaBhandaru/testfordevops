export class DivisionDomainModel {
  division: number;
  divName: string;
  divisionDescription: string;
  divisionId: number;
}
