export class PartnerDomainModel {
    partnerType: string;
    partnerId: string;
    partnerDesc: string;
    status: string;
    currency: string;
    exchangeRate: number;
    terms: string;
}
