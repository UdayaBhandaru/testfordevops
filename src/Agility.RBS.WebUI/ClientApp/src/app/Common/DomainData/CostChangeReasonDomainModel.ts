﻿export class CostChangeReasonDomainModel {
    reason: number;

    reasonDescription: string;

    reasonStatus: string;
}