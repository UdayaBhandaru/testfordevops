export class SupplierDetailDomainModel {
  supplier: number;
  supName: string;
  supStatus: string;
  qcInd: string;
  termsId: string;
  freightTerms: string;
  paymentMethod: string;
  shipMethod: string;
  currencyCode: string;
  termsDesc: string;
  freightTermsDesc: string;
  paymentMethodDesc: string;
  shipMethodDesc: string;
  retMinDolAmt: number;
  handlingPct: number;
  retCourier: string;
  exchangeRate: number;
}
