﻿export class OrderItemListCommonModel {
    item?: number;

    unitCost?: number;

    costCurrency: string;

    unitRetail?: number;

    retailCurrency: string;

    quantityExpected?: number;

    weightExpected?: number;

    weightExpUom: string;

    isExpiry: string;
}