﻿export class ClassDomainModel {
    dept: number;
    class: number;
    className: string;
    classDescription: string;
}