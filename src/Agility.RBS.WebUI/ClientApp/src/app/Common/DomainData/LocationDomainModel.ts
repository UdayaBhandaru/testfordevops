﻿export class LocationDomainModel {
    companyId: number;
    countryCode?: string;
    regionCode?: string;
    formatCode?: string;
    locationId?: number;
    locationName: string;
    locationDescription: string;
    locType: string;
}