﻿export class UserDomainModel {
    userId: string;

    userName: string;

    email: string;

    reportingMgrId: string;

    reportingMgrName: string;

    status: string;
}
