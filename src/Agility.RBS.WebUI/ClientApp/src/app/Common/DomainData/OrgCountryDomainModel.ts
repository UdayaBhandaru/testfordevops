export class OrgCountryDomainModel {
    companyId: number;
    countryCode: string;
    countryName: string;
    countryStatus: string;
}
