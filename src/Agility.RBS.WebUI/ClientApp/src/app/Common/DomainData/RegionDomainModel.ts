export class RegionDomainModel {    
  region: string;
  regionName: string;
  regionDescription: string;  
  companyId: number;
  countryCode: string;
  regionCode: string;
  regionStatus: string;
}
