export class SupplierObigationDomainModel {
  supplier: number;
  supName: string;
  supStatus: string;
  currencyCode: string;
  exchangeRate: number;
  terms: string;
}
