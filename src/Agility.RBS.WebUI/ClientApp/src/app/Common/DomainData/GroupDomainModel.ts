export class GroupDomainModel {
  division: number;
  groupNo: number;
  groupName: string;
  groupDescription: string;
}
