export class TransShipmentDomainModel {
  shipId?: number;
  shipVesselId: string;
  shipVoyageFltId: string;
  shipEstDeptDate: Date;
}
