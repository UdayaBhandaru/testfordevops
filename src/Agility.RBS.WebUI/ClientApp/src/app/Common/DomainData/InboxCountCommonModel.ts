﻿export class InboxCountCommonModel {
    pendingCnt?: number;

    snoozedCnt?: number;

    completedCnt?: number;

    sentCnt?: number;

    userID: string;

    percentCnt?: number;

    duedateCnt?: number;

    highPriorityCnt?: number;

    draftCnt?: number;
}