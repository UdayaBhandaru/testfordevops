export class SupplierDomainModel {
  supplier: number;
  supName: string;
  supNameDesc: string;
  supStatus: string;
  contactName: string;
  contactPhone: string;
  contactFax: string;
  contactEmail: string;
  rebateDiscount: number;
}
