﻿export class PromotionDomainModel {
    promoId: number;
    name: string;
    promotionType: string;
    state: string;
}