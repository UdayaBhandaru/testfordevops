﻿export class LanguageDomainModel {
    languageID: number;

    languageDesc: string;

    languageStatus: string;
}
