export class CompanyHeadDomainModel {
    companyId: number;
    companyName: string;
    companyNameDesc: string;
}
