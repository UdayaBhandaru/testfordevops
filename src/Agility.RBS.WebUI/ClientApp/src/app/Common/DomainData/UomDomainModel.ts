﻿export class UomDomainModel {
    uomCode: string;
    uomClass: string;
    description: string;
    uomDescription: string;
    status: string;
}