export class ComponentTraitDomainModel {
  compId: string;
  compDesc: string;
  programPhase: string;
  programMessage: string;
  perror: string;
}
