export class VatRegionDomainModel {
    vatRegionCode: number;
    vatRegionName: string;
    vatRegionStatus: string;
    vatRegionDescription: string;
}
