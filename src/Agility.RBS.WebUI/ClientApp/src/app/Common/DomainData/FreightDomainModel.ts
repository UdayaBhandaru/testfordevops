﻿export class FreightDomainModel {
    freightTermsId: string;

    freightTermsName: string;

    freightTermsDesc: string;

    freightStatus: string;
}
