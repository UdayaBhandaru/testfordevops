﻿export class DeptBuyerDomainModel {
    deptId: number;
    deptDesc: string;
    deptStatus: string;
    buyerId: string;
    buyerName: string;
    deptDescription: string;
}