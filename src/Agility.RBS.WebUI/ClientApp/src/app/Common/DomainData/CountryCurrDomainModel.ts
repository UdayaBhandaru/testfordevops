export class CountryCurrDomainModel {
  countryCode: string;
  countryName: string;
  currencyCode: string;
  exchangeRate: number;
}
