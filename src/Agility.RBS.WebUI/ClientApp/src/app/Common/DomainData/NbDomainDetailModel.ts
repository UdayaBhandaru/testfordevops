export class NbDomainDetailModel {
  codeType: string;
  code: string;
  codeDesc: string;
  codeStatus: string;
}
