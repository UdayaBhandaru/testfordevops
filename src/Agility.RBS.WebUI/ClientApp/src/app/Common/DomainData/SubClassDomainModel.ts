﻿export class SubClassDomainModel {
    dept: number;
    class: number;
    subclass: number;
    subName: string;
    subClassDescription?: string;
}