export class RpmCodeDomainModel {
  codeId: number;
  codeType: number;
  code: string;
  description: string;
}
