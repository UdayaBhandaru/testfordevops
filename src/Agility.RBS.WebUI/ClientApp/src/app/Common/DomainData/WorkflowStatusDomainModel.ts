﻿export class WorkflowStatusDomainModel {
     id: string;
     name: string;
     status: string;
}