﻿import { ErrorHandler, Injectable, Injector } from "@angular/core";
import { SharedService } from "../Common/SharedService";
import { CommonService } from "@agility/frameworkcore";

@Injectable()
export class RbErrorHandler implements ErrorHandler {
    constructor(public injector: Injector) {
    }
    handleError(error: any): void {
        const commonService: CommonService = this.injector.get(CommonService);
        // your custom error handling logic
        commonService.showAlert(error, null, 5000);
    }
}