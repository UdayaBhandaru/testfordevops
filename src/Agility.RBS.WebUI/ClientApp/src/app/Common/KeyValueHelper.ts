﻿export class KeyValueHelper {
    public key: string;
    public value: string;
    public field: string;
    public domainfield: string;

    constructor(_key: string, _value: string, _field: string, _domainfield: string ) {
        this.key = _key;
        this.value = _value;
        this.field = _field;
        this.domainfield = _domainfield;
    }

}
