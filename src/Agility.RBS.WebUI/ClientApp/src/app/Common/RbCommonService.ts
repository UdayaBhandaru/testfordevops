import { Injectable } from "@angular/core";
import { Subject } from "rxjs";
import { FormGroup, FormControl, Validators, FormBuilder } from "@angular/forms";
import { MessageType } from "@agility/frameworkcore";
import { GridOptions } from "ag-grid-community";
import { SharedService } from "../Common/SharedService";
import { RbButton } from "./controls/RbControlModels";
import { ENTER_SELECTOR } from '@angular/animations/browser/src/util';

@Injectable()
export class RbCommonService {
    public _saveAnnouncedSource = new Subject<string>();
    public _saveContinueAnnouncedSource = new Subject<string>();
    public _resetAnnouncedSource = new Subject<string>();
    public _closeAnnouncedSource = new Subject<string>();
    public _previousAnnouncedSource = new Subject<string>();
    public _nextAnnouncedSource = new Subject<string>();
    public _itemCodeSource = new Subject<number>();
    public _submitAnnouncedSource = new Subject<string>();
    public _needInfoAnnouncedSource = new Subject<string>();
    public _rejectAnnouncedSource = new Subject<string>();
    public _discardAnnouncedSource = new Subject<string>();

    priorityDropDownData = [{ key: "H", value: "High" }, { key: "M", value: "Medium" }, { key: "L", value: "Low" }];
    gridOptions: GridOptions = {};
    actionType: string;

    constructor(public sharedService: SharedService) {

    }

    saveAnnounced$ = this._saveAnnouncedSource.asObservable();
    saveContinueAnnounced$ = this._saveContinueAnnouncedSource.asObservable();
    resetAnnounced$ = this._resetAnnouncedSource.asObservable();
    closeAnnounced$ = this._closeAnnouncedSource.asObservable();
    previousAnnounced$ = this._previousAnnouncedSource.asObservable();
    nextAnnounced$ = this._nextAnnouncedSource.asObservable();
    itemCodeAnnounced$ = this._itemCodeSource.asObservable();
    submitAnnounced$ = this._submitAnnouncedSource.asObservable();
    needInfoAnnounced$ = this._needInfoAnnouncedSource.asObservable();
    rejectAnnounced$ = this._rejectAnnouncedSource.asObservable();
    discardAnnounced$ = this._discardAnnouncedSource.asObservable();
    announceSaveContinue(name: string): void {
        this._saveContinueAnnouncedSource.next(name);
    }

    announceSave(name: string): void {
        this._saveAnnouncedSource.next(name);
    }

    announceReset(name: string): void {
        this._resetAnnouncedSource.next(name);
    }

    announceClose(name: string): void {
        this._closeAnnouncedSource.next(name);
    }

    announcePrevious(name: string): void {
        this._previousAnnouncedSource.next(name);
    }

    announceNext(name: string): void {
        this._nextAnnouncedSource.next(name);
    }

    announceItemCode(itemCode: number): void {
        this._itemCodeSource.next(itemCode);
    }

    announceSubmit(name: string): void {
        this._submitAnnouncedSource.next(name);
    }

    announceNeedInfo(name: string): void {
        this._needInfoAnnouncedSource.next(name);
    }

    announceReject(name: string): void {
        this._rejectAnnouncedSource.next(name);
    }
    announceDiscard(name: string): void {
        this._discardAnnouncedSource.next(name);
    }

    isFirstColumn(params: any): boolean {
        let displayedColumns: any = params.columnApi.getAllDisplayedColumns();
        let thisIsFirstColumn: boolean = displayedColumns[0] === params.column;
        return thisIsFirstColumn;
    }

    deleteRecord(data: any[], deletedData: any[], api: any, index: number): void {
        if (data[index] && data[index].createdBy) {
            data[index]["operation"] = "D";
            deletedData.push(data[index]);
        }
        data.splice(index, 1);
        api.setRowData(data);
    }

    save(that: any, saveOnly: boolean, controlName: string, reditectURL: string, localizationData: string, id?: string | number): void {
        that.serviceDocument.dataProfile.profileForm.controls[controlName] = new FormControl();
        if (that.deletedRows.length > 0) {
            that.deletedRows.forEach(x => that.data.push(x));
        }
        that.serviceDocument.dataProfile.profileForm.controls[controlName].setValue(that.data);
        that.service.save().subscribe(() => {
            that.canNavigate = true;
            if (that.service.serviceDocument.result.type === MessageType.success) {
                that.sharedService.saveForm(localizationData).subscribe(() => {
                    if (saveOnly) {
                        that.router.navigate([this.sharedService.previousUrl], { skipLocationChange: true });
                    } else {
                      if (that.PageTitle === "TransferDetails") {
                        that.router.navigate(["/Transfer/List"], { skipLocationChange: true, queryParams: { id: reditectURL + (id ? id : that.item) } });
                      } else {
                        that.router.navigate(["/Blank"], { skipLocationChange: true, queryParams: { id: reditectURL + (id ? id : that.item) } });
                      }
                    }
                });
            } else {
                that.sharedService.errorForm(that.service.serviceDocument.result.innerException);
            }
        });
    }

    close(that: any, recordCount: number): void {
        that.messageResult.message = `<div>All your changes will be lost if you don't save them.</div>
                                    <div>Click on “Save” to Keep the changes.</div>
                                    <div>Click on “Don't save” to proceed further without saving changes.</div>
                                    <div>Click on “Cancel” to stay on the current page.</div>`;
        if (that.data.length !== recordCount) {
            that.dialogRef = that.dialogService.openMessageDialog("Warning", that.messageResult, [new RbButton("", "Cancel", "alertCancel")
                , new RbButton("", "Don't Save", "alertdontsave")
                , new RbButton("", "Save", "alertSave")], "37%", "", "iconsdontsave retailsNavIcons", "dontsaveAlert", "dontsaveAlertTitle");
            that.dialogRef.componentInstance.click.subscribe(
                btnName => {
                    if (btnName === "Cancel") {
                        that.dialogRef.close();
                    }
                    if (btnName === "Don't Save") {
                        that.canNavigate = true;
                        that.dialogRef.close();
                        that.router.navigate([this.sharedService.previousUrl], { skipLocationChange: true });
                    }
                    if (btnName === "Save") {
                        that.canNavigate = true;
                        that.dialogRef.close();
                        that.save(true);
                        // that.router.navigate([this.sharedService.previousUrl], { skipLocationChange: true });
                    }
                });

        } else {
            if (that.PageTitle == "Order") {
                that.router.navigate(["/Order/List"], { skipLocationChange: true });
            } else {
                that.router.navigate([this.sharedService.previousUrl], { skipLocationChange: true });
            }
        }
    }
}
