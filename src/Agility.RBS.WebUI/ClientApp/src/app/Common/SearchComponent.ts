import { Component, Input, Output, OnInit, ChangeDetectorRef, EventEmitter } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { SharedService } from "./SharedService";
import { KeyValueHelper } from "./KeyValueHelper";
import { MessageType } from "@agility/frameworkcore";
import { LoaderService } from "./LoaderService";
import { DatePipe } from "@angular/common";
import { FormGroup, ValidationErrors } from "@angular/forms"

@Component({
  selector: "rbs-search-panel",
  templateUrl: "./SearchComponent.html",
  styleUrls: ['./SearchComponent.scss']
})
export class SearchComponent implements OnInit {
  @Input() service: any;
  @Input() hidden: boolean;
  @Input() componentParentName: any;
  @Input() serviceDocument;
  @Input() model: any;
  @Input() redirectUrl: any;
  @Input() navigate: boolean = true;
  @Input() showAdd: boolean = true;
  @Input() additionalSearchParams: any;
  @Input() restrictSearch: boolean = false;
  @Input() checkValidation: boolean = false;
  @Output() searchCriteria: EventEmitter<any> = new EventEmitter();
  @Output() addEmitter: EventEmitter<any> = new EventEmitter();
  @Output() resetEvent: EventEmitter<any> = new EventEmitter();
  @Output() validationEvent: EventEmitter<any> = new EventEmitter();
  @Input() formGroup: FormGroup;
  sIndex: number;
  showSearchCriteria: boolean = true;
  arChipsDataSearch: KeyValueHelper[] = [];
  public dateColumns: string[] = [];
  validForm: boolean = true;

  constructor(private ref: ChangeDetectorRef, private router: Router, private sharedService: SharedService
    , private loaderService: LoaderService, private datePipe: DatePipe) {
  }

  ngOnInit(): void {
    if (this.sharedService.searchData) {
      this.arChipsDataSearch = [];
      this.search();
    }
  }

  public search(): void {
    if (this.checkValidation) {
      this.checkValidationFailure();
    }
    if (!this.restrictSearch) {
      setTimeout(() => {
        this.loaderService.display(true);
        delete this.sharedService.searchData;
        this.service.search(this.additionalSearchParams).subscribe((response: any) => {
          if (this.service.serviceDocument.result.type === MessageType.success) {
            this.serviceDocument = this.service.serviceDocument;
            this.sIndex = this.getFilteredColumns().map(key => this.serviceDocument.dataProfile.profileForm.value[key])
              .filter((val: any) => { return val !== "" && val != null; }).length;
            this.sIndex = this.sIndex > 4 ? 5 : 4;
            this.searchCriteria.emit(false);
            this.showSearchCriteria = false;
            this.arChipsDataSearch = this.getdataforchips();
          } else {
            this.sharedService.showSTDialog(this.service.serviceDocument.result.innerException
              , this.service.serviceDocument.result.stackTrace || response.model.StackTraceString);
          }
          this.loaderService.display(false);
        }, (error: string) => {
          this.loaderService.display(false);
        });
      }, 2);
    } else {
      this.searchCriteria.emit("search");
    }
  }

  reset(): void {
    this.service.newModel(this.model);
    this.ref.detectChanges();
    delete this.sharedService.searchData;
    this.serviceDocument = this.service.serviceDocument;
    delete this.serviceDocument.dataProfile.dataList;
    if (this.resetEvent.observers.length > 0) {
      this.resetEvent.emit();
    }
  }

  add($event: MouseEvent): void {
    $event.preventDefault();
    this.sharedService.deleteAllToolBarData();
    this.sharedService.setToolbarSave(true);
    if (this.navigate === true) {
      this.router.navigate(["/" + this.redirectUrl + "/New/"], { skipLocationChange: true });
    } else {
      this.addEmitter.emit();
    }
  }

  delete($event: MouseEvent, key: string): void {
    $event.preventDefault();
    this.serviceDocument.dataProfile.profileForm.controls[key].setValue(null);
    this.serviceDocument.dataProfile.dataModel[key] = null;
    this.search();
    this.sIndex = 200;
  }

  more($event: MouseEvent): void {
    $event.preventDefault();
    this.sIndex = 200;
  }

  getArray(object: any): any[] {
    let obj: any[] = [];
    for (let key in object) {
      if (object[key] != null && object[key] !== "") {
        obj.push({ key: key, value: object[key] });
      }
    }
    return obj;
  }

  addSearch($event: MouseEvent): void {
    $event.preventDefault();
    this.searchCriteria.emit(true);
    this.showSearchCriteria = true;
  }

  back($event: MouseEvent): void {
    $event.preventDefault();
    delete this.serviceDocument.dataProfile.dataList;
    this.searchCriteria.emit(true);
    this.showSearchCriteria = true;
  }

  getdataforchips(): any[] {
    let chipobject: any = { key: String, value: String };
    let arChips: KeyValueHelper[] = [];
    let arChipsMetaData: KeyValueHelper[] = [];

    arChipsMetaData.push(new KeyValueHelper("companyId", "company", "companyName", "companyId"));
    arChipsMetaData.push(new KeyValueHelper("seasonId", "season", "seasonName", "seasonId"));
    arChipsMetaData.push(new KeyValueHelper("divisionId", "division", "divisionDescription", "division"));
    arChipsMetaData.push(new KeyValueHelper("groupId", "group", "groupDesc", "groupId"));
    arChipsMetaData.push(new KeyValueHelper("deptId", "department", "deptDesc", "deptId"));
    arChipsMetaData.push(new KeyValueHelper("classId", "class", "classDesc", "classId"));
    arChipsMetaData.push(new KeyValueHelper("subClassId", "subClass", "subClassDesc", "subClassId"));
    arChipsMetaData.push(new KeyValueHelper("roleLevel", "rolelevel", "rolelevel", "roleLevel"));
    arChipsMetaData.push(new KeyValueHelper("roleId", "roleId", "name", "id"));
    arChipsMetaData.push(new KeyValueHelper("countryCode", "country", "countryName", "countryCode"));
    arChipsMetaData.push(new KeyValueHelper("categoryId", "category", "categoryDesc", "categoryId"));
    arChipsMetaData.push(new KeyValueHelper("regionCode", "region", "regionName", "regionCode"));
    arChipsMetaData.push(new KeyValueHelper("vatRegionCode", "vatRegionCode", "vatRegionName", "vatRegionCode"));
    arChipsMetaData.push(new KeyValueHelper("uomClass", "uomClass", "uomClassDescription", "uomClass"));
    arChipsMetaData.push(new KeyValueHelper("fromUom", "uomFrom", "uomDescription", "uomCode"));
    arChipsMetaData.push(new KeyValueHelper("toUom", "uomFrom", "uomDescription", "uomCode"));
    arChipsMetaData.push(new KeyValueHelper("location", "location", "locationDescription", "locationId"));
    arChipsMetaData.push(new KeyValueHelper("loc", "location", "locationDescription", "locationId"));
    arChipsMetaData.push(new KeyValueHelper("locationId", "priceLocation", "name", "id"));
    arChipsMetaData.push(new KeyValueHelper("priceZone", "priceZone", "name", "id"));
    arChipsMetaData.push(new KeyValueHelper("costZone", "costZone", "name", "id"));
    arChipsMetaData.push(new KeyValueHelper("users", "users", "userName", "userId"));
    arChipsMetaData.push(new KeyValueHelper("changeLevelValue", "changeLevelValue", "name", "id"));
    arChipsMetaData.push(new KeyValueHelper("brandId", "brand", "name", "id"));
    arChipsMetaData.push(new KeyValueHelper("orgLevelValue", "orgLevelValueData", "description", "id"));
    arChipsMetaData.push(new KeyValueHelper("merchLevelValue", "merchLevelValueData", "description", "id"));
    arChipsMetaData.push(new KeyValueHelper("costZonelocationId", "costZoneLocDrdDomainData", "locationName", "locationId"));
    arChipsMetaData.push(new KeyValueHelper("priceZonelocationId", "priceZoneLocDrdDomainData", "locationName", "locationId"));
    arChipsMetaData.push(new KeyValueHelper("zoneGroupId", "costZoneGrpDrdDomainData", "name", "id"));
    arChipsMetaData.push(new KeyValueHelper("spaceOrgId", "spaceOrgId", "name", "id"));
    arChipsMetaData.push(new KeyValueHelper("usersRoleId", "userRolesData", "name", "id"));
    arChipsMetaData.push(new KeyValueHelper("usersLocationId", "userLocData", "locationDescription", "locationId"));
    arChipsMetaData.push(new KeyValueHelper("countryCode", "userCountryData", "countryName", "countryCode"));
    arChipsMetaData.push(new KeyValueHelper("regionCode", "userRegionData", "regionName", "regionCode"));
    arChipsMetaData.push(new KeyValueHelper("formatCode", "formatData", "formatDescription", "formatCode"));
    arChipsMetaData.push(new KeyValueHelper("supplierId", "supplier", "supNameDesc", "supplier"));
    arChipsMetaData.push(new KeyValueHelper("supplier", "supplier", "supNameDesc", "supplier"));
    arChipsMetaData.push(new KeyValueHelper("formatCode", "userFormatData", "formatName", "formatCode"));

    arChipsMetaData.push(new KeyValueHelper("receivingLocationId", "location", "locationDescription", "locationId"));
    arChipsMetaData.push(new KeyValueHelper("createdBy", "users", "userName", "email"));
    arChipsMetaData.push(new KeyValueHelper("supplierType", "supplierTypesData", "name", "id"));
    arChipsMetaData.push(new KeyValueHelper("statusCode", "status", "codeDesc", "code"));
    arChipsMetaData.push(new KeyValueHelper("toLocName", "location", "locationName", "locationId"));
    arChipsMetaData.push(new KeyValueHelper("supplier", "supplier", "supName", "supplier"));
    arChipsMetaData.push(new KeyValueHelper("reason", "reason", "reasonDescription", "reason"));

    arChipsMetaData.push(new KeyValueHelper("locationId", "locationId", "location", "locationId"));
    arChipsMetaData.push(new KeyValueHelper("supplier", "supNameDesc", "supplier", "supplier"));
    arChipsMetaData.push(new KeyValueHelper("obligationLevel", "obligation", "codeDesc", "code"));
    arChipsMetaData.push(new KeyValueHelper("partnerType", "partnerType", "codeDesc", "code"));
    arChipsMetaData.push(new KeyValueHelper("groupId", "groupDescription", "securityGroups", "groupId"));
    arChipsMetaData.push(new KeyValueHelper("code", "codeDesc", "merchLevel", "merchHierLevels"));
    arChipsMetaData.push(new KeyValueHelper("id", "description", "merchLevelValue", "merchHierValues"));
    arChipsMetaData.push(new KeyValueHelper("errorType", "errorTypeList", "codeDesc", "code"));
    arChipsMetaData.push(new KeyValueHelper("language", "languagesList", "languageDesc", "languageID"));


    let oParent: any = this.serviceDocument.dataProfile.dataModel;
    if (oParent == null || oParent === undefined) {
      return arChips;
    }
    let filteredColmns: string[] = this.getFilteredColumns();

    let cFieldName: string;
    let cFieldValue: string;
    let cPropertyName: string;
    let domainfield: string;

    // list the children element names
    for (let i: number = 0; i < filteredColmns.length; i++) {
      // display the name and value of the child field object;
      cFieldName = filteredColmns[i]; // get the name of the field object;
      let cfMeta: KeyValueHelper = arChipsMetaData.find(x => (x.value != null && x.key === cFieldName));
      if (cfMeta != null && cfMeta !== undefined) {
        cFieldValue = cfMeta.value;
        cPropertyName = cfMeta.field;
        domainfield = cfMeta.domainfield;
        this.getChips(cFieldValue, cFieldName, cPropertyName, domainfield, arChips, oParent);
      } else {
        this.setValuetoChips(cFieldName, arChips, oParent);
      }
    }

    return arChips;
  }

  getFilteredColumns(): string[] {
    let oParent: any = this.serviceDocument.dataProfile.dataModel;
    let aChildren: string[] = Object.keys(oParent); // place child fields in an array;
    let ignoredColms: string[] = ["operation", "modifiedBy", "createdDate", "modifiedDate", "deletedInd", "langId", "programPhase"
      , "programMessage", "error", "operation", "tableName", "uomClassDescription", "uomDescription", "domainStatusDesc"
      , "domainDetails", "startRow", "endRow", "totalRows", "sortModel", "listStatusDesc", "itemListDetails", "workflowInstance"
      , "startRow", "endRow", "totalRows", "sortModel", "isWorkflowCompleted", "costZoneLocList", "locationRemovedData"
      , "priceZoneLocList", "rolesList", "rolesRemovedData", "companiesList", "countriesList", "formatsList", "locationsList"
      , "regionsList", "userOrgHierarchyModel", "removedUserCompanyModel", "removedUserCountryModel", "removedUserFormatModel"
      , "removedUserRegionModel", "removedUserLocationModel", "bulkItemDetails", "forecastDetails", "priceChangeDetails"
      , "spaceOrgData", "spaceLevelData", "workflowForm", "orgHierarchy", "promoDetails", "formatLocList", "itemDetails", "itemUdaValues"];
    let filteredColmns: string[] = aChildren.filter((element: string) => {
      return ignoredColms.indexOf(element) === -1;
    });
    return filteredColmns;
  }

  getChips(domainData: string, fieldName: string, propertyName: string, domainfield: string, arChips: KeyValueHelper[], oParent: any): void {
    let cFieldValue: string;
    let myObj: any;
    let domaindatalist: any = this.sharedService.domainData[domainData];
    cFieldValue = this.serviceDocument.dataProfile.dataModel[fieldName]; // get the value fo the field object;

    if (domaindatalist !== undefined && domaindatalist != null && cFieldValue != null) {
      myObj = domaindatalist.find(x => x[domainfield] === cFieldValue);
      let fieldValue: string;
      if (myObj !== undefined && myObj != null) {
        fieldValue = myObj[propertyName];
        let kv: KeyValueHelper = new KeyValueHelper(fieldName, fieldValue, "", "");
        arChips.push(kv);
      } else {
        this.setValuetoChips(fieldName, arChips, oParent);
      }
    } else {
      this.setValuetoChips(fieldName, arChips, oParent);
    }
  }

  setValuetoChips(fieldName: string, arChips: KeyValueHelper[], oParent: any): void {
    let cFieldValue: any = oParent[fieldName]; // get the value fo the field object;

    let cfMeta: string = this.dateColumns.find(x => x === fieldName);
    if (cfMeta != null && cfMeta !== undefined && cFieldValue != null) {
      let dt: Date;
      dt = new Date(cFieldValue);
      cFieldValue = this.datePipe.transform(dt, "dd-MMM-yyyy");
    }

    if (cFieldValue != null) {
      let kv: KeyValueHelper = new KeyValueHelper(fieldName, cFieldValue, "", "");
      arChips.push(kv);
    }
  }

  ngAfterViewChecked(): void {
    this.ref.detectChanges();
  }

  checkValidationFailure() {
    this.validationEvent.emit();
    this.validForm = true;
    Object.keys(this.formGroup.controls).forEach(key => {

      const controlErrors: ValidationErrors = this.formGroup.get(key).errors;
      if (controlErrors != null && controlErrors.error != null) {
        this.validForm = false;
      }
    });
  }
}
