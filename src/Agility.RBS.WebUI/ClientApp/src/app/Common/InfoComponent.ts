import { Component } from "@angular/core";
import { DatePipe } from "@angular/common";
import { FormGroup, ReactiveFormsModule } from "@angular/forms";
import { SharedService } from "./SharedService";
import { Subscription } from "rxjs";
import { CommonService, FormMode } from "@agility/frameworkcore";
import { MatDialog, MatDialogRef } from "@angular/material";

@Component({
    selector: "info",
  templateUrl: "./InfoComponent.html",
  styleUrls: ['./InfoComponent.scss']
})
export class InfoComponent {
    infoGroup: FormGroup;
    sharedSubscription: Subscription;
    constructor(private service: SharedService, private commonService: CommonService, public dialog: MatDialog, private datePipe: DatePipe) {
        this.infoGroup = this.commonService.getFormGroup(this.service.editObject, FormMode.View);
        var date: Date = new Date(this.infoGroup.controls["createdDate"].value);
        this.infoGroup.controls["createdDate"].setValue(this.datePipe.transform(date, "dd-MMM-yyyy"));
        date = new Date(this.infoGroup.controls["modifiedDate"].value);
        this.infoGroup.controls["modifiedDate"].setValue(this.datePipe.transform(date, "dd-MMM-yyyy"));
    }

    close(): void {
        this.dialog.closeAll();
    }
}
