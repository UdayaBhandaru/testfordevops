﻿import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

@Injectable()
export class LoaderService {
    public status: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    public spinner = { mode: "indeterminate", color: "accent", value: 0, diameter: 100, strokeWidth: 9 };
    display(value: boolean): void {
        this.status.next(value);
    }
}