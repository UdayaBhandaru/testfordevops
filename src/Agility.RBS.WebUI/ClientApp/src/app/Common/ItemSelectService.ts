import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument, WorkflowAction } from "@agility/frameworkcore";
import { ItemSelectModel } from "./ItemSelectModel";
import { HttpHelperService } from "./HttpHelperService";

@Injectable()
export class ItemSelectService {
  serviceDocument: ServiceDocument<ItemSelectModel> = new ServiceDocument<ItemSelectModel>();

  constructor(private httpHelperService: HttpHelperService) { }

  newModel(model: ItemSelectModel): ServiceDocument<ItemSelectModel> {
    this.serviceDocument.newModel(model);
    return this.serviceDocument;
  }

  list(): Observable<ServiceDocument<ItemSelectModel>> {
    return this.serviceDocument.list("/api/ItemSelect/List");
  }

  search(): Observable<ServiceDocument<ItemSelectModel>> {
    return this.serviceDocument.search("/api/ItemSelect/Search");
  }

  getItemsFromAPI(apiUrl: string, itemSelectModel: ItemSelectModel): Observable<any[]> {
    return this.httpHelperService.post(apiUrl, itemSelectModel);
  }

  getItems(apiUrl: string): Observable<any> {
    return this.httpHelperService.get(apiUrl);
  }
}
