﻿export class CommonPrintModel {
    tableName: string;
    columns: string[];
    data: any[];
    moduleSpecificColumns?: any[];
}