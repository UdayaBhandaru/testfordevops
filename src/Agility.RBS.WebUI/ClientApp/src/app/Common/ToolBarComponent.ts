import { Component, Input, Output, EventEmitter, OnInit, ElementRef, ChangeDetectorRef, OnDestroy } from "@angular/core";
import { SharedService } from "../Common/SharedService";
import { MatDialogRef, MatDialog } from "@angular/material";
import { MessageResult, ServiceDocument, WorkflowAction, CommonService } from "@agility/frameworkcore";
import { Router } from "@angular/router";
import { FormGroup } from "@angular/forms";
import { InfoComponent } from "./InfoComponent";
import { DocumentsPopupComponent } from "../Documents/DocumentsPopupComponent";
import { DocumentsService } from "../Documents/DocumentsService";
import { DocumentHeaderModel } from "../Documents/DocumentHeaderModel";
import { DocumentDetailModel } from "../Documents/DocumentDetailModel";
import { Subscription } from "rxjs";
import { RbDialog, RbButton } from "../Common/controls/RbControlModels";
import { RbDialogService } from "../Common/controls/RbDialogService";

@Component({
  selector: "toolBar",
  templateUrl: "./ToolBarComponent.html",
  styleUrls: ['./ToolbarComponent.scss']
})
export class ToolBarComponent implements OnInit, OnDestroy {
  @Output() save = new EventEmitter();
  @Output() reset = new EventEmitter();
  @Output() saveAndContinue = new EventEmitter();
  @Output() firstRecord = new EventEmitter();
  @Output() previousRecord = new EventEmitter();
  @Output() nextRecord = new EventEmitter();
  @Output() lastRecord = new EventEmitter();
  @Output() close = new EventEmitter();
  @Input() closeRedirectUrl: string;
  @Input() objectValue: string;
  @Input() objectName: string;
  @Input() formGroup: FormGroup;
  @Input() service: any;
  @Input() isItemModule: boolean;
  @Input() isSupplierModule: boolean;
  @Output() notifyPrint = new EventEmitter();
  @Input() isWrkflowEnbled: boolean;
  serviceDocSubscription: Subscription;
  public editMode: boolean = false;
  rbDialog: RbDialog = new RbDialog();
  dialogRef: MatDialogRef<any>;
  private messageResult: MessageResult = new MessageResult();
  status: string;
  result: string;
  documentHeaderDataList: DocumentHeaderModel[] = [];
  documentDetailsDataList: DocumentDetailModel[] = [];
  isEnableDocumentModule: boolean;
  wfServiceDocument: ServiceDocument<any>;
  wfAction: WorkflowAction[];
  wfTransitionClaim: string = null;
  isHaveSavePermission = true;

  constructor(private commonService: CommonService, public _sharedService: SharedService, private _dialogService: RbDialogService,
    private _router: Router, private _documentService: DocumentsService, private ref: ChangeDetectorRef) {
    this.editMode = _sharedService.editMode;
  }

  ngOnInit(): void {
    this.isEnableDocumentModule = (this.isItemModule || this.isSupplierModule);
    if (this.isWrkflowEnbled) {
      this.serviceDocSubscription = this._sharedService.wfPermitionsAnnounced$.subscribe((sDoc: ServiceDocument<any>) => {
        if (sDoc) {
          this.wfServiceDocument = sDoc;
          this.wfAction = this.wfServiceDocument.dataProfile.actionService.allowedActions.filter(a => {
            return a["transitionType"] === "ST" && a.actionName === "Save" || a["transitionType"] === "NT";
          });
          if (this.wfAction && this.wfAction.length > 0) {
            this.isHaveSavePermission = true;
          } else {
            this.isHaveSavePermission = false;

            this.commonService.showAlert("Sorry!!.You dont have permision to add/save");
          }
        }
      });

    }
  }

  ngOnDestroy(): void {
    if (this.serviceDocSubscription) {
      this.serviceDocSubscription.unsubscribe();
    }
    this.save.unsubscribe();
    this.reset.unsubscribe();
    this.saveAndContinue.unsubscribe();
    this.firstRecord.unsubscribe();
    this.previousRecord.unsubscribe();
    this.nextRecord.unsubscribe();
    this.lastRecord.unsubscribe();
    this.close.unsubscribe();
    this.notifyPrint.unsubscribe();
  }

  onSave(): void {
    this.save.emit();
  }

  onReset(): void {
    this.reset.emit();
  }

  Add(wfTransitionClaim: string): void {
    this.wfServiceDocument.dataProfile.actionService.currentActionId = wfTransitionClaim;
    this.onSave();
  }

  onSaveAndContinue(): void {
    this.saveAndContinue.emit();
  }
  onNewSearch(): void {
    if (this._sharedService !== null) {
      delete this._sharedService.searchData;
    }
    this._router.navigate([this.closeRedirectUrl], { skipLocationChange: true });
  }
  onFirstRecord(): void {
    this.commonRecordMoment(this.firstRecordCommon);
  }
  onPrevious(): void {

    this.commonRecordMoment(this.previousCommon);
  }
  onNext(): void {
    this.commonRecordMoment(this.nextCommon);
  }
  onLastRecord(): void {
    this.commonRecordMoment(this.lastRecordCommon);
  }
  onClose(): void {
    this.rbDialog = new RbDialog();
    this.messageResult.message = `<div>All your changes will be lost if you don't save them.</div>
                                    <div>Click on “Save” to Keep the changes.</div>
                                    <div>Click on “Don't save” to proceed further without saving changes.</div>
                                    <div>Click on “Cancel” to stay on the current page.</div>`;
    if (this.isItemModule) {
      this.close.emit();
    } else {
      if (this.formGroup && this.formGroup.dirty) {
        this.dialogRef = this._dialogService.openMessageDialog("Warning", this.messageResult
          , [new RbButton("", "Cancel", "alertCancel"), new RbButton("", "Don't Save", "alertdontsave")
            , new RbButton("", "Save", "alertSave")], "37%", "", "iconsdontsave retailsNavIcons", "dontsaveAlert", "dontsaveAlertTitle");
        this.dialogRef.componentInstance.click.subscribe(
          btnName => {
            if (btnName === "Cancel") {
              this.dialogRef.close();
            }
            if (btnName === "Don't Save") {
              this.editMode = false;
              delete this._sharedService.indexOfCurrentItem;
              delete this._sharedService.toolbarDataList;
              delete this._sharedService.toolbarEditObject;
              delete this._sharedService.editObject;
              delete this._sharedService.editMode;
              this.formGroup.markAsUntouched();
              this.result = "Don't Save";
              this.dialogRef.close();
              this._router.navigate([this.closeRedirectUrl], { skipLocationChange: true });
            }
            if (btnName === "Save") {
              this.dialogRef.close();
              this.save.emit();
            }
          });
      } else {
        this._router.navigate([this.closeRedirectUrl], { skipLocationChange: true });
      }
    }
  }

  getAndSetIndexOfEditObjFromToolBarList(): number {
    return this._sharedService.indexOfCurrentItem = this._sharedService.toolbarDataList.indexOf(this._sharedService.toolbarEditObject);
  }

  onInfo(): void {
    this._sharedService.editObject = this._sharedService.toolbarEditObject;
    this.rbDialog = new RbDialog();
    this.rbDialog.title = "User Details";
    this.rbDialog.height = "45%";
    this.rbDialog.width = "45%";
    this.rbDialog.componentRef = InfoComponent;
    this.dialogRef = this._dialogService.openDialog(this.rbDialog);
  }


  onHelp(): void {
   // this._sharedService.editObject = this._sharedService.toolbarEditObject;
    this.rbDialog = new RbDialog();
    this.rbDialog.title = "RetailBussinessSystem Help Desk";
    this.rbDialog.height = "85%";
    this.rbDialog.width = "85%";
    this.rbDialog.content = "<div>Hello</div>";
    this.dialogRef = this._dialogService.openDialog(this.rbDialog);
  }

  private previousCommon(obj: ToolBarComponent): void {
    if (obj.isItemModule) {
      obj.previousRecord.emit();
      return;
    }
    obj.getAndSetIndexOfEditObjFromToolBarList();
    if (obj._sharedService.indexOfCurrentItem > 0) {
      obj._sharedService.indexOfCurrentItem--;
      obj._sharedService.editObject = obj._sharedService.toolbarEditObject = obj._sharedService.toolbarDataList[obj._sharedService.indexOfCurrentItem];
      obj.previousRecord.emit();
    }
  }

  private lastRecordCommon(obj: ToolBarComponent): void {
    if (obj.getAndSetIndexOfEditObjFromToolBarList() < obj._sharedService.toolbarDataList.length - 1) {
      obj._sharedService.indexOfCurrentItem = obj._sharedService.toolbarDataList.length - 1;
      obj._sharedService.editObject = obj._sharedService.toolbarEditObject = obj._sharedService.toolbarDataList[obj._sharedService.indexOfCurrentItem];
      obj.lastRecord.emit();
    }
  }

  private nextCommon(obj: ToolBarComponent): void {
    if (obj.isItemModule) {
      obj.nextRecord.emit();
      return;
    }
    obj.getAndSetIndexOfEditObjFromToolBarList();
    if (obj._sharedService.indexOfCurrentItem < obj._sharedService.toolbarDataList.length - 1) {
      obj._sharedService.indexOfCurrentItem++;
      obj._sharedService.editObject = obj._sharedService.toolbarEditObject = obj._sharedService.toolbarDataList[obj._sharedService.indexOfCurrentItem];
      obj.nextRecord.emit();
    }
  }

  private firstRecordCommon(obj: ToolBarComponent): void {
    obj._sharedService.indexOfCurrentItem = 0;
    obj._sharedService.editObject = obj._sharedService.toolbarEditObject = obj._sharedService.toolbarDataList[0];
    obj.firstRecord.emit();
  }

  commonRecordMoment(pagFfunction: Function): void {
    this.rbDialog = new RbDialog();
    this.messageResult.message = `<div>All your changes will be lost if you don't save them.</div>
                                    <div>Click on “Save” to Keep the changes.</div>
                                    <div>Click on “Don't save” to proceed further without saving changes.</div>
                                    <div>Click on “Cancel” to stay on the current page.</div>`;
    if (this.formGroup && this.formGroup.dirty) {
      this.dialogRef = this._dialogService.openMessageDialog("Warning", this.messageResult
        , [new RbButton("", "Cancel", "alertCancel"), new RbButton("", "Don't Save", "alertdontsave")
          , new RbButton("", "Save", "alertSave")], "37%", "", "iconsdontsave retailsNavIcons", "dontsaveAlert", "dontsaveAlertTitle");
      this.dialogRef.componentInstance.click.subscribe(
        btnName => {
          if (btnName === "Cancel") {
            this.dialogRef.close();
          }
          if (btnName === "Don't Save") {
            pagFfunction(this);
          }
          if (btnName === "Save") {
            this.dialogRef.afterClosed().subscribe((res: any) => {
              this.saveAndContinue.emit();
            });
          }
        });
    } else {
      pagFfunction(this);
    }
  }

  deleteAllToolBarData(): void {
    delete this._sharedService.indexOfCurrentItem;
    delete this._sharedService.toolbarDataList;
    delete this._sharedService.toolbarEditObject;
    delete this._sharedService.editObject;
    delete this._sharedService.editMode;
  }

  onPrint(): void {
    if (this.notifyPrint.observers.length > 0) {
      this.notifyPrint.emit();
    } else {
      try {
        var toolBarGroupComponentPrintElement: HTMLElement = document.getElementById("toolBarDivid");
        var groupComponentScrollPrintElement: HTMLElement = document.getElementById("printMainComponentDivID");
        toolBarGroupComponentPrintElement.style.visibility = "hidden";
        groupComponentScrollPrintElement.style.overflow = "visible";
        // window.print();
      } catch (exception) {
        exception = "An Error Occurred While Printing";
        this._sharedService.errorForm(exception);
      } finally {
        toolBarGroupComponentPrintElement.style.visibility = "visible";
        groupComponentScrollPrintElement.style.overflow = "scroll";
      }
    }
  }

  onDocumentsClick(): void {
    this.fetchDocumentList();
  }

  fetchDocumentList(): void {
    this._documentService.fetchDocumentList(this.objectName, this.objectValue).subscribe(() => {
      if (this._documentService.serviceDocument.dataProfile.dataList) {
        this.documentHeaderDataList = this._documentService.serviceDocument.dataProfile.dataList;
        this.documentDetailsDataList = this.documentHeaderDataList[0].documentDetails;
      }
      this._sharedService.openPopupWithDataList(
        DocumentsPopupComponent,
        this.documentDetailsDataList,
        "Document Upload",
        this.objectName,
        this.objectValue);
    },
      err => console.log(err)
    );
  }
}
