﻿import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";
import { map, catchError } from 'rxjs/operators';

@Injectable()
export class HttpHelperService {
    constructor(private http: HttpClient) {
    }

    public get(url: string, search?: HttpParams): Observable<any> {
        const requestOptions: any = {
            params: new HttpParams()
        };

        if (search !== undefined) {
            requestOptions.params = search;
        }

        return this.http.get(url, requestOptions)
            .pipe(catchError(this.handleError));
    }

    public post(url: string, body?: any, search?: HttpParams): Observable<any> {
        const options: any = {
            params: new HttpParams()
        };

        if (search !== undefined) {
            options.params = search;
        }
        
        return this.http.post<any>(url, body)
            .pipe(catchError(this.handleError));
    }

    private handleError(error: Response | any): Observable<any> {
        let errMsg: string;
        if (error instanceof Response) {
            const body: any = error.json() || "";
            const err: any = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ""} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}