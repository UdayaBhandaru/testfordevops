

import { SelectionModel } from '@angular/cdk/collections';
import { FlatTreeControl } from '@angular/cdk/tree';
import { Component, Injectable, Input } from '@angular/core';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { BehaviorSubject } from 'rxjs';
import { MatIconModule } from '@angular/material/icon';
import { FxContext } from '@agility/frameworkcore';
import { PromotionService } from '../promo/PromotionService';


/**
 * Node for to-do item
 */
export class RoleNodeModel {
  public nodeId: number;
  public format: number;
  public formatName: string;
  locationId: number;
  locationName: string;
  name: string;
  public level: number;
  public expandable: boolean;
  public parentId: number;
  public childNodes: RoleNodeModel[];


}

export class RoleModel {
  public format: number;
  public formatName: string;
  locationId: number;
  locationName: string;
  name: string;
  public parentId: number;
  public childRoles: RoleModel[];
}



/**
 * The Json object for to-do list data.
 */
//const TREE_DATA = [{ "format": 6, "formatName": "NON Trading Store", "childRoles": [{ "locationId": 96, "locationName": "Head office Merchandise", "locType": null, "programMessage": null, "error": null, "name": "Head office Merchandise" }, { "locationId": 90, "locationName": "Market Vision(TRADING) Inter Company", "locType": null, "programMessage": null, "error": null, "name": "Market Vision(TRADING) Inter Company" }, { "locationId": 91, "locationName": "Bahrain Export", "locType": null, "programMessage": null, "error": null, "name": "Bahrain Export" }, { "locationId": 92, "locationName": "Oman Export", "locType": null, "programMessage": null, "error": null, "name": "Oman Export" }, { "locationId": 93, "locationName": "Lebanon Export", "locType": null, "programMessage": null, "error": null, "name": "Lebanon Export" }, { "locationId": 95, "locationName": "Jordan Export", "locType": null, "programMessage": null, "error": null, "name": "Jordan Export" }], "programMessage": null, "error": null, "name": "NON Trading Store" }, { "format": 1, "formatName": "Large Service Stores\n", "childRoles": [{ "locationId": 13, "locationName": "Salmiya", "locType": null, "programMessage": null, "error": null, "name": "Salmiya" }, { "locationId": 29, "locationName": "Sharq", "locType": null, "programMessage": null, "error": null, "name": "Sharq" }, { "locationId": 38, "locationName": "Al- Kout", "locType": null, "programMessage": null, "error": null, "name": "Al- Kout" }, { "locationId": 16, "locationName": "Hawalli", "locType": null, "programMessage": null, "error": null, "name": "Hawalli" }, { "locationId": 27, "locationName": "Shaab", "locType": null, "programMessage": null, "error": null, "name": "Shaab" }, { "locationId": 45, "locationName": "Boulevard Salmiya", "locType": null, "programMessage": null, "error": null, "name": "Boulevard Salmiya" }], "programMessage": null, "error": null, "name": "Large Service Stores\n" }, { "format": 7, "formatName": "Production Plant", "childRoles": [{ "locationId": 94, "locationName": "Central Bakey Salmiya", "locType": null, "programMessage": null, "error": null, "name": "Central Bakey Salmiya" }, { "locationId": 97, "locationName": "Central Bakery Sharq", "locType": null, "programMessage": null, "error": null, "name": "Central Bakery Sharq" }], "programMessage": null, "error": null, "name": "Production Plant" }, { "format": 2, "formatName": "Small Service Stores\n", "childRoles": [{ "locationId": 12, "locationName": "Fahaheel.", "locType": null, "programMessage": null, "error": null, "name": "Fahaheel." }, { "locationId": 14, "locationName": "Salwa", "locType": null, "programMessage": null, "error": null, "name": "Salwa" }, { "locationId": 15, "locationName": "Ahmadi", "locType": null, "programMessage": null, "error": null, "name": "Ahmadi" }, { "locationId": 23, "locationName": "Jabriya", "locType": null, "programMessage": null, "error": null, "name": "Jabriya" }, { "locationId": 98, "locationName": "Alrai WHS", "locType": null, "programMessage": null, "error": null, "name": "Alrai WHS" }, { "locationId": 25, "locationName": "Promenade", "locType": null, "programMessage": null, "error": null, "name": "Promenade" }], "programMessage": null, "error": null, "name": "Small Service Stores\n" }, { "format": 4, "formatName": "Xpress Stores\n", "childRoles": [{ "locationId": 520, "locationName": "Mina Abdulla 1", "locType": null, "programMessage": null, "error": null, "name": "Mina Abdulla 1" }, { "locationId": 36, "locationName": "Chevron", "locType": null, "programMessage": null, "error": null, "name": "Chevron" }, { "locationId": 521, "locationName": "Mina Abdulla 2", "locType": null, "programMessage": null, "error": null, "name": "Mina Abdulla 2" }, { "locationId": 522, "locationName": "Nuwaiseeb", "locType": null, "programMessage": null, "error": null, "name": "Nuwaiseeb" }, { "locationId": 523, "locationName": "Hilaliya", "locType": null, "programMessage": null, "error": null, "name": "Hilaliya" }, { "locationId": 534, "locationName": "Shuwaikh University", "locType": null, "programMessage": null, "error": null, "name": "Shuwaikh University" }, { "locationId": 535, "locationName": "Masaleh 1", "locType": null, "programMessage": null, "error": null, "name": "Masaleh 1" }, { "locationId": 536, "locationName": "Masaleh 2", "locType": null, "programMessage": null, "error": null, "name": "Masaleh 2" }, { "locationId": 537, "locationName": "Masaleh 3", "locType": null, "programMessage": null, "error": null, "name": "Masaleh 3" }, { "locationId": 538, "locationName": "Kcmcc 1", "locType": null, "programMessage": null, "error": null, "name": "Kcmcc 1" }, { "locationId": 539, "locationName": "Kcmcc 2", "locType": null, "programMessage": null, "error": null, "name": "Kcmcc 2" }, { "locationId": 540, "locationName": "Kcmcc 3", "locType": null, "programMessage": null, "error": null, "name": "Kcmcc 3" }, { "locationId": 541, "locationName": "Saleh Shehab", "locType": null, "programMessage": null, "error": null, "name": "Saleh Shehab" }, { "locationId": 544, "locationName": "Kabd", "locType": null, "programMessage": null, "error": null, "name": "Kabd" }, { "locationId": 545, "locationName": "West Shuaiba", "locType": null, "programMessage": null, "error": null, "name": "West Shuaiba" }, { "locationId": 546, "locationName": "Shaab, Gulf Road", "locType": null, "programMessage": null, "error": null, "name": "Shaab, Gulf Road" }, { "locationId": 547, "locationName": "Farwaniya", "locType": null, "programMessage": null, "error": null, "name": "Farwaniya" }, { "locationId": 566, "locationName": "KOC Mini Market", "locType": null, "programMessage": null, "error": null, "name": "KOC Mini Market" }, { "locationId": 543, "locationName": "West Mishrif", "locType": null, "programMessage": null, "error": null, "name": "West Mishrif" }, { "locationId": 500, "locationName": "XPRESS Depo for RETURNS", "locType": null, "programMessage": null, "error": null, "name": "XPRESS Depo for RETURNS" }, { "locationId": 549, "locationName": "Oula Sharq", "locType": null, "programMessage": null, "error": null, "name": "Oula Sharq" }, { "locationId": 550, "locationName": "Oula Daaeya", "locType": null, "programMessage": null, "error": null, "name": "Oula Daaeya" }, { "locationId": 551, "locationName": "Oula Al Shuhada", "locType": null, "programMessage": null, "error": null, "name": "Oula Al Shuhada" }, { "locationId": 552, "locationName": "Oula Al Quyoon", "locType": null, "programMessage": null, "error": null, "name": "Oula Al Quyoon" }, { "locationId": 565, "locationName": "Masaleh Bneid Algar", "locType": null, "programMessage": null, "error": null, "name": "Masaleh Bneid Algar" }, { "locationId": 548, "locationName": "Kidzania", "locType": null, "programMessage": null, "error": null, "name": "Kidzania" }, { "locationId": 558, "locationName": "Alfa Shuwaikh", "locType": null, "programMessage": null, "error": null, "name": "Alfa Shuwaikh" }, { "locationId": 559, "locationName": "KNPC - DOHA", "locType": null, "programMessage": null, "error": null, "name": "KNPC - DOHA" }, { "locationId": 560, "locationName": "KNPC - GHAZALI REHAB", "locType": null, "programMessage": null, "error": null, "name": "KNPC - GHAZALI REHAB" }, { "locationId": 561, "locationName": "KNPC - WAHA JAHRA", "locType": null, "programMessage": null, "error": null, "name": "KNPC - WAHA JAHRA" }, { "locationId": 562, "locationName": "KNPC - SIDDIQ", "locType": null, "programMessage": null, "error": null, "name": "KNPC - SIDDIQ" }, { "locationId": 563, "locationName": "KNPC - JABER STADIUM", "locType": null, "programMessage": null, "error": null, "name": "KNPC - JABER STADIUM" }, { "locationId": 564, "locationName": "Thatcher Complex", "locType": null, "programMessage": null, "error": null, "name": "Thatcher Complex" }, { "locationId": 553, "locationName": "Alfa Shaab", "locType": null, "programMessage": null, "error": null, "name": "Alfa Shaab" }, { "locationId": 554, "locationName": "Alfa Salmiya", "locType": null, "programMessage": null, "error": null, "name": "Alfa Salmiya" }, { "locationId": 555, "locationName": "Alfa Sabahiya", "locType": null, "programMessage": null, "error": null, "name": "Alfa Sabahiya" }, { "locationId": 556, "locationName": "TSC xpress Equate", "locType": null, "programMessage": null, "error": null, "name": "TSC xpress Equate" }, { "locationId": 557, "locationName": "Benaidar", "locType": null, "programMessage": null, "error": null, "name": "Benaidar" }], "programMessage": null, "error": null, "name": "Xpress Stores\n" }, { "format": 5, "formatName": "Discount Stores\n", "childRoles": [], "programMessage": null, "error": null, "name": "Discount Stores\n" }, { "format": 3, "formatName": "Warehouse Stores\n", "childRoles": [{ "locationId": 20, "locationName": "Shuwaikh", "locType": null, "programMessage": null, "error": null, "name": "Shuwaikh" }, { "locationId": 24, "locationName": "Mangaf", "locType": null, "programMessage": null, "error": null, "name": "Mangaf" }, { "locationId": 28, "locationName": "Jahra", "locType": null, "programMessage": null, "error": null, "name": "Jahra" }, { "locationId": 35, "locationName": "Sulaibiya", "locType": null, "programMessage": null, "error": null, "name": "Sulaibiya" }, { "locationId": 37, "locationName": "Dhajeej", "locType": null, "programMessage": null, "error": null, "name": "Dhajeej" }, { "locationId": 30, "locationName": "Egaila Wholesale", "locType": null, "programMessage": null, "error": null, "name": "Egaila Wholesale" }], "programMessage": null, "error": null, "name": "Warehouse Stores\n" }];

//  var test= {
//  id: {
//    id: "1",
//    name: "India",
//    childRoles: [{ id: "1", name: "pradesh", childRoles: [{ id: "1", name: "Khammam" }, { id: "2", name: "Warangal" }] }, { id: "2", name: "Telangana" }]
//  },
//  name: {
//    id: "2",
//    name: "USA",
//    childRoles: [{ id: "1", name: "Bihar" }, { id: "2", name: "Orrisa" }]
//  },
//};



/**
 * Checklist database, it can build a tree structured Json object.
 * Each node in Json object represents a to-do item or a category.
 * If a node is a category, it has children items and new items can be added under the category.
 */
@Injectable()
export class ChecklistDatabase {
  public dataChange = new BehaviorSubject<RoleModel[]>([]);
  get data(): RoleModel[] { return this.dataChange.value; }

  constructor(private promotionService: PromotionService) {
    this.initialize();
  }
  private initialize() {
    //const data = this.buildFileTree(TREE_DATA, 0);
    //this.dataChange.next(data);
    this.promotionService.GetMerchTreeview().subscribe((roles: RoleModel[]) => {
      const data = this.buildFileTree(roles, 0);
      this.dataChange.next(data);
    });


  }
  private buildFileTree(obj: object, level: number): RoleModel[] {
    return Object.keys(obj).reduce<RoleModel[]>((accumulator, key) => {
      const value = obj[key];
      const node = new RoleModel();
      node.format = value.format;
      node.formatName = value.formatName;
      node.locationId = value.locationId;
      node.locationName = value.locationName;
      node.name = value.name;
      node.parentId = value.parentId;
      if (value.childRoles && value.childRoles.length > 0) {
        node.childRoles = this.buildFileTree(value.childRoles, level + 1);
      }

      return accumulator.concat(node);
    }, []);
  }

}

/**
 * @title Tree with checkboxes
 */
@Component({
  selector: 'treeView',
  templateUrl: 'TreeViewComponent.html',
  styleUrls: ['TreeViewComponent.css'],
})
export class TreeViewComponent {
 //// @Input() treeData: any;
  public nodesData: RoleNodeModel[] = [];
  public checklistSelection = new SelectionModel<RoleNodeModel>(true);
  // private roleHierarchyForm: FormGroup;
  private nodeId: number = 0;
  private previousLevel: number = 0;
  /** Map from nested node to flattened node. This helps us to keep the same object for selection */
  private nestedNodeMap = new Map<RoleModel, RoleNodeModel>();
  public treeControl: FlatTreeControl<RoleNodeModel>;
  private treeFlattener: MatTreeFlattener<RoleModel, RoleNodeModel>;
  public dataSource: MatTreeFlatDataSource<RoleModel, RoleNodeModel>;

  /** Map from flat node to nested node. This helps us finding the nested node to be modified */
  flatNodeMap = new Map<RoleNodeModel, RoleModel>();

  /** A selected parent node to be inserted */
  selectedParent: RoleNodeModel | null = null;

  /** The new item's name */
  newItemName = '';

  
  constructor(private database: ChecklistDatabase, public fxContext: FxContext) {
    // this.treeFlattener = new MatTreeFlattener(this.transformer, this.getLevel,
    //   this.isExpandable, this.getChildren);
    // this.treeControl = new FlatTreeControl<TodoItemFlatNode>(this.getLevel, this.isExpandable);
    this.treeFlattener = new MatTreeFlattener(this.transformer, this.getLevel, this.isExpandable, this.getChildren);
    this.treeControl = new FlatTreeControl<RoleNodeModel>(this.getLevel, this.isExpandable);
    this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
    var dim = this.checklistSelection.selected;
    database.dataChange.subscribe(data => {
      this.dataSource.data = data;
    });
  }

  private getLevel = (node: RoleNodeModel) => node.level;
  private isExpandable = (node: RoleNodeModel) => node.expandable;
  private getChildren = (node: RoleModel): RoleModel[] => node.childRoles;
  public hasChild = (_: number, nodeData: RoleNodeModel) => nodeData.expandable;
  public hasNoContent = (_: number, nodeData: RoleNodeModel) => nodeData.formatName === "";

 
  private transformer = (node: RoleModel, level: number) => {
    const existingNode = this.nestedNodeMap.get(node);
    const flatNode = existingNode && existingNode.name === node.name ? existingNode : new RoleNodeModel();
    flatNode.format = node.format;
    flatNode.level = level;
    flatNode.expandable = !!node.childRoles;
    flatNode.formatName = node.formatName;
    flatNode.locationId = node.locationId;
    flatNode.locationName = node.locationName;
    flatNode.parentId = node.parentId;
    flatNode.name = node.name;
    if (level === 0 || this.previousLevel > level) {
      flatNode.nodeId = ++this.nodeId;
    } else {
      flatNode.nodeId = this.nodeId;
    }
    this.previousLevel = level;
    this.nodesData.push(flatNode);
    this.nestedNodeMap.set(node, flatNode);
    return flatNode;
  }
  /** Whether all the descendants of the node are selected. */
  descendantsAllSelected(node: RoleNodeModel): boolean {
    const parentId = node.parentId;
    const descendants = this.treeControl.getDescendants(node);
    
    const descAllSelected = descendants.every(child =>

      this.checklistSelection.isSelected(child)
    );
    return descAllSelected;
  }

  /** Whether part of the descendants are selected */
  descendantsPartiallySelected(node: RoleNodeModel): boolean {
    const descendants = this.treeControl.getDescendants(node);
    const result = descendants.some(child => this.checklistSelection.isSelected(child));
    return result && !this.descendantsAllSelected(node);
  }

  /** Toggle the to-do item selection. Select/deselect all the descendants node */
  todoItemSelectionToggle(node: RoleNodeModel): void {
    this.checklistSelection.toggle(node);
    const descendants = this.treeControl.getDescendants(node);
    this.checklistSelection.isSelected(node)
      ? this.checklistSelection.select(...descendants)
      : this.checklistSelection.deselect(...descendants);

    // Force update for the parent
    descendants.every(child =>
      this.checklistSelection.isSelected(child)
    );
    // this.checkAllParentsSelection(node);
  }

  /** Toggle a leaf to-do item selection. Check all the parents to see if they changed */
  todoLeafItemSelectionToggle(node: RoleNodeModel): void {
    this.checklistSelection.toggle(node);
    //this.checkAllParentsSelection(node);
  }

  /** Check root node checked state and change it accordingly */
  checkRootNodeSelection(node: RoleNodeModel): void {
    const nodeSelected = this.checklistSelection.isSelected(node);
    const descendants = this.treeControl.getDescendants(node);
    const descAllSelected = descendants.every(child =>
      this.checklistSelection.isSelected(child)
    );
    if (nodeSelected && !descAllSelected) {
      this.checklistSelection.deselect(node);
    } else if (!nodeSelected && descAllSelected) {
      this.checklistSelection.select(node);
    }
  }

 

}
