import { MessageResult, FxContext } from "@agility/frameworkcore";
import { Injectable, OnDestroy} from "@angular/core";
import { Router } from "@angular/router";
import { Idle } from "@ng-idle/core";
import { Keepalive } from "@ng-idle/keepalive";
import { Subject, Observable} from 'rxjs';
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable()
export class KeepAliveSessionService implements OnDestroy {
    private ngUnsubscribe = new Subject();
   
  private messageResult: MessageResult = new MessageResult();
  constructor(private http: HttpClient, private router: Router, private keepalive: Keepalive, private idle: Idle,
     private fxContext: FxContext) {
    }

  private getData(): Observable<any> {
    const headers = new HttpHeaders();
    headers.set('Content-Type', 'text/plain; charset=utf-8');
    var resp = this.http.get("/api/account/RefreshToken", { responseType: 'text' });
    return resp;
  }
  public setKeepAliveSessionConfiguration(): void {
    let timeInterval: number = 1000;
    this.keepalive.onPing.subscribe(() => {
      this.getData().subscribe(data => {
        this.fxContext.userProfile.userAccessToken = data.toString();
      });
    });
    this.keepalive.interval(timeInterval);

  }
  

    public start(): void {
        this.keepalive.start();
    }

    public stop(): void {
        this.idle.stop();     
    }

   
    public clearTimeout(timeOutHandle: any): void {
        window.clearTimeout(timeOutHandle);
    }

    public yesClickEvent(timeOutHandle: any): void {
        this.clearTimeout(timeOutHandle);
        this.start();
    }

    public noClickEvent(timeOutHandle: any): void {
        this.clearTimeout(timeOutHandle);
        this.stop();
        this.router.navigate(["/Account"]);
    }

    public ngOnDestroy(): void {
        if (this.idle) {
            this.stop();
        }
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }
}
