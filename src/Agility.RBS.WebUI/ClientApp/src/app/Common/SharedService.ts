import { Injectable } from "@angular/core";
import { MatDialogRef, MatDialog } from "@angular/material";
import { Router, ActivatedRoute } from "@angular/router";
import { FormGroup, ValidatorFn, AbstractControl } from "@angular/forms";
import { MessageResult, ServiceDocument, IdentityMenuModel } from "@agility/frameworkcore";
import { Observable, Subject } from "rxjs";
import { GridOptions } from "ag-grid-community";
import { StackTraceComponent } from "./StackTraceComponent";
import { RbDialog, RbButton } from "../Common/controls/RbControlModels";
import { RbDialogService } from "../Common/controls/RbDialogService";
import { HttpHeaders, HttpClient, HttpParams } from "@angular/common/http";
import { Location } from "@angular/common";
import { startWith, map } from "rxjs/operators";

@Injectable()
export class SharedService {
  public editObject: any;
  public searchData: any;
  public itemListData: any;
  public editMode: boolean = false;
  public toolbarDataList: any[];
  public dataList: any[];
  public toolbarEditObject: any;
  public indexOfCurrentItem: number;
  public domainData: { [key: string]: any; } = {};
  public previousUrl: string;
  public currentUrl: string;
  public favouritePagePath: string;
  public favouritePage: string;
  public isUpdatableByUser: boolean = true;
  public selectedDsdLocation = { locId: 0, locName: "" };
  public routes: string[] = ["/UOMClass/New"
    , "/UOM/New"
    , "/UOMConversion/New"
    , "/Users/New"
    , "/DomainHeader/New"
    , "/DomainHeader/Edit"
    , "/Shipment/New"
    , "/Shipment/Edit"
    , "/Division/New"
    , "/Group/New"
    , "/Department/New"
    , "/Class/New"
    , "/SubClass/New"
    , "/CategoryRole/New"
    , "/MerchantSpace/New"
    , "/Company/New"
    , "/OrgCountry/New"
    , "/Region/New"
    , "/Format/New"
    , "/VatRegion/New"
    , "/VatCode/New"
    , "/vatrates/New"
    , "/Season/New"
    , "/Price/New"
    , "/Location/New"
    , "/Country/New"
    , "/Currency/New"
    , "/VatRegionVatCode/New"
    , "/Phase/New"
    , "/Roles/New"
    , "/Item/New"
    , "/Item/New/BasicDetails",
    , "/Item/New/BasicDetails/:id",
    , "/Item/New/ItemPackDetails/:id"
    , "/Item/New/ItemCompany/:id"
    , "/Item/New/ItemCountry/:id"
    , "/Item/New/ItemRegion/:id"
    , "/Item/New/ItemFormat/:id"
    , "/Item/New/ItemSupplier/:id"
    , "/Item/New/ItemSupplierCountry/:id"
    , "/Item/New/ItemSuppCountryLocation/:id"
    , "/Item/New/ItemSupplierCountryDimension/:id"
    , "/Item/New/ItemSupplierCountryHts/:id"
    , "/SupplierMaster/New"
    , "/SupplierAddressMaster/New"
    , "/SupplierMaster/New/:id"
    , "/CostChange/New"
    , "/CostChange/New/CostChangeHead"
    , "/CostChange/New/CostChangeHead/:id"
    , "/CostChange/New/CostChangeDetails/:id"
    , "/ItemListHead/New"
    , "/ReturnToVendor/New"
    , "/CostZoneGroup/New"
    , "/PriceZoneGroup/New"
    , "/CostChangeReason/New"
    , "/PriceChangeReason/New"
    , "/CostZone/New"
    , "/PriceZone/New"
    , "/PriceChange/New"
    , "/PriceChange/New/PriceChangeHead"
    , "/PriceChange/New/PriceChangeHead/:id"
    , "/PriceChange/New/PriceChangeDetails/:id"
    , "/Promotion/New"
    , "/Promotion/New/PromotionHead"
    , "/Promotion/New/PromotionHead/:id"
    , "/Promotion/New/PromotionDetails/:id"
    , "/Order/New"
    , "/Order/New/OrderHead"
    , "/Order/New/OrderHead/:id"
    , "/Order/New/OrderDetails/:id"
    , "/Promotion/New/PromotionLocation/:id"
    , "/Promotion/New/PromotionLocation/:id/:idComponent"
    , "/Brand/New"
    , "/BrandCategory/New"
    , "/SupplyChain/New"
    , "/SupplyChain/New/ShipmentDetails"
    , "/SupplyChain/New/ShipmentDetails/:id"
    , "/SupplyChain/New/ShipmentSku/:id"
    , "/Partner/New"
    , "/Deal/New"
    , "/Deal/New/DealHead"
    , "/Deal/New/DealHead/:id"
    , "/Deal/New/DealDetail/:id"
    , "/Deal/New/DealItemLocation/:id"
    , "/SupplierGroup/New"
    , "/Promotion/New/PromotionNew"
    , "/Promotion/New/PromotionNew/:id"
    , "/BulkItem/New"
    , "/ItemClose/New"
    , "/ItemReactive/New"
    , "/Forecast/New"
    , "/ItemSuperSearch"
    , "/TransferEntity/New"
    , "/TransferZone/New"
    , "/Transfer/New"
    , "/Transfer/New/TransferHead/"
    , "/Transfer/New/TransferHead/:id"
    , "/Transfer/New/TransferDetails/:id"
    , "/Shipment/New"
    , "/Chain/New"
    , "/Area/New"
    , "/District/New",
    , "/StoreFormat/New"
    , "/Competitors/New"
    , "/Obligation/New"
    , "/Obligation/New/ObligationHead"
    , "/Obligation/New/ObligationHead/:id"
    , "/Obligation/New/ObligationDetails/:id"
    , "/supplieroptions/New"
    , "/systemoptions/New"
    , "tolerance/New"
    , "reasoncode/New"
    , "/DsdOrders/New"
    , "/DsdMinMax/New"
    , "/Buyer/New"
    , "/PriceZoneGroup/New"
    , "/employee/New"
    , "/CompetitivePriceHistory/New"
    , "/SupplierTrait/New"
    , "/LocationTrait/New"
    , "/Outloc/New"
    , "/GLCrossReference/New"
    , "/UserAttributes/New"
    , "/DynamicHierarchy/New"
    , "/WareHouse/New"
    , "/Half/New"
    , "/CurrencyExchangeType/New"
    , "/Document/New"
    , "NbSystemOptions/New"
    , "RpmSystemOptions/New"
    , "SaSystemOptions/New"
    , "/ItemReclassification/New"
    , "/InventoryAdjutmentReason/New"
    , "/invoiceMatchingDocument/New"
    , "/invoiceMatchingDocument/New/InvoiceMatchingDocHead"
    , "/invoiceMatchingDocument/New/InvoiceMatchingDocHead/:id"
    , "/invoiceMatchingDocument/New/InvoiceMatchingDocDetails/:id"
    , "/securitygroup/New"
    , "/ShipmentTracking/New"
    , "/securityusergroup/New"
    , "/AverageCostAdjustment/New"
    , "/CurrencyRate/New"
    , "/SecurityGroupOrgHierarchy/New"
    , "/SecurityGroupHierarchy/New"
    , "/Transportation/New"
    , "/imCostDiscrepancy/New"
    , "/imQtyDiscrepancy/New"
    , "/NonMerchCode/New"
    , "/UserRolePrivileges/New"
    , "/MessageSearch/New"
    , "/RegionalityGroup/New"
    , "/RegionalityUserGroup/New"
    ,"/securityusergroup/New"   
    , "/CurrencyRate/New" 
    , "/imDocMaintenance/New"
    , "/SystemVariables/List"
    , "/ItemUda/New"   
    , "/ReceiverCostAdjustment/New"
  ];
  rbDialog: RbDialog = new RbDialog();
  public gridOptions: GridOptions;
  dialogRef: MatDialogRef<any>;
  private messageResult: MessageResult = new MessageResult();
  public menus: IdentityMenuModel[];
  public fileCount: number;
  public loggedinUserName: string = "";
  public loggedinUserPic: string;
  public loggedinUser: string;
  public stateID: string;
  shipmentId: number;
  schedulerView: string;
  calendarViewName: string;
  calendarViewDate: Date;
  apiUrl: string = "";
  userRoles: any[];
  public submitOperations: { reject: string, sendback: string, submit: string } = { reject: "REJ", sendback: "SBR", submit: "CHGS" };

  constructor(private router: Router, private dialogService: RbDialogService, private httpClient: HttpClient, public location: Location) {

  }
  private myAnnouncedSource = new Subject<any>();
  public wfPermitionsAnnouncedSource = new Subject<ServiceDocument<any>>();
  myAnnounced$ = this.myAnnouncedSource.asObservable();
  wfPermitionsAnnounced$ = this.wfPermitionsAnnouncedSource.asObservable();

  announceItem(item: any): void {
    this.myAnnouncedSource.next(item);
  }

  checkWfPermitions(serviceDocument: ServiceDocument<any>): void {
    this.wfPermitionsAnnouncedSource.next(serviceDocument);
  }

  validateForm(formGroup: any): void {
    Object.keys(formGroup.controls).map(
      controlName => formGroup.controls[controlName]).forEach(
        control => { control.markAsTouched(); }
      );
  }

  resetForm(formGroup: any): void {
    Object.keys(formGroup.controls).map(
      controlName => formGroup.controls[controlName]).forEach(
        control => { control.markAsUntouched(); }
      );
  }

  saveForm(content: any): Observable<any> {
    this.rbDialog = new RbDialog();
    this.messageResult.message = content;
    this.dialogRef = this.dialogService.openMessageDialog("Success", this.messageResult, [new RbButton("", "OK", "")]
      , "30%", "", "iconcssSuceessStyle retailsNavIcons", "successIndicationDialog", "successAlertDialog");

    this.dialogRef.componentInstance.click.subscribe(
      btnName => {
        if (btnName === "Ok") {
          this.dialogRef.close();
        }
      });
    return this.dialogRef.afterClosed();
  }

  successForm(content: string): void {
    this.messageResult.message = content;
    this.dialogRef = this.dialogService.openMessageDialog("Success", this.messageResult, [new RbButton("", "OK", "")]
      , "30%", "", "iconcssSuceessStyle retailsNavIcons", "successIndicationDialog", "successAlertDialog");
  }

  errorForm(content: string): void {
    this.messageResult.message = content;
    this.dialogRef = this.dialogService.openMessageDialog("Error", this.messageResult, [new RbButton("", "Ok", "")]
      , "30%", "", "iconcssErrorStyle retailsNavIcons", "uomClassErrorDiaglog", "ErrorAlertDialog");
  }

  showSTDialog(message: any, stackTrace: any): void {
    this.rbDialog.title = "Error";
    this.rbDialog.width = "50%";
    this.rbDialog.height = "50%";
    this.rbDialog.componentRef = StackTraceComponent;
    this.rbDialog.data = { message: message, stackTrace: stackTrace };
    this.dialogRef = this.dialogService.openDialog(this.rbDialog);
  }

  showSimpleDialog(message: string): void {
    this.rbDialog.title = "Error";
    this.rbDialog.width = "50%";
    this.rbDialog.content = message;
    this.rbDialog.titleCss = "uomClassErrorDiaglog";
    this.dialogRef = this.dialogService.openDialog(this.rbDialog);
  }

  viewForm(InfoComponent: any, cell: any, mode: string, redirectURL: string, dataList: any[]): void {
    this.editObject = cell.data;
    if (redirectURL === "Roles") {
      this.dataList = dataList;
    }
    if (mode === "edit") {
      this.setEditObjAndToolBarListForToolBar(cell.data, dataList);
      this.editMode = true;
      if (redirectURL === "DomainHeader") {
        this.router.navigate(["/" + redirectURL + "/Edit"], { skipLocationChange: true });
      } else if (redirectURL.indexOf("Item/New") > -1 || redirectURL.indexOf("PriceChange/New") > -1
        || redirectURL.indexOf("CostChange/New") > -1 || redirectURL.indexOf("Order/New") > -1
        || redirectURL.indexOf("Promotion/New") > -1 || redirectURL.indexOf("SupplyChain/New") > -1
        || redirectURL.indexOf("ItemClose/New") > -1 || redirectURL.indexOf("ItemReactive/New") > -1
        || redirectURL.indexOf("Deal/New") > -1 || redirectURL.indexOf("SupplierMaster/New") > -1
        || redirectURL.indexOf("Forecast/New") > -1 || redirectURL.indexOf("ReturnToVendor/New") > -1
        || redirectURL.indexOf("Transfer/New") > -1 || redirectURL.indexOf("Shipment/New") > -1
          || redirectURL.indexOf("Obligation/New") > -1 || redirectURL.indexOf("DsdOrders/New") > -1
          || redirectURL.indexOf("employee/New") > -1 || redirectURL.indexOf("UserAttributes/New") > -1 || redirectURL.indexOf("DynamicHierarchy/New") > -1 || redirectURL.indexOf("invoiceMatchingDocument/New") > -1
        || redirectURL.indexOf("securitygroup/New") > -1 || redirectURL.indexOf("imCostDiscrepancy/New") > -1 || redirectURL.indexOf("imQtyDiscrepancy/New") > -1
        || redirectURL.indexOf("Transportation/New") > -1 || redirectURL.indexOf("ItemUda/New") > -1
        || redirectURL.indexOf("securitygroup/New") > -1 || redirectURL.indexOf("GLCrossReference/New") > -1
      ) {
        this.router.navigate(["/" + redirectURL], { skipLocationChange: true });
      } else {
        this.router.navigate(["/" + redirectURL + "/New"], { skipLocationChange: true });
      }
    } else if (mode === "view") {
      this.rbDialog.title = "User Details";
      this.rbDialog.width = "45%";
      this.rbDialog.height = "45%";
      this.rbDialog.componentRef = InfoComponent;
      this.dialogRef = this.dialogService.openDialog(this.rbDialog);
    } else if (mode === "orderdtlsview" || mode === "shipmentSkuView") {
      this.rbDialog.title = (mode === "orderdtlsview") ? "Order Details" : "Shipment SKU";
      this.rbDialog.width = "80%";
      this.rbDialog.height = "85%";
      this.rbDialog.componentRef = InfoComponent;
      this.dialogRef = this.dialogService.openDialog(this.rbDialog);
    }
  }

  updateEditObjInSharedServiceAndToolBarList(editObject: {}): {} {
    return this.editObject = this.toolbarEditObject = this.toolbarDataList[this.indexOfCurrentItem] = editObject;
  }

  setEditObjAndToolBarListForToolBar(cellData: {}, gridOptionsRowData: any[]): void {
    this.editObject = this.toolbarEditObject = cellData;
    this.toolbarDataList = gridOptionsRowData;
    this.indexOfCurrentItem = this.toolbarDataList.indexOf(this.toolbarEditObject);
  }

  deleteAllToolBarData(): void {
    delete this.indexOfCurrentItem;
    delete this.toolbarDataList;
    delete this.toolbarEditObject;
    delete this.editObject;
    delete this.editMode;
  }

  openPopupWithDataList(popupComponent: any, dataList: any[], title: string, objectName: string, objectValue: string | number
    , showAttachFiles: boolean = true, rowData?: { viewMode: boolean, cellData: any }, invokingModule?: string): void {
    this.editObject = dataList;
    this.rbDialog.title = title;
    this.rbDialog.height = "70%";
    this.rbDialog.width = "80%";
    this.rbDialog.data = { objectName: objectName, objectValue: objectValue };
    if (title === "Document Upload") {
      this.rbDialog.height = "90%";
      this.rbDialog.data = { objectName: objectName, objectValue: objectValue, dataList: dataList, showAttachFiles: showAttachFiles };
    }
    if (title in { "Inbox QuickView": 1, "Shipment List": 1, "Item Cost & Prices": 1, "Item Supplier": 1, "Item Ranging": 1 }) {
      this.rbDialog.width = "60%";
      this.rbDialog.height = "63%";
      this.rbDialog.data = { dataList: dataList, rowData: rowData, objectName: objectName, objectValue: objectValue };
      if (title in { "Shipment List": 1, "Item Cost & Prices": 1, "Item Supplier": 1, "Item Ranging": 1 }) {
        this.rbDialog.height = "70%";
        this.rbDialog.width = "80%";
        this.rbDialog.data.invokingModule = invokingModule;
      }
    }
    if (title === "Locations") {
      this.rbDialog.height = "70%";
      this.rbDialog.width = "60%";
      this.rbDialog.data = { objectName: objectName, objectValue: objectValue, dataList: dataList, showAttachFiles: showAttachFiles };

    }
    this.rbDialog.componentRef = popupComponent;
    this.dialogRef = this.dialogService.openDialog(this.rbDialog);
  }

  openDilogWithInputData(popupComponent: any, data: any, title: string, otherData?: { screenId: string, screenName: string }): void {
    this.rbDialog.data = data;
    this.rbDialog.title = title;
    this.rbDialog.height = "30%";
    this.rbDialog.width = "50%";
    if (title in {
      "Order Print": 1, "Cost Change Request Print": 1, "Promotion Print": 1,
      "Price Change Request Print": 1, "Item Print": 1, "Shipment Print": 1, "Deal Print": 1
    }) {
      this.rbDialog.data = { inputData: data, otherInputData: otherData };
      this.rbDialog.width = "100%";
      this.rbDialog.maxWidth = "inherit";
      this.rbDialog.height = "100%";
    } else if (title in { "item Dimensions View": 1, "item Basic View": 1, "document preview": 1 }) {
      this.rbDialog.width = "80%";
      this.rbDialog.height = "85%";
    }
    this.rbDialog.componentRef = popupComponent;
    this.dialogRef = this.dialogService.openDialog(this.rbDialog);
  }

  openDilogForFindItems(popupComponent: any, data: any, title: string): Observable<any> {
    this.rbDialog = new RbDialog();
    this.rbDialog.data = data;
    this.rbDialog.title = title;
    this.rbDialog.height = "90%";
    this.rbDialog.width = "85%";
    this.rbDialog.componentRef = popupComponent;
    this.dialogRef = this.dialogService.openDialog(this.rbDialog);
    return this.dialogRef.afterClosed();
  }

  viewChangePasswordForm(InfoComponent: any, cell: any, mode: string, redirectURL: string, dataList: any[]): void {
    this.rbDialog.title = "User Details";
    this.rbDialog.width = "50%";
    this.rbDialog.height = "68%";
    this.rbDialog.componentRef = InfoComponent;
    this.dialogRef = this.dialogService.openDialog(this.rbDialog);
  }

  setToolbarSave(flag: boolean): void {
    this.isUpdatableByUser = flag;
  }
  setOffSet(inputDate: Date): Date {
    let dateValue: Date = new Date(inputDate);
    let d: Date = new Date();
    let n: number = d.getTimezoneOffset();
    if (dateValue !== null) {
      dateValue = new Date(dateValue.getTime() - n * 60 * 1000);
    }
    return dateValue;
  }

  emailValidator(errorMessage: string): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      if (control.value) {
        if (control.value.match(
          /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
        )) {
          return null;
        } else {
          return { errorMessage };
        }
      }
    };
  }

  numberValidator(errorMessage: string, minValue: number): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      if (control.value) {
        if (+control.value > minValue) {
          return null;
        } else {
          return { errorMessage };
        }
      }
    };
  }

  booleanFormatter(params: any): string {
    if (params && params.value) {
      return params.value.toLowerCase() === "y" ? "Yes" : (params.value.toLowerCase() === "n" ? "No" : params.value);
    }
    return;
  }

  calculateMargin(price: number, cost: number, exchangeRate: number): number {
    let margin: number;
    if (price && cost && exchangeRate) {
      margin = ((price - (exchangeRate * cost)) / price) * 100;
      return +margin.toPrecision(3);
    }

    if (margin) {
      return margin;
    } else {
      return 0;
    }
  }

  public deepCopy(oldObj: any): any {
    let newObj: any = oldObj;
    if (oldObj && typeof oldObj === "object") {
      newObj = Object.prototype.toString.call(oldObj) === "[object Array]" ? [] : {};
      for (let i in oldObj) {
        // if (oldObj.hasOwnProperty(i)) {
        newObj[i] = this.deepCopy(oldObj[i]);
        // }
      }
    }
    return newObj;
  }

  downloadExcel(id: number, screenName: string): void {
    const type: any = "application/vnd.ms-excel";
    const filename: any = "file.xls";
    if (screenName === "BulkItem") {
      this.apiUrl = "/api/BulkItem/OpenLocalItemExcel";
    } else if (screenName === "CostChange") {
      this.apiUrl = "/api/CostChangeHead/OpenCostManagementExcel";
    } else if (screenName === "Promotion") {
      this.apiUrl = "/api/PromotionDetails/OpenPromoDetailExcel";
    } else if (screenName === "ItemReactive") {
      this.apiUrl = "/api/ItemReactive/OpenItemReactivationExcel";
    }

    let headers1: HttpHeaders = new HttpHeaders();
    headers1.set("Accept", type);
    this.httpClient.get(this.apiUrl,
      {
        headers: headers1, responseType: "blob" as "json"
        , params: new HttpParams().set("id", id.toString())
      })
      .pipe(map((response: any) => {
        if (response instanceof Response) {
          return response.blob();
        }
        return response;
      }))
      .subscribe(res => {
        console.log("start download:", res);
        let url: string = window.URL.createObjectURL(res);
        let a: HTMLAnchorElement = document.createElement("a");
        document.body.appendChild(a);
        a.setAttribute("style", "display: none");
        a.href = url;
        a.download = screenName + "-" + id.toString() + ".xlsm";
        a.click();
        window.URL.revokeObjectURL(url);
        a.remove();
      }, (error: string) => { console.log(error); });
  }

  calculateAnnualDiscount(rebateDiscount: number, dealDiscount: number): number {
    let annualDisc: number = (rebateDiscount + dealDiscount) / 100;
    return +annualDisc.toPrecision(3);
  }

  checkRoleForItemSuper(): boolean {
    let isSuperRole: boolean;
    for (let i = 0; i < this.userRoles.length; i++) {
      if (this.userRoles[i].name === "MDM Admin") {
        isSuperRole = true;
        break;
      }
    }
    return isSuperRole;
  }

  openDilogForFindImDoc(popupComponent: any, data: any, title: string): Observable<any> {
    this.rbDialog = new RbDialog();
    this.rbDialog.data = data;
    this.rbDialog.title = title;
    this.rbDialog.height = "70%";
    this.rbDialog.width = "60%";
    this.rbDialog.componentRef = popupComponent;
    this.dialogRef = this.dialogService.openDialog(this.rbDialog);
    return this.dialogRef.afterClosed();
  }

}
