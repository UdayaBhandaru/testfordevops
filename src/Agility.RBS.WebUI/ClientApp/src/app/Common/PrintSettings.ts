﻿export class PrintSettings {
    public commonStyles: any = { overflow: "linebreak", columnWidth: "wrap" };
    public costPriceDetailColumnStyles: any = {
        0: { columnWidth: 80 }, 1: { columnWidth: "auto" }, 2: { columnWidth: "auto" },
        5: { columnWidth: 80 }, 6: { columnWidth: 70 }, 7: { columnWidth: 70 }, 8: { columnWidth: "auto" }
    };
    public orderHeadColumnStyles: any = {
        details: { fillColor: [41, 128, 185], textColor: 255, fontStyle: "bold", columnWidth: 120 },
        values: { columnWidth: 120 }
    };
    public orderDetailColumnStyles: any = {
        2: { columnWidth: 80 }, 3: { columnWidth: 70 }, 4: { columnWidth: "auto" }
    };
    public orderSupplierColumnStyles: any = {
        supplierDetails: { fillColor: [41, 128, 185], textColor: 255, fontStyle: "bold", columnWidth: 100 },
        values: { columnWidth: 200 }
    };
    public orderHeadMargins: any = { left: 400, right: 40, top: 80 };
    public orderSupplierMargins: any = { left: 40, right: 400, top: 80 };
    public promoHeadColumnStyles: any = {
        0: { columnWidth: "auto" }, 1: { columnWidth: "auto" }, 2: { columnWidth: "auto" }, 3: { columnWidth: "auto" },
        4: { columnWidth: "auto" }, 5: { columnWidth: "auto" }, 6: { columnWidth: "auto" }, 7: { columnWidth: "auto" },
        8: { columnWidth: "auto" }, 9: { columnWidth: "auto" }, 10: { columnWidth: "auto" }, 11: { columnWidth: "auto" }
    };
    public promoDetailColumnStyles: any = {
        0: { columnWidth: "auto" }, 1: { columnWidth: "auto" }, 2: { columnWidth: "auto" }, 3: { columnWidth: "auto" },
        4: { columnWidth: "auto" }, 5: { columnWidth: "auto" }, 6: { columnWidth: "auto" }, 7: { columnWidth: "auto" },
        8: { columnWidth: "auto" }, 9: { columnWidth: "auto" }, 10: { columnWidth: "auto" }, 11: { columnWidth: "auto" },
        12: { columnWidth: "auto" }, 13: { columnWidth: "auto" }, 14: { columnWidth: "auto" }
    };
    public itemHeadColumnStyles: any = {
        0: { columnWidth: "auto" }, 1: { columnWidth: "auto" }, 2: { columnWidth: "auto" }, 3: { columnWidth: "auto" },
        4: { columnWidth: "auto" }, 5: { columnWidth: "auto" }, 6: { columnWidth: "auto" }, 7: { columnWidth: "auto" },
        8: { columnWidth: "auto" }, 9: { columnWidth: "auto" }, 10: { columnWidth: "auto" }
    };
    public shipmentDetailColumnStyles: any = {
        0: { columnWidth: "auto" }, 1: { columnWidth: 70 }, 2: { columnWidth: 70 }, 3: { columnWidth: "auto" },
        4: { columnWidth: "auto" }, 5: { columnWidth: "auto" }, 6: { columnWidth: "auto" }, 7: { columnWidth: "auto" },
        8: { columnWidth: 70 }, 9: { columnWidth: "auto" }, 10: { columnWidth: "auto" }, 11: { columnWidth: "auto" }
    };
}