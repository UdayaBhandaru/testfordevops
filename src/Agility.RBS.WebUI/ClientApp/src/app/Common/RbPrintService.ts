import { Injectable } from "@angular/core";
//import * as  jsPDF from "jspdf";
import "jspdf-autotable";
import { DocumentsConstants } from "./DocumentsConstants";
import { CommonPrintModel } from "./CommonPrintModel";
import { PrintSettings } from "./PrintSettings";

@Injectable()
export class RbPrintService {
    documentsConstants: DocumentsConstants = new DocumentsConstants();
    printSettings: PrintSettings = new PrintSettings();

    print(headObject: CommonPrintModel, detailObject: CommonPrintModel[]
        , screenDetails: { screenId: string | number, screenName: string }, supplierObject?: CommonPrintModel): any {
        let totalPagesExp: string = "{total_pages_count_string}";
      let doc: any;/// = new jsPDF("l", "pt", "a4");

        let headTableOptions: any = {
            margin: { top: 80 },
            styles: {},
            columnStyles: {}
        };
        let headColumns: any[] = headObject.columns;
        switch (screenDetails.screenName) {
            case this.documentsConstants.orderObjectName: {
                let supplierTableOptions: any = {
                    drawHeaderRow: () => {
                        return false;
                    },
                    margin: this.printSettings.orderSupplierMargins,
                    styles: { overflow: "linebreak" },
                    columnStyles: this.printSettings.orderSupplierColumnStyles
                };
                doc.autoTable(supplierObject.moduleSpecificColumns, supplierObject.data, supplierTableOptions);
                headColumns = headObject.moduleSpecificColumns;
                headTableOptions = {
                    drawHeaderRow: () => {
                        return false;
                    },
                    margin: this.printSettings.orderHeadMargins,
                    columnStyles: this.printSettings.orderHeadColumnStyles
                };
                break;
            }
            case this.documentsConstants.promoPrintObjectName: {
                headTableOptions["styles"] = this.printSettings.commonStyles;
                headTableOptions["columnStyles"] = this.printSettings.promoHeadColumnStyles;
                break;
            }
            default: {
                break;
            }
        }

        doc.autoTable(headColumns, headObject.data, headTableOptions);

        let header: any = function (data: any): void {
            doc.setFontSize(18);
            doc.setTextColor(40);
            doc.setFontStyle("normal");
            doc.text(screenDetails.screenName + " " + screenDetails.screenId, data.settings.margin.left, 50);
        };

        let detailTableOptions: any = {
            theme: "grid",
            addPageContent: header,
            margin: {
                top: 80
            },
            startY: doc.autoTableEndPosY() + 20,
            styles: {},
            columnStyles: {}
        };

        detailTableOptions["styles"] = this.printSettings.commonStyles;

        switch (screenDetails.screenName) {
            case this.documentsConstants.costPrintObjectName:
            case this.documentsConstants.pricePrintObjectName: {
                detailTableOptions["columnStyles"] = this.printSettings.costPriceDetailColumnStyles;
                break;
            }
            case this.documentsConstants.promoPrintObjectName: {
                detailTableOptions["columnStyles"] = this.printSettings.promoDetailColumnStyles;
                break;
            }
            case this.documentsConstants.shipmentObjectName: {
                detailTableOptions["columnStyles"] = this.printSettings.shipmentDetailColumnStyles;
                break;
            }
            case this.documentsConstants.orderObjectName: {
                detailTableOptions["styles"] = { overflow: "linebreak" };
                detailTableOptions["columnStyles"] = this.printSettings.orderDetailColumnStyles;
                break;
            }
            default: {
                break;
            }
        }

        doc.autoTable(detailObject[0].columns, detailObject[0].data, detailTableOptions);
        return doc;
    }

    printWithMultipleDetails(headObject: CommonPrintModel, detailObject: CommonPrintModel[]
        , screenDetails: { screenId: string | number, screenName: string }): any {
        let totalPagesExp: string = "{total_pages_count_string}";
      let doc: any; ///= ///new jsPDF("l", "pt", "a4");

        let headTableOptions: any = {};

        headTableOptions = {
            margin: { top: 80 },
            startY: 95,
            styles: this.printSettings.commonStyles,
            columnStyles: {}
        };
        if (screenDetails.screenName === this.documentsConstants.itemObjectName) {
            headTableOptions["columnStyles"] = this.printSettings.itemHeadColumnStyles;
        }
        doc.setFontSize(12);
        doc.setTextColor(0);
        doc.setFontStyle("bold");
        doc.text(headObject.tableName, 40, 90);
        doc.autoTable(headObject.columns, headObject.data, headTableOptions);

        let header: any = function (data: any): void {
            doc.setFontSize(18);
            doc.setTextColor(40);
            doc.setFontStyle("normal");
            doc.text(screenDetails.screenName + " " + screenDetails.screenId, data.settings.margin.left, 50);
        };
        for (let i: number = 0; i < detailObject.length; i++) {
            doc.setFontSize(12);
            doc.setTextColor(0);
            doc.setFontStyle("bold");
            doc.text(detailObject[i].tableName, 40, doc.autoTable.previous.finalY + 40);
            doc.autoTable(detailObject[i].columns, detailObject[i].data, {
                addPageContent: header,
                margin: {
                    top: 80
                },
                startY: doc.autoTable.previous.finalY + 45,
                pageBreak: "avoid",
                styles: this.printSettings.commonStyles,
                theme: "grid"
            });
        }
        return doc;
    }
}
