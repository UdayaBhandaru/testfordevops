export class DocumentsConstants {
  public supplierObjectName: string = "SUPPLIER";
  public itemObjectName: string = "ITEM";
  public bulkitemObjectName: string = "BULKITEM";
  public forecastObjectName: string = "FORECAST";
  public costObjectName: string = "COST";
  public priceObjectName: string = "PRICE";
  public promoObjectName: string = "PROMO";
  public dealObjectName: string = "DEAL";
  public orderObjectName: string = "ORDER";
  public shipmentObjectName: string = "SHIPMENT";
  public costLineObjectName: string = "CostChangeLine";
  public priceLineObjectName: string = "PriceChangeLine";
  public promoLineObjectName: string = "PromotionComponentLocation";
  public supplierPrintObjectName: string = "Supplier";
  public itemPrintObjectName: string = "Item";
  public bulkitemPrintObjectName: string = "Bulk Item Request";
  public costPrintObjectName: string = "Cost Change";
  public pricePrintObjectName: string = "Price Change";
  public promoPrintObjectName: string = "Promotion";
  public dealPrintObjectName: string = "Deal";
  public orderPrintObjectName: string = "Order";
  public shipmentPrintObjectName: string = "Shipment";
  public costLinePrintObjectName: string = "Cost Change Line";
  public priceLinePrintObjectName: string = "Price Change Line";
  public itemCompaniesObjectName: string = "ITEM COMPANIES";
  public itemCountriesObjectName: string = "ITEM COUNTRIES";
  public itemRegionsObjectName: string = "ITEM REGIONS";
  public itemFormatsObjectName: string = "ITEM FORMATS";
  public itemLocationsObjectName: string = "ITEM LOCATIONS";
  public itemBarcodesObjectName: string = "ITEM BARCODES";
  public itemVatsObjectName: string = "ITEM VATS";
  public itemSuppliersObjectName: string = "ITEM SUPPLIERS";
  public itemSupplierCountriesObjectName: string = "ITEM SUPPLIER COUNTRIES";
  public itemSupplierCountryLocationsObjectName: string = "ITEM SUPPLIER COUNTRY LOCATIONS";
  public itemBasicDetailsObjectName: string = "ITEM BASIC DETAILS";
  public costHeadObjectName: string = "COST CHANGE HEAD";
  public costDetailsObjectName: string = "COST CHANGE DETAILS";
  public priceHeadObjectName: string = "PRICE CHANGE HEAD";
  public priceDetailsObjectName: string = "PRICE CHANGE DETAILS";
  public promotionHeadObjectName: string = "PROMOTION HEAD";
  public promotionDetailsObjectName: string = "PROMOTION DETAILS";
  public orderHeadObjectName: string = "ORDER HEAD";
  public orderDetailsObjectName: string = "ORDER DETAILS";
  public orderSupplierObjectName: string = "ORDER SUPPLIER";
  public shipmentHeadObjectName: string = "SHIPMENT HEAD";
  public shipmentDetailsObjectName: string = "SHIPMENT DETAILS";
  public dealHeadObjectName: string = "DEAL HEAD";
  public dealDetailsObjectName: string = "DEAL DETAILS";
  public ItemReactiveObjectName: string = "ItemReactive";
  public ItemCloseObjectName: string = "ItemClose";
  public transferObjectName: string = "TRANSFER";
  public obligationObjectName: string = "OBLIGATION";
  public rtvObjectName: string = "RTV";
  public transObjectName: string = "TRANSPORTATION";
  public invoiceMatchingDocObjectName: string = "IM";

  public modulesHeadPageRoutes = [
    { "key": this.costObjectName, "value": "CostChange/New/" }

    , { "key": this.priceObjectName, "value": "PriceChange/New/" }
    , { "key": this.promoObjectName, "value": "Promotion/New/" }
    , { "key": this.itemObjectName, "value": "Item/New/BasicDetails/" }
    , { "key": this.supplierObjectName, "value": "SupplierMaster/New/" }
    , { "key": this.shipmentObjectName, "value": "SupplyChain/New/ShipmentDetails/" }
    , { "key": this.orderObjectName, "value": "Order/New/" }
    , { "key": this.dealObjectName, "value": "Deal/New/DealHead/" }
    , { "key": this.bulkitemObjectName, "value": "/BulkItem/New/" }
    , { "key": this.ItemReactiveObjectName, "value": "/ItemReactive/New/" }
    , { "key": this.ItemCloseObjectName, "value": "/ItemClose/New/" }
    , { "key": this.forecastObjectName, "value": "/Forecast/New/" }
    , { "key": this.transferObjectName, "value": "/Transfer/New/TransferHead/" }
    , { "key": this.obligationObjectName, "value": "Obligation/New/" }
    , { "key": this.rtvObjectName, "value": "/ReturnToVendor/New/" }
    , { "key": this.invoiceMatchingDocObjectName, "value": "/invoiceMatchingDocument/New/" }
  ];
}
