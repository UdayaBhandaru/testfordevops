import { Component, OnInit, OnDestroy, Inject } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { Router } from "@angular/router";
import { CommonService, FormMode } from "@agility/frameworkcore";
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from "@angular/material";
import { GridOptions, GridReadyEvent, ColDef, GridApi } from "ag-grid-community";
import { OrderShipmentResponseModel } from "./OrderShipmentResponseModel";
import { GridDateComponent } from "../Common/grid/GridDateComponent";
import { SharedService } from "../Common/SharedService";
import { GridEditComponent } from "../Common/grid/GridEditComponent";
import { DocumentsConstants } from "../Common/DocumentsConstants";

@Component({
    selector: "order-shipment",
    templateUrl: "./ShipmentPopupComponent.html"
})

export class ShipmentPopupComponent implements OnInit {
    shipmentList: OrderShipmentResponseModel[] = [];
    objectValue: string;
    objectName: string;
    componentName: any;
    columns: {}[];
    gridOptions: GridOptions;
    redirectUrl: string;
    documentsConstants: DocumentsConstants;
    gridApi: GridApi;

    constructor(
        private sharedService: SharedService
        , private _commonService: CommonService
        , public dialog: MatDialog
        , private _router: Router
        , @Inject(MAT_DIALOG_DATA) public data: {
            objectName: string, objectValue: string, dataList: OrderShipmentResponseModel[], invokingModule: string
        }
    ) {
        this.redirectUrl = "SupplyChain/New/ShipmentDetails/";
        this.documentsConstants = new DocumentsConstants();
    }

    ngOnInit(): void {
        this.shipmentList = this.data.dataList;
        this.objectValue = this.data.objectValue;
        this.objectName = this.data.objectName;
        this.componentName = this;
        this.columns = [
            {
                headerName: "Shipment ID", field: "shipmentId", tooltipField: "Shipment ID"
            },
            {
                headerName: "Estimated Arrival Date", field: "estimatedArrivalDate", tooltipField: "Estimated Arrival Date"
                , cellRendererFramework: GridDateComponent
            },
            {
                headerName: "Location", field: "locationName", tooltipField: "Location Name"
            },
            {
                headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 50
                , cellRendererParams: { restrictViewIcon: true }
            }
        ];

        this.gridOptions = {
            onGridReady: (params: any) => {
              this.gridApi = params.api;
              this.gridApi.sizeColumnsToFit();
            },
            context: {
                componentParent: this
            }
        };
    }

    open(cell: any, mode: string): void {
        this.close();
        if (mode === "edit") {
            this.sharedService.previousUrl = (this.data.invokingModule === this.documentsConstants.orderObjectName)
                ? "../Order/List" : "../SupplyChain/List";
            this.sharedService.editMode = true;
        }
        this.sharedService.shipmentId = cell.data.shipmentId;
        this._router.navigate(["/" + this.redirectUrl + cell.data.shipmentId], { skipLocationChange: true });
    }

    close(): void {
        this.dialog.closeAll();
    }
}
