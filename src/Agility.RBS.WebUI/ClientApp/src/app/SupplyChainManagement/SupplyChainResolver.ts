﻿import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { SupplyChainSchedulerService } from "./Scheduler/SupplyChainSchedulerService";
import { SupplyChainSchedulerModel } from "./Scheduler/SupplyChainSchedulerModel";

@Injectable()
export class SupplyChainSchedulerResolver implements Resolve<ServiceDocument<SupplyChainSchedulerModel>> {
    constructor(private service: SupplyChainSchedulerService) { }
    resolve(): Observable<ServiceDocument<SupplyChainSchedulerModel>> {
        return this.service.list();
    }
}