﻿import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { ShipmentDetailsModel } from "./ShipmentDetailsModel";
import { ShipmentDetailsService } from "./ShipmentDetailsService";

@Injectable()
export class ShipmentDetailsResolver implements Resolve<ServiceDocument<ShipmentDetailsModel>> {
    constructor(private service: ShipmentDetailsService) { }
    resolve(route: ActivatedRouteSnapshot): Observable<ServiceDocument<ShipmentDetailsModel>> {
        return this.service.addEdit(route.params["id"]);
    }
}