﻿export class ShipmentDetailsModel {
    shipment?: number;

    orderNo?: number;

    billOfLadingNo: string;

    advanceShipmentNotice: string;

    shipDate?: Date;

    receiveDate?: Date;

    estArrDate?: Date;

    shipOrigin: string;

    statusCode: string;

    invcMatchStatus: string;

    invcMatchDate?: Date;

    toLoc?: number;

    toLocType: string;

    fromLoc?: number;

    fromLocType: string;

    courier: string;

    noOfBoxes?: number;

    extRefNoIn: string;

    extRefNoOut: string;

    comments: string;

    parentShipment?: number;

    matchLocation?: number;

    fileCount?: number;

    operation: string;

    tableName?: string;
}