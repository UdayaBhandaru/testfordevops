﻿import { Component, OnInit, TemplateRef, ViewChild, OnDestroy, ElementRef } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ServiceDocument, CommonService, MessageResult, MessageType } from "@agility/frameworkcore";
import { MatDialogRef } from "@angular/material";
import { GridOptions } from "ag-grid-community";
import { Subscription } from "rxjs";
import { ShipmentDetailsModel } from "./ShipmentDetailsModel";
import { SharedService } from "../../Common/SharedService";
import { ShipmentDetailsService } from "./ShipmentDetailsService";
import { LoaderService } from "../../Common/LoaderService";
import { SupplyChainSharedService } from "../SupplyChainSharedService";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { CommonModel } from "../../Common/CommonModel";
import { RbButton } from "../../Common/controls/RbControlModels";
import { RbDialogService } from "../../Common/controls/RbDialogService";

@Component({
    selector: "shipmentDetails-add",
    templateUrl: "./ShipmentDetailsComponent.html"
})
export class ShipmentDetailsComponent implements OnInit, OnDestroy {
    _componentName = "Shipment Details";
    public serviceDocument: ServiceDocument<ShipmentDetailsModel>;
    localizationData: any;
    // shipmentId: number;
    primaryId: number; // used in CanDeactivate method, should be same name if we use candeactivate
    editModel: ShipmentDetailsModel = {
        advanceShipmentNotice: null, billOfLadingNo: null, comments: null, courier: null, estArrDate: null
        , extRefNoIn: null, extRefNoOut: null, fromLoc: null, fromLocType: null, invcMatchDate: null, invcMatchStatus: null
        , matchLocation: null, noOfBoxes: null, operation: null, orderNo: null, parentShipment: null, receiveDate: null, shipDate: null
        , shipment: null, shipOrigin: null, statusCode: null, toLoc: null, toLocType: null
    };
    editMode: boolean;
    model: ShipmentDetailsModel;
    dialogRef: MatDialogRef<any>;
    public messageResult: MessageResult = new MessageResult();
    canNavigate: boolean;
    saveContinueSubscribtion: Subscription;
    resetSubscribtion: Subscription;
    saveSubscribtion: Subscription;
    closeSubscribtion: Subscription;
    saveServiceSubscription: Subscription;
    routeServiceSubscription: Subscription;
    saveSuccessSubscription: Subscription;
    nextSubscription: Subscription;
    effectiveMinDate: Date;
    shipOriginData: DomainDetailModel[];
    statusCodeData: DomainDetailModel[];
    invoiceMatchingData: DomainDetailModel[];
    storeLocationsData: CommonModel[];
    wareHouseLocationsData: CommonModel[];
    locationTypeData: DomainDetailModel[];
    fromLocationData: CommonModel[];
    toLocationData: CommonModel[];
    currentDate: Date = new Date();

    constructor(
        private route: ActivatedRoute
        , public router: Router
        , public sharedService: SharedService
        , private service: ShipmentDetailsService
        , private commonService: CommonService
        , private loaderService: LoaderService
        , private suppChainSharedService: SupplyChainSharedService
        , public dialogService: RbDialogService
    ) {

        this.saveSubscribtion = suppChainSharedService.saveAnnounced$.subscribe((response: string) => {
            if (response === this._componentName) {
                this.save(true);
            }
        });

        this.saveContinueSubscribtion = suppChainSharedService.saveContinueAnnounced$.subscribe((response: string) => {
            if (response === this._componentName) {
                this.save(false);
            }
        });

        this.resetSubscribtion = suppChainSharedService.resetAnnounced$.subscribe((response: string) => {
            if (response === this._componentName) {
                this.reset();
            }
        });

        this.closeSubscribtion = suppChainSharedService.closeAnnounced$.subscribe((response: string) => {
            if (response === this._componentName) {
                this.close();
            }
        });

        this.nextSubscription = suppChainSharedService.nextAnnounced$.subscribe((response: string) => {
            if (response === this._componentName) {
                this.nextTab();
            }
        });
    }

    ngOnInit(): void {
        this.shipOriginData = Object.assign([], this.service.serviceDocument.domainData["shipOrigin"]);
        this.statusCodeData = Object.assign([], this.service.serviceDocument.domainData["statusCode"]);
        this.invoiceMatchingData = Object.assign([], this.service.serviceDocument.domainData["invoiceMatching"]);
        this.locationTypeData = Object.assign([], this.service.serviceDocument.domainData["locationType"]);
        this.storeLocationsData = Object.assign([], this.service.serviceDocument.domainData["storeLocations"]);
        this.wareHouseLocationsData = Object.assign([], this.service.serviceDocument.domainData["wareHouseLocations"]);
        this.storeLocationsData.forEach(x => x.status = +x.status);
        this.wareHouseLocationsData.forEach(x => x.status = +x.status);
        this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
        this.bind();
    }

    bind(): void {
        this.routeServiceSubscription = this.route.params.subscribe((resp: any) => {
            this.primaryId = resp["id"];
        });
        this.sharedService.shipmentId = this.primaryId;
        if (this.primaryId.toString() !== "0") {
            this.editMode = true;
            this.sharedService.toolbarEditObject = this.service.serviceDocument.dataProfile.dataModel;
            this.fromLocationData = (this.service.serviceDocument.dataProfile.dataModel.fromLocType === "S")
                ? Object.assign([], this.storeLocationsData) : Object.assign([], this.wareHouseLocationsData);
            this.toLocationData = (this.service.serviceDocument.dataProfile.dataModel.toLocType === "S")
                ? Object.assign([], this.storeLocationsData) : Object.assign([], this.wareHouseLocationsData);
        } else {
            this.toLocationData = (this.suppChainSharedService.toLocationType === "S")
                ? Object.assign([], this.storeLocationsData) : Object.assign([], this.wareHouseLocationsData);
            this.model = {
                advanceShipmentNotice: null, billOfLadingNo: null, comments: null, courier: null
                , estArrDate: this.suppChainSharedService.estimatedArrivalDate, extRefNoIn: null
                , extRefNoOut: null, fromLoc: null, fromLocType: null, invcMatchDate: null, invcMatchStatus: null
                , matchLocation: null, noOfBoxes: null, operation: null, orderNo: this.suppChainSharedService.orderNo, parentShipment: null
                , receiveDate: null, shipDate: null, shipment: null, shipOrigin: null, statusCode: null
                , toLoc: this.suppChainSharedService.toLocationId, toLocType: this.suppChainSharedService.toLocationType
            };
            this.service.newModel(this.model);
            this.service.serviceDocument.dataProfile.dataModel = Object.assign(this.service.serviceDocument.dataProfile.dataModel
                , { operation: "I" });
            this.service.serviceDocument.dataProfile.profileForm.patchValue(this.service.serviceDocument.dataProfile.dataModel);
        }
        this.serviceDocument = this.service.serviceDocument;
        this.suppChainSharedService.orderNo = this.service.serviceDocument.dataProfile.dataModel.orderNo;
        this.sharedService.fileCount = this.service.serviceDocument.dataProfile.dataModel.fileCount;
    }

    save(saveOnly: boolean): void {
        if (this.serviceDocument.dataProfile.profileForm.valid) {
            this.saveSubscription(saveOnly);
        } else {
            this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
        }
    }

    submit(): void {
        this.saveAndContinue();
    }

    saveAndContinue(): void {
        this.save(false);
    }

    saveSubscription(saveOnly: boolean): void {
        this.loaderService.display(true);
        this.saveServiceSubscription = this.service.save().subscribe(() => {
            if (this.service.serviceDocument.result.type === MessageType.success) {
                this.primaryId = this.service.serviceDocument.dataProfile.dataModel.shipment;
                this.sharedService.shipmentId = this.primaryId;
                this.saveSuccessSubscription = this.sharedService.saveForm(
                    `${this.localizationData.shipmentDetails.shipmentdetailssave} - Shipment ID ${this.primaryId}`
                ).subscribe(() => {
                    if (saveOnly) {
                        this.router.navigate(["/SupplyChain/List"], { skipLocationChange: true });
                    } else {
                        this.router.navigate(["/Blank"], {
                            skipLocationChange: true
                            , queryParams: { id: "/SupplyChain/New/ShipmentSku/" + this.primaryId }
                        });
                    }
                });
            } else {
                this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
            }
            this.loaderService.display(false);
        });
    }

    ngOnDestroy(): void {
        this.saveSubscribtion.unsubscribe();
        this.saveContinueSubscribtion.unsubscribe();
        this.resetSubscribtion.unsubscribe();
        this.closeSubscribtion.unsubscribe();
        this.routeServiceSubscription.unsubscribe();
        if (this.saveServiceSubscription) {
            this.saveServiceSubscription.unsubscribe();
        }
        if (this.saveSuccessSubscription) {
            this.saveSuccessSubscription.unsubscribe();
        }
        this.nextSubscription.unsubscribe();
    }

    reset(): void {
        Object.keys(this.serviceDocument.dataProfile.profileForm.controls).forEach(control => {
            this.serviceDocument.dataProfile.profileForm.controls[control].setValue(this.serviceDocument.dataProfile.dataModel[control]);
        });
        this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
    }

    close(): void {
        this.messageResult.message = `<div>All your changes will be lost if you don't save them.</div>
                                    <div>Click on “Save” to Keep the changes.</div>
                                    <div>Click on “Don't save” to proceed further without saving changes.</div>
                                    <div>Click on “Cancel” to stay on the current page.</div>`;
        if ((this.serviceDocument.dataProfile.profileForm && this.serviceDocument.dataProfile.profileForm.dirty)
            || (this.serviceDocument.dataProfile.profileForm.dirty && !this.serviceDocument.dataProfile.profileForm.valid)) {

            this.dialogRef = this.dialogService.openMessageDialog("Warning", this.messageResult, [new RbButton("", "Cancel", "alertCancel")
                , new RbButton("", "Don't Save", "alertdontsave"), new RbButton("", "Save", "alertSave")], "37%", ""
                , "iconsdontsave retailsNavIcons", "dontsaveAlert", "dontsaveAlertTitle");
            this.dialogRef.componentInstance.click.subscribe(
                btnName => {
                    if (btnName === "Cancel") {
                        this.dialogRef.close();
                    }
                    if (btnName === "Don't Save") {
                        this.serviceDocument.dataProfile.profileForm.markAsUntouched();
                        this.canNavigate = true;
                        this.dialogRef.close();
                        this.router.navigate([this.sharedService.previousUrl], { skipLocationChange: true });
                    }
                    if (btnName === "Save") {
                        this.dialogRef.close();
                        this.save(true);
                        this.router.navigate([this.sharedService.previousUrl], { skipLocationChange: true });
                    }
                });
        } else {
            this.canNavigate = true;
            this.router.navigate([this.sharedService.previousUrl], { skipLocationChange: true });
        }
    }


    updateDependentControls(event: any): void {
        this.service.serviceDocument.dataProfile.profileForm.controls["fromLoc"].setValue(null);
        if (this.service.serviceDocument.dataProfile.profileForm.controls["fromLocType"].value === "S") {
            this.fromLocationData = this.storeLocationsData;
        } else if (this.service.serviceDocument.dataProfile.profileForm.controls["fromLocType"].value === "W") {
            this.fromLocationData = this.wareHouseLocationsData;
        }
    }

    nextTab(): void {
        if (this.primaryId) {
            this.router.navigate(["/SupplyChain/New/ShipmentSku/" + this.primaryId], { skipLocationChange: true });
        }
    }
}