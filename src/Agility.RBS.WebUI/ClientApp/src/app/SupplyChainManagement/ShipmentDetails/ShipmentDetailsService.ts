﻿import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument, WorkflowAction } from "@agility/frameworkcore";
import { ShipmentDetailsModel } from "./ShipmentDetailsModel";
import { SharedService } from "../../Common/SharedService";
import { HttpParams } from "@angular/common/http";

@Injectable()
export class ShipmentDetailsService {
    serviceDocument: ServiceDocument<ShipmentDetailsModel> = new ServiceDocument<ShipmentDetailsModel>();

    constructor(private sharedService: SharedService) { }

    newModel(model: ShipmentDetailsModel): ServiceDocument<ShipmentDetailsModel> {
        return this.serviceDocument.newModel(model);
    }

    addEdit(id: number): Observable<ServiceDocument<ShipmentDetailsModel>> {
        if (+id === 0) {
            return this.serviceDocument.new("/api/ShipmentDetails/New");
        } else {
            return this.serviceDocument.open("/api/ShipmentDetails/Open", new HttpParams().set("id", id.toString()));
        }
    }

    save(): Observable<ServiceDocument<ShipmentDetailsModel>> {
        return this.serviceDocument.save("/api/ShipmentDetails/Save", true, () => this.setDate());
    }

    search(): Observable<ServiceDocument<ShipmentDetailsModel>> {
        return this.serviceDocument.search("/api/ShipmentDetails/Search");
    }

    setDate(): void {
        let sd: ShipmentDetailsModel = this.serviceDocument.dataProfile.dataModel;
        if (sd !== null) {
            if (sd.shipDate != null) {
                sd.shipDate = this.sharedService.setOffSet(sd.shipDate);
            }
            if (sd.receiveDate != null) {
                sd.receiveDate = this.sharedService.setOffSet(sd.receiveDate);
            }
            if (sd.estArrDate != null) {
                sd.estArrDate = this.sharedService.setOffSet(sd.estArrDate);
            }
            if (sd.invcMatchDate != null) {
                sd.invcMatchDate = this.sharedService.setOffSet(sd.invcMatchDate);
            }
        }
    }
}