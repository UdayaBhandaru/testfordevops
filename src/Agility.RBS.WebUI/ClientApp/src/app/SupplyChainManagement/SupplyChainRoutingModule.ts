﻿import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { SupplyChainSchedulerComponent } from "./Scheduler/SupplyChainSchedulerComponent";
import { SupplyChainSchedulerResolver } from "./SupplyChainResolver";
import { ShipmentComponent } from "./ShipmentComponent";
import { ShipmentDetailsComponent } from "./ShipmentDetails/ShipmentDetailsComponent";
import { ShipmentDetailsResolver } from "./ShipmentDetails/ShipmentDetailsResolver";
import { ShipmentSkuComponent } from "./ShipmentSKU/ShipmentSkuComponent";
import { ShipmentSkuResolver } from "./ShipmentSKU/ShipmentSkuResolver";
import RbHeadGuard from "../guards/RbHeadGuard";
import RbDetailsGuard from "../guards/RbDetailsGuard";

const SupplyChainRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: SupplyChainSchedulerComponent,
                resolve:
                    {
                        serviceDocument: SupplyChainSchedulerResolver
                    }
            },
            {
                path: "New",
                component: ShipmentComponent,
                children: [
                    {
                        path: "", redirectTo: "ShipmentDetails/", pathMatch: "full"
                    },
                    {
                        path: "ShipmentDetails/:id",
                        component: ShipmentDetailsComponent,
                        resolve: {
                            serviceDocument: ShipmentDetailsResolver
                        },
                        canDeactivate: [RbHeadGuard]
                    },
                    {
                        path: "ShipmentSku/:id",
                        component: ShipmentSkuComponent,
                        resolve: {
                            serviceDocument: ShipmentSkuResolver
                        },
                        canDeactivate: [RbDetailsGuard]
                    }
                ]
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(SupplyChainRoutes)
    ]
})
export class SupplyChainRoutingModule { }
