﻿export class SupplyChainSchedulerModel {
    receivingLocationId?: number;

    locationName: string;

    locationType: string;

    locationTypeDescription: string;

    orderNo?: number;

    supplierId?: number;

    supplierName: string;

    eta?: Date;

    langId?: number;

    fromDate?: Date;

    toDate?: Date;
}