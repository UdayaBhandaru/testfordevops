﻿export class ScmSchedulerInputModel {
    locationId?: number;

    startDate?: Date;

    endDate?: Date;

    userEmail?: string;
}