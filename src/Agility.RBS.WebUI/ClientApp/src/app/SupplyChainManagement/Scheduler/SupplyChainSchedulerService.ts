﻿import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { SupplyChainSchedulerModel } from "./SupplyChainSchedulerModel";
import { ScmSchedulerInputModel } from "./ScmSchedulerInputModel";
import { SchedulerViewModel } from "./SchedulerViewModel";
import { HttpHelperService } from "../../Common/HttpHelperService";
import { SharedService } from "../../Common/SharedService";

@Injectable()
export class SupplyChainSchedulerService {
    serviceDocument: ServiceDocument<SupplyChainSchedulerModel> = new ServiceDocument<SupplyChainSchedulerModel>();
    searchData: any;
    schInputDataModel: ScmSchedulerInputModel;

    constructor(private httpHelperService: HttpHelperService, private sharedService: SharedService) { }

    newModel(model: SupplyChainSchedulerModel): ServiceDocument<SupplyChainSchedulerModel> {
        return this.serviceDocument.newModel(model);
    }

    list(): Observable<ServiceDocument<SupplyChainSchedulerModel>> {
        return this.serviceDocument.list("/api/SupplyChainScheduler/List");
    }

    search(additionalParams: any): Observable<ServiceDocument<SupplyChainSchedulerModel>> {
        return this.serviceDocument.search("/api/SupplyChainScheduler/GetSchedulerDataForGrid", true, () => this.setSubClassId(additionalParams));
    }

    setSubClassId(additionalParams: any): void {
        var sd: SupplyChainSchedulerModel = this.serviceDocument.dataProfile.dataModel;
        if (sd !== null) {
            if (sd.fromDate != null) {
                sd.fromDate = this.sharedService.setOffSet(sd.fromDate);
            }
            if (sd.toDate != null) {
                sd.toDate = this.sharedService.setOffSet(sd.toDate);
            }
        }
    }
}