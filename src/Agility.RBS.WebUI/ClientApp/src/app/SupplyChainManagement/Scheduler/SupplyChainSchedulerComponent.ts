import { Component, OnInit, ViewChild, ChangeDetectorRef, NgZone, OnDestroy } from "@angular/core";
import { FormGroup, FormBuilder, FormControl, Validators, ValidatorFn, AbstractControl } from "@angular/forms";
import { FxContext, CommonService, ServiceDocument, MessageType } from "@agility/frameworkcore";
///import { CalendarComponent } from "ap-angular2-fullcalendar/src/calendar/calendar";
import { Router, ActivatedRoute } from "@angular/router";
import { GridOptions, ColDef } from "ag-grid-community";
import { Subscription } from "rxjs";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { SupplyChainSchedulerService } from "./SupplyChainSchedulerService";
import { CommonModel } from "../../Common/CommonModel";
import { SupplyChainSchedulerModel } from "./SupplyChainSchedulerModel";
import { LoaderService } from "../../Common/LoaderService";
import { SchedulerViewModel } from "./SchedulerViewModel";
import { SupplyChainSharedService } from "../SupplyChainSharedService";
import { SharedService } from "../../Common/SharedService";
import { ShipmentService } from "../ShipmentService";
import { OrderShipmentRequestModel } from "../OrderShipmentRequestModel";
import { GridEditComponent } from "../../Common/grid/GridEditComponent";
import { GridDateComponent } from "../../Common/grid/GridDateComponent";
import { SearchComponent } from "../../Common/SearchComponent";
import { LocationDomainModel } from "../../Common/DomainData/LocationDomainModel";
import { OrderShipmentResponseModel } from "../OrderShipmentResponseModel";
import { ShipmentPopupComponent } from "../ShipmentPopupComponent";
import { DocumentsConstants } from "../../Common/DocumentsConstants";
import { SchedulerActionsComponent } from "./SchedulerActionsComponent";

@Component({
  selector: "scheduler",
  templateUrl: "./SupplyChainSchedulerComponent.html",
  styleUrls: ["./fullcalendar.css"]
})
export class SupplyChainSchedulerComponent implements OnInit, OnDestroy {
  /// @ViewChild(CalendarComponent)
  myCalendar: any;
  /// CalendarComponent;
  @ViewChild("searchPanelScheduler") searchPanelScheduler: SearchComponent;
  public serviceDocument: ServiceDocument<SupplyChainSchedulerModel>;
  gridOptions: GridOptions;
  public selected: string;
  calendarOptions: any;
  eventSourceObject: any = null;
  schedulerForm: FormGroup;
  shipOrderServiceSubscription: Subscription;
  searchServiceSubscription: Subscription;
  shipmentListServiceSubscription: Subscription;
  locationTypeData: DomainDetailModel[];
  locationData: LocationDomainModel[];
  locationList: LocationDomainModel[];
  model: SupplyChainSchedulerModel = {
    locationType: null, receivingLocationId: null, fromDate: null, toDate: null, eta: null
    , langId: null, locationName: null, locationTypeDescription: null, orderNo: null, supplierId: null, supplierName: null
  };
  schedulerData: SupplyChainSchedulerModel[];
  calendarData: {}[];
  hideCalendar: boolean = true;
  redirectUrl = "SupplyChain/New/ShipmentDetails/";
  PageTitle = "Scheduler";
  calendarFirstInvoke: boolean = false;
  columns: ColDef[];
  showSearchCriteria: boolean = true;
  componentName: any;
  documentsConstants: DocumentsConstants = new DocumentsConstants();
  propList: string[] = [
    "locationType",
    "receivingLocationId",
    "fromDate",
    "toDate"
  ];

  constructor(
    private service: SupplyChainSchedulerService
    , private ref: ChangeDetectorRef
    , private ngZone: NgZone
    , private loaderService: LoaderService
    , private commonService: CommonService
    , private suppChainSharedService: SupplyChainSharedService
    , private router: Router
    , public sharedService: SharedService
    , private shipmentService: ShipmentService
  ) { }

  ngOnInit(): void {
    this.locationTypeData = Object.assign([], this.service.serviceDocument.domainData["locationType"]);
    this.locationList = Object.assign([], this.service.serviceDocument.domainData["locationList"]);
    this.componentName = this;
    this.columns = [
      { headerName: "Order No", field: "orderNo", tooltipField: "orderNo", width: 60, sort: "desc" },
      { headerName: "Supplier Name", field: "supplierName", tooltipField: "supplierName", width: 140 },
      {
        headerName: "ETA", field: "eta", tooltipField: "eta | date:'dd-MMM - yyyy'", cellRendererFramework: GridDateComponent, width: 100
      },
      { headerName: "Location Type", field: "locationTypeDescription", tooltipField: "locationTypeDescription", width: 120 },
      { headerName: "Location Name", field: "locationName", tooltipField: "locationName", width: 140 },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: SchedulerActionsComponent, width: 80
      }
    ];

    this.calendarData = [];
    this.setCalendarOptions();
    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
      this.locationData = this.locationList.filter(item => item.locType
        === this.service.serviceDocument.dataProfile.dataModel.locationType);
      this.hideCalendar = this.sharedService.schedulerView === "Grid" ? true : false;
      setTimeout(() => {
        let viewObject: any = this.myCalendar.fullCalendar("getView");
        if (viewObject.name !== this.sharedService.calendarViewName) {
          this.myCalendar.fullCalendar("changeView", this.sharedService.calendarViewName, this.sharedService.calendarViewDate);
        }
      }, 150);
    } else {
      this.locationData = this.locationList.filter(item => item.locType === "W");
      delete this.sharedService.domainData;
      this.sharedService.domainData = {
        location: this.locationData, locationType: this.locationTypeData
      };
      let fromDate: Date = new Date();
      let toDate: Date = new Date();
      toDate.setDate(toDate.getDate() + 7);
      let defaultSearchData: SupplyChainSchedulerModel = {
        locationType: "W", receivingLocationId: this.locationData[0].locationId, fromDate: fromDate, toDate: toDate, eta: null
        , langId: null, locationName: null, locationTypeDescription: null, orderNo: null, supplierId: null, supplierName: null
      };
      this.service.newModel(defaultSearchData);
    }
    this.serviceDocument = this.service.serviceDocument;
    if (this.hideCalendar) {
      setTimeout(() => {
        this.searchPanelScheduler.dateColumns = ["fromDate", "toDate"];
        this.searchPanelScheduler.restrictSearch = false;
        this.searchPanelScheduler.search();
      }, 150);
    }
  }

  ngAfterViewChecked(): void {
    this.ref.detectChanges();
  }

  setCalendarOptions(): void {
    let _self: SupplyChainSchedulerComponent = this;
    _self.calendarOptions = {
      fixedWeekCount: false,
      header: {
        left: "prev,next,prevYear,nextYear today",
        center: "title",
        right: "month,agendaWeek,agendaDay,list"
      },
      editable: true,
      eventLimit: true,
      events: null,
      eventClick: this.EventClick.bind(this),
      eventRender: function (event: any, element: any): void {
        // no action
      },
      viewRender: this.ngZone.run(() => this.handleDateChangeEvent.bind(this)),
      eventColor: "#23a89f",
      displayEventTime: false,
      defaultView: "agendaDay"
    };

    if (_self.eventSourceObject) {
      // not implemented
    }
  }

  EventClick(event: any): void {
    let _self: SupplyChainSchedulerComponent = this;
    _self.eventSourceObject = event.source;
    _self.ref.detectChanges();
    _self.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    _self.sharedService.schedulerView = "Calendar";
    let viewObject: any = _self.myCalendar.fullCalendar("getView");
    _self.sharedService.calendarViewName = viewObject.name;
    _self.sharedService.calendarViewDate = event.start._d;
    let orderShipObj: OrderShipmentRequestModel = {
      estimatedArrivalDate: event.start._d
      , locationId: _self.serviceDocument.dataProfile.profileForm.controls["receivingLocationId"].value, orderNo: +(event.title.split(" ")[1])
    };
    _self.shipOrderServiceSubscription = _self.shipmentService.getShipmentForOrder(orderShipObj).subscribe(
      (response: number) => {
        _self.sharedService.previousUrl = "../SupplyChain/List";
        if (response > 0) {
          _self.sharedService.shipmentId = response;
          _self.sharedService.editMode = true;
        } else {
          _self.suppChainSharedService.orderNo = +(event.title.split(" ")[1]);
          _self.suppChainSharedService.toLocationType = _self.serviceDocument.dataProfile.profileForm.controls["locationType"].value;
          _self.suppChainSharedService.toLocationId = _self.serviceDocument.dataProfile.profileForm.controls["receivingLocationId"].value;
          _self.suppChainSharedService.estimatedArrivalDate = event.start._d;
          delete _self.sharedService.shipmentId;
          _self.sharedService.deleteAllToolBarData();
          _self.sharedService.setToolbarSave(true);
        }
        this.ngZone.run(() => _self.router.navigate(["/" + _self.redirectUrl + response.toString()], { skipLocationChange: true }));
      },
      (resp: any) => {
        // no returntype
      }
    );
  }

  locationChangeEvent(): void {
    if (!this.hideCalendar) {
      this.commonCalendarLogic();
    }
  }

  fetchSupplyScheduleData(scheduleStartDate: Date, scheduleEndDate: Date): void {
    this.calendarData = [];
    this.service.serviceDocument.dataProfile.profileForm.patchValue({
      fromDate: scheduleStartDate,
      toDate: scheduleEndDate
    });
    this.searchServiceSubscription = this.service.search(null).subscribe(() => {
      this.loaderService.display(true);
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.schedulerData = this.service.serviceDocument.dataProfile.dataList;
        if (this.schedulerData != null && this.schedulerData.length > 0) {
          this.schedulerData.forEach(x => {
            let eventTitle: string = "Order" + " " + x.orderNo.toString() + " " + "Supplier" + " " + x.supplierName;
            let calendarObj: any = {
              title: eventTitle,
              start: x.eta,
              allDay: false
            };
            this.calendarData.push(calendarObj);
          });
        }
      }
      this.myCalendar.fullCalendar("removeEventSources");
      this.myCalendar.fullCalendar("addEventSource", this.calendarData);
      this.loaderService.display(false);
    }, () => {
      // no implementation
    });
  }

  updateDependentControls(event: any): void {
    this.serviceDocument.dataProfile.profileForm.controls["receivingLocationId"].setValue(null);
    this.locationData = this.locationList.filter(item => item.locType === this.serviceDocument.dataProfile.profileForm.controls["locationType"].value);
    this.sharedService.domainData["location"] = this.locationData;
  }

  handleDateChangeEvent(view: any, element: any): void {
    this.calendarData = [];
    let scheduleStartDate: Date = view.start.toDate();
    let scheduleEndDate: Date = view.end.toDate();
    this.fetchSupplyScheduleData(scheduleStartDate, scheduleEndDate);
  }

  open(cell: any, mode: string): void {
    if (mode === "shipments") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
      this.sharedService.previousUrl = "../SupplyChain/List";
      this.sharedService.schedulerView = "Grid";
    }
    let orderShipObj: OrderShipmentRequestModel = {
      estimatedArrivalDate: null
      , locationId: null, orderNo: +(cell.data.orderNo)
    };
    this.shipmentListServiceSubscription = this.shipmentService.getShipmentListForOrder(orderShipObj).subscribe(
      (response: OrderShipmentResponseModel[]) => {
        if (response != null && response.length > 0) {
          this.sharedService.openPopupWithDataList(
            ShipmentPopupComponent,
            response,
            "Shipment List",
            this.documentsConstants.orderObjectName,
            +(cell.data.orderNo), false, null, this.documentsConstants.shipmentObjectName);
        } else {
          this.suppChainSharedService.orderNo = +(cell.data.orderNo);
          this.suppChainSharedService.toLocationType = cell.data.locationType;
          this.suppChainSharedService.toLocationId = +(cell.data.receivingLocationId);
          this.suppChainSharedService.estimatedArrivalDate = cell.data.eta;
          delete this.sharedService.shipmentId;
          this.sharedService.deleteAllToolBarData();
          this.sharedService.setToolbarSave(true);
          this.router.navigate(["/" + this.redirectUrl + "0"], { skipLocationChange: true });
        }
      },
      (resp: any) => {
        // no returntype
      }
    );
  }

  showSearchCriteriaChild(event: any): void {
    this.showSearchCriteria = event === "search" ? true : event;
    if (event === "search") {
      this.propList.forEach(x => this.serviceDocument.dataProfile.profileForm.controls[x].setValidators([Validators.required]));
      this.propList.forEach(x => this.serviceDocument.dataProfile.profileForm.controls[x].updateValueAndValidity());
     /// this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
      if (this.serviceDocument.dataProfile.profileForm.valid) {
        setTimeout(() => {
          this.searchPanelScheduler.dateColumns = ["fromDate", "toDate"];
          this.searchPanelScheduler.restrictSearch = false;
          this.searchPanelScheduler.search();
        }, 150);
      }
    } else if (event) {
      this.searchPanelScheduler.restrictSearch = true;
    }
  }

  openListView($event: MouseEvent): void {
    $event.preventDefault();
    this.hideCalendar = true;
    this.setStep("listView", this.selected);
  }

  openCalendarView($event: MouseEvent): void {
    $event.preventDefault();
    this.hideCalendar = false;
    this.setStep("calendarView", this.selected);
  }

  commonCalendarLogic(): void {
    let selectedDate: any = this.myCalendar.fullCalendar("getDate");
    let scheduleStartDate: Date = selectedDate.toDate();
    let scheduleEndDate: Date = selectedDate.toDate();
    scheduleEndDate.setDate(scheduleEndDate.getDate() + 1);
    this.fetchSupplyScheduleData(scheduleStartDate, scheduleEndDate);
  }

  setStep(presentView: string, pastView: string): void {
    this.selected = (presentView !== pastView) ? presentView : "";
  }

  ngOnDestroy(): void {
    if (this.shipOrderServiceSubscription) {
      this.shipOrderServiceSubscription.unsubscribe();
    }
    if (this.searchServiceSubscription) {
      this.searchServiceSubscription.unsubscribe();
    }
    if (this.shipmentListServiceSubscription) {
      this.shipmentListServiceSubscription.unsubscribe();
    }
  }
}
