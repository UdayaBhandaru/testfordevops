﻿import { Component } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular/main";

@Component({
    selector: "child-cell",
    template: `<div fxLayout="row" class="grid-actions">
            <a href="#" (click)="invokeParentMethod($event,'shipments')" matTooltip="Shipments">
            <span class="receivingIcon retailsNavIcons"></span></a>
        </div>`
})
export class SchedulerActionsComponent implements ICellRendererAngularComp {
    public params: any;

    agInit(params: any): void {
        this.params = params;
    }

    refresh(params: any): boolean {
        return true;
    }

    public invokeParentMethod($event: MouseEvent, mode: string): void {
        $event.preventDefault();
        this.params.context.componentParent.open(this.params, mode);
    }
}