﻿export class OrderShipmentResponseModel {
    orderNo?: number;

    shipmentId?: number;

    estimatedArrivalDate?: Date;

    locationId?: number;

    locationName: string;
}