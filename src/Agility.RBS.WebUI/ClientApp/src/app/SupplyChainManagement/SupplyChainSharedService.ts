﻿import { Injectable } from "@angular/core";
import { Subject } from "rxjs";
import { Observable } from "rxjs";
import { HttpParams } from "@angular/common/http";
import { MessageType, ServiceDocument } from "@agility/frameworkcore";
import { GridOptions } from "ag-grid-community";
import { SharedService } from "../Common/SharedService";
import { RbCommonService } from "../Common/RbCommonService";
import { ShipmentHeadPrintModel } from "./ShipmentHeadPrintModel";
import { ShipmentSkuModel } from "./ShipmentSKU/ShipmentSkuModel";

@Injectable()
export class SupplyChainSharedService extends RbCommonService {
    estimatedArrivalDate: Date;
    shipmentId: number;
    orderNo: number;
    toLocationType: string;
    toLocationId: number;
    serviceDocument: ServiceDocument<ShipmentHeadPrintModel> = new ServiceDocument<ShipmentHeadPrintModel>();
    headColumns = ["Shipment ID", "Order No", "Bill Of Lading No", "Advance Shipment Notice", "Ship Date", "Receive Date",
        "Est Arr Date", "Ship Origin", "Status Code"];
    detailColumns = ["Seq No", "Item", "Distro Type", "Status Code", "Unit Cost", "Unit Retail", "Qty Expected", "Qty Received",
        "Qty Matched", "Weight Expected", "Weight Received", "Actual Receiving Store"];

    constructor(public sharedService: SharedService) {
        super(sharedService);
    }

    fetchPrintData(shipmentId: number): Observable<ServiceDocument<ShipmentHeadPrintModel>> {
        return this.serviceDocument.open("/api/ShipmentPrint/List", new HttpParams().set("shipmentId", shipmentId.toString()));
    }

    buildHeadObject(shipmentHead: ShipmentHeadPrintModel): any[] {
        let headTableObj: any[] = [];
        let shipDate: string = new Date(shipmentHead.shipDate).toDateString();
        let receiveDate: string = new Date(shipmentHead.receiveDate).toDateString();
        let estArrDate: string = new Date(shipmentHead.estArrDate).toDateString();
        let headObj: any = [shipmentHead.shipment, shipmentHead.orderNo, shipmentHead.billOfLadingNo, shipmentHead.advanceShipmentNotice,
            shipDate, receiveDate, estArrDate, shipmentHead.shipOriginDescription, shipmentHead.statusCodeDescription];
        headTableObj.push(headObj);
        return headTableObj;
    }

    buildDetailObject(shipmentSkuDetails: ShipmentSkuModel[]): any[] {
        let detailTableObj: any[] = [];
        shipmentSkuDetails.forEach(x => {
            let weightExpected: string = x.weightExpected ? x.weightExpected.toString().concat(" ", x.weightExpectedUom) : "";
            let weightReceived: string = x.weightReceived ? x.weightReceived.toString().concat(" ", x.weightReceivedUom) : "";
            let detailObj: any = [x.seqNo, x.item, x.distroTypeDescription, x.statusCodeDescription, x.unitCost, x.unitRetail, x.qtyExpected
                , x.qtyReceived, x.qtyMatched, weightExpected, weightReceived, x.actualReceivingStoreName];
            detailTableObj.push(detailObj);
        });
        return detailTableObj;
    }
}