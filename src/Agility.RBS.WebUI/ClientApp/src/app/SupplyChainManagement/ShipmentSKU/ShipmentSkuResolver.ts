﻿import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { ShipmentSkuModel } from "./ShipmentSkuModel";
import { ShipmentSkuService } from "./ShipmentSkuService";

@Injectable()
export class ShipmentSkuResolver implements Resolve<ServiceDocument<ShipmentSkuModel>> {
    constructor(private service: ShipmentSkuService) { }
    resolve(route: ActivatedRouteSnapshot): Observable<ServiceDocument<ShipmentSkuModel>> {
        return this.service.list(route.params["id"]);
    }
}