﻿import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument, WorkflowAction } from "@agility/frameworkcore";
import { ShipmentSkuModel } from "./ShipmentSkuModel";
import { SharedService } from "../../Common/SharedService";
import { SupplyChainSharedService } from "../SupplyChainSharedService";
import { HttpParams } from "@angular/common/http";

@Injectable()
export class ShipmentSkuService {
    serviceDocument: ServiceDocument<ShipmentSkuModel> = new ServiceDocument<ShipmentSkuModel>();

    constructor(private sharedService: SharedService, private suppChainSharedService: SupplyChainSharedService) { }

    search(): Observable<ServiceDocument<ShipmentSkuModel>> {
        return this.serviceDocument.search("/api/ShipmentSku/Search");
    }

    newModel(model: ShipmentSkuModel): ServiceDocument<ShipmentSkuModel> {
        return this.serviceDocument.newModel(model);
    }

    save(): Observable<ServiceDocument<ShipmentSkuModel>> {
        return this.serviceDocument.save("/api/ShipmentSku/Save", true, () => this.setDate());
    }

    list(id: number): Observable<ServiceDocument<ShipmentSkuModel>> {
        let params: HttpParams = new HttpParams();
        params = params.append("shipmentId", id.toString());
        params = params.append("orderId", this.suppChainSharedService.orderNo.toString());
        return this.serviceDocument.open("/api/ShipmentSku/List", params);
    }

    setDate(): void {
        let shipSkuList: ShipmentSkuModel[] = this.serviceDocument.dataProfile.profileForm.controls["listOfShipmentSkuModels"].value;
        if (shipSkuList !== null && shipSkuList.length > 0) {
            shipSkuList.forEach(shipSkuObj => {
                if (shipSkuObj.reconcileDate != null) {
                    shipSkuObj.reconcileDate = this.sharedService.setOffSet(shipSkuObj.reconcileDate);
                }
            });
        }
    }
}