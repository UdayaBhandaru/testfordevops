﻿import { Component, OnInit, TemplateRef, ViewChild, OnDestroy, ChangeDetectorRef, AfterViewChecked } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { FormGroup, FormControl } from "@angular/forms";
import { ServiceDocument, CommonService, MessageResult, MessageType } from "@agility/frameworkcore";
import { SharedService } from "../../Common/SharedService";
import { LoaderService } from "../../Common/LoaderService";
import { MatDialogRef } from "@angular/material";
import { GridOptions, RowNode, GridApi } from "ag-grid-community";
import { Subscription } from "rxjs";
import { ShipmentSkuModel } from "./ShipmentSkuModel";
import { ShipmentSkuService } from "./ShipmentSkuService";
import { SupplyChainSharedService } from "../SupplyChainSharedService";
import { GridEditComponent } from "../../Common/grid/GridEditComponent";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { CommonModel } from "../../Common/CommonModel";
import { GridDeleteComponent } from "../../Common/grid/GridDeleteComponent";
import { GridDateComponent } from "../../Common/grid/GridDateComponent";
import { OrderItemListCommonModel } from "../../Common/DomainData/OrderItemListCommonModel";
import { RbDialogService } from "../../Common/controls/RbDialogService";
import { GridSelectComponent } from "../../Common/grid/GridSelectComponent";
import { GridInputComponent } from "../../Common/grid/GridInputComponent";
import { GridNumericComponent } from "../../Common/grid/GridNumericComponent";
import { ShipmentInfoComponent } from "./ShipmentInfoComponent";
import { UomDomainModel } from "../../Common/DomainData/UomDomainModel";

@Component({
    selector: "shipment-sku",
    templateUrl: "./ShipmentSkuComponent.html"
})
export class ShipmentSkuComponent implements OnInit, AfterViewChecked, OnDestroy {
    _componentName = "Shipment SKU";
    PageTitle = "Shipment SKU";
    public serviceDocument: ServiceDocument<ShipmentSkuModel>;
    public gridOptions: GridOptions;
    localizationData: any;
    shipment: number;
    model: ShipmentSkuModel =
        {
            shipment: this.shipment, seqNo: null, item: null, distroNo: null, distroType: null, refItem: null, carton: null, invStatus: null, statusCode: null
            , qtyReceived: null, unitCost: null, unitRetail: null, qtyExpected: null, matchInvcId: null, adjustType: null, actualReceivingStore: null
            , reconcileUserId: null, reconcileDate: null, tamperedInd: null, dispositionedInd: null, qtyMatched: null, weightReceived: null
            , weightReceivedUom: null, weightExpected: null, weightExpectedUom: null, operation: "I", createdBy: null, createdDate: null, lastUpdatedBy: null
            , lastUpdatedDate: null, programPhase: null, programMessage: null, error: null, listOfShipmentSkuModels: [], distroTypeDescription: null
            , statusCodeDescription: null, actualReceivingStoreName: null, isExpiry: null, expiryDate: null
        };
    editModel: ShipmentSkuModel;
    dialogRef: MatDialogRef<any>;
    data: ShipmentSkuModel[] = [];
    dataReset: ShipmentSkuModel[] = [];
    columns: any[];
    componentName: any;

    locationsData: CommonModel[];
    distroTypeData: DomainDetailModel[];
    skuStatusCodeData: DomainDetailModel[];
    weightUomsData: UomDomainModel[];
    indicatorData: DomainDetailModel[];
    orderItemListData: OrderItemListCommonModel[];
    gridSaveData: ShipmentSkuModel[] = [];

    deletedRows: any[] = [];
    shipmentSkuCount: number;
    saveContinueSubscribtion: Subscription;
    resetSubscribtion: Subscription;
    saveSubscribtion: Subscription;
    closeSubscribtion: Subscription;
    routeServiceSubscription: Subscription;
    previousSubscription: Subscription;
    public messageResult: MessageResult = new MessageResult();
    canNavigate: boolean = false;

    dupeText: string = "Duplicate";
    addText: string = " Update Grid";
    pForm: FormGroup;
    node: any;
    public sheetName: string = "ShipmentSku";
    mandatoryShipmentSkuFields: string[] = [
        "qtyReceived",
        "distroType"
    ];
    isEnableUpdateBtn: boolean = false;
    editMode: boolean = false;
    orginalValue: string = "";
    gridApi: GridApi;

    constructor(private route: ActivatedRoute, private service: ShipmentSkuService
        , private sharedService: SharedService, private supplyChainSharedService: SupplyChainSharedService
        , private commonService: CommonService, public router: Router
        , private changeRef: ChangeDetectorRef, public dialogService: RbDialogService) {
        this.saveSubscribtion = supplyChainSharedService.saveAnnounced$.subscribe((response: any) => {
            if (response === this._componentName) {
                this.save(true);
            }
        });
        this.saveContinueSubscribtion = supplyChainSharedService.saveContinueAnnounced$.subscribe((response: string) => {
            if (response === this._componentName) {
                this.save(false);
            }
        });
        this.resetSubscribtion = supplyChainSharedService.resetAnnounced$.subscribe((response: string) => {
            if (response === this._componentName) {
                this.reset();
            }
        });
        this.closeSubscribtion = supplyChainSharedService.closeAnnounced$.subscribe((response: string) => {
            if (response === this._componentName) {
                this.close();
            }
        });
        this.previousSubscription = supplyChainSharedService.previousAnnounced$.subscribe((response: string) => {
            if (response === this._componentName) {
                this.previousTab();
            }
        });
    }

    ngOnInit(): void {
        this.routeServiceSubscription = this.route.params.subscribe((resp: any) => {
            this.shipment = +resp["id"];
        });
        this.componentName = this;
        this.service.serviceDocument.dataProfile.dataModel = this.model;
        this.orderItemListData = Object.assign([], this.service.serviceDocument.domainData["orderItemList"]);
        if (this.service.serviceDocument.dataProfile.dataList) {
            Object.assign(this.data, this.service.serviceDocument.dataProfile.dataList);
            Object.assign(this.dataReset, this.service.serviceDocument.dataProfile.dataList);
            this.orginalValue = JSON.stringify(this.dataReset);
        }
        this.service.newModel(this.model);
        this.service.serviceDocument.dataProfile.profileForm.patchValue(this.service.serviceDocument.dataProfile.dataModel);
        this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
        this.serviceDocument = this.service.serviceDocument;
        this.distroTypeData = Object.assign([], this.service.serviceDocument.domainData["distroType"]);
        this.skuStatusCodeData = Object.assign([], this.service.serviceDocument.domainData["skuStatusCode"]);
        this.locationsData = Object.assign([], this.service.serviceDocument.domainData["locations"]);
        this.locationsData.forEach(x => x.id = +x.id);
        this.weightUomsData = Object.assign([], this.service.serviceDocument.domainData["weightUoms"]);
        this.indicatorData = Object.assign([], this.service.serviceDocument.domainData["indicator"]);
        this.setGridColumns();
        this.gridOptions = {
            onGridReady: (params: any) => {
                this.gridApi = params.api;
            },
            context: {
                componentParent: this
            }
        };

        if (this.orderItemListData != null && this.orderItemListData.length > 0) {
            if (this.data != null && this.data.length > 0) {
                this.orderItemListData.forEach(orderItemObj => {
                    let skuObj: ShipmentSkuModel[] = this.data ? this.data.filter(x => x.item === orderItemObj.item.toString()) : null;
                    if (!skuObj || skuObj.length !== 1) {
                        this.data.push(this.buildShipSkuObject(orderItemObj));
                    }
                });
            } else {
                this.orderItemListData.forEach(orderItemObj => {
                    this.data.push(this.buildShipSkuObject(orderItemObj));
                });
            }
            if (this.gridApi) {
                this.gridApi.setRowData(this.data);
            }
        }

        this.shipmentSkuCount = this.data.length;
        this.service.serviceDocument.dataProfile.profileForm.controls["shipment"].setValue(this.shipment);
    }

    setGridColumns(): void {
        this.columns = [
            {
                headerName: "Item", field: "item", tooltipField: "Item"
            },
            {
                headerName: "Distro Type", field: "distroType", tooltipField: "Distro Type"
                , cellRendererFramework: GridSelectComponent, cellRendererParams: { references: this.distroTypeData }
                , cellClassRules: {
                    "invalid-grid-row": function (params: any): any { return !params.value; }
                }
            },
            {
                headerName: "Distro Number", field: "distroNo", tooltipField: "Distro Number", cellRendererFramework: GridNumericComponent
            },
            {
                headerName: "Reference Item", field: "refItem", tooltipField: "Reference Item", cellRendererFramework: GridInputComponent
            },
            {
                headerName: "Quantity Received", field: "qtyReceived", tooltipField: "Quantity Received"
                , cellRendererFramework: GridNumericComponent
                , cellClassRules: {
                    "invalid-grid-row": function (params: any): any { return !params.value; }
                }
            },
            {
                headerName: "Is Expiry", field: "isExpiry", cellRendererFramework: GridSelectComponent
                , cellRendererParams: { references: this.indicatorData }
            },
            {
                headerName: "Expiry Date", field: "expiryDate", tooltipField: "expiryDate | date:'dd-MMM - yyyy'"
                , cellRendererFramework: GridDateComponent, filter: "date", suppressSizeToFit: true, width: 150
                , cellClassRules: {
                    "invalid-grid-row": function (params: any): any { return (params.data.isExpiry === "Y" && !params.value); }
                }
            },
            {
                headerName: "Weight Received", field: "weightReceived", tooltipField: "Weight Received"
                , cellRendererFramework: GridNumericComponent
            },
            {
                headerName: "Weight Received UOM", field: "weightReceivedUom", tooltipField: "Weight Received UOM"
                , cellRendererFramework: GridSelectComponent
                , cellRendererParams: { references: this.weightUomsData, keyvaluepair: { key: "uomCode", value: "uomDescription" } }
            },
            {
                headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent
                , width: 150, cellRendererParams: { restrictEditIcon: true, restrictDeleteIcon: true }
            }
        ];
    }

    buildShipSkuObject(orderItemObj: OrderItemListCommonModel): ShipmentSkuModel {
        let skuModel: ShipmentSkuModel = {
            item: orderItemObj.item.toString(), unitCost: orderItemObj.unitCost
            , unitRetail: orderItemObj.unitRetail, qtyExpected: orderItemObj.quantityExpected
            , weightExpected: orderItemObj.weightExpected, weightExpectedUom: orderItemObj.weightExpUom
            , shipment: this.shipment, actualReceivingStore: null, adjustType: null, carton: null, createdBy: null, createdDate: null
            , dispositionedInd: null, distroNo: null, distroType: null, error: null, invStatus: null, lastUpdatedBy: null, lastUpdatedDate: null
            , listOfShipmentSkuModels: null, matchInvcId: null, operation: "I", programMessage: null, programPhase: null
            , qtyMatched: null, qtyReceived: null, reconcileDate: null, reconcileUserId: null, refItem: null, seqNo: null
            , statusCode: null, tamperedInd: null, weightReceived: null, weightReceivedUom: null, distroTypeDescription: null
            , statusCodeDescription: null, actualReceivingStoreName: null, isExpiry: orderItemObj.isExpiry, expiryDate: null
        };
        return skuModel;
    }

    ngAfterViewChecked(): void {
        this.changeRef.detectChanges();
    }

    save(saveOnly: boolean): void {
        this.saveSubscription(saveOnly);
    }

    saveAndContinue(): void {
        this.save(false);
    }

    saveSubscription(saveOnly: boolean): void {
        let obj: any = this;
        if (this.data.length > 0 || this.deletedRows.length > 0) {
            if (this.validateData()) {
                let redirectURL: string = "/SupplyChain/New/ShipmentSku/";
                this.supplyChainSharedService.save(obj, saveOnly, "listOfShipmentSkuModels", redirectURL
                    , `${this.localizationData.shipmentSku.shipmentskusave} - Shipment ID ${this.shipment}`, this.shipment);
            } else {
                this.sharedService.errorForm(this.localizationData.shipmentSku.shipmentskumandatory);
            }
        } else {
            this.sharedService.errorForm(this.localizationData.shipmentSku.shipmentskuatleastone);
        }
    }

    reset(): void {
        this.data = [];
        this.resetControls();
        Object.assign(this.data, this.dataReset);
    }

    close(): void {
        let obj: any = this;
        this.supplyChainSharedService.close(obj, this.shipmentSkuCount);
    }

    delete(cell: any): void {
        this.supplyChainSharedService.deleteRecord(this.data, this.deletedRows, this.gridApi, cell.rowIndex);
    }

    resetControls(): void {
        this.service.newModel(this.model);
        this.serviceDocument = this.service.serviceDocument;
        this.service.serviceDocument.dataProfile.profileForm.controls["shipment"].setValue(this.shipment);
    }

    ngOnDestroy(): void {
        this.saveSubscribtion.unsubscribe();
        this.saveContinueSubscribtion.unsubscribe();
        this.resetSubscribtion.unsubscribe();
        this.closeSubscribtion.unsubscribe();
        this.routeServiceSubscription.unsubscribe();
        this.previousSubscription.unsubscribe();
    }

    validateData(): boolean {
        let valid: boolean = true;
        this.data.forEach(row => {
            this.mandatoryShipmentSkuFields.forEach(field => {
                if (!row[field]) {
                    valid = false;
                }
            });
            if (row.isExpiry === "Y" && !row["expiryDate"]) {
                valid = false;
            }
        });
        return valid;
    }

    previousTab(): void {
        this.router.navigate(["/SupplyChain/New/ShipmentDetails/" + this.shipment], { skipLocationChange: true });
    }

    select(cell: any, event: any): void {
        this.data[cell.rowIndex][cell.column.colId] = event.target.value;
        let rowNode: RowNode = this.gridApi.getRowNode(cell.rowIndex);
        rowNode.setData(rowNode.data);
    }

    update(cell: any, event: any): void {
        this.data[cell.rowIndex][cell.column.colId] = event.target.value;
        if (cell.column.colId === "qtyReceived") {
            let qtyReceived: number = this.data[cell.rowIndex]["qtyReceived"];
            let qtyExpected: number = this.data[cell.rowIndex]["qtyExpected"];
            let item: string = this.data[cell.rowIndex]["item"];
            if (qtyReceived && qtyExpected && (qtyReceived > qtyExpected)) {
                let validationMessage: string = `Item: ${item} - Quantity Received can't be more than Quantity Expected: ${qtyExpected}`;
                this.commonService.showAlert(validationMessage, "", 5000);
                this.data[cell.rowIndex]["qtyReceived"] = null;
            }
        }
        let rowNode: RowNode = this.gridApi.getRowNode(cell.rowIndex);
        rowNode.setData(rowNode.data);
    }

    open(cell: any, mode: string): void {
        this.sharedService.editObject = this.data[cell.rowIndex];
        this.sharedService.viewForm(ShipmentInfoComponent, cell, "shipmentSkuView", "", this.service.serviceDocument.dataProfile.dataList);
    }
}