﻿import { Component } from "@angular/core";
import { DatePipe } from "@angular/common";
import { FormGroup } from "@angular/forms";
import { Subscription } from "rxjs";
import { CommonService, FormMode } from "@agility/frameworkcore";
import { MatDialog, MatDialogRef } from "@angular/material";
import { SharedService } from "../../Common/SharedService";

@Component({
    selector: "shipmentInfo",
    templateUrl: "./ShipmentInfoComponent.html"
})
export class ShipmentInfoComponent {
    infoGroup: FormGroup;
    sharedSubscription: Subscription;
    constructor(private service: SharedService, private commonService: CommonService, public dialog: MatDialog, private datePipe: DatePipe) {
        this.infoGroup = this.commonService.getFormGroup(this.service.editObject, FormMode.View);
        if (this.infoGroup.controls["reconcileDate"].value) {
            var date: Date = new Date(this.infoGroup.controls["reconcileDate"].value);
            this.infoGroup.controls["reconcileDate"].setValue(this.datePipe.transform(date, "dd-MMM-yyyy"));
        }
    }

    close(): void {
        this.dialog.closeAll();
    }
}