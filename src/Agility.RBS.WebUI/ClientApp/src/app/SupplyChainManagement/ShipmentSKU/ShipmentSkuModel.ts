﻿export class ShipmentSkuModel {
    shipment?: number;

    seqNo?: number;

    item: string;

    distroNo?: number;

    distroType: string;

    distroTypeDescription: string;

    refItem: string;

    carton: string;

    invStatus?: number;

    statusCode: string;

    statusCodeDescription: string;

    qtyReceived?: number;

    unitCost?: number;

    unitRetail?: number;

    qtyExpected?: number;

    matchInvcId?: number;

    adjustType: string;

    actualReceivingStore?: number;

    actualReceivingStoreName: string;

    reconcileUserId: string;

    reconcileDate?: Date;

    tamperedInd: string;

    dispositionedInd: string;

    qtyMatched?: number;

    weightReceived?: number;

    weightReceivedUom: string;

    weightExpected?: number;

    weightExpectedUom: string;

    isExpiry: string;

    expiryDate?: Date;

    operation: string;

    // additional needed parameters for the
    createdBy: string;

    createdDate: Date;

    lastUpdatedBy: string;

    lastUpdatedDate: Date;

    programPhase: string;

    programMessage: string;

    error: string;

    listOfShipmentSkuModels: ShipmentSkuModel[];

    tableName?: string;
}