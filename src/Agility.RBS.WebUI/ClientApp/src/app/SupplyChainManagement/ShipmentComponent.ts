import { Component, ViewChild, OnInit, OnDestroy } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MatDialogRef } from "@angular/material";
import { Subscription } from "rxjs";
import { CommonService, ServiceDocument, FxContext, IdentityMenuModel, MessageResult } from "@agility/frameworkcore";
import { SharedService } from "../Common/SharedService";
import { SupplyChainSharedService } from "./SupplyChainSharedService";
import { RbMessageDialogComponent } from "../Common/controls/RbMessageDialogComponent";
import { DocumentsConstants } from "../Common/DocumentsConstants";
import { LoaderService } from "../Common/LoaderService";
import { RbPrintPopupComponent } from "../Documents/RbPrintPopupComponent";
import { ShipmentHeadPrintModel } from "./ShipmentHeadPrintModel";
import { CommonPrintModel } from "../Common/CommonPrintModel";
import { OrderSearchModel } from '../order/OrderSearchModel';

@Component({
    selector: "shipment-add",
    templateUrl: "./ShipmentComponent.html"
})
export class ShipmentComponent implements OnInit, OnDestroy {
    dialogRef: MatDialogRef<RbMessageDialogComponent>;
    public menus: any = [];
    selected: string;
    childComponentName: string;
    PageTitle: string;
    public shipmentId: number = 0;
    isWorkFlowStarted: boolean;
    currentItemWfStatus: string;
    documentsConstants: DocumentsConstants = new DocumentsConstants();
    objectName: string;
    printServiceSubscription: Subscription;
    printData: ShipmentHeadPrintModel[];
    orderSearchModel: OrderSearchModel;

    constructor(
        public fxContext: FxContext
        , private suppChainSharedService: SupplyChainSharedService
        , private commonService: CommonService
        , private route: ActivatedRoute
        , private sharedService: SharedService
        , private router: Router
        , private loaderService: LoaderService
    ) {
    }

    save(): void {
        this.suppChainSharedService.announceSave(this.childComponentName);
    }

    saveAndContinue(): void {
        this.suppChainSharedService.announceSaveContinue(this.childComponentName);
    }

    reset(): void {
        this.suppChainSharedService.announceReset(this.childComponentName);
    }

    notify(component: any): void {
        this.childComponentName = component._componentName;
        this.setPageTitle();
    }

    close(): void {
        this.suppChainSharedService.announceClose(this.childComponentName);
    }

    ngOnInit(): void {
        this.shipmentId = this.sharedService.shipmentId ? this.sharedService.shipmentId : 0;
        this.selected = "SHIPMENT";
        this.menus = this.fxContext.userProfile.menus.filter(itm => itm.groupMenu === 7);
        this.setPageTitle();
        this.objectName = this.documentsConstants.shipmentObjectName;
    }

    private setPageTitle(): void {
        this.PageTitle = `${this.childComponentName} - ADD`;
        if (this.shipmentId && this.shipmentId.toString() !== "0") {
            this.PageTitle = `${this.childComponentName} - EDIT`;
            this.PageTitle = `${this.PageTitle} - ${this.shipmentId}`;
        }
    }

    select(url: string): string {
        this.shipmentId = this.sharedService.shipmentId ? this.sharedService.shipmentId : 0;
        return url + this.shipmentId;
    }

    isActive(item: string): boolean {
        return this.selected === item;
    }

    executePrintCommand(): void {
        this.loaderService.display(true);
        this.printServiceSubscription = this.suppChainSharedService.fetchPrintData(this.shipmentId).subscribe(() => {
            this.printData = this.suppChainSharedService.serviceDocument.dataProfile.dataList;
            let localizationDataPrint: any = Object.assign({}, this.suppChainSharedService.serviceDocument.localizationData);
            this.loaderService.display(false);
            if (this.printData != null && this.printData.length > 0) {
                let shipmentHead: ShipmentHeadPrintModel = this.printData[0];
                if (shipmentHead.shipmentSkuDetails != null && shipmentHead.shipmentSkuDetails.length > 0) {
                    let detailObject: CommonPrintModel[] = [{
                        tableName: this.documentsConstants.shipmentDetailsObjectName, columns: this.suppChainSharedService.detailColumns
                        , data: this.suppChainSharedService.buildDetailObject(shipmentHead.shipmentSkuDetails)
                    }];
                    let headObject: CommonPrintModel = {
                        tableName: this.documentsConstants.shipmentHeadObjectName, columns: this.suppChainSharedService.headColumns
                        , data: this.suppChainSharedService.buildHeadObject(shipmentHead)
                    };
                    this.sharedService.openDilogWithInputData(
                        RbPrintPopupComponent,
                        {
                            headObject: headObject, detailObject: detailObject
                        },
                        "Shipment Print",
                        {
                            screenId: this.shipmentId.toString(), screenName: this.documentsConstants.shipmentObjectName
                        });
                } else {
                    this.sharedService.errorForm(localizationDataPrint.rbsPrint.noprintdata);
                }
            } else {
                this.sharedService.errorForm(localizationDataPrint.rbsPrint.noprintdata);
            }
        },
            err => {
                this.sharedService.errorForm(this.suppChainSharedService.serviceDocument.result.innerException);
            }
        );
    }

    nextTab(): void {
        this.suppChainSharedService.announceNext(this.childComponentName);
    }

    previousTab(): void {
        this.suppChainSharedService.announcePrevious(this.childComponentName);
    }

    openOrderPage($event: MouseEvent): void {
        $event.preventDefault();
        this.orderSearchModel = {
            ordeR_NO: this.suppChainSharedService.orderNo, createdDate: null, dateType: null, deptDesc: null, deptId: null, endDate: null
            , endRow: null, itemBarcode: null, itemDesc: null, location: null, locationName: null, locType: null, locTypeDesc: null
            , operation: null, orderDesc: null, orderType: null, orderTypeDesc: null, sortModel: null, startDate: null, startRow: null
            , status: null, statusDesc: null, supName: null, supplier: null, tableName: null, totalRows: null
        };
        this.sharedService.searchData = Object.assign({}, this.orderSearchModel);
        this.router.navigate(["/Order"], { skipLocationChange: true });
    }

    ngOnDestroy(): void {
        if (this.printServiceSubscription) {
            this.printServiceSubscription.unsubscribe();
        }
    }
}
