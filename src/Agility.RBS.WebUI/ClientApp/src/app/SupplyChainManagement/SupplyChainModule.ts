import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../AgGridSharedModule";
import { TitleSharedModule } from "../TitleSharedModule";
import { SupplyChainSchedulerComponent } from "./Scheduler/SupplyChainSchedulerComponent";
import { SupplyChainSchedulerService } from "./Scheduler/SupplyChainSchedulerService";
import { SupplyChainSchedulerResolver } from "./SupplyChainResolver";
import { SupplyChainRoutingModule } from "./SupplyChainRoutingModule";
///import { CalendarComponent } from "ap-angular2-fullcalendar/src/calendar/calendar";
import { ShipmentDetailsComponent } from "./ShipmentDetails/ShipmentDetailsComponent";
import { ShipmentDetailsResolver } from "./ShipmentDetails/ShipmentDetailsResolver";
import { ShipmentDetailsService } from "./ShipmentDetails/ShipmentDetailsService";
import { ShipmentComponent } from "./ShipmentComponent";
import { SupplyChainSharedService } from "./SupplyChainSharedService";
import { ShipmentSkuComponent } from "./ShipmentSKU/ShipmentSkuComponent";
import { ShipmentSkuResolver } from "./ShipmentSKU/ShipmentSkuResolver";
import { ShipmentSkuService } from "./ShipmentSKU/ShipmentSkuService";
import { ShipmentService } from "./ShipmentService";
import { RbsSharedModule } from "../RbsSharedModule";

@NgModule({
    imports: [
        FrameworkCoreModule,
        SupplyChainRoutingModule,
        AgGridModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        SupplyChainSchedulerComponent,
       /// CalendarComponent,
        ShipmentComponent,
        ShipmentDetailsComponent,
        ShipmentSkuComponent
    ],
    providers: [
        SupplyChainSchedulerResolver,
        SupplyChainSchedulerService,
        ShipmentDetailsResolver,
        ShipmentDetailsService,
        SupplyChainSharedService,
        ShipmentSkuResolver,
        ShipmentSkuService,
        ShipmentService
    ]
})
export class SupplyChainModule {
}
