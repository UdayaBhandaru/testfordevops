﻿export class OrderShipmentRequestModel {
    orderNo?: number;

    estimatedArrivalDate?: Date;

    locationId?: number;
}