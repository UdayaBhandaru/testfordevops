﻿import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../Common/HttpHelperService";
import { OrderShipmentRequestModel } from "./OrderShipmentRequestModel";
import { OrderShipmentResponseModel } from "./OrderShipmentResponseModel";

@Injectable()
export class ShipmentService {

    constructor(private httpHelperService: HttpHelperService) { }

    getShipmentForOrder(orderShipObj: OrderShipmentRequestModel): Observable<number> {
        let apiUrl: string = "/api/SupplyChainScheduler/GetShipmentForOrder";

        return this.httpHelperService.post(apiUrl, orderShipObj);
    }

    getShipmentListForOrder(orderShipObj: OrderShipmentRequestModel): Observable<OrderShipmentResponseModel[]> {
        let apiUrl: string = "/api/SupplyChainScheduler/GetShipmentListForOrder";

        return this.httpHelperService.post(apiUrl, orderShipObj);
    }
}