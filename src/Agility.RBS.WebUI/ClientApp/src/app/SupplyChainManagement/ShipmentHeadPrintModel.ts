﻿import { ShipmentSkuModel } from "./ShipmentSKU/ShipmentSkuModel";

export class ShipmentHeadPrintModel {
    shipment?: number;

    orderNo?: number;

    shipmentSkuDetails?: ShipmentSkuModel[];

    billOfLadingNo: string;

    advanceShipmentNotice: string;

    shipDate?: Date;

    receiveDate?: Date;

    estArrDate?: Date;

    shipOriginDescription: string;

    statusCodeDescription: string;
}