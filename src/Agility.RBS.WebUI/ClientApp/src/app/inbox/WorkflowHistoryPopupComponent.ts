import { Component, OnInit, OnDestroy, Inject } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { SharedService } from "../Common/SharedService";
import { DatePipe } from "@angular/common";
import { Router } from "@angular/router";
import { CommonService, FormMode } from "@agility/frameworkcore";
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from "@angular/material";
import { GridOptions, GridApi } from "ag-grid-community";
import { WorkflowHistoryModel } from "./WorkflowHistoryModel";
import { GridDateComponent } from "../Common/grid/GridDateComponent";
@Component({
  selector: "workflowHistory",
  templateUrl: "./WorkflowHistoryPopupComponent.html",
  styleUrls: ['./WorkflowHistoryPopupComponent.scss']
})

export class WorkflowHistoryPopupComponent implements OnInit {
  workFlowHistoryDataList: WorkflowHistoryModel[] = [];
  objectValue: string;
  objectName: string;
  componentName: any;
  columns: {}[];
  gridOptions: GridOptions;
  gridApi: GridApi;
  rbsGridCustomOptions: { showSearchCriteria: boolean } = { showSearchCriteria: false };
  latestStatus: string;
  constructor(private _service: SharedService, private _commonService: CommonService, public dialog: MatDialog,
    private _datePipe: DatePipe, private _router: Router,
    @Inject(MAT_DIALOG_DATA) public data: { objectName: string, objectValue: string }) {
  }

  ngOnInit(): void {
    this.workFlowHistoryDataList = this._service.editObject;
    this.objectValue = this.data.objectValue;
    this.objectName = this.data.objectName;
    this.componentName = this;
    this.columns = [
      {
        headerName: "From", field: "senderName", tooltipField: "Sender", maxwidth: 175,
      },
      {
        headerName: "To", field: "recipientName", tooltipField: "Sender", maxwidth: 175
      },
      {
        headerName: "Status", field: "actionName", tooltipField: "Item Status"
      },
      {
        headerName: "Date Time", field: "reqRecievedDate", tooltipField: "Date", cellRendererFramework: GridDateComponent
        , maxwidth: 84, suppressSizeToFit: true
      },
      {
        headerName: "Comments", field: "comments", tooltipField: "Item Status"
      }
    ];

    this.gridOptions = {
      onGridReady: () => {
        //this.gridOptions.api.sizeColumnsToFit();
        this.gridApi.sizeColumnsToFit();
      },
      context: {
        componentParent: this
      },
      suppressPaginationPanel: true,
      suppressScrollOnNewData: false,
      paginationPageSize: 10,
      pagination: true,
      enableSorting: true,
      rowHeight: 40,
      enableColResize: true,
      rowSelection: "multiple",
      suppressRowClickSelection: true,
      suppressCellSelection: true,
      groupUseEntireRow: true,
      groupDefaultExpanded: -1,
      domLayout: "autoHeight",
      toolPanelSuppressSideButtons: true,
      embedFullWidthRows: true,
     
    };
    if (this.workFlowHistoryDataList && this.workFlowHistoryDataList.length > 0) {
      this.workFlowHistoryDataList.sort((a: WorkflowHistoryModel, b: WorkflowHistoryModel) => {
        return b.inboxId - a.inboxId;
      });
      let latestRecord: WorkflowHistoryModel = this.workFlowHistoryDataList[0];
      let initialRecord: WorkflowHistoryModel = this.workFlowHistoryDataList[this.workFlowHistoryDataList.length - 1];
      if (latestRecord.actionType === "C") {
        this.latestStatus = `Completed: ${latestRecord.inProgressDays}`;
      } else {
        this.latestStatus = `In Progress: ${latestRecord.inProgressDays}`;
      }
      initialRecord.iconCss = "initial-record";
      latestRecord.iconCss = "latest-record";
    }
  }

  close(): void {
    this.dialog.closeAll();
  }
}
