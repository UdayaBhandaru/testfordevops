import { Component } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular/main";

@Component({
  selector: "child-cell",
  template: `<div fxLayout="row" class="grid-actions">
            <button  mat-raised-button [style.background-color]="color" style="cursor: default;">{{priority}} </button>
        </div>
        <style>
         button {
          line-height: 24px;
          color: white;
          width: 80px;
        }
        </style>`
})
export class InboxPriorityComponent implements ICellRendererAngularComp {
  public color: string;
  public priority: string;

  agInit(params: any): void {
    if (params.value === "H") {
      this.color = "#ff0000";
      this.priority = "High";
    } else if (params.value === "M") {
      this.color = "#ffc000";
      this.priority = "Medium";
    } else if (params.value === "L") {
      this.color = "#92d050";
      this.priority = "Low";
    } else {
      this.color = "#ffc000";
      this.priority = "Medium";
    }
  }

  refresh(params: any): boolean {
    return true;
  }
}
