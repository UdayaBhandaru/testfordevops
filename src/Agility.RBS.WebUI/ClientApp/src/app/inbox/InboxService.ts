﻿import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { InboxModel } from "./InboxModel";
import { ServiceDocument } from "@agility/frameworkcore";
import { InboxQuickViewModel } from "./InboxQuickViewModel";
import { WorkflowHistoryModel } from "./WorkflowHistoryModel";
import { HttpParams } from "@angular/common/http";

@Injectable()
export class InboxService {
    serviceDocument: ServiceDocument<InboxModel> = new ServiceDocument<InboxModel>();
    serviceDocumentQuickView: ServiceDocument<InboxQuickViewModel> = new ServiceDocument<InboxQuickViewModel>();
    serviceDocumentWorkflowHistory: ServiceDocument<WorkflowHistoryModel> = new ServiceDocument<WorkflowHistoryModel>();

    list(inboxTabType: string): Observable<ServiceDocument<InboxModel>> {
        return this.serviceDocument.list("/api/inbox/List", new HttpParams().set("inboxTabType", inboxTabType.toString()));
    }

    itemQuickView(dataProfileId: string, workFlowId: string, profileInstanceId: string): Observable<ServiceDocument<InboxQuickViewModel>> {
        return this.serviceDocumentQuickView.list("/api/inbox/ListQuickView", new HttpParams().set("dataProfileId", dataProfileId).
            append("workFlowId", workFlowId).append("profileInstanceId", profileInstanceId));
    }

    fetchWorkflowHistory(objectName: string, objectValue: number): Observable<ServiceDocument<WorkflowHistoryModel>> {
        return this.serviceDocumentWorkflowHistory.list("/api/inbox/GetHistory",
            new HttpParams().set("objectName", objectName).set("objectValue", objectValue.toString()));
    }
}