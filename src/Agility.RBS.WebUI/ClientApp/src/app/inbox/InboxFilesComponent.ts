import { Component } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular/main";
import { HttpHeaders, HttpParams, HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";

@Component({
  selector: "file-cell",
  template: `<div fxLayout="row">
       <a href="#" (click)="invokeParentMethod($event)" *ngIf="documentCount > 0"><mat-icon class="file-actions retailsNavIcons">
        </mat-icon>({{documentCount}})</a>
         <a  (click)="openExcelSheet(params)" *ngIf="module ==='PRICE' || module ==='COST' || module ==='PROMO' || module ==='BULKITEM' || module ==='ITEMCLOSE' || module ==='ITEMREACTIVE'" >
            <mat-icon class="excel-icon retailsNavIcons">
        </mat-icon></a>
        </div>`
})
export class InboxFilesComponent implements ICellRendererAngularComp {
  public params: any;
  public documentCount: number = 0;
  public module: string;
  constructor(private httpClient: HttpClient) {

  }
  agInit(params: any): void {
    this.params = params;
    this.documentCount = this.params.value;
    this.module = this.params.data.moduleName;
  }

  refresh(params: any): boolean {
    return true;
  }

  public invokeParentMethod($event: MouseEvent): void {
    $event.preventDefault();
    this.params.context.componentParent.onDocumentsClick(this.params);
  }

  public openExcelSheet(params: any): void {
    this.downloadExcel(params);
  }

  public downloadExcel(params: any): void {
    let url: string;
    let fileName: string;
    let httpParams: HttpParams = new HttpParams().set("id", params.data.profileInstanceID.toString());
    switch (params.data.moduleName) {
      case "PRICE": 
        url = "/api/PriceChange/OpenPriceDetailExcel";
        fileName = "PriceChange";
        break;
      case "COST":
        url = "/api/CostChangeHead/OpenCostManagementExcel";
        fileName = "CostChange";
        break;
      case "PROMO":
        url = "/api/PromotionDetails/OpenPromoDetailExcel";
        fileName = "PromotionChange";
        break;
      case "BULKITEM":
        url = "/api/BulkItem/OpenLocalItemExcel";
        fileName = "BulkItem";
        break;
      case "ITEMCLOSE":
        url = "/api/ItemClose/OpenItemCloseExcel";
        fileName = "Item Close-Out Form";
        break;
      case "ITEMREACTIVE":
        url = "/api/ItemReactive/OpenItemReactivationExcel";
        fileName = "ItemReactivation";
        break;
      default:
        alert("dont have details");
        break;
    }

    if (url) {
      const type: any = "application/vnd.ms-excel";
      const filename: any = "file.xlsm";
      let headers1: HttpHeaders = new HttpHeaders();
      headers1.set("Accept", type);
      this.httpClient.get(url, { headers: headers1, responseType: "blob" as "json", params: httpParams })
        .pipe(map((response: any) => {
          if (response instanceof Response) {
            return response.blob();
          }
          return response;
        }))
        .subscribe(res => {
          console.log("start download:", res);
          let url: string = window.URL.createObjectURL(res);
          let a: HTMLAnchorElement = document.createElement("a");
          document.body.appendChild(a);
          a.setAttribute("style", "display: none");
          a.href = url;
          a.download = fileName + "-" + params.data.profileInstanceID + ".xlsm";
          a.click();
          window.URL.revokeObjectURL(url);
          a.remove();
        }, (error: string) => { console.log(error); });
    }
  }
}
