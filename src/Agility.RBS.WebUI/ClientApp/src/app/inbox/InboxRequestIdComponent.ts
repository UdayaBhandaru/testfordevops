import { Component } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular/main";

@Component({
  selector: "child-cell",
  template: `<div fxLayout="row" class="grid-actions">
            <a class="request-id-info" href="#" (click)="invokeParentMethod($event)">{{moduleName}} {{params.value}}</a>
        </div>
      <style>
      .request-id-info {
          color:#23a89f;
        }
      </style>`
})
export class InboxRequestIdComponent implements ICellRendererAngularComp {
  public params: any;
  private color: string;
  public moduleName: string;

  agInit(params: any): void {
    this.params = params;
    this.moduleName = this.params.data.moduleName;
  }

  refresh(params: any): boolean {
    return true;
  }

  public invokeParentMethod($event: MouseEvent): void {
    $event.preventDefault();
    if (this.params.data.moduleName) {
      this.params.context.componentParent.routeToReqPage(this.params, this.moduleName);
    } else {
      console.warn("no information");
      console.log(this.params);
      this.params.context.componentParent.routeToReqPage(this.params, this.moduleName);
    }
  }
}
