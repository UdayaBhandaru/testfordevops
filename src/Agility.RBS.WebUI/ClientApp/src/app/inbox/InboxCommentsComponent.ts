import { Component, OnInit, OnDestroy, Inject } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { Router } from "@angular/router";
import { CommonService, FormMode } from "@agility/frameworkcore";
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from "@angular/material";
import { InboxModel } from "./InboxModel";

@Component({
  selector: "inbox-comments",
  template: `<div fxLayout="column" class="workflow-inbox-tabs" fxFill>
    <div fxFlex="initial">
        <div mat-dialog-title fxLayout="row" class="recordDetailsDialog inbox-workflow-history">
            <div fxFlex>
                <p class="workflow-dailog-title m-left-10">Comments</p>
            </div>
            <div fxFlex="initial" class="toolbarActions-template">

                <button mat-button class="headerToolbaricon" (click)="close()">
                    <span class="closeIcon closeImage"></span>
                    <p class="closeText">CLOSE</p>
                </button>
            </div>
        </div>
    </div>
    <div fxFlex class="itemMaster-main-content">
            <div fxLayout="column" class="padding-right-10 padding-top-10 padding-left-10 padding-bottom-10">
                    <p><span>{{data?.data.comments}}</span></p>
                    <p>Comments by: <b style="color:#fc8605;">{{data?.data.senderName}}</b></p>
          </div>
    </div>
   </div>
    <style>
        .toolbarActions-template {
          background-color: #2b3d4b;
          padding: 0px 11px;
        }
        .toolbarActions-template .headerToolbaricon {
              padding: 5px !important;
              min-width: 80px !important;
        }
        .toolbarActions-template .closeIcon {
                  background: url('../../assets/images/Untitled-1.svg') no-repeat;
                  display: inline-block;
        }
        .toolbarActions-template .closeImage {
                  background-position: -79px 0;
                  width: 23px;
                  height: 23px;
        }
        .closeText{
                font-size: 12px;
                margin: 0;
                line-height: 1.5;
        }
        .workflow-dailog-title{
          margin:0 !important;
        }
</style>`
})

export class InboxCommentsComponent implements OnInit {
  constructor(public dialog: MatDialog, @Inject(MAT_DIALOG_DATA) public data: InboxModel) {
  }

  ngOnInit(): void {
    // inboxCommentsComponent
  }

  close(): void {
    this.dialog.closeAll();
  }
}
