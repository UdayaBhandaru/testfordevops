import { Component } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular/main";

@Component({
    selector: "child-cell",
    template: `<div fxLayout="row" class="grid-actions">
                  <span class="upload-img-styles">
                    <img *ngIf="params.data.userPicPath" [src]="params.data.userPicPath"
                    class="timeline-upload-image" width="30" height="30" style="border-radius:50px;" />
                    <img *ngIf="!params.data.userPicPath" src="../../assets/images/User Add.png" class="timeline-upload-image"
                    width="30" height="30" style="border-radius:50px;" />
                  </span>
                  <span class="grid-contact-person"> {{params.value}}</span>
        </div>`
})
export class GridImageComponent implements ICellRendererAngularComp {
    public params: any;
    constructor() {
        // gridImageComponent
    }

    agInit(params: any): void {
        this.params = params;
    }

    refresh(params: any): boolean {
        return true;
    }
}
