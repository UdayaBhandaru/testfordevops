﻿import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { InboxComponent } from "./InboxComponent";
import { InboxResolver } from "./InboxResolver";


const InboxRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: InboxComponent,
                resolve:
                {
                    serviceDocument: InboxResolver
                }
            }
        ]
    }
];


@NgModule({
    imports: [
        RouterModule.forChild(InboxRoutes)
    ]
})
export class InboxRoutingModule { }
