﻿export class WorkflowHistoryModel {

    senderName: string;

    recipientName: string;

    actionName: string;

    actionType: string;

    reqRecievedDate: Date;

    comments: string;

    inProgressDays: string;

    inboxId: number;

    userPicPath: string;

    iconCss: string;
}