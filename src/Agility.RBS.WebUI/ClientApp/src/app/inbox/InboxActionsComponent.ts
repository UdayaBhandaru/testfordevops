import { Component } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular/main";

@Component({
  selector: "child-cell",
  template: `<div fxLayout="row" class="grid-actions inbox-styles-action">
           <a href="#" (click)="invokeParentMethod($event,'view')" matTooltip="Quickview" [matTooltipPosition]="position">
                <span class="Quickview"></span></a>
           <a href="#" (click)="invokeParentMethod($event,'history')" matTooltip="History" [matTooltipPosition]="position">
                <span class="history retailsNavIcons"></span></a>
            <a *ngIf="params?.data?.comments?.trim()?.length>0" href="#" (click)="invokeParentMethod($event,'comments')" matTooltip="Comments"
                [matTooltipPosition]="position">
            <span class="comments retailsNavIcons"></span></a>            
        </div>
        <style>
          .Quickview {
            background: url("../../../assets/images/grid-action-icons.png") no-repeat center;
            width: 16px;
            height: 16px;
            display: block;
        }         
         .history {
          background-position: -2016px -26px !important;
          width: 13px;
          height: 13px;
          display: block;
          margin-left: 10px;
          margin-top: 2px;
        }

        .comments {
          background-position: -1986px -26px !important;
          width: 13px;
          height: 13px;
          display: block;
          margin-left: 10px;
          margin-top: 2px;
        }
        </style>`
})
export class InboxActionsComponent implements ICellRendererAngularComp {
  public params: any;
  public position: string = "above";

  agInit(params: any): void {
    this.params = params;
  }

  refresh(params: any): boolean {
    return true;
  }

  public invokeParentMethod($event: MouseEvent, mode: string): void {
    $event.preventDefault();
    this.params.context.componentParent.open(this.params, mode);
  }
}
