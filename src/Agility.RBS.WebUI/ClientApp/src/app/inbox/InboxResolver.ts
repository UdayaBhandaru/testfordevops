﻿import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { InboxService } from "./InboxService";
import { InboxModel } from "./InboxModel";
import { ServiceDocument, FxContext } from "@agility/frameworkcore";

@Injectable()
export class InboxResolver implements Resolve<ServiceDocument<InboxModel>> {
    userDataObj: any;
    constructor(private service: InboxService, private _fxContext: FxContext) { }
    resolve(route: ActivatedRouteSnapshot): Observable<ServiceDocument<InboxModel>> {
        this.userDataObj = this._fxContext.userProfile;
        return this.service.list("PDG");
    }
}