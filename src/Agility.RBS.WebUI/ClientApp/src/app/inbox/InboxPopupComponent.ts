import { Component, OnInit, Inject } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { Router } from "@angular/router";
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from "@angular/material";
import { InboxQuickViewModel } from "./InboxQuickViewModel";
import { InboxService } from "./InboxService";
import { SharedService } from "../Common/SharedService";
import { DocumentsConstants } from "../Common/DocumentsConstants";

@Component({
    selector: "inbox-quickview",
    templateUrl: "./InboxPopupComponent.html",
    styleUrls: ['./InboxPopupComponent.scss'],
    providers: [
        InboxService

    ],
})

export class InboxPopupComponent implements OnInit {
    popupDataList: InboxQuickViewModel[] = [];
    objectValue: number;
    objectName: string;
    documentsConstants: DocumentsConstants = new DocumentsConstants();
    redirectUrl: string;

    constructor(public dialog: MatDialog
        , private _router: Router
        , @Inject(MAT_DIALOG_DATA)
        public data: {
            dataList: InboxQuickViewModel[], rowData: { viewMode: boolean, cellData: any }
            , objectName: string, objectValue: number
        }
        , private inboxService: InboxService
        , private sharedService: SharedService
    ) { }

    ngOnInit(): void {
        if (this.data !== null) {
            this.popupDataList = this.data.dataList;
            this.objectValue = this.data.objectValue;
            this.objectName = this.data.objectName;
        } else {
            this.popupDataList = [];
        }
    }
    close(): void {
        this.dialog.closeAll();
    }

    viewCompleteDetails($event: MouseEvent): void {
        $event.preventDefault();
        this.close();
        this.sharedService.previousUrl = "../Inbox";
        this.redirectUrl = this.documentsConstants.modulesHeadPageRoutes.find(x => x.key === this.objectName).value;
        this._router.navigate(["/" + this.redirectUrl + this.objectValue], { skipLocationChange: true });
    }
}
