﻿import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { AgGridSharedModule } from "../AgGridSharedModule";
import { TitleSharedModule } from "../TitleSharedModule";
import { InboxService } from "./InboxService";
import { InboxResolver } from "./InboxResolver";
import { InboxComponent } from "./InboxComponent";
import { InboxRoutingModule } from "./InboxRoutingModule";
import { MatExpansionModule } from "@angular/material";
import { RbsSharedModule } from "../RbsSharedModule";

@NgModule({
    imports: [
        FrameworkCoreModule,
        InboxRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        MatExpansionModule,
        RbsSharedModule
    ],
    declarations: [
        InboxComponent
    ],
    providers: [
        InboxService,
        InboxResolver
    ]
})
export class InboxModule {
}
