import { ItemListDetailModel } from "../itemlist/ItemListDetailModel";

export class ItemListHeadModel {
    listId: number;
    listDesc: string;
    staticInd: string;
    staticIndDesc: string;
    commentDesc?: string;
    filterOrgId: number;
    visibilityInd: string;
    visibilityIndDesc: string;
    lastRebuildDate: Date;
    listStatus: string;
    listStatusDesc?: string;
    error?: string;
    operation?: string;
    itemListDetails?: ItemListDetailModel[];
    itemNo?: string;

    supplierId?: number;
    suppName?: string;
    itemBarcode?: string;
    itemDesc?: string;
    deptId?: number;
    deptDesc?: string;
    categoryId?: number;
    categoryDesc?: string;
    brandId?: number;
    brandName?: string;
    divisionId?: number;
    divisionDesc?: string;
}
