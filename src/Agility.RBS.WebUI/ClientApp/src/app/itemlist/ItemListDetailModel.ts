﻿export class ItemListDetailModel {
    listId?: number;
    item?: string;
    lineStatus?: string;
    lineStatusDesc: string;
    operation?: string;
    createdBy?: string;

    supplierId?: number;
    suppName?: string;
    itemBarcode?: string;
    itemDesc?: string;
    deptId?: number;
    deptDesc?: string;
    categoryId?: number;
    categoryDesc?: string;
    brandId?: number;
    brandName?: string;
    divisionId?: number;
    divisionDesc?: string;
}