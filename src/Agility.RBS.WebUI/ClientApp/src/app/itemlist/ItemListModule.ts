﻿import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { ItemListRoutingModule } from "./ItemListRoutingModule";
import { ItemListHeadComponent } from "./ItemListHeadComponent";
import { ItemListMstComponent } from "./ItemListMstComponent";
import { ItemListService } from "./ItemListService";
import { ItemListResolver } from "./ItemListResolver";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../AgGridSharedModule";
import { TitleSharedModule } from "../TitleSharedModule";
import { ItemSharedService } from "../item/ItemSharedService";
import { RbsSharedModule } from "../RbsSharedModule";

@NgModule({
    imports: [
        FrameworkCoreModule,
        ItemListRoutingModule,
        AgGridModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        ItemListHeadComponent,
        ItemListMstComponent
    ],
    providers: [
        ItemListService,
        ItemSharedService,
        ItemListResolver
    ]
})
export class ItemListModule {
}
