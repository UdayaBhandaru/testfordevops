﻿import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ItemListHeadComponent } from "./ItemListHeadComponent";
import { ItemListMstComponent } from "./ItemListMstComponent";
import { ItemListResolver } from "./ItemListResolver";

const ItemListRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: ItemListHeadComponent,
                resolve:
                {
                    references: ItemListResolver
                }
            },
            {
                path: "New",
                component: ItemListMstComponent,
                resolve:
                {
                    references: ItemListResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(ItemListRoutes)
    ]
})
export class ItemListRoutingModule { }
