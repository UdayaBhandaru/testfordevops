import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { ItemListHeadModel } from "./ItemListHeadModel";
import { ItemListService } from "./ItemListService";
import { FormGroup } from "@angular/forms";
import { SharedService } from "../Common/SharedService";
import { ItemListDetailModel } from "./ItemListDetailModel";
import { GridEditComponent } from "../Common/grid/GridEditComponent";
import { GridExportModel } from "../Common/grid/GridExportModel";
import { InfoComponent } from "../Common/InfoComponent";
import { DomainDetailModel } from "../domain/DomainDetailModel";
import { GridDateComponent } from "../Common/grid/GridDateComponent";
import { GridOptions } from 'ag-grid-community';

@Component({
  selector: "item-list-head",
  templateUrl: "./ItemListHeadComponent.html"
})
export class ItemListHeadComponent implements OnInit {
  public serviceDocument: ServiceDocument<ItemListHeadModel>;
  PageTitle = "Item List";
  redirectUrl = "ItemListHead";
  componentName: any;
  columns: any[];
  domainDtlData: DomainDetailModel[]; indicatorData: DomainDetailModel[];
  model: ItemListHeadModel = {
    listId: null, listDesc: null, listStatus: null, listStatusDesc: null, filterOrgId: null, lastRebuildDate: null, staticInd: null,
    visibilityInd: null, itemListDetails: null, staticIndDesc: null, visibilityIndDesc: null
  };
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  gridOptions: GridOptions;

  constructor(public service: ItemListService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "Description", field: "listDesc", tooltipField: "listDesc" },
      { headerName: "Static Ind", field: "staticIndDesc", tooltipField: "staticIndDesc", width: 60 },
      { headerName: "Visibility Ind", field: "visibilityIndDesc", tooltipField: "visibilityIndDesc", width: 60 },
      {
        headerName: "Action", field: "Actions", cellRendererFramework: GridEditComponent, width: 80
      }
    ];

    //this.domainDtlData = Object.assign([], this.service.serviceDocument.domainData["status"]);
    this.indicatorData = Object.assign([], this.service.serviceDocument.domainData["indicator"]);
    this.sharedService.domainData = { status: this.domainDtlData, indicator: this.indicatorData };
    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }
}
