﻿import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { ItemListHeadModel } from "./ItemListHeadModel";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { ItemListService } from "./ItemListService";

@Injectable()
export class ItemListResolver implements Resolve<ServiceDocument<ItemListHeadModel>> {
    constructor(private service: ItemListService) { }
    resolve(): Observable<ServiceDocument<ItemListHeadModel>> {
        return this.service.list();
    }
}