import { Component, OnInit, ChangeDetectorRef, OnDestroy } from "@angular/core";
import { ServiceDocument, FormMode, CommonService, MessageResult, MessageType } from "@agility/frameworkcore";
import { Router } from "@angular/router";
import { FormControl, FormGroup } from "@angular/forms";
import { Subscription } from "rxjs";
import { SharedService } from "../Common/SharedService";
import { ItemListHeadModel } from "./ItemListHeadModel";
import { ItemListDetailModel } from "./ItemListDetailModel";
import { ItemListService } from "./ItemListService";
import { GridOptions, GridApi } from "ag-grid-community";
import { DomainDetailModel } from "../domain/DomainDetailModel";
import { ItemSelectComponent } from "../Common/ItemSelectComponent";
import { ItemSelectModel } from "../Common/ItemSelectModel";
import { GridEditComponent } from '../Common/grid/GridEditComponent';

@Component({
  selector: "itemlistmst",
  templateUrl: "./ItemListMstComponent.html",
  styleUrls: ['./ItemListMstComponent.scss'],
})
export class ItemListMstComponent implements OnInit, OnDestroy {
  public serviceDocument: ServiceDocument<ItemListHeadModel>;
  componentName: any;
  listId: number = 0;
  apiUrl: string;
  model: ItemListHeadModel = {
    listId: null, listDesc: null, staticInd: null, commentDesc: null, visibilityInd: null, listStatus: "A", filterOrgId: null,
    lastRebuildDate: null, itemListDetails: [], operation: "I", itemNo: null, staticIndDesc: null, visibilityIndDesc: null
  };
  localizationData: any;
  deletedRows: any[] = [];
  companyCount: number;
  node: any;
  addText: string = "+ Add To Grid";
  pForm: FormGroup;
  PageTitle = "Item List";
  sheetName: string = "Item List";
  redirectUrl = "ItemListHead";
  public gridOptions: GridOptions;

  columns: any[];
  data: ItemListDetailModel[] = [];
  dataReset: ItemListDetailModel[] = [];
  domainDtlData: DomainDetailModel[]; indicatorData: DomainDetailModel[];
  editMode: boolean = false;
  editModel: any;
  searchItemGetServiceSubscription: Subscription;
  saveServiceSubscription: Subscription;
  saveSuccessSubscription: Subscription;
  findItemsServiceSubscription: Subscription;
  private messageResult: MessageResult = new MessageResult();
  componentParentName: ItemListMstComponent;
  gridApi: GridApi;

  constructor(private service: ItemListService, private commonService: CommonService,
    private sharedService: SharedService, private router: Router, private ref: ChangeDetectorRef) {
  }

  ngAfterViewChecked(): void {
    this.ref.detectChanges();
  }

  ngOnInit(): void {
    this.componentParentName = this;
    this.domainDtlData = Object.assign([], this.service.serviceDocument.domainData["status"]);
    this.indicatorData = Object.assign([], this.service.serviceDocument.domainData["indicator"]);
    this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
    this.bind();
  }

  bind(): void {
    this.columns = [
      { headerName: "Item", field: "item", tooltipField: "item" },
      { headerName: "Item Description", field: "itemDesc", tooltipField: "itemDesc" },
      { headerName: "Division", field: "divisionDesc", tooltipField: "divisionDesc" },
      { headerName: "Department", field: "deptDesc", tooltipField: "deptDesc" },
      { headerName: "Category", field: "categoryDesc", tooltipField: "categoryDesc" },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 150
        , cellRendererParams: { restrictEditIcon: true }
      }
    ];

    this.gridOptions = {
      onGridReady: (params: any) => {
        this.gridApi = params.api;
      },
    };
    delete this.editModel;
    if (this.sharedService.editObject) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
      Object.assign(this.service.serviceDocument.dataProfile.dataModel, { itemNo: null });
      this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
      Object.assign(this.data, this.service.serviceDocument.dataProfile.dataModel.itemListDetails);
      Object.assign(this.dataReset, this.service.serviceDocument.dataProfile.dataModel.itemListDetails);
      this.listId = this.service.serviceDocument.dataProfile.dataModel.listId;
      this.editMode = true;
      delete this.sharedService.editObject;
    } else {
      this.model = {
        listId: null, listDesc: null, staticInd: null, commentDesc: null, visibilityInd: null, listStatus: "A", filterOrgId: null,
        lastRebuildDate: null, itemListDetails: [], operation: "I", itemNo: null, staticIndDesc: null, visibilityIndDesc: null
      };
      this.service.serviceDocument.dataProfile.dataModel = this.model;
      this.data = this.service.serviceDocument.dataProfile.dataModel.itemListDetails;
    }
    this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    this.serviceDocument = this.service.serviceDocument;
  }

  save(saveOnly: boolean): void {
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      if (this.data.length > 0) {
        this.saveSubscription(saveOnly);
      } else {
        this.sharedService.errorForm("Please add atleast one Item");
      }
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  saveAndContinue(): void {
    this.save(false);
  }

  add($event: MouseEvent): void {
    $event.preventDefault();
    this.pForm = this.serviceDocument.dataProfile.profileForm;
    if (this.data.length === 0) {
      this.service.serviceDocument.dataProfile.dataModel.itemListDetails = [];
    }
    if (this.pForm.controls["itemNo"].value != null) {
      this.apiUrl = "/api/ItemList/SearchItemGet?item=" + this.pForm.controls["itemNo"].value;
      this.searchItemGetServiceSubscription = this.service.getDataFromAPI(this.apiUrl).subscribe(response => {
        if (response) {
          let itmList: ItemListDetailModel[] = this.data ? this.data.filter(itm => itm.item === response.item) : null;
          if (!itmList || itmList.length === 0) {
            let itemListDtls: ItemListDetailModel = {
              brandId: response.brandId, brandName: response.brandName, categoryDesc: response.categoryDesc
              , categoryId: response.categoryId, deptId: response.deptId, deptDesc: response.deptDesc
              , divisionDesc: response.divisionDesc, divisionId: response.divisionId, item: response.item
              , itemBarcode: response.item, itemDesc: response.itemDesc, operation: "I", listId: this.listId
              , supplierId: response.supplierId, suppName: response.suppName, lineStatus: "A", createdBy: null
              , lineStatusDesc: "ACTIVE"
            };
            this.data.push(Object.assign({}, itemListDtls));
          } else if (itmList) {
            itmList[0].lineStatus = "A";
            itmList[0].lineStatusDesc = "ACTIVE";
          }
          if (this.gridApi) {
            this.gridApi.setRowData(this.data);
          }
        } else {
          this.sharedService.errorForm(this.localizationData.itemList.itemlistitemvalid);
        }
        this.pForm.controls["itemNo"].setValue(null);
      });
    } else {
      this.sharedService.errorForm(this.localizationData.itemList.itemlistitemempty);
    }
  }

  delete(cell: any): void {
    if (cell.data.operation === "U") {
      cell.data.operation = "D";
      this.deletedRows.push(cell.data);
    }
    this.data.splice(cell.rowIndex, 1);
    this.gridApi.setRowData(this.data);
  }

  reset(): void {
    if (!this.editMode) {
      this.model = {
        listId: null, listDesc: null, staticInd: null, commentDesc: null, visibilityInd: null, listStatus: "A", filterOrgId: null
        , lastRebuildDate: null, itemListDetails: [], operation: "I", itemNo: null, staticIndDesc: null, visibilityIndDesc: null
      };
      this.service.serviceDocument.dataProfile.dataModel = this.model;
      this.data = this.service.serviceDocument.dataProfile.dataModel.itemListDetails;
      this.service.newModel(this.model);
    } else {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);


      Object.assign(this.data, this.dataReset);
    }
    this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
  }

  saveSubscription(saveOnly: boolean): void {
    if (this.deletedRows.length > 0) {
      this.deletedRows.forEach(x => this.data.push(x));
    }
    this.serviceDocument.dataProfile.profileForm.controls["itemListDetails"] = new FormControl();
    this.serviceDocument.dataProfile.profileForm.controls["itemListDetails"].setValue(this.data);
    this.saveServiceSubscription = this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.saveSuccessSubscription = this.sharedService.saveForm(this.localizationData.itemList.itemlistsave).subscribe(() => {
          if (saveOnly) {
            this.router.navigate(["/" + this.redirectUrl + "/List"], { skipLocationChange: true });
          } else {
            if (!this.editMode) {
              this.model = {
                listId: null, listDesc: null, staticInd: null, commentDesc: null, visibilityInd: null, listStatus: "A"
                , filterOrgId: null, lastRebuildDate: null, itemListDetails: [], operation: "I", itemNo: null
                , staticIndDesc: null, visibilityIndDesc: null
              };
              this.service.serviceDocument.dataProfile.dataModel = this.model;
              this.data = this.service.serviceDocument.dataProfile.dataModel.itemListDetails;
              this.service.newModel(this.model);
            } else {
              Object.assign(this.service.serviceDocument.dataProfile.dataModel, { itemNo: null });
              Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
              this.data = this.service.serviceDocument.dataProfile.dataModel.itemListDetails;
              this.dataReset = this.data;
              this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.service.serviceDocument.dataProfile.dataModel);
              this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            }
          }
        }, (error: string) => { console.log(error); });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
    }, (error: string) => { console.log(error); });
  }

  openItemSearch(): void {
    let data: any = [{ "Name": "ITM" }];
    this.findItemsServiceSubscription = this.sharedService.openDilogForFindItems(ItemSelectComponent, data, "Filter")
      .subscribe((selectedList: ItemSelectModel[]) => {
        if (selectedList) {
          selectedList.forEach(x => {
            let itemListDtls: ItemListDetailModel = {
              brandId: x.brandId, brandName: x.brandName, categoryDesc: x.categoryDesc, categoryId: x.categoryId
              , deptId: x.deptId, deptDesc: x.deptDesc, divisionDesc: x.divisionDesc, divisionId: x.divisionId
              , item: x.itemBarcode, itemBarcode: x.itemBarcode, itemDesc: x.itemDesc, operation: "I", listId: this.listId
              , supplierId: x.supplierId, suppName: x.suppName, lineStatus: "A", createdBy: null, lineStatusDesc: "ACTIVE"
            };

            let itmList: ItemListDetailModel[] = this.data ? this.data.filter(itm => itm.item === x.itemBarcode) : null;
            if (!itmList || itmList.length === 0) {
              this.data.push(Object.assign({}, itemListDtls));
            } else if (itmList) {
              itmList[0].lineStatus = "A";
              itmList[0].lineStatusDesc = "ACTIVE";
            }
          });
          if (this.gridOptions.api) {
            this.gridOptions.api.setRowData(this.data);
          }
        }
      });
  }

  ngOnDestroy(): void {
    if (this.saveServiceSubscription) {
      this.saveServiceSubscription.unsubscribe();
    }
    if (this.saveSuccessSubscription) {
      this.saveSuccessSubscription.unsubscribe();
    }
    if (this.searchItemGetServiceSubscription) {
      this.searchItemGetServiceSubscription.unsubscribe();
    }
    if (this.findItemsServiceSubscription) {
      this.findItemsServiceSubscription.unsubscribe();
    }
  }
}
