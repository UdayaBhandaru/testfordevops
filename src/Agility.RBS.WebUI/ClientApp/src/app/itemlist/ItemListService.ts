﻿import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { ItemListHeadModel } from "./ItemListHeadModel";
import { ItemListDetailModel } from "./ItemListDetailModel";
import { SharedService } from "../Common/SharedService";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../Common/HttpHelperService";

@Injectable()
export class ItemListService {
    serviceDocument: ServiceDocument<ItemListHeadModel> = new ServiceDocument<ItemListHeadModel>();
    constructor(private httpHelperService: HttpHelperService, private sharedService: SharedService) { }

    newModel(model: ItemListHeadModel): ServiceDocument<ItemListHeadModel> {
        return this.serviceDocument.newModel(model);
    }

    list(): Observable<ServiceDocument<ItemListHeadModel>> {
        return this.serviceDocument.list("/api/ItemList/List");
    }

    search(): Observable<ServiceDocument<ItemListHeadModel>> {
        return this.serviceDocument.search("/api/ItemList/Search");
    }

    save(): Observable<ServiceDocument<ItemListHeadModel>> {
        return this.serviceDocument.save("/api/ItemList/Save", true, () => this.setDate());
    }

    getDataFromAPI(apiUrl: string): Observable<any> {
        var resp: any = this.httpHelperService.get(apiUrl);
        return resp;
    }

    setDate(): void {
        let sd: ItemListHeadModel = this.serviceDocument.dataProfile.dataModel;
        if (sd !== null) {
            if (sd.lastRebuildDate != null) {
                sd.lastRebuildDate = this.sharedService.setOffSet(sd.lastRebuildDate);
            }
        }
    }

    isExistingDomain(name: string): Observable<boolean> {
        return this.httpHelperService.get("/api/ItemList/IsExistingDomain", new HttpParams().set("name", name));
    }
}