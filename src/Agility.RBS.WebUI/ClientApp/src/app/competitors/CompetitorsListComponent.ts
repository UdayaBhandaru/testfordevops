import { Component, OnInit, ChangeDetectorRef, NgModule } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { GridOptions } from "ag-grid-community";
import { SharedService } from '../Common/SharedService';
import { CompetitorsService } from './CompetitorsService';
import { CompetitorsModel } from './CompetitorsModel';
import { InfoComponent } from '../Common/InfoComponent';
import { GridExportModel } from '../Common/grid/GridExportModel';
import { GridEditComponent } from '../Common/grid/GridEditComponent';
import { CountryDomainModel } from '../Common/DomainData/CountryDomainModel';

@Component({
  selector: "competitors-list",
  templateUrl: "./CompetitorsListComponent.html"
})
export class CompetitorsListComponent implements OnInit {
  public serviceDocument: ServiceDocument<CompetitorsModel>;
  PageTitle = "Competitors";
  redirectUrl = "Competitors";
  componentName: any;
  columns: any[];
  model: CompetitorsModel = { competitor: null, compName: null, address1: null, countryId:null  };
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  gridOptions: GridOptions;
  countryData: CountryDomainModel[];

  constructor(public service: CompetitorsService, private sharedService: SharedService, private router: Router, private ref: ChangeDetectorRef) {
  }

  ngAfterViewChecked(): void {
    this.ref.detectChanges();
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "ID", field: "competitor", tooltipField: "competitor", width: 60 },
      { headerName: "COMPNAME", field: "compName", tooltipField: "compName", width: 90 },
      { headerName: "ADDRESS", field: "address1", tooltipField: "address1", width: 90 },
      { headerName: "COUNTRY", field: "countryId", tooltipField: "countryId", width: 90 },
      {
        headerName: "ACTIONS", field: "Actions", cellRendererFramework: GridEditComponent, width: 60
      }
    ];

    this.countryData = Object.assign([], this.service.serviceDocument.domainData["country"]);

    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }
}
