import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CompetitorsListComponent } from './CompetitorsListComponent';
import { CompetitorsResolver, CompetitorsListResolver } from './CompetitorsResolver';
import { CompetitorsComponent } from './CompetitorsComponent';

const UtilityRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: CompetitorsListComponent,
                resolve:
                {
                  serviceDocument: CompetitorsListResolver
                }
            },
            {
                path: "New",
                component: CompetitorsComponent,
                resolve:
                {
                  serviceDocument: CompetitorsResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(UtilityRoutes)
    ]
})
export class CompetitorsRoutingModule { }
