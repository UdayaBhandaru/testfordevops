import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from '../Common/HttpHelperService';
import { CompetitorsModel } from './CompetitorsModel';

@Injectable()
export class CompetitorsService {
  serviceDocument: ServiceDocument<CompetitorsModel> = new ServiceDocument<CompetitorsModel>();
    constructor(private httpHelperService: HttpHelperService) { }

  newModel(model: CompetitorsModel): ServiceDocument<CompetitorsModel> {
        return this.serviceDocument.newModel(model);
    }

  search(): Observable<ServiceDocument<CompetitorsModel>> {
    return this.serviceDocument.search("/api/Competitors/Search");
    }

  save(): Observable<ServiceDocument<CompetitorsModel>> {
    return this.serviceDocument.save("/api/Competitors/Save", true);
    }

  list(): Observable<ServiceDocument<CompetitorsModel>> {
    return this.serviceDocument.list("/api/Competitors/List");
    }

  addEdit(): Observable<ServiceDocument<CompetitorsModel>> {
    return this.serviceDocument.list("/api/Competitors/New");
    }

  isExistingCompetitors(competitor: number, compName: string): Observable<boolean> {
    return this.httpHelperService.get("/api/Competitors/IsExistingCompetitors", new HttpParams().set("competitor", competitor.toString()).set("compName", compName));
    }
}
