import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { AgGridSharedModule } from '../AgGridSharedModule';
import { TitleSharedModule } from '../TitleSharedModule';
import { RbsSharedModule } from '../RbsSharedModule';
import { CompetitorsRoutingModule } from './CompetitorsRoutingModule';
import { CompetitorsListComponent } from './CompetitorsListComponent';
import { CompetitorsComponent } from './CompetitorsComponent';
import { CompetitorsService } from './CompetitorsService';
import { CompetitorsResolver, CompetitorsListResolver } from './CompetitorsResolver';

@NgModule({
    imports: [
        FrameworkCoreModule,
        CompetitorsRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        CompetitorsListComponent,
        CompetitorsComponent
    ],
    providers: [
        CompetitorsService,
        CompetitorsResolver,
        CompetitorsListResolver
    ]
})
export class CompetitorsModule {
    constructor() {
        LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
    }
}
