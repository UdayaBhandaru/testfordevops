import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { FormControl, FormGroup } from "@angular/forms";
import { SharedService } from '../Common/SharedService';
import { CompetitorsModel } from './CompetitorsModel';
import { CompetitorsService } from './CompetitorsService';
import { CountryDomainModel } from '../Common/DomainData/CountryDomainModel';

@Component({
    selector: "competitors",
    templateUrl: "./CompetitorsComponent.html"
})
export class CompetitorsComponent implements OnInit {
    PageTitle = "Competitors";
    serviceDocument: ServiceDocument<CompetitorsModel>;
    localizationData: any;
    editModel: CompetitorsModel;
    editMode: boolean = false;
    defaultStatus: string;
    model: CompetitorsModel;
    countryData: CountryDomainModel[];


  constructor(private service: CompetitorsService, private sharedService: SharedService, private router: Router) {
    }

  ngOnInit(): void {
        this.countryData = Object.assign([], this.service.serviceDocument.domainData["country"]);
        this.bind();
    }

    bind(): void {
        delete this.editModel;
        if (this.sharedService.editObject) {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
            this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            delete this.sharedService.editObject;
            this.editMode = true;
        } else {
            this.model = {competitor: null, compName: null, address1: null, countryId: null, operation: "I"};
            this.service.newModel(this.model);
        }
      this.serviceDocument = this.service.serviceDocument;
    }

    save(saveOnly: boolean): void {
        if (this.serviceDocument.dataProfile.profileForm.valid) {
            this.saveSubscription(saveOnly);
        } else {
            this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
        }
    }

    saveAndContinue(): void {
        this.save(false);
    }

    saveSubscription(saveOnly: boolean): void {
        this.checkCompetitors(saveOnly);
    }
    saveCompetitors(saveOnly: boolean): void {
        this.service.save().subscribe(() => {
            if (this.service.serviceDocument.result.type === MessageType.success) {
              this.sharedService.saveForm("Competitors Saved Successfully").subscribe(() => {
                    if (saveOnly) {
                      this.router.navigate(["/Competitors/List"], { skipLocationChange: true });
                    } else {
                        if (!this.editMode) {
                            this.model = {
                              competitor: null, compName: null, address1: null, countryId: null, operation: "I"
                            };
                            this.service.newModel(this.model);
                        } else {
                            Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
                            this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.editModel);
                            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
                        }
                    }
                }, (error: string) => { console.log(error); });
            } else {
                this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
            }
        }, (error: string) => { console.log(error); });
    }

    checkCompetitors(saveOnly: boolean): void {
        if (!this.editMode) {
            var form: FormGroup = this.serviceDocument.dataProfile.profileForm;
          if (form.controls["competitor"].value && form.controls["compName"].value) {
            this.service.isExistingCompetitors(form.controls["competitor"].value, form.controls["compName"].value)
                    .subscribe((response: any) => {
                        if (response) {
                            this.sharedService.errorForm("Record alredy exits");
                        } else {
                            this.saveCompetitors(saveOnly);
                        }
                    });
            }
        } else {
            this.saveCompetitors(saveOnly);
        }
    }

    reset(): void {
        if (!this.editMode) {
            this.model = {
              competitor: null, compName: null, address1: null, countryId: null, operation: "I"
            };
            this.service.newModel(this.model);
        } else {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
        }
        this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
    }    
}
