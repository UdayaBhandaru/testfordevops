import { Injectable } from "@angular/core";
import { Resolve } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { CompetitorsModel } from './CompetitorsModel';
import { CompetitorsService } from './CompetitorsService';
@Injectable()
export class CompetitorsListResolver implements Resolve<ServiceDocument<CompetitorsModel>> {
  constructor(private service: CompetitorsService) { }
  resolve(): Observable<ServiceDocument<CompetitorsModel>> {
        return this.service.list();
    }
}

@Injectable()
export class CompetitorsResolver implements Resolve<ServiceDocument<CompetitorsModel>> {
  constructor(private service: CompetitorsService) { }
  resolve(): Observable<ServiceDocument<CompetitorsModel>> {
        return this.service.addEdit();
    }
}


