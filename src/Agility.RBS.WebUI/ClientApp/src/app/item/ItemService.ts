import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument, WorkflowAction } from "@agility/frameworkcore";
import { HttpParams, HttpHeaders, HttpClient } from "@angular/common/http";
import { SharedService } from "../Common/SharedService";
import { NewItemModel } from './NewItemModel';
import { HttpHelperService } from '../Common/HttpHelperService';

@Injectable()
export class ItemService {
  serviceDocument: ServiceDocument<NewItemModel> = new ServiceDocument<NewItemModel>();
  constructor(private httpHelperService: HttpHelperService) { }

  save(): Observable<ServiceDocument<NewItemModel>> {
    let currentAction: WorkflowAction = this.serviceDocument.dataProfile.actionService.allowedActions[0];
    if (!this.serviceDocument.dataProfile.dataModel.heaD_SEQ && currentAction) {
      this.serviceDocument.dataProfile.profileForm.controls["currentActionId"].setValue(currentAction.transitionClaim);
      return this.serviceDocument.submit("/api/NewItem/Submit", true);
    } else {
      return this.serviceDocument.save("/api/NewItem/Save", true);
    }
  }

  submit(): Observable<ServiceDocument<NewItemModel>> {
    return this.serviceDocument.submit("/api/NewItem/Submit", true);
  }

  getDataFromAPI(apiUrl: string): Observable<any> {
    var resp: any = this.httpHelperService.get(apiUrl);
    return resp;
  }

  addEdit(id: number): Observable<ServiceDocument<NewItemModel>> {
    if (+id === 0) {
      return this.serviceDocument.new("/api/NewItem/New");
    } else {
      return this.serviceDocument.open("/api/NewItem/Open", new HttpParams().set("id", id.toString()));
    }
  }
}
