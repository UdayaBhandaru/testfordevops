import { Injectable } from "@angular/core";
import { Observable, Subject } from "rxjs";
import { HttpParams } from "@angular/common/http";
import { FormGroup, FormControl, Validators, FormBuilder } from "@angular/forms";
import { MessageType, ServiceDocument } from "@agility/frameworkcore";
import { SharedService } from "../Common/SharedService";
import { RbCommonService } from "../Common/RbCommonService";

@Injectable()
export class ItemSharedService extends RbCommonService {
  public _itemCode: number;
  public itemDetails: any;
  public itemType: string;
  public simplePackInd: string;
  public grandparentItem: string;

  itemWorkflowForm: FormGroup;

  constructor(public sharedService: SharedService, private fb: FormBuilder) {
    super(sharedService);
    this.itemWorkflowForm = this.fb.group({
      priority: ["", Validators.required],
      subComments: ""
    });
  }

  submitActions(that: any, reditectURL: string, localizationData: string, operation: string): void {
    if (that.serviceDocument.dataProfile.profileForm.valid) {
      that.serviceDocument.dataProfile.profileForm.controls["priority"].setValue(this.itemWorkflowForm.controls["priority"].value);
      that.serviceDocument.dataProfile.profileForm.controls["subComments"].setValue(this.itemWorkflowForm.controls["subComments"].value);
      if (operation === "submit") {
        that.service.submit().subscribe(() => {
          this.commonSubmitActionResult(that, reditectURL, localizationData, true);
        });
      } else if (operation === "reject") {
        that.service.reject().subscribe(() => {
          this.commonSubmitActionResult(that, reditectURL, localizationData);
        });
      } else if (operation === "needInfo") {
        that.service.sendBackForReview().subscribe(() => {
          this.commonSubmitActionResult(that, reditectURL, localizationData);
        });
      } else {
        this.sharedService.errorForm("operation is not valid");
      }
    } else {
      that.sharedService.validateForm(that.serviceDocument.dataProfile.profileForm);
      this.sharedService.errorForm("Please fill required fields");

    }
  }

  commonSubmitActionResult(that: any, reditectURL: string, localizationData: string, setItemCode?: boolean): void {
    if (that.service.serviceDocument.result.type === MessageType.success) {
      that.sharedService.saveForm(that.service.serviceDocument.dataProfile.dataModel.item + " " + localizationData).subscribe(() => {
        if (setItemCode) {
          this._itemCode = that.itemCode = +that.service.serviceDocument.dataProfile.dataModel.item;
        }
        that.router.navigate([reditectURL], { skipLocationChange: true });
      });
    } else {
      that.sharedService.errorForm(that.service.serviceDocument.result.innerException);
    }
  }

  changeDivision(that: any, item: any): void {
    that.category = [];
    that.class = [];
    that.subclass = [];
    that.serviceDocument.dataProfile.profileForm.controls["groupId"].setValue(null);
    that.serviceDocument.dataProfile.profileForm.controls["deptId"].setValue(null);
    that.serviceDocument.dataProfile.profileForm.controls["classId"].setValue(null);
    that.serviceDocument.dataProfile.profileForm.controls["subClassId"].setValue(null);
    that.group = that.groupData.filter(x => x.division === item.options.division);
  }

  changeGroup(that: any, item: any): void {
    that.class = [];
    that.subclass = [];
    that.serviceDocument.dataProfile.profileForm.controls["deptId"].setValue(null);
    that.serviceDocument.dataProfile.profileForm.controls["classId"].setValue(null);
    that.serviceDocument.dataProfile.profileForm.controls["subClassId"].setValue(null);
    that.department = that.departmentData.filter(x => x.groupNo === item.options.groupNo);
  }

  changeDepartment(that: any, item: any): void {
    that.subclass = [];
    that.serviceDocument.dataProfile.profileForm.controls["classId"].setValue(null);
    that.serviceDocument.dataProfile.profileForm.controls["subClassId"].setValue(null);
    that.class = that.classData.filter(x => x.dept === item.options.dept);
  }

  changeClass(that: any, item: any): void {
    that.serviceDocument.dataProfile.profileForm.controls["subClassId"].setValue(null);
    that.apiUrl = "/api/ItemBasicInfo/SubClassGetbyName?deptId=" + that.serviceDocument.dataProfile.profileForm.controls["deptId"].value
      + "&classId=" + item.options.class;

    that.subClassGetbyNameServiceSubscription = that.service.getDataFromAPI(that.apiUrl).subscribe(response => {
      that.subclass = response;
    });
  }


  buildBarCodeObject(itemBarcodes: any[]): any[] {
    let barCodeTableObj: any[] = [];
    if (itemBarcodes != null && itemBarcodes.length > 0) {
      itemBarcodes.forEach(x => {
        let lastScannedDate: string = new Date(x.lastScannedDate).toDateString();
        let barCodeObj: string[] = [x.barcodeType, x.barcodeType, x.prefix, lastScannedDate, x.primayRefInd];
        barCodeTableObj.push(barCodeObj);
      });
    }
    return barCodeTableObj;
  }
}

