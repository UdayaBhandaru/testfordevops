import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ItemComponent } from "./ItemComponent";
import { ItemListComponent } from "./ItemListComponent";
import { ItemResolver } from "./ItemResolver";
import { ItemListResolver } from './ItemListResolver';

const ItemRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "", redirectTo: "List", pathMatch: "full"
      },
      {
        path: "List",
        component: ItemListComponent,
        resolve:
        {
          serviceDocument: ItemListResolver
        }
      },
      {
        path: "New/:id",
        component: ItemComponent,
        resolve:
        {
          references: ItemResolver
        }
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(ItemRoutes)
  ]
})
export class ItemRoutingModule { }
