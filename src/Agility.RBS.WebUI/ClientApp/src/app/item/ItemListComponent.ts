import { Component, OnInit, ViewChild, OnDestroy } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, AjaxModel, AjaxResult } from "@agility/frameworkcore";
import { FormGroup, Validators, FormControl } from "@angular/forms";
import { Subscription } from "rxjs";
import { GridExportModel } from "../Common/grid/GridExportModel";
import { SharedService } from "../Common/SharedService";
import { GridEditComponent } from "../Common/grid/GridEditComponent";
import { DomainDetailModel } from "../domain/DomainDetailModel";
import { LoaderService } from "../Common/LoaderService";
import { SupplierDomainModel } from "../Common/DomainData/SupplierDomainModel";
import { SearchComponent } from "../Common/SearchComponent";
import { GridOptions, GridReadyEvent, IServerSideDatasource, IServerSideGetRowsParams, ColDef, GridApi } from "ag-grid-community";
import { DivisionDomainModel } from "../Common/DomainData/DivisionDomainModel";
import { DepartmentDomainModel } from "../Common/DomainData/DepartmentDomainModel";
import { GroupDomainModel } from "../Common/DomainData/GroupDomainModel";
import { ClassDomainModel } from "../Common/DomainData/ClassDomainModel";
import { SubClassDomainModel } from "../Common/DomainData/SubClassDomainModel";
import { WorkflowStatusDomainModel } from "../Common/DomainData/WorkflowStatusDomainModel";
import { BrandDomainModel } from "../Common/DomainData/BrandDomainModel";
import { NewItemSearchModel } from "./NewItemSearchModel";
import { ItemListService } from './ItemListService';
import { InfoComponent } from '../Common/InfoComponent';
import { ItemSharedService } from './ItemSharedService';

@Component({
  selector: "item-list",
  templateUrl: "./ItemListComponent.html"
})
export class ItemListComponent implements OnInit, IServerSideDatasource, OnDestroy {
  loadedPage: boolean;
  PageTitle = "Item Master";
  sheetName: string = "Item Master";
  redirectUrl = "Item/New/";
  searchServiceSubscription: Subscription;
  searchBarcodeServiceSubscription: Subscription;
  subClassGetbyNameServiceSubscription: Subscription;
  itemListSubscriptions: Subscription[] = [];
  domainDetailsIndicator: DomainDetailModel[]; // defaultIndicator: string;
  divisionData: DivisionDomainModel[];
  group: GroupDomainModel[]; groupData: GroupDomainModel[];
  department: DepartmentDomainModel[]; departmentData: DepartmentDomainModel[];
  class: ClassDomainModel[]; classData: ClassDomainModel[];
  subclass: SubClassDomainModel[]; subclassData: SubClassDomainModel[];
  workflowStatusList: WorkflowStatusDomainModel[];
  brandData: BrandDomainModel[];
  supplierData: SupplierDomainModel[];

  public serviceDocument: ServiceDocument<NewItemSearchModel>;
  componentName: any;
  columns: ColDef[];
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  model: NewItemSearchModel;
  additionalSearchParams: any;
  apiUrl: string; apiUrlBrand: string; apiUrlSupplier: string;
  formControl: FormControl;
  isDataLoaded: boolean = false;
  @ViewChild("searchPanel") searchPanel: SearchComponent;
  invalid: boolean = false;

  gridOptions: GridOptions;
  constructor(private sharedService: SharedService, private router: Router, public service: ItemListService
    , private loaderService: LoaderService
  ) {
  }

  ngOnInit(): void {
    this.formControl = new FormControl("", Validators.required);
    this.loaderService.display(true);
    this.componentName = this;
    this.columns = [
      { colId: "HEAD_SEQ", headerName: "Seq", field: "heaD_SEQ", tooltipField: "heaD_SEQ", sort: "desc" },
      { colId: "ITEM", headerName: "Item", field: "item", tooltipField: "item" },
      { colId: "DESCRIPTION", headerName: "Description", field: "description", tooltipField: "description" },
      { colId: "DIVISION_DESC", headerName: "Division", field: "divisionDesc", tooltipField: "divisionDesc" },
      { colId: "DEPT_DESC", headerName: "Department", field: "deptDesc", tooltipField: "deptDesc" },
      { colId: "CATEGORY_DESC", headerName: "Category", field: "categoryDesc", tooltipField: "categoryDesc" },
      { colId: "CLASS_DESC", headerName: "FineLine", field: "classDesc", tooltipField: "classDesc" },
      { colId: "SUB_CLASS_DESC", headerName: "Segment", field: "subclassDesc", tooltipField: "subclassDesc" },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 150, suppressSorting: true
      }
    ];
    this.gridOptions = {
      onGridReady: (params: GridReadyEvent) => {
        params.api.setServerSideDatasource(this);
      },
      context: {
        componentParent: this
      },
      suppressRowClickSelection: true,
      suppressCellSelection: true,
      rowModelType: "serverSide",
      paginationPageSize: this.service.itemMasterPaging.pageSize,
      pagination: true,
      enableServerSideSorting: true,
      enableServerSideFilter: true,
      cacheBlockSize: this.service.itemMasterPaging.cacheSize,
    };

    this.divisionData = this.service.serviceDocument.domainData["division"];
    this.groupData = this.service.serviceDocument.domainData["department"];
    this.departmentData = this.service.serviceDocument.domainData["category"];
    this.classData = this.service.serviceDocument.domainData["class"];
    this.workflowStatusList = this.service.serviceDocument.domainData["workflowStatusList"];
    this.apiUrlSupplier = "/api/Supplier/SuppliersGetName?name=";
    this.apiUrlBrand = "/api/ItemBasicInfo/BrandsGetName?name=";

    this.sharedService.domainData = {
      "division": this.divisionData,
      "department": this.groupData,
      "category": this.departmentData,
      "class": this.classData,
      "subClass": this.subclassData
    };

    this.model = {
      item: null, supplierId: null, barcode: null, divisionId: null, deptId: null, categoryId: null, classId: null
      , subclassId: null, description: null, heaD_SEQ: null, status: null
    };
    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
    this.loaderService.display(false);
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  getRows(params: IServerSideGetRowsParams): void {
    this.isDataLoaded = false;
    this.additionalSearchParams = { startRow: params.request.startRow, endRow: params.request.endRow, sortModel: params.request.sortModel };
    if (!this.loadedPage) {
      this.loadedPage = true;
      params.successCallback(
        this.serviceDocument.dataProfile.dataList.slice(params.request.startRow, params.request.endRow),
        this.service.serviceDocument.dataProfile.dataModel.totalRows);
    } else {
      this.searchServiceSubscription = this.service.search(this.additionalSearchParams).subscribe((sDoc) => {
        this.isDataLoaded = true;
        params.successCallback(sDoc.dataProfile.dataList, this.service.serviceDocument.dataProfile.dataModel.totalRows);
      });
    }
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
      this.sharedService.previousUrl = "../Item/List";
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl + cell.data.heaD_SEQ, this.service.serviceDocument.dataProfile.dataList);
  }

  changeDivision(item: any): void {
    this.department = [];
    this.class = [];
    this.subclass = [];
    this.serviceDocument.dataProfile.profileForm.controls["categoryId"].setValue(null);
    this.serviceDocument.dataProfile.profileForm.controls["deptId"].setValue(null);
    this.serviceDocument.dataProfile.profileForm.controls["classId"].setValue(null);
    this.serviceDocument.dataProfile.profileForm.controls["subclassId"].setValue(null);
    this.group = this.groupData.filter(x => x.division === item.options.division);
  }

  changeDepartment(item: any): void {
    this.subclass = [];
    this.serviceDocument.dataProfile.profileForm.controls["classId"].setValue(null);
    this.serviceDocument.dataProfile.profileForm.controls["subclassId"].setValue(null);
    this.class = this.classData.filter(x => x.dept === item.options.dept);
  }

  changeGroup(item: any): void {
    this.class = [];
    this.subclass = [];
    this.serviceDocument.dataProfile.profileForm.controls["categoryId"].setValue(null);
    this.serviceDocument.dataProfile.profileForm.controls["classId"].setValue(null);
    this.serviceDocument.dataProfile.profileForm.controls["subclassId"].setValue(null);
    this.department = this.departmentData.filter(x => x.groupNo === item.options.groupNo);
  }

  changeClass(item: any): void {
    this.serviceDocument.dataProfile.profileForm.controls["subclassId"].setValue(null);
    this.apiUrl = "/api/Item/SubClassGetbyName?deptId=" + this.serviceDocument.dataProfile.profileForm.controls["categoryId"].value
      + "&classId=" + item.options.class;

    this.subClassGetbyNameServiceSubscription = this.service.getDataFromAPI(this.apiUrl)
      .subscribe((response: AjaxModel<SubClassDomainModel[]>) => {
        if (response.result === AjaxResult.success) {
          this.subclass = response.model;
        } else {
          this.sharedService.errorForm(response.message);
        }
        //this.changeRef.detectChanges();
      }, (error: string) => { console.log(error); });
  }

  add(): void {
    this.router.navigate(["/" + this.redirectUrl + "0"], { skipLocationChange: true });
    this.sharedService.previousUrl = "../Item/List";
  }

  ngOnDestroy(): void {
    if (this.searchServiceSubscription) {
      this.searchServiceSubscription.unsubscribe();
    }
    if (this.searchBarcodeServiceSubscription) {
      this.searchBarcodeServiceSubscription.unsubscribe();
    }
    if (this.subClassGetbyNameServiceSubscription) {
      this.subClassGetbyNameServiceSubscription.unsubscribe();
    }

    this.divisionData.length = 0;
    this.departmentData.length = 0;
    this.groupData.length = 0;
    this.classData.length = 0;
  }
}
