export class NewItemPriceModel {
  currentPrice?: number;
  priceCurrencyCode?: string;
  margin?: number;
  exchangeRate?: number;
  format?: string;
  netMargin?: number;
  countryCode: string;
  countryName: string;
  operation: string;


  ssValue?: number;
  sssValue?: number;
  whsValue?: number;
  csValue?: number;

  ssMargin?: number;
  sssMargin?: number;
  whsMargin?: number;
  csMargin?: number;

  ssNetMargin?: number;
  sssNetMargin?: number;
  whsNetMargin?: number;
  csNetMargin?: number;
  descLong?: string;
  compQty?: number;
  item?: string;
}
