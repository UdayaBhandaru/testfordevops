import { ItemMasterModel } from './ItemMasterModel';
import { ItemStoreRangingModel } from './ItemStoreRangingModel';

export class NewItemModel {
  heaD_SEQ?: number;
  description: string;
  supplierId?: number;
  supplierName: string;
  divisionId?: number;
  divisionDesc: string;
  operation: string;
  organizationId?: number;
  createdBy: string;
  modifiedBy: string;
  deletedInd?: boolean;
  createdDate?: Date;
  modifiedDate?: Date;
  wfNxtStateInd?: number;
  status: string;
  fileCount?: number;
  itemMasters: ItemMasterModel[];
  deptId?: number;
  categoryId?: number;
  classId?: number;
  subclassId?: number;
  itemTier: string;
  brandId: string;
  brandName: string;
  standardUom: string;
  originCountry: string;
  workflowInstance?: string;

  expiryFlag?: string;
  expiryOutbound?: number;
  expiryInbound?: number;
  item?: string;
  replenish?: string;

  deptDesc?: string;
  categoryDesc?: string;
  classDesc?: string;
  subclassDesc?: string;

  countries?: string[];
  itemType?: string;
  countryId?: string;
  itemStoreRangings?: ItemStoreRangingModel[];
}
