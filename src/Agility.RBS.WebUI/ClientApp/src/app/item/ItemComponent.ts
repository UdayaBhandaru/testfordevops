import { Component, ViewChild, OnInit, ChangeDetectorRef, OnDestroy } from "@angular/core";
import { Subscription } from "rxjs";
import { CommonService, ServiceDocument, FxContext, IdentityMenuModel, MessageResult, MessageType, AjaxModel, AjaxResult } from "@agility/frameworkcore";
import { ActivatedRoute, Router } from "@angular/router";
import { SharedService } from "../Common/SharedService";
import { ItemService } from "./ItemService";
import { DocumentsConstants } from "../Common/DocumentsConstants";
import { LoaderService } from "../Common/LoaderService";
import { NewItemModel } from './NewItemModel';
import { ItemMasterModel } from './ItemMasterModel';
import { GridApi, GridOptions, RowNode, ColDef, GridReadyEvent } from 'ag-grid-community';
import { GridInputComponent } from '../Common/grid/GridInputComponent';
import { GridSelectComponent } from '../Common/grid/GridSelectComponent';
import { GridNumericComponent } from '../Common/grid/GridNumericComponent';
import { DomainDetailModel } from '../domain/DomainDetailModel';
import { DivisionDomainModel } from '../Common/DomainData/DivisionDomainModel';
import { GroupDomainModel } from '../Common/DomainData/GroupDomainModel';
import { DepartmentDomainModel } from '../Common/DomainData/DepartmentDomainModel';
import { ClassDomainModel } from '../Common/DomainData/ClassDomainModel';
import { SubClassDomainModel } from '../Common/DomainData/SubClassDomainModel';
import { BrandDomainModel } from '../Common/DomainData/BrandDomainModel';
import { SupplierDomainModel } from '../Common/DomainData/SupplierDomainModel';
import { UomDomainModel } from '../Common/DomainData/UomDomainModel';
import { CountryDomainModel } from '../Common/DomainData/CountryDomainModel';
import { FormControl } from '@angular/forms';
import { GridDeleteComponent } from '../Common/grid/GridDeleteComponent';
import { GridAmountComponent } from '../Common/grid/GridAmountComponent';
import { GridGetBarcodeComponent } from '../Common/grid/GridGetBarcodeComponent';
import { ItemStoreRangingModel } from './ItemStoreRangingModel';
import { CountryCurrDomainModel } from '../Common/DomainData/CountryCurrDomainModel';
import { NewItemPriceModel } from './NewItemPriceModel';
import { NumericEditorComponent } from '../Common/grid/NumericEditorComponent';

@Component({
  selector: "item-add",
  templateUrl: "./ItemComponent.html",
  styleUrls: ['./ItemComponent.scss']
})
export class ItemComponent implements OnInit {
  _componentName = "Item Creation";
  PageTitle: string = "Item Creation";
  public serviceDocument: ServiceDocument<NewItemModel>;
  localizationData: any;
  model: NewItemModel;
  editMode: boolean;
  primaryId: number; // used in CanDeactivate method, should be same name if we use candeactivate
  routeServiceSubscription: Subscription;
  subClassGetbyNameServiceSubscription: Subscription;
  barcodeServiceSubscription: Subscription;
  documentsConstants: DocumentsConstants = new DocumentsConstants();
  screenName: string;
  column: any[]; columnCost: any[]; columnPrice: any[]; columnBarcode: any[]; columnStore: any[];
  gridApi: GridApi; gridApiCost: GridApi; gridApiPrice: GridApi; gridApiBarcode: GridApi; gridApiStore: GridApi;
  gridOptions: GridOptions; gridOptionsCost: GridOptions; gridOptionsPrice: GridOptions; gridOptionsBarcode: GridOptions; gridOptionsStore: GridOptions;
  data: ItemMasterModel[] = [];
  dataPrice: NewItemPriceModel[] = [];
  dataStore: ItemStoreRangingModel[] = [];
  itemMasterModel: ItemMasterModel;
  itemPriceModel: NewItemPriceModel;
  indicatorData: DomainDetailModel[];
  apiUrlSupplier: string; apiUrlBrand: string; apiUrl: string;
  componentParentName: ItemComponent;
  status: string;
  divisionData: DivisionDomainModel[];
  group: GroupDomainModel[]; groupData: GroupDomainModel[];
  department: DepartmentDomainModel[]; departmentData: DepartmentDomainModel[];
  class: ClassDomainModel[]; classData: ClassDomainModel[];
  subclass: SubClassDomainModel[]; subclassData: SubClassDomainModel[];
  brandData: BrandDomainModel[];
  supplierData: SupplierDomainModel[];
  uomData: UomDomainModel[];
  iTierData: DomainDetailModel[];
  somData: DomainDetailModel[];
  countryData: CountryDomainModel[];
  itemtypeData: DomainDetailModel[];
  sourceMethodData: DomainDetailModel[];
  rangeCountryData: CountryCurrDomainModel[];
  constructor(private route: ActivatedRoute, public router: Router, private changeRef: ChangeDetectorRef
    , public sharedService: SharedService, private service: ItemService, private loaderService: LoaderService) {
  }

  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }

  ngOnInit(): void {
    this.componentParentName = this;
    this.screenName = this.documentsConstants.itemObjectName;
    this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
    this.apiUrlSupplier = "/api/Supplier/SuppliersGetName?name=";
    this.divisionData = this.service.serviceDocument.domainData["division"];
    this.groupData = this.service.serviceDocument.domainData["group"];
    this.departmentData = this.service.serviceDocument.domainData["department"];
    this.classData = this.service.serviceDocument.domainData["class"];
    this.apiUrlBrand = "/api/Item/BrandsGetName?name=";
    this.indicatorData = this.service.serviceDocument.domainData["indicator"];
    this.uomData = this.service.serviceDocument.domainData["uomList"];
    this.iTierData = this.service.serviceDocument.domainData["itemtier"];
    this.somData = this.service.serviceDocument.domainData["som"];
    this.countryData = Object.assign([], this.service.serviceDocument.domainData["country"]);
    this.supplierData = this.service.serviceDocument.domainData["supplier"];
    this.subclass = this.service.serviceDocument.domainData["subclass"];
    this.itemtypeData = this.service.serviceDocument.domainData["itemType"];
    this.sourceMethodData = this.service.serviceDocument.domainData["sourceMethods"];
    ////this.uomData = this.uomData.filter(id => id.uomClass === "MASS");
    this.rangeCountryData = this.service.serviceDocument.domainData["rangecountry"];

    this.column = [
      { headerName: "Long Desc", field: "descLong", tooltipField: "descLong", editable: true },
      { headerName: "Short Desc", field: "shortDesc", tooltipField: "shortDesc", editable: true },
      { headerName: "Arabic Desc", field: "secondaryDesc", tooltipField: "secondaryDesc", editable: true },
      {
        headerName: "Sellable", field: "sellable", cellRendererFramework: GridSelectComponent
        , cellRendererParams: { references: this.indicatorData }, tooltipField: "programMessage"
      },
      {
        headerName: "Orderable", field: "orderable", cellRendererFramework: GridSelectComponent
        , cellRendererParams: { references: this.indicatorData }, tooltipField: "programMessage"
      },
      {
        headerName: "Inventory", field: "inventory", cellRendererFramework: GridSelectComponent
        , cellRendererParams: { references: this.indicatorData }, tooltipField: "programMessage"
      },
      {
        headerName: "SOM", field: "som", cellRendererFramework: GridSelectComponent
        , cellRendererParams:
          { references: this.somData }, tooltipField: "programMessage"
      },
      // { headerName: "Pack Size", field: "compQty", tooltipField: "compQty", cellRendererFramework: GridNumericComponent },

      {
        headerName: "Pack Size", field: "compQty", tooltipField: "compQty", editable: true, cellEditorFramework: NumericEditorComponent
      },
      {
        headerName: "UOP", field: "uop", cellRendererFramework: GridSelectComponent
        , cellRendererParams:
          { references: this.uomData, keyvaluepair: { key: "uomCode", value: "uomDescription" } }, tooltipField: "programMessage"
      },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridDeleteComponent, width: 80
      }

    ];
    this.gridOptions = {
      singleClickEdit: true,
      onGridReady: (params: GridReadyEvent) => {
        this.gridApi = params.api;
      }
    };

    this.columnCost = [
      { headerName: "Description", field: "descLong", tooltipField: "descLong" },
      { headerName: "Pack Size", field: "compQty", tooltipField: "compQty" },
      { headerName: "VPN", field: "vpn", tooltipField: "vpn", cellRendererFramework: GridInputComponent },
      {
        headerName: "PACK DETAILS",
        children: [
          { headerName: "Inner", field: "innerQty", tooltipField: "innerQty", cellRendererFramework: GridNumericComponent },
          { headerName: "Case", field: "caseQty", tooltipField: "caseQty", cellRendererFramework: GridNumericComponent }
        ]
      },
      {
        headerName: "SUPPLIER COST",
        children: [
          { headerName: "Case", field: "suppCaseCost", tooltipField: "suppCaseCost", cellRendererFramework: GridNumericComponent },
          { headerName: "Unit", field: "suppUnitCost", tooltipField: "suppUnitCost", cellRendererFramework: GridAmountComponent },
          { headerName: "Invoice Discount %", field: "invoiceDiscount", tooltipField: "invoiceDiscount" }
        ]
      },
      {
        headerName: "NET SUPPLIER COST",
        children: [
          { headerName: "Case", field: "netSuppCaseCost", tooltipField: "netSuppCaseCost", cellRendererFramework: GridAmountComponent },
          { headerName: "Unit", field: "netSuppUnitCost", tooltipField: "netSuppUnitCost", cellRendererFramework: GridAmountComponent }
        ]
      }
    ];
    this.gridOptionsCost = {
      onGridReady: (params: any) => {
        this.gridApiCost = params.api;
      }
    };

    this.columnPrice = [
      { headerName: "Country", field: "countryName", tooltipField: "countryName", rowGroup: true, hide: true },
      { headerName: "Currency", field: "priceCurrencyCode", tooltipField: "priceCurrencyCode" },
      { headerName: "Description", field: "descLong", tooltipField: "descLong" },
      { headerName: "Pack Size", field: "compQty", tooltipField: "compQty" },
      {
        headerName: "RSP",
        children: [
          {
            headerName: "SS", field: "ssValue", tooltipField: "ssValue", editable: true, cellEditorFramework: NumericEditorComponent, cellStyle: { border: "1px solid green" }
          },
          {
            headerName: "SS-", field: "sssValue", tooltipField: "sssValue", editable: true, cellEditorFramework: NumericEditorComponent
          },
          {
            headerName: "WHS", field: "whsValue", tooltipField: "whsValue", editable: true, cellEditorFramework: NumericEditorComponent
          },
          {
            headerName: "CS", field: "csValue", tooltipField: "csValue", editable: true, cellEditorFramework: NumericEditorComponent
          }
        ]
      },
      {
        headerName: "FRONT MARGIN",
        children: [
          { headerName: "SS", field: "ssMargin", tooltipField: "ssMargin" },
          { headerName: "SS-", field: "sssMargin", tooltipField: "sssMargin" },
          { headerName: "WHS", field: "whsMargin", tooltipField: "whsMargin" },
          { headerName: "CS", field: "csMargin", tooltipField: "csMargin" }
        ]
      },
      {
        headerName: "NET MARGIN",
        children: [
          { headerName: "SS", field: "ssNetMargin", tooltipField: "ssNetMargin" },
          { headerName: "SS-", field: "sssNetMargin", tooltipField: "sssNetMargin" },
          { headerName: "WHS", field: "whsNetMargin", tooltipField: "whsNetMargin" },
          { headerName: "CS", field: "csNetMargin", tooltipField: "csNetMargin" }
        ]
      },
    ];
    this.gridOptionsPrice = {
      singleClickEdit: true,
      onGridReady: (params: any) => {
        this.gridApiPrice = params.api;
      }
    };

    this.columnBarcode = [
      { headerName: "Description", field: "descLong", tooltipField: "descLong" },
      { headerName: "Pack Size", field: "compQty", tooltipField: "compQty" },
      {
        headerName: "Barcode Type", field: "itemType", cellRendererFramework: GridSelectComponent
        , cellRendererParams: { references: this.itemtypeData }, tooltipField: "programMessage"
      },
      { headerName: "Barcode", field: "barcode", tooltipField: "barcode" },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridGetBarcodeComponent, width: 100
      }

    ];
    this.gridOptionsBarcode = {
      onGridReady: (params: any) => {
        this.gridApiBarcode = params.api;
      }
    };

    this.columnStore = [
      { headerName: "Location", field: "loc", tooltipField: "loc" },
      { headerName: "Location Name", field: "locName", tooltipField: "locName" },
      {
        headerName: "Sourcing method", field: "sourceMethod", tooltipField: "sourceMethod"
        , cellRendererFramework: GridSelectComponent
        , cellRendererParams: { references: this.sourceMethodData }
      }
    ];
    this.gridOptionsStore = {
      onGridReady: (params: any) => {
        this.gridApiStore = params.api;
      }
    };

    if (this.service.serviceDocument.dataProfile.dataModel) {
      this.bind();
    }
  }

  bind(): void {
    this.routeServiceSubscription = this.route.params.subscribe((resp: any) => {
      this.primaryId = resp["id"];
    });
    if (this.primaryId.toString() !== "0") {
      this.editMode = true;
      this.PageTitle = `${this.PageTitle} - EDIT - ${this.primaryId}`;
      if (this.editMode) {
        this.status = this.service.serviceDocument.dataProfile.dataModel.status;
        if (this.service.serviceDocument.dataProfile.dataModel.itemMasters) {
          Object.assign(this.data, this.service.serviceDocument.dataProfile.dataModel.itemMasters);
          for (var n = 0; n < this.data.length; n++) {
            let rCountries: string[] = this.service.serviceDocument.dataProfile.dataModel.countries;
            let itemPrices: NewItemPriceModel[] = this.data[n].itemPrices;
            if (rCountries.length > 0) {
              for (var i = 0; i < rCountries.length; i++) {
                let countryDetails: CountryCurrDomainModel = this.rangeCountryData.filter(ids => ids.countryCode === rCountries[i])[0];
                if (itemPrices.length > 0) {
                  this.itemPriceModel = {
                    operation: "U", countryCode: countryDetails.countryCode, countryName: countryDetails.countryName, exchangeRate: countryDetails.exchangeRate
                    , priceCurrencyCode: countryDetails.currencyCode, ssValue: null, sssValue: null, whsValue: null, csValue: null, compQty: this.data[n].compQty
                    , descLong: this.data[n].descLong, item: this.data[n].item
                  };

                  var itemPriceCountry = itemPrices.filter(ids => ids.countryCode === countryDetails.countryCode);
                  for (var j = 0; j < itemPriceCountry.length; j++) {
                    if (itemPriceCountry[j].format === "SS") {
                      this.itemPriceModel.ssValue = itemPriceCountry[j].currentPrice
                    } else if (itemPriceCountry[j].format === "SS-") {
                      this.itemPriceModel.sssValue = itemPriceCountry[j].currentPrice
                    } else if (itemPriceCountry[j].format === "WHS") {
                      this.itemPriceModel.whsValue = itemPriceCountry[j].currentPrice
                    } else if (itemPriceCountry[j].format === "CS") {
                      this.itemPriceModel.csValue = itemPriceCountry[j].currentPrice
                    }
                  }
                  this.dataPrice.unshift(this.itemPriceModel);
                } else {
                  this.itemPriceModel = {
                    operation: "I", countryCode: countryDetails.countryCode, countryName: countryDetails.countryName, exchangeRate: countryDetails.exchangeRate
                    , priceCurrencyCode: countryDetails.currencyCode, ssValue: null, sssValue: null, whsValue: null, csValue: null, compQty: this.data[n].compQty
                    , descLong: this.data[n].descLong, item: this.data[n].item
                  };

                  this.dataPrice.unshift(this.itemPriceModel);
                }
              }
            }
          }
          if (this.gridApiPrice) {
            this.gridApiPrice.setRowData(this.dataPrice);
          }
        }

        if (this.service.serviceDocument.dataProfile.dataModel.itemStoreRangings) {
          Object.assign(this.dataStore, this.service.serviceDocument.dataProfile.dataModel.itemStoreRangings);
        }
      }
    } else {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign(this.service.serviceDocument.dataProfile.dataModel
        , { expiryFlag: "N", operation: "I", status: "WORKSHEET" }, { workflowInstance: this.service.serviceDocument.dataProfile.dataModel.workflowInstance });
      this.service.serviceDocument.dataProfile.profileForm.patchValue(this.service.serviceDocument.dataProfile.dataModel);
      this.PageTitle = `${this.PageTitle} - ADD`;

      this.newBasicModel();
      this.itemMasterModel.compQty = 1;
      this.data.unshift(this.itemMasterModel);
      if (this.gridApi) {
        this.gridApi.setRowData(this.data);

      }
    }

    this.serviceDocument = this.service.serviceDocument;
    this.serviceDocument.dataProfile.profileForm.controls["countries"] = new FormControl();
    this.serviceDocument.dataProfile.profileForm.controls["countries"].setValue(this.serviceDocument.dataProfile.dataModel.countries);
    this.sharedService.fileCount = this.service.serviceDocument.dataProfile.dataModel.fileCount;
    this.sharedService.checkWfPermitions(this.serviceDocument);
  }

  save(saveOnly: boolean): void {
    this.gridApi.stopEditing();
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.saveSubscription(saveOnly);
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  saveAndContinue(): void {
    this.save(false);
  }

  saveSubscription(saveOnly: boolean): void {
    this.loaderService.display(true);
    this.serviceDocument.dataProfile.profileForm.controls["itemStoreRangings"] = new FormControl();
    this.serviceDocument.dataProfile.profileForm.controls["itemStoreRangings"].setValue(this.dataStore);
    this.serviceDocument.dataProfile.profileForm.controls["itemMasters"] = new FormControl();
    this.serviceDocument.dataProfile.profileForm.controls["itemMasters"].setValue(this.data);
    this.dataPrice = this.dataPrice.filter(id => id.ssValue !== null || id.sssValue !== null || id.whsValue !== null || id.csValue !== null);
    if (this.dataPrice.length > 0) {
      for (var n = 0; n < this.data.length; n++) {
        let rCountries: string[] = this.service.serviceDocument.dataProfile.dataModel.countries;
        for (var i = 0; i < rCountries.length; i++) {
          let itemPriceDtls: NewItemPriceModel = this.dataPrice.filter(id => id.countryCode === rCountries[i] && id.item === this.data[n].item)[0];
          if (itemPriceDtls.ssValue) {
            this.itemPriceModel = {
              operation: itemPriceDtls.operation, countryCode: itemPriceDtls.countryCode, format: "SS", currentPrice: itemPriceDtls.ssValue
              , countryName: itemPriceDtls.countryName
            };
            this.data[n].itemPrices.push(this.itemPriceModel);
          }

          if (itemPriceDtls.sssValue) {
            this.itemPriceModel = {
              operation: itemPriceDtls.operation, countryCode: itemPriceDtls.countryCode, format: "SS-", currentPrice: itemPriceDtls.sssValue
              , countryName: itemPriceDtls.countryName
            };
            this.data[n].itemPrices.push(this.itemPriceModel);
          }

          if (itemPriceDtls.whsValue) {
            this.itemPriceModel = {
              operation: itemPriceDtls.operation, countryCode: itemPriceDtls.countryCode, format: "WHS", currentPrice: itemPriceDtls.whsValue
              , countryName: itemPriceDtls.countryName
            };
            this.data[n].itemPrices.push(this.itemPriceModel);
          }

          if (itemPriceDtls.csValue) {
            this.itemPriceModel = {
              operation: itemPriceDtls.operation, countryCode: itemPriceDtls.countryCode, format: "CS", currentPrice: itemPriceDtls.csValue
              , countryName: itemPriceDtls.countryName
            };
            this.data[n].itemPrices.push(this.itemPriceModel);
          }
        }
      }
    }

    this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.primaryId = this.service.serviceDocument.dataProfile.dataModel.heaD_SEQ;
        this.sharedService.saveForm(`Item Saved successfully - Request ID ${this.primaryId}`).subscribe(() => {
          if (saveOnly) {
            this.router.navigate(["/Item/List"], { skipLocationChange: true });
          } else {
            this.router.navigate(["/Blank"], {
              skipLocationChange: true, queryParams: {
                id: "/Item/New/" + this.primaryId
              }
            });
            this.loaderService.display(false);
          }
        }, (error: string) => {
          console.log(error);
          this.loaderService.display(false);
        });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
        this.loaderService.display(false);
      }
    }, (error: string) => {
      console.log(error);
      this.loaderService.display(false);
    });
  }

  reset(): void {

  }

  ngOnDestroy(): void {
    this.routeServiceSubscription.unsubscribe();
  }

  newBasicModel(): void {
    this.itemMasterModel = {
      operation: "I", item: null, descLong: null, shortDesc: null, secondaryDesc: null, sellable: null, orderable: null, inventory: null
      , som: null, tranLevel: null, uop: null, vpn: null, hts: null, compQty: null
    };
  }

  changeDivision(item: any): void {
    this.department = [];
    this.class = [];
    this.subclass = [];
    this.serviceDocument.dataProfile.profileForm.controls["categoryId"].setValue(null);
    this.serviceDocument.dataProfile.profileForm.controls["deptId"].setValue(null);
    this.serviceDocument.dataProfile.profileForm.controls["classId"].setValue(null);
    this.serviceDocument.dataProfile.profileForm.controls["subclassId"].setValue(null);
    this.group = this.groupData.filter(x => x.division === item.options.division);
  }

  changeDepartment(item: any): void {
    this.subclass = [];
    this.serviceDocument.dataProfile.profileForm.controls["classId"].setValue(null);
    this.serviceDocument.dataProfile.profileForm.controls["subclassId"].setValue(null);
    this.class = this.classData.filter(x => x.dept === item.options.dept);
  }

  changeGroup(item: any): void {
    this.class = [];
    this.subclass = [];
    this.serviceDocument.dataProfile.profileForm.controls["categoryId"].setValue(null);
    this.serviceDocument.dataProfile.profileForm.controls["classId"].setValue(null);
    this.serviceDocument.dataProfile.profileForm.controls["subclassId"].setValue(null);
    this.department = this.departmentData.filter(x => x.groupNo === item.options.groupNo);
  }

  changeClass(item: any): void {
    this.serviceDocument.dataProfile.profileForm.controls["subclassId"].setValue(null);
    this.apiUrl = "/api/Item/SubClassGetbyName?deptId=" + this.serviceDocument.dataProfile.profileForm.controls["categoryId"].value
      + "&classId=" + item.options.class;

    this.subClassGetbyNameServiceSubscription = this.service.getDataFromAPI(this.apiUrl)
      .subscribe((response: AjaxModel<SubClassDomainModel[]>) => {
        if (response.result === AjaxResult.success) {
          this.subclass = response.model;
        } else {
          this.sharedService.errorForm(response.message);
        }
        //this.changeRef.detectChanges();
      }, (error: string) => { console.log(error); });
  }

  addBasic($event: MouseEvent): void {
    $event.preventDefault();
    let filterData: ItemMasterModel[] = this.data.filter(id => id.descLong === null);
    if (filterData.length === 0) {
      this.newBasicModel();
      this.data.unshift(this.itemMasterModel);
      if (this.gridApi) {
        this.gridApi.setRowData(this.data);
      }
    }
  }

  select(cell: any, event: any): void {
    if (cell.column.colId === "sourceMethod") {
      this.dataStore[cell.rowIndex][cell.column.colId] = event.target.value;
      let rowNode: RowNode = this.gridApiStore.getRowNode(cell.rowIndex);
      rowNode.setData(rowNode.data);
    }
    else {
      this.data[cell.rowIndex][cell.column.colId] = event.target.value;
      let rowNode: RowNode = this.gridApi.getRowNode(cell.rowIndex);
      rowNode.setData(rowNode.data);
    }
  }

  generateBarcode(cell: any, event: any): void {
    if (this.data[cell.rowIndex]["itemType"]) {
      this.apiUrl = "/api/Item/GenerateBarcode?type=" + this.data[cell.rowIndex]["itemType"];
      this.barcodeServiceSubscription = this.service.getDataFromAPI(this.apiUrl)
        .subscribe((response: AjaxModel<string>) => {
          if (response.result === AjaxResult.success) {
            this.data[cell.rowIndex]["barcode"] = response.model;
            let rowNode: RowNode = this.gridApiBarcode.getRowNode(cell.rowIndex);
            rowNode.setData(rowNode.data);
          } else {
            this.sharedService.errorForm(response.message);
          }
        }, (error: string) => { console.log(error); });
    } else {
      this.data[cell.rowIndex]["barcode"] = null;
    }
  }

  update(cell: any, event: any): void {
    if (cell.column.colId === "descLong" || cell.column.colId === "shortDesc" || cell.column.colId === "secondaryDesc"
      || cell.column.colId === "compQty") {
      this.data[cell.rowIndex][cell.column.colId] = event.target.value;
      let rowNode: RowNode = this.gridApi.getRowNode(cell.rowIndex);
      rowNode.setData(rowNode.data);
    } else if (cell.column.colId === "vpn" || cell.column.colId === "innerQty" || cell.column.colId === "caseQty"
      || cell.column.colId === "suppCaseCost") {
      this.data[cell.rowIndex][cell.column.colId] = event.target.value;
      if (cell.column.colId === "caseQty" || cell.column.colId === "suppCaseCost") {
        this.data[cell.rowIndex]["suppUnitCost"] = null;
        if (this.data[cell.rowIndex]["caseQty"] && this.data[cell.rowIndex]["suppCaseCost"]) {
          let caseQty: number = this.data[cell.rowIndex]["caseQty"];
          let suppCaseCost: number = this.data[cell.rowIndex]["suppCaseCost"];
          this.data[cell.rowIndex]["suppUnitCost"] = suppCaseCost / caseQty;
          if (this.data[cell.rowIndex]["invoiceDiscount"]) {
            let invoiceDiscount: number = this.data[cell.rowIndex]["invoiceDiscount"];
            let suppUnitCost: number = this.data[cell.rowIndex]["suppUnitCost"];
            this.data[cell.rowIndex]["netSuppCaseCost"] = suppCaseCost - (suppCaseCost * invoiceDiscount);
            this.data[cell.rowIndex]["netSuppUnitCost"] = suppUnitCost - (suppUnitCost * invoiceDiscount);
          } else {
            this.data[cell.rowIndex]["netSuppCaseCost"] = suppCaseCost;
            this.data[cell.rowIndex]["netSuppUnitCost"] = this.data[cell.rowIndex]["suppUnitCost"];
          }
        }
      }

      let rowNode: RowNode = this.gridApiCost.getRowNode(cell.rowIndex);
      rowNode.setData(rowNode.data);
    }

    else if (cell.column.colId === "ssValue" || cell.column.colId === "sssValue" || cell.column.colId === "whsValue"
      || cell.column.colId === "csValue") {
      this.dataPrice[cell.rowIndex][cell.column.colId] = event.target.value;
      //var itemDtls = this.data.filter(ids => ids.item === this.dataPrice[cell.rowIndex].item)[0];
      //let invoiceDiscount: number = itemDtls["invoiceDiscount"];
      //let suppUnitCost: number = itemDtls["suppUnitCost"];

      //if (cell.column.colId === "ssValue") {
      //  if (this.dataPrice[cell.rowIndex]["ssValue"]) {
      //    let ssValue: number = this.dataPrice[cell.rowIndex]["ssValue"];
      //    let ssMargin: number = Math.trunc(((ssValue - suppUnitCost) / ssValue) * 100)
      //    this.dataPrice[cell.rowIndex]["ssMargin"] = ssMargin;
      //    this.dataPrice[cell.rowIndex]["ssNetMargin"] = invoiceDiscount + ssMargin;
      //  }
      //} else if (cell.column.colId === "sssValue") {
      //  if (this.dataPrice[cell.rowIndex]["sssValue"]) {
      //    let sssValue: number = this.dataPrice[cell.rowIndex]["sssValue"];
      //    let sssMargin: number = Math.trunc(((sssValue - suppUnitCost) / sssValue) * 100)
      //    this.dataPrice[cell.rowIndex]["sssMargin"] = sssMargin;
      //    this.dataPrice[cell.rowIndex]["sssNetMargin"] = invoiceDiscount + sssMargin;
      //  }
      //} else if (cell.column.colId === "whsValue") {
      //  if (this.dataPrice[cell.rowIndex]["whsValue"]) {
      //    let whsValue: number = this.dataPrice[cell.rowIndex]["whsValue"];
      //    let whsMargin: number = Math.trunc(((whsValue - suppUnitCost) / whsValue) * 100)
      //    this.dataPrice[cell.rowIndex]["whsMargin"] = whsMargin;
      //    this.dataPrice[cell.rowIndex]["whsNetMargin"] = invoiceDiscount + whsMargin;
      //  }
      //} else if (cell.column.colId === "csValue") {
      //  if (this.dataPrice[cell.rowIndex]["csValue"]) {
      //    let csValue: number = this.dataPrice[cell.rowIndex]["csValue"];
      //    let csMargin: number = Math.trunc(((csValue - suppUnitCost) / csValue) * 100)
      //    this.dataPrice[cell.rowIndex]["csMargin"] = csMargin;
      //    this.dataPrice[cell.rowIndex]["csNetMargin"] = invoiceDiscount + csMargin;
      //  }
      //}
      let rowNode: RowNode = this.gridApiPrice.getRowNode(cell.rowIndex);
      rowNode.setData(rowNode.data);
    }
  }

  ////changeSelectedCountry(event: any): void {
  ////  if (!this.editMode) {

  ////  }
  ////}
}

