import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../AgGridSharedModule";
import { TitleSharedModule } from "../TitleSharedModule";
import { ItemRoutingModule } from "./ItemRoutingModule";

import { ItemComponent } from "./ItemComponent";
import { ItemService } from "./ItemService";
import { ItemListComponent } from "./ItemListComponent";
import { ItemResolver } from "./ItemResolver";
import { ItemSharedService } from "./ItemSharedService";
import { MatExpansionModule } from "@angular/material";
import { InboxService } from "../inbox/InboxService";
import { LicenseManager } from "ag-grid-enterprise/main";
import { RbsSharedModule } from "../RbsSharedModule";
import { ItemListResolver } from './ItemListResolver';
import { ItemListService } from './ItemListService';

@NgModule({
  imports: [
    FrameworkCoreModule,
    ItemRoutingModule,
    AgGridModule,
    AgGridSharedModule,
    TitleSharedModule,
    MatExpansionModule,
    RbsSharedModule
  ],
  declarations: [
    ItemComponent,
    ItemListComponent,
  ],
  providers: [
    ItemService,
    ItemResolver,
    ItemListService,
    ItemListResolver,
    ItemSharedService,
    InboxService
  ]
})
export class ItemModule {
  constructor() {
    LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
  }
}

