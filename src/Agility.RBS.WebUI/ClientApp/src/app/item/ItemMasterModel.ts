import { NewItemPriceModel } from './NewItemPriceModel';

export class ItemMasterModel {
  item: string;
  itemType?: string;
  itemLevel?: string;
  tranLevel?: string;
  descLong?: string;
  secondaryDesc?: string;
  shortDesc?: string;
  brandId?: number;
  systemDesc?: string;
  handlingTemp?: string;
  handlingSensitivity?: string;
  packType?: string;
  comments?: string;
  operation?: string;
  supplier?: number;
  sellable?: string;
  orderable?: string;
  inventory?: string;
  vpn?: string;
  barcode?: string;
  fileCount?: number;
  itemStatus?: string;
  itemTier?: string;
  shortDescArabic?: string;
  itemSpecification?: string;
  itemSpecificationValue?: string;
  itemNature?: string;
  itemNatureValue?: string;
  expiryFlag?: string;
  expiryOutbound?: number;
  expiryInbound?: number;
  som: string;
  uop: string;
  hts: string;
  innerQty?: number;
  caseQty?: number;
  suppCaseCost?: number;
  suppUnitCost?: number;
  netSuppCaseCost?: number;
  netSuppUnitCost?: number;
  invoiceDiscount?: number;
  itemPrices?: NewItemPriceModel[];

  compQty?: number;

 
}
