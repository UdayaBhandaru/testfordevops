import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, Router } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { NewItemSearchModel } from './NewItemSearchModel';
import { ItemListService } from './ItemListService';

@Injectable()
export class ItemListResolver implements Resolve<ServiceDocument<NewItemSearchModel>> {
  constructor(private service: ItemListService, private router: Router) { }
    resolve(route: ActivatedRouteSnapshot): Observable<ServiceDocument<NewItemSearchModel>> {
        return this.service.list();
    }
}
