export class NewItemSearchModel {
  heaD_SEQ?: number;
  description: string;
  supplierId?: number;
  supplierName?: string;
  divisionId?: number;
  divisionDesc?: string;
  deptId?: number;
  deptDesc?: string;
  categoryId?: number;
  categoryDesc?: string;
  classId?: number;
  classDesc?: string;
  subclassId?: number;
  subclassDesc?: string;
  item: string;
  barcode: string;
  status: string;
  startRow?: number;
  endRow?: number;
  totalRows?: number;
  sortModel?: {
    colId: string;
    sort: string;
  }[];
}

