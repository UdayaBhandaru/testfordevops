import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument, CommonService } from "@agility/frameworkcore";
import { HttpHelperService } from "../Common/HttpHelperService";
import { NewItemSearchModel } from "./NewItemSearchModel";

@Injectable()
export class ItemListService {
  serviceDocument: ServiceDocument<NewItemSearchModel> = new ServiceDocument<NewItemSearchModel>();
  itemMasterPaging: {
    startRow: number,
    pageSize: number,
    cacheSize: number,
  } = {
      startRow: 0,
      pageSize: 10,
      cacheSize: 100,
    };

  constructor(private httpHelperService: HttpHelperService) { }

  newModel(model: NewItemSearchModel): ServiceDocument<NewItemSearchModel> {
    return this.serviceDocument.newModel(model);
  }

  search(additionalParams: any): Observable<ServiceDocument<NewItemSearchModel>> {
    return this.serviceDocument.search("/api/Item/Search", true, () => this.setSubClassId(additionalParams));
  }

  list(): Observable<ServiceDocument<NewItemSearchModel>> {
    return this.serviceDocument.list("/api/Item/List");
  }

  setSubClassId(additionalParams: any): void {
    if (additionalParams) {
      this.serviceDocument.dataProfile.dataModel.startRow = additionalParams.startRow ? additionalParams.startRow : this.itemMasterPaging.startRow;
      this.serviceDocument.dataProfile.dataModel.endRow = additionalParams.endRow ? additionalParams.endRow : this.itemMasterPaging.cacheSize;
      this.serviceDocument.dataProfile.dataModel.sortModel = additionalParams.sortModel;
    } else {
      this.serviceDocument.dataProfile.dataModel.startRow = this.itemMasterPaging.startRow;
      this.serviceDocument.dataProfile.dataModel.endRow = this.itemMasterPaging.cacheSize;
    }
  }

  getDataFromAPI(apiUrl: string): Observable<any> {
    var resp: any = this.httpHelperService.get(apiUrl);
    return resp;
  }
}
