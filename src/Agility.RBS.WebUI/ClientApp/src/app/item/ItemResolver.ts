import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, Router } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { ItemService } from "./ItemService";
import { NewItemModel } from './NewItemModel';

@Injectable()
export class ItemResolver implements Resolve<ServiceDocument<NewItemModel>> {
  constructor(private service: ItemService) { }
  resolve(route: ActivatedRouteSnapshot): Observable<ServiceDocument<NewItemModel>> {
    return this.service.addEdit(route.params["id"]);
  }
}
