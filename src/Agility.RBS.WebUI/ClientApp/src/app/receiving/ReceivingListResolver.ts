import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { ReceivingHeaderModel } from './ReceivingHeaderModel';
import { ReceivingService } from './ReceivingService';

@Injectable()
export class ReceivingListResolver implements Resolve<ServiceDocument<ReceivingHeaderModel>> {
  constructor(private service:  ReceivingService) { }
  resolve(): Observable<ServiceDocument< ReceivingHeaderModel>> {
        return this.service.list();
    }
}
