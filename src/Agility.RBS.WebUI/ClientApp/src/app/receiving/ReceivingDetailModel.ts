export class ReceivingDetailModel {
  orderNo: string;
  supplier: string;
  supName: string;
  location: string;
  locName: string;
  locTypeDesc: string;
  locType: string;
  item: string;
  itemDesc: string;
  itemUom: string;
  orderedQuantity: number;
  unitCost: number;
  prescaledQty: number;
  receiveDate: Date;
  expiryDate: Date;
  isExpiry: boolean;
  receivedQuantity: string;
  currencyCode: string;
  weight: string;
  weightUom: string;
  userId: string;
  divisionId: string;
  operation: string;
  expectedDate?: Date;
}
