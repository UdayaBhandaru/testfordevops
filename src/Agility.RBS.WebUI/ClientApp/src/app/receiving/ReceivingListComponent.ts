import { Component, OnInit, ViewChild, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType, AjaxModel, AjaxResult } from "@agility/frameworkcore";
import { FormGroup, FormControl } from "@angular/forms";
import { GridOptions, ColDef, GridReadyEvent, GridApi } from 'ag-grid-community';
import { RbAutoCompleteComponent } from '../Common/controls/RbAutoCompleteComponent';
import { ReceivingHeaderModel } from './ReceivingHeaderModel';
import { SupplierDomainModel } from '../Common/DomainData/SupplierDomainModel';
import { SharedService } from '../Common/SharedService';
import { GridEditComponent } from '../Common/grid/GridEditComponent';
import { InfoComponent } from '../Common/InfoComponent';
import { GridExportModel } from '../Common/grid/GridExportModel';
import { ReceivingService } from './ReceivingService';
import { ReceivingDetailModel } from './ReceivingDetailModel';
import { GridDateComponent } from '../Common/grid/GridDateComponent';
import { LoaderService } from '../Common/LoaderService';
import { formatDate } from '@angular/common';
import { error } from '@angular/compiler/src/util';
import { NumericEditorComponent } from '../Common/grid/NumericEditorComponent';

@Component({
  selector: "receiving-List",
  templateUrl: "./ReceivingListComponent.html"
})
export class ReceivingListComponent implements OnInit {
  public serviceDocument: ServiceDocument<ReceivingHeaderModel>;
  dataList: ReceivingDetailModel[] = [];
  PageTitle = "Receiving";
  componentName: any;
  columns: ColDef[];
  headDisplayModel: ReceivingDetailModel = new ReceivingDetailModel();
  model: ReceivingHeaderModel = {
    orderNo: null
    , operation: null
    , supplier: null
    , supName: null
    , location: null
    , locName: null
    , divisionId: null
    , locTypeDesc: null
    , locType: null
    , createdBy: null
    , receiveDate: null
    , expectedDate: null
  };
  gridOptions: GridOptions;
  gridApi: GridApi;
  constructor(public service: ReceivingService, private sharedService: SharedService, private router: Router, private loaderService: LoaderService,
   private ref: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { colId: "item", headerName: "ItemNo", field: "item", tooltipField: "item" },
      { colId: "itemDesc", headerName: "Item", field: "itemDesc", tooltipField: "itemDesc" },
      { colId: "orderedQuantity", headerName: "Expected", field: "orderedQuantity", tooltipField: "orderedQuantity" },
      {
        colId: "receivedQuantity", headerName: "Received", field: "receivedQuantity", tooltipField: "receivedQuantity",
        editable: true, cellEditorFramework: NumericEditorComponent,singleClickEdit: true
      },
      { colId: "expiryDate", headerName: "Expiry Date", field: "expiryDate", cellRendererFramework: GridDateComponent },
    ];
    this.gridOptions = {
      onGridReady: (params: GridReadyEvent): void => {
        this.gridApi = params.api;
        params.api.startEditingCell({ rowIndex: 0, colKey: "receivedQuantity" });

      }
    }
    this.service.newModel(this.model);
    this.serviceDocument = this.service.serviceDocument;
  }

  ngOnAfterViewInit(): void {
    this.ref.detectChanges();
  }

  update(cell, event) {
    if (event.target.value) {
      this.dataList[cell.rowIndex][cell.column.colId] = event.target.value;
    }
  }

  getDetails(): void {
    this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.loaderService.display(true);
      this.service.GetReceivedOrders(this.serviceDocument.dataProfile.profileForm.controls["orderNo"].value).subscribe(
        (rd: AjaxModel<ReceivingDetailModel[]>) => {
          if (rd.result === AjaxResult.success) {
            this.dataList = rd.model;
            this.loaderService.display(false);
            if (this.dataList && this.dataList.length > 0) {
              this.headDisplayModel = this.dataList[0];
              this.serviceDocument.dataProfile.profileForm.controls["supName"].setValue(this.headDisplayModel.supName);
              this.serviceDocument.dataProfile.profileForm.controls["locName"].setValue(this.headDisplayModel.locName);
              this.serviceDocument.dataProfile.profileForm.controls["expectedDate"].setValue(formatDate(this.headDisplayModel.expectedDate, "dd/M/yyyy", "en-US"));
            }
          } else {
            this.loaderService.display(false);
            this.sharedService.errorForm(rd.message);
          }
        },
        (error: string) => {
          this.loaderService.display(false);
          this.sharedService.errorForm(error);
        },
        () => {
          this.loaderService.display(false);
        }
      );
    }
  }

  saveReceiving() {
    if (this.dataList.length > 0) {
      this.gridApi.stopEditing();
      this.loaderService.display(true);
      this.serviceDocument.dataProfile.profileForm.controls["receivingDetails"] = new FormControl();
      this.serviceDocument.dataProfile.profileForm.controls["receivingDetails"].setValue(this.dataList);
      this.service.save().subscribe(
        e => {
          this.loaderService.display(false);
          if (e.result.type === MessageType.success) {
            this.sharedService.saveForm("Items received successfully.Shipment No: " + e.dataProfile.dataModel.shipmentNo).subscribe(() => {
              this.router.navigate(["/Blank"], {
                skipLocationChange: true, queryParams: {
                  id: "/Receiving/List/"
                }
              });

            });
          }
          else {
            this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
          }
          this.loaderService.display(false);
        },
        error => {
          console.log(error);
          this.loaderService.display(false);
        });
    }
    else {
      this.sharedService.errorForm("Please save item details first");
    }
  }
}
