import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ReceivingListComponent } from './ReceivingListComponent';
import { ReceivingListResolver } from './ReceivingListResolver';

const Receiving: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
              component: ReceivingListComponent,
                resolve:
                {
                  references: ReceivingListResolver
                }
            }
            
        ]
    }
];

@NgModule({
    imports: [
      RouterModule.forChild(Receiving)
    ]
})
export class ReceivingRoutingModule { }
