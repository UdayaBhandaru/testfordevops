import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument, WorkflowAction, AjaxModel } from "@agility/frameworkcore";
import { HttpHelperService } from "../Common/HttpHelperService";
import { HttpParams } from "@angular/common/http";
import { SharedService } from "../Common/SharedService";
import { ReceivingHeaderModel } from './ReceivingHeaderModel';
import { ReceivingDetailModel } from './ReceivingDetailModel';

@Injectable()
export class ReceivingService {
  serviceDocument: ServiceDocument<ReceivingHeaderModel> = new ServiceDocument<ReceivingHeaderModel>();
  searchData: any;
  constructor(private httpHelperService: HttpHelperService, private sharedService: SharedService) { }

  newModel(model: ReceivingHeaderModel): ServiceDocument<ReceivingHeaderModel> {
    return this.serviceDocument.newModel(model);
  }

  list(): Observable<ServiceDocument<ReceivingHeaderModel>> {
    return this.serviceDocument.list("/api/receiving/List");
  }

  search(): Observable<ServiceDocument<ReceivingHeaderModel>> {
    return this.serviceDocument.search("/api/receiving/Search", true);
  }

  GetReceivedOrders(orderNo: string): Observable<AjaxModel<ReceivingDetailModel[]>> {
    return this.httpHelperService.get("api/receiving/GetOrdersReceived", new HttpParams().set("orderNo", orderNo.toString()));
  }

  save(): Observable<ServiceDocument<ReceivingHeaderModel>> {
    return this.serviceDocument.save("/api/receiving/Save", true);
  }
}
