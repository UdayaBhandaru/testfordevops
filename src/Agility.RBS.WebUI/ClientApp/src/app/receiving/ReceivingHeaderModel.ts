import { ReceivingDetailModel } from './ReceivingDetailModel';

export class ReceivingHeaderModel {
  orderNo?: string;
  operation?: string;
  supplier?: number;
  supName?: number;
  location?: number;
  locName?: string;
  divisionId?: number;
  locTypeDesc?: string;
  locType : string;
  createdBy?: string;
  receiveDate: Date;
  expectedDate: Date;
  receivingDetails?: ReceivingDetailModel[];
  shipmentNo?: string;
}
