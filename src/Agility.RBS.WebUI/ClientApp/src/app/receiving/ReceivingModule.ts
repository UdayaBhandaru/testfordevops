import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../AgGridSharedModule";
import { TitleSharedModule } from "../TitleSharedModule";
import { RbsSharedModule } from "../RbsSharedModule";
import { ReceivingRoutingModule } from './ReceivingRoutingModule';
import { ReceivingListComponent } from './ReceivingListComponent';
import { ReceivingService } from './ReceivingService';
import { ReceivingListResolver } from './ReceivingListResolver';

@NgModule({
  imports: [
    FrameworkCoreModule,
    ReceivingRoutingModule,
    AgGridModule,
    AgGridSharedModule,
    TitleSharedModule,
    RbsSharedModule
  ],
  declarations: [
    ReceivingListComponent
  ],
  providers: [
    ReceivingService,
    ReceivingListResolver
  ]
})
export class ReceivingModule {
}
