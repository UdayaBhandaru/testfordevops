import { Component, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { SharedService } from "../Common/SharedService";
import { GridEditComponent } from "../Common/grid/GridEditComponent";
import { GridExportModel } from "../Common/grid/GridExportModel";
import { InfoComponent } from "../Common/InfoComponent";
import { BulkItemService } from "./BulkItemService";
import { BulkItemHeaderModel } from "./BulkItemHeaderModel";
import { SupplierDomainModel } from "../Common/DomainData/SupplierDomainModel";
import { DomainDetailModel } from "../domain/DomainDetailModel";
import { UserDomainModel } from "../Common/DomainData/UserDomainModel";
import { DivisionDomainModel } from "../Common/DomainData/DivisionDomainModel";
import { WorkflowStatusDomainModel } from "../Common/DomainData/WorkflowStatusDomainModel";
import { SearchComponent } from "../Common/SearchComponent";
import { GridOptions } from 'ag-grid-community';
import { RbAutoCompleteComponent } from '../Common/controls/RbAutoCompleteComponent';

@Component({
  selector: "bulkitem-List",
  templateUrl: "./BulkItemListComponent.html"
})
export class BulkItemListComponent implements OnInit {
  public serviceDocument: ServiceDocument<BulkItemHeaderModel>;
  PageTitle = "Bulk Item";
  redirectUrl = "BulkItem/New/";
  componentName: any;
  columns: any[];
  apiUrl: string;
  @ViewChild(RbAutoCompleteComponent) autoViewRef: RbAutoCompleteComponent;
  model: BulkItemHeaderModel = {
    description: null, supplierId: null, supplierName: null, divisionId: null, divisionDesc: null, heaD_SEQ: null
    , startDate: null, endDate: null, dateType: null, createdBy: null, workflowInstance: null, status: null
    , totalRebates: null, originCountryId: null, originCountryName: null, currencyCode: null, incotermsCode: null
    , incotermsDesc: null, supplierType: null
  };
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  supplierData: SupplierDomainModel[];
  divisionData: DivisionDomainModel[];
  workflowStatusList: WorkflowStatusDomainModel[];
  @ViewChild("searchPanel") searchPanel: SearchComponent;
  gridOptions: GridOptions;

  constructor(public service: BulkItemService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "ID", field: "heaD_SEQ", tooltipField: "heaD_SEQ" },
      { headerName: "Supplier", field: "supplierName", tooltipField: "supplierName" },
      { headerName: "Division", field: "divisionDesc", tooltipField: "divisionDesc" },
      { headerName: "Supplier Type", field: "supplierType", tooltipField: "supplierType" },
      { headerName: "Status", field: "status", tooltipField: "status" },
      {
        headerName: "ACTIONS", field: "Actions", cellRendererFramework: GridEditComponent,
      }
    ];

    this.apiUrl = "/api/Supplier/SuppliersGetName?name=";
    this.divisionData = Object.assign([], this.service.serviceDocument.domainData["division"]);
    this.workflowStatusList = Object.assign([], this.service.serviceDocument.domainData["workflowStatusList"]);
    this.sharedService.domainData = {
      supplier: this.supplierData,
      division: this.divisionData
    };
    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.autoViewRef.ngOnInit();
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl + cell.data.heaD_SEQ, this.service.serviceDocument.dataProfile.dataList);
  }

  addValidation(): void {
    this.router.navigate(["/" + this.redirectUrl + "0"], { skipLocationChange: true });
    this.sharedService.previousUrl = "../BulkItem/List";
  }

  changeSupplier(event: any): void {
    this.sharedService.domainData.supplier = [event.options];
  }

  reset(): void {
    this.autoViewRef.ngOnInit();
  }
}
