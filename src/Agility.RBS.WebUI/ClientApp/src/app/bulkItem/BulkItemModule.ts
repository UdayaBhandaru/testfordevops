﻿import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../AgGridSharedModule";
import { TitleSharedModule } from "../TitleSharedModule";
import { BulkItemService } from "./BulkItemService";
import { BulkItemResolver } from "./BulkItemResolver";
import { BulkItemRoutingModule } from "./BulkItemRoutingModule";
import { BulkItemListComponent } from "./BulkItemListComponent";
import { BulkItemComponent } from "./BulkItemComponent";
import { BulkItemListResolver } from "./BulkItemListResolver";
import { RbsSharedModule } from "../RbsSharedModule";

@NgModule({
    imports: [
        FrameworkCoreModule,
        BulkItemRoutingModule,
        AgGridModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        BulkItemListComponent,
        BulkItemComponent
    ],
    providers: [
        BulkItemService,
        BulkItemResolver,
        BulkItemListResolver
    ]
})
export class BulkItemModule {
}
