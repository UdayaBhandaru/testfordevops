﻿import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { BulkItemService } from "./BulkItemService";
import { BulkItemHeaderModel } from "./BulkItemHeaderModel";

@Injectable()
export class BulkItemListResolver implements Resolve<ServiceDocument<BulkItemHeaderModel>> {
    constructor(private service: BulkItemService) { }
    resolve(): Observable<ServiceDocument<BulkItemHeaderModel>> {
        return this.service.list();
    }
}