﻿import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { BulkItemResolver } from "./BulkItemResolver";
import { BulkItemListComponent } from "./BulkItemListComponent";
import { BulkItemComponent } from "./BulkItemComponent";
import { BulkItemListResolver } from "./BulkItemListResolver";

const BulkItemRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: BulkItemListComponent,
                resolve:
                {
                        references: BulkItemListResolver
                }
            },
            {
                path: "New/:id",
                component: BulkItemComponent,
                resolve:
                    {
                        references: BulkItemResolver
                    }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(BulkItemRoutes)
    ]
})
export class BulkItemRoutingModule { }
