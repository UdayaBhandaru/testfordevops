import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument, WorkflowAction } from "@agility/frameworkcore";
import { HttpHelperService } from "../Common/HttpHelperService";
import { BulkItemHeaderModel } from "./BulkItemHeaderModel";
import { HttpParams } from "@angular/common/http";
import { SharedService } from "../Common/SharedService";

@Injectable()
export class BulkItemService {
    serviceDocument: ServiceDocument<BulkItemHeaderModel> = new ServiceDocument<BulkItemHeaderModel>();
    searchData: any;
    constructor(private httpHelperService: HttpHelperService, private sharedService: SharedService) { }

    newModel(model: BulkItemHeaderModel): ServiceDocument<BulkItemHeaderModel> {
        return this.serviceDocument.newModel(model);
    }

    list(): Observable<ServiceDocument<BulkItemHeaderModel>> {
        return this.serviceDocument.list("/api/BulkItem/List");
    }

    search(): Observable<ServiceDocument<BulkItemHeaderModel>> {
        return this.serviceDocument.search("/api/BulkItem/Search", true, () => this.setBulkItemDates());
    }

    save(): Observable<ServiceDocument<BulkItemHeaderModel>> {
        let currentAction: WorkflowAction = this.serviceDocument.dataProfile.actionService.allowedActions[0];
        if (!this.serviceDocument.dataProfile.dataModel.heaD_SEQ && currentAction) {
            this.serviceDocument.dataProfile.profileForm.controls["currentActionId"].setValue(currentAction.transitionClaim);
            return this.serviceDocument.submit("/api/BulkItem/Submit", true);
        } else {
            return this.serviceDocument.save("/api/BulkItem/Save", true);
        }
    }

  submit(operation: string): Observable<ServiceDocument<BulkItemHeaderModel>> {
    let url: string = "/api/BulkItem/Submit";
    switch (operation) {
      case this.sharedService.submitOperations.submit:
        url = "/api/BulkItem/Submit"
        break;
      case this.sharedService.submitOperations.reject:
        url = "/api/BulkItem/Reject"
        break;
      case this.sharedService.submitOperations.sendback:
        url = "/api/BulkItem/SendBackForReview"
        break;
      default:
        url = "/api/BulkItem/Submit";
    }
    return this.serviceDocument.submit(url, true);
    }

    addEdit(id: number): Observable<ServiceDocument<BulkItemHeaderModel>> {
        if (+id === 0) {
            return this.serviceDocument.new("/api/BulkItem/New");
        } else {
            return this.serviceDocument.open("/api/BulkItem/Open", new HttpParams().set("id", id.toString()));
        }
    }

    setBulkItemDates(): void {
        var sd: BulkItemHeaderModel = this.serviceDocument.dataProfile.dataModel;
        if (sd !== null) {
            if (sd.startDate != null) {
                sd.startDate = this.sharedService.setOffSet(sd.startDate);
            }
            if (sd.endDate != null) {
                sd.endDate = this.sharedService.setOffSet(sd.endDate);
            }
        }
    }
}
