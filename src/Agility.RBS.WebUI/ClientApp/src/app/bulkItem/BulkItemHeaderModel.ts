﻿import { BulkItemDetailModel } from "./BulkItemDetailModel";

export class BulkItemHeaderModel {
    description?: string;
    operation?: string;
    supplierId?: number;
    heaD_SEQ?: number;
    tableName?: string;
    startDate?: Date;
    endDate?: Date;
    supplierName?: string;
    divisionId?: number;
    divisionDesc?: string;
    dateType: string;
    createdBy?: string;
    workflowInstance: string;
    status?: string;
    totalRebates?: number;
    originCountryId?: string;
    originCountryName?: string;
    currencyCode?: string;
    incotermsCode?: string;
    incotermsDesc?: string;
    supplierType?: string;
    fileCount?: number;
    bulkItemDetails?: BulkItemDetailModel[]
}