﻿import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { BulkItemHeaderModel } from "./BulkItemHeaderModel";
import { BulkItemService } from "./BulkItemService";

@Injectable()
export class BulkItemResolver implements Resolve<ServiceDocument<BulkItemHeaderModel>> {
    constructor(private service: BulkItemService) { }
    resolve(route: ActivatedRouteSnapshot): Observable<ServiceDocument<BulkItemHeaderModel>> {
        return this.service.addEdit(route.params["id"]);
    }
}