import { Component, OnInit, TemplateRef, ViewChild, OnDestroy, ElementRef, ChangeDetectorRef } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ServiceDocument, CommonService, MessageResult, MessageType } from "@agility/frameworkcore";
import { MatDialogRef } from "@angular/material";
import { GridOptions, GridApi } from "ag-grid-community";
import { Subscription } from "rxjs";
import { BulkItemHeaderModel } from "./BulkItemHeaderModel";
import { BulkItemService } from "./BulkItemService";
import { SharedService } from "../Common/SharedService";
import { LoaderService } from "../Common/LoaderService";
import { SupplierDomainModel } from "../Common/DomainData/SupplierDomainModel";
import { DivisionDomainModel } from "../Common/DomainData/DivisionDomainModel";
import { DocumentsConstants } from "../Common/DocumentsConstants";
import { BulkItemDetailModel } from "./BulkItemDetailModel";
import { HttpParams, HttpHeaders, HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";

@Component({
  selector: "bulkitem",
  templateUrl: "./BulkItemComponent.html"
})

export class BulkItemComponent implements OnInit {
  _componentName = "Bulk Item";
  PageTitle: string = "Bulk Item";
  public serviceDocument: ServiceDocument<BulkItemHeaderModel>;
  localizationData: any;
  model: BulkItemHeaderModel;
  editMode: boolean;
  primaryId: number; // used in CanDeactivate method, should be same name if we use candeactivate
  supplierData: SupplierDomainModel[];
  divisionData: DivisionDomainModel[];
  routeServiceSubscription: Subscription;
  documentsConstants: DocumentsConstants = new DocumentsConstants();
  screenName: string;
  supplierType: string;
  showExcelLink: boolean;
  bulkStatus: string;
  apiUrl: string;
  componentName: any;
  columns: any[];
  public sheetName: string = "BulkItem Detail";
  data: BulkItemDetailModel[] = [];
  public gridOptions: GridOptions;
  gridApi: GridApi;
  rejectSubscription: any;

  constructor(private route: ActivatedRoute, public router: Router, private changeRef: ChangeDetectorRef, private httpClient: HttpClient
    , public sharedService: SharedService, private service: BulkItemService, private loaderService: LoaderService) {
  }

  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }

  ngOnInit(): void {
    this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
    this.supplierData = this.service.serviceDocument.domainData["supplier"];
    this.divisionData = Object.assign([], this.service.serviceDocument.domainData["division"]);
    this.screenName = this.documentsConstants.bulkitemObjectName;
    this.apiUrl = "/api/Supplier/SuppliersGetName?name=";
    this.componentName = this;
    this.columns = [
      { headerName: "Item", field: "item", tooltipField: "item" },
      { headerName: "Item Desc", field: "itemDesc", tooltipField: "itemDesc" },
      { headerName: "Department", field: "groupDesc", tooltipField: "groupDesc" },
      { headerName: "Category", field: "deptDesc", tooltipField: "deptDesc" },
      { headerName: "Fineline", field: "classDesc", tooltipField: "classDesc" },
      { headerName: "Segment", field: "subclassDesc", tooltipField: "subclassDesc" }
    ];

    if (this.service.serviceDocument.dataProfile.dataModel) {
      this.bind();
    }
  }

  bind(): void {
    this.routeServiceSubscription = this.route.params.subscribe((resp: any) => {
      this.primaryId = resp["id"];
    });
    if (this.primaryId.toString() !== "0") {
      this.editMode = true;
      this.showExcelLink = true;
      this.PageTitle = `${this.PageTitle} - EDIT - ${this.primaryId}`;
    } else {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign(this.service.serviceDocument.dataProfile.dataModel
        , { operation: "I", status: "Worksheet" }
        , { workflowInstance: this.service.serviceDocument.dataProfile.dataModel.workflowInstance });
      this.service.serviceDocument.dataProfile.profileForm.patchValue(this.service.serviceDocument.dataProfile.dataModel);
      this.PageTitle = `${this.PageTitle} - ADD`;
    }
    if (this.editMode) {
      this.bulkStatus = this.service.serviceDocument.dataProfile.dataModel.status;
      if (this.service.serviceDocument.dataProfile.dataModel.bulkItemDetails) {
        Object.assign(this.data, this.service.serviceDocument.dataProfile.dataModel.bulkItemDetails);
      }
    }
    this.serviceDocument = this.service.serviceDocument;
    this.sharedService.fileCount = this.service.serviceDocument.dataProfile.dataModel.fileCount;
    this.sharedService.checkWfPermitions(this.serviceDocument);
  }

  save(saveOnly: boolean): void {
    if (this.bulkStatus !== "RequestCompleted") {
      if (this.serviceDocument.dataProfile.profileForm.valid) {
        this.saveSubscription(saveOnly);
      } else {
        this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
      }
    }
  }

  saveAndContinue(): void {
    this.save(false);
  }

  saveSubscription(saveOnly: boolean): void {
    this.loaderService.display(true);
    this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.primaryId = this.service.serviceDocument.dataProfile.dataModel.heaD_SEQ;
        this.sharedService.saveForm(`${this.localizationData.bulkItem.bulkitemsave} - Request ID ${this.primaryId}`).subscribe(() => {
          if (saveOnly) {
            this.router.navigate(["/BulkItem/List"], { skipLocationChange: true });
          } else {
            this.router.navigate(["/Blank"], {
              skipLocationChange: true, queryParams: {
                id: "/BulkItem/New/" + this.primaryId
              }
            });
            this.loaderService.display(false);
            this.openLocalItemExcel();


          }
        }, (error: string) => { console.log(error); });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
    }, (error: string) => { console.log(error); });
  }

  reset(): void {
    Object.keys(this.serviceDocument.dataProfile.profileForm.controls).forEach(control => {
      this.serviceDocument.dataProfile.profileForm.controls[control].setValue(this.serviceDocument.dataProfile.dataModel[control]);
    });
    this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
  }

  openLocalItemExcel(): void {
    let id: number = this.service.serviceDocument.dataProfile.dataModel.heaD_SEQ;
    const type: any = "application/vnd.ms-excel";
    this.apiUrl = "/api/BulkItem/OpenLocalItemExcel";
    let headers1: HttpHeaders = new HttpHeaders();
    headers1.set("Accept", type);
    this.httpClient.get(this.apiUrl,
      {
        headers: headers1, responseType: "blob" as "json"
        , params: new HttpParams().set("id", id.toString())
      })
      .pipe(map((response: any) => {
        if (response instanceof Response) {
          return response.blob();
        }
        return response;
      }))
      .subscribe(res => {
        console.log("start download:", res);
        let url: string = window.URL.createObjectURL(res);
        let a: HTMLAnchorElement = document.createElement("a");
        document.body.appendChild(a);
        a.setAttribute("style", "display: none");
        a.href = url;
        a.download = "BulkItem-" + id.toString() + ".xlsm";
        a.click();
        window.URL.revokeObjectURL(url);
        a.remove();
      }, (error: string) => { console.log(error); });
  }

  ngOnDestroy(): void {
    this.routeServiceSubscription.unsubscribe();
  }

  changeSupplier($event: any): void {
    this.serviceDocument.dataProfile.profileForm.patchValue({
      originCountryId: $event.options.originCountryId,
      originCountryName: $event.options.originCountryName,
      currencyCode: $event.options.currencyCode,
      incotermsCode: $event.options.incotermsCode,
      incotermsDesc: $event.options.incotermsDesc,
      supplierType: $event.options.supplierType,
      totalRebates: $event.options.rebateDiscount
    });
  }

  submitAction(): void {
    this.submitOperations(this.sharedService.submitOperations.submit, this.localizationData.bulkItem.bulkitemsave);
  }

  rejectAction() {
    this.submitOperations(this.sharedService.submitOperations.reject, "Successfully rejected ");
  }

  submitOperations(operation: string, message: string): void {
    this.loaderService.display(true);
    this.service.submit(operation).subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.primaryId = this.service.serviceDocument.dataProfile.dataModel.heaD_SEQ;
        this.sharedService.saveForm(`${message} - Request ID ${this.primaryId}`).subscribe(() => {
          this.router.navigate(["/BulkItem/List"], { skipLocationChange: true });
        }, (error: string) => {
          console.log(error);
          this.loaderService.display(false);
        });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
        this.loaderService.display(false);
      }
    }, (error: string) => {
      console.log(error);
      this.loaderService.display(false);
    });
  }
}
