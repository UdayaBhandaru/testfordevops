export class BulkItemDetailModel {
    divisionDesc: string;
    deptDesc: string;
    groupDesc: string;
    classDesc: string;
    subclassDesc: string;
    item: string;
    itemDesc: string;
}
