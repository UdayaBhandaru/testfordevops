export class BuyerModel { 
  buyer?: number;
  buyerName?: string;
  buyerPhone?: string;
  buyerFax?: string;
  operation?: string;
}
