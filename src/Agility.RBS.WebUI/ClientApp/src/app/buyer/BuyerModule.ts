import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { BuyerListComponent } from "./BuyerListComponent";
import { BuyerService } from "./BuyerService";
import { BuyerRoutingModule } from "./BuyerRoutingModule";
import { BuyerComponent } from "./BuyerComponent";
import { BuyerListResolver, BuyerResolver } from "./BuyerResolver";
import { RbsSharedModule } from '../RbsSharedModule';
import { AgGridSharedModule } from '../AgGridSharedModule';
import { TitleSharedModule } from '../TitleSharedModule';

@NgModule({
    imports: [
        FrameworkCoreModule,
        BuyerRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        BuyerListComponent,
        BuyerComponent,
    ],
    providers: [
        BuyerService,
        BuyerListResolver,
        BuyerResolver,
    ]
})
export class BuyerModule {
    constructor() {
        LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
    }
}
