import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { BuyerModel } from "./BuyerModel";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from '../Common/HttpHelperService';

@Injectable()
export class BuyerService {
    serviceDocument: ServiceDocument<BuyerModel> = new ServiceDocument<BuyerModel>();
    constructor(private httpHelperService: HttpHelperService) { }

    newModel(model: BuyerModel): ServiceDocument<BuyerModel> {
        return this.serviceDocument.newModel(model);
    }

    search(): Observable<ServiceDocument<BuyerModel>> {
      return this.serviceDocument.search("/api/Buyer/Search");
    }

    save(): Observable<ServiceDocument<BuyerModel>> {
      return this.serviceDocument.save("/api/Buyer/Save", true);
    }

    list(): Observable<ServiceDocument<BuyerModel>> {
      return this.serviceDocument.list("/api/Buyer/List");
    }

    addEdit(): Observable<ServiceDocument<BuyerModel>> {
      return this.serviceDocument.list("/api/Buyer/New");
    }
  isExistingBuyer(buyer: number): Observable<boolean> {
    return this.httpHelperService.get("/api/Buyer/IsExistingBuyer", new HttpParams().set("buyer", buyer.toString()));
    }
}
