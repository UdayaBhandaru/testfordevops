import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { BuyerService } from "./BuyerService";
import { BuyerModel } from "./BuyerModel";
@Injectable()
export class BuyerListResolver implements Resolve<ServiceDocument<BuyerModel>> {
    constructor(private service: BuyerService) { }
    resolve(): Observable<ServiceDocument<BuyerModel>> {
        return this.service.list();
    }
}

@Injectable()
export class BuyerResolver implements Resolve<ServiceDocument<BuyerModel>> {
    constructor(private service: BuyerService) { }
    resolve(): Observable<ServiceDocument<BuyerModel>> {
        return this.service.addEdit();
    }
}



