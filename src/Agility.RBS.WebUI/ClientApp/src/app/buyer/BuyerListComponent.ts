import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { GridOptions } from "ag-grid-community";
import { BuyerModel } from './BuyerModel';
import { GridExportModel } from '../Common/grid/GridExportModel';
import { BuyerService } from './BuyerService';
import { SharedService } from '../Common/SharedService';
import { GridEditComponent } from '../Common/grid/GridEditComponent';
import { InfoComponent } from '../Common/InfoComponent';
@Component({
  selector: "Buyer-list",
  templateUrl: "./BuyerListComponent.html"
})
export class BuyerListComponent implements OnInit {
  PageTitle = "Buyer";
  redirectUrl = "Buyer";
  componentName: any;
  serviceDocument: ServiceDocument<BuyerModel>;
  public gridOptions: GridOptions;
  columns: any[];
  model: BuyerModel;
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;

  constructor(public service: BuyerService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "Buyer", field: "buyer", tooltipField: "buyer" },
      { headerName: "Buyer Name", field: "buyerName", tooltipField: "formatNabuyerNameme" },
      { headerName: "Buyer Phone", field: "buyerPhone", tooltipField: "buyerPhone" },
      { headerName: "Buyer Fax", field: "buyerFax", tooltipField: "buyerFax" },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent
      }
    ];
    
    this.model = { buyer: null, buyerName: null };

    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }
}
