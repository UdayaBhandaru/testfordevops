import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { BuyerListComponent } from "./BuyerListComponent";
import { BuyerListResolver, BuyerResolver} from "./BuyerResolver";
import { BuyerComponent } from "./BuyerComponent";
const UtilityRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
              component: BuyerListComponent,
                resolve:
                {
                    serviceDocument: BuyerListResolver
                }
            },
            {
                path: "New",
                component: BuyerComponent,
                resolve:
                {
                    serviceDocument: BuyerResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(UtilityRoutes)
    ]
})
export class BuyerRoutingModule { }
