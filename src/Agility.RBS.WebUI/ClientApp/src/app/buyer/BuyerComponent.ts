import { Component, OnInit, ChangeDetectorRef  } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { BuyerModel } from './BuyerModel';
import { BuyerService } from './BuyerService';
import { SharedService } from '../Common/SharedService';

@Component({
  selector: "Buyer",
  templateUrl: "./BuyerComponent.html"
})

export class BuyerComponent implements OnInit {
  PageTitle = "Buyer";
  redirectUrl = "Buyer";
    serviceDocument: ServiceDocument<BuyerModel>;
    editMode: boolean = false;
    editModel: any;
    localizationData: any;  
    model: BuyerModel;  


    constructor(private service: BuyerService, private sharedService: SharedService, private router: Router
        , private ref: ChangeDetectorRef) {
    }

    ngOnInit(): void {
        this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
        this.bind();
  }
  ngAfterViewChecked(): void {
    this.ref.detectChanges();
  }

    bind(): void {
        delete this.editModel;
        if (this.sharedService.editObject) {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);         
            Object.assign(this.service.serviceDocument.dataProfile.dataModel, { operation: "U" });
            this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            delete this.sharedService.editObject;
            this.editMode = true;

        } else {            
          this.model = { buyer: null, buyerName: null, buyerFax: null, buyerPhone: null,operation: "I"};
           
            this.service.serviceDocument.dataProfile.dataModel = this.model;
            this.service.newModel(this.model);
        }
        this.serviceDocument = this.service.serviceDocument;
    }

    save(saveOnly: boolean): void {
        if (this.serviceDocument.dataProfile.profileForm.valid) {
            this.saveSubscription(saveOnly);
        } else {
            this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
        }
    }

    saveSubscription(saveOnly: boolean): void {
        this.checkBuyer(saveOnly);
  }

    saveBuyer(saveOnly: boolean): void {
        this.service.save().subscribe(() => {
            if (this.service.serviceDocument.result.type === MessageType.success) {
                this.sharedService.saveForm("Buyer saved successfully.").subscribe(() => {
                    if (saveOnly) {
                        this.router.navigate(["/" + this.redirectUrl + "/List"], { skipLocationChange: true });
                    } else {
                        if (!this.editMode) {
                          this.model = { buyer: null, buyerName: null,buyerFax:null,buyerPhone:null, operation: "I" };                            
                            this.service.newModel(this.model);
                        } else {
                            Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
                            this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.service.serviceDocument.dataProfile.dataModel);
                            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
                        }
                    }
                }, (error: string) => { console.log(error); });
            } else {
                this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
            }
        }, (error: string) => { console.log(error); });
    }

    checkBuyer(saveOnly: boolean): void {
        if (!this.editMode) {
          this.service.isExistingBuyer(this.serviceDocument.dataProfile.profileForm.controls["buyer"].value).subscribe((response: any) => {
                if (response) {
                    this.sharedService.errorForm("Buyer already exisit.");
                } else {
                    this.saveBuyer(saveOnly);
                }
            });
        } else {
            this.saveBuyer(saveOnly);
        }
    }

    reset(): void {
        if (!this.editMode) {
            this.model = {
              buyer: null, buyerName: null, buyerFax: null, buyerPhone: null, operation: "I"
            };
           
            this.service.serviceDocument.dataProfile.dataModel = this.model;
            this.service.newModel(this.model);

        } else {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);            
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
        }
        this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
  }
  //ngAfterContentChecked() {
  //  this.ref.detectChanges();

  //}
}
