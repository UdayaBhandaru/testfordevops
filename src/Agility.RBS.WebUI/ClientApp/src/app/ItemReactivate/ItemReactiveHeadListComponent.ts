import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { SharedService } from "../Common/SharedService";
import { GridEditComponent } from "../Common/grid/GridEditComponent";
import { GridExportModel } from "../Common/grid/GridExportModel";
import { InfoComponent } from "../Common/InfoComponent";
import { ItemReactiveHeadService } from "./ItemReactiveHeadService";
import { ItemReactiveHeadModel } from "./ItemReactiveHeadModel";
import { GridOptions } from 'ag-grid-community';
import { WorkflowStatusDomainModel } from '../Common/DomainData/WorkflowStatusDomainModel';

@Component({
  selector: "ItemReactive-List",
  templateUrl: "./ItemReactiveHeadListComponent.html"
})
export class ItemReactiveHeadListComponent implements OnInit {
  public serviceDocument: ServiceDocument<ItemReactiveHeadModel>;
  PageTitle = "Re-activate Item";
  redirectUrl = "ItemReactive/New/";
  componentName: any;
  columns: any[];
  model: ItemReactiveHeadModel = {
    id: null, operation: null, description: null, supplierType: null
  };
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  gridOptions: GridOptions;
  workflowStatusList: WorkflowStatusDomainModel[];

  constructor(public service: ItemReactiveHeadService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "Id", field: "id", tooltipField: "Id", width: 50 },
      { headerName: "Description", field: "description", tooltipField: "Description", width: 150 },
      {
        headerName: "ACTIONS", field: "Actions", cellRendererFramework: GridEditComponent, width: 40
      }
    ];

    this.workflowStatusList = Object.assign([], this.service.serviceDocument.domainData["workflowStatusList"]);

    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl + cell.data.id, this.service.serviceDocument.dataProfile.dataList);
  }

  addValidation(): void {
    this.router.navigate(["/" + this.redirectUrl + "0"], { skipLocationChange: true });
    this.sharedService.previousUrl = "../ItemReactive/List";
  }
}
