﻿import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ItemReactiveHeadResolver } from "./ItemReactiveHeadResolver";
import { ItemReactiveHeadListComponent } from "./ItemReactiveHeadListComponent";
import { ItemReactiveHeadComponent } from "./ItemReactiveHeadComponent";
import { ItemReactiveHeadListResolver } from "./ItemReactiveHeadListResolver";

const ItemReactiveHeadRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: ItemReactiveHeadListComponent,
                resolve:
                {
                        references: ItemReactiveHeadListResolver
                }
            },
            {
                path: "New/:id",
                component: ItemReactiveHeadComponent,
                resolve:
                    {
                        references: ItemReactiveHeadResolver
                    }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(ItemReactiveHeadRoutes)
    ]
})
export class ItemReactiveHeadRoutingModule { }
