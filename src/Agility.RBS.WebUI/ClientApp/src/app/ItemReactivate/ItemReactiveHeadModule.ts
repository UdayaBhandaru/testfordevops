﻿import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../AgGridSharedModule";
import { TitleSharedModule } from "../TitleSharedModule";
import { ItemReactiveHeadService } from "./ItemReactiveHeadService";
import { ItemReactiveHeadResolver } from "./ItemReactiveHeadResolver";
import { ItemReactiveHeadRoutingModule } from "./ItemReactiveHeadRoutingModule";
import { ItemReactiveHeadListComponent } from "./ItemReactiveHeadListComponent";
import { ItemReactiveHeadComponent } from "./ItemReactiveHeadComponent";
import { ItemReactiveHeadListResolver } from "./ItemReactiveHeadListResolver";
import { RbsSharedModule } from "../RbsSharedModule";

@NgModule({
    imports: [
        FrameworkCoreModule,
        ItemReactiveHeadRoutingModule,
        AgGridModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        ItemReactiveHeadListComponent,
        ItemReactiveHeadComponent
    ],
    providers: [
        ItemReactiveHeadService,
        ItemReactiveHeadResolver,
        ItemReactiveHeadListResolver
    ]
})
export class ItemReactiveHeadModule {
}
