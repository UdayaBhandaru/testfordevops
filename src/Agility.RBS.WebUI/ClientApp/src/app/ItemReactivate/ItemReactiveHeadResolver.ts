﻿import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { ItemReactiveHeadModel } from "./ItemReactiveHeadModel";
import { ItemReactiveHeadService } from "./ItemReactiveHeadService";

@Injectable()
export class ItemReactiveHeadResolver implements Resolve<ServiceDocument<ItemReactiveHeadModel>> {
    constructor(private service: ItemReactiveHeadService) { }
    resolve(route: ActivatedRouteSnapshot): Observable<ServiceDocument<ItemReactiveHeadModel>> {
        return this.service.addEdit(route.params["id"]);
    }
}