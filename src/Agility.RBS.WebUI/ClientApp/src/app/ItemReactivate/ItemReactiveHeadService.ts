﻿import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument, WorkflowAction } from "@agility/frameworkcore";
import { HttpHelperService } from "../Common/HttpHelperService";
import { ItemReactiveHeadModel } from "./ItemReactiveHeadModel";
import { HttpParams, HttpHeaders, HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";

@Injectable()
export class ItemReactiveHeadService {   
    serviceDocument: ServiceDocument<ItemReactiveHeadModel> = new ServiceDocument<ItemReactiveHeadModel>();
    searchData: any;
    supplierType: string;
    constructor(private httpHelperService: HttpHelperService, private httpClient: HttpClient) { }

    newModel(model: ItemReactiveHeadModel): ServiceDocument<ItemReactiveHeadModel> {
        return this.serviceDocument.newModel(model);
    }

    list(): Observable<ServiceDocument<ItemReactiveHeadModel>> {
        return this.serviceDocument.list("/api/ItemReactive/List");
    }

    search(): Observable<ServiceDocument<ItemReactiveHeadModel>> {
        return this.serviceDocument.search("/api/ItemReactive/Search");
    }

    save(): Observable<ServiceDocument<ItemReactiveHeadModel>> {
        if (!this.serviceDocument.dataProfile.dataModel.id) {
            this.serviceDocument.dataProfile.profileForm.controls["currentActionId"].setValue("ItmReactWksheetClaim");
            return this.serviceDocument.submit("/api/ItemReactive/Submit", true);
        } else {
            return this.serviceDocument.save("/api/ItemReactive/Save", true);
        }
    }

    submit(): Observable<ServiceDocument<ItemReactiveHeadModel>> {
        return this.serviceDocument.submit("/api/ItemReactive/Submit", true);
    }

    addEdit(id: number): Observable<ServiceDocument<ItemReactiveHeadModel>> {
        if (+id === 0) {
            return this.serviceDocument.new("/api/ItemReactive/New");
        } else {
            return this.serviceDocument.open("/api/ItemReactive/Open", new HttpParams().set("id", id.toString()));
        }
    }


    downloadExcel(id: number, supplierType: string, supplierId: number): void {
        const type: any = "application/vnd.ms-excel";
        this.supplierType = null;
        if (supplierType == "FOREIGN")
                {
                this.supplierType = "Foreign";
        }
        else{
                this.supplierType = "Local";
        }

        if (id) {            
            let headers1: HttpHeaders = new HttpHeaders();
            headers1.set("Accept", type);
            this.httpClient.get("/api/ItemReactive/OpenItemReactivationExcel",
                { headers: headers1, responseType: "blob" as "json", params: new HttpParams().set("id", id.toString()).append("supplierType", this.supplierType).append("supplierId", supplierId.toString()) })
                .pipe(map((response: any) => {
                    if (response instanceof Response) {
                        return response.blob();
                    }
                    return response;
                }))
                .subscribe(res => {
                    let url: string = window.URL.createObjectURL(res);
                    let a: HTMLAnchorElement = document.createElement("a");
                    document.body.appendChild(a);

                    a.setAttribute("style", "display: none");
                    a.href = url;
                    a.download = "ItemReactivation-" + id + ".xlsm";
                    a.click();
                    window.URL.revokeObjectURL(url);
                    a.remove();
                }, (error: string) => { console.log(error); });
        }
    }
}