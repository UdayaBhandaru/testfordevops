import { Component, OnInit, TemplateRef, ViewChild, OnDestroy, ElementRef, ChangeDetectorRef } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ServiceDocument, CommonService, MessageResult, MessageType } from "@agility/frameworkcore";
import { MatDialogRef } from "@angular/material";
import { GridOptions } from "ag-grid-community";
import { Subscription } from "rxjs";
import { ItemReactiveHeadModel } from "./ItemReactiveHeadModel";
import { ItemReactiveHeadService } from "./ItemReactiveHeadService";
import { SharedService } from "../Common/SharedService";
import { LoaderService } from "../Common/LoaderService";
import { SupplierDomainModel } from "../Common/DomainData/SupplierDomainModel";
import { DocumentsConstants } from "../Common/DocumentsConstants";
import { FormGroup, Validators } from "@angular/forms";
import { CommonModel } from "../Common/CommonModel";
import { HttpParams, HttpClient } from "@angular/common/http";
import { ItemDetailModel } from '../ItemClose/ItemDetailModel';

@Component({
  selector: "ItemReactive",
  templateUrl: "./ItemReactiveHeadComponent.html"
})

export class ItemReactiveHeadComponent implements OnInit {
  _componentName = "Reactivate Item";
  PageTitle = "Re-activate Item";
  public serviceDocument: ServiceDocument<ItemReactiveHeadModel>;
  localizationData: any;
  model: ItemReactiveHeadModel;
  editMode: boolean; editModel: ItemReactiveHeadModel;
  primaryId: number; // used in CanDeactivate method, should be same name if we use candeactivate

  routeServiceSubscription: Subscription;
  documentsConstants: DocumentsConstants = new DocumentsConstants();
  screenName: string;
  supplierTypesData: CommonModel[];
  supplierData: SupplierDomainModel[];
  apiUrl: string;
  itemStatus: string;
  componentName: any;
  columns: any[];
  data: ItemDetailModel[] = [];
  gridOptions: GridOptions;

  constructor(private route: ActivatedRoute, public router: Router, private changeRef: ChangeDetectorRef
    , public sharedService: SharedService, private service: ItemReactiveHeadService, private loaderService: LoaderService, private httpClient: HttpClient) {
  }

  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }

  ngOnInit(): void {
    this.supplierTypesData = Object.assign([], this.service.serviceDocument.domainData["supplierTypes"]);
    this.supplierData = Object.assign([], this.service.serviceDocument.domainData["supplier"]);
    this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
    this.screenName = this.documentsConstants.ItemReactiveObjectName;
    this.apiUrl = "/api/Supplier/SuppliersGetName?name=";
    this.componentName = this;
    this.columns = [
      { headerName: "Item", field: "item", tooltipField: "item" },
      { headerName: "Item Desc", field: "itemDesc", tooltipField: "itemDesc" }
    ];

    if (this.service.serviceDocument.dataProfile.dataModel) {
      this.bind();
    }
  }

  bind(): void {
    this.routeServiceSubscription = this.route.params.subscribe((resp: any) => {
      this.primaryId = +resp["id"];
    });
    if (this.primaryId.toString() !== "0") {
      this.editMode = true;
    } else {
      this.service.serviceDocument.dataProfile.dataModel =
        Object.assign(
          this.service.serviceDocument.dataProfile.dataModel, { operation: "I", status: "Worksheet", supplierType: "" }
          , { workflowInstance: this.service.serviceDocument.dataProfile.dataModel.workflowInstance });
      this.service.serviceDocument.dataProfile.profileForm.patchValue(this.service.serviceDocument.dataProfile.dataModel);
    }
    if (this.editMode) {
      this.itemStatus = this.service.serviceDocument.dataProfile.dataModel.status;
      if (this.service.serviceDocument.dataProfile.dataModel.itemDetails) {
        Object.assign(this.data, this.service.serviceDocument.dataProfile.dataModel.itemDetails);
      }
      this.service.serviceDocument.dataProfile.dataModel = Object.assign(this.service.serviceDocument.dataProfile.dataModel
        , { operation: "I" }, { workflowInstance: this.service.serviceDocument.dataProfile.dataModel.workflowInstance });
      this.service.serviceDocument.dataProfile.profileForm.patchValue(this.service.serviceDocument.dataProfile.dataModel);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  save(saveOnly: boolean): void {
    let fm: FormGroup = this.serviceDocument.dataProfile.profileForm;
    fm.controls["supplierType"].setValidators([Validators.required]);
    fm.controls["supplierType"].updateValueAndValidity();
    this.sharedService.validateForm(fm);
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.saveSubscription(saveOnly);
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  saveAndContinue(): void {
    this.save(false);
  }

  saveSubscription(saveOnly: boolean): void {
    this.loaderService.display(true);
    this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.primaryId = this.service.serviceDocument.dataProfile.dataModel.id;
        this.sharedService.saveForm(`Saved successfully - Request ID ${this.primaryId}`).subscribe(() => {
          if (saveOnly) {
            this.router.navigate(["/ItemReactive/List"], { skipLocationChange: true });
          } else {
            this.router.navigate(["/Blank"], { skipLocationChange: true, queryParams: { id: "/ItemReactive/New/" + this.primaryId } });
            this.openItemReactivationExcel(true);
            this.loaderService.display(false);
          }
        }, (error: string) => { console.log(error); });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
    }, (error: string) => { console.log(error); });
  }

  reset(): void {
    Object.keys(this.serviceDocument.dataProfile.profileForm.controls).forEach(control => {
      this.serviceDocument.dataProfile.profileForm.controls[control].setValue(this.serviceDocument.dataProfile.dataModel[control]);
    });
    this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
  }

  ngOnDestroy(): void {
    this.routeServiceSubscription.unsubscribe();
  }

  submitAction(): void {
    this.loaderService.display(true);
    this.service.submit().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.primaryId = this.service.serviceDocument.dataProfile.dataModel.id;
        this.sharedService.saveForm(`Saved successfully - Request ID  ${this.primaryId}`).subscribe(() => {
          this.router.navigate(["/ItemReactive/List"], { skipLocationChange: true });
        }, (error: string) => {
          console.log(error);
          this.loaderService.display(false);
        });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
        this.loaderService.display(false);
      }
    }, (error: string) => {
      console.log(error);
      this.loaderService.display(false);
    });
  }

  openItemReactivationExcel(saveOnly: boolean): void {
    //this.sharedService.downloadExcel(this.service.serviceDocument.dataProfile.dataModel.id, "ItemReactive");
    if (this.serviceDocument.dataProfile.profileForm.controls["supplierType"].value == null) {
      //this.sharedService.errorForm("Please select Supplier Type")
      let fm: FormGroup = this.serviceDocument.dataProfile.profileForm;
      fm.controls["supplierType"].setValidators([Validators.required]);
      fm.controls["supplierType"].updateValueAndValidity();
      this.sharedService.validateForm(fm);
    }
    else if (this.serviceDocument.dataProfile.profileForm.controls["supplierId"].value == null) {
      //this.sharedService.errorForm("Please select Supplier")
      let fm: FormGroup = this.serviceDocument.dataProfile.profileForm;
      fm.controls["supplierId"].setValidators([Validators.required]);
      fm.controls["supplierId"].updateValueAndValidity();
      this.sharedService.validateForm(fm);
    }
    else {
      this.service.downloadExcel(this.service.serviceDocument.dataProfile.dataModel.id, this.serviceDocument.dataProfile.profileForm.controls["supplierType"].value, this.serviceDocument.dataProfile.profileForm.controls["supplierId"].value);
    }
  }

  changeSupplier($event: any): void {
    this.serviceDocument.dataProfile.profileForm.patchValue({
      supplierType: $event.options.supplierType
    });
  }
}
