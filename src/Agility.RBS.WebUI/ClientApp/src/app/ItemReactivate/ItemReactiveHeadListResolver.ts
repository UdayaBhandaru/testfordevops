﻿import { Injectable } from "@angular/core";
import { ServiceDocument } from "@agility/frameworkcore";
import { Observable } from "rxjs";
import { ItemReactiveHeadService } from "./ItemReactiveHeadService";
import { ItemReactiveHeadModel } from "./ItemReactiveHeadModel";
import { Resolve } from "@angular/router";

@Injectable()
export class ItemReactiveHeadListResolver implements Resolve<ServiceDocument<ItemReactiveHeadModel>> {
    constructor(private service: ItemReactiveHeadService) { }
    resolve(): Observable<ServiceDocument<ItemReactiveHeadModel>> {
        return this.service.list();
    }
}