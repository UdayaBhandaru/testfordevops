import { ItemDetailModel } from '../ItemClose/ItemDetailModel';

export class ItemReactiveHeadModel {
  description?: string;
  operation?: string;
  id?: number;
  workflowInstance?: string;
  status?: string;
  supplierType: string;
  supplierId?: number;
  supplierName?: string;
  itemDetails?: ItemDetailModel[]
}
