﻿export class SupplierAddressMasterModel {
    supplierId?: number;

    supplierName?: string;

    addressType: string;

    addressTypeDesc?: string;

    sequenceNo?: number;

    oracleVendorSiteID?: number;

    contactName: string;

    contactPhone: string;

    contactFax: string;

    contactEmail: string;

    contactTelex: string;

    primaryAddressIndicator: string;

    primaryAddressIndicatorDesc?: string;

    addressLine1: string;

    addressLine2: string;

    addressLine3: string;

    city: string;

    state: string;

    countryID: string;

    postalCode: string;

    ediAddressChg: string;

    country: string;

    publishIndicator: string;

    operation: string;

    tableName: string;
}