﻿import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { SupplierAddressMasterService } from "./SupplierAddressMasterService";
import { SupplierAddressMasterModel } from "./SupplierAddressMasterModel";

@Injectable()
export class SupplierAddressMasterListResolver implements Resolve<ServiceDocument<SupplierAddressMasterModel>> {
    constructor(private service: SupplierAddressMasterService) { }
    resolve(): Observable<ServiceDocument<SupplierAddressMasterModel>> {
        return this.service.list();
    }
}

@Injectable()
export class SupplierAddressMasterResolver implements Resolve<ServiceDocument<SupplierAddressMasterModel>> {
    constructor(private service: SupplierAddressMasterService) { }
    resolve(): Observable<ServiceDocument<SupplierAddressMasterModel>> {
        return this.service.addEdit();
    }
}