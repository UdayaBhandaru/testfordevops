import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup, FormBuilder, FormControl, Validators, ValidatorFn, AbstractControl } from "@angular/forms";
import { SharedService } from "../../Common/SharedService";
import { GridOptions } from "ag-grid-community";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { GridExportModel } from "../../Common/grid/GridExportModel";
import { InfoComponent } from "../../Common/InfoComponent";
import { GridEditComponent } from "../../Common/grid/GridEditComponent";
import { GridDateComponent } from "../../Common/grid/GridDateComponent";
import { SupplierAddressMasterModel } from "./SupplierAddressMasterModel";
import { SupplierAddressMasterService } from "./SupplierAddressMasterService";
import { SupplierDomainModel } from "../../Common/DomainData/SupplierDomainModel";

@Component({
  selector: "supplierAddressMaster-list",
  templateUrl: "./SupplierAddressMasterListComponent.html"
})
export class SupplierAddressMasterListComponent implements OnInit {
  public serviceDocument: ServiceDocument<SupplierAddressMasterModel>;
  PageTitle = "Supplier Address";
  redirectUrl = "SupplierAddressMaster";
  componentName: any;
  domainDtlData: DomainDetailModel[];
  addressTypeData: DomainDetailModel[];
  columns: any[];
  model: SupplierAddressMasterModel;
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  supplierData: SupplierDomainModel[];
  gridOptions: GridOptions;

  constructor(public service: SupplierAddressMasterService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      {
        headerName: "Supplier", field: "supplierName", tooltipField: "supplierName"
      },
      {
        headerName: "Address", field: "addressLine1", tooltipField: "addressLine1"
      },
      {
        headerName: "City", field: "city", tooltipField: "city"
      },
      {
        headerName: "Contact Name", field: "contactName", tooltipField: "contactName"
      },
      {
        headerName: "Primary Address", field: "primaryAddressIndicatorDesc", tooltipField: "primaryAddressIndicatorDesc"
      },
      {
        headerName: "Oracle Vendor Site ID", field: "oracleVendorSiteID", tooltipField: "oracleVendorSiteID"
      },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent
      }
    ];

    this.domainDtlData = Object.assign([], this.service.serviceDocument.domainData["status"]);
    this.addressTypeData = Object.assign([], this.service.serviceDocument.domainData["addressType"]);
    this.supplierData = Object.assign([], this.service.serviceDocument.domainData["supplier"]);

    this.sharedService.domainData = {
      status: this.domainDtlData, addressLine1: this.addressTypeData, supplier: this.supplierData
    };

    this.model = {
      supplierId: null, addressType: null, sequenceNo: null, oracleVendorSiteID: null,
      contactName: null, contactPhone: null, contactFax: null, contactTelex: null, contactEmail: null,
      addressLine1: null, addressLine2: null, addressLine3: null, city: null, state: null, countryID: null, postalCode: null,
      ediAddressChg: null, publishIndicator: null, country: null, operation: null, tableName: null, primaryAddressIndicator: null
    };

    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }
}
