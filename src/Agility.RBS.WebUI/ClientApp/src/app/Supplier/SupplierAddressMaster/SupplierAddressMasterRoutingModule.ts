﻿import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { SupplierAddressMasterListResolver, SupplierAddressMasterResolver } from "./SupplierAddressMasterResolver";
import { SupplierAddressMasterListComponent } from "./SupplierAddressMasterListComponent";
import { SupplierAddressMasterComponent } from "./SupplierAddressMasterComponent";


const SupplierRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: SupplierAddressMasterListComponent,
                resolve:
                {
                    serviceDocument: SupplierAddressMasterListResolver
                }
            },
            {
                path: "New",
                component: SupplierAddressMasterComponent,
                resolve:
                {
                    serviceDocument: SupplierAddressMasterResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(SupplierRoutes)
    ]
})
export class SupplierAddressMasterRoutingModule { }
