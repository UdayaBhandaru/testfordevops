﻿import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { SupplierAddressMasterRoutingModule } from "./SupplierAddressMasterRoutingModule";
import { SupplierAddressMasterListResolver, SupplierAddressMasterResolver } from "./SupplierAddressMasterResolver";
import { SupplierAddressMasterService } from "./SupplierAddressMasterService";
import { SupplierAddressMasterListComponent } from "./SupplierAddressMasterListComponent";
import { SupplierAddressMasterComponent } from "./SupplierAddressMasterComponent";
import { RbsSharedModule } from "../../RbsSharedModule";


@NgModule({
    imports: [
        FrameworkCoreModule,
        SupplierAddressMasterRoutingModule,
        AgGridModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        SupplierAddressMasterListComponent,
        SupplierAddressMasterComponent
    ],
    providers: [
        SupplierAddressMasterListResolver,
        SupplierAddressMasterResolver,
        SupplierAddressMasterService
    ]
})
export class SupplierAddressMasterModule {
}
