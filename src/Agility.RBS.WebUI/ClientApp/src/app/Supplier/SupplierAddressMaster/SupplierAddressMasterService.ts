﻿import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { SupplierAddressMasterModel } from "./SupplierAddressMasterModel";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";

@Injectable()
export class SupplierAddressMasterService {
    serviceDocument: ServiceDocument<SupplierAddressMasterModel> = new ServiceDocument<SupplierAddressMasterModel>();
    supplierAddressMasterModelObj = new SupplierAddressMasterModel();
    searchData: any;
    constructor(private httpHelperService: HttpHelperService) { }

    newModel(model: SupplierAddressMasterModel): ServiceDocument<SupplierAddressMasterModel> {
        return this.serviceDocument.newModel(model);
    }

    search(): Observable<ServiceDocument<SupplierAddressMasterModel>> {
        return this.serviceDocument.search("/api/SupplierAddress/Search");
    }

    save(): Observable<ServiceDocument<SupplierAddressMasterModel>> {
        return this.serviceDocument.save("/api/SupplierAddress/Save");
    }

    list(): Observable<ServiceDocument<SupplierAddressMasterModel>> {
        return this.serviceDocument.list("/api/SupplierAddress/List");
    }

    addEdit(): Observable<ServiceDocument<SupplierAddressMasterModel>> {
        return this.serviceDocument.list("/api/SupplierAddress/New");
    }

    isExistingSupplierAddress(supplierId: number, addressType: string): Observable<boolean> {
        return this.httpHelperService.get("/api/SupplierAddress/IsExistingSupplierAddress", new HttpParams().set("supplierId", supplierId.toString())
            .set("addressType", addressType));
    }
}