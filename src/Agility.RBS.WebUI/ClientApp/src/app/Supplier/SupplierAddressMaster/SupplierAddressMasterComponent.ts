﻿import { Component, OnInit, ChangeDetectorRef, AfterViewInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { Validators, FormGroup } from "@angular/forms";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { SharedService } from "../../Common/SharedService";
import { SupplierAddressMasterModel } from "./SupplierAddressMasterModel";
import { SupplierAddressMasterService } from "./SupplierAddressMasterService";
import { SupplierMasterModel } from "../SupplierMaster/SupplierMasterModel";

import { CountryDomainModel } from "../../Common/DomainData/CountryDomainModel";
import { SupplierDomainModel } from "../../Common/DomainData/SupplierDomainModel";
import { DomainDetailModel } from "../../domain/DomainDetailModel";

@Component({
    selector: "supplierAddressMaster",
    templateUrl: "./SupplierAddressMasterComponent.html"
})

export class SupplierAddressMasterComponent implements OnInit, AfterViewInit {
    PageTitle = "Supplier Address";
    redirectUrl = "SupplierAddressMaster";
    serviceDocument: ServiceDocument<SupplierAddressMasterModel>;
    domainDtlData: DomainDetailModel[];
    indicatorData: DomainDetailModel[];
    supplierData: SupplierDomainModel[];
    countryData: CountryDomainModel[];
    addressTypeData: DomainDetailModel[];
    editModel: any;
    editMode: boolean = false;
    localizationData: any;
    model: SupplierAddressMasterModel;
    supplierMasterModel: SupplierMasterModel;
    defaultStatus: string;
    defaultIndicator: string;

    constructor(private service: SupplierAddressMasterService, private sharedService: SharedService, private router: Router
        , private ref: ChangeDetectorRef) {
    }

    ngAfterViewChecked(): void {
        this.ref.detectChanges();
    }

    ngOnInit(): void {
        this.supplierData = Object.assign([], this.service.serviceDocument.domainData["supplier"]);
        this.domainDtlData = Object.assign([], this.service.serviceDocument.domainData["status"]);
        this.indicatorData = Object.assign([], this.service.serviceDocument.domainData["indicator"]);
        this.countryData = Object.assign([], this.service.serviceDocument.domainData["country"]);
        this.addressTypeData = Object.assign([], this.service.serviceDocument.domainData["addressType"]);
        this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
        this.defaultStatus = this.domainDtlData.filter(itm => itm.requiredInd === "1")[0].code;
        this.defaultIndicator = this.indicatorData.filter(itm => itm.requiredInd === "1")[0].code;
        this.bind();
    }

    ngAfterViewInit(): void {
        this.serviceDocument.dataProfile.profileForm.controls["contactEmail"].setValidators([Validators.required
            , this.sharedService.emailValidator("Invalid Email Address")]);
    }

    bind(): void {
        delete this.editModel;
        if (this.sharedService.editObject) {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
            this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            delete this.sharedService.editObject;
            this.editMode = true;
        } else {
            this.model = {
                supplierId: null, addressType: null, sequenceNo: null, oracleVendorSiteID: null,
                contactName: null, contactPhone: null, contactFax: null, contactTelex: null, contactEmail: null,
                primaryAddressIndicator: this.defaultIndicator, addressLine1: null, addressLine2: null, addressLine3: null, city: null
                , state: null, countryID: null, postalCode: null, ediAddressChg: this.defaultIndicator, publishIndicator: this.defaultIndicator
                , country: null, operation: "I", tableName: null
            };
            this.supplierData = this.supplierData.filter(item => item.supStatus === "A");
            this.service.serviceDocument.dataProfile.dataModel = this.model;
            this.service.newModel(this.model);
        }
        this.serviceDocument = this.service.serviceDocument;
    }

    save(saveOnly: boolean): void {
        if (this.serviceDocument.dataProfile.profileForm.valid) {
            this.saveSubscription(saveOnly);
        } else {
            this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
        }
    }

    saveSubscription(saveOnly: boolean): void {
        this.checkSupplierAddress(saveOnly);
    }

    saveSupplierAddressMaster(saveOnly: boolean): void {
        this.service.save().subscribe(() => {
            if (this.service.serviceDocument.result.type === MessageType.success) {
                this.sharedService.saveForm(this.localizationData.supplierAddressMaster.supplieraddressmastersave).subscribe(() => {
                    if (saveOnly) {
                        this.router.navigate(["/" + this.redirectUrl + "/List"], { skipLocationChange: true });
                    } else {
                        if (!this.editMode) {
                            this.model = {
                                supplierId: null, addressType: null, sequenceNo: null, oracleVendorSiteID: null,
                                contactName: null, contactPhone: null, contactFax: null, contactTelex: null, contactEmail: null,
                                primaryAddressIndicator: this.defaultIndicator, addressLine1: null, addressLine2: null, addressLine3: null
                                , city: null, state: null, countryID: null, postalCode: null, ediAddressChg: this.defaultIndicator
                                , publishIndicator: this.defaultIndicator, country: null, operation: "I", tableName: null
                            };
                            this.service.serviceDocument.dataProfile.dataModel = this.model;
                            this.service.newModel(this.model);
                        } else {
                            Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
                            this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.service.serviceDocument.dataProfile.dataModel);
                            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
                        }
                    }
                }, (error: string) => { console.log(error); });
            } else {
                this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
            }
        }, (error: string) => { console.log(error); });
    }

    reset(): void {
        if (!this.editMode) {
            this.model = {
                supplierId: null, addressType: null, sequenceNo: null, oracleVendorSiteID: null,
                contactName: null, contactPhone: null, contactFax: null, contactTelex: null, contactEmail: null,
                primaryAddressIndicator: this.defaultIndicator, addressLine1: null, addressLine2: null, addressLine3: null, city: null
                , state: null, countryID: null, postalCode: null, ediAddressChg: this.defaultIndicator, publishIndicator: this.defaultIndicator
                , country: null, operation: "I", tableName: null
            };
            this.service.newModel(this.model);
        } else {
            this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);
            this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
        }
        this.serviceDocument = this.service.serviceDocument;
        this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
        this.serviceDocument.dataProfile.profileForm.controls["contactEmail"].setValidators([Validators.required
            , this.sharedService.emailValidator("Invalid Email Address")]);
    }

    openSupplierMaster($event: MouseEvent): void {
        $event.preventDefault();
        this.supplierMasterModel = {
            supplier: this.service.serviceDocument.dataProfile.dataModel.supplierId, supplierName: null, supplierStatus: null
            , supplierStatusDesc: null, supplierSecondaryName: null, languageId: null, languageDesc: null, contactName1: null
            , contactPhone: null, contactFax: null, contactPager: null, contactEmail1: null, qcIndicator: null, qcPercentage: null
            , qcFrequency: null, vcIndicator: null, vcPercentage: null, vcFrequency: null, currencyCode: null, vatExempt: null
            , vatRegistrationId: null, vatRegion: null, paymentMode: null, autoApproveInvoiceIndicator: null, costChgPercentageVar: null
            , costChgAmountVar: null, allowRebate: null, rebatePeriod: null, rebateDiscount: null, displayDiscount: null, dbtMemoCode: null
            , settlementCode: null, bracketCostingIndicator: null, autoApproveDbtMemoIndicator: null, invoicePayLocation: null
            , invoiceRecieveLocation: null, addInvoiceGrossNet: null, prePayInvoiceIndicator: null, backOrderIndicator: null, allowOverDue: null
            , ediPoIndicator: null, ediPoChange: null, ediPoConfirm: null, ediAsn: null, ediSalesRepeatFrequency: null
            , ediSupportAvailableIndicator: null, ediContractIndicator: null, ediInvoiceIndicator: null, freightChargeIndicator: null
            , paymentTerms: null, freightTerms: null, ofTransferFlag: null, ofEnableTransferFlag: null, servicePerfReqIndicator: null
            , shipMethod: null, finalDstIndicator: null, defaultItemLeadTime: null, deliveryPolicy: null, dunsNumber: null, dunsLocation: null
            , dsdIndicator: null, invManagementLevel: null, retAllowIndicator: null, retAuthorizationRequired: null, retMinimumDolAmount: null
            , retailCourier: null, replenApprovalIndicator: null, handlingPercentage: null, preMarkIndicator: null, vmiOrderStatus: null
            , commentDescription: null, operation: "I", tableName: null
            , addressType: null, sequenceNo: null, primaryAddressIndicator: null, addressLine1: null
            , addressLine2: null, addressLine3: null, city: null, state: null, countryID: null, postalCode: null
            , minServiceLevel: null, supplierRetailFlag: null, supplierType: null, dcIndicator: null
            , accountNumberType: null, bankName: null, bankAddress: null, bankDetails: null
            , supplierGroup: null, supplierTier: null, listOfAddress: []
            , address1AddressType: null, address1AddressLine1: null, address1City: null, address1State: null, address1CountryID: null, address1PostalCode: null
            , address2AddressType: null, address2AddressLine1: null, address2City: null, address2State: null, address2CountryID: null, address2PostalCode: null
            , contactEmail2: null, contactName2: null, contactTitle1: null, contactTitle2: null, isExpiry: null, others: null
            , barCodePrintCharge: null, barCodePrintingFlag: null, costHideInvoice: null, discountHideInvoice: null, paymentTermType: null, rebateHead: null
            , workflowInstance: null
        };
        this.sharedService.searchData = Object.assign({}, this.supplierMasterModel);
        this.router.navigate(["/SupplierMaster"], { skipLocationChange: true });
    }

    checkSupplierAddress(saveOnly: boolean): void {
        if (!this.editMode) {
            let formObj: FormGroup = this.serviceDocument.dataProfile.profileForm;
            this.service.isExistingSupplierAddress(formObj.controls["supplierId"].value
                , formObj.controls["addressType"].value).subscribe((response: boolean) => {
                    if (response) {
                        this.sharedService.errorForm(this.localizationData.supplierAddressMaster.supplieraddressmastercheck);
                    } else {
                        this.saveSupplierAddressMaster(saveOnly);
                    }
                });
        } else {
            this.saveSupplierAddressMaster(saveOnly);
        }
    }

    changeSupplier($event: any): void {
        this.service.serviceDocument.dataProfile.profileForm.patchValue({
            contactPhone: $event.options.contactPhone,
            contactFax: $event.options.contactFax,
            contactEmail: $event.options.contactEmail,
            contactName: $event.options.contactName
        });
    }
}