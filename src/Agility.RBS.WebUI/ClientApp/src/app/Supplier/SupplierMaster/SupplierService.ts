﻿import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { SupplierSearchModel } from "./SupplierSearchModel";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";

@Injectable()
export class SupplierService {
    serviceDocument: ServiceDocument<SupplierSearchModel> = new ServiceDocument<SupplierSearchModel>();
    supplierModelObj = new SupplierSearchModel();
    searchData: any;
    supplierPaging: {
        startRow: number,
        pageSize: number,
        cacheSize: number,
    } = {
            startRow: 0,
            pageSize: 10,
            cacheSize: 100,
        };

    constructor(private httpHelperService: HttpHelperService) { }

    newModel(model: SupplierSearchModel): ServiceDocument<SupplierSearchModel> {
        return this.serviceDocument.newModel(model);
    }

    search(additionalParams: any): Observable<ServiceDocument<SupplierSearchModel>> {
        return this.serviceDocument.search("/api/SupplierSearch/Search", true, () => this.setSupplierPaging(additionalParams));
    }

    list(): Observable<ServiceDocument<SupplierSearchModel>> {
        return this.serviceDocument.list("/api/SupplierSearch/List");
    }

    setSupplierPaging(additionalParams: any): void {
        if (additionalParams) {
            this.serviceDocument.dataProfile.dataModel.startRow = additionalParams.startRow ? additionalParams.startRow : this.supplierPaging.startRow;
            this.serviceDocument.dataProfile.dataModel.endRow = additionalParams.endRow ? additionalParams.endRow : this.supplierPaging.cacheSize;
            this.serviceDocument.dataProfile.dataModel.sortModel = additionalParams.sortModel;
        } else {
            this.serviceDocument.dataProfile.dataModel.startRow = this.supplierPaging.startRow;
            this.serviceDocument.dataProfile.dataModel.endRow = this.supplierPaging.cacheSize;
        }
    }
}