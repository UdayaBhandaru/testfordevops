﻿export class RebateValidationModel {
    public slabValidationData = [
        {
            "key": "slab10", "value": {
                current: "slab1GrowthMinThreshold", next: "slab1GrowthMaxThreshold"
                , validationMsg: "Max should be more than Min"
            }
        },
        {
            "key": "slab12", "value": {
                current: "slab1GrowthMaxThreshold", next: "slab2GrowthMinThreshold"
                , validationMsg: "Value should be more than previous Slab"
            }
        },
        {
            "key": "slab20", "value": {
                current: "slab2GrowthMinThreshold", next: "slab2GrowthMaxThreshold"
                , validationMsg: "Max should be more than Min"
            }
        },
        {
            "key": "slab23", "value": {
                current: "slab2GrowthMaxThreshold", next: "slab3GrowthMinThreshold"
                , validationMsg: "Value should be more than previous Slab"
            }
        },
        {
            "key": "slab30", "value": {
                current: "slab3GrowthMinThreshold", next: "slab3GrowthMaxThreshold"
                , validationMsg: "Max should be more than Min"
            }
        },
        {
            "key": "slab34", "value": {
                current: "slab3GrowthMaxThreshold", next: "slab4GrowthMinThreshold"
                , validationMsg: "Value should be more than previous Slab"
            }
        },
        {
            "key": "slab40", "value": {
                current: "slab4GrowthMinThreshold", next: "slab4GrowthMaxThreshold"
                , validationMsg: "Max should be more than Min"
            }
        },
        {
            "key": "slab45", "value": {
                current: "slab4GrowthMaxThreshold", next: "slab5GrowthMinThreshold"
                , validationMsg: "Value should be more than previous Slab"
            }
        },
        {
            "key": "slab50", "value": {
                current: "slab5GrowthMinThreshold", next: "slab5GrowthMaxThreshold"
                , validationMsg: "Max should be more than Min"
            }
        }];
}