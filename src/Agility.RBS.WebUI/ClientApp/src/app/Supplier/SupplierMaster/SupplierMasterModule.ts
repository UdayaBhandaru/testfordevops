﻿import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { AgGridModule } from "ag-grid-angular/main";
import { AgGridSharedModule } from "../../AgGridSharedModule";
import { TitleSharedModule } from "../../TitleSharedModule";
import { SupplierMasterRoutingModule } from "./SupplierMasterRoutingModule";
import { SupplierMasterListResolver, SupplierMasterResolver } from "./SupplierMasterResolver";
import { SupplierMasterService } from "./SupplierMasterService";
import { SupplierMasterListComponent } from "./SupplierMasterListComponent";
import { SupplierMasterComponent } from "./SupplierMasterComponent";
import { SupplierService } from "./SupplierService";
import { SupplierDealService } from "../SupplierDeals/SupplierDealService";
import { InboxService } from "../../inbox/InboxService";
import { RbsSharedModule } from "../../RbsSharedModule";

@NgModule({
    imports: [
        FrameworkCoreModule,
        SupplierMasterRoutingModule,
        AgGridModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        SupplierMasterListComponent,
        SupplierMasterComponent
    ],
    providers: [
        SupplierMasterListResolver,
        SupplierMasterResolver,
        SupplierMasterService,
        SupplierService,
        SupplierDealService,
        InboxService
    ]
})
export class SupplierMasterModule {
}
