﻿export class SupplierRebateDetailModel {
    rebateId?: number;
    rebateDetailId?: number;
    growthPercent?: number;
    minThresholdLimit?: number;
    maxThresholdLimit?: number;
    rebatePercent?: number;
    rebateStartDate?: Date;
    rebateEndDate?: Date;
    rebateDetailStatus: string;
    operation: string;
    createdBy?: string;
    modifiedBy?: string;
    createdDate?: Date;
    modifiedDate?: Date;
}