﻿import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument, WorkflowAction } from "@agility/frameworkcore";
import { SupplierMasterModel } from "./SupplierMasterModel";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from "../../Common/HttpHelperService";

@Injectable()
export class SupplierMasterService {
    serviceDocument: ServiceDocument<SupplierMasterModel> = new ServiceDocument<SupplierMasterModel>();
    supplierMasterModelObj = new SupplierMasterModel();
    searchData: any;

    constructor(private httpHelperService: HttpHelperService) { }

    newModel(model: SupplierMasterModel): ServiceDocument<SupplierMasterModel> {
        return this.serviceDocument.newModel(model);
    }

    addEdit(id: number): Observable<ServiceDocument<SupplierMasterModel>> {
        if (+id === 0) {
            return this.serviceDocument.new("/api/Supplier/New");
        } else {
            return this.serviceDocument.open("/api/Supplier/Open", new HttpParams().set("id", id.toString()));
        }
    }

    save(): Observable<ServiceDocument<SupplierMasterModel>> {
        if (this.serviceDocument.dataProfile.dataModel.supplier == null || this.serviceDocument.dataProfile.dataModel.supplier === 0) {
            let currentAction: WorkflowAction = this.serviceDocument.dataProfile.actionService.allowedActions
                .find((v) => v.transitionClaim === "SubmitSupplierStartToWorksheetClaim");
            if (!this.serviceDocument.dataProfile.dataModel.supplier && currentAction) {
                this.serviceDocument.dataProfile.profileForm.controls["currentActionId"].setValue(currentAction.transitionClaim);
                return this.serviceDocument.submit("/api/Supplier/Submit", true);
            }
        } else {
            return this.serviceDocument.save("/api/Supplier/Save", true);
        }
    }

    submit(): Observable<ServiceDocument<SupplierMasterModel>> {
        return this.serviceDocument.submit("/api/Supplier/Submit", true);
    }

    isExistingSupplier(supplierId: number): Observable<boolean> {
        return this.httpHelperService.get("/api/Supplier/IsExistingSupplier", new HttpParams().set("supplierId", supplierId.toString()));
    }
}