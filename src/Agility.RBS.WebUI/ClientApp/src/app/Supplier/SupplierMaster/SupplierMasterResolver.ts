﻿import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { SupplierMasterService } from "./SupplierMasterService";
import { SupplierMasterModel } from "./SupplierMasterModel";
import { SupplierService } from "./SupplierService";
import { SupplierSearchModel } from "./SupplierSearchModel";

@Injectable()
export class SupplierMasterListResolver implements Resolve<ServiceDocument<SupplierSearchModel>> {
    constructor(private service: SupplierService) { }
    resolve(): Observable<ServiceDocument<SupplierSearchModel>> {
        return this.service.list();
    }
}

@Injectable()
export class SupplierMasterResolver implements Resolve<ServiceDocument<SupplierMasterModel>> {
    constructor(private service: SupplierMasterService) { }
    resolve(route: ActivatedRouteSnapshot): Observable<ServiceDocument<SupplierMasterModel>> {
        return this.service.addEdit(route.params["id"]);
    }
}
