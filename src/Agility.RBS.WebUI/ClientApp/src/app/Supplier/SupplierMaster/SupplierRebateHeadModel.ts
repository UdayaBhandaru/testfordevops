﻿import { SupplierRebateDetailModel } from "./SupplierRebateDetailModel";

export class SupplierRebateHeadModel {
    rebateId?: number;
    supplierId?: number;
    rebateType: string;
    defaultRebatePercent?: number;
    rebateStatus: string;
    operation: string;
    createdBy?: string;
    modifiedBy?: string;
    createdDate?: Date;
    modifiedDate?: Date;
    rebateDetails?: SupplierRebateDetailModel[];
}