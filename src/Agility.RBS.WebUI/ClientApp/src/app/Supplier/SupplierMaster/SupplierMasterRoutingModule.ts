﻿import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { SupplierMasterListResolver, SupplierMasterResolver } from "./SupplierMasterResolver";
import { SupplierMasterListComponent } from "./SupplierMasterListComponent";
import { SupplierMasterComponent } from "./SupplierMasterComponent";
const SupplierRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
                component: SupplierMasterListComponent,
                resolve:
                {
                    serviceDocument: SupplierMasterListResolver
                }
            },
            {
                path: "New/:id",
                component: SupplierMasterComponent,
                resolve:
                {
                    serviceDocument: SupplierMasterResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(SupplierRoutes)
    ]
})
export class SupplierMasterRoutingModule { }
