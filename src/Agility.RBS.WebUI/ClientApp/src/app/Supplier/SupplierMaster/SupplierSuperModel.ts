﻿export class SupplierSuperModel {
    supplier?: number;
    supplierName: string;
    supplierDescription: string;
    supplierType: string;
    supplierTypeName: string;
    fixedRebatePercent?: number;
    autoReplenishment: string;
    primarySupplier: string;
    orderMultiple?: number;
    fullAddress: string;
}