﻿export class SupplierSearchModel {
    supplierId?: number;

    supplierName: string;

    contactName1: string;

    supplierStatus: string;

    supplierStatusDesc: string;

    currencyCode: string;

    languageId?: number;

    languageDesc: string;

    supplierType: string;

    supplierTypeDesc: string;

    rowId: string;

    startRow?: number;

    endRow?: number;

    totalRows?: number;

    sortModel?: {}[];

    tableName: string;
}