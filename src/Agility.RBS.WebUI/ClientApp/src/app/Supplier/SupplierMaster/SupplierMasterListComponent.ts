import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup, FormBuilder, FormControl, Validators, ValidatorFn, AbstractControl } from "@angular/forms";
import { SharedService } from "../../Common/SharedService";
import { GridOptions, GridReadyEvent, IServerSideDatasource, IServerSideGetRowsParams, ColDef } from "ag-grid-community";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { GridExportModel } from "../../Common/grid/GridExportModel";
import { InfoComponent } from "../../Common/InfoComponent";
import { GridEditComponent } from "../../Common/grid/GridEditComponent";
import { GridDateComponent } from "../../Common/grid/GridDateComponent";
import { SupplierSearchModel } from "./SupplierSearchModel";
import { SupplierService } from "./SupplierService";
import { CommonModel } from "../../Common/CommonModel";

@Component({
  selector: "supplierMaster-list",
  templateUrl: "./SupplierMasterListComponent.html"
})
export class SupplierMasterListComponent implements OnInit, IServerSideDatasource {
  public serviceDocument: ServiceDocument<SupplierSearchModel>;
  PageTitle = "Supplier";
  redirectUrl = "SupplierMaster/New/";
  componentName: any;
  domainDtlData: DomainDetailModel[];
  columns: ColDef[];
  model: SupplierSearchModel;
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  gridOptions: GridOptions;
  additionalSearchParams: any;
  loadedPage: boolean;
  supplierTypesData: CommonModel[];

  constructor(public service: SupplierService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      {
        colId: "SUPPLIER", headerName: "Supplier ID", field: "supplierId", tooltipField: "supplierId", sort: "desc"
      },
      {
        colId: "SUP_NAME", headerName: "Supplier", field: "supplierName", tooltipField: "supplierName"
      },
      {
        colId: "SUPPLIER_TYPE_DESC", headerName: "Supplier Type", field: "supplierTypeDesc", tooltipField: "supplierTypeDesc"
      },
      {
        colId: "CONTACT_NAME1", headerName: "Contact Name", field: "contactName1", tooltipField: "contactName1"
      },
      {
        colId: "CURRENCY_CODE", headerName: "Currency", field: "currencyCode", tooltipField: "currencyCode"
      },
      //{
      //    colId: "LANG_DESC", headerName: "Language", field: "languageDesc", tooltipField: "languageDesc"
      //},
      //{
      //  headerName: "Status", field: "supplierStatusDesc", tooltipField: "supplierStatusDesc"
      //},
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent
      }
    ];
    this.gridOptions = {
      onGridReady: (params: GridReadyEvent) => {
        params.api.setServerSideDatasource(this);
      },
      context: {
        componentParent: this
      },
      suppressRowClickSelection: true,
      suppressCellSelection: true,
      rowModelType: "serverSide",
      paginationPageSize: this.service.supplierPaging.pageSize,
      enableServerSideSorting: true,
      enableServerSideFilter: true,
      cacheBlockSize: this.service.supplierPaging.cacheSize
    };

    this.domainDtlData = Object.assign([], this.service.serviceDocument.domainData["status"]);
    this.supplierTypesData = Object.assign([], this.service.serviceDocument.domainData["supplierTypes"]);

    this.sharedService.domainData = {
      status: this.domainDtlData,
      supplierTypesData: this.supplierTypesData
    };

    this.model = {
      contactName1: null, currencyCode: null, endRow: null, languageDesc: null
      , supplierId: null, supplierName: null, supplierStatus: null, supplierStatusDesc: null
      , supplierType: null, supplierTypeDesc: null, rowId: null, startRow: null, totalRows: null, tableName: null
    };

    if (this.sharedService.searchData) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
    if (this.showSearchCriteria) {
      this.destroy();
    }
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl + cell.data.supplierId, this.service.serviceDocument.dataProfile.dataList);
  }

  getRows(params: IServerSideGetRowsParams): void {
    this.additionalSearchParams = { startRow: params.request.startRow, endRow: params.request.endRow, sortModel: params.request.sortModel };
    if (!this.loadedPage) {
      this.loadedPage = true;
      params.successCallback(
        this.serviceDocument.dataProfile.dataList.slice(params.request.startRow, params.request.endRow),
        this.service.serviceDocument.dataProfile.dataModel.totalRows);
    } else {
      this.service.search(this.additionalSearchParams).subscribe((sDoc) => {
        params.successCallback(sDoc.dataProfile.dataList, this.service.serviceDocument.dataProfile.dataModel.totalRows);
      });
    }
  }
  destroy() {
    this.additionalSearchParams = {};
    this.loadedPage = false;
  }

  addValidation(): void {
    this.router.navigate(["/" + this.redirectUrl + "0"], { skipLocationChange: true });
    this.sharedService.previousUrl = "../SupplierMaster/List";
  }
}
