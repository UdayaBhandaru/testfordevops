﻿import { SupplierAddressMasterModel } from "../SupplierAddressMaster/SupplierAddressMasterModel";
import { SupplierRebateHeadModel } from "./SupplierRebateHeadModel";

export class SupplierMasterModel {
    supplier?: number;

    supplierName: string;

    supplierSecondaryName: string;

    supplierStatus: string;

    supplierStatusDesc: string;

    languageId?: number;

    languageDesc: string;

    contactName1: string;

    contactPhone: string;

    contactFax: string;

    contactPager: string;

    contactEmail1: string;

    qcIndicator: string;

    qcPercentage?: number;

    qcFrequency?: number;

    vcIndicator: string;

    vcPercentage?: number;

    vcFrequency?: number;

    currencyCode: string;

    vatExempt: string;

    vatRegistrationId: string;

    vatRegion: string;

    paymentMode: string;

    autoApproveInvoiceIndicator: string;

    costChgPercentageVar?: number;

    costChgAmountVar?: number;

    allowRebate: string;

    rebatePeriod: string;

    rebateDiscount?: number;

    displayDiscount: string;

    dbtMemoCode: string;

    settlementCode: string;

    bracketCostingIndicator: string;

    autoApproveDbtMemoIndicator: string;

    invoicePayLocation: string;

    invoiceRecieveLocation: string;

    addInvoiceGrossNet: string;

    prePayInvoiceIndicator: string;

    backOrderIndicator: string;

    allowOverDue: string;

    ediPoIndicator: string;

    ediPoChange: string;

    ediPoConfirm: string;

    ediAsn: string;

    ediSalesRepeatFrequency: string;

    ediSupportAvailableIndicator: string;

    ediContractIndicator: string;

    ediInvoiceIndicator: string;

    freightChargeIndicator: string;

    paymentTerms: string;

    freightTerms: string;

    ofTransferFlag: string;

    ofEnableTransferFlag: string;

    servicePerfReqIndicator: string;

    shipMethod: string;

    finalDstIndicator: string;

    defaultItemLeadTime?: number;

    deliveryPolicy: string;

    dunsNumber: string;

    dunsLocation: string;

    dsdIndicator: string;

    invManagementLevel: string;

    retAllowIndicator: string;

    retAuthorizationRequired: string;

    retMinimumDolAmount?: number;

    retailCourier: string;

    replenApprovalIndicator: string;

    handlingPercentage?: number;

    preMarkIndicator: string;

    vmiOrderStatus: string;

    commentDescription: string;

    operation?: string;

    tableName: string;

    addressType: string;
    addressTypeDesc?: string;
    sequenceNo?: number;
    primaryAddressIndicator: string;
    primaryAddressIndicatorDesc?: string;
    addressLine1: string;
    addressLine2: string;
    addressLine3: string;
    city: string;
    state: string;
    countryID: string;
    postalCode: string;

    supplierGroup: string;
    supplierTier: string;

    contactName2: string;
    contactTitle1: string;
    contactTitle2: string;
    contactEmail2: string;

    listOfAddress: SupplierAddressMasterModel[];
    address1AddressType: string;
    address1AddressLine1: string;
    address1City: string;
    address1State: string;
    address1CountryID: string;
    address1PostalCode: string;
    address2AddressType: string;
    address2AddressLine1: string;
    address2City: string;
    address2State: string;
    address2CountryID: string;
    address2PostalCode: string;
    others: string;
    isExpiry: string;
    minServiceLevel: string;
    supplierRetailFlag: string;

    supplierType: string;
    accountNumberType: string;
    bankName: string;
    bankAddress: string;
    bankDetails: string;

    newStoreOpeningFee?: number;
    expectedAnnualVol?: number;
    accountOpeningFee?: number;
    listingFee?: number;
    paymentTermType: string;
    barCodePrintCharge?: number;
    barCodePrintingFlag: string;
    costHideInvoice: string;
    discountHideInvoice: string;

    dcIndicator: string;

    rebateHead: SupplierRebateHeadModel;
    slab1GrowthMinThreshold?: number;
    slab1GrowthMaxThreshold?: number;
    slab1Rebate?: number;
    slab2GrowthMinThreshold?: number;
    slab2GrowthMaxThreshold?: number;
    slab2Rebate?: number;
    slab3GrowthMinThreshold?: number;
    slab3GrowthMaxThreshold?: number;
    slab3Rebate?: number;
    slab4GrowthMinThreshold?: number;
    slab4GrowthMaxThreshold?: number;
    slab4Rebate?: number;
    slab5GrowthMinThreshold?: number;
    slab5GrowthMaxThreshold?: number;
    slab5Rebate?: number;

    createdDate?: Date;
    createdBy?: string;
    workflowInstance: string;
    supplierWfStatus?: string;
}