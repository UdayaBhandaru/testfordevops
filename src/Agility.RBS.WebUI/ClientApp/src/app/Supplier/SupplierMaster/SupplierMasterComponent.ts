import { Component, OnInit, AfterViewInit, ChangeDetectorRef } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { FormGroup, Validators, ValidatorFn, AbstractControl, FormControl } from "@angular/forms";
import { ColDef, GridOptions, GridReadyEvent, IServerSideGetRowsParams, IServerSideDatasource, GridApi, RowNode } from "ag-grid-community";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { Subscription } from "rxjs";
import { SharedService } from "../../Common/SharedService";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { CompanyModel } from "../../organizationhierarchy/company/CompanyModel";
import { SupplierMasterModel } from "./SupplierMasterModel";
import { SupplierMasterService } from "./SupplierMasterService";
import { CurrencyDomainModel } from "../../Common/DomainData/CurrencyDomainModel";
import { LanguageDomainModel } from "../../Common/DomainData/LanguageDomainModel";
import { PaymentDomainModel } from "../../Common/DomainData/PaymentDomainModel";
import { ShipmentDomainModel } from "../../Common/DomainData/ShipmentDomainModel";
import { TermsDomainModel } from "../../Common/DomainData/TermsDomainModel";
import { FreightDomainModel } from "../../Common/DomainData/FreightDomainModel";
import { DeliveryPolicyDomainModel } from "../../Common/DomainData/DeliveryPolicyDomainModel";
import { OutLocationDomainModel } from "../../Common/DomainData/OutLocationDomainModel";
import { ValidationModel } from "./ValidationModel";
import { DocumentsConstants } from "../../Common/DocumentsConstants";
import { SupplierAddressMasterModel } from "../../Supplier/SupplierAddressMaster/SupplierAddressMasterModel";
import { CountryDomainModel } from "../../Common/DomainData/CountryDomainModel";
import { GridDateComponent } from "../../Common/grid/GridDateComponent";
import { GridEditComponent } from "../../Common/grid/GridEditComponent";
import { CommonModel } from "../../Common/CommonModel";
import { SupplierRebateHeadModel } from "./SupplierRebateHeadModel";
import { SupplierRebateDetailModel } from "./SupplierRebateDetailModel";
import { SupplierDealPopupComponent } from "../SupplierDeals/SupplierDealPopupComponent";
import { InfoComponent } from "../../Common/InfoComponent";
import { SupplierDealService } from "../SupplierDeals/SupplierDealService";
import { DealGridModel } from "../SupplierDeals/DealGridModel";
import { DealSearchNewModel } from "../SupplierDeals/DealSearchNewModel";
import { LoaderService } from "../../Common/LoaderService";
import { RebateValidationModel } from "./RebateValidationModel";
import { InboxService } from "../../inbox/InboxService";
import { WorkflowHistoryPopupComponent } from "../../inbox/WorkflowHistoryPopupComponent";
import { WorkflowHistoryModel } from "../../inbox/WorkflowHistoryModel";
import { SupplierDealModel } from "../SupplierDeals/SupplierDealModel";
import { DealDomainModel } from '../SupplierDeals/DealDomainModel';

@Component({
  selector: "supplierMaster",
  templateUrl: "./SupplierMasterComponent.html",
  styleUrls: ['./SupplierMasterComponent.scss']
})

export class SupplierMasterComponent implements OnInit, AfterViewInit {
  PageTitle: string = "Supplier";
  redirectUrl: string = "SupplierMaster";
  serviceDocument: ServiceDocument<SupplierMasterModel>;
  domainDtlData: DomainDetailModel[];
  indicatorData: DomainDetailModel[];
  vatRegionData: DomainDetailModel[];
  currencyData: CurrencyDomainModel[];
  languageData: LanguageDomainModel[];
  paymentMethodData: DomainDetailModel[];
  shipMethodData: DomainDetailModel[];
  termsData: TermsDomainModel[];
  freightData: FreightDomainModel[];
  deliveryPolicyData: DomainDetailModel[];
  rebatePeriodData: DomainDetailModel[];
  vmiOrderStatusData: DomainDetailModel[];
  ediSalesRptFrequencyData: DomainDetailModel[];
  settlementCodeData: DomainDetailModel[];
  dbtMemoCodeData: DomainDetailModel[];
  inventoryMgmtLevelData: DomainDetailModel[];
  outLocationData: OutLocationDomainModel[];
  countryData: CountryDomainModel[];
  addressTypeData: DomainDetailModel[];
  suppAddrModel: SupplierAddressMasterModel = {
    supplierId: null, addressType: null, sequenceNo: null, oracleVendorSiteID: null,
    contactName: null, contactPhone: null, contactFax: null, contactTelex: null, contactEmail: null,
    addressLine1: null, addressLine2: null, addressLine3: null, city: null, state: null, countryID: null, postalCode: null,
    ediAddressChg: null, publishIndicator: null, country: null, operation: null, tableName: null, primaryAddressIndicator: null
  };
  editModel: any;
  editMode: boolean = false;
  localizationData: any;
  model: SupplierMasterModel;
  supplierID: number = 0;
  documentsConstants: DocumentsConstants = new DocumentsConstants();
  objectName: string;
  validationMetaObj: ValidationModel[] = [];
  miscMandatoryFieldList: string[] = [
    "retAuthorizationRequired",
    "retMinimumDolAmount"
  ];
  qcMandatoryFieldList: string[] = [
    "qcPercentage",
    "qcFrequency"
  ];
  vcMandatoryFieldList: string[] = [
    "vcPercentage",
    "vcFrequency"
  ];

  rebateMandatoryFieldList: string[] = [];
  gridOptions: GridOptions;
  columns: ColDef[];
  additionalSearchParams: any;

  loadedPage: boolean;
  componentName: any;
  dataList: DealSearchNewModel[];
  supplierGroupsData: CommonModel[];
  supplierTypesData: DomainDetailModel[];
  supplierTiersData: CommonModel[];
  payTermsTypesData: CommonModel[];
  routeServiceSubscription: Subscription;
  supplierDealPopupSubscription: Subscription;
  supplierDealServiceSubscription: Subscription;
  submitServiceSubscription: Subscription;
  submitSuccessSubscription: Subscription;
  saveServiceSubscription: Subscription;
  saveSuccessSubscription: Subscription;
  workflowHistoryServiceSubscription: Subscription;
  updateSlab1: boolean = false;
  updateSlab2: boolean = false;
  updateSlab3: boolean = false;
  updateSlab4: boolean = false;
  updateSlab5: boolean = false;
  updateAddress2: boolean = false;
  rebateValidationObj = new RebateValidationModel();
  address2Props: string[] = [
    "address2AddressLine1", "address2City", "address2State", "address2CountryID", "address2PostalCode"];
  slab1Props: string[] = [
    "slab1GrowthMinThreshold", "slab1GrowthMaxThreshold", "slab1Rebate"];
  slab2Props: string[] = [
    "slab2GrowthMinThreshold", "slab2GrowthMaxThreshold", "slab2Rebate"];
  slab3Props: string[] = [
    "slab3GrowthMinThreshold", "slab3GrowthMaxThreshold", "slab3Rebate"];
  slab4Props: string[] = [
    "slab4GrowthMinThreshold", "slab4GrowthMaxThreshold", "slab4Rebate"];
  slab5Props: string[] = [
    "slab5GrowthMinThreshold", "slab5GrowthMaxThreshold", "slab5Rebate"];
  supplierWfStatus: string;
  workflowHistoryListObj: WorkflowHistoryModel[] = [];
  gridApi: GridApi;

  constructor(private service: SupplierMasterService, public sharedService: SharedService, private router: Router
    , private route: ActivatedRoute, private supplierDealService: SupplierDealService
    , private loaderService: LoaderService, private inboxService: InboxService, private changeRef: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    this.domainDtlData = Object.assign([], this.service.serviceDocument.domainData["status"]);
    this.indicatorData = Object.assign([], this.service.serviceDocument.domainData["indicator"]);
    this.vatRegionData = Object.assign([], this.service.serviceDocument.domainData["vatRegion"]);
    this.currencyData = Object.assign([], this.service.serviceDocument.domainData["currency"]);
    this.languageData = Object.assign([], this.service.serviceDocument.domainData["language"]);
    this.paymentMethodData = Object.assign([], this.service.serviceDocument.domainData["paymentMethods"]);
    this.shipMethodData = Object.assign([], this.service.serviceDocument.domainData["shipmethod"]);
    this.termsData = Object.assign([], this.service.serviceDocument.domainData["terms"]);
    this.freightData = Object.assign([], this.service.serviceDocument.domainData["freight"]);
    this.deliveryPolicyData = Object.assign([], this.service.serviceDocument.domainData["deliveryPolicy"]);
    this.countryData = Object.assign([], this.service.serviceDocument.domainData["country"]);
    this.supplierGroupsData = Object.assign([], this.service.serviceDocument.domainData["supplierGroups"]);
    this.supplierTypesData = Object.assign([], this.service.serviceDocument.domainData["supplierTypes"]);
    this.supplierTiersData = Object.assign([], this.service.serviceDocument.domainData["supplierTiers"]);
    this.payTermsTypesData = Object.assign([], this.service.serviceDocument.domainData["payTermsTypes"]);
    this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
    this.bind();
  }

  ngAfterViewInit(): void {
    this.serviceDocument.dataProfile.profileForm.controls["contactEmail1"].setValidators(
      this.sharedService.emailValidator("Invalid Email Address"));
    this.serviceDocument.dataProfile.profileForm.controls["contactEmail2"].setValidators(
      this.sharedService.emailValidator("Invalid Email Address"));
  }

  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }

  bind(): void {
    delete this.editModel;
    this.routeServiceSubscription = this.route.params.subscribe((resp: any) => {
      this.supplierID = resp["id"];
    });
    if (this.supplierID.toString() !== "0") {
      this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
      this.editMode = true;
      this.PageTitle = `${this.PageTitle} - EDIT - ${this.supplierID}`;
      this.supplierWfStatus = this.service.serviceDocument.dataProfile.dataModel.supplierWfStatus;
    } else {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign(this.service.serviceDocument.dataProfile.dataModel
        , {
          operation: "I", supplierWfStatus: "WORKSHEET", supplierStatus: "A", vcIndicator: "N", freightChargeIndicator: "N"
          , bracketCostingIndicator: "N", prePayInvoiceIndicator: "N", backOrderIndicator: "N", dsdIndicator: "N"
          , autoApproveDbtMemoIndicator: "N", autoApproveInvoiceIndicator: "N", dcIndicator: "N", retAllowIndicator: "N"
          , ediPoChange: "N", ediPoConfirm: "N", ediAsn: "N", ediSalesRepeatFrequency: "D", replenApprovalIndicator: "N"
          , ediSupportAvailableIndicator: "N", ediContractIndicator: "N", ediInvoiceIndicator: "N"
          , servicePerfReqIndicator: "N", finalDstIndicator: "N"
          , invManagementLevel: "A", preMarkIndicator: "N", ediPoIndicator: "N", qcIndicator: "N", retAuthorizationRequired: "N"
        }
        , { workflowInstance: this.service.serviceDocument.dataProfile.dataModel.workflowInstance });
      this.service.serviceDocument.dataProfile.profileForm.patchValue(this.service.serviceDocument.dataProfile.dataModel);
      this.PageTitle = `${this.PageTitle} - ADD`;
    }
    this.serviceDocument = this.service.serviceDocument;
    this.validationMetaObj.push({ controlName: "retAllowIndicator", dependentControlList: this.miscMandatoryFieldList });
    this.validationMetaObj.push({ controlName: "qcIndicator", dependentControlList: this.qcMandatoryFieldList });
    this.validationMetaObj.push({ controlName: "vcIndicator", dependentControlList: this.vcMandatoryFieldList });
    this.validationMetaObj.push({ controlName: "allowRebate", dependentControlList: this.rebateMandatoryFieldList });
    this.objectName = this.documentsConstants.supplierObjectName;
    this.mapAddressFields();
    this.mapProgressiveRebateFields();
    this.componentName = this;
    this.setDealGridColumnsWithOptions();
    this.sharedService.checkWfPermitions(this.serviceDocument);
  }

  save(saveOnly: boolean): void {
    let fm: FormGroup = this.serviceDocument.dataProfile.profileForm;
    fm.controls["supplierType"].setValidators([Validators.required]);
    fm.controls["supplierType"].updateValueAndValidity();
    this.sharedService.validateForm(fm);
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.saveSubscription(saveOnly);
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  saveSubscription(saveOnly: boolean): void {
    this.checkSupplier(saveOnly);
  }

  saveSupplierMaster(saveOnly: boolean): void {
    this.serviceDocument.dataProfile.profileForm.controls["listOfAddress"] = new FormControl();
    this.serviceDocument.dataProfile.profileForm.controls["listOfAddress"].setValue(this.buildAddressObject());
    this.serviceDocument.dataProfile.profileForm.controls["rebateHead"] = new FormControl();
    this.serviceDocument.dataProfile.profileForm.controls["rebateHead"].setValue(this.buildProgressiveRebateObject());
    this.saveServiceSubscription = this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.supplierID = this.service.serviceDocument.dataProfile.dataModel.supplier;
        this.saveSuccessSubscription = this.sharedService.saveForm(
          `${this.localizationData.supplierMaster.suppliermastersave} - Supplier ID ${this.supplierID}`).subscribe(() => {
            if (saveOnly) {
              this.router.navigate(["/SupplierMaster/List"], { skipLocationChange: true });
            } else {
              this.router.navigate(["/Blank"], {
                skipLocationChange: true
                , queryParams: { id: "/SupplierMaster/New/" + this.supplierID }
              });
            }
          }, (error: string) => { console.log(error); });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
    }, (error: string) => { console.log(error); });

  }

  submitAction(): void {
    this.loaderService.display(true);
    this.submitServiceSubscription = this.service.submit().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.supplierID = this.service.serviceDocument.dataProfile.dataModel.supplier;
        this.submitSuccessSubscription = this.sharedService.saveForm(
          `Submitted - Supplier ID ${this.supplierID}`).subscribe(() => {
            this.router.navigate(["/SupplierMaster/List"], { skipLocationChange: true });
          }, (error: string) => {
            console.log(error);
            this.loaderService.display(false);
          });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
        this.loaderService.display(false);
      }
    }, (error: string) => {
      console.log(error);
      this.loaderService.display(false);
    });
  }

  reset(): void {
    if (!this.editMode) {
      this.newModel();
      this.service.newModel(this.model);
    } else {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    }
    this.serviceDocument = this.service.serviceDocument;
    this.mapAddressFields();
    this.mapProgressiveRebateFields();
    this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
    this.serviceDocument.dataProfile.profileForm.controls["contactEmail1"].setValidators(
      this.sharedService.emailValidator("Invalid Email Address"));
    this.serviceDocument.dataProfile.profileForm.controls["contactEmail2"].setValidators(
      this.sharedService.emailValidator("Invalid Email Address"));
  }

  updateDependentControlStatus(event: any, mainControl: string): void {
    if (this.serviceDocument.dataProfile.profileForm.controls[mainControl].value === "N") {
      this.validationMetaObj.find(x => x.controlName === mainControl).dependentControlList.forEach(x => {
        this.serviceDocument.dataProfile.profileForm.controls[x].setValue(null);
        this.serviceDocument.dataProfile.profileForm.controls[x].disable();
      });
    } else {
      this.validationMetaObj.find(x => x.controlName === mainControl).dependentControlList.forEach(x => {
        this.serviceDocument.dataProfile.profileForm.controls[x].enable();
      });
    }
  }

  checkSupplier(saveOnly: boolean): void {
    if (!this.editMode) {
      this.service.isExistingSupplier(this.serviceDocument.dataProfile.profileForm.controls["supplier"].value).subscribe((response: boolean) => {
        if (response) {
          this.sharedService.errorForm(this.localizationData.supplierMaster.suppliermastercheck);
        } else {
          this.saveSupplierMaster(saveOnly);
        }
      });
    } else {
      this.saveSupplierMaster(saveOnly);
    }
  }

  openSuppAddrMaster($event: MouseEvent): void {
    $event.preventDefault();
    this.suppAddrModel = {
      supplierId: this.service.serviceDocument.dataProfile.dataModel.supplier, addressType: null, sequenceNo: null,
      contactName: null, contactPhone: null, contactFax: null, contactTelex: null, contactEmail: null, oracleVendorSiteID: null,
      addressLine1: null, addressLine2: null, addressLine3: null, city: null, state: null, countryID: null, postalCode: null,
      ediAddressChg: null, publishIndicator: null, country: null, operation: null, tableName: null, primaryAddressIndicator: null
    };
    this.sharedService.searchData = Object.assign({}, this.suppAddrModel);
    this.router.navigate(["/SupplierAddressMaster"], { skipLocationChange: true });
  }

  public tabOnClick(eventData: number): void {
    let selectedTabIndex: number = (eventData !== null) ? eventData : 0;
    if (selectedTabIndex === 4) {
      this.fetchSupplierDeals();
    }
  }

  changeSupplierType(event: any): void {
    this.serviceDocument.dataProfile.profileForm.controls["currencyCode"].setValue(null);
    if (this.serviceDocument.dataProfile.profileForm.controls["supplierType"].value === "LC") {
      this.serviceDocument.dataProfile.profileForm.controls["currencyCode"].setValue("KWD");
    }
  }

  mapAddressFields(): void {
    if (this.serviceDocument.dataProfile.dataModel.listOfAddress != null &&
      this.serviceDocument.dataProfile.dataModel.listOfAddress.length > 0) {
      let addr1Obj: SupplierAddressMasterModel[] = this.serviceDocument.dataProfile.dataModel.listOfAddress.
        filter(x => x.primaryAddressIndicator === "Y");
      let addr2Obj: SupplierAddressMasterModel[] = this.serviceDocument.dataProfile.dataModel.listOfAddress.
        filter(x => x.primaryAddressIndicator === "N");
      if (addr1Obj != null && addr1Obj.length > 0) {
        this.service.serviceDocument.dataProfile.profileForm.patchValue({
          address1AddressType: addr1Obj[0].addressType, address1AddressLine1: addr1Obj[0].addressLine1,
          address1City: addr1Obj[0].city, address1State: addr1Obj[0].state,
          address1CountryID: addr1Obj[0].countryID, address1PostalCode: addr1Obj[0].postalCode, sequenceNo: 1
        });
      }
      if (addr2Obj != null && addr2Obj.length > 0) {
        this.service.serviceDocument.dataProfile.profileForm.patchValue({
          address2AddressType: addr2Obj[0].addressType, address2AddressLine1: addr2Obj[0].addressLine1,
          address2City: addr2Obj[0].city, address2State: addr2Obj[0].state,
          address2CountryID: addr2Obj[0].countryID, address2PostalCode: addr2Obj[0].postalCode, sequenceNo: 2
        });
        this.updateAddress2 = true;
      }
    }
  }

  buildAddressObject(): SupplierAddressMasterModel[] {
    let addressList: SupplierAddressMasterModel[] = [];
    let addressObj: SupplierAddressMasterModel;
    addressObj = {
      supplierId: this.serviceDocument.dataProfile.profileForm.controls["supplier"].value,
      addressType: null, primaryAddressIndicator: "Y",
      addressLine1: this.serviceDocument.dataProfile.profileForm.controls["address1AddressLine1"].value,
      city: this.serviceDocument.dataProfile.profileForm.controls["address1City"].value,
      state: this.serviceDocument.dataProfile.profileForm.controls["address1State"].value,
      countryID: this.serviceDocument.dataProfile.profileForm.controls["address1CountryID"].value,
      postalCode: this.serviceDocument.dataProfile.profileForm.controls["address1PostalCode"].value,
      operation: this.editMode === true ? "U" : "I",
      sequenceNo: 1,
      contactName: null, contactPhone: null, contactFax: null, contactEmail: null, contactTelex: null,
      addressLine2: null, addressLine3: null, ediAddressChg: null, country: null, publishIndicator: null, tableName: null
    };
    addressList.push(addressObj);
    if (this.checkFieldsFilled(this.address2Props)) {
      addressObj = {
        supplierId: this.serviceDocument.dataProfile.profileForm.controls["supplier"].value,
        addressType: null, primaryAddressIndicator: "N",
        addressLine1: this.serviceDocument.dataProfile.profileForm.controls["address2AddressLine1"].value,
        city: this.serviceDocument.dataProfile.profileForm.controls["address2City"].value,
        state: this.serviceDocument.dataProfile.profileForm.controls["address2State"].value,
        countryID: this.serviceDocument.dataProfile.profileForm.controls["address2CountryID"].value,
        postalCode: this.serviceDocument.dataProfile.profileForm.controls["address2PostalCode"].value,
        operation: this.updateAddress2 === true ? "U" : "I",
        sequenceNo: 2,
        contactName: null, contactPhone: null, contactFax: null, contactEmail: null, contactTelex: null,
        addressLine2: null, addressLine3: null, ediAddressChg: null, country: null, publishIndicator: null, tableName: null
      };
      addressList.push(addressObj);
    }
    return addressList;
  }

  checkFieldsFilled(fieldList: string[]): boolean {
    let isFilled: boolean = false;
    for (let i: number = 0; i < fieldList.length; i++) {
      let controlName: string = fieldList[i];
      if (this.serviceDocument.dataProfile.profileForm.controls[controlName].value) {
        isFilled = true;
        break;
      }
    }
    return isFilled;
  }

  buildProgressiveRebateObject(): SupplierRebateHeadModel {
    let supplierRebateHeadModel: SupplierRebateHeadModel = {
      rebateId: this.serviceDocument.dataProfile.dataModel.rebateHead ? this.serviceDocument.dataProfile.dataModel.rebateHead.rebateId : null
      , supplierId: this.editMode === true ? this.supplierID : this.serviceDocument.dataProfile.profileForm.controls["supplier"].value
      , rebateType: null, defaultRebatePercent: null, rebateStatus: "A"
      , operation: this.serviceDocument.dataProfile.dataModel.rebateHead ? "U" : "I"
    };
    supplierRebateHeadModel.rebateDetails = [];
    let rebateDetail: SupplierRebateDetailModel;
    if (this.checkFieldsFilled(this.slab1Props)) {
      rebateDetail = {
        rebateDetailId: 1
        , rebateId: this.serviceDocument.dataProfile.dataModel.rebateHead ? this.serviceDocument.dataProfile.dataModel.rebateHead.rebateId : null
        , minThresholdLimit: this.serviceDocument.dataProfile.profileForm.controls["slab1GrowthMinThreshold"].value
        , maxThresholdLimit: this.serviceDocument.dataProfile.profileForm.controls["slab1GrowthMaxThreshold"].value
        , rebatePercent: this.serviceDocument.dataProfile.profileForm.controls["slab1Rebate"].value
        , growthPercent: null, rebateDetailStatus: "A", rebateStartDate: null, rebateEndDate: null
        , operation: this.updateSlab1 === true ? "U" : "I"
      };
      supplierRebateHeadModel.rebateDetails.push(rebateDetail);
    }
    if (this.checkFieldsFilled(this.slab2Props)) {
      rebateDetail = {
        rebateDetailId: 2
        , rebateId: this.serviceDocument.dataProfile.dataModel.rebateHead ? this.serviceDocument.dataProfile.dataModel.rebateHead.rebateId : null
        , minThresholdLimit: this.serviceDocument.dataProfile.profileForm.controls["slab2GrowthMinThreshold"].value
        , maxThresholdLimit: this.serviceDocument.dataProfile.profileForm.controls["slab2GrowthMaxThreshold"].value
        , rebatePercent: this.serviceDocument.dataProfile.profileForm.controls["slab2Rebate"].value
        , growthPercent: null, rebateDetailStatus: "A", rebateStartDate: null, rebateEndDate: null
        , operation: this.updateSlab2 === true ? "U" : "I"
      };
      supplierRebateHeadModel.rebateDetails.push(rebateDetail);
    }
    if (this.checkFieldsFilled(this.slab3Props)) {
      rebateDetail = {
        rebateDetailId: 3
        , rebateId: this.serviceDocument.dataProfile.dataModel.rebateHead ? this.serviceDocument.dataProfile.dataModel.rebateHead.rebateId : null
        , minThresholdLimit: this.serviceDocument.dataProfile.profileForm.controls["slab3GrowthMinThreshold"].value
        , maxThresholdLimit: this.serviceDocument.dataProfile.profileForm.controls["slab3GrowthMaxThreshold"].value
        , rebatePercent: this.serviceDocument.dataProfile.profileForm.controls["slab3Rebate"].value
        , growthPercent: null, rebateDetailStatus: "A", rebateStartDate: null, rebateEndDate: null
        , operation: this.updateSlab3 === true ? "U" : "I"
      };
      supplierRebateHeadModel.rebateDetails.push(rebateDetail);
    }
    if (this.checkFieldsFilled(this.slab4Props)) {
      rebateDetail = {
        rebateDetailId: 4
        , rebateId: this.serviceDocument.dataProfile.dataModel.rebateHead ? this.serviceDocument.dataProfile.dataModel.rebateHead.rebateId : null
        , minThresholdLimit: this.serviceDocument.dataProfile.profileForm.controls["slab4GrowthMinThreshold"].value
        , maxThresholdLimit: this.serviceDocument.dataProfile.profileForm.controls["slab4GrowthMaxThreshold"].value
        , rebatePercent: this.serviceDocument.dataProfile.profileForm.controls["slab4Rebate"].value
        , growthPercent: null, rebateDetailStatus: "A", rebateStartDate: null, rebateEndDate: null
        , operation: this.updateSlab4 === true ? "U" : "I"
      };
      supplierRebateHeadModel.rebateDetails.push(rebateDetail);
    }
    if (this.checkFieldsFilled(this.slab5Props)) {
      rebateDetail = {
        rebateDetailId: 5
        , rebateId: this.serviceDocument.dataProfile.dataModel.rebateHead ? this.serviceDocument.dataProfile.dataModel.rebateHead.rebateId : null
        , minThresholdLimit: this.serviceDocument.dataProfile.profileForm.controls["slab5GrowthMinThreshold"].value
        , maxThresholdLimit: this.serviceDocument.dataProfile.profileForm.controls["slab5GrowthMaxThreshold"].value
        , rebatePercent: this.serviceDocument.dataProfile.profileForm.controls["slab5Rebate"].value
        , growthPercent: null, rebateDetailStatus: "A", rebateStartDate: null, rebateEndDate: null
        , operation: this.updateSlab5 === true ? "U" : "I"
      };
      supplierRebateHeadModel.rebateDetails.push(rebateDetail);
    }
    return supplierRebateHeadModel;
  }

  mapProgressiveRebateFields(): void {
    if (this.serviceDocument.dataProfile.dataModel.rebateHead &&
      this.serviceDocument.dataProfile.dataModel.rebateHead.rebateDetails != null &&
      this.serviceDocument.dataProfile.dataModel.rebateHead.rebateDetails.length > 0) {

      let slab1Obj: SupplierRebateDetailModel[] = this.serviceDocument.dataProfile.dataModel.rebateHead.rebateDetails
        .filter(x => x.rebateDetailId === 1);
      let slab2Obj: SupplierRebateDetailModel[] = this.serviceDocument.dataProfile.dataModel.rebateHead.rebateDetails
        .filter(x => x.rebateDetailId === 2);
      let slab3Obj: SupplierRebateDetailModel[] = this.serviceDocument.dataProfile.dataModel.rebateHead.rebateDetails
        .filter(x => x.rebateDetailId === 3);
      let slab4Obj: SupplierRebateDetailModel[] = this.serviceDocument.dataProfile.dataModel.rebateHead.rebateDetails
        .filter(x => x.rebateDetailId === 4);
      let slab5Obj: SupplierRebateDetailModel[] = this.serviceDocument.dataProfile.dataModel.rebateHead.rebateDetails
        .filter(x => x.rebateDetailId === 5);

      if (slab1Obj !== null && slab1Obj.length > 0) {
        this.service.serviceDocument.dataProfile.profileForm.patchValue({
          slab1GrowthMinThreshold: slab1Obj[0].minThresholdLimit,
          slab1GrowthMaxThreshold: slab1Obj[0].maxThresholdLimit,
          slab1Rebate: slab1Obj[0].rebatePercent
        });
        this.updateSlab1 = true;
      }
      if (slab2Obj !== null && slab2Obj.length > 0) {
        this.service.serviceDocument.dataProfile.profileForm.patchValue({
          slab2GrowthMinThreshold: slab2Obj[0].minThresholdLimit,
          slab2GrowthMaxThreshold: slab2Obj[0].maxThresholdLimit,
          slab2Rebate: slab2Obj[0].rebatePercent
        });
        this.updateSlab2 = true;
      }
      if (slab3Obj !== null && slab3Obj.length > 0) {
        this.service.serviceDocument.dataProfile.profileForm.patchValue({
          slab3GrowthMinThreshold: slab3Obj[0].minThresholdLimit,
          slab3GrowthMaxThreshold: slab3Obj[0].maxThresholdLimit,
          slab3Rebate: slab3Obj[0].rebatePercent
        });
        this.updateSlab3 = true;
      }
      if (slab4Obj !== null && slab4Obj.length > 0) {
        this.service.serviceDocument.dataProfile.profileForm.patchValue({
          slab4GrowthMinThreshold: slab4Obj[0].minThresholdLimit,
          slab4GrowthMaxThreshold: slab4Obj[0].maxThresholdLimit,
          slab4Rebate: slab4Obj[0].rebatePercent
        });
        this.updateSlab4 = true;
      }
      if (slab5Obj !== null && slab5Obj.length > 0) {
        this.service.serviceDocument.dataProfile.profileForm.patchValue({
          slab5GrowthMinThreshold: slab5Obj[0].minThresholdLimit,
          slab5GrowthMaxThreshold: slab5Obj[0].maxThresholdLimit,
          slab5Rebate: slab5Obj[0].rebatePercent
        });
        this.updateSlab5 = true;
      }
    }
  }

  newModel(): void {
    this.model = {
      supplier: null, supplierName: null, supplierStatus: "A", supplierStatusDesc: null, supplierSecondaryName: null
      , costChgPercentageVar: null, costChgAmountVar: null
      , bracketCostingIndicator: "N", prePayInvoiceIndicator: "N", backOrderIndicator: "N"
      , autoApproveDbtMemoIndicator: "N", autoApproveInvoiceIndicator: "N", supplierRetailFlag: null
      , ediPoChange: "N", ediPoConfirm: "N", ediAsn: "N", ediSalesRepeatFrequency: "D"
      , ediSupportAvailableIndicator: "N", ediContractIndicator: "N", ediInvoiceIndicator: "N"
      , servicePerfReqIndicator: "N", finalDstIndicator: "N", defaultItemLeadTime: null
      , invManagementLevel: "A", preMarkIndicator: "N", handlingPercentage: null
      , languageId: null, languageDesc: null, contactName1: null, contactPhone: null, contactFax: null, contactPager: null
      , contactEmail1: null, qcIndicator: null, qcPercentage: null, qcFrequency: null
      , vcIndicator: null, vcPercentage: null, vcFrequency: null, currencyCode: null
      , vatExempt: null, vatRegistrationId: null, vatRegion: null, paymentMode: null
      , allowRebate: null, rebatePeriod: null, rebateDiscount: null, displayDiscount: null
      , dbtMemoCode: null, settlementCode: null
      , invoicePayLocation: null, invoiceRecieveLocation: null, addInvoiceGrossNet: null
      , allowOverDue: null, ediPoIndicator: null, freightChargeIndicator: null, paymentTerms: null
      , freightTerms: null, ofTransferFlag: null, ofEnableTransferFlag: null
      , shipMethod: null, deliveryPolicy: null, dunsNumber: null, dunsLocation: null, dsdIndicator: null
      , retAllowIndicator: null, retAuthorizationRequired: null
      , retMinimumDolAmount: null, retailCourier: null, replenApprovalIndicator: null
      , vmiOrderStatus: null, commentDescription: null, operation: "I", tableName: null
      , addressType: null, sequenceNo: null, primaryAddressIndicator: null, addressLine1: null
      , addressLine2: null, addressLine3: null, city: null, state: null, countryID: null, postalCode: null
      , minServiceLevel: null, supplierType: null, dcIndicator: null
      , accountNumberType: null, bankName: null, bankAddress: null, bankDetails: null
      , supplierGroup: null, supplierTier: null, listOfAddress: []
      , address1AddressType: null, address1AddressLine1: null, address1City: null, address1State: null, address1CountryID: null, address1PostalCode: null
      , address2AddressType: null, address2AddressLine1: null, address2City: null, address2State: null, address2CountryID: null, address2PostalCode: null
      , newStoreOpeningFee: null, expectedAnnualVol: null, accountOpeningFee: null, listingFee: null
      , contactEmail2: null, contactName2: null, contactTitle1: null, contactTitle2: null, isExpiry: null, others: null
      , barCodePrintCharge: null, barCodePrintingFlag: null, costHideInvoice: null, discountHideInvoice: null, paymentTermType: null
      , slab1Rebate: null, slab2Rebate: null, slab3Rebate: null, slab4Rebate: null, slab5Rebate: null, rebateHead: null
      , slab1GrowthMinThreshold: null, slab1GrowthMaxThreshold: null, slab2GrowthMinThreshold: null, slab2GrowthMaxThreshold: null
      , slab3GrowthMinThreshold: null, slab3GrowthMaxThreshold: null, slab4GrowthMinThreshold: null, slab4GrowthMaxThreshold: null
      , slab5GrowthMinThreshold: null, slab5GrowthMaxThreshold: null, createdBy: null, createdDate: null, workflowInstance: null
    };
  }

  barCodePrintingFlagChanged(): void {
    if (this.serviceDocument.dataProfile.profileForm.controls["barCodePrintingFlag"].value === "Y") {
      this.serviceDocument.dataProfile.profileForm.controls["barCodePrintCharge"].enable();
    } else {
      this.serviceDocument.dataProfile.profileForm.controls["barCodePrintCharge"].disable();
    }
  }

  growthValidation(event: any, slabId: string): void {
    let slabValidnObj: any = this.rebateValidationObj.slabValidationData.find(x => x.key === slabId);
    this.serviceDocument.dataProfile.profileForm.controls[slabValidnObj.value.next].setValidators(
      this.sharedService.numberValidator(slabValidnObj.value.validationMsg,
        +(this.serviceDocument.dataProfile.profileForm.controls[slabValidnObj.value.current].value)));
    this.serviceDocument.dataProfile.profileForm.controls[slabValidnObj.value.next].updateValueAndValidity();
  }

  fetchSupplierDeals(): void {
    this.loaderService.display(true);
    this.supplierDealServiceSubscription = this.supplierDealService.dealSearch(this.serviceDocument.dataProfile.dataModel.supplier)
      .subscribe((response: DealSearchNewModel[]) => {
        if (response != null && response.length > 0) {
          this.dataList = response;
        }
        this.loaderService.display(false);
      },
        (err: any) => { this.sharedService.errorForm(err); });
  }

  open(cell: any, mode: string): void {
    this.loaderService.display(true);
    if (mode === "edit") {
      this.sharedService.editMode = true;
      this.supplierDealService.dealDomainData(cell.data.deaL_ID)
        .subscribe((response: DealDomainModel) => {
          if (response != null) {
            let data: any = {
              "DealId": cell.data.deaL_ID, "SupplierId": this.supplierID
              , "SupplierName": this.serviceDocument.dataProfile.dataModel.supplierName
              , "CurrencyCode": this.serviceDocument.dataProfile.profileForm.controls["currencyCode"].value
              , "SupplierDeal": response
            };

            this.supplierDealPopupSubscription = this.sharedService.openDilogForFindItems(SupplierDealPopupComponent, data, "Deal")
              .subscribe((message: string) => {
                if (message === "reloadDealGrid") {
                  this.fetchSupplierDeals();
                  this.changeRef.detectChanges();
                }
              });
            this.loaderService.display(false);
          }
        },
          (err: any) => { this.sharedService.errorForm(err); });
    } else {
      this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl + cell.data.deaL_ID, this.dataList);
    }
  }

  add($event: MouseEvent): void {
    $event.preventDefault();
    this.supplierDealService.dealDomainData(0)
      .subscribe((response: DealDomainModel) => {
        if (response != null) {
          let data: any = {
            "DealId": 0, "SupplierId": this.supplierID, "SupplierName": this.serviceDocument.dataProfile.dataModel.supplierName
            , "CurrencyCode": this.serviceDocument.dataProfile.profileForm.controls["currencyCode"].value, "SupplierDeal": response
          };
          this.supplierDealPopupSubscription = this.sharedService.openDilogForFindItems(SupplierDealPopupComponent, data, "Deal")
            .subscribe((message: string) => {
              if (message === "reloadDealGrid") {
                this.fetchSupplierDeals();
                this.changeRef.detectChanges();
              }
            });
        }
      },
        (err: any) => { this.sharedService.errorForm(err); });
  }

  setDealGridColumnsWithOptions(): void {
    this.columns = [
      { headerName: "Deal Id", field: "deaL_ID", tooltipField: "deaL_ID", width: 60 },
     // { headerName: "Deal Description", field: "dealDescription", tooltipField: "dealDescription", maxWidth: 175 },
      { headerName: "Deal Type", field: "dealType", tooltipField: "dealType", width: 120 },
      {
        headerName: "Effective From", field: "fromDate", tooltipField: "fromDate | date:'dd-MMM - yyyy'"
        , cellRendererFramework: GridDateComponent, width: 120
      },
      {
        headerName: "Effective To", field: "toDate", tooltipField: "toDate | date:'dd-MMM - yyyy'"
        , cellRendererFramework: GridDateComponent, width: 120
      },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 60, suppressSorting: true
      }
    ];
    this.gridOptions = {
      onGridReady: (params: GridReadyEvent) => {
        this.gridApi = params.api;
      },
      context: {
        componentParent: this
      }
    };
  }

  viewWorkflowHistoryFromSupplier($event: MouseEvent): void {
    $event.preventDefault();
    this.workflowHistoryServiceSubscription = this.inboxService.fetchWorkflowHistory(this.objectName, this.supplierID).subscribe(() => {

      this.workflowHistoryListObj = this.inboxService.serviceDocumentWorkflowHistory.dataProfile.dataList;

      this.sharedService.openPopupWithDataList(
        WorkflowHistoryPopupComponent,
        this.workflowHistoryListObj,
        "Inbox Workflow",
        this.objectName,
        this.supplierID);
    });
  }

  ngOnDestroy(): void {
    this.routeServiceSubscription.unsubscribe();
    if (this.supplierDealPopupSubscription) {
      this.supplierDealPopupSubscription.unsubscribe();
    }
    if (this.supplierDealServiceSubscription) {
      this.supplierDealServiceSubscription.unsubscribe();
    }
    if (this.saveServiceSubscription) {
      this.saveServiceSubscription.unsubscribe();
    }
    if (this.saveSuccessSubscription) {
      this.saveSuccessSubscription.unsubscribe();
    }
    if (this.submitServiceSubscription) {
      this.submitServiceSubscription.unsubscribe();
    }
    if (this.submitSuccessSubscription) {
      this.submitSuccessSubscription.unsubscribe();
    }
    if (this.workflowHistoryServiceSubscription) {
      this.workflowHistoryServiceSubscription.unsubscribe();
    }
  }
}
