import { DomainDetailModel } from '../../domain/DomainDetailModel';
import { DealCompTypeDomainModel } from '../../Common/DomainData/DealCompTypeDomainModel';
import { GroupDomainModel } from '../../Common/DomainData/GroupDomainModel';
import { DepartmentDomainModel } from '../../Common/DomainData/DepartmentDomainModel';
import { SupplierDealModel } from './SupplierDealModel';
import { CommonModel } from '../../Common/CommonModel';

export class DealDomainModel {
  dealTypes?: DomainDetailModel[];
  dealCompTypes?: DealCompTypeDomainModel[];
  merchLevels?: DomainDetailModel[];
  divisions?: CommonModel[];
  groups?: CommonModel[];
  departments?: CommonModel[];
  discountTypes?: DomainDetailModel[];
  supplierDeal?: SupplierDealModel;
}
