import { DealItemLocNewModel } from "./DealItemLocNewModel";

export class SupplierDealModel {
  deaL_ID: number;
  supplier?: number;
  dealType: string;
  dealCompType: string;
  startDate?: Date;
  closeDate?: Date;
  operation?: string;
  supplierName?: string;
  currencyCode?: string;
  listOfDealItemLocations?: DealItemLocNewModel[];
}
