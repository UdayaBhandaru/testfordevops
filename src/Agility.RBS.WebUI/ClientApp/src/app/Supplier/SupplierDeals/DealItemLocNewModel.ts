export class DealItemLocNewModel {
  dealId: number;
  dealDetailId: number;
  merchLevel: string;
  merchLevelDesc: string;
  merchLevelValue?: number;
  merchLevelValueDesc: string;
  discountType: string;
  discountValue: number;
  item: string;
  itemDesc: string;
  effectiveStartDate?: Date;
  effectiveEndDate?: Date;
  operation?: string;
  dealType?: string;
  dealDescription?: string;
  oldCost?: number;
  newCost?: number;
  headStartDate?: Date;
  headEndDate?: Date;
  comments: string;
}
