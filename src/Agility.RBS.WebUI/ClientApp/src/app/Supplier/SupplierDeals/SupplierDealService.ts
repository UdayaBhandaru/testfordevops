import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpParams } from "@angular/common/http";
import { ServiceDocument, WorkflowAction } from "@agility/frameworkcore";
import { SupplierDealModel } from "./SupplierDealModel";
import { SharedService } from "../../Common/SharedService";
import { DealGridModel } from "./DealGridModel";
import { HttpHelperService } from "../../Common/HttpHelperService";
import { DealDomainModel } from './DealDomainModel';

@Injectable()
export class SupplierDealService {
  serviceDocument: ServiceDocument<SupplierDealModel> = new ServiceDocument<SupplierDealModel>();
  dealMasterPaging: {
    startRow: number,
    pageSize: number,
    cacheSize: number,
  } = {
      startRow: 0,
      pageSize: 10,
      cacheSize: 100,
    };

  constructor(private sharedService: SharedService, private httpHelperService: HttpHelperService) { }

  dealSearch(supplierId: number): Observable<any[]> {
    return this.httpHelperService.get("/api/SupplierDeals/GetSupplierDealList", new HttpParams().set("supplierId", supplierId.toString()));
  }

  dealDomainData(id: number): Observable<DealDomainModel> {
    return this.httpHelperService.get("/api/SupplierDeals/DealDomainData", new HttpParams().set("id", id.toString()));
  }

  dealGet(id: number): Observable<SupplierDealModel> {
    return this.httpHelperService.get("/api/SupplierDeals/DealGet", new HttpParams().set("id", id.toString()));
  }

  addEdit(id: number): Observable<ServiceDocument<SupplierDealModel>> {
    if (+id === 0) {
      return this.serviceDocument.new("/api/SupplierDeals/New");
    } else {
      return this.serviceDocument.open("/api/SupplierDeals/Open", new HttpParams().set("id", id.toString()));
    }
  }

  save(): Observable<ServiceDocument<SupplierDealModel>> {
    return this.serviceDocument.save("/api/SupplierDeals/Save", true, () => this.setDate());
  }

  saveDeal(model: SupplierDealModel): Observable<boolean> {
    return this.httpHelperService.post("/api/SupplierDeals/DealSave", model);
  }

  newModel(model: SupplierDealModel): ServiceDocument<SupplierDealModel> {
    this.serviceDocument.newModel(model);
    return this.serviceDocument;
  }

  getItems(apiUrl: string): Observable<any[]> {
    return this.httpHelperService.get(apiUrl);
  }

  setDate(): void {
    let sd: SupplierDealModel = this.serviceDocument.dataProfile.dataModel;
    if (sd !== null) {
      if (sd.startDate != null) {
        sd.startDate = this.sharedService.setOffSet(sd.startDate);
      }

      if (sd.closeDate != null) {
        sd.closeDate = this.sharedService.setOffSet(sd.closeDate);
      }

      sd.listOfDealItemLocations.forEach(x => {
        if (x.effectiveStartDate != null) {
          x.effectiveStartDate = this.sharedService.setOffSet(x.effectiveStartDate);
        }
        if (x.effectiveEndDate != null) {
          x.effectiveEndDate = this.sharedService.setOffSet(x.effectiveEndDate);
        }
      });
    }
  }
}
