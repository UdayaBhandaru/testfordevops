﻿export class DealSearchNewModel {
    supplier?: number;
    supplierName: string;
    deaL_ID?: number;
    dealDescription: string;
    dealTypeId?: string;
    dealType: string;
    fromDate?: Date;
    toDate?: Date;
    operation: string;
    tableName: string;
    startRow?: number;
    endRow?: number;
    totalRows?: number;
    sortModel?: {}[];
}