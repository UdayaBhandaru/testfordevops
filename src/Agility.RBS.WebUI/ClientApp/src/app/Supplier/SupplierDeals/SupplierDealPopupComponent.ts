import { Component, OnInit, OnDestroy, Inject, ChangeDetectorRef } from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";
import { Router } from "@angular/router";
import { ServiceDocument, CommonService, FormMode, MessageType, AjaxModel, AjaxResult } from "@agility/frameworkcore";
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from "@angular/material";
import { GridOptions, GridReadyEvent, ColDef, RowNode, GridApi, ColumnApi } from "ag-grid-community";
import { SupplierDealModel } from "./SupplierDealModel";
import { SupplierDealService } from "./SupplierDealService";
import { DealItemLocNewModel } from "./DealItemLocNewModel";
import { DomainDetailModel } from "../../domain/DomainDetailModel";
import { CommonModel } from "../../Common/CommonModel";
import { GridGetItemComponent } from "../../Common/grid/GridGetItemComponent";
import { GridSelectComponent } from "../../Common/grid/GridSelectComponent";
import { SharedService } from "../../Common/SharedService";
import { LoaderService } from "../../Common/LoaderService";
import { Subscription } from "rxjs";
import { GridMerchLvlValComponent } from "../../Common/grid/GridMerchLvlValComponent";
import { ItemSelectService } from "../../Common/ItemSelectService";
import { GridNumericComponent } from "../../Common/grid/GridNumericComponent";
import { BrandDomainModel } from "../../Common/DomainData/BrandDomainModel";
import { DealCompTypeDomainModel } from '../../Common/DomainData/DealCompTypeDomainModel';
import { CostChangeItemGroupsModel } from '../../CostChange/CostChangeItemGroupsModel';
import { GridEditComponent } from '../../Common/grid/GridEditComponent';
import { DealDomainModel } from './DealDomainModel';
import { GridInputComponent } from '../../Common/grid/GridInputComponent';

@Component({
  selector: "supplier-Deal",
  templateUrl: "./SupplierDealPopupComponent.html",
  styleUrls: ['./SupplierDealPopupComponent.scss'],
  providers: [
    SupplierDealService,
    ItemSelectService
  ]
})

export class SupplierDealPopupComponent implements OnInit {
  infoGroup: FormGroup;
  public serviceDocument: ServiceDocument<SupplierDealModel>;
  dealItemLocModel: DealItemLocNewModel;
  dataList: DealItemLocNewModel[] = [];
  PageTitle: string = "Deal";
  objectValue: string;
  objectName: string;
  componentName: any;
  columns: {}[];
  gridOptions: GridOptions;
  redirectUrl: string;
  parentData: any[];
  dealId: number;
  supplierId: number;
  supplierName: string;
  currencyCode: string;
  dealTypeData: DomainDetailModel[];
  dealCompTypes: DealCompTypeDomainModel[];
  merchLevelData: DomainDetailModel[];
  discountTypeData: DomainDetailModel[];
  divisionData: CommonModel[];
  departmentData: CommonModel[];
  categoryData: CommonModel[];
  brandsData: BrandDomainModel[];
  merchLevelValueData: CommonModel[];
  saveServiceSubscription: Subscription;
  saveSuccessSubscription: Subscription;
  searchServiceSubscription: Subscription;
  localizationData: any;
  deletedRows: any[] = [];
  gridApi: GridApi;
  gridColumnApi: ColumnApi;
  findItemsApiUrl = "/api/CostManagement/CostChangeItemGetForParentChildItems";
  editMode: boolean = false;
  effectiveMinDate: Date;
  message: string;
  mandatoryDealLocFields: string[] = [
    "merchLevel"
  ];
  model: SupplierDealModel;

  constructor(private service: SupplierDealService, private _commonService: CommonService, public dialog: MatDialog
    , private _router: Router, @Inject(MAT_DIALOG_DATA) public data: any, public dialogRef: MatDialogRef<any>
    , private sharedService: SharedService, private loaderService: LoaderService
    , private itemSelectService: ItemSelectService, private changeRef: ChangeDetectorRef
  ) {
    this.parentData = data;
    this.dealId = data.DealId;
    this.supplierId = data.SupplierId;
    this.supplierName = data.SupplierName;
    this.currencyCode = data.CurrencyCode;

    this.dealTypeData = data.SupplierDeal.dealTypes;
    this.dealCompTypes = data.SupplierDeal.dealCompTypes;
    this.merchLevelData = data.SupplierDeal.merchLevels;
    this.discountTypeData = data.SupplierDeal.discountTypes;
    this.divisionData = data.SupplierDeal.divisions;
    this.departmentData = data.SupplierDeal.groups;
    this.categoryData = data.SupplierDeal.departments;

    this.model = {
      deaL_ID: null, supplier: this.supplierId, supplierName: this.supplierName, dealType: null
      , dealCompType: null, startDate: null, closeDate: null, operation: "I", currencyCode: this.currencyCode
    };
    Object.assign(this.model, data.SupplierDeal.supplierDeal);
    this.infoGroup = this._commonService.getFormGroup(this.model, FormMode.Open);
  }

  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }

  ngOnInit(): void {
    this.bind();
  }

  bind(): void {
    this.setGridColumnsWithOptions();
    if (this.dealId.toString() !== "0") {
      this.editMode = true;
      this.PageTitle = `${this.PageTitle} - EDIT - ${this.dealId} (Supplier - ${this.supplierName})`;
      if (this.model.listOfDealItemLocations && this.model.listOfDealItemLocations.length > 0) {
        this.dataList = [];
        Object.assign(this.dataList, this.model.listOfDealItemLocations);
      }
      this.effectiveMinDate = this.model.startDate;
    } else {
      this.effectiveMinDate = new Date();
      this.PageTitle = `${this.PageTitle} - ADD (Supplier - ${this.supplierName})`;
    }
  }

  setGridColumnsWithOptions(): void {
    this.columns = [
      {
        headerName: "Merch Level", field: "merchLevel", cellRendererFramework: GridSelectComponent
        , cellRendererParams: { references: this.merchLevelData }, tooltipField: "programMessage"
        , cellClassRules: {
          "invalid-grid-row": function (params: any): any { return !params.value; }
        }
      },
      {
        headerName: "Merch Level Value", field: "merchLevelValue", cellRendererFramework: GridMerchLvlValComponent
        , cellRendererParams: {
          deptData: this.departmentData, ctgData: this.categoryData, divisionData: this.divisionData
          , keyvaluepair: { key: "id", value: "description" }

        }, tooltipField: "programMessage"
        , cellClassRules: {
          "invalid-grid-row": function (params: any): any { return !params.value; }
        }
      },
      {
        headerName: "Item", field: "item", cellRendererFramework: GridGetItemComponent
        , cellClassRules: {
          "invalid-grid-row": function (params: any): any { return params.data.merchLevel === "12" && !params.value; }
        }
        , tooltipField: "programMessage"
      },
      { headerName: "Item Description", field: "itemDesc", tooltipField: "itemDesc" },
      { headerName: "Old Cost", field: "oldCost", tooltipField: "oldCost", width: 120 },
      {
        headerName: "New Cost", field: "newCost", tooltipField: "newCost"
        , cellRendererFramework: GridNumericComponent
        , cellClassRules: {
          "invalid-grid-row": function (params: any): any { return params.data.merchLevel === "12" && !params.value; }
        }, width: 120
      },
      {
        headerName: "Discount Type", field: "discountType", cellRendererFramework: GridSelectComponent
        , cellRendererParams: { references: this.discountTypeData }, tooltipField: "programMessage", width: 140
      },
      {
        headerName: "Disc Value", field: "discountValue", tooltipField: "discountValue"
        , cellRendererFramework: GridNumericComponent
        , cellClassRules: {
          "invalid-grid-row": function (params: any): any { return params.data.merchLevel !== "12" && !params.value; }
        }, width: 120
      },
      { headerName: "Comments", field: "comments", cellRendererFramework: GridInputComponent },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent, width: 150
        , cellRendererParams: { restrictEditIcon: true }
      }
    ];

    this.gridOptions = {
      onGridReady: (params: any) => {
        this.gridApi = params.api;
        this.gridApi.sizeColumnsToFit();
        this.gridColumnApi = params.columnApi;
      },
      context: {
        componentParent: this
      },
      suppressPaginationPanel: true,
      suppressScrollOnNewData: false,
      paginationPageSize: 10,
      pagination: true,
      enableSorting: true,
      rowHeight: 40,
      enableColResize: true,
      rowSelection: "multiple",
      suppressRowClickSelection: true,
      suppressCellSelection: true,
      groupUseEntireRow: true,
      groupDefaultExpanded: -1,
      domLayout: "autoHeight",
      toolPanelSuppressSideButtons: true,
      embedFullWidthRows: true
    };
  }

  select(cell: any, event: any): void {
    this.dataList[cell.rowIndex][cell.column.colId] = event.target.value;
    if (cell.column.colId === "merchLevel") {
      this.dataList[cell.rowIndex]["merchLevelValue"] = null;
      if (this.dataList[cell.rowIndex]["merchLevel"] === "12") {
        this.dataList[cell.rowIndex]["discountType"] = "A";
      } else {
        this.dataList[cell.rowIndex]["item"] = null;
        this.dataList[cell.rowIndex]["itemDesc"] = null;
        this.dataList[cell.rowIndex]["oldCost"] = null;
        this.dataList[cell.rowIndex]["newCost"] = null;
        this.dataList[cell.rowIndex]["discountType"] = "P";
        this.dataList[cell.rowIndex]["discountValue"] = null;
      }
    }
    let rowNode: RowNode = this.gridApi.getRowNode(cell.rowIndex);
    rowNode.setData(rowNode.data);
  }

  update(cell: any, event: any): void {
    this.dataList[cell.rowIndex][cell.column.colId] = event.target.value;
    if (cell.column.colId === "item") {
      this.itemBasedLogic(cell, event);
    } else if (cell.column.colId === "newCost") {
      let discountValue: number = null;
      let newCost: number = this.dataList[cell.rowIndex]["newCost"];
      let oldCost: number = this.dataList[cell.rowIndex]["oldCost"];
      if (oldCost && newCost && oldCost > newCost) {
        discountValue = +(oldCost - newCost).toPrecision(4);
      }
      this.dataList[cell.rowIndex]["discountValue"] = discountValue;
    }
    let rowNode: RowNode = this.gridApi.getRowNode(cell.rowIndex);
    rowNode.setData(rowNode.data);
  }

  itemBasedLogic(cell: any, event: any): void {
    if (this.dataList[cell.rowIndex]["item"]) {
      let itmGridDataList: DealItemLocNewModel[] = this.dataList ? this.dataList.filter(itm => (itm.item === this.dataList[cell.rowIndex]["item"]
        && itm.operation.toString() === this.dataList[cell.rowIndex]["operation"].toString()
      )) : null;
      if (itmGridDataList.length > 1) {
        this.sharedService.errorForm("Deal Item Location already exists");
      } else {
        this.infoGroup.patchValue({
          item: this.dataList[cell.rowIndex]["item"]
        });
        this.findItemsApiUrl = "/api/CostManagement/CostChangeItemGetForParentChildItems?itemNo=" + this.dataList[cell.rowIndex]["item"] + "&supplier=" + this.supplierId;
        this.searchServiceSubscription = this.itemSelectService.getItems(this.findItemsApiUrl)
          .subscribe((response: AjaxModel<CostChangeItemGroupsModel[]>) => {
            if (response.result === AjaxResult.success) {
              let dataModel: CostChangeItemGroupsModel = response.model[0];
              this.dataList[cell.rowIndex]["itemDesc"] = dataModel.itemDesc;
              this.dataList[cell.rowIndex]["oldCost"] = dataModel.unitCost;
              this.dataList[cell.rowIndex]["operation"] = "I";
              let rowNode: RowNode = this.gridApi.getRowNode(cell.rowIndex);
              rowNode.setData(rowNode.data);
            } else {
              this.sharedService.errorForm(response.message);
            }
            this.changeRef.detectChanges();
          }, (error: string) => { console.log(error); });
      }
    }
  }

  newDealItemLocModel(): void {
    this.dealItemLocModel = {
      dealDetailId: null, dealId: this.dealId, discountType: null, discountValue: null, item: null, itemDesc: null, merchLevel: null
      , merchLevelDesc: null, merchLevelValue: null, merchLevelValueDesc: null, operation: "I", comments: null, newCost: null, oldCost: null
    };
  }

  add($event: MouseEvent): void {
    $event.preventDefault();
    this.newDealItemLocModel();
    this.dataList.unshift(this.dealItemLocModel);
    if (this.gridApi) {
      this.gridApi.setRowData(this.dataList);
    }
  }

  save(): void {
    if (this.infoGroup.valid) {
      this.saveSubscription();
    } else {
      this.sharedService.validateForm(this.infoGroup);
    }
  }

  saveSubscription(): void {
    if (this.validateData()) {
      if (this.deletedRows.length > 0) {
        this.deletedRows.forEach(x => this.dataList.push(x));
      }
      let supplierDealModel: SupplierDealModel = {
        deaL_ID: this.dealId, dealType: this.infoGroup.controls["dealType"].value, startDate: this.infoGroup.controls["startDate"].value
        , closeDate: this.infoGroup.controls["closeDate"].value, dealCompType: this.infoGroup.controls["dealCompType"].value
        , listOfDealItemLocations: this.dataList, supplier: this.supplierId, currencyCode: this.currencyCode, operation: this.model.operation
      };
      this.saveServiceSubscription = this.service.saveDeal(supplierDealModel).subscribe((response: boolean) => {
        if (response === true) {
          this.saveSuccessSubscription = this.sharedService.saveForm(
            "Deal Saved Successfully").subscribe(() => {
              this.message = "reloadDealGrid";
              this.dialogRef.close(this.message);
            });
        } else {
          this.sharedService.errorForm("error");
        }
      });
    } else {
      this.sharedService.errorForm("Please fill Mandatory fields in Grid");
    }
  }

  validateData(): boolean {
    let valid: boolean = true;
    if (this.dataList != null && this.dataList.length > 0) {
      this.dataList.forEach(row => {
        this.mandatoryDealLocFields.forEach(field => {
          if (!row[field]) {
            valid = false;
          }
        });
        if ((row.merchLevel !== "12" && (!row["merchLevelValue"] || !row["discountValue"]))
          || (row.merchLevel === "12" && (!row["item"] || !row["newCost"]))) {
          if (row.merchLevel !== "1") {
            valid = false;
          }
        }
      });
    }
    return valid;
  }

  delete(cell: any): void {
    this.gridApi.updateRowData({ remove: [cell.data] });
    cell.data.operation = "D";
    this.deletedRows.push(cell.data);
    this.dataList.splice(cell.rowIndex, 1);
    if (this.dataList.length === 0) {
      this.newDealItemLocModel();
      this.dataList.unshift(this.dealItemLocModel);
      if (this.gridApi) {
        this.gridApi.setRowData(this.dataList);
      }
    }
  }

  reset(): void {
    this.dataList = [];
    this.serviceDocument = this.service.newModel(<SupplierDealModel>{
      closeDate: null, dealCompDesc: null, dealCompType: null, dealType: null, deaL_ID: this.dealId > 0 ? this.dealId : null
      , description: null, listOfDealItemLocations: [], operation: "I", startDate: null
      , status: null, tableName: null
      , supplier: this.supplierId, supplierName: `${this.supplierName} (${this.supplierId})`
    });
    this.sharedService.resetForm(this.infoGroup);
  }

  close(): void {
    this.dialogRef.close(this.message);
  }
}
