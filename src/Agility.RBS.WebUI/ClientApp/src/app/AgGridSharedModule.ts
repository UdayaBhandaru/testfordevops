import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { AgGridModule } from "ag-grid-angular";
import { GridEditComponent } from "./Common/grid/GridEditComponent";
import { InfoComponent } from "./Common/InfoComponent";
import { IndicatorComponent } from "./Common/grid/IndicatorComponent";
import { GridInputComponent } from "./Common/grid/GridInputComponent";
import { GridSelectComponent } from "./Common/grid/GridSelectComponent";
import { GridDeleteComponent } from "./Common/grid/GridDeleteComponent";
import { GridComponent } from "./Common/grid/GridComponent";
import { SearchComponent } from "./Common/SearchComponent";
import { ToolBarComponent } from "./Common/ToolBarComponent";
import { StackTraceComponent } from "./Common/StackTraceComponent";
import { InboxPriorityComponent } from "./inbox/InboxPriorityComponent";
import { InboxRequestIdComponent } from "./inbox/InboxRequestIdComponent";
import { InboxActionsComponent } from "./inbox/InboxActionsComponent";
import { InboxFilesComponent } from "./inbox/InboxFilesComponent";
import { RbsButtonListComponent } from "./Common/controls/RbsButtonListComponent";
import { GridImageComponent } from "./inbox/GridImageComponent";
import { GridDateComponent } from "./Common/grid/GridDateComponent";
import { RbAutoCompleteComponent } from "./Common/controls/RbAutoCompleteComponent";
import { RbDateComponent } from "./Common/controls/RbDateComponent";
import { RbAutoCompleteComponentWithChips } from "./Common/controls/RbAutoCompleteComponentWithChips";
import { FavouriteComponent } from "./favourites/FavouriteComponent";
import { RbRadioGroupComponent } from "./Common/controls/RbRadioGroupComponent";
import { ItemSelectComponent } from "./Common/ItemSelectComponent";
import { GridFilesComponent } from "./Common/grid/GridFilesComponent";
import { RbInputComponent } from "./Common/controls/RbInputComponent";
import { InboxPopupComponent } from "./inbox/InboxPopupComponent";
import { WorkflowHistoryPopupComponent } from "./inbox/WorkflowHistoryPopupComponent";
import { InboxCommentsComponent } from "./inbox/InboxCommentsComponent";
import { GridAmountComponent } from "./Common/grid/GridAmountComponent";
import { RbMessageDialogComponent } from "./Common/controls/RbMessageDialogComponent";
import { ShipmentPopupComponent } from "./SupplyChainManagement/ShipmentPopupComponent";
import { RbFlowHeaderComponent } from "./Common/controls/RbFlowHeaderComponent";
import { GridGetItemComponent } from "./Common/grid/GridGetItemComponent";
import { GridNumericComponent } from "./Common/grid/GridNumericComponent";
import { ShipmentInfoComponent } from "./SupplyChainManagement/ShipmentSKU/ShipmentInfoComponent";
import { SchedulerActionsComponent } from "./SupplyChainManagement/Scheduler/SchedulerActionsComponent";
import { GridGetBarcodeComponent } from "./Common/grid/GridGetBarcodeComponent";
import { SupplierDealPopupComponent } from "./Supplier/SupplierDeals/SupplierDealPopupComponent";
import { GridMerchLvlValComponent } from "./Common/grid/GridMerchLvlValComponent";
import { ItemDimensionsPopupComponent } from "./itemSuper/ItemDimensionsPopupComponent";
import { ItemCostPricesPopupComponent } from "./itemSuper/ItemCostPricesPopupComponent";
import { ItemBasicPopupComponent } from "./itemSuper/ItemBasicPopupComponent";
import { ItemSupplierPopupComponent } from "./itemSuper/ItemSupplierPopupComponent";
import { ItemRangingPopupComponent } from "./itemSuper/ItemRangingPopupComponent";
import { NumericEditorComponent } from "./Common/grid/NumericEditorComponent";
import { FormsModule } from "@angular/forms";
import { ShipmentInfoPopupComponent } from "./returnToVendor/ShipmentInfoPopupComponent";
import { DocumentsActionsComponent } from "./Documents/DocumentsActionsComponent";
import { DocumentPreviewComponent } from "./Documents/DocumentPreviewComponent";
import { SafePipe } from "./Documents/SafePipe";
import { GridLinkRendererComponent } from "./Common/grid/GridLinkRendererComponent";
import { RbErrorMessageComponent } from "./Common/controls/RbErrorMessageComponent";
import { RbsSharedModule } from "./RbsSharedModule";
import { RbSelectComponent } from "./Common/controls/RbSelectComponent";
import { ItemFuturePricePopupComponent } from './itemSuper/ItemFuturePricePopupComponent';
import { ItemFutureCostPopupComponent } from './itemSuper/ItemFutureCostPopupComponent';
import { ItemPromotionPopupComponent } from './itemSuper/ItemPromotionPopupComponent';
import { NonMerchandiseComponent } from './invoiceMatchingDoc/NonMerchandiseCost/NonMerchandiseComponent';
import { NonMerchandiseCostPopupComponent } from './invoiceMatchingDoc/NonMerchandiseCost/NonMerchandiseCostPopupComponent';
import { VatBreakdownPopupComponent } from './invoiceMatchingDoc/VatBreakdown/VatBreakdownPopupComponent';
import { RbAmountComponent } from './Common/controls/RbAmountComponent';
import { ItemBarcodePopupComponent } from './itemSuper/ItemBarcodePopupComponent';
import { TsfLocationsPopupComponent } from './transfer/TransferEntity/TsfLocationsPopupComponent';
import { TotalsPopupComponent } from './transportation/TotalsPopupComponent';
import { AllocationPopupComponent } from './allocation/AllocationPopupComponent';
import { AllocationComponent } from './allocation/AllocationComponent';
import { RbCheckBoxComponent } from './Common/controls/RbCheckBoxComponent';
import { GridWithoutPagination } from './Common/grid/GridWithoutPagination';

@NgModule({
  imports: [
    FrameworkCoreModule, FormsModule, RbsSharedModule,
    AgGridModule.withComponents([GridEditComponent, IndicatorComponent, GridInputComponent, GridSelectComponent, GridNumericComponent
      , GridDeleteComponent, InboxPriorityComponent, InboxActionsComponent, InboxFilesComponent, GridGetItemComponent
      , InboxRequestIdComponent, GridDateComponent, GridImageComponent, GridFilesComponent, GridAmountComponent, SchedulerActionsComponent
      , GridGetBarcodeComponent, GridMerchLvlValComponent, NumericEditorComponent, DocumentsActionsComponent, GridLinkRendererComponent, NonMerchandiseComponent, AllocationComponent
    ])
  ],
  declarations: [
    GridEditComponent,
    InfoComponent,
    IndicatorComponent,
    GridInputComponent,
    GridSelectComponent,
    GridDeleteComponent,
    GridComponent,
    GridWithoutPagination,
    SearchComponent,
    FavouriteComponent,
    ToolBarComponent,
    StackTraceComponent,
    InboxPriorityComponent,
    InboxActionsComponent,
    InboxFilesComponent,
    InboxRequestIdComponent,
    RbsButtonListComponent,
    GridDateComponent,
    GridImageComponent,
    RbAutoCompleteComponent,
    RbAutoCompleteComponentWithChips,
    RbRadioGroupComponent,
    RbDateComponent,
    ItemSelectComponent,
    GridFilesComponent,
    RbInputComponent,
    RbAmountComponent,
    WorkflowHistoryPopupComponent,
    InboxPopupComponent,
    InboxCommentsComponent,
    GridAmountComponent,
    RbMessageDialogComponent,
    ShipmentPopupComponent,
    RbFlowHeaderComponent,
    GridGetItemComponent, GridNumericComponent, ShipmentInfoComponent, SchedulerActionsComponent,
    GridGetBarcodeComponent, SupplierDealPopupComponent
    , GridMerchLvlValComponent, ItemDimensionsPopupComponent, ItemCostPricesPopupComponent
    , ItemBasicPopupComponent, ItemSupplierPopupComponent, ItemRangingPopupComponent, NumericEditorComponent, ShipmentInfoPopupComponent
    , DocumentsActionsComponent, DocumentPreviewComponent, SafePipe, RbErrorMessageComponent, ItemPromotionPopupComponent,
    RbSelectComponent, GridLinkRendererComponent, ItemFuturePricePopupComponent, ItemFutureCostPopupComponent, NonMerchandiseComponent, NonMerchandiseCostPopupComponent, VatBreakdownPopupComponent
    , ItemBarcodePopupComponent, TsfLocationsPopupComponent, TotalsPopupComponent, AllocationPopupComponent, AllocationComponent, RbCheckBoxComponent
  ],
  entryComponents: [InfoComponent, StackTraceComponent, ItemSelectComponent, WorkflowHistoryPopupComponent, InboxPopupComponent
    , InboxCommentsComponent, RbMessageDialogComponent, ShipmentPopupComponent
    , ShipmentInfoComponent, SupplierDealPopupComponent, ItemDimensionsPopupComponent, ItemCostPricesPopupComponent
    , ItemBasicPopupComponent, ItemSupplierPopupComponent, ItemRangingPopupComponent, ShipmentInfoPopupComponent, DocumentPreviewComponent
    , GridLinkRendererComponent, ItemFuturePricePopupComponent, ItemFutureCostPopupComponent, ItemPromotionPopupComponent, NonMerchandiseCostPopupComponent, VatBreakdownPopupComponent
    , ItemBarcodePopupComponent, TsfLocationsPopupComponent, TotalsPopupComponent, AllocationPopupComponent
  ],
  exports: [GridComponent, SearchComponent, FavouriteComponent, ToolBarComponent, RbsButtonListComponent, GridImageComponent
    , RbAutoCompleteComponent, RbAutoCompleteComponentWithChips, RbRadioGroupComponent, RbMessageDialogComponent
    , ItemSelectComponent, RbDateComponent, RbInputComponent, RbFlowHeaderComponent, RbAmountComponent
    , RbErrorMessageComponent, RbSelectComponent, RbCheckBoxComponent, GridWithoutPagination
  ]
})
export class AgGridSharedModule {
}
