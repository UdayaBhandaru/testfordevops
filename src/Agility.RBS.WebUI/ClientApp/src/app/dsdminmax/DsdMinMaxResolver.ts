import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { DsdMinMaxService } from "./DsdMinMaxService";
import { DsdMinMaxModel } from "./DsdMinMaxModel";
@Injectable()
export class DsdMinMaxListResolver implements Resolve<ServiceDocument<DsdMinMaxModel>> {
    constructor(private service: DsdMinMaxService) { }
    resolve(): Observable<ServiceDocument<DsdMinMaxModel>> {
        return this.service.list();
    }
}

@Injectable()
export class DsdMinMaxResolver implements Resolve<ServiceDocument<DsdMinMaxModel>> {
    constructor(private service: DsdMinMaxService) { }
    resolve(): Observable<ServiceDocument<DsdMinMaxModel>> {
        return this.service.addEdit();
    }
}



