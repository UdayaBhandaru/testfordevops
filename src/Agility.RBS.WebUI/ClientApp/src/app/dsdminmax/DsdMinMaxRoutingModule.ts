import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { DsdMinMaxListResolver, DsdMinMaxResolver} from "./DsdMinMaxResolver";
import { DsdMinMaxComponent } from "./DsdMinMaxComponent";
import { DsdMinMaxListComponent } from './DsdMinMaxListComponent';
const UtilityRoutes: Routes = [
    {
        path: "",
        children: [
            {
                path: "", redirectTo: "List", pathMatch: "full"
            },
            {
                path: "List",
              component: DsdMinMaxListComponent,
                resolve:
                {
                    serviceDocument: DsdMinMaxListResolver
                }
            },
            {
                path: "New",
                component: DsdMinMaxComponent,
                resolve:
                {
                    serviceDocument: DsdMinMaxResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(UtilityRoutes)
    ]
})
export class DsdMinMaxRoutingModule { }
