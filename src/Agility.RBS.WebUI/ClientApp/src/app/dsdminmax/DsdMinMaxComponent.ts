import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument, MessageType } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { DsdMinMaxModel } from './DsdMinMaxModel';
import { DsdMinMaxService } from './DsdMinMaxService';
import { SharedService } from '../Common/SharedService';
import { SupplierDomainModel } from '../Common/DomainData/SupplierDomainModel';
import { LocationTypeDomainModel } from '../Common/DomainData/LocationTypeDomainModel';
import { ItemQtyDomainModel } from '../TransferManagement/Details/ItemQtyDomainModel';
import { Subscription } from 'rxjs';
import { Observable } from "rxjs";
import { ItemDescpDetailsDomainModel } from './ItemDescpDetailsDomainModel';
import { DomainDetailModel } from '../domain/DomainDetailModel';

@Component({
  selector: "DsdMinMax",
  templateUrl: "./DsdMinMaxComponent.html"
})

export class DsdMinMaxComponent implements OnInit {
  PageTitle = "DSD MinMax";
  redirectUrl = "DsdMinMax";
  serviceDocument: ServiceDocument<DsdMinMaxModel>;
  editMode: boolean = false;
  editModel: any;
  localizationData: any;
  model: DsdMinMaxModel;
  supplierData: SupplierDomainModel[];
  locationData: LocationTypeDomainModel[];
  itemDesc: string;
  itemDetailsServiceSubscription: Subscription;
  indicatorData: DomainDetailModel[];
  items = [
    {
      id: 1,
      text: 'Po Grop 1'
    },
    {
      id: 2,
      text: 'Po Grop 2'
    },
    {
      id: 3,
      text: 'Po Grop 3'
    }
    ,
    {
      id: 4,
      text: 'Po Grop 4'
    }
    ,
    {
      id: 5,
      text: 'Po Grop 5'
    }
  ];
  apiUrl: string;

  constructor(private service: DsdMinMaxService, private sharedService: SharedService, private router: Router
    , private ref: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    this.supplierData = Object.assign([], this.service.serviceDocument.domainData["supplier"]);
    this.locationData = Object.assign([], this.service.serviceDocument.domainData["locType"]);
    this.indicatorData = Object.assign([], this.service.serviceDocument.domainData["indicatorStatus"]);
    this.localizationData = Object.assign({}, this.service.serviceDocument.localizationData);
    this.bind();
  }

  bind(): void {
    delete this.editModel;
    if (this.sharedService.editObject) {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.editObject);
      Object.assign(this.service.serviceDocument.dataProfile.dataModel, { operation: "U" });
      this.editModel = Object.assign({}, this.service.serviceDocument.dataProfile.dataModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
      delete this.sharedService.editObject;
      this.editMode = true;

    } else {
      this.model = {
        supplier: null, supName: null, item: null, itemDesc: null, location: null, locName: null, minQty: null,
        maxQty: null, replDay: null, day1: "N", day2: "N", day3: "N", day4: "N", day5: "N", day6: "N",
        day7: "N", poGroup: null, operation: "I"
      };

      this.service.serviceDocument.dataProfile.dataModel = this.model;
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  save(saveOnly: boolean): void {
    if (this.serviceDocument.dataProfile.profileForm.valid) {
      this.saveSubscription(saveOnly);
    } else {
      this.sharedService.validateForm(this.serviceDocument.dataProfile.profileForm);
    }
  }

  saveSubscription(saveOnly: boolean): void {
    this.checkDsdMinMax(saveOnly);
  }

  saveDsdMinMax(saveOnly: boolean): void {
    this.service.save().subscribe(() => {
      if (this.service.serviceDocument.result.type === MessageType.success) {
        this.sharedService.saveForm("DsdMinMax saved successfully.").subscribe(() => {
          if (saveOnly) {
            this.router.navigate(["/" + this.redirectUrl + "/List"], { skipLocationChange: true });
          } else {
            if (!this.editMode) {
              this.model = {
                supplier: null, supName: null, item: null, itemDesc: null, location: null, locName: null, minQty: null,
                maxQty: null, replDay: null, day1: "N", day2: "N", day3: "N", day4: "N", day5: "N", day6: "N",
                day7: "N", poGroup: null, operation: "I"
              };
              this.service.newModel(this.model);
            } else {
              Object.assign(this.editModel, this.service.serviceDocument.dataProfile.dataModel);
              this.sharedService.updateEditObjInSharedServiceAndToolBarList(this.service.serviceDocument.dataProfile.dataModel);
              this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
            }
          }
        }, (error: string) => { console.log(error); });
      } else {
        this.sharedService.errorForm(this.service.serviceDocument.result.innerException);
      }
    }, (error: string) => { console.log(error); });
  }

  checkDsdMinMax(saveOnly: boolean): void {
    if (!this.editMode) {
      this.service.isExistingDsdMinMax(this.serviceDocument.dataProfile.profileForm.controls["supplier"].value,
        this.serviceDocument.dataProfile.profileForm.controls["item"].value,
        this.serviceDocument.dataProfile.profileForm.controls["location"].value,
        this.serviceDocument.dataProfile.profileForm.controls["poGroup"].value).subscribe((response: any) => {
          if (response) {
            this.saveDsdMinMax(saveOnly);
          } else {
            this.saveDsdMinMax(saveOnly);
          }
        });
    } else {
      this.saveDsdMinMax(saveOnly);
    }
  }

  reset(): void {
    if (!this.editMode) {
      this.model = {
        supplier: null, supName: null, item: null, itemDesc: null, location: null, locName: null, minQty: null,
        maxQty: null, replDay: null, day1: "N", day2: "N", day3: "N", day4: "N", day5: "N", day6: "N",
        day7: "N", poGroup: null, operation: "I"
      };

      this.service.serviceDocument.dataProfile.dataModel = this.model;
      this.service.newModel(this.model);

    } else {
      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.editModel);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    }
    this.sharedService.resetForm(this.serviceDocument.dataProfile.profileForm);
  }

  changItemInfo(event: any): void {
    this.itemDetailsServiceSubscription = this.service.getDataFromAPI(event).subscribe((response: ItemDescpDetailsDomainModel) => {
      if (response) {
        var reslt: any = response.itemDesc;
        this.serviceDocument.dataProfile.profileForm.patchValue({
          itemDesc: reslt
        });
      }
    });
  }
}
