export class DsdMinMaxModel { 
  supplier?: number;
  supName?: string;
  item?: string;
  itemDesc?: string;
  location?: number;
  locName?: string;
  minQty?: number;
  maxQty?: number;
  replDay?: number;
  day1?: string;
  day2?: string;
  day3?: string;
  day4?: string;
  day5?: string;
  day6?: string;
  day7?: string;
  poGroup: string;
  operation?: string;
  error?: string;
  programPhase?: string;
  programMessage?: string;  
}
