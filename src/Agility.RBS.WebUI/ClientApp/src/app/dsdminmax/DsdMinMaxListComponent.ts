import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ServiceDocument } from "@agility/frameworkcore";
import { FormGroup } from "@angular/forms";
import { GridOptions } from "ag-grid-community";
import { DsdMinMaxModel } from './DsdMinMaxModel';
import { GridExportModel } from '../Common/grid/GridExportModel';
import { DsdMinMaxService } from './DsdMinMaxService';
import { SharedService } from '../Common/SharedService';
import { GridEditComponent } from '../Common/grid/GridEditComponent';
import { InfoComponent } from '../Common/InfoComponent';
import { LocationTypeDomainModel } from '../Common/DomainData/LocationTypeDomainModel';
import { SupplierDomainModel } from '../Common/DomainData/SupplierDomainModel';
@Component({
  selector: "DsdMinMax-list",
  templateUrl: "./DsdMinMaxListComponent.html"
})
export class DsdMinMaxListComponent implements OnInit {
  PageTitle = "DSD MinMax";
  redirectUrl = "DsdMinMax";
  componentName: any;
  serviceDocument: ServiceDocument<DsdMinMaxModel>;
  public gridOptions: GridOptions;
  columns: any[];
  model: DsdMinMaxModel;
  gridExportModel: GridExportModel; gridExportGroup: FormGroup;
  showSearchCriteria: boolean = true;
  supplierData: SupplierDomainModel[];
  locationData: LocationTypeDomainModel[];
  apiUrl: string;

  constructor(public service: DsdMinMaxService, private sharedService: SharedService, private router: Router) {
  }

  ngOnInit(): void {
    this.componentName = this;
    this.columns = [
      { headerName: "supName", field: "supName", tooltipField: "supName" },
      { headerName: "item", field: "item", tooltipField: "item" },
      { headerName: "itemDesc", field: "itemDesc", tooltipField: "itemDesc" },
      { headerName: "locName", field: "locName", tooltipField: "locName" },
      { headerName: "minQty", field: "minQty", tooltipField: "minQty" },
      { headerName: "maxQty", field: "maxQty", tooltipField: "maxQty" },
      { headerName: "poGroup", field: "poGroup", tooltipField: "poGroup" },
      {
        headerName: "Actions", field: "Actions", cellRendererFramework: GridEditComponent
      }
    ];
    this.supplierData = Object.assign([], this.service.serviceDocument.domainData["supplier"]);
    this.locationData = Object.assign([], this.service.serviceDocument.domainData["locType"]);
    this.model = {
      supplier: null, supName: null, item: null, itemDesc: null, location: null, locName: null, minQty: null,
      maxQty: null, replDay: null, day1: null, day2: null, day3: null, day4: null, day5: null, day6: null,
      day7: null, poGroup: null, operation: "I"
    };

    if (this.sharedService.searchData) {

      this.service.serviceDocument.dataProfile.dataModel = Object.assign({}, this.sharedService.searchData);
      this.service.newModel(this.service.serviceDocument.dataProfile.dataModel);
    } else {
      this.service.newModel(this.model);
    }
    this.serviceDocument = this.service.serviceDocument;
  }

  showSearchCriteriaChild(event: boolean): void {
    this.showSearchCriteria = event;
  }

  open(cell: any, mode: string): void {
    if (mode === "edit") {
      this.sharedService.searchData = Object.assign({}, this.serviceDocument.dataProfile.profileForm.value);
    }
    this.sharedService.viewForm(InfoComponent, cell, mode, this.redirectUrl, this.service.serviceDocument.dataProfile.dataList);
  }
}
