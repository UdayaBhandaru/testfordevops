import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ServiceDocument } from "@agility/frameworkcore";
import { DsdMinMaxModel } from "./DsdMinMaxModel";
import { HttpParams } from "@angular/common/http";
import { HttpHelperService } from '../Common/HttpHelperService';

@Injectable()
export class DsdMinMaxService {
  serviceDocument: ServiceDocument<DsdMinMaxModel> = new ServiceDocument<DsdMinMaxModel>();
  constructor(private httpHelperService: HttpHelperService) { }

  newModel(model: DsdMinMaxModel): ServiceDocument<DsdMinMaxModel> {
    return this.serviceDocument.newModel(model);
  }

  search(): Observable<ServiceDocument<DsdMinMaxModel>> {
    return this.serviceDocument.search("/api/DsdMinMax/Search");
  }

  save(): Observable<ServiceDocument<DsdMinMaxModel>> {
    return this.serviceDocument.save("/api/DsdMinMax/Save", true);
  }

  list(): Observable<ServiceDocument<DsdMinMaxModel>> {
    return this.serviceDocument.list("/api/DsdMinMax/List");
  }

  addEdit(): Observable<ServiceDocument<DsdMinMaxModel>> {
    return this.serviceDocument.list("/api/DsdMinMax/New");
  }
  isExistingDsdMinMax(supplier: number, item: string, location: number, poGroup: string): Observable<boolean> {
    return this.httpHelperService.get("/api/DsdMinMax/IsExistingDsdMinMax", new HttpParams().set("supplier", supplier.toString())
      .set("item", item).set("location", location.toString()).set("poGroup", poGroup));
  }

  getDataFromAPI(item: string): Observable<any> {
    return this.httpHelperService.get("/api/DsdMinMax/GetItemDescription", new HttpParams().set("item", item));
  }

}
