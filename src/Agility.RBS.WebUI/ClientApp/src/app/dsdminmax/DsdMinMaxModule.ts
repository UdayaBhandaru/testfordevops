import { NgModule } from "@angular/core";
import { FrameworkCoreModule } from "@agility/frameworkcore";
import { LicenseManager } from "ag-grid-enterprise/main";
import { DsdMinMaxListComponent } from "./DsdMinMaxListComponent";
import { DsdMinMaxService } from "./DsdMinMaxService";
import { DsdMinMaxRoutingModule } from "./DsdMinMaxRoutingModule";
import { DsdMinMaxComponent } from "./DsdMinMaxComponent";
import { DsdMinMaxListResolver, DsdMinMaxResolver } from "./DsdMinMaxResolver";
import { RbsSharedModule } from '../RbsSharedModule';
import { AgGridSharedModule } from '../AgGridSharedModule';
import { TitleSharedModule } from '../TitleSharedModule';

@NgModule({
    imports: [
        FrameworkCoreModule,
        DsdMinMaxRoutingModule,
        AgGridSharedModule,
        TitleSharedModule,
        RbsSharedModule
    ],
    declarations: [
        DsdMinMaxListComponent,
        DsdMinMaxComponent,
    ],
    providers: [
        DsdMinMaxService,
        DsdMinMaxListResolver,
        DsdMinMaxResolver,
    ]
})
export class DsdMinMaxModule {
    constructor() {
        LicenseManager.setLicenseKey("AGILITY_E_SERVICES_PRIVATE_LIMITED_RBS_4Devs29_November_2018__MTU0MzQ0OTYwMDAwMA==08b2bc93fc84aa93aec91c68efb052f9");
    }
}
