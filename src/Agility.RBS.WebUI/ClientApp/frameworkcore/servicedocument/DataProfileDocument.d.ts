import { FormGroup } from "@angular/forms";
import { CommonService } from "../CommonService";
import { DataProfileDocumentBase } from "./DataProfileDocumentBase";
import { FormMode } from "./FormMode";
export declare class DataProfileDocument<TDataModel> extends DataProfileDocumentBase {
    dataModel: TDataModel;
    dataList: TDataModel[];
    profileForm: FormGroup;
    formMode: FormMode;
    commonService: CommonService;
    private _http;
    constructor();
}
