export declare class PageClaims {
    claimType: string;
    claimValue: string;
    description: string;
    controller: string;
}
