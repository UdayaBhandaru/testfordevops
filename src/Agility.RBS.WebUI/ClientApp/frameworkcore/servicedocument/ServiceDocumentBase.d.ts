import { HttpClient, HttpResponse } from "@angular/common/http";
import { FormArray, FormGroup } from "@angular/forms";
import { Observable } from "rxjs";
import { CultureModel } from "../account/CultureModel";
import { UserProfile } from "../account/UserProfile";
import { FormMode } from "./FormMode";
import { MessageResult } from "./MessageResult";
import { ServiceDocumentResult } from "./ServiceDocumentResult";
export declare class ServiceDocumentBase {
    userProfile: UserProfile;
    domainData: {
        [key: string]: any;
    };
    localizationData: {
        [key: string]: any;
    };
    responseContext: {
        [key: string]: any;
    };
    messages: {
        [key: string]: MessageResult;
    };
    result: ServiceDocumentResult;
    cultureInfo: CultureModel[];
    formMode: FormMode;
    http: HttpClient;
    private _commonService;
    constructor();
    getFormGroup(dataModel: any): FormGroup;
    addSubProfile(dataModel: any, formArray: FormArray, field: string): void;
    currentAction(dataModel: any): any;
    handleError(error: HttpResponse<any> | any): Observable<any>;
}
