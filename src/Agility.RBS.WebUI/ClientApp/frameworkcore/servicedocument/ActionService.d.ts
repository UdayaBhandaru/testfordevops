import { PageClaims } from "./PageClaims";
import { WorkflowAction } from "./WorkflowAction";
export declare class ActionService {
    allowedActions: WorkflowAction[];
    currentActionId: string;
    pageClaims: PageClaims[];
}
