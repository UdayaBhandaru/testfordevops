export declare const enum MessageType {
    success = 0,
    exception = 1,
    validationException = 2,
    fxException = 4
}
