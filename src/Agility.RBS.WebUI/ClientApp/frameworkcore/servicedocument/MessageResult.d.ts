import { MessageType } from "./MessageType";
export declare class MessageResult {
    message: string;
    type: MessageType;
}
