export declare class WorkflowAction {
    actionId: string;
    actionName: string;
    fromStateId: string;
    toStateId: string;
    claimType: string;
    claimValue: string;
    transitionClaim: string;
    image?: string;
}
