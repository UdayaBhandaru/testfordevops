import { HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";
import { DataProfileDocument } from "./DataProfileDocument";
import { ServiceDocumentBase } from "./ServiceDocumentBase";
export declare class ServiceDocument<TDataModel> extends ServiceDocumentBase {
    dataProfile: DataProfileDocument<TDataModel>;
    constructor();
    list(url: string, search?: any): Observable<ServiceDocument<TDataModel>>;
    new(url: string, search?: HttpParams): Observable<ServiceDocument<TDataModel>>;
    newModel(dataModel: TDataModel): ServiceDocument<TDataModel>;
    view(url: string, search?: HttpParams): Observable<ServiceDocument<TDataModel>>;
    open(url: string, search?: HttpParams): Observable<ServiceDocument<TDataModel>>;
    search(url: string, getRawValue?: boolean, callback?: Function): Observable<ServiceDocument<TDataModel>>;
    delete(url: string, callback?: Function): Observable<any>;
    save(url: string, getRawValue?: boolean, callback?: Function): Observable<any>;
    submit(url: string, getRawValue?: boolean, callback?: Function): Observable<any>;
    login(url: string, loginModel: any): Observable<ServiceDocument<TDataModel>>;
    logout(url: string): Observable<any>;
    addSubProfile(field: string): void;
    private _initialize;
    private _get;
    private _post;
}
