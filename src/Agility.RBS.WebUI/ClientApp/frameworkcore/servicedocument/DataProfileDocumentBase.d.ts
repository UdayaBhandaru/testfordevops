import { ActionService } from "./ActionService";
import { ServiceDocumentResult } from "./ServiceDocumentResult";
export declare class DataProfileDocumentBase {
    actionService: ActionService;
    result: ServiceDocumentResult;
}
