import { MessageResult } from "../servicedocument/MessageResult";
export declare class ServiceDocumentResult extends MessageResult {
    innerException: string;
    stackTrace: string;
}
