export declare class ValidationService {
    static getValidatorErrorMessage(validatorName: string, validatorValue?: any): any;
}
