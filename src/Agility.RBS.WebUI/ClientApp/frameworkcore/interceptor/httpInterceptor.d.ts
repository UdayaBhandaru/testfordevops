import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { MatSnackBar } from "@angular/material";
import { Router } from "@angular/router";
import { Observable } from "rxjs";
import { FxContext } from "../../frameworkcore/context/FxContext";
export declare class InterceptedHttp implements HttpInterceptor {
    private baseUrl;
    private router;
    snackBar: MatSnackBar;
    fxContext: FxContext;
    private rawApiUrl;
    private apiUrl;
    constructor(baseUrl: string, router: Router, snackBar: MatSnackBar);
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>;
}
