export declare const enum AjaxResult {
    success = 0,
    exception = 1,
    validationException = 2,
    unAutherized = 401
}
