import { CheckModel } from "../models";
export declare class ClaimModel extends CheckModel {
    id: number;
    claimType: string;
    claimValue: string;
    claimCategory: string;
    description: string;
}
