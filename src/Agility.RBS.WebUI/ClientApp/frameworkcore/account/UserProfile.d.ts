import { ProfileModel } from "../profile";
import { IdentityMenuModel } from "./IdentityMenuModel";
import { UserRolesModel } from "./UserRolesModel";
export declare class UserProfile extends ProfileModel {
    id: number;
    userId: string;
    name: string;
    userName: string;
    userAccessToken: string;
    adminRoles: string[];
    menus: IdentityMenuModel[];
    userRoles: UserRolesModel[];
}
