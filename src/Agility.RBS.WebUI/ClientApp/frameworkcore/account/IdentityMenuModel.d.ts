export declare class IdentityMenuModel {
    menuId: string;
    claimId: string;
    menuText: string;
    description: string;
    parentMenuId: string;
    parentMenuText: string;
    groupMenu?: number;
    displayOrder: string;
    url: string;
    iconCss: string;
    items: IdentityMenuModel[];
}
