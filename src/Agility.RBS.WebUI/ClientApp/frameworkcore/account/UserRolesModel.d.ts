export declare class UserRolesModel {
    id: string;
    name: string;
    normalizedName: string;
    roleType: string;
}
