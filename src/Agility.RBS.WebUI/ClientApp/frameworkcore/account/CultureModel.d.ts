export declare class CultureModel {
    cultureID: number;
    culture: string;
    description: string;
    languageID: number;
    IsRtl: any;
}
