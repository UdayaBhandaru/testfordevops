import { ProfileBaseModel } from "./ProfileBaseModel";
export declare class ProfileModel extends ProfileBaseModel {
    workflowInstanceId: number;
    workflowStateId: string;
    workflowInstance: any;
    isWorkflowCompleted: boolean;
}
