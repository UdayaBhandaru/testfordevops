export declare class BaseModel {
    createdBy: string;
    modifiedBy: string;
    createdDate: Date;
    modifiedDate: Date;
    deletedInd: boolean;
}
