import { BaseModel } from "./BaseModel";
export declare class ProfileBaseModel extends BaseModel {
    organizationId?: number;
    tenantId?: number;
}
