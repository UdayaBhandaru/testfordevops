import { Injector, ModuleWithProviders } from "@angular/core";
import "hammerjs";
import "./RxjsOperators";
export declare class FrameworkCoreModule {
    private injector;
    static forRoot(): ModuleWithProviders;
    constructor(injector: Injector);
}
