import { FxContext } from "./context/FxContext";
export declare class TranslateService {
    private fxContext;
    TranslationsMap: Map<string, any>;
    constructor(fxContext: FxContext);
    instant(key: string): string;
    createTranslationMap(language: string, translations: any): void;
    private translate;
}
