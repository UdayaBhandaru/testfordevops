import { HttpClient, HttpParams } from "@angular/common/http";
import { FormArray, FormGroup } from "@angular/forms";
import { Response } from "@angular/http";
import { MatSnackBar, MatSnackBarRef, SimpleSnackBar } from "@angular/material";
import { Observable } from 'rxjs';
import { WorkflowAction } from "./servicedocument/WorkflowAction";
export declare class CommonService {
    private snackBar;
    http: HttpClient;
    private disableFormControl;
    constructor(snackBar: MatSnackBar);
    hideAction(allowedActions: WorkflowAction[], profileId: string, actionId: string, stateId: string): boolean;
    hideAddAction(allowedActions: WorkflowAction[], profileId: string, actionId: string): boolean;
    getFormGroup(dataModel: any, formMode: number, includeCurrentActionId?: boolean): FormGroup;
    addSubProfile(dataModel: any, formArray: FormArray, field: string, formMode: number): void;
    get(url: string, search?: HttpParams): Observable<any>;
    post(url: string, body: any): Observable<any>;
    handleError(error: Response | any): Observable<any>;
    showAlert(message: string, action?: string, duration?: number): MatSnackBarRef<SimpleSnackBar>;
    private _getFormGroupControls;
    private _getFormArrayControl;
    private _getFormArrayControls;
    private _buildFormGroup;
    private _buildFormArray;
    private _buildFormControl;
    private _setFormMode;
}
