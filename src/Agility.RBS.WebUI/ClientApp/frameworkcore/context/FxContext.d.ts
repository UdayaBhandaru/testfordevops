import { IdentityMenuModel } from "../account";
import { UserProfile } from "../account";
import { CultureModel } from "../account";
export declare class FxContext {
    IsAuthenticated: boolean;
    menus: IdentityMenuModel[];
    userProfile: UserProfile;
    culture: CultureModel;
    clientContext: {
        [key: string]: any;
    };
    languageId: number;
    languages: any[];
}
