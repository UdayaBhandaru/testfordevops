import { AjaxResult } from "../index";
export declare class AjaxModel<TDataModel> {
    result: AjaxResult;
    message: string;
    model: TDataModel;
}
