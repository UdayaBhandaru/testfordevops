export declare class TreeViewModel {
    expanded: boolean;
    toggle(): void;
}
