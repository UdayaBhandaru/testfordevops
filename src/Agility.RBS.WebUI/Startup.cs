// <copyright file="Startup.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.WebUI
{
    using System;
    using System.Collections.Generic;
    using System.IO.Compression;
    using System.Text;
    using Agility.Framework.Core;
    using Agility.Framework.Core.Common;
    using Agility.Framework.Designer;
    using Agility.Framework.Oracle;
    using Agility.Framework.Web;
    using Agility.Framework.Web.Extensions;
    using Agility.RBS.Allocation;
    using Agility.RBS.Core;
    using Agility.RBS.CostManagement;
    using Agility.RBS.Dashboard;
    using Agility.RBS.DealManagement;
    using Agility.RBS.Documents;
    using Agility.RBS.Finance;
    using Agility.RBS.ForecastManagement;
    using Agility.RBS.Inbox;
    using Agility.RBS.InventoryManagement;
    using Agility.RBS.InvoiceMatching;
    using Agility.RBS.ItemManagement;
    using Agility.RBS.MDM;
    using Agility.RBS.OrderManagement;
    using Agility.RBS.PriceManagement;
    using Agility.RBS.Promotion;
    using Agility.RBS.Receiving;
    using Agility.RBS.Security;
    using Agility.RBS.SupplierManagement;
    using Agility.RBS.SupplyChainManagement;
    using Agility.RBS.Transportation;
    using Microsoft.AspNetCore.Antiforgery;
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.ResponseCompression;
    using Microsoft.AspNetCore.Rewrite;
    using Microsoft.AspNetCore.SpaServices.AngularCli;
    using Microsoft.AspNetCore.SpaServices.Webpack;
    using Microsoft.Extensions.Caching.Distributed;
    using Microsoft.Extensions.Caching.Memory;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using Microsoft.IdentityModel.Tokens;
    using NLog.Web;

    public class Startup : FxTokenStartup
    {
        public Startup(IHostingEnvironment env)
        {
            IConfigurationRoot configRoot = null;
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            this.AddStartupComponents(new List<IComponentStartup>
            {
                new MdmStartup(),
                new RbsSecurityStartup(), new ItemManagementStartup(),
                new DocumentsStartup(), new DashboardStartup(),
                new PriceManagementStartup(), new CostManagementStartup(),
                new PromotionStartup(), new OrderManagementStartup(),
                new SupplyChainStartup(), new InboxStartup(), new ForecastManagementStartup(),
                new ReportsStartup(), new DealManagementStartup(),
                new SupplierManagementStartup(), new InventoryManagementStartup(), new FinanceStartup(),
                new Sales.SalesStartup(), new TransportationStartup(), new ReceivingStartup(),
                new InvoiceMatchingStartup(), new AllocationStartup()
            });

            configRoot = builder.Build();
            this.Startup(env, configRoot);
            SetConfigValues(configRoot);
        }

        public override IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddTransient(typeof(IDbStartup), typeof(OracleStartup));
            services.AddMVCFx();
            services.AddResponseCompression();
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist";
            });
            return base.ConfigureServices(services);
        }

        public override void Configure(
            IApplicationBuilder app,
            IHostingEnvironment env,
            ILoggerFactory loggerFactory,
            IDistributedCache distributedCache,
            IMemoryCache memoryCache,
            IServiceProvider serviceProvider)
        {
            base.Configure(app, env, loggerFactory, distributedCache, memoryCache, serviceProvider);
            app.UseStaticFiles(new Microsoft.AspNetCore.Builder.StaticFileOptions
            {
                FileProvider = new Microsoft.Extensions.FileProviders.PhysicalFileProvider(
            Core.RbSettings.DocumentsServerPhysicalPath),
                RequestPath = new Microsoft.AspNetCore.Http.PathString(Core.RbSettings.DocumentsServerVirtualPath)
            });
            app.UseSpaStaticFiles();

            app.UseMvcFx();
            app.UseSpa(spa =>
            {
                // To learn more about options for serving an Angular SPA from ASP.NET Core,
                //// see https://go.microsoft.com/fwlink/?linkid=864501

                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseAngularCliServer(npmScript: "start");
                }
            });
        }

        private static void SetConfigValues(IConfigurationRoot configRoot)
        {
            RbSettings.DocumentsServerPhysicalPath = configRoot["DocumentsServerPhysicalPath"];
            RbSettings.DocumentsServerVirtualPath = configRoot["DocumentsServerVirtualPath"];

            RbSettings.ServiceAccountMail = configRoot["ServiceAccountMail"];
            RbSettings.ServiceAccountMode = Convert.ToBoolean(configRoot["ServiceAccountMode"]);
            RbSettings.WebRootPath = configRoot["Web"];
            RbSettings.ServiceAccountName = configRoot["ServiceAccountName"];
            RbSettings.SmtpServer = configRoot["SmtpServer"];
            RbSettings.Web = configRoot["Web"];
            RbSettings.AllowSendingMail = Convert.ToBoolean(configRoot["AllowSendingMail"]);
            RbSettings.DemoEmails = configRoot["DemoEmails"];
            RbSettings.DemoMode = Convert.ToBoolean(configRoot["DemoMode"]);
            RbSettings.UserLogoPath = configRoot["RbsLogoPath"];
            RbSettings.RbsExcelFileDownloadPath = configRoot["RbsExcelFileDownloadPath"];

            ////RbSettings.SheetName = configRoot["SheetName"];
            ////RbSettings.PromoFile = configRoot["PromoFile"];
            ////RbSettings.CostChangeFormProcesPath = configRoot["CostChangeFormProcesPath"];
            ////RbSettings.CostChangeFormAchievePath = configRoot["CostChangeFormAchievePath"];

            ////RbSettings.ItemCloseOutFormTemplate = configRoot["ItemCloseOutFormTemplate"];
            ////RbSettings.ItemCloseOutFormProcesPath = configRoot["ItemCloseOutFormProcesPath"];
            ////RbSettings.ItemCloseOutFormAchievePath = configRoot["ItemCloseOutFormAchievePath"];
        }
    }
}
