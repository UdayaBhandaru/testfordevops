﻿//-------------------------------------------------------------------------------------------------
// <copyright file="ForecastController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// ForecastController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.ForecastManagement.Forecast
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Constants;
    using Agility.RBS.Core.CustomExceptions;
    using Agility.RBS.Core.Models;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.Documents;
    using Agility.RBS.Documents.Models;
    using Agility.RBS.ForecastManagement.Forecast.Models;
    using Agility.RBS.ForecastManagement.Forecast.Repositories;
    using Agility.RBS.Inbox.Common;
    using Agility.RBS.Inbox.Models;
    using Agility.RBS.Inbox.Repository;
    using Agility.RBS.MDM.Repositories;
    using DocumentFormat.OpenXml.Packaging;
    using DocumentFormat.OpenXml.Spreadsheet;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    [Route("api/[controller]/[action]")]
    public class ForecastController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly ForecastRepository forecastRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly InboxModel inboxModel;
        private readonly InboxRepository inboxRepository;
        private ServiceDocument<ForecastHeadModel> serviceDocument;
        private int headSeq;

        public ForecastController(
            ServiceDocument<ForecastHeadModel> serviceDocument,
            ForecastRepository forecastRepository,
            DomainDataRepository domainDataRepository,
            InboxRepository inboxRepository)
        {
            this.forecastRepository = forecastRepository;
            this.domainDataRepository = domainDataRepository;
            this.inboxRepository = inboxRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
            this.inboxModel = new InboxModel { Operation = "CHGS", ModuleName = "FORECAST" };
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in the Bulk Item List page.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        public async Task<ServiceDocument<ForecastHeadModel>> List()
        {
            this.serviceDocument.DomainData.Add("location", this.domainDataRepository.LocationDomainGet().Result);
            this.serviceDocument.DomainData.Add("workflowStatusList", this.domainDataRepository.WorkflowStatusGet(RbCommonConstants.ForecastModule).Result.OrderBy(x => x.Name));
            await this.BasicDomainData();
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in Bulk Item Head page when Add button clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<ForecastHeadModel>> New()
        {
            this.serviceDocument.DomainData.Add("location", this.domainDataRepository.LocationDomainGet().Result);
            this.serviceDocument.New(false);
            await this.BasicDomainData();
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for populating fields in the Bulk Item Head page when Edit icon clicked.
        /// </summary>
        /// <param name="id">Cost Change Id</param>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<ForecastHeadModel>> Open(int id)
        {
            this.serviceDocument.DomainData.Add("location", this.domainDataRepository.LocationDomainGet().Result);
            this.headSeq = id;
            await this.serviceDocument.OpenAsync(this.ForecastOpen);
            //// this.serviceDocument.DomainData.Add("inboxCommonData", this.domainDataRepository.InboxCommonGet(RbCommonConstants.CostModule, id).Result);
            await this.BasicDomainData();
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for submitting cost change to next level when Submit button clicked.
        /// </summary>
        /// <param name="serviceDocument">Service Document</param>
        /// <returns>ServiceDocument</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<ForecastHeadModel>> Submit([FromBody]ServiceDocument<ForecastHeadModel> serviceDocument)
        {
            this.serviceDocument = serviceDocument;
            this.inboxModel.StateID = GuidConverter.DotNetToOracle(Convert.ToString(serviceDocument.DataProfile.DataModel.WorkflowInstance?.WorkflowStateId));
            await this.serviceDocument.TransitAsync(this.ForecastSave);

            if (this.serviceDocument.Result.Type == MessageType.Success)
            {
                if (this.serviceDocument.DataProfile.DataModel.WorkflowInstance.StateName == "Worksheet")
                {
                    this.inboxModel.Operation = "DFT";
                }

                await this.inboxRepository.SetInbox(this.inboxModel, this.serviceDocument);
            }

            return this.serviceDocument;
        }

        public async Task<ServiceDocument<ForecastHeadModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.ForecastSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<ForecastHeadModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.ForecastSave);
            return this.serviceDocument;
        }

        public async Task<FileResult> OpenExcel(int id, string locName)
        {
            ////var user = Core.Utility.UserProfileHelper.GetInMemoryUser();
            ////var companyid = this.HttpContext.Request.Headers["companyid"];
            ////var jwtTokenTuple = System.Tuple.Create<string, string, string>(this.HttpContext.Session.Id, companyid, Core.JwtTokenHelper.GetToken(user.UserId, user.UserName));
            var file = this.domainDataRepository.FilePathGet(this.inboxModel.ModuleName).Result;
            string templateFile = $"{file.TemplatePath}\\{locName}.xlsm";
            string bulkItemFile = $"{RbSettings.RbsExcelFileDownloadPath}\\Forecast-{id}.xlsm";
            string xlsm = RbSettings.RbsExcelFileDownloadPath + "\\" + Guid.NewGuid().ToString() + ".xlsm";
            System.IO.File.Copy(System.IO.File.Exists(bulkItemFile) ? bulkItemFile : templateFile, xlsm);
            if (!System.IO.File.Exists(bulkItemFile))
            {
                System.IO.File.Copy(templateFile, bulkItemFile);
            }

            return await DocumentsHelper.GetFilestream(xlsm, this);
        }

        [HttpPost]
        public async Task<bool> FileUpload([FromBody]FileDataUpload file)
        {
            return await DocumentsRepository.CopyingFile(file);
        }

        private async Task BasicDomainData()
        {
            this.serviceDocument.LocalizationData.AddData("Forecast");
        }

        private async Task<List<ForecastHeadModel>> ForecastSearch()
        {
            try
            {
                var lstUomClass = await this.forecastRepository.GetForecast(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstUomClass;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<ForecastHeadModel> ForecastOpen()
        {
            try
            {
                var bulkItem = await this.forecastRepository.ForecastGet(this.headSeq, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return bulkItem;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> ForecastSave()
        {
            this.serviceDocument.DataProfile.DataModel.Status = RbsServiceDocCustomizationHelper.GetStateNameByProfileIdAndCurActionId(this.serviceDocument);
            this.serviceDocument.Result = await this.forecastRepository.ForecastSet(this.serviceDocument.DataProfile.DataModel);
            if (this.serviceDocument.Result.Type != MessageType.Success)
            {
                return false;
            }

            this.serviceDocument.DataProfile.DataModel.ID = this.forecastRepository.Id;
            return true;
        }
    }
}
