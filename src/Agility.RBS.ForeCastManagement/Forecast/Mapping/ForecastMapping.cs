﻿// <copyright file="ForecastMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ForecastManagement.Forecast.Mapping
{
    using System;
    using Agility.RBS.Core;
    using Agility.RBS.ForecastManagement.Forecast.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class ForecastMapping : Profile
    {
        public ForecastMapping()
            : base("ForecastMapping")
        {
            this.CreateMap<OracleObject, ForecastHeadModel>()
                .ForMember(m => m.ID, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["ID"])))
                .ForMember(m => m.Comments, opt => opt.MapFrom(r => r["COMMENTS"]))
                .ForMember(m => m.LocationId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["LOCATION_ID"])))
                .ForMember(m => m.LocationName, opt => opt.MapFrom(r => r["LOC_NAME"]))
                .ForMember(m => m.CreatedBy, opt => opt.MapFrom(r => r["CREATED_BY"]))
                .ForMember(m => m.ModifiedBy, opt => opt.MapFrom(r => r["LAST_UPDATED_BY"]))
                .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
                .ForMember(m => m.CreatedDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["CREATED_DATE"])))
                .ForMember(m => m.ModifiedDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["LAST_UPDATED_DATE"])))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "BULK_ITEM_HEAD"))
                .ForMember(m => m.Status, opt => opt.MapFrom(r => r["STATUS"]))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));
        }
    }
}