﻿// <copyright file="ForecastRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// ForeCastRepository
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.ForecastManagement.Forecast.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.ForecastManagement.Common;
    using Agility.RBS.ForecastManagement.Forecast.Models;
    using Agility.RBS.MDM.Common;
    using AutoMapper;
    using Devart.Data.Oracle;
    using DocumentFormat.OpenXml;
    using DocumentFormat.OpenXml.Packaging;
    using DocumentFormat.OpenXml.Spreadsheet;
    using ExcelDataReader;
    using Microsoft.AspNetCore.Authorization;

    public class ForecastRepository : BaseOraclePackage
    {
        public ForecastRepository()
        {
            this.PackageName = ForecastManagementConstants.GetItemPackage;
        }

        public int Id { get; private set; }

        public async Task<ForecastHeadModel> ConvertExcelDataToType(DataTable eSheet, ForecastHeadModel iHeadModel)
        {
            if (eSheet != null)
            {
                List<ForecastDetailModel> orderDetailsList = (from DataRow dr in eSheet.Rows
                                                              where Convert.ToString(dr[1]).ToUpper() == iHeadModel.LocationName.ToUpper()
                                                              select new ForecastDetailModel
                                                              {
                                                                  DeptDesc = Convert.ToString(dr[3]),
                                                                  CategoryDesc = Convert.ToString(dr[5]),
                                                                  ClassDesc = Convert.ToString(dr[7]),
                                                                  SubclassDesc = Convert.ToString(dr[9]),
                                                                  Item = Convert.ToString(dr[10]),
                                                                  ItemDesc = Convert.ToString(dr[11]),
                                                                  NewMin = Convert.ToString(dr[125]),
                                                                  NewMax = Convert.ToString(dr[126])
                                                              }).ToList();

                iHeadModel.ForecastDetails.AddRange(orderDetailsList);
            }

            return iHeadModel;
        }

        public async Task<List<ForecastHeadModel>> GetForecast(ForecastHeadModel forecastHeadModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(ForecastManagementConstants.ObjTypeForeCastHead, ForecastManagementConstants.GetProcForeCastHead);
            OracleObject recordObject = this.SetParamsForecast(forecastHeadModel, true);
            return await this.GetProcedure2<ForecastHeadModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ForecastHeadModel> ForecastGet(int id, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType(ForecastManagementConstants.RecordObjForeCastHead);
            OracleObject recordObject = this.GetOracleObject(recordType);
            this.SetId(id, recordObject);
            PackageParams packageParameter = this.GetPackageParams(ForecastManagementConstants.ObjTypeForeCastHead, ForecastManagementConstants.GetProcForeCastHead);
            var forecastHeads = await this.GetProcedure2<ForecastHeadModel>(recordObject, packageParameter, serviceDocumentResult);
            if (forecastHeads != null && forecastHeads.Count > 0)
            {
                await this.ConvertExcelDataToType(await this.GetDataTableByReadingExcel($"{RbSettings.RbsExcelFileDownloadPath}\\Forecast-{forecastHeads[0].ID.Value}.xlsm"), forecastHeads[0]);
                return forecastHeads[0];
            }

            return null;
        }

        public virtual void SetId(int id, OracleObject recordObject)
        {
            recordObject["ID"] = id;
        }

        public async Task<ServiceDocumentResult> ForecastSet(ForecastHeadModel forecastHeadModel)
        {
            this.Id = 0;
            PackageParams packageParameter = this.GetPackageParams(ForecastManagementConstants.ObjTypeForeCastHead, ForecastManagementConstants.SetProcForeCastHead);
            OracleObject recordObject = this.SetParamsForecast(forecastHeadModel, false);
            var result = await this.SetProcedure(recordObject, packageParameter);
            if (this.ObjResult != null)
            {
                this.Id = Convert.ToInt32(this.ObjResult["ID"]);
            }

            return result;
        }

        public virtual void SetParamsForecastSearch(ForecastHeadModel forecastHeadModel, OracleObject recordObject)
        {
            recordObject["ID"] = forecastHeadModel.ID;
            recordObject["COMMENTS"] = forecastHeadModel.Comments;
            recordObject["LOCATION_ID"] = forecastHeadModel.LocationId;
            recordObject["STATUS"] = forecastHeadModel.Status;
        }

        public virtual void SetParamsForecastSave(ForecastHeadModel forecastHeadModel, OracleObject recordObject)
        {
            recordObject["Operation"] = forecastHeadModel.Operation;
            recordObject["CREATED_BY"] = this.GetCreatedBy(forecastHeadModel.CreatedBy);
            recordObject["LAST_UPDATED_BY"] = FxContext.Context.Name;
        }

        public OracleObject SetParamsForecast(ForecastHeadModel forecastHeadModel, bool isSearch)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType(ForecastManagementConstants.RecordObjForeCastHead);
            OracleObject recordObject = this.GetOracleObject(recordType);
            if (!isSearch)
            {
                this.SetParamsForecastSave(forecastHeadModel, recordObject);
            }

            this.SetParamsForecastSearch(forecastHeadModel, recordObject);
            return recordObject;
        }
    }
}