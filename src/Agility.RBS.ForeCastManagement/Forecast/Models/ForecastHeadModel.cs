﻿// <copyright file="ForecastHeadModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// ForecastHeadModel
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.ForecastManagement.Forecast.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Inbox.Common;

    [DataProfile(Name = "FORECASTHEAD")]
    public class ForecastHeadModel : ProfileEntity<ForecastHeadWfModel>, IInboxCommonModel
    {
        public ForecastHeadModel()
        {
            this.ForecastDetails = new List<ForecastDetailModel>();
        }

        [Column("ID")]
        public int? ID { get; set; }

        [NotMapped]
        public string Comments { get; set; }

        [NotMapped]
        public int? LocationId { get; set; }

        [NotMapped]
        public string LocationName { get; set; }

        [NotMapped]
        public int? LangId { get; set; }

        [NotMapped]
        public string ProgramPhase { get; set; }

        [NotMapped]
        public string ProgramMessage { get; set; }

        [NotMapped]
        public string Error { get; set; }

        [NotMapped]
        public string Operation { get; set; }

        [NotMapped]
        public string TableName { get; set; }

        [NotMapped]
        public Core.Models.RbWfHeaderModel WorkflowForm { get; set; }

        [NotMapped]
        [Column("ORGANISATION_ID")]
        public new int? OrganizationId { get; set; }

        [NotMapped]
        [Column("CREATED_BY")]
        public new string CreatedBy { get; set; }

        [NotMapped]
        [Column("LAST_UPDATED_BY")]
        public new string ModifiedBy { get; set; }

        [NotMapped]
        [Column("DeletedInd")]
        public new bool? DeletedInd { get; set; }

        [NotMapped]
        [Column("CREATED_DATE")]
        public new DateTimeOffset? CreatedDate { get; set; }

        [NotMapped]
        [Column("LAST_UPDATED_DATE")]
        public new DateTimeOffset? ModifiedDate { get; set; }

        [NotMapped]
        public int? WfNxtStateInd { get; set; }

        [NotMapped]
        public string Status { get; set; }

        [NotMapped]
        public int? FileCount { get; set; }

        [NotMapped]
        public List<ForecastDetailModel> ForecastDetails { get; private set; }
    }
}
