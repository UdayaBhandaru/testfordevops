﻿// <copyright file="ForecastDetailModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ForecastManagement.Forecast.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class ForecastDetailModel
    {
        public string DivisionDesc { get; set; }

        public string DeptDesc { get; set; }

        public string CategoryDesc { get; set; }

        public string ClassDesc { get; set; }

        public string SubclassDesc { get; set; }

        public string Item { get; set; }

        public string ItemDesc { get; set; }

        public string NewMin { get; set; }

        public string NewMax { get; set; }
    }
}
