﻿// <copyright file="ForecastManagementConstants.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
namespace Agility.RBS.ForecastManagement.Common
{
    internal static class ForecastManagementConstants
    {
        // Result
        public const string OracleParameterResult = "RESULT";

        // Item Package
        public const string GetItemPackage = "UTL_FORECAST";

        // Forecast
        public const string GetProcForeCastHead = "GETFORECASTHEAD";
        public const string SetProcForeCastHead = "SETFORECASTHEAD";
        public const string ObjTypeForeCastHead = "FORECAST_HEAD_TAB";
        public const string RecordObjForeCastHead = "FORECAST_HEAD_REC";
    }
}
