﻿// <copyright file="BaseOracleParameter.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Core
{
    using System.Data;
    using Devart.Data.Oracle;

    public class BaseOracleParameter
    {
        public string Name { get; set; }

        public OracleDbType DataType { get; set; }

        public ParameterDirection Direction { get; set; }

        public string Value { get; set; }

        public string ObjectTypeName { get; set; }
    }
}
