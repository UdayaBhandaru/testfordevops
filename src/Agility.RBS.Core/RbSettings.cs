﻿// <copyright file="RbSettings.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
namespace Agility.RBS.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public static class RbSettings
    {
        public static string DocumentsServerPhysicalPath { get; set; }

        public static string DocumentsServerVirtualPath { get; set; }

        public static string ServiceAccountMail { get; set; }

        public static string ServiceAccountName { get;  set; }

        public static bool ServiceAccountMode { get;  set; }

        public static string WebRootPath { get;  set; }

        public static string SmtpServer { get;  set; }

        public static string Web { get; set; }

        public static bool AllowSendingMail { get; set; }

        public static bool DemoMode { get; set; }

        public static string DemoEmails { get; set; }

        public static string UserLogoPath { get; set; }

        ////public static string RbsExcelFileExistPath { get; set; }

        public static string RbsExcelFileDownloadPath { get; set; }

        ////public static string CostManagementTemplate { get; set; }

        ////public static string Issuer { get; set; }

        ////public static string Audience { get; set; }

        ////public static string SheetName { get; set; }

        ////public static string PriceExcelFileExistPath { get; set; }

        ////public static string PromoFile { get; set; }

        ////public static string CostChangeFormProcesPath { get; set; }

        ////public static string CostChangeFormAchievePath { get; set; }

        ////public static string ItemCloseOutFormTemplate { get; set; }

        ////public static string ItemCloseOutFormProcesPath { get; set; }

        ////public static string ItemCloseOutFormAchievePath { get; set; }

        ////public static string LocalItemReactivationFormTemplate { get; set; }

        ////public static string ForeignItemReactivationFormTemplate { get; set; }
    }
}
