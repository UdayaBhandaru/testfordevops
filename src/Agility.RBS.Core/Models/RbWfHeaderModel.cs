﻿// <copyright file="RbWfHeaderModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
namespace Agility.RBS.Core.Models
{
    using System.ComponentModel.DataAnnotations.Schema;

    public class RbWfHeaderModel
    {
        [NotMapped]
        public string Comments { get; set; }

        [NotMapped]
        public string Priority { get; set; }

        // TransitionClaim
        [NotMapped]
        public string Decision { get; set; }

        [NotMapped]
        public string RecipientID { get; set; }

        [NotMapped]
        public string RecipientName { get; set; }

        [NotMapped]
        public string StateId { get; set; }
    }
}