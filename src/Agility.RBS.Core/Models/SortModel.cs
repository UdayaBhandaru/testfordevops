﻿// <copyright file="SortModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Core.Models
{
  public class SortModel
    {
        public string ColId { get; set; }

        public string Sort { get; set; }
    }
}
