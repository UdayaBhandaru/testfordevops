﻿// <copyright file="InboxNotificationModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Core.Models
{
    using System;
    using System.Collections.Generic;
    using Agility.Framework.Core.Profile.Entities;

    public class InboxNotificationModel
    {
        public string ProfileInstance { get; set; }

        public string LogoPath { get; set; }

        public string Email { get; set; }

        public string SenderName { get; set; }

        public string RequestId { get; set; }

        public string Status { get; set; }

        public DateTime? DueDate { get; set; }

        public string ApplicationLoginUrl { get; set; }

        public DateTime? RequestCreatedDate { get; set; }

        public string Description { get; set; }

        public string CreatorName { get; set; }

        public string ApproveLink { get; set; }

        public string SenderEmail { get; set; }

        public string RecipientEmails { get; set; }

        public string RecipientName { get; set; }
    }
}