﻿// <copyright file="FileDataUpload.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Core.Models
{
    using System;
    using System.Collections.Generic;
    using Agility.Framework.Core.Profile.Entities;

    public class FileDataUpload
    {
        public string FileData { get; set; }

        public string RequestId { get; set; }

        public string FileName { get; set; }
    }
}