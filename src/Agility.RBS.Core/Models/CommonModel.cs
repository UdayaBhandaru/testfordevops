﻿// <copyright file="CommonModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Core.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class CommonModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Status { get; set; }

        public string Description
        {
            get
            {
                return this.Name + "(" + this.Id + ")";
            }
        }
    }
}
