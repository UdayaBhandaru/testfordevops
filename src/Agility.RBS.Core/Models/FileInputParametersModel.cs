﻿// <copyright file="FileInputParametersModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Core.Models
{
    using System;
    using System.Collections.Generic;
    using Agility.Framework.Core.Profile.Entities;

    public class FileInputParametersModel
    {
        public string From { get; set; }

        public string DisplayName { get; set; }

        public string[] ToMails { get; set; }

        public string MessageBody { get; set; }

        public string Subject { get; set; }

        public string RequestCode { get; set; }

        public string ProfileInstansId { get; set; }

        public string LogoPath { get; set; }
    }
}