﻿// <copyright file="OracleNullHandler.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public static class OracleNullHandler
    {
        public static int? DbNullIntHandler(object obj)
        {
            return obj == DBNull.Value ? default(int?) : Convert.ToInt32(obj);
        }

        public static decimal? DbNullDecimalHandler(object obj)
        {
            return obj == DBNull.Value ? default(decimal?) : Convert.ToDecimal(obj);
        }

        public static double? DbNullDoubleHandler(object obj)
        {
            return obj == DBNull.Value ? default(double?) : Convert.ToDouble(obj);
        }

        public static DateTime? DbNullDateTimeHandler(object obj)
        {
            return obj == DBNull.Value || obj == "null" || obj == null ? default(DateTime?) : (DateTime)obj;
        }

        public static long? DbNullInt64Handler(object obj)
        {
            return obj == DBNull.Value ? default(long?) : Convert.ToInt64(obj);
        }
    }
}
