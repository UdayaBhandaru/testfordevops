﻿// <copyright file="GlobalSuppressions.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1652:Enable XML documentation output", Justification = "Header is defined in stylecop.json")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Sonar.CSharp.DocumentationRules", "S125:Remove this commented out code", Justification = "Header is defined in stylecop.json")]