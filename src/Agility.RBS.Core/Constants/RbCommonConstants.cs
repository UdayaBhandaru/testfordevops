﻿// <copyright file="RbCommonConstants.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Core.Constants
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public static class RbCommonConstants
    {
        public const string InsertOperation = "I";
        public const string UpdateOperation = "U";

        public const string CostModule = "COST";
        public const string ItemModule = "ITEM";
        public const string PriceModule = "PRICE";
        public const string OrderModule = "ORDER";
        public const string TransferModule = "TRANSFER";
        public const string PromoModule = "PROMO";
        public const string BulkModule = "BULKITEM";
        public const string ForecastModule = "FORECAST";
        public const string RTVModule = "RTV";
        public const string ItemCloseModule = "ITEMCLOSE";
        public const string ItemReactiveModule = "ITEMREACTIVE";

        public const string UomClassMass = "MASS";
        public const string UomClassDimension = "DIMEN";
        public const string UomClassVolume = "VOL";

        public const string JwtSigningKey = "SECRET_KEY_Rbs_123456789012345678";
    }
}
