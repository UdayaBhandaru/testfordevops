﻿// <copyright file="ExcelHelper.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Core.Utility
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Linq;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Core.Security.Models;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.Framework.Web.Security.Middleware;
    using Devart.Data.Oracle;
    using DocumentFormat.OpenXml;
    using DocumentFormat.OpenXml.Packaging;
    using DocumentFormat.OpenXml.Spreadsheet;

    public static class ExcelHelper
    {
        public static string GetCellValue(string colIndex, int rowIndex, Worksheet workSheet, WorkbookPart wbPart)
        {
            Cell theCell = workSheet.Descendants<Cell>().Where(c => c.CellReference == $"{colIndex}{rowIndex}").FirstOrDefault();
            if (theCell == null)
            {
                return string.Empty;
            }

            string value = string.Empty;
            if (theCell != null)
            {
                value = theCell.InnerText;

                // If the cell represents an integer number, you are done.
                // For dates, this code returns the serialized value that
                // represents the date. The code handles strings and
                // Booleans individually. For shared strings, the code
                // looks up the corresponding value in the shared string
                // table. For Booleans, the code converts the value into
                // the words TRUE or FALSE.
                if (theCell.DataType != null)
                {
                    switch (theCell.DataType.Value)
                    {
                        case CellValues.SharedString:

                            // For shared strings, look up the value in the
                            // shared strings table.
                            var stringTable =
                                wbPart.GetPartsOfType<SharedStringTablePart>()
                                .FirstOrDefault();

                            // If the shared string table is missing, something
                            // is wrong. Return the index that is in
                            // the cell. Otherwise, look up the correct text in
                            // the table.
                            if (stringTable != null)
                            {
                                value =
                                    stringTable.SharedStringTable
                                    .ElementAt(int.Parse(value)).InnerText;
                            }

                            break;

                        case CellValues.Boolean:
                            switch (value)
                            {
                                case "0":
                                    value = "FALSE";
                                    break;
                                default:
                                    value = "TRUE";
                                    break;
                            }

                            break;
                    }
                }
                else
                {
                    if (theCell.CellFormula != null)
                    {
                        value = Convert.ToString(theCell.CellValue.InnerText);
                    }
                }
            }

            return value;
        }

        public static bool WriteToCell<T>(string colIndex, int rowIndex, Worksheet workSheet, T value)
        {
            Cell theCell = workSheet.Descendants<Cell>().Where(c => c.CellReference == $"{colIndex}{rowIndex}").FirstOrDefault();
            if (theCell != null)
            {
                theCell.DataType = new EnumValue<CellValues>(GetCellType<T>(value));
                CellValue cellValue = new CellValue(Convert.ToString(value));
                theCell.CellValue = cellValue;
            }

            return true;
        }

        public static CellValues GetCellType<T>(T value)
        {
            switch (value?.GetType().Name.ToLower())
            {
                case "int32":
                    return CellValues.Number;
                    break;
                case "int64":
                    return CellValues.Number;
                    break;
                case "string":
                    return CellValues.String;
                    break;
                case "double":
                    return CellValues.Number;
                    break;
                case "datetime":
                    return CellValues.Date;
                    break;
                case "date":
                    return CellValues.Date;
                    break;
                default:
                    return CellValues.String;
                    break;
            }
        }

        public static bool WriteToCellForString(string colIndex, int rowIndex, Worksheet workSheet, string value, string dataType)
        {
            Cell theCell = workSheet.Descendants<Cell>().Where(c => c.CellReference == $"{colIndex}{rowIndex}").FirstOrDefault();
            if (theCell == null)
            {
                theCell = InsertCellInWorksheet(colIndex, Convert.ToUInt32(rowIndex), workSheet, value);
            }

            theCell.DataType = new EnumValue<CellValues>(CellValues.String);
            CellValue cellValue = new CellValue(value);
            theCell.CellValue = cellValue;
            return true;
        }

        public static bool WriteToCellForInteger(string colIndex, int rowIndex, Worksheet workSheet, int value, string dataType)
        {
            Cell theCell = workSheet.Descendants<Cell>().Where(c => c.CellReference == $"{colIndex}{rowIndex}").FirstOrDefault();
            theCell.DataType = new EnumValue<CellValues>(CellValues.Number);
            CellValue cellValue = new CellValue(value.ToString());
            theCell.CellValue = cellValue;
            return true;
        }

        public static bool WriteToCellForLong(string colIndex, int rowIndex, Worksheet workSheet, long? value, string dataType)
        {
            Cell theCell = workSheet.Descendants<Cell>().Where(c => c.CellReference == $"{colIndex}{rowIndex}").FirstOrDefault();
            theCell.DataType = new EnumValue<CellValues>(CellValues.Number);
            CellValue cellValue = new CellValue(Convert.ToString(value));
            theCell.CellValue = cellValue;
            return true;
        }

        public static bool WriteToCellForDate(string colIndex, int rowIndex, Worksheet workSheet, DateTime value, string dataType)
        {
            Cell theCell = workSheet.Descendants<Cell>().Where(c => c.CellReference == $"{colIndex}{rowIndex}").FirstOrDefault();
            theCell.DataType = new EnumValue<CellValues>(CellValues.Date);
            CellValue cellValue = new CellValue(value);
            theCell.CellValue = cellValue;
            return true;
        }

        public static bool WriteToCellForDateNullable(string colIndex, int rowIndex, Worksheet workSheet, DateTime value, string dataType)
        {
            Cell theCell = workSheet.Descendants<Cell>().Where(c => c.CellReference == $"{colIndex}{rowIndex}").FirstOrDefault();
            if (theCell != null)
            {
                theCell.DataType = new EnumValue<CellValues>(CellValues.Date);
                CellValue cellValue = new CellValue(value);
                theCell.CellValue = cellValue;
            }

            return true;
        }

        public static string GetContentType(string path)
        {
            var types = ExcelHelper.GetMimeTypes();
            var ext = Path.GetExtension(path).ToUpperInvariant();
            return types[ext];
        }

        public static Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {
                { ".TXT", "text/plain" },
                { ".PDF", "application/pdf" },
                { ".DOC", "application/vnd.ms-word" },
                { ".DOCX", "application/vnd.ms-word" },
                { ".XLS", "application/vnd.ms-excel" },
                { ".XLSM", "application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet" },
                { ".PNG", "image/png" },
                { ".JPG", "image/jpeg" },
                { ".JPEG", "image/jpeg" },
                { ".GIF", "image/gif" },
                { ".CSV", "text/csv" }
            };
        }

        private static Cell InsertCellInWorksheet(string columnName, uint rowIndex, Worksheet workSheet, string value)
        {
            SheetData sheetData = workSheet.GetFirstChild<SheetData>();
            string cellReference = columnName + rowIndex;

            // If the worksheet does not contain a row with the specified row index, insert one.
            Row row;
            if (sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).Count() != 0)
            {
                row = sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).First();
            }
            else
            {
                row = new Row() { RowIndex = rowIndex };
                sheetData.Append(row);
            }

            // If there is not a cell with the specified column name, insert one.
            if (row.Elements<Cell>().Where(c => c.CellReference.Value == columnName + rowIndex).Count() > 0)
            {
                return row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).First();
            }
            else
            {
                // Cells must be in sequential order according to CellReference. Determine where to insert the new cell.
                Cell refCell = null;
                foreach (Cell cell in row.Elements<Cell>())
                {
                    if (cell.CellReference.Value.Length == cellReference.Length)
                    {
                        if (string.Compare(cell.CellReference.Value, cellReference, true) > 0)
                        {
                            refCell = cell;
                            break;
                        }
                    }
                }

                Cell newCell = new Cell() { CellReference = cellReference };
                row.InsertBefore(newCell, refCell);

                workSheet.Save();
                return newCell;
            }
        }
    }
}
