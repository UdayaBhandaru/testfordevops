﻿// <copyright file="MailUtility.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Core.Utility
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net.Mail;
    using System.Net.Mime;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using Agility.RBS.Core.Models;
    using Agility.RBS.Core.Services;

    public class MailUtility
    {
        private const string EmailPattern = @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
                                           [0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
                                           [0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|([a-zA-Z0-9]+[\w-]+\.)+[a-zA-Z]{1}[a-zA-Z0-9-]{1,23})$";

        private RazorTemplateService razorTemplateService;

        public MailUtility(RazorTemplateService razorTemplateService)
        {
            this.razorTemplateService = razorTemplateService;
        }

        public static async ValueTask<bool> SendEmailMessage(FileInputParametersModel fileInputParametersModel)
        {
            string fromEmail = string.Empty;
            MailMessage mailMessage = new MailMessage();
            foreach (string toMail in fileInputParametersModel.ToMails)
            {
                if (RbSettings.DemoMode)
                {
                    mailMessage.To.Add(RbSettings.DemoEmails);
                }
                else
                {
                    if (Regex.IsMatch(toMail.Trim(), MailUtility.EmailPattern))
                    {
                        mailMessage.To.Add(toMail.Trim());
                    }
                    else
                    {
                        throw new Exception("Recipient email is not in a valid Format");
                    }
                }
            }

            if (RbSettings.ServiceAccountMode)
            {
                fromEmail = RbSettings.ServiceAccountMail;
                mailMessage.From = new MailAddress(RbSettings.ServiceAccountMail, RbSettings.ServiceAccountName);
            }
            else
            {
                fromEmail = fileInputParametersModel.From;
                mailMessage.From = new MailAddress(fileInputParametersModel.From, fileInputParametersModel.DisplayName);
            }

            mailMessage.Subject = string.IsNullOrEmpty(fileInputParametersModel.Subject) ? "User Mail" : fileInputParametersModel.Subject;
            mailMessage.Body = fileInputParametersModel.MessageBody;
            mailMessage.IsBodyHtml = true;

            System.Net.Mail.Attachment attachment;
            attachment = new System.Net.Mail.Attachment("c:/Promotion attachmnt.xlsx");
            mailMessage.Attachments.Add(attachment);
            return await SendEmail(mailMessage, fileInputParametersModel);
        }

        private static async ValueTask<bool> SendEmail(MailMessage mailMessage, FileInputParametersModel fileInputParametersModel)
        {
            bool result = false;
            AlternateView htmlView = AlternateView.CreateAlternateViewFromString(mailMessage.Body, new System.Net.Mime.ContentType("text/html"));
            if (File.Exists(fileInputParametersModel.LogoPath))
            {
                LinkedResource resource = new LinkedResource(fileInputParametersModel.LogoPath);
                resource.ContentId = "rbs-logo";
                htmlView.LinkedResources.Add(resource);
            }

            mailMessage.AlternateViews.Add(htmlView);
            try
            {
                if (!string.IsNullOrWhiteSpace(RbSettings.SmtpServer))
                {
                    SmtpClient server = new SmtpClient(RbSettings.SmtpServer);
                    server.Send(mailMessage);
                }

                result = true;
            }
            catch (System.Exception exception)
            {
                result = false;
            }
            finally
            {
            }

            return result;
        }
    }
}
