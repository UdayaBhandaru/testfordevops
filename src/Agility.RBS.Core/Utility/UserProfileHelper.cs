﻿// <copyright file="UserProfileHelper.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Core.Utility
{
    using System.Data;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Core.Security.Models;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.Framework.Web.Security.Middleware;
    using Devart.Data.Oracle;

    public class UserProfileHelper
    {
        public static UserProfile GetInMemoryUser()
        {
            Framework.Core.Security.Models.UserProfile userProfile = SecurityCacheManager.GetUserProfile();
            return userProfile;
        }

        public static string GetInMemoryUserId()
        {
            return GetInMemoryUser()?.UserId;
        }

        public static BaseOracleParameter GetUserParameter()
        {
            BaseOracleParameter baseOracleParameter = new BaseOracleParameter
            {
                Name = "P_USER",
                DataType = OracleDbType.VarChar,
                Direction = ParameterDirection.Input,
                Value = UserProfileHelper.GetInMemoryUserId()
            };
            return baseOracleParameter;
        }

        public static BaseOracleParameter GetUserName()
        {
            BaseOracleParameter baseOracleParameter = new BaseOracleParameter
            {
                Name = "P_USER",
                DataType = OracleDbType.VarChar,
                Direction = ParameterDirection.Input,
                Value = FxContext.Context.Name
            };
            return baseOracleParameter;
        }
    }
}
