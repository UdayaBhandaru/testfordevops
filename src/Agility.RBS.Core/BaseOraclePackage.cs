﻿// <copyright file="BaseOraclePackage.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Core
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Data;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Common;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Core.Models;
    using Agility.Framework.Web.Core;
    using Agility.Framework.Web.Core.ServiceDocument;
    using AutoMapper;
    using Devart.Data.Oracle;
    using ExcelDataReader;

    public class BaseOraclePackage : OraclePackage
    {
        public BaseOraclePackage()
        {
            this.Connection = new OracleConnection(CoreSettings.CoreConfiguration.ConnectionStrings.App.ConnectionString);
        }

        public static string NumericCellNullCheck(string str)
        {
            if (!string.IsNullOrEmpty(str))
            {
                return str;
            }

            return null;
        }

        public static string UpperCase(string str)
        {
            return str.ToUpper();
        }

        public virtual new OracleConnection Connection { get => base.Connection; set => base.Connection = value; }

        public OracleObject ObjResult { get; private set; }

        public async Task<OracleTable> ExecutePackage(OracleObject recordObject, PackageParams packageParams, OracleParameterCollection parameters = null)
        {
            try
            {
                if (recordObject != null)
                {
                    OracleType tableType = OracleType.GetObjectType(packageParams.TableType, this.Connection);
                    OracleTable pUomClassMstTab = new OracleTable(tableType);
                    pUomClassMstTab.Add(recordObject);
                    if (parameters == null)
                    {
                        parameters = this.Parameters;
                        parameters.Clear();
                    }

                    OracleParameter parameter;
                    parameter = new OracleParameter("RESULT", Devart.Data.Oracle.OracleDbType.Boolean)
                    {
                        Direction = ParameterDirection.ReturnValue
                    };
                    parameters.Add(parameter);
                    parameter = new OracleParameter(packageParams.ObjectTypeTable, Devart.Data.Oracle.OracleDbType.Table)
                    {
                        Direction = ParameterDirection.InputOutput,
                        ObjectTypeName = packageParams.ObjectTypeName,
                        Value = pUomClassMstTab
                    };
                    parameters.Add(parameter);
                }

                await Task.Run(() => this.ExecuteProcedure(packageParams.ProcedureName, parameters));
                if (this.Parameters[packageParams.ObjectTypeTable].Value != DBNull.Value)
                {
                    return (Devart.Data.Oracle.OracleTable)this.Parameters[packageParams.ObjectTypeTable].Value;
                }
                else
                {
                    return null;
                }
            }
            finally
            {
                this.Connection.Close();
            }
        }

        public async Task<OracleTable> GetProcedure(OracleObject recordObject, PackageParams packageParams, ServiceDocumentResult serviceDocumentResult, OracleParameterCollection parameters = null)
        {
            var pOracleTable = await this.ExecutePackage(recordObject, packageParams, parameters);
            if (pOracleTable != null && pOracleTable.Count == 1)
            {
                OracleObject obj = (OracleObject)pOracleTable[0];
                if (obj != null && !string.IsNullOrEmpty(Convert.ToString(obj["P_Error"])))
                {
                    if (serviceDocumentResult != null)
                    {
                        serviceDocumentResult.InnerException = Convert.ToString(obj["Program_Message"]);
                        serviceDocumentResult.StackTrace = Convert.ToString(obj["P_Error"]);
                        serviceDocumentResult.Type = MessageType.Exception;
                    }

                    return null;
                }
            }

            return pOracleTable;
        }

        public async Task<OracleTable> GetProcedureAjaxModel(OracleObject recordObject, PackageParams packageParams, OracleParameterCollection parameters = null)
        {
            return await this.ExecutePackage(recordObject, packageParams, parameters);
        }

        public virtual async Task<List<T>> GetProcedure2<T>(OracleObject recordObject, PackageParams packageParams, ServiceDocumentResult serviceDocumentResult, OracleParameterCollection parameters = null)
        {
            OracleTable pOracleTable = await this.ExecutePackage(recordObject, packageParams, parameters);
            if (pOracleTable != null && pOracleTable.Count == 1)
            {
                OracleObject obj = (OracleObject)pOracleTable[0];
                if (obj != null && !string.IsNullOrEmpty(Convert.ToString(obj["P_Error"])))
                {
                    serviceDocumentResult.InnerException = Convert.ToString(obj["Program_Message"]);
                    serviceDocumentResult.StackTrace = Convert.ToString(obj["P_Error"]);
                    serviceDocumentResult.Type = MessageType.Exception;
                    return null;
                }
            }

            List<T> result = Mapper.Map<List<T>>(pOracleTable);
            return result;
        }

        public virtual async Task<AjaxModel<List<T>>> GetProcedureAjaxModel<T>(OracleObject recordObject, PackageParams packageParams, OracleParameterCollection parameters = null)
        {
            OracleTable pOracleTable = await this.ExecutePackage(recordObject, packageParams, parameters);
            if (pOracleTable != null && pOracleTable.Count == 1)
            {
                OracleObject obj = (OracleObject)pOracleTable[0];
                if (obj != null && !string.IsNullOrEmpty(Convert.ToString(obj["P_Error"])))
                {
                    return new AjaxModel<List<T>> { Result = AjaxResult.ValidationException, Message = Convert.ToString(obj["Program_Message"]), Model = null };
                }
            }

            List<T> result = Mapper.Map<List<T>>(pOracleTable);
            return new AjaxModel<List<T>> { Result = AjaxResult.Success, Model = result };
        }

        public virtual async Task<ServiceDocumentResult> SetProcedure(OracleObject recordObject, PackageParams packageParams, OracleParameterCollection parameters = null)
        {
            ServiceDocumentResult result = new ServiceDocumentResult() { InnerException = string.Empty, StackTrace = string.Empty, Type = MessageType.Success };
            var pOracleTable = await this.ExecutePackage(recordObject, packageParams, parameters);
            if (pOracleTable != null)
            {
                OracleObject obj = (OracleObject)pOracleTable[0];
                if (obj != null && !string.IsNullOrEmpty(Convert.ToString(obj["P_Error"])))
                {
                    result.InnerException = Convert.ToString(obj["Program_Message"]);
                    result.StackTrace = Convert.ToString(obj["P_Error"]);
                    result.Type = MessageType.Exception;
                }
                else
                {
                    this.ObjResult = (OracleObject)obj.Clone();
                    result.InnerException = string.Empty;
                    result.StackTrace = string.Empty;
                    result.Type = MessageType.Success;
                    result.Message = string.Empty;
                }
            }

            return result;
        }

        public virtual async Task<ServiceDocumentResult> SetProcedureBulk(OracleObject recordObject, PackageParams packageParams, OracleParameterCollection parameters = null)
        {
            ServiceDocumentResult result = new ServiceDocumentResult() { InnerException = string.Empty, StackTrace = string.Empty, Type = MessageType.Success };
            var pOracleTable = await this.ExecutePackage(recordObject, packageParams, parameters);
            if (pOracleTable != null)
            {
                OracleObject obj = (OracleObject)pOracleTable[0];
            }

            return result;
        }

        public PackageParams GetPackageParams(string objTypeName, string procedure)
        {
            PackageParams packageParameter = new PackageParams
            {
                ProcedureName = procedure,
                ObjectTypeTable = "P_" + objTypeName,
                ObjectTypeName = objTypeName,
                TableType = objTypeName
            };
            return packageParameter;
        }

        public OracleParameter ParameterBuilder(BaseOracleParameter baseOracleParameter)
        {
            OracleParameter parameter;
            switch (baseOracleParameter.Direction)
            {
                case ParameterDirection.Input:
                    parameter = new OracleParameter(baseOracleParameter.Name, baseOracleParameter.DataType);
                    parameter.Direction = ParameterDirection.Input;
                    parameter.Value = baseOracleParameter.Value;
                    break;
                case ParameterDirection.InputOutput:
                    parameter = new OracleParameter(baseOracleParameter.Name, baseOracleParameter.DataType);
                    parameter.Direction = ParameterDirection.InputOutput;
                    parameter.ObjectTypeName = baseOracleParameter.ObjectTypeName;
                    parameter.Value = baseOracleParameter.Value;
                    break;
                case ParameterDirection.Output:
                    parameter = new OracleParameter(baseOracleParameter.Name, baseOracleParameter.DataType);
                    parameter.Direction = ParameterDirection.Output;
                    parameter.ObjectTypeName = baseOracleParameter.ObjectTypeName;
                    break;
                case ParameterDirection.ReturnValue:
                    parameter = new OracleParameter(baseOracleParameter.Name, baseOracleParameter.DataType);
                    parameter.Direction = ParameterDirection.ReturnValue;
                    break;
                default:
                    parameter = new OracleParameter();
                    break;
            }

            return parameter;
        }

        public string GetCreatedBy(string createdBy)
        {
            return createdBy ?? FxContext.Context.Name;
        }

        public virtual OracleType GetObjectType(string name)
        {
            return OracleType.GetObjectType(name, this.Connection);
        }

        public virtual OracleObject GetOracleObject(OracleType oracleType)
        {
            return new OracleObject(oracleType);
        }

        public virtual OracleTable GetOracleTable(OracleType oracleType)
        {
            return new OracleTable(oracleType);
        }

        public async Task<DataTable> GetDataTableByReadingExcel(string fileName)
        {
            if (System.IO.File.Exists(fileName))
            {
                using (var stream = System.IO.File.Open(fileName, FileMode.Open, FileAccess.Read))
                {
                    using (var reader = ExcelReaderFactory.CreateReader(stream))
                    {
                        return reader.AsDataSet().Tables[0];
                    }
                }
            }

            return null;
        }
    }
}
