﻿// <copyright file="FileUploadException.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Core.CustomExceptions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class FileUploadException : Exception
    {
        public FileUploadException(string exceptionMessage)
            : base(exceptionMessage)
        {
        }
    }
}
