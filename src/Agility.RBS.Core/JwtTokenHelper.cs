﻿// <copyright file="JwtTokenHelper.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Core
{
    using System;
    using System.Collections.Generic;
    using System.IdentityModel.Tokens.Jwt;
    using System.Security.Claims;
    using System.Text;
    using Agility.RBS.Core.Constants;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.IdentityModel.Tokens;

    public static class JwtTokenHelper
    {
        public static string GetToken(string uId, string uName)
        {
            var utcNow = DateTime.Now;
            var claims = new Claim[]
            {
                        new Claim(JwtRegisteredClaimNames.Sub, uId),
                        new Claim(JwtRegisteredClaimNames.UniqueName, uName),
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                        new Claim(JwtRegisteredClaimNames.Iat, utcNow.ToString())
            };
            var jwt = new JwtSecurityToken(
                signingCredentials: new SigningCredentials(
                    new SymmetricSecurityKey(Encoding.ASCII.GetBytes(RbCommonConstants.JwtSigningKey)), SecurityAlgorithms.HmacSha256),
                claims: claims,
                notBefore: utcNow,
                expires: utcNow.AddSeconds(120),
                audience: RbSettings.Web,
                issuer: RbSettings.Web);

            return new JwtSecurityTokenHandler().WriteToken(jwt);
        }
    }
}
