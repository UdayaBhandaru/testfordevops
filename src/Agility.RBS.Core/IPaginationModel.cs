﻿// <copyright file="IPaginationModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Core
{
    public interface IPaginationModel
    {
        int? StartRow { get; set; }

        int? EndRow { get; set; }

        long? TotalRows { get; set; }
    }
}
