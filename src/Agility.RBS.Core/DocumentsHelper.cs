﻿// <copyright file="DocumentsHelper.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Core
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core.CustomExceptions;
    using Agility.RBS.Core.Models;
    using Devart.Data.Oracle;
    using Microsoft.AspNetCore.Mvc;

    public class DocumentsHelper
    {
        public static string FileUpload(string folderName, string fileName, string fileData, string subFolderName = null)
        {
            string filePathToBrowse = string.Empty;
            string subDocumentsPath = string.Empty;
            string documentsServerPhysicalPath = RbSettings.DocumentsServerPhysicalPath;
            string documentsServerVirtualPath = RbSettings.DocumentsServerVirtualPath;

            if (!string.IsNullOrEmpty(subFolderName))
            {
                subDocumentsPath = string.Format(@"\{0}\{1}", folderName, subFolderName);
                filePathToBrowse = string.Format("{0}/{1}/{2}/{3}", documentsServerVirtualPath, folderName, subFolderName, fileName);
            }
            else
            {
                subDocumentsPath = string.Format(@"\{0}", folderName);
                filePathToBrowse = string.Format("{0}/{1}/{2}", documentsServerVirtualPath, folderName, fileName);
            }

            byte[] file = Convert.FromBase64String(fileData);
            string buildFileDirectory = string.Format(@"{0}{1}", documentsServerPhysicalPath, subDocumentsPath);
            System.IO.Directory.CreateDirectory(buildFileDirectory);
            System.IO.File.WriteAllBytes(string.Format(@"{0}\{1}", buildFileDirectory, fileName), file);
            return filePathToBrowse;
        }

        public static string FileUpload(string folderName, string fileName, byte[] fileData, string subFolderName = null)
        {
            string filePathToBrowse = string.Empty;
            string subDocumentsPath = string.Empty;
            string documentsServerPhysicalPath = RbSettings.DocumentsServerPhysicalPath;
            string documentsServerVirtualPath = RbSettings.DocumentsServerVirtualPath;

            if (!string.IsNullOrEmpty(subFolderName))
            {
                subDocumentsPath = string.Format(@"\{0}\{1}", folderName, subFolderName);
                filePathToBrowse = string.Format("{0}/{1}/{2}/{3}", documentsServerVirtualPath, folderName, subFolderName, fileName);
            }
            else
            {
                subDocumentsPath = string.Format(@"\{0}", folderName);
                filePathToBrowse = string.Format("{0}/{1}/{2}", documentsServerVirtualPath, folderName, fileName);
            }

            string buildFileDirectory = string.Format(@"{0}{1}", documentsServerPhysicalPath, subDocumentsPath);
            System.IO.Directory.CreateDirectory(buildFileDirectory);
            System.IO.File.WriteAllBytes(string.Format(@"{0}\{1}", buildFileDirectory, fileName), fileData);
            return filePathToBrowse;
        }

        public static string FileUpload(string folderName, string fileName, Stream fileData, string subFolderName = null)
        {
            string filePathToBrowse = string.Empty;
            string subDocumentsPath = string.Empty;
            string documentsServerPhysicalPath = RbSettings.DocumentsServerPhysicalPath;
            string documentsServerVirtualPath = RbSettings.DocumentsServerVirtualPath;

            if (!string.IsNullOrEmpty(subFolderName))
            {
                subDocumentsPath = string.Format(@"\{0}\{1}", folderName, subFolderName);
                filePathToBrowse = string.Format("{0}/{1}/{2}/{3}", documentsServerVirtualPath, folderName, subFolderName, fileName);
            }
            else
            {
                subDocumentsPath = string.Format(@"\{0}", folderName);
                filePathToBrowse = string.Format("{0}/{1}/{2}", documentsServerVirtualPath, folderName, fileName);
            }

            string buildFileDirectory = string.Format(@"{0}{1}", documentsServerPhysicalPath, subDocumentsPath);
            System.IO.Directory.CreateDirectory(buildFileDirectory);
            using (var fileStream = File.Create(string.Format(@"{0}\{1}", buildFileDirectory, fileName)))
            {
                fileData.Seek(0, SeekOrigin.Begin);
                fileData.CopyTo(fileStream);
            }

            return filePathToBrowse;
        }

        public static string GetBase64FromFile(string filePath)
        {
            byte[] asBytes = File.ReadAllBytes(@filePath);
            string asBase64String = Convert.ToBase64String(asBytes);
            return asBase64String;
        }

        public static string GetFullPathForDownload(string virtualPath)
        {
            string documentsServerPhysicalPath = RbSettings.DocumentsServerPhysicalPath;
            string fullDownloadPath = string.Empty;
            virtualPath = virtualPath.Replace("/Documents", string.Empty).Replace("/", @"\");
            fullDownloadPath = string.Format(@"{0}{1}", documentsServerPhysicalPath, virtualPath);
            return fullDownloadPath;
        }

        public static string GetContentType(string path)
        {
            var types = DocumentsHelper.GetMimeTypes();
            var ext = Path.GetExtension(path).ToUpperInvariant();
            return types[ext];
        }

        public static Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {
                { ".TXT", "text/plain" },
                { ".PDF", "application/pdf" },
                { ".DOC", "application/vnd.ms-word" },
                { ".DOCX", "application/vnd.ms-word" },
                { ".XLS", "application/vnd.ms-excel" },
                { ".XLSM", "application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet" },
                { ".PNG", "image/png" },
                { ".JPG", "image/jpeg" },
                { ".JPEG", "image/jpeg" },
                { ".GIF", "image/gif" },
                { ".CSV", "text/csv" }
            };
        }

        public static async Task<FileResult> GetFilestream(string xlsm, ControllerBase cBase)
        {
            MemoryStream memory = new MemoryStream();
            using (var stream = new FileStream(xlsm, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }

            memory.Position = 0;
            var contentDispositionHeader = new System.Net.Mime.ContentDisposition
            {
                Inline = true,
                FileName = xlsm
            };
            cBase.Response.Headers.Add("Content-Disposition", contentDispositionHeader.ToString());
            var reult = cBase.File(memory, DocumentsHelper.GetContentType(xlsm), Path.GetFileName(xlsm));
            if (File.Exists(xlsm))
            {
                File.Delete(xlsm);
            }

            return reult;
        }
    }
}
