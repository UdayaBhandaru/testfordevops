﻿// <copyright file="PackageParams.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Core
{
    public class PackageParams
    {
        public string PackageName { get; set; }

        public string ProcedureName { get; set; }

        public string ObjectTypeName { get; set; }

        public string ObjectTypeTable { get; set; }

        public string TableType { get; set; }
    }
}
