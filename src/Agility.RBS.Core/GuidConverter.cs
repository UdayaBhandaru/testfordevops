﻿// <copyright file="GuidConverter.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Core
{
    using System;

    public static class GuidConverter
    {
        public static string OracleToDotNet(string text)
        {
            if (!string.IsNullOrEmpty(text))
            {
                byte[] bytes = ParseHex(text);
                Guid guid = new Guid(bytes);
                return guid.ToString("N").ToUpperInvariant();
            }

            return null;
        }

        public static string DotNetToOracle(string text)
        {
            Guid guid = new Guid(text);
            return BitConverter.ToString(guid.ToByteArray()).Replace("-", string.Empty);
        }

        private static byte[] ParseHex(string text)
        {
            byte[] ret = new byte[text.Length / 2];
            for (int i = 0; i < ret.Length; i++)
            {
                ret[i] = Convert.ToByte(text.Substring(i * 2, 2), 16);
            }

            return ret;
        }
    }
}
