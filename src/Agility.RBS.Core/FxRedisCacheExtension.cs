﻿// <copyright file="FxRedisCacheExtension.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Core
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.Caching.Distributed;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Serialization;

    public static class FxRedisCacheExtension
    {
        public static void SetCacheValue<T>(this IDistributedCache distributedCache, string key, T value)
        {
            if (distributedCache != null)
            {
                string json = JsonConvert.SerializeObject(value, new JsonSerializerSettings() { ContractResolver = new JsonIgnoreContractResolver() });
                distributedCache.SetStringAsync(
                        key, json);
            }
        }

        public static T GetCacheValue<T>(this IDistributedCache distributedCache, string key)
        {
            try
            {
                if (distributedCache != null)
                {
                    if (distributedCache.GetAsync(key).Result != null)
                    {
                        var output = distributedCache.GetAsync(key).Result;
                        string result = System.Text.Encoding.UTF8.GetString(output);
                        if (!string.IsNullOrWhiteSpace(result))
                        {
                            return JsonConvert.DeserializeObject<T>(result, new JsonSerializerSettings() { ContractResolver = new JsonIgnoreContractResolver() });
                        }
                    }
                }
            }
            catch (Exception ee)
            {
                throw ee;
            }

            return default(T);
        }

        public static async Task DeleteCacheValue(this IDistributedCache distributedCache, string key)
        {
            try
            {
                if (distributedCache != null)
                {
                    await distributedCache.RemoveAsync(key);
                }
            }
            catch (Exception ee)
            {
                throw ee;
            }
        }
    }
}