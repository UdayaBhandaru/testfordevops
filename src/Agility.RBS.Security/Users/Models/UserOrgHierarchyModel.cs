﻿// <copyright file="UserOrgHierarchyModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Security.Users.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.Framework.Web.Security;
    using Agility.Framework.Web.Security.Models;
    using Agility.RBS.MDM.OrganizationHierarchy.Models;
    using Agility.RBS.MDM.Repositories.Models;

    public class UserOrgHierarchyModel : ProfileEntity
    {
        public int? Id { get; set; }

        public string UserId { get; set; }

        public CompanyModel UserCompanyModel { get; set; }

        public OrgCountryModel UserCountryModel { get; set; }

        public RegionModel UserRegionModel { get; set; }

        public FormatModel UserFormatModel { get; set; }

        public LocationModel UserLocationModel { get; set; }

        public List<RoleModel> RolesRemovedData { get; internal set; }

        public int? CompanyId { get; set; }

        public string CompanyName { get; set; }

        public string CountryCode { get; set; }

        public string CountryName { get; set; }

        public string RegionCode { get; set; }

        public string RegionName { get; set; }

        public string FormatCode { get; set; }

        public string FormatName { get; set; }

        public int? LocationId { get; set; }

        public string LocationName { get; set; }

        public string Error { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }
    }
}
