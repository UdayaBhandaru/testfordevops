﻿// <copyright file="UserModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Security.Users.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.Framework.Web.Security;
    using Agility.Framework.Web.Security.Models;
    using Agility.RBS.MDM.OrganizationHierarchy.Models;
    using Agility.RBS.MDM.Repositories.Models;

    public class UserModel : ProfileEntity, ICloneable
    {
        public UserModel()
        {
            this.CompaniesList = new List<CompanyModel>();
            this.CountriesList = new List<OrgCountryModel>();
            this.RegionsList = new List<RegionModel>();
            this.FormatsList = new List<FormatModel>();
            this.LocationsList = new List<LocationModel>();
            this.RolesList = new List<RoleModel>();
            this.RolesRemovedData = new List<RoleModel>();
            this.UserOrgHierarchyModel = new List<UserOrgHierarchyModel>();
        }

        public string UserId { get; set; }

        public string ReportingMgrId { get; set; }

        public string ReportingMgrName { get; set; }

        public string MgrEmailAddr { get; set; }

        public int? CompanyId { get; set; }

        public string CountryCode { get; set; }

        public string FormatCode { get; set; }

        public string RegionCode { get; set; }

        public int? UsersLocationId { get; set; }

        public string UserPicPath { get; set; }

        public string InputValue { get; set; }

        public string Id { get; set; }

        public string Name { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public string UsersRoleId { get; set; }

        public string FileName { get; set; }

        public string FileDataAsBase64 { get; set; }

        public string Password { get; set; }

        public List<CompanyModel> CompaniesList { get; private set; }

        public List<OrgCountryModel> CountriesList { get; private set; }

        public List<RegionModel> RegionsList { get; private set; }

        public List<FormatModel> FormatsList { get; private set; }

        public List<LocationModel> LocationsList { get; private set; }

        public List<RoleModel> RolesList { get; internal set; }

        public List<RoleModel> RolesRemovedData { get; internal set; }

        public string Status { get; set; }

        public string StatusDesc { get; set; }

        public int? LangId { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Error { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }

        public List<UserOrgHierarchyModel> UserOrgHierarchyModel { get; internal set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
