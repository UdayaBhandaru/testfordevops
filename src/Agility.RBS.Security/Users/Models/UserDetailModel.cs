﻿// <copyright file="UserDetailModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Security.Users.Models
{
    public class UserDetailModel
    {
        public string UserId { get; set; }

        public string UserPicPath { get; set; }

        public string UserName { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Error { get; set; }
    }
}
