﻿// <copyright file="RandomSecureVersion.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Security.Users
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;
    using System.Threading.Tasks;

    internal class RandomSecureVersion : IDisposable
    {
        private readonly RNGCryptoServiceProvider rngProvider = new RNGCryptoServiceProvider();
        private bool disposedValue;

        ~RandomSecureVersion()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            this.Dispose(false);
        }

        public int Next()
        {
            var randomBuffer = new byte[4];
            this.rngProvider.GetBytes(randomBuffer);
            var result = BitConverter.ToInt32(randomBuffer, 0);
            return result;
        }

        public int Next(int maximumValue)
        {
            // Do not use Next() % maximumValue because the distribution is not OK
            return this.Next(0, maximumValue);
        }

        public int Next(int minimumValue, int maximumValue)
        {
            var seed = this.Next();
            return new Random(seed).Next(minimumValue, maximumValue);
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposedValue)
            {
                if (disposing)
                {
                    this.rngProvider.Dispose();
                }

                this.disposedValue = true;
            }
        }
    }
}
