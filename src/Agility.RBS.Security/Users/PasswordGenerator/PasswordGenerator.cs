﻿// <copyright file="PasswordGenerator.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Security.Users
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;

    public class PasswordGenerator : IDisposable
    {
        private readonly string allAvailableChars;

        private readonly RandomSecureVersion randomSecure = new RandomSecureVersion();

        private readonly int minimumNumberOfChars;

        private bool disposedValue;

        static PasswordGenerator()
        {
            // Define characters that are valid and reject ambiguous characters such as ilo, IO and 1 or 0
            AllLowerCaseChars = GetCharRange('a', 'z', exclusiveChars: "ilo");
            AllUpperCaseChars = GetCharRange('A', 'Z', exclusiveChars: "IO");
            AllNumericChars = GetCharRange('2', '9');
            AllSpecialChars = "!@#%*()$?+-=";
        }

        public PasswordGenerator(int minimumLengthPassword, int maximumLengthPassword)
        {
            int minimumLowerCaseChars = 2;
            int minimumUpperCaseChars = 2;
            int minimumNumericChars = 2;
            int minimumSpecialChars = 2;
            this.minimumNumberOfChars = minimumLowerCaseChars + minimumUpperCaseChars +
                                    minimumNumericChars + minimumSpecialChars;
            this.MinimumLengthPassword = minimumLengthPassword;
            this.MaximumLengthPassword = maximumLengthPassword;

            this.MinimumLowerCaseChars = minimumLowerCaseChars;
            this.MinimumUpperCaseChars = minimumUpperCaseChars;
            this.MinimumNumericChars = minimumNumericChars;
            this.MinimumSpecialChars = minimumSpecialChars;

            this.allAvailableChars =
                this.OnlyIfOneCharIsRequired(minimumLowerCaseChars, AllLowerCaseChars) +
                this.OnlyIfOneCharIsRequired(minimumUpperCaseChars, AllUpperCaseChars) +
                this.OnlyIfOneCharIsRequired(minimumNumericChars, AllNumericChars) +
                this.OnlyIfOneCharIsRequired(minimumSpecialChars, AllSpecialChars);
        }

        ~PasswordGenerator()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            this.Dispose(false);
        }

        public static string AllLowerCaseChars { get; private set; }

        public static string AllUpperCaseChars { get; private set; }

        public static string AllNumericChars { get; private set; }

        public static string AllSpecialChars { get; private set; }

        public int MinimumLengthPassword { get; private set; }

        public int MaximumLengthPassword { get; private set; }

        public int MinimumLowerCaseChars { get; private set; }

        public int MinimumUpperCaseChars { get; private set; }

        public int MinimumNumericChars { get; private set; }

        public int MinimumSpecialChars { get; private set; }

        public string Generate()
        {
            var lengthOfPassword = this.randomSecure.Next(this.MinimumLengthPassword, this.MaximumLengthPassword);

            var minimumChars = this.GetRandomString(AllLowerCaseChars, this.MinimumLowerCaseChars) +
                            this.GetRandomString(AllUpperCaseChars, this.MinimumUpperCaseChars) +
                            this.GetRandomString(AllNumericChars, this.MinimumNumericChars) +
                            this.GetRandomString(AllSpecialChars, this.MinimumSpecialChars);
            var rest = this.GetRandomString(this.allAvailableChars, lengthOfPassword - minimumChars.Length);
            var unshuffeledResult = minimumChars + rest;

            // Shuffle the result so the order of the characters are unpredictable
            var result = unshuffeledResult.ShuffleTextSecure();
            return result;
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposedValue)
            {
                if (disposing)
                {
                    this.randomSecure.Dispose();
                }

                this.disposedValue = true;
            }
        }

        private static string GetCharRange(char minimum, char maximum, string exclusiveChars = "")
        {
            var result = new StringBuilder();
            for (char value = minimum; value <= maximum; value++)
            {
                result.Append(value);
            }

            if (!string.IsNullOrEmpty(exclusiveChars))
            {
                var inclusiveChars = result.ToString().Except(exclusiveChars).ToArray();
                result.Append(new string(inclusiveChars));
            }

            return result.ToString();
        }

        private string OnlyIfOneCharIsRequired(int minimum, string allChars)
        {
            return minimum > 0 || this.minimumNumberOfChars == 0 ? allChars : string.Empty;
        }

        private string GetRandomString(string possibleChars, int lenght)
        {
            var result = new StringBuilder();
            for (var position = 0; position < lenght; position++)
            {
                var index = this.randomSecure.Next(possibleChars.Length);
                result.Append(possibleChars[index]);
            }

            return result.ToString();
        }
    }
}