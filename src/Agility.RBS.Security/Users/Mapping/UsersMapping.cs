﻿// <copyright file="UsersMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Security.Users.Mapping
{
    using System;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.OrganizationHierarchy.Models;
    using Agility.RBS.MDM.ValueAddedTax.Models;
    using Agility.RBS.Security.Account.Models;
    using Agility.RBS.Security.Users.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class UsersMapping : Profile
    {
        public UsersMapping()
            : base("UsersMapping")
        {
            this.CreateMap<OracleObject, UserModel>()
                .ForMember(m => m.UserId, opt => opt.MapFrom(r => r["USER_ID"]))
                .ForMember(m => m.UserName, opt => opt.MapFrom(r => r["USER_NAME"]))
                .ForMember(m => m.Email, opt => opt.MapFrom(r => r["USER_EMAIL_ADDR"]))
                .ForMember(m => m.ReportingMgrId, opt => opt.MapFrom(r => r["REPORTING_MGR_ID"]))
                .ForMember(m => m.ReportingMgrName, opt => opt.MapFrom(r => r["REPORTING_MGR_NAME"]))
                .ForMember(m => m.MgrEmailAddr, opt => opt.MapFrom(r => r["MGR_EMAIL_ADDR"]))
                .ForMember(m => m.UsersRoleId, opt => opt.MapFrom(r => r["ROLE"]))
                .ForMember(m => m.Status, opt => opt.MapFrom(r => r["STATUS"]))
                .ForMember(m => m.StatusDesc, opt => opt.MapFrom(r => r["STATUS_DESC"]))
                .ForMember(m => m.UserPicPath, opt => opt.MapFrom(r => r["USER_PIC_URL"]))
                .ForMember(m => m.CreatedBy, opt => opt.MapFrom(r => r["CREATED_BY"]))
                .ForMember(m => m.ModifiedBy, opt => opt.MapFrom(r => r["LAST_UPDATED_BY"]))
                .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
                .ForMember(m => m.CreatedDate, opt => opt.MapFrom(r => r["CREATED_DATE"]))
                .ForMember(m => m.ModifiedDate, opt => opt.MapFrom(r => r["LAST_UPDATED_DATE"]))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "USERS_LANDING_TAB"))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, Agility.Framework.Web.Security.Models.RoleModel>()
             .ForMember(m => m.Id, opt => opt.MapFrom(r => r["ROLE_ID"]))
             .ForMember(m => m.Name, opt => opt.MapFrom(r => r["ROLE_NAME"]));

            this.CreateMap<OracleObject, UserOrgHierarchyModel>()
             .ForMember(m => m.Id, opt => opt.MapFrom(r => r["ID"]))
             .ForMember(m => m.UserId, opt => opt.MapFrom(r => r["USER_ID"]))
             .ForMember(m => m.CompanyId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["COMPANY_ID"])))
             .ForMember(m => m.CompanyName, opt => opt.MapFrom(r => r["COMPANY_NAME"]))
             .ForMember(m => m.CountryCode, opt => opt.MapFrom(r => r["COUNTRY_CODE"]))
             .ForMember(m => m.CountryName, opt => opt.MapFrom(r => r["COUNTRY_NAME"]))
             .ForMember(m => m.RegionCode, opt => opt.MapFrom(r => r["REGION_CODE"]))
             .ForMember(m => m.RegionName, opt => opt.MapFrom(r => r["REGION_NAME"]))
             .ForMember(m => m.FormatCode, opt => opt.MapFrom(r => r["FORMAT_CODE"]))
             .ForMember(m => m.FormatName, opt => opt.MapFrom(r => r["FORMAT_NAME"]))
             .ForMember(m => m.LocationId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["LOCATION_ID"])))
             .ForMember(m => m.LocationName, opt => opt.MapFrom(r => r["LOCATION_NAME"]))
             .ForMember(m => m.CreatedBy, opt => opt.MapFrom(r => r["CREATED_BY"]))
             .ForMember(m => m.ModifiedBy, opt => opt.MapFrom(r => r["LAST_UPDATED_BY"]))
             .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
             .ForMember(m => m.CreatedDate, opt => opt.MapFrom(r => r["CREATED_DATE"]))
             .ForMember(m => m.ModifiedDate, opt => opt.MapFrom(r => r["LAST_UPDATED_DATE"]))
             .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, UserDetailModel>()
                .ForMember(m => m.UserId, opt => opt.MapFrom(r => r["USER_ID"]))
                .ForMember(m => m.UserName, opt => opt.MapFrom(r => r["USER_NAME"]))
                .ForMember(m => m.UserPicPath, opt => opt.MapFrom(r => r["USER_PIC_URL"]))
                .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]));
        }
    }
}
