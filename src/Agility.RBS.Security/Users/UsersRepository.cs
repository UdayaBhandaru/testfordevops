﻿// <copyright file="UsersRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Security.Users
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.Framework.Web.Security.Domain;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.OrganizationHierarchy.Models;
    using Agility.RBS.MDM.Repositories.Models;
    using Agility.RBS.MDM.ValueAddedTax.Models;
    using Agility.RBS.Security.Account.Models;
    using Agility.RBS.Security.Common;
    using Agility.RBS.Security.Users.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class UsersRepository : BaseOraclePackage
    {
        public UsersRepository()
        {
            this.PackageName = RbsSecurityConstants.GetSecurityUsersPackage;
        }

        public async Task<List<UserModel>> GetUsers(UserModel usersModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(RbsSecurityConstants.ObjTypeUsers, RbsSecurityConstants.GetProcUsers);
            OracleObject recordObject = this.SetParamsUsers(usersModel, true);
            OracleTable pGetUsersMstTab = await this.GetProcedure(recordObject, packageParameter, serviceDocumentResult);
            List<UserModel> listUserModel = new List<UserModel>();
            var lstHeader = Mapper.Map<List<UserModel>>(pGetUsersMstTab);
            if (lstHeader != null)
            {
                for (int i = 0; i < lstHeader.Count; i++)
                {
                    OracleObject obj = (OracleObject)pGetUsersMstTab[i];
                    OracleTable rolesDetails = (Devart.Data.Oracle.OracleTable)obj["USER_ROLE"];
                    OracleTable orgHierachyList = (Devart.Data.Oracle.OracleTable)obj["USER_ORG_HIERARCHY"];
                    List<UserOrgHierarchyModel> orgModel = Mapper.Map<List<UserOrgHierarchyModel>>(orgHierachyList);
                    List<Agility.Framework.Web.Security.Models.RoleModel> rolesModel = Mapper.Map<List<Agility.Framework.Web.Security.Models.RoleModel>>(rolesDetails);
                    foreach (var item in orgModel)
                    {
                        lstHeader[i].UserOrgHierarchyModel = (from c in orgModel
                                                              select PrepareUserOrgModel(c)).ToList();
                    }

                    lstHeader[i].RolesList.AddRange(rolesModel);
                }
            }

            if (lstHeader != null && lstHeader.Count > 0)
            {
                PrepareRolesList(listUserModel, lstHeader);
            }

            return listUserModel;
        }

        public async Task<UserDetailModel> GetUserDetails(string userId, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType("USER_NAME_PIC_REC", this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["USER_ID"]] = userId;
            PackageParams packageParameter = this.GetPackageParams(RbsSecurityConstants.ObjTypeUserPic, RbsSecurityConstants.GetProcUserPic);
            List<UserDetailModel> lstHeader = await this.GetProcedure2<UserDetailModel>(recordObject, packageParameter, serviceDocumentResult);
            return lstHeader != null ? lstHeader[0] : null;
        }

        public async Task<ServiceDocumentResult> SetUsers(UserModel usersModel, bool isSearch)
        {
            PackageParams packageParameter = this.GetPackageParams(RbsSecurityConstants.ObjTypeUsers, RbsSecurityConstants.SetProcUsers);
            OracleObject recordObject = this.SetParamsUsers(usersModel, isSearch);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async ValueTask<bool> IsExistingUsers(string userName, string email)
        {
            int result = 0;
            bool res = false;

            if (!string.IsNullOrEmpty(userName))
            {
                OracleCommand myCommand = new OracleCommand("Select count(1) from USERS where USER_NAME='" + userName + "'" + "or USER_EMAIL_ADDR='" + email + "'");
                result = await this.PrepareOracleExecuteScalrCommand(myCommand);
                res = result > 0;
                if (!res)
                {
                    myCommand = new OracleCommand("Select count(1) from USERS where USER_NAME='" + userName + "'" + "and USER_EMAIL_ADDR='" + email + "'");
                    result = await this.PrepareOracleExecuteScalrCommand(myCommand);
                    res = result > 0;
                }
            }

            return res;
        }

        public async ValueTask<bool> IsExistingUserId(string email)
        {
            int result;
            OracleCommand myCommand = new OracleCommand("Select count(1) from sec.IdentityUser where Email= '" + email + "'");
            myCommand.Connection = this.Connection;
            this.Connection.Open();
            result = Convert.ToInt32(await myCommand.ExecuteScalarAsync());
            this.Connection.Close();
            return result > 0;
        }

        private static void PrepareRolesList(ICollection<UserModel> listUserModel, IEnumerable<UserModel> lstHeader)
        {
            var users = lstHeader.Select(u => u.UserId).Distinct().ToList();
            List<Framework.Web.Security.Models.RoleModel> rolesList = null;
            foreach (var item in users)
            {
                UserModel um = null;
                var filteredUser = lstHeader.Where(w => w.UserId == item).ToList();
                um = (UserModel)filteredUser.FirstOrDefault().Clone();
                rolesList = new List<Framework.Web.Security.Models.RoleModel>();
                foreach (var fusr in filteredUser)
                {
                    foreach (var rl in fusr.RolesList)
                    {
                        rolesList.Add(rl);
                    }
                }

                um.RolesList = rolesList;
                listUserModel.Add(um);
            }
        }

        private static UserOrgHierarchyModel PrepareUserOrgModel(UserOrgHierarchyModel c)
        {
            return new UserOrgHierarchyModel
            {
                UserCompanyModel = SetCompanyModel(c),

                UserCountryModel = SetCountryModel(c),
                UserRegionModel = (c.LocationId == 0 || c.LocationId is null) && !string.IsNullOrEmpty(c.RegionCode) ? new RegionModel
                {
                    CompanyId = c.CompanyId,
                    CompanyName = c.CompanyName,
                    CountryCode = c.CountryCode,
                    CountryName = c.CountryName,
                    RegionCode = c.RegionCode,
                    RegionName = c.RegionName
                }
                : null,

                UserFormatModel = (c.LocationId == 0 || c.LocationId is null) && !string.IsNullOrEmpty(c.FormatCode) ? new FormatModel
                {
                    CompanyId = c.CompanyId,
                    CompanyName = c.CompanyName,
                    CountryCode = c.CountryCode,
                    CountryName = c.CountryName,
                    RegionCode = c.RegionCode,
                    RegionName = c.RegionName,
                    FormatCode = c.FormatCode,
                    FormatName = c.FormatName
                }
                : null,

                UserLocationModel = c.LocationId > 0 ? new LocationModel
                {
                    CompanyId = c.CompanyId,
                    CompanyName = c.CompanyName,
                    CountryCode = c.CountryCode,
                    CountryName = c.CountryName,
                    RegionCode = c.RegionCode,
                    RegionName = c.RegionName,
                    FormatCode = c.FormatCode,
                    FormatName = c.FormatName,
                    LocationId = c.LocationId,
                    LocationName = c.LocationName
                }
                : null,
                Id = c.Id
            };
        }

        private static CompanyModel SetCompanyModel(UserOrgHierarchyModel c)
        {
            return (c.LocationId == 0 || c.LocationId is null) && c.CompanyId > 0 ? new CompanyModel { Company = c.CompanyId, CoName = c.CompanyName } : null;
        }

        private static OrgCountryModel SetCountryModel(UserOrgHierarchyModel c)
        {
            return (c.LocationId == 0 || c.LocationId is null) && !string.IsNullOrEmpty(c.CountryCode) ? new OrgCountryModel
            {
                CompanyId = c.CompanyId,
                CompanyName = c.CompanyName,
                CountryCode = c.CountryCode,
                CountryName = c.CountryName
            }
            : null;
        }

        private static void SetUserOrgHierarchyLocation(OracleType dtlRecordType, UserOrgHierarchyModel item, OracleObject dtlRecordObject)
        {
            if (item.UserLocationModel != null && item.UserLocationModel.Operation != "D")
            {
                dtlRecordObject[dtlRecordType.Attributes["COMPANY_ID"]] = item.UserLocationModel.CompanyId;
                dtlRecordObject[dtlRecordType.Attributes["COMPANY_NAME"]] = item.UserLocationModel.CompanyName;
                dtlRecordObject[dtlRecordType.Attributes["COUNTRY_CODE"]] = item.UserLocationModel.CountryCode;
                dtlRecordObject[dtlRecordType.Attributes["COUNTRY_NAME"]] = item.UserLocationModel.CountryName;
                dtlRecordObject[dtlRecordType.Attributes["REGION_CODE"]] = item.UserLocationModel.RegionCode;
                dtlRecordObject[dtlRecordType.Attributes["REGION_NAME"]] = item.UserLocationModel.RegionName;
                dtlRecordObject[dtlRecordType.Attributes["FORMAT_CODE"]] = item.UserLocationModel.FormatCode;
                dtlRecordObject[dtlRecordType.Attributes["FORMAT_NAME"]] = item.UserLocationModel.FormatName;
                dtlRecordObject[dtlRecordType.Attributes["LOCATION_ID"]] = item.UserLocationModel.LocationId;
                dtlRecordObject[dtlRecordType.Attributes["LOCATION_NAME"]] = item.UserLocationModel.LocationName;
                dtlRecordObject[dtlRecordType.Attributes["Operation"]] = "I";
            }
            else
            {
                dtlRecordObject[dtlRecordType.Attributes["LOCATION_ID"]] = null;
                dtlRecordObject[dtlRecordType.Attributes["LOCATION_NAME"]] = null;
                dtlRecordObject[dtlRecordType.Attributes["COMPANY_ID"]] = null;
                dtlRecordObject[dtlRecordType.Attributes["COMPANY_NAME"]] = null;
                dtlRecordObject[dtlRecordType.Attributes["COUNTRY_CODE"]] = null;
                dtlRecordObject[dtlRecordType.Attributes["COUNTRY_NAME"]] = null;
                dtlRecordObject[dtlRecordType.Attributes["REGION_CODE"]] = null;
                dtlRecordObject[dtlRecordType.Attributes["REGION_NAME"]] = null;
                dtlRecordObject[dtlRecordType.Attributes["FORMAT_CODE"]] = null;
                dtlRecordObject[dtlRecordType.Attributes["FORMAT_NAME"]] = null;
                dtlRecordObject[dtlRecordType.Attributes["Operation"]] = "I";
            }
        }

        private static void SetUserOrgHierarchyCompany(OracleType dtlRecordType, UserOrgHierarchyModel item, OracleObject dtlRecordObject)
        {
            if (item.UserCompanyModel != null && item.UserCompanyModel.Operation != "D")
            {
                dtlRecordObject[dtlRecordType.Attributes["COMPANY_ID"]] = item.UserCompanyModel.Company;
                dtlRecordObject[dtlRecordType.Attributes["COMPANY_NAME"]] = item.UserCompanyModel.CoName;
                dtlRecordObject[dtlRecordType.Attributes["Operation"]] = "I";
            }
            else
            {
                dtlRecordObject[dtlRecordType.Attributes["COMPANY_ID"]] = null;
                dtlRecordObject[dtlRecordType.Attributes["COMPANY_NAME"]] = string.Empty;
                dtlRecordObject[dtlRecordType.Attributes["Operation"]] = "I";
            }
        }

        private static void SetUserOrgHierarchyCountry(OracleType dtlRecordType, UserOrgHierarchyModel item, OracleObject dtlRecordObject)
        {
            if (item.UserCountryModel != null && item.UserCountryModel.Operation != "D")
            {
                dtlRecordObject[dtlRecordType.Attributes["COUNTRY_CODE"]] = item.UserCountryModel.CountryCode;
                dtlRecordObject[dtlRecordType.Attributes["COUNTRY_NAME"]] = item.UserCountryModel.CountryName;
                dtlRecordObject[dtlRecordType.Attributes["Operation"]] = "I";
            }
            else
            {
                dtlRecordObject[dtlRecordType.Attributes["COUNTRY_CODE"]] = string.Empty;
                dtlRecordObject[dtlRecordType.Attributes["COUNTRY_NAME"]] = string.Empty;
                dtlRecordObject[dtlRecordType.Attributes["Operation"]] = "I";
            }
        }

        private static void SetUserOrgHierarchyReagion(OracleType dtlRecordType, UserOrgHierarchyModel item, OracleObject dtlRecordObject)
        {
            if (item.UserRegionModel != null && item.UserRegionModel.Operation != "D")
            {
                dtlRecordObject[dtlRecordType.Attributes["REGION_CODE"]] = item.UserRegionModel.RegionCode;
                dtlRecordObject[dtlRecordType.Attributes["REGION_NAME"]] = item.UserRegionModel.RegionName;
                dtlRecordObject[dtlRecordType.Attributes["Operation"]] = "I";
            }
            else
            {
                dtlRecordObject[dtlRecordType.Attributes["REGION_CODE"]] = string.Empty;
                dtlRecordObject[dtlRecordType.Attributes["REGION_NAME"]] = string.Empty;
                dtlRecordObject[dtlRecordType.Attributes["Operation"]] = "I";
            }
        }

        private static void SetUserOrgHierarchyFormat(OracleType dtlRecordType, UserOrgHierarchyModel item, OracleObject dtlRecordObject)
        {
            if (item.UserFormatModel != null && item.UserFormatModel.Operation != "D")
            {
                dtlRecordObject[dtlRecordType.Attributes["FORMAT_CODE"]] = item.UserFormatModel.FormatCode;
                dtlRecordObject[dtlRecordType.Attributes["FORMAT_NAME"]] = item.UserFormatModel.FormatName;
                dtlRecordObject[dtlRecordType.Attributes["Operation"]] = "I";
            }
            else
            {
                dtlRecordObject[dtlRecordType.Attributes["FORMAT_CODE"]] = string.Empty;
                dtlRecordObject[dtlRecordType.Attributes["FORMAT_NAME"]] = string.Empty;
                dtlRecordObject[dtlRecordType.Attributes["Operation"]] = "I";
            }
        }

        private async Task<int> PrepareOracleExecuteScalrCommand(OracleCommand myCommand)
        {
            int result;
            myCommand.Connection = this.Connection;
            this.Connection.Open();
            result = Convert.ToInt32(await myCommand.ExecuteScalarAsync());
            this.Connection.Close();
            return result;
        }

        private OracleObject SetParamsUsers(UserModel usersModel, bool isSerach)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType("USERS_REC", this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            return this.SetGetUsersParamas(usersModel, recordType, recordObject, isSerach);
        }

        private OracleObject SetGetUsersParamas(UserModel usersModel, OracleType recordType, OracleObject recordObject, bool isSerach)
        {
            if (isSerach)
            {
                UserOrgHierarchyModel model = new UserOrgHierarchyModel();
                {
                    if (usersModel.UsersLocationId != null)
                    {
                        model.UserLocationModel = new LocationModel
                        {
                            CompanyId = null,
                            CompanyName = string.Empty,
                            CountryCode = string.Empty,
                            CountryName = string.Empty,
                            RegionCode = string.Empty,
                            RegionName = string.Empty,
                            FormatCode = string.Empty,
                            FormatName = string.Empty,
                            LocationId = usersModel.UsersLocationId,
                            LocationName = string.Empty
                        };
                        model.UserCompanyModel = new CompanyModel
                        {
                            Company = null,
                            CoName = string.Empty
                        };
                        model.UserCountryModel = new OrgCountryModel
                        {
                            CompanyId = null,
                            CompanyName = string.Empty,
                            CountryCode = string.Empty,
                            CountryName = string.Empty
                        };

                        model.UserRegionModel = new RegionModel
                        {
                            CompanyId = null,
                            CompanyName = string.Empty,
                            CountryCode = string.Empty,
                            CountryName = string.Empty,
                            RegionCode = string.Empty,
                            RegionName = string.Empty
                        };
                        model.UserFormatModel = new FormatModel
                        {
                            CompanyId = null,
                            CompanyName = string.Empty,
                            CountryCode = string.Empty,
                            CountryName = string.Empty,
                            RegionCode = string.Empty,
                            RegionName = string.Empty,
                            FormatCode = string.Empty,
                            FormatName = string.Empty
                        };
                    }
                    else
                    {
                        model.UserCompanyModel = new CompanyModel
                        {
                            Company = usersModel.CompanyId,
                            CoName = string.Empty
                        };
                        model.UserCountryModel = new OrgCountryModel
                        {
                            CompanyId = usersModel.CompanyId,
                            CompanyName = string.Empty,
                            CountryCode = usersModel.CountryCode,
                            CountryName = string.Empty
                        };

                        model.UserRegionModel = new RegionModel
                        {
                            CompanyId = usersModel.CompanyId,
                            CompanyName = string.Empty,
                            CountryCode = usersModel.CountryCode,
                            CountryName = string.Empty,
                            RegionCode = usersModel.RegionCode,
                            RegionName = string.Empty
                        };
                        model.UserFormatModel = new FormatModel
                        {
                            CompanyId = usersModel.CompanyId,
                            CompanyName = string.Empty,
                            CountryCode = usersModel.CountryCode,
                            CountryName = string.Empty,
                            RegionCode = usersModel.RegionCode,
                            RegionName = string.Empty,
                            FormatCode = usersModel.FormatCode,
                            FormatName = string.Empty
                        };
                        model.UserLocationModel = new LocationModel
                        {
                            CompanyId = null,
                            CompanyName = string.Empty,
                            CountryCode = string.Empty,
                            CountryName = string.Empty,
                            RegionCode = string.Empty,
                            RegionName = string.Empty,
                            FormatCode = string.Empty,
                            FormatName = string.Empty,
                            LocationId = null,
                            LocationName = string.Empty
                        };
                    }
                }

                usersModel.UserOrgHierarchyModel.Add(model);
            }

            recordObject[recordType.Attributes["CREATED_BY"]] = FxContext.Context.Name;
            recordObject[recordType.Attributes["LAST_UPDATED_BY"]] = FxContext.Context.Name;
            recordObject[recordType.Attributes["Operation"]] = usersModel.Operation;
            recordObject[recordType.Attributes["STATUS"]] = usersModel.Status;
            OracleTable pUsersOrgHierarchyTab;
            if (isSerach)
            {
                pUsersOrgHierarchyTab = this.UsersOrgHierachyParamsObject(usersModel);
            }
            else
            {
                pUsersOrgHierarchyTab = this.UsersOrgHierachyParamsObject(usersModel);
            }

            recordObject[recordType.Attributes["USER_ORG_HIERARCHY"]] = pUsersOrgHierarchyTab;

            OracleType dtlTableType = OracleType.GetObjectType("USER_ROLE_TAB", this.Connection);
            OracleType dtlRecordType = OracleType.GetObjectType("USER_ROLE_REC", this.Connection);
            OracleTable pUsersTab = new OracleTable(dtlTableType);
            foreach (Agility.Framework.Web.Security.Models.RoleModel rolesModel in usersModel.RolesList)
            {
                var dtlRecordObject = new OracleObject(dtlRecordType);
                dtlRecordObject[dtlRecordType.Attributes["USER_ID"]] = usersModel.UserId;
                dtlRecordObject[dtlRecordType.Attributes["ROLE_ID"]] = rolesModel.Id;
                dtlRecordObject[dtlRecordType.Attributes["ROLE_NAME"]] = rolesModel.Name;
                dtlRecordObject[dtlRecordType.Attributes["PROGRAM_PHASE"]] = usersModel.ProgramPhase;
                dtlRecordObject[dtlRecordType.Attributes["PROGRAM_MESSAGE"]] = usersModel.ProgramMessage;
                dtlRecordObject[dtlRecordType.Attributes["P_ERROR"]] = usersModel.Error;
                dtlRecordObject[dtlRecordType.Attributes["Operation"]] = "I";
                pUsersTab.Add(dtlRecordObject);
            }

            recordObject[recordType.Attributes["USER_ROLE"]] = pUsersTab;
            recordObject[recordType.Attributes["ROLE"]] = usersModel.UsersRoleId;
            recordObject[recordType.Attributes["USER_ID"]] = usersModel.UserId;
            recordObject[recordType.Attributes["USER_NAME"]] = usersModel.UserName;
            recordObject[recordType.Attributes["USER_EMAIL_ADDR"]] = usersModel.Email;
            recordObject[recordType.Attributes["REPORTING_MGR_ID"]] = usersModel.ReportingMgrId;
            recordObject[recordType.Attributes["USER_PIC_URL"]] = usersModel.UserPicPath;
            return recordObject;
        }

        private OracleTable UsersOrgHierachyParamsObject(UserModel usersModel)
        {
            OracleType dtlTableType = OracleType.GetObjectType("USER_ORG_HIERARCHY_TAB", this.Connection);
            OracleType dtlRecordType = OracleType.GetObjectType("USER_ORG_HIERARCHY_REC", this.Connection);
            OracleTable pCompaniesTab = new OracleTable(dtlTableType);

            if (usersModel.UserOrgHierarchyModel != null && usersModel.UserOrgHierarchyModel.Count > 0)
            {
                foreach (var item in usersModel.UserOrgHierarchyModel)
                {
                    var dtlRecordObject = new OracleObject(dtlRecordType);
                    this.SetUserHierarchyDetailsRecordObject(dtlRecordType, item, dtlRecordObject, usersModel);
                    pCompaniesTab.Add(dtlRecordObject);
                }
            }

            return pCompaniesTab;
        }

        private void SetUserHierarchyDetailsRecordObject(OracleType dtlRecordType, UserOrgHierarchyModel item, OracleObject dtlRecordObject, UserModel userModel)
        {
            if (item.UserCompanyModel.Company == null)
            {
                SetUserOrgHierarchyLocation(dtlRecordType, item, dtlRecordObject);
            }
            else
            {
                SetUserOrgHierarchyCompany(dtlRecordType, item, dtlRecordObject);
                SetUserOrgHierarchyCountry(dtlRecordType, item, dtlRecordObject);
                SetUserOrgHierarchyReagion(dtlRecordType, item, dtlRecordObject);
                SetUserOrgHierarchyFormat(dtlRecordType, item, dtlRecordObject);
            }

            dtlRecordObject[dtlRecordType.Attributes["USER_ID"]] = userModel.UserId;
            dtlRecordObject[dtlRecordType.Attributes["CREATED_BY"]] = this.GetCreatedBy(item.CreatedBy);
            dtlRecordObject[dtlRecordType.Attributes["LAST_UPDATED_BY"]] = FxContext.Context.Name;
            dtlRecordObject[dtlRecordType.Attributes["PROGRAM_PHASE"]] = userModel.ProgramPhase;
            dtlRecordObject[dtlRecordType.Attributes["PROGRAM_MESSAGE"]] = userModel.ProgramMessage;
            dtlRecordObject[dtlRecordType.Attributes["P_ERROR"]] = userModel.Error;
        }
    }
}
