﻿//-------------------------------------------------------------------------------------------------
// <copyright file="UsersController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UsersController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.Security.Users
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Transactions;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.Framework.Web.Security.Domain;
    using Agility.Framework.Web.Security.Models;
    using Agility.Framework.Web.Security.Repositories;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Models;
    using Agility.RBS.Core.Services;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.ValueAddedTax.Models;
    using Agility.RBS.Security.Account.Models;
    using Agility.RBS.Security.Common;
    using Agility.RBS.Security.Users.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;

    [AllowAnonymous]
    public class UsersController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocument<Models.UserModel> serviceDocument;
        private readonly UsersRepository usersRepository;
        private readonly AccountManager accountDomain;
        private readonly RazorTemplateService razorTemplateService;

        public UsersController(
            ServiceDocument<Models.UserModel> serviceDocument,
            UsersRepository usersRepository,
            DomainDataRepository domainDataRepository,
            RazorTemplateService razorTemplateService,
            AccountManager accountDomain)
        {
            this.usersRepository = usersRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
            this.accountDomain = accountDomain;
            this.razorTemplateService = razorTemplateService;
        }

        public async Task<ServiceDocument<Models.UserModel>> List()
        {
            return await this.UsersDomainData();
        }

        public async Task<ServiceDocument<Models.UserModel>> New()
        {
            return await this.UsersDomainData();
        }

        public async Task<ServiceDocument<Models.UserModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.UsersSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<Models.UserModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.UsersSave);
            return this.serviceDocument;
        }

        public async Task<UserDetailModel> GetUserDetails(string userId)
        {
            try
            {
               return await this.usersRepository.GetUserDetails(userId, this.serviceDocument.Result);
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };

                return null;
            }
        }

        public async ValueTask<bool> IsExistingUsers(string userName, string email)
        {
            return await this.usersRepository.IsExistingUsers(userName, email);
        }

        public async ValueTask<bool> IsExistingUserId(string email)
        {
            return await this.usersRepository.IsExistingUserId(email);
        }

        public async ValueTask<bool> SendMail(string email, string password)
        {
            bool result = false;
            string expectedEmail = string.Empty;
            int endIndex;
            int startIndex = 0;
            endIndex = email.IndexOf('@');
            if (endIndex > 0)
            {
                int length = endIndex - startIndex;
                expectedEmail = email.Substring(startIndex, length);
            }

            UserMailModel emailTemplateModel = new UserMailModel
            {
                LogoPath = RbSettings.UserLogoPath,
                Email = email,
                ApplicationLoginPath = $"{RbSettings.WebRootPath}/Account/Login/" + expectedEmail,
                CreatedDate = DateTime.Now,
                Subject = "Welcome To RBS System",
                CreatorName = expectedEmail,
                Password = password
            };

            string template = await this.razorTemplateService.GetHtmlAsync("~/Views/EmailTemplate/UserCreationMail.cshtml", emailTemplateModel);
            result = await MailUtility.SendEmailMessage(
                new FileInputParametersModel
                {
                    From = RbSettings.ServiceAccountMail,
                    DisplayName = string.Empty,
                    ToMails = new[] { email },
                    MessageBody = template,
                    Subject = "Welcome To RBS System",
                    ProfileInstansId = string.Empty,
                    LogoPath = emailTemplateModel.LogoPath,
                });

            return result;
        }

        private static SecurityDbContext GetSecurityDbContext()
        {
            SecurityDbContext securityDbContext = (SecurityDbContext)FxWebContext.HttpContext.RequestServices.GetService(typeof(SecurityDbContext));
            return securityDbContext;
        }

        private static void AddFileData(string imagePath, Models.UserModel item)
        {
            if (System.IO.File.Exists(imagePath))
            {
                string base64 = DocumentsHelper.GetBase64FromFile(imagePath);
                item.FileDataAsBase64 = base64;
                item.FileName = System.IO.Path.GetFileName(imagePath);
            }
        }

        private async Task<List<Models.UserModel>> UsersSearch()
        {
            try
            {
                ServiceDocumentResult serviceDocumentResult = new ServiceDocumentResult
                {
                    InnerException = string.Empty,
                    StackTrace = string.Empty,
                    Type = MessageType.Success
                };
                string imagePath = string.Empty;
                var users = await this.usersRepository.GetUsers(this.serviceDocument.DataProfile.DataModel, serviceDocumentResult);
                if (users == null)
                {
                    this.serviceDocument.Result = serviceDocumentResult;
                    return users;
                }

                foreach (var item in users)
                {
                    if (!string.IsNullOrEmpty(item.UserPicPath))
                    {
                        imagePath = item.UserPicPath.Replace(RbSettings.DocumentsServerVirtualPath, string.Empty);
                        imagePath = RbSettings.DocumentsServerPhysicalPath + imagePath;
                        AddFileData(imagePath, item);
                    }
                }

                this.serviceDocument.Result = serviceDocumentResult;
                return users;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> UsersSave()
        {
            string password = string.Empty;
            bool sendMail = false;
            SecurityDbContext securityDbContext = GetSecurityDbContext();
            try
            {
                using (var transaction = await securityDbContext.Database.BeginTransactionAsync())
                {
                    password = await this.CreateUser();
                    this.UserImageUpload();
                    this.serviceDocument.Result = await this.usersRepository.SetUsers(this.serviceDocument.DataProfile.DataModel, false);
                    if (this.serviceDocument.Result != null && !string.IsNullOrEmpty(this.serviceDocument.Result.InnerException))
                    {
                        this.serviceDocument.DataProfile.DataModel.UserId = string.Empty;
                        transaction.Rollback();
                    }
                    else
                    {
                        transaction.Commit();
                        sendMail = true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return false;
            }

            if (sendMail && !string.IsNullOrEmpty(password))
            {
                bool result = await this.SendMail(this.serviceDocument.DataProfile.DataModel.Email, password);
                if (result)
                {
                    this.serviceDocument.DataProfile.DataModel.Password = password;
                }
            }

            return true;
        }

        private async Task<ServiceDocument<Models.UserModel>> UsersDomainData()
        {
            this.serviceDocument.DomainData.Add("users", this.domainDataRepository.UsersDomainGet().Result);
            this.serviceDocument.DomainData.Add("location", this.domainDataRepository.LocationDomainGet().Result);
            this.serviceDocument.DomainData.Add("company", this.domainDataRepository.CompanyDomainGet().Result);
            this.serviceDocument.DomainData.Add("orgcountry", this.domainDataRepository.OrgCountryDomainGet().Result);
            this.serviceDocument.DomainData.Add("region", this.domainDataRepository.RegionDomainGet().Result);
            this.serviceDocument.DomainData.Add("format", this.domainDataRepository.FormatDomainGet().Result);
            this.serviceDocument.DomainData.Add("status", this.domainDataRepository.DomainDetailData(RbsSecurityConstants.DomainHdrStatus).Result);
            this.serviceDocument.DomainData.Add("roles", this.domainDataRepository.UsersRolesDomainGet().Result);
            this.serviceDocument.LocalizationData.AddData("Users");
            return this.serviceDocument;
        }

        private async Task<string> CreateUser()
        {
            try
            {
                string userPassword = string.Empty;
                if (string.IsNullOrEmpty(this.serviceDocument.DataProfile.DataModel.UserId))
                {
                    Agility.Framework.Web.Security.Models.UserModel userModel = this.PrepareUserModel();
                    userPassword = userModel.Password;
                    await this.accountDomain.CreateUser(userModel);
                    this.serviceDocument.DataProfile.DataModel.UserId = userModel.Id;
                    this.serviceDocument.DataProfile.DataModel.RolesList.Clear();
                }
                else
                {
                    if (this.serviceDocument.DataProfile.DataModel.RolesRemovedData != null && this.serviceDocument.DataProfile.DataModel.RolesRemovedData.Count > 0)
                    {
                        Agility.Framework.Web.Security.Models.UserModel userModel = this.PrepareRemovableRolesModel();
                        await this.accountDomain.RemoveUserFromRoles(userModel);
                        this.serviceDocument.DataProfile.DataModel.UserId = userModel.Id;
                        this.serviceDocument.DataProfile.DataModel.RolesRemovedData.Clear();
                    }
                }

                return userPassword;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private Agility.Framework.Web.Security.Models.UserModel PrepareUserModel()
        {
            string userPassword = string.Empty;
            Agility.Framework.Web.Security.Models.UserModel userModel = new Framework.Web.Security.Models.UserModel();
            PasswordGenerator passwordGenerator = new PasswordGenerator(RbsSecurityConstants.PasswordLength, RbsSecurityConstants.PasswordLength);
            userPassword = passwordGenerator.Generate();
            userModel.UserName = this.serviceDocument.DataProfile.DataModel.Email;
            userModel.Name = this.serviceDocument.DataProfile.DataModel.Email;
            userModel.Email = this.serviceDocument.DataProfile.DataModel.Email;
            if (this.serviceDocument.DataProfile.DataModel.RolesList != null)
            {
                userModel.RoleNames = this.serviceDocument.DataProfile.DataModel.RolesList.Select(x => x.Name).ToArray();
            }

            userModel.Password = userPassword;
            userModel.OrganizationId = RbsSecurityConstants.OrganizationId;
            userModel.TenantId = RbsSecurityConstants.OrganizationId;
            return userModel;
        }

        private Agility.Framework.Web.Security.Models.UserModel PrepareRemovableRolesModel()
        {
            string userPassword = string.Empty;
            Agility.Framework.Web.Security.Models.UserModel userModel = new Framework.Web.Security.Models.UserModel();
            userModel.Id = this.serviceDocument.DataProfile.DataModel.UserId;
            userModel.UserName = this.serviceDocument.DataProfile.DataModel.Email;
            userModel.Name = this.serviceDocument.DataProfile.DataModel.Email;
            userModel.Email = this.serviceDocument.DataProfile.DataModel.Email;
            if (this.serviceDocument.DataProfile.DataModel.RolesRemovedData != null)
            {
                userModel.RoleNames = this.serviceDocument.DataProfile.DataModel.RolesRemovedData.Select(x => x.Name).ToArray();
            }

            userModel.Roles = this.serviceDocument.DataProfile.DataModel.RolesRemovedData;

            userModel.Password = userPassword;
            userModel.OrganizationId = RbsSecurityConstants.OrganizationId;
            return userModel;
        }

        private void UserImageUpload()
        {
            try
            {
                string filePath = string.Empty;
                string userIdforImage = this.serviceDocument.DataProfile.DataModel.UserId;
                string fileDataAsBase64 = this.serviceDocument.DataProfile.DataModel.FileDataAsBase64;
                string fileName = this.serviceDocument.DataProfile.DataModel.FileName;
                if (!string.IsNullOrEmpty(fileDataAsBase64))
                {
                    filePath = DocumentsHelper.FileUpload(RbsSecurityConstants.UserSubFolderName, userIdforImage + "_" + fileName, fileDataAsBase64);
                }

                this.serviceDocument.DataProfile.DataModel.UserPicPath = filePath;
            }
            catch (Exception e)
            {
                throw new FieldAccessException(e.Message);
            }
        }
    }
}
