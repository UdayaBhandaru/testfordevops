﻿// <copyright file="RbsSecurityConstants.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Security.Common
{
    internal static class RbsSecurityConstants
    {
        // Users Package
        public const string GetSecurityUsersPackage = "UTL_USERS";

        // Users
        public const string GetProcUsers = "GETUSERSLANDING";
        public const string SetProcUsers = "SETUSERS";
        public const string ObjTypeUsers = "USERS_TAB";

        public const string ObjTypeUserId = "USERID_TAB";
        public const string GetProcUserId = "GETUSERID";
        public const string UserSubFolderName = "User";
        public const string DomainHdrIndicator = "INDICATOR";
        public const string DomainHdrStatus = "STATUS";

        public const string ObjTypeUserPic = "USER_NAME_PIC_TAB";
        public const string GetProcUserPic = "GETUSERNAMEPIC";

        // Modified user user_org_hierarchy_tab
        public const string GetProcUsersOrgHierachy = "GETUSERORGHIERARCHY";
        public const string SetProcUsersOrgHierachy = "SETUSERORGHIERARCHY";
        public const string ObjTypeUsersOrgHierachy = "USERS_TAB";

        // Password Length
        public const int PasswordLength = 8;
        public const int OrganizationId = 1;

        // Roles
        public const string GetProcRoles = "GETUSERMENU";
        public const string SetProcRoles = "SETUSERMENU";
        public const string ObjTypeRoles = "USER_MENU_TAB";
    }
}