﻿// <copyright file="AccountRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Security.Account
{
    using Agility.Framework.Core.Common;
    using Agility.Framework.Core.Repositories;
    using Microsoft.EntityFrameworkCore;

    public class AccountRepository : BaseRepository<DbContext>
    {
        public AccountRepository(IDbStartup iDbStartup)
            : base(iDbStartup)
        {
        }
    }
}
