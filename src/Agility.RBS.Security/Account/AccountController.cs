﻿// <copyright file="AccountController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Security.Account
{
    using System;
    using System.Collections.Generic;
    using System.Security.Principal;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Core.Cryptography;
    using Agility.Framework.Core.Exceptions;
    using Agility.Framework.Core.Models;
    using Agility.Framework.Core.Security.Models;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.Framework.Web.Security.Domain;
    using Agility.Framework.Web.Security.Middleware;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Models;
    using Agility.RBS.Core.Services;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.Security.Account.Models;
    using Agility.RBS.Security.Users;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;

    public class AccountController : Controller
    {
        private readonly AccountManager accountDomain;
        private readonly ServiceDocument<UserProfile> serviceDocument;
        private readonly RazorTemplateService razorTemplateService;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly AccountManager accountManager;
        ////private readonly UsersRepository usersRepository;
        ////private readonly string test;

        public AccountController(
            AccountManager accountDomain,
            ServiceDocument<UserProfile> serviceDocument,
            RazorTemplateService razorTemplateService,
          UserManager<ApplicationUser> userManager,
           AccountManager accountManager)
        {
            ////this.usersRepository = usersRepository;
            this.accountDomain = accountDomain;
            this.serviceDocument = serviceDocument;
            this.razorTemplateService = razorTemplateService;
            this.userManager = userManager;
            this.accountManager = accountManager;
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ServiceDocument<UserProfile>> Login([FromBody]LoginViewModel model)
        {
            this.serviceDocument.UserProfile = await this.accountManager.SignInAndGetUserProfile(model.Email, model.Password);
            ////if (this.serviceDocument.UserProfile != null)
            ////{
            ////    this.usersRepository.GetUserDetails(this.serviceDocument.UserProfile.UserId, this.serviceDocument.Result);
            ////}
            return this.serviceDocument;
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<AjaxModel<AjaxResult>> Register([FromBody]Framework.Web.Security.Models.UserModel userModel)
        {
            try
            {
                if (userModel != null && userModel.Organization != null)
                {
                    userModel.OrganizationId = userModel.Organization.OrganizationId;
                }

                await this.accountDomain.CreateUser(userModel);
                return new AjaxModel<AjaxResult> { Model = AjaxResult.Success };
            }
            catch (FxException exception)
            {
                return new AjaxModel<AjaxResult> { Model = AjaxResult.Exception, Message = exception.Message };
            }
        }

        [AllowAnonymous]
        public async ValueTask<bool> Logout()
        {
            this.HttpContext.Session.Clear();
            this.HttpContext.User = new GenericPrincipal(new GenericIdentity(string.Empty), null);
            this.HttpContext.Response.Cookies.Delete("UserToken");
            this.HttpContext.Response.Cookies.Delete(".Fx.Session");
            await this.accountDomain.Logout();
            FxContext.Context = null;
            this.serviceDocument.UserProfile = null;
            return true;
        }

        public List<OrganizationModel> GetOrganizations()
        {
            return new List<OrganizationModel> { new OrganizationModel { OrganizationId = 1, OrganizationName = "aes" } };
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ServiceDocument<UserProfile>> ChangePassword([FromBody] Models.ChangePasswordModel model)
        {
            Agility.Framework.Web.Security.Models.ChangePasswordModel changePasswordModel = new Framework.Web.Security.Models.ChangePasswordModel();
            Framework.Core.Security.Models.UserProfile userProfile = UserProfileHelper.GetInMemoryUser();
            if (userProfile != null)
            {
                FxContext.Context.Email = userProfile.UserName;
                changePasswordModel.CurrentPassword = model.CurrentPassword;
                changePasswordModel.NewPassword = model.NewPassword;
                changePasswordModel.ConfirmPassword = model.ConfirmPassword;
                if (changePasswordModel.CurrentPassword != null)
                {
                    await this.accountDomain.ChangePassword(changePasswordModel);
                }
            }

            return this.serviceDocument;
        }

        [HttpGet]
        [AllowAnonymous]
        public async ValueTask<bool> ForgotPassword(string email)
        {
            string expectedEmail = string.Empty;
            int endIndex;
            int startIndex = 0;
            endIndex = email.IndexOf('@');
            if (endIndex > 0)
            {
                int length = endIndex - startIndex;
                expectedEmail = email.Substring(startIndex, length);
            }

            UserMailModel emailTemplateModel = new UserMailModel
            {
                Email = email,
                SenderName = email,
                RequestId = 0,
                CreatedDate = DateTime.Now,
                Status = string.Empty,
                Subject = string.Empty,
                DueDate = DateTime.Now.AddDays(3),
                ApplicationLoginPath = $"{RbSettings.WebRootPath}/Account/ForgotPassword/" + expectedEmail,
                LogoPath = RbSettings.UserLogoPath,
                CreatorName = expectedEmail
            };

            string template = await this.razorTemplateService.GetHtmlAsync("~/Views/EmailTemplate/ResetPassowrdMail.cshtml", emailTemplateModel);
            if (await MailUtility.SendEmailMessage(
                new FileInputParametersModel
                {
                    From = RbSettings.ServiceAccountMail,
                    DisplayName = string.Empty,
                    ToMails = new[] { email },
                    MessageBody = template,
                    Subject = string.Empty,
                    ProfileInstansId = string.Empty,
                    LogoPath = emailTemplateModel.LogoPath,
                }))
            {
                return true;
            }

            return true;
        }

        [HttpPost]
        [AllowAnonymous]
        public async ValueTask<bool> ResetPassword([FromBody] Models.ForgotPasswordModel model)
        {
            try
            {
                ApplicationUser user = await this.userManager.FindByEmailAsync(model.Email);
                if (model.Email != null && user.Email != null)
                {
                    FxContext.Context.Email = model.Email;
                    FxExecuteActionModel fxExActionModel = new FxExecuteActionModel();
                    var se = SymmetricEncryption.NewInstance();
                    fxExActionModel.NewPassword = model.ConfirmPassword;
                    fxExActionModel.Email = se.EncryptData(model.Email);
                    await this.accountDomain.ResetPassword(fxExActionModel);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return false;
            }
        }

        [AllowAnonymous]
        public async Task<string> RefreshToken()
        {
            UserProfile profileModel = SecurityCacheManager.GetUserProfile(this.HttpContext.Request.Headers["SessionId"]);
            return await this.accountManager.RefreshToken(new ApplicationUser { Email = profileModel.Email });
        }
    }
}
