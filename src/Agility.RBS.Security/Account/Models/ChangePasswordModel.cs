﻿// <copyright file="ChangePasswordModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Security.Account.Models
{
    using System.ComponentModel.DataAnnotations;

    public class ChangePasswordModel
    {
        public string CurrentPassword { get; set; }

        public string NewPassword { get; set; }

        public string ConfirmPassword { get; set; }

        public string Email { get; set; }
    }
}
