﻿// <copyright file="UserMailModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Security.Account.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class UserMailModel
    {
        public string ProfileInstance { get; set; }

        public string LogoPath { get; set; }

        public string Email { get; set; }

        public string SenderName { get; set; }

        public int RequestId { get; set; }

        public string Status { get; set; }

        public DateTime? DueDate { get; set; }

        public string ApplicationLoginPath { get; set; }

        public DateTime? CreatedDate { get; set; }

        public string Subject { get; set; }

        public string CreatorName { get; set; }

        public string Password { get; set; }
    }
}