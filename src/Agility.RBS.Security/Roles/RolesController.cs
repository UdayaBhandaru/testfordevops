﻿// <copyright file="RolesController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Security.Roles
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Season.Models;
    using Agility.RBS.Security.Common;
    using Agility.RBS.Security.Roles.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    public class RolesController : Controller
    {
        private readonly ServiceDocument<RoleMenuHeaderModel> serviceDocument;
        private readonly RolesRepository rolesRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly DomainDataRepository domainDataRepository;

        public RolesController(
            ServiceDocument<RoleMenuHeaderModel> serviceDocument,
            RolesRepository rolesRepository,
            DomainDataRepository domainDataRepository)
        {
            this.rolesRepository = rolesRepository;
            this.serviceDocument = serviceDocument;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        [HttpPost]
        public async Task<ServiceDocument<RoleMenuHeaderModel>> List()
        {
            return await this.RolesDomainData();
        }

        [HttpPost]
        public async Task<ServiceDocument<RoleMenuHeaderModel>> New()
        {
            return await this.RolesDomainData();
        }

        [HttpPost]
        public async Task<ServiceDocument<RoleMenuHeaderModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.CallBackSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<RoleMenuHeaderModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.CallBackSave);
            return this.serviceDocument;
        }

        public async ValueTask<bool> IsExistingRole(string roleName)
        {
            return await this.rolesRepository.IsExistingRole(roleName);
        }

        private async Task<List<RoleMenuHeaderModel>> CallBackSearch()
        {
            try
            {
                var roles = await this.rolesRepository.GetRoles(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return roles;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> CallBackSave()
        {
            this.serviceDocument.Result = await this.rolesRepository.SetRoles(this.serviceDocument.DataProfile.DataModel);
            return true;
        }

        private async Task<ServiceDocument<RoleMenuHeaderModel>> RolesDomainData()
        {
            this.serviceDocument.DomainData.Add("status", this.domainDataRepository.DomainDetailData(RbsSecurityConstants.DomainHdrStatus).Result);
            this.serviceDocument.DomainData.Add("roles", this.domainDataRepository.UsersRolesDomainGet().Result);
            this.serviceDocument.LocalizationData.AddData("Roles");
            return this.serviceDocument;
        }
    }
}
