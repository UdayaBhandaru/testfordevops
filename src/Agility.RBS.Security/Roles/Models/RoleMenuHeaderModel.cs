﻿// <copyright file="RoleMenuHeaderModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Security.Roles.Models
{
    using System;
    using System.Collections.Generic;
    using Agility.Framework.Core.Profile.Entities;

    public class RoleMenuHeaderModel : ProfileEntity
    {
        public RoleMenuHeaderModel()
        {
            this.RoleMenuClaims = new List<RoleMenuClaimModel>();
        }

        public string RoleId { get; set; }

        public string RoleName { get; set; }

        public string SelectedInd { get; set; }

        public string Error { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }

        public List<RoleMenuClaimModel> RoleMenuClaims { get; private set; }
    }
}
