﻿// <copyright file="RoleMenuClaimModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Security.Roles.Models
{
    using System;
    using System.Collections.Generic;
    using Agility.Framework.Core.Profile.Entities;

    public class RoleMenuClaimModel : ProfileEntity
    {
        public string RoleId { get; set; }

        public string RoleName { get; set; }

        public string MenuId { get; set; }

        public string MenuText { get; set; }

        public string ClaimId { get; set; }

        public string ClaimDesc { get; set; }

        public string ClaimType { get; set; }

        public string ClaimValue { get; set; }

        public string ParentMenuId { get; set; }

        public string ParentMenuText { get; set; }

        public string SelectedInd { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Error { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }
    }
}
