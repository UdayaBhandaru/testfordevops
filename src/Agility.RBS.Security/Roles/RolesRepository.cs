﻿// <copyright file="RolesRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Security.Roles
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Season.Models;
    using Agility.RBS.Security.Common;
    using Agility.RBS.Security.Roles.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class RolesRepository : BaseOraclePackage
    {
        public RolesRepository()
        {
            this.PackageName = RbsSecurityConstants.GetSecurityUsersPackage;
        }

        public async Task<List<RoleMenuHeaderModel>> GetRoles(RoleMenuHeaderModel roleMenuClaimModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(RbsSecurityConstants.ObjTypeRoles, RbsSecurityConstants.GetProcRoles);
            OracleObject recordObject = this.SetParamsRoles(roleMenuClaimModel, true);
            OracleTable pRolesTab = await this.GetProcedure(recordObject, packageParameter, serviceDocumentResult);

            var lstHeader = Mapper.Map<List<RoleMenuHeaderModel>>(pRolesTab);
            if (lstHeader != null)
            {
                for (int i = 0; i < lstHeader.Count; i++)
                {
                    OracleObject obj = (OracleObject)pRolesTab[i];
                    OracleTable menuClaimDetails = (Devart.Data.Oracle.OracleTable)obj["USER_MENU_DTL"];
                    if (menuClaimDetails.Count > 0)
                    {
                        lstHeader[i].RoleMenuClaims.AddRange(Mapper.Map<List<RoleMenuClaimModel>>(menuClaimDetails));
                    }
                }
            }

            return lstHeader;
        }

        public async Task<ServiceDocumentResult> SetRoles(RoleMenuHeaderModel roleMenuHeaderModel)
        {
            this.PackageName = RbsSecurityConstants.GetSecurityUsersPackage;
            PackageParams packageParameter = this.GetPackageParams(RbsSecurityConstants.ObjTypeRoles, RbsSecurityConstants.SetProcRoles);
            OracleObject recordObject = this.SetParamsRoles(roleMenuHeaderModel, false);
            ServiceDocumentResult sRes = await this.SetProcedure(recordObject, packageParameter);
            return sRes;
        }

        public async ValueTask<bool> IsExistingRole(string roleName)
        {
            int result;
            OracleCommand myCommand = new OracleCommand("Select count(1) from ROLES  where ROLE_Name='" + roleName + "'");
            myCommand.Connection = this.Connection;
            this.Connection.Open();
            result = Convert.ToInt32(await myCommand.ExecuteScalarAsync());
            this.Connection.Close();
            return result > 0;
        }

        private OracleObject SetParamsRoles(RoleMenuHeaderModel roleMenuHeaderModel, bool isSearch)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType("USER_MENU_REC", this.Connection);
            OracleObject recordObject = new OracleObject(recordType);

            if (isSearch)
            {
                string roleId = roleMenuHeaderModel.RoleId;
                if (string.IsNullOrEmpty(roleId))
                {
                    recordObject[recordType.Attributes["ROLE_ID"]] = -1;
                }
                else
                {
                    recordObject[recordType.Attributes["ROLE_ID"]] = roleMenuHeaderModel.RoleId;
                }
            }
            else
            {
                recordObject[recordType.Attributes["ROLE_ID"]] = roleMenuHeaderModel.RoleId;

                // Detail Object
                OracleType dtlTableType = OracleType.GetObjectType("USER_MENU_DTL_TAB", this.Connection);
                OracleType dtlRecordType = OracleType.GetObjectType("USER_MENU_DTL_REC", this.Connection);
                OracleTable pRoleMenuClaimTab = new OracleTable(dtlTableType);
                foreach (RoleMenuClaimModel rmcm in roleMenuHeaderModel.RoleMenuClaims)
                {
                    var dtlRecordObject = new OracleObject(dtlRecordType);
                    dtlRecordObject[dtlRecordType.Attributes["CLAIM_ID"]] = rmcm.ClaimId;
                    dtlRecordObject[dtlRecordType.Attributes["CLAIM_DESC"]] = rmcm.ClaimDesc;
                    dtlRecordObject[dtlRecordType.Attributes["CLAIM_VALUE"]] = rmcm.ClaimValue;
                    dtlRecordObject[dtlRecordType.Attributes["CLAIM_TYPE"]] = rmcm.ClaimType;
                    dtlRecordObject[dtlRecordType.Attributes["PARENT_MENU_ID"]] = string.IsNullOrEmpty(rmcm.ParentMenuId) ? rmcm.MenuId : rmcm.ParentMenuId;
                    dtlRecordObject[dtlRecordType.Attributes["PARENT_MENU_TEXT"]] = rmcm.ParentMenuText;
                    dtlRecordObject[dtlRecordType.Attributes["SELECTED_IND"]] = rmcm.SelectedInd;
                    dtlRecordObject[dtlRecordType.Attributes["ROLE_ID"]] = rmcm.RoleId;
                    dtlRecordObject[dtlRecordType.Attributes["MENU_TEXT"]] = rmcm.MenuText;
                    dtlRecordObject[dtlRecordType.Attributes["Operation"]] = rmcm.Operation;
                    pRoleMenuClaimTab.Add(dtlRecordObject);
                }

                recordObject[recordType.Attributes["USER_MENU_DTL"]] = pRoleMenuClaimTab;
            }

            return recordObject;
        }
    }
}