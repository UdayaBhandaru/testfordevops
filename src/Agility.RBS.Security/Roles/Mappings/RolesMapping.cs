﻿// <copyright file="RolesMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Security.Roles.Mapping
{
    using System;
    using Agility.RBS.MDM.OrganizationHierarchy.Models;
    using Agility.RBS.MDM.ValueAddedTax.Models;
    using Agility.RBS.Security.Account.Models;
    using Agility.RBS.Security.Roles.Models;
    using Agility.RBS.Security.Users.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class RolesMapping : Profile
    {
        public RolesMapping()
            : base("RolesMapping")
        {
            this.CreateMap<OracleObject, RoleMenuClaimModel>()
                .ForMember(m => m.RoleId, opt => opt.MapFrom(r => r["ROLE_ID"]))
                .ForMember(m => m.MenuId, opt => opt.MapFrom(r => r["MENU_ID"]))
                .ForMember(m => m.MenuText, opt => opt.MapFrom(r => r["MENU_TEXT"]))
                .ForMember(m => m.ClaimId, opt => opt.MapFrom(r => r["CLAIM_ID"]))
                .ForMember(m => m.ClaimDesc, opt => opt.MapFrom(r => r["CLAIM_DESC"]))
                .ForMember(m => m.ClaimType, opt => opt.MapFrom(r => r["CLAIM_TYPE"]))
                .ForMember(m => m.ClaimValue, opt => opt.MapFrom(r => r["CLAIM_VALUE"]))
                .ForMember(m => m.ParentMenuId, opt => opt.MapFrom(r => r["PARENT_MENU_ID"]))
                .ForMember(m => m.ParentMenuText, opt => opt.MapFrom(r => r["PARENT_MENU_TEXT"]))
                .ForMember(m => m.SelectedInd, opt => opt.MapFrom(r => r["SELECTED_IND"]))
                .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, RoleMenuHeaderModel>()
                .ForMember(m => m.RoleId, opt => opt.MapFrom(r => r["ROLE_ID"]))
                .ForMember(m => m.RoleName, opt => opt.MapFrom(r => r["ROLE_NAME"]))
                .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));
        }
    }
}
