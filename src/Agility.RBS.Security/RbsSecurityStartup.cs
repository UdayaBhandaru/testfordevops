﻿// <copyright file="RbsSecurityStartup.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Security
{
    using System;
    using Agility.Framework.Core;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.ValueAddedTax;
    using Agility.RBS.MDM.ValueAddedTax.Mapping;
    using Agility.RBS.Security.Account;
    using Agility.RBS.Security.Roles;
    using Agility.RBS.Security.Roles.Mapping;
    using Agility.RBS.Security.Users;
    using Agility.RBS.Security.Users.Mapping;
    using AutoMapper;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Caching.Distributed;
    using Microsoft.Extensions.Caching.Memory;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;

    public class RbsSecurityStartup : ComponentStartup<DbContext>
    {
        public void Startup(IConfigurationRoot configuration)
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<UsersMapping>();
                cfg.AddProfile<RolesMapping>();
                this.InitilizeMapping(cfg);
            });
        }

        public override void ConfigureDependencyInjection(IServiceCollection services)
        {
            services.AddTransient<AccountRepository>();
            services.AddTransient<UsersRepository>();
            services.AddTransient<RolesRepository>();
            services.AddTransient<UsersController>();
        }

        public override void Configure(
           IApplicationBuilder app,
           IHostingEnvironment env,
           ILoggerFactory loggerFactory,
           IDistributedCache distributedCache,
           IMemoryCache memoryCache,
           IServiceProvider serviceProvider)
        {
            System.IO.Directory.CreateDirectory(Core.RbSettings.DocumentsServerPhysicalPath);
            app.UseStaticFiles(new Microsoft.AspNetCore.Builder.StaticFileOptions
            {
                FileProvider = new Microsoft.Extensions.FileProviders.PhysicalFileProvider(
            Core.RbSettings.DocumentsServerPhysicalPath),
                RequestPath = new Microsoft.AspNetCore.Http.PathString(Core.RbSettings.DocumentsServerVirtualPath)
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
        }
    }
}
