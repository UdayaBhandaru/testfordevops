﻿// <copyright file="BarcodeRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Core.Models;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.Framework.Web.Security.Middleware;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.ItemManagement.Common;
    using Agility.RBS.ItemManagement.Item.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class BarcodeRepository : BaseOraclePackage
    {
        public BarcodeRepository()
        {
            this.PackageName = ItemManagementConstants.BarcodePackage;
        }

        public async ValueTask<AjaxModel<string>> GenerateBarcode(string barcodeType, string itemNo)
        {
            try
            {
                this.Connection.Open();
                OracleParameterCollection parameters = this.Parameters;
                parameters.Clear();
                OracleParameter oracleParameter;
                oracleParameter = new OracleParameter { ParameterName = "O_error_message", OracleDbType = OracleDbType.VarChar, Direction = ParameterDirection.InputOutput };
                parameters.Add(oracleParameter);
                oracleParameter = new OracleParameter { ParameterName = "IO_item_no", OracleDbType = OracleDbType.VarChar, Direction = ParameterDirection.InputOutput, Value = itemNo };
                parameters.Add(oracleParameter);
                oracleParameter = new OracleParameter { ParameterName = "I_item_type", OracleDbType = OracleDbType.VarChar, Direction = ParameterDirection.Input, Value = barcodeType };
                parameters.Add(oracleParameter);
                oracleParameter = new OracleParameter("RESULT", Devart.Data.Oracle.OracleDbType.Boolean)
                {
                    Direction = ParameterDirection.ReturnValue
                };
                parameters.Add(oracleParameter);
                this.ExecuteProcedure("GET_NEXT", parameters);
                if (this.Parameters["Result"].Value == System.DBNull.Value)
                {
                    throw new NoNullAllowedException();
                }

                if (this.Parameters["O_error_message"].Value != null &&
                    !string.IsNullOrEmpty(Convert.ToString(this.Parameters["O_error_message"].Value)))
                {
                    return new AjaxModel<string> { Result = AjaxResult.ValidationException, Message = Convert.ToString(this.Parameters["O_error_message"]), Model = null };
                }

                return new AjaxModel<string> { Result = AjaxResult.Success, Model = Convert.ToString(this.Parameters["IO_item_no"].Value) };
            }
            catch (Exception e)
            {
                return new AjaxModel<string> { Result = AjaxResult.Exception, Message = Convert.ToString(e.Message), Model = null };
            }
            finally
            {
                this.Connection.Close();
            }
        }
    }
}