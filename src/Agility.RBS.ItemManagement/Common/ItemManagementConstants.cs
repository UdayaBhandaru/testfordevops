﻿// <copyright file="ItemManagementConstants.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
namespace Agility.RBS.ItemManagement.Common
{
    internal static class ItemManagementConstants
    {
        // Result
        public const string OracleParameterResult = "RESULT";

        // Item Package
        public const string GetItemPackage = "ITEM_MASTER2";

        // New Item Package
        public const string GetNewItemPackage = "NEWITEMPKG";

        // New Item Info
        public const string RecordObjNewItemSearch = "NEW_ITEM_SEARCH_REC";
        public const string ObjTypeNewItemSearch = "NEW_ITEM_SEARCH_TAB";
        public const string GetProcNewItemList = "GETNEWITEMSEARCH";

        // Item Basic Info
        public const string GetProcItemList = "GETITEMLIST";
        public const string GetProcItemBasic = "GETITEMBASICS";
        public const string SetProcItemBasic = "SETITEMBASICS";
        public const string VGetProcItemBasic = "VGETITEMBASICS";
        public const string ObjTypeItemBasic = "ITEM_BASICS_TAB";
        public const string RecordObjItemBasic = "ITEM_BASICS_REC";
        public const string ObjTypeItemSearch = "ITEM_SEARCH_TAB";
        public const string RecordObjItemSearch = "ITEM_SEARCH_OBJ";
        public const string ObjTypeItemBarcodeSearch = "BARCODE_TAB";
        public const string RecordObjItemBarcodeSearch = "BARCODE_REC";
        public const string SearchItemBarcode = "GETBARCODE";

        // Item Company
        public const string GetProcItemComapny = "GETITEMCOMPANY";
        public const string SetProcItemComapny = "SETITEMCOMPANY";
        public const string ObjTypeItemComapny = "ITEM_COMPANY_TAB";
        public const string RecordObjItemComapny = "ITEM_COMPANY_REC";

        // Item Country
        public const string GetProcItemCountry = "GETITEMCOUNTRY";
        public const string SetProcItemCountry = "SETITEMCOUNTRY";
        public const string ObjTypeItemCountry = "ITEM_COUNTRY_TAB";
        public const string RecordObjItemCountry = "ITEM_COUNTRY_REC";

        // Item Region
        public const string GetProcItemRegion = "GETITEMREGION";
        public const string SetProcItemRegion = "SETITEMREGION";
        public const string ObjTypeItemRegion = "ITEM_REGION_TAB";
        public const string RecordObjItemRegion = "ITEM_REGION_REC";

        // Item Format
        public const string GetProcItemFormat = "GETITEMFORMAT";
        public const string SetProcItemFormat = "SETITEMFORMAT";
        public const string ObjTypeItemFormat = "ITEM_FORMAT_TAB";
        public const string RecordObjItemFormat = "ITEM_FORMAT_REC";

        // Item Location
        public const string GetProcItemLocation = "GETITEMLOCATION";
        public const string SetProcItemLocation = "SETITEMLOCATION";
        public const string ObjTypeItemLocation = "ITEM_LOCATION_TAB";
        public const string RecordObjItemLocation = "ITEM_LOCATION_REC";

        // Item Vat
        public const string GetProcItemVat = "GETITEMVATCODE";
        public const string SetProcItemVat = "SETITEMVATCODE";
        public const string ObjTypeItemVat = "ITEM_VAT_TAB";
        public const string RecordObjItemVat = "ITEM_VAT_REC";

        // Domain Header Ids
        public const string DomainHdrItemstatus = "STST";
        public const string DomainHdrItemtype = "UPCT";
        public const string DomainHdrSource = "SOURCE";
        public const string DomainHdrAutorepl = "AUTOREPL";
        public const string DomainHdrLabeltype = "LABELTYPE";
        public const string DomainHdrYesNo = "YSNO";
        public const string DomainHdrSkuclass = "SKUCLASS";
        public const string DomainHdrIndicator = "INDICATOR";
        public const string DomainHdrSom = "ORML";

        public const string DomainHdrInventory = "INVENTORY";
        public const string DomainHdrLocType = "LOC_TYPE";
        public const string DomainHdrITier = "ITIE";
        public const string DomainHdrNature = "NATURE";
        public const string DomainHdrSpec = "SPEC";
        public const string DomainHdrDateType = "DATETYPE";
        public const string DomainHdrDimensionObject = "DIMO";
        public const string DomainHdrPresentationMethod = "PRM";
        public const string DomainHdrTareType = "TARE";
        public const string DomainHdrBasic = "BASIC";
        public const string DomainHdrLastRcvdStat = "LSTRCVDSTA";
        public const string DomainHdrProdType = "PRODTYPE";
        public const string DomainHdrRetailType = "RETTYPE";
        public const string DomainHdrSourceMethod = "SOMD";
        public const string DomainHdrTransactionType = "PCST";

        // Item Suplier Details
        public const string GetProcItemSupplier = "GETITEMSUPPLIER";
        public const string SetProcItemSupplier = "SETITEMSUPPLIER";
        public const string ObjTypeItemSupplier = "ITEM_SUPPLIER_TAB";
        public const string RecordObjItemSupplier = "ITEM_SUPPLIER_REC";

        // Item Barcode
        public const string GetProcItemBarcode = "GETITEMBARCODE";
        public const string SetProcItemBarcode = "SETITEMBARCODE";
        public const string ObjTypeItemBarcode = "ITEM_BARCODE_TAB";
        public const string RecordObjItemBarcode = "ITEM_BARCODE_REC";

        // Item Suplier Country
        public const string GetProcItemSupplierCountry = "GETITEMSUPPCOUNTRY";
        public const string SetProcItemSupplierCountry = "SETITEMSUPPCOUNTRY";
        public const string ObjTypeItemSupplierCountry = "ITEM_SUPP_COUNTRY_TAB";
        public const string RecordObjItemSupplierCountry = "ITEM_SUPP_COUNTRY_REC";

        // Item Suplier Country Location
        public const string GetProcItemSupplierCountryLocation = "GETITEMSUPPCOUNTRYLOC";
        public const string SetProcItemSupplierCountryLocation = "SETITEMSUPPCOUNTRYLOC";
        public const string ObjTypeItemSupplierCountryLocation = "ITEM_SUPP_COUNTRY_LOC_TAB";
        public const string RecordObjItemSupplierCountryLocation = "ITEM_SUPP_COUNTRY_LOC_REC";

        // Item Suplier Country Dimension
        public const string GetProcItemSupplierCountryDimension = "GETITEMSUPPCOUNTRYDIM";
        public const string SetProcItemSupplierCountryDimension = "SETITEMSUPPCOUNTRYDIM";
        public const string ObjTypeItemSupplierCountryDimension = "ITEM_SUPP_COUNTRY_DIM_TAB";
        public const string RecordObjItemSupplierCountryDimension = "ITEM_SUPP_COUNTRY_DIM_REC";

        // Item Suplier Country HTS
        public const string GetProcItemSupplierCountryHts = "GETITEMSUPPCOUNTRYHTS";
        public const string SetProcItemSupplierCountryHts = "SETITEMSUPPCOUNTRYHTS";
        public const string ObjTypeItemSupplierCountryHts = "ITEM_SUPP_COUNTRY_HTS_TAB";
        public const string RecordObjItemSupplierCountryHts = "ITEM_SUPP_COUNTRY_HTS_REC";

        // Inbox
        public const string GetProcInbox = "GET_INBOX";
        public const string SetProcInbox = "set_Inbox";
        public const string ObjTypeNameInbox = "INBOX_TBL";
        public const string InboxTbl = "P_INBOX_TBL";
        public const string ObjTypeInbox = "INBOX_TBL";
        public const string QuickViewTbl = "P_QUICKVEIW";
        public const string ObjTypeNameQuickView = "QUICKVIEW_TAB";
        public const string GetProcQuickView = "GET_INBOX_QUICKVIEW";

        public const string GetProcGlobalCount = "GETGLOBALINBOXCNT";
        public const string ObjTypeGlobalInboxCount = "GLOBAL_INBOX_CNT_TAB";

        public const string WorkFlowHistoryTbl = "P_GLOBALINBOX_HIST_TAB";
        public const string ObjTypeWorkFlowHistory = "GLOBALINBOX_HIST_TAB";
        public const string GetProcWorkflowHistory = "GETINBOXHISTORY";

        // Inbox package
        public const string GetInboxPackage = "UTL_Global_Inbox";

        // Item Pack Details
        public const string GetProcItemPack = "GETPACKITEM";
        public const string SetProcItemPack = "SETPACKITEM";
        public const string ObjTypeItemPack = "PACK_ITEM_TAB";
        public const string RecordObjItemPack = "PACK_ITEM_REC";
        public const string GetItemsForPackItem = "GETITEMSFORPACKITEM";

        // Supplier Master
        public const string GetSupplierMasterPackage = "UTL_SUPPLIER";
        public const string GetProcSupplier = "GETSUPPLIER";
        public const string SetProcSupplier = "SETSUPPLIER";
        public const string ObjTypeSupplier = "SUPPLIER_TAB";

        // Item Suplier Country HTS
        public const string GetFashionItem = "GETFASHIONITEM";
        public const string SetFashionItem = "SETFASHIONITEM";
        public const string FashionItemTab = "FASHION_ITEM_TAB";
        public const string RecordObjFashionItem = "FASHION_ITEM_REC";

        // find item
        public const string GetItemSelectPackage = "ITEM_SEARCH";

        public const string GetCostChgItemList = "GETCOSTCHGITEMLIST";
        public const string CostChangeFindItemListTab = "COST_ITEM_LIST_TAB";
        public const string RecordObjCostChangeFindItemList = "COST_ITEM_LIST_REC";
        public const string GetCostChgItemList2 = "GETCOSTCHGITEMLIST2";
        public const string CostChangeFindItemListTab2 = "COST_ITEM_LIST_TAB2";
        public const string RecordObjCostChangeFindItemList2 = "COST_ITEM_LIST_REC2";

        public const string GetCostChgItemGroupList3 = "GETCOSTCHGITEMLIST3";
        public const string CostChangeFindItemgGroupListTab3 = "COST_ITEM_LIST_TAB3";
        public const string RecordObjCostChangeFindItemList3 = "COST_ITEM_LIST_REC3";

        public const string GetPriceChgItemList = "GETPRICECHGITEMLIST";
        public const string PriceChangeFindItemListTab = "PRICE_ITEM_LIST_TAB";
        public const string RecordObjPriceChangeFindItemList = "PRICE_ITEM_LIST_REC";

        public const string FindItemListTab = "find_item_list_tab";
        public const string RecordObjFindItemList = "find_item_list_rec";

        public const string OrderItemSearchTab = "ORDER_ITEM_SEARCH_TAB";
        public const string OrderItemSearchRec = "ORDER_ITEM_SEARCH_REC";

        public const string GetOrderItemList = "getorderitemlist";

        public const string RtvItemSearchTab = "RTV_ITEM_SEARCH_TAB";
        public const string RtvItemSearchRec = "RTV_ITEM_SEARCH_REC";
        public const string GetRtvItemList = "GETRTVITEMLIST";

        public const string GetItemList = "getfinditemlist";

        public const string DomainHdrPriceOrgLevel = "PRCHGLVL";
        public const string DomainHdrCostOrgLevel = "COSTCHGLVL";

        public const string ObjTypeItemPrint = "ITEM_PRINT_TAB";
        public const string GetProcItemPrint = "GETITEMPRINT";
        public const string RecordObjItemPrint = "ITEM_PRINT_REC";
        public const string DomainHdrStatus = "STATUS";
        public const string ItemUnit = "ITEM_UNIT";

        public const string BarcodePackage = "UTL_BARCODE";
        public const string BarcodeType = "CODETYPE";

        // Bulk Item
        public const string GetProcBulkItemHead = "GETBULKITEMHEAD";
        public const string SetProcBulkItemHead = "SETBULKITEMHEAD";
        public const string ObjTypeBulkItemHead = "BULK_ITEM_HEAD_TAB";
        public const string RecordObjBulkItemHead = "BULK_ITEM_HEAD_REC";

        // New Item Get
        public const string GetProcForNewlyCreatedItemsList = "GETBULKITEMDETAIL";
        public const string ObjTypeForNewlyCreatedItemsList = "BULK_ITEM_DETAIL_TAB";
        public const string RecordObjForNewlyCreatedItemsList = "BULK_ITEM_DETAIL_REC";

        // BulkItem
        ////public const string GetBulkItemMasterDatalList = "GETITEMXLSMST";
        public const string GetBulkItemMasterDatalList = "GETSUBCLASS";
        public const string BulkItemMasterDataTab = "SUB_CLASS_XLS_TAB";
        //// public const string BulkItemMasterDataRecord = "ITEM_MST_XLS_REC";
        public const string BulkItemMasterDataRecord = "SUB_CLASS_XLS_REC";

        // Item Super
        public const string GetProcItemSuperSearch = "GETITEMSUPERSEARCH";
        public const string ObjTypeItemSuperSearch = "ITEM_SUPER_SEARCH_TAB";
        public const string RecordObjItemSuperSearch = "ITEM_SUPER_SEARCH_REC";
        public const string ItemSuperTab = "ITEM_SUPER_TAB";
        public const string RecordObjItemSuper = "ITEM_SUPER_REC";
        public const string GetProcItemPrices = "GETITEMPRICE";
        public const string ObjTypeItemPrices = "ITEM_PRICE_TAB";
        public const string RecordObjItemPrices = "ITEM_PRICE_REC";
        public const string ItemPricesTbl = "P_ITEM_PRICE_TAB";
        public const string GetProcItemStock = "GETITEMSTOCK";
        public const string ObjTypeItemStock = "ITEM_STOCK_TAB";
        public const string RecordObjItemStock = "ITEM_STOCK_REC";
        public const string ItemStockTbl = "P_ITEM_STOCK_TAB";
        public const string GetProcItemOrders = "GETITEMORDER";
        public const string ObjTypeItemOrders = "ITEM_ORDER_TAB";
        public const string RecordObjItemOrders = "ITEM_ORDER_REC";
        public const string ItemOrdersTbl = "P_ITEM_ORDER_TAB";
        public const string GetProcItemSales = "GETLOCWISEWEEKSDATA";
        public const string ObjTypeItemSales = "LOC_WISE_WEEKS_DATA_TAB";
        public const string ItemSalesTbl = "P_LOC_WISE_WEEKS_DATA_TAB";
        public const string WhsMovmntPackage = "WHS_MOVMNT";
        public const string GetProcWhsMovmnt = "GETWHSMOVMNT";
        public const string ObjTypeWhsMovmnt = "WHS_MOVMNT_TAB";
        public const string RecordObjWhsMovmnt = "WHS_MOVMNT_REC";
        public const string ItemWhsMovmntTbl = "P_WHS_MOVMNT_TAB";
        public const string DsdMovmntPackage = "DSD_ORDERS";
        public const string GetProcDsdMovmnt = "GETDSDIORDERMOVMNT";
        public const string ObjTypeDsdMovmnt = "DSD_ORDERS_MOVMNT_TAB";
        public const string RecordObjDsdMovmnt = "DSD_ORDERS_MOVMNT_REC";
        public const string ItemDsdMovmntTbl = "P_DSD_ORDERS_MOVMNT_TAB";
        public const string GetProcItemRanging = "GETITEMSTORERANGING";
        public const string SetProcItemRanging = "SETITEMSTORERANGING";
        public const string ObjTypeItemRanging = "STORE_RANGE_TAB";
        public const string RecordObjItemRanging = "STORE_RANGE_REC";
        public const string ItemRangingTbl = "P_STORE_RANGE_TAB";

        // Item Reactivate
        public const string ItemReactivateTab = "item_reactivate_tab";
        public const string SetItemReactivate = "setitemreactivate";
        public const string GetItemReactivate = "getitemreactivate";
        public const string ItemReactivateRec = "item_reactivate_rec";

        // Item Inventory
        public const string ObjTypeItemInventory = "ITEM_SOH_TAB";
        public const string RecordObjItemInventory = "ITEM_SOH_REC";
        public const string ItemInventoryTbl = "P_ITEM_SOH_TAB";
        public const string GetProcItemInventory = "GETITEMSOH";

        // Item Reclassification
        public const string GetProcItemReclassification = "getitemreclass";
        public const string SetProcItemReclassification = "setitemreclass";
        public const string ObjTypeItemReclassification = "item_reclass_tab";
        public const string RecordObjItemReclassification = "item_reclass_rec";

        // Item UDA
        public const string GetUdaList = "GETUDALIST";
        public const string GetUda = "GETUDA";
        public const string UdaRec = "UDA_REC";
        public const string UdaTab = "UDA_TAB";
        public const string SetUda = "SETUDA";
    }
}
