﻿// <copyright file="BulkItemDetailModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.BulkItem.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class BulkItemDetailModel
    {
        public int? Division { get; set; }

        public string DivisionDesc { get; set; }

        public int? DeptNumber { get; set; }

        public string DeptDesc { get; set; }

        public string GroupDesc { get; set; }

        public int? Fineline { get; set; }

        public string ClassDesc { get; set; }

        public int? Segment { get; set; }

        public string SubclassDesc { get; set; }

        public string Item { get; set; }

        public string ItemDesc { get; set; }
    }
}
