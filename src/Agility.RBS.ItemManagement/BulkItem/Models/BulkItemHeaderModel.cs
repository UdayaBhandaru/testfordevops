﻿// <copyright file="BulkItemHeaderModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// BulkItemHeaderModel
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.ItemManagement.BulkItem.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Inbox.Common;

    [DataProfile(Name = "BULKITEMHEAD")]
    public class BulkItemHeaderModel : ProfileEntity<BulkItemHeaderWfModel>, IInboxCommonModel
    {
        public BulkItemHeaderModel()
        {
            this.BulkItemDetails = new List<BulkItemDetailModel>();
        }

        [Column("HEAD_SEQ")]
        public int? HEAD_SEQ { get; set; }

        [NotMapped]
        public string Description { get; set; }

        [NotMapped]
        public int? SupplierId { get; set; }

        [NotMapped]
        public string SupplierName { get; set; }

        [NotMapped]
        public int? DivisionId { get; set; }

        [NotMapped]
        public string DivisionDesc { get; set; }

        [NotMapped]
        public int? TotalRebates { get; set; }

        [NotMapped]
        public string OriginCountryId { get; set; }

        [NotMapped]
        public string OriginCountryName { get; set; }

        [NotMapped]
        public string CurrencyCode { get; set; }

        [NotMapped]
        public string IncotermsCode { get; set; }

        [NotMapped]
        public string IncotermsDesc { get; set; }

        [NotMapped]
        public string SupplierType { get; set; }

        [NotMapped]
        public int? LangId { get; set; }

        [NotMapped]
        public string ProgramPhase { get; set; }

        [NotMapped]
        public string ProgramMessage { get; set; }

        [NotMapped]
        public string Error { get; set; }

        [NotMapped]
        public string Operation { get; set; }

        [NotMapped]
        public string TableName { get; set; }

        [NotMapped]
        public string DateType { get; set; }

        [NotMapped]
        public DateTime? StartDate { get; set; }

        [NotMapped]
        public DateTime? EndDate { get; set; }

        [NotMapped]
        public Core.Models.RbWfHeaderModel WorkflowForm { get; set; }

        [NotMapped]
        [Column("ORGANISATION_ID")]
        public new int? OrganizationId { get; set; }

        [NotMapped]
        [Column("CREATED_BY")]
        public new string CreatedBy { get; set; }

        [NotMapped]
        [Column("LAST_UPDATED_BY")]
        public new string ModifiedBy { get; set; }

        [NotMapped]
        [Column("DeletedInd")]
        public new bool? DeletedInd { get; set; }

        [NotMapped]
        [Column("CREATED_DATE")]
        public new DateTimeOffset? CreatedDate { get; set; }

        [NotMapped]
        [Column("LAST_UPDATED_DATE")]
        public new DateTimeOffset? ModifiedDate { get; set; }

        [NotMapped]
        public int? WfNxtStateInd { get; set; }

        [NotMapped]
        public string Status { get; set; }

        [NotMapped]
        public int? FileCount { get; set; }

        [NotMapped]
        public List<BulkItemDetailModel> BulkItemDetails { get; private set; }
    }
}
