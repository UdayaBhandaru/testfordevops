﻿//-------------------------------------------------------------------------------------------------
// <copyright file="BulkItemController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// BulkItemController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.ItemManagement.BulkItem
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Constants;
    using Agility.RBS.Core.CustomExceptions;
    using Agility.RBS.Core.Models;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.Documents;
    using Agility.RBS.Documents.Models;
    using Agility.RBS.Inbox.Common;
    using Agility.RBS.Inbox.Models;
    using Agility.RBS.Inbox.Repository;
    using Agility.RBS.ItemManagement.BulkItem.Models;
    using Agility.RBS.ItemManagement.BulkItem.Repositories;
    using Agility.RBS.ItemManagement.Common;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Models;
    using DocumentFormat.OpenXml.Packaging;
    using DocumentFormat.OpenXml.Spreadsheet;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [Route("api/[controller]/[action]")]
    public class BulkItemController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly BulkItemRepository bulkItemRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly InboxModel inboxModel;
        private readonly InboxRepository inboxRepository;
        private ServiceDocument<BulkItemHeaderModel> serviceDocument;
        private int headSeq;

        public BulkItemController(
            ServiceDocument<BulkItemHeaderModel> serviceDocument,
            BulkItemRepository bulkItemRepository,
            DomainDataRepository domainDataRepository,
            InboxRepository inboxRepository)
        {
            this.bulkItemRepository = bulkItemRepository;
            this.domainDataRepository = domainDataRepository;
            this.inboxRepository = inboxRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
            this.inboxModel = new InboxModel { Operation = "CHGS", ModuleName = "BULKITEM" };
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in the Bulk Item List page.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        public async Task<ServiceDocument<BulkItemHeaderModel>> List()
        {
            ////this.serviceDocument.DomainData.Add("supplier", this.domainDataRepository.SupplierDomainGet().Result);
            this.serviceDocument.DomainData.Add("workflowStatusList", this.domainDataRepository.WorkflowStatusGet(RbCommonConstants.BulkModule).Result.OrderBy(x => x.Name));
            this.serviceDocument.DomainData.Add("division", this.domainDataRepository.DivisionDomainGet().Result);
            ////this.bulkItemRepository.BulkItemDetailsSet(@"D:\Projects\Excel\BulkItem\Archive\BulkItem-245.xlsm");
            this.serviceDocument.LocalizationData.AddData("BulkItem");
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in Bulk Item Head page when Add button clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<BulkItemHeaderModel>> New()
        {
            this.serviceDocument.New(false);
            this.serviceDocument.DomainData.Add("division", this.domainDataRepository.DivisionDomainGet().Result);
            this.serviceDocument.LocalizationData.AddData("BulkItem");
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for populating fields in the Bulk Item Head page when Edit icon clicked.
        /// </summary>
        /// <param name="id">Cost Change Id</param>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<BulkItemHeaderModel>> Open(int id)
        {
            this.headSeq = id;
            await this.serviceDocument.OpenAsync(this.BulkItemOpen);
            this.serviceDocument.DomainData.Add("division", this.domainDataRepository.DivisionDomainGet().Result);
            this.serviceDocument.DomainData.Add("supplier", this.domainDataRepository.SupplierDomainGet(this.serviceDocument.DataProfile.DataModel.SupplierId.Value).Result);
            this.serviceDocument.LocalizationData.AddData("BulkItem");
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for submitting cost change to next level when Submit button clicked.
        /// </summary>
        /// <param name="serviceDocument">Service Document</param>
        /// <returns>ServiceDocument</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<BulkItemHeaderModel>> Submit([FromBody]ServiceDocument<BulkItemHeaderModel> serviceDocument)
        {
            this.serviceDocument = serviceDocument;
            this.inboxModel.StateID = GuidConverter.DotNetToOracle(Convert.ToString(serviceDocument.DataProfile.DataModel.WorkflowInstance?.WorkflowStateId));
            await this.serviceDocument.TransitAsync(this.BulkItemSave);

            if (this.serviceDocument.Result.Type == MessageType.Success)
            {
                if (this.serviceDocument.DataProfile.DataModel.WorkflowInstance.StateName == "Worksheet")
                {
                    this.inboxModel.Operation = "DFT";
                }

                await this.inboxRepository.SetInbox(this.inboxModel, this.serviceDocument);
            }

            var stateName = RbsServiceDocCustomizationHelper.GetStateNameByProfileIdAndCurActionId<BulkItemHeaderModel>(this.serviceDocument);
            if (stateName == "RequestCompleted")
            {
                var filePaths = this.domainDataRepository.FilePathGet(this.inboxModel.ModuleName).Result;
                string rootFolderPath = RbSettings.RbsExcelFileDownloadPath;
                string destinationPath = filePaths.ProcessPath;
                string file = "BulkItem-" + this.serviceDocument.DataProfile.DataModel.HEAD_SEQ.ToString() + ".xlsm";
                string fileToMove = Path.Combine(rootFolderPath, file);
                string moveTo = Path.Combine(destinationPath, file);

                //// moving file
                System.IO.File.Move(fileToMove, moveTo);
                if (System.IO.File.Exists(moveTo))
                {
                    this.bulkItemRepository.BulkItemDetailsSet(moveTo);
                    string fileToArchieve = Path.Combine(filePaths.ArchivePath, file);
                    System.IO.File.Move(moveTo, fileToArchieve);
                }
            }

            return this.serviceDocument;
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<ServiceDocument<BulkItemHeaderModel>> Reject([FromBody]ServiceDocument<BulkItemHeaderModel> serviceDocument)
        {
            this.serviceDocument = serviceDocument;
            this.inboxModel.StateID = RbsServiceDocCustomizationHelper.GetStateIdDotNetToOracle(serviceDocument);
            await this.serviceDocument.TransitAsync(GuidConverter.DotNetToOracle(serviceDocument.DataProfile.DataModel.WorkflowForm.StateId), true);
            if (this.serviceDocument.Result.Type == MessageType.Success)
            {
                this.inboxModel.Operation = "REJ";
                this.inboxModel.ActionType = "R";
                this.inboxModel.ToStateID = GuidConverter.DotNetToOracle(this.serviceDocument.DataProfile.DataModel.WorkflowStateId.ToString());
                this.inboxRepository.SetInbox(this.inboxModel, this.serviceDocument);
            }

            return this.serviceDocument;
        }

        public async Task<ServiceDocument<BulkItemHeaderModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.BulkItemSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<BulkItemHeaderModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.BulkItemSave);
            return this.serviceDocument;
        }

        public System.Threading.Tasks.Task<FileResult> OpenLocalItemExcel(int id)
        {
            return this.DownloadLocalItemExcel(id);
        }

        public async System.Threading.Tasks.Task<FileResult> DownloadLocalItemExcel(int id)
        {
            ////var user = Core.Utility.UserProfileHelper.GetInMemoryUser();
            ////var companyid = this.HttpContext.Request.Headers["companyid"];
            ////var jwtTokenTuple = System.Tuple.Create<string, string, string>(this.HttpContext.Session.Id, companyid, Core.JwtTokenHelper.GetToken(user.UserId, user.UserName));
            FilePathModel file = this.domainDataRepository.FilePathGet(this.inboxModel.ModuleName).Result;
            string xlsm = $"{RbSettings.RbsExcelFileDownloadPath}\\{Guid.NewGuid().ToString()}.xlsm";
            string bulkItemFile = $"{RbSettings.RbsExcelFileDownloadPath}\\BulkItem-{id}.xlsm";
            string bulkItemFileArc = $"{file.ArchivePath}\\BulkItem-{id}.xlsm";
            string bulkItemFileProcess = $"{file.ProcessPath}\\BulkItem-{id}.xlsm";

            if (System.IO.File.Exists(bulkItemFile))
            {
                System.IO.File.Copy(bulkItemFile, xlsm);
            }
            else if (System.IO.File.Exists(bulkItemFileProcess))
            {
                System.IO.File.Copy(bulkItemFileProcess, xlsm);
            }
            else if (System.IO.File.Exists(bulkItemFileArc))
            {
                System.IO.File.Copy(bulkItemFileArc, xlsm);
            }
            else
            {
                BulkItemHeaderModel bulkItem = await this.bulkItemRepository.BulkItemGet(id, this.serviceDocumentResult);
                string templateFile = bulkItem.SupplierType == "LOCAL" ? $"{file.TemplatePath}\\Local Item Creation.xlsm" : $"{file.TemplatePath}\\Foreign Item Creation.xlsm";
                System.IO.File.Copy(templateFile, xlsm);
                this.BulkItemExcelBuilder(xlsm, bulkItem);
            }

            return await DocumentsHelper.GetFilestream(xlsm, this);
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<bool> FileUpload([FromBody]FileDataUpload file)
        {
            return await DocumentsRepository.CopyingFile(file);
        }

        private async Task<List<BulkItemHeaderModel>> BulkItemSearch()
        {
            try
            {
                var lstUomClass = await this.bulkItemRepository.GetBulkItem(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstUomClass;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<BulkItemHeaderModel> BulkItemOpen()
        {
            try
            {
                FilePathModel file = this.domainDataRepository.FilePathGet(this.inboxModel.ModuleName).Result;
                var bulkItem = await this.bulkItemRepository.BulkItemGet(this.headSeq, file, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return bulkItem;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> BulkItemSave()
        {
            this.serviceDocument.DataProfile.DataModel.Status = RbsServiceDocCustomizationHelper.GetStateNameByProfileIdAndCurActionId(this.serviceDocument);
            this.serviceDocument.Result = await this.bulkItemRepository.BulkItemSet(this.serviceDocument.DataProfile.DataModel);
            if (this.serviceDocument.Result.Type != MessageType.Success)
            {
                return false;
            }

            this.serviceDocument.DataProfile.DataModel.HEAD_SEQ = this.bulkItemRepository.HeadSeq;
            return true;
        }

        private void BulkItemExcelBuilder(string xlsm, BulkItemHeaderModel bulkItem)
        {
            using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(xlsm, true))
            {
                WorkbookPart workbookPart1 = spreadSheet.WorkbookPart;
                Sheet sheet1 = workbookPart1.Workbook.Descendants<Sheet>().First(s => s.Name == "Creation Sheet");
                WorksheetPart worksheetPart1 = workbookPart1.GetPartById(sheet1.Id.Value) as WorksheetPart;
                Worksheet bulkworksheet = worksheetPart1.Worksheet;
                try
                {
                    if (bulkItem.SupplierType == "LOCAL")
                    {
                        ExcelHelper.WriteToCell<string>("F", 4, bulkworksheet, DateTime.Now.ToString("dd/MM/yyyy"));
                        ExcelHelper.WriteToCell<int>("F", 6, bulkworksheet, Convert.ToInt32(bulkItem.SupplierId));
                        ExcelHelper.WriteToCell<string>("F", 7, bulkworksheet, Convert.ToString(bulkItem.HEAD_SEQ));
                        ExcelHelper.WriteToCell<string>("F", 9, bulkworksheet, bulkItem.SupplierName);
                        ExcelHelper.WriteToCell<int>("F", 10, bulkworksheet, Convert.ToInt32(bulkItem.TotalRebates ?? 0));
                        ExcelHelper.WriteToCell<string>("F", 8, bulkworksheet, bulkItem.DivisionDesc + "(" + bulkItem.DivisionId + ")");
                        ExcelHelper.WriteToCell<string>("F", 11, bulkworksheet, bulkItem.SupplierType);
                    }
                    else
                    {
                        ExcelHelper.WriteToCell<string>("F", 3, bulkworksheet, DateTime.Now.ToString("dd/MM/yyyy"));
                        ExcelHelper.WriteToCell<int>("F", 5, bulkworksheet, Convert.ToInt32(bulkItem.SupplierId));
                        ExcelHelper.WriteToCell<string>("F", 6, bulkworksheet, bulkItem.SupplierName);
                        ExcelHelper.WriteToCell<string>("F", 7, bulkworksheet, bulkItem.OriginCountryName);
                        ExcelHelper.WriteToCell<string>("F", 8, bulkworksheet, bulkItem.IncotermsDesc);
                        ExcelHelper.WriteToCell<string>("F", 9, bulkworksheet, bulkItem.CurrencyCode);
                        ExcelHelper.WriteToCell<int>("F", 10, bulkworksheet, Convert.ToInt32(bulkItem.TotalRebates ?? 0));
                        ExcelHelper.WriteToCell<string>("F", 12, bulkworksheet, bulkItem.DivisionDesc + "(" + bulkItem.DivisionId + ")");
                        ExcelHelper.WriteToCell<string>("F", 11, bulkworksheet, Convert.ToString(bulkItem.HEAD_SEQ));
                    }

                    bulkworksheet.Save();
                    Sheet sheetTempData = workbookPart1.Workbook.Descendants<Sheet>().FirstOrDefault(s => s.Name == "TempData");
                    WorksheetPart worksheetPartTempData = workbookPart1.GetPartById(sheetTempData.Id.Value) as WorksheetPart;
                    Worksheet bulkworksheetTempData = worksheetPartTempData.Worksheet;
                    ExcelHelper.WriteToCell<string>("A", 1, bulkworksheetTempData, RbSettings.Web);
                    bulkworksheetTempData.Save();
                }
                catch (Exception ex)
                {
                    this.serviceDocument.Result = new ServiceDocumentResult
                    {
                        InnerException = "Exception",
                        StackTrace = ex.ToString(),
                        Type = MessageType.Exception
                    };
                }
            }
        }
    }
}
