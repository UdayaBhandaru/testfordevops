﻿// <copyright file="BulkItemMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.BulkItem.Mapping
{
    using System;
    using Agility.RBS.Core;
    using Agility.RBS.ItemManagement.BulkItem.Models;
    using Agility.RBS.ItemManagement.Item.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class BulkItemMapping : Profile
    {
        public BulkItemMapping()
            : base("BulkItemMapping")
        {
            this.CreateMap<OracleObject, BulkItemHeaderModel>()
                .ForMember(m => m.HEAD_SEQ, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["HEAD_SEQ"])))
                .ForMember(m => m.Description, opt => opt.MapFrom(r => r["DESCRIPTION"]))
                .ForMember(m => m.SupplierId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["SUPPLIER_ID"])))
                .ForMember(m => m.SupplierName, opt => opt.MapFrom(r => r["SUPPLIER_NAME"]))
                .ForMember(m => m.SupplierType, opt => opt.MapFrom(r => r["SUPPLIER_TYPE"]))
                .ForMember(m => m.DivisionId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DIVISION_ID"])))
                .ForMember(m => m.DivisionDesc, opt => opt.MapFrom(r => r["DIVISION_DESC"]))
                .ForMember(m => m.TotalRebates, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["TOTAL_REBATES"])))
                .ForMember(m => m.OriginCountryId, opt => opt.MapFrom(r => r["ORIGIN_COUNTRY_ID"]))
                .ForMember(m => m.OriginCountryName, opt => opt.MapFrom(r => r["ORIGIN_COUNTRY_NAME"]))
                .ForMember(m => m.CurrencyCode, opt => opt.MapFrom(r => r["CURRENCY_CODE"]))
                .ForMember(m => m.IncotermsCode, opt => opt.MapFrom(r => r["INCOTERMS_CODE"]))
                .ForMember(m => m.IncotermsDesc, opt => opt.MapFrom(r => r["INCOTERMS_DESC"]))
                .ForMember(m => m.CreatedBy, opt => opt.MapFrom(r => r["CREATED_BY"]))
                .ForMember(m => m.ModifiedBy, opt => opt.MapFrom(r => r["LAST_UPDATED_BY"]))
                .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
                .ForMember(m => m.CreatedDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["CREATED_DATE"])))
                .ForMember(m => m.ModifiedDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["LAST_UPDATED_DATE"])))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "BULK_ITEM_HEAD"))
                .ForMember(m => m.Status, opt => opt.MapFrom(r => r["STATUS"]))
                .ForMember(m => m.FileCount, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["FILE_CNT"])))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, BulkItemDetailModel>()
              .ForMember(m => m.Item, opt => opt.MapFrom(r => r["ITEM"]))
              .ForMember(m => m.ItemDesc, opt => opt.MapFrom(r => r["item_desc"]))
              .ForMember(m => m.DivisionDesc, opt => opt.MapFrom(r => r["DIVISION_DESC"]))
              .ForMember(m => m.GroupDesc, opt => opt.MapFrom(r => r["GROUP_DESC"]))
              .ForMember(m => m.DeptDesc, opt => opt.MapFrom(r => r["DEPT_DESC"]))
               .ForMember(m => m.ClassDesc, opt => opt.MapFrom(r => r["FINELINE_DESC"]))
               .ForMember(m => m.SubclassDesc, opt => opt.MapFrom(r => r["SEGMENT_DESC"]));

            this.CreateMap<OracleObject, BulkItemHeaderModel>()
               .ForMember(m => m.HEAD_SEQ, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["HEAD_SEQ"])))
               .ForMember(m => m.Description, opt => opt.MapFrom(r => r["DESCRIPTION"]))
               .ForMember(m => m.SupplierId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["SUPPLIER_ID"])))
               .ForMember(m => m.SupplierName, opt => opt.MapFrom(r => r["SUPPLIER_NAME"]))
               .ForMember(m => m.SupplierType, opt => opt.MapFrom(r => r["SUPPLIER_TYPE"]))
               .ForMember(m => m.DivisionId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DIVISION_ID"])))
               .ForMember(m => m.DivisionDesc, opt => opt.MapFrom(r => r["DIVISION_DESC"]))
               .ForMember(m => m.TotalRebates, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["TOTAL_REBATES"])))
               .ForMember(m => m.OriginCountryId, opt => opt.MapFrom(r => r["ORIGIN_COUNTRY_ID"]))
               .ForMember(m => m.OriginCountryName, opt => opt.MapFrom(r => r["ORIGIN_COUNTRY_NAME"]))
               .ForMember(m => m.CurrencyCode, opt => opt.MapFrom(r => r["CURRENCY_CODE"]))
               .ForMember(m => m.IncotermsCode, opt => opt.MapFrom(r => r["INCOTERMS_CODE"]))
               .ForMember(m => m.IncotermsDesc, opt => opt.MapFrom(r => r["INCOTERMS_DESC"]))
               .ForMember(m => m.CreatedBy, opt => opt.MapFrom(r => r["CREATED_BY"]))
               .ForMember(m => m.ModifiedBy, opt => opt.MapFrom(r => r["LAST_UPDATED_BY"]))
               .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
               .ForMember(m => m.CreatedDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["CREATED_DATE"])))
               .ForMember(m => m.ModifiedDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["LAST_UPDATED_DATE"])))
               .ForMember(m => m.TableName, opt => opt.MapFrom(r => "BULK_ITEM_HEAD"))
               .ForMember(m => m.Status, opt => opt.MapFrom(r => r["STATUS"]))
               .ForMember(m => m.FileCount, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["FILE_CNT"])))
               .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));
        }
    }
}
