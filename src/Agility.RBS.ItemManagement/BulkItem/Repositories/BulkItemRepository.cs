﻿// <copyright file="BulkItemRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// BulkItemRepository
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.ItemManagement.BulkItem.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.ItemManagement.BulkItem.Models;
    using Agility.RBS.ItemManagement.Common;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Repositories.Models;
    using AutoMapper;
    using Devart.Data.Oracle;
    using DocumentFormat.OpenXml;
    using DocumentFormat.OpenXml.Packaging;
    using DocumentFormat.OpenXml.Spreadsheet;
    using Microsoft.AspNetCore.Authorization;

    public class BulkItemRepository : BaseOraclePackage
    {
        public BulkItemRepository()
        {
            this.PackageName = ItemManagementConstants.GetItemPackage;
        }

        public int HeadSeq { get; private set; }

        public static string IdGet(string str)
        {
            if (!string.IsNullOrEmpty(str))
            {
                return str.Remove(str.IndexOf(')')).Substring(str.IndexOf('(') + 1);
            }

            return string.Empty;
        }

        public static string DomaimValueGet(string str) => UpperCase(str) == "YES" ? "Y" : "N";

        public static string TransLevelGet(string str) => UpperCase(str) == "LEVEL 1" ? "1" : "2";

        public static string SomValueGet(string str)
        {
            string somValue = string.Empty;
            switch (str)
            {
                case "Each":
                    somValue = "E";
                    break;
                case "Inner":
                    somValue = "I";
                    break;
                case "Case":
                    somValue = "C";
                    break;
                default:
                    somValue = "E";
                    break;
            }

            return somValue;
        }

        public static string ItemTypeGet(string str)
        {
            string itmTyp = string.Empty;
            switch (UpperCase(str))
            {
                case "REGULAR ITEM":
                    itmTyp = "R";
                    break;
                case "SIMPLE PACK":
                    itmTyp = "S";
                    break;
                case "COMPLEX PACK":
                    itmTyp = "C";
                    break;
                case "CONSIGNMENT":
                    itmTyp = "I";
                    break;
                default:
                    itmTyp = "R";
                    break;
            }

            return itmTyp;
        }

        public async Task<BulkItemHeaderModel> ConvertExcelDataToType(DataTable eSheet, BulkItemHeaderModel iHeadModel)
        {
            if (eSheet != null)
            {
                List<BulkItemDetailModel> orderDetailsList = (from DataRow dr in eSheet.Rows
                                                              where Convert.ToString(dr[1]) != "Seq" && Convert.ToString(dr[3]) != string.Empty
                                                              select new BulkItemDetailModel
                                                              {
                                                                  GroupDesc = Convert.ToString(dr[3]),
                                                                  DeptDesc = Convert.ToString(dr[4]),
                                                                  ClassDesc = Convert.ToString(dr[5]),
                                                                  SubclassDesc = Convert.ToString(dr[6]),
                                                                  ItemDesc = Convert.ToString(dr[8]),
                                                              }).ToList();

                iHeadModel.BulkItemDetails.AddRange(orderDetailsList);
            }

            return iHeadModel;
        }

        public async Task<List<BulkItemHeaderModel>> GetBulkItem(BulkItemHeaderModel bulkItemHeaderModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(ItemManagementConstants.ObjTypeBulkItemHead, ItemManagementConstants.GetProcBulkItemHead);
            OracleObject recordObject = this.SetParamsBulkItem(bulkItemHeaderModel, true);
            return await this.GetProcedure2<BulkItemHeaderModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<BulkItemHeaderModel> BulkItemGet(int headSeq, FilePathModel file, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType(ItemManagementConstants.RecordObjBulkItemHead);
            OracleObject recordObject = this.GetOracleObject(recordType);
            this.SetheadSeq(headSeq, recordObject);
            PackageParams packageParameter = this.GetPackageParams(ItemManagementConstants.ObjTypeBulkItemHead, ItemManagementConstants.GetProcBulkItemHead);
            var bulkItemHeaders = await this.GetProcedure2<BulkItemHeaderModel>(recordObject, packageParameter, serviceDocumentResult);
            if (bulkItemHeaders != null && bulkItemHeaders.Count > 0)
            {
                string bulkItemFile = $"{RbSettings.RbsExcelFileDownloadPath}\\BulkItem-{bulkItemHeaders[0].HEAD_SEQ.Value}.xlsm";
                if (bulkItemHeaders[0].Status == "RequestCompleted")
                {
                    bulkItemFile = $"{file.ProcessPath}\\BulkItem-{headSeq}.xlsm";
                    if (File.Exists(bulkItemFile))
                    {
                        await this.ConvertExcelDataToType(await this.GetDataTableByReadingExcel(bulkItemFile), bulkItemHeaders[0]);
                    }
                    else
                    {
                        var bulkItems = this.NewItemGet(headSeq, serviceDocumentResult).Result;
                        if (bulkItems != null)
                        {
                            bulkItemHeaders[0].BulkItemDetails.AddRange(bulkItems);
                        }
                    }
                }
                else
                {
                    if (File.Exists(bulkItemFile))
                    {
                        await this.ConvertExcelDataToType(await this.GetDataTableByReadingExcel(bulkItemFile), bulkItemHeaders[0]);
                    }
                }

                return bulkItemHeaders[0];
            }

            return null;
        }

        public async Task<BulkItemHeaderModel> BulkItemGet(int headSeq, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType(ItemManagementConstants.RecordObjBulkItemHead);
            OracleObject recordObject = this.GetOracleObject(recordType);
            this.SetheadSeq(headSeq, recordObject);
            PackageParams packageParameter = this.GetPackageParams(ItemManagementConstants.ObjTypeBulkItemHead, ItemManagementConstants.GetProcBulkItemHead);
            var bulkItemHeaders = await this.GetProcedure2<BulkItemHeaderModel>(recordObject, packageParameter, serviceDocumentResult);
            if (bulkItemHeaders != null && bulkItemHeaders.Count > 0)
            {
                return bulkItemHeaders[0];
            }

            return null;
        }

        public virtual void SetheadSeq(int headSeq, OracleObject recordObject)
        {
            recordObject["HEAD_SEQ"] = headSeq;
        }

        public async Task<ServiceDocumentResult> BulkItemSet(BulkItemHeaderModel bulkItemHeaderModel)
        {
            this.HeadSeq = 0;
            PackageParams packageParameter = this.GetPackageParams(ItemManagementConstants.ObjTypeBulkItemHead, ItemManagementConstants.SetProcBulkItemHead);
            OracleObject recordObject = this.SetParamsBulkItem(bulkItemHeaderModel, false);
            var result = await this.SetProcedure(recordObject, packageParameter);
            if (this.ObjResult != null)
            {
                this.HeadSeq = Convert.ToInt32(this.ObjResult["HEAD_SEQ"]);
            }

            return result;
        }

        public void BulkItemDetailsSet(string fileName)
        {
            PackageParams packageParameter = this.GetPackageParams("foreign_item_tab", "setforeignitem");
            OracleObject recordObject = this.BulkItemCreation(fileName);
            this.SetProcedureBulk(recordObject, packageParameter).ConfigureAwait(true);
        }

        public OracleObject BulkItemCreation(string fileName)
        {
            using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(fileName, false))
            {
                WorkbookPart workbookPart = spreadSheet.WorkbookPart;
                Sheet sheet = workbookPart.Workbook.Descendants<Sheet>().First(s => s.Name == "Creation Sheet");
                WorksheetPart worksheetPart = workbookPart.GetPartById(sheet.Id.Value) as WorksheetPart;
                var rows = worksheetPart.Worksheet.Descendants<Row>().ToList();
                this.Connection.Open();
                OracleType recordType = this.GetObjectType("FOREIGN_ITEM_REC");
                OracleObject recordObject = this.GetOracleObject(recordType);

                string supplierType = ExcelHelper.GetCellValue("F", 11, worksheetPart.Worksheet, workbookPart);

                // Detail Object
                OracleType dtlTableType = this.GetObjectType("FOREIGN_ITEM_DETAIL_TAB");
                OracleType dtlRecordType = this.GetObjectType("FOREIGN_ITEM_DETAIL_REC");
                OracleTable pUtlDmnDtlTab = new OracleTable(dtlTableType);
                if (supplierType == "LOCAL")
                {
                    recordObject["HEADER_ID"] = ExcelHelper.GetCellValue("F", 7, worksheetPart.Worksheet, workbookPart);
                    recordObject["Supplier"] = ExcelHelper.GetCellValue("F", 6, worksheetPart.Worksheet, workbookPart);
                    for (int i = 15; i < rows.Count; i++)
                    {
                        string itemName = ExcelHelper.GetCellValue("I", i + 1, worksheetPart.Worksheet, workbookPart);
                        if (!string.IsNullOrEmpty(itemName))
                        {
                            OracleObject dtlRecordObject = this.GetOracleObject(dtlRecordType);
                            dtlRecordObject["COMPANY_ID"] = 1;
                            dtlRecordObject["COUNTRY_CODE"] = "KWT";
                            dtlRecordObject["REGION_CODE"] = "KWT";
                            dtlRecordObject["SEQ"] = ExcelHelper.GetCellValue("B", i + 1, worksheetPart.Worksheet, workbookPart);
                            dtlRecordObject["DIVISION_ID"] = IdGet(ExcelHelper.GetCellValue("F", 8, worksheetPart.Worksheet, workbookPart));
                            dtlRecordObject["DEPT_ID"] = IdGet(ExcelHelper.GetCellValue("D", i + 1, worksheetPart.Worksheet, workbookPart));
                            dtlRecordObject["CATEGORY_ID"] = IdGet(ExcelHelper.GetCellValue("E", i + 1, worksheetPart.Worksheet, workbookPart));
                            dtlRecordObject["FL_ID"] = IdGet(ExcelHelper.GetCellValue("F", i + 1, worksheetPart.Worksheet, workbookPart));
                            dtlRecordObject["SEGMENT_ID"] = IdGet(ExcelHelper.GetCellValue("G", i + 1, worksheetPart.Worksheet, workbookPart));
                            dtlRecordObject["ITEM"] = NumericCellNullCheck(ExcelHelper.GetCellValue("H", i + 1, worksheetPart.Worksheet, workbookPart));
                            dtlRecordObject["DESC_LONG"] = itemName;
                            dtlRecordObject["SHORT_DESC"] = ExcelHelper.GetCellValue("J", i + 1, worksheetPart.Worksheet, workbookPart);
                            dtlRecordObject["SECONDARY_DESC"] = ExcelHelper.GetCellValue("K", i + 1, worksheetPart.Worksheet, workbookPart);
                            dtlRecordObject["ITEM_TIER"] = ExcelHelper.GetCellValue("L", i + 1, worksheetPart.Worksheet, workbookPart);
                            dtlRecordObject["ITEM_TYPE"] = ItemTypeGet(ExcelHelper.GetCellValue("M", i + 1, worksheetPart.Worksheet, workbookPart));
                            dtlRecordObject["SELLABLE_IND"] = DomaimValueGet(ExcelHelper.GetCellValue("N", i + 1, worksheetPart.Worksheet, workbookPart));
                            dtlRecordObject["ORDERABLE_IND"] = DomaimValueGet(ExcelHelper.GetCellValue("O", i + 1, worksheetPart.Worksheet, workbookPart));
                            dtlRecordObject["INVENTORY_IND"] = DomaimValueGet(ExcelHelper.GetCellValue("P", i + 1, worksheetPart.Worksheet, workbookPart));
                            dtlRecordObject["REPLENISHMENT_IND"] = DomaimValueGet(ExcelHelper.GetCellValue("Q", i + 1, worksheetPart.Worksheet, workbookPart));
                            dtlRecordObject["COMP_ITEM"] = NumericCellNullCheck(ExcelHelper.GetCellValue("R", i + 1, worksheetPart.Worksheet, workbookPart));
                            dtlRecordObject["COMP_DESC"] = ExcelHelper.GetCellValue("S", i + 1, worksheetPart.Worksheet, workbookPart);

                            string compQty = ExcelHelper.GetCellValue("T", i + 1, worksheetPart.Worksheet, workbookPart);
                            dtlRecordObject["COMP_QTY"] = NumericCellNullCheck(compQty);

                            dtlRecordObject["ITEM_BARCODE"] = ExcelHelper.GetCellValue("U", i + 1, worksheetPart.Worksheet, workbookPart);
                            string brandName = ExcelHelper.GetCellValue("V", i + 1, worksheetPart.Worksheet, workbookPart);
                            dtlRecordObject["BRAND_NAME"] = brandName;
                            dtlRecordObject["EXPIRY_FLAG"] = DomaimValueGet(ExcelHelper.GetCellValue("W", i + 1, worksheetPart.Worksheet, workbookPart));

                            string expiryInbound = ExcelHelper.GetCellValue("X", i + 1, worksheetPart.Worksheet, workbookPart);
                            dtlRecordObject["EXPIRY_INBOUND"] = !string.IsNullOrEmpty(expiryInbound) ? expiryInbound : "0";

                            string expiryOutbound = ExcelHelper.GetCellValue("Y", i + 1, worksheetPart.Worksheet, workbookPart);
                            dtlRecordObject["EXPIRY_OUTBOUND"] = !string.IsNullOrEmpty(expiryOutbound) ? expiryOutbound : "0";
                            dtlRecordObject["ITEM_NUMBER_TYPE"] = ExcelHelper.GetCellValue("CB", i + 1, worksheetPart.Worksheet, workbookPart);
                            dtlRecordObject["TRAN_LEVEL"] = TransLevelGet(ExcelHelper.GetCellValue("Z", i + 1, worksheetPart.Worksheet, workbookPart));
                            dtlRecordObject["SOM"] = SomValueGet(ExcelHelper.GetCellValue("AA", i + 1, worksheetPart.Worksheet, workbookPart));
                            string standardUom = ExcelHelper.GetCellValue("AB", i + 1, worksheetPart.Worksheet, workbookPart);
                            dtlRecordObject["STANDARD_UOM"] = standardUom;
                            dtlRecordObject["UOP"] = ExcelHelper.GetCellValue("AC", i + 1, worksheetPart.Worksheet, workbookPart);
                            dtlRecordObject["VPN"] = ExcelHelper.GetCellValue("AD", i + 1, worksheetPart.Worksheet, workbookPart);
                            dtlRecordObject["HTS"] = ExcelHelper.GetCellValue("AE", i + 1, worksheetPart.Worksheet, workbookPart);
                            dtlRecordObject["ORIGIN_COUNTRY"] = ExcelHelper.GetCellValue("AF", i + 1, worksheetPart.Worksheet, workbookPart);

                            dtlRecordObject["INNER_QTY"] = NumericCellNullCheck(ExcelHelper.GetCellValue("AG", i + 1, worksheetPart.Worksheet, workbookPart));
                            dtlRecordObject["CASE_QTY"] = NumericCellNullCheck(ExcelHelper.GetCellValue("AH", i + 1, worksheetPart.Worksheet, workbookPart));
                            dtlRecordObject["SUPP_CASE_COST"] = NumericCellNullCheck(ExcelHelper.GetCellValue("AI", i + 1, worksheetPart.Worksheet, workbookPart));
                            dtlRecordObject["SUPP_UNIT_COST"] = NumericCellNullCheck(ExcelHelper.GetCellValue("AJ", i + 1, worksheetPart.Worksheet, workbookPart));
                            dtlRecordObject["INVOICE_DISCOUNT"] = NumericCellNullCheck(ExcelHelper.GetCellValue("AK", i + 1, worksheetPart.Worksheet, workbookPart));
                            dtlRecordObject["SYSTEM_DESC"] = brandName + " " + itemName + " - " + compQty + " " + standardUom;

                            // RSP Object
                            OracleType rspTableType = this.GetObjectType("RSP_TAB");
                            OracleTable pRspTab = new OracleTable(rspTableType);
                            this.RspDetailsGet(pRspTab, i, worksheetPart, "SS", "AN", workbookPart);
                            this.RspDetailsGet(pRspTab, i, worksheetPart, "SS-", "AO", workbookPart);
                            this.RspDetailsGet(pRspTab, i, worksheetPart, "WHS", "AP", workbookPart);
                            this.RspDetailsGet(pRspTab, i, worksheetPart, "CS", "AQ", workbookPart);
                            dtlRecordObject["RSP"] = pRspTab;

                            // Market Object
                            OracleType marketTableType = this.GetObjectType("MARKET_PRICE_TAB");
                            OracleTable pMarketTab = new OracleTable(marketTableType);
                            this.MarketRspGet(pMarketTab, i, worksheetPart, "204", "BD", workbookPart); ////LULU
                            this.MarketRspGet(pMarketTab, i, worksheetPart, "202", "BE", workbookPart); ////Carrefour
                            this.MarketRspGet(pMarketTab, i, worksheetPart, "203", "BF", workbookPart); ////COOP
                            this.MarketRspGet(pMarketTab, i, worksheetPart, "210", "BG", workbookPart); ////Saveco
                            dtlRecordObject["MARKET_PRICE"] = pMarketTab;

                            // Loc Object
                            OracleType locTableType = this.GetObjectType("STORE_RANGING_TAB");
                            OracleTable pLocTab = new OracleTable(locTableType);
                            this.LocDetailsGet(pLocTab, i, worksheetPart, "13", "BH", workbookPart); ////Salmiya
                            this.LocDetailsGet(pLocTab, i, worksheetPart, "29", "BI", workbookPart); ////Sharq
                            this.LocDetailsGet(pLocTab, i, worksheetPart, "38", "BJ", workbookPart); ////Al- Kout
                            this.LocDetailsGet(pLocTab, i, worksheetPart, "12", "BK", workbookPart); ////Fahaheel.
                            this.LocDetailsGet(pLocTab, i, worksheetPart, "14", "BL", workbookPart); ////Salwa
                            this.LocDetailsGet(pLocTab, i, worksheetPart, "15", "BM", workbookPart); ////Ahmadi
                            this.LocDetailsGet(pLocTab, i, worksheetPart, "23", "BN", workbookPart); ////Jabriya
                            this.LocDetailsGet(pLocTab, i, worksheetPart, "25", "BO", workbookPart); ////Promenade
                            this.LocDetailsGet(pLocTab, i, worksheetPart, "16", "BP", workbookPart); ////Hawally
                            this.LocDetailsGet(pLocTab, i, worksheetPart, "30", "BQ", workbookPart); ////Egila
                            this.LocDetailsGet(pLocTab, i, worksheetPart, "45", "BR", workbookPart); ////Bulevard
                            this.LocDetailsGet(pLocTab, i, worksheetPart, "20", "BS", workbookPart); ////Shuwaikh
                            this.LocDetailsGet(pLocTab, i, worksheetPart, "24", "BT", workbookPart); ////Mangaf
                            this.LocDetailsGet(pLocTab, i, worksheetPart, "28", "BU", workbookPart); ////Jahra
                            this.LocDetailsGet(pLocTab, i, worksheetPart, "35", "BV", workbookPart); ////Sulaibiya
                            this.LocDetailsGet(pLocTab, i, worksheetPart, "37", "BW", workbookPart); ////Dhajeej

                            this.LocDetailsGet(pLocTab, i, worksheetPart, "1", "CA", workbookPart); ////DC
                            ////this.LocDetailsGet(pLocTab, i, worksheetPart, "A", "BX", workbookPart);
                            ////this.LocDetailsGet(pLocTab, i, worksheetPart, "B", "BY", workbookPart);
                            ////this.LocDetailsGet(pLocTab, i, worksheetPart, "C", "BZ", workbookPart);
                            ////this.LocDetailsGet(pLocTab, i, worksheetPart, "DC", "CA", workbookPart);
                            dtlRecordObject["STORE_RANGING"] = pLocTab;

                            pUtlDmnDtlTab.Add(dtlRecordObject);
                            recordObject["FOREIGN_ITEM_DETAIL"] = pUtlDmnDtlTab;
                        }
                        else
                        {
                            return recordObject;
                        }
                    }
                }
                else
                {
                    recordObject["HEADER_ID"] = ExcelHelper.GetCellValue("F", 11, worksheetPart.Worksheet, workbookPart);
                    recordObject["Supplier"] = ExcelHelper.GetCellValue("F", 5, worksheetPart.Worksheet, workbookPart);
                    for (int i = 17; i < rows.Count; i++)
                    {
                        string itemName = ExcelHelper.GetCellValue("I", i + 1, worksheetPart.Worksheet, workbookPart);
                        if (!string.IsNullOrEmpty(itemName))
                        {
                            var dtlRecordObject = this.GetOracleObject(dtlRecordType);
                            dtlRecordObject["COMPANY_ID"] = 1;
                            dtlRecordObject["COUNTRY_CODE"] = "KWT";
                            dtlRecordObject["REGION_CODE"] = "KWT";
                            dtlRecordObject["SEQ"] = ExcelHelper.GetCellValue("B", i + 1, worksheetPart.Worksheet, workbookPart);
                            dtlRecordObject["DIVISION_ID"] = IdGet(ExcelHelper.GetCellValue("F", 12, worksheetPart.Worksheet, workbookPart));
                            dtlRecordObject["DEPT_ID"] = IdGet(ExcelHelper.GetCellValue("D", i + 1, worksheetPart.Worksheet, workbookPart));
                            dtlRecordObject["CATEGORY_ID"] = IdGet(ExcelHelper.GetCellValue("E", i + 1, worksheetPart.Worksheet, workbookPart));
                            dtlRecordObject["FL_ID"] = IdGet(ExcelHelper.GetCellValue("F", i + 1, worksheetPart.Worksheet, workbookPart));
                            dtlRecordObject["SEGMENT_ID"] = IdGet(ExcelHelper.GetCellValue("G", i + 1, worksheetPart.Worksheet, workbookPart));
                            dtlRecordObject["ITEM"] = NumericCellNullCheck(ExcelHelper.GetCellValue("H", i + 1, worksheetPart.Worksheet, workbookPart));
                            dtlRecordObject["DESC_LONG"] = itemName;
                            dtlRecordObject["SHORT_DESC"] = ExcelHelper.GetCellValue("J", i + 1, worksheetPart.Worksheet, workbookPart);
                            dtlRecordObject["SECONDARY_DESC"] = ExcelHelper.GetCellValue("K", i + 1, worksheetPart.Worksheet, workbookPart);
                            dtlRecordObject["ITEM_TIER"] = ExcelHelper.GetCellValue("L", i + 1, worksheetPart.Worksheet, workbookPart);
                            dtlRecordObject["ITEM_TYPE"] = ItemTypeGet(ExcelHelper.GetCellValue("M", i + 1, worksheetPart.Worksheet, workbookPart));
                            dtlRecordObject["SELLABLE_IND"] = DomaimValueGet(ExcelHelper.GetCellValue("N", i + 1, worksheetPart.Worksheet, workbookPart));
                            dtlRecordObject["ORDERABLE_IND"] = DomaimValueGet(ExcelHelper.GetCellValue("O", i + 1, worksheetPart.Worksheet, workbookPart));
                            dtlRecordObject["INVENTORY_IND"] = DomaimValueGet(ExcelHelper.GetCellValue("P", i + 1, worksheetPart.Worksheet, workbookPart));
                            dtlRecordObject["REPLENISHMENT_IND"] = DomaimValueGet(ExcelHelper.GetCellValue("Q", i + 1, worksheetPart.Worksheet, workbookPart));
                            dtlRecordObject["COMP_ITEM"] = NumericCellNullCheck(ExcelHelper.GetCellValue("R", i + 1, worksheetPart.Worksheet, workbookPart));
                            dtlRecordObject["COMP_DESC"] = ExcelHelper.GetCellValue("S", i + 1, worksheetPart.Worksheet, workbookPart);
                            dtlRecordObject["ITEM_NUMBER_TYPE"] = ExcelHelper.GetCellValue("CG", i + 1, worksheetPart.Worksheet, workbookPart);

                            string compQty = ExcelHelper.GetCellValue("T", i + 1, worksheetPart.Worksheet, workbookPart);
                            dtlRecordObject["COMP_QTY"] = NumericCellNullCheck(compQty);
                            dtlRecordObject["GROCERY_ATTRIBUTES"] = ExcelHelper.GetCellValue("U", i + 1, worksheetPart.Worksheet, workbookPart);
                            dtlRecordObject["RETAIL_LABEL_TYPE"] = ExcelHelper.GetCellValue("V", i + 1, worksheetPart.Worksheet, workbookPart);
                            dtlRecordObject["NO_OF_DAYS"] = NumericCellNullCheck(ExcelHelper.GetCellValue("W", i + 1, worksheetPart.Worksheet, workbookPart));
                            dtlRecordObject["ITEM_BARCODE"] = ExcelHelper.GetCellValue("X", i + 1, worksheetPart.Worksheet, workbookPart);

                            string brandName = ExcelHelper.GetCellValue("Y", i + 1, worksheetPart.Worksheet, workbookPart);
                            dtlRecordObject["BRAND_NAME"] = brandName;
                            dtlRecordObject["EXPIRY_FLAG"] = DomaimValueGet(ExcelHelper.GetCellValue("Z", i + 1, worksheetPart.Worksheet, workbookPart));
                            dtlRecordObject["EXPIRY_INBOUND"] = NumericCellNullCheck(ExcelHelper.GetCellValue("AA", i + 1, worksheetPart.Worksheet, workbookPart));
                            dtlRecordObject["EXPIRY_OUTBOUND"] = NumericCellNullCheck(ExcelHelper.GetCellValue("AB", i + 1, worksheetPart.Worksheet, workbookPart));
                            dtlRecordObject["TRAN_LEVEL"] = TransLevelGet(ExcelHelper.GetCellValue("AC", i + 1, worksheetPart.Worksheet, workbookPart));
                            dtlRecordObject["SOM"] = SomValueGet(ExcelHelper.GetCellValue("AD", i + 1, worksheetPart.Worksheet, workbookPart));

                            string standardUom = ExcelHelper.GetCellValue("AE", i + 1, worksheetPart.Worksheet, workbookPart);
                            dtlRecordObject["STANDARD_UOM"] = standardUom;
                            dtlRecordObject["UOP"] = ExcelHelper.GetCellValue("AF", i + 1, worksheetPart.Worksheet, workbookPart);
                            dtlRecordObject["VPN"] = ExcelHelper.GetCellValue("AG", i + 1, worksheetPart.Worksheet, workbookPart);
                            dtlRecordObject["HTS"] = ExcelHelper.GetCellValue("AH", i + 1, worksheetPart.Worksheet, workbookPart);
                            dtlRecordObject["ORIGIN_COUNTRY"] = ExcelHelper.GetCellValue("AI", i + 1, worksheetPart.Worksheet, workbookPart);
                            dtlRecordObject["SHELF_LIFE"] = NumericCellNullCheck(ExcelHelper.GetCellValue("AJ", i + 1, worksheetPart.Worksheet, workbookPart));
                            dtlRecordObject["INNER_QTY"] = NumericCellNullCheck(ExcelHelper.GetCellValue("AK", i + 1, worksheetPart.Worksheet, workbookPart));
                            dtlRecordObject["CASE_QTY"] = NumericCellNullCheck(ExcelHelper.GetCellValue("AL", i + 1, worksheetPart.Worksheet, workbookPart));
                            dtlRecordObject["SUPP_CASE_COST"] = NumericCellNullCheck(ExcelHelper.GetCellValue("AM", i + 1, worksheetPart.Worksheet, workbookPart));
                            dtlRecordObject["SUPP_UNIT_COST"] = NumericCellNullCheck(ExcelHelper.GetCellValue("AN", i + 1, worksheetPart.Worksheet, workbookPart));
                            dtlRecordObject["LANDED_CASE_COST"] = NumericCellNullCheck(ExcelHelper.GetCellValue("AO", i + 1, worksheetPart.Worksheet, workbookPart));
                            dtlRecordObject["LANDED_UNIT_COST"] = NumericCellNullCheck(ExcelHelper.GetCellValue("AP", i + 1, worksheetPart.Worksheet, workbookPart));

                            dtlRecordObject["SYSTEM_DESC"] = brandName + " " + itemName + " - " + compQty + " " + standardUom;
                            string division = ExcelHelper.GetCellValue("F", 12, worksheetPart.Worksheet, workbookPart);

                            // RSP Object
                            OracleType rspTableType = this.GetObjectType("RSP_TAB");
                            OracleTable pRspTab = new OracleTable(rspTableType);

                            this.RspDetailsGet(pRspTab, i, worksheetPart, "SS", division == "HOMECENTER(3)" ? "AS" : "AQ", workbookPart);
                            this.RspDetailsGet(pRspTab, i, worksheetPart, "SS-", division == "HOMECENTER(3)" ? "AT" : "AQ", workbookPart);
                            this.RspDetailsGet(pRspTab, i, worksheetPart, "WHS", division == "HOMECENTER(3)" ? "AU" : "AQ", workbookPart);

                            if (division != "HOMECENTER(3)")
                            {
                                this.RspDetailsGet(pRspTab, i, worksheetPart, "CS", "AQ", workbookPart);
                            }

                            dtlRecordObject["RSP"] = pRspTab;

                            // Market Object
                            OracleType marketTableType = this.GetObjectType("MARKET_PRICE_TAB");
                            OracleTable pMarketTab = new OracleTable(marketTableType);
                            this.MarketRspGet(pMarketTab, i, worksheetPart, "204", "AV", workbookPart); ////Lulu
                            this.MarketRspGet(pMarketTab, i, worksheetPart, "210", "AW", workbookPart); ////Saveco
                            dtlRecordObject["MARKET_PRICE"] = pMarketTab;

                            // Loc Object
                            OracleType locTableType = this.GetObjectType("STORE_RANGING_TAB");
                            OracleTable pLocTab = new OracleTable(locTableType);
                            this.LocDetailsGet(pLocTab, i, worksheetPart, "13", "BN", workbookPart);
                            this.LocDetailsGet(pLocTab, i, worksheetPart, "29", "BO", workbookPart);
                            this.LocDetailsGet(pLocTab, i, worksheetPart, "38", "BP", workbookPart);
                            this.LocDetailsGet(pLocTab, i, worksheetPart, "12", "BQ", workbookPart);
                            this.LocDetailsGet(pLocTab, i, worksheetPart, "14", "BR", workbookPart);
                            this.LocDetailsGet(pLocTab, i, worksheetPart, "15", "BS", workbookPart);
                            this.LocDetailsGet(pLocTab, i, worksheetPart, "23", "BT", workbookPart);
                            this.LocDetailsGet(pLocTab, i, worksheetPart, "25", "BU", workbookPart);
                            this.LocDetailsGet(pLocTab, i, worksheetPart, "16", "BV", workbookPart);
                            this.LocDetailsGet(pLocTab, i, worksheetPart, "30", "BW", workbookPart);
                            this.LocDetailsGet(pLocTab, i, worksheetPart, "45", "BX", workbookPart);
                            this.LocDetailsGet(pLocTab, i, worksheetPart, "20", "BY", workbookPart);
                            this.LocDetailsGet(pLocTab, i, worksheetPart, "24", "BZ", workbookPart);
                            this.LocDetailsGet(pLocTab, i, worksheetPart, "28", "CA", workbookPart);
                            this.LocDetailsGet(pLocTab, i, worksheetPart, "35", "CB", workbookPart);
                            this.LocDetailsGet(pLocTab, i, worksheetPart, "37", "CC", workbookPart);
                            ////this.LocDetailsGet(pLocTab, i, worksheetPart, "A", "CD", workbookPart);
                            ////this.LocDetailsGet(pLocTab, i, worksheetPart, "B", "CE", workbookPart);
                            ////this.LocDetailsGet(pLocTab, i, worksheetPart, "C", "CF", workbookPart);
                            dtlRecordObject["STORE_RANGING"] = pLocTab;

                            pUtlDmnDtlTab.Add(dtlRecordObject);
                            recordObject["FOREIGN_ITEM_DETAIL"] = pUtlDmnDtlTab;
                        }
                        else
                        {
                            return recordObject;
                        }
                    }
                }

                return recordObject;
            }
        }

        public void RspDetailsGet(OracleTable pRspTab, int i, WorksheetPart worksheetPart, string formatName, string sheetColName, WorkbookPart workbookPart)
        {
            string ssValue = ExcelHelper.GetCellValue(sheetColName, i + 1, worksheetPart.Worksheet, workbookPart);
            if (!string.IsNullOrEmpty(ssValue))
            {
                OracleType rspRecordType = this.GetObjectType("RSP_REC");
                OracleObject rspRecordObject = this.GetOracleObject(rspRecordType);
                decimal price = Math.Round(Convert.ToDecimal(ssValue), 3);
                rspRecordObject["CUR_PRICE"] = price;
                rspRecordObject["format"] = formatName;
                pRspTab.Add(rspRecordObject);
            }
        }

        public void MarketRspGet(OracleTable pMarketRspTab, int i, WorksheetPart worksheetPart, string formatName, string sheetColName, WorkbookPart workbookPart)
        {
            var ssValue = ExcelHelper.GetCellValue(sheetColName, i + 1, worksheetPart.Worksheet, workbookPart);
            if (!string.IsNullOrEmpty(ssValue))
            {
                OracleType recordType = this.GetObjectType("MARKET_PRICE_REC");
                var recordObject = this.GetOracleObject(recordType);
                recordObject["competitor"] = formatName;
                recordObject["comp_name"] = formatName;
                recordObject["comp_retail"] = ssValue;
                pMarketRspTab.Add(recordObject);
            }
        }

        public void LocDetailsGet(OracleTable pLocTab, int i, WorksheetPart worksheetPart, string locName, string sheetColName, WorkbookPart workbookPart)
        {
            var ssValue = ExcelHelper.GetCellValue(sheetColName, i + 1, worksheetPart.Worksheet, workbookPart);
            if (!string.IsNullOrEmpty(ssValue))
            {
                OracleType locRecordType = this.GetObjectType("STORE_RANGING_REC");
                var locRecordObject = this.GetOracleObject(locRecordType);
                locRecordObject["loc_name"] = locName;
                locRecordObject["source_method"] = ssValue;
                pLocTab.Add(locRecordObject);
            }
        }

        public virtual void SetParamsBrandSearch(BulkItemHeaderModel bulkItemHeaderModel, OracleObject recordObject)
        {
            recordObject["HEAD_SEQ"] = bulkItemHeaderModel.HEAD_SEQ;
            recordObject["DESCRIPTION"] = bulkItemHeaderModel.Description;
            recordObject["DIVISION_ID"] = bulkItemHeaderModel.DivisionId;
            recordObject["SUPPLIER_ID"] = bulkItemHeaderModel.SupplierId;
            recordObject["STATUS"] = bulkItemHeaderModel.Status;
        }

        public virtual void SetParamsBrandSave(BulkItemHeaderModel bulkItemHeaderModel, OracleObject recordObject)
        {
            recordObject["Operation"] = bulkItemHeaderModel.Operation;
            recordObject["CREATED_BY"] = this.GetCreatedBy(bulkItemHeaderModel.CreatedBy);
            recordObject["LAST_UPDATED_BY"] = FxContext.Context.Name;
            recordObject["TOTAL_REBATES"] = bulkItemHeaderModel.TotalRebates;
        }

        public OracleObject SetParamsBulkItem(BulkItemHeaderModel bulkItemHeaderModel, bool isSearch)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType(ItemManagementConstants.RecordObjBulkItemHead);
            OracleObject recordObject = this.GetOracleObject(recordType);
            if (!isSearch)
            {
                this.SetParamsBrandSave(bulkItemHeaderModel, recordObject);
            }

            this.SetParamsBrandSearch(bulkItemHeaderModel, recordObject);
            return recordObject;
        }

        public async Task<List<BulkItemDetailModel>> NewItemGet(int headSeq, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            OracleParameter parameter = new OracleParameter("P_HEADER_ID", OracleDbType.Number)
            {
                Direction = System.Data.ParameterDirection.Input,
                Value = headSeq
            };
            parameters.Add(parameter);

            OracleType recordType = this.GetObjectType(ItemManagementConstants.RecordObjForNewlyCreatedItemsList);
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(ItemManagementConstants.ObjTypeForNewlyCreatedItemsList, ItemManagementConstants.GetProcForNewlyCreatedItemsList);
            return await this.GetProcedure2<BulkItemDetailModel>(recordObject, packageParameter, serviceDocumentResult, parameters);
        }
    }
}