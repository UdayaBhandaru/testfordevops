﻿//-------------------------------------------------------------------------------------------------
// <copyright file="ItemReclassificationController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// BulkItemController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.ItemManagement.ItemReclassification
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Constants;
    using Agility.RBS.Core.CustomExceptions;
    using Agility.RBS.Core.Models;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.Documents;
    using Agility.RBS.Documents.Models;
    using Agility.RBS.Inbox.Common;
    using Agility.RBS.Inbox.Models;
    using Agility.RBS.Inbox.Repository;
    using Agility.RBS.ItemManagement.Common;
    using Agility.RBS.ItemManagement.ItemReclassification.Models;
    using Agility.RBS.ItemManagement.ItemReclassification.Repositories;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Models;
    using DocumentFormat.OpenXml.Packaging;
    using DocumentFormat.OpenXml.Spreadsheet;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    [Route("api/[controller]/[action]")]
    public class ItemReclassificationController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly ItemReclassificationRepository itemReclassificationRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly ServiceDocument<ItemReclassificationModel> serviceDocument;

        public ItemReclassificationController(
            ServiceDocument<ItemReclassificationModel> serviceDocument,
            ItemReclassificationRepository itemReclassificationRepository,
            DomainDataRepository domainDataRepository)
        {
            this.itemReclassificationRepository = itemReclassificationRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in the Bulk Item List page.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        public async Task<ServiceDocument<ItemReclassificationModel>> List()
        {
            this.serviceDocument.DomainData.Add("department", this.domainDataRepository.DepartmentDomainGet().Result);
            this.serviceDocument.DomainData.Add("class", this.domainDataRepository.ClassDomainGet().Result);
            this.serviceDocument.LocalizationData.AddData("BulkItem");
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in Bulk Item Head page when Add button clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<ItemReclassificationModel>> New()
        {
            this.serviceDocument.New(false);
            this.serviceDocument.DomainData.Add("division", this.domainDataRepository.DivisionDomainGet().Result);
            this.serviceDocument.LocalizationData.AddData("BulkItem");
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<ItemReclassificationModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.ItemReclassificationSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<ItemReclassificationModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.ItemReclassificationSave);
            return this.serviceDocument;
        }

        private async Task<List<ItemReclassificationModel>> ItemReclassificationSearch()
        {
            try
            {
                var lstUomClass = await this.itemReclassificationRepository.GetItemReclassification(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstUomClass;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> ItemReclassificationSave()
        {
            this.serviceDocument.Result = await this.itemReclassificationRepository.ItemReclassificationSet(this.serviceDocument.DataProfile.DataModel);
            if (this.serviceDocument.Result.Type != MessageType.Success)
            {
                return false;
            }

            return true;
        }
    }
}
