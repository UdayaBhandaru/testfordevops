﻿// <copyright file="ItemReclassificationModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// ItemReclassification
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.ItemManagement.ItemReclassification.Models
{
    using System;
    using System.Collections.Generic;
    using Agility.Framework.Core.Profile.Entities;

    public class ItemReclassificationModel : ProfileEntity
    {
        public int? ReclassNo { get; set; }

        public string ReclassDesc { get; set; }

        public DateTime? ReclassDate { get; set; }

        public int? ToDept { get; set; }

        public string ToDeptDesc { get; set; }

        public int? ToClass { get; set; }

        public string ToClassDesc { get; set; }

        public int? ToSubclass { get; set; }

        public string ToSubclassDesc { get; set; }

        public string Item { get; set; }

        public string ItemDesc { get; set; }

        public string Operation { get; set; }

        public string DeptDesc { get; set; }

        public string ClassDesc { get; set; }

        public string SubclassDesc { get; set; }
    }
}
