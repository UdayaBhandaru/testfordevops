﻿// <copyright file="ItemReclassificationRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// BulkItemRepository
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.ItemManagement.ItemReclassification.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.ItemManagement.BulkItem.Models;
    using Agility.RBS.ItemManagement.Common;
    using Agility.RBS.ItemManagement.ItemReclassification.Models;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Repositories.Models;
    using AutoMapper;
    using Devart.Data.Oracle;
    using DocumentFormat.OpenXml;
    using DocumentFormat.OpenXml.Packaging;
    using DocumentFormat.OpenXml.Spreadsheet;
    using Microsoft.AspNetCore.Authorization;

    public class ItemReclassificationRepository : BaseOraclePackage
    {
        public ItemReclassificationRepository()
        {
            this.PackageName = ItemManagementConstants.GetItemPackage;
        }

        public async Task<List<ItemReclassificationModel>> GetItemReclassification(ItemReclassificationModel itemReclassificationModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(ItemManagementConstants.ObjTypeItemReclassification, ItemManagementConstants.GetProcItemReclassification);
            OracleObject recordObject = this.SetParamsItemReclassification(itemReclassificationModel);
            return await this.GetProcedure2<ItemReclassificationModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ServiceDocumentResult> ItemReclassificationSet(ItemReclassificationModel itemReclassificationModel)
        {
            PackageParams packageParameter = this.GetPackageParams(ItemManagementConstants.ObjTypeItemReclassification, ItemManagementConstants.SetProcItemReclassification);
            OracleObject recordObject = this.SetParamsItemReclassification(itemReclassificationModel);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        private OracleObject SetParamsItemReclassification(ItemReclassificationModel uomClassModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("ITEM_RECLASS_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            recordObject["RECLASS_NO"] = uomClassModel.ReclassNo;
            recordObject["RECLASS_DESC"] = uomClassModel.ReclassDesc;
            recordObject["RECLASS_DATE"] = uomClassModel.ReclassDate;
            recordObject["TO_DEPT"] = uomClassModel.ToDept;
            recordObject["TO_CLASS"] = uomClassModel.ToClass;
            recordObject["TO_SUBCLASS"] = uomClassModel.ToSubclass;
            recordObject["ITEM"] = uomClassModel.Item;
            recordObject["ITEM_DESC"] = uomClassModel.ItemDesc;
            recordObject["OPERATION"] = uomClassModel.Operation;
            return recordObject;
        }
    }
}