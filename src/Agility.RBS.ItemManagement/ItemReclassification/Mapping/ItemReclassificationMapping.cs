﻿// <copyright file="ItemReclassificationMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.ItemReclassification.Mapping
{
    using System;
    using Agility.RBS.Core;
    using Agility.RBS.ItemManagement.ItemReclassification.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class ItemReclassificationMapping : Profile
    {
        public ItemReclassificationMapping()
            : base("ItemReclassificationMapping")
        {
            this.CreateMap<OracleObject, ItemReclassificationModel>()
                .ForMember(m => m.ReclassNo, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["RECLASS_NO"])))
                .ForMember(m => m.ReclassDesc, opt => opt.MapFrom(r => r["RECLASS_DESC"]))
                .ForMember(m => m.ReclassDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["RECLASS_DATE"])))
                .ForMember(m => m.ToDept, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["TO_DEPT"])))
                .ForMember(m => m.ToDeptDesc, opt => opt.MapFrom(r => r["TO_DEPT_DESC"]))
                .ForMember(m => m.ToClass, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["TO_CLASS"])))
                .ForMember(m => m.ToClassDesc, opt => opt.MapFrom(r => r["TO_CLASS_DESC"]))
                .ForMember(m => m.ToSubclass, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["TO_SUBCLASS"])))
                .ForMember(m => m.ToSubclassDesc, opt => opt.MapFrom(r => r["TO_SUBCLASS_DESC"]))
                .ForMember(m => m.Item, opt => opt.MapFrom(r => r["ITEM"]))
                .ForMember(m => m.ItemDesc, opt => opt.MapFrom(r => r["ITEM_DESC"]))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));
        }
    }
}
