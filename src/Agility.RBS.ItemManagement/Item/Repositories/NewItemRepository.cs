﻿// <copyright file="NewItemRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.Item.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.Framework.Web.Security.Middleware;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.ItemManagement.Common;
    using Agility.RBS.ItemManagement.Item.Models;
    using Agility.RBS.MDM.Repositories.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class NewItemRepository : BaseOraclePackage
    {
        public NewItemRepository()
        {
            this.PackageName = ItemManagementConstants.GetNewItemPackage;
        }

        public int? HeadSeq { get; private set; }

        public string TranType { get; internal set; }

        public async Task<List<NewItemSearchModel>> NewItemListGetWithSdoc(ServiceDocument<NewItemSearchModel> serviceDoc, ServiceDocumentResult serviceDocumentResult)
        {
            NewItemSearchModel itemBasicInfoModel = serviceDoc.DataProfile.DataModel;
            PackageParams packageParameter = this.GetPackageParams(ItemManagementConstants.ObjTypeNewItemSearch, ItemManagementConstants.GetProcNewItemList);
            OracleObject recordObject = this.SetParamsNewItemList(itemBasicInfoModel);
            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();
            OracleParameter parameter;
            parameter = new OracleParameter("P_START", Devart.Data.Oracle.OracleDbType.Number) { Direction = System.Data.ParameterDirection.Input, Value = itemBasicInfoModel.StartRow };
            parameters.Add(parameter);
            parameter = new OracleParameter("P_END", OracleDbType.Number) { Direction = System.Data.ParameterDirection.Input, Value = itemBasicInfoModel.EndRow };
            parameters.Add(parameter);
            parameter = new OracleParameter("P_total", OracleDbType.Number) { Direction = System.Data.ParameterDirection.Output };
            parameters.Add(parameter);
            if (itemBasicInfoModel.SortModel == null)
            {
                itemBasicInfoModel.SortModel = new[] { new Core.Models.SortModel { ColId = null, Sort = null } };
            }

            parameter = new OracleParameter("P_USER", OracleDbType.VarChar) { Direction = System.Data.ParameterDirection.Input, Value = UserProfileHelper.GetInMemoryUserId() };
            parameters.Add(parameter);
            parameter = new OracleParameter("P_SortColId", OracleDbType.VarChar) { Direction = ParameterDirection.Input, Value = itemBasicInfoModel.SortModel[0].ColId };
            parameters.Add(parameter);
            parameter = new OracleParameter("P_SortOrder", OracleDbType.VarChar) { Direction = ParameterDirection.Input, Value = itemBasicInfoModel.SortModel[0].Sort };
            parameters.Add(parameter);
            var items = await this.GetProcedure2<NewItemSearchModel>(recordObject, packageParameter, serviceDocumentResult, parameters);
            itemBasicInfoModel.TotalRows = (this.Parameters["p_total"].Value != null && this.Parameters["p_total"].Value != DBNull.Value) ? Convert.ToInt64(this.Parameters["p_total"].Value) : 1;
            serviceDoc.DataProfile.DataModel = itemBasicInfoModel;
            return items;
        }

        public async Task<NewItemModel> NewItemGet(string itemId, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("NEW_ITEM_REC");
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["HEAD_SEQ"]] = itemId;
            PackageParams packageParameter = this.GetPackageParams("NEW_ITEM_TAB", "GETNEWITEM");
            OracleTable pDomainMstTab = await this.GetProcedure(recordObject, packageParameter, serviceDocumentResult);
            var lstHeader = Mapper.Map<List<NewItemModel>>(pDomainMstTab);
            if (lstHeader != null)
            {
                OracleObject obj = (OracleObject)pDomainMstTab[0];
                OracleTable domainDetails = (Devart.Data.Oracle.OracleTable)obj["ITEM_DETAIL_TAB"];
                var items = Mapper.Map<List<ItemMasterModel>>(domainDetails);
                for (int i = 0; i < items.Count; i++)
                {
                    decimal invoiceDiscount = 0;
                    if (items[i].InvoiceDiscount != null)
                    {
                        invoiceDiscount = items[i].InvoiceDiscount.Value;
                    }

                    items[i].SuppCaseCost = items[i].SuppUnitCost * items[i].CaseQty;

                    items[i].NetSuppCaseCost = items[i].SuppCaseCost - (items[i].SuppCaseCost * invoiceDiscount);
                    items[i].NetSuppUnitCost = items[i].SuppUnitCost - (items[i].SuppUnitCost * invoiceDiscount);
                    OracleObject objItem = (OracleObject)domainDetails[i];
                    OracleTable rspDetails = (Devart.Data.Oracle.OracleTable)objItem["RSP"];
                    if (rspDetails.Count > 0)
                    {
                        items[i].ItemPrices.AddRange(Mapper.Map<List<NewItemPriceModel>>(rspDetails));
                    }
                }

                lstHeader[0].ItemMasters.AddRange(items);

                var item = items.FirstOrDefault(ids => ids.CompQty == 1);
                lstHeader[0].BrandName = item.BrandName;
                lstHeader[0].BrandId = Convert.ToString(item.BrandId);
                lstHeader[0].StandardUom = item.StandardUom;
                lstHeader[0].SubclassId = item.SubclassId;
                lstHeader[0].DeptId = item.DeptId;
                lstHeader[0].CategoryId = item.CategoryId;
                lstHeader[0].ClassId = item.ClassId;
                lstHeader[0].ItemTier = item.ItemTier;
                lstHeader[0].OriginCountry = item.OriginCountry;
                lstHeader[0].ExpiryFlag = item.ExpiryFlag;
                lstHeader[0].ExpiryInbound = item.ExpiryInbound;
                lstHeader[0].ExpiryOutbound = item.ExpiryOutbound;
                lstHeader[0].Item = item.Item;
                lstHeader[0].Replenish = item.Replenish;
                lstHeader[0].Description = item.DescLong;
                lstHeader[0].DivisionDesc = item.DivisionDesc;
                lstHeader[0].DeptDesc = item.DeptDesc;
                lstHeader[0].CategoryDesc = item.CategoryDesc;
                lstHeader[0].ClassDesc = item.ClassDesc;
                lstHeader[0].SubclassDesc = item.SubclassDesc;
                lstHeader[0].Countries = lstHeader[0].CountryId.Split(',').ToList();

                OracleObject objItemStore = (OracleObject)domainDetails[0];
                OracleTable storeDetails = (Devart.Data.Oracle.OracleTable)objItemStore["STORE_RANGING"];
                var storeRanges = Mapper.Map<List<ItemStoreRangingModel>>(storeDetails);
                lstHeader[0].ItemStoreRangings.AddRange(storeRanges.Count > 0 ? storeRanges : await this.CountryStoreReangingGet(lstHeader[0].CountryId.Split(','), serviceDocumentResult));
                return lstHeader[0];
            }

            return null;
        }

        public async Task<ServiceDocumentResult> NewItemSet(NewItemModel newItemModel)
        {
            this.HeadSeq = 0;
            PackageParams packageParameter = this.GetPackageParams("NEW_ITEM_TAB", "SETNEWITEM");
            OracleObject recordObject = this.SetParamsNewItem(newItemModel);
            var result = await this.SetProcedure(recordObject, packageParameter);
            if (this.ObjResult != null)
            {
                this.HeadSeq = Convert.ToInt32(this.ObjResult["HEAD_SEQ"]);
            }

            return result;
        }

        public OracleObject SetParamsNewItem(NewItemModel newItemModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("NEW_ITEM_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);

            // Detail Object
            OracleType dtlTableType = this.GetObjectType("NEW_ITEM_DETAIL_TAB");
            OracleType dtlRecordType = this.GetObjectType("NEW_ITEM_DETAIL_REC");
            OracleTable pUtlDmnDtlTab = new OracleTable(dtlTableType);

            recordObject["HEAD_SEQ"] = newItemModel.HEAD_SEQ;
            recordObject["SUPPLIER_ID"] = newItemModel.SupplierId;
            recordObject["DIVISION_ID"] = newItemModel.DivisionId;
            recordObject["STATUS"] = newItemModel.Status;
            recordObject["COUNTRY_ID"] = string.Join(",", newItemModel.Countries);
            recordObject["ITEM_TYPE"] = "L";
            recordObject["Operation"] = newItemModel.Operation;

            foreach (var itemMaster in newItemModel.ItemMasters)
            {
                OracleObject dtlRecordObject = this.GetOracleObject(dtlRecordType);
                dtlRecordObject["COMPANY_ID"] = 1;
                dtlRecordObject["COUNTRY_CODE"] = "KWT";
                dtlRecordObject["REGION_CODE"] = "KWT";
                dtlRecordObject["DIVISION_ID"] = newItemModel.DivisionId;
                dtlRecordObject["DEPT_ID"] = newItemModel.DeptId;
                dtlRecordObject["CATEGORY_ID"] = newItemModel.CategoryId;
                dtlRecordObject["FL_ID"] = newItemModel.ClassId;
                dtlRecordObject["SEGMENT_ID"] = newItemModel.SubclassId;
                dtlRecordObject["ITEM"] = itemMaster.Item;
                dtlRecordObject["DESC_LONG"] = itemMaster.DescLong;
                dtlRecordObject["SHORT_DESC"] = itemMaster.ShortDesc;
                dtlRecordObject["SECONDARY_DESC"] = itemMaster.SecondaryDesc;
                dtlRecordObject["ITEM_TIER"] = newItemModel.ItemTier;
                dtlRecordObject["ITEM_TYPE"] = itemMaster.ItemType;
                dtlRecordObject["SELLABLE_IND"] = itemMaster.Sellable;
                dtlRecordObject["ORDERABLE_IND"] = itemMaster.Orderable;
                dtlRecordObject["INVENTORY_IND"] = itemMaster.Inventory;
                dtlRecordObject["REPLENISHMENT_IND"] = newItemModel.Replenish;
                dtlRecordObject["ITEM_BARCODE"] = itemMaster.Barcode;
                dtlRecordObject["BRAND_NAME"] = itemMaster.BrandId;
                dtlRecordObject["EXPIRY_FLAG"] = newItemModel.ExpiryFlag;
                dtlRecordObject["EXPIRY_INBOUND"] = newItemModel.ExpiryInbound;
                dtlRecordObject["EXPIRY_OUTBOUND"] = newItemModel.ExpiryOutbound;
                dtlRecordObject["ITEM_NUMBER_TYPE"] = itemMaster.ItemType;
                dtlRecordObject["TRAN_LEVEL"] = itemMaster.TranLevel;
                dtlRecordObject["SOM"] = itemMaster.Som;
                dtlRecordObject["STANDARD_UOM"] = newItemModel.StandardUom;
                dtlRecordObject["UOP"] = itemMaster.Uop;
                dtlRecordObject["VPN"] = itemMaster.Vpn;
                dtlRecordObject["HTS"] = itemMaster.Hts;
                dtlRecordObject["ORIGIN_COUNTRY"] = newItemModel.OriginCountry;
                dtlRecordObject["INNER_QTY"] = itemMaster.InnerQty;
                dtlRecordObject["CASE_QTY"] = itemMaster.CaseQty;
                dtlRecordObject["SUPP_CASE_COST"] = itemMaster.SuppCaseCost;
                dtlRecordObject["SUPP_UNIT_COST"] = itemMaster.SuppUnitCost;
                dtlRecordObject["INVOICE_DISCOUNT"] = itemMaster.InvoiceDiscount;
                dtlRecordObject["COMP_QTY"] = itemMaster.CompQty;
                dtlRecordObject["OPERATION"] = newItemModel.Operation;
                dtlRecordObject["PACK_IND"] = newItemModel.ItemMasters.Count > 1 ? "Y" : "N";

                ////dtlRecordObject["SYSTEM_DESC"] = brandName + " " + itemName + " - " + compQty + " " + standardUom;

                // RSP Object
                OracleType rspTableType = this.GetObjectType("COUNTRY_RSP_TAB");
                OracleTable pRspTab = new OracleTable(rspTableType);

                foreach (var itemprice in itemMaster.ItemPrices)
                {
                    OracleType rspRecordType = this.GetObjectType("COUNTRY_RSP_REC");
                    var rspRecordObject = this.GetOracleObject(rspRecordType);
                    rspRecordObject["COUNTRY_ID"] = itemprice.CountryCode;
                    rspRecordObject["COUNTRY_DESC"] = itemprice.CountryName;
                    rspRecordObject["CUR_PRICE"] = itemprice.CurrentPrice;
                    rspRecordObject["FORMAT"] = itemprice.Format;
                    rspRecordObject["OPERATION"] = itemprice.Operation;
                    pRspTab.Add(rspRecordObject);
                }

                dtlRecordObject["RSP"] = pRspTab;

                // Loc Object
                OracleType locTableType = this.GetObjectType("STORE_RANGING_TAB");
                OracleTable pLocTab = new OracleTable(locTableType);

                foreach (var itemstore in newItemModel.ItemStoreRangings)
                {
                    OracleType locRecordType = this.GetObjectType("STORE_RANGING_REC");
                    var locRecordObject = this.GetOracleObject(locRecordType);
                    locRecordObject["loc_name"] = itemstore.Loc;
                    locRecordObject["source_method"] = itemstore.SourceMethod;
                    pLocTab.Add(locRecordObject);
                }

                dtlRecordObject["STORE_RANGING"] = pLocTab;

                pUtlDmnDtlTab.Add(dtlRecordObject);
                recordObject["ITEM_DETAIL_TAB"] = pUtlDmnDtlTab;
            }

            return recordObject;
        }

        public virtual OracleObject SetParamsNewItemList(NewItemSearchModel itemBasicInfoModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType(ItemManagementConstants.RecordObjNewItemSearch);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject["HEAD_SEQ"] = itemBasicInfoModel.HEAD_SEQ;
            recordObject["DESCRIPTION"] = itemBasicInfoModel.Description;
            recordObject["SUPPLIER_ID"] = itemBasicInfoModel.SupplierId;
            recordObject["DIVISION_ID"] = itemBasicInfoModel.DivisionId;
            recordObject["GROUP_ID"] = itemBasicInfoModel.DeptId;
            recordObject["DEPT_ID"] = itemBasicInfoModel.CategoryId;
            recordObject["FL_ID"] = itemBasicInfoModel.ClassId;
            recordObject["SEGMENT_ID"] = itemBasicInfoModel.SubclassId;
            recordObject["ITEM"] = itemBasicInfoModel.Item;
            recordObject["BARCODE"] = itemBasicInfoModel.Barcode;
            recordObject["WORKFLOW_STATUS"] = itemBasicInfoModel.Status;
            return recordObject;
        }

        public void RspDetailsGet(OracleTable pRspTab, string formatName, string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                OracleType rspRecordType = this.GetObjectType("RSP_REC");
                OracleObject rspRecordObject = this.GetOracleObject(rspRecordType);
                decimal price = Math.Round(Convert.ToDecimal(value), 3);
                rspRecordObject["CUR_PRICE"] = price;
                rspRecordObject["format"] = formatName;
                pRspTab.Add(rspRecordObject);
            }
        }

        public async Task<List<ItemPriceHistoryModel>> ItemPriceHistoryGet(ItemPriceHistoryModel itemPriceHistory, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams("ITEM_PRICE_HIST_TAB", "GETITEMPRICEHIST");
            OracleObject recordObject = this.SetParamsItemPriceHist(itemPriceHistory);
            return await this.GetProcedure2<ItemPriceHistoryModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<List<ItemStoreRangingModel>> CountryStoreReangingGet(string[] countries, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams("COUNTRY_STORE_RANGING_TAB", "GETCOUNTRYSTORERANGING");
            this.Connection.Open();

            // Loc Object
            OracleType locTableType = this.GetObjectType("COUNTRY_STORE_RANGING_TAB");
            OracleTable pLocTab = new OracleTable(locTableType);

            foreach (string country in countries)
            {
                OracleType recordType = this.GetObjectType("COUNTRY_STORE_RANGING_REC");
                OracleObject recordObject = new OracleObject(recordType);
                recordObject["COUNTRY_ID"] = country;
                pLocTab.Add(recordObject);
            }

            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();

            OracleParameter parameter;
            parameter = new OracleParameter("RESULT", Devart.Data.Oracle.OracleDbType.Boolean)
            {
                Direction = System.Data.ParameterDirection.ReturnValue
            };
            parameters.Add(parameter);
            parameter = new OracleParameter(packageParameter.ObjectTypeTable, Devart.Data.Oracle.OracleDbType.Table)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                ObjectTypeName = packageParameter.ObjectTypeName,
                Value = pLocTab
            };
            parameters.Add(parameter);

            return await this.GetProcedure2<ItemStoreRangingModel>(null, packageParameter, serviceDocumentResult, parameters);
        }

        public virtual OracleObject SetParamsItemPriceHist(ItemPriceHistoryModel itemBasicInfoModel)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType("ITEM_PRICE_HIST_REC");
            OracleObject recordObject = new OracleObject(recordType);
            recordObject["FROM_DATE"] = itemBasicInfoModel.FromDate;
            recordObject["TO_DATE"] = itemBasicInfoModel.ToDate;
            recordObject["LOC_TYPE"] = itemBasicInfoModel.LocType;
            recordObject["LOC"] = itemBasicInfoModel.Loc;
            if (this.TranType != null)
            {
                recordObject["TRAN_TYPE"] = this.TranType;
            }

            recordObject["ITEM"] = itemBasicInfoModel.Item;
            recordObject["ITEM_DESC"] = itemBasicInfoModel.ItemDesc;
            return recordObject;
        }
    }
}