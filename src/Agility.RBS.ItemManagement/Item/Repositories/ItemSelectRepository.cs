﻿// <copyright file="ItemSelectRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.Item.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Core.Models;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.ItemManagement.Common;
    using Agility.RBS.ItemManagement.Item.Models;
    using Agility.RBS.ItemManagement.ItemStatus;
    using Agility.RBS.ItemManagement.ItemStatus.Models;
    using Agility.RBS.ItemManagement.ItemStatus.Repository;
    using Agility.RBS.MDM.Repositories.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class ItemSelectRepository : BaseOraclePackage
    {
        public ItemSelectRepository()
        {
            this.PackageName = ItemManagementConstants.GetItemSelectPackage;
        }

        public async Task<List<ItemSelectModel>> SelectItems(ItemSelectModel inputModel, ServiceDocumentResult serviceDocumentResult)
        {
            string procName = string.Empty;
            procName = ItemManagementConstants.GetItemList;
            PackageParams packageParameter = this.GetPackageParams(ItemManagementConstants.FindItemListTab, procName);
            OracleObject recordObject = this.SetParamsSelectItemsList(inputModel);
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            parameters.Add(this.ParameterBuilder(UserProfileHelper.GetUserParameter()));
            return await this.GetProcedure2<ItemSelectModel>(recordObject, packageParameter, serviceDocumentResult, parameters);
        }

        public async Task<AjaxModel<List<OrderItemSelectModel>>> SelectOrderItem(OrderItemSelectModel orderItemSelectModel)
        {
            PackageParams packageParameter = this.GetPackageParams(ItemManagementConstants.OrderItemSearchTab, ItemManagementConstants.GetOrderItemList);
            OracleObject recordObject = this.SetParamsSelectOrderItemsList(orderItemSelectModel);
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            parameters.Add(this.ParameterBuilder(UserProfileHelper.GetUserParameter()));
            return await this.GetProcedureAjaxModel<OrderItemSelectModel>(recordObject, packageParameter, parameters);
        }

        public async Task<List<RtvItemSelectModel>> SelectRtvItem(RtvItemSelectModel orderItemSelectModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(ItemManagementConstants.RtvItemSearchTab, ItemManagementConstants.GetRtvItemList);
            OracleObject recordObject = this.SetParamsSelectRtvItemsList(orderItemSelectModel);
            return await this.GetProcedure2<RtvItemSelectModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<List<CostChgItemSelectModel>> SelectCostChgItems(ItemSelectModel inputModel, ServiceDocumentResult serviceDocumentResult)
        {
            string procName = ItemManagementConstants.GetCostChgItemList;
            PackageParams packageParameter = this.GetPackageParams(ItemManagementConstants.CostChangeFindItemListTab, procName);
            OracleObject recordObject = this.SetParamsSelectCostChgItemsList(inputModel);
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            parameters.Add(this.ParameterBuilder(UserProfileHelper.GetUserParameter()));
            return await this.GetProcedure2<CostChgItemSelectModel>(recordObject, packageParameter, serviceDocumentResult, parameters);
        }

        public async Task<List<PriceChgItemSelectModel>> SelectPriceChgItems(ItemSelectModel inputModel, ServiceDocumentResult serviceDocumentResult)
        {
            string procName = ItemManagementConstants.GetPriceChgItemList;
            PackageParams packageParameter = this.GetPackageParams(ItemManagementConstants.PriceChangeFindItemListTab, procName);
            OracleObject recordObject = this.SetParamsSelectPriceChgItemsList(inputModel);
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            parameters.Add(this.ParameterBuilder(UserProfileHelper.GetUserParameter()));
            return await this.GetProcedure2<PriceChgItemSelectModel>(recordObject, packageParameter, serviceDocumentResult, parameters);
        }

        public async Task<List<CostChangeItemModel>> SelectCostChgItems(int itemNo, int supplier, ServiceDocumentResult serviceDocumentResult)
        {
            string procName = ItemManagementConstants.GetCostChgItemList2;
            PackageParams packageParameter = this.GetPackageParams(ItemManagementConstants.CostChangeFindItemListTab2, procName);
            OracleObject recordObject = this.SetParamsSelectCostChgItem(itemNo, supplier);
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            parameters.Add(this.ParameterBuilder(UserProfileHelper.GetUserParameter()));
            OracleTable pItemTab = await this.GetProcedure(recordObject, packageParameter, serviceDocumentResult, parameters);

            var lstHeader = Mapper.Map<List<CostChangeItemModel>>(pItemTab);
            if (lstHeader != null)
            {
                for (int i = 0; i < lstHeader.Count; i++)
                {
                    OracleObject obj = (OracleObject)pItemTab[i];
                    OracleTable domainDetails = (Devart.Data.Oracle.OracleTable)obj["RSP"];
                    if (domainDetails.Count > 0)
                    {
                        lstHeader[i].ItemPrices.AddRange(Mapper.Map<List<CostChangeItemPriceModel>>(domainDetails));
                    }
                }
            }

            return lstHeader;
        }

        public async Task<List<ItemCloseModel>> ClosedItemList(string itemNo, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams("item_close_tab", "getitemclose");
            ItemCloseModel model = new ItemCloseModel { ItemBarcode = itemNo };
            OracleObject recordObject = this.GetCloseItem(model);
            OracleTable pItemTab = await this.GetProcedure(recordObject, packageParameter, serviceDocumentResult);
            var lstHeader = Mapper.Map<List<ItemCloseModel>>(pItemTab);
            if (lstHeader != null)
            {
                for (int i = 0; i < lstHeader.Count; i++)
                {
                    OracleObject obj = (OracleObject)pItemTab[i];
                    OracleTable rspDetails = (Devart.Data.Oracle.OracleTable)obj["store_ranging"];
                    if (rspDetails.Count > 0)
                    {
                        lstHeader[i].ZoneGroups.AddRange(Mapper.Map<List<ZoneGroups>>(rspDetails));
                    }
                }
            }

            return lstHeader;
        }

        public async Task<List<ReactiveItemSelectModel>> ReactiveItemList(string itemNo, int supplier, ServiceDocumentResult serviceDocumentResult)
        {
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            OracleParameter parameter = new OracleParameter("P_SUPPLIER", OracleDbType.Number)
            {
                Direction = System.Data.ParameterDirection.Input,
                Value = supplier
            };
            parameters.Add(parameter);

            PackageParams packageParameter = this.GetPackageParams("FOREIGN_ITEM_DETAIL_TAB", "GETREACTIVATEITEMSEARCH");
            OracleObject recordObject = this.GetReactiveItem(itemNo);
            OracleTable pItemTab = await this.GetProcedure(recordObject, packageParameter, serviceDocumentResult, parameters);
            var lstHeader = Mapper.Map<List<ReactiveItemSelectModel>>(pItemTab);
            if (lstHeader != null)
            {
                for (int i = 0; i < lstHeader.Count; i++)
                {
                    OracleObject obj = (OracleObject)pItemTab[i];
                    OracleTable rspDetails = (Devart.Data.Oracle.OracleTable)obj["RSP"];
                    if (rspDetails.Count > 0)
                    {
                        lstHeader[i].ItemPrices.AddRange(Mapper.Map<List<CostChangeItemPriceModel>>(rspDetails));
                    }

                    OracleObject objRsp = (OracleObject)pItemTab[i];
                    OracleTable marketPriceTab = (Devart.Data.Oracle.OracleTable)objRsp["MARKET_PRICE"];
                    if (marketPriceTab.Count > 0)
                    {
                        lstHeader[i].ListOfCostChangeMarketPriceExceclModels.AddRange(Mapper.Map<List<CostChangeMarketPriceExceclModel>>(marketPriceTab));
                    }

                    OracleObject objStoreRange = (OracleObject)pItemTab[i];
                    OracleTable storeRangeDetails = (Devart.Data.Oracle.OracleTable)objStoreRange["store_ranging"];
                    if (storeRangeDetails.Count > 0)
                    {
                        lstHeader[i].ZoneGroups.AddRange(Mapper.Map<List<ZoneGroups>>(storeRangeDetails));
                    }
                }
            }

            return lstHeader;
        }

        public OracleObject GetCloseItem(ItemCloseModel dataMode)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType("item_close_rec", this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["id"]] = dataMode.Id;
            recordObject[recordType.Attributes["item_barcode"]] = dataMode.ItemBarcode;
            recordObject[recordType.Attributes["OPERATION"]] = "U";
            return recordObject;
        }

        public OracleObject GetReactiveItem(string item)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType("foreign_item_detail_rec", this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["ITEM"]] = item;
            return recordObject;
        }

        public async Task<AjaxModel<List<CostChangeItemGroupsModel>>> SelectCostChgItemsParentChildItems(string itemNo, long supplier)
        {
            string procName = ItemManagementConstants.GetCostChgItemGroupList3;
            PackageParams packageParameter = this.GetPackageParams(ItemManagementConstants.CostChangeFindItemgGroupListTab3, procName);
            OracleObject recordObject = this.SetParamsSelectCostChgItemGroup(itemNo, supplier);
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            OracleTable pItemTab = await this.GetProcedureAjaxModel(recordObject, packageParameter);

            if (pItemTab != null)
            {
                OracleObject obj1 = (OracleObject)pItemTab[0];
                if (obj1 != null && !string.IsNullOrEmpty(Convert.ToString(obj1["P_Error"])))
                {
                    return new AjaxModel<List<CostChangeItemGroupsModel>> { Result = AjaxResult.ValidationException, Message = Convert.ToString(obj1["Program_Message"]), Model = null };
                }

                var lstHeader = Mapper.Map<List<CostChangeItemGroupsModel>>(pItemTab);
                if (lstHeader != null)
                {
                    for (int i = 0; i < lstHeader.Count; i++)
                    {
                        OracleObject obj = (OracleObject)pItemTab[i];
                        OracleTable rspDetails = (Devart.Data.Oracle.OracleTable)obj["RSP"];
                        if (rspDetails.Count > 0)
                        {
                            lstHeader[i].ItemPrices.AddRange(Mapper.Map<List<CostChangeItemPriceModel>>(rspDetails));
                        }

                        OracleObject objRsp = (OracleObject)pItemTab[i];
                        OracleTable marketPriceTab = (Devart.Data.Oracle.OracleTable)objRsp["MARKET_PRICE"];
                        if (marketPriceTab.Count > 0)
                        {
                            lstHeader[i].ListOfCostChangeMarketPriceExceclModels.AddRange(Mapper.Map<List<CostChangeMarketPriceExceclModel>>(marketPriceTab));
                        }
                    }
                }

                return new AjaxModel<List<CostChangeItemGroupsModel>> { Result = AjaxResult.Success, Model = lstHeader };
            }

            return new AjaxModel<List<CostChangeItemGroupsModel>> { Result = AjaxResult.Exception, Model = null, Message = "No Item available" };
        }

        public async Task<List<ItemDetailsPromoModel>> GetItemsForPromotion(string itemNo, string screenId, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams("promotion_form_tab", "getpromotionitemlist");
            OracleObject recordObject = this.SetParamsSelectPromoItemGroup(itemNo);
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            this.Connection.Open();
            OracleParameter parameter = new OracleParameter("p_Promo_id", OracleDbType.Number)
            {
                Direction = System.Data.ParameterDirection.Input,
                Value = screenId
            };
            parameters.Add(parameter);
            OracleTable pItemTab = await this.GetProcedure(recordObject, packageParameter, serviceDocumentResult, parameters);

            var lstHeader = Mapper.Map<List<ItemDetailsPromoModel>>(pItemTab);
            return lstHeader;
        }

        public async Task<ItemInventoryModel> GetItemDataForInventory(string item, string locType, int locId, ServiceDocumentResult serviceDocumentResult)
        {
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();

            string objType = ItemManagementConstants.ObjTypeItemInventory;
            string tableType = ItemManagementConstants.ItemInventoryTbl;
            string procedureName = ItemManagementConstants.GetProcItemInventory;

            this.Connection.Open();
            OracleParameter parameter = new OracleParameter("P_LOCATION", OracleDbType.Number)
            {
                Direction = System.Data.ParameterDirection.Input,
                Value = locId
            };
            parameters.Add(parameter);
            parameter = new OracleParameter("P_ITEM", OracleDbType.VarChar)
            {
                Direction = System.Data.ParameterDirection.Input,
                Value = item
            };
            parameters.Add(parameter);
            parameter = new OracleParameter("P_LOC_TYPE", OracleDbType.VarChar)
            {
                Direction = System.Data.ParameterDirection.Input,
                Value = locType
            };
            parameters.Add(parameter);
            parameter = new OracleParameter(tableType, Devart.Data.Oracle.OracleDbType.Table)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                ObjectTypeName = objType,
            };
            parameters.Add(parameter);
            parameter = new OracleParameter("RESULT", Devart.Data.Oracle.OracleDbType.Boolean)
            {
                Direction = System.Data.ParameterDirection.ReturnValue
            };
            parameters.Add(parameter);
            PackageParams packageParameter = this.GetPackageParams(objType, procedureName);
            var res = await this.GetProcedure2<ItemInventoryModel>(null, packageParameter, serviceDocumentResult, parameters);
            return res != null ? res[0] : null;
        }

        public virtual OracleObject SetParamsSelectOrderItemsList(OrderItemSelectModel inputModel)
        {
            this.Connection.Open();
            OracleObject recordObject = this.GetOracleObject(this.GetObjectType(ItemManagementConstants.OrderItemSearchRec));
            recordObject["ITEM_BARCODE"] = inputModel.ItemBarcode;
            recordObject["DEPT_ID"] = inputModel.DeptId > 0 ? inputModel.DeptId : null;
            recordObject["SUPPLIER"] = inputModel.Supplier;
            recordObject["LOCATION_ID"] = inputModel.LocationId;
            return recordObject;
        }

        public virtual OracleObject SetParamsSelectRtvItemsList(RtvItemSelectModel inputModel)
        {
            this.Connection.Open();
            OracleObject recordObject = this.GetOracleObject(this.GetObjectType(ItemManagementConstants.RtvItemSearchRec));
            recordObject["ITEM"] = inputModel.Item;
            recordObject["LOCATION_ID"] = inputModel.LocationId;
            recordObject["SUPPLIER"] = inputModel.Supplier;
            return recordObject;
        }

        public virtual OracleObject SetParamsSelectCostChgItemsList(ItemSelectModel inputModel)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(ItemManagementConstants.RecordObjCostChangeFindItemList, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["ITEM_BARCODE"]] = inputModel.ItemBarcode;
            recordObject[recordType.Attributes["ITEM_DESC"]] = inputModel.ItemDesc;
            recordObject[recordType.Attributes["DEPT_ID"]] = inputModel.DeptId > 0 ? inputModel.DeptId : null;
            recordObject[recordType.Attributes["CATEGORY_ID"]] = inputModel.CategoryId;
            recordObject[recordType.Attributes["ORG_LVL"]] = inputModel.OrgLvl;
            recordObject[recordType.Attributes["ORG_LVL_VAL"]] = inputModel.OrgLvlVal;
            recordObject[recordType.Attributes["SUPPLIER_ID"]] = inputModel.SupplierId > 0 ? inputModel.SupplierId : null;
            recordObject[recordType.Attributes["ITEM_LIST"]] = inputModel.ItemList;
            recordObject[recordType.Attributes["BRAND_ID"]] = inputModel.BrandId;
            recordObject[recordType.Attributes["DIVISION_ID"]] = inputModel.DivisionId;

            return recordObject;
        }

        public virtual OracleObject SetParamsSelectPriceChgItemsList(ItemSelectModel inputModel)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(ItemManagementConstants.RecordObjPriceChangeFindItemList, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["ITEM_BARCODE"]] = inputModel.ItemBarcode;
            recordObject[recordType.Attributes["ITEM_DESC"]] = inputModel.ItemDesc;
            recordObject[recordType.Attributes["DEPT_ID"]] = inputModel.DeptId > 0 ? inputModel.DeptId : null;
            recordObject[recordType.Attributes["CATEGORY_ID"]] = inputModel.CategoryId;
            recordObject[recordType.Attributes["ORG_LVL"]] = inputModel.OrgLvl;
            recordObject[recordType.Attributes["ORG_LVL_VAL"]] = inputModel.OrgLvlVal;
            recordObject[recordType.Attributes["ITEM_LIST"]] = inputModel.ItemList;
            recordObject[recordType.Attributes["BRAND_ID"]] = inputModel.BrandId;
            recordObject[recordType.Attributes["DIVISION_ID"]] = inputModel.DivisionId;

            return recordObject;
        }

        private OracleObject SetParamsSelectItemsList(ItemSelectModel inputModel)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(ItemManagementConstants.RecordObjFindItemList, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["ITEM_BARCODE"]] = inputModel.ItemBarcode;
            recordObject[recordType.Attributes["ITEM_DESC"]] = inputModel.ItemDesc;
            recordObject[recordType.Attributes["DEPT_ID"]] = inputModel.DeptId > 0 ? inputModel.DeptId : null;
            recordObject[recordType.Attributes["CATEGORY_ID"]] = inputModel.CategoryId;
            recordObject[recordType.Attributes["ORG_LVL"]] = inputModel.OrgLvl;
            recordObject[recordType.Attributes["ORG_LVL_VAL"]] = inputModel.OrgLvlVal;
            recordObject[recordType.Attributes["supplier_id"]] = inputModel.SupplierId > 0 ? inputModel.SupplierId : null;
            recordObject[recordType.Attributes["item_list"]] = inputModel.ItemList;
            recordObject[recordType.Attributes["brand_id"]] = inputModel.BrandId;
            recordObject[recordType.Attributes["division_id"]] = inputModel.DivisionId;

            return recordObject;
        }

        private OracleObject SetParamsSelectCostChgItem(int itemNo, int supplier)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(ItemManagementConstants.RecordObjCostChangeFindItemList2, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["ITEM_BARCODE"]] = itemNo;
            recordObject[recordType.Attributes["SUPPLIER_ID"]] = supplier;
            return recordObject;
        }

        private OracleObject SetParamsSelectCostChgItemGroup(string itemNo, long supplier)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(ItemManagementConstants.RecordObjCostChangeFindItemList3, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject["ITEM_BARCODE"] = itemNo;
            recordObject["SUPPLIER_ID"] = supplier;
            return recordObject;
        }

        private OracleObject SetParamsSelectPromoItemGroup(string itemNo)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType("promotion_form_rec", this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["item"]] = itemNo.Trim();
            return recordObject;
        }
    }
}
