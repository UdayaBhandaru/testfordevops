﻿// <copyright file="ItemRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.Item.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.Framework.Web.Security.Middleware;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.ItemManagement.Common;
    using Agility.RBS.ItemManagement.Item.Models;
    using Agility.RBS.MDM.Repositories.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class ItemRepository : BaseOraclePackage
    {
        public ItemRepository()
        {
            this.PackageName = ItemManagementConstants.GetItemPackage;
        }

        public int? HeadSeq { get; private set; }

        public async Task<List<ItemBasicInfoModel>> ItemBasics(string itemId, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(ItemManagementConstants.RecordObjItemBasic, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["ITEM"]] = itemId;
            PackageParams packageParameter = this.GetPackageParams(ItemManagementConstants.ObjTypeItemBasic, ItemManagementConstants.GetProcItemBasic);
            return await this.GetProcedure2<ItemBasicInfoModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ItemBasicInfoModel> ItemBasicInfoGet(string itemId, ServiceDocumentResult serviceDocumentResult)
        {
            ItemBasicInfoModel itemBasicInfoModel = null;
            List<ItemBasicInfoModel> items = await this.ItemBasics(itemId, serviceDocumentResult);
            itemBasicInfoModel = items.Find(id => id.ItemParent is null);

            foreach (var item in items)
            {
                if (itemBasicInfoModel.Item != item.Item)
                {
                    ItemAdditionalUomModel itemAdditionalUomModel = new ItemAdditionalUomModel
                    {
                        Item = item.Item,
                        ItemParent = item.ItemParent,
                        OnlineDesc = item.OnlineDesc,
                        ShortDesc = item.ShortDesc,
                        ShortDescArabic = item.ShortDescArabic,
                        StandardUomValue = Convert.ToString(item.CompQty),
                        StandardUom = item.StandardUom,
                        SystemDesc = item.SystemDesc
                    };
                    itemBasicInfoModel.ItemAdditionalUoms.Add(itemAdditionalUomModel);
                }
            }

            return itemBasicInfoModel;
        }

        public async Task<ServiceDocumentResult> SetItemSuppCountryDimension(ItemSupplierCountryDimensionModel itemSupplierCountryDimensionModel, string screenId)
        {
            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();

            PackageParams packageParameter = this.GetPackageParams(ItemManagementConstants.ObjTypeItemSupplierCountryDimension, ItemManagementConstants.SetProcItemSupplierCountryDimension);
            OracleParameter parameter;
            parameter = new OracleParameter("RESULT", Devart.Data.Oracle.OracleDbType.Boolean)
            {
                Direction = System.Data.ParameterDirection.ReturnValue
            };
            parameters.Add(parameter);
            parameter = new OracleParameter(packageParameter.ObjectTypeTable, Devart.Data.Oracle.OracleDbType.Table)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                ObjectTypeName = packageParameter.ObjectTypeName,
                Value = this.SetParamsItemSupplierCountryDimension(itemSupplierCountryDimensionModel, screenId)
            };
            parameters.Add(parameter);
            return await this.SetProcedure(null, packageParameter, parameters);
        }

        public virtual OracleTable SetParamsItemSupplierCountryDimension(ItemSupplierCountryDimensionModel itemSupplierCountryDimension, string screen)
        {
            this.Connection.Open();
            OracleType itemComTableType = OracleType.GetObjectType(ItemManagementConstants.ObjTypeItemSupplierCountryDimension, this.Connection);
            OracleType recordType = OracleType.GetObjectType(ItemManagementConstants.RecordObjItemSupplierCountryDimension, this.Connection);
            OracleTable pItemCompanyTab = new OracleTable(itemComTableType);
            if (screen.Equals("ItemSuper"))
            {
                pItemCompanyTab.Add(this.SetParamsRecObjItemSuppCntDmns(recordType, itemSupplierCountryDimension));
            }
            else
            {
                foreach (ItemSupplierCountryDimensionModel itemSupplierCountryDimensionModel in itemSupplierCountryDimension.ItemSupplierCountryDimensions)
                {
                    pItemCompanyTab.Add(this.SetParamsRecObjItemSuppCntDmns(recordType, itemSupplierCountryDimensionModel));
                }
            }

            return pItemCompanyTab;
        }

        public virtual OracleObject SetParamsRecObjItemSuppCntDmns(OracleType recordType, ItemSupplierCountryDimensionModel itemSupplierCountryDimensionModel)
        {
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["ITEM"]] = itemSupplierCountryDimensionModel.Item;
            recordObject[recordType.Attributes["COMPANY_ID"]] = itemSupplierCountryDimensionModel.CompanyId;
            recordObject[recordType.Attributes["COUNTRY_CODE"]] = itemSupplierCountryDimensionModel.CountryCode;
            recordObject[recordType.Attributes["IMPORT_COUNTRY"]] = itemSupplierCountryDimensionModel.ImportCountry;
            recordObject[recordType.Attributes["SUPPLIER"]] = itemSupplierCountryDimensionModel.Supplier;
            recordObject[recordType.Attributes["DIM_OBJECT"]] = itemSupplierCountryDimensionModel.DimObject;
            recordObject[recordType.Attributes["PRESENTATION_METHOD"]] = itemSupplierCountryDimensionModel.PresentationMethod;
            recordObject[recordType.Attributes["LENGTH"]] = itemSupplierCountryDimensionModel.Length;
            recordObject[recordType.Attributes["WIDTH"]] = itemSupplierCountryDimensionModel.Width;
            recordObject[recordType.Attributes["HEIGHT"]] = itemSupplierCountryDimensionModel.Height;
            recordObject[recordType.Attributes["LWH_UOM"]] = itemSupplierCountryDimensionModel.LwhUom;
            recordObject[recordType.Attributes["WEIGHT"]] = itemSupplierCountryDimensionModel.Weight;
            recordObject[recordType.Attributes["NET_WEIGHT"]] = itemSupplierCountryDimensionModel.NetWeight;
            recordObject[recordType.Attributes["WEIGHT_UOM"]] = itemSupplierCountryDimensionModel.WeightUom;
            recordObject[recordType.Attributes["LIQUID_VOLUME"]] = itemSupplierCountryDimensionModel.LiquidVolume;
            recordObject[recordType.Attributes["LIQUID_VOLUME_UOM"]] = itemSupplierCountryDimensionModel.LiquidVolumeUom;
            recordObject[recordType.Attributes["STAT_CUBE"]] = itemSupplierCountryDimensionModel.StatCube;
            recordObject[recordType.Attributes["TARE_WEIGHT"]] = itemSupplierCountryDimensionModel.TareWeight;
            recordObject[recordType.Attributes["TARE_TYPE"]] = itemSupplierCountryDimensionModel.TareType;
            recordObject[recordType.Attributes["STATUS"]] = itemSupplierCountryDimensionModel.Status;
            recordObject[recordType.Attributes["LAST_UPDATED_BY"]] = FxContext.Context.Name;
            recordObject[recordType.Attributes["Operation"]] = itemSupplierCountryDimensionModel.Operation;
            if (itemSupplierCountryDimensionModel.Operation == "I")
            {
                recordObject[recordType.Attributes["CREATED_BY"]] = FxContext.Context.Name;
            }

            return recordObject;
        }
    }
}