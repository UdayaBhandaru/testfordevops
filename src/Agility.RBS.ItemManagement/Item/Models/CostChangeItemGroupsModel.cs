﻿// <copyright file="CostChangeItemGroupsModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.Item.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.Framework.Core.Profile.Entities;

    public class CostChangeItemGroupsModel
    {
        public CostChangeItemGroupsModel()
        {
            this.ItemPrices = new List<CostChangeItemPriceModel>();
            this.ListOfCostChangeMarketPriceExceclModels = new List<CostChangeMarketPriceExceclModel>();
        }

        public int? SupplierId { get; set; }

        public string SuppName { get; set; }

        public decimal? Rebates { get; set; }

        public string ItemBarcode { get; set; }

        public string ItemDesc { get; set; }

        public string UnitOfMeasure { get; set; }

        public string UomDesc { get; set; }

        public string UomUnit { get; set; }

        public string ParentItem { get; set; }

        public string ParentItemDesc { get; set; }

        public int? DeptId { get; set; }

        public string DeptDesc { get; set; }

        public string ItemTier { get; set; }

        public decimal? CaseCost { get; set; }

        public string CostCurCode { get; set; }

        public decimal? UnitCost { get; set; }

        public decimal AnnualDiscount { get; set; }

        public decimal? NetUnitCost { get; set; }

        public decimal? Variance { get; set; }

        public decimal? AverageCategoryMargin { get; set; }

        public decimal? MarketPrice { get; set; }

        public List<CostChangeItemPriceModel> ItemPrices { get; private set; }

        public List<CostChangeMarketPriceExceclModel> ListOfCostChangeMarketPriceExceclModels { get; private set; }
    }
}
