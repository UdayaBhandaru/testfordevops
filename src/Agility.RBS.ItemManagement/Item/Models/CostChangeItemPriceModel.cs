﻿// <copyright file="CostChangeItemPriceModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.Item.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.Framework.Core.Profile.Entities;

    public class CostChangeItemPriceModel
    {
        public decimal? CurrentPrice { get; set; }

        public string PriceCurrencyCode { get; set; }

        public decimal? Margin { get; set; }

        public decimal? ExchangeRate { get; set; }

        public string Format { get; set; }

        public string FormatName { get; set; }
    }
}
