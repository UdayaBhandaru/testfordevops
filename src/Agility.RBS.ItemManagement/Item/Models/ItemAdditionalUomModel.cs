﻿// <copyright file="ItemAdditionalUomModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.Item.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using Agility.Framework.Core.Profile.Entities;

    public class ItemAdditionalUomModel
    {
        [NotMapped]
        public string Item { get; set; }

        [NotMapped]
        public string ItemParent { get; set; }

        public string StandardUom { get; set; }

        [NotMapped]
        public string StandardUomValue { get; set; }

        [NotMapped]
        public string ShortDesc { get; set; }

        [NotMapped]
        public string OnlineDesc { get; set; }

        [NotMapped]
        public string ShortDescArabic { get; set; }

        [NotMapped]
        public string SystemDesc { get; set; }

        public decimal? RegularRetail { get; set; }

        public string Operation { get; set; }

        public decimal? UnitCost { get; set; }

        public decimal? CaseCost { get; set; }

        public decimal? InnerSize { get; set; }

        public decimal? CaseSize { get; set; }
    }
}