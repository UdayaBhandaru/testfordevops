﻿// <copyright file="ItemReactivateModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.Item.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Core.Models;
    using Agility.RBS.Inbox.Common;
    using Agility.RBS.ItemManagement.ItemStatus.Models;

    public class ItemReactivateModel : ProfileEntity
    {
        public ItemReactivateModel()
        {
            this.ZoneGroups = new List<ZoneGroups>();
        }

        public string DescLong { get; set; }

        public int? DivisionId { get; set; }

        public string DivisionDesc { get; set; }

        public string ItemBarcode { get; set; }

        public string Item { get; set; }

        public string ItemTier { get; set; }

        public int? Sales52Week { get; set; }

        public int? Gp52Week { get; set; }

        public int? DcSoh { get; set; }

        public int? StoresSoh { get; set; }

        public string Operation { get; set; }

         public int? Supplier { get; set; }

        public int? HeaderId { get; set; }

        public string SuppName { get; set; }

        public string Origin { get; set; }

        public string Incoterms { get; set; }

        public string Currency { get; set; }

        public int? TotalRebates { get; set; }

        [NotMapped]
        public string CurrencyCode { get; set; }

        [NotMapped]
        public string IncotermsCode { get; set; }

        [NotMapped]
        public string IncotermsDesc { get; set; }

        [NotMapped]
        public string SupplierType { get; set; }

        [NotMapped]
        [Column("CREATED_BY")]
        public new string CreatedBy { get; set; }

        [NotMapped]
        [Column("LAST_UPDATED_BY")]
        public new string ModifiedBy { get; set; }

        [NotMapped]
        [Column("CREATED_DATE")]
        public new DateTimeOffset? CreatedDate { get; set; }

        [NotMapped]
        [Column("LAST_UPDATED_DATE")]
        public new DateTimeOffset? ModifiedDate { get; set; }

        [NotMapped]
        public string Error { get; set; }

        [NotMapped]
        public string TableName { get; set; }

        [NotMapped]
        public string Status { get; set; }

        [NotMapped]
        public List<ZoneGroups> ZoneGroups { get; private set; }
    }
}
