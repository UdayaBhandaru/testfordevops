﻿// <copyright file="ReactiveItemSelectModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.Item.Models
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.ComponentModel.DataAnnotations.Schema;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Core.Models;
    using Agility.RBS.Inbox.Common;
    using Agility.RBS.ItemManagement.ItemStatus.Models;

    public class ReactiveItemSelectModel : ProfileEntity
    {
        public ReactiveItemSelectModel()
        {
            this.ItemPrices = new List<CostChangeItemPriceModel>();
            this.ListOfCostChangeMarketPriceExceclModels = new List<CostChangeMarketPriceExceclModel>();
            this.ZoneGroups = new List<ZoneGroups>();
        }

        public string Item { get; set; }

        public string ItemType { get; set; }

        public string ItemTypeDesc { get; set; }

        public string ItemGrandparent { get; set; }

        public string PackInd { get; set; }

        public int? DivisionId { get; set; }

        public string DivisionDesc { get; set; }

        public int? DeptId { get; set; }

        public string DeptDesc { get; set; }

        public int? CategoryId { get; set; }

        public string CategoryDesc { get; set; }

        public int? SubClassId { get; set; }

        public string SubclassDesc { get; set; }

        public string DescLong { get; set; }

        public int? BrandId { get; set; }

        public string SystemDesc { get; set; }

        public string Status { get; set; }

        public string StandardUom { get; set; }

        public string Sellable { get; set; }

        public string Orderable { get; set; }

        public string Inventory { get; set; }

        public string Vpn { get; set; }

        public string Barcode { get; set; }

        public string SubComments { get; set; }

        public string ItemStatusDesc { get; set; }

        public string ItemTier { get; set; }

        public string ShortDescArabic { get; set; }

        public string ItemSpecification { get; set; }

        public string ItemSpecificationValue { get; set; }

        public string ItemNature { get; set; }

        public string ItemNatureValue { get; set; }

        public int? CompanyId { get; set; }

        public string CountryCode { get; set; }

        public string RegionCode { get; set; }

        public string FormatCode { get; set; }

        public int? LocationId { get; set; }

        public string ItemParent { get; set; }

        public string SimplePackInd { get; set; }

        public string ItemLevel { get; set; }

        public string TranLevel { get; set; }

        public string DescUp { get; set; }

        public string SecondaryDesc { get; set; }

        public string ShortDesc { get; set; }

        public string OnlineDesc { get; set; }

        public string SuppDesc { get; set; }

        public string PackingDesc { get; set; }

        public string UnitOfMeasure { get; set; }

        public string UomUnit { get; set; }

        public int? CompQty { get; set; }

        public string ItemBarcode { get; set; }

        public string BrandName { get; set; }

        public string PackType { get; set; }

        public string Comments { get; set; }

        public string PerishableInd { get; set; }

        public string ExpiryFlag { get; set; }

       public decimal? SuppCaseCost { get; set; }

        public decimal? SuppUnitCost { get; set; }

        public decimal? LandedCaseCost { get; set; }

        public decimal? LandedUnitCost { get; set; }

        public List<CostChangeItemPriceModel> ItemPrices { get; private set; }

        public List<CostChangeMarketPriceExceclModel> ListOfCostChangeMarketPriceExceclModels { get; private set; }

        public List<ZoneGroups> ZoneGroups { get; private set; }

        public string ItemException { get; set; }

        public decimal? InvoiceDiscount { get; set; }
    }
}