﻿// <copyright file="ItemDetailsPromoModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.Item.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.Framework.Core.Profile.Entities;

    public class ItemDetailsPromoModel
    {
        public ItemDetailsPromoModel()
        {
            this.ItemsList = new List<ItemDetailsPromoModel>();
        }

        public string DeptName { get; set; }

        public string Item { get; set; }

        public string ItemDesc { get; set; }

        public string SystemDesc { get; set; }

        public string EnglishDesc { get; set; }

        public string Features { get; set; }

        public string ArabicDesc { get; set; }

        public string Installa { get; set; }

        public int? InstallaQty { get; set; }

        public int? LimitedQty { get; set; }

        public string ReturnableInd { get; set; }

        public string SourceMethod { get; set; }

        public DateTime? SourceMethodChgDt { get; set; }

        public decimal? CostRegular { get; set; }

        public decimal? CostAd { get; set; }

        public decimal? RspRegular { get; set; }

        public decimal? RspAd { get; set; }

        public decimal? Supplier { get; set; }

        public string SuppName { get; set; }

        public int? CompQty { get; set; }

        public string Operation { get; set; }

        public List<ItemDetailsPromoModel> ItemsList { get; private set; }
    }
}
