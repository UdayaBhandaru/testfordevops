﻿// <copyright file="ItemInventoryModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.Item.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class ItemInventoryModel
    {
        public string Item { get; set; }

        public string ItemDesc { get; set; }

        public decimal? StockOnHand { get; set; }

        public string DivisionDesc { get; set; }

        public string DeptDesc { get; set; }

        public string CategoryDesc { get; set; }

        public string ClassDesc { get; set; }

        public string SubClassDesc { get; set; }
    }
}
