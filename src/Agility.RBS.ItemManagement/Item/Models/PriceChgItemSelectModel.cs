﻿// <copyright file="PriceChgItemSelectModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.Item.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.Framework.Core.Profile.Entities;

    public class PriceChgItemSelectModel : ProfileEntity
    {
        public string OrgLvl { get; set; }

        public int? OrgLvlVal { get; set; }

        public string OrgLvlDesc { get; set; }

        public string OrgLvlValDesc { get; set; }

        public string ItemBarcode { get; set; }

        public string ItemDesc { get; set; }

        public int? DeptId { get; set; }

        public string DeptDesc { get; set; }

        public int? CategoryId { get; set; }

        public string CategoryDesc { get; set; }

        public string CostCurrencyCode { get; set; }

        public string PriceCurrencyCode { get; set; }

        public string ScreenId { get; set; }

        public int? ItemList { get; set; }

        public string ListDesc { get; set; }

        public int? BrandId { get; set; }

        public string BrandName { get; set; }

        public int? DivisionId { get; set; }

        public string DivisionDesc { get; set; }

        public decimal? CurrentCost { get; set; }

        public decimal? CurrentPrice { get; set; }

        public decimal? Margin { get; set; }

        public decimal? ExchangeRate { get; set; }

        public string SellingUom { get; set; }

        public string SellingUomDesc { get; set; }

        public string LocType { get; set; }

        public decimal AfterDiscUnitCost { get; set; }

        public decimal DiscountPercent { get; set; }

        public string DiscountType { get; set; }
    }
}
