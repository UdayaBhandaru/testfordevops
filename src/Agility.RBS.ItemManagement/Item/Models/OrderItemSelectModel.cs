﻿// <copyright file="OrderItemSelectModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
namespace Agility.RBS.ItemManagement.Item.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class OrderItemSelectModel
    {
        public string ItemBarcode { get; set; }

        public string ItemDesc { get; set; }

        public int? DivisionId { get; set; }

        public string DivisionDesc { get; set; }

        public int? DeptId { get; set; }

        public string DeptDesc { get; set; }

        public int? CategoryId { get; set; }

        public string CategoryDesc { get; set; }

        public int? Supplier { get; set; }

        public string SupName { get; set; }

        public int? LocationId { get; set; }

        public string LocName { get; set; }

        public string LocType { get; set; }

        public string ImportCountry { get; set; }

        public int? ItemList { get; set; }

        public int? BrandId { get; set; }

        public string BrandName { get; set; }

        public decimal? UnitCost { get; set; }

        public string DefaultUnit { get; set; }

        public string DefaultUnitDesc { get; set; }

        public decimal? DefaultUnitSize { get; set; }

        public decimal? DiscountPercent { get; set; }

        public string DiscountType { get; set; }

        public decimal? AfterDiscUnitCost { get; set; }

        public decimal? UnitRetail { get; set; }

        public string CostCurrency { get; set; }

        public string RetailCurrency { get; set; }
    }
}