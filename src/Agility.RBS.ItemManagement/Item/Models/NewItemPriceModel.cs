﻿// <copyright file="NewItemPriceModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.Item.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.Framework.Core.Profile.Entities;

    public class NewItemPriceModel
    {
        public decimal? CurrentPrice { get; set; }

        public string PriceCurrencyCode { get; set; }

        public decimal? Margin { get; set; }

        public decimal? ExchangeRate { get; set; }

        public string Format { get; set; }

        public decimal? NetMargin { get; set; }

        public string CountryCode { get; set; }

        public string CountryName { get; set; }

        public string Operation { get; set; }
    }
}
