﻿// <copyright file="ItemStoreRangingModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.Item.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.Framework.Core.Profile.Entities;

    public class ItemStoreRangingModel
    {
        public int? Loc { get; set; }

        public string LocName { get; set; }

        public string SourceMethod { get; set; }
    }
}
