﻿// <copyright file="ItemBasicInfoModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.Item.Models
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.ComponentModel.DataAnnotations.Schema;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Core.Models;
    using Agility.RBS.Inbox.Common;

    [DataProfile(Name = "ITEMBASICINFO")]

    public class ItemBasicInfoModel : ProfileEntity<ItemBasicInfoWfModel>, Core.IPaginationModel, IInboxCommonModel
    {
        public ItemBasicInfoModel()
        {
            this.WorkflowForm = new RbWfHeaderModel();
            this.ItemAdditionalUoms = new List<ItemAdditionalUomModel>();
        }

        [Column("ITEM")]
        public string Item { get; set; }

        [NotMapped]
        public string ItemType { get; set; }

        [NotMapped]
        public string ItemTypeDesc { get; set; }

        [NotMapped]
        public string ItemParent { get; set; }

        [NotMapped]
        [Column("ITEM_GRANDPARENT")]
        public string ItemGrandparent { get; set; }

        [NotMapped]
        [Column("PACK_IND")]
        public string PackInd { get; set; }

        [NotMapped]
        [Column("SIMPLE_PACK_IND")]
        public string SimplePackInd { get; set; }

        [NotMapped]
        [Column("ITEM_LEVEL")]
        public string ItemLevel { get; set; }

        [NotMapped]
        [Column("TRAN_LEVEL")]
        public string TranLevel { get; set; }

        [NotMapped]
        [Column("DIVISION_ID")]
        public int? DivisionId { get; set; }

        [NotMapped]
        public string DivisionDesc { get; set; }

        [NotMapped]
        [Column("DEPT_ID")]
        public int? DeptId { get; set; }

        [NotMapped]
        public string DeptDesc { get; set; }

        [NotMapped]
        [Column("CATEGORY_ID")]
        public int? CategoryId { get; set; }

        [NotMapped]
        public string CategoryDesc { get; set; }

        [NotMapped]
        [Column("CLASS_ID")]
        public int? ClassId { get; set; }

        [NotMapped]
        public string ClassDesc { get; set; }

        [Column("SUBCLASS_ID")]
        public int? SubClassId { get; set; }

        [NotMapped]
        public string SubclassDesc { get; set; }

        [NotMapped]
        [Column("ITEM_AGGREGATE_IND")]
        public string ItemAggregateInd { get; set; }

        [NotMapped]
        [Column("DIFF_1")]
        public string Diff1 { get; set; }

        [NotMapped]
        [Column("DIFF_1_AGGREGATE_IND")]
        public string Diff1AggregateInd { get; set; }

        [NotMapped]
        [Column("DIFF_2")]
        public string Diff2 { get; set; }

        [NotMapped]
        [Column("DIFF_2_AGGREGATE_IND")]
        public string Diff2AggregateInd { get; set; }

        [NotMapped]
        [Column("DIFF_3")]
        public string Diff3 { get; set; }

        [NotMapped]
        [Column("DIFF_3_AGGREGATE_IND")]
        public string Diff3AggregateInd { get; set; }

        [NotMapped]
        [Column("DIFF_4")]
        public string Diff4 { get; set; }

        [NotMapped]
        [Column("DIFF_4_AGGREGATE_IND")]
        public string Diff4AggregateInd { get; set; }

        [NotMapped]
        [Column("DIFF_5")]
        public string Diff5 { get; set; }

        [NotMapped]
        [Column("DIFF_5_AGGREGATE_IND")]
        public string Diff5AggregateInd { get; set; }

        [NotMapped]
        [Column("DIFF_6")]
        public string Diff6 { get; set; }

        [NotMapped]
        [Column("DIFF_6_AGGREGATE_IND")]
        public string Diff6AggregateInd { get; set; }

        [NotMapped]
        [Column("DIFF_7")]
        public string Diff7 { get; set; }

        [NotMapped]
        [Column("DIFF_7_AGGREGATE_IND")]
        public string Diff7AggregateInd { get; set; }

        [NotMapped]
        [Column("DESC_LONG")]
        public string DescLong { get; set; }

        [NotMapped]
        [Column("DESC_UP")]
        public string DescUp { get; set; }

        [NotMapped]
        [Column("SECONDARY_DESC")]
        public string SecondaryDesc { get; set; }

        [NotMapped]
        [Column("SHORT_DESC")]
        public string ShortDesc { get; set; }

        [NotMapped]
        [Column("ONLINE_DESC")]
        public string OnlineDesc { get; set; }

        [NotMapped]
        [Column("SUPP_DESC")]
        public string SuppDesc { get; set; }

        [NotMapped]
        [Column("BRAND_ID")]
        public int? BrandId { get; set; }

        [NotMapped]
        [Column("UNIT_OF_MEASURE")]
        public string UnitOfMeasure { get; set; }

        [NotMapped]
        [Column("UOM_UNIT")]
        public string UomUnit { get; set; }

        [NotMapped]
        [Column("COMP_QTY")]
        public int? CompQty { get; set; }

        [NotMapped]
        [Column("COMP_UOM")]
        public string CompUom { get; set; }

        [NotMapped]
        [Column("PACKING_DESC")]
        public string PackingDesc { get; set; }

        [NotMapped]
        [Column("SYSTEM_DESC")]
        public string SystemDesc { get; set; }

        [NotMapped]
        [Column("STATUS")]
        public string Status { get; set; }

        [NotMapped]
        [Column("STANDARD_UOM")]
        public string StandardUom { get; set; }

        [NotMapped]
        [Column("UOM_CONV_FACTOR")]
        public decimal? UomConvFactor { get; set; }

        [NotMapped]
        [Column("HANDLING_TEMP")]
        public string HandlingTemp { get; set; }

        [NotMapped]
        [Column("HANDLING_SENSITIVITY")]
        public string HandlingSensitivity { get; set; }

        [NotMapped]
        [Column("CATCH_WEIGHT_IND")]
        public string CatchWeightInd { get; set; }

        [NotMapped]
        [Column("DEFAULT_WASTE_PCT")]
        public decimal? DefaultWastePct { get; set; }

        [NotMapped]
        [Column("CONST_DIMEN_IND")]
        public string ConstDimenInd { get; set; }

        [NotMapped]
        [Column("PACK_TYPE")]
        public string PackType { get; set; }

        [NotMapped]
        [Column("ORDER_AS_TYPE")]
        public string OrderAsType { get; set; }

        [NotMapped]
        [Column("COMMENTS")]
        public string Comments { get; set; }

        [NotMapped]
        [Column("GIFT_WRAP_IND")]
        public string GiftWrapInd { get; set; }

        [NotMapped]
        [Column("SHIP_ALONE_IND")]
        public string ShipAloneInd { get; set; }

        [NotMapped]
        [Column("ITEM_XFORM_IND")]
        public string ItemXformInd { get; set; }

        [NotMapped]
        [Column("PERISHABLE_IND")]
        public string PerishableInd { get; set; }

        [NotMapped]
        public string LangId { get; set; }

        [NotMapped]
        public string ProgramPhase { get; set; }

        [NotMapped]
        public string ProgramMessage { get; set; }

        [NotMapped]
        public string PError { get; set; }

        [NotMapped]
        public string Operation { get; set; }

        [NotMapped]
        public int? Supplier { get; set; }

        [NotMapped]
        [Column("SELLABLE_IND")]
        public string Sellable { get; set; }

        [NotMapped]
        [Column("ORDERABLE_IND")]
        public string Orderable { get; set; }

        [NotMapped]
        [Column("INVENTORY_IND")]
        public string Inventory { get; set; }

        [NotMapped]
        public string Vpn { get; set; }

        [NotMapped]
        public string Barcode { get; set; }

        [NotMapped]
        public string TableName { get; set; }

        [NotMapped]
        public string SubComments { get; set; }

        [NotMapped]
        public string Priority { get; set; }

        [NotMapped]
        public string ItemStatusDesc { get; set; }

        [NotMapped]
        public int? StartRow { get; set; }

        [NotMapped]
        public int? EndRow { get; set; }

        [NotMapped]
        public long? TotalRows { get; set; }

        [NotMapped]
        public SortModel[] SortModel { get; set; }

        [NotMapped]
        [Column("ORGANISATION_ID")]
        public int? OrganizationId { get; set; }

        [NotMapped]
        [Column("CREATED_BY")]
        public string CreatedBy { get; set; }

        [NotMapped]
        [Column("LAST_UPDATED_BY")]
        public string ModifiedBy { get; set; }

        [NotMapped]
        [Column("DeletedInd")]
        public bool DeletedInd { get; set; }

        [NotMapped]
        [Column("CREATED_DATE")]
        public DateTimeOffset? CreatedDate { get; set; }

        [NotMapped]
        [Column("LAST_UPDATED_DATE")]
        public DateTimeOffset? ModifiedDate { get; set; }

        [NotMapped]
        public RbWfHeaderModel WorkflowForm { get; set; }

        [NotMapped]
        public int? FileCount { get; set; }

        [NotMapped]
        public string ItemStatus { get; set; }

        [NotMapped]
        public string ItemTier { get; set; }

        [NotMapped]
        public string ShortDescArabic { get; set; }

        [NotMapped]
        public string ItemSpecification { get; set; }

        [NotMapped]
        public string ItemSpecificationValue { get; set; }

        [NotMapped]
        public string ItemNature { get; set; }

        [NotMapped]
        public string ItemNatureValue { get; set; }

        [NotMapped]
        public string ParentItem { get; set; }

        [NotMapped]
        public string ExpiryFlag { get; set; }

        [NotMapped]
        public List<ItemAdditionalUomModel> ItemAdditionalUoms { get; private set; }
    }
}