﻿// <copyright file="NewItemModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// NewItemModel
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.ItemManagement.Item.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Inbox.Common;
    using Agility.RBS.MDM.Repositories.Models;

    [DataProfile(Name = "NEWITEM")]
    public class NewItemModel : ProfileEntity<NewItemWfModel>, IInboxCommonModel
    {
        public NewItemModel()
        {
            this.ItemMasters = new List<ItemMasterModel>();
            this.Countries = new List<string>();
            this.ItemStoreRangings = new List<ItemStoreRangingModel>();
        }

        [Column("HEAD_SEQ")]
        public int? HEAD_SEQ { get; set; }

        [NotMapped]
        public string Description { get; set; }

        [NotMapped]
        public int? SupplierId { get; set; }

        [NotMapped]
        public string SupplierName { get; set; }

        [NotMapped]
        public int? DivisionId { get; set; }

        [NotMapped]
        public string DivisionDesc { get; set; }

        [NotMapped]
        public string Operation { get; set; }

        [NotMapped]
        public Core.Models.RbWfHeaderModel WorkflowForm { get; set; }

        [NotMapped]
        [Column("ORGANISATION_ID")]
        public new int? OrganizationId { get; set; }

        [NotMapped]
        [Column("CREATED_BY")]
        public new string CreatedBy { get; set; }

        [NotMapped]
        [Column("LAST_UPDATED_BY")]
        public new string ModifiedBy { get; set; }

        [NotMapped]
        [Column("DeletedInd")]
        public new bool? DeletedInd { get; set; }

        [NotMapped]
        [Column("CREATED_DATE")]
        public new DateTimeOffset? CreatedDate { get; set; }

        [NotMapped]
        [Column("LAST_UPDATED_DATE")]
        public new DateTimeOffset? ModifiedDate { get; set; }

        [NotMapped]
        public int? WfNxtStateInd { get; set; }

        [NotMapped]
        public string Status { get; set; }

        [NotMapped]
        public int? FileCount { get; set; }

        [NotMapped]
        public List<ItemMasterModel> ItemMasters { get; private set; }

        [NotMapped]
        public List<ItemStoreRangingModel> ItemStoreRangings { get; private set; }

        [NotMapped]
        public int? DeptId { get; set; }

        [NotMapped]
        public int? CategoryId { get; set; }

        [NotMapped]
        public int? ClassId { get; set; }

        [NotMapped]
        public int? SubclassId { get; set; }

        [NotMapped]
        public string ItemTier { get; set; }

        [NotMapped]
        public string BrandId { get; set; }

        [NotMapped]
        public string BrandName { get; set; }

        [NotMapped]
        public string StandardUom { get; set; }

        [NotMapped]
        public string OriginCountry { get; set; }

        [NotMapped]
        public string ExpiryFlag { get; set; }

        [NotMapped]
        public int? ExpiryOutbound { get; set; }

        [NotMapped]
        public int? ExpiryInbound { get; set; }

        [NotMapped]
        public string Item { get; set; }

        [NotMapped]
        public string Replenish { get; set; }

        [NotMapped]
        public string DeptDesc { get; set; }

        [NotMapped]
        public string CategoryDesc { get; set; }

        [NotMapped]
        public string ClassDesc { get; set; }

        [NotMapped]
        public string SubclassDesc { get; set; }

        [NotMapped]
        public List<string> Countries { get; internal set; }

        [NotMapped]
        public string ItemType { get; set; }

        [NotMapped]
        public string CountryId { get; set; }
    }
}
