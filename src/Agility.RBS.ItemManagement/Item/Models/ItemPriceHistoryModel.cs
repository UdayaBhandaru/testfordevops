﻿// <copyright file="ItemPriceHistoryModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.Item.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Core.Models;
    using Agility.RBS.Inbox.Common;

    public class ItemPriceHistoryModel : ProfileEntity
    {
        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        public string LocType { get; set; }

        public string LocTypeDesc { get; set; }

        public int? Loc { get; set; }

        public string LocName { get; set; }

        public string TranType { get; set; }

        public string TranTypeDesc { get; set; }

        public string Item { get; set; }

        public string ItemDesc { get; set; }

        public DateTime? TranDate { get; set; }

        public decimal? UnitCost { get; set; }

        public decimal? UnitRetail { get; set; }

        public string Uom { get; set; }

        public string LocalCurr { get; set; }
    }
}