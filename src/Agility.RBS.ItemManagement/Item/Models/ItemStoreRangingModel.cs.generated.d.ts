declare module Server.Dtos {
	interface ItemStoreRangingModel {
		loc?: number;
		locName: string;
		sourceMethod: string;
	}
}
