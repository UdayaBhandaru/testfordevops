﻿// <copyright file="ItemMasterModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.Item.Models
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.ComponentModel.DataAnnotations.Schema;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Core.Models;
    using Agility.RBS.Inbox.Common;

    public class ItemMasterModel
    {
        public ItemMasterModel()
        {
            this.ItemPrices = new List<NewItemPriceModel>();
        }

        public string Item { get; set; }

        public string ItemType { get; set; }

        public string TranLevel { get; set; }

        public string DescLong { get; set; }

        public string SecondaryDesc { get; set; }

        public string ShortDesc { get; set; }

        public int? BrandId { get; set; }

        public string BrandName { get; set; }

        public string SystemDesc { get; set; }

        public string Operation { get; set; }

        public string Sellable { get; set; }

        public string Orderable { get; set; }

        public string Inventory { get; set; }

        public string Vpn { get; set; }

        public string Barcode { get; set; }

        public string ItemTier { get; set; }

        public string ShortDescArabic { get; set; }

        public string Som { get; set; }

        public string Uop { get; set; }

        public string Hts { get; set; }

        public int? InnerQty { get; set; }

        public int? CaseQty { get; set; }

        public decimal? SuppCaseCost { get; set; }

        public decimal? SuppUnitCost { get; set; }

        public decimal? InvoiceDiscount { get; set; }

        public List<NewItemPriceModel> ItemPrices { get; private set; }

        public int? CompQty { get; set; }

        public int? DivisionId { get; set; }

        public int? DeptId { get; set; }

        public int? CategoryId { get; set; }

        public int? ClassId { get; set; }

        public int? SubclassId { get; set; }

        public string StandardUom { get; set; }

        public string ExpiryFlag { get; set; }

        public int? ExpiryOutbound { get; set; }

        public int? ExpiryInbound { get; set; }

        public string Replenish { get; set; }

        public string OriginCountry { get; set; }

        public string DivisionDesc { get; set; }

        public string DeptDesc { get; set; }

        public string CategoryDesc { get; set; }

        public string ClassDesc { get; set; }

        public string SubclassDesc { get; set; }

        public decimal? NetSuppCaseCost { get; set; }

        public decimal? NetSuppUnitCost { get; set; }
    }
}