﻿// <copyright file="RtvItemSelectModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
namespace Agility.RBS.ItemManagement.Item.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class RtvItemSelectModel
    {
        public string Item { get; set; }

        public string ItemDesc { get; set; }

        public int? Supplier { get; set; }

        public string SupName { get; set; }

        public int? LocationId { get; set; }

        public string LocName { get; set; }

        public string LocType { get; set; }

        public string ImportCountry { get; set; }

        public decimal? UnitCost { get; set; }

        public decimal? UnitRetail { get; set; }

        public string CostCurrency { get; set; }

        public string RetailCurrency { get; set; }

        public string SupCurrency { get; set; }
    }
}