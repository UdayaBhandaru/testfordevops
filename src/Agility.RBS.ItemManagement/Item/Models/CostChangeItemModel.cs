﻿// <copyright file="CostChangeItemModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.Item.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.Framework.Core.Profile.Entities;

    public class CostChangeItemModel
    {
        public CostChangeItemModel()
        {
            this.ItemPrices = new List<CostChangeItemPriceModel>();
        }

        public int? SupplierId { get; set; }

        public string SuppName { get; set; }

        public string ItemBarcode { get; set; }

        public string ItemDesc { get; set; }

        public int? DeptId { get; set; }

        public string DeptDesc { get; set; }

        public string CostCurrencyCode { get; set; }

        public decimal? CurrentCost { get; set; }

        public decimal? CaseCost { get; set; }

        public List<CostChangeItemPriceModel> ItemPrices { get; private set; }
    }
}
