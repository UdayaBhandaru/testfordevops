﻿// <copyright file="NewItemSearchModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.Item.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Core.Models;
    using Agility.RBS.Inbox.Common;

    public class NewItemSearchModel : ProfileEntity, Core.IPaginationModel
    {
        public int? HEAD_SEQ { get; set; }

        public string Description { get; set; }

        public int? SupplierId { get; set; }

        public string SupplierName { get; set; }

        public int? DivisionId { get; set; }

        public string DivisionDesc { get; set; }

        public int? DeptId { get; set; }

        public string DeptDesc { get; set; }

        public int? CategoryId { get; set; }

        public string CategoryDesc { get; set; }

        public int? ClassId { get; set; }

        public string ClassDesc { get; set; }

        public int? SubclassId { get; set; }

        public string SubclassDesc { get; set; }

        public string Item { get; set; }

        public string Barcode { get; set; }

        public string Status { get; set; }

        public int? StartRow { get; set; }

        public int? EndRow { get; set; }

        public long? TotalRows { get; set; }

        public SortModel[] SortModel { get; set; }
    }
}