﻿// <copyright file="ItemSupplierCountryDimensionModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.Item.Models
{
    using System;
    using System.Collections.Generic;
    using Agility.Framework.Core.Profile.Entities;

    public class ItemSupplierCountryDimensionModel : ProfileEntity
    {
        public ItemSupplierCountryDimensionModel()
        {
            this.ItemSupplierCountryDimensions = new List<ItemSupplierCountryDimensionModel>();
            this.DomainData = new Dictionary<string, object>();
        }

        public string Item { get; set; }

        public string CountryCode { get; set; }

        public string CountryName { get; set; }

        public int? CompanyId { get; set; }

        public string CompanyName { get; set; }

        public string ImportCountry { get; set; }

        public int? Supplier { get; set; }

        public string SuppName { get; set; }

        public string DimObject { get; set; }

        public string PresentationMethod { get; set; }

        public decimal? Length { get; set; }

        public decimal? Width { get; set; }

        public decimal? Height { get; set; }

        public string LwhUom { get; set; }

        public decimal? Weight { get; set; }

        public decimal? NetWeight { get; set; }

        public string WeightUom { get; set; }

        public decimal? LiquidVolume { get; set; }

        public string LiquidVolumeUom { get; set; }

        public decimal? StatCube { get; set; }

        public decimal? TareWeight { get; set; }

        public string TareType { get; set; }

        public string Status { get; set; }

        public string Operation { get; set; }

        public string DimObjectDesc { get; set; }

        public string PresentationMethodDesc { get; set; }

        public string TareTypeDesc { get; set; }

        public List<ItemSupplierCountryDimensionModel> ItemSupplierCountryDimensions { get; private set; }

        public Dictionary<string, object> DomainData { get; private set; }
    }
}