﻿//-------------------------------------------------------------------------------------------------
// <copyright file="NewItemController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// BulkItemController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.ItemManagement.Item.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Constants;
    using Agility.RBS.Core.CustomExceptions;
    using Agility.RBS.Core.Models;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.Documents;
    using Agility.RBS.Documents.Models;
    using Agility.RBS.Inbox.Common;
    using Agility.RBS.Inbox.Models;
    using Agility.RBS.Inbox.Repository;
    using Agility.RBS.ItemManagement.BulkItem.Models;
    using Agility.RBS.ItemManagement.BulkItem.Repositories;
    using Agility.RBS.ItemManagement.Common;
    using Agility.RBS.ItemManagement.Item.Models;
    using Agility.RBS.ItemManagement.Item.Repositories;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Models;
    using DocumentFormat.OpenXml.Packaging;
    using DocumentFormat.OpenXml.Spreadsheet;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [Route("api/[controller]/[action]")]
    [AllowAnonymous]
    public class NewItemController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly NewItemRepository itemRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly InboxModel inboxModel;
        private readonly InboxRepository inboxRepository;

        private ServiceDocument<NewItemModel> serviceDocument;
        private string itemCode;

        public NewItemController(
            ServiceDocument<NewItemModel> serviceDocument,
            NewItemRepository itemRepository,
            DomainDataRepository domainDataRepository,
            InboxRepository inboxRepository)
        {
            this.itemRepository = itemRepository;
            this.domainDataRepository = domainDataRepository;
            this.inboxRepository = inboxRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
            this.inboxModel = new InboxModel { Operation = "CHGS", ModuleName = "ITEM" };
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in Bulk Item Head page when Add button clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<NewItemModel>> New()
        {
            this.serviceDocument.New(false);
            var itemModel = this.domainDataRepository.ItemDomainGet().Result;
            this.serviceDocument.DomainData.Add("division", itemModel.Divisions.OrderBy(x => x.DivisionDescription));
            this.serviceDocument.DomainData.Add("group", itemModel.Groups.OrderBy(x => x.GroupName));
            this.serviceDocument.DomainData.Add("department", itemModel.Departments.OrderBy(x => x.DeptName));
            this.serviceDocument.DomainData.Add("class", itemModel.Classes.OrderBy(x => x.ClassName));
            return await this.GetDomainData();
        }

        /// <summary>
        /// This API invoked for populating fields in the Item Head page when Edit icon clicked.
        /// </summary>
        /// <param name="id">Item Id</param>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<NewItemModel>> Open(long id)
        {
            this.itemCode = id.ToString();
            await this.serviceDocument.OpenAsync(this.CallBackOpen);
            return await this.GetDomainData();
        }

        /// <summary>
        /// This API invoked for submitting Item to next level when Submit button clicked.
        /// </summary>
        /// <param name="serviceDocument">Service Document</param>
        /// <returns>ServiceDocument</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<NewItemModel>> Submit([FromBody]ServiceDocument<NewItemModel> serviceDocument)
        {
            this.serviceDocument = serviceDocument;
            this.inboxModel.StateID = GuidConverter.DotNetToOracle(Convert.ToString(serviceDocument.DataProfile.DataModel.WorkflowInstance?.WorkflowStateId));
            await this.serviceDocument.TransitAsync(this.ItemSave);

            if (this.serviceDocument.Result.Type == MessageType.Success)
            {
                if (this.serviceDocument.DataProfile.DataModel.WorkflowInstance.StateName == "Worksheet")
                {
                    this.inboxModel.Operation = "DFT";
                }

                await this.inboxRepository.SetInbox(this.inboxModel, this.serviceDocument);
            }

            return this.serviceDocument;
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<ServiceDocument<NewItemModel>> Reject([FromBody]ServiceDocument<NewItemModel> serviceDocument)
        {
            this.serviceDocument = serviceDocument;
            this.inboxModel.StateID = RbsServiceDocCustomizationHelper.GetStateIdDotNetToOracle(serviceDocument);
            await this.serviceDocument.TransitAsync(GuidConverter.DotNetToOracle(serviceDocument.DataProfile.DataModel.WorkflowForm.StateId), true);
            if (this.serviceDocument.Result.Type == MessageType.Success)
            {
                this.inboxModel.Operation = "REJ";
                this.inboxModel.ActionType = "R";
                this.inboxModel.ToStateID = GuidConverter.DotNetToOracle(this.serviceDocument.DataProfile.DataModel.WorkflowStateId.ToString());
                this.inboxRepository.SetInbox(this.inboxModel, this.serviceDocument);
            }

            return this.serviceDocument;
        }

        public async Task<ServiceDocument<NewItemModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.ItemSave);
            return this.serviceDocument;
        }

        private async Task<NewItemModel> CallBackOpen()
        {
            try
            {
                var itemBasicInfo = await this.itemRepository.NewItemGet(this.itemCode, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return itemBasicInfo;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> ItemSave()
        {
            try
            {
                this.serviceDocument.Result = await this.itemRepository.NewItemSet(this.serviceDocument.DataProfile.DataModel);
                if (this.serviceDocument.Result.Type != MessageType.Success)
                {
                    return false;
                }

                this.serviceDocument.DataProfile.DataModel.HEAD_SEQ = this.itemRepository.HeadSeq;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
            }

            return true;
        }

        private async Task<ServiceDocument<NewItemModel>> GetDomainData()
        {
            this.serviceDocument.DomainData.Add("indicator", this.domainDataRepository.DomainDetailData(ItemManagementConstants.DomainHdrYesNo).Result);
            this.serviceDocument.DomainData.Add("som", this.domainDataRepository.DomainDetailData(ItemManagementConstants.DomainHdrSom).Result);
            this.serviceDocument.DomainData.Add("uomList", this.domainDataRepository.UomDomainGet().Result.OrderBy(x => x.Description));
            this.serviceDocument.DomainData.Add("itemtier", this.domainDataRepository.DomainDetailData(ItemManagementConstants.DomainHdrITier).Result);
            this.serviceDocument.DomainData.Add("country", this.domainDataRepository.CountryDomainGet().Result);
            this.serviceDocument.DomainData.Add("itemType", this.domainDataRepository.DomainDetailData(ItemManagementConstants.DomainHdrItemtype).Result.OrderBy(x => x.CodeSeq));
            this.serviceDocument.DomainData.Add("sourceMethods", this.domainDataRepository.DomainDetailData(ItemManagementConstants.DomainHdrSourceMethod).Result.OrderBy(x => x.CodeSeq));
            this.serviceDocument.DomainData.Add("rangecountry", this.domainDataRepository.CountryCurrDomainGet().Result);
            this.serviceDocument.LocalizationData.AddData("Item");
            return this.serviceDocument;
        }
    }
}
