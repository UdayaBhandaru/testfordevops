﻿// <copyright file="ItemSelectController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.Item.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Workflow.Models;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Models;
    using Agility.RBS.Core.Services;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.ItemManagement.Common;
    using Agility.RBS.ItemManagement.Item.Models;
    using Agility.RBS.ItemManagement.Item.Repositories;
    using Agility.RBS.MDM.ItemList;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    public class ItemSelectController : Controller
    {
        private readonly ItemSelectRepository itemRepository;
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly ServiceDocument<ItemSelectModel> serviceDocument;

        public ItemSelectController(ServiceDocument<ItemSelectModel> serviceDocument, ItemSelectRepository itemRepository, DomainDataRepository domainDataRepository)
        {
            this.serviceDocument = serviceDocument;
            this.itemRepository = itemRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<ItemSelectModel>> List()
        {
            return await this.GetDomainData();
        }

        [HttpPost]
        public async Task<ServiceDocument<ItemSelectModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.SearchItem);
            return this.serviceDocument;
        }

        [HttpPost]
        public async Task<List<CostChgItemSelectModel>> GetCostChgItemList([FromBody]ItemSelectModel itemSelectModel)
        {
            return await this.itemRepository.SelectCostChgItems(itemSelectModel, this.serviceDocumentResult);
        }

        public async Task<List<ItemDetailsPromoModel>> GetPromoItemDetails(string item, string screenId)
        {
            return await this.itemRepository.GetItemsForPromotion(item, screenId, this.serviceDocumentResult);
        }

        [HttpPost]
        public async Task<List<PriceChgItemSelectModel>> GetPriceChgItemList([FromBody]ItemSelectModel itemSelectModel)
        {
            return await this.itemRepository.SelectPriceChgItems(itemSelectModel, this.serviceDocumentResult);
        }

        private async Task<ServiceDocument<ItemSelectModel>> GetDomainData()
        {
            this.serviceDocument.DomainData.Add("division", this.domainDataRepository.DivisionDomainGet().Result);
            this.serviceDocument.DomainData.Add("group", this.domainDataRepository.GroupDomainGet().Result);
            this.serviceDocument.DomainData.Add("department", this.domainDataRepository.DepartmentDomainGet().Result);
            this.serviceDocument.DomainData.Add("supplier", this.domainDataRepository.SupplierDomainGet().Result);
            this.serviceDocument.DomainData.Add("brand", this.domainDataRepository.BrandDomainGet().Result);
            this.serviceDocument.DomainData.Add("costOrgLevel", this.domainDataRepository.DomainDetailData(ItemManagementConstants.DomainHdrCostOrgLevel).Result);
            this.serviceDocument.DomainData.Add("priceOrgLevel", this.domainDataRepository.DomainDetailData(ItemManagementConstants.DomainHdrPriceOrgLevel).Result);
            this.serviceDocument.DomainData.Add("locType", this.domainDataRepository.DomainDetailData(ItemManagementConstants.DomainHdrLocType).Result);
            this.serviceDocument.DomainData.Add("location", this.domainDataRepository.LocationDomainGet().Result);
            this.serviceDocument.DomainData.Add("pricezone", this.domainDataRepository.CommonData("PZ").Result);
            this.serviceDocument.DomainData.Add("costzone", this.domainDataRepository.CommonData("CZ").Result);
            this.serviceDocument.DomainData.Add("itemlist", this.domainDataRepository.ItemListDomainGet().Result);
            return this.serviceDocument;
        }

        private async Task<List<ItemSelectModel>> SearchItem()
        {
            try
            {
                var items = await this.itemRepository.SelectItems(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return items;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }
    }
}
