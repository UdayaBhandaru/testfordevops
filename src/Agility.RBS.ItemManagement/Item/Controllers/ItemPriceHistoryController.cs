﻿// <copyright file="ItemPriceHistoryController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.Item.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Models;
    using Agility.Framework.Core.Workflow.Entities;
    using Agility.Framework.Core.Workflow.Models;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Constants;
    using Agility.RBS.Core.Models;
    using Agility.RBS.Core.Services;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.Inbox.Common;
    using Agility.RBS.Inbox.Models;
    using Agility.RBS.Inbox.Repository;
    using Agility.RBS.ItemManagement.Common;
    using Agility.RBS.ItemManagement.Item.Models;
    using Agility.RBS.ItemManagement.Item.Repositories;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [Route("api/[controller]/[action]")]
    public class ItemPriceHistoryController : Controller
    {
        private readonly NewItemRepository itemRepository;
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly ServiceDocument<ItemPriceHistoryModel> serviceDocument;

        public ItemPriceHistoryController(
            ServiceDocument<ItemPriceHistoryModel> serviceDocument,
            NewItemRepository itemRepository,
            DomainDataRepository domainDataRepository)
        {
            this.serviceDocument = serviceDocument;
            this.itemRepository = itemRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in the Item List page.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<ItemPriceHistoryModel>> List()
        {
            return await this.GetDomainData();
        }

        /// <summary>
        /// This API invoked for fetching Item records based on search criteria when Search button clicked.
        /// </summary>
        /// <returns>ServiceDocument</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<ItemPriceHistoryModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.SearchItem);
            return this.serviceDocument;
        }

        private async Task<List<ItemPriceHistoryModel>> SearchItem()
        {
            try
            {
                this.itemRepository.TranType = null;
                if (!string.IsNullOrEmpty(this.serviceDocument.DataProfile.DataModel.TranTypeDesc))
                {
                    var transTypes = this.domainDataRepository.DomainDetailData(ItemManagementConstants.DomainHdrTransactionType).Result;
                    this.itemRepository.TranType = transTypes.FirstOrDefault(ids => ids.CodeDesc == this.serviceDocument.DataProfile.DataModel.TranTypeDesc).Code;
                }

                var items = await this.itemRepository.ItemPriceHistoryGet(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return items;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<ServiceDocument<ItemPriceHistoryModel>> GetDomainData()
        {
            this.serviceDocument.DomainData.Add("transType", this.domainDataRepository.DomainDetailData(ItemManagementConstants.DomainHdrTransactionType).Result);
            this.serviceDocument.DomainData.Add("location", this.domainDataRepository.LocationDomainGet().Result);
            return this.serviceDocument;
        }
    }
}
