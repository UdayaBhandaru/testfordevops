﻿// <copyright file="ItemController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.Item.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Models;
    using Agility.Framework.Core.Workflow.Entities;
    using Agility.Framework.Core.Workflow.Models;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Constants;
    using Agility.RBS.Core.Models;
    using Agility.RBS.Core.Services;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.Inbox.Common;
    using Agility.RBS.Inbox.Models;
    using Agility.RBS.Inbox.Repository;
    using Agility.RBS.ItemManagement.Common;
    using Agility.RBS.ItemManagement.Item.Models;
    using Agility.RBS.ItemManagement.Item.Repositories;
    using Agility.RBS.ItemManagement.Repositories;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    [Route("api/[controller]/[action]")]
    public class ItemController : Controller
    {
        private readonly NewItemRepository itemRepository;
        private readonly DomainDataRepository domainDataRepository;
        private readonly BarcodeRepository barcodeRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly ServiceDocument<NewItemSearchModel> serviceDocument;

        public ItemController(
            ServiceDocument<NewItemSearchModel> serviceDocument,
            NewItemRepository itemRepository,
            DomainDataRepository domainDataRepository,
            BarcodeRepository barcodeRepository,
            RazorTemplateService razorTemplateService)
        {
            this.serviceDocument = serviceDocument;
            this.itemRepository = itemRepository;
            this.domainDataRepository = domainDataRepository;
            this.barcodeRepository = barcodeRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in the Item List page.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<NewItemSearchModel>> List()
        {
            return await this.GetDomainData();
        }

        /// <summary>
        /// This API invoked for fetching Item records based on search criteria when Search button clicked.
        /// </summary>
        /// <returns>ServiceDocument</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<NewItemSearchModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.SearchItem);
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for populating dependent field Sub Class data based on selected Division, Dept, Category, Class.
        /// </summary>
        /// <param name="deptId">Department Id</param>
        /// <param name="classId">Class Id</param>
        /// <returns>List Of SubClassDomainModel</returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<AjaxModel<List<SubClassDomainModel>>> SubClassGetbyName(int deptId, int classId)
        {
            try
            {
                return await this.domainDataRepository.SubClassDomainGet(deptId, classId);
            }
            catch (Exception ex)
            {
                return new AjaxModel<List<SubClassDomainModel>> { Result = AjaxResult.Exception, Message = ex.Message, Model = null };
            }
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<List<BrandDomainModel>> BrandsGetName(string name)
        {
            return await this.domainDataRepository.BrandDomainGet(name);
        }

        /// <summary>
        /// This API invoked for generating barcode for item based on selected barcode type.
        /// </summary>
        /// <param name="type">Barcode Type</param>
        /// <param name="itemNo">Item Id</param>
        /// <returns>Barcode</returns>
        [AllowAnonymous]
        [HttpGet]
        public async ValueTask<AjaxModel<string>> GenerateBarcode(string type, string itemNo)
        {
            return await this.barcodeRepository.GenerateBarcode(type, itemNo);
        }

        private async Task<List<NewItemSearchModel>> SearchItem()
        {
            try
            {
                var items = await this.itemRepository.NewItemListGetWithSdoc(this.serviceDocument, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return items;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<ServiceDocument<NewItemSearchModel>> GetDomainData()
        {
            var itemModel = this.domainDataRepository.ItemDomainGet().Result;
            this.serviceDocument.DomainData.Add("division", itemModel.Divisions.OrderBy(x => x.DivisionDescription));
            this.serviceDocument.DomainData.Add("department", itemModel.Groups.OrderBy(x => x.GroupName));
            this.serviceDocument.DomainData.Add("category", itemModel.Departments.OrderBy(x => x.DeptName));
            this.serviceDocument.DomainData.Add("class", itemModel.Classes.OrderBy(x => x.ClassName));
            this.serviceDocument.DomainData.Add("workflowStatusList", this.domainDataRepository.WorkflowStatusGet(RbCommonConstants.ItemModule).Result.OrderBy(x => x.Name));
            this.serviceDocument.LocalizationData.AddData("Item");
            return this.serviceDocument;
        }
    }
}
