﻿// <copyright file="ItemBasicInfoController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.Item
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Workflow.Entities;
    using Agility.Framework.Core.Workflow.Models;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Constants;
    using Agility.RBS.Core.Models;
    using Agility.RBS.Core.Services;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.Inbox.Common;
    using Agility.RBS.Inbox.Models;
    using Agility.RBS.Inbox.Repository;
    using Agility.RBS.ItemManagement.Common;
    using Agility.RBS.ItemManagement.Item.Models;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [Route("api/[controller]/[action]")]
    public class ItemBasicInfoController : Controller
    {
        private readonly ItemRepository itemRepository;
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly InboxRepository inboxRepository;
        private readonly InboxModel inboxModel;
        private ServiceDocument<ItemBasicInfoModel> serviceDocument;
        private string itemCode;

        public ItemBasicInfoController(
            ServiceDocument<ItemBasicInfoModel> serviceDocument,
            ItemRepository itemRepository,
            DomainDataRepository domainDataRepository,
            InboxRepository inboxRepository,
            RazorTemplateService razorTemplateService)
        {
            this.serviceDocument = serviceDocument;
            this.itemRepository = itemRepository;
            this.domainDataRepository = domainDataRepository;
            this.inboxRepository = inboxRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
            this.inboxModel = new InboxModel { Operation = "CHGS", ModuleName = "ITEM" };
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in Item Head page when Add button clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<ItemBasicInfoModel>> New()
        {
            this.serviceDocument.New(false);
            await this.GetDomainData();
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for populating fields in the Item Head page when Edit icon clicked.
        /// </summary>
        /// <param name="id">Item Id</param>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<ItemBasicInfoModel>> Open(long id)
        {
            this.itemCode = id.ToString();
            await this.serviceDocument.OpenAsync(this.CallBackOpen);
            bool valid = await this.VerifyItemLimitInDpt(this.serviceDocument.DataProfile.DataModel.Item);
            if (!valid)
            {
                this.serviceDocument.DataProfile.ActionService.AllowedActions = new List<WorkflowAction>();
            }

            await this.GetDomainData();
            this.serviceDocument.DomainData.Add("inboxCommonData", this.domainDataRepository.InboxCommonGet(RbCommonConstants.ItemModule, id).Result);
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for saving field values in the Item Head page when Save button clicked.
        /// </summary>
        /// <param name="serviceDocument">Service Document</param>
        /// <returns>ServiceDocument</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<ItemBasicInfoModel>> Save([FromBody]ServiceDocument<ItemBasicInfoModel> serviceDocument)
        {
            await this.serviceDocument.SaveAsync(this.ItemSave);
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for submitting Item to next level when Submit button clicked.
        /// </summary>
        /// <param name="serviceDocument">Service Document</param>
        /// <returns>ServiceDocument</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<ItemBasicInfoModel>> Submit([FromBody]ServiceDocument<ItemBasicInfoModel> serviceDocument)
        {
            this.serviceDocument = serviceDocument;
            if (this.serviceDocument.DataProfile.DataModel.Item != null &&
                !await this.VerifyItemLimitInDpt(this.serviceDocument.DataProfile.DataModel.Item))
            {
                this.serviceDocument.Result.Message = "Exceeds items in Department";
                this.serviceDocument.Result.Type = MessageType.ValidationException;
                this.serviceDocument.SaveAsync(this.ItemSave);
            }
            else
            {
                this.inboxModel.StateID = RbsServiceDocCustomizationHelper.GetStateIdDotNetToOracle(this.serviceDocument);
                if (this.serviceDocument.DataProfile.DataModel.Item == "0" || this.serviceDocument.DataProfile.DataModel.Item == null)
                {
                    await this.serviceDocument.TransitAsync(this.ItemSubmit);
                    if (this.serviceDocument.DataProfile.ActionService.CurrentActionId == "sendToCreatedState")
                    {
                        this.inboxModel.Operation = "DFT";
                    }
                }
                else
                {
                    await this.serviceDocument.TransitAsync(this.ItemSubmit);
                }

                this.inboxRepository.SetInbox(this.inboxModel, this.serviceDocument);
            }

            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for rejecting Item to previous level when Reject button clicked.
        /// </summary>
        /// <param name="serviceDocument">Service Document</param>
        /// <returns>ServiceDocument</returns>
        /// <response code="401">Un Authorized</response>
        [AllowAnonymous]
        [HttpPost]
        public async Task<ServiceDocument<ItemBasicInfoModel>> Reject([FromBody]ServiceDocument<ItemBasicInfoModel> serviceDocument)
        {
            this.inboxModel.StateID = GuidConverter.DotNetToOracle(Convert.ToString(serviceDocument.DataProfile.DataModel.WorkflowInstance?.WorkflowStateId));
            await this.serviceDocument.TransitAsync(GuidConverter.DotNetToOracle(serviceDocument.DataProfile.DataModel.WorkflowForm.StateId), true);
            if (this.serviceDocument.Result.Type == MessageType.Success)
            {
                this.inboxModel.Operation = "REJ";
                this.inboxModel.ActionType = "R";
                this.inboxModel.ToStateID = GuidConverter.DotNetToOracle(this.serviceDocument.DataProfile.DataModel.WorkflowStateId.ToString());
                this.inboxRepository.SetInbox(this.inboxModel, this.serviceDocument);
            }

            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for rejecting Item to previous level when Send Back For Review button clicked.
        /// </summary>
        /// <param name="serviceDocument">Service Document</param>
        /// <returns>ServiceDocument</returns>
        /// <response code="401">Un Authorized</response>
        [AllowAnonymous]
        [HttpPost]
        public async Task<ServiceDocument<ItemBasicInfoModel>> SendBackForReview([FromBody]ServiceDocument<ItemBasicInfoModel> serviceDocument)
        {
            var workFlowInstance = serviceDocument.DataProfile.DataModel.WorkflowInstance;
            string toStateId = this.inboxRepository.GetStateId("SBR", workFlowInstance);
            toStateId = GuidConverter.OracleToDotNet(toStateId);
            await this.serviceDocument.TransitAsync(toStateId, true);

            if (this.serviceDocument.Result.Type == MessageType.Success)
            {
                this.inboxRepository.SetInbox(
                    new InboxModel
                    {
                        Operation = "SBR",
                        ActionType = "B",
                        ToStateID = GuidConverter.DotNetToOracle(workFlowInstance.WorkflowStateId.ToString()),
                        StateID = GuidConverter.DotNetToOracle(toStateId)
                    },
                    this.serviceDocument);
            }

            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for approving item when Approve button clicked.
        /// </summary>
        /// <param name="serviceDocument">Service Document</param>
        /// <returns>ServiceDocument</returns>
        /// <response code="401">Un Authorized</response>
        [AllowAnonymous]
        [HttpPost]
        public async Task<ServiceDocument<ItemBasicInfoModel>> Approve([FromBody]ServiceDocument<ItemBasicInfoModel> serviceDocument)
        {
            this.itemCode = serviceDocument.DataProfile.DataModel.Item;
            var openedSdoc = await this.serviceDocument.OpenAsync(this.CallBackOpen);
            try
            {
                WorkflowAction wfApproveAction = openedSdoc.DataProfile.ActionService.AllowedActions.FirstOrDefault(a => a.ActionName == "Approve");
                if (wfApproveAction != null)
                {
                    openedSdoc.DataProfile.ActionService.CurrentActionId = wfApproveAction.TransitionClaim;
                    await openedSdoc.TransitAsync(this.ItemSubmit);
                    this.inboxRepository.SetInbox(this.inboxModel, openedSdoc);
                }
            }
            catch (Exception e)
            {
                openedSdoc.Result = new ServiceDocumentResult
                {
                    InnerException = string.Empty,
                    StackTrace = e.StackTrace,
                    Type = MessageType.Exception
                };
                return this.serviceDocument;
            }

            return openedSdoc;
        }

        /// <summary>
        /// This API invoked to discard Item.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<ItemBasicInfoModel>> Delete()
        {
            await this.serviceDocument.SaveAsync(this.ItemDelete);
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for populating dependent field Sub Class data based on selected Division, Dept, Category, Class.
        /// </summary>
        /// <param name="deptId">Department Id</param>
        /// <param name="classId">Class Id</param>
        /// <returns>List Of SubClassDomainModel</returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<List<SubClassDomainModel>> SubClassGetbyName(int deptId, int classId)
        {
            return await this.domainDataRepository.SubClassDomainGet(deptId, classId);
        }

        /// <summary>
        /// This API invoked for searching barcode.
        /// </summary>
        /// <param name="barcode">Barcode</param>
        /// <returns>message</returns>
        [AllowAnonymous]
        [HttpGet]
        public async ValueTask<string> CheckBarCode(string barcode)
        {
            return await this.itemRepository.SearchBarcode(barcode, this.serviceDocumentResult);
        }

        /// <summary>
        /// This API invoked for Verifying Item Limit In Dpt.
        /// </summary>
        /// <param name="item">Item Id</param>
        /// <returns>true or false</returns>
        [HttpGet]
        public async ValueTask<bool> VerifyItemLimitInDpt(string item)
        {
            return await this.itemRepository.VerifyItemLimitInDpt(item);
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<List<BrandDomainModel>> BrandsGetName(string name)
        {
            return await this.domainDataRepository.BrandDomainGet(name);
        }

        private async Task<bool> ItemSubmit()
        {
            try
            {
                this.serviceDocument.Result = await this.itemRepository.ItemBasicInfoSet(this.serviceDocument.DataProfile.DataModel);
                if (this.serviceDocument.Result.Type != MessageType.Success)
                {
                    return false;
                }

                this.serviceDocument.DataProfile.DataModel.Item = this.itemRepository.ItemCode;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
            }

            return true;
        }

        private async Task<bool> ItemSave()
        {
            try
            {
                this.serviceDocument.Result = await this.itemRepository.ItemBasicInfoSet(this.serviceDocument.DataProfile.DataModel);
                if (this.serviceDocument.Result.Type != MessageType.Success)
                {
                    return false;
                }

                this.serviceDocument.DataProfile.DataModel.Item = this.itemRepository.ItemCode;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
            }

            return true;
        }

        private async Task<bool> ItemDelete()
        {
            this.serviceDocument.DataProfile.DataModel.Operation = "D";
            this.serviceDocument.Result = await this.itemRepository.ItemBasicInfoSet(this.serviceDocument.DataProfile.DataModel);
            return true;
        }

        private async Task<ItemBasicInfoModel> CallBackOpen()
        {
            try
            {
                var itemBasicInfo = await this.itemRepository.ItemBasicInfoGet(this.itemCode, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return itemBasicInfo;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<ServiceDocument<ItemBasicInfoModel>> GetDomainData()
        {
            var itemModel = this.domainDataRepository.ItemDomainGet().Result;
            this.serviceDocument.DomainData.Add("itemType", this.domainDataRepository.DomainDetailData(ItemManagementConstants.DomainHdrItemtype).Result);
            this.serviceDocument.DomainData.Add("inventory", this.domainDataRepository.DomainDetailData(ItemManagementConstants.DomainHdrInventory).Result);
            this.serviceDocument.DomainData.Add("indicator", this.domainDataRepository.DomainDetailData(ItemManagementConstants.DomainHdrIndicator).Result);
            this.serviceDocument.DomainData.Add("division", itemModel.Divisions.OrderBy(x => x.DivisionDescription));
            this.serviceDocument.DomainData.Add("group", itemModel.Groups.OrderBy(x => x.GroupName));
            this.serviceDocument.DomainData.Add("department", itemModel.Departments.OrderBy(x => x.DeptName));
            this.serviceDocument.DomainData.Add("class", itemModel.Classes.OrderBy(x => x.ClassName));
            ////this.serviceDocument.DomainData.Add("supplier", this.domainDataRepository.SupplierDomainGet().Result);
            this.serviceDocument.DomainData.Add("workflowStatusList", this.domainDataRepository.WorkflowStatusGet(RbCommonConstants.ItemModule).Result.OrderBy(x => x.Name));
            this.serviceDocument.DomainData.Add("statusIndicator", this.domainDataRepository.DomainDetailData(ItemManagementConstants.DomainHdrStatus).Result);
            this.serviceDocument.DomainData.Add("uomList", this.domainDataRepository.UomDomainGet().Result.OrderBy(x => x.Description));
            ////this.serviceDocument.DomainData.Add("brand", this.domainDataRepository.BrandDomainGet().Result);
            this.serviceDocument.DomainData.Add("itemtier", this.domainDataRepository.DomainDetailData(ItemManagementConstants.DomainHdrITier).Result);
            this.serviceDocument.DomainData.Add("itemspec", this.domainDataRepository.DomainDetailData(ItemManagementConstants.DomainHdrSpec).Result);
            this.serviceDocument.DomainData.Add("itemnature", this.domainDataRepository.DomainDetailData(ItemManagementConstants.DomainHdrNature).Result);
            this.serviceDocument.LocalizationData.AddData("Item");
            return this.serviceDocument;
        }
    }
}
