﻿// <copyright file="ItemManagementStartup.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;
    using System.Text;
    using Agility.Framework.Core;
    using Agility.RBS.Core.Services;
    using Agility.RBS.Inbox.Repository;
    using Agility.RBS.ItemManagement.BulkItem;
    using Agility.RBS.ItemManagement.BulkItem.Repositories;
    using Agility.RBS.ItemManagement.Item.Controllers;
    using Agility.RBS.ItemManagement.Item.Repositories;
    using Agility.RBS.ItemManagement.ItemReclassification;
    using Agility.RBS.ItemManagement.ItemReclassification.Repositories;
    using Agility.RBS.ItemManagement.ItemStatus.Repository;
    using Agility.RBS.ItemManagement.ItemSuper;
    using Agility.RBS.ItemManagement.ItemSuper.Repositories;
    using Agility.RBS.ItemManagement.ItemUserDefined;
    using Agility.RBS.ItemManagement.Repositories;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Caching.Distributed;
    using Microsoft.Extensions.Caching.Memory;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using Swashbuckle.AspNetCore.Swagger;

    public class ItemManagementStartup : ComponentStartup<DbContext>
    {
        public override void ConfigureDependencyInjection(IServiceCollection services)
        {
            services.AddTransient<ItemRepository>();
            services.AddTransient<BulkItemRepository>();
            services.AddTransient<RazorTemplateService>();
            services.AddTransient<ItemSelectRepository>();
            services.AddTransient<BarcodeRepository>();
            services.AddTransient<ItemSelectController>();
            services.AddTransient<BulkItemController>();
            services.AddTransient<ItemSuperController>();
            services.AddTransient<ItemSuperRepository>();
            services.AddTransient<ItemStatusRepository>();
            services.AddTransient<WarehouseRepository>();
            services.AddTransient<DsdRepository>();
            services.AddTransient<NewItemRepository>();
            services.AddTransient<NewItemController>();

            services.AddTransient<ItemReclassificationRepository>();
            services.AddTransient<ItemReclassificationController>();
            services.AddTransient<ItemPriceHistoryController>();
            services.AddTransient<ItemUserDefinedController>();
            services.AddTransient<ItemUserDefinedRepository>();
        }

        public void Startup(Microsoft.Extensions.Configuration.IConfigurationRoot configuration)
        {
            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<Item.Mapping.ItemMapping>();
                cfg.AddProfile<BulkItem.Mapping.BulkItemMapping>();
                cfg.AddProfile<ItemStatus.Mapping.ItemStatusMapping>();
                cfg.AddProfile<ItemReclassification.Mapping.ItemReclassificationMapping>();
                cfg.AddProfile<ItemUserDefined.Mapping.ItemUserDefinedMapping>();
                this.InitilizeMapping(cfg);
            });
        }

        public override void ConfigureServices(IServiceCollection services)
        {
            base.ConfigureServices(services);
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "My API", Version = "v1" });
                var xmlFile = $"{this.GetType().GetTypeInfo().Assembly.GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }

        public override void Configure(
           IApplicationBuilder app,
           IHostingEnvironment env,
           ILoggerFactory loggerFactory,
           IDistributedCache distributedCache,
           IMemoryCache memoryCache,
           IServiceProvider serviceProvider)
        {
            base.Configure(app, env, loggerFactory, distributedCache, memoryCache, serviceProvider);
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("//swagger//v1//swagger.json", "My API v1");
                c.SupportedSubmitMethods();
            });
        }
    }
}
