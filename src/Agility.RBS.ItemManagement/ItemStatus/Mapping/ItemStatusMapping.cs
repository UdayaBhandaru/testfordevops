﻿// <copyright file="ItemStatusMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.ItemStatus.Mapping
{
    using System;
    using Agility.RBS.Core;
    using Agility.RBS.ItemManagement.BulkItem.Models;
    using Agility.RBS.ItemManagement.Item.Models;
    using Agility.RBS.ItemManagement.ItemStatus.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class ItemStatusMapping : Profile
    {
        public ItemStatusMapping()
            : base("ItemStatusMapping")
        {
            this.CreateMap<OracleObject, ItemCloseHeadModel>()
             .ForMember(m => m.Id, opt => opt.MapFrom(r => OracleNullHandler.DbNullInt64Handler(r["id"])))
                .ForMember(m => m.Reason, opt => opt.MapFrom(r => r["reason"]))
                .ForMember(m => m.Status, opt => opt.MapFrom(r => r["STATUS"]))
                .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
                .ForMember(m => m.ProgramMessage, opt => opt.MapFrom(r => r["Program_Message"]))
                .ForMember(m => m.ProgramPhase, opt => opt.MapFrom(r => r["program_phase"]))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, ItemCloseModel>()
             .ForMember(m => m.Id, opt => opt.MapFrom(r => OracleNullHandler.DbNullInt64Handler(r["id"])))
                .ForMember(m => m.DivisionId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DIVISION_ID"])))
                 .ForMember(m => m.ItemTier, opt => opt.MapFrom(r => r["item_tier"]))
                 .ForMember(m => m.ItemBarcode, opt => opt.MapFrom(r => r["item_barcode"]))
                .ForMember(m => m.Reason, opt => opt.MapFrom(r => r["reason"]))
                .ForMember(m => m.ItemDesc, opt => opt.MapFrom(r => r["item_desc"]))
                .ForMember(m => m.Sales52Week, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["sales_52week"])))
                .ForMember(m => m.Gp52Week, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["gp_52week"])))
                .ForMember(m => m.DcSoh, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["dc_soh"])))
                .ForMember(m => m.StoresSoh, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["stores_soh"])))
                .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
                .ForMember(m => m.ProgramMessage, opt => opt.MapFrom(r => r["Program_Message"]))
                .ForMember(m => m.ProgramPhase, opt => opt.MapFrom(r => r["program_phase"]));

            this.CreateMap<OracleObject, ZoneGroups>()
               .ForMember(m => m.LocationId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["location_id"])))
               .ForMember(m => m.LocName, opt => opt.MapFrom(r => r["loc_name"]))
               .ForMember(m => m.SourceMethod, opt => opt.MapFrom(r => r["source_method"]));

            this.CreateMap<OracleObject, ItemReactivateHeadModel>()
             .ForMember(m => m.Id, opt => opt.MapFrom(r => OracleNullHandler.DbNullInt64Handler(r["id"])))
                .ForMember(m => m.Description, opt => opt.MapFrom(r => r["DESCRIPTION"]))
                .ForMember(m => m.SupplierId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["SUPPLIER"])))
                .ForMember(m => m.SupplierName, opt => opt.MapFrom(r => r["SUP_NAME"]))
                .ForMember(m => m.SupplierType, opt => opt.MapFrom(r => r["SUPP_TYPE"]))
                .ForMember(m => m.TotalRebates, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["TOTAL_REBATES"])))
                .ForMember(m => m.OriginCountryName, opt => opt.MapFrom(r => r["ORIGIN_COUNTRY_NAME"]))
                .ForMember(m => m.CurrencyCode, opt => opt.MapFrom(r => r["CURRENCY_CODE"]))
                .ForMember(m => m.IncotermsDesc, opt => opt.MapFrom(r => r["INCOTERMS_DESC"]))
                .ForMember(m => m.Status, opt => opt.MapFrom(r => r["STATUS"]))
                .ForMember(m => m.CreatedDate, opt => opt.MapFrom(r => r["CREATED_DATE"]))
                .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
                .ForMember(m => m.ProgramMessage, opt => opt.MapFrom(r => r["Program_Message"]))
                .ForMember(m => m.ProgramPhase, opt => opt.MapFrom(r => r["program_phase"]))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, ReactiveItemSelectModel>()
                 .ForMember(m => m.DivisionId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DIVISION_ID"])))
                 .ForMember(m => m.ItemTier, opt => opt.MapFrom(r => r["ITEM_TIER"]))
                 .ForMember(m => m.ItemBarcode, opt => opt.MapFrom(r => r["ITEM_BARCODE"]))
                 .ForMember(m => m.DeptDesc, opt => opt.MapFrom(r => r["DEPT_DESC"]))
                 .ForMember(m => m.DescLong, opt => opt.MapFrom(r => r["DESC_UP"]))
                 .ForMember(m => m.SuppCaseCost, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["SUPP_CASE_COST"])))
                 .ForMember(m => m.SuppUnitCost, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["SUPP_UNIT_COST"])))
                 .ForMember(m => m.InvoiceDiscount, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["INVOICE_DISCOUNT"])))
                 .ForMember(m => m.LandedUnitCost, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["LANDED_UNIT_COST"])))
                 .ForMember(m => m.LandedCaseCost, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["LANDED_CASE_COST"])))
                 .ForMember(m => m.DivisionId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DIVISION_ID"])));
        }
    }
}
