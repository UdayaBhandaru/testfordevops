﻿// <copyright file="ItemStatusRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.ItemStatus.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Constants;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.ItemManagement.Common;
    using Agility.RBS.ItemManagement.ItemStatus.Models;
    using Agility.RBS.MDM.Repositories.Models;
    using Devart.Data.Oracle;
    using DocumentFormat.OpenXml.Packaging;
    using DocumentFormat.OpenXml.Spreadsheet;
    using ExcelDataReader;

    public class ItemStatusRepository : BaseOraclePackage
    {
        public ItemStatusRepository()
        {
            this.PackageName = ItemManagementConstants.GetItemPackage;
        }

        public long Id { get; internal set; }

        public long ReActId { get; internal set; }

        public static string GetItemStatusFileName(string toItemStatus)
        {
            return toItemStatus == "ITEMCLOSE" ? "Item Close-Out Form" : "ItemReactivation";
        }

        public async Task<ServiceDocumentResult> CloseItemAsync(ItemCloseHeadModel dataModel)
        {
            this.Id = 0;
            PackageParams packageParameter = this.GetPackageParams("item_close_tab", "setitemcloseout");
            OracleObject recordObject = this.SetCloseItem(dataModel);
            var result = await this.SetProcedure(recordObject, packageParameter);
            if (this.ObjResult != null)
            {
                this.Id = Convert.ToInt32(this.ObjResult["ID"]);
            }

            return result;
        }

        public async Task<ItemCloseHeadModel> ClosedItemGet(long id, FilePathModel file, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams("item_close_tab", "getitemcloseout");
            ItemCloseHeadModel model = new ItemCloseHeadModel { Id = id };
            OracleObject recordObject = this.GetCloseItem(model);
            var items = await this.GetProcedure2<ItemCloseHeadModel>(recordObject, packageParameter, serviceDocumentResult);
            if (items != null && items.Count > 0)
            {
                string bulkItemFile = $"{RbSettings.RbsExcelFileDownloadPath}\\Item Close-Out Form-{id}.xlsm";
                if (!File.Exists(bulkItemFile))
                {
                    bulkItemFile = $"{file.ProcessPath}\\Item Close-Out Form-{id}.xlsm";
                    if (!File.Exists(bulkItemFile))
                    {
                        bulkItemFile = $"{file.ArchivePath}\\Item Close-Out Form-{id}.xlsm";
                    }
                }

                await this.ExcelDataToItemClose(await this.GetDataTableByReadingExcel(bulkItemFile), items[0]);
                return items[0];
            }

            return null;
        }

        public async Task<ItemCloseHeadModel> ExcelDataToItemClose(DataTable eSheet, ItemCloseHeadModel iHeadModel)
        {
            if (eSheet != null)
            {
                List<ItemDetailModel> itemDetailsList = (from DataRow dr in eSheet.Rows
                                                              where Convert.ToString(dr[2]) != string.Empty
                                                              && Convert.ToString(dr[1]) != "Item"
                                                              && Convert.ToString(dr[1]) != string.Empty
                                                              select new ItemDetailModel
                                                              {
                                                                  Item = Convert.ToString(dr[1]),
                                                                  ItemDesc = Convert.ToString(dr[2]),
                                                              }).ToList();
                iHeadModel.ItemDetails.AddRange(itemDetailsList);
            }

            return iHeadModel;
        }

        public async Task<ItemReactivateHeadModel> ExcelDataToItemReactive(DataTable eSheet, ItemReactivateHeadModel iHeadModel)
        {
            if (eSheet != null)
            {
                List<ItemDetailModel> itemDetailsList = (from DataRow dr in eSheet.Rows
                                                              where Convert.ToString(dr[2]) != string.Empty
                                                              && Convert.ToString(dr[2]) != "#Item"
                                                              && Convert.ToString(dr[4]) != string.Empty
                                                              select new ItemDetailModel
                                                              {
                                                                  Item = Convert.ToString(dr[2]),
                                                                  ItemDesc = Convert.ToString(dr[4]),
                                                              }).ToList();
                iHeadModel.ItemDetails.AddRange(itemDetailsList);
            }

            return iHeadModel;
        }

        public async Task<List<ItemCloseHeadModel>> ClosedItemAGet(ItemCloseHeadModel itemCloseHeadModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams("item_close_tab", "getitemcloseout");
            OracleObject recordObject = this.GetCloseItem(itemCloseHeadModel);
            return await this.GetProcedure2<ItemCloseHeadModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public OracleObject GetCloseItem(ItemCloseHeadModel dataModel)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType("item_close_rec", this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject["id"] = dataModel.Id;
            recordObject["reason"] = dataModel.Reason;
            return recordObject;
        }

        public async Task<ServiceDocumentResult> ItemStatusChanging(long id, string toItemStatus)
        {
            string file = $"{RbSettings.RbsExcelFileDownloadPath}\\{ItemStatusRepository.GetItemStatusFileName(toItemStatus)}-{id}.xlsm";
            DataTable dt = await this.GetDataTableByReadingExcel(file);
                    List<ItemDetailedStatusModel> iList = toItemStatus == "ITEMREACTIVATE"
                        ? (from DataRow dr in dt.Rows
                           where Convert.ToString(dr[2]) != string.Empty
                           select new ItemDetailedStatusModel
                           {
                               Item = Convert.ToString(dr[2]),
                               ItemStatus = toItemStatus
                           }).ToList()
                        : (from DataRow dr in dt.Rows
                           where Convert.ToString(dr[1]) != string.Empty
                           select new ItemDetailedStatusModel
                           {
                               Item = Convert.ToString(dr[1]),
                               ItemStatus = toItemStatus
                           }).ToList();
            return await this.UpdateItemStatus(iList);
        }

        public async Task<ServiceDocumentResult> SaveReactivateItemAsync(ItemReactivateHeadModel dataModel)
        {
            this.ReActId = 0;
            PackageParams packageParameter = this.GetPackageParams(ItemManagementConstants.ItemReactivateTab, ItemManagementConstants.SetItemReactivate);
            OracleObject recordObject = this.SetReactivateItem(dataModel);
            var result = await this.SetProcedure(recordObject, packageParameter);
            if (this.ObjResult != null)
            {
                this.ReActId = Convert.ToInt32(this.ObjResult["ID"]);
            }

            return result;
        }

        public async Task<ItemReactivateHeadModel> OpenReactivatedItem(long id, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(ItemManagementConstants.ItemReactivateTab, ItemManagementConstants.GetItemReactivate);
            ItemReactivateHeadModel model = new ItemReactivateHeadModel { Id = id };
            OracleObject recordObject = this.GetReactItem(model);
            var item = await this.GetProcedure2<ItemReactivateHeadModel>(recordObject, packageParameter, serviceDocumentResult);
            return item == null ? null : item[0];
        }

        public async Task<ItemReactivateHeadModel> OpenReactivatedItem(long id, FilePathModel file, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(ItemManagementConstants.ItemReactivateTab, ItemManagementConstants.GetItemReactivate);
            ItemReactivateHeadModel model = new ItemReactivateHeadModel { Id = id };
            OracleObject recordObject = this.GetReactItem(model);
            var items = await this.GetProcedure2<ItemReactivateHeadModel>(recordObject, packageParameter, serviceDocumentResult);
            if (items != null && items.Count > 0)
            {
                string bulkItemFile = $"{RbSettings.RbsExcelFileDownloadPath}\\ItemReactivation-{id}.xlsm";
                if (!File.Exists(bulkItemFile))
                {
                    bulkItemFile = $"{file.ProcessPath}\\ItemReactivation-{id}.xlsm";
                    if (!File.Exists(bulkItemFile))
                    {
                        bulkItemFile = $"{file.ArchivePath}\\ItemReactivation-{id}.xlsm";
                    }
                }

                await this.ExcelDataToItemReactive(await this.GetDataTableByReadingExcel(bulkItemFile), items[0]);
                return items[0];
            }

            return null;
        }

        public async Task<List<ItemReactivateHeadModel>> SeachReactivateItems(ItemReactivateHeadModel itemReactivateHeadModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(ItemManagementConstants.ItemReactivateTab, ItemManagementConstants.GetItemReactivate);
            OracleObject recordObject = this.GetReactItem(itemReactivateHeadModel);
            return await this.GetProcedure2<ItemReactivateHeadModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        private OracleObject GetReactItem(ItemReactivateHeadModel dataModel)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(ItemManagementConstants.ItemReactivateRec, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["id"]] = dataModel.Id;
            recordObject[recordType.Attributes["DESCRIPTION"]] = dataModel.Description;
            return recordObject;
        }

        private OracleObject SetReactivateItem(ItemReactivateHeadModel dataModel)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(ItemManagementConstants.ItemReactivateRec, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["id"]] = dataModel.Id;
            recordObject[recordType.Attributes["DESCRIPTION"]] = dataModel.Description;
            recordObject[recordType.Attributes["CREATED_BY"]] = FxContext.Context.Name;
            recordObject[recordType.Attributes["LAST_UPDATED_BY"]] = FxContext.Context.Name;
            recordObject[recordType.Attributes["SUPPLIER"]] = dataModel.SupplierId;
            recordObject[recordType.Attributes["SUPP_TYPE"]] = dataModel.SupplierType;
            recordObject[recordType.Attributes["OPERATION"]] = (dataModel.Id == 0 || dataModel.Id == null) ? RbCommonConstants.InsertOperation : RbCommonConstants.UpdateOperation;
            return recordObject;
        }

        private async Task<ServiceDocumentResult> UpdateItemStatus(IList<ItemDetailedStatusModel> dataList)
        {
            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();
            PackageParams packageParameter = this.GetPackageParams("item_close_reactive_tab", "item_close_reactive");
            OracleParameter parameter;
            parameter = new OracleParameter("RESULT", OracleDbType.Boolean)
            {
                Direction = System.Data.ParameterDirection.ReturnValue
            };
            parameters.Add(parameter);
            parameter = new OracleParameter(packageParameter.ObjectTypeTable, OracleDbType.Table)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                ObjectTypeName = packageParameter.ObjectTypeName,
                Value = this.SetItemsDetailedStatusModel(dataList)
            };
            parameters.Add(parameter);
            var result = await this.SetProcedure(null, packageParameter, parameters);
            return result;
        }

        private OracleTable SetItemsDetailedStatusModel(IList<ItemDetailedStatusModel> lst)
        {
            this.Connection.Open();
            OracleType dtlTableType = this.GetObjectType("item_close_reactive_tab");
            OracleType dtlRecordType = this.GetObjectType("item_close_reactive_rec");
            OracleTable tblTab = new OracleTable(dtlTableType);
            foreach (var item in lst)
            {
                OracleObject dtlRecordObject = this.GetOracleObject(dtlRecordType);
                try
                {
                    dtlRecordObject[dtlRecordType.Attributes["item"]] = item.Item;
                    dtlRecordObject[dtlRecordType.Attributes["item_status"]] = item.ItemStatus;
                    tblTab.Add(dtlRecordObject);
                }
                catch (Exception)
                {
                    tblTab.Add(dtlRecordObject);
                }
            }

            return tblTab;
        }

        private OracleObject SetCloseItem(ItemCloseHeadModel dataModel)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType("item_close_rec", this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["id"]] = dataModel.Id;
            recordObject[recordType.Attributes["reason"]] = dataModel.Reason;
            recordObject[recordType.Attributes["CREATED_BY"]] = FxContext.Context.Name;
            recordObject[recordType.Attributes["LAST_UPDATED_BY"]] = FxContext.Context.Name;
            recordObject[recordType.Attributes["OPERATION"]] = (dataModel.Id == 0 || dataModel.Id == null) ? RbCommonConstants.InsertOperation : RbCommonConstants.UpdateOperation;
            return recordObject;
        }
    }
}
