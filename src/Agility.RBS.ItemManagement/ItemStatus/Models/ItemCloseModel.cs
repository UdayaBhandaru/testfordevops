﻿// <copyright file="ItemCloseModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.ItemStatus.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Core.Models;
    using Agility.RBS.Inbox.Common;

    public class ItemCloseModel
    {
        public ItemCloseModel()
        {
            this.ZoneGroups = new List<ZoneGroups>();
        }

        public long? Id { get; set; }

        public string Reason { get; set; }

        public int? DivisionId { get; set; }

        public string ItemBarcode { get; set; }

        public string ItemTier { get; set; }

        public decimal? Sales52Week { get; set; }

        public decimal? Gp52Week { get; set; }

        [NotMapped]
        public decimal? DcSoh { get; set; }

        public decimal? StoresSoh { get; set; }

        public List<ZoneGroups> ZoneGroups { get; private set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Error { get; set; }

        public string ItemException { get; set; }

        public string ItemDesc { get; set; }
    }
}
