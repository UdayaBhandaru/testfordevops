﻿// <copyright file="ItemReactivateHeadModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.ItemStatus.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Core.Models;
    using Agility.RBS.Inbox.Common;

    [DataProfile(Name = "ITEMREACTIVATEHEAD")]
    public class ItemReactivateHeadModel : ProfileEntity<ItemReactivateHeadWfModel>, IInboxCommonModel
    {
        public ItemReactivateHeadModel()
        {
            this.ItemDetails = new List<ItemDetailModel>();
        }

        [Column("ID")]
        public long? Id { get; set; }

        [NotMapped]
        public int? SupplierId { get; set; }

        [NotMapped]
        public string SupplierName { get; set; }

        [NotMapped]
        public string SupplierType { get; set; }

        [NotMapped]
        public string Description { get; set; }

        [NotMapped]
        public string Operation { get; internal set; }

        [NotMapped]
        [Column("ORGANISATION_ID")]
        public int? OrganizationId { get; set; }

        [NotMapped]
        [Column("CREATED_BY")]
        public string CreatedBy { get; set; }

        [NotMapped]
        [Column("LAST_UPDATED_BY")]
        public string ModifiedBy { get; set; }

        [NotMapped]
        [Column("DeletedInd")]
        public bool DeletedInd { get; set; }

        [NotMapped]
        [Column("CREATED_DATE")]
        public DateTimeOffset? CreatedDate { get; set; }

        [NotMapped]
        [Column("LAST_UPDATED_DATE")]
        public DateTimeOffset? ModifiedDate { get; set; }

        [NotMapped]
        public RbWfHeaderModel WorkflowForm { get; set; }

        [NotMapped]
        public string ProgramPhase { get; set; }

        [NotMapped]
        public string ProgramMessage { get; set; }

        [NotMapped]
        public string Error { get; set; }

        [NotMapped]
        public string Status { get; set; }

        [NotMapped]
        public string OriginCountryName { get; set; }

        [NotMapped]
        public string IncotermsDesc { get; set; }

        [NotMapped]
        public string CurrencyCode { get; set; }

        [NotMapped]
        public decimal? TotalRebates { get; set; }

        [NotMapped]
        public List<ItemDetailModel> ItemDetails { get; private set; }
    }
}
