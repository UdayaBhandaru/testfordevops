﻿// <copyright file="ZoneGroups.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.ItemStatus.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text;
    using Agility.Framework.Core.Profile.Entities;

    public class ZoneGroups
    {
        [NotMapped]
        public int? LocationId { get; set; }

        [NotMapped]
        public string LocName { get; set; }

        [NotMapped]
        public string SourceMethod { get; set; }
    }
}
