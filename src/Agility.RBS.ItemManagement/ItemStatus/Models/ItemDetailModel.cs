﻿// <copyright file="ItemDetailModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.ItemStatus.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text;
    using Agility.Framework.Core.Profile.Entities;

    public class ItemDetailModel
    {
        public string Item { get; set; }

        public string ItemDesc { get; set; }
    }
}
