﻿// <copyright file="ItemReactiveController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.ItemStatus
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.IO;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Models;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Constants;
    using Agility.RBS.Core.Models;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.Documents;
    using Agility.RBS.Inbox.Common;
    using Agility.RBS.Inbox.Models;
    using Agility.RBS.Inbox.Repository;
    using Agility.RBS.ItemManagement.BulkItem.Models;
    using Agility.RBS.ItemManagement.BulkItem.Repositories;
    using Agility.RBS.ItemManagement.Common;
    using Agility.RBS.ItemManagement.Item.Models;
    using Agility.RBS.ItemManagement.Item.Repositories;
    using Agility.RBS.ItemManagement.ItemStatus.Models;
    using Agility.RBS.ItemManagement.ItemStatus.Repository;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Models;
    using DocumentFormat.OpenXml.Packaging;
    using DocumentFormat.OpenXml.Spreadsheet;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    [Route("api/[controller]/[action]")]
    public class ItemReactiveController : Controller
    {
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly ItemStatusRepository itemStatusRepository;
        private readonly InboxModel inboxModel;
        private readonly InboxRepository inboxRepository;
        private readonly ItemSelectRepository itemSelectRepository;
        private readonly DomainDataRepository domainDataRepository;
        private ServiceDocument<ItemReactivateHeadModel> serviceDocument;

        public ItemReactiveController(
            ServiceDocument<ItemReactivateHeadModel> serviceDocument,
            DomainDataRepository domainDataRepository,
            ItemStatusRepository itemStatusRepository,
            ItemSelectRepository itemSelectRepository,
            InboxRepository inboxRepository)
        {
            this.serviceDocument = serviceDocument;
            this.itemStatusRepository = itemStatusRepository;
            this.inboxRepository = inboxRepository;
            this.itemSelectRepository = itemSelectRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
            this.inboxModel = new InboxModel { Operation = "CHGS", ModuleName = "ITEMREACTIVE" };
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in the Bulk Item List page.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        public async Task<ServiceDocument<ItemReactivateHeadModel>> List()
        {
            this.serviceDocument.DomainData.Add("workflowStatusList", this.domainDataRepository.WorkflowStatusGet(RbCommonConstants.ItemReactiveModule).Result.OrderBy(x => x.Name));
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in Bulk Item Head page when Add button clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<ItemReactivateHeadModel>> New()
        {
            this.serviceDocument.New(false);
            return this.serviceDocument;
        }

        [HttpGet]
        public async Task<ServiceDocument<ItemReactivateHeadModel>> Open(int id)
        {
            this.itemStatusRepository.ReActId = id;
            await this.serviceDocument.OpenAsync(this.CallBackOpen);
            this.serviceDocument.DomainData.Add("supplier", this.domainDataRepository.SupplierDomainGet(this.serviceDocument.DataProfile.DataModel.SupplierId.Value).Result);
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for submitting cost change to next level when Submit button clicked.
        /// </summary>
        /// <param name="serviceDocument">Service Document</param>
        /// <returns>ServiceDocument</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<ItemReactivateHeadModel>> Submit([FromBody]ServiceDocument<ItemReactivateHeadModel> serviceDocument)
        {
            this.inboxModel.StateID = RbsServiceDocCustomizationHelper.GetStateIdDotNetToOracle(this.serviceDocument);
            this.serviceDocument = serviceDocument;
            if (this.serviceDocument.DataProfile.DataModel.Id == 0 || this.serviceDocument.DataProfile.DataModel.Id == null)
            {
                this.inboxModel.Operation = "DFT";
            }

            await this.serviceDocument.TransitAsync(this.ItemSave);
            var stateName = this.serviceDocument.DataProfile.DataModel.WorkflowInstance.StateName;
            if (stateName == "Request Completed" || stateName == "RequestCompleted")
            {
                await this.itemStatusRepository.ItemStatusChanging(this.serviceDocument.DataProfile.DataModel.Id.Value, "ITEMREACTIVATE");
            }

            await this.inboxRepository.SetInbox(this.inboxModel, this.serviceDocument);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<ItemReactivateHeadModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.ItemReactiveHeadSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<ItemReactivateHeadModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.ItemSave);
            return this.serviceDocument;
        }

        [HttpGet]
        public System.Threading.Tasks.Task<FileResult> OpenItemReactivationExcel(int id)
        {
            return this.DownloadItemReactiveExcel(id);
        }

        public async System.Threading.Tasks.Task<FileResult> DownloadItemReactiveExcel(int id)
        {
            ////var user = Core.Utility.UserProfileHelper.GetInMemoryUser();
            ////var companyid = this.HttpContext.Request.Headers["companyid"];
            ////var jwtTokenTuple = System.Tuple.Create<string, string, string>(this.HttpContext.Session.Id, companyid, Core.JwtTokenHelper.GetToken(user.UserId, user.UserName));
            FilePathModel file = this.domainDataRepository.FilePathGet(this.inboxModel.ModuleName).Result;
            string xlsm = $"{RbSettings.RbsExcelFileDownloadPath}\\{Guid.NewGuid().ToString()}.xlsm";
            string itemReactiveFile = $"{RbSettings.RbsExcelFileDownloadPath}\\ItemReactivation-{id}.xlsm";
            string itemReactiveFileArc = $"{file.ArchivePath}\\ItemReactivation-{id}.xlsm";
            string itemReactiveFileProcess = $"{file.ProcessPath}\\ItemReactivation-{id}.xlsm";

            if (System.IO.File.Exists(itemReactiveFile))
            {
                System.IO.File.Copy(itemReactiveFile, xlsm);
            }
            else if (System.IO.File.Exists(itemReactiveFileProcess))
            {
                System.IO.File.Copy(itemReactiveFileProcess, xlsm);
            }
            else if (System.IO.File.Exists(itemReactiveFileArc))
            {
                System.IO.File.Copy(itemReactiveFileArc, xlsm);
            }
            else
            {
                ItemReactivateHeadModel head = await this.itemStatusRepository.OpenReactivatedItem(id, this.serviceDocumentResult);
                string templateFile = head.SupplierType.ToUpper() == "LOCAL" ? $"{file.TemplatePath}\\Local Item Reactivation Form.xlsm" : $"{file.TemplatePath}\\Foreign Item Reactivation Form.xlsm";
                System.IO.File.Copy(templateFile, xlsm);
                this.ItemReactiveBuilder(xlsm, head);
            }

            return await DocumentsHelper.GetFilestream(xlsm, this);
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<bool> FileUpload([FromBody]FileDataUpload file)
        {
            return await DocumentsRepository.CopyingFile(file);
        }

        [HttpGet]
        public async Task<List<ReactiveItemSelectModel>> ReactiveItemsList(string itemNo, int supplier)
        {
            List<ReactiveItemSelectModel> closeItemList = null;

            try
            {
                closeItemList = await this.itemSelectRepository.ReactiveItemList(itemNo, supplier, this.serviceDocumentResult);

                if (closeItemList == null)
                {
                    this.serviceDocument.Result = this.serviceDocumentResult;
                    closeItemList = new List<ReactiveItemSelectModel> { new ReactiveItemSelectModel { ItemException = this.serviceDocument.Result.InnerException } };
                }
                else
                {
                    return closeItemList;
                }
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
            }

            return closeItemList;
        }

        private async Task<ItemReactivateHeadModel> CallBackOpen()
        {
            try
            {
                FilePathModel file = this.domainDataRepository.FilePathGet(this.inboxModel.ModuleName).Result;
                var head = await this.itemStatusRepository.OpenReactivatedItem(this.itemStatusRepository.ReActId, file, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return head;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> ItemSave()
        {
            try
            {
                this.serviceDocument.Result = await this.itemStatusRepository.SaveReactivateItemAsync(this.serviceDocument.DataProfile.DataModel);
                if (this.serviceDocument.Result.Type != MessageType.Success)
                {
                    return false;
                }

                this.serviceDocument.DataProfile.DataModel.Id = this.itemStatusRepository.ReActId;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
            }

            return true;
        }

        private async Task<List<ItemReactivateHeadModel>> ItemReactiveHeadSearch()
        {
            try
            {
                var list = await this.itemStatusRepository.SeachReactivateItems(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return list;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private void ItemReactiveBuilder(string xlsm, ItemReactivateHeadModel head)
        {
            using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(xlsm, true))
            {
                WorkbookPart workbookPart1 = spreadSheet.WorkbookPart;
                Sheet sheet1 = workbookPart1.Workbook.Descendants<Sheet>().First(s => s.Name == "ItemActivation");

                WorksheetPart worksheetPart1 = workbookPart1.GetPartById(sheet1.Id.Value) as WorksheetPart;
                Worksheet reactiveItemworksheet = worksheetPart1.Worksheet;
                try
                {
                    ////Jwt Token
                    ////ExcelHelper.WriteToCellForString("AS", 14, costworksheet, jwtTokenTuple.Item1, "String");
                    ////ExcelHelper.WriteToCellForString("AS", 15, costworksheet, jwtTokenTuple.Item1, "String");
                    ////ExcelHelper.WriteToCellForString("AS", 16, costworksheet, jwtTokenTuple.Item1, "String");
                    ExcelHelper.WriteToCell<long?>("D", 7, reactiveItemworksheet, head.Id);
                    ExcelHelper.WriteToCell<string>("D", 8, reactiveItemworksheet, head.CreatedDate?.ToString("dd/MM/yyyy"));
                    ExcelHelper.WriteToCell<int>("D", 9, reactiveItemworksheet, Convert.ToInt32(head.SupplierId));
                    ExcelHelper.WriteToCell<string>("D", 10, reactiveItemworksheet, head.SupplierName);
                    if (head.SupplierType == "Foreign")
                    {
                        ExcelHelper.WriteToCell<string>("D", 11, reactiveItemworksheet, head.OriginCountryName);
                        ExcelHelper.WriteToCell<string>("D", 12, reactiveItemworksheet, head.IncotermsDesc);
                        ExcelHelper.WriteToCell<string>("D", 13, reactiveItemworksheet, head.CurrencyCode);
                        ExcelHelper.WriteToCell<int>("D", 14, reactiveItemworksheet, Convert.ToInt32(head.TotalRebates));
                    }
                    else
                    {
                        ExcelHelper.WriteToCell<int>("D", 11, reactiveItemworksheet, Convert.ToInt32(head.TotalRebates));
                    }

                    reactiveItemworksheet.Save();

                    Sheet sheetTempData = workbookPart1.Workbook.Descendants<Sheet>().FirstOrDefault(s => s.Name == "TempData");
                    WorksheetPart worksheetPartTempData = workbookPart1.GetPartById(sheetTempData.Id.Value) as WorksheetPart;
                    Worksheet bulkworksheetTempData = worksheetPartTempData.Worksheet;
                    ExcelHelper.WriteToCell<string>("A", 1, bulkworksheetTempData, RbSettings.Web);
                    bulkworksheetTempData.Save();
                }
                catch (Exception ex)
                {
                    this.serviceDocument.Result = new ServiceDocumentResult
                    {
                        InnerException = "Exception",
                        StackTrace = ex.ToString(),
                        Type = MessageType.Exception
                    };
                }
            }
        }
    }
}
