﻿// <copyright file="ItemCloseController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.ItemStatus
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Constants;
    using Agility.RBS.Core.Models;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.Documents;
    using Agility.RBS.Inbox.Common;
    using Agility.RBS.Inbox.Models;
    using Agility.RBS.Inbox.Repository;
    using Agility.RBS.ItemManagement.Common;
    using Agility.RBS.ItemManagement.Item.Repositories;
    using Agility.RBS.ItemManagement.ItemStatus.Models;
    using Agility.RBS.ItemManagement.ItemStatus.Repository;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Models;
    using DocumentFormat.OpenXml.Packaging;
    using DocumentFormat.OpenXml.Spreadsheet;
    using ExcelDataReader;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    [Route("api/[controller]/[action]")]
    public class ItemCloseController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly ItemStatusRepository itemStatusRepository;
        private readonly InboxModel inboxModel;
        private readonly InboxRepository inboxRepository;
        private readonly ItemSelectRepository itemSelectRepository;
        private ServiceDocument<ItemCloseHeadModel> serviceDocument;

        public ItemCloseController(
            ServiceDocument<ItemCloseHeadModel> serviceDocument,
            DomainDataRepository domainDataRepository,
            ItemStatusRepository itemStatusRepository,
            ItemSelectRepository itemSelectRepository,
            InboxRepository inboxRepository)
        {
            this.serviceDocument = serviceDocument;
            this.domainDataRepository = domainDataRepository;
            this.itemStatusRepository = itemStatusRepository;
            this.inboxRepository = inboxRepository;
            this.itemSelectRepository = itemSelectRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
            this.inboxModel = new InboxModel { Operation = "CHGS", ModuleName = "ITEMCLOSE" };
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in the Bulk Item List page.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        public async Task<ServiceDocument<ItemCloseHeadModel>> List()
        {
            this.serviceDocument.DomainData.Add("workflowStatusList", this.domainDataRepository.WorkflowStatusGet(RbCommonConstants.ItemCloseModule).Result.OrderBy(x => x.Name));
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in Bulk Item Head page when Add button clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public ServiceDocument<ItemCloseHeadModel> New()
        {
            this.serviceDocument.New(false);
            return this.serviceDocument;
        }

        [HttpGet]
        public async Task<ServiceDocument<ItemCloseHeadModel>> Open(int id)
        {
            this.itemStatusRepository.Id = id;
            await this.serviceDocument.OpenAsync(this.CallBackOpen);
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for submitting cost change to next level when Submit button clicked.
        /// </summary>
        /// <param name="serviceDocument">Service Document</param>
        /// <returns>ServiceDocument</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<ItemCloseHeadModel>> Submit([FromBody]ServiceDocument<ItemCloseHeadModel> serviceDocument)
        {
            this.inboxModel.StateID = RbsServiceDocCustomizationHelper.GetStateIdDotNetToOracle(this.serviceDocument);
            this.serviceDocument = serviceDocument;
            if (this.serviceDocument.DataProfile.DataModel.Id == 0 || this.serviceDocument.DataProfile.DataModel.Id == null)
            {
                this.inboxModel.Operation = "DFT";
            }

            await this.serviceDocument.TransitAsync(this.ItemSave);
            var stateName = this.serviceDocument.DataProfile.DataModel.WorkflowInstance.StateName;
            if (stateName == "Request Completed" || stateName == "RequestCompleted")
            {
                await this.itemStatusRepository.ItemStatusChanging(this.serviceDocument.DataProfile.DataModel.Id.Value, "ITEMCLOSE");
            }

            await this.inboxRepository.SetInbox(this.inboxModel, this.serviceDocument);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<ItemCloseHeadModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.ItemCloseHeadSearch);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<ItemCloseHeadModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.ItemSave);
            return this.serviceDocument;
        }

        public System.Threading.Tasks.Task<FileResult> OpenItemCloseExcel(int id, string reason)
        {
            return this.DownloadItemCloseExcel(id, reason);
        }

        [HttpGet]
        public async Task<List<ItemCloseModel>> CloseItemsList(string itemNo)
        {
            List<ItemCloseModel> closeItemList = null;

            try
            {
                closeItemList = await this.itemSelectRepository.ClosedItemList(itemNo, this.serviceDocumentResult);

                if (closeItemList == null)
                {
                    this.serviceDocument.Result = this.serviceDocumentResult;
                    closeItemList = new List<ItemCloseModel> { new ItemCloseModel { ItemException = this.serviceDocument.Result.InnerException } };
                }
                else
                {
                    return closeItemList;
                }
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
            }

            return closeItemList;
        }

        public async System.Threading.Tasks.Task<FileResult> DownloadItemCloseExcel(int id, string reason)
        {
            var user = Core.Utility.UserProfileHelper.GetInMemoryUser();
            var companyid = this.HttpContext.Request.Headers["companyid"];
            var jwtTokenTuple = System.Tuple.Create<string, string, string>(this.HttpContext.Session.Id, companyid, Core.JwtTokenHelper.GetToken(user.UserId, user.UserName));
            FilePathModel file = this.domainDataRepository.FilePathGet(this.inboxModel.ModuleName).Result;
            string xlsm = $"{RbSettings.RbsExcelFileDownloadPath}\\{Guid.NewGuid().ToString()}.xlsm";
            string itemReactiveFile = $"{RbSettings.RbsExcelFileDownloadPath}\\Item Close-Out Form-{id}.xlsm";
            string itemReactiveFileArc = $"{file.ArchivePath}\\Item Close-Out Form-{id}.xlsm";
            string itemReactiveFileProcess = $"{file.ProcessPath}\\Item Close-Out Form-{id}.xlsm";

            if (System.IO.File.Exists(itemReactiveFile))
            {
                System.IO.File.Copy(itemReactiveFile, xlsm);
            }
            else if (System.IO.File.Exists(itemReactiveFileProcess))
            {
                System.IO.File.Copy(itemReactiveFileProcess, xlsm);
            }
            else if (System.IO.File.Exists(itemReactiveFileArc))
            {
                System.IO.File.Copy(itemReactiveFileArc, xlsm);
            }
            else
            {
                ////var head = await this.itemStatusRepository.ClosedItemGet(id, this.serviceDocumentResult);
                System.IO.File.Copy(file.TemplatePath, xlsm);
                using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(xlsm, true))
                {
                    WorkbookPart workbookPart1 = spreadSheet.WorkbookPart;
                    Sheet sheet1 = workbookPart1.Workbook.Descendants<Sheet>().First(s => s.Name == "Close Out");
                    WorksheetPart worksheetPart1 = workbookPart1.GetPartById(sheet1.Id.Value) as WorksheetPart;
                    Worksheet costworksheet = worksheetPart1.Worksheet;
                    try
                    {
                        ////Jwt Token
                        ExcelHelper.WriteToCell<string>("AS", 14, costworksheet, jwtTokenTuple.Item1);
                        ExcelHelper.WriteToCell<string>("AS", 15, costworksheet, jwtTokenTuple.Item1);
                        ExcelHelper.WriteToCell<string>("AS", 16, costworksheet, jwtTokenTuple.Item1);

                        ////Header Row
                        ExcelHelper.WriteToCell<int>("F", 6, costworksheet, Convert.ToInt32(id));
                        ExcelHelper.WriteToCell<string>("F", 7, costworksheet, reason);
                        costworksheet.Save();
                        Sheet sheetTempData = workbookPart1.Workbook.Descendants<Sheet>().FirstOrDefault(s => s.Name == "TempData");
                        WorksheetPart worksheetPartTempData = workbookPart1.GetPartById(sheetTempData.Id.Value) as WorksheetPart;
                        Worksheet itemCloseworksheetTempData = worksheetPartTempData.Worksheet;
                        ExcelHelper.WriteToCell<string>("A", 1, itemCloseworksheetTempData, RbSettings.Web);
                        itemCloseworksheetTempData.Save();
                    }
                    catch (Exception ex)
                    {
                        this.serviceDocument.Result = new ServiceDocumentResult
                        {
                            InnerException = "Exception",
                            StackTrace = ex.ToString(),
                            Type = MessageType.Exception
                        };
                    }
                }
            }

            return await DocumentsHelper.GetFilestream(xlsm, this);
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<bool> FileUpload([FromBody]FileDataUpload file)
        {
            return await DocumentsRepository.CopyingFile(file);
        }

        private async Task<bool> ItemSave()
        {
            try
            {
                this.serviceDocument.Result = await this.itemStatusRepository.CloseItemAsync(this.serviceDocument.DataProfile.DataModel);
                if (this.serviceDocument.Result.Type != MessageType.Success)
                {
                    return false;
                }

                this.serviceDocument.DataProfile.DataModel.Id = this.itemStatusRepository.Id;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
            }

            return true;
        }

        private async Task<ItemCloseHeadModel> CallBackOpen()
        {
            try
            {
                FilePathModel file = this.domainDataRepository.FilePathGet(this.inboxModel.ModuleName).Result;
                var head = await this.itemStatusRepository.ClosedItemGet(this.itemStatusRepository.Id, file, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return head;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<List<ItemCloseHeadModel>> ItemCloseHeadSearch()
        {
            try
            {
                var list = await this.itemStatusRepository.ClosedItemAGet(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return list;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }
    }
}
