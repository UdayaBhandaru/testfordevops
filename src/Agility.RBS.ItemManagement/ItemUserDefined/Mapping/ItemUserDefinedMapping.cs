﻿// <copyright file="ItemUserDefinedMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.ItemUserDefined.Mapping
{
    using Agility.RBS.Core;
    using Agility.RBS.ItemManagement.ItemUserDefined.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class ItemUserDefinedMapping : Profile
    {
        public ItemUserDefinedMapping()
            : base("ItemUserDefinedMapping")
        {
            this.CreateMap<OracleObject, ItemUdaModel>()
                .ForMember(m => m.UdaId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["UDA_ID"])))
                .ForMember(m => m.UdaDesc, opt => opt.MapFrom(r => r["UDA_DESC"]))
                .ForMember(m => m.Module, opt => opt.MapFrom(r => r["MODULE"]))
                .ForMember(m => m.DisplayType, opt => opt.MapFrom(r => r["DISPLAY_TYPE"]))
                .ForMember(m => m.DisplayTypeDesc, opt => opt.MapFrom(r => r["DISPLAY_TYPE_DESC"]))
                .ForMember(m => m.DataType, opt => opt.MapFrom(r => r["DATA_TYPE"]))
                .ForMember(m => m.DataTypeDesc, opt => opt.MapFrom(r => r["DATA_TYPE_DESC"]))
                .ForMember(m => m.DataLength, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DATA_LENGTH"])))
                .ForMember(m => m.SingleValueInd, opt => opt.MapFrom(r => r["SINGLE_VALUE_IND"]))
                .ForMember(m => m.FilterOrgId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["FILTER_ORG_ID"])))
                .ForMember(m => m.FilterOrgIdDesc, opt => opt.MapFrom(r => r["FILTER_ORG_ID_DESC"]))
                .ForMember(m => m.FilterMerchId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["FILTER_MERCH_ID"])))
                .ForMember(m => m.FilterMerchIdDesc, opt => opt.MapFrom(r => r["FILTER_MERCH_ID_DESC"]))
                .ForMember(m => m.FilterMerchIdClass, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["FILTER_MERCH_ID_CLASS"])))
                .ForMember(m => m.FilterMerchIdClassDesc, opt => opt.MapFrom(r => r["FILTER_MERCH_ID_CLASS_DESC"]))
                .ForMember(m => m.FilterMerchIdSubclass, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["FILTER_MERCH_ID_SUBCLASS"])))
                .ForMember(m => m.FilterMerchIdSubclassDesc, opt => opt.MapFrom(r => r["FILTER_MERCH_ID_SUBCLASS_DESC"]))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, ItemUdaValueModel>()
                .ForMember(m => m.UdaId, opt => opt.MapFrom(r => r["UDA_ID"]))
                .ForMember(m => m.UdaValue, opt => opt.MapFrom(r => r["UDA_VALUE"]))
                .ForMember(m => m.UdaValueDesc, opt => opt.MapFrom(r => r["UDA_VALUE_DESC"]))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));
        }
    }
}
