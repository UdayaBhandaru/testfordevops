﻿//-------------------------------------------------------------------------------------------------
// <copyright file="ItemUdaValueModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// ItemUdaValueModel
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.ItemManagement.ItemUserDefined.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class ItemUdaValueModel
    {
        public int? UdaId { get; set; }

        public int? UdaValue { get; set; }

        public string UdaValueDesc { get; set; }

        public string Operation { get; set; }
    }
}
