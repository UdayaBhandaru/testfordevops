﻿//-------------------------------------------------------------------------------------------------
// <copyright file="ItemUdaModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// ItemUdaModel
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.ItemManagement.ItemUserDefined.Models
{
    using System.Collections.Generic;
    using Agility.Framework.Core.Profile.Entities;

    public class ItemUdaModel : ProfileEntity
    {
        public ItemUdaModel()
        {
            this.ItemUdaValues = new List<ItemUdaValueModel>();
        }

        public List<ItemUdaValueModel> ItemUdaValues { get; private set; }

        public int? UdaId { get; set; }

        public string UdaDesc { get; set; }

        public string Module { get; set; }

        public string DisplayType { get; set; }

        public string DisplayTypeDesc { get; set; }

        public string DataType { get; set; }

        public string DataTypeDesc { get; set; }

        public int? DataLength { get; set; }

        public string SingleValueInd { get; set; }

        public int? FilterOrgId { get; set; }

        public string FilterOrgIdDesc { get; set; }

        public int? FilterMerchId { get; set; }

        public string FilterMerchIdDesc { get; set; }

        public int? FilterMerchIdClass { get; set; }

        public string FilterMerchIdClassDesc { get; set; }

        public int? FilterMerchIdSubclass { get; set; }

        public string FilterMerchIdSubclassDesc { get; set; }

        public string Operation { get; set; }
    }
}
