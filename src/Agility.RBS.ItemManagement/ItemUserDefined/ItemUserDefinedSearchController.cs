﻿//-------------------------------------------------------------------------------------------------
// <copyright file="ItemUserDefinedSearchController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UtilityDomainController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.ItemManagement.ItemUserDefined
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.ItemManagement.ItemUserDefined.Models;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    public class ItemUserDefinedSearchController : Controller
    {
        private readonly ServiceDocument<ItemUdaModel> serviceDocument;
        private readonly ItemUserDefinedRepository udaRepository;
        private readonly ILogger<ItemUserDefinedController> logger;
        private readonly ServiceDocumentResult serviceDocumentResult;

        public ItemUserDefinedSearchController(
            ServiceDocument<ItemUdaModel> serviceDocument,
            ItemUserDefinedRepository udaRepository,
            ILogger<ItemUserDefinedController> logger)
        {
            this.serviceDocument = serviceDocument;
            this.udaRepository = udaRepository;
            this.logger = logger;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public async Task<ServiceDocument<ItemUdaModel>> List()
        {
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<ItemUdaModel>> Search()
        {
            this.logger.LogInformation("Search Started");
            await this.serviceDocument.ToListAsync(this.ItemUdaSearch);
            this.logger.LogInformation("Search Completed");
            return this.serviceDocument;
        }

        private async Task<List<ItemUdaModel>> ItemUdaSearch()
        {
            try
            {
                var domains = await this.udaRepository.ItemUdaListGet(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return domains;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }
    }
}
