﻿//-------------------------------------------------------------------------------------------------
// <copyright file="ItemUserDefinedController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UtilityDomainController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.ItemManagement.ItemUserDefined
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.ItemManagement.Common;
    using Agility.RBS.ItemManagement.ItemUserDefined.Models;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    public class ItemUserDefinedController : Controller
    {
        private readonly ServiceDocument<ItemUdaModel> serviceDocument;
        private readonly ItemUserDefinedRepository udaRepository;
        private readonly ILogger<ItemUserDefinedController> logger;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly DomainDataRepository domainDataRepository;
        private int udaId;

        public ItemUserDefinedController(
            ServiceDocument<ItemUdaModel> serviceDocument,
            ItemUserDefinedRepository udaRepository,
            DomainDataRepository domainDataRepository,
            ILogger<ItemUserDefinedController> logger)
        {
            this.serviceDocument = serviceDocument;
            this.udaRepository = udaRepository;
            this.domainDataRepository = domainDataRepository;
            this.logger = logger;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in Bulk Item Head page when Add button clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<ItemUdaModel>> New()
        {
            return await this.ItemUdaDomainData();
        }

        /// <summary>
        /// This API invoked for populating fields in the Bulk Item Head page when Edit icon clicked.
        /// </summary>
        /// <param name="id">Cost Change Id</param>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<ItemUdaModel>> Open(int id)
        {
            this.logger.LogInformation("Search Started");
            this.udaId = id;
            await this.serviceDocument.OpenAsync(this.ItemUdaOpen);
            this.logger.LogInformation("Search Completed");
            return await this.ItemUdaDomainData();
        }

        public async Task<ServiceDocument<ItemUdaModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.ItemUdaSave);
            return this.serviceDocument;
        }

        private async Task<ItemUdaModel> ItemUdaOpen()
        {
            try
            {
                var transportation = await this.udaRepository.ItemUdaGet(this.udaId, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return transportation;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<ServiceDocument<ItemUdaModel>> ItemUdaDomainData()
        {
            this.serviceDocument.DomainData.Add("disptype", this.domainDataRepository.DomainDetailData("UDIS").Result);
            this.serviceDocument.DomainData.Add("datatype", this.domainDataRepository.DomainDetailData("UDDT").Result);
            this.serviceDocument.DomainData.Add("chain", this.domainDataRepository.ChainDomainGet().Result);
            this.serviceDocument.DomainData.Add("category", this.domainDataRepository.DepartmentDomainGet().Result);
            this.serviceDocument.DomainData.Add("indicator", this.domainDataRepository.DomainDetailData(ItemManagementConstants.DomainHdrYesNo).Result);
            return this.serviceDocument;
        }

        private async Task<bool> ItemUdaSave()
        {
            this.serviceDocument.Result = await this.udaRepository.ItemUdaSet(this.serviceDocument.DataProfile.DataModel);
            if (this.serviceDocument.Result.Type != MessageType.Success)
            {
                return false;
            }

            this.serviceDocument.DataProfile.DataModel.UdaId = this.udaRepository.UdaId;
            return true;
        }
    }
}
