﻿//-------------------------------------------------------------------------------------------------
// <copyright file="ItemUserDefinedRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// UtilityDomainRepository
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.ItemManagement.ItemUserDefined
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.ItemManagement.Common;
    using Agility.RBS.ItemManagement.ItemUserDefined.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class ItemUserDefinedRepository : BaseOraclePackage
    {
        public ItemUserDefinedRepository()
        {
            this.PackageName = ItemManagementConstants.GetNewItemPackage;
        }

        public int UdaId { get; private set; }

        public async Task<List<ItemUdaModel>> ItemUdaListGet(ItemUdaModel itemUdaModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(ItemManagementConstants.UdaTab, ItemManagementConstants.GetUdaList);
            OracleObject recordObject = this.SetParamsDomain(itemUdaModel, true);
            return await this.GetProcedure2<ItemUdaModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ItemUdaModel> ItemUdaGet(int udaId, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType(ItemManagementConstants.UdaRec);
            OracleObject recordObject = this.GetOracleObject(recordType);
            recordObject["UDA_ID"] = udaId;

            PackageParams packageParameter = this.GetPackageParams(ItemManagementConstants.UdaTab, ItemManagementConstants.GetUda);
            OracleTable pDomainMstTab = await this.GetProcedure(recordObject, packageParameter, serviceDocumentResult);

            var lstHeader = Mapper.Map<List<ItemUdaModel>>(pDomainMstTab);
            if (lstHeader != null)
            {
                for (int i = 0; i < lstHeader.Count; i++)
                {
                    OracleObject obj = (OracleObject)pDomainMstTab[i];
                    if (obj["UDA_VALUE"] != DBNull.Value)
                    {
                        OracleTable domainDetails = (Devart.Data.Oracle.OracleTable)obj["UDA_VALUE"];
                        if (domainDetails.Count > 0)
                        {
                            lstHeader[i].ItemUdaValues.AddRange(Mapper.Map<List<ItemUdaValueModel>>(domainDetails));
                        }
                    }
                }
            }

            if (lstHeader != null && lstHeader.Count > 0)
            {
                return lstHeader[0];
            }

            return null;
        }

        public async Task<ServiceDocumentResult> ItemUdaSet(ItemUdaModel domainHeaderModel)
        {
            PackageParams packageParameter = this.GetPackageParams(ItemManagementConstants.UdaTab, ItemManagementConstants.SetUda);
            OracleObject recordObject = this.SetParamsDomain(domainHeaderModel, false);
            var result = await this.SetProcedure(recordObject, packageParameter);
            if (this.ObjResult != null)
            {
                this.UdaId = Convert.ToInt32(this.ObjResult["UDA_ID"]);
            }

            return result;
        }

        public virtual void SetParamsDomainSearch(ItemUdaModel domainHeaderModel, OracleObject recordObject)
        {
            recordObject["UDA_ID"] = domainHeaderModel.UdaId;
            recordObject["UDA_DESC"] = domainHeaderModel.UdaDesc;
        }

        public virtual void SetParamsDomainSave(ItemUdaModel domainHeaderModel, OracleObject recordObject)
        {
            recordObject["MODULE"] = domainHeaderModel.Module;
            recordObject["DISPLAY_TYPE"] = domainHeaderModel.DisplayType;
            recordObject["DATA_TYPE"] = domainHeaderModel.DataType;
            recordObject["DATA_LENGTH"] = domainHeaderModel.DataLength;
            recordObject["SINGLE_VALUE_IND"] = domainHeaderModel.SingleValueInd;
            recordObject["FILTER_ORG_ID"] = domainHeaderModel.FilterOrgId;
            recordObject["FILTER_MERCH_ID"] = domainHeaderModel.FilterMerchId;
            recordObject["OPERATION"] = domainHeaderModel.Operation;
            if (domainHeaderModel.ItemUdaValues.Count > 0)
            {
                // Detail Object
                OracleType dtlTableType = this.GetObjectType("UDA_VALUE_TAB");
                OracleType dtlRecordType = this.GetObjectType("UDA_VALUE_REC");
                OracleTable pUtlDmnDtlTab = new OracleTable(dtlTableType);
                foreach (ItemUdaValueModel detail in domainHeaderModel.ItemUdaValues)
                {
                    var dtlRecordObject = this.GetOracleObject(dtlRecordType);
                    dtlRecordObject["UDA_ID"] = domainHeaderModel.UdaId;
                    dtlRecordObject["UDA_VALUE"] = detail.UdaValue;
                    dtlRecordObject["UDA_VALUE_DESC"] = detail.UdaValueDesc;
                    dtlRecordObject["OPERATION"] = detail.Operation;
                    pUtlDmnDtlTab.Add(dtlRecordObject);
                }

                recordObject["UDA_VALUE"] = pUtlDmnDtlTab;
            }
        }

        private OracleObject SetParamsDomain(ItemUdaModel domainHeaderModel, bool isSearch)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType(ItemManagementConstants.UdaRec);
            OracleObject recordObject = this.GetOracleObject(recordType);
            if (!isSearch)
            {
                this.SetParamsDomainSave(domainHeaderModel, recordObject);
            }

            this.SetParamsDomainSearch(domainHeaderModel, recordObject);

            return recordObject;
        }
    }
}