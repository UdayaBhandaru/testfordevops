﻿// <copyright file="WeeksDataModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.ItemSuper.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class WeeksDataModel
    {
        public int? WeekId { get; set; }

        public int? WeekVal { get; set; }
    }
}
