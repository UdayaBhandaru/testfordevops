﻿// <copyright file="WhsMovmntModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.ItemSuper.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class WhsMovmntModel
    {
        public int? FromLoc { get; set; }

        public int? ToLoc { get; set; }

        public string LocName { get; set; }

        public string Status { get; set; }

        public DateTime? CreateDate { get; set; }

        public DateTime? ApprovalDate { get; set; }

        public string TsfType { get; set; }

        public int? InvStatus { get; set; }

        public decimal? TsfPrice { get; set; }

        public decimal? TsfQty { get; set; }

        public decimal? FillQty { get; set; }

        public decimal? DistroQty { get; set; }

        public decimal? SelectedQty { get; set; }

        public decimal? CancelledQty { get; set; }

        public decimal? WhsPackSize { get; set; }

        public decimal? SuppPackSize { get; set; }

        public decimal? WhsStock { get; set; }

        public string OverridePackSize { get; set; }

        public int? CompPackSize { get; set; }
    }
}
