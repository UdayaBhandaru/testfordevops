﻿// <copyright file="DsdOrdersMovmntModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.ItemSuper.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class DsdOrdersMovmntModel
    {
        public DateTime? OrderDate { get; set; }

        public int? Supplier { get; set; }

        public string SupName { get; set; }

        public int? OrderNumber { get; set; }

        public int? Location { get; set; }

        public string LocName { get; set; }

        public decimal? OrderQty { get; set; }

        public decimal? PackSize { get; set; }

        public decimal? CostPrice { get; set; }

        public decimal? RetailPrice { get; set; }

        public decimal? MarginPct { get; set; }

        public decimal? TotalCost { get; set; }

        public decimal? TotalPrice { get; set; }
    }
}
