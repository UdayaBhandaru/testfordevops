﻿// <copyright file="ItemRangingSuperModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.ItemSuper.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class ItemRangingSuperModel
    {
        public ItemRangingSuperModel()
        {
            this.DataList = new List<ItemRangingModel>();
            this.DomainData = new Dictionary<string, object>();
        }

        public Dictionary<string, object> DomainData { get; private set; }

        public List<ItemRangingModel> DataList { get; private set; }

        public long? Item { get; set; }
    }
}
