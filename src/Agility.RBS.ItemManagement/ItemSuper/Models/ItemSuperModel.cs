﻿// <copyright file="ItemSuperModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.ItemSuper.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Core.Models;

    public class ItemSuperModel : ProfileEntity
    {
        public long? Item { get; set; }

        public string ItemType { get; set; }

        public string ItemTypeDesc { get; set; }

        public string ShortDescArabic { get; set; }

        public int? Supplier { get; set; }

        public string Vpn { get; set; }

        public string ItemBarcode { get; set; }

        public string DescLong { get; set; }

        public int? DivisionId { get; set; }

        public string DivisionDesc { get; set; }

        public int? DeptId { get; set; }

        public string DeptDesc { get; set; }

        public int? GroupId { get; set; }

        public string GroupDesc { get; set; }

        public int? ClassId { get; set; }

        public string ClassDesc { get; set; }

        public int? SubClassId { get; set; }

        public string SubClassDesc { get; set; }

        public long? ItemGrandparent { get; set; }

        public string Status { get; set; }

        public string ItemTier { get; set; }

        public string SellableUom { get; set; }

        public string PackInd { get; set; }

        public int? Brand { get; set; }

        public string OriginCountry { get; set; }

        public string OrderableInd { get; set; }

        public string SellableInd { get; set; }

        public string InventoryInd { get; set; }

        public string TableName { get; set; }

        public string SupplierName { get; set; }

        public string SupplierDesc
        {
            get
            {
                return this.SupplierName + "(" + this.Supplier + ")";
            }
        }

        public string BrandDesc { get; set; }

        public string ItemTierDesc { get; set; }

        public string SellableUomDesc { get; set; }

        public int? StartRow { get; set; }

        public int? EndRow { get; set; }

        public long? TotalRows { get; set; }

        public SortModel[] SortModel { get; set; }

        public string ExpiryFlag { get; set; }

        public int? ExpiryInbound { get; set; }

        public int? ExpiryOutbound { get; set; }
    }
}
