﻿// <copyright file="ItemOrderModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.ItemSuper.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class ItemOrderModel
    {
        public int? OrderNo { get; set; }

        public int? DivisionId { get; set; }

        public string DivisionName { get; set; }

        public DateTime? OrderedDate { get; set; }

        public DateTime? ReceivedDate { get; set; }

        public int? SupplierId { get; set; }

        public string SuppName { get; set; }

        public string Status { get; set; }

        public int? LineNo { get; set; }

        public decimal? Fob { get; set; }

        public decimal? ReceivedCost { get; set; }

        public int? ReceivedQty { get; set; }

        public decimal? DiscountPct { get; set; }

        public int? Received { get; set; }

        public int? LocationId { get; set; }

        public string LocName { get; set; }

        public int? ParentOrder { get; set; }

        public string SupplierDescription
        {
            get
            {
                return this.SuppName + "(" + this.SupplierId + ")";
            }
        }

        public string DivisionDescription
        {
            get
            {
                return this.DivisionName + "(" + this.DivisionId + ")";
            }
        }

        public string LocationDescription
        {
            get
            {
                return this.LocName + "(" + this.LocationId + ")";
            }
        }
    }
}
