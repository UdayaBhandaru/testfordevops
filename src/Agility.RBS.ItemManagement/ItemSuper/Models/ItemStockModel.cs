﻿// <copyright file="ItemStockModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.ItemSuper.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class ItemStockModel
    {
        public int? DivisionId { get; set; }

        public string DivisionName { get; set; }

        public string LocationName { get; set; }

        public int? LocationId { get; set; }

        public string LocationType { get; set; }

        public decimal? AvCost { get; set; }

        public decimal? UnitCost { get; set; }

        public decimal? StockOnHand { get; set; }

        public DateTime? SohUpdateDatetime { get; set; }

        public decimal? InTransitQty { get; set; }

        public decimal? PackCompIntran { get; set; }

        public decimal? PackCompSoh { get; set; }

        public decimal? TsfReservedQty { get; set; }

        public decimal? PackCompResv { get; set; }

        public decimal? TsfExpectedQty { get; set; }

        public decimal? PackCompExp { get; set; }

        public decimal? NonSellableQty { get; set; }

        public DateTime? CreateDatetime { get; set; }

        public DateTime? LastUpdateDatetime { get; set; }

        public string LastUpdateId { get; set; }

        public DateTime? FirstReceived { get; set; }

        public DateTime? LastReceived { get; set; }

        public decimal? QtyReceived { get; set; }

        public DateTime? FirstSold { get; set; }

        public DateTime? LastSold { get; set; }

        public decimal? QtySold { get; set; }

        public int? PrimarySupp { get; set; }

        public string PrimarySuppName { get; set; }

        public string PrimaryCntry { get; set; }

        public decimal? PackCompNonSellable { get; set; }

        public string LocationDescription
        {
            get
            {
                return this.LocationName + "(" + this.LocationId + ")";
            }
        }
    }
}
