﻿// <copyright file="ItemRangingModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.ItemSuper.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class ItemRangingModel
    {
        public int? LocationId { get; set; }

        public string LocName { get; set; }

        public string FormatCode { get; set; }

        public string SourceMethod { get; set; }

        public string LocationDescription
        {
            get
            {
                return this.LocName + "(" + this.LocationId + ")";
            }
        }
    }
}
