﻿// <copyright file="BarcodeModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.ItemSuper.Models
{
    using System;
    using System.Collections.Generic;
    using Agility.Framework.Core.Profile.Entities;

    public class BarcodeModel
    {
        public string ItemBarcode { get; set; }

        public string BarcodeType { get; set; }

        public string StandardUom { get; set; }

        public string ItemDesc { get; set; }

        public decimal CompQty { get; set; }
    }
}