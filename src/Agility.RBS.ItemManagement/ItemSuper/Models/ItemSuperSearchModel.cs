﻿// <copyright file="ItemSuperSearchModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.ItemSuper.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Core.Models;

    public class ItemSuperSearchModel : ProfileEntity, Core.IPaginationModel
    {
        public ItemSuperSearchModel()
        {
            this.SuperItemList = new List<ItemSuperModel>();
        }

        public string Item { get; set; }

        public string ItemType { get; set; }

        public string ItemTypeDesc { get; set; }

        public string ItemGrandparent { get; set; }

        public string PackInd { get; set; }

        public int? DivisionId { get; set; }

        public string DivisionDesc { get; set; }

        public int? DeptId { get; set; }

        public string DeptDesc { get; set; }

        public int? CategoryId { get; set; }

        public string CategoryDesc { get; set; }

        public int? ClassId { get; set; }

        public string ClassDesc { get; set; }

        public int? SubClassId { get; set; }

        public string SubclassDesc { get; set; }

        public string DescLong { get; set; }

        public int? BrandId { get; set; }

        public string SystemDesc { get; set; }

        public string Status { get; set; }

        public string StandardUom { get; set; }

        public string LangId { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string PError { get; set; }

        public string Operation { get; set; }

        public int? Supplier { get; set; }

        public string Sellable { get; set; }

        public string Orderable { get; set; }

        public string Inventory { get; set; }

        public string Vpn { get; set; }

        public string Barcode { get; set; }

        public string TableName { get; set; }

        public string SubComments { get; set; }

        public string ItemStatusDesc { get; set; }

        public int? StartRow { get; set; }

        public int? EndRow { get; set; }

        public long? TotalRows { get; set; }

        public SortModel[] SortModel { get; set; }

        public int? OrganizationId { get; set; }

        public string CreatedBy { get; set; }

        public string ModifiedBy { get; set; }

        public bool DeletedInd { get; set; }

        public DateTimeOffset? CreatedDate { get; set; }

        public DateTimeOffset? ModifiedDate { get; set; }

        public string ItemStatus { get; set; }

        public string ItemTier { get; set; }

        public string ShortDescArabic { get; set; }

        public List<ItemSuperModel> SuperItemList { get; private set; }
    }
}
