﻿// <copyright file="ItemPriceModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.ItemSuper.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class ItemPriceModel
    {
        public int? LocationId { get; set; }

        public string LocName { get; set; }

        public string Fob { get; set; }

        public string Disc { get; set; }

        public decimal? Rsp { get; set; }

        public decimal? RspMrg { get; set; }

        public decimal? Ssp { get; set; }

        public decimal? SspMrg { get; set; }

        public decimal? Psp { get; set; }

        public decimal? PspMrg { get; set; }

        public decimal? Sclsp { get; set; }

        public string Allowed { get; set; }

        public string Ranged { get; set; }

        public string DsdAllowed { get; set; }

        public string WhseAllowed { get; set; }

        public string VatCode { get; set; }

        public decimal? OrderOnHand { get; set; }

        public string Operation { get; set; }

        public string LocDescription
        {
            get
            {
                return this.LocName + "(" + this.LocationId + ")";
            }
        }
    }
}
