﻿// <copyright file="LocWiseWeeksDataModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.ItemSuper.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class LocWiseWeeksDataModel
    {
        public LocWiseWeeksDataModel()
        {
            this.ListOfWeeksData = new List<WeeksDataModel>();
        }

        public int? LocationId { get; set; }

        public string LocName { get; set; }

        public List<WeeksDataModel> ListOfWeeksData { get; private set; }
    }
}
