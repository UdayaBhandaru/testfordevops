﻿// <copyright file="WarehouseRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.ItemSuper.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.ItemManagement.Common;
    using Agility.RBS.ItemManagement.ItemSuper.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class WarehouseRepository : BaseOraclePackage
    {
        public WarehouseRepository()
        {
            this.PackageName = ItemManagementConstants.WhsMovmntPackage;
        }

        public async Task<List<WhsMovmntModel>> GetItemWarehouseMovement(string itemCode, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            OracleParameter parameter = new OracleParameter("P_ITEM", OracleDbType.Number)
            {
                Direction = System.Data.ParameterDirection.Input,
                Value = itemCode
            };
            parameters.Add(parameter);
            parameter = new OracleParameter(ItemManagementConstants.ItemWhsMovmntTbl, Devart.Data.Oracle.OracleDbType.Table)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                ObjectTypeName = ItemManagementConstants.ObjTypeWhsMovmnt,
            };
            parameters.Add(parameter);
            parameter = new OracleParameter("RESULT", Devart.Data.Oracle.OracleDbType.Boolean)
            {
                Direction = System.Data.ParameterDirection.ReturnValue
            };
            parameters.Add(parameter);
            PackageParams packageParameter = this.GetPackageParams(ItemManagementConstants.ObjTypeWhsMovmnt, ItemManagementConstants.GetProcWhsMovmnt);
            return await this.GetProcedure2<WhsMovmntModel>(null, packageParameter, serviceDocumentResult, parameters);
        }
    }
}
