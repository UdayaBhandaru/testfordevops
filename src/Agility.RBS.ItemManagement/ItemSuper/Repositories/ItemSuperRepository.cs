﻿// <copyright file="ItemSuperRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.ItemSuper.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Text;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Core.Models;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.ItemManagement.Common;
    using Agility.RBS.ItemManagement.Item.Models;
    using Agility.RBS.ItemManagement.ItemSuper.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class ItemSuperRepository : BaseOraclePackage
    {
        public ItemSuperRepository()
        {
            this.PackageName = ItemManagementConstants.GetItemPackage;
        }

        public async Task<List<ItemSuperModel>> ItemListGetWithSdoc(ServiceDocument<ItemSuperModel> serviceDoc, ServiceDocumentResult serviceDocumentResult)
        {
            ItemSuperModel itemSearchModel = serviceDoc.DataProfile.DataModel;
            PackageParams packageParameter = this.GetPackageParams(ItemManagementConstants.ItemSuperTab, ItemManagementConstants.GetProcItemSuperSearch);
            OracleObject recordObject = this.SetParamsItemList(itemSearchModel);
            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();
            OracleParameter parameter;
            parameter = new OracleParameter("P_START", OracleDbType.Number) { Direction = ParameterDirection.Input, Value = itemSearchModel.StartRow };
            parameters.Add(parameter);
            parameter = new OracleParameter("P_END", OracleDbType.Number) { Direction = ParameterDirection.Input, Value = itemSearchModel.EndRow };
            parameters.Add(parameter);
            parameter = new OracleParameter("P_total", OracleDbType.Number) { Direction = ParameterDirection.Output };
            parameters.Add(parameter);
            if (itemSearchModel.SortModel == null)
            {
                itemSearchModel.SortModel = new[] { new Core.Models.SortModel { ColId = null, Sort = null } };
            }

            parameter = new OracleParameter("P_SortColId", OracleDbType.VarChar) { Direction = ParameterDirection.Input, Value = itemSearchModel.SortModel[0].ColId };
            parameters.Add(parameter);
            parameters.Add(this.ParameterBuilder(UserProfileHelper.GetUserParameter()));
            parameter = new OracleParameter("P_SortOrder", OracleDbType.VarChar) { Direction = ParameterDirection.Input, Value = itemSearchModel.SortModel[0].Sort };
            parameters.Add(parameter);
            var items = await this.GetProcedure2<ItemSuperModel>(recordObject, packageParameter, serviceDocumentResult, parameters);
            itemSearchModel.TotalRows = (this.Parameters["p_total"].Value != null && this.Parameters["p_total"].Value != DBNull.Value) ? Convert.ToInt64(this.Parameters["p_total"].Value) : 1;
            serviceDoc.DataProfile.DataModel = itemSearchModel;
            return items;
        }

        public async Task<ItemSupplierCountryDimensionModel> GetItemSuppCountryDimension(
            string itemCode, string importCountryId, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            OracleObject recordObject = this.SetParamsItemSuppCountryDimension(itemCode, importCountryId);
            PackageParams packageParameter = this.GetPackageParams(ItemManagementConstants.ObjTypeItemSupplierCountryDimension, ItemManagementConstants.GetProcItemSupplierCountryDimension);
            var response = await this.GetProcedure2<ItemSupplierCountryDimensionModel>(recordObject, packageParameter, serviceDocumentResult);
            if (response != null && response.Count > 0)
            {
                return response[0];
            }

            return null;
        }

        public async Task<List<T>> GetItemRelatedData<T>(string itemId, ServiceDocumentResult serviceDocumentResult, string screenName)
        {
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            string objType = string.Empty;
            string tableType = string.Empty;
            string procedureName = string.Empty;
            switch (screenName)
            {
                case "ItemStock":
                    objType = ItemManagementConstants.ObjTypeItemStock;
                    tableType = ItemManagementConstants.ItemStockTbl;
                    procedureName = ItemManagementConstants.GetProcItemStock;
                    break;
                case "ItemOrders":
                    objType = ItemManagementConstants.ObjTypeItemOrders;
                    tableType = ItemManagementConstants.ItemOrdersTbl;
                    procedureName = ItemManagementConstants.GetProcItemOrders;
                    break;
                case "ItemRanging":
                    objType = ItemManagementConstants.ObjTypeItemRanging;
                    tableType = ItemManagementConstants.ItemRangingTbl;
                    procedureName = ItemManagementConstants.GetProcItemRanging;
                    break;
                default:
                    procedureName = string.Empty;
                    break;
            }

            this.Connection.Open();
            OracleParameter parameter = new OracleParameter("P_ITEM", OracleDbType.Number)
            {
                Direction = System.Data.ParameterDirection.Input,
                Value = itemId
            };
            parameters.Add(parameter);
            parameter = new OracleParameter(tableType, Devart.Data.Oracle.OracleDbType.Table)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                ObjectTypeName = objType,
            };
            parameters.Add(parameter);
            parameter = new OracleParameter("RESULT", Devart.Data.Oracle.OracleDbType.Boolean)
            {
                Direction = System.Data.ParameterDirection.ReturnValue
            };
            parameters.Add(parameter);
            PackageParams packageParameter = this.GetPackageParams(objType, procedureName);
            return await this.GetProcedure2<T>(null, packageParameter, serviceDocumentResult, parameters);
        }

        public async Task<List<LocWiseWeeksDataModel>> GetItemSales(string itemCode, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            OracleParameter parameter = new OracleParameter("P_ITEM", OracleDbType.Number)
            {
                Direction = System.Data.ParameterDirection.Input,
                Value = itemCode
            };
            parameters.Add(parameter);
            parameter = new OracleParameter(ItemManagementConstants.ItemSalesTbl, Devart.Data.Oracle.OracleDbType.Table)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                ObjectTypeName = ItemManagementConstants.ObjTypeItemSales,
            };
            parameters.Add(parameter);
            parameter = new OracleParameter("RESULT", Devart.Data.Oracle.OracleDbType.Boolean)
            {
                Direction = System.Data.ParameterDirection.ReturnValue
            };
            parameters.Add(parameter);
            PackageParams packageParameter = this.GetPackageParams(ItemManagementConstants.ObjTypeItemSales, ItemManagementConstants.GetProcItemSales);
            OracleTable pDomainMstTab = await this.GetProcedure(null, packageParameter, serviceDocumentResult, parameters);
            var lstHeader = Mapper.Map<List<LocWiseWeeksDataModel>>(pDomainMstTab);
            if (lstHeader != null)
            {
                for (int i = 0; i < lstHeader.Count; i++)
                {
                    OracleObject obj = (OracleObject)pDomainMstTab[i];
                    OracleTable domainDetails = (Devart.Data.Oracle.OracleTable)obj["WEEK_DATA"];
                    if (domainDetails.Count > 0)
                    {
                        lstHeader[i].ListOfWeeksData.AddRange(Mapper.Map<List<WeeksDataModel>>(domainDetails));
                    }
                }
            }

            return lstHeader;
        }

        public async Task<ServiceDocumentResult> SetItemRangingDetails(ItemRangingSuperModel itemRangingSuperModel)
        {
            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();

            PackageParams packageParameter = this.GetPackageParams(ItemManagementConstants.ObjTypeItemRanging, ItemManagementConstants.SetProcItemRanging);
            OracleParameter parameter;
            parameter = new OracleParameter("RESULT", Devart.Data.Oracle.OracleDbType.Boolean)
            {
                Direction = System.Data.ParameterDirection.ReturnValue
            };
            parameters.Add(parameter);
            parameter = new OracleParameter("P_ITEM", Devart.Data.Oracle.OracleDbType.VarChar)
            {
                Direction = System.Data.ParameterDirection.Input,
                Value = itemRangingSuperModel.Item
            };
            parameters.Add(parameter);
            parameter = new OracleParameter(packageParameter.ObjectTypeTable, Devart.Data.Oracle.OracleDbType.Table)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                ObjectTypeName = packageParameter.ObjectTypeName,
                Value = this.SetParamsItemRanging(itemRangingSuperModel.DataList)
            };
            parameters.Add(parameter);
            return await this.SetProcedure(null, packageParameter, parameters);
        }

        public virtual OracleObject SetParamsItemList(ItemSuperModel itemBasicInfoModel)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(ItemManagementConstants.RecordObjItemSuper, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["ITEM"]] = itemBasicInfoModel.Item;
            recordObject[recordType.Attributes["ITEM_TYPE"]] = itemBasicInfoModel.ItemType;
            recordObject[recordType.Attributes["DESC_LONG"]] = itemBasicInfoModel.DescLong;
            recordObject[recordType.Attributes["DIVISION_ID"]] = itemBasicInfoModel.DivisionId;
            recordObject[recordType.Attributes["GROUP_ID"]] = itemBasicInfoModel.GroupId;
            recordObject[recordType.Attributes["DEPT_ID"]] = itemBasicInfoModel.DeptId;
            recordObject[recordType.Attributes["CLASS_ID"]] = itemBasicInfoModel.ClassId;
            recordObject[recordType.Attributes["SUB_CLASS_ID"]] = itemBasicInfoModel.SubClassId;
            recordObject[recordType.Attributes["ITEM_GRANDPARENT"]] = itemBasicInfoModel.ItemGrandparent;
            recordObject[recordType.Attributes["SUPPLIER"]] = itemBasicInfoModel.Supplier;
            recordObject[recordType.Attributes["VPN"]] = itemBasicInfoModel.Vpn;
            recordObject[recordType.Attributes["ITEM_BARCODE"]] = itemBasicInfoModel.ItemBarcode;
            recordObject[recordType.Attributes["ORDERABLE_IND"]] = itemBasicInfoModel.OrderableInd;
            recordObject[recordType.Attributes["SELLABLE_IND"]] = itemBasicInfoModel.SellableInd;
            recordObject[recordType.Attributes["INVENTORY_IND"]] = itemBasicInfoModel.InventoryInd;
            recordObject[recordType.Attributes["ITEM_DESC_SECONDARY"]] = itemBasicInfoModel.ShortDescArabic;
            recordObject[recordType.Attributes["BRAND"]] = itemBasicInfoModel.Brand;
            recordObject[recordType.Attributes["ITEM_TIER"]] = itemBasicInfoModel.ItemTier;
            recordObject[recordType.Attributes["STATUS"]] = itemBasicInfoModel.Status;
            recordObject[recordType.Attributes["SELLABLE_UOM"]] = itemBasicInfoModel.SellableUom;
            recordObject[recordType.Attributes["PACK_IND"]] = itemBasicInfoModel.PackInd;
            return recordObject;
        }

        public virtual OracleObject SetParamsItemSuppCountryDimension(string itemCode, string importCountryId)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(ItemManagementConstants.RecordObjItemSupplierCountryDimension, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["ITEM"]] = itemCode;
            recordObject[recordType.Attributes["IMPORT_COUNTRY"]] = importCountryId;
            return recordObject;
        }

        public virtual OracleTable SetParamsItemRanging(List<ItemRangingModel> rangingDetailsData)
        {
            this.Connection.Open();
            OracleType itemComTableType = OracleType.GetObjectType(ItemManagementConstants.ObjTypeItemRanging, this.Connection);
            OracleType recordType = OracleType.GetObjectType(ItemManagementConstants.RecordObjItemRanging, this.Connection);
            OracleTable pItemRangingTab = new OracleTable(itemComTableType);
            foreach (ItemRangingModel itmRangingModel in rangingDetailsData)
            {
                OracleObject recordObject = new OracleObject(recordType);
                recordObject[recordType.Attributes["LOCATION_ID"]] = itmRangingModel.LocationId;
                recordObject[recordType.Attributes["LOC_NAME"]] = itmRangingModel.LocName;
                recordObject[recordType.Attributes["FORMAT_CODE"]] = itmRangingModel.FormatCode;
                recordObject[recordType.Attributes["SOURCE_METHOD"]] = itmRangingModel.SourceMethod;
                pItemRangingTab.Add(recordObject);
            }

            return pItemRangingTab;
        }

        public async Task<AjaxModel<List<BarcodeModel>>> BarcodeDetailsGet(string item)
        {
            this.Connection.Open();
            OracleObject recordObject = this.GetOracleObject(this.GetObjectType("BARCODE_REC"));
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            OracleParameter parameter = new OracleParameter("P_ITEM", OracleDbType.Number)
            {
                Direction = System.Data.ParameterDirection.Input,
                Value = item
            };
            parameters.Add(parameter);
            PackageParams packageParameter = this.GetPackageParams("BARCODE_TAB", "GETITEMBARCODE");
            return await this.GetProcedureAjaxModel<BarcodeModel>(recordObject, packageParameter, parameters);
        }
    }
}
