﻿// <copyright file="ItemSuperController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.ItemManagement.ItemSuper
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Models;
    using Agility.Framework.Core.Workflow.Entities;
    using Agility.Framework.Core.Workflow.Models;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Constants;
    using Agility.RBS.Core.Models;
    using Agility.RBS.Core.Services;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.Inbox.Common;
    using Agility.RBS.Inbox.Models;
    using Agility.RBS.Inbox.Repository;
    using Agility.RBS.ItemManagement.Common;
    using Agility.RBS.ItemManagement.Item.Models;
    using Agility.RBS.ItemManagement.Item.Repositories;
    using Agility.RBS.ItemManagement.ItemSuper.Models;
    using Agility.RBS.ItemManagement.ItemSuper.Repositories;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    [Route("api/[controller]/[action]")]
    public class ItemSuperController : Controller
    {
        private readonly ItemSuperRepository itemSuperRepository;
        private readonly ItemRepository itemRepository;
        private readonly DomainDataRepository domainDataRepository;
        private readonly WarehouseRepository warehouseRepository;
        private readonly DsdRepository dsdRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly ServiceDocument<ItemSuperModel> serviceDocument;

        public ItemSuperController(
            ServiceDocument<ItemSuperModel> serviceDocument,
            ItemSuperRepository itemSuperRepository,
            ItemRepository itemRepository,
            DomainDataRepository domainDataRepository,
            WarehouseRepository warehouseRepository,
            DsdRepository dsdRepository)
        {
            this.serviceDocument = serviceDocument;
            this.itemSuperRepository = itemSuperRepository;
            this.itemRepository = itemRepository;
            this.domainDataRepository = domainDataRepository;
            this.warehouseRepository = warehouseRepository;
            this.dsdRepository = dsdRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in the Item List page.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<ItemSuperModel>> List()
        {
            return await this.GetDomainData();
        }

        /// <summary>
        /// This API invoked for fetching Item records based on search criteria when Search button clicked.
        /// </summary>
        /// <returns>ServiceDocument</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<ItemSuperModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.SearchItem);
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for loading Dimensions popup in Product Tab of Item Super Screen
        /// </summary>
        /// <param name="itemCode">Item Code</param>
        /// <param name="importCountryId">Import Country Id</param>
        /// <returns>ItemSupplierCountryDimensionModel</returns>
        [HttpGet]
        public async Task<ItemSupplierCountryDimensionModel> GetItemDimensions(string itemCode, string importCountryId)
        {
            try
            {
                var itemDimensions = await this.itemSuperRepository.GetItemSuppCountryDimension(itemCode, importCountryId, this.serviceDocumentResult);
                this.serviceDocument.DomainData.Add("dimensionObjects", this.domainDataRepository.DomainDetailData(ItemManagementConstants.DomainHdrDimensionObject).Result.OrderBy(x => x.CodeSeq));
                ////itemDimensions.DomainData.Add("dimensionObjects", this.domainDataRepository.CommonData(ItemManagementConstants.DomainHdrDimensionObject).Result);
                ////itemDimensions.DomainData.Add("presentationMethods", this.domainDataRepository.CommonData(ItemManagementConstants.DomainHdrPresentationMethod).Result);
                this.serviceDocument.DomainData.Add("uomList", this.domainDataRepository.UomDomainGet().Result.OrderBy(x => x.Description));
                ////itemDimensions.DomainData.Add("DimensionUoms", this.domainDataRepository.FilteredUomsDomainGet(RbCommonConstants.UomClassDimension).Result);
                ////itemDimensions.DomainData.Add("WeightUoms", this.domainDataRepository.FilteredUomsDomainGet(RbCommonConstants.UomClassMass).Result);
                ////itemDimensions.DomainData.Add("VolumeUoms", this.domainDataRepository.FilteredUomsDomainGet(RbCommonConstants.UomClassVolume).Result);
                ////itemDimensions.DomainData.Add("tareTypes", this.domainDataRepository.CommonData(ItemManagementConstants.DomainHdrTareType).Result);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return itemDimensions;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        /// <summary>
        /// This API invoked for loading grid in Stock Tab of Item Super Screen
        /// </summary>
        /// <param name="itemNo">item Code</param>
        /// <returns>List Of ItemStockModel</returns>
        [HttpGet]
        public async Task<List<ItemStockModel>> GetItemStock(string itemNo)
        {
            try
            {
                var itemStock = await this.itemSuperRepository.GetItemRelatedData<ItemStockModel>(itemNo, this.serviceDocumentResult, "ItemStock");
                this.serviceDocument.Result = this.serviceDocumentResult;
                return itemStock;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        /// <summary>
        /// This API invoked for loading grid in Orders Tab of Item Super Screen
        /// </summary>
        /// <param name="itemNo">item Code</param>
        /// <returns>List Of ItemOrderModel</returns>
        [HttpGet]
        public async Task<List<ItemOrderModel>> GetItemOrders(string itemNo)
        {
            try
            {
                var itemOrders = await this.itemSuperRepository.GetItemRelatedData<ItemOrderModel>(itemNo, this.serviceDocumentResult, "ItemOrders");
                this.serviceDocument.Result = this.serviceDocumentResult;
                return itemOrders;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        /// <summary>
        /// This API invoked for loading Basic Details popup in Product Tab of Item Super Screen
        /// </summary>
        /// <param name="itemCode">item Code</param>
        /// <returns>ItemBasicInfoModel</returns>
        [HttpGet]
        public async Task<ItemBasicInfoModel> GetItemBasicDetails(string itemCode)
        {
            try
            {
                var itemBasicInfo = await this.itemRepository.ItemBasicInfoGet(itemCode, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return itemBasicInfo;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        /// <summary>
        /// This API invoked for loading grid in Sales Tab of Item Super Screen
        /// </summary>
        /// <param name="itemNo">item Code</param>
        /// <returns>List Of LocWiseWeeksDataModel</returns>
        [HttpGet]
        public async Task<List<LocWiseWeeksDataModel>> GetItemSales(string itemNo)
        {
            try
            {
                var itemSales = await this.itemSuperRepository.GetItemSales(itemNo, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return itemSales;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        /// <summary>
        /// This API invoked for loading grid in Warehouse Movement Tab of Item Super Screen
        /// </summary>
        /// <param name="itemNo">item Code</param>
        /// <returns>List Of WhsMovmntModel</returns>
        [HttpGet]
        public async Task<List<WhsMovmntModel>> GetItemWarehouseMovement(string itemNo)
        {
            try
            {
                var itemWhsMvmnt = await this.warehouseRepository.GetItemWarehouseMovement(itemNo, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return itemWhsMvmnt;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        /// <summary>
        /// This API invoked for loading grid in Dsd Movement Tab of Item Super Screen
        /// </summary>
        /// <param name="itemNo">item Code</param>
        /// <returns>List Of DsdOrdersMovmntModel</returns>
        [HttpGet]
        public async Task<List<DsdOrdersMovmntModel>> GetItemDsdMovement(string itemNo)
        {
            try
            {
                var itemDsdMvmnt = await this.dsdRepository.GetItemDsdMovement(itemNo, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return itemDsdMvmnt;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        [HttpGet]
        public async Task<List<BrandDomainModel>> BrandsGetName(string name)
        {
            return await this.domainDataRepository.BrandDomainGet(name);
        }

        [HttpPost]
        public async ValueTask<bool> SaveItemDimensions([FromBody]ItemSupplierCountryDimensionModel dimensionModel)
        {
            try
            {
                await this.itemRepository.SetItemSuppCountryDimension(dimensionModel, "ItemSuper");
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// This API invoked for loading Ranging Details popup in Product Tab of Item Super Screen
        /// </summary>
        /// <param name="itemCode">item Code</param>
        /// <returns>ItemRangingSuperModel</returns>
        [HttpGet]
        public async Task<ItemRangingSuperModel> GetItemRangingDetails(string itemCode)
        {
            try
            {
                ItemRangingSuperModel irsm = new ItemRangingSuperModel();
                var itemRangingDetails = await this.itemSuperRepository.GetItemRelatedData<ItemRangingModel>(itemCode, this.serviceDocumentResult, "ItemRanging");
                irsm.DataList.Clear();
                irsm.DataList.AddRange(itemRangingDetails);
                irsm.DomainData.Add("sourceMethods", this.domainDataRepository.DomainDetailData(ItemManagementConstants.DomainHdrSourceMethod).Result);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return irsm;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        [HttpPost]
        public async ValueTask<bool> SaveItemRangingDetails([FromBody]ItemRangingSuperModel itemRangingSuperModel)
        {
            try
            {
                await this.itemSuperRepository.SetItemRangingDetails(itemRangingSuperModel);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// This API invoked for loading Barcode Details popup in Item Super Screen
        /// </summary>
        /// <param name="itemNo">item</param>
        /// <returns>BarcodeModel</returns>
        [HttpGet]
        public async Task<AjaxModel<List<BarcodeModel>>> BarcodeDetailsGet(string itemNo)
        {
            try
            {
                return await this.itemSuperRepository.BarcodeDetailsGet(itemNo);
            }
            catch (Exception ex)
            {
                return new AjaxModel<List<BarcodeModel>> { Result = AjaxResult.Exception, Message = ex.Message, Model = null };
            }
        }

        private async Task<List<ItemSuperModel>> SearchItem()
        {
            try
            {
                var items = await this.itemSuperRepository.ItemListGetWithSdoc(this.serviceDocument, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return items;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<ServiceDocument<ItemSuperModel>> GetDomainData()
        {
            var itemModel = this.domainDataRepository.ItemDomainGet().Result;
            this.serviceDocument.DomainData.Add("itemType", this.domainDataRepository.DomainDetailData(ItemManagementConstants.DomainHdrItemtype).Result.OrderBy(x => x.CodeSeq));
            this.serviceDocument.DomainData.Add("itemStatus", this.domainDataRepository.DomainDetailData(ItemManagementConstants.DomainHdrItemstatus).Result.OrderBy(x => x.CodeSeq));
            this.serviceDocument.DomainData.Add("itemYesNo", this.domainDataRepository.DomainDetailData(ItemManagementConstants.DomainHdrYesNo).Result.OrderBy(x => x.CodeSeq));
            this.serviceDocument.DomainData.Add("workflowStatusList", this.domainDataRepository.WorkflowStatusGet(RbCommonConstants.ItemModule).Result.OrderBy(x => x.Name));
            this.serviceDocument.DomainData.Add("division", itemModel.Divisions.OrderBy(x => x.DivisionDescription));
            this.serviceDocument.DomainData.Add("group", itemModel.Groups.OrderBy(x => x.GroupName));
            this.serviceDocument.DomainData.Add("department", itemModel.Departments.OrderBy(x => x.DeptName));
            this.serviceDocument.DomainData.Add("class", itemModel.Classes.OrderBy(x => x.ClassName));
            ////this.serviceDocument.DomainData.Add("supplier", this.domainDataRepository.SupplierDomainGet().Result);
            this.serviceDocument.DomainData.Add("uomList", itemModel.Uoms.OrderBy(x => x.UomDescription));
            ////this.serviceDocument.DomainData.Add("brand", this.domainDataRepository.BrandDomainGet().Result);
            this.serviceDocument.DomainData.Add("itemtier", this.domainDataRepository.DomainDetailData(ItemManagementConstants.DomainHdrITier).Result);
            this.serviceDocument.DomainData.Add("country", this.domainDataRepository.CountryDomainGet().Result);
            this.serviceDocument.LocalizationData.AddData("Item");
            return this.serviceDocument;
        }
    }
}
