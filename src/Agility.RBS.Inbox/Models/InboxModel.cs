﻿// <copyright file="InboxModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Inbox.Models
{
    using System;
    using Agility.Framework.Core.Profile.Entities;

    public class InboxModel : ProfileEntity
    {
        public string SenderID { get; set; }

        public string SenderName { get; set; }

        public string RecipientID { get; set; }

        public string RecipientName { get; set; }

        public string DataProfileID { get; set; }

        public string ProfileInstanceID { get; set; }

        public string WorkFlowID { get; set; }

        public string WorkFlowInstanceID { get; set; }

        public DateTime? ReqDate { get; set; }

        public string DueDate { get; set; }

        public string DueDays { get; set; }

        public string Status { get; set; }

        public string ReqCreatedBy { get; set; }

        public string ReqCreatedByName { get; set; }

        public DateTime? ReqDateCreated { get; set; }

        public string ActionType { get; set; }

        public string RequestFor { get; set; }

        public string RequestForName { get; set; }

        public int? Amount { get; set; }

        public string Purpose { get; set; }

        public string StateID { get; set; }

        public string ToStateID { get; set; }

        public string RequestRead { get; set; }

        public string RequestReadBy { get; set; }

        public string IsSnoozed { get; set; }

        public string IsSnoozedBy { get; set; }

        public string ActivityCompleted { get; set; }

        public string WorkFlowCompleted { get; set; }

        public string IsDeclined { get; set; }

        public string ReqDocLoc { get; set; }

        public string ProxyIndicator { get; set; }

        public string ProxyUser { get; set; }

        public DateTime? LastModifiedOn { get; set; }

        public string LastModifiedBy { get; set; }

        public string Comments { get; set; }

        public string Operation { get; set; }

        public int? LangID { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Perror { get; set; }

        public string DateGroup { get; set; }

        public string TableName { get; set; }

        public string ActionName { get; internal set; }

        public string Subject { get; set; }

        public string UserPicPath { get; set; }

        public int? FileCount { get; set; }

        public string ModuleName { get; set; }

        public string TransitionClaim { get; set; }
    }
}
