﻿// <copyright file="InboxQuickViewModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Inbox.Models
{
    using System;
    using Agility.Framework.Core.Profile.Entities;

    public class InboxQuickViewModel : ProfileEntity
    {
        public string ColName { get; set; }

        public string ColValue { get; set; }
    }
}
