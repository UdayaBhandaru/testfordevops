﻿// <copyright file="WorkflowHistoryModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Inbox.Models
{
    using System;
    using Agility.Framework.Core.Profile.Entities;

    public class WorkflowHistoryModel : ProfileEntity
    {
        public string SenderName { get; set; }

        public string RecipientName { get; set; }

        public string ActionName { get; set; }

        public string ActionType { get; set; }

        public DateTime? ReqRecievedDate { get; set; }

        public string Comments { get; set; }

        public string InProgressDays { get; set; }

        public int InboxId { get; set; }

        public string UserPicPath { get; set; }

        public string Error { get; set; }
    }
}
