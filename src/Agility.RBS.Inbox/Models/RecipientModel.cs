﻿// <copyright file="RecipientModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Inbox.Models
{
    public class RecipientModel
    {
        public string RecipientID { get; set; }

        public string RecipientName { get; set; }

        public string StateId { get; set; }
    }
}