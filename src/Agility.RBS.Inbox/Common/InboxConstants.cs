﻿// <copyright file="InboxConstants.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
namespace Agility.RBS.Inbox.Common
{
    internal static class InboxConstants
    {
        // Result
        public const string OracleParameterResult = "RESULT";

        // Item Package
        public const string GetItemPackage = "ITEM_MASTER";

        // Domain Header Ids
        public const string DomainHdrItemstatus = "ITEMSTATUS";
        public const string DomainHdrItemtype = "ITEMTYPE";
        public const string DomainHdrSource = "SOURCE";
        public const string DomainHdrAutorepl = "AUTOREPL";
        public const string DomainHdrLabeltype = "LABELTYPE";
        public const string DomainHdrSkuclass = "SKUCLASS";
        public const string DomainHdrIndicator = "INDICATOR";
        public const string DomainHdrInventory = "INVENTORY";

        // Inbox
        public const string GetProcInbox = "GET_INBOX";
        public const string SetProcInbox = "set_Inbox";
        public const string ObjTypeNameInbox = "INBOX_TBL";
        public const string InboxTbl = "P_INBOX_TBL";

        public const string QuickViewTbl = "P_QUICKVEIW";
        public const string ObjTypeNameQuickView = "QUICKVIEW_TAB";
        public const string GetProcQuickView = "GET_INBOX_QUICKVIEW";

        public const string WorkFlowHistoryTbl = "P_GLOBALINBOX_HIST_TAB";
        public const string ObjTypeWorkFlowHistory = "GLOBALINBOX_HIST_TAB";
        public const string GetProcWorkflowHistory = "GETINBOXHISTORY";

        // Inbox package
        public const string GetInboxPackage = "UTL_Global_Inbox";
        public const string UsernameTab = "username_tab";
        public const string GetRecepientsProc = "getrecepients";
    }
}
