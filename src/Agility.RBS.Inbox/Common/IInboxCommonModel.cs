﻿// <copyright file="IInboxCommonModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Inbox.Common
{
    public interface IInboxCommonModel
    {
        Core.Models.RbWfHeaderModel WorkflowForm { get; set; }
    }
}
