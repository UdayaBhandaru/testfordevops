﻿// <copyright file="RbsServiceDocCustomizationHelper.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Inbox.Common
{
    using System;
    using System.Linq;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;

    public static class RbsServiceDocCustomizationHelper
    {
        public static string GetStateIdDotNetToOracle<T>(ServiceDocument<T> serviceDocument)
            where T : ProfileEntity
        {
            return GuidConverter.DotNetToOracle(Convert.ToString(serviceDocument.DataProfile.DataModel.WorkflowInstance?.WorkflowStateId));
        }

        public static string GetStateNameByProfileIdAndCurActionId<T>(ServiceDocument<T> serviceDocument)
            where T : ProfileEntity
        {
            if (string.IsNullOrWhiteSpace(serviceDocument.DataProfile.ActionService.CurrentActionId))
            {
                return null;
            }

            var result = ServiceDocumentHelper.GetWorkflowTransitions(serviceDocument.DataProfile.DataModel.WorkflowInstance.DataProfileId);
            var nextWfAction = result.FirstOrDefault(r => r.TransitionClaim == serviceDocument.DataProfile.ActionService.CurrentActionId);
            return nextWfAction?.WorkflowState?.StateName;
        }
    }
}
