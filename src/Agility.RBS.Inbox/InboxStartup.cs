﻿// <copyright file="InboxStartup.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Inbox
{
    using Agility.Framework.Core;
    using Agility.RBS.Inbox.Repository;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;

    public class InboxStartup : ComponentStartup<DbContext>
    {
        public override void ConfigureDependencyInjection(IServiceCollection services)
        {
            services.AddTransient<InboxRepository>();
        }

        public void Startup(Microsoft.Extensions.Configuration.IConfigurationRoot configuration)
        {
            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<Mapping.InboxMapping>();
                this.InitilizeMapping(cfg);
            });
        }
    }
}
