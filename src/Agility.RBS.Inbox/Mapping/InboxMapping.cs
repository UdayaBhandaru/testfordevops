﻿// <copyright file="InboxMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Inbox.Mapping
{
    using Agility.RBS.Core;
    using Agility.RBS.Inbox.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class InboxMapping : Profile
    {
        public InboxMapping()
            : base("InboxMapping")
        {
            this.CreateMap<OracleObject, InboxModel>()
                 .ForMember(m => m.SenderID, opt => opt.MapFrom(r => r["Sender_ID"]))
             .ForMember(m => m.SenderName, opt => opt.MapFrom(r => r["Sender_Name"]))
             .ForMember(m => m.RecipientID, opt => opt.MapFrom(r => r["Recipient_ID"]))
             .ForMember(m => m.RecipientName, opt => opt.MapFrom(r => r["Recipient_Name"]))
             .ForMember(m => m.DataProfileID, opt => opt.MapFrom(r => r["DataProfile_ID"]))
             .ForMember(m => m.ProfileInstanceID, opt => opt.MapFrom(r => r["ProfileInstance_ID"]))
             .ForMember(m => m.WorkFlowID, opt => opt.MapFrom(r => r["WorkFlow_ID"]))
             .ForMember(m => m.WorkFlowInstanceID, opt => opt.MapFrom(r => r["WorkFlowInstance_ID"]))
             .ForMember(m => m.ReqDate, opt => opt.MapFrom(r => r["Req_Date"]))
             .ForMember(m => m.DueDate, opt => opt.MapFrom(r => r["Due_Date"]))
             .ForMember(m => m.Status, opt => opt.MapFrom(r => r["Status"]))
             .ForMember(m => m.ReqCreatedBy, opt => opt.MapFrom(r => r["Req_CreatedBy"]))
             .ForMember(m => m.ReqCreatedByName, opt => opt.MapFrom(r => r["Req_CreatedByName"]))
             .ForMember(m => m.ReqDateCreated, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["Req_DateCreated"])))
             .ForMember(m => m.ActionType, opt => opt.MapFrom(r => r["ActionType"]))
             .ForMember(m => m.ActionName, opt => opt.MapFrom(r => r["Action_Name"]))
             .ForMember(m => m.DueDays, opt => opt.MapFrom(r => r["Due_Days"]))
             .ForMember(m => m.RequestFor, opt => opt.MapFrom(r => r["RequestFor"]))
             .ForMember(m => m.RequestForName, opt => opt.MapFrom(r => r["RequestForName"]))
             .ForMember(m => m.Amount, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["Amount"])))
             .ForMember(m => m.Purpose, opt => opt.MapFrom(r => r["Purpose"]))
             .ForMember(m => m.StateID, opt => opt.MapFrom(r => r["StateID"]))
             .ForMember(m => m.ToStateID, opt => opt.MapFrom(r => r["ToStateID"]))
             .ForMember(m => m.RequestRead, opt => opt.MapFrom(r => r["RequestRead"]))
             .ForMember(m => m.RequestReadBy, opt => opt.MapFrom(r => r["RequestReadBy"]))
             .ForMember(m => m.IsSnoozed, opt => opt.MapFrom(r => r["IsSnoozed"]))
             .ForMember(m => m.IsSnoozedBy, opt => opt.MapFrom(r => r["IsSnoozedBy"]))
             .ForMember(m => m.ActivityCompleted, opt => opt.MapFrom(r => r["ActivityCompleted"]))
             .ForMember(m => m.WorkFlowCompleted, opt => opt.MapFrom(r => r["WorkFlowCompleted"]))
             .ForMember(m => m.IsDeclined, opt => opt.MapFrom(r => r["IsDeclined"]))
             .ForMember(m => m.ReqDocLoc, opt => opt.MapFrom(r => r["Req_Doc_Loc"]))
             .ForMember(m => m.ProxyIndicator, opt => opt.MapFrom(r => r["ProxyIndicator"]))
             .ForMember(m => m.ProxyUser, opt => opt.MapFrom(r => r["ProxyUser"]))
             .ForMember(m => m.LastModifiedOn, opt => opt.MapFrom(r => r["Last_ModifiedOn"]))
             .ForMember(m => m.LastModifiedBy, opt => opt.MapFrom(r => r["Last_ModifiedBy"]))
             .ForMember(m => m.Comments, opt => opt.MapFrom(r => r["Comments"]))
             .ForMember(m => m.Operation, opt => opt.MapFrom(r => r["Operation"]))
             .ForMember(m => m.LangID, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["Lang_ID"])))
             .ForMember(m => m.ProgramPhase, opt => opt.MapFrom(r => r["Program_Phase"]))
             .ForMember(m => m.ProgramMessage, opt => opt.MapFrom(r => r["Program_Message"]))
             .ForMember(m => m.DateGroup, opt => opt.MapFrom(r => r["Date_Group"]))
             .ForMember(m => m.Perror, opt => opt.MapFrom(r => r["P_error"]))
             .ForMember(m => m.TableName, opt => opt.MapFrom(r => "INBOX_TBL"))
             .ForMember(m => m.Subject, opt => opt.MapFrom(r => r["subject"]))
             .ForMember(m => m.FileCount, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["FILE_CNT"])))
             .ForMember(m => m.UserPicPath, opt => opt.MapFrom(r => r["USER_PIC_URL"]))
             .ForMember(m => m.ModuleName, opt => opt.MapFrom(r => r["MODULE_NAME"]));

            this.CreateMap<OracleObject, InboxQuickViewModel>()
                .ForMember(m => m.ColName, opt => opt.MapFrom(r => r["Col_Name"]))
                .ForMember(m => m.ColValue, opt => opt.MapFrom(r => r["Col_Value"]));

            this.CreateMap<OracleObject, WorkflowHistoryModel>()
                .ForMember(m => m.ActionName, opt => opt.MapFrom(r => r["ACTION_NAME"]))
                .ForMember(m => m.ActionType, opt => opt.MapFrom(r => r["ACTION_TYPE"]))
                .ForMember(m => m.Comments, opt => opt.MapFrom(r => r["COMMENTS"]))
                .ForMember(m => m.RecipientName, opt => opt.MapFrom(r => r["RECIPIENTNAME"]))
                .ForMember(m => m.ReqRecievedDate, opt => opt.MapFrom(r => r["REQ_RECIEVED_DATE"]))
                .ForMember(m => m.SenderName, opt => opt.MapFrom(r => r["SENDERNAME"]))
                .ForMember(m => m.InProgressDays, opt => opt.MapFrom(r => r["IN_PROGRESS"]))
                 .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_Error"]))
                .ForMember(m => m.InboxId, opt => opt.MapFrom(r => r["GLOBALINBOXID"]))
                .ForMember(m => m.UserPicPath, opt => opt.MapFrom(r => r["USER_PIC"]));

            this.CreateMap<OracleObject, RecipientModel>()
                 .ForMember(m => m.RecipientID, opt => opt.MapFrom(r => r["id"]))
             .ForMember(m => m.RecipientName, opt => opt.MapFrom(r => r["username"]))
             .ForMember(m => m.StateId, opt => opt.MapFrom(r => r["stateid"]));
        }
    }
}
