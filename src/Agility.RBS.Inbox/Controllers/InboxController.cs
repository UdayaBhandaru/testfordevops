﻿// <copyright file="InboxController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Inbox
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Inbox.Models;
    using Agility.RBS.Inbox.Repository;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    public class InboxController : Controller
    {
        private readonly ServiceDocument<InboxModel> serviceDocument;
        private readonly InboxRepository workFlowRepository;
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly ServiceDocument<InboxQuickViewModel> serviceDocumentQuickView;
        private readonly ServiceDocument<WorkflowHistoryModel> serviceDocumentWorkflowHistory;
        private string tabType;
        private string module;
        private string dataProfileId;
        private string workFlowId;
        private string profileInstanceId;
        private string profileNameCode;
        private long profilePkValue;

        public InboxController(
            ServiceDocument<InboxModel> serviceDocument,
             InboxRepository workFlowRepository,
             ServiceDocument<InboxQuickViewModel> serviceDocumentQuickView,
             ServiceDocument<WorkflowHistoryModel> serviceDocumentWorkflowHistory,
             DomainDataRepository domainDataRepository)
        {
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
            this.workFlowRepository = workFlowRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocumentQuickView = serviceDocumentQuickView;
            this.serviceDocumentWorkflowHistory = serviceDocumentWorkflowHistory;
        }

        public async Task<ServiceDocument<InboxModel>> List(string inboxTabType, string module)
        {
            this.tabType = inboxTabType;
            this.module = module;
            this.serviceDocument.DomainData.Add("inboxCounts", this.domainDataRepository.GetAllInboxTabsCountFromDB(this.module).Result);
            await this.serviceDocument.ToListAsync(this.InboxList);
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<WorkflowHistoryModel>> GetHistory(string objectName, int objectValue)
        {
            this.profileNameCode = objectName;
            this.profilePkValue = objectValue;
            await this.serviceDocumentWorkflowHistory.ToListAsync(this.HistoryGet);
            return this.serviceDocumentWorkflowHistory;
        }

        public async Task<ServiceDocument<InboxQuickViewModel>> ListQuickView(string dataProfileId, string workFlowId, string profileInstanceId)
        {
            this.dataProfileId = dataProfileId;
            this.workFlowId = workFlowId;
            this.profileInstanceId = profileInstanceId;
            await this.serviceDocumentQuickView.ToListAsync(this.QuickViewGet);
            return this.serviceDocumentQuickView;
        }

        public async Task<List<RecipientModel>> RecipientsGet(string dataProfileId, string profileInstanceId, string workFlowId, string stateId, string actionType, string moduleName)
        {
            return await this.workFlowRepository.GetRecipients(dataProfileId, profileInstanceId, workFlowId, stateId, actionType, moduleName);
        }

        private async Task<List<InboxQuickViewModel>> QuickViewGet()
        {
            try
            {
                var quickViewDetails = await this.workFlowRepository.GetQuickView(this.dataProfileId, this.workFlowId, this.profileInstanceId, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return quickViewDetails;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<List<WorkflowHistoryModel>> HistoryGet()
        {
            try
            {
                List<WorkflowHistoryModel> historyList = await this.workFlowRepository.GetHistory(this.profileNameCode, this.profilePkValue, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return historyList;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<List<InboxModel>> InboxList()
        {
            try
            {
                var items = await this.workFlowRepository.GetInboxList(this.tabType, this.module, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return items;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }
    }
}
