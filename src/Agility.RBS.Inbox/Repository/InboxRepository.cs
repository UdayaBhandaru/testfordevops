﻿// <copyright file="InboxRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Inbox.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.Framework.Core.Workflow.Entities;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Models;
    using Agility.RBS.Core.Services;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.Inbox.Common;
    using Agility.RBS.Inbox.Models;
    using AutoMapper;
    using Devart.Data.Oracle;
    using Microsoft.AspNetCore.DataProtection;
    using Newtonsoft.Json;

    public class InboxRepository : BaseOraclePackage
    {
        private readonly RazorTemplateService razorTemplateService;
        private readonly IDataProtector protector;

        public InboxRepository(RazorTemplateService razorTemplateService, IDataProtectionProvider provider)
        {
            this.PackageName = InboxConstants.GetInboxPackage;
            this.razorTemplateService = razorTemplateService;
            this.protector = provider.CreateProtector(this.GetType().FullName);
        }

        public async Task<List<InboxQuickViewModel>> GetQuickView(string dataProfileId, string workFlowId, string profileInstanceId, ServiceDocumentResult serviceDocumentResult)
        {
            try
            {
                this.Connection.Open();
                OracleParameterCollection parameters;
                parameters = this.Parameters;
                parameters.Clear();
                OracleParameter parameter;
                parameter = new OracleParameter("RESULT", OracleDbType.Boolean, ParameterDirection.ReturnValue);
                parameters.Add(parameter);
                parameter = new OracleParameter("DATAPROFILE_ID", OracleDbType.VarChar)
                {
                    Direction = System.Data.ParameterDirection.Input,
                    Value = dataProfileId
                };
                parameters.Add(parameter);
                parameter = new OracleParameter("WORKFLOW_ID", OracleDbType.VarChar)
                {
                    Direction = System.Data.ParameterDirection.Input,
                    Value = workFlowId
                };
                parameters.Add(parameter);
                parameter = new OracleParameter("PROFILE_INSTANCE_ID", OracleDbType.VarChar)
                {
                    Direction = System.Data.ParameterDirection.Input,
                    Value = profileInstanceId
                };
                parameters.Add(parameter);
                parameter = new OracleParameter(InboxConstants.QuickViewTbl, Devart.Data.Oracle.OracleDbType.Table)
                {
                    Direction = System.Data.ParameterDirection.Output,
                    ObjectTypeName = InboxConstants.ObjTypeNameQuickView,
                };

                parameters.Add(parameter);
                PackageParams packageParameter = this.GetPackageParams("QUICKVEIW", InboxConstants.GetProcQuickView);
                return await this.GetProcedure2<InboxQuickViewModel>(null, packageParameter, serviceDocumentResult, parameters);
            }
            finally
            {
                this.Connection.Close();
            }
        }

        public async Task<List<WorkflowHistoryModel>> GetHistory(string profileCode, long profilePkValue, ServiceDocumentResult serviceDocumentResult)
        {
            try
            {
                this.Connection.Open();
                OracleParameterCollection parameters = this.Parameters;
                parameters.Clear();
                PackageParams packageParameter = this.GetPackageParams(InboxConstants.ObjTypeWorkFlowHistory, InboxConstants.GetProcWorkflowHistory);

                OracleParameter parameter = new OracleParameter("P_ITEM", OracleDbType.Number)
                {
                    Direction = System.Data.ParameterDirection.Input,
                    Value = profilePkValue
                };
                parameters.Add(parameter);
                parameter = new OracleParameter("P_MODULE_NAME", OracleDbType.VarChar)
                {
                    Direction = System.Data.ParameterDirection.Input,
                    Value = profileCode
                };
                parameters.Add(parameter);

                parameter = new OracleParameter(InboxConstants.WorkFlowHistoryTbl, Devart.Data.Oracle.OracleDbType.Table)
                {
                    Direction = System.Data.ParameterDirection.InputOutput,
                    ObjectTypeName = InboxConstants.ObjTypeWorkFlowHistory,
                };
                parameters.Add(parameter);
                parameter = new OracleParameter("RESULT", Devart.Data.Oracle.OracleDbType.Boolean)
                {
                    Direction = System.Data.ParameterDirection.ReturnValue
                };
                parameters.Add(parameter);
                return await this.GetProcedure2<WorkflowHistoryModel>(null, packageParameter, serviceDocumentResult, parameters);
            }
            finally
            {
                this.Connection.Close();
            }
        }

        public async Task<ServiceDocumentResult> SetInbox<T>(InboxModel inboxModel, ServiceDocument<T> serviceDocument)
            where T : ProfileEntity, IInboxCommonModel
        {
            try
            {
                if (serviceDocument.Result.Type == MessageType.Success)
                {
                    PackageParams packageParameter = this.GetPackageParams(InboxConstants.ObjTypeNameInbox, InboxConstants.SetProcInbox);
                    OracleObject recordObject = this.SetParamsInbox(inboxModel, serviceDocument);
                    serviceDocument.Result = await this.SetProcedure(recordObject, packageParameter);
                    await this.SendMail(inboxModel, serviceDocument);
                }
            }
            catch (Exception e)
            {
                serviceDocument.Result = new ServiceDocumentResult { StackTrace = e.StackTrace, Message = e.Message, Type = MessageType.Exception };
            }
            finally
            {
                this.Connection.Close();
            }

            return serviceDocument.Result;
        }

        public string GetStateId(string operation, IWorkFlowInstance workFlowInstance)
        {
            try
            {
                this.Connection.Open();
                OracleParameterCollection parameters;
                parameters = this.Parameters;
                parameters.Clear();
                OracleParameter parameter;
                parameter = new OracleParameter("RESULT", OracleDbType.VarChar, ParameterDirection.ReturnValue);
                parameters.Add(parameter);
                parameter = new OracleParameter("p_operation", OracleDbType.VarChar)
                {
                    Direction = System.Data.ParameterDirection.Input,
                    Value = operation
                };
                parameters.Add(parameter);
                parameter = new OracleParameter("p_stateid", OracleDbType.VarChar)
                {
                    Direction = System.Data.ParameterDirection.Input,
                    Value = DotNetToOracle(workFlowInstance.WorkflowStateId.ToString())
                };
                parameters.Add(parameter);

                parameter = new OracleParameter("p_profileinstanceid", OracleDbType.VarChar)
                {
                    Direction = System.Data.ParameterDirection.Input,
                    Value = workFlowInstance.ProfileInstanceId.ToString()
                };
                parameters.Add(parameter);
                parameter = new OracleParameter("p_workflowid", OracleDbType.VarChar)
                {
                    Direction = System.Data.ParameterDirection.Input,
                    Value = DotNetToOracle(workFlowInstance.DataProfileId.ToString())
                };
                parameters.Add(parameter);

                this.ExecuteProcedure("getStateid", parameters);

                return this.Parameters["RESULT"].Value == System.DBNull.Value ? null : this.Parameters["RESULT"].Value.ToString();
            }
            finally
            {
                this.Connection.Close();
            }
        }

        public async Task<List<RecipientModel>> GetRecipients(string dataProfileId, string profileInstanceId, string workFlowId, string stateId, string actionType, string moduleName)
        {
            try
            {
                this.Connection.Open();
                OracleParameterCollection parameters;
                parameters = this.Parameters;
                parameters.Clear();
                OracleParameter parameter;
                parameter = new OracleParameter("p_user", OracleDbType.VarChar)
                {
                    Direction = System.Data.ParameterDirection.Input,
                    Value = UserProfileHelper.GetInMemoryUserId()
                };
                parameters.Add(parameter);
                parameter = new OracleParameter("p_operation", OracleDbType.VarChar)
                {
                    Direction = System.Data.ParameterDirection.Input,
                    Value = actionType
                };
                parameters.Add(parameter);
                parameter = new OracleParameter("p_stateid", OracleDbType.VarChar)
                {
                    // submit --- toStateId
                    // send back or reject --- fromstate/state/empty
                    Direction = System.Data.ParameterDirection.Input,
                    Value = string.IsNullOrWhiteSpace(stateId) ? string.Empty : DotNetToOracle(stateId)
                };
                parameters.Add(parameter);

                parameter = new OracleParameter("p_profileinstance_id", OracleDbType.VarChar)
                {
                    Direction = System.Data.ParameterDirection.Input,
                    Value = profileInstanceId
                };
                parameters.Add(parameter);

                parameter = new OracleParameter("p_dataprofileid", OracleDbType.VarChar)
                {
                    Direction = System.Data.ParameterDirection.Input,
                    Value = GuidConverter.DotNetToOracle(dataProfileId)
                };
                parameters.Add(parameter);
                parameter = new OracleParameter("p_modulename", OracleDbType.VarChar)
                {
                    Direction = System.Data.ParameterDirection.Input,
                    Value = moduleName
                };
                parameters.Add(parameter);

                parameter = new OracleParameter(InboxConstants.UsernameTab, OracleDbType.Table, ParameterDirection.ReturnValue);
                parameter.ObjectTypeName = InboxConstants.UsernameTab;
                parameters.Add(parameter);

                this.ExecuteProcedure(InboxConstants.GetRecepientsProc, parameters);
                if (this.Parameters[InboxConstants.UsernameTab].Value != System.DBNull.Value)
                {
                    return Mapper.Map<List<RecipientModel>>(this.Parameters[InboxConstants.UsernameTab].Value);
                }

                return new List<RecipientModel>();
            }
            finally
            {
                this.Connection.Close();
            }
        }

        public async Task<List<InboxModel>> GetInboxList(string tabType, string module, ServiceDocumentResult serviceDocumentResult)
        {
            try
            {
                this.Connection.Open();
                OracleParameterCollection parameters;
                parameters = this.Parameters;
                parameters.Clear();
                OracleParameter parameter;
                parameter = new OracleParameter("RESULT", OracleDbType.Boolean, ParameterDirection.ReturnValue);
                parameters.Add(parameter);
                parameter = new OracleParameter("USERID", OracleDbType.VarChar)
                {
                    Direction = System.Data.ParameterDirection.Input,
                    Value = UserProfileHelper.GetInMemoryUserId()
                };
                parameters.Add(parameter);
                parameter = new OracleParameter(InboxConstants.ObjTypeNameInbox, OracleDbType.VarChar)
                {
                    Direction = System.Data.ParameterDirection.Input,
                    Value = tabType
                };
                parameters.Add(parameter);

                parameter = new OracleParameter("p_module", OracleDbType.VarChar)
                {
                    Direction = System.Data.ParameterDirection.Input,
                    Value = module
                };
                parameters.Add(parameter);

                parameter = new OracleParameter(InboxConstants.InboxTbl, Devart.Data.Oracle.OracleDbType.Table)
                {
                    Direction = System.Data.ParameterDirection.Output,
                    ObjectTypeName = InboxConstants.ObjTypeNameInbox,
                };

                parameters.Add(parameter);
                return await this.GetProcedure2<InboxModel>(null, this.GetPackageParams(InboxConstants.ObjTypeNameInbox, InboxConstants.GetProcInbox), serviceDocumentResult, parameters);
            }
            finally
            {
                this.Connection.Close();
            }
        }

        public virtual OracleObject SetParamsInbox<T>(InboxModel inboxModel, ServiceDocument<T> serviceDocument)
             where T : ProfileEntity, IInboxCommonModel
        {
            this.Connection.Open();
            Framework.Core.Security.Models.UserProfile userProfile = UserProfileHelper.GetInMemoryUser();
            OracleType recordType = OracleType.GetObjectType("Inbox_Obj", this.Connection);
            OracleObject recordObject = new OracleObject(recordType);

            recordObject[recordType.Attributes["Sender_ID"]] = inboxModel.SenderID = userProfile.UserId;
            recordObject[recordType.Attributes["Sender_Name"]] = inboxModel.SenderName = userProfile.Email;
            recordObject[recordType.Attributes["Recipient_ID"]] = inboxModel.RecipientID = serviceDocument.DataProfile.DataModel.WorkflowForm?.RecipientID;
            recordObject[recordType.Attributes["Recipient_Name"]] = inboxModel.RecipientName = serviceDocument.DataProfile.DataModel.WorkflowForm?.RecipientName;
            recordObject[recordType.Attributes["DataProfile_ID"]] = inboxModel.DataProfileID = DotNetToOracle(serviceDocument.DataProfile.DataModel.WorkflowInstance.DataProfileId.ToString());
            recordObject[recordType.Attributes["ProfileInstance_ID"]] = inboxModel.ProfileInstanceID = serviceDocument.DataProfile.DataModel.WorkflowInstance.ProfileInstanceId.ToString();
            recordObject[recordType.Attributes["WorkFlow_ID"]] = inboxModel.WorkFlowID = DotNetToOracle(serviceDocument.DataProfile.DataModel.WorkflowInstance.DataProfileId.ToString());
            recordObject[recordType.Attributes["WorkFlowInstance_ID"]] = inboxModel.WorkFlowInstanceID = serviceDocument.DataProfile.DataModel.WorkflowInstance.WorkflowInstanceId.ToString();
            recordObject[recordType.Attributes["Req_Date"]] = DateTime.Now;
            recordObject[recordType.Attributes["Due_Date"]] = inboxModel.DueDate;
            recordObject[recordType.Attributes["Due_Days"]] = inboxModel.DueDays;
            recordObject[recordType.Attributes["Status"]] = serviceDocument.DataProfile.DataModel.WorkflowForm?.Priority ?? "M";
            recordObject[recordType.Attributes["Req_CreatedBy"]] = userProfile.UserId;
            recordObject[recordType.Attributes["Req_CreatedByName"]] = userProfile.Name;
            recordObject[recordType.Attributes["Req_DateCreated"]] = DateTime.Now;
            recordObject[recordType.Attributes["ActionType"]] = inboxModel.ActionType;
            recordObject[recordType.Attributes["Action_Name"]] = inboxModel.ActionName;
            recordObject[recordType.Attributes["RequestFor"]] = inboxModel.RequestFor;
            recordObject[recordType.Attributes["RequestForName"]] = inboxModel.RequestForName;
            recordObject[recordType.Attributes["Amount"]] = inboxModel.Amount;
            recordObject[recordType.Attributes["Purpose"]] = inboxModel.Purpose;
            recordObject[recordType.Attributes["StateID"]] = inboxModel.StateID =
                inboxModel.StateID ?? DotNetToOracle(serviceDocument.DataProfile.DataModel.WorkflowInstance?.WorkflowStateId.ToString());
            recordObject[recordType.Attributes["ToStateID"]] = inboxModel.ToStateID =
                inboxModel.ToStateID ?? DotNetToOracle(serviceDocument.DataProfile.ActionService.AllowedActions
                .Find(a => a.TransitionClaim == serviceDocument.DataProfile.ActionService.CurrentActionId).ToStateId.ToString());
            recordObject[recordType.Attributes["RequestRead"]] = inboxModel.RequestRead;
            recordObject[recordType.Attributes["RequestReadBy"]] = inboxModel.RequestReadBy;
            recordObject[recordType.Attributes["IsSnoozed"]] = inboxModel.IsSnoozed;
            recordObject[recordType.Attributes["IsSnoozedBy"]] = inboxModel.IsSnoozedBy;
            recordObject[recordType.Attributes["ActivityCompleted"]] = inboxModel.ActivityCompleted;
            recordObject[recordType.Attributes["WorkFlowCompleted"]] = inboxModel.WorkFlowCompleted;
            recordObject[recordType.Attributes["IsDeclined"]] = inboxModel.IsDeclined;
            recordObject[recordType.Attributes["Req_Doc_Loc"]] = inboxModel.ReqDocLoc;
            recordObject[recordType.Attributes["ProxyIndicator"]] = inboxModel.ProxyIndicator;
            recordObject[recordType.Attributes["ProxyUser"]] = inboxModel.ProxyUser;
            recordObject[recordType.Attributes["Last_ModifiedOn"]] = inboxModel.LastModifiedOn;
            recordObject[recordType.Attributes["Last_ModifiedBy"]] = inboxModel.LastModifiedBy;
            recordObject[recordType.Attributes["Comments"]] = serviceDocument.DataProfile.DataModel.WorkflowForm?.Comments;
            recordObject[recordType.Attributes["Operation"]] = inboxModel.Operation;
            recordObject[recordType.Attributes["Lang_ID"]] = inboxModel.LangID;
            recordObject[recordType.Attributes["MODULE_NAME"]] = inboxModel.ModuleName;
            recordObject[recordType.Attributes["TransitionClaim"]] = null;
            recordObject[recordType.Attributes["TransitionType"]] = null;
            return recordObject;
        }

        public string UnProtect(string inputCipherText)
        {
            return this.protector.Unprotect(inputCipherText);
        }

        private static string DotNetToOracle(string text)
        {
            if (!string.IsNullOrEmpty(text))
            {
                Guid guid = new Guid(text);
                return BitConverter.ToString(guid.ToByteArray()).Replace("-", string.Empty);
            }

            return string.Empty;
        }

        private static string GetProcessUl(string module)
        {
            if (module == "PROMO")
            {
                return "PromotionNew";
            }
            else if (module == "ITEM")
            {
                return "ITEMBASIC";
            }
            else
            {
                return "a";
            }

            return "I";
        }

        private async Task SendMail<T>(InboxModel inboxModel, ServiceDocument<T> serviceDocument)
           where T : ProfileEntity, IInboxCommonModel
        {
            var cipherText = this.protector.Protect(JsonConvert.SerializeObject(new { userId = 101 }));
            if (serviceDocument.Result.Type == MessageType.Success && inboxModel.Operation != "DFT" && RbSettings.AllowSendingMail)
            {
                InboxNotificationModel emailTemplateModel = new InboxNotificationModel
                {
                    SenderEmail = inboxModel.SenderName,
                    SenderName = inboxModel.SenderName,
                    RecipientEmails = serviceDocument.DataProfile.DataModel.WorkflowForm?.RecipientName,
                    RecipientName = serviceDocument.DataProfile.DataModel.WorkflowForm?.RecipientName,
                    RequestId = inboxModel.ProfileInstanceID,
                    RequestCreatedDate = DateTime.Now,
                    Status = serviceDocument.DataProfile.DataModel.WorkflowForm?.Priority ?? "Medium",
                    Description = "RBS Notification:" + inboxModel.ModuleName,
                    DueDate = DateTime.Now.AddDays(3),
                    ApproveLink = $"{RbSettings.Web}/api/{GetProcessUl(inboxModel.ModuleName)}/Approve?inputData={cipherText}",
                    ApplicationLoginUrl = $"{RbSettings.WebRootPath}/account/login",
                    LogoPath = "../images/RBS with text white.png",
                    CreatorName = serviceDocument.DataProfile.DataModel.CreatedBy
                };
                if (string.IsNullOrWhiteSpace(emailTemplateModel.RecipientEmails) || RbSettings.DemoMode)
                {
                    emailTemplateModel.RecipientEmails = RbSettings.DemoEmails;
                    emailTemplateModel.RecipientName = RbSettings.DemoEmails;
                }

                string template = await this.razorTemplateService.GetHtmlAsync("~/Pages/EmailTemplate/SubmissionMail.cshtml", emailTemplateModel);
                await MailUtility.SendEmailMessage(
                            new FileInputParametersModel
                            {
                                From = RbSettings.ServiceAccountMail,
                                DisplayName = string.Empty,
                                ToMails = new[]
                                {
                                    emailTemplateModel.RecipientEmails
                                },
                                MessageBody = template,
                                Subject = "RBS Notification:" + inboxModel.ModuleName,
                                ProfileInstansId = emailTemplateModel.RequestId,
                                LogoPath = emailTemplateModel.LogoPath,
                            });
            }
        }
    }
}