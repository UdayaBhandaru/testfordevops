﻿// <copyright file="PriceManagementRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.PriceManagement.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Constants;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.PriceManagement.Common;
    using Agility.RBS.PriceManagement.Models;
    using Agility.RBS.PriceManagement.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class PriceManagementRepository : BaseOraclePackage
    {
        private readonly ServiceDocumentResult serviceDocumentResult;

        public PriceManagementRepository()
        {
            this.PackageName = PriceManagementConstants.GetPriceManagementPackage;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        public long? PriceChangeRequestId { get; private set; }

        public static void PreapareRspLit(List<PriceChangeHeadExcelModel> lstHeader, int i, OracleTable priceChangeDetails)
        {
            foreach (var item in priceChangeDetails)
            {
                OracleObject objRsp = (OracleObject)item;
                OracleTable rspTab = (Devart.Data.Oracle.OracleTable)objRsp["RSP"];
                if (rspTab.Count > 0)
                {
                    for (int j = 0; j < lstHeader[i].PriceDetailsList.Count; j++)
                    {
                        lstHeader[i].PriceDetailsList[j].ListOfPriceChangeRspExcelModels.AddRange(Mapper.Map<List<PriceChangeRspExcelModel>>(rspTab));
                    }
                }
            }
        }

        public async Task<List<PriceChangeReasonModel>> PriceChangeReasonsDomainGet()
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(PriceManagementConstants.RecordObjPriceChangeReason, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams(PriceManagementConstants.ObjTypeDomainPriceChangeReason, PriceManagementConstants.GetProcPriceChangeReasonDomain);
            return await this.GetProcedure2<PriceChangeReasonModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<List<PriceChangeSearchModel>> PriceChangeHeadSearch(PriceChangeSearchModel priceChangeSearchHeadModel, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(PriceManagementConstants.ObjTypePriceSearchHead, PriceManagementConstants.GetProcPriceChangeHeadLIST);
            OracleObject recordObject = this.SetParamsPriceChangeHeadList(priceChangeSearchHeadModel);
            return await this.GetProcedure2<PriceChangeSearchModel>(recordObject, packageParameter, this.serviceDocumentResult);
        }

        public async Task<PriceChangeHeadModel> PriceChangeHeadGet(long priceChangeRequestId, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType(PriceManagementConstants.RecordObjPriceChangeHead);
            OracleObject recordObject = this.GetOracleObject(recordType);
            this.SetPriceChangeRequestId(priceChangeRequestId, recordObject);
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            parameters.Add(this.ParameterBuilder(UserProfileHelper.GetUserParameter()));
            PackageParams packageParameter = this.GetPackageParams(PriceManagementConstants.ObjTypePriceChangeHead, PriceManagementConstants.GetProcPriceChangeHead);
            List<PriceChangeHeadModel> priceChangeHeads = await this.GetProcedure2<PriceChangeHeadModel>(recordObject, packageParameter, serviceDocumentResult, parameters);
            return priceChangeHeads != null ? priceChangeHeads[0] : null;
        }

        public virtual void SetPriceChangeRequestId(long priceChangeRequestId, OracleObject recordObject)
        {
            recordObject["PRICE_CHANGE_ID"] = priceChangeRequestId;
        }

        public async Task<ServiceDocumentResult> PriceChangeHeadSet(PriceChangeHeadModel priceChangeHeadModel)
        {
            this.PriceChangeRequestId = 0;
            PackageParams packageParameter = this.GetPackageParams(PriceManagementConstants.ObjTypePriceChangeHead, PriceManagementConstants.SetProcPriceChangeHead);
            OracleObject recordObject = this.SetParamsPriceChangeHead(priceChangeHeadModel);
            var result = await this.SetProcedure(recordObject, packageParameter);
            if (this.ObjResult != null && priceChangeHeadModel.PRICE_CHANGE_ID == null)
            {
                this.PriceChangeRequestId = Convert.ToInt32(this.ObjResult["PRICE_CHANGE_ID"]);
            }
            else
            {
                this.PriceChangeRequestId = priceChangeHeadModel.PRICE_CHANGE_ID;
            }

            return result;
        }

        public async Task<List<PriceChangeHeadModel>> PriceChangeHeadSetWf(PriceChangeHeadModel priceChangeHeadModel)
        {
            PackageParams packageParameter = this.GetPackageParams(PriceManagementConstants.ObjTypePriceChangeHeadWf, PriceManagementConstants.SetProcPriceChangeHeadWf);
            priceChangeHeadModel.Operation = "U";
            OracleObject recordObject = this.SetParamsPriceChangeHead(priceChangeHeadModel);
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            parameters.Add(this.ParameterBuilder(UserProfileHelper.GetUserParameter()));
            var result = await this.ExecutePackage(recordObject, packageParameter, parameters);
            List<PriceChangeHeadModel> modelReturned = Mapper.Map<List<PriceChangeHeadModel>>(result);
            return modelReturned;
        }

        public async Task<List<PriceChangeDetailModel>> GetPriceChangeDetails(int priceChangeRequestId, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType(PriceManagementConstants.RecordObjPriceChangeDetail);
            OracleObject recordObject = this.GetOracleObject(recordType);
            this.SetPriceChangeRequestId(priceChangeRequestId, recordObject);
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            parameters.Add(this.ParameterBuilder(UserProfileHelper.GetUserParameter()));
            PackageParams packageParameter = this.GetPackageParams(PriceManagementConstants.ObjTypePriceChangeDetail, PriceManagementConstants.GetPriceChangeDetail);
            return await this.GetProcedure2<PriceChangeDetailModel>(recordObject, packageParameter, serviceDocumentResult, parameters);
        }

        public async Task<ServiceDocumentResult> SetPriceChangeDetails(PriceChangeDetailModel priceChangeDetail)
        {
            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();
            PackageParams packageParameter = this.GetPackageParams(PriceManagementConstants.ObjTypePriceChangeDetail, PriceManagementConstants.SetPriceChangeDetail);
            OracleParameter parameter;
            parameter = new OracleParameter("RESULT", OracleDbType.Boolean)
            {
                Direction = System.Data.ParameterDirection.ReturnValue
            };
            parameters.Add(parameter);
            parameter = new OracleParameter(packageParameter.ObjectTypeTable, OracleDbType.Table)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                ObjectTypeName = packageParameter.ObjectTypeName,
                Value = this.SetParamsPriceChangeDetails(priceChangeDetail)
            };
            parameters.Add(parameter);
            return await this.SetProcedure(null, packageParameter, parameters);
        }

        public async Task<ServiceDocumentResult> PriceChangeDetailsExcelSave(PriceChangeHeadExcelModel headModel)
        {
            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();
            PackageParams packageParameter = this.GetPackageParams("PRICE_CHANGE_FORM_TAB", PriceManagementConstants.ExcelSetPriceChangeDetail);
            OracleParameter parameter;
            parameter = new OracleParameter("RESULT", OracleDbType.Boolean)
            {
                Direction = System.Data.ParameterDirection.ReturnValue
            };
            parameters.Add(parameter);
            parameter = new OracleParameter(packageParameter.ObjectTypeTable, OracleDbType.Table)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                ObjectTypeName = packageParameter.ObjectTypeName,
                Value = this.SavePriceChangeDetailsExcel(headModel)
            };
            parameters.Add(parameter);
            return await this.SetProcedure(null, packageParameter, parameters);
        }

        public async Task<List<PriceHeadPrintModel>> GetPricePrintData(int priceChangeId, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(PriceManagementConstants.ObjTypePriceChangePrint, PriceManagementConstants.GetProcPriceChangePrint);
            OracleObject recordObject = this.SetParamsPricePrint(priceChangeId);
            OracleTable pPricePrintMstTab = await this.GetProcedure(recordObject, packageParameter, serviceDocumentResult);

            List<PriceHeadPrintModel> lstHeader = Mapper.Map<List<PriceHeadPrintModel>>(pPricePrintMstTab);
            if (lstHeader != null)
            {
                for (int i = 0; i < lstHeader.Count; i++)
                {
                    OracleObject obj = (OracleObject)pPricePrintMstTab[i];
                    OracleTable priceDetailsObj = (Devart.Data.Oracle.OracleTable)obj["PRICE_CHANGE_DETAIL"];
                    if (priceDetailsObj.Count > 0)
                    {
                        lstHeader[i].PriceDetails.AddRange(Mapper.Map<List<PriceChangeDetailModel>>(priceDetailsObj));
                    }
                }
            }

            return lstHeader;
        }

        public async Task<List<PriceChangeHeadExcelModel>> GetPriceChangeDetailExcecl(int priceChangeRequestId, int supplierId, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(
                PriceManagementConstants.PriceChangeHeadExcelListTab,
                PriceManagementConstants.GetPriceChangeDetailExceclList);
            OracleObject recordObject = this.SetParamsPriceChgHeadItem(priceChangeRequestId, supplierId);
            OracleTable pItemTab = await this.GetProcedure(recordObject, packageParameter, serviceDocumentResult);

            var lstHeader = Mapper.Map<List<PriceChangeHeadExcelModel>>(pItemTab);
            if (lstHeader != null)
            {
                for (int i = 0; i < lstHeader.Count; i++)
                {
                    OracleObject obj = (OracleObject)pItemTab[i];
                    OracleTable priceChangeDetails = (Devart.Data.Oracle.OracleTable)obj["Price_DTL_FORM"];
                    if (priceChangeDetails.Count > 0)
                    {
                        lstHeader[i].PriceDetailsList.AddRange(Mapper.Map<List<PriceChangeDetailExcelModel>>(priceChangeDetails));

                        PreapareRspLit(lstHeader, i, priceChangeDetails);
                    }
                }
            }

            return lstHeader;
        }

        public OracleObject SetParamsPriceChangeHeadList(PriceChangeSearchModel priceChangeHeadModel)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(PriceManagementConstants.RecordObjPriceSearchHead, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);

            recordObject[recordType.Attributes["ITEM_BARCODE"]] = priceChangeHeadModel.ItemBarcode;
            recordObject[recordType.Attributes["ITEM_DESC"]] = priceChangeHeadModel.ItemDesc;
            recordObject[recordType.Attributes["ORG_LVL"]] = priceChangeHeadModel.OrgLvl;
            recordObject[recordType.Attributes["ORG_LVL_VALUE"]] = priceChangeHeadModel.OrgLevelValue;
            recordObject[recordType.Attributes["PRICE_CHANGE_ID"]] = priceChangeHeadModel.PRICE_CHANGE_ID;
            recordObject[recordType.Attributes["PRICE_STATUS"]] = priceChangeHeadModel.PriceStatus;
            recordObject[recordType.Attributes["PRICE_CHANGE_DESC"]] = priceChangeHeadModel.PriceChangeDesc;
            recordObject[recordType.Attributes["DATE_TYPE"]] = priceChangeHeadModel.DateType;
            recordObject[recordType.Attributes["START_DATE"]] = priceChangeHeadModel.StartDate;
            recordObject[recordType.Attributes["END_DATE"]] = priceChangeHeadModel.EndDate;
            recordObject[recordType.Attributes["CREATED_BY"]] = priceChangeHeadModel.CreatedBy;
            recordObject[recordType.Attributes["SENT_TO"]] = priceChangeHeadModel.SentTo;
            return recordObject;
        }

        public OracleTable SavePriceChangeDetailsExcel(PriceChangeHeadExcelModel headModel)
        {
            this.Connection.Open();
            /////head
            List<PriceChangeDetailExcelModel> priceChangeDetails = headModel.PriceDetailsList;
            OracleType headRecordType = OracleType.GetObjectType("PRICE_CHANGE_FORM_REC", this.Connection);
            OracleObject headRecordObject = new OracleObject(headRecordType);
            OracleType headTab = OracleType.GetObjectType("PRICE_CHANGE_FORM_TAB", this.Connection);
            OracleTable headTable = new OracleTable(headTab);
            headRecordObject[headRecordType.Attributes["REASON"]] = headModel.Reason;
            headRecordObject[headRecordType.Attributes["PRICE_STATUS"]] = headModel.PriceStatus;
            headRecordObject[headRecordType.Attributes["PRICE_CHANGE_ID"]] = headModel.PriceDetailsList[0]?.PriceChangeId;
            ////headRecordObject[headRecordType.Attributes["PRICE_CHANGE_DESC"]] = headModel.PriceChangeDesc;
            ////headRecordObject[headRecordType.Attributes["PRICE_CHANGE_ORIGIN"]] = headModel.PriceChangeOrigin;
            ////headRecordObject[headRecordType.Attributes["EFFECTIVE_START_DATE"]] = headModel.EffectiveStartDate;
            ////headRecordObject[headRecordType.Attributes["EFFECTIVE_END_DATE"]] = headModel.EffectiveEndDate;
            ////headRecordObject[headRecordType.Attributes["RUN_DATE"]] = headModel.RunDate;
            ////headRecordObject[headRecordType.Attributes["OPERATION"]] = headModel.Operation;
            ////headRecordObject[headRecordType.Attributes["CREATED_BY"]] = this.GetCreatedBy(headModel.CreatedBy);
            ////headRecordObject[headRecordType.Attributes["LAST_UPDATED_BY"]] = FxContext.Context.Name;
            ////headRecordObject[headRecordType.Attributes["WF_NXT_STATE_IND"]] = null;
            ////headRecordObject[headRecordType.Attributes["LANG_ID"]] = "1";

            ////details
            OracleType priceChangeTableType = OracleType.GetObjectType(PriceManagementConstants.ExcelObjTypePriceChangeDetail, this.Connection);
            OracleType priceDetailRecordType = OracleType.GetObjectType(PriceManagementConstants.ExcelRecordObjPriceChangeDetail, this.Connection);
            OracleTable priceChangeDetailTab = new OracleTable(priceChangeTableType);
            foreach (PriceChangeDetailExcelModel priceChangeDetailModel in priceChangeDetails)
            {
                OracleObject priceDetailRecordObject = new OracleObject(priceDetailRecordType);

                priceDetailRecordObject[priceDetailRecordType.Attributes["price_change_id"]] = 823;
                priceDetailRecordObject[priceDetailRecordType.Attributes["dept_desc"]] = null;
                priceDetailRecordObject[priceDetailRecordType.Attributes["dept_id"]] = 1;
                priceDetailRecordObject[priceDetailRecordType.Attributes["category_id"]] = 15;
                priceDetailRecordObject[priceDetailRecordType.Attributes["category_desc"]] = null;
                priceDetailRecordObject[priceDetailRecordType.Attributes["item_barcode"]] = 100052678;
                priceDetailRecordObject[priceDetailRecordType.Attributes["item_desc"]] = "testc";
                priceDetailRecordObject[priceDetailRecordType.Attributes["item_attr_sales"]] = 1023;
                priceDetailRecordObject[priceDetailRecordType.Attributes["case_cost"]] = 10;
                priceDetailRecordObject[priceDetailRecordType.Attributes["cost_cur_code"]] = 10;
                priceDetailRecordObject[priceDetailRecordType.Attributes["unit_cost"]] = 11;
                priceDetailRecordObject[priceDetailRecordType.Attributes["annual_discount"]] = 1;
                priceDetailRecordObject[priceDetailRecordType.Attributes["net_unit_cost"]] = 50;
                priceDetailRecordObject[priceDetailRecordType.Attributes["varianc"]] = 20;
                priceDetailRecordObject[priceDetailRecordType.Attributes["rsp"]] = null;
                priceDetailRecordObject[priceDetailRecordType.Attributes["average_category_margin"]] = 10;
                priceDetailRecordObject[priceDetailRecordType.Attributes["operation"]] = "I";
                priceDetailRecordObject[priceDetailRecordType.Attributes["market_price"]] = null;
                priceChangeDetailTab.Add(priceDetailRecordObject);
            }

            headRecordObject[headRecordType.Attributes["price_dtl_form"]] = priceChangeDetailTab;
            headTable.Add(headRecordObject);
            return headTable;
        }

        public virtual OracleObject SetParamsPriceChangeHead(PriceChangeHeadModel priceChgHdrModel)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(PriceManagementConstants.RecordObjPriceChangeHead, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["REASON"]] = priceChgHdrModel.Reason;
            recordObject[recordType.Attributes["PRICE_STATUS"]] = priceChgHdrModel.PriceStatus;
            recordObject[recordType.Attributes["PRICE_CHANGE_ID"]] = priceChgHdrModel.PRICE_CHANGE_ID;
            recordObject[recordType.Attributes["PRICE_CHANGE_DESC"]] = priceChgHdrModel.PriceChangeDesc;
            recordObject[recordType.Attributes["PRICE_CHANGE_ORIGIN"]] = priceChgHdrModel.PriceChangeOrigin;
            recordObject[recordType.Attributes["EFFECTIVE_START_DATE"]] = priceChgHdrModel.StartDate;
            recordObject[recordType.Attributes["EFFECTIVE_END_DATE"]] = priceChgHdrModel.EndDate;
            recordObject[recordType.Attributes["RUN_DATE"]] = priceChgHdrModel.RunDate;
            recordObject[recordType.Attributes["OPERATION"]] = priceChgHdrModel.Operation;
            recordObject[recordType.Attributes["CREATED_BY"]] = this.GetCreatedBy(priceChgHdrModel.CreatedBy);
            recordObject[recordType.Attributes["LAST_UPDATED_BY"]] = FxContext.Context.Name;
            recordObject[recordType.Attributes["WF_NXT_STATE_IND"]] = null;
            recordObject[recordType.Attributes["LANG_ID"]] = "1";
            recordObject[recordType.Attributes["SUPPLIER_ID"]] = priceChgHdrModel.SupplierId;
            return recordObject;
        }

        private OracleObject SetParamsPriceChgHeadItem(int priceChangeId, int supplierId)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(PriceManagementConstants.RecordObjPriceChangeHeadExcelList, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["PRICE_CHANGE_ID"]] = priceChangeId;
            recordObject[recordType.Attributes["supplier_Id"]] = supplierId;
            return recordObject;
        }

        private OracleTable SetParamsPriceChangeDetails(PriceChangeDetailModel priceChangeDetail)
        {
            this.Connection.Open();
            OracleType priceChangeTableType = OracleType.GetObjectType(PriceManagementConstants.ObjTypePriceChangeDetail, this.Connection);
            OracleType recordType = OracleType.GetObjectType(PriceManagementConstants.RecordObjPriceChangeDetail, this.Connection);
            OracleTable pPriceChangeTab = new OracleTable(priceChangeTableType);
            foreach (PriceChangeDetailModel priceChangeDetailModel in priceChangeDetail.PriceChangeDetails)
            {
                OracleObject recordObject = new OracleObject(recordType);

                recordObject[recordType.Attributes["PRICE_CHANGE_ID"]] = priceChangeDetailModel.PriceChangeId;
                recordObject[recordType.Attributes["PRICE_CHANGE_LINE_ID"]] = priceChangeDetailModel.PriceChangeLineId;
                recordObject[recordType.Attributes["REASON_CODE"]] = priceChangeDetailModel.ReasonCode;
                recordObject[recordType.Attributes["ITEM"]] = priceChangeDetailModel.Item;
                recordObject[recordType.Attributes["ORG_LVL"]] = priceChangeDetailModel.OrgLvl;
                recordObject[recordType.Attributes["ORG_LVL_VALUE"]] = priceChangeDetailModel.OrgLvlValue;
                recordObject[recordType.Attributes["ZONE_NODE_TYPE"]] = priceChangeDetailModel.ZoneNodeType;
                recordObject[recordType.Attributes["LINK_CODE"]] = priceChangeDetailModel.LinkCode;
                recordObject[recordType.Attributes["EFFECTIVE_START_DATE"]] = priceChangeDetailModel.EffectiveStartDate;
                recordObject[recordType.Attributes["EFFECTIVE_END_DATE"]] = priceChangeDetailModel.EffectiveEndDate;
                recordObject[recordType.Attributes["CHANGE_TYPE"]] = priceChangeDetailModel.ChangeType;
                recordObject[recordType.Attributes["CHANGE_AMOUNT"]] = priceChangeDetailModel.ChangeAmount;
                recordObject[recordType.Attributes["CHANGE_CURRENCY"]] = priceChangeDetailModel.ChangeCurrency;
                recordObject[recordType.Attributes["CHANGE_PERCENT"]] = priceChangeDetailModel.ChangePercent;
                recordObject[recordType.Attributes["CHANGE_SELLING_UOM"]] = priceChangeDetailModel.ChangeSellingUom;
                recordObject[recordType.Attributes["CURRENT_UNIT_COST"]] = priceChangeDetailModel.CurrentUnitCost;
                recordObject[recordType.Attributes["COST_CURRENCY"]] = priceChangeDetailModel.CostCurrency;
                recordObject[recordType.Attributes["CURRENT_RSP"]] = priceChangeDetailModel.CurrentRsp;
                recordObject[recordType.Attributes["RETAIL_CURRENCY"]] = priceChangeDetailModel.RetailCurrency;
                recordObject[recordType.Attributes["NEW_RSP"]] = priceChangeDetailModel.NewRsp;
                recordObject[recordType.Attributes["MARGIN"]] = priceChangeDetailModel.Margin;
                recordObject[recordType.Attributes["EXCHANGE_RATE"]] = priceChangeDetailModel.ExchangeRate;
                recordObject[recordType.Attributes["PRICE_GUIDE_ID"]] = priceChangeDetailModel.PriceGuideId;
                recordObject[recordType.Attributes["VENDOR_FUNDED_IND"]] = priceChangeDetailModel.VendorFundedInd;
                recordObject[recordType.Attributes["FUNDING_TYPE"]] = priceChangeDetailModel.FundingType;
                recordObject[recordType.Attributes["FUNDING_AMOUNT"]] = priceChangeDetailModel.FundingAmount;
                recordObject[recordType.Attributes["FUNDING_AMOUNT_CURRENCY"]] = priceChangeDetailModel.FundingAmountCurrency;
                recordObject[recordType.Attributes["FUNDING_PERCENT"]] = priceChangeDetailModel.FundingPercent;
                recordObject[recordType.Attributes["DEAL_ID"]] = priceChangeDetailModel.Deald;
                recordObject[recordType.Attributes["DEAL_DETAIL_ID"]] = priceChangeDetailModel.DealDetailId;
                recordObject[recordType.Attributes["APPROVED_BY"]] = priceChangeDetailModel.ApprovedBy;
                recordObject[recordType.Attributes["IGNORE_CONSTRAINTS"]] = priceChangeDetailModel.IgnoreConstraints;
                recordObject[recordType.Attributes["OPERATION"]] = priceChangeDetailModel.Operation;
                recordObject[recordType.Attributes["DEPT_ID"]] = priceChangeDetailModel.DeptId;
                recordObject[recordType.Attributes["WORKFLOW_STATUS"]] = priceChangeDetailModel.WorkflowStatus;
                recordObject[recordType.Attributes["CATEGORY_ID"]] = priceChangeDetailModel.CategoryId;

                if (priceChangeDetailModel.Operation.Equals(RbCommonConstants.InsertOperation))
                {
                    recordObject[recordType.Attributes["CREATED_BY"]] = FxContext.Context.Name;
                }

                recordObject[recordType.Attributes["LAST_UPDATED_BY"]] = FxContext.Context.Name;
                pPriceChangeTab.Add(recordObject);
            }

            return pPriceChangeTab;
        }

        private OracleObject SetParamsPricePrint(int priceChangeId)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(PriceManagementConstants.RecordObjPriceChangePrint, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);

            recordObject[recordType.Attributes["PRICE_CHANGE_ID"]] = priceChangeId;
            return recordObject;
        }
    }
}