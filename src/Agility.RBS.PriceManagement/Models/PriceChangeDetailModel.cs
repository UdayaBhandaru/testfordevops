﻿// <copyright file="PriceChangeDetailModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.PriceManagement.Models
{
    using System;
    using System.Collections.Generic;
    using Agility.Framework.Core.Profile.Entities;

    public class PriceChangeDetailModel : ProfileEntity
    {
        public PriceChangeDetailModel()
        {
            this.PriceChangeDetails = new List<PriceChangeDetailModel>();
        }

        public int? PriceChangeId { get; set; }

        public int? PriceChangeLineId { get; set; }

        public string State { get; set; }

        public int? ReasonCode { get; set; }

        public string ReasonDesc { get; set; }

        public string Item { get; set; }

        public string ItemDesc { get; set; }

        public string OrgLvl { get; set; }

        public int? OrgLvlValue { get; set; }

        public string OrgLvlValueDesc { get; set; }

        public int? ZoneNodeType { get; set; }

        public int? LinkCode { get; set; }

        public DateTime? EffectiveStartDate { get; set; }

        public DateTime? EffectiveEndDate { get; set; }

        public string ChangeType { get; set; }

        public decimal? ChangeAmount { get; set; }

        public string ChangeCurrency { get; set; }

        public decimal? ChangePercent { get; set; }

        public string ChangeSellingUom { get; set; }

        public string ChangeSellingUomDesc { get; set; }

        public long? PriceGuideId { get; set; }

        public string VendorFundedInd { get; set; }

        public int? FundingType { get; set; }

        public decimal? FundingAmount { get; set; }

        public string FundingAmountCurrency { get; set; }

        public decimal? FundingPercent { get; set; }

        public int? Deald { get; set; }

        public int? DealDetailId { get; set; }

        public DateTime? ApprovedDate { get; set; }

        public string ApprovedBy { get; set; }

        public int? IgnoreConstraints { get; set; }

        public string TableName { get; set; }

        public string Operation { get; set; }

        public int? LangId { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Error { get; set; }

        public List<PriceChangeDetailModel> PriceChangeDetails { get; private set; }

        public string SellingUom { get; set; }

        public string SellingUomDesc { get; set; }

        public decimal? CurrentUnitCost { get; set; }

        public decimal? CurrentRsp { get; set; }

        public decimal? NewRsp { get; set; }

        public decimal? Margin { get; set; }

        public string CostCurrency { get; set; }

        public string RetailCurrency { get; set; }

        public decimal? ExchangeRate { get; set; }

        public int? DeptId { get; set; }

        public string DeptDesc { get; set; }

        public int? CategoryId { get; set; }

        public string CategoryDesc { get; set; }

        public string OrgLvlDesc { get; set; }

        public string WorkflowStatus { get; set; }

        public string WorkflowStatusDesc { get; set; }
    }
}