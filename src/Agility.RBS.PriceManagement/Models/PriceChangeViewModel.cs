﻿// <copyright file="PriceChangeViewModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.PriceManagement.Models
{
    using System.Collections.Generic;
    using Agility.RBS.PriceManagement.Models;

    public class PriceChangeViewModel
    {
        public PriceChangeViewModel()
        {
            this.ExcelHead = new PriceChangeHeadModel();
            this.ExcelDetails = new List<PriceChangeDetailExcelModel>();
        }

        public PriceChangeHeadModel ExcelHead { get; set; }

        public List<PriceChangeDetailExcelModel> ExcelDetails { get; private set; }
    }
}