﻿// <copyright file="PriceChangeHeadModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.PriceManagement.Models
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Inbox.Common;

    [DataProfile(Name = "PRICECHANGEHEAD")]
    public class PriceChangeHeadModel : ProfileEntity<PriceChangeHeadWfModel>, IInboxCommonModel
    {
        [Column("PRICE_CHANGE_ID")]
        public long? PRICE_CHANGE_ID { get; set; }

        [NotMapped]
        public int? Reason { get; set; }

        [NotMapped]
        public int? LangId { get; set; }

        [NotMapped]
        public string PriceChangeDesc { get; set; }

        [NotMapped]
        public string PriceStatus { get; set; }

        [NotMapped]
        public string PriceStatusDesc { get; set; }

        [NotMapped]
        public string ReasonDesc { get; set; }

        [NotMapped]
        public string PriceChangeOrigin { get; set; }

        [NotMapped]
        public string LastUpdatedBy { get; set; }

        [NotMapped]
        public string Operation { get; set; }

        [NotMapped]
        public string ProgramPhase { get; set; }

        [NotMapped]
        public string ProgramMessage { get; set; }

        [NotMapped]
        public string Error { get; set; }

        [NotMapped]
        public DateTime? StartDate { get; set; }

        [NotMapped]
        public DateTime? EndDate { get; set; }

        [NotMapped]
        public DateTime? LastUpdatedDate { get; set; }

        [NotMapped]
        public DateTime? RunDate { get; set; }

        [NotMapped]
        public string TableName { get; set; }

        [NotMapped]
        public DateTime? ApprovedDate { get; set; }

        [NotMapped]
        public string ApprovedBy { get; set; }

        [NotMapped]
        [Column("ORGANISATION_ID")]
        public new int? OrganizationId { get; set; }

        [NotMapped]
        [Column("CREATED_BY")]
        public new string CreatedBy { get; set; }

        [NotMapped]
        [Column("LAST_UPDATED_BY")]
        public new string ModifiedBy { get; set; }

        [NotMapped]
        [Column("DELETEDIND")]
        public new bool DeletedInd { get; set; }

        [NotMapped]
        [Column("CREATED_DATE")]
        public new DateTimeOffset? CreatedDate { get; set; }

        [NotMapped]
        [Column("LAST_UPDATED_DATE")]
        public new DateTimeOffset? ModifiedDate { get; set; }

        [NotMapped]
        public int? WfNxtStateInd { get; set; }

        [NotMapped]
        public int? LineItemCnt { get; set; }

        [NotMapped]
        public int? NoMarginItemCnt { get; set; }

        [NotMapped]
        public Core.Models.RbWfHeaderModel WorkflowForm { get; set; }

        [NotMapped]
        public int? FileCount { get; set; }

        [NotMapped]
        public int? SupplierId { get; set; }

        [NotMapped]
        public string SupplierName { get; set; }
    }
}