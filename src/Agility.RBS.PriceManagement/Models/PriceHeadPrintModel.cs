﻿// <copyright file="PriceHeadPrintModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.PriceManagement.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.Framework.Core.Profile.Entities;

    public class PriceHeadPrintModel : ProfileEntity
    {
        public PriceHeadPrintModel()
        {
            this.PriceDetails = new List<PriceChangeDetailModel>();
        }

        public int? PriceChangeId { get; set; }

        public string PriceChangeDescription { get; set; }

        public List<PriceChangeDetailModel> PriceDetails { get; private set; }

        public int? Reason { get; set; }

        public string PriceChangeOrigin { get; set; }

        public string ReasonDescription { get; set; }

        public string PriceChangeOriginDescription { get; set; }

        public string PriceChangeStatus { get; set; }

        public DateTime? EffectiveStartDate { get; set; }

        public DateTime? EffectiveEndDate { get; set; }

        public DateTime? RunDate { get; set; }

        public DateTime? ApprovalDate { get; set; }

        public string ApprovedBy { get; set; }
    }
}
