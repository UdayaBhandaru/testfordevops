﻿// <copyright file="PriceChangeReasonModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.PriceManagement.Models
{
    using Agility.Framework.Core.Profile.Entities;

    public class PriceChangeReasonModel : ProfileEntity
    {
        public int? Reason { get; set; }

        public string ReasonDesc { get; set; }

        public string ReasonStatus { get; set; }
    }
}
