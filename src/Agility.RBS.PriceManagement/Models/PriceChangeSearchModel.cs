﻿// <copyright file="PriceChangeSearchModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.PriceManagement.Models
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;
    using Agility.Framework.Core.Profile.Entities;

    public class PriceChangeSearchModel : ProfileEntity
    {
        [Column("PRICE_CHANGE_ID")]
        public long? PRICE_CHANGE_ID { get; set; }

        [NotMapped]
        public int? Reason { get; set; }

        [NotMapped]
        public int? LangId { get; set; }

        [NotMapped]
        public string PriceChangeDesc { get; set; }

        [NotMapped]
        public string ItemBarcode { get; set; }

        [NotMapped]
        public string ItemDesc { get; set; }

        [NotMapped]
        public string PriceType { get; set; }

        [NotMapped]
        public string PriceTypeDesc { get; set; }

        [NotMapped]
        public string PriceStatus { get; set; }

        [NotMapped]
        public string PriceStatusDesc { get; set; }

        [NotMapped]
        public string SentTo { get; set; }

        [NotMapped]
        public string DateType { get; set; }

        [NotMapped]
        public string ReasonDesc { get; set; }

        [NotMapped]
        public string PriceChangeOrigin { get; set; }

        [NotMapped]
        public string PriceChgOriginDesc { get; set; }

        [NotMapped]
        public string OrgLvl { get; set; }

        [NotMapped]
        public int? OrgLevelValue { get; set; }

        [NotMapped]
        public DateTime? StartDate { get; set; }

        [NotMapped]
        public DateTime? EndDate { get; set; }

        [NotMapped]
        public DateTime? RunDate { get; set; }

        [NotMapped]
        public string TableName { get; set; }

        [NotMapped]
        public DateTime? ApprovedDate { get; set; }

        [NotMapped]
        public string ApprovedBy { get; set; }

        [NotMapped]
        public string Operation { get; set; }

        [NotMapped]
        public string ProgramPhase { get; set; }

        [NotMapped]
        public string ProgramMessage { get; set; }

        [NotMapped]
        public string Error { get; set; }

        [NotMapped]
        [Column("ORGANISATION_ID")]
        public new int? OrganizationId { get; set; }

        [NotMapped]
        [Column("CREATED_BY")]
        public new string CreatedBy { get; set; }

        [NotMapped]
        [Column("LAST_UPDATED_BY")]
        public new string ModifiedBy { get; set; }

        [NotMapped]
        [Column("DELETEDIND")]
        public new bool DeletedInd { get; set; }

        [NotMapped]
        [Column("CREATED_DATE")]
        public new DateTimeOffset? CreatedDate { get; set; }

        [NotMapped]
        [Column("LAST_UPDATED_DATE")]
        public new DateTimeOffset? ModifiedDate { get; set; }
    }
}