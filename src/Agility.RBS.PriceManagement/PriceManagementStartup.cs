﻿// <copyright file="PriceManagementStartup.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.PriceManagement
{
    using System;
    using System.IO;
    using System.Reflection;
    using Agility.Framework.Core;
    using Agility.RBS.Inbox.Repository;
    using Agility.RBS.PriceManagement.Controllers;
    using Agility.RBS.PriceManagement.PriceChange;
    using Agility.RBS.PriceManagement.PriceChange.Mapping;
    using Agility.RBS.PriceManagement.PriceChange.Repositories;
    using Agility.RBS.PriceManagement.Repository;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Caching.Distributed;
    using Microsoft.Extensions.Caching.Memory;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using Swashbuckle.AspNetCore.Swagger;

    public class PriceManagementStartup : ComponentStartup<DbContext>
    {
        public void Startup(Microsoft.Extensions.Configuration.IConfigurationRoot configuration)
        {
            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<Mapping.PriceManagementModelMapping>();
                cfg.AddProfile<PriceChangeMapping>();
                this.InitilizeMapping(cfg);
            });
        }

        public override void ConfigureServices(IServiceCollection services)
        {
            base.ConfigureServices(services);
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("PriceChangeV1", new Info { Title = "My API", Version = "v1" });
                var xmlFile = $"{this.GetType().GetTypeInfo().Assembly.GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }

        public override void Configure(
           IApplicationBuilder app,
           IHostingEnvironment env,
           ILoggerFactory loggerFactory,
           IDistributedCache distributedCache,
           IMemoryCache memoryCache,
           IServiceProvider serviceProvider)
        {
            base.Configure(app, env, loggerFactory, distributedCache, memoryCache, serviceProvider);
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("//swagger//PriceChangeV1//swagger.json", "My API v1");
                c.SupportedSubmitMethods();
            });
        }

        public override void ConfigureDependencyInjection(IServiceCollection services)
        {
            services.AddTransient<PriceManagementRepository>();
            services.AddTransient<PriceChangeSearchController>();
            services.AddTransient<PriceChangeHeadController>();
            services.AddTransient<PriceChangeRepository>();
            services.AddTransient<PriceChangeController>();
        }
    }
}