﻿//-------------------------------------------------------------------------------------------------
// <copyright file="PriceChangeController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// PriceChangeController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.PriceManagement.PriceChange
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Constants;
    using Agility.RBS.Core.CustomExceptions;
    using Agility.RBS.Core.Models;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.Documents;
    using Agility.RBS.Documents.Models;
    using Agility.RBS.Inbox.Common;
    using Agility.RBS.Inbox.Models;
    using Agility.RBS.Inbox.Repository;
    using Agility.RBS.ItemManagement.Common;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Models;
    using Agility.RBS.PriceManagement.Common;
    using Agility.RBS.PriceManagement.PriceChange.Models;
    using Agility.RBS.PriceManagement.PriceChange.Repositories;
    using DocumentFormat.OpenXml.Packaging;
    using DocumentFormat.OpenXml.Spreadsheet;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    [Route("api/[controller]/[action]")]
    public class PriceChangeController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly PriceChangeRepository priceChangeRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly InboxModel inboxModel;
        private readonly InboxRepository inboxRepository;
        private ServiceDocument<PriceChangeHeadModel> serviceDocument;
        private int priceChangeRequestId;

        public PriceChangeController(
            ServiceDocument<PriceChangeHeadModel> serviceDocument,
            PriceChangeRepository priceChangeRepository,
            DomainDataRepository domainDataRepository,
            InboxRepository inboxRepository)
        {
            this.priceChangeRepository = priceChangeRepository;
            this.domainDataRepository = domainDataRepository;
            this.inboxRepository = inboxRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
            this.inboxModel = new InboxModel { Operation = "CHGS", ModuleName = "PRICE" };
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in Bulk Item Head page when Add button clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<PriceChangeHeadModel>> New()
        {
            this.serviceDocument.New(false);
            this.serviceDocument.DomainData.Add("priceChangeReasons", this.domainDataRepository.RpmCodesGet().Result);
            this.serviceDocument.LocalizationData.AddData("PriceChangeHead");
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for populating fields in the Bulk Item Head page when Edit icon clicked.
        /// </summary>
        /// <param name="id">Cost Change Id</param>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<PriceChangeHeadModel>> Open(int id)
        {
            this.priceChangeRequestId = id;
            await this.serviceDocument.OpenAsync(this.PriceChangeOpen);
            this.serviceDocument.DomainData.Add("priceChangeReasons", this.domainDataRepository.RpmCodesGet().Result);
            this.serviceDocument.LocalizationData.AddData("PriceChangeHead");
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for submitting cost change to next level when Submit button clicked.
        /// </summary>
        /// <param name="serviceDocument">Service Document</param>
        /// <returns>ServiceDocument</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<PriceChangeHeadModel>> Submit([FromBody]ServiceDocument<PriceChangeHeadModel> serviceDocument)
        {
            this.serviceDocument = serviceDocument;
            this.inboxModel.StateID = GuidConverter.DotNetToOracle(Convert.ToString(serviceDocument.DataProfile.DataModel.WorkflowInstance?.WorkflowStateId));
            await this.serviceDocument.TransitAsync(this.PriceChangeSave);

            if (this.serviceDocument.Result.Type == MessageType.Success)
            {
                if (this.serviceDocument.DataProfile.DataModel.WorkflowInstance.StateName == "Worksheet")
                {
                    this.inboxModel.Operation = "DFT";
                }

                await this.inboxRepository.SetInbox(this.inboxModel, this.serviceDocument);
            }

            var stateName = RbsServiceDocCustomizationHelper.GetStateNameByProfileIdAndCurActionId<PriceChangeHeadModel>(this.serviceDocument);
            if (stateName == "Request Completed")
            {
                var filePaths = this.domainDataRepository.FilePathGet(this.inboxModel.ModuleName).Result;
                string rootFolderPath = RbSettings.RbsExcelFileDownloadPath;
                string destinationPath = filePaths.ProcessPath;
                string file = "PriceChange-" + this.serviceDocument.DataProfile.DataModel.PRICE_CHANGE_ID.ToString() + ".xlsm";
                string fileToMove = Path.Combine(rootFolderPath, file);
                string moveTo = Path.Combine(destinationPath, file);

                //// moving file
                System.IO.File.Move(fileToMove, moveTo);
                if (System.IO.File.Exists(moveTo))
                {
                    this.priceChangeRepository.PriceChangeDetailsSet(moveTo);
                    string fileToArchieve = Path.Combine(filePaths.ArchivePath, file);
                    System.IO.File.Move(moveTo, fileToArchieve);
                }
            }

            return this.serviceDocument;
        }

        public async Task<ServiceDocument<PriceChangeHeadModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.PriceChangeSave);
            return this.serviceDocument;
        }

        [HttpPost]
        public async Task<bool> FileUpload([FromBody]FileDataUpload file)
        {
            return await DocumentsRepository.CopyingFile(file);
        }

        /// <summary>
        /// This API invoked after saving and to add details.
        /// id and supplierId
        /// </summary>
        /// <param name="id">header id</param>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [AllowAnonymous]
        public async Task<FileResult> OpenPriceDetailExcel(int id)
        {
            FilePathModel file = this.domainDataRepository.FilePathGet(this.inboxModel.ModuleName).Result;
            string xlsm = $"{RbSettings.RbsExcelFileDownloadPath}\\{Guid.NewGuid().ToString()}.xlsm";
            string priceChangeFile = $"{RbSettings.RbsExcelFileDownloadPath}\\PriceChange-{id}.xlsm";
            string priceChangeFileArc = $"{file.ArchivePath}\\PriceChange-{id}.xlsm";
            string priceChangeFileProcess = $"{file.ProcessPath}\\PriceChange-{id}.xlsm";

            if (System.IO.File.Exists(priceChangeFile))
            {
                System.IO.File.Copy(priceChangeFile, xlsm);
            }
            else if (System.IO.File.Exists(priceChangeFileProcess))
            {
                System.IO.File.Copy(priceChangeFileProcess, xlsm);
            }
            else if (System.IO.File.Exists(priceChangeFileArc))
            {
                System.IO.File.Copy(priceChangeFileArc, xlsm);
            }
            else
            {
                PriceChangeHeadModel priceChange = await this.priceChangeRepository.PriceChangeGet(id, this.serviceDocumentResult);
                System.IO.File.Copy(file.TemplatePath, xlsm);
                using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(xlsm, true))
                {
                    WorkbookPart workbookPart = spreadSheet.WorkbookPart;
                    Sheet sheet = workbookPart.Workbook.Descendants<Sheet>().First(s => s.Name == "Reg RSP Change");
                    WorksheetPart worksheetPart = workbookPart.GetPartById(sheet.Id.Value) as WorksheetPart;
                    var worksheet = worksheetPart.Worksheet;

                    //////// Header Row
                    ExcelHelper.WriteToCell<long>("B", 8, worksheet, Convert.ToInt64(priceChange.Rebates));
                    ExcelHelper.WriteToCell<int>("F", 6, worksheet, Convert.ToInt32(priceChange.PRICE_CHANGE_ID));
                    ExcelHelper.WriteToCell<string>("F", 7, worksheet, priceChange.PriceChangeDesc);
                    ExcelHelper.WriteToCell<string>("F", 8, worksheet, priceChange.ReasonDesc);
                    ExcelHelper.WriteToCell<string>("I", 7, worksheet, priceChange.StartDate?.ToString("dd/MM/yyyy"));
                    ExcelHelper.WriteToCell<string>("I", 8, worksheet, priceChange.EndDate?.ToString("dd/MM/yyyy"));

                    worksheet.Save();
                    Sheet sheetTempData = workbookPart.Workbook.Descendants<Sheet>().FirstOrDefault(s => s.Name == "TempData");
                    WorksheetPart worksheetPartTempData = workbookPart.GetPartById(sheetTempData.Id.Value) as WorksheetPart;
                    Worksheet promotionworksheetTempData = worksheetPartTempData.Worksheet;
                    ExcelHelper.WriteToCell<string>("A", 1, promotionworksheetTempData, RbSettings.Web);
                    promotionworksheetTempData.Save();
                }
            }

            return await DocumentsHelper.GetFilestream(xlsm, this);
        }

        /// <summary>
        /// This API invoked for future prices in Product Tab of Item Super Screen
        /// </summary>
        /// <param name="itemNo">Item Code</param>
        /// <returns>List oF FuturePriceModel</returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<List<FuturePriceModel>> GetFuturePriceData(string itemNo)
        {
            return await this.priceChangeRepository.GetFuturePriceData(itemNo, this.serviceDocumentResult);
        }

        private async Task<PriceChangeHeadModel> PriceChangeOpen()
        {
            try
            {
                FilePathModel file = this.domainDataRepository.FilePathGet(this.inboxModel.ModuleName).Result;
                var bulkItem = await this.priceChangeRepository.PriceChangeGet(this.priceChangeRequestId, file, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return bulkItem;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> PriceChangeSave()
        {
            this.serviceDocument.DataProfile.DataModel.PriceStatus = RbsServiceDocCustomizationHelper.GetStateNameByProfileIdAndCurActionId(this.serviceDocument);
            this.serviceDocument.Result = await this.priceChangeRepository.PriceChangeSet(this.serviceDocument.DataProfile.DataModel);
            if (this.serviceDocument.Result.Type != MessageType.Success)
            {
                return false;
            }

            this.serviceDocument.DataProfile.DataModel.PRICE_CHANGE_ID = this.priceChangeRepository.PriceChangeRequestId;
            return true;
        }
    }
}
