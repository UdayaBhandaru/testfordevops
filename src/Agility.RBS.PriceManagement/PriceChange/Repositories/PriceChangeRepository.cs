﻿// <copyright file="PriceChangeRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// BulkItemRepository
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.PriceManagement.PriceChange.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.ItemManagement.BulkItem.Models;
    using Agility.RBS.ItemManagement.Common;
    using Agility.RBS.MDM.Common;
    using Agility.RBS.MDM.Repositories.Models;
    using Agility.RBS.PriceManagement.Common;
    using Agility.RBS.PriceManagement.PriceChange.Models;
    using AutoMapper;
    using Devart.Data.Oracle;
    using DocumentFormat.OpenXml;
    using DocumentFormat.OpenXml.Packaging;
    using DocumentFormat.OpenXml.Spreadsheet;
    using Microsoft.AspNetCore.Authorization;

    public class PriceChangeRepository : BaseOraclePackage
    {
        public PriceChangeRepository()
        {
            this.PackageName = PriceManagementConstants.GetPriceManagementPackage;
        }

        public long? PriceChangeRequestId { get; private set; }

        public async Task<PriceChangeHeadModel> ConvertExcelDataToType(DataTable eSheet, PriceChangeHeadModel iHeadModel)
        {
            if (eSheet != null)
            {
                List<PriceChangeDetailModel> orderDetailsList = (from DataRow dr in eSheet.Rows
                                                                 where Convert.ToString(dr[0]) != "Department" && Convert.ToString(dr[2]) != string.Empty
                                                                 select new PriceChangeDetailModel
                                                                 {
                                                                     Item = Convert.ToString(dr[1]),
                                                                     Department = Convert.ToString(dr[0]),
                                                                     ItemDesc = Convert.ToString(dr[2]),
                                                                     SsOld = Convert.ToString(dr[15]),
                                                                     SsNew = Convert.ToString(dr[16]),
                                                                     SssOld = Convert.ToString(dr[18]),
                                                                     SssNew = Convert.ToString(dr[19]),
                                                                     WhsOld = Convert.ToString(dr[21]),
                                                                     WhsNew = Convert.ToString(dr[22]),
                                                                     CsOld = Convert.ToString(dr[24]),
                                                                     CsNew = Convert.ToString(dr[25]),
                                                                     MarginSsNew = Convert.ToString(dr[29]),
                                                                     MarginSssNew = Convert.ToString(dr[31]),
                                                                     MarginWhsNew = Convert.ToString(dr[33]),
                                                                     MarginCsNew = Convert.ToString(dr[35])
                                                                 }).ToList();

                iHeadModel.PriceChangeDetails.AddRange(orderDetailsList);
            }

            return iHeadModel;
        }

        public async Task<List<PriceChangeSearchModel>> PriceChangeListGet(ServiceDocument<PriceChangeSearchModel> serviceDoc, ServiceDocumentResult serviceDocumentResult)
        {
            PriceChangeSearchModel priceChangeSearch = serviceDoc.DataProfile.DataModel;
            PackageParams packageParameter = this.GetPackageParams(PriceManagementConstants.ObjTypePriceSearchHead, PriceManagementConstants.GetProcPriceChangeHeadLIST);
            OracleObject recordObject = this.SetParamsPriceChangeHeadList(priceChangeSearch);

            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();

            BaseOracleParameter baseOracleParameter;
            baseOracleParameter = new BaseOracleParameter
            {
                Name = "P_START",
                DataType = OracleDbType.Number,
                Direction = ParameterDirection.Input,
                Value = Convert.ToString(priceChangeSearch.StartRow)
            };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            baseOracleParameter = new BaseOracleParameter { Name = "P_END", DataType = OracleDbType.Number, Direction = ParameterDirection.Input, Value = Convert.ToString(priceChangeSearch.EndRow) };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            baseOracleParameter = new BaseOracleParameter { Name = "P_total", DataType = OracleDbType.Number, Direction = ParameterDirection.Output };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            if (priceChangeSearch.SortModel == null)
            {
                priceChangeSearch.SortModel = new[] { new Core.Models.SortModel { ColId = null, Sort = null } };
            }

            baseOracleParameter = new BaseOracleParameter { Name = "P_SortColId", DataType = OracleDbType.VarChar, Direction = ParameterDirection.Input, Value = priceChangeSearch.SortModel[0].ColId };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            baseOracleParameter = new BaseOracleParameter { Name = "P_SortOrder", DataType = OracleDbType.VarChar, Direction = ParameterDirection.Input, Value = priceChangeSearch.SortModel[0].Sort };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));

            parameters.Add(this.ParameterBuilder(UserProfileHelper.GetUserParameter()));
            var orders = await this.GetProcedure2<PriceChangeSearchModel>(recordObject, packageParameter, serviceDocumentResult, parameters);
            priceChangeSearch.TotalRows = (this.Parameters["p_total"].Value != null && this.Parameters["p_total"].Value != DBNull.Value) ? Convert.ToInt64(this.Parameters["p_total"].Value) : 1;
            serviceDoc.DataProfile.DataModel = priceChangeSearch;
            return orders;
        }

        public async Task<PriceChangeHeadModel> PriceChangeGet(int headSeq, FilePathModel file, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType(PriceManagementConstants.RecordObjPriceChangeHead);
            OracleObject recordObject = this.GetOracleObject(recordType);
            this.SetPriceChangeRequestId(headSeq, recordObject);
            PackageParams packageParameter = this.GetPackageParams(PriceManagementConstants.ObjTypePriceChangeHead, PriceManagementConstants.GetProcPriceChangeHead);
            var priceChangeHeads = await this.GetProcedure2<PriceChangeHeadModel>(recordObject, packageParameter, serviceDocumentResult);
            if (priceChangeHeads != null && priceChangeHeads.Count > 0)
            {
                string pricechangeFile = $"{RbSettings.RbsExcelFileDownloadPath}\\PriceChange-{priceChangeHeads[0].PRICE_CHANGE_ID.Value}.xlsm";
                if (priceChangeHeads[0].PriceStatus == "Request Completed")
                {
                    pricechangeFile = $"{file.ProcessPath}\\PriceChange-{headSeq}.xlsm";
                    if (!File.Exists(pricechangeFile))
                    {
                        pricechangeFile = $"{file.ArchivePath}\\PriceChange-{headSeq}.xlsm";
                    }
                }

                if (File.Exists(pricechangeFile))
                {
                    await this.ConvertExcelDataToType(await this.GetDataTableByReadingExcel(pricechangeFile), priceChangeHeads[0]);
                }

                return priceChangeHeads[0];
            }

            return null;
        }

        public async Task<PriceChangeHeadModel> PriceChangeGet(long priceChangeRequestId, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            OracleType recordType = this.GetObjectType(PriceManagementConstants.RecordObjPriceChangeHead);
            OracleObject recordObject = this.GetOracleObject(recordType);
            this.SetPriceChangeRequestId(priceChangeRequestId, recordObject);
            PackageParams packageParameter = this.GetPackageParams(PriceManagementConstants.ObjTypePriceChangeHead, PriceManagementConstants.GetProcPriceChangeHead);
            List<PriceChangeHeadModel> priceChangeHeads = await this.GetProcedure2<PriceChangeHeadModel>(recordObject, packageParameter, serviceDocumentResult);
            return priceChangeHeads != null ? priceChangeHeads[0] : null;
        }

        public virtual void SetPriceChangeRequestId(long priceChangeRequestId, OracleObject recordObject)
        {
            recordObject["PRICE_CHANGE_ID"] = priceChangeRequestId;
        }

        public async Task<ServiceDocumentResult> PriceChangeSet(PriceChangeHeadModel priceChangeHeadModel)
        {
            this.PriceChangeRequestId = 0;
            PackageParams packageParameter = this.GetPackageParams(PriceManagementConstants.ObjTypePriceChangeHead, PriceManagementConstants.SetProcPriceChangeHead);
            OracleObject recordObject = this.SetParamsPriceChangeHead(priceChangeHeadModel);
            var result = await this.SetProcedure(recordObject, packageParameter);
            if (this.ObjResult != null && priceChangeHeadModel.PRICE_CHANGE_ID == null)
            {
                this.PriceChangeRequestId = Convert.ToInt32(this.ObjResult["PRICE_CHANGE_ID"]);
            }
            else
            {
                this.PriceChangeRequestId = priceChangeHeadModel.PRICE_CHANGE_ID;
            }

            return result;
        }

        public void PriceChangeDetailsSet(string fileName)
        {
            PackageParams packageParameter = this.GetPackageParams("PRICE_CHANGE_FORM_tab", "SETPRICECHGDETAIL");
            OracleObject recordObject = this.PriceChangeDetailCreation(fileName);
            this.SetProcedureBulk(recordObject, packageParameter).ConfigureAwait(true);
        }

        public OracleObject PriceChangeDetailCreation(string fileName)
        {
            using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(fileName, false))
            {
                WorkbookPart workbookPart = spreadSheet.WorkbookPart;
                Sheet sheet = workbookPart.Workbook.Descendants<Sheet>().First(s => s.Name == "Reg RSP Change");
                WorksheetPart worksheetPart = workbookPart.GetPartById(sheet.Id.Value) as WorksheetPart;
                var rows = worksheetPart.Worksheet.Descendants<Row>().ToList();
                this.Connection.Open();
                OracleType recordType = this.GetObjectType("PRICE_CHANGE_FORM_REC");
                OracleObject recordObject = this.GetOracleObject(recordType);

                // Detail Object
                OracleType dtlTableType = this.GetObjectType("PRICE_DTL_FORM_TAB");
                OracleType dtlRecordType = this.GetObjectType("PRICE_DTL_FORM_REC");
                OracleTable pUtlDmnDtlTab = new OracleTable(dtlTableType);

                for (int i = 13; i < rows.Count; i++)
                {
                    string itemBarcode = ExcelHelper.GetCellValue("B", i + 1, worksheetPart.Worksheet, workbookPart);
                    if (!string.IsNullOrEmpty(itemBarcode))
                    {
                        OracleObject dtlRecordObject = this.GetOracleObject(dtlRecordType);
                        dtlRecordObject["PRICE_CHANGE_ID"] = ExcelHelper.GetCellValue("F", 6, worksheetPart.Worksheet, workbookPart);
                        dtlRecordObject["ITEM_BARCODE"] = itemBarcode;
                        dtlRecordObject["UNIT_COST"] = NumericCellNullCheck(ExcelHelper.GetCellValue("L", i + 1, worksheetPart.Worksheet, workbookPart));
                        dtlRecordObject["NET_UNIT_COST"] = NumericCellNullCheck(ExcelHelper.GetCellValue("N", i + 1, worksheetPart.Worksheet, workbookPart));
                        dtlRecordObject["OPERATION"] = "I";

                        // RSP Object
                        OracleType rspTableType = this.GetObjectType("RSP_TAB");
                        OracleTable pRspTab = new OracleTable(rspTableType);
                        this.RspDetailsGet(pRspTab, i, worksheetPart, "SS", "Q", workbookPart, "AD");
                        this.RspDetailsGet(pRspTab, i, worksheetPart, "SS-", "T", workbookPart, "AF");
                        this.RspDetailsGet(pRspTab, i, worksheetPart, "WHS", "W", workbookPart, "AH");
                        this.RspDetailsGet(pRspTab, i, worksheetPart, "CS", "Z", workbookPart, "AJ");
                        dtlRecordObject["RSP"] = pRspTab;

                        // Market Object
                        OracleType marketTableType = this.GetObjectType("MARKET_PRICE_TAB");
                        OracleTable pMarketTab = new OracleTable(marketTableType);
                        this.MarketRspGet(pMarketTab, i, worksheetPart, "Lulu", "AQ", workbookPart);
                        this.MarketRspGet(pMarketTab, i, worksheetPart, "Carrefour", "AR", workbookPart);
                        this.MarketRspGet(pMarketTab, i, worksheetPart, "COOP", "AS", workbookPart);
                        this.MarketRspGet(pMarketTab, i, worksheetPart, "Saveco", "AT", workbookPart);
                        dtlRecordObject["MARKET_PRICE"] = pMarketTab;

                        pUtlDmnDtlTab.Add(dtlRecordObject);
                        recordObject["PRICE_DTL_FORM"] = pUtlDmnDtlTab;
                    }
                    else
                    {
                        return recordObject;
                    }
                }

                return recordObject;
            }
        }

        public void RspDetailsGet(OracleTable pRspTab, int i, WorksheetPart worksheetPart, string formatName, string sheetColName, WorkbookPart workbookPart, string marginCol)
        {
            var pnValue = ExcelHelper.GetCellValue(sheetColName, i + 1, worksheetPart.Worksheet, workbookPart);
            var marValue = ExcelHelper.GetCellValue(sheetColName, i + 1, worksheetPart.Worksheet, workbookPart);
            if (!string.IsNullOrEmpty(pnValue))
            {
                OracleType rspRecordType = this.GetObjectType("RSP_REC");
                var rspRecordObject = this.GetOracleObject(rspRecordType);
                rspRecordObject["CUR_PRICE"] = pnValue;
                rspRecordObject["FORMAT"] = formatName;
                rspRecordObject["MARGIN"] = marValue;
                pRspTab.Add(rspRecordObject);
            }
        }

        public void MarketRspGet(OracleTable pMarketRspTab, int i, WorksheetPart worksheetPart, string formatName, string sheetColName, WorkbookPart workbookPart)
        {
            var ssValue = ExcelHelper.GetCellValue(sheetColName, i + 1, worksheetPart.Worksheet, workbookPart);
            if (!string.IsNullOrEmpty(ssValue))
            {
                OracleType recordType = this.GetObjectType("MARKET_PRICE_REC");
                var recordObject = this.GetOracleObject(recordType);
                recordObject["COMPETITOR"] = formatName;
                recordObject["COMP_NAME"] = formatName;
                recordObject["COMP_RETAIL"] = ssValue;
                pMarketRspTab.Add(recordObject);
            }
        }

        public OracleObject SetParamsPriceChangeHeadList(PriceChangeSearchModel priceChangeHeadModel)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(PriceManagementConstants.RecordObjPriceSearchHead, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["ITEM_BARCODE"]] = priceChangeHeadModel.ItemBarcode;
            recordObject[recordType.Attributes["ITEM_DESC"]] = priceChangeHeadModel.ItemDesc;
            recordObject[recordType.Attributes["PRICE_CHANGE_ID"]] = priceChangeHeadModel.PRICE_CHANGE_ID;
            recordObject[recordType.Attributes["PRICE_STATUS"]] = priceChangeHeadModel.PriceStatus;
            recordObject[recordType.Attributes["PRICE_CHANGE_DESC"]] = priceChangeHeadModel.PriceChangeDesc;
            recordObject[recordType.Attributes["START_DATE"]] = priceChangeHeadModel.StartDate;
            recordObject[recordType.Attributes["END_DATE"]] = priceChangeHeadModel.EndDate;
            return recordObject;
        }

        public virtual OracleObject SetParamsPriceChangeHead(PriceChangeHeadModel priceChgHdrModel)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(PriceManagementConstants.RecordObjPriceChangeHead, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["REASON"]] = priceChgHdrModel.Reason;
            recordObject[recordType.Attributes["PRICE_STATUS"]] = priceChgHdrModel.PriceStatus;
            recordObject[recordType.Attributes["PRICE_CHANGE_ID"]] = priceChgHdrModel.PRICE_CHANGE_ID;
            recordObject[recordType.Attributes["PRICE_CHANGE_DESC"]] = priceChgHdrModel.PriceChangeDesc;
            recordObject[recordType.Attributes["EFFECTIVE_START_DATE"]] = priceChgHdrModel.StartDate;
            recordObject[recordType.Attributes["EFFECTIVE_END_DATE"]] = priceChgHdrModel.EndDate;
            recordObject[recordType.Attributes["RUN_DATE"]] = priceChgHdrModel.RunDate;
            recordObject[recordType.Attributes["OPERATION"]] = priceChgHdrModel.Operation;
            recordObject[recordType.Attributes["CREATED_BY"]] = this.GetCreatedBy(priceChgHdrModel.CreatedBy);
            recordObject[recordType.Attributes["LAST_UPDATED_BY"]] = FxContext.Context.Name;
            recordObject[recordType.Attributes["WF_NXT_STATE_IND"]] = null;
            recordObject[recordType.Attributes["LANG_ID"]] = "1";
            return recordObject;
        }

        public async Task<List<FuturePriceModel>> GetFuturePriceData(string itemId, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            OracleParameterCollection parameters = this.Parameters;
            parameters.Clear();
            BaseOracleParameter baseOracleParameter;
            baseOracleParameter = new BaseOracleParameter { Name = "P_ITEM", DataType = OracleDbType.VarChar, Direction = ParameterDirection.Input, Value = itemId };
            parameters.Add(this.ParameterBuilder(baseOracleParameter));
            OracleType recordType = this.GetObjectType("FUTURE_RSP_REC");
            OracleObject recordObject = this.GetOracleObject(recordType);
            PackageParams packageParameter = this.GetPackageParams("FUTURE_RSP_TAB", "GETFUTURERSP");
            return await this.GetProcedure2<FuturePriceModel>(recordObject, packageParameter, serviceDocumentResult, parameters);
        }
    }
}