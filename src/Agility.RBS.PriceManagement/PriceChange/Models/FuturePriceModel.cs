﻿// <copyright file="FuturePriceModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.PriceManagement.PriceChange.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class FuturePriceModel
    {
        public string DeptDesc { get; set; }

        public string ClassDesc { get; set; }

        public string SubclassDesc { get; set; }

        public string LocName { get; set; }

        public string SellingUomDesc { get; set; }

        public DateTime? ActionDate { get; set; }

        public string PcChangeTypeDesc { get; set; }

        public decimal? PcChangeAmount { get; set; }

        public int? Promotion1Id { get; set; }

        public string Promotion1IdDesc { get; set; }
    }
}
