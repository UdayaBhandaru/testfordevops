﻿// <copyright file="PriceChangeSearchModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.PriceManagement.PriceChange.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Core.Models;
    using Agility.RBS.Inbox.Common;

    public class PriceChangeSearchModel : ProfileEntity, Core.IPaginationModel
    {
        public long? PRICE_CHANGE_ID { get; set; }

        public string PriceChangeDesc { get; set; }

        public string ItemBarcode { get; set; }

        public string ItemDesc { get; set; }

        public string PriceStatus { get; set; }

        public string PriceStatusDesc { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string Reason { get; set; }

        public string ReasonDesc { get; set; }

        public string Operation { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Error { get; set; }

        public int? StartRow { get; set; }

        public int? EndRow { get; set; }

        public long? TotalRows { get; set; }

        public SortModel[] SortModel { get; set; }
    }
}