﻿// <copyright file="PriceChangeDetailExcelModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.PriceManagement.PriceChange.Models
{
    using System;
    using System.Collections.Generic;
    using Agility.Framework.Core.Profile.Entities;

    public class PriceChangeDetailExcelModel : ProfileEntity
    {
        public PriceChangeDetailExcelModel()
        {
            this.ListOfPriceChangeRspExcelModels = new List<PriceChangeRspExcelModel>();
            this.MarketPrice = new List<PriceChangeMarketTabExcelModel>();
        }

        public int? PriceChangeId { get; set; }

        public int? DeptId { get; set; }

        public string DeptDesc { get; set; }

        public int? CategoryId { get; set; }

        public string CategoryDesc { get; set; }

        public string ItemBarcode { get; set; }

        public string ItemDesc { get; set; }

        public int? ItemAttrSales { get; set; }

        public long? CaseCost { get; set; }

        public string CostCurCode { get; set; }

        public int? UnitCost { get; set; }

        public long? AnnualDiscount { get; set; }

        public long? NetUnitCost { get; set; }

        public int? Variance { get; set; }

        public long? AverageCategoryMargin { get; set; }

        public List<PriceChangeMarketTabExcelModel> MarketPrice { get; private set; }

        public List<PriceChangeRspExcelModel> ListOfPriceChangeRspExcelModels { get; private set; }
    }
}
