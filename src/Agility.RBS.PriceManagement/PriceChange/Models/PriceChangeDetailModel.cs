﻿// <copyright file="PriceChangeDetailModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.PriceManagement.PriceChange.Models
{
    using System;
    using System.Collections.Generic;
    using Agility.Framework.Core.Profile.Entities;

    public class PriceChangeDetailModel : ProfileEntity
    {
        public string Department { get; set; }

        public string Item { get; set; }

        public string ItemDesc { get; set; }

        public string Reason { get; set; }

        public string SsOld { get; set; }

        public string SsNew { get; set; }

        public string SssOld { get; set; }

        public string SssNew { get; set; }

        public string WhsOld { get; set; }

        public string WhsNew { get; set; }

        public string CsOld { get; set; }

        public string CsNew { get; set; }

        public string MarginSsNew { get; set; }

        public string MarginSssNew { get; set; }

        public string MarginWhsNew { get; set; }

        public string MarginCsNew { get; set; }
    }
}