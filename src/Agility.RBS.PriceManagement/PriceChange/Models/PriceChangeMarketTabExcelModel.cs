﻿// <copyright file="PriceChangeMarketTabExcelModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.PriceManagement.PriceChange.Models
{
    using System;
    using System.Collections.Generic;
    using Agility.Framework.Core.Profile.Entities;

    public class PriceChangeMarketTabExcelModel : ProfileEntity
    {
        public int CurrentPrice { get; set; }

        public string PriceCurrentCode { get; set; }

        public int? Margin { get; set; }

        public int? ExchangeRate { get; set; }

        public string Format { get; set; }

        public string FormatName { get; set; }
    }
}
