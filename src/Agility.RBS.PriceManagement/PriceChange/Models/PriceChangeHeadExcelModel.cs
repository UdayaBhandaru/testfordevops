﻿// <copyright file="PriceChangeHeadExcelModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.PriceManagement.PriceChange.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using Agility.Framework.Core.Profile.Entities;

    public class PriceChangeHeadExcelModel : ProfileEntity
    {
        public PriceChangeHeadExcelModel()
        {
            this.PriceDetailsList = new List<PriceChangeDetailExcelModel>();
        }

        public int? SupplierId { get; set; }

        public string SuppName { get; set; }

        public long? Rebates { get; set; }

        public int? DivisionId { get; set; }

        public string DivisionDesc { get; set; }

        public int? PriceChangeId { get; set; }

        public string PriceChangeDesc { get; set; }

        public string PriceStatus { get; set; }

        public string PriceStatusDesc { get; set; }

        public int? Reason { get; set; }

        public string ReasonDesc { get; set; }

        public string PriceChangeOrigin { get; set; }

        public DateTime? EffectiveStartDate { get; set; }

        public DateTime? EffectiveEndDate { get; set; }

        public DateTime? RunDate { get; set; }

        public DateTime? CreatedDate { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? ApprovalDate { get; set; }

        public string ApprovedBy { get; set; }

        public DateTime? LaseUpdatedDate { get; set; }

        public string LaseUpdatedBy { get; set; }

        public string WfNxtStateInd { get; set; }

        public int? LineItemCnt { get; set; }

        public int? NoMarginItemCnt { get; set; }

        public int? FileCnt { get; set; }

        public int? LangID { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }

        public List<PriceChangeDetailExcelModel> PriceDetailsList { get; private set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string PError { get; set; }
    }
}
