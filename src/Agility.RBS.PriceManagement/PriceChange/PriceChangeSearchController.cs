﻿//-------------------------------------------------------------------------------------------------
// <copyright file="PriceChangeSearchController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
// PriceChangeController
//-------------------------------------------------------------------------------------------------

namespace Agility.RBS.PriceManagement.PriceChange
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Constants;
    using Agility.RBS.Core.CustomExceptions;
    using Agility.RBS.Core.Models;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.Documents;
    using Agility.RBS.Documents.Models;
    using Agility.RBS.Inbox.Common;
    using Agility.RBS.Inbox.Models;
    using Agility.RBS.Inbox.Repository;
    using Agility.RBS.ItemManagement.Common;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Models;
    using Agility.RBS.PriceManagement.Common;
    using Agility.RBS.PriceManagement.PriceChange.Models;
    using Agility.RBS.PriceManagement.PriceChange.Repositories;
    using DocumentFormat.OpenXml.Packaging;
    using DocumentFormat.OpenXml.Spreadsheet;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [AllowAnonymous]
    [Route("api/[controller]/[action]")]
    public class PriceChangeSearchController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly PriceChangeRepository bulkItemRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly ServiceDocument<PriceChangeSearchModel> serviceDocument;

        public PriceChangeSearchController(
            ServiceDocument<PriceChangeSearchModel> serviceDocument,
            PriceChangeRepository bulkItemRepository,
            DomainDataRepository domainDataRepository,
            InboxRepository inboxRepository)
        {
            this.bulkItemRepository = bulkItemRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in the Bulk Item List page.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        public async Task<ServiceDocument<PriceChangeSearchModel>> List()
        {
            this.serviceDocument.DomainData.Add("workflowStatusList", this.domainDataRepository.WorkflowStatusGet(RbCommonConstants.PriceModule).Result.OrderBy(x => x.Name));
            this.serviceDocument.LocalizationData.AddData("PriceChangeHead");
            ////this.bulkItemRepository.PriceChangeDetailsSet(@"D:\Projects\rbsweb\RBSWeb\wwwroot\Excel\PriceChange\Archive\PriceChange-1087.xlsm");
            return this.serviceDocument;
        }

        public async Task<ServiceDocument<PriceChangeSearchModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.PriceChangeSearch);
            return this.serviceDocument;
        }

        private async Task<List<PriceChangeSearchModel>> PriceChangeSearch()
        {
            try
            {
                var lstUomClass = await this.bulkItemRepository.PriceChangeListGet(this.serviceDocument, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return lstUomClass;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }
    }
}
