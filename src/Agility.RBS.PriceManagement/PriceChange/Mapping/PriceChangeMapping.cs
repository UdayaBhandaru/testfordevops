﻿// <copyright file="PriceChangeMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.PriceManagement.PriceChange.Mapping
{
    using Agility.RBS.Core;
    using Agility.RBS.MDM.Repositories.Models;
    using Agility.RBS.PriceManagement.PriceChange.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class PriceChangeMapping : Profile
    {
        public PriceChangeMapping()
            : base("PriceChangeMapping")
        {
            this.CreateMap<OracleObject, PriceChangeHeadModel>()
             .ForMember(m => m.PRICE_CHANGE_ID, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["PRICE_CHANGE_ID"])))
             .ForMember(m => m.PriceChangeDesc, opt => opt.MapFrom(r => r["PRICE_CHANGE_DESC"]))
             .ForMember(m => m.Reason, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["REASON"])))
              .ForMember(m => m.ReasonDesc, opt => opt.MapFrom(r => r["REASON_DESC"]))
             .ForMember(m => m.PriceStatus, opt => opt.MapFrom(r => r["PRICE_STATUS"]))
             .ForMember(m => m.PriceStatusDesc, opt => opt.MapFrom(r => r["PRICE_STATUS_DESC"]))
             .ForMember(m => m.StartDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["EFFECTIVE_START_DATE"])))
             .ForMember(m => m.EndDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["EFFECTIVE_END_DATE"])))
             .ForMember(m => m.RunDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["RUN_DATE"])))
             .ForMember(m => m.CreatedBy, opt => opt.MapFrom(r => r["CREATED_BY"]))
             .ForMember(m => m.CreatedDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["CREATED_DATE"])))
             .ForMember(m => m.ModifiedBy, opt => opt.MapFrom(r => r["LAST_UPDATED_BY"]))
             .ForMember(m => m.ModifiedDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["LAST_UPDATED_DATE"])))
             .ForMember(m => m.WfNxtStateInd, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["WF_NXT_STATE_IND"])))
             .ForMember(m => m.LineItemCnt, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["LINE_ITEM_CNT"])))
             .ForMember(m => m.NoMarginItemCnt, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["NO_MARGIN_ITEM_CNT"])))
             .ForMember(m => m.FileCount, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["FILE_CNT"])))
             .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"))
             .ForMember(m => m.ProgramPhase, opt => opt.MapFrom(r => r["PROGRAM_PHASE"]))
             .ForMember(m => m.ProgramMessage, opt => opt.MapFrom(r => r["PROGRAM_MESSAGE"]))
             .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_ERROR"]))
             .ForMember(m => m.TableName, opt => opt.MapFrom(r => "PRICE_CHANGE_HEAD"));

            this.CreateMap<OracleObject, PriceChangeSearchModel>()
             .ForMember(m => m.PRICE_CHANGE_ID, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["PRICE_CHANGE_ID"])))
             .ForMember(m => m.PriceChangeDesc, opt => opt.MapFrom(r => r["PRICE_CHANGE_DESC"]))
             .ForMember(m => m.Reason, opt => opt.MapFrom(r => r["REASON"]))
              .ForMember(m => m.ReasonDesc, opt => opt.MapFrom(r => r["REASON_DESC"]))
             .ForMember(m => m.PriceStatus, opt => opt.MapFrom(r => r["PRICE_STATUS"]))
             .ForMember(m => m.PriceStatusDesc, opt => opt.MapFrom(r => r["PRICE_STATUS_DESC"]))
             .ForMember(m => m.StartDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["START_DATE"])))
             .ForMember(m => m.EndDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["END_DATE"])))
             .ForMember(m => m.CreatedBy, opt => opt.MapFrom(r => r["CREATED_BY"]))
             .ForMember(m => m.CreatedDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["CREATED_DATE"])))
             .ForMember(m => m.ModifiedBy, opt => opt.MapFrom(r => r["LAST_UPDATED_BY"]))
             .ForMember(m => m.ModifiedDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["LAST_UPDATED_DATE"])))
             .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"))
             .ForMember(m => m.ProgramPhase, opt => opt.MapFrom(r => r["PROGRAM_PHASE"]))
             .ForMember(m => m.ProgramMessage, opt => opt.MapFrom(r => r["PROGRAM_MESSAGE"]))
             ////.ForMember(m => m.SupplierId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["Supplier_Id"])))
             ////.ForMember(m => m.SupplierName, opt => opt.MapFrom(r => r["SUPPLIER_NAME"]))
             .ForMember(m => m.Error, opt => opt.MapFrom(r => r["P_ERROR"]));

            this.CreateMap<OracleObject, FuturePriceModel>()
             .ForMember(m => m.DeptDesc, opt => opt.MapFrom(r => r["DEPT_DESC"]))
             .ForMember(m => m.ClassDesc, opt => opt.MapFrom(r => r["CLASS_DESC"]))
             .ForMember(m => m.SubclassDesc, opt => opt.MapFrom(r => r["SUBCLASS_DESC"]))
             .ForMember(m => m.LocName, opt => opt.MapFrom(r => r["LOC_NAME"]))
             .ForMember(m => m.SellingUomDesc, opt => opt.MapFrom(r => r["SELLING_UOM__DESC"]))
             .ForMember(m => m.ActionDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["ACTION_DATE"])))
              .ForMember(m => m.PcChangeTypeDesc, opt => opt.MapFrom(r => r["PC_CHANGE_TYPE_DESC"]))
              .ForMember(m => m.Promotion1IdDesc, opt => opt.MapFrom(r => r["PROMOTION1_ID_DESC"]))
             .ForMember(m => m.PcChangeAmount, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["PC_CHANGE_AMOUNT"])))
             .ForMember(m => m.Promotion1Id, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["PROMOTION1_ID"])));
        }
    }
}