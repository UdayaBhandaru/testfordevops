﻿// <copyright file="PriceManagementConstants.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.PriceManagement.Common
{
    internal static class PriceManagementConstants
    {
        public const string OracleParameterResult = "RESULT";

        // Documents Package
        public const string GetPriceManagementPackage = "UTL_PRICE";
        public const string GetProcPriceZoneGroup = "GETPRICEZONEGROUP";
        public const string SetProcPriceZoneGroup = "SETPRICEZONEGROUP";
        public const string ObjTypePriceZoneGroup = "PRICE_ZONE_GROUP_TAB";

        public const string SetProcPriceZone = "SETPRICEZONE";
        public const string GetProcPriceZone = "GETPRICEZONE";
        public const string ObjTypePriceZone = "PRICE_ZONE_GROUP_TAB";

        public const string GetProcPriceChangeHeadLIST = "GETPRICECHANGELIST";
        public const string ObjTypePriceSearchHead = "PRICE_CHG_SEARCH_TAB";
        public const string RecordObjPriceSearchHead = "PRICE_CHG_SEARCH_REC";

        public const string GetProcPriceChangeHead = "GETPRICECHANGEHEAD";
        public const string SetProcPriceChangeHead = "SETPRICECHANGEHEAD";
        public const string ObjTypePriceChangeHead = "PRICE_CHANGE_HEAD_TAB";
        public const string RecordObjPriceChangeHead = "PRICE_CHANGE_HEAD_REC";

        public const string ObjTypePriceChangeHeadWf = "PRICE_CHANGE_HEAD_TAB";
        public const string SetProcPriceChangeHeadWf = "SETPRICECHANGE_WORKFLOW";

        public const string GetPriceChangeDetail = "GETPRICECHANGEDETAIL";
        public const string SetPriceChangeDetail = "SETPRICECHANGEDETAIL";
        public const string ObjTypePriceChangeDetail = "PRICE_CHANGE_DETAIL_TAB";
        public const string RecordObjPriceChangeDetail = "PRICE_CHANGE_DETAIL_REC";

        public const string ExcelSetPriceChangeDetail = "setpricechginfo1";
        public const string ExcelObjTypePriceChangeDetail = "PRICE_DTL_FORM_TAB";
        public const string ExcelRecordObjPriceChangeDetail = "PRICE_DTL_FORM_REC";

        public const string GetPriceChangeReason = "GETPRICECHANGEREASON";
        public const string SetPriceChangeReason = "SETPRICECHANGEREASON";
        public const string ObjTypePriceChangeReason = "PRICE_CHG_REASON_TAB";
        public const string RecordObjPriceChangeReason = "PRICE_CHG_REASON_REC";

        public const string GetPriceChangeErrors = "GETPRICECHANGEERRORS";
        public const string SetPriceChangeErrors = "SETPRICECHANGEERRORS";
        public const string ObjTypePriceChangeErrors = "PRICE_CHANGE_ERRORS_TAB";

        public const string ObjTypePriceChangePrint = "PRICE_CHANGE_PRINT_TAB";
        public const string GetProcPriceChangePrint = "GETPRICECHANGEPRINT";
        public const string RecordObjPriceChangePrint = "PRICE_CHANGE_PRINT_REC";

        // Domain Header Ids
        public const string DomainHdrDateType = "DATETYPE";
        public const string DomainHdrIndicator = "INDICATOR";
        public const string DomainHdrPriceChgReqStatus = "PRICECHST";
        public const string DomainHdrOriginType = "PRICE_ORGN";
        public const string DomainHdrPriceReason = "PETP";

        public const string ObjTypeDomainPriceChangeReason = "PRICE_CHG_REASON_TAB";
        public const string GetProcPriceChangeReasonDomain = "GETPRICECHGREASON";
        public const string DomainHdrChangeLvl = "PRCHGLVL";
        public const string DomainHdrChangeType = "PRCHGTYPE";
        //// Price Change Detail For Excel

        public const string GetPriceChangeDetailExceclList = "GETPRICECHGINFO1";

        public const string PriceChangeHeadExcelListTab = "PRICE_CHANGE_FORM_TAB";
        public const string RecordObjPriceChangeHeadExcelList = "PRICE_CHANGE_FORM_REC";

        public const string PriceChangeDetailExcelListTab = "PRICE_DTL_FORM_TAB";
        public const string RecordObjPriceChangeDetailExcelList = "PRICE_DTL_FORM_REC";

        public const string PriceChangeRspExcelListTab = "PRICE_DTL_FORM_TAB";
        public const string RecordObjPriceChangeRspExcelList = "PRICE_DTL_FORM_TAB";
    }
}
