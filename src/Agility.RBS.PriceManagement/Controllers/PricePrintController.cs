﻿// <copyright file="PricePrintController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.PriceManagement.PriceChange
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.PriceManagement.Models;
    using Agility.RBS.PriceManagement.Repository;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    [Route("api/[controller]/[action]")]
    public class PricePrintController : Controller
    {
        private readonly ServiceDocument<PriceHeadPrintModel> serviceDocument;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly PriceManagementRepository priceChangeRepository;
        private readonly ILogger<PricePrintController> logger;
        private int priceChangeId;

        public PricePrintController(
            ServiceDocument<PriceHeadPrintModel> serviceDocument,
            PriceManagementRepository priceChangeRepository,
            ILogger<PricePrintController> logger)
        {
            this.serviceDocument = serviceDocument;
            this.priceChangeRepository = priceChangeRepository;
            this.logger = logger;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        /// <summary>
        /// This API invoked for fetching Price Change Head and Details data when Print button clicked.
        /// </summary>
        /// <param name="priceChangeId">Price Change Id</param>
        /// <returns>ServiceDocument</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<PriceHeadPrintModel>> List(int priceChangeId)
        {
            this.priceChangeId = priceChangeId;
            this.logger.LogInformation("Price Print Started");
            await this.serviceDocument.ToListAsync(this.GetPricePrintData);
            this.serviceDocument.LocalizationData.AddData("RbsPrint");
            this.logger.LogInformation("Price Print Completed");
            return this.serviceDocument;
        }

        private async Task<List<PriceHeadPrintModel>> GetPricePrintData()
        {
            try
            {
                var printData = await this.priceChangeRepository.GetPricePrintData(this.priceChangeId, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return printData;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                this.logger.LogInformation(ex.ToString());
                return null;
            }
        }
    }
}