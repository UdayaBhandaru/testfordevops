﻿// <copyright file="PriceChangeSearchController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.PriceManagement.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core.Constants;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.PriceManagement.Common;
    using Agility.RBS.PriceManagement.Models;
    using Agility.RBS.PriceManagement.Repository;
    using Microsoft.AspNetCore.Mvc;

    [Route("api/[controller]/[action]")]
    public class PriceChangeSearchController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly PriceManagementRepository priceChangeRepository;
        private readonly ServiceDocument<PriceChangeSearchModel> serviceDocument;

        public PriceChangeSearchController(
            ServiceDocument<PriceChangeSearchModel> serviceDocument,
            PriceManagementRepository priceChangeRepository,
        DomainDataRepository domainDataRepository)
        {
            this.serviceDocument = serviceDocument;
            this.priceChangeRepository = priceChangeRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in the Price Change List page.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<PriceChangeSearchModel>> List()
        {
            ////this.serviceDocument.DomainData.Add("orgLevel", this.domainDataRepository.DomainDetailData(PriceManagementConstants.DomainHdrChangeLvl).Result);
            ////this.serviceDocument.DomainData.Add("pricezone", this.domainDataRepository.CommonData("PZ").Result);
            ////this.serviceDocument.DomainData.Add("location", await this.domainDataRepository.CommonData("LOC"));
            ////this.serviceDocument.DomainData.Add("dateType", this.domainDataRepository.DomainDetailData(PriceManagementConstants.DomainHdrDateType).Result);
            ////this.serviceDocument.DomainData.Add("users", this.domainDataRepository.UsersDomainGet().Result);
            this.serviceDocument.DomainData.Add("workflowStatusList", this.domainDataRepository.WorkflowStatusGet(RbCommonConstants.PriceModule).Result.OrderBy(x => x.Name));
            this.serviceDocument.LocalizationData.AddData("PriceChangeHead");
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for fetching Price Change records based on search criteria when Search button clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<PriceChangeSearchModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.PriceChangeHeadSearch);
            return this.serviceDocument;
        }

        private async Task<List<PriceChangeSearchModel>> PriceChangeHeadSearch()
        {
            try
            {
                var priceManagements = await this.priceChangeRepository.PriceChangeHeadSearch(this.serviceDocument.DataProfile.DataModel, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return priceManagements;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }
    }
}
