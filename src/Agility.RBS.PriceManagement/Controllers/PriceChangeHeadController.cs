﻿// <copyright file="PriceChangeHeadController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.PriceManagement.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Constants;
    using Agility.RBS.Inbox.Common;
    using Agility.RBS.Inbox.Models;
    using Agility.RBS.Inbox.Repository;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.PriceManagement.Common;
    using Agility.RBS.PriceManagement.Models;
    using Agility.RBS.PriceManagement.Repository;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [Route("api/[controller]/[action]")]
    public class PriceChangeHeadController : Controller
    {
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly PriceManagementRepository priceChangeRepository;
        private readonly InboxRepository inboxRepository;
        private readonly InboxModel inboxModel;
        private ServiceDocument<PriceChangeHeadModel> serviceDocument;
        private int priceChangeRequestId;

        public PriceChangeHeadController(
        ServiceDocument<PriceChangeHeadModel> serviceDocument,
        PriceManagementRepository priceChangeRepository,
        DomainDataRepository domainDataRepository,
        InboxRepository inboxRepository)
        {
            this.serviceDocument = serviceDocument;
            this.priceChangeRepository = priceChangeRepository;
            this.domainDataRepository = domainDataRepository;
            this.inboxRepository = inboxRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
            this.inboxModel = new InboxModel { Operation = "CHGS", ModuleName = "PRICE" };
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in Price Change Head page when Add button clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<PriceChangeHeadModel>> New()
        {
            this.serviceDocument.New(false);
            this.serviceDocument.DomainData.Add("priceChangeReasons", await this.domainDataRepository.PriceChangeReasonsDomainGet());
            this.serviceDocument.LocalizationData.AddData("PriceChangeHead");
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for populating fields in the Price Change Head page when Edit icon clicked.
        /// </summary>
        /// <param name="id">Price Change Id</param>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<PriceChangeHeadModel>> Open(int id)
        {
            this.priceChangeRequestId = id;
            await this.serviceDocument.OpenAsync(this.CallBackOpen);
            this.serviceDocument.DomainData.Add("priceChangeReasons", await this.domainDataRepository.CommonData("PRICE_CHG_REASON"));
            this.serviceDocument.LocalizationData.AddData("PriceChangeHead");
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for saving field values in the Price Change Head page when Save button clicked.
        /// </summary>
        /// <param name="serviceDocument">Service Document</param>
        /// <returns>ServiceDocument</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<PriceChangeHeadModel>> Save([FromBody]ServiceDocument<PriceChangeHeadModel> serviceDocument)
        {
            await this.serviceDocument.SaveAsync(this.PriceChangeHeadSave);
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for submitting price change to next level when Submit button clicked.
        /// </summary>
        /// <param name="serviceDocument">Service Document</param>
        /// <returns>ServiceDocument</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<PriceChangeHeadModel>> Submit([FromBody]ServiceDocument<PriceChangeHeadModel> serviceDocument)
        {
                this.SetServiceDocumentAndInboxStateId(serviceDocument);
                await this.serviceDocument.TransitAsync(this.PriceChangeHeadSave);

            if (this.serviceDocument.Result.Type == MessageType.Success)
            {
                if (this.serviceDocument.DataProfile.DataModel.WorkflowInstance.StateName == "Worksheet")
                {
                    this.inboxModel.Operation = "DFT";
                }

                await this.inboxRepository.SetInbox(this.inboxModel, this.serviceDocument);
            }

            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for rejecting price change to previous level when Reject button clicked.
        /// </summary>
        /// <param name="serviceDocument">Service Document</param>
        /// <returns>ServiceDocument</returns>
        /// <response code="401">Un Authorized</response>
        [AllowAnonymous]
        [HttpPost]
        public async Task<ServiceDocument<PriceChangeHeadModel>> Reject([FromBody]ServiceDocument<PriceChangeHeadModel> serviceDocument)
        {
            this.SetServiceDocumentAndInboxStateId(serviceDocument);
            await this.serviceDocument.TransitAsync(GuidConverter.DotNetToOracle(serviceDocument.DataProfile.DataModel.WorkflowForm.StateId), true);
            if (this.serviceDocument.Result.Type == MessageType.Success)
            {
                this.inboxModel.Operation = "REJ";
                this.inboxModel.ActionType = "R";
                this.inboxModel.ToStateID = GuidConverter.DotNetToOracle(this.serviceDocument.DataProfile.DataModel.WorkflowStateId.ToString());
                this.inboxRepository.SetInbox(this.inboxModel, this.serviceDocument);
            }

            return this.serviceDocument;
        }

        private async Task<PriceChangeHeadModel> CallBackOpen()
        {
            try
            {
                var priceChangeHead = await this.priceChangeRepository.PriceChangeHeadGet(this.priceChangeRequestId, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return priceChangeHead;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> PriceChangeHeadSave()
        {
            this.serviceDocument.DataProfile.DataModel.PriceStatus = RbsServiceDocCustomizationHelper.GetStateNameByProfileIdAndCurActionId(this.serviceDocument);
            this.serviceDocument.Result = await this.priceChangeRepository.PriceChangeHeadSet(this.serviceDocument.DataProfile.DataModel);
            if (this.serviceDocument.Result.Type != MessageType.Success)
            {
                return false;
            }

            this.serviceDocument.DataProfile.DataModel.PRICE_CHANGE_ID = this.priceChangeRepository.PriceChangeRequestId.Value;
            return true;
        }

        private void SetServiceDocumentAndInboxStateId(ServiceDocument<PriceChangeHeadModel> serviceDocument)
        {
            this.serviceDocument = serviceDocument;
            this.inboxModel.StateID = RbsServiceDocCustomizationHelper.GetStateIdDotNetToOracle(serviceDocument);
        }
    }
}
