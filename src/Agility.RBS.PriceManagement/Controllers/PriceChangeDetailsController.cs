﻿// <copyright file="PriceChangeDetailsController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.PriceManagement.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Models;
    using Agility.RBS.Core.Utility;
    using Agility.RBS.ItemManagement.Item;
    using Agility.RBS.ItemManagement.Item.Models;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.PriceManagement.Common;
    using Agility.RBS.PriceManagement.Models;
    using Agility.RBS.PriceManagement.Models;
    using Agility.RBS.PriceManagement.Repository;
    using DocumentFormat.OpenXml;
    using DocumentFormat.OpenXml.Packaging;
    using DocumentFormat.OpenXml.Spreadsheet;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [Route("api/[controller]/[action]")]
    public class PriceChangeDetailsController : Controller
    {
        private const string priceFilePrefix = "PriceChange";
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly ServiceDocument<PriceChangeDetailModel> serviceDocument;
        private readonly PriceManagementRepository priceChangeRepository;
        private readonly ItemSelectRepository itemSelectRepository;
        private int priceChangeRequestId;

        public PriceChangeDetailsController(
            ServiceDocument<PriceChangeDetailModel> serviceDocument,
            PriceManagementRepository priceChangeRepository,
        DomainDataRepository domainDataRepository,
        ItemSelectRepository itemSelectRepository)
        {
            this.serviceDocument = serviceDocument;
            this.priceChangeRepository = priceChangeRepository;
            this.domainDataRepository = domainDataRepository;
            this.itemSelectRepository = itemSelectRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        /// <summary>
        /// This API invoked for loading grid data along with data for autocomplete, radio button fields in Price Change Details Page.
        /// </summary>
        /// <param name="id">Price Change Id</param>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<PriceChangeDetailModel>> List(int id)
        {
            this.priceChangeRequestId = id;
            this.serviceDocument.DomainData.Add("changelevel", this.domainDataRepository.DomainDetailData(PriceManagementConstants.DomainHdrChangeLvl).Result);
            this.serviceDocument.DomainData.Add("changetype", this.domainDataRepository.DomainDetailData(PriceManagementConstants.DomainHdrChangeType).Result);
            this.serviceDocument.DomainData.Add("currency", this.domainDataRepository.CurrencyDomainGet().Result);
            this.serviceDocument.DomainData.Add("changereason", await this.domainDataRepository.CommonData("PRICE_CHG_REASON"));
            this.serviceDocument.DomainData.Add("location", this.domainDataRepository.CommonData("LOC").Result);
            this.serviceDocument.DomainData.Add("pricezone", this.domainDataRepository.CommonData("PZ").Result);

            this.serviceDocument.LocalizationData.AddData("PriceChangeDetails");
            await this.serviceDocument.ToListAsync(this.PriceChangeDetailsSearch);
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for populating dependent fields when Get Item hyperlink clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<PriceChangeDetailModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.SearchItem);
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for saving grid data in the Price Change Details page when Save button clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<PriceChangeDetailModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.PriceChangeDetailSave);
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for saving grid data in the Price Change Details page when Save button clicked.
        /// </summary>
        /// <param name="details">Price Change Id</param>
        /// <returns>viewModel</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        [AllowAnonymous]
        public async Task<bool> PriceDetailExcelSave([FromBody] List<PriceChangeDetailExcelModel> details)
        {
            PriceChangeHeadExcelModel head = new PriceChangeHeadExcelModel();
            head.PriceDetailsList.AddRange(details);
            this.priceChangeRepository.PriceChangeDetailsExcelSave(head);
            return true;
        }

        /// <summary>
        /// This API invoked after saving and to add details.
        /// id and supplierId
        /// </summary>
        /// <param name="id">header id</param>
        /// <param name="supplierId">suplierId</param>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [AllowAnonymous]
        public async Task<FileResult> OpenPriceDetailExcel(int id, int supplierId)
        {
            ////string templateFile = RbSettings.PriceExcelFileExistPath;
            var file = this.domainDataRepository.FilePathGet("PRICE").Result;
            string xlsmNew = RbSettings.RbsExcelFileDownloadPath + "\\" + Guid.NewGuid().ToString() + ".xlsm";
            string oldFile = $"{RbSettings.RbsExcelFileDownloadPath}\\{priceFilePrefix}-{id}.xlsm";
            if (System.IO.File.Exists(oldFile))
            {
                System.IO.File.Copy(oldFile, xlsmNew);
            }
            else
            {
                System.IO.File.Copy(file.TemplatePath, xlsmNew);
                List<PriceChangeHeadExcelModel> listHead = await this.priceChangeRepository.GetPriceChangeDetailExcecl(id, supplierId, this.serviceDocumentResult);
                using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(xlsmNew, true))
                {
                    WorkbookPart workbookPart = spreadSheet.WorkbookPart;
                    Sheet sheet = workbookPart.Workbook.Descendants<Sheet>().First(s => s.Name == "Reg RSP Change");
                    WorksheetPart worksheetPart = workbookPart.GetPartById(sheet.Id.Value) as WorksheetPart;
                    var worksheet = worksheetPart.Worksheet;
                    if (listHead.Count > 0)
                    {
                        //////// Header Row
                        ExcelHelper.WriteToCell<int?>("B", 6, worksheet, listHead[0].SupplierId);
                        ExcelHelper.WriteToCell<string>("B", 7, worksheet, listHead[0].SuppName);
                        ExcelHelper.WriteToCell<long>("B", 8, worksheet, Convert.ToInt64(listHead[0].Rebates));
                        ExcelHelper.WriteToCell<int>("F", 6, worksheet, Convert.ToInt32(listHead[0].PriceChangeId));
                        ExcelHelper.WriteToCell<string>("F", 7, worksheet, listHead[0].PriceChangeDesc);
                        ExcelHelper.WriteToCell<string>("F", 8, worksheet, listHead[0].ReasonDesc);
                        ExcelHelper.WriteToCell<string>("I", 6, worksheet, Convert.ToString(listHead[0].PriceChangeOrigin));
                        ExcelHelper.WriteToCell<string>("I", 7, worksheet, listHead[0].EffectiveStartDate?.ToString("dd/MM/yyyy"));
                        ExcelHelper.WriteToCell<string>("I", 8, worksheet, listHead[0].EffectiveEndDate?.ToString("dd/MM/yyyy"));
                    }

                    worksheet.Save();
                    Sheet sheetTempData = workbookPart.Workbook.Descendants<Sheet>().FirstOrDefault(s => s.Name == "TempData");
                    WorksheetPart worksheetPartTempData = workbookPart.GetPartById(sheetTempData.Id.Value) as WorksheetPart;
                    Worksheet promotionworksheetTempData = worksheetPartTempData.Worksheet;
                    ExcelHelper.WriteToCell<string>("A", 1, promotionworksheetTempData, RbSettings.Web);
                    promotionworksheetTempData.Save();
                }
            }

            var memory = new MemoryStream();
            using (var stream = new FileStream(xlsmNew, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }

            memory.Position = 0;

            var contentDispositionHeader = new System.Net.Mime.ContentDisposition
            {
                Inline = true,
                FileName = xlsmNew
            };
            this.Response.Headers.Add("Content-Disposition", contentDispositionHeader.ToString());

            var fileResultant = this.File(memory, DocumentsHelper.GetContentType(xlsmNew), Path.GetFileName(xlsmNew));
            System.IO.File.Delete(xlsmNew);
            return fileResultant;
        }

        [AllowAnonymous]
        public async Task<bool> FileUpload([FromBody]FileDataUpload file)
        {
            var tempFile = $"{Guid.NewGuid()}.xlsm";
            DocumentsHelper.FileUpload(string.Empty, tempFile, file.FileData);
            tempFile = $"{RbSettings.DocumentsServerPhysicalPath}\\{tempFile}";
            var physicalFile = $"{RbSettings.RbsExcelFileDownloadPath}\\{priceFilePrefix}-{file.RequestId}.xlsm";
            if (System.IO.File.Exists(physicalFile))
            {
                System.IO.File.Delete(physicalFile);
            }

            System.IO.File.Copy(tempFile, physicalFile);
            System.IO.File.Delete(tempFile);
            return true;
        }

        private async Task<List<PriceChangeDetailModel>> SearchItem()
        {
            try
            {
                ItemSelectModel itmSelModel = new ItemSelectModel
                {
                    ItemBarcode = this.serviceDocument.DataProfile.DataModel.Item,
                    OrgLvl = this.serviceDocument.DataProfile.DataModel.OrgLvl,
                    OrgLvlVal = this.serviceDocument.DataProfile.DataModel.OrgLvlValue,
                    ScreenId = "PZ"
                };
                List<PriceChgItemSelectModel> items = await this.itemSelectRepository.SelectPriceChgItems(itmSelModel, this.serviceDocumentResult);

                List<PriceChangeDetailModel> priceChgDetailList = new List<PriceChangeDetailModel>();

                if (items != null && items.Count > 0)
                {
                    items.ForEach(item =>
                    {
                        PriceChangeDetailModel priceChangeDetail = new PriceChangeDetailModel
                        {
                            DeptId = item.DeptId,
                            DeptDesc = item.DeptDesc,
                            CategoryId = item.CategoryId,
                            CategoryDesc = item.CategoryDesc,
                            ItemDesc = item.ItemDesc,
                            OrgLvlDesc = item.OrgLvlDesc,
                            CurrentUnitCost = item.CurrentCost,
                            CurrentRsp = item.CurrentPrice,
                            ExchangeRate = item.ExchangeRate,
                            CostCurrency = item.CostCurrencyCode,
                            RetailCurrency = item.PriceCurrencyCode,
                            SellingUom = item.SellingUom,
                            SellingUomDesc = item.SellingUomDesc
                        };
                        priceChgDetailList.Add(priceChangeDetail);
                    });
                }

                this.serviceDocument.Result = this.serviceDocumentResult;
                return priceChgDetailList;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<List<PriceChangeDetailModel>> PriceChangeDetailsSearch()
        {
            try
            {
                var costChangeDetails = await this.priceChangeRepository.GetPriceChangeDetails(this.priceChangeRequestId, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return costChangeDetails;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> PriceChangeDetailSave()
        {
            this.serviceDocument.Result = await this.priceChangeRepository.SetPriceChangeDetails(this.serviceDocument.DataProfile.DataModel);
            if (this.serviceDocument.Result.Type != MessageType.Success)
            {
                return false;
            }

            return true;
        }
    }
}
