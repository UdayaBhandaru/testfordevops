﻿// <copyright file="DealRepository.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.DealManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Agility.RBS.DealManagement.Common;
    using Agility.RBS.DealManagement.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class DealRepository : BaseOraclePackage
    {
        public DealRepository()
        {
            this.PackageName = DealManagementConstants.PackageName;
        }

        public int? DealId { get; private set; }

        public async Task<List<DealSearchNewModel>> DealListGetWithdocs(int supplierId, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            PackageParams packageParameter = this.GetPackageParams(DealManagementConstants.ObjTypeDealSearch, DealManagementConstants.GetProcDealList);
            OracleType recordType = OracleType.GetObjectType(DealManagementConstants.RecDealSearch, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["SUPPLIER"]] = supplierId;
            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();
            OracleParameter parameter;
            parameter = new OracleParameter("P_START", OracleDbType.Number) { Direction = ParameterDirection.Input, Value = 0 };
            parameters.Add(parameter);
            parameter = new OracleParameter("P_END", OracleDbType.Number) { Direction = ParameterDirection.Input, Value = 20 };
            parameters.Add(parameter);

            parameter = new OracleParameter("P_SortColId", OracleDbType.VarChar) { Direction = ParameterDirection.Input, Value = null };
            parameters.Add(parameter);
            parameter = new OracleParameter("P_SortOrder", OracleDbType.VarChar) { Direction = ParameterDirection.Input, Value = null };
            parameters.Add(parameter);
            parameter = new OracleParameter("P_total", OracleDbType.Number) { Direction = ParameterDirection.Output };
            parameters.Add(parameter);
            return await this.GetProcedure2<DealSearchNewModel>(recordObject, packageParameter, serviceDocumentResult, parameters);
        }

        public async Task<SupplierDealModel> GetDeal(int dealId, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(DealManagementConstants.RecordDeal, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["DEAL_ID"]] = dealId;
            PackageParams packageParameter = this.GetPackageParams(DealManagementConstants.ObjTypeDeal, DealManagementConstants.GetDeal);
            OracleTable pDomainMstTab = await this.GetProcedure(recordObject, packageParameter, serviceDocumentResult);

            var lstHeader = Mapper.Map<List<SupplierDealModel>>(pDomainMstTab);
            if (lstHeader != null)
            {
                OracleObject obj = (OracleObject)pDomainMstTab[0];
                OracleTable domainDetails = (Devart.Data.Oracle.OracleTable)obj["DEAL_ITEMLOC"];
                if (domainDetails.Count > 0)
                {
                    lstHeader[0].ListOfDealItemLocations.AddRange(Mapper.Map<List<DealItemLocNewModel>>(domainDetails));
                }

                return lstHeader[0];
            }

            return null;
        }

        public async Task<ServiceDocumentResult> SetDeal(SupplierDealModel dealHeadModel)
        {
            PackageParams packageParameter = this.GetPackageParams(DealManagementConstants.ObjTypeDeal, DealManagementConstants.SetDeal);
            OracleObject recordObject = this.SetParamsDeal(dealHeadModel);
            return await this.SetProcedure(recordObject, packageParameter);
        }

        public async Task<List<DealSearchModel>> DealListGetWithdocs(ServiceDocument<DealSearchModel> serviceDoc, ServiceDocumentResult serviceDocumentResult)
        {
            DealSearchModel dealSearchModel = serviceDoc.DataProfile.DataModel;
            PackageParams packageParameter = this.GetPackageParams(DealManagementConstants.ObjTypeDealSearch, DealManagementConstants.GetProcDealList);
            OracleObject recordObject = this.SetParamsDealList(dealSearchModel);
            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();
            OracleParameter parameter;
            parameter = new OracleParameter("P_START", OracleDbType.Number) { Direction = ParameterDirection.Input, Value = dealSearchModel.StartRow };
            parameters.Add(parameter);
            parameter = new OracleParameter("P_END", OracleDbType.Number) { Direction = ParameterDirection.Input, Value = dealSearchModel.EndRow };
            parameters.Add(parameter);
            if (dealSearchModel.SortModel == null)
            {
                dealSearchModel.SortModel = new[] { new Core.Models.SortModel { ColId = null, Sort = null } };
            }

            parameter = new OracleParameter("P_SortColId", OracleDbType.VarChar) { Direction = ParameterDirection.Input, Value = dealSearchModel.SortModel[0].ColId };
            parameters.Add(parameter);
            parameter = new OracleParameter("P_SortOrder", OracleDbType.VarChar) { Direction = ParameterDirection.Input, Value = dealSearchModel.SortModel[0].Sort };
            parameters.Add(parameter);
            parameter = new OracleParameter("P_total", OracleDbType.Number) { Direction = ParameterDirection.Output };
            parameters.Add(parameter);
            var deals = await this.GetProcedure2<DealSearchModel>(recordObject, packageParameter, serviceDocumentResult, parameters);
            dealSearchModel.TotalRows = (this.Parameters["p_total"].Value != null && this.Parameters["p_total"].Value != DBNull.Value) ? Convert.ToInt64(this.Parameters["p_total"].Value) : 1;
            serviceDoc.DataProfile.DataModel = dealSearchModel;
            return deals;
        }

        public async Task<DealHeadModel> GetDealHead(int dealId, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(DealManagementConstants.RecordDealHead, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["DEAL_ID"]] = dealId;
            PackageParams packageParameter = this.GetPackageParams(DealManagementConstants.ObjTypeDealHead, DealManagementConstants.GetDealHead);
            var dealHeads = await this.GetProcedure2<DealHeadModel>(recordObject, packageParameter, serviceDocumentResult);
            if (dealHeads != null && dealHeads.Count > 0)
            {
                return dealHeads[0];
            }

            return null;
        }

        public async Task<ServiceDocumentResult> SetDealHead(DealHeadModel dealHeadModel)
        {
            PackageParams packageParameter = this.GetPackageParams(DealManagementConstants.ObjTypeDealHead, DealManagementConstants.SetDealHead);
            OracleObject recordObject = this.SetParamsDealHead(dealHeadModel);
            var result = await this.SetProcedure(recordObject, packageParameter);
            if (this.ObjResult != null && dealHeadModel.DEAL_ID == null)
            {
                this.DealId = Convert.ToInt32(this.ObjResult["DEAL_ID"]);
            }
            else
            {
                this.DealId = dealHeadModel.DEAL_ID;
            }

            return result;
        }

        public async Task<List<DealDetailModel>> GetDealDetail(int dealId, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(DealManagementConstants.RecordDealDetail, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["DEAL_ID"]] = dealId;
            PackageParams packageParameter = this.GetPackageParams(DealManagementConstants.ObjTypeDealDetail, DealManagementConstants.GetDealDetail);
            return await this.GetProcedure2<DealDetailModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ServiceDocumentResult> SetDealDetail(DealDetailModel dealDetailModel)
        {
            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();
            PackageParams packageParameter = this.GetPackageParams(DealManagementConstants.ObjTypeDealDetail, DealManagementConstants.SetDealDetail);
            OracleParameter parameter;
            parameter = new OracleParameter("RESULT", OracleDbType.Boolean)
            {
                Direction = ParameterDirection.ReturnValue
            };
            parameters.Add(parameter);
            parameter = new OracleParameter(packageParameter.ObjectTypeTable, OracleDbType.Table)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                ObjectTypeName = packageParameter.ObjectTypeName,
                Value = this.SetParamsDealDetail(dealDetailModel)
            };
            parameters.Add(parameter);
            return await this.SetProcedure(null, packageParameter, parameters);
        }

        public async Task<List<DealItemLocationModel>> GetDealItemLocations(int dealId, ServiceDocumentResult serviceDocumentResult)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(DealManagementConstants.RecordDealItemLocation, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["DEAL_ID"]] = dealId;
            PackageParams packageParameter = this.GetPackageParams(DealManagementConstants.ObjTypeDealItemLocation, DealManagementConstants.GetDealItemLocation);
            return await this.GetProcedure2<DealItemLocationModel>(recordObject, packageParameter, serviceDocumentResult);
        }

        public async Task<ServiceDocumentResult> SetDealItemLocation(DealItemLocationModel dealItemLocationModel)
        {
            OracleParameterCollection parameters;
            parameters = this.Parameters;
            parameters.Clear();
            PackageParams packageParameter = this.GetPackageParams(DealManagementConstants.ObjTypeDealItemLocation, DealManagementConstants.SetDealItemLocation);
            OracleParameter parameter;
            parameter = new OracleParameter("RESULT", OracleDbType.Boolean)
            {
                Direction = ParameterDirection.ReturnValue
            };
            parameters.Add(parameter);
            parameter = new OracleParameter(packageParameter.ObjectTypeTable, OracleDbType.Table)
            {
                Direction = System.Data.ParameterDirection.InputOutput,
                ObjectTypeName = packageParameter.ObjectTypeName,
                Value = this.SetParamsDealItemLocation(dealItemLocationModel)
            };
            parameters.Add(parameter);
            return await this.SetProcedure(null, packageParameter, parameters);
        }

        public async Task<List<DealHeadPrintModel>> GetDealPrintData(int dealId, ServiceDocumentResult serviceDocumentResult)
        {
            PackageParams packageParameter = this.GetPackageParams(DealManagementConstants.ObjTypeDealPrint, DealManagementConstants.GetProcDealPrint);
            OracleObject recordObject = this.SetParamsDealPrint(dealId);
            OracleTable pDealPrintMstTab = await this.GetProcedure(recordObject, packageParameter, serviceDocumentResult);

            List<DealHeadPrintModel> lstHeader = Mapper.Map<List<DealHeadPrintModel>>(pDealPrintMstTab);
            if (lstHeader != null)
            {
                for (int i = 0; i < lstHeader.Count; i++)
                {
                    OracleObject obj = (OracleObject)pDealPrintMstTab[i];
                    OracleTable dealDetailsObj = (Devart.Data.Oracle.OracleTable)obj["DEAL_DTL_TAB"];
                    if (dealDetailsObj.Count > 0)
                    {
                        lstHeader[i].DealDetails.AddRange(Mapper.Map<List<DealDetailPrintModel>>(dealDetailsObj));
                    }
                }
            }

            return lstHeader;
        }

        public async Task<List<DealDetailIdModel>> GetAllDetailIdsOfDeal(int dealRequestId, ServiceDocumentResult serviceDocumentResult)
        {
            try
            {
                this.Connection.Open();
                OracleParameterCollection parameters;
                parameters = this.Parameters;
                parameters.Clear();
                OracleParameter parameter;
                parameter = new OracleParameter("RESULT", OracleDbType.Boolean, ParameterDirection.ReturnValue);
                parameters.Add(parameter);
                parameter = new OracleParameter("P_DEAL_ID", OracleDbType.Number)
                {
                    Direction = System.Data.ParameterDirection.Input,
                    Value = dealRequestId
                };
                parameters.Add(parameter);
                parameter = new OracleParameter(DealManagementConstants.DealDetailIdTbl, Devart.Data.Oracle.OracleDbType.Table)
                {
                    Direction = System.Data.ParameterDirection.InputOutput,
                    ObjectTypeName = DealManagementConstants.ObjTypeDealDetailId,
                };

                parameters.Add(parameter);
                PackageParams packageParameter = this.GetPackageParams(DealManagementConstants.ObjTypeDealDetailId, DealManagementConstants.GetDealDetailId);
                return await this.GetProcedure2<DealDetailIdModel>(null, packageParameter, serviceDocumentResult, parameters);
            }
            finally
            {
                this.Connection.Close();
            }
        }

        private OracleObject SetParamsDeal(SupplierDealModel dealModel)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(DealManagementConstants.RecordDeal, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["DEAL_ID"]] = dealModel.DEAL_ID;
            recordObject[recordType.Attributes["SUPPLIER"]] = dealModel.Supplier;
            recordObject[recordType.Attributes["TYPE"]] = dealModel.DealType;
            recordObject[recordType.Attributes["DEAL_COMP_TYPE"]] = dealModel.DealCompType;
            recordObject[recordType.Attributes["ACTIVE_DATE"]] = dealModel.StartDate;
            recordObject[recordType.Attributes["CLOSE_DATE"]] = dealModel.CloseDate;
            recordObject[recordType.Attributes["OPERATION"]] = dealModel.Operation;
            recordObject[recordType.Attributes["CURRENCY_CODE"]] = dealModel.CurrencyCode;
            recordObject[recordType.Attributes["CREATED_BY"]] = this.GetCreatedBy(dealModel.CreatedBy);
            recordObject[recordType.Attributes["LAST_UPDATED_BY"]] = FxContext.Context.Name;

            // Deal Location Object
            OracleType dtlTableType = OracleType.GetObjectType(DealManagementConstants.ObjTypeDealItemLocation, this.Connection);
            OracleType dtlRecordType = OracleType.GetObjectType(DealManagementConstants.RecordDealItemLocation, this.Connection);
            OracleTable pDealItemLocationTab = new OracleTable(dtlTableType);
            foreach (DealItemLocNewModel dealItemLocModel in dealModel.ListOfDealItemLocations)
            {
                var dtlRecordObject = new OracleObject(dtlRecordType);
                dtlRecordObject[dtlRecordType.Attributes["DEAL_ID"]] = dealModel.DEAL_ID;
                dtlRecordObject[dtlRecordType.Attributes["DEAL_DETAIL_ID"]] = dealItemLocModel.DealDetailId;
                dtlRecordObject[dtlRecordType.Attributes["DEAL_MERCH_LVL"]] = dealItemLocModel.MerchLevel;
                dtlRecordObject[dtlRecordType.Attributes["DEAL_MERCH_LVL_VALUE"]] = dealItemLocModel.MerchLevelValue;
                dtlRecordObject[dtlRecordType.Attributes["DEAL_DISCOUNT_TYPE"]] = dealItemLocModel.DiscountType;
                dtlRecordObject[dtlRecordType.Attributes["DEAL_DISCOUNT_VALUE"]] = dealItemLocModel.DiscountValue;
                dtlRecordObject[dtlRecordType.Attributes["ITEM"]] = dealItemLocModel.Item;
                dtlRecordObject[dtlRecordType.Attributes["EFFECTIVE_START_DATE"]] = dealModel.StartDate;
                dtlRecordObject[dtlRecordType.Attributes["EFFECTIVE_END_DATE"]] = dealModel.CloseDate;
                dtlRecordObject[dtlRecordType.Attributes["COMMENTS"]] = dealItemLocModel.Comments;
                dtlRecordObject[dtlRecordType.Attributes["LAST_UPDATED_BY"]] = FxContext.Context.Name;
                dtlRecordObject[dtlRecordType.Attributes["OPERATION"]] = dealItemLocModel.Operation;

                if (dealItemLocModel.Operation == "I")
                {
                    dtlRecordObject[dtlRecordType.Attributes["CREATED_BY"]] = FxContext.Context.Name;
                }

                pDealItemLocationTab.Add(dtlRecordObject);
            }

            recordObject[recordType.Attributes["DEAL_ITEMLOC"]] = pDealItemLocationTab;
            return recordObject;
        }

        private OracleObject SetParamsDealPrint(int dealId)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(DealManagementConstants.ObjRecDealPrint, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);

            recordObject[recordType.Attributes["DEAL_ID"]] = dealId;
            return recordObject;
        }

        private OracleObject SetParamsDealList(DealSearchModel dealSearchModel)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(DealManagementConstants.RecDealSearch, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["SUPPLIER"]] = dealSearchModel.Supplier;
            recordObject[recordType.Attributes["ITEM_BARCODE"]] = dealSearchModel.ItemBarcode;
            recordObject[recordType.Attributes["ITEM_DESCRIPTION"]] = dealSearchModel.ItemDescription;
            recordObject[recordType.Attributes["DEAL_ORG_LVL"]] = dealSearchModel.OrgLevel;
            recordObject[recordType.Attributes["DEAL_ORG_LVL_VALUE"]] = dealSearchModel.OrgLevelValue;
            recordObject[recordType.Attributes["DEAL_MERCH_LVL"]] = dealSearchModel.MerchLevel;
            recordObject[recordType.Attributes["DEAL_MERCH_LVL_VALUE"]] = dealSearchModel.MerchLevelValue;
            recordObject[recordType.Attributes["DEAL_ID"]] = dealSearchModel.DEAL_ID;
            recordObject[recordType.Attributes["DEAL_DESC"]] = dealSearchModel.DealDescription;
            recordObject[recordType.Attributes["DEAL_TYPE_ID"]] = dealSearchModel.DealTypeId;
            recordObject[recordType.Attributes["DEAL_STATUS_ID"]] = dealSearchModel.DealStatusId;
            recordObject[recordType.Attributes["DATE_TYPE_ID"]] = dealSearchModel.DateType;
            recordObject[recordType.Attributes["FROM_DATE"]] = dealSearchModel.FromDate;
            recordObject[recordType.Attributes["TO_DATE"]] = dealSearchModel.ToDate;
            return recordObject;
        }

        private OracleObject SetParamsDealHead(DealHeadModel dealHeadModel)
        {
            this.Connection.Open();
            OracleType recordType = OracleType.GetObjectType(DealManagementConstants.RecordDealHead, this.Connection);
            OracleObject recordObject = new OracleObject(recordType);
            recordObject[recordType.Attributes["DEAL_ID"]] = dealHeadModel.DEAL_ID;
            recordObject[recordType.Attributes["CREATED_BY"]] = this.GetCreatedBy(dealHeadModel.CreatedBy);
            recordObject[recordType.Attributes["LAST_UPDATED_BY"]] = FxContext.Context.Name;
            recordObject[recordType.Attributes["SUPPLIER"]] = dealHeadModel.Supplier;
            recordObject[recordType.Attributes["TYPE"]] = dealHeadModel.DealType;
            recordObject[recordType.Attributes["DEAL_DESC"]] = dealHeadModel.Description;
            recordObject[recordType.Attributes["CURRENCY_CODE"]] = dealHeadModel.CurrencyCode;
            recordObject[recordType.Attributes["EXT_REF_NO"]] = dealHeadModel.ExternalRefNo;
            recordObject[recordType.Attributes["ORDER_NO"]] = dealHeadModel.OrderNo;
            recordObject[recordType.Attributes["RECALC_APPROVED_ORDERS"]] = dealHeadModel.RecalculateApprovedOrders;
            recordObject[recordType.Attributes["ACTIVE_DATE"]] = dealHeadModel.StartDate;
            recordObject[recordType.Attributes["CLOSE_DATE"]] = dealHeadModel.CloseDate;
            recordObject[recordType.Attributes["COMMENTS"]] = dealHeadModel.Comments;
            recordObject[recordType.Attributes["OPERATION"]] = dealHeadModel.Operation;
            recordObject[recordType.Attributes["STATUS"]] = dealHeadModel.Status;
            return recordObject;
        }

        private OracleTable SetParamsDealDetail(DealDetailModel dealDetail)
        {
            this.Connection.Open();
            OracleType dealDetailTableType = OracleType.GetObjectType(DealManagementConstants.ObjTypeDealDetail, this.Connection);
            OracleType recordType = OracleType.GetObjectType(DealManagementConstants.RecordDealDetail, this.Connection);
            OracleTable pDealDetailTab = new OracleTable(dealDetailTableType);
            foreach (DealDetailModel dealDetailModel in dealDetail.DealDetails)
            {
                OracleObject recordObject = new OracleObject(recordType);
                recordObject[recordType.Attributes["DEAL_ID"]] = dealDetailModel.DealId;
                recordObject[recordType.Attributes["DEAL_DETAIL_ID"]] = dealDetailModel.DealDetailId;
                recordObject[recordType.Attributes["LAST_UPDATED_BY"]] = FxContext.Context.Name;
                recordObject[recordType.Attributes["DEAL_COMP_TYPE"]] = dealDetailModel.DealCompType;
                recordObject[recordType.Attributes["APPLICATION_ORDER"]] = dealDetailModel.ApplicationOrder;
                recordObject[recordType.Attributes["COLLECT_START_DATE"]] = dealDetailModel.CollectStartDate;
                recordObject[recordType.Attributes["COLLECT_END_DATE"]] = dealDetailModel.CollectEndDate;
                recordObject[recordType.Attributes["COST_APPL_IND"]] = dealDetailModel.CostApplicationInd;
                recordObject[recordType.Attributes["PRICE_COST_APPL_IND"]] = dealDetailModel.PriceCostApplicationInd;
                recordObject[recordType.Attributes["DEAL_CLASS"]] = dealDetailModel.DealClass;
                recordObject[recordType.Attributes["THRESHOLD_VALUE_TYPE"]] = dealDetailModel.ThresholdValueType;
                recordObject[recordType.Attributes["TRAN_DISCOUNT_IND"]] = dealDetailModel.TranDiscountInd;
                recordObject[recordType.Attributes["COMMENTS"]] = dealDetailModel.Comments;
                recordObject[recordType.Attributes["OPERATION"]] = dealDetailModel.Operation;
                if (dealDetailModel.Operation == "I")
                {
                    recordObject[recordType.Attributes["CREATED_BY"]] = FxContext.Context.Name;
                }

                pDealDetailTab.Add(recordObject);
            }

            return pDealDetailTab;
        }

        private OracleTable SetParamsDealItemLocation(DealItemLocationModel dealItemLocationModel)
        {
            this.Connection.Open();
            OracleType dealItemLocationTableType = OracleType.GetObjectType(DealManagementConstants.ObjTypeDealItemLocation, this.Connection);
            OracleType recordType = OracleType.GetObjectType(DealManagementConstants.RecordDealItemLocation, this.Connection);
            OracleTable pDealItemLocationTab = new OracleTable(dealItemLocationTableType);
            foreach (DealItemLocationModel dealItemLocModel in dealItemLocationModel.ListOfDealItemLocations)
            {
                OracleObject recordObject = new OracleObject(recordType);
                recordObject[recordType.Attributes["DEAL_ID"]] = dealItemLocModel.DealId;
                recordObject[recordType.Attributes["DEAL_DETAIL_ID"]] = dealItemLocModel.DealDetailId;
                recordObject[recordType.Attributes["SEQ_NO"]] = dealItemLocModel.SeqNo;
                recordObject[recordType.Attributes["DEAL_ORG_LVL"]] = dealItemLocModel.OrgLevel;
                recordObject[recordType.Attributes["DEAL_ORG_LVL_VALUE"]] = dealItemLocModel.OrgLevelValue;
                recordObject[recordType.Attributes["DEAL_MERCH_LVL"]] = dealItemLocModel.MerchLevel;
                recordObject[recordType.Attributes["DEAL_MERCH_LVL_VALUE"]] = dealItemLocModel.MerchLevelValue;
                recordObject[recordType.Attributes["DEAL_DISCOUNT_TYPE"]] = dealItemLocModel.DiscountType;
                recordObject[recordType.Attributes["DEAL_DISCOUNT_VALUE"]] = dealItemLocModel.DiscountValue;
                recordObject[recordType.Attributes["DEAL_DISCOUNT_THRESHOLD_QTY"]] = dealItemLocModel.ThresholdQty;
                recordObject[recordType.Attributes["DEAL_THRESHOLD_FREE_QTY"]] = dealItemLocModel.ThresholdFreeQty;
                recordObject[recordType.Attributes["DEAL_THRESHOLD_FREE_ITEM"]] = dealItemLocModel.ThresholdFreeItem;
                recordObject[recordType.Attributes["ITEM"]] = dealItemLocModel.Item;
                recordObject[recordType.Attributes["EXCL_IND"]] = dealItemLocModel.ExclInd;
                recordObject[recordType.Attributes["EFFECTIVE_START_DATE"]] = dealItemLocModel.EffectiveStartDate;
                recordObject[recordType.Attributes["EFFECTIVE_END_DATE"]] = dealItemLocModel.EffectiveEndDate;
                recordObject[recordType.Attributes["LAST_UPDATED_BY"]] = FxContext.Context.Name;
                recordObject[recordType.Attributes["OPERATION"]] = dealItemLocModel.Operation;
                if (dealItemLocModel.Operation == "I")
                {
                    recordObject[recordType.Attributes["CREATED_BY"]] = FxContext.Context.Name;
                }

                pDealItemLocationTab.Add(recordObject);
            }

            return pDealItemLocationTab;
        }
    }
}
