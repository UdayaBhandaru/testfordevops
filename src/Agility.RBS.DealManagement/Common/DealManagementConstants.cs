﻿// <copyright file="DealManagementConstants.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
namespace Agility.RBS.DealManagement.Common
{
    internal static class DealManagementConstants
    {
        // Result
        public const string OracleParameterResult = "RESULT";

        // Item Package
        public const string PackageName = "UTL_DEAL";

        // Deal Search
        public const string GetProcDealList = "GETDEALSEARCH";
        public const string ObjTypeDealSearch = "DEAL_SEARCH_TAB";
        public const string RecDealSearch = "DEAL_SEARCH_REC";

        // Supplier Deal
        public const string GetDeal = "GETDEAL";
        public const string SetDeal = "SETDEAL";
        public const string RecordDeal = "DEAL_REC";
        public const string ObjTypeDeal = "DEAL_TAB";

        // Deal Header Ids
        public const string DealType = "DLHT";
        public const string DealStatus = "DEALSTATUS";
        public const string DateType = "DATETYPE";

        // Deal Header
        public const string RecordDealHead = "DEAL_HEAD_REC";
        public const string ObjTypeDealHead = "DEAL_HEAD_TAB";
        public const string GetDealHead = "GETDEALHEAD";
        public const string SetDealHead = "SETDEALHEAD";

        // Domain
        public const string DomainHdrIndicator = "INDICATOR";
        public const string DealCompType = "DEALCMPTYP";
        public const string DomainHdrOrgLevel = "COSTCHGLVL";
        public const string DomainHdrMerchLevel = "DIML";
        public const string DomainHdrChangeType = "DLL2";

        // Deal Detail
        public const string RecordDealDetail = "DEAL_DETAIL_REC";
        public const string ObjTypeDealDetail = "DEAL_DETAIL_TAB";
        public const string GetDealDetail = "GETDEALDETAIL";
        public const string SetDealDetail = "SETDEALDETAIL";

        // Deal Item Location
        public const string RecordDealItemLocation = "DEAL_ITEMLOC_REC";
        public const string ObjTypeDealItemLocation = "DEAL_ITEMLOC_TAB";
        public const string GetDealItemLocation = "GETDEALITEMLOC";
        public const string SetDealItemLocation = "SETDEALITEMLOC";

        // Deal Print
        public const string ObjTypeDealPrint = "DEAL_PRINT_TAB";
        public const string ObjRecDealPrint = "DEAL_PRINT_REC";
        public const string GetProcDealPrint = "GETDEALPRINT";

        // Deal Detail Ids
        public const string ObjTypeDealDetailId = "DEAL_DETAIL_ID_TAB";
        public const string DealDetailIdTbl = "P_DEAL_DETAIL_ID_TAB";
        public const string GetDealDetailId = "GETDEALDETAILID";
    }
}
