﻿// <copyright file="DealMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.DealManagement.Mapping
{
    using Agility.RBS.Core;
    using Agility.RBS.DealManagement.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class DealMapping : Profile
    {
        public DealMapping()
           : base("DealMapping")
        {
            this.CreateMap<OracleObject, DealSearchModel>()
                .ForMember(m => m.Supplier, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["SUPPLIER"])))
                .ForMember(m => m.SupplierName, opt => opt.MapFrom(r => r["SUPPLIER_NAME"]))
                .ForMember(m => m.ItemBarcode, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["ITEM_BARCODE"])))
                .ForMember(m => m.ItemDescription, opt => opt.MapFrom(r => r["ITEM_DESCRIPTION"]))
                .ForMember(m => m.OrgLevel, opt => opt.MapFrom(r => r["DEAL_ORG_LVL"]))
                .ForMember(m => m.OrgLevelValue, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DEAL_ORG_LVL_VALUE"])))
                .ForMember(m => m.MerchLevel, opt => opt.MapFrom(r => r["DEAL_MERCH_LVL"]))
                .ForMember(m => m.MerchLevelValue, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DEAL_MERCH_LVL_VALUE"])))
                .ForMember(m => m.DEAL_ID, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DEAL_ID"])))
                .ForMember(m => m.DealDescription, opt => opt.MapFrom(r => r["DEAL_DESC"]))
                .ForMember(m => m.DealTypeId, opt => opt.MapFrom(r => r["DEAL_TYPE_ID"]))
                .ForMember(m => m.DealType, opt => opt.MapFrom(r => r["DEAL_TYPE_DESC"]))
                .ForMember(m => m.DealStatusId, opt => opt.MapFrom(r => r["DEAL_STATUS_ID"]))
                .ForMember(m => m.DealStatus, opt => opt.MapFrom(r => r["DEAL_STATUS_DESC"]))
                .ForMember(m => m.DateType, opt => opt.MapFrom(r => r["DATE_TYPE_ID"]))
                .ForMember(m => m.FromDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["FROM_DATE"])))
                .ForMember(m => m.ToDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["TO_DATE"])))
                .ForMember(m => m.CreatedBy, opt => opt.MapFrom(r => r["CREATED_BY"]))
                .ForMember(m => m.ModifiedBy, opt => opt.MapFrom(r => r["LAST_UPDATED_BY"]))
                .ForMember(m => m.CreatedDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["CREATED_DATE"])))
                .ForMember(m => m.ModifiedDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["LAST_UPDATED_DATE"])))
                .ForMember(m => m.PError, opt => opt.MapFrom(r => r["P_ERROR"]))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "DEAL_HEAD"));

            this.CreateMap<OracleObject, DealDetailModel>()
                 .ForMember(m => m.DealId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DEAL_ID"])))
                .ForMember(m => m.DealDetailId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DEAL_DETAIL_ID"])))
                .ForMember(m => m.DealCompDesc, opt => opt.MapFrom(r => r["DEAL_COMP_DESC"]))
                .ForMember(m => m.DealCompType, opt => opt.MapFrom(r => r["DEAL_COMP_TYPE"]))
                .ForMember(m => m.ApplicationOrder, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["APPLICATION_ORDER"])))
                .ForMember(m => m.CollectStartDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(
                    r["COLLECT_START_DATE"])))
                .ForMember(m => m.CollectEndDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["COLLECT_END_DATE"])))
                .ForMember(m => m.CostApplicationInd, opt => opt.MapFrom(r => r["COST_APPL_IND"]))
                .ForMember(m => m.PriceCostApplicationInd, opt => opt.MapFrom(r => r["PRICE_COST_APPL_IND"]))
                .ForMember(m => m.DealClass, opt => opt.MapFrom(r => r["DEAL_CLASS"]))
                .ForMember(m => m.DealClassDesc, opt => opt.MapFrom(r => r["DEAL_CLASS_DESC"]))
                .ForMember(m => m.ThresholdValueType, opt => opt.MapFrom(r => r["THRESHOLD_VALUE_TYPE"]))
                .ForMember(m => m.TranDiscountInd, opt => opt.MapFrom(r => r["TRAN_DISCOUNT_IND"]))
                .ForMember(m => m.Comments, opt => opt.MapFrom(r => r["COMMENTS"]))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"))
                .ForMember(m => m.CreatedBy, opt => opt.MapFrom(r => r["CREATED_BY"]))
                .ForMember(m => m.ModifiedBy, opt => opt.MapFrom(r => r["LAST_UPDATED_BY"]))
                .ForMember(m => m.CreatedDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["CREATED_DATE"])))
                .ForMember(m => m.ModifiedDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["LAST_UPDATED_DATE"])))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "DEAL_DETAIL"));

            this.CreateMap<OracleObject, DealHeadModel>()
                .ForMember(m => m.DEAL_ID, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DEAL_ID"])))
                .ForMember(m => m.Description, opt => opt.MapFrom(r => r["DEAL_DESC"]))
                .ForMember(m => m.Supplier, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["SUPPLIER"])))
                .ForMember(m => m.CurrencyCode, opt => opt.MapFrom(r => r["CURRENCY_CODE"]))
                .ForMember(m => m.ExternalRefNo, opt => opt.MapFrom(r => r["EXT_REF_NO"]))
                .ForMember(m => m.OrderNo, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["ORDER_NO"])))
                .ForMember(m => m.RecalculateApprovedOrders, opt => opt.MapFrom(r => r["RECALC_APPROVED_ORDERS"]))
                .ForMember(m => m.StartDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["ACTIVE_DATE"])))
                .ForMember(m => m.CloseDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["CLOSE_DATE"])))
                .ForMember(m => m.Comments, opt => opt.MapFrom(r => r["COMMENTS"]))
                .ForMember(m => m.Status, opt => opt.MapFrom(r => r["STATUS"]))
                .ForMember(m => m.DealType, opt => opt.MapFrom(r => r["TYPE"]))
                .ForMember(m => m.FileCount, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["FILE_CNT"])))
                .ForMember(m => m.CreatedBy, opt => opt.MapFrom(r => r["CREATED_BY"]))
                .ForMember(m => m.ModifiedBy, opt => opt.MapFrom(r => r["LAST_UPDATED_BY"]))
                .ForMember(m => m.CreatedDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["CREATED_DATE"])))
                .ForMember(m => m.ModifiedDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["LAST_UPDATED_DATE"])))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "DEAL_HEAD"));

            this.CreateMap<OracleObject, DealItemLocationModel>()
                 .ForMember(m => m.DealId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DEAL_ID"])))
                .ForMember(m => m.DealDetailId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DEAL_DETAIL_ID"])))
                .ForMember(m => m.SeqNo, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["SEQ_NO"])))
                .ForMember(m => m.OrgLevel, opt => opt.MapFrom(r => r["DEAL_ORG_LVL"]))
                .ForMember(m => m.OrgLevelDesc, opt => opt.MapFrom(r => r["DEAL_ORG_LVL_DESC"]))
                .ForMember(m => m.OrgLevelValue, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DEAL_ORG_LVL_VALUE"])))
                .ForMember(m => m.OrgLevelValueDesc, opt => opt.MapFrom(r => r["DEAL_ORG_LVL_VALUE_DESC"]))
                .ForMember(m => m.MerchLevel, opt => opt.MapFrom(r => r["DEAL_MERCH_LVL"]))
                .ForMember(m => m.MerchLevelDesc, opt => opt.MapFrom(r => r["DEAL_MERCH_LVL_DESC"]))
                .ForMember(m => m.MerchLevelValue, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DEAL_MERCH_LVL_VALUE"])))
                .ForMember(m => m.MerchLevelValueDesc, opt => opt.MapFrom(r => r["DEAL_MERCH_LVL_VALUE_DESC"]))
                .ForMember(m => m.DiscountType, opt => opt.MapFrom(r => r["DEAL_DISCOUNT_TYPE"]))
                .ForMember(m => m.DiscountTypeDesc, opt => opt.MapFrom(r => r["DEAL_DISCOUNT_TYPE_DESC"]))
                .ForMember(m => m.DiscountValue, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["DEAL_DISCOUNT_VALUE"])))
                .ForMember(m => m.ThresholdQty, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DEAL_DISCOUNT_THRESHOLD_QTY"])))
                .ForMember(m => m.ThresholdFreeQty, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DEAL_THRESHOLD_FREE_QTY"])))
                .ForMember(m => m.ThresholdFreeItem, opt => opt.MapFrom(r => r["DEAL_THRESHOLD_FREE_ITEM"]))
                .ForMember(m => m.Item, opt => opt.MapFrom(r => r["ITEM"]))
                .ForMember(m => m.ItemDesc, opt => opt.MapFrom(r => r["ITEM_DESC"]))
                .ForMember(m => m.ExclInd, opt => opt.MapFrom(r => r["EXCL_IND"]))
                .ForMember(m => m.EffectiveStartDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["EFFECTIVE_START_DATE"])))
                .ForMember(m => m.EffectiveEndDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["EFFECTIVE_END_DATE"])))
                .ForMember(m => m.CreatedDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["CREATED_DATE"])))
                .ForMember(m => m.CreatedBy, opt => opt.MapFrom(r => r["CREATED_BY"]))
                .ForMember(m => m.ModifiedBy, opt => opt.MapFrom(r => r["LAST_UPDATED_BY"]))
                .ForMember(m => m.ModifiedDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["LAST_UPDATED_DATE"])))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "DEAL_ITEMLOC"));

            this.CreateMap<OracleObject, DealHeadPrintModel>()
                .ForMember(m => m.DealType, opt => opt.MapFrom(r => r["TYPE_DESC"]))
                .ForMember(m => m.DEAL_ID, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DEAL_ID"])))
                .ForMember(m => m.Description, opt => opt.MapFrom(r => r["DEAL_DESC"]))
                .ForMember(m => m.Supplier, opt => opt.MapFrom(r => r["SUPPLIER_NAME"]))
                .ForMember(m => m.CurrencyCode, opt => opt.MapFrom(r => r["CURRENCY_CODE"]))
                .ForMember(m => m.ExternalRefNo, opt => opt.MapFrom(r => r["EXT_REF_NO"]))
                .ForMember(m => m.OrderNo, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["ORDER_NO"])))
                .ForMember(m => m.RecalculatedOrders, opt => opt.MapFrom(r => r["RECALC_APPROVED_ORDERS"]))
                .ForMember(m => m.StartDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["ACTIVE_DATE"])))
                .ForMember(m => m.CloseDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["CLOSE_DATE"])))
                .ForMember(m => m.Comments, opt => opt.MapFrom(r => r["COMMENTS"]))
                .ForMember(m => m.CreatedBy, opt => opt.MapFrom(r => r["CREATED_BY"]))
                .ForMember(m => m.ModifiedBy, opt => opt.MapFrom(r => r["LAST_UPDATE_ID"]))
                .ForMember(m => m.CreatedDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["CREATE_DATETIME"])))
                .ForMember(m => m.ModifiedDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["LAST_UPDATE_DATETIME"])));

            this.CreateMap<OracleObject, DealDetailPrintModel>()
                 .ForMember(m => m.DealId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DEAL_ID"])))
                .ForMember(m => m.DealDetailId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DEAL_DETAIL_ID"])))
                .ForMember(m => m.DealCompDesc, opt => opt.MapFrom(r => r["DEAL_COMP_DESC"]))
                .ForMember(m => m.DealCompType, opt => opt.MapFrom(r => r["DEAL_COMP_TYPE"]))
                .ForMember(m => m.ApplicationOrder, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["APPLICATION_ORDER"])))
                .ForMember(m => m.CollectStartDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(
                    r["COLLECT_START_DATE"])))
                .ForMember(m => m.CollectEndDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["COLLECT_END_DATE"])))
                .ForMember(m => m.CostApplicationInd, opt => opt.MapFrom(r => r["COST_APPL_IND"]))
                .ForMember(m => m.PriceCostApplicationInd, opt => opt.MapFrom(r => r["PRICE_COST_APPL_IND"]))
                .ForMember(m => m.ThresholdValueType, opt => opt.MapFrom(r => r["THRESHOLD_VALUE_TYPE"]))
                .ForMember(m => m.TranDiscountInd, opt => opt.MapFrom(r => r["TRAN_DISCOUNT_IND"]))
                .ForMember(m => m.Comments, opt => opt.MapFrom(r => r["COMMENTS"]))
                .ForMember(m => m.CreatedBy, opt => opt.MapFrom(r => r["CREATED_BY"]))
                .ForMember(m => m.ModifiedBy, opt => opt.MapFrom(r => r["LAST_UPDATE_ID"]))
                .ForMember(m => m.CreatedDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["CREATE_DATETIME"])))
                .ForMember(m => m.ModifiedDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["LAST_UPDATE_DATETIME"])));

            this.CreateMap<OracleObject, DealDetailIdModel>()
                .ForMember(m => m.DetailId, opt => opt.MapFrom(r => r["DEAL_DETAIL_ID"]));
        }
    }
}
