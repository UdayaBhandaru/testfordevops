﻿// <copyright file="SupplierDealMapping.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.DealManagement.Mapping
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.RBS.Core;
    using Agility.RBS.DealManagement.Models;
    using AutoMapper;
    using Devart.Data.Oracle;

    public class SupplierDealMapping : Profile
    {
        public SupplierDealMapping()
            : base("SupplierDealMapping")
        {
            this.CreateMap<OracleObject, DealSearchNewModel>()
                .ForMember(m => m.Supplier, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["SUPPLIER"])))
                .ForMember(m => m.SupplierName, opt => opt.MapFrom(r => r["SUPPLIER_NAME"]))
                .ForMember(m => m.DEAL_ID, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DEAL_ID"])))
                .ForMember(m => m.DealDescription, opt => opt.MapFrom(r => r["DEAL_DESC"]))
                .ForMember(m => m.DealTypeId, opt => opt.MapFrom(r => r["DEAL_TYPE_ID"]))
                .ForMember(m => m.DealType, opt => opt.MapFrom(r => r["DEAL_TYPE_DESC"]))
                .ForMember(m => m.FromDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["FROM_DATE"])))
                .ForMember(m => m.ToDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["TO_DATE"])))
                .ForMember(m => m.CreatedBy, opt => opt.MapFrom(r => r["CREATED_BY"]))
                .ForMember(m => m.ModifiedBy, opt => opt.MapFrom(r => r["LAST_UPDATED_BY"]))
                .ForMember(m => m.CreatedDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["CREATED_DATE"])))
                .ForMember(m => m.ModifiedDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["LAST_UPDATED_DATE"])))
                .ForMember(m => m.PError, opt => opt.MapFrom(r => r["P_ERROR"]))
                .ForMember(m => m.TableName, opt => opt.MapFrom(r => "DEAL_HEAD"));

            this.CreateMap<OracleObject, SupplierDealModel>()
                .ForMember(m => m.DEAL_ID, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DEAL_ID"])))
                .ForMember(m => m.Supplier, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["SUPPLIER"])))
                .ForMember(m => m.StartDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["ACTIVE_DATE"])))
                .ForMember(m => m.CloseDate, opt => opt.MapFrom(r => OracleNullHandler.DbNullDateTimeHandler(r["CLOSE_DATE"])))
                .ForMember(m => m.DealType, opt => opt.MapFrom(r => r["TYPE"]))
                .ForMember(m => m.DealCompType, opt => opt.MapFrom(r => r["DEAL_COMP_TYPE"]))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));

            this.CreateMap<OracleObject, DealItemLocNewModel>()
                .ForMember(m => m.DealId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DEAL_ID"])))
                .ForMember(m => m.DealDetailId, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DEAL_DETAIL_ID"])))
                .ForMember(m => m.MerchLevel, opt => opt.MapFrom(r => r["DEAL_MERCH_LVL"]))
                .ForMember(m => m.MerchLevelDesc, opt => opt.MapFrom(r => r["DEAL_MERCH_LVL_DESC"]))
                .ForMember(m => m.MerchLevelValue, opt => opt.MapFrom(r => OracleNullHandler.DbNullIntHandler(r["DEAL_MERCH_LVL_VALUE"])))
                .ForMember(m => m.MerchLevelValueDesc, opt => opt.MapFrom(r => r["DEAL_MERCH_LVL_VALUE_DESC"]))
                .ForMember(m => m.DiscountType, opt => opt.MapFrom(r => r["DEAL_DISCOUNT_TYPE"]))
                .ForMember(m => m.DiscountTypeDesc, opt => opt.MapFrom(r => r["DEAL_DISCOUNT_TYPE_DESC"]))
                .ForMember(m => m.DiscountValue, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["DEAL_DISCOUNT_VALUE"])))
                .ForMember(m => m.Item, opt => opt.MapFrom(r => r["ITEM"]))
                .ForMember(m => m.ItemDesc, opt => opt.MapFrom(r => r["ITEM_DESC"]))
                .ForMember(m => m.Comments, opt => opt.MapFrom(r => r["COMMENTS"]))
                .ForMember(m => m.OldCost, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["OLD_COST"])))
                .ForMember(m => m.NewCost, opt => opt.MapFrom(r => OracleNullHandler.DbNullDecimalHandler(r["NEW_COST"])))
                .ForMember(m => m.Operation, opt => opt.MapFrom(r => "U"));
        }
    }
}
