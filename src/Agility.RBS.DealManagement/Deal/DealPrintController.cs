﻿// <copyright file="DealPrintController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.DealManagement.Deal
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.DealManagement.Models;
    using Agility.RBS.DealManagement.Repositories;
    using Agility.RBS.ItemManagement.Item.Repositories;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    [Route("api/[controller]/[action]")]
    public class DealPrintController : Controller
    {
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly ServiceDocument<DealHeadPrintModel> serviceDocument;
        private readonly DealRepository dealRepository;
        private int dealId;

        public DealPrintController(
            ServiceDocument<DealHeadPrintModel> serviceDocument,
            DealRepository dealRepository,
        DomainDataRepository domainDataRepository,
        ItemSelectRepository itemSelectRepository,
        ILogger<DealPrintController> logger)
        {
            this.serviceDocument = serviceDocument;
            this.dealRepository = dealRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        /// <summary>
        /// This API invoked for fetching Deal Head and Details data when Print button clicked.
        /// </summary>
        /// <param name="dealId">Deal Id</param>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<DealHeadPrintModel>> List(int dealId)
        {
            this.dealId = dealId;
            await this.serviceDocument.ToListAsync(this.GetDealPrintData);
            this.serviceDocument.LocalizationData.AddData("RbsPrint");
            return this.serviceDocument;
        }

        private async Task<List<DealHeadPrintModel>> GetDealPrintData()
        {
            try
            {
                var printData = await this.dealRepository.GetDealPrintData(this.dealId, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return printData;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }
    }
}
