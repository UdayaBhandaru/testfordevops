﻿// <copyright file="DealHeadController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.DealManagement.Deal
{
    using System;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.DealManagement.Common;
    using Agility.RBS.DealManagement.Models;
    using Agility.RBS.DealManagement.Repositories;
    using Agility.RBS.Inbox.Models;
    using Agility.RBS.Inbox.Repository;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Mvc;

    [Route("api/[controller]/[action]")]
    public class DealHeadController : Controller
    {
        private readonly DealRepository dealRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly DomainDataRepository domainDataRepository;
        private readonly InboxModel inboxModel = new InboxModel { Operation = "CHGS", ModuleName = "DEAL" };
        private readonly InboxRepository inboxRepository;
        private ServiceDocument<DealHeadModel> serviceDocumentHeader;
        private int dealId;

        public DealHeadController(
            DealRepository dealRepository,
            ServiceDocument<DealSearchModel> serviceDocument,
            ServiceDocument<DealHeadModel> serviceDocumentHeader,
            DomainDataRepository domainDataRepository,
        InboxRepository inboxRepository)
        {
            this.dealRepository = dealRepository;
            this.serviceDocumentHeader = serviceDocumentHeader;
            this.domainDataRepository = domainDataRepository;
            this.inboxRepository = inboxRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in Deal Head page when Add button clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<DealHeadModel>> New()
        {
            this.serviceDocumentHeader.New(false);
            await this.HeaderDomainData();
            return this.serviceDocumentHeader;
        }

        /// <summary>
        /// This API invoked for populating fields in the Deal Head page when Edit icon clicked.
        /// </summary>
        /// <param name="id">Deal Id</param>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<DealHeadModel>> Open(int id)
        {
            this.dealId = id;
            await this.serviceDocumentHeader.OpenAsync(this.OpenDeal);
            await this.HeaderDomainData();
            return this.serviceDocumentHeader;
        }

        /// <summary>
        /// This API invoked for saving field values in the Deal Head page when Save button clicked.
        /// </summary>
        /// <param name="serviceDocument">Service Document</param>
        /// <returns>ServiceDocument</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<DealHeadModel>> Save([FromBody]ServiceDocument<DealHeadModel> serviceDocument)
        {
            await this.serviceDocumentHeader.SaveAsync(this.DealSave);
            return this.serviceDocumentHeader;
        }

        /// <summary>
        /// This API invoked for submitting Deal to next level when Submit button clicked.
        /// </summary>
        /// <param name="serviceDocument">Service Document</param>
        /// <returns>ServiceDocument</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<DealHeadModel>> Submit([FromBody]ServiceDocument<DealHeadModel> serviceDocument)
        {
            this.serviceDocumentHeader = serviceDocument;
            if (this.serviceDocumentHeader.DataProfile.DataModel.DEAL_ID == 0 || this.serviceDocumentHeader.DataProfile.DataModel.DEAL_ID == null)
            {
                this.inboxModel.Operation = "DFT";
            }

            await this.serviceDocumentHeader.TransitAsync(this.DealSave);
            await this.inboxRepository.SetInbox(this.inboxModel, this.serviceDocumentHeader);
            return this.serviceDocumentHeader;
        }

        private async Task<ServiceDocument<DealHeadModel>> HeaderDomainData()
        {
            this.serviceDocumentHeader.DomainData.Add("dealtype", this.domainDataRepository.DomainDetailData(DealManagementConstants.DealType).Result);
            this.serviceDocumentHeader.DomainData.Add("supplier", this.domainDataRepository.SupplierDomainGet().Result);
            this.serviceDocumentHeader.DomainData.Add("indicator", this.domainDataRepository.DomainDetailData(DealManagementConstants.DomainHdrIndicator).Result);
            this.serviceDocumentHeader.DomainData.Add("currency", this.domainDataRepository.CurrencyDomainGet().Result);
            this.serviceDocumentHeader.LocalizationData.AddData("Deal");
            return this.serviceDocumentHeader;
        }

        private async Task<DealHeadModel> OpenDeal()
        {
            try
            {
                var dealHead = await this.dealRepository.GetDealHead(this.dealId, this.serviceDocumentResult);
                this.serviceDocumentHeader.Result = this.serviceDocumentResult;
                return dealHead;
            }
            catch (Exception ex)
            {
                this.serviceDocumentHeader.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> DealSave()
        {
            this.serviceDocumentHeader.Result = await this.dealRepository.SetDealHead(this.serviceDocumentHeader.DataProfile.DataModel);
            if (this.serviceDocumentHeader.Result.Type != MessageType.Success)
            {
                return false;
            }

            this.serviceDocumentHeader.DataProfile.DataModel.DEAL_ID = this.dealRepository.DealId;
            return true;
        }
    }
}
