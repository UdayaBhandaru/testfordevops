﻿// <copyright file="DealDetailController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.DealManagement.Deal
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.DealManagement.Common;
    using Agility.RBS.DealManagement.Models;
    using Agility.RBS.DealManagement.Repositories;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Mvc;

    [Route("api/[controller]/[action]")]
    public class DealDetailController : Controller
    {
        private readonly ServiceDocument<DealDetailModel> serviceDocument;
        private readonly DealRepository dealRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly DomainDataRepository domainDataRepository;
        private int dealId;

        public DealDetailController(
            ServiceDocument<DealDetailModel> serviceDocument,
            DealRepository dealRepository,
            DomainDataRepository domainDataRepository)
        {
            this.serviceDocument = serviceDocument;
            this.dealRepository = dealRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        /// <summary>
        /// This API invoked for loading grid data along with data for autocomplete, radio button fields in Deal Details Page.
        /// </summary>
        /// <param name="id">Deal Id</param>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<DealDetailModel>> List(int id)
        {
            this.dealId = id;
            this.serviceDocument.DomainData.Add("dealcomptype", this.domainDataRepository.DomainDetailData(DealManagementConstants.DealCompType).Result);
            this.serviceDocument.DomainData.Add("indicator", this.domainDataRepository.DomainDetailData(DealManagementConstants.DomainHdrIndicator).Result);
            await this.serviceDocument.ToListAsync(this.GetDealDetails);
            this.serviceDocument.LocalizationData.AddData("Deal");
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for saving grid data in the Deal Details page when Save button clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<DealDetailModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.SaveDealDetails);
            return this.serviceDocument;
        }

        private async Task<List<DealDetailModel>> GetDealDetails()
        {
            try
            {
                var itemCompanies = await this.dealRepository.GetDealDetail(this.dealId, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return itemCompanies;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> SaveDealDetails()
        {
            this.serviceDocument.Result = await this.dealRepository.SetDealDetail(this.serviceDocument.DataProfile.DataModel);
            return true;
        }
    }
}
