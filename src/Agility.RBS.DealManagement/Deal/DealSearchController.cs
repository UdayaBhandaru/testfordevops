﻿// <copyright file="DealSearchController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.DealManagement.Deal
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.DealManagement.Common;
    using Agility.RBS.DealManagement.Models;
    using Agility.RBS.DealManagement.Repositories;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [Route("api/[controller]/[action]")]
    public class DealSearchController : Controller
    {
        private readonly DealRepository dealRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly ServiceDocument<DealSearchModel> serviceDocument;
        private readonly DomainDataRepository domainDataRepository;

        public DealSearchController(
            DealRepository dealRepository,
            ServiceDocument<DealSearchModel> serviceDocument,
            DomainDataRepository domainDataRepository)
        {
            this.dealRepository = dealRepository;
            this.serviceDocument = serviceDocument;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        /// <summary>
        /// This API invoked mainly for fetching data for autocomplete, radio button fields in the Deal List page.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<DealSearchModel>> List()
        {
            this.serviceDocument.DomainData.Add("dealtype", this.domainDataRepository.DomainDetailData(DealManagementConstants.DealType).Result);
            this.serviceDocument.DomainData.Add("dealstatus", this.domainDataRepository.DomainDetailData(DealManagementConstants.DealStatus).Result);
            this.serviceDocument.DomainData.Add("datetype", this.domainDataRepository.DomainDetailData(DealManagementConstants.DateType).Result);
            this.serviceDocument.DomainData.Add("supplier", this.domainDataRepository.SupplierDomainGet().Result);
            this.serviceDocument.DomainData.Add("orgLevel", this.domainDataRepository.DomainDetailData(DealManagementConstants.DomainHdrOrgLevel).Result);
            this.serviceDocument.DomainData.Add("merchLevel", this.domainDataRepository.DomainDetailData(DealManagementConstants.DomainHdrMerchLevel).Result);
            this.serviceDocument.DomainData.Add("location", await this.domainDataRepository.CommonData("LOC"));
            this.serviceDocument.DomainData.Add("costZone", this.domainDataRepository.CommonData("CZ").Result);
            this.serviceDocument.DomainData.Add("group", this.domainDataRepository.CommonData("DEPT").Result);
            this.serviceDocument.DomainData.Add("categories", this.domainDataRepository.CommonData("CATEGORY").Result);
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for fetching Deal records based on search criteria when Search button clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<DealSearchModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.SearchDeal);
            return this.serviceDocument;
        }

        private async Task<List<DealSearchModel>> SearchDeal()
        {
            try
            {
                var deals = await this.dealRepository.DealListGetWithdocs(this.serviceDocument, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return deals;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }
    }
}
