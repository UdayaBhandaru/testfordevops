﻿// <copyright file="SupplierDealsController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.DealManagement.Deal
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core.Models;
    using Agility.RBS.DealManagement.Common;
    using Agility.RBS.DealManagement.Models;
    using Agility.RBS.DealManagement.Repositories;
    using Agility.RBS.Inbox.Models;
    using Agility.RBS.Inbox.Repository;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [Route("api/[controller]/[action]")]
    public class SupplierDealsController : Controller
    {
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly DealRepository supplierDealRepository;
        private readonly DomainDataRepository domainDataRepository;
        private readonly ServiceDocument<SupplierDealModel> serviceDocument;
        private int dealId;

        public SupplierDealsController(
            ServiceDocument<SupplierDealModel> serviceDocument,
            DealRepository supplierDealRepository,
            DomainDataRepository domainDataRepository,
            InboxRepository inboxRepository)
        {
            this.supplierDealRepository = supplierDealRepository;
            this.domainDataRepository = domainDataRepository;
            this.serviceDocument = serviceDocument;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        /// <summary>
        /// This API invoked for fetching Deal records based on supplier when Search button clicked.
        /// </summary>
        /// <param name="supplierId">supplier Id</param>
        /// <returns>List Of DealSearchNewModel</returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<List<DealSearchNewModel>> GetSupplierDealList(int supplierId)
        {
            try
            {
                var deals = await this.supplierDealRepository.DealListGetWithdocs(supplierId, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return deals;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        /// <summary>
        /// This API invoked for fetching Deal records based on supplier when Search button clicked.
        /// </summary>
        /// <param name="id">Deal Id</param>
        /// <returns>List Of DealSearchNewModel</returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<DealDomainModel> DealDomainData(int id)
        {
            try
            {
                DealDomainModel dealDomainModel = new DealDomainModel();
                dealDomainModel.DealTypes.AddRange(this.domainDataRepository.DomainDetailData(DealManagementConstants.DealType).Result);
                dealDomainModel.DealCompTypes.AddRange(this.domainDataRepository.DealCompTypeGet().Result);
                var merchLevels = this.domainDataRepository.DomainDetailData(DealManagementConstants.DomainHdrMerchLevel).Result;
                foreach (var merchLevel in merchLevels)
                {
                    string somValue = string.Empty;
                    switch (merchLevel.CodeDesc)
                    {
                        case "@MH2":
                            somValue = "Division";
                            break;
                        case "@MH3":
                            somValue = "Department";
                            break;
                        case "@MH4":
                            somValue = "Category";
                            break;
                        case "@MH5":
                            somValue = "Fineline";
                            break;
                        case "@MH6":
                            somValue = "Segment";
                            break;
                        default:
                            somValue = string.Empty;
                            break;
                    }

                    if (!string.IsNullOrEmpty(somValue))
                    {
                        merchLevel.CodeDesc = somValue;
                    }
                }

                dealDomainModel.MerchLevels.AddRange(merchLevels);

                var divisions = this.domainDataRepository.DivisionDomainGet().Result;
                List<CommonModel> lstDivision = new List<CommonModel>();
                foreach (var division in divisions)
                {
                    lstDivision.Add(new CommonModel { Id = Convert.ToString(division.Division), Name = division.DivName });
                }

                dealDomainModel.Divisions.AddRange(lstDivision);

                var groups = this.domainDataRepository.GroupDomainGet().Result;
                List<CommonModel> lstGroup = new List<CommonModel>();
                foreach (var group in groups)
                {
                    lstGroup.Add(new CommonModel { Id = Convert.ToString(group.GroupNo), Name = group.GroupName });
                }

                dealDomainModel.Groups.AddRange(lstGroup);
                var depts = this.domainDataRepository.DepartmentDomainGet().Result;
                List<CommonModel> lstDept = new List<CommonModel>();
                foreach (var dept in depts)
                {
                    lstDept.Add(new CommonModel { Id = Convert.ToString(dept.Dept), Name = dept.DeptName });
                }

                dealDomainModel.Departments.AddRange(lstDept);
                dealDomainModel.DiscountTypes.AddRange(this.domainDataRepository.DomainDetailData(DealManagementConstants.DomainHdrChangeType).Result);
                if (id != 0)
                {
                    dealDomainModel.SupplierDeal = await this.supplierDealRepository.GetDeal(id, this.serviceDocumentResult);
                }

                return dealDomainModel;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        /// <summary>
        /// This API invoked mainly for loading details in Deal popup when Add button clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<SupplierDealModel>> New()
        {
            await this.GetDomainData();
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for populating fields in the Deal popup when Edit icon clicked.
        /// </summary>
        /// <param name="id">Deal Id</param>
        /// <returns>ServiceDocument</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<SupplierDealModel>> Open(int id)
        {
            this.dealId = id;
            await this.serviceDocument.OpenAsync(this.OpenDeal);
            await this.GetDomainData();
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for saving field values in the Deal Head page when Save button clicked.
        /// </summary>
        /// <param name="serviceDocument">Service Document</param>
        /// <returns>ServiceDocument</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<SupplierDealModel>> Save([FromBody]ServiceDocument<SupplierDealModel> serviceDocument)
        {
            await this.serviceDocument.SaveAsync(this.DealSave);
            return this.serviceDocument;
        }

        [AllowAnonymous]
        [HttpPost]
        public async ValueTask<bool> DealSave([FromBody]SupplierDealModel supplierDealModel)
        {
            try
            {
                await this.supplierDealRepository.SetDeal(supplierDealModel);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<SupplierDealModel> DealGet(int id)
        {
            try
            {
                var dealHead = await this.supplierDealRepository.GetDeal(id, this.serviceDocumentResult);
                return dealHead;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<SupplierDealModel> OpenDeal()
        {
            try
            {
                var dealHead = await this.supplierDealRepository.GetDeal(this.dealId, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return dealHead;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> DealSave()
        {
            this.serviceDocument.Result = await this.supplierDealRepository.SetDeal(this.serviceDocument.DataProfile.DataModel);
            if (this.serviceDocument.Result.Type != MessageType.Success)
            {
                return false;
            }

            this.serviceDocument.DataProfile.DataModel.DEAL_ID = this.supplierDealRepository.DealId;
            return true;
        }

        private async Task<ServiceDocument<SupplierDealModel>> GetDomainData()
        {
            this.serviceDocument.DomainData.Add("dealtype", this.domainDataRepository.DomainDetailData(DealManagementConstants.DealType).Result);
            this.serviceDocument.DomainData.Add("dealcomptype", this.domainDataRepository.DealCompTypeGet().Result);
            var merchLevels = this.domainDataRepository.DomainDetailData(DealManagementConstants.DomainHdrMerchLevel).Result;
            foreach (var merchLevel in merchLevels)
            {
                string somValue = string.Empty;
                switch (merchLevel.CodeDesc)
                {
                    case "@MH2":
                        somValue = "Division";
                        break;
                    case "@MH3":
                        somValue = "Department";
                        break;
                    case "@MH4":
                        somValue = "Category";
                        break;
                    case "@MH5":
                        somValue = "Fineline";
                        break;
                    case "@MH6":
                        somValue = "Segment";
                        break;
                    default:
                        somValue = string.Empty;
                        break;
                }

                if (!string.IsNullOrEmpty(somValue))
                {
                    merchLevel.CodeDesc = somValue;
                }
            }

            this.serviceDocument.DomainData.Add("merchLevel", merchLevels);
            this.serviceDocument.DomainData.Add("group", this.domainDataRepository.GroupDomainGet().Result);
            this.serviceDocument.DomainData.Add("department", this.domainDataRepository.DepartmentDomainGet().Result);
            this.serviceDocument.DomainData.Add("brands", this.domainDataRepository.BrandDomainGet().Result);
            this.serviceDocument.DomainData.Add("discountType", this.domainDataRepository.DomainDetailData(DealManagementConstants.DomainHdrChangeType).Result);
            this.serviceDocument.LocalizationData.AddData("Deal");
            return this.serviceDocument;
        }
    }
}