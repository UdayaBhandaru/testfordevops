﻿// <copyright file="DealItemLocationController.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.DealManagement.Deal
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Agility.Framework.Localization;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.DealManagement.Common;
    using Agility.RBS.DealManagement.Models;
    using Agility.RBS.DealManagement.Repositories;
    using Agility.RBS.ItemManagement.Item.Models;
    using Agility.RBS.ItemManagement.Item.Repositories;
    using Agility.RBS.MDM.Repositories;
    using Microsoft.AspNetCore.Mvc;

    [Route("api/[controller]/[action]")]
    public class DealItemLocationController : Controller
    {
        private readonly ServiceDocument<DealItemLocationModel> serviceDocument;
        private readonly DealRepository dealRepository;
        private readonly ServiceDocumentResult serviceDocumentResult;
        private readonly DomainDataRepository domainDataRepository;
        private readonly ItemSelectRepository itemSelectRepository;
        private int dealId;

        public DealItemLocationController(
            ServiceDocument<DealItemLocationModel> serviceDocument,
            DealRepository dealRepository,
            DomainDataRepository domainDataRepository,
            ItemSelectRepository itemSelectRepository)
        {
            this.serviceDocument = serviceDocument;
            this.dealRepository = dealRepository;
            this.domainDataRepository = domainDataRepository;
            this.itemSelectRepository = itemSelectRepository;
            this.serviceDocumentResult = new ServiceDocumentResult
            {
                InnerException = string.Empty,
                StackTrace = string.Empty,
                Type = MessageType.Success
            };
        }

        /// <summary>
        /// This API invoked for loading grid data along with data for autocomplete, radio button fields in Deal Location Page.
        /// </summary>
        /// <param name="id">Deal Id</param>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpGet]
        public async Task<ServiceDocument<DealItemLocationModel>> List(int id)
        {
            this.dealId = id;
            this.serviceDocument.DomainData.Add("orgLevel", this.domainDataRepository.DomainDetailData(DealManagementConstants.DomainHdrOrgLevel).Result);
            this.serviceDocument.DomainData.Add("merchLevel", this.domainDataRepository.DomainDetailData(DealManagementConstants.DomainHdrMerchLevel).Result);
            this.serviceDocument.DomainData.Add("location", await this.domainDataRepository.CommonData("LOC"));
            this.serviceDocument.DomainData.Add("costZone", this.domainDataRepository.CommonData("CZ").Result);
            this.serviceDocument.DomainData.Add("group", this.domainDataRepository.CommonData("DEPT").Result);
            this.serviceDocument.DomainData.Add("categories", this.domainDataRepository.CommonData("CATEGORY").Result);
            this.serviceDocument.DomainData.Add("discountType", this.domainDataRepository.DomainDetailData(DealManagementConstants.DomainHdrChangeType).Result);
            this.serviceDocument.DomainData.Add("indicator", this.domainDataRepository.DomainDetailData(DealManagementConstants.DomainHdrIndicator).Result);
            this.serviceDocument.DomainData.Add("detailIds", this.dealRepository.GetAllDetailIdsOfDeal(id, this.serviceDocumentResult).Result);
            await this.serviceDocument.ToListAsync(this.GetDealItemLocations);
            this.serviceDocument.LocalizationData.AddData("Deal");
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for saving grid data in the Deal Location page when Save button clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<DealItemLocationModel>> Save()
        {
            await this.serviceDocument.SaveAsync(this.SaveDealDetails);
            return this.serviceDocument;
        }

        /// <summary>
        /// This API invoked for populating dependent fields when Get Item hyperlink clicked.
        /// </summary>
        /// <returns>Service Document</returns>
        /// <response code="401">Un Authorized</response>
        [HttpPost]
        public async Task<ServiceDocument<DealItemLocationModel>> Search()
        {
            await this.serviceDocument.ToListAsync(this.SearchItem);
            return this.serviceDocument;
        }

        private async Task<List<DealItemLocationModel>> GetDealItemLocations()
        {
            try
            {
                var dealItemLocations = await this.dealRepository.GetDealItemLocations(this.dealId, this.serviceDocumentResult);
                this.serviceDocument.Result = this.serviceDocumentResult;
                return dealItemLocations;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }

        private async Task<bool> SaveDealDetails()
        {
            this.serviceDocument.Result = await this.dealRepository.SetDealItemLocation(this.serviceDocument.DataProfile.DataModel);
            return true;
        }

        private async Task<List<DealItemLocationModel>> SearchItem()
        {
            try
            {
                ItemSelectModel itmSelModel = new ItemSelectModel
                {
                    ItemBarcode = this.serviceDocument.DataProfile.DataModel.Item,
                    OrgLvl = this.serviceDocument.DataProfile.DataModel.OrgLevel,
                    OrgLvlVal = this.serviceDocument.DataProfile.DataModel.OrgLevelValue,
                    ScreenId = "CZ"
                };
                List<CostChgItemSelectModel> items = await this.itemSelectRepository.SelectCostChgItems(itmSelModel, this.serviceDocumentResult);

                List<DealItemLocationModel> dealItemLocations = new List<DealItemLocationModel>();

                if (items != null && items.Count > 0)
                {
                    items.ForEach(item =>
                    {
                        DealItemLocationModel costCdModel = new DealItemLocationModel
                        {
                            ItemDesc = item.ItemDesc,
                            OrgLevel = item.OrgLvl
                        };
                        dealItemLocations.Add(costCdModel);
                    });
                }

                this.serviceDocument.Result = this.serviceDocumentResult;
                return dealItemLocations;
            }
            catch (Exception ex)
            {
                this.serviceDocument.Result = new ServiceDocumentResult
                {
                    InnerException = "Exception",
                    StackTrace = ex.ToString(),
                    Type = MessageType.Exception
                };
                return null;
            }
        }
    }
}
