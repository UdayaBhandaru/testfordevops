﻿// <copyright file="SupplierDealModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.DealManagement.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Inbox.Common;

    public class SupplierDealModel : ProfileEntity
    {
        public SupplierDealModel()
        {
            this.ListOfDealItemLocations = new List<DealItemLocNewModel>();
        }

        [Key]
        public int? DEAL_ID { get; set; }

        public int? Supplier { get; set; }

        public string DealType { get; set; }

        public string DealCompType { get; set; }

        public string Status { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? CloseDate { get; set; }

        public string Operation { get; set; }

        public string CurrencyCode { get; set; }

        [NotMapped]
        public List<DealItemLocNewModel> ListOfDealItemLocations { get; private set; }
    }
}
