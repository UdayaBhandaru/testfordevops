﻿// <copyright file="DealDomainModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.DealManagement.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using Agility.RBS.Core.Models;
    using Agility.RBS.MDM.Repositories.Models;
    using Agility.RBS.MDM.UtilityDomain.Models;

    public class DealDomainModel
    {
        public DealDomainModel()
        {
            this.DealTypes = new List<DomainDetailModel>();
            this.DealCompTypes = new List<DealCompTypeDomainModel>();
            this.MerchLevels = new List<DomainDetailModel>();
            this.Divisions = new List<CommonModel>();
            this.Groups = new List<CommonModel>();
            this.Departments = new List<CommonModel>();
            this.DiscountTypes = new List<DomainDetailModel>();
        }

        public List<DomainDetailModel> DealTypes { get; private set; }

        public List<DealCompTypeDomainModel> DealCompTypes { get; private set; }

        public List<DomainDetailModel> MerchLevels { get; private set; }

        public List<CommonModel> Divisions { get; private set; }

        public List<CommonModel> Groups { get; private set; }

        public List<CommonModel> Departments { get; private set; }

        public List<DomainDetailModel> DiscountTypes { get; private set; }

        public SupplierDealModel SupplierDeal { get; set; }
    }
}
