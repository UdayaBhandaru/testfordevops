﻿// <copyright file="DealGridModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.DealManagement.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class DealGridModel
    {
        public DealGridModel()
        {
            this.DataList = new List<DealSearchNewModel>();
        }

        public DealSearchNewModel DataModel { get; set; }

        public List<DealSearchNewModel> DataList { get; private set; }
    }
}
