﻿// <copyright file="DealItemLocationModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.DealManagement.Models
{
    using System;
    using System.Collections.Generic;
    using Agility.Framework.Core.Profile.Entities;

    public class DealItemLocationModel : ProfileEntity
    {
        public DealItemLocationModel()
        {
            this.ListOfDealItemLocations = new List<DealItemLocationModel>();
        }

        public int? DealId { get; set; }

        public int? DealDetailId { get; set; }

        public int? SeqNo { get; set; }

        public string OrgLevel { get; set; }

        public string OrgLevelDesc { get; set; }

        public int? OrgLevelValue { get; set; }

        public string OrgLevelValueDesc { get; set; }

        public string MerchLevel { get; set; }

        public string MerchLevelDesc { get; set; }

        public int? MerchLevelValue { get; set; }

        public string MerchLevelValueDesc { get; set; }

        public string DiscountType { get; set; }

        public string DiscountTypeDesc { get; set; }

        public decimal? DiscountValue { get; set; }

        public int? ThresholdQty { get; set; }

        public int? ThresholdFreeQty { get; set; }

        public string ThresholdFreeItem { get; set; }

        public string Item { get; set; }

        public string ItemDesc { get; set; }

        public string ExclInd { get; set; }

        public DateTime? EffectiveStartDate { get; set; }

        public DateTime? EffectiveEndDate { get; set; }

        public string Operation { get; set; }

        public string TableName { get; set; }

        public List<DealItemLocationModel> ListOfDealItemLocations { get; private set; }
    }
}
