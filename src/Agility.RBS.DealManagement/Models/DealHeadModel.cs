﻿// <copyright file="DealHeadModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.DealManagement.Models
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Inbox.Common;

    [DataProfile(Name = "DEALHEAD")]
    public class DealHeadModel : ProfileEntity<DealHeadWfModel>, IInboxCommonModel
    {
        [NotMapped]
        public string DealType { get; set; }

        [Column("DEAL_ID", Order = 0, TypeName = "number(10,0)")]
        public int? DEAL_ID { get; set; }

        [NotMapped]
        public string Description { get; set; }

        [NotMapped]
        public int? Supplier { get; set; }

        [NotMapped]
        public string CurrencyCode { get; set; }

        [NotMapped]
        public string ExternalRefNo { get; set; }

        [NotMapped]
        public int? OrderNo { get; set; }

        [NotMapped]
        public string RecalculateApprovedOrders { get; set; }

        [NotMapped]
        public DateTime? StartDate { get; set; }

        [NotMapped]
        public DateTime? CloseDate { get; set; }

        [NotMapped]
        public string Comments { get; set; }

        [NotMapped]
        public string Operation { get; set; }

        [NotMapped]
        public string Status { get; set; }

        [NotMapped]
        [Column("ORGANISATION_ID")]
        public int? OrganizationId { get; set; }

        [NotMapped]
        [Column("CREATED_BY")]
        public string CreatedBy { get; set; }

        [NotMapped]
        [Column("LAST_UPDATED_BY")]
        public string ModifiedBy { get; set; }

        [NotMapped]
        [Column("DeletedInd")]
        public bool DeletedInd { get; set; }

        [NotMapped]
        [Column("CREATED_DATE")]
        public DateTimeOffset? CreatedDate { get; set; }

        [NotMapped]
        [Column("LAST_UPDATED_DATE")]
        public DateTimeOffset? ModifiedDate { get; set; }

        [NotMapped]
        public string Priority { get; set; }

        [NotMapped]
        public string SubComments { get; set; }

        [NotMapped]
        public Core.Models.RbWfHeaderModel WorkflowForm { get; set; }

        [NotMapped]
        public string TableName { get; set; }

        [NotMapped]
        public int? FileCount { get; set; }
    }
}
