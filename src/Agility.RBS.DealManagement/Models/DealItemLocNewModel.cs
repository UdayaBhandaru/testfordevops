﻿// <copyright file="DealItemLocNewModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.DealManagement.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Agility.Framework.Core.Profile.Entities;

    public class DealItemLocNewModel
    {
        public int? DealId { get; set; }

        public int? DealDetailId { get; set; }

        public string MerchLevel { get; set; }

        public string MerchLevelDesc { get; set; }

        public int? MerchLevelValue { get; set; }

        public string MerchLevelValueDesc { get; set; }

        public string DiscountType { get; set; }

        public string DiscountTypeDesc { get; set; }

        public decimal? DiscountValue { get; set; }

        public string Item { get; set; }

        public string ItemDesc { get; set; }

        public string Operation { get; set; }

        public decimal? OldCost { get; set; }

        public decimal? NewCost { get; set; }

        public string Comments { get; set; }
    }
}
