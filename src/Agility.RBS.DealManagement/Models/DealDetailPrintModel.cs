﻿// <copyright file="DealDetailPrintModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.DealManagement.Models
{
    using System;
    using Agility.Framework.Core.Profile.Entities;

    public class DealDetailPrintModel : ProfileEntity
    {
        public int DealId { get; set; }

        public int DealDetailId { get; set; }

        public string DealType { get; set; }

        public string DealDescription { get; set; }

        public string DealCompDesc { get; set; }

        public string Supplier { get; set; }

        public string DealCompType { get; set; }

        public int? ApplicationOrder { get; set; }

        public DateTime? CollectStartDate { get; set; }

        public DateTime? CollectEndDate { get; set; }

        public string CostApplicationInd { get; set; }

        public string PriceCostApplicationInd { get; set; }

        public string ThresholdValueType { get; set; }

        public string TranDiscountInd { get; set; }

        public string Comments { get; set; }
    }
}
