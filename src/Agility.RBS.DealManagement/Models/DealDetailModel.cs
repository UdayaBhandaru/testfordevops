﻿// <copyright file="DealDetailModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.DealManagement.Models
{
    using System;
    using System.Collections.Generic;
    using Agility.Framework.Core.Profile.Entities;

    public class DealDetailModel : ProfileEntity
    {
        public DealDetailModel()
        {
            this.DealDetails = new List<DealDetailModel>();
        }

        public int? DealId { get; set; }

        public int? DealDetailId { get; set; }

        public string DealCompDesc { get; set; }

        public string DealCompType { get; set; }

        public string DealCompTypeId { get; set; }

        public int? ApplicationOrder { get; set; }

        public DateTime? CollectStartDate { get; set; }

        public DateTime? CollectEndDate { get; set; }

        public string CostApplicationInd { get; set; }

        public string PriceCostApplicationInd { get; set; }

        public string DealClass { get; set; }

        public string DealClassDesc { get; set; }

        public string ThresholdValueType { get; set; }

        public string TranDiscountInd { get; set; }

        public string Comments { get; set; }

        public string Operation { get; set; }

        public string Program_Phase { get; set; }

        public string Program_Message { get; set; }

        public string Error { get; set; }

        public string TableName { get; set; }

        public List<DealDetailModel> DealDetails { get; private set; }
    }
}
