﻿// <copyright file="DealDetailIdModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.DealManagement.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class DealDetailIdModel
    {
        public int DetailId { get; set; }

        public string ProgramPhase { get; set; }

        public string ProgramMessage { get; set; }

        public string Error { get; set; }
    }
}
