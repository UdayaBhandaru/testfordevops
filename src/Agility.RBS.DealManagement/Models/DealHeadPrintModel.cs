﻿// <copyright file="DealHeadPrintModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.DealManagement.Models
{
    using System;
    using System.Collections.Generic;
    using Agility.Framework.Core.Profile.Entities;

    public class DealHeadPrintModel : ProfileEntity
    {
        public DealHeadPrintModel()
        {
            this.DealDetails = new List<DealDetailPrintModel>();
        }

        public List<DealDetailPrintModel> DealDetails { get; private set; }

        public string DealType { get; set; }

        public int DEAL_ID { get; set; }

        public string Description { get; set; }

        public string Supplier { get; set; }

        public string CurrencyCode { get; set; }

        public string ExternalRefNo { get; set; }

        public int? OrderNo { get; set; }

        public string RecalculatedOrders { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? CloseDate { get; set; }

        public string Comments { get; set; }
    }
}
