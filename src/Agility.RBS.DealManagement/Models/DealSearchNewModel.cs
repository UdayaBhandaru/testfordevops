﻿// <copyright file="DealSearchNewModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.DealManagement.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.RBS.Core.Models;

    public class DealSearchNewModel : ProfileEntity, Core.IPaginationModel
    {
        [NotMapped]
        public int? Supplier { get; set; }

        [NotMapped]
        public string SupplierName { get; set; }

        [NotMapped]
        [Column("DEAL_ID")]
        public int? DEAL_ID { get; set; }

        [NotMapped]
        public string DealDescription { get; set; }

        [NotMapped]
        public string DealTypeId { get; set; }

        [NotMapped]
        public string DealType { get; set; }

        [NotMapped]
        public DateTime? FromDate { get; set; }

        [NotMapped]
        public DateTime? ToDate { get; set; }

        [NotMapped]
        public string TableName { get; set; }

        [NotMapped]
        public string PError { get; set; }

        [NotMapped]
        public int? StartRow { get; set; }

        [NotMapped]
        public int? EndRow { get; set; }

        [NotMapped]
        public long? TotalRows { get; set; }

        [NotMapped]
        public SortModel[] SortModel { get; set; }
    }
}
