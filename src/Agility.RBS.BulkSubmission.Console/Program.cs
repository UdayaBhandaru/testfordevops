﻿// <copyright file="Program.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.BulkSubmission.Console
{
    using System;
    using System.IO;
    using Agility.Framework.Core.Common;
    using Agility.Framework.Core.Models.Configuration;
    using Agility.RBS.Core;
    using Agility.RBS.ItemManagement.BulkItem.Repositories;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using NLog;
    using NLog.Extensions.Logging;

    class Program
    {
        static void Main(string[] args)
        {
            IConfigurationRoot iConfiguration = new ConfigurationBuilder()
                  .AddJsonFile("appsettings.json")
                  .Build();

            var serviceProvider = new ServiceCollection()
               .AddLogging()
               .BuildServiceProvider();

            //configure console logging
            serviceProvider
                .GetService<ILoggerFactory>();

            var loggerFactory = serviceProvider.GetService<ILoggerFactory>();
            loggerFactory.AddNLog();
            loggerFactory.ConfigureNLog("nlog.config");

            var Logger = serviceProvider.GetService<ILoggerFactory>().CreateLogger<Program>();

            CoreSettings.CoreConfiguration = iConfiguration.Get<CoreConfiguration>();

            SetConfigValues(iConfiguration);

            try
            {
                Logger.LogInformation("Application started...");
                BulkItemRepository bulkItemRepository = new BulkItemRepository();
                string destinationPath = RbSettings.BulkItemFilePath;
                string[] files = System.IO.Directory.GetFiles(destinationPath, "*.xlsm");
                foreach (var file in files)
                {
                    string fileName = Path.GetFileName(file);
                    Logger.LogInformation("Process File: " + fileName);
                    string moveTo = Path.Combine(destinationPath, fileName);
                    if (System.IO.File.Exists(moveTo))
                    {
                        bulkItemRepository.BulkItemDetailsSet(moveTo);
                        Logger.LogInformation("Archive File: " + fileName);
                        string fileToArchieve = Path.Combine(RbSettings.BulkItemArchievePath, fileName);
                        System.IO.File.Move(moveTo, fileToArchieve);
                        Logger.LogInformation("Process Completed:" + fileName);
                    }
                }
            }
            catch (Exception e)
            {
                //Logger.Fatal(e, "An unexpected exception has occured");
            }
            finally
            {
                Logger.LogInformation("Application terminated. Press <enter> to exit...");
                //Console.ReadLine();
            }
        }

        private static void SetConfigValues(IConfigurationRoot configRoot)
        {
            RbSettings.BulkItemFilePath = configRoot["BulkItemFilePath"];
            RbSettings.BulkItemArchievePath = configRoot["BulkItemAchievePath"];
            RbSettings.CostChangeFormProcesPath = configRoot["CostChangeFormProcesPath"];
            RbSettings.CostChangeFormAchievePath = configRoot["CostChangeFormAchievePath"];
        }
    }
}
