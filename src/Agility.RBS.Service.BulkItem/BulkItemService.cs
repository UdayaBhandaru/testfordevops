﻿// <copyright file="BulkItemService.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Service.BulkItem
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.ServiceProcess;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Common;
    using Agility.Framework.Core.Models.Configuration;
    using Agility.RBS.Core;
    using Agility.RBS.ItemManagement.BulkItem.Repositories;
    using Microsoft.Extensions.Configuration;
    using NLog;

    public partial class BulkItemService : ServiceBase
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private System.Timers.Timer timer = null;

        public BulkItemService()
        {
            this.InitializeComponent();
            IConfigurationRoot iConfiguration = new ConfigurationBuilder()
                 .AddJsonFile("appsettings.json")
                 .Build();
            CoreSettings.CoreConfiguration = iConfiguration.Get<CoreConfiguration>();

            SetConfigValues(iConfiguration);

            this.timer = new System.Timers.Timer
            {
                Enabled = true,
                Interval = 300000
            };
            this.timer.Elapsed += this.Timer_Elapsed;
        }

        protected override void OnStart(string[] args)
        {
        }

        protected override void OnStop()
        {
            this.timer.Enabled = false;
        }

        private static void SetConfigValues(IConfigurationRoot configRoot)
        {
            RbSettings.BulkItemFilePath = configRoot["BulkItemFilePath"];
            RbSettings.BulkItemArchievePath = configRoot["BulkItemAchievePath"];
            RbSettings.CostChangeFormProcesPath = configRoot["CostChangeFormProcesPath"];
            RbSettings.CostChangeFormAchievePath = configRoot["CostChangeFormAchievePath"];
        }

        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                logger.Info("Application started...");
                BulkItemRepository bulkItemRepository = new BulkItemRepository();
                string destinationPath = RbSettings.BulkItemFilePath;
                string[] files = System.IO.Directory.GetFiles(destinationPath, "*.xlsm");
                foreach (var file in files)
                {
                    string fileName = Path.GetFileName(file);
                    logger.Info("Process File: " + fileName);
                    string moveTo = Path.Combine(destinationPath, fileName);
                    if (File.Exists(moveTo))
                    {
                        bulkItemRepository.BulkItemDetailsSet(moveTo);
                        logger.Info("Archive File: " + fileName);
                        string fileToArchieve = Path.Combine(RbSettings.BulkItemArchievePath, fileName);
                        File.Move(moveTo, fileToArchieve);
                        logger.Info("Process Completed:" + fileName);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Fatal(ex, "An unexpected exception has occured");
            }
            finally
            {
                logger.Info("Application terminated.");
            }
        }
    }
}
