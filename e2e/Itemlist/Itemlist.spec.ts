import { browser, ExpectedConditions as EC, $, $$, element, by } from 'protractor'
import { HomePage } from '../home/home.page'
import { LoginPage } from '../login/login.page'
import { ItemlistPage } from "./Itemlist.page";
import { protractor } from 'protractor/built/ptor';
declare let expect: any


describe('Itemlist search', function () {
    let homePage: HomePage;
    let loginpage: LoginPage;
    let itemlistPage: ItemlistPage;
    beforeEach(async () => {
       
        
    })   

    it('should open the Itemlist Add page', async (): Promise<any> => {
        loginpage = new LoginPage();
        await loginpage.open();      
        await loginpage.login();  
        homePage = new HomePage();
        itemlistPage = new ItemlistPage();
        browser.waitForAngularEnabled(false);
        await browser.driver.sleep(5000);
        let Utilities  = await homePage.Utilities; 
        expect(Utilities.isPresent()).toBeTruthy();
        await Utilities.click(); 
        await browser.driver.sleep(500); 
        await itemlistPage.ItemlistMenu.click();   
        await browser.driver.sleep(5000);
        expect(itemlistPage.addBtn.isPresent()).toBeTruthy();
        await homePage.ClickAddButton();
        await browser.driver.sleep(5000);
    }) 

    it('should add/Edit the Itemlist', async (): Promise<any> => {
      
      await itemlistPage.typedescription("Item List Test fjhgfe 5");
      await itemlistPage.typeItem("100002637");
     // await itemlistPage.AddtogridButtonClick();
      await homePage.saveButtonClick();
      let successmsg = await itemlistPage.fxbutton;
      expect(successmsg.isPresent()).toBeTruthy();
      await itemlistPage.successOk();
      await element(by.className('closeIcon')).click();
      await browser.driver.sleep(5000);
    
 
    })
      
  it('should open the Itemlist search page', async (): Promise<any> => {

    expect (itemlistPage.searchbtn.isPresent()).toBeTruthy();
    await homePage.ClickSearchButton();
    await browser.driver.sleep(5000); 
    await itemlistPage.Viewiconclick();
    await browser.driver.sleep(5000);
    await element(by.className('closeIcon')).click();
    await itemlistPage.Editiconclick();
    await homePage.saveButtonClick();
    let successmsg = await itemlistPage.fxbutton;
    expect(successmsg.isPresent()).toBeTruthy();
    await itemlistPage.successOk();
   
    })

   


    afterEach(() => {

    });
});