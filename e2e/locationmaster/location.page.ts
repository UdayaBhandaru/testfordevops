import { browser, element, by, $, WebElement } from 'protractor';
import { BasePage } from '../core/base.page';
import { HomePage } from '../home/home.page';
import { DropDownHelper } from "../core/DropdownHelper";
import { DateFormat } from "../core/dateFormat";

export class LocationPage extends BasePage 
{
    public locationMenu = element(by.css("a[href*='Location']"));
    public addBtn = element(by.id("searchbarAdd"));
    public searchbtn = element(by.className('searchbtn'));
    public fxbutton = element(by.className('fx-button'));
    public listitem = element(by.className('listitem'));
    private dropdownHelper: DropDownHelper;
    async LocationPageSearch(): Promise<any>
    {
        let submit = await element(by.id('searchbarSearch'));
        return submit.click();
    }

    async LocationPageAdd(): Promise<any>
    {
        var span1 = element(by.binding('person.name'));
    }

   
    async Editiconclick() 
    {
        console.log("In Editiconclick");
        let Editicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'Edit')]"));

        return await Editicon.click();       
    }
    async Viewiconclick()
    {
        console.log("In Viewiconclick");
        let Viewicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'View')]"));

        return await Viewicon.click();       
    }
   
    async successOk()
    {
        let ok = await element(by.xpath('//*[contains(text(), "OK")]'));
        await ok.click();
        await browser.driver.sleep(8000);
    }
    async ClickFormatlink(): Promise<any> 
    {
        console.log("In Formatlink");
        let formatlink = element(by.className("listitem"));
        return await formatlink.click();
    }
}