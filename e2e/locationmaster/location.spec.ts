import { browser, ExpectedConditions as EC, $, $$, element, by } from 'protractor'
import { HomePage } from '../home/home.page'
import { LoginPage } from '../login/login.page'
import { LocationPage } from "./location.page";
import { protractor } from 'protractor/built/ptor';
declare let expect: any


describe('Location search', function () {
    let homePage: HomePage;
    let loginpage: LoginPage;
    let locationPage: LocationPage;
    beforeEach(async () => {
       
       
    })    


    it('should open the Location  Addpage', async (): Promise<any> => {
        loginpage = new LoginPage();
        await loginpage.open();      
        await loginpage.login();  
        homePage = new HomePage();
        locationPage = new LocationPage();
        browser.waitForAngularEnabled(false);
        await browser.driver.sleep(5000);
        await homePage.mdmmenuClick();
        let organization  = await homePage.orgHierarchy; 
        expect(organization.isPresent()).toBeTruthy();
        await organization.click();       
        await locationPage.locationMenu.click();
        await browser.driver.sleep(5000);
        expect(locationPage.addBtn.isPresent()).toBeTruthy();
        await homePage.ClickAddButton();
        await browser.driver.sleep(5000);
        await homePage.saveButtonClick();
        await element(by.className('closeIcon')).click();
       
    }) 
    

    it('should open the Location search page', async (): Promise<any> => {

        expect(locationPage.searchbtn.isPresent()).toBeTruthy();
        await homePage.ClickSearchButton();
        await browser.driver.sleep(5000); 
        await locationPage.Viewiconclick();
        await browser.driver.sleep(5000);
        await element(by.className('closeIcon')).click();
        await locationPage.Editiconclick();
        await browser.driver.sleep(5000);
        await homePage.saveButtonClick(); 
        let successmsg = await locationPage.fxbutton;
        expect(successmsg.isPresent()).toBeTruthy();
        await locationPage.successOk();
        expect (locationPage.listitem.isPresent()).toBeTruthy();
        await locationPage.ClickFormatlink(); 
        await browser.driver.sleep(5000);
    })

    
    afterEach(() => {

    });

});