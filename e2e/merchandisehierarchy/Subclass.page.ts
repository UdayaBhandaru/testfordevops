import { browser, element, by, $, WebElement } from 'protractor';
import { BasePage } from '../core/base.page';
import { HomePage } from '../home/home.page';
import { DropDownHelper } from "../core/DropdownHelper";

export class SubclassPage extends BasePage 
{
   public SubclassMenu = element(by.css("a[href*='SubClass']"));
   public addBtn = element(by.id("searchbarAdd"));
    public fxbutton = element(by.className('fx-button'));
    public searchbtn = element(by.className('searchbtn'));
    public listitem = element(by.className('listitem'));
    private dropdownHelper: DropDownHelper;
    async SubclassPageSearch(): Promise<any>
    {
        let submit = await element(by.id('searchbarSearch'));
        return submit.click();
    }

    async SubclassPageAdd(): Promise<any>
    {
        var span1 = element(by.binding('person.name'));
    }

 
   
    async typeCompany(Company: string)
    {
        let CompanyInput = await element(by.name('Company'));   
        await CompanyInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let SubclassInput = element(by.xpath("//*[contains(text(),'TSC KUWAIT(1)')]"));   
        await SubclassInput.click();
        await browser.driver.sleep(800);
    }
    async typeDivision(Division: string)
    {
        let DivisionInput = await element(by.name('Division'));   
        await DivisionInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let SubclassInput = element(by.xpath("//*[contains(text(),'SUPERMARKET(1)')]"));   
        await SubclassInput.click();
        await browser.driver.sleep(800);
    }
    async typeDepartment(Department: string)
    {
        let DepartmentInput = await element(by.name('Department'));   
        await DepartmentInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let SubclassInput = element(by.xpath("//*[contains(text(),'Pet Supplies(11)')]"));   
        await SubclassInput.click();
        await browser.driver.sleep(800);
    } 
    async typeCategory(Category: string)
    {
        let CategoryInput = await element(by.name('Category'));   
        await CategoryInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let SubclassInput = element(by.xpath("//*[contains(text(),'Keyed Dept Sales - Pet Supplies(152)')]"));   
        await SubclassInput.click();
        await browser.driver.sleep(800);
    }
    async typeClass(Class: string)
    {
        let ClassInput = await element(by.name('Class'));   
        await ClassInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let SubclassInput = element(by.xpath("//*[contains(text(),'Keyed Dept Sales(1)')]"));   
        await SubclassInput.click();
        await browser.driver.sleep(800);
    }

    async typeSubclassId(SubclassId: string)
    {
        let ClassInput = await element(by.name('subClassId'));      
        await ClassInput.sendKeys(SubclassId);
        await browser.driver.sleep(500);
    }


    async typedescription(description: string)
    {
        let descriptionInput = await element(by.name('subClassDesc')); 
        await descriptionInput.sendKeys(description);
        await browser.driver.sleep(500);
    }
    
    async typeMerchandise(Merchandise: string)  
    {
        let MerchandiseInput = await element(by.name('Merchandiser'));   
        await MerchandiseInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let SubclassInput = element(by.xpath("//*[contains(text(),'PavanKumarG')]"));   
        await SubclassInput.click();
        await browser.driver.sleep(800);
    }
    async typebuyer(buyer: string)  
    {
        let buyerInput = await element(by.name('Buyer'));   
        await buyerInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let SubclassInput = element(by.xpath("//*[contains(text(),'raleti')]"));   
        await SubclassInput.click();
        await browser.driver.sleep(800);
    }
    async Editiconclick()
    {
        console.log("In Editiconclick");
        let Editicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'Edit')]"));

        return await Editicon.click();       
    }
    async Viewiconclick()
    {
        console.log("In Viewiconclick");
        let Viewicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'View')]"));

        return await Viewicon.click();       
    }
   
    async successOk()
    {
        let ok = await element(by.xpath('//*[contains(text(), "OK")]'));
        await ok.click();
        await browser.driver.sleep(8000);
    }
    async ClickClassLink(): Promise<any> 
    {
        console.log("In Classlink");
        let classlink = element(by.className("listitem"));
        return await classlink.click();
    }
}