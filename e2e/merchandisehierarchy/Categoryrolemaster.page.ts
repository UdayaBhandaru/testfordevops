import { browser, element, by, $, WebElement } from 'protractor';
import { BasePage } from '../core/base.page';
import { HomePage } from '../home/home.page';
import { DropDownHelper } from "../core/DropdownHelper";
 
export class CategoryrolemasterPage extends BasePage 
{
   public CategoryrolemasterMenu = element(by.css("a[href*='CategoryRole']"));
   public addBtn = element(by.id("searchbarAdd"));
    public fxbutton = element(by.className('fx-button'));
    public searchbtn = element(by.className('searchbtn'));
    public listitem = element(by.className('listitem'));
    private dropdownHelper: DropDownHelper;
    async CategoryrolemasterPageSearch(): Promise<any>
    {
        let submit = await element(by.id('searchbarSearch'));
        return submit.click();
    }

    async CategoryrolemasterPageAdd(): Promise<any>
    {
        var span1 = element(by.binding('person.name'));
    }
  

   
    async typeCompany(Company: string)
    {
        let CompanyInput = await element(by.name('Company'));   
        await CompanyInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let CategoryrolemasterInput = element(by.xpath("//*[contains(text(),'TSC KUWAIT(1)')]"));   
        await CategoryrolemasterInput.click();
        await browser.driver.sleep(800);
    }
    async typeDivision(Division: string)
    {
        let DivisionInput = await element(by.name('Division'));   
        await DivisionInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let CategoryrolemasterInput = element(by.xpath("//*[contains(text(),'GROCERIES(501)')]"));   
        await CategoryrolemasterInput.click();
        await browser.driver.sleep(800);
    }
    async typeDepartment(Department: string)
    {
        let DepartmentInput = await element(by.name('Department'));   
        await DepartmentInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let CategoryrolemasterInput = element(by.xpath("//*[contains(text(),'FRESH FOOD(5002)')]"));   
        await CategoryrolemasterInput.click();
        await browser.driver.sleep(800);
    }
    async typeCategory(Category: string)
    {
        let CategoryInput = await element(by.name('Category'));   
        await CategoryInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let CategoryrolemasterInput = element(by.xpath("//*[contains(text(),'FRUIT & VEG(50002)')]"));   
        await CategoryrolemasterInput.click();
        await browser.driver.sleep(800);
    }

    async typeRolelevel(Rolelevel: string)
    {
        let RolelevelInput = await element(by.name('Role Level'));   
        await RolelevelInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let CategoryrolemasterInput = element(by.xpath("//*[contains(text(),'COMPANY')]"));   
        await CategoryrolemasterInput.click();
        await browser.driver.sleep(800);
    }


    async typeRole(Role: string)
    {
        let RoleInput = await element(by.name('Role'));   
        await RoleInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let CategoryrolemasterInput = element(by.xpath("//*[contains(text(),'TSC BAHRAIN')]"));   
        await CategoryrolemasterInput.click();
        await browser.driver.sleep(800);
    }
    
    async typeStrategytype(Strategytype: string)
    {
        let StrategytypeInput = await element(by.name('Strategy Type'));   
        await StrategytypeInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let CategoryrolemasterInput = element(by.xpath("//*[contains(text(),'PRIMARY')]"));   
        await CategoryrolemasterInput.click();
        await browser.driver.sleep(800);
    }
    async typeCategoryrole(Categoryrole: string)
    {
        let CategoryroleInput = await element(by.name('Category Role'));   
        await CategoryroleInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let CategoryrolemasterInput = element(by.xpath("//*[contains(text(),'PRIMARY')]"));   
        await CategoryrolemasterInput.click();
        await browser.driver.sleep(800);
    }
    async typeCategorystrategy(Categorystrategy: string)
    {
        let CategorystrategyInput = await element(by.name('Category Strategy'));   
        await CategorystrategyInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let CategoryrolemasterInput = element(by.xpath("//*[contains(text(),'SECONDARY')]"));   
        await CategoryrolemasterInput.click();
        await browser.driver.sleep(800);
    }
    async Editiconclick()
    {
        console.log("In Editiconclick");
        let Editicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'Edit')]"));

        return await Editicon.click();       
    }
    async Viewiconclick()
    {
        console.log("In Viewiconclick");
        let Viewicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'View')]"));

        return await Viewicon.click();       
    }
   
    
    async successOk()
    {
        let ok = await element(by.xpath('//*[contains(text(), "OK")]'));
        await ok.click();
        await browser.driver.sleep(8000);
    }
    async ClickClassLink(): Promise<any> 
    {
        console.log("In Classlink");
        let classlink = element(by.className("listitem"));
        return await classlink.click();
    }
}