import { browser, element, by, $, WebElement } from 'protractor';
import { BasePage } from '../core/base.page';
import { HomePage } from '../home/home.page';
import { DropDownHelper } from "../core/DropdownHelper";

export class MerchantspacemasterPage extends BasePage 
{
   public MerchantspacemasterMenu = element(by.css("a[href*='MerchantSpace']"));
   public addBtn = element(by.id("searchbarAdd"));
    public searchButton = element(by.id('searchbarSearch'));
    public fxbutton = element(by.className('fx-button'));
    public searchbtn = element(by.className('searchbtn'));
    private dropdownHelper: DropDownHelper;
    async MerchantspacemasterPageSearch(): Promise<any>
    {
        let submit = await element(by.id('searchbarSearch'));
        return submit.click();
    }

    async MerchantspacemasterPageAdd(): Promise<any>
    {
        var span1 = element(by.binding('person.name'));
    }
 

   
    async typeCompany(Company: string)
    {
        let CompanyInput = await element(by.name('Company'));   
        await CompanyInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let MerchantspacemasterInput = element(by.xpath("//*[contains(text(),'TSC KUWAIT(1)')]"));   
        await MerchantspacemasterInput.click();
        await browser.driver.sleep(800);
    }
    async typeDivision(Division: string)
    {
        let DivisionInput = await element(by.name('Division'));   
        await DivisionInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let MerchantspacemasterInput = element(by.xpath("//*[contains(text(),'GROCERIES(501)')]"));   
        await MerchantspacemasterInput.click();
        await browser.driver.sleep(800);
    }
    async typeDepartment(Department: string)
    {
        let DepartmentInput = await element(by.name('Department'));   
        await DepartmentInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let MerchantspacemasterInput = element(by.xpath("//*[contains(text(),'FRESH FOOD(5002)')]"));   
        await MerchantspacemasterInput.click();
        await browser.driver.sleep(800);
    }
    async typeCategory(Category: string)
    {
        let CategoryInput = await element(by.name('Category'));   
        await CategoryInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let MerchantspacemasterInput = element(by.xpath("//*[contains(text(),'FRUIT & VEG(50002)')]"));   
        await MerchantspacemasterInput.click();
        await browser.driver.sleep(800);
    }

    async typeSpacelevel(Spacelevel: string)
    {
        let SpacelevelInput = await element(by.name('Space Level'));   
        await SpacelevelInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let MerchantspacemasterInput = element(by.xpath("//*[contains(text(),'COMPANY')]"));   
        await MerchantspacemasterInput.click();
        await browser.driver.sleep(800);
    }


    async typeSpaceorganization(Spaceorganization: string)
    {
        let SpaceorganizationInput = await element(by.name('Space Organization'));   
        await SpaceorganizationInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let MerchantspacemasterInput = element(by.xpath("//*[contains(text(),'TSC BAHRAIN')]"));   
        await MerchantspacemasterInput.click();
        await browser.driver.sleep(800);
    }
    
    async typePrimarybrand(Primarybrand: string)
    {
        let PrimarybrandInput = await element(by.name('Primary Brand'));   
        await PrimarybrandInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let MerchantspacemasterInput = element(by.xpath("//*[contains(text(),'FAIRY')]"));   
        await MerchantspacemasterInput.click();
        await browser.driver.sleep(800);
    }
    async Editiconclick()
    {
        console.log("In Editiconclick");
        let Editicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'Edit')]"));

        return await Editicon.click();       
    }
    async Viewiconclick()
    {
        console.log("In Viewiconclick");
        let Viewicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'View')]"));

        return await Viewicon.click();       
    }
   
    
    async successOk()
    {
        let ok = await element(by.xpath('//*[contains(text(), "OK")]'));
        await ok.click();
        await browser.driver.sleep(8000);
    }
}