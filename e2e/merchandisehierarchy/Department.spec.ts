import { browser, ExpectedConditions as EC, $, $$, element, by } from 'protractor'
import { HomePage } from '../home/home.page'
import { LoginPage } from '../login/login.page'
import { DepartmentPage } from "./Department.page";
import { protractor } from 'protractor/built/ptor';
declare let expect: any


describe('Department search', function () {
    let homePage: HomePage;
    let loginpage: LoginPage;
    let departmentPage: DepartmentPage;
    beforeEach(async () => {
       
        
    })   

    it('should open the Department Addpage', async (): Promise<any> => {
        loginpage = new LoginPage();
        await loginpage.open();      
        await loginpage.login();  
        homePage = new HomePage();
        departmentPage = new DepartmentPage();
        browser.waitForAngularEnabled(false);
        await browser.driver.sleep(5000);
        await homePage.mdmmenuClick();
        let organization  = await homePage.merchandisehierarchy; 
        expect(organization.isPresent()).toBeTruthy();
        await organization.click();       
        await departmentPage.DepartmentMenu.click();
       
      
       
    })  
    it('should add/Edit the Department', async (): Promise<any> =>  {
       
        await browser.driver.sleep(5000);
        expect(departmentPage.addBtn.isPresent()).toBeTruthy();
        await browser.driver.sleep(5000);
        await homePage.ClickAddButton();
        await browser.driver.sleep(5000);
       await departmentPage.typeCompany("TSC KUWAIT(1)");
       await departmentPage.typeDivision("HOMECENTER(3)");
       await departmentPage.typeDepartmentId("50342");
       await departmentPage.typedescription("FURNITURE 01");
       await departmentPage.typebuyer("raleti");
       await departmentPage.typeMerchandise("PavanKumarG");
       await departmentPage.typetotalmarketamount("6574");
       await departmentPage.typevatcode("VAT06");
       await homePage.saveButtonClick();
       await browser.driver.sleep(5000);
       let successmsg = await departmentPage.fxbutton;
       expect(successmsg.isPresent()).toBeTruthy();
       await departmentPage.successOk();
       await element(by.className('toolbar-icons searchIcon')).click();
       await browser.driver.sleep(5000);

     })

    it('should open the Department search page', async (): Promise<any> => {

        await departmentPage.typeCompany("TSC KUWAIT(1)");
        expect(departmentPage.searchbtn.isPresent()).toBeTruthy();
        await homePage.ClickSearchButton();
        await browser.driver.sleep(5000); 
        await departmentPage.Viewiconclick();
        await browser.driver.sleep(5000);
        await element(by.className('closeIcon')).click();
        await departmentPage.Editiconclick();
        await homePage.saveButtonClick();
        let successmsg = await departmentPage.fxbutton; 
        expect(successmsg.isPresent()).toBeTruthy();
        await departmentPage.successOk();
        await browser.driver.sleep(5000)
        expect(departmentPage.listitem.isPresent()).toBeTruthy();
        await departmentPage.ClickCatagoryLink();
       ;
        })
    afterEach(() => {

    });
});