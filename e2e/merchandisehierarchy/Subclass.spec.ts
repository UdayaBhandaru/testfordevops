import { browser, ExpectedConditions as EC, $, $$, element, by } from 'protractor'
import { HomePage } from '../home/home.page'
import { LoginPage } from '../login/login.page'
import { SubclassPage } from "./Subclass.page";
import { protractor } from 'protractor/built/ptor';
declare let expect: any


describe('Subclass search', function () {
    let homePage: HomePage;
    let loginpage: LoginPage;
    let subclassPage: SubclassPage;
    beforeEach(async () => {
       
       
    })    

    it('should open the subclass page', async (): Promise<any> => {
        loginpage = new LoginPage();
        await loginpage.open();      
        await loginpage.login();  
        homePage = new HomePage();
        subclassPage = new SubclassPage();
        browser.waitForAngularEnabled(false);
        await browser.driver.sleep(5000);
        await homePage.mdmmenuClick();
        let organization  = await homePage.merchandisehierarchy; 
        expect(organization.isPresent()).toBeTruthy();
        await organization.click();       
        await subclassPage.SubclassMenu.click();
        await browser.driver.sleep(5000);
        expect(subclassPage.addBtn.isPresent()).toBeTruthy();
        await homePage.ClickAddButton();
        
       
    })  
    it('should add/Edit  the Class', async (): Promise<any> => {
        await browser.driver.sleep(5000);
        await subclassPage.typeCompany("TSC KUWAIT(1)");
        await subclassPage.typeDivision("SUPERMARKET(1)");
        await subclassPage.typeDepartment("Pet Supplies(11)");
        await subclassPage.typeCategory("Keyed Dept Sales - Pet Supplies(152)");
        await subclassPage.typeClass("Keyed Dept Sales(1)");
        await subclassPage.typeSubclassId("756");
        await subclassPage.typedescription("GORDON 4 SEATER ROUND DINING TABLE SET");
        await subclassPage.typeMerchandise("PavanKumarG");
        await subclassPage.typebuyer("raleti");
        await homePage.saveButtonClick();
        let successmsg = await subclassPage.fxbutton;
        expect(successmsg.isPresent()).toBeTruthy();
        await browser.driver.sleep(5000);
        await subclassPage.successOk();
        await element(by.className('toolbar-icons searchIcon')).click();
        await browser.driver.sleep(5000);
    })

    it('should open the Subclass search page', async (): Promise<any> => {

        await subclassPage.typeCompany("TSC BAHRAIN(1001)");
        expect(subclassPage.searchbtn.isPresent()).toBeTruthy();
        await homePage.ClickSearchButton();
        await browser.driver.sleep(5000); 
        await subclassPage.Viewiconclick();
        await browser.driver.sleep(5000);
        await element(by.className('closeIcon')).click();
        await subclassPage.Editiconclick();
        await browser.driver.sleep(5000);
        await homePage.saveButtonClick();
        let successmsg = await subclassPage.fxbutton;
        expect(successmsg.isPresent()).toBeTruthy();
        await subclassPage.successOk();
        expect (subclassPage.listitem.isPresent()).toBeTruthy();
        await subclassPage.ClickClassLink();
        await browser.driver.sleep(5000);
       
    }) 
    afterEach(() => {

    });
});