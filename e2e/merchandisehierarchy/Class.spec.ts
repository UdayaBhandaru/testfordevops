import { browser, ExpectedConditions as EC, $, $$, element, by } from 'protractor'
import { HomePage } from '../home/home.page'
import { LoginPage } from '../login/login.page'
import { ClassPage } from "./Class.page";
import { protractor } from 'protractor/built/ptor';
declare let expect: any


describe('Class search', function () {
    let homePage: HomePage;
    let loginpage: LoginPage;
    let classPage: ClassPage;
    beforeEach(async () => {
       
        
    })   

    it('should open the Class Addpage', async (): Promise<any> => {
        loginpage = new LoginPage();
        await loginpage.open();      
        await loginpage.login();  
        homePage = new HomePage();
        classPage = new ClassPage();
        browser.waitForAngularEnabled(false);
        await browser.driver.sleep(5000);
        await homePage.mdmmenuClick();
        let organization  = await homePage.merchandisehierarchy; 
        expect(organization.isPresent()).toBeTruthy();
        await organization.click();       
        await classPage.ClassMenu.click();
        await browser.driver.sleep(5000);
        expect(classPage.addBtn.isPresent()).toBeTruthy();
        await homePage.ClickAddButton();
       
        
    }) 
    it('should add/Edit the Class', async (): Promise<any> =>  {
       await browser.driver.sleep(5000);
       await classPage.typeCompany("TSC BAHRAIN(1001)");
       await classPage.typeDivision("FSHRDTYH(789654)");
       await classPage.typeDepartment("SSSA(789654)");
       await classPage.typeCategory("INDOOR FURNITURE(898)");
       await classPage.typeClass("4726");
       await classPage.typedescription("DINNING TABLE");
       await classPage.typeMerchandise("Shaik");
       await classPage.typebuyer("raleti");
       await homePage.saveButtonClick();
       let successmsg = await classPage.fxbutton;
       expect(successmsg.isPresent()).toBeTruthy();
       await classPage.successOk();
       await element(by.className('toolbar-icons searchIcon')).click();
       await browser.driver.sleep(5000);

    })
    it('should open the Class search page', async (): Promise<any> => {
        await classPage.typeCompany("TSC BAHRAIN(1001)");
        expect(classPage.searchbtn.isPresent()).toBeTruthy();
        await homePage.ClickSearchButton();
        await browser.driver.sleep(5000); 
        await classPage.Viewiconclick();
        await browser.driver.sleep(5000);
        await element(by.className('closeIcon')).click();
        await classPage.Editiconclick();
        await browser.driver.sleep(5000);
        await homePage.saveButtonClick();
        let successmsg = await classPage.fxbutton;
        expect(successmsg.isPresent()).toBeTruthy();
        await classPage.successOk();
        expect (classPage.listitem.isPresent()).toBeTruthy();
        await classPage.ClickCatagoryLink();
        await browser.driver.sleep(5000);
       
        })
    
    


    afterEach(() => {

    });
});