import { browser, element, by, $, WebElement } from 'protractor';
import { BasePage } from '../core/base.page';
import { HomePage } from '../home/home.page';
import { DropDownHelper } from "../core/DropdownHelper";

export class DivisionPage extends BasePage 
{
   public DivisionMenu = element(by.css("a[href*='Division']"));
   public addBtn = element(by.id("searchbarAdd"));
    public searchbtn = element(by.className('searchbtn'));
    public fxbutton = element(by.className('fx-button'));
    public listitem = element(by.className('listitem'));
    private dropdownHelper: DropDownHelper;
    async DivisionPageSearch(): Promise<any>
    {
        let submit = await element(by.id('searchbarSearch'));
        return submit.click();
    }

    async DivisionPageAdd(): Promise<any>
    {
        var span1 = element(by.binding('person.name'));
    }
 

    
    async typeCompany(Company: string)
    {
        let CompanyInput = await element(by.name('Company'));   
        await CompanyInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let DivisionInput = element(by.xpath("//*[contains(text(),'TSC BAHRAIN(1001)')]"));   
        await DivisionInput.click();
        await browser.driver.sleep(8000);
    }
    async typeDivisionId(DivisionId: string)
    {
        let DivisionIdInput = await element(by.name('divisionId'));      
        await DivisionIdInput.sendKeys(DivisionId);
        await browser.driver.sleep(5000);
    }


    async typedescription(description: string)
    {
        let descriptionInput = await element(by.name('description')); 
        await descriptionInput.sendKeys(description);
        await browser.driver.sleep(5000);
    }

    async typemanager(manager: string)  
    {
        let managerInput = await element(by.name('Manager'));   
        await managerInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let DivisionInput = element(by.xpath("//*[contains(text(),'Firas')]"));   
        await DivisionInput.click();
        await browser.driver.sleep(8000);
    }
    async typeProxymanager(Proxymanager: string)  
    {
        let ProxymanagerInput = await element(by.name('Proxy Manager'));   
        await ProxymanagerInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let DivisionInput = element(by.xpath("//*[contains(text(),'raleti')]"));   
        await DivisionInput.click();
        await browser.driver.sleep(8000);
    }
    async Editiconclick()
    {
        console.log("In Editiconclick");
        let Editicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'Edit')]"));

        return await Editicon.click();       
    }
    async Viewiconclick()
    {
        console.log("In Viewiconclick");
        let Viewicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'View')]"));

        return await Viewicon.click();       
    }
   
    async successOk()
    {
        let ok = await element(by.xpath('//*[contains(text(), "OK")]'));
        await ok.click();
        await browser.driver.sleep(8000);
    }
    async ClickDepartmentLink(): Promise<any> 
    {
        console.log("In deparmaentLink");
        let departmentLink = element(by.className("listitem"));
        return await departmentLink.click();
    }
}