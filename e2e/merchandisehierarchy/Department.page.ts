import { browser, element, by, $, WebElement } from 'protractor';
import { BasePage } from '../core/base.page';
import { HomePage } from '../home/home.page';
import { DropDownHelper } from "../core/DropdownHelper";

export class DepartmentPage extends BasePage 
{
   public DepartmentMenu = element(by.css("a[href*='Department']"));
   public addBtn = element(by.id("searchbarAdd"));
    public searchbtn = element(by.className('searchbtn'));
    public fxbutton = element(by.className('fx-button'));
    public listitem = element(by.className('listitem'));
    private dropdownHelper: DropDownHelper;
    async DepartmentPageSearch(): Promise<any>
    {
        let submit = await element(by.id('searchbarSearch'));
        return submit.click();
    }
 
    async DepartmentPageAdd(): Promise<any>
    {
        var span1 = element(by.binding('person.name'));
    }

 
   
    async typeCompany(Company: string)
    {
        let CompanyInput = await element(by.name('Company'));   
        await CompanyInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let DepartmentInput = element(by.xpath("//*[contains(text(),'TSC KUWAIT(1)')]"));   
        await DepartmentInput.click();
        await browser.driver.sleep(800);
    }
    async typeDivision(Division: string)
    {
        let DivisionInput = await element(by.name('Division'));   
        await DivisionInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let DepartmentInput = element(by.xpath("//*[contains(text(),'HOMECENTER(3)')]"));   
        await DepartmentInput.click();
        await browser.driver.sleep(800);
    }
    async typeDepartmentId(DepartmentId: string)
    {
        let DepartmentIdInput = await element(by.name('deptId'));      
        await DepartmentIdInput.sendKeys(DepartmentId);
        await browser.driver.sleep(500);
    }


    async typedescription(description: string)
    {
        let descriptionInput = await element(by.name('deptDesc')); 
        await descriptionInput.sendKeys(description);
        await browser.driver.sleep(500);
    }

    async typebuyer(buyer: string)  
    {
        let buyerInput = await element(by.name('Buyer'));   
        await buyerInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let DepartmentInput = element(by.xpath("//*[contains(text(),'raleti')]"));   
        await DepartmentInput.click();
        await browser.driver.sleep(800);
    }
    async typeMerchandise(Merchandise: string)  
    {
        let MerchandiseInput = await element(by.name('Merchandiser'));   
        await MerchandiseInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let DepartmentInput = element(by.xpath("//*[contains(text(),'PavanKumarG')]"));   
        await DepartmentInput.click();
        await browser.driver.sleep(800);
    }

    async typetotalmarketamount(totalmarketamount: string)
    {
        let typetotalmarketamountInput = await element(by.name('totalMarketAmount'));      
        await typetotalmarketamountInput.sendKeys(totalmarketamount);
        await browser.driver.sleep(500);
    }
    
    async typevatcode(vatcode: string)  
    {
        let vatcodeInput = await element(by.name('VAT Code'));   
        await vatcodeInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let DepartmentInput = element(by.xpath("//*[contains(text(),'VAT06')]"));   
        await DepartmentInput.click();
        await browser.driver.sleep(800);
    }
    async Editiconclick()
    {
        console.log("In Editiconclick");
        let Editicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'Edit')]"));

        return await Editicon.click();       
    }
    async Viewiconclick()
    {
        console.log("In Viewiconclick");
        let Viewicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'View')]"));

        return await Viewicon.click();       
    }
   
    async successOk()
    {
        let ok = await element(by.xpath('//*[contains(text(), "OK")]'));
        await ok.click();
        await browser.driver.sleep(8000);
    }
    async ClickCatagoryLink(): Promise<any> 
    {
        console.log("In categoryLink");
        let CatagoryLink = element(by.className("listitem"));
        return await CatagoryLink.click();
    }
}