import { browser, element, by, $, WebElement } from 'protractor';
import { BasePage } from '../core/base.page';
import { HomePage } from '../home/home.page';
import { DropDownHelper } from "../core/DropdownHelper";

export class CategoryPage extends BasePage 
{
   public CategoryMenu = element(by.css("a[href*='Category']"));
   public addBtn = element(by.id("searchbarAdd"));
    public fxbutton = element(by.className('fx-button'));
    public searchbtn = element(by.className('searchbtn'));
    public listitem = element(by.className('listitem'));
    private dropdownHelper: DropDownHelper;
    async CategoryPageSearch(): Promise<any>
    {
        let submit = await element(by.id('searchbarSearch'));
        return submit.click();
    } 

    async CategoryPageAdd(): Promise<any>
    {
        var span1 = element(by.binding('person.name'));
    }
 

    
    async typeCompany(Company: string)
    {
        let CompanyInput = await element(by.name('Company'));   
        await CompanyInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let CategoryInput = element(by.xpath("//*[contains(text(),'TSC KUWAIT(1)')]"));   
        await CategoryInput.click();
        await browser.driver.sleep(800);
    }
    async typeDivision(Division: string)
    {
        let DivisionInput = await element(by.name('Division'));   
        await DivisionInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let CategoryInput = element(by.xpath("//*[contains(text(),'SUPERMARKET(1)')]"));   
        await CategoryInput.click();
        await browser.driver.sleep(800);
    }
    async typeDepartment(Department: string)
    {
        let DepartmentInput = await element(by.name('Department'));   
        await DepartmentInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let CategoryInput = element(by.xpath("//*[contains(text(),'Grocery Foods(40)')]"));   
        await CategoryInput.click();
        await browser.driver.sleep(800);
    }
    async typeCategory(CategoryId: string)
    {
        let CategoryInput = await element(by.name('categoryId'));      
        await CategoryInput.sendKeys(CategoryId);
        await browser.driver.sleep(500);
    }


    async typedescription(description: string)
    {
        let descriptionInput = await element(by.name('categoryDesc')); 
        await descriptionInput.sendKeys(description);
        await browser.driver.sleep(500);
    }
    async typetotalmarketamount(totalmarketamount: string)
    {
        let typetotalmarketInput = await element(by.name('totalMarketAmount'));      
        await typetotalmarketInput.sendKeys(totalmarketamount);
        await browser.driver.sleep(500);
    }
    
    async typebuyer(buyer: string)  
    {
        let buyerInput = await element(by.name('Buyer'));   
        await buyerInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let CategoryInput = element(by.xpath("//*[contains(text(),'raleti')]"));   
        await CategoryInput.click();
        await browser.driver.sleep(800);
    }
    async typeMerchandise(Merchandise: string)  
    {
        let MerchandiseInput = await element(by.name('Merchandiser'));   
        await MerchandiseInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let CategoryInput = element(by.xpath("//*[contains(text(),'Shaik')]"));   
        await CategoryInput.click();
        await browser.driver.sleep(800);
    }

    
    async typevatcode(vatcode: string)  
    {
        let vatcodeInput = await element(by.name('VAT Code'));   
        await vatcodeInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let CategoryInput = element(by.xpath("//*[contains(text(),'VAT02')]"));   
        await CategoryInput.click();
        await browser.driver.sleep(800);
    }

    async Editiconclick()
    {
        console.log("In Editiconclick");
        let Editicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'Edit')]"));

        return await Editicon.click();       
    }
    async Viewiconclick()
    {
        console.log("In Viewiconclick");
        let Viewicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'View')]"));

        return await Viewicon.click();       
    }
   
    async successOk()
    {
        let ok = await element(by.xpath('//*[contains(text(), "OK")]'));
        await ok.click();
        await browser.driver.sleep(8000);
    }
    async departmentLink(): Promise<any> 
    {
        console.log("In departmentlink");
        let departmentLink = element(by.className("listitem"));
        return await departmentLink.click();
    }
}