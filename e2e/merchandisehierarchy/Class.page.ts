import { browser, element, by, $, WebElement } from 'protractor';
import { BasePage } from '../core/base.page';
import { HomePage } from '../home/home.page';
import { DropDownHelper } from "../core/DropdownHelper";

export class ClassPage extends BasePage 
{
   public ClassMenu = element(by.css("a[href*='Class']"));
   public addBtn = element(by.id("searchbarAdd"));
    public fxbutton = element(by.className('fx-button'));
    public searchbtn = element(by.className('searchbtn'));
    public listitem = element(by.className('listitem'));
    private dropdownHelper: DropDownHelper; 
    async ClassPageSearch(): Promise<any>
    {
        let submit = await element(by.id('searchbarSearch'));
        return submit.click();
    }

    async ClassPageAdd(): Promise<any> 
    {
        var span1 = element(by.binding('person.name'));
    }


   
    async typeCompany(Company: string)
    {
        let CompanyInput = await element(by.name('Company'));   
        await CompanyInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let ClassInput = element(by.xpath("//*[contains(text(),'TSC BAHRAIN(1001)')]"));   
        await ClassInput.click();
        await browser.driver.sleep(800);
    }
    async typeDivision(Division: string)
    {
        let DivisionInput = await element(by.name('Division'));   
        await DivisionInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let ClassInput = element(by.xpath("//*[contains(text(),'FSHRDTYH(789654)')]"));   
        await ClassInput.click();
        await browser.driver.sleep(800);
    }
    async typeDepartment(Department: string)
    {
        let DepartmentInput = await element(by.name('Department'));   
        await DepartmentInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let ClassInput = element(by.xpath("//*[contains(text(),'SSSA(789654)')]"));   
        await ClassInput.click();
        await browser.driver.sleep(800);
    }
    async typeCategory(Category: string)
    {
        let CategoryInput = await element(by.name('Category'));   
        await CategoryInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let ClassInput = element(by.xpath("//*[contains(text(),'INDOOR FURNITURE(898)')]"));   
        await ClassInput.click();
        await browser.driver.sleep(800);
    }

    async typeClass(ClassId: string)
    {
        let ClassInput = await element(by.name('classId'));      
        await ClassInput.sendKeys(ClassId);
        await browser.driver.sleep(500);
    }


    async typedescription(description: string)
    {
        let descriptionInput = await element(by.name('classDesc')); 
        await descriptionInput.sendKeys(description);
        await browser.driver.sleep(500);
    }
    
    async typeMerchandise(Merchandise: string)  
    {
        let MerchandiseInput = await element(by.name('Merchandiser'));   
        await MerchandiseInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let ClassInput = element(by.xpath("//*[contains(text(),'Shaik')]"));   
        await ClassInput.click();
        await browser.driver.sleep(800);
    }
    async typebuyer(buyer: string)  
    {
        let buyerInput = await element(by.name('Buyer'));   
        await buyerInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let ClassInput = element(by.xpath("//*[contains(text(),'raleti')]"));   
        await ClassInput.click();
        await browser.driver.sleep(800);
    }
    async Editiconclick()
    {
        console.log("In Editiconclick");
        let Editicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'Edit')]"));

        return await Editicon.click();       
    }
    async Viewiconclick()
    {
        console.log("In Viewiconclick");
        let Viewicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'View')]"));

        return await Viewicon.click();       
    }
   
    
    async successOk()
    {
        let ok = await element(by.xpath('//*[contains(text(), "OK")]'));
        await ok.click();
        await browser.driver.sleep(8000);
    }
    async ClickCatagoryLink(): Promise<any> 
    {
        console.log("In Classlink");
        let CatagoryLink = element(by.className("listitem"));
        return await CatagoryLink.click();
    }
}