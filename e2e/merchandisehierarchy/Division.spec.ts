import { browser, ExpectedConditions as EC, $, $$, element, by } from 'protractor'
import { HomePage } from '../home/home.page'
import { LoginPage } from '../login/login.page'
import { DivisionPage } from "./Division.page";
import { protractor } from 'protractor/built/ptor';
declare let expect: any


describe('Division search', function () {
    let homePage: HomePage;
    let loginpage: LoginPage;
    let divisionPage: DivisionPage;
    beforeEach(async () => {
       
       
    })    

    it('should open the Division Screen', async (): Promise<any> => {
        loginpage = new LoginPage();
        await loginpage.open();      
        await loginpage.login();  
        homePage = new HomePage();
        divisionPage = new DivisionPage();
        browser.waitForAngularEnabled(false);
        await browser.driver.sleep(5000);
        await homePage.mdmmenuClick();
        let organization  = await homePage.merchandisehierarchy; 
        expect(organization.isPresent()).toBeTruthy();
        await organization.click();       
        await divisionPage.DivisionMenu.click();
        await browser.driver.sleep(5000);
        expect(divisionPage.addBtn.isPresent()).toBeTruthy();
        await homePage.ClickAddButton();
        await browser.driver.sleep(5000);
          
       
    }) 

    it('should add/Edit the Division', async (): Promise<any> =>  
      
     {
       homePage = new HomePage();
       divisionPage = new DivisionPage();
       await browser.driver.sleep(10);      
       await divisionPage.typeCompany("TSC BAHRAIN(1001)");
       await divisionPage.typeDivisionId("6655");
       await divisionPage.typedescription("HOMECENTRE 09");
       await divisionPage.typemanager("Firas");
       await divisionPage.typeProxymanager("raleti");
       await homePage.saveButtonClick();
       let successmsg = await divisionPage.fxbutton;
       expect(successmsg.isPresent()).toBeTruthy();
       await divisionPage.successOk();
       await element(by.className('toolbar-icons searchIcon')).click();
       await browser.driver.sleep(5000);

     })

     
    it('should open the Division search result page', async (): Promise<any> => {
   
        await divisionPage.typeCompany("TSC BAHRAIN(1001)");
        await browser.driver.sleep(500);
        expect(divisionPage.searchbtn.isPresent()).toBeTruthy();
        await homePage.ClickSearchButton();
        await browser.driver.sleep(5000); 
        await divisionPage.Viewiconclick();
        await browser.driver.sleep(5000);
        await element(by.className('closeIcon')).click();
        await divisionPage.Editiconclick();
        await browser.driver.sleep(5000);
        await homePage.saveButtonClick();
        let successmsg = await divisionPage.fxbutton;
        expect(successmsg.isPresent()).toBeTruthy();
        await divisionPage.successOk();
        expect(divisionPage.listitem.isPresent()).toBeTruthy();
        await divisionPage.ClickDepartmentLink();
        await browser.driver.sleep(5000)
       
        })
    

    afterEach(() => {

    });
});