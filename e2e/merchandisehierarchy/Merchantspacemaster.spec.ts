import { browser, ExpectedConditions as EC, $, $$, element, by } from 'protractor'
import { HomePage } from '../home/home.page'
import { LoginPage } from '../login/login.page'
import { MerchantspacemasterPage } from "./Merchantspacemaster.page";
import { protractor } from 'protractor/built/ptor';
declare let expect: any


describe('Merchantspacemaster search', function () {
    let homePage: HomePage;
    let loginpage: LoginPage;
    let merchantspacemasterPage: MerchantspacemasterPage;
    beforeEach(async () => {
       
       
    })    

    it('should open the Merchantspacemaster Addpage', async (): Promise<any> => {
        loginpage = new LoginPage();
        await loginpage.open();      
        await loginpage.login();  
        homePage = new HomePage();
        merchantspacemasterPage = new MerchantspacemasterPage();
        browser.waitForAngularEnabled(false);
        await browser.driver.sleep(5000);
        await homePage.mdmmenuClick();
        let organization  = await homePage.merchandisehierarchy; 
        expect(organization.isPresent()).toBeTruthy();
        await organization.click();       
        await merchantspacemasterPage.MerchantspacemasterMenu.click();
        await browser.driver.sleep(5000);
        expect(merchantspacemasterPage.addBtn.isPresent()).toBeTruthy();
        await homePage.ClickAddButton();
        await browser.driver.sleep(5000);
       
    }) 
    it('should add/Edit the Merchantspacemaster', async (): Promise<any> => {
      await browser.driver.sleep(10);      
      await merchantspacemasterPage.typeCompany("TSC KUWAIT(1)");
      await merchantspacemasterPage.typeDivision("GROCERIES(501)");
      await merchantspacemasterPage.typeDepartment("FRESH FOOD(5002)");
      await merchantspacemasterPage.typeCategory("FRUIT & VEG(50002)");
      await merchantspacemasterPage.typeSpacelevel("COMPANY");
      await merchantspacemasterPage.typeSpaceorganization("TSC BAHRAIN");
      await merchantspacemasterPage.typePrimarybrand("FAIRY");
      await homePage.saveButtonClick();
      let successmsg = await  merchantspacemasterPage.fxbutton;
      expect(successmsg.isPresent()).toBeTruthy();
      await merchantspacemasterPage.successOk();
      await element(by.className('toolbar-icons searchIcon')).click();
      await browser.driver.sleep(5000);
    

    })

    it('should open the Merchantspacemaster search page', async (): Promise<any> => {

        await merchantspacemasterPage.typeCompany("TSC BAHRAIN(1001)");
        expect(merchantspacemasterPage.searchbtn.isPresent()).toBeTruthy();
        await homePage.ClickSearchButton();
        await browser.driver.sleep(5000); 
        await merchantspacemasterPage.Viewiconclick();
        await browser.driver.sleep(5000);
        await element(by.className('closeIcon')).click();
        await merchantspacemasterPage.Editiconclick();
        await browser.driver.sleep(5000);
        await homePage.saveButtonClick();
        let successmsg = await  merchantspacemasterPage.fxbutton;
        expect(successmsg.isPresent()).toBeTruthy();
        await merchantspacemasterPage.successOk();
        await element(by.className('closeIcon')).click();
        })
   

    afterEach(() => {

    });
});