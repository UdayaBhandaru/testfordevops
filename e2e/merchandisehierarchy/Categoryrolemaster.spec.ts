import { browser, ExpectedConditions as EC, $, $$, element, by } from 'protractor'
import { HomePage } from '../home/home.page'
import { LoginPage } from '../login/login.page'
import { CategoryrolemasterPage } from "./Categoryrolemaster.page";
import { protractor } from 'protractor/built/ptor';
declare let expect: any


describe('Class search', function () {
    let homePage: HomePage;
    let loginpage: LoginPage;
    let categoryrolemasterPage: CategoryrolemasterPage;
    beforeEach(async () => {
       
       
    })    

    it('should open the categoryrolemaster Addpage', async (): Promise<any> => {
        loginpage = new LoginPage();
        await loginpage.open();      
        await loginpage.login();  
        homePage = new HomePage();
        categoryrolemasterPage = new CategoryrolemasterPage();
        browser.waitForAngularEnabled(false);
        await browser.driver.sleep(5000);
        await homePage.mdmmenuClick();
        let organization  = await homePage.merchandisehierarchy; 
        expect(organization.isPresent()).toBeTruthy();
        await organization.click();       
        await categoryrolemasterPage.CategoryrolemasterMenu.click();
        await browser.driver.sleep(5000);
        expect(categoryrolemasterPage.addBtn.isPresent()).toBeTruthy();
        await homePage.ClickAddButton();
       
    }) 
    it('should open the add the Categoryrolemaster', async (): Promise<any> => {
        await browser.driver.sleep(5000);
        await categoryrolemasterPage.typeCompany("TSC KUWAIT(1)");
       await categoryrolemasterPage.typeDivision("GROCERIES(501)");
       await categoryrolemasterPage.typeDepartment("FRESH FOOD(5002)");
       await categoryrolemasterPage.typeCategory("FRUIT & VEG(50002)");
       await categoryrolemasterPage.typeRolelevel("COMPANY");
       await categoryrolemasterPage.typeRole("TSC BAHRAIN");
       await categoryrolemasterPage.typeStrategytype("PRIMARY");
       await categoryrolemasterPage.typeCategoryrole("PRIMARY");
       await categoryrolemasterPage.typeCategorystrategy("SECONDARY");
       await homePage.saveButtonClick();
       await browser.driver.sleep(5000);
       let successmsg = await categoryrolemasterPage.fxbutton;
       expect(successmsg.isPresent()).toBeTruthy();
       await categoryrolemasterPage.successOk();
       await element(by.className('toolbar-icons searchIcon')).click();
       await browser.driver.sleep(5000);
       await categoryrolemasterPage.typeCompany("TSC KUWAIT(1)");
    }) 
 
    it('should open the add/Edit the Categoryrolemaster', async (): Promise<any> => {
        await browser.driver.sleep(500);
        expect(categoryrolemasterPage.searchbtn.isPresent()).toBeTruthy();
        await homePage.ClickSearchButton();
        await browser.driver.sleep(5000); 
        await categoryrolemasterPage.Viewiconclick();
        await browser.driver.sleep(5000);
        await element(by.className('closeIcon')).click();
        await categoryrolemasterPage.Editiconclick();
        await homePage.saveButtonClick();
        let successmsg = await categoryrolemasterPage.fxbutton;
        expect(successmsg.isPresent()).toBeTruthy();
        await categoryrolemasterPage.successOk();
        expect (categoryrolemasterPage.listitem.isPresent()).toBeTruthy();
        await categoryrolemasterPage.ClickClassLink();
        await browser.driver.sleep(5000);
       
    })
    
   

    afterEach(() => {

    });
});