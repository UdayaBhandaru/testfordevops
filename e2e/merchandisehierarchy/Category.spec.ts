import { browser, ExpectedConditions as EC, $, $$, element, by } from 'protractor'
import { HomePage } from '../home/home.page'
import { LoginPage } from '../login/login.page'
import { CategoryPage } from "./Category.page";
import { protractor } from 'protractor/built/ptor';
declare let expect: any


describe('Category search', function () {
    let homePage: HomePage;
    let loginpage: LoginPage;
    let categoryPage: CategoryPage;
    beforeEach(async () => {
       
       
    })   
 
    it('should open the Category Addpage', async (): Promise<any> => {
        loginpage = new LoginPage();
        await loginpage.open();      
        await loginpage.login();  
        homePage = new HomePage();
        categoryPage = new CategoryPage();
        browser.waitForAngularEnabled(false);
        await browser.driver.sleep(500);
        await homePage.mdmmenuClick();
        let organization  = await homePage.merchandisehierarchy; 
        expect(organization.isPresent()).toBeTruthy();
        await organization.click();       
        await categoryPage.CategoryMenu.click();
        await browser.driver.sleep(5000);
        expect(categoryPage.addBtn.isPresent()).toBeTruthy();
        await homePage.ClickAddButton();
        await browser.driver.sleep(1000);  
       
    }) 

    it('should add/Edit the Category', async (): Promise<any> =>  {

        await categoryPage.typeCompany("TSC KUWAIT(1)");
        await categoryPage.typeDivision("SUPERMARKET(1)");
        await categoryPage.typeDepartment("Grocery Foods(40)");
        await categoryPage.typeCategory("0895");
        await categoryPage.typedescription("INDOOR FURNITURE");
        await categoryPage.typetotalmarketamount("5334");
        await categoryPage.typebuyer("raleti");
        await categoryPage.typeMerchandise("Shaik");
        await categoryPage.typevatcode("VAT02");
        await homePage.saveButtonClick();
        await browser.driver.sleep(5000);
        let successmsg = await categoryPage.fxbutton;
        expect(successmsg.isPresent()).toBeTruthy();
        await categoryPage.successOk();
        await element(by.className('toolbar-icons searchIcon')).click();
        await browser.driver.sleep(5000);


    })

    it('should open the Category search page', async (): Promise<any> => {

        await categoryPage.typeCompany("TSC BAHRAIN(1001)");
        expect(categoryPage.searchbtn.isPresent()).toBeTruthy();
        await homePage.ClickSearchButton();
        await browser.driver.sleep(5000); 
        await categoryPage.Viewiconclick();
        await browser.driver.sleep(5000);
        await element(by.className('closeIcon')).click();
        await categoryPage.Editiconclick();
        await homePage.saveButtonClick();
        let successmsg = await categoryPage.fxbutton;
        expect(successmsg.isPresent()).toBeTruthy();
        await categoryPage.successOk();
        expect (categoryPage.listitem.isPresent()).toBeTruthy();
        await categoryPage.departmentLink();
        })
    
   

    afterEach(() => {

    });
});