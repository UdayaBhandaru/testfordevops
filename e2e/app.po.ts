import { browser, by, element } from 'protractor';

export class TestingprojPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
      return element(by.id('md-tab-label-0-0')).getText();
  }
}
