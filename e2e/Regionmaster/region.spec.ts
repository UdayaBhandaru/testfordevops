import { browser, ExpectedConditions as EC, $, $$, element, by } from 'protractor'
import { HomePage } from '../home/home.page'
import { LoginPage } from '../login/login.page'
import { RegionPage } from "./region.page";
import { protractor } from 'protractor/built/ptor';
//import { orgcountryPage} from '../orgcountrymaster/orgcountry.page';

declare let expect: any


describe('Region search', function () {
    let homePage: HomePage;
    let loginpage: LoginPage;
    let regionPage: RegionPage;
    beforeEach(async () => {
       
       
    })    

    it('should open the Region page', async (): Promise<any> => {
        loginpage = new LoginPage();
        await loginpage.open();      
        await loginpage.login();  
        homePage = new HomePage();
        regionPage = new RegionPage();
        browser.waitForAngularEnabled(false);
        await browser.driver.sleep(5000);
        await homePage.mdmmenuClick();
        let organization  = await homePage.orgHierarchy; 
        expect(organization.isPresent()).toBeTruthy();
        await organization.click();       
        await regionPage.regionMenu.click();
       
    }) 

  
        it('should add/edit the Region page', async (): Promise<any> =>  {
            await browser.driver.sleep(10000); 
            await homePage.ClickAddButton();        
            await element(by.xpath('//*[contains(text(),"SAVE & CLOSE")]')).click();       
             await element(by.xpath('//*[contains(text(),"RESET")]')).click();
             await regionPage.typeCompany("TSC KUWAIT(1)"); 
             await regionPage.typeCountry("AMERICAN SAMOA");
             await regionPage.typecode("7985");   
             await regionPage.typeRegionName("TSc");
             await regionPage.typelegalname("Kuwait");
             await regionPage.typeRegionName("TSK")
            await homePage.saveButtonClick();
            await browser.driver.sleep(5000);
            let successmsg = await regionPage.fxbutton;
            expect(successmsg.isPresent()).toBeTruthy();
            await regionPage.successOk();
            await browser.driver.sleep(500);
            await element(by.className('closeIcon')).click();
            await browser.driver.sleep(5000);
        })

        it('should open the Region search page', async (): Promise<any> => {
        
            expect(regionPage.searchbtn.isPresent()).toBeTruthy();
            await homePage.ClickSearchButton();
            await browser.driver.sleep(5000); 
            await regionPage.Viewiconclick();
            await browser.driver.sleep(5000);
            await element(by.className('closeIcon')).click();
            await regionPage.Editiconclick();
            await browser.driver.sleep(5000);
            await homePage.saveButtonClick();
            let successmsg = await regionPage.fxbutton;
            expect(successmsg.isPresent()).toBeTruthy();
             await regionPage.successOk();
             await browser.driver.sleep(5000); 
             await regionPage.ClickOrgcountrylink();
            await browser.driver.sleep(5000);    
            await homePage.Editiconclick(); 
            await homePage.saveButtonClick();
            await regionPage.successOk();
            await browser.driver.sleep(5000);
             await element(by.className('toolbar-icons searchIcon')).click();
            
         }) 
   
     
    afterEach(() => {

    });
});