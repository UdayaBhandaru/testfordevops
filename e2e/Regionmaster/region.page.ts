import { browser, element, by, $, WebElement } from 'protractor';
import { BasePage } from '../core/base.page';
import { HomePage } from '../home/home.page';
import { DropDownHelper } from "../core/DropdownHelper";
import { DateFormat } from "../core/dateFormat";

export class RegionPage extends BasePage 
{
   public regionMenu = element(by.css("a[href*='Region']"));
   public addBtn = element(by.id("searchbarAdd"));
    public searchButton = element(by.name('search'));
    public fxbutton = element(by.className('fx-button'));
    public searchbtn = element(by.className('searchbtn'));
    private dropdownHelper: DropDownHelper;
    async RegionPageSearch(): Promise<any>
    {
        let submit = await element(by.name('search'));
        return submit.click();
    }

    async RegionPageAdd(): Promise<any>
    {
        var span1 = element(by.binding('person.name'));
    } 

    async typeCompanyId(CompanyId: string)
    {
        let companyIdInput = await $('[ng-reflect-name="CompanyId"]');       
        await companyIdInput.sendKeys(CompanyId);
        await browser.driver.sleep(800);
    }


    async typeCompany(Company: string)  
    {
        let CompanyInput = await element(by.name('Company'));   
        await CompanyInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let orgcountryInput = element(by.xpath("//*[contains(text(),'TSC KUWAIT(1)')]"));   
        await orgcountryInput.click();
        await browser.driver.sleep(8000);
    }

    async typeCountry(Country: string)  
    {
        let CountryInput = await element(by.name('Country'));   
        await CountryInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let orgcountryInput = element(by.xpath("//*[contains(text(),'AMERICAN SAMOA')]"));   
        await orgcountryInput.click();
        await browser.driver.sleep(8000);
    }
     
    async typecode(code: string)
    {
        let codeInput = await element(by.name('regionCode')); 
        await codeInput.sendKeys(code);
        await browser.driver.sleep(5000);
    }
    
    async typeRegionName(Regionname: string)
    {
        let RegionnameInput = await element(by.name('regionName')); 
        await RegionnameInput.sendKeys(Regionname);
        await browser.driver.sleep(800);
    }



    async typelegalname(legalname: string)
    {
        let legalnameInput = await element(by.name('legalName'));
        await legalnameInput.sendKeys(legalname);
        await browser.driver.sleep(5000);
    }

    async Editiconclick()
    {
        console.log("In Editiconclick");
        let Editicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'Edit')]"));

        return await Editicon.click();       
    }
    async Viewiconclick()
    {
        console.log("In Viewiconclick");
        let Viewicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'View')]"));

        return await Viewicon.click();       
    }
   
    
    async successOk()
    {
        let ok = await element(by.xpath("//*[contains(text(), 'OK')]"));
        return ok.click();
    }
    async ClickOrgcountrylink(): Promise<any> 
    {
        console.log("In Orgcountrylink");
        let Orgcountrylink = element(by.className("listitem"));
        return await Orgcountrylink.click();
    }
    async ClickFormatlink(): Promise<any> 
    {
        console.log("In ClickFormatink");
        let Formatink = element(by.className("listitem"));
        return await Formatink.click();
    }
    async ClickRegionlink(): Promise<any> 
    {
        console.log("In Regionlink");
        let Regionlink = element(by.className("formatLink"));
        return await Regionlink.click();
    }
}