import { Config, browser } from 'protractor'

let conf: Config = {
    // Connecting directly to ChromeDriverServer
    directConnect: true,
    specs: [
        // './organizationhierarchy/organizationhierarchy.spec.js',
        //  './companymaster/company.spec.js',
       //'./orgcountrymaster/orgcountry.spec.js',
        // './Regionmaster/region.spec.js',
        //   './formatmaster/Format.spec.js',
        //  './Locationmaster/location.spec.js',
        // './merchandisehierarchy/Division.spec.js',
        //  './merchandisehierarchy/Department.spec.js',
        //  './merchandisehierarchy/Category.spec.js',
        // './merchandisehierarchy/Class.spec.js',
        // './merchandisehierarchy/Subclass.spec.js',
        // './merchandisehierarchy/Categoryrolemaster.spec.js',
        //  './merchandisehierarchy/Merchantspacemaster.spec.js',
        //  './value added tax/vatregion.spec.js',
        //   './value added tax/vatcode.spec.js',
        //   './value added tax/vatrates.spec.js',
        // './value added tax/vatregionvatcode.spec.js',
        //'./Brandmaster/Brand.spec.js',
         //'./Brandmaster/Brandcategory.spec.js',
          //  './Itemlist/Itemlist.spec.js',
        //    './Costzone/Costchangereason.spec.js',
        //   './Costzone/Costzonegroup.spec.js',
        //    './Pricezone/Pricechangereason.spec.js',
        // './Pricezone/Pricezonegroup.spec.js',
    // './Costmanagement/Costmanagement.spec.js',
     // './Pricemanagement/Pricemanagement.spec.js',
     
      //  './Itemmaster/Itemmaster.spec.js',
       // './Ordermanagement/order.spec.js',
     
      './Supplier/supplieraddress.spec.js',
       './Supplier/supplier.spec.js',
    //    './phase/phase.spec.js',
    ],
    baseUrl: 'http://10.138.71.249:8767/Account/Login',
    allScriptsTimeout: 1000000,
    onPrepare: () => {

        let ConsoleReporter = require('jasmine2-reporter').Jasmine2Reporter
        let console_reporter_options = {
            startingSpec: true
        }

        browser.manage().timeouts().implicitlyWait(2500000);
        jasmine.getEnv().addReporter(new ConsoleReporter(console_reporter_options))

        // Adding reporting that is applicable for Jenkins or other CI tool
        // Provided by: https://github.com/larrymyers/jasmine-reporters
        let JUnitXmlReporter = require('jasmine-reporters').JUnitXmlReporter
        let junit_reporter_options = {
            savePath: 'test_results/',
            consolidateAll: true
        }
        jasmine.getEnv().addReporter(new JUnitXmlReporter(junit_reporter_options))

        // Specifying global beforeEach and afterEach jasmine2 hooks.
        beforeEach(() => {
            // Adding .toAppear() and .toDisappear() into available matchers.
            // https://github.com/Xotabu4/jasmine-protractor-matchers
            jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000000;
            let matchers = require('jasmine-protractor-matchers')
            jasmine.addMatchers(matchers);
        });

        afterEach(async () => {
            // Clearing browser data after each test
            await browser.manage().deleteAllCookies();
            await browser.executeScript('window.sessionStorage.clear(); window.localStorage.clear();')
        });
    },
    useAllAngular2AppRoots: true,
    capabilities: {
        'browserName': 'chrome',
        'chromeOptions': {
            // 'args': ["--headless", "--disable-gpu", "--window-size=800,600"],

        }
    },
    SELENIUM_PROMISE_MANAGER: false
};

exports.config = conf;