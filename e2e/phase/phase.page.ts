import { browser, element, by, $, WebElement } from 'protractor';
import { BasePage } from '../core/base.page';
import { HomePage } from '../home/home.page';
import { DropDownHelper } from "../core/DropdownHelper";
import { DateFormat } from "../core/dateFormat";

export class PhasePage extends BasePage 
{
    public PhaseMenu = element(by.css("a[href*='/Phase']"));
    public addBtn = element(by.id("searchbarAdd"));
    public fxbutton = element(by.className('fx-button'));
    public searchbtn = element(by.className('searchbtn'));
    public listitem = element(by.className('listitem'));
    private dropdownHelper: DropDownHelper;
    async PhasePageSearch(): Promise<any>
    {
        let submit = await element(by.name('search'));
        return submit.click();
    } 

    async PhasePageAdd(): Promise<any>
    {
        var span1 = element(by.binding('person.name'));
    }

    async typeCompany(companyname: string)
    {
       
        let CompanyInput = await element(by.name('Company'));   
        await CompanyInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let CategoryInput = element(by.xpath("//*[contains(text(),'TSC BAHRAIN(1001)')]"));   
        await CategoryInput.click();
        await browser.driver.sleep(800);
    }

    async typeName(name: string) {
        let supplierNameInput = await element(by.name('phaseName'));
        await supplierNameInput.sendKeys(name);
        await browser.driver.sleep(800);
    } 
   
    async typeSeason(season: string)
    {
        let seasonInputs = await element(by.name('Season'));   
        await seasonInputs.click();    
        this.dropdownHelper = new DropDownHelper();
        let seasonInput = element(by.xpath("//*[contains(text(),'WINTER TEST')]"));   
        await seasonInput.click();
        await browser.driver.sleep(800);
    }

    async typeStartDate(dateMMddYYYY : string) 
    {
        let startDateInput = await element(by.css("[name = 'startdate']"));
       

        await startDateInput.sendKeys(dateMMddYYYY);   
        // browser.driver.sleep(5000); 
        // await startDateInput.sendKeys();
        await browser.driver.sleep(800);
    }
 
    async typeEndDate()
    {
        let endDateInput = await element(by.name('enddate'));
        const endDate = new Date();
        endDate.setDate(endDate.getDate() + 30);
        const dateMMddYYYY = await DateFormat.dateMMddYYYY(endDate);
       
        await endDateInput.sendKeys(dateMMddYYYY);        
        await browser.driver.sleep(800);
    }
    
    async Editiconclick()
    {
        console.log("In Editiconclick");
        let Editicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'Edit')]"));

        return await Editicon.click();       
    }
    async Viewiconclick()
    {
        console.log("In Viewiconclick");
        let Viewicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'View')]"));

        return await Viewicon.click();       
    }
   
    async successOk()
    {
        let ok = await element(by.xpath('//*[contains(text(), "OK")]'));
        await ok.click();
        await browser.driver.sleep(8000);
    }
}