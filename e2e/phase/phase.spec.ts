import { browser, ExpectedConditions as EC, $, $$, element, by } from 'protractor'
import { HomePage } from '../home/home.page'
import { LoginPage } from '../login/login.page'
import { PhasePage } from "./phase.page";
import { protractor } from 'protractor/built/ptor';
import { DateFormat } from '../core/dateFormat';
declare let expect: any


describe('phase search', function () {
    let homePage: HomePage;
    let loginpage: LoginPage;
    let phasePage: PhasePage;
    beforeEach(async () => {
      
    }) 
 
    
    it('should open the phase', async (): Promise<any> => {
       
        loginpage = new LoginPage();
        await loginpage.open();      
        await loginpage.login();  
        homePage = new HomePage();
        phasePage = new PhasePage();
        browser.waitForAngularEnabled(false);
        await browser.driver.sleep(1000);
        await homePage.mdmmenuClick();
        let organization  = await homePage.season; 
        expect(organization.isPresent()).toBeTruthy();
        await organization.click();       
        await phasePage.PhaseMenu.click();
        await browser.driver.sleep(5000);
        expect(phasePage.addBtn.isPresent()).toBeTruthy();
        await homePage.ClickAddButton();
        await browser.driver.sleep(1000);  
       
    })

    it('should add/edit the phase', async (): Promise<any> =>
    {
                      
        await phasePage.typeCompany("TSC BAHRAIN(1001)");
        await phasePage.typeSeason("WINTER TEST");
        await phasePage.typeName("TSC2");
        const startDate = new Date();
        startDate.setDate(startDate.getDate() + 2);
        const dateMMddYYYY = await DateFormat.dateMMddYYYY(startDate);
        await phasePage.typeStartDate("adsfasd");
        await phasePage.typeEndDate();  
        await homePage.saveButtonClick();
        let successmsg = await phasePage.fxbutton;
        expect(successmsg.isPresent()).toBeTruthy();
        await phasePage.successOk();
    })
    it('should open the Phase search page', async (): Promise<any> => {

        expect(phasePage.searchbtn.isPresent()).toBeTruthy();
        await homePage.ClickSearchButton();
        await browser.driver.sleep(5000); 
        await phasePage.Viewiconclick();
        await browser.driver.sleep(5000);
        await element(by.className('closeIcon')).click();
        await phasePage.Editiconclick();
        await browser.driver.sleep(5000);
        let successmsg = await phasePage.fxbutton;
        expect(successmsg.isPresent()).toBeTruthy();
        await phasePage.successOk();
        
       
        })

    afterEach(() => {

    });
});