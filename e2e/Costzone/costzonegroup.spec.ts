import { browser, ExpectedConditions as EC, $, $$, element, by } from 'protractor'
import { HomePage } from '../home/home.page'
import { LoginPage } from '../login/login.page'
import { CostzonegroupPage } from "./Costzonegroup.page";
import { protractor } from 'protractor/built/ptor';
declare let expect: any


describe('Costzonegroup search', function () {
    let homePage: HomePage;
    let loginpage: LoginPage;
    let costzonegroupPage: CostzonegroupPage;
    beforeEach(async () => {
    })    
    it('should open the costzonegroup Add page', async (): Promise<any> => {
        loginpage = new LoginPage();
        await loginpage.open();      
        await loginpage.login();  
        homePage = new HomePage();
        costzonegroupPage = new CostzonegroupPage();
        await browser.driver.sleep(5000);
        browser.waitForAngularEnabled(false);
        await homePage.mdmmenuClick();
        let costzone  = await homePage.costzone; 
        expect(costzone.isPresent()).toBeTruthy();
        await costzone.click();  
        await homePage.costzonegroup.click();   
        expect(costzonegroupPage.addBtn.isPresent()).toBeTruthy();
        await homePage.ClickAddButton();
        await browser.driver.sleep(5000);
        
    }) 

    it('should add/Edit the costzonegroup', async (): Promise<any> => {

      await costzonegroupPage.typedescription("kwt cost zone group Mini");
      await costzonegroupPage.typeOrganizationlevel("Location");
      await homePage.saveButtonClick();
      await browser.driver.sleep(5000); 
      let successmsg = await costzonegroupPage.fxbutton;
      expect(successmsg.isPresent()).toBeTruthy();
      await costzonegroupPage.successOk();
      await element(by.className('closeIcon')).click();
    })

    it('should open the costzonegroup search page', async (): Promise<any> => {

      expect (costzonegroupPage.searchbtn.isPresent()).toBeTruthy();
        await homePage.ClickSearchButton();
        await browser.driver.sleep(5000); 
        await costzonegroupPage.Viewiconclick();
        await browser.driver.sleep(5000);
        await element(by.className('closeIcon')).click();
        await costzonegroupPage.Editiconclick();
        await homePage.saveButtonClick();
        let successmsg = await costzonegroupPage.fxbutton;
        expect(successmsg.isPresent()).toBeTruthy();
        await costzonegroupPage.successOk();
        expect (costzonegroupPage.listitem.isPresent()).toBeTruthy();
        await costzonegroupPage.ClickCostZoneLink();
        await browser.driver.sleep(5000);
       
        })

    afterEach(() => {

    });
});