import { browser, element, by, $, WebElement } from 'protractor';
import { BasePage } from '../core/base.page';
import { HomePage } from '../home/home.page';
import { DropDownHelper } from "../core/DropdownHelper";

export class CostzonegroupPage extends BasePage 
{
   public CostzonegroupMenu = element(by.css("a[href*='CostZonegroup']"));
   public addBtn = element(by.id("searchbarAdd"));
   public fxbutton = element(by.className('fx-button'));
   public searchbtn = element(by.className('searchbtn'));
   public listitem = element(by.className('listitem'));
    private dropdownHelper: DropDownHelper;
    async CostzonegroupPageSearch(): Promise<any>
    {
        let submit = await element(by.id('searchbarSearch'));
        return submit.click();
    }

    async CostzonegroupPageAdd(): Promise<any>
    {
        var span1 = element(by.binding('person.name'));
    }
    

    async typedescription(description: string)
    {
        let descriptionInput = await element(by.name('description')); 
        await descriptionInput.sendKeys(description);
        await browser.driver.sleep(5000);
    }

    async typeOrganizationlevel(Organizationlevel: string)  
    {
        let OrganizationlevelInput = await element(by.name('Organization Level'));   
        await OrganizationlevelInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let costzonegroupInput = element(by.xpath("//*[contains(text(),'Location')]")); 
     
        await costzonegroupInput.click();
        await browser.driver.sleep(8000);
    }
    
    async Editiconclick()
    {
        console.log("In Editiconclick");
        let Editicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'Edit')]"));

        return await Editicon.click();       
    }
    async Viewiconclick()
    {
        console.log("In Viewiconclick");
        let Viewicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'View')]"));

        return await Viewicon.click();       
    }
   
    async successOk()
    {
        let ok = await element(by.xpath('//*[contains(text(), "OK")]'));
        await ok.click();
        await browser.driver.sleep(8000);
    }
    async ClickCostZoneLink(): Promise<any> 
    {
        console.log("In  ClickCostZoneLink");
        let  ClickCostZoneLink = element(by.className("listitem"));
        return await  ClickCostZoneLink.click();
    }
}