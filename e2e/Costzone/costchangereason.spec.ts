import { browser, ExpectedConditions as EC, $, $$, element, by } from 'protractor'
import { HomePage } from '../home/home.page'
import { LoginPage } from '../login/login.page'
import { CostchangereasonPage } from "./Costchangereason.page";
import { protractor } from 'protractor/built/ptor';
declare let expect: any


describe('Costchangereason search', function () {
    let homePage: HomePage;
    let loginpage: LoginPage;
    let costchangereasonPage: CostchangereasonPage;
    beforeEach(async () => {
       
       
    })    

    it('should open the Costchangereason Add page', async (): Promise<any> => {
        loginpage = new LoginPage();
        await loginpage.open();      
        await loginpage.login();  
        homePage = new HomePage();
        costchangereasonPage = new CostchangereasonPage();
        browser.waitForAngularEnabled(false);
        await browser.driver.sleep(5000);
        await homePage.mdmmenuClick();
        let costzone  = await homePage.costzone; 
        browser.waitForAngularEnabled(false);
        expect(costzone.isPresent()).toBeTruthy();
        await costzone.click();  
        await homePage.costchangereason.click();   
        expect(costchangereasonPage.addBtn.isPresent()).toBeTruthy();
        await browser.driver.sleep(5000);
        await homePage.ClickAddButton();
        await browser.driver.sleep(5000);
       
    }) 
    it('should add the costchangereason', async (): Promise<any> =>  {
  
       await costchangereasonPage.typedescription("New Year Cost rate");
       await homePage.saveButtonClick();
       let successmsg = await costchangereasonPage.fxbutton;
       expect(successmsg.isPresent()).toBeTruthy();
       await costchangereasonPage.successOk();
       await element(by.className('closeIcon')).click();
     
     })

     it('should open the Costchangereason search page', async (): Promise<any> => {
        await browser.driver.sleep(5000);
        expect (costchangereasonPage.searchbtn.isPresent()).toBeTruthy();
        await homePage.ClickSearchButton(); 
        await browser.driver.sleep(5000);
        await costchangereasonPage.Viewiconclick();
        await browser.driver.sleep(5000);
        await element(by.className('closeIcon')).click();
        await costchangereasonPage.Editiconclick();
        await browser.driver.sleep(1000);
        await homePage.saveButtonClick();
        let successmsg = await costchangereasonPage.fxbutton;
        expect(successmsg.isPresent()).toBeTruthy();
        await costchangereasonPage.successOk();
        })
    
    afterEach(() => {

    });
});