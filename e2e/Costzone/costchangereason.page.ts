import { browser, element, by, $, WebElement } from 'protractor';
import { BasePage } from '../core/base.page';
import { HomePage } from '../home/home.page';

export class CostchangereasonPage extends BasePage 
{
   public costchangereasonMenu = element(by.css("a[href*='Cost Change Reason]"));
   public addBtn = element(by.id("searchbarAdd"));
   public fxbutton = element(by.className('fx-button'));
   public searchbtn = element(by.className('searchbtn'));
   public listitem = element(by.className('listitem'));
    public OkButton = element(by.name('OK'));
    
    async costchangereasonPageSearch(): Promise<any>
    {
        let submit = await element(by.name('search'));
        return submit.click();
    }
 
    async costchangereasonPageAdd(): Promise<any>
    {
        var span1 = element(by.binding('person.name'));
    }


   
    async typedescription(description: string)
    {
        let descriptionInput = await element(by.name('reasonDesc'));      
        await descriptionInput.sendKeys(description);
        await browser.driver.sleep(5000);
    }

    async Editiconclick()
    {
        console.log("In Editiconclick");
        let Editicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'Edit')]"));

        return await Editicon.click();       
    }
    async Viewiconclick()
    {
        console.log("In Viewiconclick");
        let Viewicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'View')]"));

        return await Viewicon.click();       
    }
   
    

    
    async successOk()
    {
        let ok = await element(by.xpath('//*[contains(text(), "OK")]'));
        await ok.click();
        await browser.driver.sleep(8000);
    }
}