import { browser, element, by, $, WebElement } from 'protractor';
import { BasePage } from '../core/base.page';
import { HomePage } from '../home/home.page';
import { DropDownHelper } from "../core/DropdownHelper";
import { DateFormat } from "../core/dateFormat";

export class CostmanagementPage extends BasePage 
{
   public CostmanagementMenu = element(by.css("a[href*='CostChange']"));
   public addBtn = element(by.id("searchbarAdd"));
   public fxbutton = element(by.className('fx-button'));
   public searchbtn = element(by.className('searchbtn'));
   public listitem = element(by.className('listitem'));
   public addRow= element(by.className("AddRow"));
    private dropdownHelper: DropDownHelper;
    async CostmanagementPageSearch(): Promise<any>
    {
        let submit = await element(by.id('searchbarSearch'));
        return submit.click();
    }

    async CostmanagementPageAdd(): Promise<any>
    {
        var span1 = element(by.binding('person.name'));
    }


   
    async typeCostchangedescription(Costchangedescription: string)
    {
        let costchangedescriptionInput = await element(by.name('costChangeDesc'));      
        await costchangedescriptionInput.sendKeys(Costchangedescription);
        await browser.driver.sleep(500);
    }
     
    async typeStatus(Status: string)  
    {
        let StatusInput = await element(by.name('Status'));   
        await StatusInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let CostmanagementInput = element(by.xpath("//*[contains(text(),'Worksheet')]"));   
        await CostmanagementInput.click();
        await browser.driver.sleep(800);
    }

    async typeReason(Reason: string)  
    {
        let ReasonInput = await element(by.name('Reason'));   
        await ReasonInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let CostmanagementInput = element(by.xpath("//*[contains(text(),'COST CHANGE TEST')]"));   
        await CostmanagementInput.click();
        await browser.driver.sleep(800);
    }
    async typeCostchangeorigin(Costchangeorigin: string)  
    {
        let CostchangeoriginInput = await element(by.name('Cost Change Origin'));   
        await CostchangeoriginInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let CostmanagementInput = element(by.xpath("//*[contains(text(),'COST CHANGE ORIGIN2')]"));   
        await CostmanagementInput.click();
        await browser.driver.sleep(800);
    }
    async typeOrgLevel(OrgLevel: string)  
    {
        let Orglevelnput = await element(by.name('Org Level'));   
        await Orglevelnput.click();    
        this.dropdownHelper = new DropDownHelper();
        let CostmanagementInput = element(by.xpath("//*[contains(text(),'COST ZONE')]"));   
        await CostmanagementInput.click();
        await browser.driver.sleep(800);
    }

    async typeOrgLevelValue(OrgLevelValue: string)  
    {
        let   typeOrgLevelValuelnput = await element(by.name('Org Level Value'));   
        await typeOrgLevelValuelnput.click();    
        this.dropdownHelper = new DropDownHelper();
        let CostmanagementInput = element(by.xpath("//*[contains(text(),'BAHRAIN')]"));   
        await CostmanagementInput.click();
        await browser.driver.sleep(800);
    }
   
    async  typeItemBarcode(ItemBarcode: string)
    {
        let ItemBarcodeInput = await element(by.name('item'));      
        await ItemBarcodeInput.sendKeys(ItemBarcode);
        await browser.driver.sleep(500);
    }
    
    async  typeNewUnitCost(NewUnitCost: string)
    {
        let NewUnitCostInput = await element(by.name('newUnitCost'));      
        await NewUnitCostInput.sendKeys(NewUnitCost);
        await browser.driver.sleep(500);
    }

    async typeCostChangeType(CostChangeType: string)  
    {
        let CostChangeTypelnput = await element(by.name('Cost Change Type'));   
        await CostChangeTypelnput.click();    
        this.dropdownHelper = new DropDownHelper();
        let CostmanagementInput = element(by.xpath("//*[contains(text(),'REGULAR COST')]"));   
        await CostmanagementInput.click();
        await browser.driver.sleep(800);
    }
    async typeOrginCountryID(OrginCountryID: string)  
    {
        let OrginCountryIDlnput = await element(by.name('Origin Country ID'));   
        await OrginCountryIDlnput.click();    
        this.dropdownHelper = new DropDownHelper();
        let OrginCountryIDInput = element(by.xpath("//*[contains(text(),'ANGUILLA(AIA)')]"));   
        await OrginCountryIDInput.click();
        await browser.driver.sleep(800);
    }
    async typeRecalculateOrder()
    {
        let RecalculateOrderIndicatorInput = await element(by.xpath("//*[contains(@controlname ,'reCalculateOrderIndicator')]//following-sibling::mat-radio-button[1]"));
        await RecalculateOrderIndicatorInput.click();
        await browser.driver.sleep(500);
    }
    async Editiconclick()
    {
        console.log("In Editiconclick");
        let Editicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'Edit')]"));

        return await Editicon.click();       
    }
    async Viewiconclick()
    {
        console.log("In Viewiconclick");
        let Viewicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'View')]"));

        return await Viewicon.click();       
    }
   
    async addButtonClick()
    {
        console.log("In addButtonClick");
        let saveBtn = element(by.xpath("//button[contains(@class,'AddRow')]"));
        return saveBtn.click();       
    }

    async successOk()
    {
        let ok = await element(by.xpath('//*[contains(text(), "OK")]'));
        await ok.click();
        await browser.driver.sleep(8000);
    }
    
}