import { browser, ExpectedConditions as EC, $, $$, element, by } from 'protractor'
import { HomePage } from '../home/home.page'
import { LoginPage } from '../login/login.page'
import { CostmanagementPage } from "./Costmanagement.page";
import { protractor } from 'protractor/built/ptor';
declare let expect: any


describe('Costmanagement search', function () {
    let homePage: HomePage;
    let loginpage: LoginPage;
    let costmanagementPage: CostmanagementPage;
    beforeEach(async () => {
    })    

    it('should open the Costmanagement Addpage', async (): Promise<any> => {
        loginpage = new LoginPage();
        await loginpage.open();      
        await loginpage.login();  
        homePage = new HomePage();
        costmanagementPage = new CostmanagementPage();
        browser.waitForAngularEnabled(false);
        await browser.driver.sleep(5000);
        let Costmanagement  = await homePage.Costmanagement; 
        expect(Costmanagement.isPresent()).toBeTruthy();
        await Costmanagement.click();       
        await browser.driver.sleep(500);
        await costmanagementPage.typeStatus("Worksheet");
     
    }) 
    
    it('should open the Costmanagement search page', async (): Promise<any> => {

        expect (costmanagementPage.searchbtn.isPresent()).toBeTruthy();
        await homePage.ClickSearchButton();
        await browser.driver.sleep(5000); 
        await costmanagementPage.Viewiconclick();
        await browser.driver.sleep(5000);
        await element(by.className('closeIcon')).click();
        await costmanagementPage.Editiconclick();
        await browser.driver.sleep(5000);
   
    })

    it('should add/Edit the Costmanagement', async (): Promise<any> =>  {

       await costmanagementPage.typeCostchangedescription("Cost Management change");
       await costmanagementPage.typeReason("COST CHANGE TEST");
       await costmanagementPage.typeCostchangeorigin("COST CHANGE ORIGIN1");
       await homePage.saveButtonClick();
       let successmsg = await costmanagementPage.fxbutton;
       expect(successmsg.isPresent()).toBeTruthy();
       await costmanagementPage.successOk();
       await browser.driver.sleep(5000);

    })
     it('should add header page and Click to savebutton', async (): Promise<any> => {
        await browser.driver.sleep(5000);
        await costmanagementPage.typeOrgLevel("COST ZONE");
        await costmanagementPage.typeOrgLevelValue("BAHRAIN");
        await costmanagementPage.typeItemBarcode("100002626");
        await costmanagementPage.typeCostChangeType("REGULAR COST");
        await costmanagementPage.typeOrginCountryID("ANGUILLA(AIA)");
        await costmanagementPage.typeNewUnitCost("1");
        await costmanagementPage.typeRecalculateOrder();
        expect(costmanagementPage.addRow.isPresent()).toBeTruthy();
        await costmanagementPage.addButtonClick();
        await browser.driver.sleep(5000);
     }) 

    afterEach(() => {

    });
});