export class DateFormat {

    public static dateMMddYYYY(date: Date): string {

        let month = (date.getMonth() + 1).toString();
        month = month.length > 1 ? month : "0" + month;

        let day = (date.getDate()).toString();

        day = day.length > 1 ? day : "0" + day;

        const year = date.getFullYear().toString();

        const dateMMddYYYY = month + "/" + day + "/" + year;

        return dateMMddYYYY;
    }

}
