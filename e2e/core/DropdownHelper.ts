import { by, element } from "protractor";

export class DropDownHelper {
    public async selectDropdownOption(selector: string, ddlControl: any) {
        await ddlControl.click();
        await element(by.css(selector)).click();
    }

    public async selectDropdownByTagName(optionTagName: string, ddlControl: any, value: string) {
        await ddlControl.click();
        await element.all(by.tagName(optionTagName)).filter(function (elem, index) {
            return elem.getText().then(function (text) {
                return text === value;
            });
        }).first().click();
    }
}
