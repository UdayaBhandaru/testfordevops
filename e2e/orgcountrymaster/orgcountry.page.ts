import { browser, element, by, $, WebElement } from 'protractor';
import { BasePage } from '../core/base.page';
import { HomePage } from '../home/home.page';
import { DropDownHelper } from "../core/DropdownHelper";

export class OrgcountryPage extends BasePage 
{
   public orgcountryMenu = element(by.css("a[href*='OrgCountry']"));
   public addBtn = element(by.id("searchbarAdd"));
    public searchButton = element(by.name('Search'));
    public searchbtn = element(by.className('searchbtn'));
    public fxbutton = element(by.className('fx-button'));
    private dropdownHelper: DropDownHelper;
    async OrgcountryPageSearch(): Promise<any>
    {
        let submit = await element(by.name('Search'));
        return submit.click();
    }

    async OrgcountryPageAdd(): Promise<any>
    {
        var span1 = element(by.binding('person.name'));
    }

    async typeCompany(Company: string)  
    {
        let CompanyInput = await element(by.name('Company'));   
        await CompanyInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let orgcountryInput = element(by.xpath("//*[contains(text(),'TSC KUWAIT')]"));   
        await orgcountryInput.click();
        await browser.driver.sleep(8000);
    }

    async typeCountry(Country: string)  
    {
        let CountryInput = await element(by.name('Country'));   
        await CountryInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let orgcountryInput = element(by.xpath("//*[contains(text(),'AMERICAN SAMOA(ASM)')]"));   
        await orgcountryInput.click();
        await browser.driver.sleep(8000);
    }

    async typelegalname(legalname: string)
    {
        let legalnameInput = await element(by.name('legalName'));
        await legalnameInput.sendKeys(legalname);
        await browser.driver.sleep(5000);
    }
    
    async Editiconclick()
    {
        console.log("In Editiconclick");
        let Editicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'Edit')]"));

        return await Editicon.click();       
    }
    async Viewiconclick()
    {
        console.log("In Viewiconclick");
        let Viewicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'View')]"));

        return await Viewicon.click();       
    }
   
    
    async successOk()
    {
        let ok = await element(by.xpath("//*[contains(text(), 'OK')]"));
        return ok.click();
    }
    async ClickCompanylink(): Promise<any> 
    {
        console.log("In Companylink");
        let Companylink = element(by.className("listitem"));
        return await Companylink.click();
    }
    async ClickOrgcountrylink(): Promise<any> 
    {
        console.log("In Orgcountrylink");
        let Orgcountrylink = element(by.className("listitem"));
        return await Orgcountrylink.click();
    }
    async ClickRegionlink(): Promise<any> 
    {
        console.log("In Regionlink");
        let Regionlink = element(by.className("RegionLink"));
        return await Regionlink.click();
    }
}