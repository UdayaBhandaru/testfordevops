import { browser, ExpectedConditions as EC, $, $$, element, by } from 'protractor'
import { HomePage } from '../home/home.page'
import { LoginPage } from '../login/login.page'
import { OrgcountryPage } from "./orgcountry.page";
import { CompanyPage } from '../companymaster/Company.page';
import { protractor } from 'protractor/built/ptor';
declare let expect: any


describe('Orgcountry search', function () {
    let homePage: HomePage;
    let loginpage: LoginPage;
    let orgcountryPage: OrgcountryPage;
    let  companyPage = new CompanyPage();
    beforeEach(async () => {
       
       
    })    

    it('should open the Orgcountry Addpage', async (): Promise<any> => {
        loginpage = new LoginPage();
        await loginpage.open();      
        await loginpage.login();  
        homePage = new HomePage();
        orgcountryPage = new OrgcountryPage();
       
        await browser.driver.sleep(1000);
        browser.waitForAngularEnabled(false);
        await homePage.mdmmenuClick();
        let organization  = await homePage.orgHierarchy; 
        browser.waitForAngularEnabled(false);
        expect(organization.isPresent()).toBeTruthy();
        await organization.click();       
        await orgcountryPage.orgcountryMenu.click();
        await browser.driver.sleep(5000);
        expect(orgcountryPage.addBtn.isPresent()).toBeTruthy();
       
    }) 

    it('should add/Edit the Company', async (): Promise<any> => {    
        await browser.driver.sleep(10); 
        await homePage.ClickAddButton();        
        await element(by.xpath('//*[contains(text(),"SAVE & CLOSE")]')).click();       
         await element(by.xpath('//*[contains(text(),"RESET")]')).click();
         await orgcountryPage.typeCompany("TSC KUWAIT"); 
         await orgcountryPage.typeCountry("AMERICAN SAMOA");    
        await homePage.saveButtonClick();
        await browser.driver.sleep(5000);
        let successmsg = await orgcountryPage.fxbutton;
        expect(successmsg.isPresent()).toBeTruthy();
        await orgcountryPage.successOk();
        await browser.driver.sleep(500);
        await element(by.className('closeIcon')).click();
        await browser.driver.sleep(5000);

      })
    it('should open the Orgcountry search page', async (): Promise<any> => {
        expect(orgcountryPage.searchbtn.isPresent()).toBeTruthy();
        await homePage.ClickSearchButton();
        await browser.driver.sleep(5000); 
        await orgcountryPage.Viewiconclick();
        await element(by.className('closeIcon')).click();
        await orgcountryPage.Editiconclick();
        await browser.driver.sleep(10);   
        await homePage.saveButtonClick();
        let successmsg = await orgcountryPage.fxbutton;
        expect(successmsg.isPresent()).toBeTruthy();
         await orgcountryPage.successOk();
         await browser.driver.sleep(5000);         
        })
        
     
    it('should add/Edit the Orgcountry', async (): Promise<any> =>  {      
      
       await orgcountryPage.ClickCompanylink();   
       await browser.driver.sleep(5000);    
       await homePage.Editiconclick(); 
       await browser.driver.sleep(5000);
       await homePage.saveButtonClick();
        await orgcountryPage.successOk();
       await browser.driver.sleep(5000);
       await companyPage.ClickOrgcountrylink();
       await browser.driver.sleep(5000);    
       await homePage.Editiconclick(); 
       await browser.driver.sleep(5000);
       await homePage.saveButtonClick();
        await browser.driver.sleep(5000);
        let successmsg = await orgcountryPage.fxbutton;
        expect(successmsg.isPresent()).toBeTruthy();
         await orgcountryPage.successOk(); 
         await element(by.className('closeIcon')).click();     
  
     })
     

    afterEach(() => {

    });
});