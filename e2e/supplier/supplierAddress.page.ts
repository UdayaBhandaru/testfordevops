import { browser, element, by, $, WebElement } from 'protractor';
import { BasePage } from '../core/base.page';
import { HomePage } from '../home/home.page';
import { DropDownHelper } from "../core/DropdownHelper";
import { DateFormat } from "../core/dateFormat";

export class SupplierAddressPage extends BasePage {

  
    public SupplierAddressMenu = element(by.css("a[href*='/SupplierAddressMaster']"));
    public addBtn = element(by.id("searchbarAdd"));
    public fxbutton = element(by.className('fx-button'));
    public searchbtn = element(by.className('searchbtn'));
    public listitem = element(by.className('listitem'));
    private dropdownHelper: DropDownHelper;

    async supplierPageSearch(): Promise<any> {
        let submit = await element(by.name('search'));
        return submit.click();
    }

    async supplierPageAdd(): Promise<any> {
        var span1 = element(by.binding('person.name'));
    }  

    async typeSupplierId(supplierId: string) {
      
        let supplierInputs = await element(by.name('Supplier ID'));   
        await supplierInputs.click();    
        this.dropdownHelper = new DropDownHelper();
        let supplierInput = element(by.xpath("//*[contains(text(),'AL ARABIA ELECTRICAL CO(654)')]"));   
        await supplierInput.click();
        await browser.driver.sleep(800);
    }

    async typeAddressType(addressId: string) {
       
        let addressTypeInputs = await element(by.name('Address Type'));   
        await addressTypeInputs.click();    
        this.dropdownHelper = new DropDownHelper();
        let addressTypeInput = element(by.xpath("//*[contains(text(),'Address Type 2')]"));   
        await addressTypeInput.click();
        await browser.driver.sleep(800);
    }

    async typeSequenceNo(sequenceNo: number) {
        let sequenceNoInput = await element(by.name('sequenceNo'));
        await sequenceNoInput.sendKeys(sequenceNo);
        await browser.driver.sleep(800);
    }

    async typeOracleVendorSiteID(oracleVendorSiteID: number) {
        let oracleVendorSiteIDInput = await element(by.name('oracleVendorSiteID'));
        await oracleVendorSiteIDInput.sendKeys(oracleVendorSiteID);
        await browser.driver.sleep(800);
    }

    async typeContactName(contactName: string) {
        let contactNameInput = await element(by.name('contactName'));
        await contactNameInput.sendKeys(contactName);
        await browser.driver.sleep(800);
    }

    async typeContactEmail(contactEmail: string) {
        let contactEmailInput = await element(by.name('contactEmail'));
        await contactEmailInput.sendKeys(contactEmail);
        await browser.driver.sleep(800);
    }

    async typeContactPhone(contactPhone: string) {
        let contactPhoneInput = await element(by.name('contactPhone'));
        await contactPhoneInput.sendKeys(contactPhone);
        await browser.driver.sleep(800);
    }

    async typeContactFax(contactFax: string) {
        let contactFaxInput = await element(by.name('contactFax'));
        await contactFaxInput.sendKeys(contactFax);
        await browser.driver.sleep(800);
    }

    async typeContactTelex(contactTelex: string) {
        let contactTelexInput = await element(by.name('contactTelex'));
        await contactTelexInput.sendKeys(contactTelex);
        await browser.driver.sleep(800);
    }

    async typeAddressLine1(addressLine1: string) {
        let addressLine1Input = await element(by.name('addressLine1'));
        await addressLine1Input.sendKeys(addressLine1);
        await browser.driver.sleep(800);
    }

    async typeAddressLine2(addressLine2: string) {
        let addressLine2Input = await element(by.name('addressLine2'));
        await addressLine2Input.sendKeys(addressLine2);
        await browser.driver.sleep(800);
    }

    async typeAddressLine3(addressLine3: string) {
        let addressLine3Input = await element(by.name('addressLine3'));
        await addressLine3Input.sendKeys(addressLine3);
        await browser.driver.sleep(800);
    }

    async typeCity(city: string) {
        let cityInput = await element(by.name('city'));
        await cityInput.sendKeys(city);
        await browser.driver.sleep(800);
    }

    async typeState(state: string) {
        let stateInput = await element(by.name('state'));
        await stateInput.sendKeys(state);
        await browser.driver.sleep(800);
    }

    async typeCountryID(countryId: string) {
        let countryIDInputs = await element(by.name('Country ID'));   
        await countryIDInputs.click();    
        this.dropdownHelper = new DropDownHelper();
        let countryIDInput = element(by.xpath("//*[contains(text(),'ANGOLA')]"));   
        await countryIDInput.click();
        await browser.driver.sleep(800);
    } 

    async typePostalCode(postalCode: string) {
        let postalCodeInput = await element(by.name('postalCode'));
        await postalCodeInput.sendKeys(postalCode);
        await browser.driver.sleep(800);
    }

    async typeCounty(county: string) {
        let countyInput = await element(by.name('country'));
        await countyInput.sendKeys(county);
        await browser.driver.sleep(800);
    }

    async successOk()
    {
        let ok = await element(by.xpath('//*[contains(text(), "OK")]'));
        await ok.click();
        await browser.driver.sleep(8000);
    }
    async Editiconclick()
    {
        console.log("In Editiconclick");
        let Editicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'Edit')]"));

        return await Editicon.click();       
    }
    async Viewiconclick()
    {
        console.log("In Viewiconclick");
        let Viewicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'View')]"));

        return await Viewicon.click();       
    }
}