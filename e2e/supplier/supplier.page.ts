import { browser, element, by, $, WebElement } from 'protractor';
import { BasePage } from '../core/base.page';
import { HomePage } from '../home/home.page';
import { DropDownHelper } from "../core/DropdownHelper";
import { DateFormat } from "../core/dateFormat";

export class SupplierPage extends BasePage {

   
    public SupplierMenu = element(by.css("a[href*='/SupplierMaster']"));
    public addBtn = element(by.id("searchbarAdd"));
    public fxbutton = element(by.className('fx-button'));
    public searchbtn = element(by.className('searchbtn'));
    public listitem = element(by.className('listitem'));
    private dropdownHelper: DropDownHelper;
 
    async supplierPageSearch(): Promise<any> {
        let submit = await element(by.name('search'));
        return submit.click();
    }

    async supplierPageAdd(): Promise<any> {
        var span1 = element(by.binding('person.name'));
    }
    async Editiconclick()
    {
        console.log("In Editiconclick");
        let Editicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'Edit')]"));

        return await Editicon.click();       
    }
    async Viewiconclick()
    {
        console.log("In Viewiconclick");
        let Viewicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'View')]"));

        return await Viewicon.click();       
    }

    async typeSupplierName(supplierName: string) {
        let supplierNameInput = await element(by.name('supplierName'));
        await supplierNameInput.sendKeys(supplierName);
        await browser.driver.sleep(800);
    } 

    async typeSupplierId(supplierId: number) {
        let supplierIdInput = await element(by.name('supplierId'));
        await supplierIdInput.sendKeys(supplierId);
        await browser.driver.sleep(500);
    }

    async typeLanguage(language: string) {
        let languageInputs = await element(by.name('Language ID'));   
        await languageInputs.click();    
        this.dropdownHelper = new DropDownHelper();
        let languageInput = element(by.xpath("//*[contains(text(),'ENGLISH')]"));   
        await languageInput.click();
        await browser.driver.sleep(800);
    }

    async typeSecondaryName(secondaryName: string) {
        let secondaryNameInput = await element(by.name('supplierSecondaryName'));
        await secondaryNameInput.sendKeys(secondaryName);
        await browser.driver.sleep(800);
    }

    async typeContactName(contactName: string) {
        let contactNameInput = await element(by.name('contactName'));
        await contactNameInput.sendKeys(contactName);
        await browser.driver.sleep(800);
    }

    async typeContactEmail(contactEmail: string) {
        let contactEmailInput = await element(by.name('contactEmail'));
        await contactEmailInput.sendKeys(contactEmail);
        await browser.driver.sleep(800);
    }

    async typeContactPhone(contactPhone: string) {
        let contactPhoneInput = await element(by.name('contactPhone'));
        await contactPhoneInput.sendKeys(contactPhone);
        await browser.driver.sleep(800);
    }

    async typeContactFax(contactFax: string) {
        let contactFaxInput = await element(by.name('contactFax'));
        await contactFaxInput.sendKeys(contactFax);
        await browser.driver.sleep(800);
    }

    async typeContactPager(contactPager: string) {
        let contactPagerInput = await element(by.name('contactPager'));
        await contactPagerInput.sendKeys(contactPager);
        await browser.driver.sleep(800);
    }

    async typeQCPercentage(qcPercentage: number) {
        let qcPercentageInput = await element(by.name('qcPercentage'));
        await qcPercentageInput.sendKeys(qcPercentage);
        await browser.driver.sleep(800);
    }

    async typeQCFrequency(qcFrequency: number) {
        let qcFrequencyInput = await element(by.name('qcFrequency'));
        await qcFrequencyInput.sendKeys(qcFrequency);
        await browser.driver.sleep(800);
    }

    async typeVCPercentage(vcPercentage: number) {
        let vcPercentageInput = await element(by.name('vcPercentage'));
        await vcPercentageInput.sendKeys(vcPercentage);
        await browser.driver.sleep(800);
    }

    async typeVCFrequency(vcFrequency: number) {
        let vcFrequencyInput = await element(by.name('vcFrequency'));
        await vcFrequencyInput.sendKeys(vcFrequency);
        await browser.driver.sleep(800);
    }

    async typeCurrencyCode(currencyCode: string){
        let currencyCodeInputs = await element(by.name('Currency Code'));   
        await currencyCodeInputs.click();    
        this.dropdownHelper = new DropDownHelper();
        let currencyCodeInput = element(by.xpath("//*[contains(text(),'ETHIOPIA BIRR')]"));   
        await currencyCodeInput.click();
        await browser.driver.sleep(800);
    }

    async typeVatRegistrationID(vatRegistrationId: string) {
        let vatRegistrationIdInput = await element(by.name('vatRegistrationId'));
        await vatRegistrationIdInput.sendKeys(vatRegistrationId);
        await browser.driver.sleep(800);
    }

    async typePaymentMethod(paymentMethod: string) {
        let paymentMethodInputs = await element(by.name('Payment Method'));   
        await paymentMethodInputs.click();    
        this.dropdownHelper = new DropDownHelper();
        let paymentMethodInput = element(by.xpath("//*[contains(text(),'Domestic Check')]"));   
        await paymentMethodInput.click();
        await browser.driver.sleep(800);
    }

    async typeRebateDiscount(rebateDiscount: number) {
        let rebateDiscountInput = await element(by.name('rebateDiscount'));
        await rebateDiscountInput.sendKeys(rebateDiscount);
        await browser.driver.sleep(800);
    }

    async typeTermsId(TermsId: string) {
        let termsIdInputs = await element(by.name('Terms Id'));   
        await termsIdInputs.click();    
        this.dropdownHelper = new DropDownHelper();
        let termsIdInput = element(by.xpath("//*[contains(text(),'TERMS 120 DAYS')]"));   
        await termsIdInput.click();
        await browser.driver.sleep(800);
    }

    async typeFreightTerms(freightTerms: string) {
       
        let freightTermsInputs = await element(by.name('Freight Terms'));   
        await freightTermsInputs.click();    
        this.dropdownHelper = new DropDownHelper();
        let freightTermsInput = element(by.xpath("//*[contains(text(),'AIR')]"));   
        await freightTermsInput.click();
        await browser.driver.sleep(800);
    }

    async typeShipMethod(freightTermsms: string) {
        let shipMethodInputs = await element(by.name('Ship Method'));   
        await shipMethodInputs.click();    
        this.dropdownHelper = new DropDownHelper();
        let shipMethodInput = element(by.xpath("//*[contains(text(),'AIR FREIGHT')]"));   
        await shipMethodInput.click();
        await browser.driver.sleep(800);
    }

    async typeDeliveryPolicy() {
        let deliveryPolicyInputs = await element(by.name('Delivery Policy'));   
        await deliveryPolicyInputs.click();    
        this.dropdownHelper = new DropDownHelper();
        let deliveryPolicyInput = element(by.xpath("//*[contains(text(),'option 1 Added in DB Table')]"));   
        await deliveryPolicyInput.click();
        await browser.driver.sleep(800);
    }

    async typeRetMinDollarAmount(retMinimumDolAmount: number) {
        let retMinimumDolAmountInput = await element(by.name('retMinimumDolAmount'));
        await retMinimumDolAmountInput.sendKeys(retMinimumDolAmount);
        await browser.driver.sleep(800);
    }

    async typeRetailCourier(retailCourier: string) {
        let retailCourierInput = await element(by.name('retailCourier'));
        await retailCourierInput.sendKeys(retailCourier);
        await browser.driver.sleep(800);
    }

    async typeHandlingPercentage(handlingPercentage: number) {
        let handlingPercentageInput = await element(by.name('handlingPercentage'));
        await handlingPercentageInput.sendKeys(handlingPercentage);
        await browser.driver.sleep(800);
    }
    async successOk()
    {
        let ok = await element(by.xpath('//*[contains(text(), "OK")]'));
        await ok.click();
        await browser.driver.sleep(8000);
    }
}