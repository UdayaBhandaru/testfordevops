import { browser, ExpectedConditions as EC, $, $$, element, by } from 'protractor'
import { HomePage } from '../home/home.page'
import { LoginPage } from '../login/login.page'
import { SupplierAddressPage } from "./supplierAddress.page";
import { protractor } from 'protractor/built/ptor';
declare let expect: any


describe('supplier address search', function () {
    let homePage: HomePage;
    let loginpage: LoginPage;
    let supplierAddressPage: SupplierAddressPage;
    beforeEach(async () => {

    })  

    it('should open the supplier Address', async (): Promise<any> => {
   
        loginpage = new LoginPage();
        await loginpage.open();      
        await loginpage.login();  
        homePage = new HomePage();
        supplierAddressPage = new SupplierAddressPage();
        browser.waitForAngularEnabled(false);
        await browser.driver.sleep(5000);
        let Supplier  = await homePage.Supplier; 
        expect(Supplier.isPresent()).toBeTruthy();
        await Supplier.click(); 
        await browser.driver.sleep(500); 
        await supplierAddressPage.SupplierAddressMenu.click();   
        expect(supplierAddressPage.searchbtn.isPresent()).toBeTruthy();
        await homePage.ClickSearchButton();
        await browser.driver.sleep(5000);
    })

    it('should Search the supplier', async (): Promise<any> => {
     
        await supplierAddressPage.Viewiconclick();
        await browser.driver.sleep(5000); 
        await element(by.className('closeIcon')).click();
        await supplierAddressPage.Editiconclick();
        await homePage.saveButtonClick();
        let successmsg = await supplierAddressPage.fxbutton;
        expect(successmsg.isPresent()).toBeTruthy();
        await  supplierAddressPage.successOk();
        await element(by.className('toolbar-icons searchIcon')).click();
        await browser.driver.sleep(1000);
     })

    it('should add the supplier Address', async (): Promise<any> => {

        expect(supplierAddressPage.addBtn.isPresent()).toBeTruthy();
        await homePage.ClickAddButton();
        await browser.driver.sleep(5000);
        await supplierAddressPage.typeSupplierId("AL ARABIA ELECTRICAL CO(654)");
        await supplierAddressPage.typeAddressType("Address Type 3");
        await supplierAddressPage.typeSequenceNo(12355);
        await supplierAddressPage.typeOracleVendorSiteID(12345);
        await supplierAddressPage.typeContactTelex("9878986799");
        await supplierAddressPage.typeAddressLine1("ADDRESS 1");
        await supplierAddressPage.typeAddressLine2("ADDRESS 3");
        await supplierAddressPage.typeAddressLine3("ADDRESS 2");
        await supplierAddressPage.typeCity("TESTCITY");
        await supplierAddressPage.typeState("TESTSTATE");
        await supplierAddressPage.typeCountryID("ANGOLA");
        await supplierAddressPage.typePostalCode("345345345");
        await supplierAddressPage.typeCounty("COUNTY");
        await homePage.saveButtonClick();
        await  supplierAddressPage.successOk();
        let successmsg = await supplierAddressPage.fxbutton;
        expect(successmsg.isPresent()).toBeTruthy();
        await browser.driver.sleep(5000);
    })


    afterEach(() => {

    });
});