import { browser, ExpectedConditions as EC, $, $$, element, by } from 'protractor'
import { HomePage } from '../home/home.page'
import { LoginPage } from '../login/login.page'
import { SupplierPage } from "./supplier.page";
import { protractor } from 'protractor/built/ptor';
declare let expect: any


describe('supplier search', function () {
    let homePage: HomePage;
    let loginpage: LoginPage;
    let supplierPage: SupplierPage;
    beforeEach(async () => {

    })
 

    it('should open the supplier', async (): Promise<any> => {
    loginpage = new LoginPage();
        await loginpage.open();      
        await loginpage.login();  
        homePage = new HomePage();
        supplierPage = new SupplierPage();
        browser.waitForAngularEnabled(false);
        await browser.driver.sleep(5000);
        let Supplier  = await homePage.Supplier; 
        expect(Supplier.isPresent()).toBeTruthy();
        await Supplier.click(); 
        await browser.driver.sleep(500); 
        await supplierPage.SupplierMenu.click();   
        await browser.driver.sleep(5000);
        expect(supplierPage.searchbtn.isPresent()).toBeTruthy();
        await homePage.ClickSearchButton();
        await browser.driver.sleep(5000);
    })

     it('should Search the supplier', async (): Promise<any> => {
       
        await supplierPage.Viewiconclick();
        await browser.driver.sleep(5000); 
        await element(by.className('closeIcon')).click();
        await supplierPage.Editiconclick();
        await homePage.saveButtonClick();
        let successmsg = await supplierPage.fxbutton;
        expect(successmsg.isPresent()).toBeTruthy();
        await  supplierPage.successOk();
        await element(by.className('toolbar-icons searchIcon')).click();
        
     })
  
    it('should add the supplier', async (): Promise<any> => {
        await browser.driver.sleep(5000);
        expect(supplierPage.addBtn.isPresent()).toBeTruthy();
        await homePage.ClickAddButton();
        await browser.driver.sleep(100);
        await supplierPage.typeSupplierId(3290);
        await supplierPage.typeSupplierName("BEETEL");
        await supplierPage.typeSecondaryName("TELCO ");
        await supplierPage.typeLanguage("ENGLISH");
        await supplierPage.typeContactName("BEETEL");
        await supplierPage.typeContactEmail("beetel@test.co.in");
        await supplierPage.typeContactPhone("9878985987");
        await supplierPage.typeContactFax("230987898789");
        await supplierPage.typeContactPager("9878957898");
        await browser.driver.sleep(5000);
        await supplierPage.typeCurrencyCode("ETHIOPIA BIRR");
        await supplierPage.typeVatRegistrationID("ABC01234556897");
        await supplierPage.typePaymentMethod("Domestic Check");
        await supplierPage.typeRebateDiscount(10.25);
        await supplierPage.typeQCPercentage(12);
        await supplierPage.typeQCFrequency(14);
        await supplierPage.typeVCPercentage(16);
        await supplierPage.typeVCFrequency(18.56);
        await browser.driver.sleep(5000);
        await supplierPage.typeTermsId("TERMS 120 DAYS");
        await supplierPage.typeFreightTerms("AIR");
        await supplierPage.typeShipMethod("AIR FREIGHT");
        await supplierPage.typeDeliveryPolicy();
        await supplierPage.typeRetMinDollarAmount(23);
        await supplierPage.typeRetailCourier("COURIER");
        await supplierPage.typeHandlingPercentage(30);
        await homePage.saveButtonClick();
        let successmsg = await supplierPage.fxbutton;
        expect(successmsg.isPresent()).toBeTruthy();
        await browser.driver.sleep(5000);
        await supplierPage.successOk();
    })


    afterEach(() => {

    });
});