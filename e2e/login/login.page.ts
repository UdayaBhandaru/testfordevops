import { browser, element, by, $ } from 'protractor'
import { BasePage } from '../core/base.page'
import { HomePage } from '../home/home.page'

export class LoginPage extends BasePage {
    async typeUsername(un: string) {

        let usernameInput = await element(by.name('email'));
        await usernameInput.sendKeys(un);
    }

    async typePassword(password: string) {
        let passwordInput = await element(by.name('password'));
        await passwordInput.sendKeys(password);
    }

    async login(): Promise<any> {
        let submit = await element(by.name('loginbtn'));
        await this.typeUsername("raleti@agility.com");
        await this.typePassword("Agility@123");
        return await submit.click();
    }

    async logout(): Promise<any> {
        let logout = await element(by.className('logout'));      
        return await logout.click();
    }

}