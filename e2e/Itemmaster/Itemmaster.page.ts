import { browser, element, by, $, WebElement } from 'protractor';
import { BasePage } from '../core/base.page';
import { HomePage } from '../home/home.page';
import { DropDownHelper } from "../core/DropdownHelper";

export class ItemmasterPage extends BasePage 
{
   public ItemmasterMenu = element(by.css("a[href*=' Item Master']"));
   public addButton = element(by.id("searchbarAdd"));
    public searchButton = element(by.id('searchbarSearch'));
    public resetButton = element(by.id ('searchbarReset'));
    public fxbutton = element(by.className('fx-button'));
    public searchbtn = element(by.className('searchbtn'));
    public listitem = element(by.className('listitem'));
    public addRow= element(by.className("AddRow"));

    private dropdownHelper: DropDownHelper;
    async ItemmasterPageSearch(): Promise<any>
    {
        let submit = await element(by.id('searchbarSearch'));
        return submit.click();
    }

    async  ItemmasterPageAdd(): Promise<any>
    {
        var span1 = element(by.binding('person.name'));
    }
    

    async typeItemtype(Itemtype: string)  
    {
        let ItemtypeInput = await element(by.name('Type'));   
        await ItemtypeInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let ItemmasterInput = element(by.xpath("//*[contains(text(),'REGULAR')]"));   
        await ItemmasterInput.click();
        await browser.driver.sleep(800);
    }
    async typelongdescription(longdescription: string)
    {
        let longdescriptionInput = await element(by.name('descLong'));
        await longdescriptionInput.sendKeys(longdescription);
        await browser.driver.sleep(500);
    }
 
    async typeDivision(Division: string)  
    {
        let DivisionInput = await element(by.name('Division'));   
        await DivisionInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let ItemmasterInput = element(by.xpath("//*[contains(text(),'HOMECENTER(3)')]"));   
        await ItemmasterInput.click();
        await browser.driver.sleep(500);
    }
    async typeDepartment(Department: string)  
    {
        let DepartmentInput = await element(by.name('Department'));   
        await DepartmentInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let ItemmasterInput = element(by.xpath("//*[contains(text(),'ELECTRONICS(14)')]"));   
        await ItemmasterInput.click();
        await browser.driver.sleep(500);
    }
    async typeCategory(Category: string)  
    {
        let CategoryInput = await element(by.name('Category'));   
        await CategoryInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let ItemmasterInput = element(by.xpath("//*[contains(text(),'COMPUTER ACCESSORIES(185)')]"));   
        await ItemmasterInput.click();
        await browser.driver.sleep(500);
    }
    async typeClass(Class: string)  
    {
        let ClassInput = await element(by.name('Class'));   
        await ClassInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let ItemmasterInput = element(by.xpath("//*[contains(text(),'COMMUNICATION(8)')]"));   
        await ItemmasterInput.click();
        await browser.driver.sleep(500);
    }
   
    async typeSubclass(Subclass: string)  
    {
        let SubclassInput = await element(by.name('Sub Class'));   
        await SubclassInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let ItemmasterInput = element(by.xpath("//*[contains(text(),'BLUETOOTH(3)')]"));   
        await ItemmasterInput.click();
        await browser.driver.sleep(500);
    }
    async typesellable()
    {
        let sellableInput = await element(by.xpath("//*[contains(@controlname ,'sellable')]//following-sibling::mat-radio-button[1]"));
        await sellableInput.click();
        await browser.driver.sleep(500);
    }

    async typeorderable()
    {
        let orderableInput = await element(by.xpath("//*[contains(@controlname ,'orderable')]//following-sibling::mat-radio-button[1]")); 
        await orderableInput.click();
        await browser.driver.sleep(500);
    }

    // async typeInventory(Inventory: string)
    // {
    //     let InventoryInput = await element(by.id("mat-radio-32-input"));
    //     await InventoryInput.sendKeys(Inventory);
    //     await browser.driver.sleep(500);
    // }
   
    async typeItemstatus()
    {
        let ItemstatusInput = await element(by.xpath("//*[contains(@controlname ,'itemStatus')]//following-sibling::mat-radio-button[1]")); 
        await ItemstatusInput.click();
        await browser.driver.sleep(500);
    }
    

    async typeBrand(Brand: string)  
    {
        let BrandInput = await element(by.name('Brand'));   
        await BrandInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let ItemmasterInput = element(by.xpath("//*[contains(text(),'SAMSUNG')]"));   
        await ItemmasterInput.click();
        await browser.driver.sleep(800);
    }
    
    async typeUOMUnit(UOMUnit: string)
    {
        let UOMUnitInput = await element(by.name('uomUnit'));      
        await UOMUnitInput.sendKeys(UOMUnit);
        await browser.driver.sleep(500);
    }
     
    async typeUOM(UOM: string)  
    {
        let UOMInput = await element(by.name('UOM'));   
        await UOMInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let ItemmasterInput = element(by.xpath("//*[contains(text(),'EACH(EA)')]"));   
        await ItemmasterInput.click();
        await browser.driver.sleep(800);
    }

    async typePackingdescription(Packingdescription: string)
    {
        let PackingdescriptionInput = await element(by.name('packingDesc'));      
        await PackingdescriptionInput.sendKeys(Packingdescription);
        await browser.driver.sleep(500);
    }

    

    async typeSecondarydescription(Secondarydescription: string)
    {
        let SecondarydescriptionInput = await element(by.name('secondaryDesc'));      
        await SecondarydescriptionInput.sendKeys(Secondarydescription);
        await browser.driver.sleep(500);
    }
    async typeShortdescription(Shortdescription: string)
    {
        let ShortdescriptionInput = await element(by.name('shortDesc'));      
        await ShortdescriptionInput.sendKeys(Shortdescription);
        await browser.driver.sleep(500);
    }
    async typeOnlinedescription(Onlinedescription: string)
    {
        let OnlinedescriptionInput = await element(by.name('onlineDesc'));      
        await OnlinedescriptionInput.sendKeys(Onlinedescription);
        await browser.driver.sleep(500);
    }
    async typeSupplierdescription(Supplierdescription: string)
    {
        let SupplierdescriptionInput = await element(by.name('suppDesc'));      
        await SupplierdescriptionInput.sendKeys(Supplierdescription);
        await browser.driver.sleep(500);
    }
    
    async typeInventorymaintained(Inventorymaintained: string)  
    {
        let InventorymaintainedInput = await element(by.name('Inventory Maintained At'));   
        await InventorymaintainedInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let ItemmasterInput = element(by.xpath("//*[contains(text(),'LEVEL 3')]"));   
        await ItemmasterInput.click();
        await browser.driver.sleep(800);
    }
    async typeTransactionmaintained(Transactionmaintained: string)  
    {
        let TransactionmaintainedInput = await element(by.name('Transaction Maintained At'));   
        await TransactionmaintainedInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let ItemmasterInput = element(by.xpath("//*[contains(text(),'LEVEL 3')]"));   
        await ItemmasterInput.click();
        await browser.driver.sleep(800);
    }
    
    async typeStandardUOM(StandardUOM: string)  
    {
        let StandardUOMInput = await element(by.name('Standard UOM'));   
        await StandardUOMInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let ItemmasterInput = element(by.xpath("//*[contains(text(),'EACH(EA)')]"));   
        await ItemmasterInput.click();
        await browser.driver.sleep(800);
    }
    async typecatchweightindicator()
    {
        let catchweightindicatorInput = await element(by.xpath("//*[contains(@controlname ,'catchWeightInd')]//following-sibling::mat-radio-button[2]"));
        await catchweightindicatorInput.click();
        await browser.driver.sleep(500);
    }
    
    async typeItemtransformationallowed()
    {
        let ItemtransformationallowedInput = await element(by.xpath("//*[contains(@controlname ,'itemXformInd')]//following-sibling::mat-radio-button[1]"));
        await ItemtransformationallowedInput.click();
        await browser.driver.sleep(500);
    }
    async typePerishableindicator()
    {
        let PerishableindicatorInput = await element(by.xpath("//*[contains(@controlname ,'perishableInd')]//following-sibling::mat-radio-button[1]"));
        await PerishableindicatorInput.click();
        await browser.driver.sleep(500);
    }
    

    async typeComments(Comments: string)
    {
        let CommentsInput = await element(by.name('comments'));      
        await CommentsInput.sendKeys(Comments);
        await browser.driver.sleep(500);
    }
    async typeItemId(ItemId: string)
    {
        let ItemIdInput = await element(by.name('item'));      
        await ItemIdInput.sendKeys(ItemId);
        await browser.driver.sleep(500);
    }

    async Editiconclick()
    {
        console.log("In Editiconclick");
        let Editicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'Edit')]"));

        return await Editicon.click();       
    }
    async Viewiconclick()
    {
        console.log("In Viewiconclick");
        let Viewicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'View')]"));

        return await Viewicon.click();       
    }
    
    async typeCompany(Company: string)  
    {
        let CompanyInput = await element(by.name('Company'));   
        await CompanyInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let ItemmasterInput = element(by.xpath("//*[contains(text(),'TSC KUWAIT')]"));   
        await ItemmasterInput.click();
        await browser.driver.sleep(800);
    }


    async typeDefaultlocation(Defaultlocation: string)  
    {
        let DefaultlocationInput = await element(by.name('Default Location'));   
        await DefaultlocationInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let ItemmasterInput = element(by.xpath("//*[contains(text(),'SHUWAIKH(201)')]"));   
        await ItemmasterInput.click();
        await browser.driver.sleep(800);
    }

    async typeCountry(Country: string)  
    {
        let CountryInput = await element(by.name('Country'));   
        await CountryInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let ItemmasterInput = element(by.xpath("//*[contains(text(),'KUWAIT')]"));   
        await ItemmasterInput.click();
        await browser.driver.sleep(800);
    }
    async typeRegion(Region: string)  
    {
        let RegionInput = await element(by.name('Region'));   
        await RegionInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let ItemmasterInput = element(by.xpath("//*[contains(text(),'KUWAIT')]"));   
        await ItemmasterInput.click();
        await browser.driver.sleep(800);
    }
    async typeFormat(Format: string)  
    {
        let FormatInput = await element(by.name('Format'));   
        await FormatInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let ItemmasterInput = element(by.xpath("//*[contains(text(),'WHOLESALE STORES(WST)')]"));   
        await ItemmasterInput.click();
        await browser.driver.sleep(800);
    }
    async typeDefaultWarehouse(Warehouse: string)  
    {
        let DefaultWarehouseInput = await element(by.name('Default Warehouse'));   
        await DefaultWarehouseInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let ItemmasterInput = element(by.xpath("//*[contains(text(),'SHUWAIKH(201)')]"));   
        await ItemmasterInput.click();
        await browser.driver.sleep(800);
    }
    async typeDefaultImportcountry(DefaultImportcountry: string)  
    {
        let DefaultImportcountryInput = await element(by.name('Default Import Country'));   
        await DefaultImportcountryInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let ItemmasterInput = element(by.xpath("//*[contains(text(),'UNITED ARAB EMIRATES(ARE)')]"));   
        await ItemmasterInput.click();
        await browser.driver.sleep(800);
    }
    async typeCountryoforigin(Countryoforigin: string)  
    {
        let CountryoforiginInput = await element(by.name('Country Of Origin'));   
        await CountryoforiginInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let ItemmasterInput = element(by.xpath("//*[contains(text(),'CHINA(CHN)')]"));   
        await ItemmasterInput.click();
        await browser.driver.sleep(800);
    }
    async typePrimarysupplier(Primarysupplier: string)  
    {
        let PrimarysupplierInput = await element(by.name('Primary Supplier'));   
        await PrimarysupplierInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let ItemmasterInput = element(by.xpath("//*[contains(text(),'AL-BASTAIN(342)')]"));   
        await ItemmasterInput.click();
        await browser.driver.sleep(800);
    }
    async typeDefaultsourcemethod(Defaultsourcemethod: string)  
    {
        let DefaultsourcemethodInput = await element(by.name('Default Source Method'));   
        await DefaultsourcemethodInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let ItemmasterInput = element(by.xpath("//*[contains(text(),'LOCAL')]"));   
        await ItemmasterInput.click();
        await browser.driver.sleep(800);
    }
    async typeItemsource(Itemsource: string)  
    {
        let ItemsourceInput = await element(by.name('Item Source'));   
        await ItemsourceInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let ItemmasterInput = element(by.xpath("//*[contains(text(),'LOCAL')]"));   
        await ItemmasterInput.click();
        await browser.driver.sleep(800);
    }
    async typePricezone(Pricezone: string)  
    {
        let PricezoneInput = await element(by.name('Default Price Zone'));   
        await PricezoneInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let ItemmasterInput = element(by.xpath("//*[contains(text(),'KWT TEST')]"));   
        await ItemmasterInput.click();
        await browser.driver.sleep(800);
    }
    
    async typeVatonretail()
    {
        let VatonretailInput = await element(by.xpath("//*[contains(@controlname ,'vatOnRetail')]//following-sibling::mat-radio-button[2]"));
        await VatonretailInput.click();
        await browser.driver.sleep(500);
    }
    async typeBaseretailprice(Baseretailprice: string)
    {
        let BaseretailpriceInput = await element(by.name('baseRetailPrice'));      
        await BaseretailpriceInput.sendKeys(Baseretailprice);
        await browser.driver.sleep(500);
    }
    async typeBasecostprice(Basecostprice: string)
    {
        let BasecostpriceInput = await element(by.name('baseCostPrice'));      
        await BasecostpriceInput.sendKeys(Basecostprice);
        await browser.driver.sleep(500);
    }
    async typeDiscountallowed()
    {
        let DiscountallowedInput = await element(by.xpath("//*[contains(@controlname ,'discountAllowed')]//following-sibling::mat-radio-button[1]"));
        await DiscountallowedInput.click();
        await browser.driver.sleep(500);
    }
    async typeMarkdownallowed()
    {
        let MarkdownallowedInput = await element(by.xpath("//*[contains(@controlname ,'markDownAllowed')]//following-sibling::mat-radio-button[1]"));
        await MarkdownallowedInput.click();
        await browser.driver.sleep(500);
    }
    async typePromotionallowed()
    {
        let PromotionallowedInput = await element(by.xpath("//*[contains(@controlname ,'promotionAllowed')]//following-sibling::mat-radio-button[1]"));
        await PromotionallowedInput.click();
        await browser.driver.sleep(500);
    }
    async typeInstoreproduction()
    {
        let InstoreproductionInput = await element(by.xpath("//*[contains(@controlname ,'inStoreProduction')]//following-sibling::mat-radio-button[2]"));
        await InstoreproductionInput.click();
        await browser.driver.sleep(500);
    }
    async typeDefaultautoRepl()
    {
        let DefaultautoReplInput = await element(by.xpath("//*[contains(@controlname ,'deaultAutoRepl')]//following-sibling::mat-radio-button[2]"));
        await DefaultautoReplInput.click();
        await browser.driver.sleep(500);
    }
    async typeForecastallowed()
    {
        let ForecastallowedInput = await element(by.xpath("//*[contains(@controlname ,'forecastAllowed')]//following-sibling::mat-radio-button[2]"));
        await ForecastallowedInput.click();
        await browser.driver.sleep(500);
    }
    async typeGiftwrapindicator()
    {
        let GiftwrapindicatorInput = await element(by.xpath("//*[contains(@controlname ,'giftWrapInd')]//following-sibling::mat-radio-button[1]"));
        await GiftwrapindicatorInput.click();
        await browser.driver.sleep(500);
    }

    async typeshipaloneallowed()
    {
        let shipaloneallowedInput = await element(by.xpath("//*[contains(@controlname ,'shipAlone')]//following-sibling::mat-radio-button[1]"));
        await shipaloneallowedInput.click();
        await browser.driver.sleep(500);
    }
    async typeExpiryitem()
    {
        let ExpiryitemInput = await element(by.xpath("//*[contains(@controlname ,'expiryFlag')]//following-sibling::mat-radio-button[2]"));
        await ExpiryitemInput.click();
        await browser.driver.sleep(500);
    }
    
    async typeBreakpack()
    {
        let BreakpackInput = await element(by.xpath("//*[contains(@controlname ,'breakPackInd')]//following-sibling::mat-radio-button[2]"));
        await BreakpackInput.click();
        await browser.driver.sleep(500);
    }
    async ClickAddtogridButton(): Promise<any> 
    {
        console.log("In AddtogridButtonClick");
        let AddtogridButton = element(by.className("AddRow"));
        return await AddtogridButton.click();
    }
    
    async typeSupplier(Supplier: string)  
    {
        let SupplierInput = await element(by.name('Supplier'));   
        await SupplierInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let ItemmasterInput = element(by.xpath("//*[contains(text(),'AL-BASTAIN(342)')]"));   
        await ItemmasterInput.click();
        await browser.driver.sleep(800);
    }

    async typeVPN(VPN: string)
    {
        let VPNInput = await element(by.name('vpn'));      
        await VPNInput.sendKeys(4321);
        await browser.driver.sleep(500);
    }

    async typePrimarySupplier()
    {
        let PrimarySupplierInput = await element(by.xpath("//*[contains(@controlname ,'primarySuppInd')]//following-sibling::mat-radio-button[1]"));
        await PrimarySupplierInput.click();
        await browser.driver.sleep(500);
    }

    async typeSupplierlabel(Comments: string)
    {
        let SupplierlabelInput = await element(by.name('supplabel'));      
        await SupplierlabelInput.sendKeys();
        await browser.driver.sleep(500);
    }

    async successOk()
    {
        let ok = await element(by.xpath('//*[contains(text(), "OK")]'));
        await ok.click();
        await browser.driver.sleep(8000);
    }
   
    

}