import { browser, ExpectedConditions as EC, $, $$, element, by } from 'protractor'
import { HomePage } from '../home/home.page'
import { LoginPage } from '../login/login.page'
import { ItemmasterPage } from "./Itemmaster.page";
import { protractor } from 'protractor/built/ptor';
declare let expect: any


describe('Itemmaster search', function () {
    let homePage: HomePage;
    let loginpage: LoginPage;
    let itemmasterPage: ItemmasterPage;
    beforeEach(async () => {
       
       
    })   

    it('should open the Itemmaster page', async (): Promise<any> => 
    {
        loginpage = new LoginPage();
        await loginpage.open();      
        await loginpage.login();  
        homePage = new HomePage();
        browser.waitForAngularEnabled(false);
        await browser.driver.sleep(800);
        await homePage.ItemmastermenuClick();
        let Itemmaster  = await homePage.Itemmaster; 
        expect(Itemmaster.isPresent()).toBeTruthy();
        await Itemmaster.click();        
        await browser.driver.sleep(800);
        await homePage.ClickAddButton();
        await browser.driver.sleep(500);
       expect(itemmasterPage.resetButton.isPresent()).toBeTruthy();
        await homePage.ClickresetButton();



}) 

        
        it('should add the Itemmaster', async (): Promise<any> =>      
        {
        homePage = new HomePage();
       itemmasterPage = new ItemmasterPage();
       await browser.driver.sleep(10);
       await itemmasterPage.typeItemtype("REGULAR");
       await browser.driver.sleep(1000);
       await itemmasterPage.typelongdescription("MOBILE BLUETOOTH");
       await browser.driver.sleep(1000);
       await itemmasterPage.typeDivision("HOMECENTER(3)");
       await browser.driver.sleep(1000);
       await itemmasterPage.typeDepartment("ELECTRONICS(14)");
       await browser.driver.sleep(1000);
       await itemmasterPage.typeCategory("COMPUTER ACCESSORIES(185)");
       await browser.driver.sleep(1000);
       await itemmasterPage.typeClass("COMMUNICATION(8)");
       await browser.driver.sleep(1000);
       await itemmasterPage.typeSubclass("BLUETOOTH(3)");
       await browser.driver.sleep(1000);
       await itemmasterPage.typesellable();
       await browser.driver.sleep(1000);
       await itemmasterPage.typeorderable();
       //await itemmasterPage.typeInventory("YES");
       await itemmasterPage.typeItemstatus();
       await browser.driver.sleep(1000);
       expect(itemmasterPage.addButton.isPresent()).toBeTruthy();
       await homePage.ClickAddButton();
       await browser.driver.sleep(1000);
      
     })
 
     it('should add the Itemmaster Details', async (): Promise<any> =>      
     {
     homePage = new HomePage();
    itemmasterPage = new ItemmasterPage();
    await browser.driver.sleep(100);
    await itemmasterPage.typeBrand("SAMSUNG");
    await browser.driver.sleep(1000);
    await itemmasterPage.typeUOMUnit("1");
    await browser.driver.sleep(1000);
    await itemmasterPage.typeUOM("EACH(EA)");
    await browser.driver.sleep(1000);
    await itemmasterPage.typePackingdescription("1 PACK");
    await browser.driver.sleep(1000);
    await itemmasterPage.typeSecondarydescription("BLUETOOTH");
    await browser.driver.sleep(1000);
    await itemmasterPage.typeShortdescription("BLUETOOTH");
    await browser.driver.sleep(1000);
    await itemmasterPage.typeOnlinedescription("BLUETOOTH");
    await browser.driver.sleep(1000);
    await itemmasterPage.typeSupplierdescription("MOBILE BLUETOOTH");
    await browser.driver.sleep(1000);
    await itemmasterPage.typeInventorymaintained("LEVEL 3");
    await browser.driver.sleep(1000);
    await itemmasterPage.typeTransactionmaintained("LEVEL 3");
    await browser.driver.sleep(1000);
    await itemmasterPage.typeStandardUOM("EACH(EA)");
    await browser.driver.sleep(1000);
    await itemmasterPage.typecatchweightindicator();
    await browser.driver.sleep(1000);
    await itemmasterPage.typeshipaloneallowed();
    await browser.driver.sleep(1000);
    await itemmasterPage.typeItemtransformationallowed();
    await browser.driver.sleep(1000);
    await itemmasterPage.typePerishableindicator();
    await browser.driver.sleep(1000);
    await itemmasterPage.typeGiftwrapindicator();
    await browser.driver.sleep(1000);
    await homePage.saveButtonClick();
    await browser.driver.sleep(5000);
    let successmsg = await itemmasterPage.fxbutton;
    expect(successmsg.isPresent()).toBeTruthy();
    await itemmasterPage.successOk();
    await browser.driver.sleep(5000);
    await element(by.className('toolbar-icons nextIcon')).click();

   
  })


    
     
       it('should Add  Itemcompany', async (): Promise<any> =>      
        {
        homePage = new HomePage();
       itemmasterPage = new ItemmasterPage();
       await browser.driver.sleep(1000);
       await itemmasterPage.typeCompany("TSC KUWAIT(1)")
       await itemmasterPage.typeDefaultlocation("SHUWAIKH(201)");
       expect(itemmasterPage.addRow.isPresent()).toBeTruthy();
       await itemmasterPage.ClickAddtogridButton();
       await browser.driver.sleep(5000);
       await homePage.saveButtonClick();
       await browser.driver.sleep(5000);
       let successmsg = await itemmasterPage.fxbutton;
       expect(successmsg.isPresent()).toBeTruthy();
       await itemmasterPage.successOk();
       await browser.driver.sleep(5000);
       await element(by.className('toolbar-icons nextIcon')).click(); 
       await browser.driver.sleep(5000); 
       })
     
       it('should Add  ItemCountry', async (): Promise<any> =>      
       {
       homePage = new HomePage();
      itemmasterPage = new ItemmasterPage();
      await browser.driver.sleep(1000);
      await itemmasterPage.typeCompany("TSC KUWAIT");
      await itemmasterPage.typeCountry("KUWAIT(KWT)");
      await itemmasterPage.typeDefaultlocation("SHUWAIKH(201)");
      await itemmasterPage.typeDefaultWarehouse("SHUWAIKH(201)");
      await itemmasterPage.typeCountryoforigin("CHINA(CHN)");
      await itemmasterPage.typePricezone("KWT TEST)");
      await itemmasterPage.typeBaseretailprice("200");
      await itemmasterPage.typeBasecostprice("190");
      await itemmasterPage.typeVatonretail();
      await itemmasterPage.typeDiscountallowed();
      await itemmasterPage.typeMarkdownallowed();
      await itemmasterPage.typePromotionallowed();
      await itemmasterPage.typeInstoreproduction();
      await itemmasterPage.typeDefaultautoRepl();
      await itemmasterPage.typeForecastallowed();
      await itemmasterPage.typeGiftwrapindicator();
      await itemmasterPage.typeshipaloneallowed();
      await itemmasterPage.typeExpiryitem();
      await itemmasterPage.typeBreakpack();
      await itemmasterPage.typeComments("MOBILE BLUETOOTH");
      await browser.driver.sleep(5000);
      await itemmasterPage.ClickAddtogridButton();
      await browser.driver.sleep(5000);
      await homePage.saveButtonClick();
      await browser.driver.sleep(5000);
      let successmsg = await itemmasterPage.fxbutton;
      expect(successmsg.isPresent()).toBeTruthy();
      await itemmasterPage.successOk();
      await browser.driver.sleep(5000);
      await element(by.className('toolbar-icons nextIcon')).click(); 
      await browser.driver.sleep(5000); 
      })
     

      it('should Add  ItemRegion', async (): Promise<any> =>      
       {
       homePage = new HomePage();
      itemmasterPage = new ItemmasterPage();
      await browser.driver.sleep(1000);
      await itemmasterPage.typeCompany("TSC KUWAIT");
      await itemmasterPage.typeCountry("KUWAIT");
      await itemmasterPage.typeRegion("KUWAIT(KWT)");
      await browser.driver.sleep(500);
      await itemmasterPage.typeDefaultImportcountry("UNITED ARAB EMIRATES(ARE)");
      await browser.driver.sleep(500);
      await itemmasterPage.typePrimarysupplier("AL-BASTAIN(342))");
      await browser.driver.sleep(500);
      await itemmasterPage.typeDefaultsourcemethod("LOCAL");
      await browser.driver.sleep(500);
      await itemmasterPage.typeItemsource("LOCAL");
      await browser.driver.sleep(500);
      expect(itemmasterPage.addRow.isPresent()).toBeTruthy();
      await itemmasterPage.ClickAddtogridButton();
      await browser.driver.sleep(5000);
      await homePage.saveButtonClick();
      await browser.driver.sleep(5000);
      let successmsg = await itemmasterPage.fxbutton;
      expect(successmsg.isPresent()).toBeTruthy();
      await itemmasterPage.successOk();
      await browser.driver.sleep(5000);
      await element(by.className('toolbar-icons nextIcon')).click(); 
      await browser.driver.sleep(5000); 
      })
      
     it('should Add  ItemFormat', async (): Promise<any> =>      
      {
       homePage = new HomePage();
      itemmasterPage = new ItemmasterPage();
      await browser.driver.sleep(1000);
      await itemmasterPage.typeCompany("TSC KUWAIT");
      await itemmasterPage.typeCountry("KUWAIT(KWT)");
      await itemmasterPage.typeRegion("KUWAIT");
      await itemmasterPage.typeFormat("WHOLESALE STORES(WST)");
      await browser.driver.sleep(5000);
      expect(itemmasterPage.addRow.isPresent()).toBeTruthy();
      await itemmasterPage.ClickAddtogridButton();
      await browser.driver.sleep(5000);
      await homePage.saveButtonClick();
      let successmsg = await itemmasterPage.fxbutton;
      expect(successmsg.isPresent()).toBeTruthy();
      await itemmasterPage.successOk();
     await browser.driver.sleep(5000);
      await element(by.className('toolbar-icons nextIcon')).click(); 
      await browser.driver.sleep(5000); 
      })
      
      it('should Add  ItemSupplier', async (): Promise<any> =>      
      {
      homePage = new HomePage();
     itemmasterPage = new ItemmasterPage();
     await browser.driver.sleep(1000);
     await itemmasterPage.typeSupplier("AL-BASTAIN(342)");
     await itemmasterPage.typeVPN("4321");
     await itemmasterPage.typePrimarySupplier();
     await itemmasterPage.typeSupplierlabel("AL-B");
     await browser.driver.sleep(5000);
     expect(itemmasterPage.addRow.isPresent()).toBeTruthy();
     await itemmasterPage.ClickAddtogridButton();
     await browser.driver.sleep(5000);
     await homePage.saveButtonClick();
     let successmsg = await itemmasterPage.fxbutton;
     expect(successmsg.isPresent()).toBeTruthy();
     await itemmasterPage.successOk();
     await browser.driver.sleep(5000);
     //await element(by.className('toolbar-icons nextIcon')).click(); 
     await element(by.className('closeIcon')).click(); 
     })
   

     it('should search  Itemmaster', async (): Promise<any> =>      
        {
        homePage = new HomePage();
       itemmasterPage = new ItemmasterPage();
       await browser.driver.sleep(1000);
      // await itemmasterPage.typeItemId("");
       await browser.driver.sleep(1000);
       await itemmasterPage.typeItemtype("REGULAR");
       await itemmasterPage.typeDivision("HOMECENTER(3)");
       await itemmasterPage.typeDepartment("ELECTRONICS(14)");
       await itemmasterPage.typeCategory("COMPUTER ACCESSORIES(185)");
       await itemmasterPage.typeClass("COMMUNICATION(8)");
       await itemmasterPage.typeSubclass("BLUETOOTH(3)");
       expect(itemmasterPage.searchbtn.isPresent()).toBeTruthy();
       await homePage.ClickSearchButton();
       await browser.driver.sleep(5000);
       await itemmasterPage.Editiconclick();
       await browser.driver.sleep(5000);
       await element(by.className('toolbar-icons nextIcon')).click();
       await browser.driver.sleep(5000);
       await element(by.className('toolbar-icons nextIcon')).click();
       await browser.driver.sleep(5000);
       await element(by.className('toolbar-icons nextIcon')).click();
       await browser.driver.sleep(5000);
       await itemmasterPage.Viewiconclick();
       await browser.driver.sleep(5000);

        
        
       
       })
      afterEach(() => {

    });


});

