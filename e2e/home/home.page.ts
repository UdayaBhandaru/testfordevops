import { browser, $, element,by} from 'protractor'
import { BasePage } from '../core/base.page'

export class HomePage extends BasePage {

    public bannerimage = $('.left-bar');
    public elementMdm = element(by.css('[ng-reflect-class-base="mdmIconSubModule"]'));
    public seasonSubmenu = element(by.css('[ng-reflect-ng-class="seasonIcon"]'));
    public uomSubmenu = element(by.css('[ng-reflect-ng-class="measure"]')); 
    public orgHierarchy = element(by.xpath('//*[contains(text(),"Organization Hierarchy")]')); 
    public orgcountry  = element(by.css('ng-reflect-ng-class="/OrgCountry"]')); 
    public format       = element(by.css('ng-reflect-ng-class="/Format"]'));
    public region       = element(by.css('ng-reflect-ng-class="/Region"]'));
    public location      = element(by.css('ng-reflect-ng-class="/Location"]'));
    public phaseSubmenu = element(by.partialLinkText('Phase'));
    public valueaddedtax = element(by.xpath('//*[contains(text(),"Value Added Tax")]'));
    public vatregion      = element(by.xpath('//*[contains(text(),"VAT Region")]'));
    public vatcode      = element(by.xpath('//*[contains(text(),"VAT Code")]'));
    public vatrates      = element(by.xpath('//*[contains(text(),"VAT Rates")]'));
    public vatregionvatcode      = element(by.xpath('//*[contains(text(),"VAT Region VAT Code")]'));
    public costzone = element(by.xpath('//*[contains(text(),"Cost Zone")]'));
    public costchangereason  = element(by.xpath('//*[contains(text(),"Cost Change Reason")]'));
    public costzonegroup  = element(by.xpath('//*[contains(text(),"Cost Zone Group")]'));
    public Pricezone = element(by.xpath('//*[contains(text(),"Price Zone")]'));
    public Pricechangereason  = element(by.xpath('//*[contains(text(),"Price Change Reason")]'));
    public Pricezonegroup  = element(by.xpath('//*[contains(text(),"Price Zone Group")]'));
    public Brandmaster = element(by.xpath('//*[contains(text(),"Brand")]'));
    public Brand      = element(by.xpath('//*[contains(text(),"Brand")]'));
    public Brandcategory      = element(by.xpath('//*[contains(text(),"Brand Category")]'));
    public Suppliermaster = element(by.xpath('//*[contains(text()," Supplier")]'));
    public Supplier = element(by.xpath('//*[contains(text(),"Supplier")]'));
    public Supplieraddress       = element(by.css('ng-reflect-ng-class="/SupplierAddressMaster"]'));
    public Itemmaster      = element(by.xpath('//*[contains(text()," Item Master")]'));
    public Order      = element(by.xpath('//*[contains(text(),"Order Management")]'));
    public merchandisehierarchy = element(by.xpath('//*[contains(text(),"Merchandise Hierarchy")]'));
    public season = element(by.xpath('//*[contains(text(),"Season")]'));
    public Division       = element(by.css('ng-reflect-ng-class="/Division Master"]'));
    public Department       = element(by.css('ng-reflect-ng-class="/Department Master"]'));
    public Category       = element(by.css('ng-reflect-ng-class="/Category Master"]'));
    public Class       = element(by.css('ng-reflect-ng-class="/Class Master"]'));
    public Subclass       = element(by.css('ng-reflect-ng-class="/Subclass Master"]'));
    public Categoryrolemaster       = element(by.css('ng-reflect-ng-class="/Category Role Master"]'));
    public Merchantspacemaster       = element(by.css('ng-reflect-ng-class="/Merchant Space Master"]'));
    public Utilities = element(by.xpath('//*[contains(text()," Utilities")]'));
    public Itemlist       = element(by.css('ng-reflect-ng-class="/ItemListHead"]'));
    public Costmanagement      = element(by.xpath('//*[contains(text()," Cost Management")]'));
    public Pricemanagement      = element(by.xpath('//*[contains(text()," Price Management")]'));
   
    public sideButton = $('.mainContentHeader');//$('[ng-reflect-class-base="dropbtn"]');

    async mdmmenuClick()
    {
        let mdmmenu = await element(by.xpath("//*[contains(text(),'MDM')]"));
        return mdmmenu.click();
    }
      
    async uomclasslinkClick()
    {
        let uomclasslink = await element(by.css('[ng-reflect-router-link="../UOMClass"]'));
        return uomclasslink.click();
    }

    async seasonMenuClick()
    {
        let seasonmenu = await $('span[ng-reflect-ng-class="seasonIcon"]');
        return seasonmenu.click();
    }

    async phaselinkClick()
    {
        let phaseclasslink = await element(by.partialLinkText('Phase'));
        return phaseclasslink.click();
    }
     
    async companymasterlink()
    {
        let companymasterlink = await element(by.css('[ng-reflect-router-link="../Company"]'));
        return companymasterlink.click();
    }

    async orgcountrylink()
    {
        let orgcountrylink = await element(by.css('[ng-reflect-router-link="../Org Country"]'));
        return orgcountrylink.click();
    }
    
    async formatlink()
    {
        let Formatlink = await element(by.css('[ng-reflect-router-link="../Format"]'));
        return Formatlink.click();
    }
    async regionlink()
    {
        let regionlink = await element(by.css('[ng-reflect-router-link="../Region"]'));
        return regionlink.click();
    }
    async locationlink()
    {
        let locationlink = await element(by.css('[ng-reflect-router-link="../Location"]'));
        return locationlink.click();

    }

    async Divisionlink()
    {
        let Divisionmasterlink = await element(by.css('[ng-reflect-router-link="../Division Master"]'));
        return Divisionmasterlink.click();
    }
    async Departmentlink()
    {
        let Departmentmasterlink = await element(by.css('[ng-reflect-router-link="../Department Master"]'));
        return Departmentmasterlink.click();
    }
    async Categorylink()
    {
        let Categorymasterlink = await element(by.css('[ng-reflect-router-link="../Category Master"]'));
        return Categorymasterlink.click();
    }
    async Classlink()
    {
        let Classmasterlink = await element(by.css('[ng-reflect-router-link="../Class Master"]'));
        return Classmasterlink.click();
    }
    async Subclasslink()
    {
        let Subclassmasterlink = await element(by.css('[ng-reflect-router-link="../Subclass Master"]'));
        return Subclassmasterlink.click();
    }
    async Categoryrolemasterlink()
    {
        let Categoryrolemasterlink = await element(by.css('[ng-reflect-router-link="../Category Role Master"]'));
        return Categoryrolemasterlink.click();
    }
    async Merchantspacemasterlink()
    {
        let Merchantspacemasterlink = await element(by.css('[ng-reflect-router-link="../Merchant Space Master"]'));
        return Merchantspacemasterlink.click();
    }
    async vatregionlink()
    {
        let vatregionlink = await element(by.css('[ng-reflect-router-link="../VAT Region"]'));
        return vatregionlink.click();

    }
    async vatcodelink()
    {
        let vatcodelink = await element(by.css('[ng-reflect-router-link="../VAT Code"]'));
        return vatcodelink.click();

    }
    async vatrateslink()
    {
        let vatrateslink = await element(by.css('[ng-reflect-router-link="../VAT Rates"]'));
        return vatrateslink.click();

    }
    async vatregionvatcodelink()
    {
        let vatregionvatcodelink = await element(by.css('[ng-reflect-router-link="../VAT Region VAT Code"]'));
        return vatregionvatcodelink.click();

    }
    async costchangereasonlink()
    {
        let costchangereasonlink = await element(by.css('[ng-reflect-router-link="../Cost Change Reason"]'));
        return costchangereasonlink.click();

    }
    async Pricechangereasonlink()
    {
        let Pricechangereasonlink = await element(by.css('[ng-reflect-router-link="../Price Change Reason"]'));
        return Pricechangereasonlink.click();

    }
    async Pricezonegrouplink()
    {
        let Pricezonegrouplink = await element(by.css('[ng-reflect-router-link="../Price Zone Group"]'));
        return Pricezonegrouplink.click();

    }
    async Itemlistlink()
    {
        let Itemlistlink = await element(by.css('[ng-reflect-router-link="../Item List"]'));
        return Itemlistlink.click();

    }

    async Brandlink()
    {
        let Brandlink = await element(by.css('[ng-reflect-router-link="../Brand"]'));
        return Brandlink.click();

    }
    async Brandcategorylink()
    {
        let Brandcategorylink = await element(by.css('[ng-reflect-router-link="../Brand Category"]'));
        return Brandcategorylink.click();

    }
    
    async Supplierlink()
    {
        let Supplierlink = await element(by.css('[ng-reflect-router-link="../Supplier"]'));
        return Supplierlink.click();
    }

    async supplieraddresslink()
    {
        let supplieraddresslink = await element(by.css('[ng-reflect-router-link="../Supplier Address"]'));
        return supplieraddresslink.click();
    }

    async ItemmastermenuClick()
    {
        let Itemmastermenu = await element(by.xpath("//*[contains(text(),' Item Master')]"));
        return Itemmastermenu.click();
    }
    async OrdermenuClick()
    {
        let Ordermenu = await element(by.xpath("//*[contains(text(),' Order Management')]"));
        return Ordermenu.click();
    }
    async CostmanagementmenuClick()
    {
        let Costmanagementmenu = await element(by.xpath("//*[contains(text(),' Cost Management')]"));
        return Costmanagementmenu.click();
    }
    async PricemanagementmenuClick()
    {
        let Pricemanagementmenu = await element(by.xpath("//*[contains(text(),' Price Management')]"));
        return Pricemanagementmenu.click();
    }
    async ClickAddButton(): Promise<any> {
        let submit = element(by.id("searchbarAdd"));
        return await submit.click();
    }
    async ClickSearchButton(): Promise<any> {
        let submit = element(by.id("searchbarSearch"));
        return await submit.click();
    }
    async searchButtonClick()
    {
        console.log("In searchButtonClick");
        browser.debugger();         
        let searchBtn = await element(by.name('Search'));
        console.log(searchBtn);
        if (searchBtn != null)
        {
            return searchBtn.click();
        }
        else
        {
            let searchBtn = await $('span[ng-reflect-ng-class-base="searchbtn"]');
            return searchBtn.click();
        }
    }
    async ClickresetButton(): Promise<any> {
        let submit = element(by.id("searchbarReset"));
        return await submit.click();
    }
    
    async ClickAddtogridButton(): Promise<any> 
    {
        console.log("In AddtogridButtonClick");
        let AddtogridButton = element(by.xpath("//button[contains(@name,'+ Add to Grid')]"));
        return await AddtogridButton.click();
    }

    async saveButtonClick()
    {
        console.log("In saveButtonClick");
        let saveBtn = element(by.xpath("//button[contains(@name,'savecontinue')]"));
        return saveBtn.click();       
    }

    async Editiconclick()
    {
        console.log("In Editiconclick");
        let Editicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'Edit')]"));

        return await Editicon.click();       
    }
    async Viewiconclick()
    {
        console.log("In Viewiconclick");
        let Viewicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'View')]"));

        return await Viewicon.click();       
    }
    async ClickCompanylink(): Promise<any> 
    {
        console.log("In Companylink");
        let Companylink = element(by.className("listitem"));
        return await Companylink.click();
    }
}