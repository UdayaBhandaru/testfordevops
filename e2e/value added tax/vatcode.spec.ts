import { browser, ExpectedConditions as EC, $, $$, element, by } from 'protractor'
import { HomePage } from '../home/home.page'
import { LoginPage } from '../login/login.page'
import { VatcodePage } from "./vatcode.page";
import { protractor } from 'protractor/built/ptor';
declare let expect: any


describe('vatcode search', function () {
    let homePage: HomePage;
    let loginpage: LoginPage;
    let vatcodePage: VatcodePage;
    beforeEach(async () => {
       
       
    })    

    it('should open the Vatcode page', async (): Promise<any> => {
        loginpage = new LoginPage();
        await loginpage.open();      
        await loginpage.login();  
        homePage = new HomePage();
        vatcodePage = new VatcodePage();
        browser.waitForAngularEnabled(false);
        await browser.driver.sleep(5000);
        await homePage.mdmmenuClick();
        let valueaddedtax  = await homePage.valueaddedtax; 
        expect(valueaddedtax.isPresent()).toBeTruthy();
        await valueaddedtax.click();  
        await homePage.vatcode.click();   
        expect(vatcodePage.addBtn.isPresent()).toBeTruthy();
        await homePage.ClickAddButton();
       await browser.driver.sleep(5000);
        
     }) 
     it('should add the vatcode', async (): Promise<any> =>  
      
     {
       await browser.driver.sleep(10);      
       await vatcodePage.typevatcode("VAT278");
       await homePage.saveButtonClick();
       let successmsg = await vatcodePage.fxbutton;
       expect(successmsg.isPresent()).toBeTruthy();
       await  vatcodePage.successOk();
       await element(by.className('toolbar-icons searchIcon')).click();

     })
   
     it('should open the Vatcode search page', async (): Promise<any> => {

        expect(vatcodePage.searchbtn.isPresent()).toBeTruthy();
        await homePage.ClickSearchButton();
        await browser.driver.sleep(5000);
        await vatcodePage.Viewiconclick();
        await browser.driver.sleep(5000); 
        await element(by.className('closeIcon')).click();
        await vatcodePage.Editiconclick();
        await browser.driver.sleep(5000);
        expect (vatcodePage.listitem.isPresent()).toBeTruthy();
        await vatcodePage.ClickVatRates();
        await browser.driver.sleep(5000);
        
       
        })


    afterEach(() => {

    });
});