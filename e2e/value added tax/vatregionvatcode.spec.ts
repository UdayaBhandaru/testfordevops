import { browser, ExpectedConditions as EC, $, $$, element, by } from 'protractor'
import { HomePage } from '../home/home.page'
import { LoginPage } from '../login/login.page'
import { vatregionvatcodePage } from "./Vatregionvatcode.page";
import { protractor } from 'protractor/built/ptor';
declare let expect: any


describe('Vatregiobvatcode search', function () {
    let homePage: HomePage;
    let loginpage: LoginPage;
    let VatregionvatcodePage: vatregionvatcodePage;
    beforeEach(async () => {
       
       
    })   

    it('should open the Vatregionvatcode page', async (): Promise<any> => 
      {
          loginpage = new LoginPage();
          await loginpage.open();      
          await loginpage.login();  
          homePage = new HomePage();
          VatregionvatcodePage = new vatregionvatcodePage();
          browser.waitForAngularEnabled(false);
          await browser.driver.sleep(5000);
          await homePage.mdmmenuClick();
          let valueaddedtax  = await homePage.valueaddedtax; 
          expect(valueaddedtax.isPresent()).toBeTruthy();
          await valueaddedtax.click();  
          await homePage.vatregionvatcode.click();      
          await browser.driver.sleep(5000);
          expect(VatregionvatcodePage.addBtn.isPresent()).toBeTruthy();
          await homePage.ClickAddButton();
          await browser.driver.sleep(5000); 
       
     }) 
     
    

     it('should add/Edit the vatregionvatcode', async (): Promise<any> =>  
      
     { 
        await VatregionvatcodePage.typevatcode("VAT26");
        await VatregionvatcodePage.typevatregioncode("TSC KUWAIT(VAT73)");
        await homePage.saveButtonClick();
        let successmsg = await VatregionvatcodePage.fxbutton;
        expect(successmsg.isPresent()).toBeTruthy();
        await VatregionvatcodePage.successOk();
        await element(by.className('closeIcon ')).click();
        await browser.driver.sleep(5000);
      
      })
 
      
      it('should open the Vatregionvatcode search page', async (): Promise<any> => {

        expect(VatregionvatcodePage.searchbtn.isPresent()).toBeTruthy();
        await homePage.ClickSearchButton();
        await browser.driver.sleep(5000); 
        await VatregionvatcodePage.Viewiconclick();
        await browser.driver.sleep(5000);
        await element(by.className('closeIcon')).click();
        await browser.driver.sleep(5000);
        await VatregionvatcodePage.Editiconclick();
        expect (VatregionvatcodePage.listitem.isPresent()).toBeTruthy();
        await VatregionvatcodePage.ClickVatRates();
        await browser.driver.sleep(5000);
       
        })
  
  
    


    afterEach(() => {

    });
});