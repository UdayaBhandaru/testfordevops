import { browser, element, by, $, WebElement } from 'protractor';
import { BasePage } from '../core/base.page';
import { HomePage } from '../home/home.page';

export class VatratesPage extends BasePage 
{
   public vatcodeMenu = element(by.css("a[href*='vatrates']"));
   public addBtn = element(by.id("searchbarAdd"));
    public resetButton = element(by.name('Reset'));
    public fxbutton = element(by.className('fx-button'));
    public searchbtn = element(by.className('searchbtn'));
    public listitem = element(by.className('listitem'));
    
    async vatratesPageSearch(): Promise<any>
    {
        let submit = await element(by.name('search'));
        return submit.click();
    }

    async vatratesPageAdd(): Promise<any>
    {
        var span1 = element(by.binding('person.name'));
    }

 
   
    async typevatrates(vatrate: string)
    {
        let vatratesInput = await element(by.name('vatRate'));      
        await vatratesInput.sendKeys(vatrate);
        await browser.driver.sleep(5000);
    }

    async typevatcode(vatcode: string)
    {
        let vatcodeInput = await element(by.name('Vat Code'));      
        await vatcodeInput.sendKeys(vatcode);
        await browser.driver.sleep(5000);
    }
    async Editiconclick()
    {
        console.log("In Editiconclick");
        let Editicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'Edit')]"));

        return await Editicon.click();       
    }
    async Viewiconclick()
    {
        console.log("In Viewiconclick");
        let Viewicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'View')]"));

        return await Viewicon.click();       
    }
   

    async successOk()
    {
        let ok = await element(by.xpath('//*[contains(text(), "OK")]'));
        await ok.click();
        await browser.driver.sleep(8000);
    }
    async ClickVatCode(): Promise<any> 
    {
        console.log("In ClickVatcode");
        let ClickVatcode = element(by.className("listitem"));
        return await ClickVatcode.click();
    }
} 