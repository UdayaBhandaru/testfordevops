import { browser, ExpectedConditions as EC, $, $$, element, by } from 'protractor'
import { HomePage } from '../home/home.page'
import { LoginPage } from '../login/login.page'
import { VatregionPage } from "./vatregion.page";
import { protractor } from 'protractor/built/ptor';
declare let expect: any


describe('vatregion search', function () {
    let homePage: HomePage;
    let loginpage: LoginPage;
    let vatregionPage: VatregionPage;
    beforeEach(async () => {
        
       
    })   

    it('should open the Vatregion page', async (): Promise<any> => {
        loginpage = new LoginPage();
        await loginpage.open();      
        await loginpage.login();  
        homePage = new HomePage();
        vatregionPage = new VatregionPage();
        browser.waitForAngularEnabled(false);
        await browser.driver.sleep(5000);
        await homePage.mdmmenuClick();
        let valueaddedtax  = await homePage.valueaddedtax; 
        expect(valueaddedtax.isPresent()).toBeTruthy();
        await valueaddedtax.click();  
        await homePage.vatregion.click();   
        await browser.driver.sleep(5000);
        expect(vatregionPage.addBtn.isPresent()).toBeTruthy();
        await homePage.ClickAddButton();
        await browser.driver.sleep(5000);
        
        
       
    }) 
    
    it('should add/Edit the vatregion', async (): Promise<any> =>  
      
     {
       await browser.driver.sleep(10);      
       await vatregionPage.typevatcode("VAT179");
       await vatregionPage.typevatregionName("TSC KUWAIT");
       await homePage.saveButtonClick();
       await browser.driver.sleep(5000);
       let successmsg = await vatregionPage.fxbutton;
       expect(successmsg.isPresent()).toBeTruthy();
       await  vatregionPage.successOk();
       await element(by.className('closeIcon')).click();
       await browser.driver.sleep(5000);
    
     })
 

    it('should open the Vatregion search page', async (): Promise<any> => {

        await vatregionPage.typevatcode("VAT10");
        expect(vatregionPage.searchbtn.isPresent()).toBeTruthy();
        await homePage.ClickSearchButton();
        await browser.driver.sleep(5000); 
        await vatregionPage.Viewiconclick();
        await browser.driver.sleep(5000);
        await element(by.className('closeIcon')).click();
        await browser.driver.sleep(5000);
        await vatregionPage.Editiconclick();
        await element(by.className('toolbar-icons searchIcon')).click();
        await browser.driver.sleep(5000);
       
        })



    afterEach(() => {

    });
});