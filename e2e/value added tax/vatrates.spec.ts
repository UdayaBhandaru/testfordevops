import { browser, ExpectedConditions as EC, $, $$, element, by } from 'protractor'
import { HomePage } from '../home/home.page'
import { LoginPage } from '../login/login.page'
import { VatratesPage } from "./vatrates.page";
import { protractor } from 'protractor/built/ptor';
declare let expect: any


describe('vatrates search', function () {
    let homePage: HomePage;
    let loginpage: LoginPage;
    let vatratesPage: VatratesPage;
    beforeEach(async () => {
       
       
    })   
 
    it('should open the Vatrates page', async (): Promise<any> => 
    {
        loginpage = new LoginPage();
        await loginpage.open();      
        await loginpage.login();  
        homePage = new HomePage();
        vatratesPage = new VatratesPage();
        browser.waitForAngularEnabled(false);
        await browser.driver.sleep(5000);
        await homePage.mdmmenuClick();
        let valueaddedtax  = await homePage.valueaddedtax; 
        expect(valueaddedtax.isPresent()).toBeTruthy();
        await valueaddedtax.click();  
        await homePage.vatrates.click();     
        await browser.driver.sleep(5000);
        expect(vatratesPage.addBtn.isPresent()).toBeTruthy();
        await homePage.ClickAddButton();
        await browser.driver.sleep(5000);
        
        
       
     }) 
     it('should add/Edit the vatrates', async (): Promise<any> =>  
      
     {
        await browser.driver.sleep(10);      
        await vatratesPage.typevatcode("VAT18");
        await browser.driver.sleep(100);
        await vatratesPage.typevatrates("VAT01");
       await homePage.saveButtonClick();
    //    await browser.driver.sleep(5000);
    //    let successmsg = await vatratesPage.fxbutton;
    //    expect(successmsg.isPresent()).toBeTruthy();
    //    await  vatratesPage.successOk();
       await element(by.className('toolbar-icons searchIcon')).click();
      
      })

     it('should open the Vatrates search page', async (): Promise<any> => {
        expect(vatratesPage.searchbtn.isPresent()).toBeTruthy();
        await homePage.ClickSearchButton();
        await browser.driver.sleep(5000); 
        await vatratesPage.Viewiconclick();
        await browser.driver.sleep(5000);
        await element(by.className('closeIcon')).click();
        await browser.driver.sleep(5000);
        await vatratesPage.Editiconclick();
        expect (vatratesPage.listitem.isPresent()).toBeTruthy();
        await vatratesPage.ClickVatCode();
        await browser.driver.sleep(5000);
        })
    
 
      
    afterEach(() => {

    });
});