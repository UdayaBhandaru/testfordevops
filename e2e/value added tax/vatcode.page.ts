import { browser, element, by, $, WebElement } from 'protractor';
import { BasePage } from '../core/base.page';
import { HomePage } from '../home/home.page';

export class VatcodePage extends BasePage 
{
   public vatcodeMenu = element(by.css("a[href*='vatcode']"));
   public addBtn = element(by.id("searchbarAdd"));
   public listitem = element(by.className('listitem'));
    public resetButton = element(by.name('Reset'));
    public fxbutton = element(by.className('fx-button'));
    public searchbtn = element(by.className('searchbtn'));
    
    async vatcodePageSearch(): Promise<any>
    {
        let submit = await element(by.name('search'));
        return submit.click();
    }

    async vatcodePageAdd(): Promise<any>
    {
        var span1 = element(by.binding('person.name'));
    }
 

   
    async typevatcode(vatcode: string)
    {
        let vatcodeInput = await element(by.name('vatCode'));      
        await vatcodeInput.sendKeys(vatcode);
        await browser.driver.sleep(5000);
    }

    async Editiconclick()
    {
        console.log("In Editiconclick");
        let Editicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'Edit')]"));

        return await Editicon.click();       
    }
    async Viewiconclick()
    {
        console.log("In Viewiconclick");
        let Viewicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'View')]"));

        return await Viewicon.click();       
    }
   

    async successOk()
    {
        let ok = await element(by.xpath('//*[contains(text(), "OK")]'));
        await ok.click();
        await browser.driver.sleep(8000);
    }
    async ClickVatRates(): Promise<any> 
    {
        console.log("In ClickVatRates");
        let ClickVatRates = element(by.className("listitem"));
        return await ClickVatRates.click();
    }
} 