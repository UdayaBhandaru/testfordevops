import { browser, element, by, $, WebElement } from 'protractor';
import { BasePage } from '../core/base.page';
import { HomePage } from '../home/home.page';
import { DropDownHelper } from "../core/DropdownHelper";
export class vatregionvatcodePage extends BasePage 
{
    public vatregionvatcodeMenu = element(by.css("a[href*='vatrates']"));
    public addBtn = element(by.id("searchbarAdd"));
    private dropdownHelper: DropDownHelper;
    public resetButton = element(by.name('Reset'));
    public fxbutton = element(by.className('fx-button'));
    public listitem = element(by.className('listitem'));
    public searchbtn = element(by.className('searchbtn'));
    
    async vatregionvatcodePageSearch(): Promise<any>
    {
        let submit = await element(by.name('search'));
        return submit.click();
    }

    async vatregionvatcodePageAdd(): Promise<any>
    {
        var span1 = element(by.binding('person.name'));
    }

 
   
    async typevatcode(Vatcode: string)  
    {
        let VatcodeInput = await element(by.name('VAT Code'));   
        await VatcodeInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let vatregionvatcodeInput = element(by.xpath("//*[contains(text(),'VAT26')]"));   
        await vatregionvatcodeInput.click();
        await browser.driver.sleep(8000);
    }
    async typevatregioncode(vatregioncode: string)  
    {
        let vatregioncodeInput = await element(by.name('VAT Region Code'));   
        await vatregioncodeInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let vatregionvatcodeInput = element(by.xpath("//*[contains(text(),'TSC KUWAIT(VAT73)')]"));   
        await vatregionvatcodeInput.click();
        await browser.driver.sleep(8000);
    }
    
    async Editiconclick()
    {
        console.log("In Editiconclick");
        let Editicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'Edit')]"));

        return await Editicon.click();       
    }
    async Viewiconclick()
    {
        console.log("In Viewiconclick");
        let Viewicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'View')]"));

        return await Viewicon.click();       
    }
   


    async successOk()
    {
        let ok = await element(by.xpath('//*[contains(text(), "OK")]'));
        await ok.click();
        await browser.driver.sleep(8000);
    }
    async ClickVatRates(): Promise<any> 
    {
        console.log("In ClickVatRates");
        let ClickVatRates = element(by.className("listitem"));
        return await ClickVatRates.click();
    }
}