import { browser, ExpectedConditions as EC, $, $$, element, by } from 'protractor'
import { HomePage } from '../home/home.page'
import { LoginPage } from '../login/login.page'
import { CompanyPage } from "./Company.page";
import { protractor } from 'protractor/built/ptor';
declare let expect: any


describe('Company search', function () {
    let homePage: HomePage;
    let loginpage: LoginPage;
    let companyPage: CompanyPage;
    beforeEach(async () => {
       
        
    })   

    it('should open the Company Addpage', async (): Promise<any> => {
        loginpage = new LoginPage();
        await loginpage.open();      
        await loginpage.login();  
        homePage = new HomePage();
        companyPage = new CompanyPage();
        browser.waitForAngularEnabled(false);
        await browser.driver.sleep(1000);
        await homePage.mdmmenuClick();
        let organization  = await homePage.orgHierarchy;       
        expect(organization.isPresent()).toBeTruthy();
        await organization.click();       
        await companyPage.companyMenu.click();
        await browser.driver.sleep(5000);
        expect(companyPage.addBtn.isPresent()).toBeTruthy();
       
    }) 
    
    it('should add/Edit the Company', async (): Promise<any> => {    
       await browser.driver.sleep(10); 
       await homePage.ClickAddButton();        
       await element(by.xpath('//*[contains(text(),"SAVE & CLOSE")]')).click();       
       await element(by.xpath('//*[contains(text(),"RESET")]')).click();    
       await companyPage.typeCompanyId("899");
       await companyPage.typeCompanyName("SULTAN CENTRE 01");
       await companyPage.typelegalname("TSJ 1 ");
       await companyPage.typeCurrencycode("KUWAIT DINAR");
       await homePage.saveButtonClick();
       await browser.driver.sleep(5000);
       let successmsg = await companyPage.fxbutton;
       expect(successmsg.isPresent()).toBeTruthy();
       await companyPage.successOk();
      
       await browser.driver.sleep(500);
       await element(by.className('closeIcon')).click();
       await browser.driver.sleep(5000);
     })
    
     it('should open the company search page', async (): Promise<any> => {

      // await companyPage.typeCompanyId("89");
    expect(companyPage.searchbtn.isPresent()).toBeTruthy();
     await homePage.ClickSearchButton();  
     await browser.driver.sleep(5000);  
     await companyPage.Viewiconclick();    
     await element(by.className('closeIcon')).click();
     await companyPage.Editiconclick();  
     await browser.driver.sleep(5000);    
     await homePage.saveButtonClick(); 
     let successmsg = await companyPage.fxbutton;
     expect(successmsg.isPresent()).toBeTruthy();
     await companyPage.successOk();     
     await browser.driver.sleep(5000);   
     await companyPage.ClickOrgcountrylink();   
     await browser.driver.sleep(5000);    
     await homePage.Editiconclick(); 
     await browser.driver.sleep(5000);      
     await homePage.ClickCompanylink();   
     //await element(by.className('closeIcon')).click();
    
     })

    afterEach(() => {

    });
});