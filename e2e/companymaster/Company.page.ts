import { browser, element, by, $, WebElement } from 'protractor';
import { BasePage } from '../core/base.page';
import { HomePage } from '../home/home.page';
import { DropDownHelper } from "../core/DropdownHelper";

export class CompanyPage extends BasePage  
{
   public companyMenu = element(by.css("a[href*='Company']"));
   public addBtn = element(by.id("searchbarAdd"));
    public searchButton = element(by.id('searchbarSearch'));
    public searchbtn = element(by.className('searchbtn'));
    public backbtn = element(by.className('helpIconFor'));
    public noSearchIcon = element(by.className('noSearch-records'));
    public fxbutton = element(by.className('fx-button'));
    private dropdownHelper: DropDownHelper;
    async CompanyPageSearch(): Promise<any>
    {
        let submit = await element(by.id('searchbarSearch'));
        return submit.click();
    }

    async CompanyPageAdd(): Promise<any>
    {
        var span1 = element(by.binding('person.name'));
    }


   
    async typeCompanyId(CompanyId: string)
    {
        let companyIdInput = await element(by.name('companyId'));      
        await companyIdInput.sendKeys(CompanyId);
        await browser.driver.sleep(5000);
    }


    async typeCompanyName(Companyname: string)
    {
        let CompanynameInput = await element(by.name('companyName')); 
        await CompanynameInput.sendKeys(Companyname);
        await browser.driver.sleep(5000);
    }


    async typelegalname(legalname: string)
    {
        let legalnameInput = await element(by.name('legalName'));
       // await legalnameInput.sendKeys(legalname);
        await legalnameInput.clear(legalname);
        await legalnameInput.sendKeys(legalname);
        await browser.driver.sleep(5000);
    }

    async typeCurrencycode(Currencycode: string)  
    {
        let CurrencycodeInput = await element(by.name('Currency Code'));   
        await CurrencycodeInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let companyInput = element(by.xpath("//*[contains(text(),'KUWAIT DINAR')]"));   
        await companyInput.click();
        await browser.driver.sleep(8000);
    }
    
    async Editiconclick()
    {
        console.log("In Editiconclick");
        let Editicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'Edit')]"));

        return await Editicon.click();       
    }
    async Viewiconclick()
    {
        console.log("In Viewiconclick");
        let Viewicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'View')]"));

        return await Viewicon.click();       
    }

    async Orgcountrylinkclick()
    {
        let Orgcountrylink = await element(by.css('[ng-reflect-router-link="../Org Country"]'));
        return await Orgcountrylink.click();
    }
    async successOk()
    {
        let ok = await element(by.xpath('//*[contains(text(), "OK")]'));
        await ok.click();
        await browser.driver.sleep(8000);
    }

    async ClickOrgcountrylink(): Promise<any> 
    {
        console.log("In Orgcountrylink");
        let Orgcountrylink = element(by.className("listitem"));
        return await Orgcountrylink.click();
    }
}