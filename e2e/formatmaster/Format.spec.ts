import { browser, ExpectedConditions as EC, $, $$, element, by } from 'protractor'
import { HomePage } from '../home/home.page'
import { LoginPage } from '../login/login.page'
import { FormatPage } from "./format.page";
import { protractor } from 'protractor/built/ptor';
declare let expect: any


describe('Format search', function () {
    let homePage: HomePage;
    let loginpage: LoginPage;
    let formatPage: FormatPage;
    beforeEach(async () => {
       
        
    })   

    it('should open the Format page', async (): Promise<any> => {
        loginpage = new LoginPage();
        await loginpage.open();      
        await loginpage.login();  
        homePage = new HomePage();
        formatPage = new FormatPage();
        browser.waitForAngularEnabled(false);
        await browser.driver.sleep(1000);
        await homePage.mdmmenuClick();
        let organization  = await homePage.orgHierarchy; 
        expect(organization.isPresent()).toBeTruthy();
        await organization.click();       
        await formatPage.formatMenu.click();
        await browser.driver.sleep(5000);
        expect(formatPage.addBtn.isPresent()).toBeTruthy();
        await homePage.ClickAddButton();
        await browser.driver.sleep(5000);
        await homePage.saveButtonClick();
    })

it('should add/Edit the Format page', async (): Promise<any> => {
       await browser.driver.sleep(10);   
       await formatPage.typeCompany("TSC KUWAIT(1)");   
       await formatPage.typeCountry("ARUBA(ABW)");
       await formatPage.typeRegion('TSC KUWAIT');
       await formatPage.typeCode("4659");
       await formatPage.typeFormatName("Express Shopes Mark");
       await formatPage.typelegalname("TSCK");
       await homePage.saveButtonClick();
       await browser.driver.sleep(5000);
       let successmsg = await formatPage.fxbutton;
       expect(successmsg.isPresent()).toBeTruthy();
       await formatPage.successOk();
       await element(by.className('closeIcon')).click();
     })

  
    it('should open the Format search page', async (): Promise<any> => {
        expect(formatPage.searchbtn.isPresent()).toBeTruthy();
        await homePage.ClickSearchButton();
        await browser.driver.sleep(5000); 
        await formatPage.Viewiconclick();
        await browser.driver.sleep(5000);
        await element(by.className('closeIcon')).click();
        await formatPage.Editiconclick();
        await browser.driver.sleep(5000);
        await homePage.saveButtonClick();
        let successmsg = await formatPage.fxbutton;
        expect(successmsg.isPresent()).toBeTruthy();
        await formatPage.successOk();
        
        })
    

    afterEach(() => {

    });
});