import { browser, element, by, $, WebElement } from 'protractor';
import { BasePage } from '../core/base.page';
import { HomePage } from '../home/home.page';
import { DropDownHelper } from "../core/DropdownHelper";
import { DateFormat } from "../core/dateFormat";

export class FormatPage extends BasePage  
{
   public formatMenu = element(by.css("a[href*='Format']"));
   public addBtn = element(by.id("searchbarAdd"));
    public searchButton = element(by.id('searchbarSearch'));
    public searchbtn = element(by.className('searchbtn'));
    public backbtn = element(by.className('helpIconFor'));
    public noSearchIcon = element(by.className('noSearch-records'));
    public fxbutton = element(by.className('fx-button'));
    private dropdownHelper: DropDownHelper;
    async FormatPageSearch(): Promise<any>
    {
        let submit = await element(by.name('search'));
        return submit.click();
    }

    async FormatPageAdd(): Promise<any>
    {
        var span1 = element(by.binding('person.name'));
    }
    
    async typeCompany(Company: string)  
    {
        let CompanyInput = await element(by.name('Company'));   
        await CompanyInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let orgcountryInput = element(by.xpath("//*[contains(text(),'TSC KUWAIT(1)')]"));   
        await orgcountryInput.click();
        await browser.driver.sleep(8000);
    }

    async typeCountry(Country: string)  
    {
        let CountryInput = await element(by.name('Country'));   
        await CountryInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let orgcountryInput = element(by.xpath("//*[contains(text(),'ARUBA(ABW)')]"));   
        await orgcountryInput.click();
        await browser.driver.sleep(8000);
    }

    async typeRegion(Region: string)  
    {
        let RegionInput = await element(by.name('Region'));   
        await RegionInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let orgcountryInput = element(by.xpath("//*[contains(text(),'TSC KUWAIT')]"));   
        await orgcountryInput.click();
        await browser.driver.sleep(8000);
    }
     
    async typeCode(code: string)
    {
        let codeInput = await element(by.name('formatCode')); 
        await codeInput.sendKeys(code);
        await browser.driver.sleep(5000);
    }
    
    async typeFormatName(Formatname: string)
    {
        let formatnameInput = await element(by.name('formatName')); 
        await formatnameInput.sendKeys(Formatname);
        await browser.driver.sleep(800);
    }
    async typelegalname(legalname: string)
    {
        let legalnameInput = await element(by.name('legalName'));
        await legalnameInput.sendKeys(legalname);
        await browser.driver.sleep(5000);
    }
    async Editiconclick()
    {
        console.log("In Editiconclick");
        let Editicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'Edit')]"));

        return await Editicon.click();       
    }
    async Viewiconclick()
    {
        console.log("In Viewiconclick");
        let Viewicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'View')]"));

        return await Viewicon.click();       
    }
   

    async successOk()
    {
        let ok = await element(by.xpath('//*[contains(text(), "OK")]'));
        await ok.click();
        await browser.driver.sleep(8000);
    }
}