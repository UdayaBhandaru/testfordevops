import { browser, ExpectedConditions as EC, $, $$, element, by } from 'protractor'
import { HomePage } from '../home/home.page'
import { LoginPage } from '../login/login.page'
import { UomClassPage } from "./uomclass.page";
import { protractor } from 'protractor/built/ptor';
declare let expect: any


describe('uom class search', function () {
    let homePage: HomePage;
    let loginpage: LoginPage;
    let uomclassPage: UomClassPage;
    beforeEach(async () => {
        loginpage = new LoginPage();
        await loginpage.open();
    })

    it('should login to the application', async (): Promise<any> => {       
        homePage = new HomePage();
        await loginpage.login();       
        await expect(homePage.bannerimage.isPresent()).toBe(true);        
    })

    it('should open the uom class page', async (): Promise<any> => {
        await loginpage.login();
        homePage = new HomePage();
        uomclassPage = new UomClassPage();
        await homePage.mdmmenuClick();
        let uomSubmenu = await homePage.uomSubmenu; 
        browser.waitForAngularEnabled(false);
        expect(uomSubmenu.isPresent()).toBeTruthy();
        await uomSubmenu.click();       
        await homePage.uomclasslinkClick();
        await browser.driver.sleep(5000);
        expect(await uomclassPage.searchButton.isPresent()).toBe(true);
    })   

    afterEach(() => {

    });
});