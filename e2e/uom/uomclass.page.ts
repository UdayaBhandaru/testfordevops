import { browser, element, by, $ } from 'protractor'
import { BasePage } from '../core/base.page'
import { HomePage } from '../home/home.page'

export class UomClassPage extends BasePage {
    public searchButton = element(by.css('[ng-reflect-ng-class="measure"]'));
    async uomclasssearch(): Promise<any> {
        let submit = await element(by.css('[ng-reflect-class-base="searchbtn"]'));
        return submit;
    }

    async typeUomClass(uomclass: string) {
        let uomclassInput = await $('input[ng-reflect-name=uomClass]');
        await uomclassInput.sendKeys(uomclass);
    }

    async typeDesc(desc: string) {
        let descInput = await $('input[ng-reflect-name=uomClassDesc]');
        await descInput.sendKeys(desc);
    }

    async save(uomclass, description) {
        let save = await $('button[name=save]');
        await this.typeUomClass(uomclass);
        await this.typeDesc(description);
        return save.click();
    }
    
    async Editiconclick()
    {
        console.log("In Editiconclick");
        let Editicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'Edit')]"));

        return await Editicon.click();       
    }
    async Viewiconclick()
    {
        console.log("In Viewiconclick");
        let Viewicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'View')]"));

        return await Viewicon.click();       
    }
   
    async successOk() {
        let ok = await $('.fx-button');
        return ok.click();
    }
}