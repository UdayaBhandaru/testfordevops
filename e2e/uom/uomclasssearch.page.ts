import { browser, element, by, $ } from 'protractor'
import { BasePage } from '../core/base.page'
import { HomePage } from '../home/home.page'

export class UomClassSearchPage extends BasePage {

   
    async typeUomClass(uomclass: string) {
        let usernameInput = await $('input[ng-reflect-name=uomClass]');
        await usernameInput.sendKeys(uomclass);
    }

    async typeDescription(description: string) {
        let passwordInput = await $('input[ng-reflect-name=uomClassDesc]');
        await passwordInput.sendKeys(description);
    }

    async selectStatus(status: string) {
        let passwordInput = await $('input[ng-reflect-name=uomClassDesc]');
        await passwordInput.sendKeys(status);
    }

   
    public searchButton =  $('button[name=search]');

    async search(uomclass, description, status) {
        let search = await $('button[name=search]');
        await this.typeUomClass(uomclass);
        await this.typeDescription(description);
        return search.click();
    }

    async add() {
        let add = await $('button[name=add]');
        return add.click();
    }
}