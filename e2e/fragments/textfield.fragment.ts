
import { BaseArrayFragment, BaseFragment } from 'protractor-element-extend';

export class TextField extends BaseFragment {
 
    constructor(element) {
    super(element) ;
  }

   async setText(val:string){
       console.log(val);
       await this.sendKeys(val);
    
   }
  }
