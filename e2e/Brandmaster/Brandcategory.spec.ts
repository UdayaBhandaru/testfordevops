import { browser, ExpectedConditions as EC, $, $$, element, by } from 'protractor'
import { HomePage } from '../home/home.page'
import { LoginPage } from '../login/login.page'
import { BrandcategoryPage } from "./Brandcategory.page";
import { protractor } from 'protractor/built/ptor';
declare let expect: any


describe('Brandcategory search', function () {
    let homePage: HomePage;
    let loginpage: LoginPage;
    let brandcategoryPage: BrandcategoryPage;
    beforeEach(async () => {
       
        
    })   

    it('should open the Brandcategory Addpage', async (): Promise<any> => {
        loginpage = new LoginPage();
        await loginpage.open();      
        await loginpage.login();  
        homePage = new HomePage();
        brandcategoryPage = new BrandcategoryPage();
        browser.waitForAngularEnabled(false);
        await browser.driver.sleep(5000);
        await homePage.mdmmenuClick();
        let Brandmaster  = await homePage.Brand; 
        expect(Brandmaster.isPresent()).toBeTruthy();
        await Brandmaster.click();       
        await brandcategoryPage.BrandcategoryMenu.click();
        expect(brandcategoryPage.addBtn.isPresent()).toBeTruthy();
        await homePage.ClickAddButton();
        await browser.driver.sleep(5000);  
    })
    it('should add/Edit the Brandcategory', async (): Promise<any> =>  {

      await brandcategoryPage.typeBrandId("DURACELL");      
      await brandcategoryPage.typeBrandcategoryId("12985");
      await brandcategoryPage.typeBrandkeycategory("Electrical");
      await homePage.saveButtonClick();
      let successmsg = await brandcategoryPage.fxbutton;
      expect(successmsg.isPresent()).toBeTruthy();
      await brandcategoryPage.successOk();
      await element(by.className('closeIcon')).click();
      await browser.driver.sleep(500);
    })

    it('should open the Brandcategory search page', async (): Promise<any> => {

       await brandcategoryPage.typeBrandcategoryId("12435");
       expect (brandcategoryPage.searchbtn.isPresent()).toBeTruthy();
       await homePage.ClickSearchButton();
       await browser.driver.sleep(5000);
       await brandcategoryPage.Viewiconclick();
       await browser.driver.sleep(5000);
       await element(by.className('closeIcon')).click();
       await brandcategoryPage.Editiconclick();
       await browser.driver.sleep(5000);
       expect (brandcategoryPage.listitem.isPresent()).toBeTruthy();
       await brandcategoryPage.ClickBrandLink();
     })

    
    afterEach(() => {

    });
});