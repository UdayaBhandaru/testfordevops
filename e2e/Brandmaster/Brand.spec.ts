import { browser, ExpectedConditions as EC, $, $$, element, by } from 'protractor'
import { HomePage } from '../home/home.page'
import { LoginPage } from '../login/login.page'
import { BrandPage } from "./Brand.page";
import { protractor } from 'protractor/built/ptor';
declare let expect: any


describe('Brand search', function () {
    let homePage: HomePage;
    let loginpage: LoginPage;
    let brandPage: BrandPage;
    beforeEach(async () => {
       
       
    })    

    it('should open the Brand Addpage', async (): Promise<any> => {
        loginpage = new LoginPage();
        await loginpage.open();      
        await loginpage.login();  
        homePage = new HomePage();
        brandPage = new BrandPage();
        browser.waitForAngularEnabled(false);
        await browser.driver.sleep(5000);
        await homePage.mdmmenuClick();
        let Brand  = await homePage.Brand; 
        expect(Brand.isPresent()).toBeTruthy();
        await Brand.click();       
        await brandPage.BrandMenu.click();
        expect(brandPage.addBtn.isPresent()).toBeTruthy();
        await homePage.ClickAddButton();
        await browser.driver.sleep(5000);
       
    })

    it('should add/Edit the Brand', async (): Promise<any> =>  {
     
       await brandPage.typeBrandId("1343");
       await brandPage.typeBrandName("HPL4tye dfd");
       await brandPage.typeBrandshortname("HP TESTs");
       await homePage.saveButtonClick();
       let successmsg = await brandPage.fxbutton;
       expect(successmsg.isPresent()).toBeTruthy();
       await brandPage.successOk();
       await element(by.className('toolbar-icons searchIcon')).click();

    })

    it('should open the Brand search page', async (): Promise<any> => {
        
        expect(brandPage.searchbtn.isPresent()).toBeTruthy();
        await homePage.ClickSearchButton();
        await browser.driver.sleep(5000); 
        await brandPage.Viewiconclick();
        await browser.driver.sleep(5000);
        await element(by.className('closeIcon')).click();
        await brandPage.Editiconclick();
        await browser.driver.sleep(5000);
        expect (brandPage.listitem.isPresent()).toBeTruthy();
        await brandPage.ClickBrandCatogoryLink();
        await browser.driver.sleep(5000);
        
     
    })
     
    afterEach(() => {

    });
});