import { browser, element, by, $, WebElement } from 'protractor';
import { BasePage } from '../core/base.page';
import { HomePage } from '../home/home.page';
import { DropDownHelper } from "../core/DropdownHelper";

export class BrandPage extends BasePage 
{
   public BrandMenu = element(by.css("a[href*='Brand']"));
   public addBtn = element(by.id("searchbarAdd"));
   public fxbutton = element(by.className('fx-button'));
   public searchbtn = element(by.className('searchbtn'));
   public listitem = element(by.className('listitem'));
    private dropdownHelper: DropDownHelper;

    async BrandPageSearch(): Promise<any>
    {
        let submit = await element(by.id('searchbarSearch'));
        return submit.click();
    }
 
    async BrandPageAdd(): Promise<any>
    {
        var span1 = element(by.binding('person.name'));
    }


   
    async typeBrandId(BrandId: string)
    {
        let BrandIdInput = await element(by.name('brandId'));      
        await BrandIdInput.sendKeys(BrandId);
        await browser.driver.sleep(5000);
    }


    async typeBrandName(Brandname: string)
    {
        let BrandnameInput = await element(by.name('brandName')); 
        await BrandnameInput.sendKeys(Brandname);
        await browser.driver.sleep(5000);
    }


    async typeBrandshortname(Brandshortname: string)
    {
        let BrandshortnameInput = await element(by.name('brandShortName'));
        await BrandshortnameInput.sendKeys(Brandshortname);
        await browser.driver.sleep(5000);
    }
    

    async Editiconclick()
    {
        console.log("In Editiconclick");
        let Editicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'Edit')]"));

        return await Editicon.click();       
    }
    async Viewiconclick()
    {
        console.log("In Viewiconclick");
        let Viewicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'View')]"));

        return await Viewicon.click();       
    }
    
    async successOk()
    {
        let ok = await element(by.xpath('//*[contains(text(), "OK")]'));
        await ok.click();
        await browser.driver.sleep(8000);
    }
    async ClickBrandCatogoryLink(): Promise<any> 
    {
        console.log("In ClickBrandCatogory");
        let ClickBrandCatogory = element(by.className("listitem"));
        return await ClickBrandCatogory.click();
    }
}