import { browser, element, by, $, WebElement } from 'protractor';
import { BasePage } from '../core/base.page';
import { HomePage } from '../home/home.page';
import { DropDownHelper } from "../core/DropdownHelper";

export class BrandcategoryPage extends BasePage 
{
   public BrandcategoryMenu = element(by.css("a[href*='BrandCategory/List']"));
   public addBtn = element(by.id("searchbarAdd"));
   public fxbutton = element(by.className('fx-button'));
   public searchbtn = element(by.className('searchbtn'));
   public listitem = element(by.className('listitem'));
   
    private dropdownHelper: DropDownHelper;

    async BrandcategoryPageSearch(): Promise<any>
    {
        let submit = await element(by.id('searchbarSearch'));
        return submit.click();
    }

    async BrandcategoryPageAdd(): Promise<any>
    {
        var span1 = element(by.binding('person.name'));
    }
 
    async typeBrandId(BrandId: string)  
    {
        let BrandIdInput = await element(by.name('Brand Id'));   
        await BrandIdInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let BrandcategoryInput = element(by.xpath("//*[contains(text(),'DURACELL')]"));   
        await BrandcategoryInput.click();
        await browser.driver.sleep(8000);
    }
   
    async typeBrandcategoryId(BrandId: string)
    {
        let BrandcategoryIdInput = await element(by.name('brandCatId'));      
        await BrandcategoryIdInput.sendKeys(BrandId);
        await browser.driver.sleep(5000);
    }


    async typeBrandkeycategory(Brandkeycategory: string)
    {
        let BrandkeycategoryInput = await element(by.name('brandKeyCat')); 
        await BrandkeycategoryInput.sendKeys(Brandkeycategory);
        await browser.driver.sleep(5000);
    }
   

    async Editiconclick()
    {
        console.log("In Editiconclick");
        let Editicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'Edit')]"));

        return await Editicon.click();       
    }
    async Viewiconclick()
    {
        console.log("In Viewiconclick");
        let Viewicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'View')]"));

        return await Viewicon.click();       
    }
    async successOk()
    {
        let ok = await element(by.xpath('//*[contains(text(), "OK")]'));
        await ok.click();
        await browser.driver.sleep(8000);
    }
    async ClickBrandLink(): Promise<any> 
    {
        console.log("In  ClickBrandLink");
        let  ClickBrandLink = element(by.className("listitem"));
        return await  ClickBrandLink.click();
    }
}