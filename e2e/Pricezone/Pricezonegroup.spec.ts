import { browser, ExpectedConditions as EC, $, $$, element, by } from 'protractor'
import { HomePage } from '../home/home.page'
import { LoginPage } from '../login/login.page'
import { PricezonegroupPage } from "./Pricezonegroup.page";
import { protractor } from 'protractor/built/ptor';
declare let expect: any


describe('Costzonegroup search', function () {
    let homePage: HomePage;
    let loginpage: LoginPage;
    let pricezonegroupPage: PricezonegroupPage;
    beforeEach(async () => {
       
       
    })    

    it('should open the Pricezonegroup Add page', async (): Promise<any> => {
        loginpage = new LoginPage();
        await loginpage.open();      
        await loginpage.login();  
        homePage = new HomePage();
        pricezonegroupPage = new PricezonegroupPage();
        browser.waitForAngularEnabled(false);
        await browser.driver.sleep(5000);
        await homePage.mdmmenuClick();
        let Pricezone  = await homePage.Pricezone; 
        expect(Pricezone.isPresent()).toBeTruthy();
        await Pricezone.click();  
        await homePage.Pricezonegroup.click(); 
        expect(pricezonegroupPage.addBtn.isPresent()).toBeTruthy();  
        await homePage.ClickAddButton();
        await browser.driver.sleep(5000);
        
    }) 
    it('should add/edit the Pricezonegroup', async (): Promise<any> =>  {
        
      await pricezonegroupPage.typezonegroupdisplayid("4315");
      await pricezonegroupPage.typename("Price zone KWaitsd");
      await homePage.saveButtonClick();
      let successmsg = await pricezonegroupPage.fxbutton;
      expect(successmsg.isPresent()).toBeTruthy();
      await pricezonegroupPage.successOk();
      await element(by.className('closeIcon')).click();
      await browser.driver.sleep(5000);
    
    })
    it('should open the Pricezonegroup search page', async (): Promise<any> => {

        expect (pricezonegroupPage.searchbtn.isPresent()).toBeTruthy();
        await homePage.ClickSearchButton();
        await browser.driver.sleep(5000); 
        await pricezonegroupPage.Viewiconclick();
        await browser.driver.sleep(5000);
        await element(by.className('closeIcon')).click();
        await pricezonegroupPage.Editiconclick();
        expect (pricezonegroupPage.listitem.isPresent()).toBeTruthy();
        await pricezonegroupPage.ClickPriceZoneLink();
        await browser.driver.sleep(5000);
    })
    afterEach(() => {

    });
});