import { browser, ExpectedConditions as EC, $, $$, element, by } from 'protractor'
import { HomePage } from '../home/home.page'
import { LoginPage } from '../login/login.page'
import { PricechangereasonPage } from "./Pricechangereason.page";
import { protractor } from 'protractor/built/ptor';
declare let expect: any


describe('Pricechangereason search', function () {
    let homePage: HomePage;
    let loginpage: LoginPage;
    let pricechangereasonPage: PricechangereasonPage;
    beforeEach(async () => {
       
       
    })    

    it('should open the Pricechangereason Add page', async (): Promise<any> => {
        loginpage = new LoginPage();
        await loginpage.open();      
        await loginpage.login();  
        homePage = new HomePage();
        pricechangereasonPage = new PricechangereasonPage();
        browser.waitForAngularEnabled(false);
        await browser.driver.sleep(5000);
        await homePage.mdmmenuClick();
        let Pricezone  = await homePage.Pricezone; 
        expect(Pricezone.isPresent()).toBeTruthy();
        await Pricezone.click();  
        await homePage.Pricechangereason.click();  
        expect(pricechangereasonPage.addBtn.isPresent()).toBeTruthy(); 
        await homePage.ClickAddButton();
        await browser.driver.sleep(5000);
       
    }) 
   
    it('should add/edit  the Pricechangereason', async (): Promise<any> => {

            
       await pricechangereasonPage.typedescription("New Year Price changed as floows");
       await homePage.saveButtonClick();
       let successmsg = await pricechangereasonPage.fxbutton;
       expect(successmsg.isPresent()).toBeTruthy(); 
       await pricechangereasonPage.successOk();
       await element(by.className('closeIcon')).click();
       await browser.driver.sleep(5000);
     })

     it('should open the Pricechangereason search page', async (): Promise<any> => {

        expect (pricechangereasonPage.searchbtn.isPresent()).toBeTruthy();
        await homePage.ClickSearchButton();
        await browser.driver.sleep(5000); 
        await pricechangereasonPage.Viewiconclick();
        await browser.driver.sleep(5000);
        await element(by.className('closeIcon')).click();
        await pricechangereasonPage.Editiconclick();
        await homePage.saveButtonClick();
        let successmsg = await pricechangereasonPage.fxbutton;
        expect(successmsg.isPresent()).toBeTruthy(); 
        await pricechangereasonPage.successOk();
        })


    afterEach(() => {

    });
});