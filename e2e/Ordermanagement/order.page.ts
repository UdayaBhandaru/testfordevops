import { browser, element, by, $, WebElement } from 'protractor';
import { BasePage } from '../core/base.page';
import { HomePage } from '../home/home.page';
import { DropDownHelper } from "../core/DropdownHelper";
import { DateFormat } from "../core/dateFormat";


export class OrderPage extends BasePage 
{
   public orderMenu = element(by.css("a[href*='Company']"));
   public addBtn = element(by.id("searchbarAdd"));
   public fxbutton = element(by.className('fx-button'));
   public searchbtn = element(by.className('searchbtn'));
    private dropdownHelper: DropDownHelper;
    async orderPageSearch(): Promise<any>
    {
        let submit = await element(by.id('searchbarSearch'));
        return submit.click();
    }

    async OrderPageAdd(): Promise<any>
    {
        var span1 = element(by.binding('person.name'));
    }
 
    async typeOrdertype(Ordertype: string)  
    {
        let OrdertypeInput = await element(by.name('Order Type'));   
        await OrdertypeInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let OrderInput = element(by.xpath("//*[contains(text(),'REGULAR')]"));   
        await OrderInput.click();
        await browser.driver.sleep(8000);
    }
    async typeOrderstatus(status: string)
    {
        let statusInput = await element(by.name('Worksheet'));
        await statusInput.sendKeys(status);
        await browser.driver.sleep(500);
    }

    async typeorderdescription(orderdescription: string)
    {
        let orderdescriptionInput = await element(by.name('orderDesc'));
        await orderdescriptionInput.sendKeys(orderdescription);
        await browser.driver.sleep(500);
    }
    async typeSupplier(Supplier: string)  
    {
        let SupplierInput = await element(by.name('Supplier'));   
        await SupplierInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let OrderInput = element(by.xpath("//*[contains(text(),'AL-BABTAIN ELECTRONICS CO(4323)')]"));   
        await OrderInput.click();
        await browser.driver.sleep(8000);
    }
    
    async typeSupplAddressseqnumber(SupplAddressseqnumber: string)  
    {
        let SupplAddressseqnumberInput = await element(by.name('Suppl Address Seq No'));   
        await SupplAddressseqnumberInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let orderInput = element(by.xpath("//*[contains(text(),'3254')]"));   
        await orderInput.click();
        await browser.driver.sleep(8000);
    }
    
    async typeEarliestshipDate() 
    {
        let EarliestshipDateInput = await $('[ng-reflect-name="Earliest Ship Date"]');
        const Earliestshipdate = new Date();
        Earliestshipdate.setDate(Earliestshipdate.getDate() + 2);
        const dateMMddYYYY = await DateFormat.dateMMddYYYY(Earliestshipdate);
        await EarliestshipDateInput.clear();
        await EarliestshipDateInput.sendKeys(dateMMddYYYY);
        await browser.driver.sleep(800);
    }
    async typeLatestshipdate() 
    {
        let LatestshipDateInput = await $('[ng-reflect-name="Latest Ship Date"]');
        const LatestshipDate = new Date();
        LatestshipDate.setDate(LatestshipDate.getDate() + 1);
        const dateMMddYYYY = await DateFormat.dateMMddYYYY(LatestshipDate);
        await LatestshipDateInput.clear();
        await LatestshipDateInput.sendKeys(dateMMddYYYY);
        await browser.driver.sleep(800);
    }
    async typeNotbeforeDate() 
    {
        let NotbeforeDateInput = await $('[ng-reflect-name="Not Before Date"]');
        const NotbeforeDate = new Date();
        NotbeforeDate.setDate(NotbeforeDate.getDate() + 2);
        const dateMMddYYYY = await DateFormat.dateMMddYYYY(NotbeforeDate);
        await NotbeforeDateInput.clear();
        await NotbeforeDateInput.sendKeys(dateMMddYYYY);
        await browser.driver.sleep(800);
    } 
    async typeNotafterdate() 
    {
        let NotafterdateInput = await $('[ng-reflect-name="Not After Date"]');
        const Notafterdate = new Date();
        Notafterdate.setDate(Notafterdate.getDate() + 2);
        const dateMMddYYYY = await DateFormat.dateMMddYYYY(Notafterdate);
        await NotafterdateInput.clear();
        await NotafterdateInput.sendKeys(dateMMddYYYY);
        await browser.driver.sleep(800);
    }
    
    
    async typeOriginindicator(Originindicator: string)  
    {
        let OriginindicatorInput = await element(by.name('Originindicator'));   
        await OriginindicatorInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let companyInput = element(by.xpath("//*[contains(text(),'Regular')]"));   
        await companyInput.click();
        await browser.driver.sleep(8000);
    }
    async Editiconclick()
    {
        console.log("In Editiconclick");
        let Editicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'Edit')]"));

        return await Editicon.click();       
    }
    async Viewiconclick()
    {
        console.log("In Viewiconclick");
        let Viewicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'View')]"));

        return await Viewicon.click();       
    }
   

    async successOk()
    {
        let ok = await element(by.xpath('//*[contains(text(), "OK")]'));
        await ok.click();
        await browser.driver.sleep(8000);
    }
}     