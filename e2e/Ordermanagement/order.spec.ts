import { browser, ExpectedConditions as EC, $, $$, element, by } from 'protractor'
import { HomePage } from '../home/home.page'
import { LoginPage } from '../login/login.page'
import { OrderPage } from "./order.page";
import { protractor } from 'protractor/built/ptor';
declare let expect: any
describe('Order search', function () {
        let homePage: HomePage;
        let loginpage: LoginPage;
        let orderPage: OrderPage;
        beforeEach(async () => {
       
        })    
        it('should open the Order Addpage', async (): Promise<any> => {
            loginpage = new LoginPage();
            await loginpage.open();      
            await loginpage.login();  
            homePage = new HomePage();
            orderPage = new OrderPage;
            browser.waitForAngularEnabled(false);
            await browser.driver.sleep(5000);
            await homePage.OrdermenuClick();
            let Order  = await homePage.Order; 
            expect(Order.isPresent()).toBeTruthy();
            await Order.click();        
            await browser.driver.sleep(5000);
            await orderPage.typeOrdertype("REGULAR");
           
         
        })
        it('should open the Order search page', async (): Promise<any> => {
   
            await homePage.ClickSearchButton();
            await browser.driver.sleep(5000);
            await orderPage.Viewiconclick();
            await browser.driver.sleep(5000);
            await element(by.className('closeIcon')).click();
            await orderPage.Editiconclick();
            await browser.driver.sleep(5000);
            await homePage.saveButtonClick();
            let successmsg = await orderPage.fxbutton;
            expect(successmsg.isPresent()).toBeTruthy();
            await orderPage.successOk();
            await browser.driver.sleep(10);
            await element(by.className('toolbar-icons searchIcon')).click();
            
           
            })

            it('should add/edit the Order head', async (): Promise<any> =>   {

            await browser.driver.sleep(10);
            expect(orderPage.addBtn.isPresent()).toBeTruthy();
            await homePage.ClickAddButton();
            await browser.driver.sleep(5000);
            await orderPage.typeOrdertype("Regular");
            await orderPage.typeorderdescription("Order Test 02");
            await orderPage.typeSupplier("AL-BABTAIN ELECTRONICS CO(4323)");
            await orderPage.typeSupplAddressseqnumber("3254");
            await element(by.className('toolbar-icons searchIcon')).click();
            //    await orderPage.typeEarliestshipdate("");
            //     await orderPage.typeLatestshipdate("");
            //    await orderPage.typeNotbeforedate("");
            //    await orderPage.typeNotafterdate("");
            // await orderPage.typeOriginindicator("Regular");
            // await homePage.saveButtonClick();
            // await browser.driver.sleep(5000);
            // await orderPage.successOk()
          
           
      })


        afterEach(() => {


        })

});