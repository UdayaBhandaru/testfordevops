import { browser, ExpectedConditions as EC, $, $$, element, by } from 'protractor'
import { HomePage } from '../home/home.page'
import { LoginPage } from '../login/login.page'
import { PricemanagementPage } from "./Pricemanagement.page";
import { protractor } from 'protractor/built/ptor';
declare let expect: any


describe('Pricemanagement search', function () {
    let homePage: HomePage;
    let loginpage: LoginPage;
    let pricemanagementPage: PricemanagementPage;
    beforeEach(async () => {
       
       
    })    

    it('should open the Pricemanagement searchpage', async (): Promise<any> => {
        loginpage = new LoginPage();
        await loginpage.open();      
        await loginpage.login();  
        homePage = new HomePage();
        pricemanagementPage = new PricemanagementPage();
        browser.waitForAngularEnabled(false);
        await browser.driver.sleep(500);
        let pricemanagement  = await homePage.Pricemanagement; 
        expect(pricemanagement.isPresent()).toBeTruthy();
        await pricemanagement.click();       
        await browser.driver.sleep(500);
       
      
     }) 
     it('should open the Pricemanagement add page', async (): Promise<any> => {
        
        await pricemanagementPage.typeStatus("Worksheet");
        expect (pricemanagementPage.searchbtn.isPresent()).toBeTruthy();
        await homePage.ClickSearchButton();
        await browser.driver.sleep(5000);
        await pricemanagementPage.Viewiconclick();
        await browser.driver.sleep(5000);
        await element(by.className('closeIcon')).click();
        await pricemanagementPage.Editiconclick();
        await browser.driver.sleep(5000);
        
    })
    it('should Header page for Pricemanagement', async (): Promise<any> =>  {
       await homePage.saveButtonClick();
       let successmsg = await pricemanagementPage.fxbutton;
       expect(successmsg.isPresent()).toBeTruthy();
       await pricemanagementPage.successOk();
       await pricemanagementPage.ClickHeaderPage();
       await homePage.saveButtonClick();
       let successmsgalert = await pricemanagementPage.fxbutton;
       expect(successmsgalert.isPresent()).toBeTruthy();
       await pricemanagementPage.successOk();
       await browser.driver.sleep(5000);
       await element(by.className('toolbar-icons searchIcon')).click();

     })
    afterEach(() => {

    });
});