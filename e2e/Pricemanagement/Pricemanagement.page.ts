import { browser, element, by, $, WebElement } from 'protractor';
import { BasePage } from '../core/base.page';
import { HomePage } from '../home/home.page';
import { DropDownHelper } from "../core/DropdownHelper";
import { DateFormat } from "../core/dateFormat";

export class PricemanagementPage extends BasePage 
{
   public PricemanagementMenu = element(by.css("a[href*='CostChange']"));
   public addBtn = element(by.id("searchbarAdd"));
   public fxbutton = element(by.className('fx-button'));
   public searchbtn = element(by.className('searchbtn'));
    private dropdownHelper: DropDownHelper;
    async PricemanagementPageSearch(): Promise<any>
    {
        let submit = await element(by.id('searchbarSearch'));
        return submit.click();
    }

    async PricemanagementPageAdd(): Promise<any>
    {
        var span1 = element(by.binding('person.name'));
    }

    async typePricechangedescription(Pricechangedescription: string)
    {
        let pricechangedescriptionInput = await element(by.name('costChangeDesc'));      
        await pricechangedescriptionInput.sendKeys(Pricechangedescription);
        await browser.driver.sleep(500);
    }
     
    async typeStatus(Status: string)  
    {
        let StatusInput = await element(by.name('Status'));   
        await StatusInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let PricemanagementInput = element(by.xpath("//*[contains(text(),'Worksheet')]"));   
        await PricemanagementInput.click();
        await browser.driver.sleep(800);
    }

    async typeReason(Reason: string)  
    {
        let ReasonInput = await element(by.name('Reason'));   
        await ReasonInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let PricemanagementInput = element(by.xpath("//*[contains(text(),'PRICE CHANGE TEST')]"));   
        await PricemanagementInput.click();
        await browser.driver.sleep(800);
    }
    async typePricechangeorigin(Costchangeorigin: string)  
    {
        let PricechangeoriginInput = await element(by.name('Price Change Origin'));   
        await PricechangeoriginInput.click();    
        this.dropdownHelper = new DropDownHelper();
        let PricemanagementInput = element(by.xpath("//*[contains(text(),'PRICE CHANGE ORIGIN2')]"));   
        await PricemanagementInput.click();
        await browser.driver.sleep(800);
    }
     
    async Editiconclick()
    {
        console.log("In Editiconclick");
        let Editicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'Edit')]"));

        return await Editicon.click();       
    }
    async Viewiconclick()
    {
        console.log("In Viewiconclick");
        let Viewicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'View')]"));

        return await Viewicon.click();       
    }
   
    async successOk()
    {
        let ok = await element(by.xpath('//*[contains(text(), "OK")]'));
        await ok.click();
        await browser.driver.sleep(8000);
    }
    async ClickHeaderPage(): Promise<any> 
    {
        console.log("In  ClickHeaderPage");
        let  ClickHeaderPage = element(by.css("a[href*='/PriceChange/New/PriceChangeDetails/823']"));
        return await  ClickHeaderPage.click();
    }
}