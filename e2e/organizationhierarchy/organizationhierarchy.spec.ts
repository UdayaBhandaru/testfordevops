import { browser, ExpectedConditions as EC, $, $$, element, by } from 'protractor'
import { HomePage } from '../home/home.page'
import { LoginPage } from '../login/login.page'
import { OrganizationhierarchyPage } from "./organizationhierarchy.page";


import { protractor } from 'protractor/built/ptor';
import { CompanyPage } from '../companymaster/Company.page';
import { RegionPage } from '../Regionmaster/region.page';
import { FormatPage } from '../Formatmaster/format.page';
declare let expect: any


describe('organizationhierarchy search', function () {
    let homePage: HomePage;
    let loginpage: LoginPage;
    let organizationhierarchyPage: OrganizationhierarchyPage;
    let companyPage: CompanyPage;
    let regionPage : RegionPage;
    let formatPage : FormatPage;
    
    beforeEach(async () => {
       
       
    })   

    it('should open the organizationhierarchypage', async (): Promise<any> => {
        loginpage = new LoginPage();
        await loginpage.open();      
        await loginpage.login();  
        homePage = new HomePage();
        companyPage = new CompanyPage();        
        await browser.driver.sleep(5000);
        browser.waitForAngularEnabled(false);
        await homePage.mdmmenuClick();
        let organization  = await homePage.orgHierarchy;       
        expect(organization.isPresent()).toBeTruthy();
        await organization.click();       
        await companyPage.companyMenu.click();       
        // await homePage.ClickAddButton();
        // await browser.driver.sleep(5000);
        // await element(by.className('toolbar-icons saveIcon')).click();
        // await browser.driver.sleep(5000);
        // await element(by.className('closeIcon')).click();
        await companyPage.typeCompanyId("4213");
        await homePage.ClickSearchButton();
        await browser.driver.sleep(5000);
        expect(companyPage.noSearchIcon.isPresent()).toBeTruthy();
        await companyPage.backbtn.click();
       // await element(by.className('exportIcon')).click();
        // regionPage = new RegionPage();       
        // await regionPage.regionMenu.click();
        // await browser.driver.sleep(5000);
        // await homePage.ClickAddButton();
        // await browser.driver.sleep(5000);
        // await element(by.className('toolbar-icons saveIcon')).click();
        // await browser.driver.sleep(5000);
        // await element(by.className('toolbar-icons resetIcon')).click();
        // await browser.driver.sleep(5000);
        // await element(by.className('toolbar-icons searchIcon')).click();
        
        // await browser.driver.sleep(5000);
        // formatPage = new FormatPage();       
        // await formatPage.formatMenu.click();
        // await browser.driver.sleep(5000);
        // await homePage.ClickAddButton();
        // await browser.driver.sleep(5000);
        // await element(by.className('toolbar-icons saveIcon')).click();
        // await browser.driver.sleep(5000);
        // await element(by.className('closeIcon')).click();
         await browser.driver.sleep(5000);
        
       
    }) 


        afterEach(() => {

        });
    });