import { browser, element, by, $, WebElement } from 'protractor';
import { BasePage } from '../core/base.page';
import { HomePage } from '../home/home.page';
import { DropDownHelper } from "../core/DropdownHelper";
import { DateFormat } from "../core/dateFormat";

export class OrganizationhierarchyPage extends BasePage 
{
   public companyMenu = element(by.css("a[href*='Company']"));
   public regionMenu = element(by.css("a[href*='Region']"));
   public formatMenu = element(by.css("a[href*='Format']"));
   public addBtn = element(by.id("searchbarAdd"));
   public searchBtn = element(by.id("searchbarSearch"));

    //public searchButton = element(by.name('search'));//
    private dropdownHelper: DropDownHelper;
    
    async typeCompanyId(CompanyId: string)
    {
        let companyIdInput = await element(by.name('companyId'));      
        await companyIdInput.sendKeys(CompanyId);
        await browser.driver.sleep(5000);
    }

    async Editiconclick()
    {
        console.log("In Editiconclick");
        let Editicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'Edit')]"));

        return await Editicon.click();       
    }
    async Viewiconclick()
    {
        console.log("In Viewiconclick");
        let Viewicon = await element(by.xpath("//*[contains(@class, 'ag-body-container')]//*[contains(@role,'row')][1]//*[contains(@col-id, 'Actions')]//*[contains(@mattooltip, 'View')]"));

        return await Viewicon.click();       
    }
   

    async successOk()
    {
        let ok = await $('.fx-button');
        return ok.click();
    }
}