﻿// <copyright file="RbsCollectionFixture.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>
namespace Agility.RBS.Tests
{
    using Agility.Framework.Test;
    using Xunit;

    [CollectionDefinition("RbsCollection")]
    public class RbsCollectionFixture : ICollectionFixture<FxFixture>
    {
        // This class has no code, and is never created. Its purpose is simply
        // to be the place to apply [CollectionDefinition] and all the
        // ICollectionFixture<> interfaces.
    }
}
