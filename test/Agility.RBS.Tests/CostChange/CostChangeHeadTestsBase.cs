﻿// <copyright file="CostChangeHeadTestsBase.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.CostChange
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Core;
    using Agility.Framework.Designer.Mapping;
    using Agility.Framework.Test;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.Framework.Web.Security.Mapping;
    using Agility.RBS.Core;
    using Agility.RBS.CostManagement;
    using Agility.RBS.CostManagement.CostChange;
    using Agility.RBS.CostManagement.CostChange.Mapping;
    using Agility.RBS.CostManagement.CostChange.Models;
    using Agility.RBS.Inbox.Models;
    using Agility.RBS.Inbox.Repository;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Models;
    using Agility.RBS.MDM.UtilityDomain;
    using Agility.RBS.MDM.UtilityDomain.Models;
    using AutoMapper;
    using Devart.Data.Oracle;
    using Moq;

    public class CostChangeHeadTestsBase : RbsFixture
    {
        public CostChangeHeadTestsBase(FxFixture fxFixture)
            : base(fxFixture)
        {
            Mock<UtilityDomainRepository> utilityDomainRepository = this.MockBaseOraclePackageRepository<UtilityDomainRepository, DomainDetailModel>(this.ItemTestDataModel.DomainDetails, new object[] { });
            Mock<DomainDataRepository> domainDataRepository = this.MockBaseOraclePackageRepository<DomainDataRepository, CostChangeReasonDomainModel>(this.ItemTestDataModel.CostChangeReasonDomains, new object[] { utilityDomainRepository.Object, null });
            domainDataRepository.Setup(x => x.GetProcedure2<InboxCommonModel>(It.IsAny<OracleObject>(), It.IsAny<PackageParams>(), It.IsAny<ServiceDocumentResult>(), It.IsAny<OracleParameterCollection>()))
               .Returns(Task.FromResult(this.ItemTestDataModel.InboxCommons));

            Mock<CostChangeRepository> itemRepository = this.MockBaseOraclePackageRepository<CostChangeRepository, CostChangeHeadModel>(this.ItemTestDataModel.CostChangeHeads, new object[] { });
            itemRepository.Setup(x => x.SetParamsCostChangeHead(It.IsAny<CostChangeHeadModel>()))
               .Verifiable();
            itemRepository.Setup(x => x.SetCostChangeRequestId(It.IsAny<int>(), It.IsAny<OracleObject>()))
               .Verifiable();

            this.MockBaseOraclePackageRepository<InboxRepository, InboxModel>(this.ItemTestDataModel.Inboxs, new object[] { null });

            this.Controller = this.Setup<CostChangeHeadController>();
        }

        protected CostChangeHeadController Controller { get; set; }

        protected ServiceDocument<CostChangeHeadModel> ServiceDocument { get; set; }

        protected CostTestDataModel ItemTestDataModel { get; set; }

        public override void ConfigureServices(string controllerName)
        {
            base.ConfigureServices("ItemFormat");
            this.CreateTestData(new List<string>(), new List<string> { "Security.json" });
            this.ItemTestDataModel = this.CreateTestData<CostTestDataModel>(new List<string> { "Cost.json" }, "CostChange");
        }

        protected override void ComponentStartups(List<ComponentStartup> startups)
        {
            base.ComponentStartups(startups);
            startups.Add(new CostManagementStartup());
        }

        protected override void InitializeMappings()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<FxMappingProfile>();
                cfg.AddProfile<SecurityMappingProfile>();

                cfg.AddProfile<CostChangeModelMapping>();
                this.ConfigureMapping(cfg);
            });
        }
    }
}
