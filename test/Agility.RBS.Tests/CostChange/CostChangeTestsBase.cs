﻿// <copyright file="CostChangeTestsBase.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.CostChange
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Core;
    using Agility.Framework.Designer.Mapping;
    using Agility.Framework.Test;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.Framework.Web.Security.Mapping;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Models;
    using Agility.RBS.CostManagement;
    using Agility.RBS.CostManagement.CostChange;
    using Agility.RBS.CostManagement.CostChange.Mapping;
    using Agility.RBS.CostManagement.CostChange.Models;
    using Agility.RBS.ItemManagement.Item.Models;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Models;
    using Agility.RBS.MDM.UtilityDomain;
    using Agility.RBS.MDM.UtilityDomain.Models;
    using AutoMapper;
    using Devart.Data.Oracle;
    using Moq;

    public class CostChangeTestsBase : RbsFixture
    {
        public CostChangeTestsBase(FxFixture fxFixture)
            : base(fxFixture)
        {
            Mock<UtilityDomainRepository> utilityDomainRepository = this.MockBaseOraclePackageRepository<UtilityDomainRepository, DomainDetailModel>(this.ItemTestDataModel.DomainDetails, new object[] { });
            Mock<DomainDataRepository> domainDataRepository = this.MockBaseOraclePackageRepository<DomainDataRepository, SupplierDomainModel>(this.ItemTestDataModel.SupplierDomains, new object[] { utilityDomainRepository.Object, null });
            domainDataRepository.Setup(x => x.GetProcedure2<CommonModel>(It.IsAny<OracleObject>(), It.IsAny<PackageParams>(), It.IsAny<ServiceDocumentResult>(), It.IsAny<OracleParameterCollection>()))
                .Returns(Task.FromResult(this.ItemTestDataModel.CommonModels));
            domainDataRepository.Setup(x => x.GetProcedure2<UsersDomainModel>(It.IsAny<OracleObject>(), It.IsAny<PackageParams>(), It.IsAny<ServiceDocumentResult>(), It.IsAny<OracleParameterCollection>()))
                .Returns(Task.FromResult(this.ItemTestDataModel.UsersDomains));
            domainDataRepository.Setup(x => x.GetProcedure2<WorkflowStatusDomainModel>(It.IsAny<OracleObject>(), It.IsAny<PackageParams>(), It.IsAny<ServiceDocumentResult>(), It.IsAny<OracleParameterCollection>()))
                .Returns(Task.FromResult(this.ItemTestDataModel.WorkflowStatusDomains));

            Mock<CostChangeRepository> itemRepository = this.MockBaseOraclePackageRepository<CostChangeRepository, CostChangeSearchModel>(this.ItemTestDataModel.CostChangeSearchs, new object[] { });
            itemRepository.Setup(x => x.SetParamsCostChangeRequest(It.IsAny<CostChangeSearchModel>()))
               .Verifiable();
            this.Controller = this.Setup<CostChangeController>();
        }

        protected CostChangeController Controller { get; set; }

        protected ServiceDocument<CostChangeSearchModel> ServiceDocument { get; set; }

        protected CostTestDataModel ItemTestDataModel { get; set; }

        public override void ConfigureServices(string controllerName)
        {
            base.ConfigureServices("ItemFormat");
            this.CreateTestData(new List<string>(), new List<string> { "Security.json" });
            this.ItemTestDataModel = this.CreateTestData<CostTestDataModel>(new List<string> { "Cost.json" }, "CostChange");
        }

        protected override void ComponentStartups(List<ComponentStartup> startups)
        {
            base.ComponentStartups(startups);
            startups.Add(new CostManagementStartup());
        }

        protected override void InitializeMappings()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<FxMappingProfile>();
                cfg.AddProfile<SecurityMappingProfile>();

                cfg.AddProfile<CostChangeModelMapping>();
                this.ConfigureMapping(cfg);
            });
        }
    }
}
