﻿// <copyright file="CostChangeHeadTest.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.CostChange
{
    using System;
    using System.Threading.Tasks;
    using Agility.Framework.Test;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core.Models;
    using Agility.RBS.CostManagement.CostChange.Models;
    using Agility.RBS.Inbox.Models;
    using Agility.RBS.ItemManagement.Item.Models;
    using Xunit;

    [Collection("RbsCollection")]
    public class CostChangeHeadTest : CostChangeHeadTestsBase
    {
        public CostChangeHeadTest(FxFixture fxFixture)
            : base(fxFixture)
        {
        }

        [Fact]
        public async Task CostChangeHeadNew()
        {
            this.ServiceDocument = new ServiceDocument<CostChangeHeadModel>();
            this.ServiceDocument.New(false);
            this.ServiceDocument = await this.Controller.New();
            Assert.True(this.ServiceDocument.DomainData.Count == 2);
        }

        [Fact]
        public async Task CostChangeHeadOpen()
        {
            this.ServiceDocument = await this.Controller.Open(1);
            Assert.True(this.ServiceDocument.Result.Type == MessageType.Success);
        }

        [Fact]
        public async Task CostChangeHeadSave()
        {
            this.ServiceDocument = await this.Controller.New();
            this.ServiceDocument.DataProfile.DataModel = new CostChangeHeadModel
            {
                CostChangeStatus = "A"
            };
            this.ServiceDocument = await this.Controller.Save(this.ServiceDocument);
            Assert.True(this.ServiceDocument.Result.Type == MessageType.Success);
        }

        [Fact]
        public async Task CostChangeHeadSubmit()
        {
            this.ServiceDocument = await this.Controller.New();
            this.ServiceDocument.DataProfile.DataModel = new CostChangeHeadModel
            {
                CostChangeStatus = "A",
                WorkflowInstance = { WorkflowStateId = Guid.NewGuid() }
            };
            this.ServiceDocument = await this.Controller.Submit(this.ServiceDocument);
            Assert.True(this.ServiceDocument.Result.Type == MessageType.Success);
        }

        [Fact]
        public async Task CostChangeHeadReject()
        {
            this.ServiceDocument = await this.Controller.New();
            var stateId = Convert.ToString(Guid.NewGuid());
            this.ServiceDocument.DataProfile.DataModel = new CostChangeHeadModel
            {
                CostChangeStatus = "A",
                WorkflowStateId = Guid.NewGuid(),
                WorkflowForm = new RbWfHeaderModel { StateId = stateId },
                WorkflowInstance = { WorkflowStateId = Guid.NewGuid() },
            };
            this.ServiceDocument = await this.Controller.Reject(this.ServiceDocument);
            Assert.True(this.ServiceDocument.Result.Type == MessageType.Success);
        }

        [Fact]
        public async Task CostChangeHeadDelete()
        {
            this.ServiceDocument = await this.Controller.New();
            this.ServiceDocument.DataProfile.DataModel = new CostChangeHeadModel
            {
                CostChangeStatus = "A",
            };
            this.ServiceDocument = await this.Controller.Delete();
            Assert.True(this.ServiceDocument.Result.Type == MessageType.Success);
        }
    }
}
