﻿// <copyright file="CostChangeTest.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.CostChange
{
    using System.Threading.Tasks;
    using Agility.Framework.Test;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.CostManagement.CostChange.Models;
    using Agility.RBS.ItemManagement.Item.Models;
    using Xunit;

    [Collection("RbsCollection")]
    public class CostChangeTest : CostChangeTestsBase
    {
        public CostChangeTest(FxFixture fxFixture)
            : base(fxFixture)
        {
        }

        [Fact]
        public async Task CostChangeList()
        {
            this.ServiceDocument = await this.Controller.List();
            Assert.True(this.ServiceDocument.DomainData.Count == 7);
        }

        [Fact]
        public async Task CostChangeSearch()
        {
            this.ServiceDocument = await this.Controller.List();
            this.ServiceDocument.DataProfile.DataModel = new CostChangeSearchModel
            {
                StartRow = 1,
                EndRow = 10
            };
            this.ServiceDocument = await this.Controller.Search();
            Assert.True(this.ServiceDocument.Result.Type == MessageType.Success);
        }
    }
}
