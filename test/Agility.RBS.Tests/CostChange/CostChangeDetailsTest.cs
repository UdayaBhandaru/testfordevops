﻿// <copyright file="CostChangeDetailsTest.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.CostChange
{
    using System.Threading.Tasks;
    using Agility.Framework.Test;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.CostManagement.CostChange.Models;
    using Agility.RBS.ItemManagement.Item.Models;
    using Xunit;

    [Collection("RbsCollection")]
    public class CostChangeDetailsTest : CostChangeDetailsTestsBase
    {
        public CostChangeDetailsTest(FxFixture fxFixture)
            : base(fxFixture)
        {
        }

        [Fact]
        public async Task CostChangeDetailsList()
        {
            this.ServiceDocument = await this.Controller.List(1);
            Assert.True(this.ServiceDocument.DomainData.Count == 7);
        }

        [Fact]
        public async Task CostChangeDetailsSave()
        {
            this.ServiceDocument = await this.Controller.Save();
            Assert.True(this.ServiceDocument.Result.Type == MessageType.Success);
        }

        [Fact]
        public async Task CostChangeDetailsSearch()
        {
            this.ServiceDocument = await this.Controller.List(1);
            this.ServiceDocument.DataProfile.DataModel = new CostChangeDetailModel
            {
                Item = "100002574",
                OrgLevel = "LOC",
                OrgLevelValue = 1,
                SupplierId = 1,
                LineStatus = "A"
            };
            this.ServiceDocument = await this.Controller.Search();
            Assert.True(this.ServiceDocument.Result.Type == MessageType.Success);
        }
    }
}
