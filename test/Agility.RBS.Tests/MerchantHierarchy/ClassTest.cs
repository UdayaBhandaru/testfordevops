﻿// <copyright file="ClassTest.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.MerchantHierarchy
{
    using System.Threading.Tasks;
    using Agility.Framework.Test;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.MerchantHierarchy.Models;
    using Xunit;

    [Collection("RbsCollection")]
    public class ClassTest : ClassTestsBase
    {
        public ClassTest(FxFixture fxFixture)
            : base(fxFixture)
        {
        }

        [Fact]
        public async Task ClassList()
        {
            ServiceDocument<ClassModel> serviceDocument = await this.Controller.List();
            Assert.True(serviceDocument.DomainData.Count == 5);
        }

        [Fact]
        public async Task ClassNew()
        {
            ServiceDocument<ClassModel> serviceDocument = await this.Controller.New();
            Assert.True(serviceDocument.DomainData.Count == 6);
        }

        [Fact]
        public async Task ClassSearch()
        {
            ServiceDocument<ClassModel> serviceDocument = await this.Controller.New();
            Assert.True(serviceDocument.DomainData.Count == 6);
            Assert.True(serviceDocument.LocalizationData.Count == 1);
            serviceDocument.DataProfile.DataModel = new ClassModel
            {
                ClassDesc = "COLOURS & BEAUTY MAKE UP,,,,"
            };
            serviceDocument = await this.Controller.Search();
            Assert.True(serviceDocument.DataProfile.DataList.Count == 2);
        }

        [Fact]
        public async Task ClassSave()
        {
            ServiceDocument<ClassModel> serviceDocument = await this.Controller.Save();
            Assert.True(serviceDocument.Result.Type == MessageType.Success);
        }
    }
}
