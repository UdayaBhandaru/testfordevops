﻿// <copyright file="CategoryRoleTestsBase.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.MerchantHierarchy
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.IO;
    using System.Threading.Tasks;
    using Agility.Framework.Core;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Designer.Mapping;
    using Agility.Framework.Test;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.Framework.Web.Security.Mapping;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Models;
    using Agility.RBS.MDM;
    using Agility.RBS.MDM.Brand.Mapping;
    using Agility.RBS.MDM.CostZone.Mapping;
    using Agility.RBS.MDM.Favourite.Mapping;
    using Agility.RBS.MDM.ItemList.Mapping;
    using Agility.RBS.MDM.MerchantHierarchy;
    using Agility.RBS.MDM.MerchantHierarchy.Mapping;
    using Agility.RBS.MDM.MerchantHierarchy.Models;
    using Agility.RBS.MDM.OrganizationHierarchy.Mapping;
    using Agility.RBS.MDM.Partner.Mapping;
    using Agility.RBS.MDM.PriceZone.Mapping;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Mapping;
    using Agility.RBS.MDM.Repositories.Models;
    using Agility.RBS.MDM.Season.Mapping;
    using Agility.RBS.MDM.UnitOfMeasure;
    using Agility.RBS.MDM.UnitOfMeasure.Mapping;
    using Agility.RBS.MDM.UnitOfMeasure.Models;
    using Agility.RBS.MDM.UtilityDomain;
    using Agility.RBS.MDM.UtilityDomain.Mapping;
    using Agility.RBS.MDM.UtilityDomain.Models;
    using Agility.RBS.MDM.ValueAddedTax.Mapping;
    using Agility.RBS.SupplierManagement.Supplier.Mapping;
    using Agility.RBS.SupplierManagement.SupplierAddress.Mapping;
    using AutoMapper;
    using Devart.Data.Oracle;
    using Microsoft.Extensions.Caching.Distributed;
    using Microsoft.Extensions.DependencyInjection;
    using Moq;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using Xunit;

    public class CategoryRoleTestsBase : RbsFixture
    {
        public CategoryRoleTestsBase(FxFixture fxFixture)
            : base(fxFixture)
        {
            Mock<UtilityDomainRepository> utilityDomainRepository = this.MockBaseOraclePackageRepository<UtilityDomainRepository, DomainDetailModel>(this.CategoryRoleTestDataModel.DomainDetailModels, new object[] { });
            Mock<DomainDataRepository> domainDataRepository = this.MockBaseOraclePackageRepository<DomainDataRepository, DivisionDomainModel>(this.CategoryRoleTestDataModel.DivisionDomainModels, new object[] { utilityDomainRepository.Object, null });
            domainDataRepository.Setup(x => x.GetProcedure2<DepartmentDomainModel>(It.IsAny<OracleObject>(), It.IsAny<PackageParams>(), It.IsAny<ServiceDocumentResult>(), It.IsAny<OracleParameterCollection>()))
.Returns(Task.FromResult(this.CategoryRoleTestDataModel.DepartmentDomainModels));
            domainDataRepository.Setup(x => x.GetProcedure2<DepartmentDomainModel>(It.IsAny<OracleObject>(), It.IsAny<PackageParams>(), It.IsAny<ServiceDocumentResult>(), It.IsAny<OracleParameterCollection>()))
.Returns(Task.FromResult(this.CategoryRoleTestDataModel.CategoryDomainModels));
            domainDataRepository.Setup(x => x.GetProcedure2<CompanyDomainModel>(It.IsAny<OracleObject>(), It.IsAny<PackageParams>(), It.IsAny<ServiceDocumentResult>(), It.IsAny<OracleParameterCollection>()))
               .Returns(Task.FromResult(this.CategoryRoleTestDataModel.CompanyDomainModels));
            domainDataRepository.Setup(x => x.GetProcedure2<CommonModel>(It.IsAny<OracleObject>(), It.IsAny<PackageParams>(), It.IsAny<ServiceDocumentResult>(), It.IsAny<OracleParameterCollection>()))
          .Returns(Task.FromResult(this.CategoryRoleTestDataModel.CommonModels));
            Mock<MerchantHierarchyRepository> merchantHierarchyRepository = this.MockBaseOraclePackageRepository<MerchantHierarchyRepository, CategoryRoleModel>(this.CategoryRoleTestDataModel.CategoryRoleModels, new object[] { null });
            merchantHierarchyRepository.Setup(x => x.SetParamsCategoryRoleSearch(It.IsAny<CategoryRoleModel>(), It.IsAny<OracleObject>()))
                  .Verifiable();
            merchantHierarchyRepository.Setup(x => x.SetParamsCategoryRoleSave(It.IsAny<CategoryRoleModel>(), It.IsAny<OracleObject>()))
               .Verifiable();
            this.Controller = this.Setup<CategoryRoleController>();
        }

        protected CategoryRoleController Controller { get; set; }

        protected ServiceDocument<CategoryRoleModel> ServiceDocument { get; set; }

        protected CategoryRoleTestDataModel CategoryRoleTestDataModel { get; set; }

        public override void ConfigureServices(string controllerName)
        {
            base.ConfigureServices("CategoryRole");
            this.CreateTestData(new List<string>(), new List<string> { "Security.json" });

            this.CategoryRoleTestDataModel = this.CreateTestData<CategoryRoleTestDataModel>(new List<string> { "CategoryRole.json" }, "MerchantHierarchy");

            base.MockFxDbContext<CategoryRoleModel>("Journey");
        }

        protected override void ComponentStartups(List<ComponentStartup> startups)
        {
            base.ComponentStartups(startups);
            startups.Add(new MdmStartup());
        }

        protected override void InitializeMappings()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<FxMappingProfile>();
                cfg.AddProfile<SecurityMappingProfile>();
                cfg.AddProfile<UnitOfMeasureMapping>();
                cfg.AddProfile<UtilityDomainMapping>();
                cfg.AddProfile<MerchantHierarchyMapping>();
                cfg.AddProfile<OrganizationHierarchyMapping>();
                cfg.AddProfile<ValueAddedTaxMapping>();
                cfg.AddProfile<SeasonMapping>();
                cfg.AddProfile<DomainDataMapping>();
                cfg.AddProfile<SupplierModelMapping>();
                cfg.AddProfile<SupplierAddressModelMapping>();
                cfg.AddProfile<FavouriteMapping>();
                cfg.AddProfile<ItemListMapping>();
                cfg.AddProfile<CostZoneMapping>();
                cfg.AddProfile<PriceZoneMapping>();
                cfg.AddProfile<BrandMapping>();
                cfg.AddProfile<PartnerMapping>();
                this.ConfigureMapping(cfg);
            });
        }
    }
}
