﻿// <copyright file="MerchantSpaceTestDataModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.MerchantHierarchy
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.RBS.Core.Models;
    using Agility.RBS.MDM.MerchantHierarchy.Models;
    using Agility.RBS.MDM.Repositories.Models;
    using Agility.RBS.MDM.UnitOfMeasure.Models;
    using Agility.RBS.MDM.UtilityDomain.Models;

    public class MerchantSpaceTestDataModel
    {
        public List<DomainDetailModel> DomainDetailModels { get; set; }

        public List<DivisionDomainModel> DivisionDomainModels { get; set; }

        public List<DepartmentDomainModel> DepartmentDomainModels { get; set; }

        public List<DepartmentDomainModel> CategoryDomainModels { get; set; }

        public List<MerchantSpaceModel> MerchantSpaceModels { get; set; }

        public List<CompanyDomainModel> CompanyDomainModels { get; set; }

        public List<CategoryRoleModel> CategoryRoleModels { get; set; }

        public List<CommonModel> CommonModels { get; set; }
    }
}
