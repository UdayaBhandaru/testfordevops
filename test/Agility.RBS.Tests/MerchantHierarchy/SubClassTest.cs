﻿// <copyright file="SubClassTest.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.MerchantHierarchy
{
    using System.Threading.Tasks;
    using Agility.Framework.Test;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.MerchantHierarchy.Models;
    using Xunit;

    [Collection("RbsCollection")]
    public class SubClassTest : SubClassTestsBase
    {
        public SubClassTest(FxFixture fxFixture)
            : base(fxFixture)
        {
        }

        [Fact]
        public async Task SubClassList()
        {
            ServiceDocument<SubClassModel> serviceDocument = await this.Controller.List();
            Assert.True(serviceDocument.DomainData.Count == 6);
        }

        [Fact]
        public async Task SubClassNew()
        {
            ServiceDocument<SubClassModel> serviceDocument = await this.Controller.New();
            Assert.True(serviceDocument.DomainData.Count == 7);
        }

        [Fact]
        public async Task SubClassSearch()
        {
            ServiceDocument<SubClassModel> serviceDocument = await this.Controller.New();
            Assert.True(serviceDocument.DomainData.Count == 7);
            Assert.True(serviceDocument.LocalizationData.Count == 1);
            serviceDocument.DataProfile.DataModel = new SubClassModel
            {
                SubClassDesc = "SUGAR AND CREAM DISPENSERS.."
            };
            serviceDocument = await this.Controller.Search();
            Assert.True(serviceDocument.DataProfile.DataList.Count == 2);
        }

        [Fact]
        public async Task SubClassSave()
        {
            ServiceDocument<SubClassModel> serviceDocument = await this.Controller.Save();
            Assert.True(serviceDocument.Result.Type == MessageType.Success);
        }
    }
}
