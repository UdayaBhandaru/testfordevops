﻿// <copyright file="MerchantSpaceTest.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.MerchantHierarchy
{
    using System.Threading.Tasks;
    using Agility.Framework.Test;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.MerchantHierarchy.Models;
    using Xunit;

    [Collection("RbsCollection")]
    public class MerchantSpaceTest : MerchantSpaceTestsBase
    {
        public MerchantSpaceTest(FxFixture fxFixture)
            : base(fxFixture)
        {
        }

        [Fact]
        public async Task MerchantSpaceList()
        {
            ServiceDocument<MerchantSpaceModel> serviceDocument = await this.Controller.List();
            Assert.True(serviceDocument.DomainData.Count == 9);
        }

        [Fact]
        public async Task MerchantSpaceNew()
        {
            ServiceDocument<MerchantSpaceModel> serviceDocument = await this.Controller.New();
            Assert.True(serviceDocument.DomainData.Count == 11);
        }

        [Fact]
        public async Task MerchantSpaceSearch()
        {
            ServiceDocument<MerchantSpaceModel> serviceDocument = await this.Controller.New();
            Assert.True(serviceDocument.DomainData.Count == 11);
            Assert.True(serviceDocument.LocalizationData.Count == 1);
            serviceDocument.DataProfile.DataModel = new MerchantSpaceModel
            {
                CompanyName = "TSC KUWAIT"
            };
            serviceDocument = await this.Controller.Search();
            Assert.True(serviceDocument.DataProfile.DataList.Count == 2);
        }

        [Fact]
        public async Task MerchantSpaceSave()
        {
            ServiceDocument<MerchantSpaceModel> serviceDocument = await this.Controller.Save();
            Assert.True(serviceDocument.Result.Type == MessageType.Success);
        }
    }
}
