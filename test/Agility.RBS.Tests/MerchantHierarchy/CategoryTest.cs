﻿// <copyright file="CategoryTest.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.MerchantHierarchy
{
    using System.Threading.Tasks;
    using Agility.Framework.Test;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.MerchantHierarchy.Models;
    using Xunit;

    [Collection("RbsCollection")]
    public class CategoryTest : CategoryTestsBase
    {
        public CategoryTest(FxFixture fxFixture)
            : base(fxFixture)
        {
        }

        [Fact]
        public async Task CategoryList()
        {
            ServiceDocument<DepartmentModel> serviceDocument = await this.Controller.List();
            Assert.True(serviceDocument.DomainData.Count == 4);
        }

        [Fact]
        public async Task CategoryNew()
        {
            ServiceDocument<DepartmentModel> serviceDocument = await this.Controller.New();
            Assert.True(serviceDocument.DomainData.Count == 7);
        }

        [Fact]
        public async Task CategorySearch()
        {
            ServiceDocument<DepartmentModel> serviceDocument = await this.Controller.New();
            Assert.True(serviceDocument.DomainData.Count == 7);
            Assert.True(serviceDocument.LocalizationData.Count == 1);
            serviceDocument.DataProfile.DataModel = new DepartmentModel
            {
                CategoryDesc = "PRINTED GOODS2"
            };
            serviceDocument = await this.Controller.Search();
            Assert.True(serviceDocument.DataProfile.DataList.Count == 2);
        }

        [Fact]
        public async Task CategorySave()
        {
            ServiceDocument<DepartmentModel> serviceDocument = await this.Controller.Save();
            Assert.True(serviceDocument.Result.Type == MessageType.Success);
        }
    }
}
