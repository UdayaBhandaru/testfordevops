﻿// <copyright file="CategoryRoleTest.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.MerchantHierarchy
{
    using System.Threading.Tasks;
    using Agility.Framework.Test;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.MerchantHierarchy.Models;
    using Xunit;

    [Collection("RbsCollection")]
    public class CategoryRoleTest : CategoryRoleTestsBase
    {
        public CategoryRoleTest(FxFixture fxFixture)
            : base(fxFixture)
        {
        }

        [Fact]
        public async Task CategoryRoleList()
        {
            ServiceDocument<CategoryRoleModel> serviceDocument = await this.Controller.List();
            Assert.True(serviceDocument.DomainData.Count == 9);
        }

        [Fact]
        public async Task CategoryRoleNew()
        {
            ServiceDocument<CategoryRoleModel> serviceDocument = await this.Controller.New();
            Assert.True(serviceDocument.DomainData.Count == 11);
        }

        [Fact]
        public async Task CategoryRoleSearch()
        {
            ServiceDocument<CategoryRoleModel> serviceDocument = await this.Controller.New();
            Assert.True(serviceDocument.DomainData.Count == 11);
            Assert.True(serviceDocument.LocalizationData.Count == 1);
            serviceDocument.DataProfile.DataModel = new CategoryRoleModel
            {
                CompanyName = "GROCERIES"
            };
            serviceDocument = await this.Controller.Search();
            Assert.True(serviceDocument.DataProfile.DataList.Count == 2);
        }

        [Fact]
        public async Task CategoryRoleSave()
        {
            ServiceDocument<CategoryRoleModel> serviceDocument = await this.Controller.Save();
            Assert.True(serviceDocument.Result.Type == MessageType.Success);
        }
    }
}
