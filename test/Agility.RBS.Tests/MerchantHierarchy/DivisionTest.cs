﻿// <copyright file="DivisionTest.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.MerchantHierarchy
{
    using System.Threading.Tasks;
    using Agility.Framework.Test;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.MerchantHierarchy.Models;
    using Agility.RBS.MDM.UnitOfMeasure.Models;
    using Agility.RBS.Tests.MerchantHierarchy;
    using Xunit;

    [Collection("RbsCollection")]
    public class DivisionTest : DivisionTestsBase
    {
        public DivisionTest(FxFixture fxFixture)
            : base(fxFixture)
        {
        }

        [Fact]
        public async Task DivisionList()
        {
            ServiceDocument<DivisionModel> serviceDocument = await this.Controller.List();
            Assert.True(serviceDocument.DomainData.Count == 2);
        }

        [Fact]
        public async Task DivisionNew()
        {
            ServiceDocument<DivisionModel> serviceDocument = await this.Controller.New();
            Assert.True(serviceDocument.DomainData.Count == 3);
        }

        [Fact]
        public async Task DivisionSearch()
        {
            ServiceDocument<DivisionModel> serviceDocument = await this.Controller.New();
            Assert.True(serviceDocument.DomainData.Count == 3);
            Assert.True(serviceDocument.LocalizationData.Count == 1);
            serviceDocument.DataProfile.DataModel = new DivisionModel
            {
                Description = "GROCERIES"
            };
            serviceDocument = await this.Controller.Search();
            Assert.True(serviceDocument.DataProfile.DataList.Count == 2);
        }

        [Fact]
        public async Task DivisionSave()
        {
            ServiceDocument<DivisionModel> serviceDocument = await this.Controller.Save();
            Assert.True(serviceDocument.Result.Type == MessageType.Success);
        }
    }
}
