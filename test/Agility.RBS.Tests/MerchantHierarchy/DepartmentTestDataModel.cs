﻿// <copyright file="DepartmentTestDataModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.MerchantHierarchy
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.RBS.MDM.MerchantHierarchy.Models;
    using Agility.RBS.MDM.Repositories.Models;
    using Agility.RBS.MDM.UnitOfMeasure.Models;
    using Agility.RBS.MDM.UtilityDomain.Models;

    public class DepartmentTestDataModel
    {
        public List<DomainDetailModel> DomainDetailModels { get; set; }

        public List<DivisionDomainModel> DivisionDomainModels { get; set; }

        public List<UsersDomainModel> UsersDomainModels { get; set; }

        public List<DepartmentModel> DepartmentModels { get; set; }

        public List<CompanyDomainModel> CompanyDomainModels { get; set; }

        public List<VatCodeDomainModel> VatCodeDomainModels { get; set; }
    }
}
