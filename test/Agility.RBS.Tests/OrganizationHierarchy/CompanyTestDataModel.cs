﻿// <copyright file="CompanyTestDataModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.OrganizationHierarchy
{
    using System.Collections.Generic;
    using Agility.RBS.MDM.OrganizationHierarchy.Models;
    using Agility.RBS.MDM.Repositories.Models;
    using Agility.RBS.MDM.UtilityDomain.Models;

    public class CompanyTestDataModel
    {
        public List<DomainDetailModel> DomainDetailModels { get; set; }

        public List<CompanyModel> CompanyModels { get; set; }

        public List<VatCodeDomainModel> VatCodeDomainModels { get; set; }

        public List<LocationDomainModel> LocationDomains { get; set; }

        public List<CurrencyDomainModel> CurrencyDomains { get; set; }
    }
}
