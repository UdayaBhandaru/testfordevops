﻿// <copyright file="OrgCountryTestDataModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.OrganizationHierarchy
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.RBS.MDM.MerchantHierarchy.Models;
    using Agility.RBS.MDM.OrganizationHierarchy.Models;
    using Agility.RBS.MDM.Repositories.Models;
    using Agility.RBS.MDM.UnitOfMeasure.Models;
    using Agility.RBS.MDM.UtilityDomain.Models;

    public class OrgCountryTestDataModel
    {
        public List<DomainDetailModel> DomainDetailModels { get; set; }

        public List<OrgCountryModel> OrgCountryModels { get; set; }

        public List<CompanyModel> CompanyModels { get; set; }

        public List<CompanyDomainModel> CompanyDomainModels { get; set; }

        public List<VatCodeDomainModel> VatCodeDomainModels { get; set; }

        public List<LocationDomainModel> LocationDomains { get; set; }

        public List<CurrencyDomainModel> CurrencyDomains { get; set; }

        public List<CountryDomainModel> CountryDomainModels { get; set; }
    }
}
