﻿// <copyright file="FormatTest.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.OrganizationHierarchy
{
    using System.Threading.Tasks;
    using Agility.Framework.Test;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.OrganizationHierarchy.Models;
    using Xunit;

    [Collection("RbsCollection")]
    public class FormatTest : FormatTestsBase
    {
        public FormatTest(FxFixture fxFixture)
            : base(fxFixture)
        {
        }

        [Fact]
        public async Task FormatList()
        {
            ServiceDocument<FormatModel> serviceDocument = await this.Controller.List();
            Assert.True(serviceDocument.DomainData.Count == 4);
        }

        [Fact]
        public async Task FormatNew()
        {
            ServiceDocument<FormatModel> serviceDocument = await this.Controller.New();
            Assert.True(serviceDocument.DomainData.Count == 7);
        }

        [Fact]
        public async Task FormatSearch()
        {
            ServiceDocument<FormatModel> serviceDocument = await this.Controller.New();
            Assert.True(serviceDocument.DomainData.Count == 7);
            Assert.True(serviceDocument.LocalizationData.Count == 1);
            serviceDocument.DataProfile.DataModel = new FormatModel
            {
                FormatName = "TSC KUWAIT"
            };
            serviceDocument = await this.Controller.Search();
            Assert.True(serviceDocument.DataProfile.DataList.Count == 2);
        }

        [Fact]
        public async Task FormatSave()
        {
            ServiceDocument<FormatModel> serviceDocument = await this.Controller.Save();
            Assert.True(serviceDocument.Result.Type == MessageType.Success);
        }
    }
}
