﻿// <copyright file="FormatTestsBase.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.OrganizationHierarchy
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Core;
    using Agility.Framework.Designer.Mapping;
    using Agility.Framework.Test;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.Framework.Web.Security.Mapping;
    using Agility.RBS.Core;
    using Agility.RBS.MDM;
    using Agility.RBS.MDM.Brand.Mapping;
    using Agility.RBS.MDM.CostZone.Mapping;
    using Agility.RBS.MDM.Favourite.Mapping;
    using Agility.RBS.MDM.ItemList.Mapping;
    using Agility.RBS.MDM.MerchantHierarchy.Mapping;
    using Agility.RBS.MDM.OrganizationHierarchy;
    using Agility.RBS.MDM.OrganizationHierarchy.Mapping;
    using Agility.RBS.MDM.OrganizationHierarchy.Models;
    using Agility.RBS.MDM.Partner.Mapping;
    using Agility.RBS.MDM.PriceZone.Mapping;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Mapping;
    using Agility.RBS.MDM.Repositories.Models;
    using Agility.RBS.MDM.Season.Mapping;
    using Agility.RBS.MDM.UnitOfMeasure.Mapping;
    using Agility.RBS.MDM.UtilityDomain;
    using Agility.RBS.MDM.UtilityDomain.Mapping;
    using Agility.RBS.MDM.UtilityDomain.Models;
    using Agility.RBS.MDM.ValueAddedTax.Mapping;
    using Agility.RBS.SupplierManagement.Supplier.Mapping;
    using Agility.RBS.SupplierManagement.SupplierAddress.Mapping;
    using AutoMapper;
    using Devart.Data.Oracle;
    using Moq;

    public class FormatTestsBase : RbsFixture
    {
        public FormatTestsBase(FxFixture fxFixture)
            : base(fxFixture)
        {
            Mock<UtilityDomainRepository> utilityDomainRepository = this.MockBaseOraclePackageRepository<UtilityDomainRepository, DomainDetailModel>(this.FormatTestDataModel.DomainDetailModels, new object[] { });
            Mock<DomainDataRepository> domainDataRepository = this.MockBaseOraclePackageRepository<DomainDataRepository, VatCodeDomainModel>(this.FormatTestDataModel.VatCodeDomainModels, new object[] { utilityDomainRepository.Object, null });
            domainDataRepository.Setup(x => x.GetProcedure2<OrgCountryDomainModel>(It.IsAny<OracleObject>(), It.IsAny<PackageParams>(), It.IsAny<ServiceDocumentResult>(), It.IsAny<OracleParameterCollection>()))
.Returns(Task.FromResult(this.FormatTestDataModel.OrgCountryDomainModels));
            domainDataRepository.Setup(x => x.GetProcedure2<CompanyDomainModel>(It.IsAny<OracleObject>(), It.IsAny<PackageParams>(), It.IsAny<ServiceDocumentResult>(), It.IsAny<OracleParameterCollection>()))
.Returns(Task.FromResult(this.FormatTestDataModel.CompanyDomainModels));
            domainDataRepository.Setup(x => x.GetProcedure2<CountryDomainModel>(It.IsAny<OracleObject>(), It.IsAny<PackageParams>(), It.IsAny<ServiceDocumentResult>(), It.IsAny<OracleParameterCollection>()))
.Returns(Task.FromResult(this.FormatTestDataModel.CountryDomainModels));
            domainDataRepository.Setup(x => x.GetProcedure2<LocationDomainModel>(It.IsAny<OracleObject>(), It.IsAny<PackageParams>(), It.IsAny<ServiceDocumentResult>(), It.IsAny<OracleParameterCollection>()))
.Returns(Task.FromResult(this.FormatTestDataModel.LocationDomains));
            domainDataRepository.Setup(x => x.GetProcedure2<CurrencyDomainModel>(It.IsAny<OracleObject>(), It.IsAny<PackageParams>(), It.IsAny<ServiceDocumentResult>(), It.IsAny<OracleParameterCollection>()))
.Returns(Task.FromResult(this.FormatTestDataModel.CurrencyDomains));
            domainDataRepository.Setup(x => x.GetProcedure2<VatCodeDomainModel>(It.IsAny<OracleObject>(), It.IsAny<PackageParams>(), It.IsAny<ServiceDocumentResult>(), It.IsAny<OracleParameterCollection>()))
         .Returns(Task.FromResult(this.FormatTestDataModel.VatCodeDomainModels));
            domainDataRepository.Setup(x => x.GetProcedure2<CompanyDomainModel>(It.IsAny<OracleObject>(), It.IsAny<PackageParams>(), It.IsAny<ServiceDocumentResult>(), It.IsAny<OracleParameterCollection>()))
.Returns(Task.FromResult(this.FormatTestDataModel.CompanyDomainModels));
            Mock<OrganizationHierarchyRepository> organizationHierarchyRepository = this.MockBaseOraclePackageRepository<OrganizationHierarchyRepository, FormatModel>(this.FormatTestDataModel.FormatModels, new object[] { });
            organizationHierarchyRepository.Setup(x => x.SetParamsOrgFormatSearch(It.IsAny<FormatModel>(), It.IsAny<OracleObject>()))
                  .Verifiable();
            organizationHierarchyRepository.Setup(x => x.SetParamsOrgCountrySearch(It.IsAny<OrgCountryModel>(), It.IsAny<OracleObject>()))
                  .Verifiable();
            organizationHierarchyRepository.Setup(x => x.SetParamsOrgRegionSearch(It.IsAny<RegionModel>(), It.IsAny<OracleObject>()))
                 .Verifiable();
            organizationHierarchyRepository.Setup(x => x.SetParamsOrgFormatSave(It.IsAny<FormatModel>(), It.IsAny<OracleObject>()))
               .Verifiable();
            organizationHierarchyRepository.Setup(x => x.GetProcedure2<OrgCountryModel>(It.IsAny<OracleObject>(), It.IsAny<PackageParams>(), It.IsAny<ServiceDocumentResult>(), It.IsAny<OracleParameterCollection>()))
.Returns(Task.FromResult(this.FormatTestDataModel.OrgCountryModels));
            organizationHierarchyRepository.Setup(x => x.GetProcedure2<RegionModel>(It.IsAny<OracleObject>(), It.IsAny<PackageParams>(), It.IsAny<ServiceDocumentResult>(), It.IsAny<OracleParameterCollection>()))
.Returns(Task.FromResult(this.FormatTestDataModel.RegionModels));
            this.Controller = this.Setup<MDM.OrganizationHierarchy.FormatController>();
        }

        protected FormatController Controller { get; set; }

        protected ServiceDocument<FormatModel> ServiceDocument { get; set; }

        protected FormatTestDataModel FormatTestDataModel { get; set; }

        public override void ConfigureServices(string controllerName)
        {
            base.ConfigureServices("Format");
            this.CreateTestData(new List<string>(), new List<string> { "Security.json" });

            this.FormatTestDataModel = this.CreateTestData<FormatTestDataModel>(new List<string> { "Format.json" }, "OrganizationHierarchy");

            base.MockFxDbContext<FormatModel>("Journey");
        }

        protected override void ComponentStartups(List<ComponentStartup> startups)
        {
            base.ComponentStartups(startups);
            startups.Add(new MdmStartup());
        }

        protected override void InitializeMappings()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<FxMappingProfile>();
                cfg.AddProfile<SecurityMappingProfile>();
                cfg.AddProfile<UnitOfMeasureMapping>();
                cfg.AddProfile<UtilityDomainMapping>();
                cfg.AddProfile<MerchantHierarchyMapping>();
                cfg.AddProfile<OrganizationHierarchyMapping>();
                cfg.AddProfile<ValueAddedTaxMapping>();
                cfg.AddProfile<SeasonMapping>();
                cfg.AddProfile<DomainDataMapping>();
                cfg.AddProfile<SupplierModelMapping>();
                cfg.AddProfile<SupplierAddressModelMapping>();
                cfg.AddProfile<FavouriteMapping>();
                cfg.AddProfile<ItemListMapping>();
                cfg.AddProfile<CostZoneMapping>();
                cfg.AddProfile<PriceZoneMapping>();
                cfg.AddProfile<BrandMapping>();
                cfg.AddProfile<PartnerMapping>();
                this.ConfigureMapping(cfg);
            });
        }
    }
}
