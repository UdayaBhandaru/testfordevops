﻿// <copyright file="OrgCountryTest.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.OrganizationHierarchy
{
    using System.Threading.Tasks;
    using Agility.Framework.Test;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.OrganizationHierarchy.Models;
    using Xunit;

    [Collection("RbsCollection")]
    public class OrgCountryTest : OrgCountryTestsBase
    {
        public OrgCountryTest(FxFixture fxFixture)
            : base(fxFixture)
        {
        }

        [Fact]
        public async Task OrgCountryList()
        {
            ServiceDocument<OrgCountryModel> serviceDocument = await this.Controller.List();
            Assert.True(serviceDocument.DomainData.Count == 3);
        }

        [Fact]
        public async Task OrgCountryNew()
        {
            ServiceDocument<OrgCountryModel> serviceDocument = await this.Controller.New();
            Assert.True(serviceDocument.DomainData.Count == 6);
        }

        [Fact]
        public async Task OrgCountrySearch()
        {
            ServiceDocument<OrgCountryModel> serviceDocument = await this.Controller.New();
            Assert.True(serviceDocument.DomainData.Count == 6);
            Assert.True(serviceDocument.LocalizationData.Count == 1);
            serviceDocument.DataProfile.DataModel = new OrgCountryModel
            {
                CountryCode = "KWT"
            };
            serviceDocument = await this.Controller.Search();
            Assert.True(serviceDocument.DataProfile.DataList.Count == 2);
        }

        [Fact]
        public async Task OrgCountrySave()
        {
            ServiceDocument<OrgCountryModel> serviceDocument = await this.Controller.Save();
            Assert.True(serviceDocument.Result.Type == MessageType.Success);
        }
    }
}
