﻿// <copyright file="OrgCountryTestsBase.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.OrganizationHierarchy
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.IO;
    using System.Threading.Tasks;
    using Agility.Framework.Core;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Designer.Mapping;
    using Agility.Framework.Test;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.Framework.Web.Security.Mapping;
    using Agility.RBS.Core;
    using Agility.RBS.MDM;
    using Agility.RBS.MDM.Brand.Mapping;
    using Agility.RBS.MDM.CostZone.Mapping;
    using Agility.RBS.MDM.Favourite.Mapping;
    using Agility.RBS.MDM.ItemList.Mapping;
    using Agility.RBS.MDM.MerchantHierarchy;
    using Agility.RBS.MDM.MerchantHierarchy.Mapping;
    using Agility.RBS.MDM.MerchantHierarchy.Models;
    using Agility.RBS.MDM.OrganizationHierarchy;
    using Agility.RBS.MDM.OrganizationHierarchy.Mapping;
    using Agility.RBS.MDM.OrganizationHierarchy.Models;
    using Agility.RBS.MDM.Partner.Mapping;
    using Agility.RBS.MDM.PriceZone.Mapping;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Mapping;
    using Agility.RBS.MDM.Repositories.Models;
    using Agility.RBS.MDM.Season.Mapping;
    using Agility.RBS.MDM.UnitOfMeasure;
    using Agility.RBS.MDM.UnitOfMeasure.Mapping;
    using Agility.RBS.MDM.UnitOfMeasure.Models;
    using Agility.RBS.MDM.UtilityDomain;
    using Agility.RBS.MDM.UtilityDomain.Mapping;
    using Agility.RBS.MDM.UtilityDomain.Models;
    using Agility.RBS.MDM.ValueAddedTax.Mapping;
    using Agility.RBS.SupplierManagement.Supplier.Mapping;
    using Agility.RBS.SupplierManagement.SupplierAddress.Mapping;
    using AutoMapper;
    using Devart.Data.Oracle;
    using Microsoft.Extensions.Caching.Distributed;
    using Microsoft.Extensions.DependencyInjection;
    using Moq;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using Xunit;

    public class OrgCountryTestsBase : RbsFixture
    {
        public OrgCountryTestsBase(FxFixture fxFixture)
            : base(fxFixture)
        {
            Mock<UtilityDomainRepository> utilityDomainRepository = this.MockBaseOraclePackageRepository<UtilityDomainRepository, DomainDetailModel>(this.OrgCountryTestDataModel.DomainDetailModels, new object[] { });
            Mock<DomainDataRepository> domainDataRepository = this.MockBaseOraclePackageRepository<DomainDataRepository, VatCodeDomainModel>(this.OrgCountryTestDataModel.VatCodeDomainModels, new object[] { utilityDomainRepository.Object, null });

            domainDataRepository.Setup(x => x.GetProcedure2<CompanyDomainModel>(It.IsAny<OracleObject>(), It.IsAny<PackageParams>(), It.IsAny<ServiceDocumentResult>(), It.IsAny<OracleParameterCollection>()))
.Returns(Task.FromResult(this.OrgCountryTestDataModel.CompanyDomainModels));
            domainDataRepository.Setup(x => x.GetProcedure2<CountryDomainModel>(It.IsAny<OracleObject>(), It.IsAny<PackageParams>(), It.IsAny<ServiceDocumentResult>(), It.IsAny<OracleParameterCollection>()))
.Returns(Task.FromResult(this.OrgCountryTestDataModel.CountryDomainModels));
            domainDataRepository.Setup(x => x.GetProcedure2<LocationDomainModel>(It.IsAny<OracleObject>(), It.IsAny<PackageParams>(), It.IsAny<ServiceDocumentResult>(), It.IsAny<OracleParameterCollection>()))
.Returns(Task.FromResult(this.OrgCountryTestDataModel.LocationDomains));
            domainDataRepository.Setup(x => x.GetProcedure2<CurrencyDomainModel>(It.IsAny<OracleObject>(), It.IsAny<PackageParams>(), It.IsAny<ServiceDocumentResult>(), It.IsAny<OracleParameterCollection>()))
.Returns(Task.FromResult(this.OrgCountryTestDataModel.CurrencyDomains));
            domainDataRepository.Setup(x => x.GetProcedure2<VatCodeDomainModel>(It.IsAny<OracleObject>(), It.IsAny<PackageParams>(), It.IsAny<ServiceDocumentResult>(), It.IsAny<OracleParameterCollection>()))
         .Returns(Task.FromResult(this.OrgCountryTestDataModel.VatCodeDomainModels));
            domainDataRepository.Setup(x => x.GetProcedure2<CompanyDomainModel>(It.IsAny<OracleObject>(), It.IsAny<PackageParams>(), It.IsAny<ServiceDocumentResult>(), It.IsAny<OracleParameterCollection>()))
.Returns(Task.FromResult(this.OrgCountryTestDataModel.CompanyDomainModels));
            Mock<OrganizationHierarchyRepository> organizationHierarchyRepository = this.MockBaseOraclePackageRepository<OrganizationHierarchyRepository, OrgCountryModel>(this.OrgCountryTestDataModel.OrgCountryModels, new object[] { });
            organizationHierarchyRepository.Setup(x => x.SetParamsOrgCountrySearch(It.IsAny<OrgCountryModel>(), It.IsAny<OracleObject>()))
                  .Verifiable();
            organizationHierarchyRepository.Setup(x => x.SetParamsCompanySearch(It.IsAny<CompanyModel>(), It.IsAny<OracleObject>()))
                  .Verifiable();
            organizationHierarchyRepository.Setup(x => x.SetParamsOrgCountrySave(It.IsAny<OrgCountryModel>(), It.IsAny<OracleObject>()))
               .Verifiable();
            organizationHierarchyRepository.Setup(x => x.GetProcedure2<CompanyModel>(It.IsAny<OracleObject>(), It.IsAny<PackageParams>(), It.IsAny<ServiceDocumentResult>(), It.IsAny<OracleParameterCollection>()))
.Returns(Task.FromResult(this.OrgCountryTestDataModel.CompanyModels));
            this.Controller = this.Setup<MDM.OrganizationHierarchy.OrgCountryController>();
        }

        protected OrgCountryController Controller { get; set; }

        protected ServiceDocument<OrgCountryModel> ServiceDocument { get; set; }

        protected OrgCountryTestDataModel OrgCountryTestDataModel { get; set; }

        public override void ConfigureServices(string controllerName)
        {
            base.ConfigureServices("OrgCountry");
            this.CreateTestData(new List<string>(), new List<string> { "Security.json" });

            this.OrgCountryTestDataModel = this.CreateTestData<OrgCountryTestDataModel>(new List<string> { "OrgCountry.json" }, "OrganizationHierarchy");

            base.MockFxDbContext<OrgCountryModel>("Journey");
        }

        protected override void ComponentStartups(List<ComponentStartup> startups)
        {
            base.ComponentStartups(startups);
            startups.Add(new MdmStartup());
        }

        protected override void InitializeMappings()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<FxMappingProfile>();
                cfg.AddProfile<SecurityMappingProfile>();
                cfg.AddProfile<UnitOfMeasureMapping>();
                cfg.AddProfile<UtilityDomainMapping>();
                cfg.AddProfile<MerchantHierarchyMapping>();
                cfg.AddProfile<OrganizationHierarchyMapping>();
                cfg.AddProfile<ValueAddedTaxMapping>();
                cfg.AddProfile<SeasonMapping>();
                cfg.AddProfile<DomainDataMapping>();
                cfg.AddProfile<SupplierModelMapping>();
                cfg.AddProfile<SupplierAddressModelMapping>();
                cfg.AddProfile<FavouriteMapping>();
                cfg.AddProfile<ItemListMapping>();
                cfg.AddProfile<CostZoneMapping>();
                cfg.AddProfile<PriceZoneMapping>();
                cfg.AddProfile<BrandMapping>();
                cfg.AddProfile<PartnerMapping>();
                this.ConfigureMapping(cfg);
            });
        }
    }
}
