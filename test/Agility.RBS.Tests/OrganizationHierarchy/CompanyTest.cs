﻿// <copyright file="CompanyTest.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.OrganizationHierarchy
{
    using System.Threading.Tasks;
    using Agility.Framework.Test;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.OrganizationHierarchy.Models;
    using Xunit;

    [Collection("RbsCollection")]
    public class CompanyTest : CompanyTestsBase
    {
        public CompanyTest(FxFixture fxFixture)
            : base(fxFixture)
        {
        }

        [Fact]
        public async Task CompanyList()
        {
            ServiceDocument<CompanyModel> serviceDocument = await this.Controller.List();
            Assert.True(serviceDocument.DomainData.Count == 1);
        }

        [Fact]
        public async Task CompanyNew()
        {
            ServiceDocument<CompanyModel> serviceDocument = await this.Controller.New();
            Assert.True(serviceDocument.DomainData.Count == 5);
        }

        [Fact]
        public async Task CompanySearch()
        {
            ServiceDocument<CompanyModel> serviceDocument = await this.Controller.New();
            Assert.True(serviceDocument.DomainData.Count == 5);
            Assert.True(serviceDocument.LocalizationData.Count == 1);
            serviceDocument.DataProfile.DataModel = new CompanyModel
            {
                CompanyName = "TSC KUWAIT"
            };
            serviceDocument = await this.Controller.Search();
            Assert.True(serviceDocument.DataProfile.DataList.Count == 2);
        }

        [Fact]
        public async Task CompanySave()
        {
            ServiceDocument<CompanyModel> serviceDocument = await this.Controller.Save();
            Assert.True(serviceDocument.Result.Type == MessageType.Success);
        }
    }
}
