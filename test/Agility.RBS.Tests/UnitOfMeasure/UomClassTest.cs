﻿// <copyright file="UomClassTest.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.UnitOfMeasure
{
    using System.Threading.Tasks;
    using Agility.Framework.Test;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.UnitOfMeasure.Models;
    using Xunit;

    [Collection("RbsCollection")]
    public class UomClassTest : UomClassTestsBase
    {
        public UomClassTest(FxFixture fxFixture)
            : base(fxFixture)
        {
        }

        [Fact]
        public async Task UomClassList()
        {
            this.ServiceDocument = await this.Controller.List();
            Assert.True(this.ServiceDocument.DomainData.Count == 1);
        }

        [Fact]
        public async Task UomClassNew()
        {
            this.ServiceDocument = await this.Controller.New();
            Assert.True(this.ServiceDocument.DomainData.Count == 1);
        }

        [Fact]
        public async Task UomClassSearch()
        {
            this.ServiceDocument = await this.Controller.New();
            Assert.True(this.ServiceDocument.DomainData.Count == 1);
            Assert.True(this.ServiceDocument.LocalizationData.Count == 1);
            this.ServiceDocument.DataProfile.DataModel = new UomClassModel
            {
                UomClass = "MASS"
            };
            this.ServiceDocument = await this.Controller.Search();
            Assert.True(this.ServiceDocument.DataProfile.DataList.Count == 2);
        }

        [Fact]
        public async Task UomClassSave()
        {
            this.ServiceDocument = await this.Controller.Save();
            Assert.True(this.ServiceDocument.Result.Type == MessageType.Success);
        }
    }
}
