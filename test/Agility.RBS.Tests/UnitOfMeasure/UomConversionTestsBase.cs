﻿// <copyright file="UomConversionTestsBase.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.UnitOfMeasure
{
    using System.Collections.Generic;
    using Agility.Framework.Core;
    using Agility.Framework.Designer.Mapping;
    using Agility.Framework.Test;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.Framework.Web.Security.Mapping;
    using Agility.RBS.MDM;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Models;
    using Agility.RBS.MDM.UnitOfMeasure;
    using Agility.RBS.MDM.UnitOfMeasure.Mapping;
    using Agility.RBS.MDM.UnitOfMeasure.Models;
    using Agility.RBS.MDM.UtilityDomain;
    using Agility.RBS.MDM.UtilityDomain.Mapping;
    using Agility.RBS.MDM.UtilityDomain.Models;
    using AutoMapper;
    using Devart.Data.Oracle;
    using Moq;

    public class UomConversionTestsBase : RbsFixture
    {
        public UomConversionTestsBase(FxFixture fxFixture)
            : base(fxFixture)
        {
            Mock<UtilityDomainRepository> utilityDomainRepository = this.MockBaseOraclePackageRepository<UtilityDomainRepository, DomainDetailModel>(this.UomTestDataModel.DomainDetailModels, new object[] { });
            this.MockBaseOraclePackageRepository<DomainDataRepository, UomDomainModel>(this.UomTestDataModel.UomDomainModels, new object[] { utilityDomainRepository.Object, null });

            Mock<UnitOfMeasureRepository> unitOfMeasureRepository = this.MockBaseOraclePackageRepository<UnitOfMeasureRepository, UomConversionModel>(this.UomTestDataModel.UomConversionModels, new object[] { });
            unitOfMeasureRepository.Setup(x => x.SetParamsUomConvSearch(It.IsAny<UomConversionModel>(), It.IsAny<OracleObject>()))
               .Verifiable();
            unitOfMeasureRepository.Setup(x => x.SetParamsUomConvSave(It.IsAny<UomConversionModel>(), It.IsAny<List<UomConversionModel>>()))
               .Verifiable();
            this.Controller = this.Setup<UomConversionController>();
        }

        protected UomConversionController Controller { get; set; }

        protected ServiceDocument<UomConversionModel> ServiceDocument { get; set; }

        protected UomTestDataModel UomTestDataModel { get; set; }

        public override void ConfigureServices(string controllerName)
        {
            base.ConfigureServices("UomConversion");
            this.CreateTestData(new List<string>(), new List<string> { "Security.json" });
            this.UomTestDataModel = this.CreateTestData<UomTestDataModel>(new List<string> { "UomClass.json" }, "UnitOfMeasure");
        }

        protected override void ComponentStartups(List<ComponentStartup> startups)
        {
            base.ComponentStartups(startups);
            startups.Add(new MdmStartup());
        }

        protected override void InitializeMappings()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<FxMappingProfile>();
                cfg.AddProfile<SecurityMappingProfile>();

                cfg.AddProfile<UnitOfMeasureMapping>();
                cfg.AddProfile<UtilityDomainMapping>();
                this.ConfigureMapping(cfg);
            });
        }
    }
}
