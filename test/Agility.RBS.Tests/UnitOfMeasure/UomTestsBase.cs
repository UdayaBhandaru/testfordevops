﻿// <copyright file="UomTestsBase.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.UnitOfMeasure
{
    using System.Collections.Generic;
    using Agility.Framework.Core;
    using Agility.Framework.Designer.Mapping;
    using Agility.Framework.Test;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.Framework.Web.Security.Mapping;
    using Agility.RBS.MDM;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Models;
    using Agility.RBS.MDM.UnitOfMeasure;
    using Agility.RBS.MDM.UnitOfMeasure.Mapping;
    using Agility.RBS.MDM.UnitOfMeasure.Models;
    using Agility.RBS.MDM.UtilityDomain;
    using Agility.RBS.MDM.UtilityDomain.Mapping;
    using Agility.RBS.MDM.UtilityDomain.Models;
    using AutoMapper;
    using Devart.Data.Oracle;
    using Moq;

    public class UomTestsBase : RbsFixture
    {
        public UomTestsBase(FxFixture fxFixture)
            : base(fxFixture)
        {
            Mock<UtilityDomainRepository> utilityDomainRepository = this.MockBaseOraclePackageRepository<UtilityDomainRepository, DomainDetailModel>(this.UomTestDataModel.DomainDetailModels, new object[] { });
            this.MockBaseOraclePackageRepository<DomainDataRepository, UomClassDomainModel>(this.UomTestDataModel.UomClassDomainModels, new object[] { utilityDomainRepository.Object, null });

            Mock<UnitOfMeasureRepository> unitOfMeasureRepository = this.MockBaseOraclePackageRepository<UnitOfMeasureRepository, UomModel>(this.UomTestDataModel.UomModels, new object[] { });
            unitOfMeasureRepository.Setup(x => x.SetParamsUomSearch(It.IsAny<UomModel>(), It.IsAny<OracleObject>()))
               .Verifiable();
            unitOfMeasureRepository.Setup(x => x.SetParamsUomSave(It.IsAny<UomModel>(), It.IsAny<OracleObject>()))
               .Verifiable();
            this.Controller = this.Setup<UomController>();
        }

        protected UomController Controller { get; set; }

        protected ServiceDocument<UomModel> ServiceDocument { get; set; }

        protected UomTestDataModel UomTestDataModel { get; set; }

        public override void ConfigureServices(string controllerName)
        {
            base.ConfigureServices("Uom");
            this.CreateTestData(new List<string>(), new List<string> { "Security.json" });
            this.UomTestDataModel = this.CreateTestData<UomTestDataModel>(new List<string> { "UomClass.json" }, "UnitOfMeasure");
        }

        protected override void ComponentStartups(List<ComponentStartup> startups)
        {
            base.ComponentStartups(startups);
            startups.Add(new MdmStartup());
        }

        protected override void InitializeMappings()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<FxMappingProfile>();
                cfg.AddProfile<SecurityMappingProfile>();

                cfg.AddProfile<UnitOfMeasureMapping>();
                cfg.AddProfile<UtilityDomainMapping>();
                this.ConfigureMapping(cfg);
            });
        }
    }
}
