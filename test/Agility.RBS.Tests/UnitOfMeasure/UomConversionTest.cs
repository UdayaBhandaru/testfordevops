﻿// <copyright file="UomConversionTest.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.UnitOfMeasure
{
    using System.Threading.Tasks;
    using Agility.Framework.Test;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.UnitOfMeasure.Models;
    using Xunit;

    [Collection("RbsCollection")]
    public class UomConversionTest : UomConversionTestsBase
    {
        public UomConversionTest(FxFixture fxFixture)
            : base(fxFixture)
        {
        }

        [Fact]
        public async Task UomConversionList()
        {
            this.ServiceDocument = await this.Controller.List();
            Assert.True(this.ServiceDocument.DomainData.Count == 1);
        }

        [Fact]
        public async Task UomConversionNew()
        {
            this.ServiceDocument = await this.Controller.New();
            Assert.True(this.ServiceDocument.DomainData.Count == 2);
        }

        [Fact]
        public async Task UomConversionSearch()
        {
            this.ServiceDocument = await this.Controller.New();
            Assert.True(this.ServiceDocument.DomainData.Count == 2);
            Assert.True(this.ServiceDocument.LocalizationData.Count == 1);
            this.ServiceDocument.DataProfile.DataModel = new UomConversionModel
            {
                FromUom = "LB"
            };
            this.ServiceDocument = await this.Controller.Search();
            Assert.True(this.ServiceDocument.DataProfile.DataList.Count == 2);
        }

        [Fact]
        public async Task UomConversionSave()
        {
            this.ServiceDocument = await this.Controller.New();
            this.ServiceDocument.DataProfile.DataModel = new UomConversionModel
            {
                FromUom = "KG",
                ToUom = "G",
                Factor = 1000,
                Operator = "M"
            };
            this.ServiceDocument = await this.Controller.Save();
            Assert.True(this.ServiceDocument.Result.Type == MessageType.Success);
        }
    }
}
