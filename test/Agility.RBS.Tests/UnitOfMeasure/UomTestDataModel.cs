﻿// <copyright file="UomTestDataModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.UnitOfMeasure
{
    using System.Collections.Generic;
    using Agility.RBS.MDM.Repositories.Models;
    using Agility.RBS.MDM.UnitOfMeasure.Models;
    using Agility.RBS.MDM.UtilityDomain.Models;

    public class UomTestDataModel
    {
        public List<DomainDetailModel> DomainDetailModels { get; set; }

        public List<UomClassDomainModel> UomClassDomainModels { get; set; }

        public List<UomModel> UomModels { get; set; }

        public List<UomClassModel> UomClassModels { get; set; }

        public List<UomDomainModel> UomDomainModels { get; set; }

        public List<UomConversionModel> UomConversionModels { get; set; }
    }
}
