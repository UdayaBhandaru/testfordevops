﻿// <copyright file="UomTest.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.UnitOfMeasure
{
    using System.Threading.Tasks;
    using Agility.Framework.Test;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.UnitOfMeasure.Models;
    using Xunit;

    [Collection("RbsCollection")]
    public class UomTest : UomTestsBase
    {
        public UomTest(FxFixture fxFixture)
            : base(fxFixture)
        {
        }

        [Fact]
        public async Task UomList()
        {
            this.ServiceDocument = await this.Controller.List();
            Assert.True(this.ServiceDocument.DomainData.Count == 2);
        }

        [Fact]
        public async Task UomNew()
        {
            this.ServiceDocument = await this.Controller.New();
            Assert.True(this.ServiceDocument.DomainData.Count == 2);
        }

        [Fact]
        public async Task UomSearch()
        {
            this.ServiceDocument = await this.Controller.New();
            Assert.True(this.ServiceDocument.DomainData.Count == 2);
            Assert.True(this.ServiceDocument.LocalizationData.Count == 1);
            this.ServiceDocument.DataProfile.DataModel = new UomModel
            {
                UomCode = "LB"
            };
            this.ServiceDocument = await this.Controller.Search();
            Assert.True(this.ServiceDocument.DataProfile.DataList.Count == 2);
        }

        [Fact]
        public async Task UomSave()
        {
            this.ServiceDocument = await this.Controller.Save();
            Assert.True(this.ServiceDocument.Result.Type == MessageType.Success);
        }
    }
}
