﻿// <copyright file="UtilityDomainTest.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.Brand
{
    using System.Threading.Tasks;
    using Agility.Framework.Test;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Brand.Models;
    using Agility.RBS.MDM.UnitOfMeasure.Models;
    using Agility.RBS.MDM.UtilityDomain.Models;
    using Agility.RBS.Tests.UnitOfMeasure;
    using Xunit;

    [Collection("RbsCollection")]
    public class UtilityDomainTest : UtilityDomainTestsBase
    {
        public UtilityDomainTest(FxFixture fxFixture)
            : base(fxFixture)
        {
        }

        [Fact]
        public async Task UtilityDomainList()
        {
            ServiceDocument<DomainHeaderModel> serviceDocument = await this.Controller.List();
            Assert.True(serviceDocument.DomainData.Count == 1);
        }

        [Fact]
        public async Task UtilityDomainSearch()
        {
            ServiceDocument<DomainHeaderModel> serviceDocument = await this.Controller.Search();
            Assert.True(serviceDocument.DomainData.Count == 2);
            Assert.True(serviceDocument.LocalizationData.Count == 1);
            serviceDocument.DataProfile.DataModel = new DomainHeaderModel
            {
                DomainDesc = "CASE"
            };
            serviceDocument = await this.Controller.Search();
            Assert.True(serviceDocument.DataProfile.DataList.Count == 2);
        }

        [Fact]
        public async Task UtilityDomainSave()
        {
            ServiceDocument<DomainHeaderModel> serviceDocument = await this.Controller.Save();
            Assert.True(serviceDocument.Result.Type == MessageType.Success);
        }
    }
}
