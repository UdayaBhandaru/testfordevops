﻿// <copyright file="UtilityDomainTestDataModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.Brand
{
    using System.Collections.Generic;
    using Agility.RBS.Core.Models;
    using Agility.RBS.MDM.UtilityDomain.Models;

    public class UtilityDomainTestDataModel
    {
        public List<DomainDetailModel> DomainDetailModel { get; set; }

        public List<DomainHeaderModel> DomainHeaderModels { get; set; }

        public List<CommonModel> CommonModels { get; set; }
    }
}
