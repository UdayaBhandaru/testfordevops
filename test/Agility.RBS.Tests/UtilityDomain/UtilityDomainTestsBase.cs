﻿// <copyright file="UtilityDomainTestsBase.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.UnitOfMeasure
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Core;
    using Agility.Framework.Designer.Mapping;
    using Agility.Framework.Test;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.Framework.Web.Security.Mapping;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Models;
    using Agility.RBS.MDM;
    using Agility.RBS.MDM.Brand.Mapping;
    using Agility.RBS.MDM.CostZone.Mapping;
    using Agility.RBS.MDM.Favourite.Mapping;
    using Agility.RBS.MDM.ItemList.Mapping;
    using Agility.RBS.MDM.MerchantHierarchy.Mapping;
    using Agility.RBS.MDM.OrganizationHierarchy.Mapping;
    using Agility.RBS.MDM.Partner.Mapping;
    using Agility.RBS.MDM.PriceZone.Mapping;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Mapping;
    using Agility.RBS.MDM.Season.Mapping;
    using Agility.RBS.MDM.UnitOfMeasure.Mapping;
    using Agility.RBS.MDM.UtilityDomain;
    using Agility.RBS.MDM.UtilityDomain.Mapping;
    using Agility.RBS.MDM.UtilityDomain.Models;
    using Agility.RBS.MDM.ValueAddedTax.Mapping;
    using Agility.RBS.SupplierManagement.Supplier.Mapping;
    using Agility.RBS.SupplierManagement.SupplierAddress.Mapping;
    using Agility.RBS.Tests.Brand;
    using AutoMapper;
    using Castle.Core.Logging;
    using Devart.Data.Oracle;
    using Microsoft.Extensions.Logging;
    using Moq;

    public class UtilityDomainTestsBase : RbsFixture
    {
        public UtilityDomainTestsBase(FxFixture fxFixture)
            : base(fxFixture)
        {
            Mock<UtilityDomainRepository> utilityDomainRepository = this.MockBaseOraclePackageRepository<UtilityDomainRepository, DomainDetailModel>(this.UtilityDomainTestDataModel.DomainDetailModel, new object[] { });
            Mock<DomainDataRepository> domainDataRepository = this.MockBaseOraclePackageRepository<DomainDataRepository, CommonModel>(this.UtilityDomainTestDataModel.CommonModels, new object[] { utilityDomainRepository.Object, null });
            domainDataRepository.Setup(x => x.GetProcedure2<DomainHeaderModel>(It.IsAny<OracleObject>(), It.IsAny<PackageParams>(), It.IsAny<ServiceDocumentResult>(), It.IsAny<OracleParameterCollection>()))
               .Returns(Task.FromResult(this.UtilityDomainTestDataModel.DomainHeaderModels));
            Mock<UtilityDomainRepository> unitOfMeasureRepository = this.MockBaseOraclePackageRepository<UtilityDomainRepository, DomainHeaderModel>(this.UtilityDomainTestDataModel.DomainHeaderModels, new object[] { });
            unitOfMeasureRepository.Setup(x => x.SetParamsDomainSearch(It.IsAny<DomainHeaderModel>(), It.IsAny<OracleObject>()))
               .Verifiable();
            unitOfMeasureRepository.Setup(x => x.SetParamsDomainSave(It.IsAny<DomainHeaderModel>(), It.IsAny<OracleObject>()))
               .Verifiable();
            this.Controller = this.Setup<UtilityDomainController>(null);
        }

        protected UtilityDomainController Controller { get; set; }

        protected ServiceDocument<DomainHeaderModel> ServiceDocument { get; set; }

        protected UtilityDomainTestDataModel UtilityDomainTestDataModel { get; set; }

        public override void ConfigureServices(string controllerName)
        {
            base.ConfigureServices("UtilityDomain");
            this.CreateTestData(new List<string>(), new List<string> { "Security.json" });

            this.UtilityDomainTestDataModel = this.CreateTestData<UtilityDomainTestDataModel>(new List<string> { "Brand.json" }, "UtilityDomain");

            base.MockFxDbContext<DomainHeaderModel>("Journey");
        }

        protected override void ComponentStartups(List<ComponentStartup> startups)
        {
            base.ComponentStartups(startups);
            startups.Add(new MdmStartup());
        }

        protected override void InitializeMappings()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<FxMappingProfile>();
                cfg.AddProfile<SecurityMappingProfile>();

                cfg.AddProfile<UnitOfMeasureMapping>();
                cfg.AddProfile<UtilityDomainMapping>();
                cfg.AddProfile<MerchantHierarchyMapping>();
                cfg.AddProfile<OrganizationHierarchyMapping>();
                cfg.AddProfile<ValueAddedTaxMapping>();
                cfg.AddProfile<SeasonMapping>();
                cfg.AddProfile<DomainDataMapping>();
                cfg.AddProfile<SupplierModelMapping>();
                cfg.AddProfile<SupplierAddressModelMapping>();
                cfg.AddProfile<FavouriteMapping>();
                cfg.AddProfile<ItemListMapping>();
                cfg.AddProfile<CostZoneMapping>();
                cfg.AddProfile<PriceZoneMapping>();
                cfg.AddProfile<BrandMapping>();
                cfg.AddProfile<PartnerMapping>();
                this.ConfigureMapping(cfg);
            });
        }
    }
}
