﻿// <copyright file="RbsFixture.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;
    using Agility.Framework.Test;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core;
    using Devart.Data.Oracle;
    using Microsoft.Extensions.Caching.Distributed;
    using Microsoft.Extensions.DependencyInjection;
    using Moq;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using Xunit;

    public class RbsFixture : FxCollectionFixture
    {
        public RbsFixture(FxFixture fxFixture)
            : base(fxFixture)
        {
            this.FxFixture = fxFixture;
            this.MockOracleConnection();
        }

        public Mock<OracleConnection> MockedOracleConnection { get; set; }

        public Mock<TRepository> MockBaseOraclePackageRepository<TRepository, TModel>(List<TModel> domainDetailModels, params object[] args)
            where TRepository : BaseOraclePackage
        {
            Mock<TRepository> mockBaseOraclePackageRepository = new Mock<TRepository>(args) { CallBase = true };
            mockBaseOraclePackageRepository.Setup(x => x.GetProcedure2<TModel>(It.IsAny<OracleObject>(), It.IsAny<PackageParams>(), It.IsAny<ServiceDocumentResult>(), It.IsAny<OracleParameterCollection>()))
               .Returns(Task.FromResult(domainDetailModels));

            ServiceDocumentResult result = new ServiceDocumentResult() { InnerException = string.Empty, StackTrace = string.Empty, Type = MessageType.Success };
            mockBaseOraclePackageRepository.Setup(x => x.SetProcedure(It.IsAny<OracleObject>(), It.IsAny<PackageParams>(), It.IsAny<OracleParameterCollection>()))
              .Returns(Task.FromResult(result));

            mockBaseOraclePackageRepository.Setup(x => x.GetObjectType(It.IsAny<string>()))
               .Returns(default(OracleType));

            mockBaseOraclePackageRepository.Setup(x => x.GetOracleObject(It.IsAny<OracleType>()))
               .Returns(default(OracleObject));

            mockBaseOraclePackageRepository.Setup(x => x.GetOracleTable(It.IsAny<OracleType>()))
              .Returns(default(OracleTable));

            mockBaseOraclePackageRepository.SetupGet(x => x.Connection).Returns(this.MockedOracleConnection.Object);
            mockBaseOraclePackageRepository.SetupSet(x => x.Connection = It.IsAny<OracleConnection>());

            this.FxFixture.ServiceCollection.AddTransient(sp => mockBaseOraclePackageRepository.Object);
            this.FxFixture.ServiceProvider = this.FxFixture.ServiceCollection.BuildServiceProvider();
            this.FxFixture.ServiceProviderMock.Setup(provider => provider.GetService(typeof(TRepository)))
                             .Returns((TRepository serviceType) => mockBaseOraclePackageRepository.Object);
            return mockBaseOraclePackageRepository;
        }

        protected TModel GetDeserializeData<TModel>(string basePath, List<string> fileNames)
          where TModel : class
        {
            TModel importExportModel = null;
            TModel tempImportExportModel = null;
            if (fileNames.Count > 0)
            {
                string[] files;
                if (this.FxFixture.FxTestCoreConfiguration.Environment == FxTestConstants.ENVIRONMENT)
                {
                    files = new string[fileNames.Count];
                    for (int index = 0; index < files.Length; index++)
                    {
                        files[index] = basePath + "\\" + fileNames[index];
                    }
                }
                else
                {
                    files = Directory.GetFiles(basePath);
                }

                JObject jObject = new JObject();
                foreach (string file in files)
                {
                    string jsonData = File.ReadAllText(file);
                    tempImportExportModel = JsonConvert.DeserializeObject<TModel>(jsonData);
                    if (importExportModel == null)
                    {
                        importExportModel = tempImportExportModel;
                        jObject = JObject.Parse(jsonData);
                    }
                    else
                    {
                        JObject jObject2 = JObject.Parse(jsonData);
                        jObject.Merge(jObject2, new JsonMergeSettings { MergeArrayHandling = MergeArrayHandling.Union });
                    }
                }

                importExportModel = JsonConvert.DeserializeObject<TModel>(jObject.ToString());
            }

            return importExportModel;
        }

        protected TModel CreateTestData<TModel>(List<string> profileJsonFileNames, string screenName)
                    where TModel : class
        {
            TModel importExportModel;
            string basePath = Directory.GetParent(@"./../../../").FullName + "/" + screenName + "/" + FxTestConstants.FIXTUREBASEPATH + "/";
            importExportModel = this.GetDeserializeData<TModel>(basePath + FxTestConstants.FIXTUREBASEPATHPROFILE + "/", profileJsonFileNames);
            return importExportModel;
        }

        private void MockOracleConnection()
        {
            this.MockedOracleConnection = new Mock<OracleConnection>();
            this.MockedOracleConnection.Setup(x => x.Open()).Verifiable();
        }
    }
}
