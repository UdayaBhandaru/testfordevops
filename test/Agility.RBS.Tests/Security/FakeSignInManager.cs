﻿// <copyright file="FakeSignInManager.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.Security
{
    using System.Threading.Tasks;
    using Agility.Framework.Core.Security.Models;
    using Microsoft.AspNetCore.Authentication;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Moq;

    public class FakeSignInManager : SignInManager<ApplicationUser>
    {
        public FakeSignInManager(IHttpContextAccessor contextAccessor)
             : base(
                   new FakeUserManager(),
                   contextAccessor,
                   new Mock<IUserClaimsPrincipalFactory<ApplicationUser>>().Object,
                   new Mock<IOptions<IdentityOptions>>().Object,
                   new Mock<ILogger<SignInManager<ApplicationUser>>>().Object,
                   new Mock<IAuthenticationSchemeProvider>().Object)
        {
        }

        public override Task SignInAsync(ApplicationUser user, bool isPersistent, string authenticationMethod = null)
        {
            return Task.FromResult(0);
        }

        public override Task<SignInResult> PasswordSignInAsync(string userName, string password, bool isPersistent, bool lockoutOnFailure)
        {
            return Task.FromResult(SignInResult.Success);
        }

        public override Task SignOutAsync()
        {
            return Task.FromResult(0);
        }
    }
}
