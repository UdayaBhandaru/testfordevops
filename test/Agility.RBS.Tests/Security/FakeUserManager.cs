﻿// <copyright file="FakeUserManager.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.Security
{
    using System;
    using System.Threading.Tasks;
    using Agility.Framework.Core.Security.Models;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Moq;

    public class FakeUserManager : UserManager<ApplicationUser>
    {
        public FakeUserManager()
            : base(
                  new Mock<IUserStore<ApplicationUser>>().Object,
                  new Mock<IOptions<IdentityOptions>>().Object,
                  new Mock<IPasswordHasher<ApplicationUser>>().Object,
                  new IUserValidator<ApplicationUser>[0],
                  new IPasswordValidator<ApplicationUser>[0],
                  new Mock<ILookupNormalizer>().Object,
                  new Mock<IdentityErrorDescriber>().Object,
                  new Mock<IServiceProvider>().Object,
                  new Mock<ILogger<UserManager<ApplicationUser>>>().Object)
        {
        }

        public override Task<IdentityResult> CreateAsync(ApplicationUser user, string password)
        {
            return Task.FromResult(IdentityResult.Success);
        }

        public override Task<ApplicationUser> FindByNameAsync(string userName)
        {
            ApplicationUser user = null;
            ApplicationUser dummyUser = new ApplicationUser() { UserName = "spulla", Email = "spulla@agility.com" };
            if (dummyUser.UserName == userName)
            {
                return Task.FromResult(dummyUser);
            }
            else
            {
                return Task.FromResult(user);
            }
        }

        public override Task<IdentityResult> UpdateAsync(ApplicationUser user)
        {
            return Task.FromResult(IdentityResult.Success);
        }

        public override Task<ApplicationUser> FindByEmailAsync(string email)
        {
            ApplicationUser user = null;
            ApplicationUser dummyUser = new ApplicationUser() { UserName = "spulla", Email = "spulla@agility.com" };
            if (dummyUser.Email == email)
            {
                return Task.FromResult(dummyUser);
            }
            else
            {
                return Task.FromResult(user);
            }
        }

        public override Task<IdentityResult> ChangePasswordAsync(ApplicationUser user, string currentPassword, string newPassword)
        {
            return Task.FromResult(IdentityResult.Success);
        }

        public override Task<string> GeneratePasswordResetTokenAsync(ApplicationUser user)
        {
            return Task.Run(() =>
            {
                return "success";
            });
        }

        public override Task<IdentityResult> ResetPasswordAsync(ApplicationUser user, string token, string newPassword)
        {
            return Task.FromResult(IdentityResult.Success);
        }
    }
}
