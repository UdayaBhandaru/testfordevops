﻿// <copyright file="FakeRoleManager.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.Security
{
    using System.Threading.Tasks;
    using Agility.Framework.Core.Security.Models;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.Extensions.Logging;
    using Moq;

    public class FakeRoleManager : RoleManager<ApplicationRole>
    {
        public FakeRoleManager()
            : base(
                  new Mock<IRoleStore<ApplicationRole>>().Object,
                  new IRoleValidator<ApplicationRole>[0],
                  new Mock<ILookupNormalizer>().Object,
                  new Mock<IdentityErrorDescriber>().Object,
                  new Mock<ILogger<RoleManager<ApplicationRole>>>().Object)
        {
        }

        public override Task<IdentityResult> CreateAsync(ApplicationRole role)
        {
            return Task.FromResult(IdentityResult.Success);
        }

        public override Task<ApplicationRole> FindByIdAsync(string roleId)
        {
            ApplicationRole dummyRole = new ApplicationRole() { Id = "PinkWarrior", Name = "SysAdmin", NormalizedName = "SYSADMIN" };
            return Task.FromResult(dummyRole);
        }

        public override Task<IdentityResult> DeleteAsync(ApplicationRole role)
        {
            return Task.FromResult(IdentityResult.Success);
        }
    }
}