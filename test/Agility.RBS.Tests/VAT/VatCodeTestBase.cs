﻿// <copyright file="VatCodeTestBase.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.Vat
{
    using System.Collections.Generic;
    using Agility.Framework.Core;
    using Agility.Framework.Designer.Mapping;
    using Agility.Framework.Test;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.Framework.Web.Security.Mapping;
    using Agility.RBS.MDM;
    using Agility.RBS.MDM.Brand.Mapping;
    using Agility.RBS.MDM.CostZone.Mapping;
    using Agility.RBS.MDM.Favourite.Mapping;
    using Agility.RBS.MDM.ItemList.Mapping;
    using Agility.RBS.MDM.MerchantHierarchy.Mapping;
    using Agility.RBS.MDM.OrganizationHierarchy.Mapping;
    using Agility.RBS.MDM.Partner.Mapping;
    using Agility.RBS.MDM.PriceZone.Mapping;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Mapping;
    using Agility.RBS.MDM.Season.Mapping;
    using Agility.RBS.MDM.UnitOfMeasure;
    using Agility.RBS.MDM.UnitOfMeasure.Mapping;
    using Agility.RBS.MDM.UnitOfMeasure.Models;
    using Agility.RBS.MDM.UtilityDomain;
    using Agility.RBS.MDM.UtilityDomain.Mapping;
    using Agility.RBS.MDM.UtilityDomain.Models;
    using Agility.RBS.MDM.ValueAddedTax;
    using Agility.RBS.MDM.ValueAddedTax.Mapping;
    using Agility.RBS.MDM.ValueAddedTax.Models;
    using Agility.RBS.SupplierManagement.Supplier.Mapping;
    using Agility.RBS.SupplierManagement.SupplierAddress.Mapping;
    using AutoMapper;
    using Devart.Data.Oracle;
    using Moq;

    public class VatCodeTestBase : RbsFixture
    {
        public VatCodeTestBase(FxFixture fxFixture)
            : base(fxFixture)
        {
            Mock<UtilityDomainRepository> utilityDomainRepository = this.MockBaseOraclePackageRepository<UtilityDomainRepository, DomainDetailModel>(this.VatTestDataModels.DomainDetailModels, new object[] { });
            this.MockBaseOraclePackageRepository<DomainDataRepository, VatCodeModel>(this.VatTestDataModels.VatCodeModel, new object[] { utilityDomainRepository.Object, null });

            Mock<ValueAddedTaxRepository> vatRepository = this.MockBaseOraclePackageRepository<ValueAddedTaxRepository, VatCodeModel>(this.VatTestDataModels.VatCodeModel, new object[] { });
            vatRepository.Setup(x => x.SetParamsVatCodeSave(It.IsAny<VatCodeModel>(), It.IsAny<OracleObject>()))
              .Verifiable();
            vatRepository.Setup(x => x.SetParamVatCodeSearch(It.IsAny<VatCodeModel>(), It.IsAny<OracleObject>()))
               .Verifiable();
            this.Controller = this.Setup<VatCodeController>();
        }

        protected VatCodeController Controller { get; set; }

        protected ServiceDocument<VatCodeModel> ServiceDocument { get; set; }

        protected VatTestDataModels VatTestDataModels { get; set; }

        public override void ConfigureServices(string controllerName)
        {
            base.ConfigureServices("VatCode");
            this.CreateTestData(new List<string>(), new List<string> { "Security.json" });

            this.VatTestDataModels = this.CreateTestData<VatTestDataModels>(new List<string> { "VatCode.json" }, "VAT");

            base.MockFxDbContext<VatCodeModel>("Journey");
        }

        protected override void ComponentStartups(List<ComponentStartup> startups)
        {
            base.ComponentStartups(startups);
            startups.Add(new MdmStartup());
        }

        protected override void InitializeMappings()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<FxMappingProfile>();
                cfg.AddProfile<SecurityMappingProfile>();

                cfg.AddProfile<UnitOfMeasureMapping>();
                cfg.AddProfile<UtilityDomainMapping>();
                cfg.AddProfile<MerchantHierarchyMapping>();
                cfg.AddProfile<OrganizationHierarchyMapping>();
                cfg.AddProfile<ValueAddedTaxMapping>();
                cfg.AddProfile<SeasonMapping>();
                cfg.AddProfile<DomainDataMapping>();
                cfg.AddProfile<SupplierModelMapping>();
                cfg.AddProfile<SupplierAddressModelMapping>();
                cfg.AddProfile<FavouriteMapping>();
                cfg.AddProfile<ItemListMapping>();
                cfg.AddProfile<CostZoneMapping>();
                cfg.AddProfile<PriceZoneMapping>();
                cfg.AddProfile<BrandMapping>();
                cfg.AddProfile<PartnerMapping>();
                this.ConfigureMapping(cfg);
            });
        }
    }
}
