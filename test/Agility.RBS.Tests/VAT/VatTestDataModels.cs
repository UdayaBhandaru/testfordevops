﻿// <copyright file="VatTestDataModels.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.Vat
{
    using System.Collections.Generic;
    using Agility.RBS.MDM.UtilityDomain.Models;
    using Agility.RBS.MDM.ValueAddedTax.Models;

    public class VatTestDataModels
    {
        public List<DomainDetailModel> DomainDetailModels { get; set; }

        public List<VatCodeModel> VatCodeModel { get; set; }
    }
}
