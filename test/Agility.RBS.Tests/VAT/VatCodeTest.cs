﻿// <copyright file="VatCodeTest.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.Vat
{
    using System.Threading.Tasks;
    using Agility.Framework.Test;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.ValueAddedTax.Models;
    using Xunit;

    [Collection("RbsCollection")]
    public class VatCodeTest : VatCodeTestBase
    {
        public VatCodeTest(FxFixture fxFixture)
            : base(fxFixture)
        {
        }

        [Fact]
        public async Task VatCodeList()
        {
            this.ServiceDocument = await this.Controller.List();
            Assert.True(this.ServiceDocument.DomainData.Count == 3);
        }

        [Fact]
        public async Task VatCodeNew()
        {
            this.ServiceDocument = await this.Controller.New();
            Assert.True(this.ServiceDocument.DomainData.Count == 3);
        }

        [Fact]
        public async Task VatCodeSearch()
        {
            this.ServiceDocument = await this.Controller.New();
            Assert.True(this.ServiceDocument.DomainData.Count == 3);
            Assert.True(this.ServiceDocument.LocalizationData.Count == 1);
            this.ServiceDocument.DataProfile.DataModel = new VatCodeModel
            {
                VatCode = "GST"
            };
            this.ServiceDocument = await this.Controller.Search();
            Assert.True(this.ServiceDocument.DataProfile.DataList.Count == 6);
        }

        [Fact]
        public async Task VatCodeSave()
        {
            this.ServiceDocument = await this.Controller.Save();
            Assert.True(this.ServiceDocument.Result.Type == MessageType.Success);
        }
    }
}
