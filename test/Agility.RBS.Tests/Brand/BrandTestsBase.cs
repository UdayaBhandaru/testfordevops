﻿// <copyright file="BrandTestsBase.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.UnitOfMeasure
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.IO;
    using System.Threading.Tasks;
    using Agility.Framework.Core;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Designer.Mapping;
    using Agility.Framework.Test;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.Framework.Web.Security.Mapping;
    using Agility.RBS.Core;
    using Agility.RBS.MDM;
    using Agility.RBS.MDM.Brand;
    using Agility.RBS.MDM.Brand.Mapping;
    using Agility.RBS.MDM.Brand.Models;
    using Agility.RBS.MDM.Brand.Repositories;
    using Agility.RBS.MDM.CostZone.Mapping;
    using Agility.RBS.MDM.Favourite.Mapping;
    using Agility.RBS.MDM.ItemList.Mapping;
    using Agility.RBS.MDM.MerchantHierarchy.Mapping;
    using Agility.RBS.MDM.OrganizationHierarchy.Mapping;
    using Agility.RBS.MDM.Partner.Mapping;
    using Agility.RBS.MDM.PriceZone.Mapping;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Mapping;
    using Agility.RBS.MDM.Repositories.Models;
    using Agility.RBS.MDM.Season.Mapping;
    using Agility.RBS.MDM.UnitOfMeasure;
    using Agility.RBS.MDM.UnitOfMeasure.Mapping;
    using Agility.RBS.MDM.UnitOfMeasure.Models;
    using Agility.RBS.MDM.UtilityDomain;
    using Agility.RBS.MDM.UtilityDomain.Mapping;
    using Agility.RBS.MDM.UtilityDomain.Models;
    using Agility.RBS.MDM.ValueAddedTax.Mapping;
    using Agility.RBS.SupplierManagement.Supplier.Mapping;
    using Agility.RBS.SupplierManagement.SupplierAddress.Mapping;
    using Agility.RBS.Tests.Brand;
    using AutoMapper;
    using Devart.Data.Oracle;
    using Microsoft.Extensions.Caching.Distributed;
    using Microsoft.Extensions.DependencyInjection;
    using Moq;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using Xunit;

    public class BrandTestsBase : RbsFixture
    {
        public BrandTestsBase(FxFixture fxFixture)
            : base(fxFixture)
        {
            Mock<UtilityDomainRepository> utilityDomainRepository = this.MockBaseOraclePackageRepository<UtilityDomainRepository, DomainDetailModel>(this.BrandTestDataModel.DomainDetailModels, new object[] { });
            this.MockBaseOraclePackageRepository<DomainDataRepository, BrandDomainModel>(this.BrandTestDataModel.BrandDomainModels, new object[] { utilityDomainRepository.Object, null });

            Mock<BrandRepository> unitOfMeasureRepository = this.MockBaseOraclePackageRepository<BrandRepository, BrandModel>(this.BrandTestDataModel.BrandModels, new object[] { });
            unitOfMeasureRepository.Setup(x => x.SetParamsBrandSearch(It.IsAny<BrandModel>(), It.IsAny<OracleObject>()))
               .Verifiable();
            unitOfMeasureRepository.Setup(x => x.SetParamsBrandSave(It.IsAny<BrandModel>(), It.IsAny<OracleObject>()))
               .Verifiable();
            this.Controller = this.Setup<BrandController>();
        }

        protected BrandController Controller { get; set; }

        protected ServiceDocument<BrandModel> ServiceDocument { get; set; }

        protected BrandTestDataModel BrandTestDataModel { get; set; }

        public override void ConfigureServices(string controllerName)
        {
            base.ConfigureServices("Brand");
            this.CreateTestData(new List<string>(), new List<string> { "Security.json" });

            this.BrandTestDataModel = this.CreateTestData<BrandTestDataModel>(new List<string> { "Brand.json" }, "Brand");

            base.MockFxDbContext<BrandModel>("Journey");
        }

        protected override void ComponentStartups(List<ComponentStartup> startups)
        {
            base.ComponentStartups(startups);
            startups.Add(new MdmStartup());
        }

        protected override void InitializeMappings()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<FxMappingProfile>();
                cfg.AddProfile<SecurityMappingProfile>();

                cfg.AddProfile<UnitOfMeasureMapping>();
                cfg.AddProfile<UtilityDomainMapping>();
                cfg.AddProfile<MerchantHierarchyMapping>();
                cfg.AddProfile<OrganizationHierarchyMapping>();
                cfg.AddProfile<ValueAddedTaxMapping>();
                cfg.AddProfile<SeasonMapping>();
                cfg.AddProfile<DomainDataMapping>();
                cfg.AddProfile<SupplierModelMapping>();
                cfg.AddProfile<SupplierAddressModelMapping>();
                cfg.AddProfile<FavouriteMapping>();
                cfg.AddProfile<ItemListMapping>();
                cfg.AddProfile<CostZoneMapping>();
                cfg.AddProfile<PriceZoneMapping>();
                cfg.AddProfile<BrandMapping>();
                cfg.AddProfile<PartnerMapping>();
                this.ConfigureMapping(cfg);
            });
        }
    }
}
