﻿// <copyright file="BrandCategoryTest.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.Brand
{
    using System.Threading.Tasks;
    using Agility.Framework.Test;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Brand.Models;
    using Agility.RBS.MDM.UnitOfMeasure.Models;
    using Agility.RBS.Tests.UnitOfMeasure;
    using Xunit;

    [Collection("RbsCollection")]
    public class BrandCategoryTest : BrandCategoryTestsBase
    {
        public BrandCategoryTest(FxFixture fxFixture)
            : base(fxFixture)
        {
        }

        [Fact]
        public async Task BrandCategoryList()
        {
            ServiceDocument<BrandCategoryModel> serviceDocument = await this.Controller.List();
            Assert.True(serviceDocument.DomainData.Count == 1);
        }

        [Fact]
        public async Task BrandCategoryNew()
        {
            ServiceDocument<BrandCategoryModel> serviceDocument = await this.Controller.New();
            Assert.True(serviceDocument.DomainData.Count == 2);
        }

        [Fact]
        public async Task BrandCategorySearch()
        {
            ServiceDocument<BrandCategoryModel> serviceDocument = await this.Controller.New();
            Assert.True(serviceDocument.DomainData.Count == 2);
            Assert.True(serviceDocument.LocalizationData.Count == 1);
            serviceDocument.DataProfile.DataModel = new BrandCategoryModel
            {
                BrandCatId = 321
            };
            serviceDocument = await this.Controller.Search();
            Assert.True(serviceDocument.DataProfile.DataList.Count == 2);
        }

        [Fact]
        public async Task BrandCategorySave()
        {
            ServiceDocument<BrandCategoryModel> serviceDocument = await this.Controller.Save();
            Assert.True(serviceDocument.Result.Type == MessageType.Success);
        }
    }
}
