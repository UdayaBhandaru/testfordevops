﻿// <copyright file="BrandTestDataModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.Brand
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.RBS.Core.Models;
    using Agility.RBS.MDM.Brand.Models;
    using Agility.RBS.MDM.Repositories.Models;
    using Agility.RBS.MDM.UnitOfMeasure.Models;
    using Agility.RBS.MDM.UtilityDomain.Models;

    public class BrandTestDataModel
    {
        public List<DomainDetailModel> DomainDetailModels { get; set; }

        public List<BrandDomainModel> BrandDomainModels { get; set; }

        public List<BrandModel> BrandModels { get; set; }

        public List<BrandCategoryModel> BrandCategoryModels { get; set; }

        public List<CommonModel> CommonModels { get; set; }
    }
}
