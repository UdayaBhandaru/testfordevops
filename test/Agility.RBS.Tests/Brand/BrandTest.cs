﻿// <copyright file="BrandTest.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.Brand
{
    using System.Threading.Tasks;
    using Agility.Framework.Test;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Brand.Models;
    using Agility.RBS.MDM.UnitOfMeasure.Models;
    using Agility.RBS.Tests.UnitOfMeasure;
    using Xunit;

    [Collection("RbsCollection")]
    public class BrandTest : BrandTestsBase
    {
        public BrandTest(FxFixture fxFixture)
            : base(fxFixture)
        {
        }

        [Fact]
        public async Task BrandList()
        {
            ServiceDocument<BrandModel> serviceDocument = await this.Controller.List();
            Assert.True(serviceDocument.DomainData.Count == 1);
        }

        [Fact]
        public async Task BrandNew()
        {
            ServiceDocument<BrandModel> serviceDocument = await this.Controller.New();
            Assert.True(serviceDocument.DomainData.Count == 1);
        }

        [Fact]
        public async Task BrandSearch()
        {
            ServiceDocument<BrandModel> serviceDocument = await this.Controller.New();
            Assert.True(serviceDocument.DomainData.Count == 1);
            Assert.True(serviceDocument.LocalizationData.Count == 1);
            serviceDocument.DataProfile.DataModel = new BrandModel
            {
                BrandName = "SONY"
            };
            serviceDocument = await this.Controller.Search();
            Assert.True(serviceDocument.DataProfile.DataList.Count == 2);
        }

        [Fact]
        public async Task BrandSave()
        {
            ServiceDocument<BrandModel> serviceDocument = await this.Controller.Save();
            Assert.True(serviceDocument.Result.Type == MessageType.Success);
        }
    }
}
