﻿// <copyright file="ItemSelectTest.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.Item
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Test;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.ItemManagement.Item.Models;
    using Xunit;

    [Collection("RbsCollection")]
    public class ItemSelectTest : ItemSelectTestsBase
    {
        public ItemSelectTest(FxFixture fxFixture)
            : base(fxFixture)
        {
        }

        [Fact]
        public async Task ItemSelectList()
        {
            this.ServiceDocument = await this.Controller.List();
            Assert.True(this.ServiceDocument.DomainData.Count == 12);
        }

        [Fact]
        public async Task ItemSelectSearch()
        {
            this.ServiceDocument = await this.Controller.Search();
            Assert.True(this.ServiceDocument.Result.Type == MessageType.Success);
        }

        [Fact]
        public async Task GetCostChgItemList()
        {
            ItemSelectModel itemSelectModel = new ItemSelectModel
            {
                ItemBarcode = "100002574",
                SupplierId = 1
            };
            List<CostChgItemSelectModel> costChgItems = await this.Controller.GetCostChgItemList(itemSelectModel);
            Assert.True(costChgItems.Count > 0);
        }

        [Fact]
        public async Task GetPriceChgItemList()
        {
            ItemSelectModel itemSelectModel = new ItemSelectModel
            {
                ItemBarcode = "100002574",
                SupplierId = 1
            };
            List<PriceChgItemSelectModel> priceChgItems = await this.Controller.GetPriceChgItemList(itemSelectModel);
            Assert.True(priceChgItems.Count > 0);
        }
    }
}
