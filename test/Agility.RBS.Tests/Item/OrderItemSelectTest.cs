﻿// <copyright file="OrderItemSelectTest.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.Item
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Test;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Repositories.Models;
    using Xunit;

    [Collection("RbsCollection")]
    public class OrderItemSelectTest : OrderItemSelectTestsBase
    {
        public OrderItemSelectTest(FxFixture fxFixture)
            : base(fxFixture)
        {
        }

        [Fact]
        public async Task OrderItemSelectList()
        {
            this.ServiceDocument = await this.Controller.List();
            Assert.True(this.ServiceDocument.DomainData.Count == 5);
        }

        [Fact]
        public async Task OrderItemSelectSearch()
        {
            this.ServiceDocument = await this.Controller.Search();
            Assert.True(this.ServiceDocument.Result.Type == MessageType.Success);
        }

        [Fact]
        public async Task CategoryGet()
        {
            List<DepartmentDomainModel> categories = await this.Controller.CategoryGet(1, 1);
            Assert.True(categories.Count > 0);
        }
    }
}
