﻿// <copyright file="ItemTestDataModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.Item
{
    using System.Collections.Generic;
    using Agility.RBS.Core.Models;
    using Agility.RBS.ItemManagement.Item.Models;
    using Agility.RBS.MDM.Repositories.Models;
    using Agility.RBS.MDM.UtilityDomain.Models;

    public class ItemTestDataModel
    {
        public List<DomainDetailModel> DomainDetails { get; set; }

        public List<UomClassDomainModel> UomClassDomains { get; set; }

        public List<CompanyDomainModel> CompanyDomains { get; set; }

        public List<LocationDomainModel> LocationDomains { get; set; }

        public List<FashionItemModel> FashionItems { get; set; }

        public List<ItemBarcodeModel> ItemBarcodes { get; set; }

        public List<ItemCompanyModel> ItemCompanies { get; set; }

        public List<ItemCountryModel> ItemCountries { get; set; }

        public List<ItemFormatModel> ItemFormats { get; set; }

        public List<ItemRegionModel> ItemRegions { get; set; }

        public List<CommonModel> CommonModels { get; set; }

        public List<VatCodeDomainModel> VatCodeDomains { get; set; }

        public List<CountryDomainModel> CountryDomains { get; set; }

        public List<OrgCountryDomainModel> OrgCountryDomains { get; set; }

        public List<SupplierDomainModel> SupplierDomains { get; set; }

        public List<FormatDomainModel> FormatDomains { get; set; }

        public List<ItemLocationModel> ItemLocations { get; set; }

        public List<ItemPackDetailsModel> ItemPackDetails { get; set; }

        public List<ItemBasicInfoModel> ItemBasicInfos { get; set; }

        public List<RegionDomainModel> RegionDomains { get; set; }

        public List<DivisionDomainModel> DivisionDomains { get; set; }

        public List<DepartmentDomainModel> DepartmentDomains { get; set; }

        public List<DepartmentDomainModel> CategoryDomains { get; set; }

        public List<BrandDomainModel> BrandDomains { get; set; }

        public List<ItemListDomainModel> ItemListDomains { get; set; }

        public List<ItemSelectModel> ItemSelects { get; set; }

        public List<CostChgItemSelectModel> CostChgItemSelects { get; set; }

        public List<PriceChgItemSelectModel> PriceChgItemSelects { get; set; }

        public List<ItemSuppCountryHtsModel> ItemSuppCountryHtss { get; set; }

        public List<ItemSupplierModel> ItemSuppliers { get; set; }

        public List<ItemSupplierCountryModel> ItemSupplierCountries { get; set; }

        public List<ItemSupplierCompanyModel> ItemSupplierCompanies { get; set; }

        public List<ItemSuppCountryLocationModel> ItemSuppCountryLocations { get; set; }

        public List<VatRegionDomainModel> VatRegionDomains { get; set; }

        public List<ItemSupplierCountryDimensionModel> ItemSupplierCountryDimensions { get; set; }

        public List<UomDomainModel> UomDomains { get; set; }

        public List<ItemVatModel> ItemVats { get; set; }

        public List<OrderItemSelectModel> OrderItemSelects { get; set; }
    }
}
