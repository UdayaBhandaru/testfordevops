﻿// <copyright file="OrderItemSelectTestsBase.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.Item
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Core;
    using Agility.Framework.Designer.Mapping;
    using Agility.Framework.Test;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.Framework.Web.Security.Mapping;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Models;
    using Agility.RBS.ItemManagement;
    using Agility.RBS.ItemManagement.Item;
    using Agility.RBS.ItemManagement.Item.Mapping;
    using Agility.RBS.ItemManagement.Item.Models;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Models;
    using Agility.RBS.MDM.UtilityDomain;
    using Agility.RBS.MDM.UtilityDomain.Models;
    using AutoMapper;
    using Devart.Data.Oracle;
    using Moq;

    public class OrderItemSelectTestsBase : RbsFixture
    {
        public OrderItemSelectTestsBase(FxFixture fxFixture)
            : base(fxFixture)
        {
            Mock<UtilityDomainRepository> utilityDomainRepository = this.MockBaseOraclePackageRepository<UtilityDomainRepository, DomainDetailModel>(this.ItemTestDataModel.DomainDetails, new object[] { });
            Mock<DomainDataRepository> domainDataRepository = this.MockBaseOraclePackageRepository<DomainDataRepository, SupplierDomainModel>(this.ItemTestDataModel.SupplierDomains, new object[] { utilityDomainRepository.Object, null });

            domainDataRepository.Setup(x => x.GetProcedure2<DivisionDomainModel>(It.IsAny<OracleObject>(), It.IsAny<PackageParams>(), It.IsAny<ServiceDocumentResult>(), It.IsAny<OracleParameterCollection>()))
              .Returns(Task.FromResult(this.ItemTestDataModel.DivisionDomains));
            domainDataRepository.Setup(x => x.GetProcedure2<DepartmentDomainModel>(It.IsAny<OracleObject>(), It.IsAny<PackageParams>(), It.IsAny<ServiceDocumentResult>(), It.IsAny<OracleParameterCollection>()))
            .Returns(Task.FromResult(this.ItemTestDataModel.DepartmentDomains));
            domainDataRepository.Setup(x => x.GetProcedure2<LocationDomainModel>(It.IsAny<OracleObject>(), It.IsAny<PackageParams>(), It.IsAny<ServiceDocumentResult>(), It.IsAny<OracleParameterCollection>()))
             .Returns(Task.FromResult(this.ItemTestDataModel.LocationDomains));
            domainDataRepository.Setup(x => x.GetProcedure2<ItemListDomainModel>(It.IsAny<OracleObject>(), It.IsAny<PackageParams>(), It.IsAny<ServiceDocumentResult>(), It.IsAny<OracleParameterCollection>()))
           .Returns(Task.FromResult(this.ItemTestDataModel.ItemListDomains));

            domainDataRepository.Setup(x => x.GetProcedure2<DepartmentDomainModel>(It.IsAny<OracleObject>(), It.IsAny<PackageParams>(), It.IsAny<ServiceDocumentResult>(), It.IsAny<OracleParameterCollection>()))
            .Returns(Task.FromResult(this.ItemTestDataModel.CategoryDomains));

            Mock<ItemSelectRepository> itemRepository = this.MockBaseOraclePackageRepository<ItemSelectRepository, OrderItemSelectModel>(this.ItemTestDataModel.OrderItemSelects, new object[] { });
            itemRepository.Setup(x => x.SetParamsSelectOrderItemsList(It.IsAny<OrderItemSelectModel>()))
               .Verifiable();
            this.Controller = this.Setup<OrderItemSelectController>();
        }

        protected OrderItemSelectController Controller { get; set; }

        protected ServiceDocument<OrderItemSelectModel> ServiceDocument { get; set; }

        protected ItemTestDataModel ItemTestDataModel { get; set; }

        public override void ConfigureServices(string controllerName)
        {
            base.ConfigureServices("ItemFormat");
            this.CreateTestData(new List<string>(), new List<string> { "Security.json" });
            this.ItemTestDataModel = this.CreateTestData<ItemTestDataModel>(new List<string> { "Item.json" }, "Item");
        }

        protected override void ComponentStartups(List<ComponentStartup> startups)
        {
            base.ComponentStartups(startups);
            startups.Add(new ItemManagementStartup());
        }

        protected override void InitializeMappings()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<FxMappingProfile>();
                cfg.AddProfile<SecurityMappingProfile>();

                cfg.AddProfile<ItemMapping>();
                this.ConfigureMapping(cfg);
            });
        }
    }
}
