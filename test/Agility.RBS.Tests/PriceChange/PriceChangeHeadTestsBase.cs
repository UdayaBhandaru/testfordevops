﻿// <copyright file="PriceChangeHeadTestsBase.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.PriceChange
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Core;
    using Agility.Framework.Core.Common;
    using Agility.Framework.Core.Profile.Entities;
    using Agility.Framework.Designer.Mapping;
    using Agility.Framework.Test;
    using Agility.Framework.Web.Core.DbContexts;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.Framework.Web.Security.Mapping;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Models;
    using Agility.RBS.CostManagement.CostChange;
    using Agility.RBS.CostManagement.CostChange.Models;
    using Agility.RBS.Inbox.Models;
    using Agility.RBS.Inbox.Repository;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Models;
    using Agility.RBS.MDM.UtilityDomain;
    using Agility.RBS.MDM.UtilityDomain.Models;
    using Agility.RBS.PriceManagement;
    using Agility.RBS.PriceManagement.Controllers;
    using Agility.RBS.PriceManagement.Mapping;
    using Agility.RBS.PriceManagement.Models;
    using Agility.RBS.PriceManagement.Repository;
    using AutoMapper;
    using Devart.Data.Oracle;
    using Moq;

    public class PriceChangeHeadTestsBase : RbsFixture
    {
        public PriceChangeHeadTestsBase(FxFixture fxFixture)
            : base(fxFixture)
        {
            Mock<UtilityDomainRepository> utilityDomainRepository = this.MockBaseOraclePackageRepository<UtilityDomainRepository, DomainDetailModel>(this.PriceTestDataModel.DomainDetails, new object[] { });
            Mock<DomainDataRepository> domainDataRepository = this.MockBaseOraclePackageRepository<DomainDataRepository, CostChangeReasonDomainModel>(this.PriceTestDataModel.CostChangeReasonDomains, new object[] { utilityDomainRepository.Object, null });

            domainDataRepository.Setup(x => x.GetProcedure2<CommonModel>(It.IsAny<OracleObject>(), It.IsAny<PackageParams>(), It.IsAny<ServiceDocumentResult>(), It.IsAny<OracleParameterCollection>()))
                .Returns(Task.FromResult(this.PriceTestDataModel.CommonModels));

            domainDataRepository.Setup(x => x.GetProcedure2<InboxCommonModel>(It.IsAny<OracleObject>(), It.IsAny<PackageParams>(), It.IsAny<ServiceDocumentResult>(), It.IsAny<OracleParameterCollection>()))
               .Returns(Task.FromResult(this.PriceTestDataModel.InboxCommons));

            Mock<PriceManagementRepository> priceManagementRepository = this.MockBaseOraclePackageRepository<PriceManagementRepository, PriceChangeHeadModel>(this.PriceTestDataModel.PriceChangeHeads, new object[] { });
            priceManagementRepository.Setup(x => x.SetParamsPriceChangeHead(It.IsAny<PriceChangeHeadModel>()))
               .Verifiable();
            priceManagementRepository.Setup(x => x.SetPriceChangeRequestId(It.IsAny<long>(), It.IsAny<OracleObject>()))
               .Verifiable();
            Mock<InboxRepository> inboxRepository = this.MockBaseOraclePackageRepository<InboxRepository, InboxModel>(this.PriceTestDataModel.Inboxs, new object[] { null });
            inboxRepository.Setup(x => x.SetParamsInbox(It.IsAny<InboxModel>(), It.IsAny<ServiceDocument<PriceChangeHeadModel>>())).Verifiable();
            this.Controller = this.Setup<PriceChangeHeadController>();
            this.ServiceDocument = new ServiceDocument<PriceChangeHeadModel>();
            this.ServiceDocument.New(false);
            this.ServiceDocument.DataProfile.DataModel.WorkflowInstance.WorkflowInstanceId = this.DbContext.Object.GetNextValueOfSequence(CoreSettings.CoreConfiguration.ConnectionStrings.App.Schema, this.ServiceDocument.DataProfile.DbContext.ProfileRepository.ProfileInfo, null, true);
            this.DbContext.Object.Set<PriceChangeHeadModel>().Add(this.ServiceDocument.DataProfile.DataModel);
            this.DbContext.Object.SaveChanges();
        }

        protected Mock<FxDbContext<PriceChangeHeadModel>> DbContext { get; set; }

        protected PriceChangeHeadController Controller { get; set; }

        protected ServiceDocument<PriceChangeHeadModel> ServiceDocument { get; set; }

        protected PriceTestDataModel PriceTestDataModel { get; set; }

        public override void ConfigureServices(string controllerName)
        {
            base.ConfigureServices("PriceChangeHead");
            this.CreateTestData(new List<string> { "PriceChange_Profile.json" }, new List<string> { "SecurityLatest.json" });
            this.PriceTestDataModel = this.CreateTestData<PriceTestDataModel>(new List<string> { "Price.json" }, "PriceChange");
            this.DbContext = base.MockFxDbContext<PriceChangeHeadModel>("PriceChangeHead");

            ////dbContext.Object.Set<PriceChangeHeadWfModel>().Add(new PriceChangeHeadWfModel
            ////{
            ////    WorkflowInstanceId = 0,
            ////    DataProfileId = new Guid("2101b8ad-ed79-4839-b0c2-9756a103b058"),
            ////    ProfileInstanceId = 1,
            ////    PreviousWorkflowStateId = new Guid("2101b8ad-ed79-4839-b0c2-9756a103b058"),
            ////    ToWorkflowStateId = new Guid("4a920f39-aae2-45b0-beac-d4ef1bdcaee9"),
            ////    WorkflowStateId = new Guid("4a920f39-aae2-45b0-beac-d4ef1bdcaee9"),
            ////    StateId = "4a920f39-aae2-45b0-beac-d4ef1bdcaee9",
            ////    StateName = "Worksheet",
            ////    IsCompleted = false
            ////});
        }

        protected override void ComponentStartups(List<ComponentStartup> startups)
        {
            base.ComponentStartups(startups);
            startups.Add(new PriceManagementStartup());
        }

        protected override void InitializeMappings()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<FxMappingProfile>();
                cfg.AddProfile<SecurityMappingProfile>();

                cfg.AddProfile<PriceManagementModelMapping>();
                this.ConfigureMapping(cfg);
            });
        }
    }
}
