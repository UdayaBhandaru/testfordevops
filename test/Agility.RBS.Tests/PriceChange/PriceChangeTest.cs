﻿// <copyright file="PriceChangeTest.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.PriceChange
{
    using System.Threading.Tasks;
    using Agility.Framework.Test;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.PriceManagement.Models;
    using Xunit;

    [Collection("RbsCollection")]
    public class PriceChangeTest : PriceChangeTestsBase
    {
        public PriceChangeTest(FxFixture fxFixture)
            : base(fxFixture)
        {
        }

        [Fact]
        public async Task PriceChangeList()
        {
            this.ServiceDocument = await this.Controller.List();
            Assert.True(this.ServiceDocument.DomainData.Count == 6);
        }

        [Fact]
        public async Task PriceChangeSearch()
        {
            this.ServiceDocument = await this.Controller.Search();
            Assert.True(this.ServiceDocument.Result.Type == MessageType.Success);
        }
    }
}
