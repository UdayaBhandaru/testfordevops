﻿// <copyright file="PriceTestDataModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.PriceChange
{
    using System.Collections.Generic;
    using Agility.RBS.Core.Models;
    using Agility.RBS.CostManagement.CostChange.Models;
    using Agility.RBS.Inbox.Models;
    using Agility.RBS.ItemManagement.Item.Models;
    using Agility.RBS.MDM.Repositories.Models;
    using Agility.RBS.MDM.UtilityDomain.Models;
    using Agility.RBS.PriceManagement.Models;

    public class PriceTestDataModel
    {
        public List<DomainDetailModel> DomainDetails { get; set; }

        public List<UsersDomainModel> UsersDomains { get; set; }

        public List<WorkflowStatusDomainModel> WorkflowStatusDomains { get; set; }

        public List<CommonModel> CommonModels { get; set; }

        public List<SupplierDomainModel> SupplierDomains { get; set; }

        public List<PriceChangeSearchModel> PriceChangeSearchs { get; set; }

        public List<CostChangeReasonDomainModel> CostChangeReasonDomains { get; set; }

        public List<PriceChangeHeadModel> PriceChangeHeads { get; set; }

        public List<InboxModel> Inboxs { get; set; }

        public List<InboxCommonModel> InboxCommons { get; set; }

        public List<CountryDomainModel> CountryDomains { get; set; }

        public List<CostChgItemSelectModel> CostChgItemSelects { get; set; }

        public List<CostChangeDetailModel> CostChangeDetails { get; set; }
    }
}
