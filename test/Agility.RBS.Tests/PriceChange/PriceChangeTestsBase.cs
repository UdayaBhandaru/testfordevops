﻿// <copyright file="PriceChangeTestsBase.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.PriceChange
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Agility.Framework.Core;
    using Agility.Framework.Designer.Mapping;
    using Agility.Framework.Test;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.Framework.Web.Security.Mapping;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Models;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Models;
    using Agility.RBS.MDM.UtilityDomain;
    using Agility.RBS.MDM.UtilityDomain.Models;
    using Agility.RBS.PriceManagement;
    using Agility.RBS.PriceManagement.Controllers;
    using Agility.RBS.PriceManagement.Mapping;
    using Agility.RBS.PriceManagement.Models;
    using Agility.RBS.PriceManagement.Repository;
    using AutoMapper;
    using Devart.Data.Oracle;
    using Moq;

    public class PriceChangeTestsBase : RbsFixture
    {
        public PriceChangeTestsBase(FxFixture fxFixture)
            : base(fxFixture)
        {
            Mock<UtilityDomainRepository> utilityDomainRepository = this.MockBaseOraclePackageRepository<UtilityDomainRepository, DomainDetailModel>(this.PriceTestDataModel.DomainDetails, new object[] { });
            Mock<DomainDataRepository> domainDataRepository = this.MockBaseOraclePackageRepository<DomainDataRepository, SupplierDomainModel>(this.PriceTestDataModel.SupplierDomains, new object[] { utilityDomainRepository.Object, null });
            domainDataRepository.Setup(x => x.GetProcedure2<CommonModel>(It.IsAny<OracleObject>(), It.IsAny<PackageParams>(), It.IsAny<ServiceDocumentResult>(), It.IsAny<OracleParameterCollection>()))
                .Returns(Task.FromResult(this.PriceTestDataModel.CommonModels));
            domainDataRepository.Setup(x => x.GetProcedure2<UsersDomainModel>(It.IsAny<OracleObject>(), It.IsAny<PackageParams>(), It.IsAny<ServiceDocumentResult>(), It.IsAny<OracleParameterCollection>()))
                .Returns(Task.FromResult(this.PriceTestDataModel.UsersDomains));
            domainDataRepository.Setup(x => x.GetProcedure2<WorkflowStatusDomainModel>(It.IsAny<OracleObject>(), It.IsAny<PackageParams>(), It.IsAny<ServiceDocumentResult>(), It.IsAny<OracleParameterCollection>()))
                .Returns(Task.FromResult(this.PriceTestDataModel.WorkflowStatusDomains));

            Mock<PriceManagementRepository> priceManagementRepository = this.MockBaseOraclePackageRepository<PriceManagementRepository, PriceChangeSearchModel>(this.PriceTestDataModel.PriceChangeSearchs, new object[] { });
            priceManagementRepository.Setup(x => x.SetParamsPriceChangeHeadList(It.IsAny<PriceChangeSearchModel>()))
               .Verifiable();
            this.Controller = this.Setup<PriceChangeSearchController>();
        }

        protected PriceChangeSearchController Controller { get; set; }

        protected ServiceDocument<PriceChangeSearchModel> ServiceDocument { get; set; }

        protected PriceTestDataModel PriceTestDataModel { get; set; }

        public override void ConfigureServices(string controllerName)
        {
            base.ConfigureServices("PriceChangeSearch");
            this.CreateTestData(new List<string>(), new List<string> { "Security.json" });
            this.PriceTestDataModel = this.CreateTestData<PriceTestDataModel>(new List<string> { "Price.json" }, "PriceChange");
        }

        protected override void ComponentStartups(List<ComponentStartup> startups)
        {
            base.ComponentStartups(startups);
            startups.Add(new PriceManagementStartup());
        }

        protected override void InitializeMappings()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<FxMappingProfile>();
                cfg.AddProfile<SecurityMappingProfile>();

                cfg.AddProfile<PriceManagementModelMapping>();
                this.ConfigureMapping(cfg);
            });
        }
    }
}
