﻿// <copyright file="PriceChangeHeadTest.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.PriceChange
{
    using System;
    using System.Threading.Tasks;
    using Agility.Framework.Test;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.Core.Models;
    using Agility.RBS.PriceManagement.Models;
    using Xunit;

    [Collection("RbsCollection")]
    public class PriceChangeHeadTest : PriceChangeHeadTestsBase
    {
        public PriceChangeHeadTest(FxFixture fxFixture)
            : base(fxFixture)
        {
        }

        [Fact]
        public async Task PriceChangeHeadNew()
        {
            this.ServiceDocument = await this.Controller.New();
            Assert.True(this.ServiceDocument.DomainData.Count == 2);
        }

        [Fact]
        public async Task PriceChangeHeadOpen()
        {
            await this.PriceChangeHeadSubmit();
            this.ServiceDocument = await this.Controller.Open(1);
            Assert.True(this.ServiceDocument.Result.Type == MessageType.Success);
        }

        [Fact]
        public async Task PriceChangeHeadSave()
        {
            this.ServiceDocument = await this.Controller.New();
            this.ServiceDocument.DataProfile.DataModel = new PriceChangeHeadModel
            {
                PriceStatus = "A",
                PRICE_CHANGE_ID = 1
            };
            this.ServiceDocument = await this.Controller.Save(this.ServiceDocument);
            Assert.True(this.ServiceDocument.Result.Type == MessageType.Success);
        }

        [Fact]
        public async Task PriceChangeHeadSubmit()
        {
            this.ServiceDocument = new ServiceDocument<PriceChangeHeadModel>();
            this.ServiceDocument.New(false);
            this.ServiceDocument.DataProfile.DataModel.PRICE_CHANGE_ID = 1;
            this.ServiceDocument.DataProfile.ActionService.CurrentActionId = "PriceStartToCreatedClaim";
            this.ServiceDocument = await this.Controller.Submit(this.ServiceDocument);
            Assert.True(this.ServiceDocument.Result.Type == MessageType.Success);
        }

        [Fact]
        public async Task PriceChangeHeadReject()
        {
            this.ServiceDocument = await this.Controller.Open(1);
            Assert.True(this.ServiceDocument.Result.Type == MessageType.Success);
            this.ServiceDocument.DataProfile.DataModel.WorkflowForm = new RbWfHeaderModel { StateId = "63335002-9b11-9788-9d09-2da73e20eca5" };
            this.ServiceDocument = await this.Controller.Reject(this.ServiceDocument);
            Assert.True(this.ServiceDocument.Result.Type == MessageType.Success);
        }
    }
}
