﻿// <copyright file="PartnerTest.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.Partner
{
    using System.Threading.Tasks;
    using Agility.Framework.Test;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.RBS.MDM.Brand.Models;
    using Agility.RBS.MDM.Partner.Models;
    using Agility.RBS.MDM.UnitOfMeasure.Models;
    using Agility.RBS.Tests.UnitOfMeasure;
    using Xunit;

    [Collection("RbsCollection")]
    public class PartnerTest : PartnerTestsBase
    {
        public PartnerTest(FxFixture fxFixture)
            : base(fxFixture)
        {
        }

        [Fact]
        public async Task PartnerList()
        {
            ServiceDocument<PartnerModel> serviceDocument = await this.Controller.List();
            Assert.True(serviceDocument.DomainData.Count == 9);
        }

        [Fact]
        public async Task PartnerNew()
        {
            ServiceDocument<PartnerModel> serviceDocument = await this.Controller.New();
            Assert.True(serviceDocument.DomainData.Count == 9);
        }

        [Fact]
        public async Task PartnerSearch()
        {
            ServiceDocument<PartnerModel> serviceDocument = await this.Controller.New();
            Assert.True(serviceDocument.DomainData.Count == 9);
            Assert.True(serviceDocument.LocalizationData.Count == 1);
            serviceDocument.DataProfile.DataModel = new PartnerModel
            {
                PartnerId = "1"
            };
            serviceDocument = await this.Controller.Search();
            Assert.True(serviceDocument.DataProfile.DataList.Count == 2);
        }

        [Fact]
        public async Task PartnerSave()
        {
            ServiceDocument<PartnerModel> serviceDocument = await this.Controller.Save();
            Assert.True(serviceDocument.Result.Type == MessageType.Success);
        }
    }
}
