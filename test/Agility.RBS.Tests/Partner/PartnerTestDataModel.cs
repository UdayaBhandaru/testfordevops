﻿// <copyright file="PartnerTestDataModel.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.Partner
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Agility.RBS.Core.Models;
    using Agility.RBS.MDM.Brand.Models;
    using Agility.RBS.MDM.Partner.Models;
    using Agility.RBS.MDM.Repositories.Models;
    using Agility.RBS.MDM.UnitOfMeasure.Models;
    using Agility.RBS.MDM.UtilityDomain.Models;

    public class PartnerTestDataModel
    {
        public List<DomainDetailModel> DomainDetailModels { get; set; }

        public List<CountryDomainModel> CountryDomains { get; set; }

        public List<LocationDomainModel> LocationDomains { get; set; }

        public List<CurrencyDomainModel> CurrencyDomains { get; set; }

        public List<LanguageDomainModel> LanguageDomains { get; set; }

        public List<VatRegionDomainModel> VatRegionDomains { get; set; }

        public List<TermsDomainModel> TermsDomains { get; set; }

        public List<PartnerModel> PartnerModels { get; set; }
    }
}
