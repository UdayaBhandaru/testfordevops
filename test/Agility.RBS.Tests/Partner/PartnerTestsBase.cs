﻿// <copyright file="PartnerTestsBase.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Agility.RBS.Tests.UnitOfMeasure
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.IO;
    using System.Threading.Tasks;
    using Agility.Framework.Core;
    using Agility.Framework.Core.Context;
    using Agility.Framework.Designer.Mapping;
    using Agility.Framework.Test;
    using Agility.Framework.Web.Core.ServiceDocument;
    using Agility.Framework.Web.Security.Mapping;
    using Agility.RBS.Core;
    using Agility.RBS.Core.Models;
    using Agility.RBS.MDM;
    using Agility.RBS.MDM.Brand;
    using Agility.RBS.MDM.Brand.Mapping;
    using Agility.RBS.MDM.Brand.Models;
    using Agility.RBS.MDM.Brand.Repositories;
    using Agility.RBS.MDM.BrandCategory;
    using Agility.RBS.MDM.CostZone.Mapping;
    using Agility.RBS.MDM.Favourite.Mapping;
    using Agility.RBS.MDM.ItemList.Mapping;
    using Agility.RBS.MDM.MerchantHierarchy.Mapping;
    using Agility.RBS.MDM.OrganizationHierarchy.Mapping;
    using Agility.RBS.MDM.Partner;
    using Agility.RBS.MDM.Partner.Mapping;
    using Agility.RBS.MDM.Partner.Models;
    using Agility.RBS.MDM.Partner.Repositories;
    using Agility.RBS.MDM.PriceZone.Mapping;
    using Agility.RBS.MDM.Repositories;
    using Agility.RBS.MDM.Repositories.Mapping;
    using Agility.RBS.MDM.Repositories.Models;
    using Agility.RBS.MDM.Season.Mapping;
    using Agility.RBS.MDM.UnitOfMeasure;
    using Agility.RBS.MDM.UnitOfMeasure.Mapping;
    using Agility.RBS.MDM.UnitOfMeasure.Models;
    using Agility.RBS.MDM.UtilityDomain;
    using Agility.RBS.MDM.UtilityDomain.Mapping;
    using Agility.RBS.MDM.UtilityDomain.Models;
    using Agility.RBS.MDM.ValueAddedTax.Mapping;
    using Agility.RBS.SupplierManagement.Supplier.Mapping;
    using Agility.RBS.SupplierManagement.SupplierAddress.Mapping;
    using Agility.RBS.Tests.Brand;
    using Agility.RBS.Tests.Partner;
    using AutoMapper;
    using Devart.Data.Oracle;
    using Microsoft.Extensions.Caching.Distributed;
    using Microsoft.Extensions.DependencyInjection;
    using Moq;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using Xunit;

    public class PartnerTestsBase : RbsFixture
    {
        public PartnerTestsBase(FxFixture fxFixture)
            : base(fxFixture)
        {
            Mock<UtilityDomainRepository> utilityDomainRepository = this.MockBaseOraclePackageRepository<UtilityDomainRepository, DomainDetailModel>(this.PartnerTestDataModel.DomainDetailModels, new object[] { });
            Mock<DomainDataRepository> domainDataRepository = this.MockBaseOraclePackageRepository<DomainDataRepository, LocationDomainModel>(this.PartnerTestDataModel.LocationDomains, new object[] { utilityDomainRepository.Object, null });
            domainDataRepository.Setup(x => x.GetProcedure2<CountryDomainModel>(It.IsAny<OracleObject>(), It.IsAny<PackageParams>(), It.IsAny<ServiceDocumentResult>(), It.IsAny<OracleParameterCollection>()))
               .Returns(Task.FromResult(this.PartnerTestDataModel.CountryDomains));

            domainDataRepository.Setup(x => x.GetProcedure2<CurrencyDomainModel>(It.IsAny<OracleObject>(), It.IsAny<PackageParams>(), It.IsAny<ServiceDocumentResult>(), It.IsAny<OracleParameterCollection>()))
         .Returns(Task.FromResult(this.PartnerTestDataModel.CurrencyDomains));

            domainDataRepository.Setup(x => x.GetProcedure2<LanguageDomainModel>(It.IsAny<OracleObject>(), It.IsAny<PackageParams>(), It.IsAny<ServiceDocumentResult>(), It.IsAny<OracleParameterCollection>()))
.Returns(Task.FromResult(this.PartnerTestDataModel.LanguageDomains));

            domainDataRepository.Setup(x => x.GetProcedure2<VatRegionDomainModel>(It.IsAny<OracleObject>(), It.IsAny<PackageParams>(), It.IsAny<ServiceDocumentResult>(), It.IsAny<OracleParameterCollection>()))
.Returns(Task.FromResult(this.PartnerTestDataModel.VatRegionDomains));

            domainDataRepository.Setup(x => x.GetProcedure2<TermsDomainModel>(It.IsAny<OracleObject>(), It.IsAny<PackageParams>(), It.IsAny<ServiceDocumentResult>(), It.IsAny<OracleParameterCollection>()))
.Returns(Task.FromResult(this.PartnerTestDataModel.TermsDomains));

            Mock<PartnerRepository> unitOfMeasureRepository = this.MockBaseOraclePackageRepository<PartnerRepository, PartnerModel>(this.PartnerTestDataModel.PartnerModels, new object[] { });
            unitOfMeasureRepository.Setup(x => x.SetParamsPartnerSearch(It.IsAny<PartnerModel>(), It.IsAny<OracleObject>()))
               .Verifiable();
            unitOfMeasureRepository.Setup(x => x.SetParamsPartnerSave(It.IsAny<PartnerModel>(), It.IsAny<OracleObject>()))
               .Verifiable();
            this.Controller = this.Setup<PartnerController>();
        }

        protected PartnerController Controller { get; set; }

        protected ServiceDocument<PartnerModel> ServiceDocument { get; set; }

        protected PartnerTestDataModel PartnerTestDataModel { get; set; }

        public override void ConfigureServices(string controllerName)
        {
            base.ConfigureServices("BrandCategory");
            this.CreateTestData(new List<string>(), new List<string> { "Security.json" });

            this.PartnerTestDataModel = this.CreateTestData<PartnerTestDataModel>(new List<string> { "Partner.json" }, "Partner");

            base.MockFxDbContext<BrandCategoryModel>("Journey");
        }

        protected override void ComponentStartups(List<ComponentStartup> startups)
        {
            base.ComponentStartups(startups);
            startups.Add(new MdmStartup());
        }

        protected override void InitializeMappings()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<FxMappingProfile>();
                cfg.AddProfile<SecurityMappingProfile>();

                cfg.AddProfile<UnitOfMeasureMapping>();
                cfg.AddProfile<UtilityDomainMapping>();
                cfg.AddProfile<MerchantHierarchyMapping>();
                cfg.AddProfile<OrganizationHierarchyMapping>();
                cfg.AddProfile<ValueAddedTaxMapping>();
                cfg.AddProfile<SeasonMapping>();
                cfg.AddProfile<DomainDataMapping>();
                cfg.AddProfile<SupplierModelMapping>();
                cfg.AddProfile<SupplierAddressModelMapping>();
                cfg.AddProfile<FavouriteMapping>();
                cfg.AddProfile<ItemListMapping>();
                cfg.AddProfile<CostZoneMapping>();
                cfg.AddProfile<PriceZoneMapping>();
                cfg.AddProfile<BrandMapping>();
                cfg.AddProfile<PartnerMapping>();
                this.ConfigureMapping(cfg);
            });
        }
    }
}
