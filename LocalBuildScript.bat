@echo off
SET msBuildLocation="C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\MSBuild\15.0\Bin\msbuild.exe"
SET /p solutionDirectory= "Enter solution directory :"
CD %solutionDirectory%
SET /p solutionname= "Enter solution name :"
echo %solutionDirectory%
echo
echo Building MS files for %solutionname% 

echo %msBuildLocation% 

call %msBuildLocation% %solutionDirectory%%solutionname%  /p:RunCodeAnalysis=true /m:4 /v:M /fl /flp:LogFile=%solutionname%.log;Verbosity=Normal /nr:false  /consoleloggerparameters:Summary;ShowTimestamp;ShowEventId;PerformanceSummary
echo -
echo --------Done building %solutionname% .--------------
echo -

